
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using jlib.db;
using jlib.functions;
using jlib.helpers;
using jlib.DataFramework;
using jlib.DataFramework.Cache;
using jlib.DataFramework.Cache.Attributes;
using jlib.DataFramework.Paging;

namespace Phoenix.LearningPortal.Data
{
	public partial class RowLinks
	{
		
	}

	public partial class RowLink
	{
		#region ***** ENUMS *****
			
		#endregion
	
		#region ***** CONSTRUCTORS *****      
        public RowLink(BaseTableRecordImpl<Phoenix.LearningPortal.Data.Context> Object1, BaseTableRecordImpl<Phoenix.LearningPortal.Data.Context> Object2) {
            this.Object1 = Object1;
            this.Object2 = Object2;
        }
		#endregion
	
		#region ***** PROPERTIES *****

		#endregion
	
		#region ***** METHODS *****
                
        BaseTableRecordImpl<Phoenix.LearningPortal.Data.Context> m_oObject1;
        public BaseTableRecordImpl<Context> Object1 {
            get {
                if (ID2 == 1 && m_oObject1 == null) return null;
                if (m_oObject1 == null)
                    m_oObject1 = BaseTableRecordImpl<Context>.instantiateObject(Table1, ID1);
                
                return m_oObject1;                                
            }
            set {
                if (value == null) {
                    this.ID1 = 0;
                    this.Table1 = "";
                } else {
                    this.ID1 = value._CurrentObjectID;
                    this.Table1 = value.TABLE_NAME;
                }
                m_oObject1 = value;
            }
        }

        BaseTableRecordImpl<Phoenix.LearningPortal.Data.Context> m_oObject2;
        public BaseTableRecordImpl<Context> Object2 {
            get {
                if (ID2 == 0 && m_oObject2==null) return null;
                if (m_oObject2 == null)
                    m_oObject2 = BaseTableRecordImpl<Context>.instantiateObject(Table2, ID2);                    

                return m_oObject2;
            }
            set {
                if (value == null) {
                    this.ID2 = 0;
                    this.Table2 = "";
                } else {
                    this.ID2 = value._CurrentObjectID;
                    this.Table2 = value.TABLE_NAME;
                }
                m_oObject2 = value;             
            }
        }

        public override void Save(SqlHelper SqlHelper, int AuthorID) {
            if (this.Fields[Folders.Columns.DeleteDate.PropertyName].RawValue == null) DeleteDate = DateTime.MaxValue;
            if (this.Fields[Folders.Columns.CreateDate.PropertyName].RawValue == null) CreateDate = DateTime.Now;
            if (m_oObject1 != null) this.ID1 = m_oObject1._CurrentObjectID;
            if (m_oObject2 != null) this.ID2 = m_oObject2._CurrentObjectID;
            base.Save(SqlHelper, AuthorID);
        }
        
		#endregion
	}
}