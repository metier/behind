using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using jlib.db;
using jlib.functions;
using jlib.helpers;
using jlib.DataFramework;
using jlib.DataFramework.Cache;
using jlib.DataFramework.Cache.Attributes;
using jlib.DataFramework.Paging;


namespace Phoenix.LearningPortal.Data
{
	public partial class Posts
	{
		
	}

	public partial class Post
	{
		#region ***** ENUMS *****
			
		#endregion
	
		#region ***** CONSTRUCTORS *****

		#endregion
	
		#region ***** PROPERTIES *****

		#endregion
	
		#region ***** METHODS *****
        public List<Phoenix.LearningPortal.Data.Folder> GetTags() {
            return this.GetChildren<Phoenix.LearningPortal.Data.Folder>().FindAll(x => { return x.Context_Value == "tag"; });
        }
        public void SetTags(List<string> TagIDs){
            this.GetTags().ForEach(Tag=>{
                //If it doesn't exist in list, remove it
                if(!TagIDs.Remove(Tag.ObjectID.Str())){
                    this.Children.ForEach(x => { if (x.ID2 == Tag.ObjectID && x.Table2 == "d_folders") x.TupleDelete(); this.Children.Remove(x); });                                
                }                        
            });
            TagIDs.ForEach(x => { if (!x.IsNullOrEmpty()) this.addChild("d_folders", x.Int()); });
        }
		#endregion
	}

}