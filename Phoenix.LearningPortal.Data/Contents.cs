
using System;
using System.Xml;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using jlib.db;
using jlib.functions;
using jlib.helpers;
using jlib.DataFramework;
using jlib.DataFramework.Cache;
using jlib.DataFramework.Cache.Attributes;
using jlib.DataFramework.Paging;


namespace Phoenix.LearningPortal.Data
{
	public partial class Contents
	{
        public enum Types {            
            HTML = 1,
            XML = 2,
            FileUrl = 3
        }
        public static QueryPackage GetTemporaryUserContent(int ObjectID, int UserID) {
            QueryPackage query = new QueryPackage();
            query.Statement =
            @"SELECT 
                TOP 1 * 
            FROM 
                [d_content] WITH (NOLOCK) 
            WHERE 
                [ObjectID] = @Objectid
                AND [ModifiedBy] = @UserID
                AND [CreateDate] = [DeleteDate]
            ORDER BY
                [DeleteDate] Desc";

            query.Parameters.ObjectID = ObjectID;
            query.Parameters.UserID = UserID;
            return query;
        }
        public static QueryPackage GetRevisions(int ObjectID) {
            //Build Package
            QueryPackage query = new QueryPackage();
            query.Statement = 
            @"SELECT 
                * 
            FROM 
                [d_content] WITH (NOLOCK) 
            WHERE 
                [ObjectID] = @Objectid";

            query.Parameters.ObjectID = ObjectID;
            return query;
        }
        public static QueryPackage GetRevisions(List<int> ObjectIDs) {
            //Build Package
            QueryPackage query = new QueryPackage();
            query.Statement =
            @"SELECT 
                * 
            FROM 
                [d_content] WITH (NOLOCK) 
            WHERE 
                [ObjectID] in (" + convert.cStr(ObjectIDs.Join(","),"-1") + ")";
            
            return query;
        }
	}

	public partial class Content
	{
		#region ***** ENUMS *****
			
		#endregion
	
		#region ***** CONSTRUCTORS *****
			
		#endregion
	
		#region ***** PROPERTIES *****
        public XmlDocument GetXmlDocument() {

            jlib.net.sgmlparser.SgmlReader oReader = new jlib.net.sgmlparser.SgmlReader();
            System.IO.StringReader oSReader = new System.IO.StringReader(this.Xml);
            oReader.InputStream = oSReader;
            oReader.DocType = "HTML";
            oReader.CaseFolding = jlib.net.sgmlparser.CaseFolding.ToLower;
            XmlDocument oDoc = new XmlDocument();
            oDoc.Load(oReader);
            return oDoc;
        }
        #endregion

        #region ***** METHODS *****
        public Content CloneContent()
        {            
            Content Content = this.Clone(DataRowVersion.Current, true) as Content;
            Content.ObjectID = 0;
            Content.CreateDate = DateTime.Now;
            Content.CreatedbyID = 0;
            Content.Fields[Phoenix.LearningPortal.Data.Contents.Columns.ObjectID.PropertyName].FieldStatus = FieldStatuses.NotSet;            
            Content.Fields[Phoenix.LearningPortal.Data.Contents.Columns.CreatedbyID.PropertyName].FieldStatus = FieldStatuses.NotSet;

            Content.RecordState = DataRowAction.Add;
            
            return Content;
        }
        #endregion
    }

}