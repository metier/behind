﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jlib.db;
using jlib.functions;
using jlib.helpers;
using jlib.DataFramework;
using jlib.DataFramework.Cache;
using jlib.DataFramework.Cache.Attributes;
using jlib.DataFramework.Paging;
using System.Reflection;

namespace Phoenix.LearningPortal.Data {
    public abstract class BaseTableRecordImpl<ContextType> : jlib.DataFramework.BaseTableRecord<ContextType> where ContextType : BaseContext, new() {

        //delete -- should also delete all children (unless they are linked to from other places?). Supply bHardDelete to indicate physical record deletion
        //save -- should save record. If bNewVersion, create new version. Should also save all new/changed Parents Children links
        private static object _ConstructorLock = new object();
        public BaseTableRecordImpl(ContextType Context)
            : base(Context) {

        }
        #region ***** PROPERTIES *****
        private RowLinks m_oParents;
        private object _ThreadLock = new object();
        public DateTime? _DeleteDate {
            get {
                object deleteDate = this.Fields.GetFieldValueByFieldName("DeleteDate");
                if (deleteDate != null) return deleteDate.Dte();
                return null;
            }
        }
        public int _CurrentObjectID {
            get {
                return this.Fields.FirstOrDefault(x=>  x.Value.IsPrimaryKey && (x.Value.DBType == DbType.Int16 || x.Value.DBType == DbType.Int32 || x.Value.DBType == DbType.Int64)).Value.RawValue.Int();
            }
        }
        [jlib.functions.json.DynamicJson.NoJSONEnumeration]
        public RowLinks Parents {
            get {
                
                if (m_oParents == null) {
                    lock (_ThreadLock) {
                        if (_CurrentObjectID == 0 || _CurrentObjectID == -1) m_oParents = new RowLinks();
                        else {
                            m_oParents = RowLinks.GetByField(Table2: this.TABLE_NAME, ID2: _CurrentObjectID).OrderBy(RowLinks.Columns.Sequence).Execute();
                            m_oParents.ForEach(x => { x.Object2 = this as Phoenix.LearningPortal.Data.BaseTableRecordImpl<Phoenix.LearningPortal.Data.Context>; });
                        }
                    }
                }
                return m_oParents;
            }
            set {
                m_oParents = value;
            }
        }

        private RowLinks m_oChildren;
        [jlib.functions.json.DynamicJson.NoJSONEnumeration]
        public RowLinks Children {
            get {
                if (m_oChildren == null) {                    
                    lock (_ThreadLock) {
                        if (_CurrentObjectID == 0 || _CurrentObjectID == -1) m_oChildren = new RowLinks();
                        else {
                            m_oChildren = RowLinks.GetByField(Table1: this.TABLE_NAME, ID1: _CurrentObjectID).OrderBy(RowLinks.Columns.Sequence).OrderBy(RowLinks.Columns.ID2).Execute();
                            m_oChildren.ForEach(x => { x.Object1 = this as Phoenix.LearningPortal.Data.BaseTableRecordImpl<Phoenix.LearningPortal.Data.Context>; });
                        }
                    }
                }
                return m_oChildren;
            }
        }
        
        public List<T> GetParents<T>() where T: Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context>{
            List<T> List = new List<T>();
            Type oType = typeof(T);            
            foreach(RowLink Link in Parents){
                if (Link.Object1 != null && Link.Object1.GetType() == oType) List.Add((T)Link.Object1);
            }
            
            return List;
        }
        public List<T> GetChildren<T>() where T : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> {
            List<T> List = new List<T>();
            Type oType = typeof(T);
            foreach (RowLink Link in Children) {
                if (Link.Object2 != null && Link.Object2.GetType() == oType) List.Add((T)Link.Object2);
            }

            return List;
        }

        public IEnumerable<T> GetAllParents<T>(int maxIterations) where T : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> {            
            for (int index = 0; index < GetParents<T>().Count; ++index) {
                yield return GetParents<T>()[index];
                if (maxIterations != 0) {
                    foreach (var item in GetParents<T>()[index].GetAllParents<T>(maxIterations - 1))
                        yield return item;
                }
            }
        }

        public IEnumerable<T> GetAllParents<T>(int maxIterations, Func<T, bool> checkFunction) where T : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> {
            for (int index = 0; index < GetParents<T>().Count; ++index) {
                T obj=GetParents<T>()[index];
                if (checkFunction(obj)) 
                    yield return obj;
                if (maxIterations != 0) {
                    foreach (var item in obj.GetAllParents<T>(maxIterations - 1, checkFunction))
                        if (checkFunction(item)) {
                            yield return item;
                        }
                }
            }
        }

        public T GetParentUntil<T>(int maxIterations, Func<T, bool> checkFunction) where T : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> {       
            for (int index = 0; index < GetParents<T>().Count; ++index) {
                T obj = GetParents<T>()[index];
                if (checkFunction(obj))
                    return obj;
                if (maxIterations != 0) {
                    obj=obj.GetParentUntil<T>(maxIterations - 1, checkFunction);
                    if (obj!=null) 
                        return obj;                                            
                }
            }
            return null;
        }
        //allParents
        public List<T> GetPathUntil<T>(int maxIterations, Func<T, bool> checkFunction) where T : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context>
        {            
            for (int index = 0; index < GetParents<T>().Count; ++index)
            {
                T obj = GetParents<T>()[index];
                if (checkFunction(obj))
                {
                    var path= new List<T> { obj };
                    return path;
                }                    
                if (maxIterations != 0)
                {
                    var path = obj.GetPathUntil<T>(maxIterations - 1, checkFunction);
                    if (path != null)
                    {
                        path.Add(obj);
                        return path;
                    }
                }
            }
            return null;
        }

        public List<BaseTableRecordImpl<Context>> CloneChildren(bool Recursive, bool Settings, bool LinkContent, int AuthorID)
        {
            List<BaseTableRecordImpl<Context>> Children = new List<BaseTableRecordImpl<Context>>();
            this.GetChildren<Folder>().ForEach(x => {
                Children.Add(x.CloneFolder(Recursive, Settings, LinkContent, AuthorID) as Folder);
            });
            this.GetChildren<Content>().ForEach(x => {
                if (LinkContent) Children.Add(x);
                else {
                    Content Content = x.Clone(DataRowVersion.Current, true) as Content;
                    Content.ObjectID = 0;
                    Content.Fields[Phoenix.LearningPortal.Data.Contents.Columns.ObjectID.FieldName].FieldStatus = FieldStatuses.NotSet;
                    Content.RecordState = DataRowAction.Add;
                    Content.Save();
                    Children.Add(Content);
                }
            });
            return Children;
        }

        private Settings m_oSettings;
        [jlib.functions.json.DynamicJson.NoJSONEnumeration]
        public Settings Settings {
            get {
                if (m_oSettings == null) {
                    lock (_ThreadLock) {
                        if (_CurrentObjectID == 0 || _CurrentObjectID == -1) m_oSettings = new Data.Settings();
                        //else if (_DeleteDate != null) m_oSettings = Data.Settings.GetByDate(this.TABLE_NAME, _CurrentObjectID, _DeleteDate.Value).Execute();
                        else m_oSettings = Data.Settings.GetByField(TableName: this.TABLE_NAME, ObjectID: _CurrentObjectID).Execute();
                    }
                }
                return m_oSettings;
            }
        }

        private static Dictionary<string, jlib.DataFramework.BaseTableRecord<Phoenix.LearningPortal.Data.Context>> m_oDataTypes;
        private static Dictionary<string, MethodInfo> m_oDataConstructors;
        public static MethodInfo getDataConstructor(string sTableName) {
            lock (_ConstructorLock) {
                if (m_oDataConstructors == null) getDataType(sTableName);
                return m_oDataConstructors.SafeGetValue(sTableName);
            }
        }
        public static jlib.DataFramework.BaseTableRecord<Phoenix.LearningPortal.Data.Context> getDataType(string sTableName) {
            lock (_ConstructorLock) {
                if (m_oDataTypes == null) {
                    m_oDataTypes = new Dictionary<string, jlib.DataFramework.BaseTableRecord<Phoenix.LearningPortal.Data.Context>>();
                    m_oDataConstructors = new Dictionary<string, MethodInfo>();
                    Type[] oTypes = System.Reflection.Assembly.GetAssembly(typeof(Phoenix.LearningPortal.Data.Attempts)).GetExportedTypes();
                    foreach (Type oType in oTypes) {
                        if (oType.BaseType.BaseType == typeof(BaseTableRecordImpl<Phoenix.LearningPortal.Data.Context>)) {
                            ConstructorInfo oConstructor = oType.GetConstructor(new Type[] { });
                            jlib.DataFramework.BaseTableRecord<Phoenix.LearningPortal.Data.Context> oObject = oConstructor.Invoke(new object[] { }) as BaseTableRecordImpl<Phoenix.LearningPortal.Data.Context>;
                            m_oDataTypes.Add(oObject.TABLE_NAME, oObject);
                            MethodInfo Constructor = oObject.GetType().GetMethod("LoadByPk", BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy, null, new Type[] { typeof(int) }, null);
                            //if (Constructor == null) Constructor = oObject.GetType().GetMethod("LoadByPk", BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy, null, new Type[] { typeof(int), typeof(DateTime), typeof(DateTime) }, null);
                            m_oDataConstructors.Add(oObject.TABLE_NAME, Constructor);
                            Constructor = oObject.GetType().GetMethod("LoadByPk", BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy, null, new Type[] { typeof(int), typeof(DateTime) }, null);
                            if (Constructor != null) m_oDataConstructors.Add(oObject.TABLE_NAME + ":versioned", Constructor);
                        }
                    }                    
                }
                return m_oDataTypes.SafeGetValue(sTableName);
            }
        }

        #endregion

        #region ***** METHODS *****
        public static BaseTableRecordImpl<Context> instantiateObject(string Key) {
            lock (_ConstructorLock) {
                object[] Arguments = (Key.Split(':').Length == 2 ? new object[] { parse.splitValue(Key, ":", 1).Int() } : new object[] { parse.splitValue(Key, ":", 1).Int(), new DateTime(parse.splitValue(Key, ":", 2).Lng()) });
                return getDataConstructor(parse.splitValue(Key, ":", 0) + (Arguments.Length == 2 ? ":versioned" : "")).Invoke(getDataType(parse.splitValue(Key, ":", 0)).GetType(), Arguments) as BaseTableRecordImpl<Phoenix.LearningPortal.Data.Context>;
            }
        }
        public static BaseTableRecordImpl<Context> instantiateObject(string TableName, int ID) {
            lock (_ConstructorLock) {
                MethodInfo constructor = getDataConstructor(TableName);
                if (constructor == null) return null;
                return constructor.Invoke(getDataType(TableName).GetType(), new object[] { ID }) as BaseTableRecordImpl<Phoenix.LearningPortal.Data.Context>;
            }
        }
        public void addParent(BaseTableRecordImpl<Context> oObject) {            
            foreach (RowLink Link in Parents)
                if (oObject._CurrentObjectID > 0 && Link.ID1 == oObject._CurrentObjectID && Link.Table1 == oObject.TABLE_NAME) return;

            RowLink Link1 = new RowLink();
            Link1.Object1 = oObject;
            Link1.Object2 = this as BaseTableRecordImpl<Context>;
            if(oObject.Children.Count>0) Link1.Sequence = oObject.Children.Last().Sequence + 1;
            lock (m_oParents) m_oParents.Add(Link1);
        }
        public void addChild(string TableName, int ObjectID) {
            if (ObjectID < 1) throw new Exception("Can't add child to parent where ObjectID<1");
            foreach (RowLink Link in Children)
                if (ObjectID > 0 && Link.ID2 == ObjectID && Link.Table2 == TableName) return;
            RowLink RowLink = new RowLink() { Table2 = TableName, ID2 = ObjectID };
            RowLink.Object1 = this as BaseTableRecordImpl<Phoenix.LearningPortal.Data.Context>;
            Children.Add(RowLink);
        }
        public void addChild(BaseTableRecordImpl<Context> oObject, BaseTableRecordImpl<Context> beforeChild) {
            if (oObject != null && beforeChild != null)
                addChild(oObject, GetChildIndex(beforeChild));
        }
        public void addChild(BaseTableRecordImpl<Context> oObject) {
            if(oObject!=null) addChild(oObject, -1);
        }
        public void addChild(BaseTableRecordImpl<Context> oObject, int iIndex) {
            if (oObject != null) {
                RowLink Link1 = null;
                foreach (RowLink Link in Children)
                    if (oObject._CurrentObjectID > 0 && Link.ID2 == oObject._CurrentObjectID && Link.Table2 == oObject.TABLE_NAME)
                    {
                        if (iIndex < 0 || Children.IndexOf(Link)==iIndex) return;
                        Link1 = Link;
                        Link1.Sequence = iIndex;
                        Children.Remove(Link1);
                        Children.Insert(Math.Min(iIndex, Children.Count-1), Link1);
                    }

                //If no link exists
                if (Link1 == null)
                {
                    Link1 = new RowLink();
                    Link1.Object1 = this as BaseTableRecordImpl<Context>;
                    Link1.Object2 = oObject;
                    if (iIndex > -1 && iIndex < m_oChildren.Count)
                    {
                        Link1.Sequence = iIndex;
                    }
                    else
                    {
                        Link1.Sequence = m_oChildren.Count;
                    }
                    lock (m_oChildren) m_oChildren.Insert(Link1.Sequence, Link1);
                }
                if (Link1.Sequence == iIndex) {
                    for (int x = 0; x < m_oChildren.Count; x++) {
                        m_oChildren[x].Sequence = x;
                    }
                }
            }
        }
        public void addChildren(List<BaseTableRecordImpl<Context>> Children) {
            Children.ForEach(x=> this.addChild(x));
        }
        public int GetChildIndex(BaseTableRecordImpl<Context> oObject) {
            RowLink link = this.Children.FirstOrDefault(x=>{return x.ID2==oObject._CurrentObjectID && x.Table2 == oObject.TABLE_NAME;});
            if(link==null)return -1;
            return this.Children.IndexOf(link);
        }
        public Setting getSettingObject(string sKey) {
            return Settings.FirstOrDefault(x => x.KeyName == sKey);
        }
        public string getSetting(string sKey) {
            Setting Current = getSettingObject(sKey);
            if (Current == null) return null;
            return Current.Value;
        }
        public void setSetting(string sKey, string sValue) {
            Setting Current = getSettingObject(sKey);
            if (Current == null) {
                Current = new Setting();
                Current.KeyName = sKey;                
                Current.TableName = this.TABLE_NAME;
                Settings.Add(Current);
            }
            Current.Value = sValue;
        }

        public void TupleDelete() {
            this.TupleDelete(0);
        }
        public void TupleDelete(int authorId) {
            this.TupleDelete(this.Context.GetSqlHelper(), authorId);
        }
        public void TupleDelete(SqlHelper SqlHelper, int AuthorID) {
            if (this.Fields.ContainsFieldName("CreateDate") && this.Fields.ContainsFieldName("DeleteDate")) {                
                BaseTableRecordImpl<Context> NewObject = this.Clone(DataRowVersion.Original) as BaseTableRecordImpl<Context>;
                NewObject.Fields.GetByFieldName("DeleteDate").SetChangedValue(DateTime.Now);
                if (NewObject.Fields.ContainsFieldName("ModifiedBy")) NewObject.Fields.GetByFieldName("ModifiedBy").SetChangedValue(AuthorID);
                NewObject.RecordState = DataRowAction.Change;
                NewObject.BaseSave(SqlHelper, AuthorID);                
            } else {
                base.Delete(SqlHelper, AuthorID);
            }
        }
        private void BaseSave(SqlHelper SqlHelper, int AuthorID) {
            base.Save(SqlHelper, AuthorID, null);
        }
        public class CMSPath {
            public CMSPath(string Path, string Context, string ID) {
                this.Path = Path;
                this.Context = Context;
                this.ID = ID;
            }
            public string Path { get; set; }
            public string Context { get; set; }
            public string ID { get; set; }
        }
        private CMSPath _Path;
        public CMSPath getPath() {
            if (_Path == null) {
                _Path = new CMSPath("","","");
                Folder Parent = this as Folder;
                if (Parent == null) { 
                    RowLink ParentLink = this.Parents.FirstOrDefault(x => x.Table1 == "d_folders");
                    Parent = (ParentLink != null ? ParentLink.Object1 : null) as Folder;
                }
                if (Parent != null) {
                    DataTable DTPath = this.Context.GetSqlHelper().Execute("select * from fn_get_path(@ObjectID,@ObjectDate)", "@ObjectID", Parent._CurrentObjectID, "@ObjectDate", this.Context.VersionDate);
                    if (DTPath.Rows.Count > 0 && DTPath.Rows[0]["ID"].Str()!="") _Path = new CMSPath(DTPath.Rows[0]["Path"].Str(), DTPath.Rows[0]["Context"].Str(), DTPath.Rows[0]["ID"].Str());                    
                }
            }            
            return _Path;
        }
        private JDataTable _Paths;
        public JDataTable getPaths() {
            if (_Paths == null) {
                _Paths = new JDataTable();
                _Paths.Columns.AddRange(new DataColumn[] { new DataColumn("Path"), new DataColumn("Context"), new DataColumn("ID") });
                List<Folder> Parents = this.GetParents<Folder>();
                foreach(Folder Parent in Parents){
                    DataTable DTPath = this.Context.GetSqlHelper().Execute("select * from fn_get_path(@ObjectID,@ObjectDate)", "@ObjectID", Parent._CurrentObjectID, "@ObjectDate", this.Context.VersionDate);
                    for (int x = 0; x < DTPath.Rows.Count; x++)
                        if(DTPath.Rows[x]["ID"].Str()!="")_Paths.Rows.Add(sqlbuilder.setRowValues(_Paths.NewRow(), "Path", DTPath.Rows[x]["Path"], "Context", DTPath.Rows[x]["Context"], "ID", DTPath.Rows[x]["ID"]));
                        //_Paths.Add(new CMSPath(DTPath.Rows[x]["Path"].Str(), DTPath.Rows[x]["Context"].Str(), DTPath.Rows[x]["ID"].Str()));
                }
            }
            return _Paths;
        }

        public override void Save(int AuthorId)
        {
            SqlHelper sqlHelper = this.Context.GetCurrentTransactionOrCreate();
            this.Save(sqlHelper, AuthorId, null);
        }        
        public override void Save(SqlHelper SqlHelper, int AuthorID, SaveOptions Options=null) {
            bool bSaveAsVersion = false;
            if (this.Fields.ContainsFieldName("CreateDate")) {
                if (this.Fields.GetByFieldName("CreateDate").RawValue.Date() <= new DateTime(1900, 1, 1)) {
                    this.Fields.GetByFieldName("CreateDate").SetChangedValue(DateTime.Now);
                    if (this.Fields.ContainsFieldName("CreatedBy")) this.Fields.GetByFieldName("CreatedBy").SetChangedValue(AuthorID);
                    if (this.Fields.ContainsFieldName("DeleteDate") && this.Fields.GetByFieldName("DeleteDate").FieldStatus!= FieldStatuses.Modified) this.Fields.GetByFieldName("DeleteDate").SetChangedValue(DateTime.MaxValue);
                }
                if (this.Fields.ContainsFieldName("ObjectID")) {
                    if (this.Fields.GetByFieldName("ObjectID").FieldStatus == FieldStatuses.NotSet) {
                        this.Fields.GetByFieldName("ObjectID").SetChangedValue(Phoenix.LearningPortal.Data.Context.nextObjectID());
                    } else if(this.RecordState!= DataRowAction.Add) {
                        bSaveAsVersion = Context.TupleVersioning;
                    }
                }
            }
            if (this.RecordState != DataRowAction.Nothing) {
                if (this.Fields.ContainsFieldName("ModifiedBy")) this.Fields.GetByFieldName("ModifiedBy").SetChangedValue(AuthorID);

                //Log changes
                if (AuthorID > 0) {
                    Data.Revision revision = new Data.Revision();
                    revision.Import(this, AuthorID);
                    revision.Save();
                }

                if (bSaveAsVersion) {

                    this.TupleDelete(SqlHelper, AuthorID);
                    this.RecordState = DataRowAction.Add;
                    this.Fields.GetByFieldName("CreateDate").SetChangedValue(DateTime.Now);
                    if (this.Fields.ContainsFieldName("CreatedBy")) this.Fields.GetByFieldName("CreatedBy").SetChangedValue(AuthorID);
                    //in case we're saving an old version, make sure we make this version current
                    //this.Fields.GetByFieldName("DeleteDate").SetChangedValue(DateTime.MaxValue);
                    foreach (KeyValuePair<string, IField> Field in this.Fields)
                        if (Field.Value.RawValue != null && !Field.Value.FieldName.StartsWith("Paging.")) Field.Value.FieldStatus = FieldStatuses.Modified;

                    //Update DeleteDate of original record
                    //this.RecordState = DataRowState.Added; //will this work?                
                }
                if(this.TABLE_NAME!="d_users") base.Save(SqlHelper, AuthorID, null);
            }
            if (m_oParents != null) {
                lock(m_oParents){
                    foreach (RowLink Link in m_oParents) {
                        if (Link.RecordState != DataRowAction.Nothing) {
                            Link.ID2 = this._CurrentObjectID;
                            if (Link.ID1 < 1 && Link.Object1 != null) Link.ID1 = Link.Object1._CurrentObjectID;
                            Link.Save();
                        }
                    }
                }
            }
            if (m_oChildren != null) {
                lock(m_oChildren){
                    foreach (RowLink Link in m_oChildren) {
                        if (Link.RecordState != DataRowAction.Nothing) {
                            Link.ID1 = this._CurrentObjectID;
                            if (Link.ID2 < 1 && Link.Object2 != null) Link.ID2 = Link.Object2._CurrentObjectID;
                            Link.Save();
                        }
                    }
                }
            }
            if (m_oSettings != null) {
                lock (m_oSettings) {
                    foreach (Setting Setting in m_oSettings) {
                        if (Setting.RecordState != DataRowAction.Nothing) {                            
                            Setting.ObjectID = this._CurrentObjectID;
                            if (_DeleteDate != null) Setting.DeleteDate = _DeleteDate.Value;
                            Setting.Save();
                        }
                    }
                }
            }
        }

        public string ToJson(bool IncludeSettings, jlib.functions.json.JsonWriter Writer = null, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null, int MaxObjectLevelDepth = 1) {
            //Create JsonWriter
            if (Writer == null) Writer = new jlib.functions.json.JsonWriter();

            //Start Object
            Writer.WriteObjectStart();

            //Object Type
            Writer.WritePropertyNameValue("_Type", this.GetType().Name);

            //For each field
            foreach (IField field in this.Fields.Values) {
                if (
                    !String.IsNullOrWhiteSpace(field.PropertyName)
                    && (IncludeColumns == null || IncludeColumns.Count == 0 || IncludeColumns.Any(x => x.PropertyName == field.PropertyName))
                    && (ExcludeColumns == null || !ExcludeColumns.Any(x => x.PropertyName == field.PropertyName))
                    )
                    Writer.WritePropertyNameValue(field.PropertyName, field.RawValue);
            }
            if (IncludeSettings && this.Settings.Count > 0) {
                //Writer.WriteObjectStart();
                Writer.WritePropertyName("Settings");
                this.Settings.ToJson(Writer, new List<IColumn>() { Phoenix.LearningPortal.Data.UserSettings.Columns.KeyName, Phoenix.LearningPortal.Data.UserSettings.Columns.Value });
                //Writer.WriteObjectEnd();
            }
            if(MaxObjectLevelDepth == 0 || MaxObjectLevelDepth > 1)
            {
                if (MaxObjectLevelDepth > 1)
                    MaxObjectLevelDepth--;

                Writer.WritePropertyName("Children");
                Writer.WriteArrayStart();
                foreach(var Child in this.Children)
                {                    
                    Child.Object2.ToJson(IncludeSettings, Writer, IncludeColumns, ExcludeColumns, MaxObjectLevelDepth);                    
                }
                Writer.WriteArrayEnd();
            }

            //End Object
            Writer.WriteObjectEnd();

            //Return string
            return Writer.ToString();
        }
        #endregion
    }
}
