﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using jlib.db;
using jlib.functions;
using jlib.helpers;
using jlib.DataFramework;

namespace Phoenix.LearningPortal.Data {
    public partial class Context {
        public Context() : base(jlib.db.SqlHelper.ConnectionTypes.MSSQL) { }
        public override SqlHelper GetSqlHelper() {
            //Return Sql Helper
            SqlHelper oSqlHelper=SqlHelper.SqlHelperCreateConnectionStrings("Attack.dsn");
            oSqlHelper.ConnectionType = this.ConnectionType;
            return oSqlHelper;
        }

        static string[] m_sVersionedTables = new string[] { "d_content", "d_exam_sections", "d_exams", "d_folders", "d_question_options", "d_questions" };//, "l_row_links" 
        static DateTime m_oLastRequestDate = DateTime.MinValue;
        static readonly object m_oNextObjectIDLocker = new object(), m_oLastRequestDateLocker = new object();
        static int m_iObjectID = -1;
        public static int nextObjectID() {
            lock (m_oNextObjectIDLocker) {
                if (m_iObjectID == -1) {                    
                    ado_helper oData = new ado_helper("sql.dsn.cms");
                    m_iObjectID = sqlbuilder.getDataTable(oData, "select max(ObjectID) from (" + parse.stripCharacter(convert.cJoinModelString("select max(ObjectID) ObjectID from {0} union ", m_sVersionedTables).Trim(), "union") + ") a").SafeGetValue(0, 0).Int();
                }
                m_iObjectID++;
                return m_iObjectID;
            }
        }
        //This method is to assure that the date on the server has not been changed back in time
        public static void incrementRequestDate() {
            lock (m_oLastRequestDateLocker) {
                if (m_oLastRequestDate == DateTime.MinValue) {
                    if (convert.cStr(System.Configuration.ConfigurationManager.AppSettings["sql.dsn.cms"]) != "") {
                        ado_helper oData = new ado_helper("sql.dsn.cms");
                        m_oLastRequestDate = sqlbuilder.getDataTable(oData, "select max(CreateDate) from (" + parse.stripCharacter(convert.cJoinModelString("select max(CreateDate) CreateDate from {0} union ", m_sVersionedTables).Trim(), "union") + ") a").SafeGetValue(0, 0).Dte();
                    }
                }
                if (m_oLastRequestDate > DateTime.Now)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["smtp.server"].IsNotNullOrEmpty())
                        jlib.net.SMTP.sendEmail("noreply@metier.no", "jorgen@vontangen.com", "WARNING: The date on the server has been tampered with", "Old value: " + m_oLastRequestDate + "\nCurrent time: " + DateTime.Now, System.Configuration.ConfigurationManager.AppSettings["smtp.server"]);
                    //throw new Exception("The date on the server has been tampered with. Execution halted.");
                }
                m_oLastRequestDate = DateTime.Now;
            }
        }
    }
}
