using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using jlib.db;
using jlib.functions;
using jlib.helpers;
using jlib.DataFramework;
using jlib.DataFramework.Cache;
using jlib.DataFramework.Cache.Attributes;
using jlib.DataFramework.Paging;


namespace Phoenix.LearningPortal.Data {
    public partial class Revisions {

    }

    public partial class Revision {
        #region ***** ENUMS *****

        #endregion

        #region ***** CONSTRUCTORS *****

        #endregion

        #region ***** PROPERTIES *****
        private DataRowState? _RowState = null;
        public DataRowState RowState {
            get {
                if (this._RowState == null) {
                    if (!String.IsNullOrEmpty(this.Data)) {
                        XmlDocument xmlData = new XmlDocument();
                        xmlData.LoadXml(this.Data);

                        //Get Row State
                        this._RowState = (DataRowState)xmlData.DocumentElement.GetAttribute("s").Int();
                    } else
                        this._RowState = DataRowState.Modified;
                }

                return this._RowState.Value;
            }
            set {
                this._RowState = value;
            }
        }

        private List<FieldRevision> _FieldRevisions = null;
        public List<FieldRevision> FieldRevisions {
            get {
                if (this._FieldRevisions == null) {
                    //Create New Collection
                    this._FieldRevisions = new List<FieldRevision>();

                    //Load From Xml
                    if (!String.IsNullOrEmpty(this.Data)) {
                        XmlDocument xmlData = new XmlDocument();
                        xmlData.LoadXml(this.Data);

                        //Get All Fields
                        XmlNodeList nodes = xmlData.SelectNodes("//f");

                        //Add to collection
                        foreach (XmlNode node in nodes) {
                            FieldRevision fieldRevision = new FieldRevision();
                            fieldRevision.PropertyName = xml.getXmlAttributeValue(node, "n");
                            fieldRevision.OriginalValue = xml.getXmlNodeTextValue(node, "o");
                            fieldRevision.NewValue = xml.getXmlNodeTextValue(node, "c");

                            this._FieldRevisions.Add(fieldRevision);
                        }
                    }
                }

                return this._FieldRevisions;
            }
        }
        #endregion

        #region ***** METHODS *****
        public void Import(IRecord Record, int AuthorID) {
            Import(Record, AuthorID, false);
        }
        public void Import(IRecord Record, int AuthorID, bool SaveAllFields) {
            //Set Row State
            this.RowState = (Record.RecordState == DataRowAction.Add ? DataRowState.Added : Record.RecordState == DataRowAction.Nothing ? DataRowState.Unchanged : DataRowState.Modified);
            this.TableName = Record.DbEntityName;

            //Set Author
            this.UserID = AuthorID;

            //Set PK Value
            this.PkID = Record.Fields.Values.First(field => field.IsPrimaryKey).RawValue.Int();

            //Create Field Revisions Collection
            this._FieldRevisions = new List<FieldRevision>();

            //Import Field Revisions
            if (RowState == DataRowState.Modified || SaveAllFields) {

                //Get Changed Fields
                foreach (IField field in Record.Fields.Values.Where(field => SaveAllFields || field.FieldStatus == FieldStatuses.Modified)) {
                    bool bDateField = (field.DBType == DbType.DateTime || field.DBType == DbType.Date || field.DBType == DbType.DateTime2);
                    object oOriginalValue = (bDateField && (field.OriginalRawValue == null || (DateTime)field.OriginalRawValue == DateTime.MinValue) ? null : field.OriginalRawValue);
                    object oNewValue = (bDateField && (field.RawValue == null || (DateTime)field.RawValue == DateTime.MinValue) ? null : field.RawValue);

                    if (oOriginalValue == null && oNewValue == null) continue;
    
                    this._FieldRevisions.Add(new FieldRevision() {
                        PropertyName = field.PropertyName,
                        OriginalValue = oOriginalValue.Str(),
                        NewValue = oNewValue.Str()

                    });


                }


            }


        }
        public override void Save() {
            //No need to save if no changes are made
            if (this._FieldRevisions != null && this._FieldRevisions.Count > 0) {
                //Map Data Field                
                XmlDocument xmlData = new XmlDocument();
                xmlData.LoadXml("<r s=\"" + (int)this.RowState + "\"></r>");

                //Add Revision Entries
                this._FieldRevisions.ForEach(revision => {
                    XmlNode xmlField = xml.setXmlAttributeValue(xml.addXmlElement(xmlData.DocumentElement, "f"), "n", revision.PropertyName);
                    if (revision.OriginalValue != revision.NewValue) xml.addXmlElement(xmlField, "o", revision.OriginalValue);
                    xml.addXmlElement(xmlField, "c", revision.NewValue);
                });

                //Output to Data Field
                this.Data = xmlData.OuterXml;

                //Save
                base.Save();
            }

        }
        #endregion
    }

    public partial class FieldRevision {
        public string PropertyName { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
    }
}
