

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using jlib.db;
using jlib.functions;
using jlib.helpers;
using jlib.DataFramework;
using jlib.DataFramework.Cache;
using jlib.DataFramework.Cache.Attributes;
using jlib.DataFramework.Paging;

namespace Phoenix.LearningPortal.Data
{

	
				public partial class Context : jlib.DataFramework.BaseContext 
				{  
					[ThreadStatic]
					public static SqlHelper CurrentTransaction = null;
					public override SqlHelper GetCurrentTransaction()
					{
						return Context.CurrentTransaction;
					}
					public override void SetCurrentTransaction(SqlHelper Sql)
					{
						Context.CurrentTransaction = Sql;
					}

				}
	

	//Typed Collection
	public partial class AttemptStatuses : AttemptStatuses<AttemptStatuses, AttemptStatus>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates AttemptStatuses
		/// </summary>
		public AttemptStatuses() : this(new Context()) {}
		
		/// <summary>
		/// Creates AttemptStatuses
		/// </summary>
		public AttemptStatuses(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class AttemptStatuses<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: AttemptStatus<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		public enum AttemptStatusTypes : int
		{
				/// <summary>
				/// Not Attempted : 0
				/// </summary>
				NotAttempted = 0,
				/// <summary>
				/// Incomplete : 1
				/// </summary>
				Incomplete = 1,
				/// <summary>
				/// Completed : 2
				/// </summary>
				Completed = 2,
			
			/// <summary>
			/// Default Enum Value, meaning not set or not recognized
			/// </summary>
			__DEFAULT = -1
		}
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Name = new jlib.DataFramework.BaseColumn<string>(FieldName: "name", PropertyName: "Name", ParameterName: "@Name", PkParameterName: "@PK_Name", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates AttemptStatuses
		/// </summary>
		public AttemptStatuses() : this(new Context()) {}
		
		/// <summary>
		/// Creates AttemptStatuses
		/// </summary>
		public AttemptStatuses(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "a_attempt_statuses";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = AttemptStatuses<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("Name", this.Name = AttemptStatuses<DataCollectionType, DataRecordType>.Columns.Name);
			
			//Call the secondary constructor method
			this.AttemptStatusesEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void AttemptStatusesEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<string> Name { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [a_attempt_statuses] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [a_attempt_statuses] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("a_attempt_statuses");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("a_attempt_statuses", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, string Name = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(Name != null)
			{
				query.Parameters.Name = Name;
				filterConditions.Add(@"[name] = @Name");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [a_attempt_statuses] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<AttemptStatuses<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new AttemptStatuses<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<AttemptStatuses<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new AttemptStatuses<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [a_attempt_statuses] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new AttemptStatusesDataReader(this);
		}
		
		public List<DtoAttemptStatus> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class AttemptStatusesDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public AttemptStatusesDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoAttemptStatus
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		Name Property
		/// </summary>
		[DataMember]
		public virtual string Name { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class AttemptStatus : AttemptStatus<AttemptStatus>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates AttemptStatus
		/// </summary>
		public AttemptStatus() : this(new Context()) {}
		
		/// <summary>
		/// Creates AttemptStatus
		/// </summary>
		public AttemptStatus(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for AttemptStatus
		/// </summary>
		protected AttemptStatus(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class AttemptStatus<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: AttemptStatus<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates AttemptStatus
		/// </summary>
		public AttemptStatus() : this(new Context()) {}
		
		/// <summary>
		/// Creates AttemptStatus
		/// </summary>
		public AttemptStatus(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "a_attempt_statuses";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, AttemptStatuses.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Name" ,this._Name = new jlib.DataFramework.BaseField<string>(this, AttemptStatuses.Columns.Name, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.AttemptStatusEx();
		}
		
		/// <summary>
		/// Deserialization method for AttemptStatus
		/// </summary>
		protected AttemptStatus(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void AttemptStatusEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "a_attempt_statuses", FieldName: "ID", IsPrimaryKey=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		Name Property
		///		Database Field:		name;
		///		Database Parameter:	@Name;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "a_attempt_statuses", FieldName: "name", IsNullable=true)]
		public virtual string Name
		{
			get 
			{ 
				return (string)this._Name.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Name.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Name.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Name { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoAttemptStatus Dto)
		{
			//Set Values
			this._ID.SetChangedValueIfDifferent(Dto.ID);
			this._Name.SetChangedValueIfDifferent(Dto.Name);
		}
		public virtual DtoAttemptStatus ToDto()
		{
			return this.ToDto(new DtoAttemptStatus());
		}
		
		public virtual DtoAttemptStatus ToDto(DtoAttemptStatus dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.Name = this.Name;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[a_attempt_statuses] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[a_attempt_statuses]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class AttemptAnswers : AttemptAnswers<AttemptAnswers, AttemptAnswer>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates AttemptAnswers
		/// </summary>
		public AttemptAnswers() : this(new Context()) {}
		
		/// <summary>
		/// Creates AttemptAnswers
		/// </summary>
		public AttemptAnswers(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class AttemptAnswers<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: AttemptAnswer<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> AttemptID = new jlib.DataFramework.BaseColumn<int>(FieldName: "attempt_id", PropertyName: "AttemptID", ParameterName: "@AttemptID", PkParameterName: "@PK_AttemptID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> QuestionID = new jlib.DataFramework.BaseColumn<int>(FieldName: "question_id", PropertyName: "QuestionID", ParameterName: "@QuestionID", PkParameterName: "@PK_QuestionID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> QuestionOrder = new jlib.DataFramework.BaseColumn<int>(FieldName: "question_order", PropertyName: "QuestionOrder", ParameterName: "@QuestionOrder", PkParameterName: "@PK_QuestionOrder", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> TimeSpent = new jlib.DataFramework.BaseColumn<int>(FieldName: "time_spent", PropertyName: "TimeSpent", ParameterName: "@TimeSpent", PkParameterName: "@PK_TimeSpent", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<decimal> Points = new jlib.DataFramework.BaseColumn<decimal>(FieldName: "points", PropertyName: "Points", ParameterName: "@Points", PkParameterName: "@PK_Points", DBType: DbType.Currency, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Answer = new jlib.DataFramework.BaseColumn<string>(FieldName: "answer", PropertyName: "Answer", ParameterName: "@Answer", PkParameterName: "@PK_Answer", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<bool> IsCorrect = new jlib.DataFramework.BaseColumn<bool>(FieldName: "is_correct", PropertyName: "IsCorrect", ParameterName: "@IsCorrect", PkParameterName: "@PK_IsCorrect", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates AttemptAnswers
		/// </summary>
		public AttemptAnswers() : this(new Context()) {}
		
		/// <summary>
		/// Creates AttemptAnswers
		/// </summary>
		public AttemptAnswers(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_attempt_answers";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = AttemptAnswers<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("AttemptID", this.AttemptID = AttemptAnswers<DataCollectionType, DataRecordType>.Columns.AttemptID);
			base.Columns.Add("QuestionID", this.QuestionID = AttemptAnswers<DataCollectionType, DataRecordType>.Columns.QuestionID);
			base.Columns.Add("QuestionOrder", this.QuestionOrder = AttemptAnswers<DataCollectionType, DataRecordType>.Columns.QuestionOrder);
			base.Columns.Add("TimeSpent", this.TimeSpent = AttemptAnswers<DataCollectionType, DataRecordType>.Columns.TimeSpent);
			base.Columns.Add("Points", this.Points = AttemptAnswers<DataCollectionType, DataRecordType>.Columns.Points);
			base.Columns.Add("Answer", this.Answer = AttemptAnswers<DataCollectionType, DataRecordType>.Columns.Answer);
			base.Columns.Add("IsCorrect", this.IsCorrect = AttemptAnswers<DataCollectionType, DataRecordType>.Columns.IsCorrect);
			
			//Call the secondary constructor method
			this.AttemptAnswersEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void AttemptAnswersEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<int> AttemptID { get; set; }
		public jlib.DataFramework.BaseColumn<int> QuestionID { get; set; }
		public jlib.DataFramework.BaseColumn<int> QuestionOrder { get; set; }
		public jlib.DataFramework.BaseColumn<int> TimeSpent { get; set; }
		public jlib.DataFramework.BaseColumn<decimal> Points { get; set; }
		public jlib.DataFramework.BaseColumn<string> Answer { get; set; }
		public jlib.DataFramework.BaseColumn<bool> IsCorrect { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_attempt_answers] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_attempt_answers] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_attempt_answers");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_attempt_answers", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, int? AttemptID = null, int? QuestionID = null, int? QuestionOrder = null, int? TimeSpent = null, decimal? Points = null, string Answer = null, bool? IsCorrect = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(AttemptID != null)
			{
				query.Parameters.AttemptID = AttemptID;
				filterConditions.Add(@"[attempt_id] = @AttemptID");
			}	
			if(QuestionID != null)
			{
				query.Parameters.QuestionID = QuestionID;
				filterConditions.Add(@"[question_id] = @QuestionID");
			}	
			if(QuestionOrder != null)
			{
				query.Parameters.QuestionOrder = QuestionOrder;
				filterConditions.Add(@"[question_order] = @QuestionOrder");
			}	
			if(TimeSpent != null)
			{
				query.Parameters.TimeSpent = TimeSpent;
				filterConditions.Add(@"[time_spent] = @TimeSpent");
			}	
			if(Points != null)
			{
				query.Parameters.Points = Points;
				filterConditions.Add(@"[points] = @Points");
			}	
			if(Answer != null)
			{
				query.Parameters.Answer = Answer;
				filterConditions.Add(@"[answer] LIKE @Answer");
			}	
			if(IsCorrect != null)
			{
				query.Parameters.IsCorrect = IsCorrect;
				filterConditions.Add(@"[is_correct] = @IsCorrect");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_attempt_answers] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<AttemptAnswers<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new AttemptAnswers<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<AttemptAnswers<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new AttemptAnswers<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_attempt_answers] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new AttemptAnswersDataReader(this);
		}
		
		public List<DtoAttemptAnswer> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class AttemptAnswersDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public AttemptAnswersDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoAttemptAnswer
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		AttemptID Property
		/// </summary>
		[DataMember]
		public virtual int AttemptID { get; set; }
		
		/// <summary>
		///		QuestionID Property
		/// </summary>
		[DataMember]
		public virtual int QuestionID { get; set; }
		
		/// <summary>
		///		QuestionOrder Property
		/// </summary>
		[DataMember]
		public virtual int QuestionOrder { get; set; }
		
		/// <summary>
		///		TimeSpent Property
		/// </summary>
		[DataMember]
		public virtual int TimeSpent { get; set; }
		
		/// <summary>
		///		Points Property
		/// </summary>
		[DataMember]
		public virtual decimal Points { get; set; }
		
		/// <summary>
		///		Answer Property
		/// </summary>
		[DataMember]
		public virtual string Answer { get; set; }
		
		/// <summary>
		///		IsCorrect Property
		/// </summary>
		[DataMember]
		public virtual bool IsCorrect { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class AttemptAnswer : AttemptAnswer<AttemptAnswer>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates AttemptAnswer
		/// </summary>
		public AttemptAnswer() : this(new Context()) {}
		
		/// <summary>
		/// Creates AttemptAnswer
		/// </summary>
		public AttemptAnswer(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for AttemptAnswer
		/// </summary>
		protected AttemptAnswer(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class AttemptAnswer<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: AttemptAnswer<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates AttemptAnswer
		/// </summary>
		public AttemptAnswer() : this(new Context()) {}
		
		/// <summary>
		/// Creates AttemptAnswer
		/// </summary>
		public AttemptAnswer(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_attempt_answers";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, AttemptAnswers.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("AttemptID" ,this._AttemptID = new jlib.DataFramework.BaseField<int>(this, AttemptAnswers.Columns.AttemptID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("QuestionID" ,this._QuestionID = new jlib.DataFramework.BaseField<int>(this, AttemptAnswers.Columns.QuestionID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("QuestionOrder" ,this._QuestionOrder = new jlib.DataFramework.BaseField<int>(this, AttemptAnswers.Columns.QuestionOrder, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("TimeSpent" ,this._TimeSpent = new jlib.DataFramework.BaseField<int>(this, AttemptAnswers.Columns.TimeSpent, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Points" ,this._Points = new jlib.DataFramework.BaseField<decimal>(this, AttemptAnswers.Columns.Points, InitialValue: 0.00M, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Answer" ,this._Answer = new jlib.DataFramework.BaseField<string>(this, AttemptAnswers.Columns.Answer, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("IsCorrect" ,this._IsCorrect = new jlib.DataFramework.BaseField<bool>(this, AttemptAnswers.Columns.IsCorrect, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.AttemptAnswerEx();
		}
		
		/// <summary>
		/// Deserialization method for AttemptAnswer
		/// </summary>
		protected AttemptAnswer(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void AttemptAnswerEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempt_answers", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		AttemptID Property
		///		Database Field:		attempt_id;
		///		Database Parameter:	@AttemptID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempt_answers", FieldName: "attempt_id")]
		public virtual int AttemptID
		{
			get 
			{ 
				return this._AttemptID.Value; 
			}

			set 
			{ 
				//Set Value
				this._AttemptID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._AttemptID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _AttemptID { get; set; }

			
		/// <summary>
		///		QuestionID Property
		///		Database Field:		question_id;
		///		Database Parameter:	@QuestionID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempt_answers", FieldName: "question_id")]
		public virtual int QuestionID
		{
			get 
			{ 
				return this._QuestionID.Value; 
			}

			set 
			{ 
				//Set Value
				this._QuestionID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._QuestionID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _QuestionID { get; set; }

			
		/// <summary>
		///		QuestionOrder Property
		///		Database Field:		question_order;
		///		Database Parameter:	@QuestionOrder;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempt_answers", FieldName: "question_order")]
		public virtual int QuestionOrder
		{
			get 
			{ 
				return this._QuestionOrder.Value; 
			}

			set 
			{ 
				//Set Value
				this._QuestionOrder.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._QuestionOrder.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _QuestionOrder { get; set; }

			
		/// <summary>
		///		TimeSpent Property
		///		Database Field:		time_spent;
		///		Database Parameter:	@TimeSpent;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempt_answers", FieldName: "time_spent")]
		public virtual int TimeSpent
		{
			get 
			{ 
				return this._TimeSpent.Value; 
			}

			set 
			{ 
				//Set Value
				this._TimeSpent.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._TimeSpent.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _TimeSpent { get; set; }

			
		/// <summary>
		///		Points Property
		///		Database Field:		points;
		///		Database Parameter:	@Points;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempt_answers", FieldName: "points")]
		public virtual decimal Points
		{
			get 
			{ 
				return this._Points.Value; 
			}

			set 
			{ 
				//Set Value
				this._Points.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Points.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<decimal> _Points { get; set; }

			
		/// <summary>
		///		Answer Property
		///		Database Field:		answer;
		///		Database Parameter:	@Answer;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempt_answers", FieldName: "answer", IsNullable=true)]
		public virtual string Answer
		{
			get 
			{ 
				return (string)this._Answer.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Answer.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Answer.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Answer { get; set; }

			
		/// <summary>
		///		IsCorrect Property
		///		Database Field:		is_correct;
		///		Database Parameter:	@IsCorrect;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempt_answers", FieldName: "is_correct")]
		public virtual bool IsCorrect
		{
			get 
			{ 
				return this._IsCorrect.Value; 
			}

			set 
			{ 
				//Set Value
				this._IsCorrect.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._IsCorrect.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _IsCorrect { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoAttemptAnswer Dto)
		{
			//Set Values
			this._AttemptID.SetChangedValueIfDifferent(Dto.AttemptID);
			this._QuestionID.SetChangedValueIfDifferent(Dto.QuestionID);
			this._QuestionOrder.SetChangedValueIfDifferent(Dto.QuestionOrder);
			this._TimeSpent.SetChangedValueIfDifferent(Dto.TimeSpent);
			this._Points.SetChangedValueIfDifferent(Dto.Points);
			this._Answer.SetChangedValueIfDifferent(Dto.Answer);
			this._IsCorrect.SetChangedValueIfDifferent(Dto.IsCorrect);
		}
		public virtual DtoAttemptAnswer ToDto()
		{
			return this.ToDto(new DtoAttemptAnswer());
		}
		
		public virtual DtoAttemptAnswer ToDto(DtoAttemptAnswer dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.AttemptID = this.AttemptID;
			dto.QuestionID = this.QuestionID;
			dto.QuestionOrder = this.QuestionOrder;
			dto.TimeSpent = this.TimeSpent;
			dto.Points = this.Points;
			dto.Answer = this.Answer;
			dto.IsCorrect = this.IsCorrect;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_attempt_answers] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_attempt_answers]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class Attempts : Attempts<Attempts, Attempt>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Attempts
		/// </summary>
		public Attempts() : this(new Context()) {}
		
		/// <summary>
		/// Creates Attempts
		/// </summary>
		public Attempts(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Attempts<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: Attempt<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CreateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CreateDate", PropertyName: "CreateDate", ParameterName: "@Createdate", PkParameterName: "@PK_Createdate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> ObjectDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "ObjectDate", PropertyName: "ObjectDate", ParameterName: "@Objectdate", PkParameterName: "@PK_Objectdate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<bool> Deleted = new jlib.DataFramework.BaseColumn<bool>(FieldName: "deleted", PropertyName: "Deleted", ParameterName: "@Deleted", PkParameterName: "@PK_Deleted", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> UserID = new jlib.DataFramework.BaseColumn<int>(FieldName: "user_id", PropertyName: "UserID", ParameterName: "@UserID", PkParameterName: "@PK_UserID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> ReleaseID = new jlib.DataFramework.BaseColumn<int>(FieldName: "release_id", PropertyName: "ReleaseID", ParameterName: "@ReleaseID", PkParameterName: "@PK_ReleaseID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<decimal> Points = new jlib.DataFramework.BaseColumn<decimal>(FieldName: "Points", PropertyName: "Points", ParameterName: "@Points", PkParameterName: "@PK_Points", DBType: DbType.Currency, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<decimal> MaxPoints = new jlib.DataFramework.BaseColumn<decimal>(FieldName: "MaxPoints", PropertyName: "MaxPoints", ParameterName: "@Maxpoints", PkParameterName: "@PK_Maxpoints", DBType: DbType.Currency, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Grade = new jlib.DataFramework.BaseColumn<string>(FieldName: "grade", PropertyName: "Grade", ParameterName: "@Grade", PkParameterName: "@PK_Grade", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> SuspendData = new jlib.DataFramework.BaseColumn<string>(FieldName: "suspend_data", PropertyName: "SuspendData", ParameterName: "@SuspendData", PkParameterName: "@PK_SuspendData", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> MaxTime = new jlib.DataFramework.BaseColumn<int>(FieldName: "MaxTime", PropertyName: "MaxTime", ParameterName: "@Maxtime", PkParameterName: "@PK_Maxtime", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> StatusID = new jlib.DataFramework.BaseColumn<int>(FieldName: "StatusID", PropertyName: "StatusID", ParameterName: "@Statusid", PkParameterName: "@PK_Statusid", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> RcoID = new jlib.DataFramework.BaseColumn<int>(FieldName: "Rco_ID", PropertyName: "RcoID", ParameterName: "@RcoID", PkParameterName: "@PK_RcoID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> ClassroomID = new jlib.DataFramework.BaseColumn<int>(FieldName: "Classroom_ID", PropertyName: "ClassroomID", ParameterName: "@ClassroomID", PkParameterName: "@PK_ClassroomID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> ScormSession = new jlib.DataFramework.BaseColumn<string>(FieldName: "ScormSession", PropertyName: "ScormSession", ParameterName: "@Scormsession", PkParameterName: "@PK_Scormsession", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Attempts
		/// </summary>
		public Attempts() : this(new Context()) {}
		
		/// <summary>
		/// Creates Attempts
		/// </summary>
		public Attempts(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_attempts";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = Attempts<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("CreateDate", this.CreateDate = Attempts<DataCollectionType, DataRecordType>.Columns.CreateDate);
			base.Columns.Add("ObjectDate", this.ObjectDate = Attempts<DataCollectionType, DataRecordType>.Columns.ObjectDate);
			base.Columns.Add("Deleted", this.Deleted = Attempts<DataCollectionType, DataRecordType>.Columns.Deleted);
			base.Columns.Add("UserID", this.UserID = Attempts<DataCollectionType, DataRecordType>.Columns.UserID);
			base.Columns.Add("ReleaseID", this.ReleaseID = Attempts<DataCollectionType, DataRecordType>.Columns.ReleaseID);
			base.Columns.Add("Points", this.Points = Attempts<DataCollectionType, DataRecordType>.Columns.Points);
			base.Columns.Add("MaxPoints", this.MaxPoints = Attempts<DataCollectionType, DataRecordType>.Columns.MaxPoints);
			base.Columns.Add("Grade", this.Grade = Attempts<DataCollectionType, DataRecordType>.Columns.Grade);
			base.Columns.Add("SuspendData", this.SuspendData = Attempts<DataCollectionType, DataRecordType>.Columns.SuspendData);
			base.Columns.Add("MaxTime", this.MaxTime = Attempts<DataCollectionType, DataRecordType>.Columns.MaxTime);
			base.Columns.Add("StatusID", this.StatusID = Attempts<DataCollectionType, DataRecordType>.Columns.StatusID);
			base.Columns.Add("RcoID", this.RcoID = Attempts<DataCollectionType, DataRecordType>.Columns.RcoID);
			base.Columns.Add("ClassroomID", this.ClassroomID = Attempts<DataCollectionType, DataRecordType>.Columns.ClassroomID);
			base.Columns.Add("ScormSession", this.ScormSession = Attempts<DataCollectionType, DataRecordType>.Columns.ScormSession);
			
			//Call the secondary constructor method
			this.AttemptsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void AttemptsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CreateDate { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> ObjectDate { get; set; }
		public jlib.DataFramework.BaseColumn<bool> Deleted { get; set; }
		public jlib.DataFramework.BaseColumn<int> UserID { get; set; }
		public jlib.DataFramework.BaseColumn<int> ReleaseID { get; set; }
		public jlib.DataFramework.BaseColumn<decimal> Points { get; set; }
		public jlib.DataFramework.BaseColumn<decimal> MaxPoints { get; set; }
		public jlib.DataFramework.BaseColumn<string> Grade { get; set; }
		public jlib.DataFramework.BaseColumn<string> SuspendData { get; set; }
		public jlib.DataFramework.BaseColumn<int> MaxTime { get; set; }
		public jlib.DataFramework.BaseColumn<int> StatusID { get; set; }
		public jlib.DataFramework.BaseColumn<int> RcoID { get; set; }
		public jlib.DataFramework.BaseColumn<int> ClassroomID { get; set; }
		public jlib.DataFramework.BaseColumn<string> ScormSession { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_attempts] WITH (NOLOCK) WHERE [deleted]=0", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_attempts] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_attempts");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_attempts", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, DateTime? CreateDate = null, DateTime? ObjectDate = null, bool? Deleted = null, int? UserID = null, int? ReleaseID = null, decimal? Points = null, decimal? MaxPoints = null, string Grade = null, string SuspendData = null, int? MaxTime = null, int? StatusID = null, int? RcoID = null, int? ClassroomID = null, string ScormSession = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
			if(Deleted == null)
			{
				filterConditions.Add(@"[deleted]=0");
			}
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(CreateDate != null)
			{
				query.Parameters.CreateDate = CreateDate;
				filterConditions.Add(@"[CreateDate] = @Createdate");
			}	
			if(ObjectDate != null)
			{
				query.Parameters.ObjectDate = ObjectDate;
				filterConditions.Add(@"[ObjectDate] = @Objectdate");
			}	
			if(Deleted != null)
			{
				query.Parameters.Deleted = Deleted;
				filterConditions.Add(@"[deleted] = @Deleted");
			}	
			if(UserID != null)
			{
				query.Parameters.UserID = UserID;
				filterConditions.Add(@"[user_id] = @UserID");
			}	
			if(ReleaseID != null)
			{
				query.Parameters.ReleaseID = ReleaseID;
				filterConditions.Add(@"[release_id] = @ReleaseID");
			}	
			if(Points != null)
			{
				query.Parameters.Points = Points;
				filterConditions.Add(@"[Points] = @Points");
			}	
			if(MaxPoints != null)
			{
				query.Parameters.MaxPoints = MaxPoints;
				filterConditions.Add(@"[MaxPoints] = @Maxpoints");
			}	
			if(Grade != null)
			{
				query.Parameters.Grade = Grade;
				filterConditions.Add(@"[grade] = @Grade");
			}	
			if(SuspendData != null)
			{
				query.Parameters.SuspendData = SuspendData;
				filterConditions.Add(@"[suspend_data] LIKE @SuspendData");
			}	
			if(MaxTime != null)
			{
				query.Parameters.MaxTime = MaxTime;
				filterConditions.Add(@"[MaxTime] = @Maxtime");
			}	
			if(StatusID != null)
			{
				query.Parameters.StatusID = StatusID;
				filterConditions.Add(@"[StatusID] = @Statusid");
			}	
			if(RcoID != null)
			{
				query.Parameters.RcoID = RcoID;
				filterConditions.Add(@"[Rco_ID] = @RcoID");
			}	
			if(ClassroomID != null)
			{
				query.Parameters.ClassroomID = ClassroomID;
				filterConditions.Add(@"[Classroom_ID] = @ClassroomID");
			}	
			if(ScormSession != null)
			{
				query.Parameters.ScormSession = ScormSession;
				filterConditions.Add(@"[ScormSession] = @Scormsession");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_attempts] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Attempts<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Attempts<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Attempts<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Attempts<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_attempts] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new AttemptsDataReader(this);
		}
		
		public List<DtoAttempt> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class AttemptsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public AttemptsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoAttempt
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		CreateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime CreateDate { get; set; }
		
		/// <summary>
		///		ObjectDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime ObjectDate { get; set; }
		
		/// <summary>
		///		Deleted Property
		/// </summary>
		[DataMember]
		public virtual bool Deleted { get; set; }
		
		/// <summary>
		///		UserID Property
		/// </summary>
		[DataMember]
		public virtual int UserID { get; set; }
		
		/// <summary>
		///		ReleaseID Property
		/// </summary>
		[DataMember]
		public virtual int ReleaseID { get; set; }
		
		/// <summary>
		///		Points Property
		/// </summary>
		[DataMember]
		public virtual decimal Points { get; set; }
		
		/// <summary>
		///		MaxPoints Property
		/// </summary>
		[DataMember]
		public virtual decimal MaxPoints { get; set; }
		
		/// <summary>
		///		Grade Property
		/// </summary>
		[DataMember]
		public virtual string Grade { get; set; }
		
		/// <summary>
		///		SuspendData Property
		/// </summary>
		[DataMember]
		public virtual string SuspendData { get; set; }
		
		/// <summary>
		///		MaxTime Property
		/// </summary>
		[DataMember]
		public virtual int MaxTime { get; set; }
		
		/// <summary>
		///		StatusID Property
		/// </summary>
		[DataMember]
		public virtual int StatusID { get; set; }
		
		/// <summary>
		///		RcoID Property
		/// </summary>
		[DataMember]
		public virtual int RcoID { get; set; }
		
		/// <summary>
		///		ClassroomID Property
		/// </summary>
		[DataMember]
		public virtual int ClassroomID { get; set; }
		
		/// <summary>
		///		ScormSession Property
		/// </summary>
		[DataMember]
		public virtual string ScormSession { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class Attempt : Attempt<Attempt>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Attempt
		/// </summary>
		public Attempt() : this(new Context()) {}
		
		/// <summary>
		/// Creates Attempt
		/// </summary>
		public Attempt(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for Attempt
		/// </summary>
		protected Attempt(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class Attempt<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: Attempt<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Attempt
		/// </summary>
		public Attempt() : this(new Context()) {}
		
		/// <summary>
		/// Creates Attempt
		/// </summary>
		public Attempt(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_attempts";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, Attempts.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreateDate" ,this._CreateDate = new jlib.DataFramework.BaseField<DateTime>(this, Attempts.Columns.CreateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ObjectDate" ,this._ObjectDate = new jlib.DataFramework.BaseField<DateTime>(this, Attempts.Columns.ObjectDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Deleted" ,this._Deleted = new jlib.DataFramework.BaseField<bool>(this, Attempts.Columns.Deleted, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserID" ,this._UserID = new jlib.DataFramework.BaseField<int>(this, Attempts.Columns.UserID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ReleaseID" ,this._ReleaseID = new jlib.DataFramework.BaseField<int>(this, Attempts.Columns.ReleaseID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Points" ,this._Points = new jlib.DataFramework.BaseField<decimal>(this, Attempts.Columns.Points, InitialValue: 0.00M, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("MaxPoints" ,this._MaxPoints = new jlib.DataFramework.BaseField<decimal>(this, Attempts.Columns.MaxPoints, InitialValue: 0.00M, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Grade" ,this._Grade = new jlib.DataFramework.BaseField<string>(this, Attempts.Columns.Grade, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("SuspendData" ,this._SuspendData = new jlib.DataFramework.BaseField<string>(this, Attempts.Columns.SuspendData, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("MaxTime" ,this._MaxTime = new jlib.DataFramework.BaseField<int>(this, Attempts.Columns.MaxTime, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("StatusID" ,this._StatusID = new jlib.DataFramework.BaseField<int>(this, Attempts.Columns.StatusID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("RcoID" ,this._RcoID = new jlib.DataFramework.BaseField<int>(this, Attempts.Columns.RcoID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ClassroomID" ,this._ClassroomID = new jlib.DataFramework.BaseField<int>(this, Attempts.Columns.ClassroomID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ScormSession" ,this._ScormSession = new jlib.DataFramework.BaseField<string>(this, Attempts.Columns.ScormSession, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.AttemptEx();
		}
		
		/// <summary>
		/// Deserialization method for Attempt
		/// </summary>
		protected Attempt(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void AttemptEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		CreateDate Property
		///		Database Field:		CreateDate;
		///		Database Parameter:	@Createdate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "CreateDate")]
		public virtual DateTime CreateDate
		{
			get 
			{ 
				return this._CreateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CreateDate { get; set; }

			
		/// <summary>
		///		ObjectDate Property
		///		Database Field:		ObjectDate;
		///		Database Parameter:	@Objectdate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "ObjectDate")]
		public virtual DateTime ObjectDate
		{
			get 
			{ 
				return this._ObjectDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._ObjectDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ObjectDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _ObjectDate { get; set; }

			
		/// <summary>
		///		Deleted Property
		///		Database Field:		deleted;
		///		Database Parameter:	@Deleted;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "deleted")]
		public virtual bool Deleted
		{
			get 
			{ 
				return this._Deleted.Value; 
			}

			set 
			{ 
				//Set Value
				this._Deleted.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Deleted.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _Deleted { get; set; }

			
		/// <summary>
		///		UserID Property
		///		Database Field:		user_id;
		///		Database Parameter:	@UserID;
		///		Primary Key:		No;
		///		Foreign Key:		Yes;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "user_id", IsForeignKey=true)]
		public virtual int UserID
		{
			get 
			{ 
				return this._UserID.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

				//Set Foreign Key Object to Null
				this._User = null;
			}
		}
		protected jlib.DataFramework.BaseField<int> _UserID { get; set; }

		/// <summary>
		///		User Foreign Object Property
		///		Foreign Key Field:	user_id
		///		Primary Key Field:	ID
		/// </summary>
		[jlib.DataFramework.Field("d_attempts", "user_id")]
		public virtual BehindUser User
		{
			get
			{
				if(this._User == null )
				{
					this._User = BehindUsers.GetByField(ID:(int)this.UserID).Execute().FirstOrDefault();
				}
				
						
				return this._User;
			}
			
			set
			{
				this._User = value;	
			}
		}
		protected BehindUser _User { get; set; }
			
		/// <summary>
		///		ReleaseID Property
		///		Database Field:		release_id;
		///		Database Parameter:	@ReleaseID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "release_id")]
		public virtual int ReleaseID
		{
			get 
			{ 
				return this._ReleaseID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ReleaseID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ReleaseID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ReleaseID { get; set; }

			
		/// <summary>
		///		Points Property
		///		Database Field:		Points;
		///		Database Parameter:	@Points;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "Points")]
		public virtual decimal Points
		{
			get 
			{ 
				return this._Points.Value; 
			}

			set 
			{ 
				//Set Value
				this._Points.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Points.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<decimal> _Points { get; set; }

			
		/// <summary>
		///		MaxPoints Property
		///		Database Field:		MaxPoints;
		///		Database Parameter:	@Maxpoints;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "MaxPoints")]
		public virtual decimal MaxPoints
		{
			get 
			{ 
				return this._MaxPoints.Value; 
			}

			set 
			{ 
				//Set Value
				this._MaxPoints.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._MaxPoints.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<decimal> _MaxPoints { get; set; }

			
		/// <summary>
		///		Grade Property
		///		Database Field:		grade;
		///		Database Parameter:	@Grade;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "grade", IsNullable=true)]
		public virtual string Grade
		{
			get 
			{ 
				return (string)this._Grade.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Grade.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Grade.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Grade { get; set; }

			
		/// <summary>
		///		SuspendData Property
		///		Database Field:		suspend_data;
		///		Database Parameter:	@SuspendData;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "suspend_data", IsNullable=true)]
		public virtual string SuspendData
		{
			get 
			{ 
				return (string)this._SuspendData.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._SuspendData.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._SuspendData.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _SuspendData { get; set; }

			
		/// <summary>
		///		MaxTime Property
		///		Database Field:		MaxTime;
		///		Database Parameter:	@Maxtime;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "MaxTime")]
		public virtual int MaxTime
		{
			get 
			{ 
				return this._MaxTime.Value; 
			}

			set 
			{ 
				//Set Value
				this._MaxTime.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._MaxTime.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _MaxTime { get; set; }

			
		/// <summary>
		///		StatusID Property
		///		Database Field:		StatusID;
		///		Database Parameter:	@Statusid;
		///		Primary Key:		No;
		///		Foreign Key:		Yes;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "StatusID", IsForeignKey=true)]
		public virtual int StatusID
		{
			get 
			{ 
				return this._StatusID.Value; 
			}

			set 
			{ 
				//Set Value
				this._StatusID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._StatusID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

				//Set Foreign Key Object to Null
				this._Status = null;
			}
		}
		protected jlib.DataFramework.BaseField<int> _StatusID { get; set; }

		/// <summary>
		///		Status Foreign Object Property
		///		Foreign Key Field:	StatusID
		///		Primary Key Field:	ID
		/// </summary>
		[jlib.DataFramework.Field("d_attempts", "StatusID")]
		public virtual AttemptStatus Status
		{
			get
			{
				if(this._Status == null )
				{
					this._Status = AttemptStatuses.GetByField(ID:(int)this.StatusID).Execute().FirstOrDefault();
				}
				
						
				return this._Status;
			}
			
			set
			{
				this._Status = value;	
			}
		}
		protected AttemptStatus _Status { get; set; }
			
		/// <summary>
		///		RcoID Property
		///		Database Field:		Rco_ID;
		///		Database Parameter:	@RcoID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "Rco_ID")]
		public virtual int RcoID
		{
			get 
			{ 
				return this._RcoID.Value; 
			}

			set 
			{ 
				//Set Value
				this._RcoID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._RcoID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _RcoID { get; set; }

			
		/// <summary>
		///		ClassroomID Property
		///		Database Field:		Classroom_ID;
		///		Database Parameter:	@ClassroomID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "Classroom_ID")]
		public virtual int ClassroomID
		{
			get 
			{ 
				return this._ClassroomID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ClassroomID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ClassroomID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ClassroomID { get; set; }

			
		/// <summary>
		///		ScormSession Property
		///		Database Field:		ScormSession;
		///		Database Parameter:	@Scormsession;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_attempts", FieldName: "ScormSession", IsNullable=true)]
		public virtual string ScormSession
		{
			get 
			{ 
				return (string)this._ScormSession.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._ScormSession.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ScormSession.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _ScormSession { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoAttempt Dto)
		{
			//Set Values
			this._CreateDate.SetChangedValueIfDifferent(Dto.CreateDate);
			this._ObjectDate.SetChangedValueIfDifferent(Dto.ObjectDate);
			this._Deleted.SetChangedValueIfDifferent(Dto.Deleted);
			this._UserID.SetChangedValueIfDifferent(Dto.UserID);
			this._ReleaseID.SetChangedValueIfDifferent(Dto.ReleaseID);
			this._Points.SetChangedValueIfDifferent(Dto.Points);
			this._MaxPoints.SetChangedValueIfDifferent(Dto.MaxPoints);
			this._Grade.SetChangedValueIfDifferent(Dto.Grade);
			this._SuspendData.SetChangedValueIfDifferent(Dto.SuspendData);
			this._MaxTime.SetChangedValueIfDifferent(Dto.MaxTime);
			this._StatusID.SetChangedValueIfDifferent(Dto.StatusID);
			this._RcoID.SetChangedValueIfDifferent(Dto.RcoID);
			this._ClassroomID.SetChangedValueIfDifferent(Dto.ClassroomID);
			this._ScormSession.SetChangedValueIfDifferent(Dto.ScormSession);
		}
		public virtual DtoAttempt ToDto()
		{
			return this.ToDto(new DtoAttempt());
		}
		
		public virtual DtoAttempt ToDto(DtoAttempt dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.CreateDate = this.CreateDate;
			dto.ObjectDate = this.ObjectDate;
			dto.Deleted = this.Deleted;
			dto.UserID = this.UserID;
			dto.ReleaseID = this.ReleaseID;
			dto.Points = this.Points;
			dto.MaxPoints = this.MaxPoints;
			dto.Grade = this.Grade;
			dto.SuspendData = this.SuspendData;
			dto.MaxTime = this.MaxTime;
			dto.StatusID = this.StatusID;
			dto.RcoID = this.RcoID;
			dto.ClassroomID = this.ClassroomID;
			dto.ScormSession = this.ScormSession;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				//Create Internal Object 'user'
				case "user":
					this._User = new BehindUser();
					this._User.Import(Row, InternalObjectIdentifier);
					break;
				
				//Create Internal Object 'status'
				case "status":
					this._Status = new AttemptStatus();
					this._Status.Import(Row, InternalObjectIdentifier);
					break;
				
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_attempts] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_attempts]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class BehindUsers : BehindUsers<BehindUsers, BehindUser>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates BehindUsers
		/// </summary>
		public BehindUsers() : this(new Context()) {}
		
		/// <summary>
		/// Creates BehindUsers
		/// </summary>
		public BehindUsers(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class BehindUsers<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: BehindUser<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Email = new jlib.DataFramework.BaseColumn<string>(FieldName: "email", PropertyName: "Email", ParameterName: "@Email", PkParameterName: "@PK_Email", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Username = new jlib.DataFramework.BaseColumn<string>(FieldName: "username", PropertyName: "Username", ParameterName: "@Username", PkParameterName: "@PK_Username", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> FirstName = new jlib.DataFramework.BaseColumn<string>(FieldName: "first_name", PropertyName: "FirstName", ParameterName: "@FirstName", PkParameterName: "@PK_FirstName", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> LastName = new jlib.DataFramework.BaseColumn<string>(FieldName: "last_name", PropertyName: "LastName", ParameterName: "@LastName", PkParameterName: "@PK_LastName", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> CompanyID = new jlib.DataFramework.BaseColumn<int>(FieldName: "company_id", PropertyName: "CompanyID", ParameterName: "@CompanyID", PkParameterName: "@PK_CompanyID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> CompanyName = new jlib.DataFramework.BaseColumn<string>(FieldName: "company_name", PropertyName: "CompanyName", ParameterName: "@CompanyName", PkParameterName: "@PK_CompanyName", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates BehindUsers
		/// </summary>
		public BehindUsers() : this(new Context()) {}
		
		/// <summary>
		/// Creates BehindUsers
		/// </summary>
		public BehindUsers(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_behind_users";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = BehindUsers<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("Email", this.Email = BehindUsers<DataCollectionType, DataRecordType>.Columns.Email);
			base.Columns.Add("Username", this.Username = BehindUsers<DataCollectionType, DataRecordType>.Columns.Username);
			base.Columns.Add("FirstName", this.FirstName = BehindUsers<DataCollectionType, DataRecordType>.Columns.FirstName);
			base.Columns.Add("LastName", this.LastName = BehindUsers<DataCollectionType, DataRecordType>.Columns.LastName);
			base.Columns.Add("CompanyID", this.CompanyID = BehindUsers<DataCollectionType, DataRecordType>.Columns.CompanyID);
			base.Columns.Add("CompanyName", this.CompanyName = BehindUsers<DataCollectionType, DataRecordType>.Columns.CompanyName);
			
			//Call the secondary constructor method
			this.BehindUsersEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void BehindUsersEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<string> Email { get; set; }
		public jlib.DataFramework.BaseColumn<string> Username { get; set; }
		public jlib.DataFramework.BaseColumn<string> FirstName { get; set; }
		public jlib.DataFramework.BaseColumn<string> LastName { get; set; }
		public jlib.DataFramework.BaseColumn<int> CompanyID { get; set; }
		public jlib.DataFramework.BaseColumn<string> CompanyName { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_behind_users] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_behind_users] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_behind_users");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_behind_users", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, string Email = null, string Username = null, string FirstName = null, string LastName = null, int? CompanyID = null, string CompanyName = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(Email != null)
			{
				query.Parameters.Email = Email;
				filterConditions.Add(@"[email] = @Email");
			}	
			if(Username != null)
			{
				query.Parameters.Username = Username;
				filterConditions.Add(@"[username] = @Username");
			}	
			if(FirstName != null)
			{
				query.Parameters.FirstName = FirstName;
				filterConditions.Add(@"[first_name] = @FirstName");
			}	
			if(LastName != null)
			{
				query.Parameters.LastName = LastName;
				filterConditions.Add(@"[last_name] = @LastName");
			}	
			if(CompanyID != null)
			{
				query.Parameters.CompanyID = CompanyID;
				filterConditions.Add(@"[company_id] = @CompanyID");
			}	
			if(CompanyName != null)
			{
				query.Parameters.CompanyName = CompanyName;
				filterConditions.Add(@"[company_name] = @CompanyName");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_behind_users] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<BehindUsers<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new BehindUsers<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<BehindUsers<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new BehindUsers<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_behind_users] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new BehindUsersDataReader(this);
		}
		
		public List<DtoBehindUser> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class BehindUsersDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public BehindUsersDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoBehindUser
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		Email Property
		/// </summary>
		[DataMember]
		public virtual string Email { get; set; }
		
		/// <summary>
		///		Username Property
		/// </summary>
		[DataMember]
		public virtual string Username { get; set; }
		
		/// <summary>
		///		FirstName Property
		/// </summary>
		[DataMember]
		public virtual string FirstName { get; set; }
		
		/// <summary>
		///		LastName Property
		/// </summary>
		[DataMember]
		public virtual string LastName { get; set; }
		
		/// <summary>
		///		CompanyID Property
		/// </summary>
		[DataMember]
		public virtual int CompanyID { get; set; }
		
		/// <summary>
		///		CompanyName Property
		/// </summary>
		[DataMember]
		public virtual string CompanyName { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class BehindUser : BehindUser<BehindUser>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates BehindUser
		/// </summary>
		public BehindUser() : this(new Context()) {}
		
		/// <summary>
		/// Creates BehindUser
		/// </summary>
		public BehindUser(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for BehindUser
		/// </summary>
		protected BehindUser(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class BehindUser<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: BehindUser<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates BehindUser
		/// </summary>
		public BehindUser() : this(new Context()) {}
		
		/// <summary>
		/// Creates BehindUser
		/// </summary>
		public BehindUser(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_behind_users";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, BehindUsers.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Email" ,this._Email = new jlib.DataFramework.BaseField<string>(this, BehindUsers.Columns.Email, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Username" ,this._Username = new jlib.DataFramework.BaseField<string>(this, BehindUsers.Columns.Username, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("FirstName" ,this._FirstName = new jlib.DataFramework.BaseField<string>(this, BehindUsers.Columns.FirstName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("LastName" ,this._LastName = new jlib.DataFramework.BaseField<string>(this, BehindUsers.Columns.LastName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CompanyID" ,this._CompanyID = new jlib.DataFramework.BaseField<int>(this, BehindUsers.Columns.CompanyID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CompanyName" ,this._CompanyName = new jlib.DataFramework.BaseField<string>(this, BehindUsers.Columns.CompanyName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.BehindUserEx();
		}
		
		/// <summary>
		/// Deserialization method for BehindUser
		/// </summary>
		protected BehindUser(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void BehindUserEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_behind_users", FieldName: "ID", IsPrimaryKey=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		Email Property
		///		Database Field:		email;
		///		Database Parameter:	@Email;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_behind_users", FieldName: "email", IsNullable=true)]
		public virtual string Email
		{
			get 
			{ 
				return (string)this._Email.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Email.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Email.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Email { get; set; }

			
		/// <summary>
		///		Username Property
		///		Database Field:		username;
		///		Database Parameter:	@Username;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_behind_users", FieldName: "username", IsNullable=true)]
		public virtual string Username
		{
			get 
			{ 
				return (string)this._Username.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Username.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Username.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Username { get; set; }

			
		/// <summary>
		///		FirstName Property
		///		Database Field:		first_name;
		///		Database Parameter:	@FirstName;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_behind_users", FieldName: "first_name", IsNullable=true)]
		public virtual string FirstName
		{
			get 
			{ 
				return (string)this._FirstName.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._FirstName.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._FirstName.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _FirstName { get; set; }

			
		/// <summary>
		///		LastName Property
		///		Database Field:		last_name;
		///		Database Parameter:	@LastName;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_behind_users", FieldName: "last_name", IsNullable=true)]
		public virtual string LastName
		{
			get 
			{ 
				return (string)this._LastName.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._LastName.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._LastName.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _LastName { get; set; }

			
		/// <summary>
		///		CompanyID Property
		///		Database Field:		company_id;
		///		Database Parameter:	@CompanyID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_behind_users", FieldName: "company_id")]
		public virtual int CompanyID
		{
			get 
			{ 
				return this._CompanyID.Value; 
			}

			set 
			{ 
				//Set Value
				this._CompanyID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CompanyID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _CompanyID { get; set; }

			
		/// <summary>
		///		CompanyName Property
		///		Database Field:		company_name;
		///		Database Parameter:	@CompanyName;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_behind_users", FieldName: "company_name", IsNullable=true)]
		public virtual string CompanyName
		{
			get 
			{ 
				return (string)this._CompanyName.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._CompanyName.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CompanyName.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _CompanyName { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoBehindUser Dto)
		{
			//Set Values
			this._ID.SetChangedValueIfDifferent(Dto.ID);
			this._Email.SetChangedValueIfDifferent(Dto.Email);
			this._Username.SetChangedValueIfDifferent(Dto.Username);
			this._FirstName.SetChangedValueIfDifferent(Dto.FirstName);
			this._LastName.SetChangedValueIfDifferent(Dto.LastName);
			this._CompanyID.SetChangedValueIfDifferent(Dto.CompanyID);
			this._CompanyName.SetChangedValueIfDifferent(Dto.CompanyName);
		}
		public virtual DtoBehindUser ToDto()
		{
			return this.ToDto(new DtoBehindUser());
		}
		
		public virtual DtoBehindUser ToDto(DtoBehindUser dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.Email = this.Email;
			dto.Username = this.Username;
			dto.FirstName = this.FirstName;
			dto.LastName = this.LastName;
			dto.CompanyID = this.CompanyID;
			dto.CompanyName = this.CompanyName;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_behind_users] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_behind_users]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class Contents : Contents<Contents, Content>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Contents
		/// </summary>
		public Contents() : this(new Context()) {}
		
		/// <summary>
		/// Creates Contents
		/// </summary>
		public Contents(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Contents<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: Content<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ObjectID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ObjectID", PropertyName: "ObjectID", ParameterName: "@Objectid", PkParameterName: "@PK_Objectid", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CreateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CreateDate", PropertyName: "CreateDate", ParameterName: "@Createdate", PkParameterName: "@PK_Createdate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> DeleteDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "DeleteDate", PropertyName: "DeleteDate", ParameterName: "@Deletedate", PkParameterName: "@PK_Deletedate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Name = new jlib.DataFramework.BaseColumn<string>(FieldName: "Name", PropertyName: "Name", ParameterName: "@Name", PkParameterName: "@PK_Name", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Xml = new jlib.DataFramework.BaseColumn<string>(FieldName: "Xml", PropertyName: "Xml", ParameterName: "@Xml", PkParameterName: "@PK_Xml", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Filename = new jlib.DataFramework.BaseColumn<string>(FieldName: "filename", PropertyName: "Filename", ParameterName: "@Filename", PkParameterName: "@PK_Filename", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> Type = new jlib.DataFramework.BaseColumn<int>(FieldName: "type", PropertyName: "Type", ParameterName: "@Type", PkParameterName: "@PK_Type", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> SourceID = new jlib.DataFramework.BaseColumn<int>(FieldName: "source_id", PropertyName: "SourceID", ParameterName: "@SourceID", PkParameterName: "@PK_SourceID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> TemplateID = new jlib.DataFramework.BaseColumn<int>(FieldName: "template_id", PropertyName: "TemplateID", ParameterName: "@TemplateID", PkParameterName: "@PK_TemplateID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> CreatedbyID = new jlib.DataFramework.BaseColumn<int>(FieldName: "CreatedBy", PropertyName: "CreatedbyID", ParameterName: "@Createdby", PkParameterName: "@PK_Createdby", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> ModifiedbyID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ModifiedBy", PropertyName: "ModifiedbyID", ParameterName: "@Modifiedby", PkParameterName: "@PK_Modifiedby", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> AuxField1 = new jlib.DataFramework.BaseColumn<string>(FieldName: "AuxField1", PropertyName: "AuxField1", ParameterName: "@Auxfield1", PkParameterName: "@PK_Auxfield1", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> AuxField2 = new jlib.DataFramework.BaseColumn<string>(FieldName: "AuxField2", PropertyName: "AuxField2", ParameterName: "@Auxfield2", PkParameterName: "@PK_Auxfield2", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Contents
		/// </summary>
		public Contents() : this(new Context()) {}
		
		/// <summary>
		/// Creates Contents
		/// </summary>
		public Contents(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_content";
			
			//Set Columns
			base.Columns.Add("ObjectID", this.ObjectID = Contents<DataCollectionType, DataRecordType>.Columns.ObjectID);
			base.Columns.Add("CreateDate", this.CreateDate = Contents<DataCollectionType, DataRecordType>.Columns.CreateDate);
			base.Columns.Add("DeleteDate", this.DeleteDate = Contents<DataCollectionType, DataRecordType>.Columns.DeleteDate);
			base.Columns.Add("Name", this.Name = Contents<DataCollectionType, DataRecordType>.Columns.Name);
			base.Columns.Add("Xml", this.Xml = Contents<DataCollectionType, DataRecordType>.Columns.Xml);
			base.Columns.Add("Filename", this.Filename = Contents<DataCollectionType, DataRecordType>.Columns.Filename);
			base.Columns.Add("Type", this.Type = Contents<DataCollectionType, DataRecordType>.Columns.Type);
			base.Columns.Add("SourceID", this.SourceID = Contents<DataCollectionType, DataRecordType>.Columns.SourceID);
			base.Columns.Add("TemplateID", this.TemplateID = Contents<DataCollectionType, DataRecordType>.Columns.TemplateID);
			base.Columns.Add("CreatedbyID", this.CreatedbyID = Contents<DataCollectionType, DataRecordType>.Columns.CreatedbyID);
			base.Columns.Add("ModifiedbyID", this.ModifiedbyID = Contents<DataCollectionType, DataRecordType>.Columns.ModifiedbyID);
			base.Columns.Add("AuxField1", this.AuxField1 = Contents<DataCollectionType, DataRecordType>.Columns.AuxField1);
			base.Columns.Add("AuxField2", this.AuxField2 = Contents<DataCollectionType, DataRecordType>.Columns.AuxField2);
			
			//Call the secondary constructor method
			this.ContentsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void ContentsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ObjectID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CreateDate { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> DeleteDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> Name { get; set; }
		public jlib.DataFramework.BaseColumn<string> Xml { get; set; }
		public jlib.DataFramework.BaseColumn<string> Filename { get; set; }
		public jlib.DataFramework.BaseColumn<int> Type { get; set; }
		public jlib.DataFramework.BaseColumn<int> SourceID { get; set; }
		public jlib.DataFramework.BaseColumn<int> TemplateID { get; set; }
		public jlib.DataFramework.BaseColumn<int> CreatedbyID { get; set; }
		public jlib.DataFramework.BaseColumn<int> ModifiedbyID { get; set; }
		public jlib.DataFramework.BaseColumn<string> AuxField1 { get; set; }
		public jlib.DataFramework.BaseColumn<string> AuxField2 { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_content] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_content] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_content");
		}
		
		
		public static QueryPackage GetByField(int? ObjectID = null, DateTime? CreateDate = null, DateTime? DeleteDate = null, string Name = null, string Xml = null, string Filename = null, int? Type = null, int? SourceID = null, int? TemplateID = null, int? CreatedbyID = null, int? ModifiedbyID = null, string AuxField1 = null, string AuxField2 = null, DateTime? ObjectDate = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ObjectID != null)
			{
				query.Parameters.ObjectID = ObjectID;
				filterConditions.Add(@"[ObjectID] = @Objectid");
			}	
			if(CreateDate != null)
			{
				query.Parameters.CreateDate = CreateDate;
				filterConditions.Add(@"[CreateDate] = @Createdate");
			}	
			if(DeleteDate != null)
			{
				query.Parameters.DeleteDate = DeleteDate;
				filterConditions.Add(@"[DeleteDate] = @Deletedate");
			}	
			if(Name != null)
			{
				query.Parameters.Name = Name;
				filterConditions.Add(@"[Name] = @Name");
			}	
			if(Xml != null)
			{
				query.Parameters.Xml = Xml;
				filterConditions.Add(@"[Xml] = @Xml");
			}	
			if(Filename != null)
			{
				query.Parameters.Filename = Filename;
				filterConditions.Add(@"[filename] = @Filename");
			}	
			if(Type != null)
			{
				query.Parameters.Type = Type;
				filterConditions.Add(@"[type] = @Type");
			}	
			if(SourceID != null)
			{
				query.Parameters.SourceID = SourceID;
				filterConditions.Add(@"[source_id] = @SourceID");
			}	
			if(TemplateID != null)
			{
				query.Parameters.TemplateID = TemplateID;
				filterConditions.Add(@"[template_id] = @TemplateID");
			}	
			if(CreatedbyID != null)
			{
				query.Parameters.CreatedbyID = CreatedbyID;
				filterConditions.Add(@"[CreatedBy] = @Createdby");
			}	
			if(ModifiedbyID != null)
			{
				query.Parameters.ModifiedbyID = ModifiedbyID;
				filterConditions.Add(@"[ModifiedBy] = @Modifiedby");
			}	
			if(AuxField1 != null)
			{
				query.Parameters.AuxField1 = AuxField1;
				filterConditions.Add(@"[AuxField1] = @Auxfield1");
			}	
			if(AuxField2 != null)
			{
				query.Parameters.AuxField2 = AuxField2;
				filterConditions.Add(@"[AuxField2] = @Auxfield2");
			}	
			if(ObjectDate != null || (CreateDate==null && DeleteDate==null))
			{
				Context oContext=new Context();
				query.Parameters.ObjectDate = (ObjectDate==null ? oContext.VersionDate : ObjectDate);
				filterConditions.Add(@"@ObjectDate BETWEEN [CreateDate] AND [DeleteDate]");
			}
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_content] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Contents<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Contents<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Contents<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Contents<DataCollectionType, DataRecordType>()));
		}
		*/

		
		public override IDataReader GetDataReader()
		{
			return new ContentsDataReader(this);
		}
		
		public List<DtoContent> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class ContentsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public ContentsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoContent
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		/// </summary>
		[DataMember]
		public virtual int ObjectID { get; set; }
		
		/// <summary>
		///		CreateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime CreateDate { get; set; }
		
		/// <summary>
		///		DeleteDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime DeleteDate { get; set; }
		
		/// <summary>
		///		Name Property
		/// </summary>
		[DataMember]
		public virtual string Name { get; set; }
		
		/// <summary>
		///		Xml Property
		/// </summary>
		[DataMember]
		public virtual string Xml { get; set; }
		
		/// <summary>
		///		Filename Property
		/// </summary>
		[DataMember]
		public virtual string Filename { get; set; }
		
		/// <summary>
		///		Type Property
		/// </summary>
		[DataMember]
		public virtual int Type { get; set; }
		
		/// <summary>
		///		SourceID Property
		/// </summary>
		[DataMember]
		public virtual int SourceID { get; set; }
		
		/// <summary>
		///		TemplateID Property
		/// </summary>
		[DataMember]
		public virtual int TemplateID { get; set; }
		
		/// <summary>
		///		CreatedbyID Property
		/// </summary>
		[DataMember]
		public virtual int CreatedbyID { get; set; }
		
		/// <summary>
		///		ModifiedbyID Property
		/// </summary>
		[DataMember]
		public virtual int ModifiedbyID { get; set; }
		
		/// <summary>
		///		AuxField1 Property
		/// </summary>
		[DataMember]
		public virtual string AuxField1 { get; set; }
		
		/// <summary>
		///		AuxField2 Property
		/// </summary>
		[DataMember]
		public virtual string AuxField2 { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class Content : Content<Content>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Content
		/// </summary>
		public Content() : this(new Context()) {}
		
		/// <summary>
		/// Creates Content
		/// </summary>
		public Content(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for Content
		/// </summary>
		protected Content(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class Content<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: Content<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Content
		/// </summary>
		public Content() : this(new Context()) {}
		
		/// <summary>
		/// Creates Content
		/// </summary>
		public Content(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_content";

			//Declare Fields
			this.Fields.Add("ObjectID" ,this._ObjectID = new jlib.DataFramework.BaseField<int>(this, Contents.Columns.ObjectID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreateDate" ,this._CreateDate = new jlib.DataFramework.BaseField<DateTime>(this, Contents.Columns.CreateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("DeleteDate" ,this._DeleteDate = new jlib.DataFramework.BaseField<DateTime>(this, Contents.Columns.DeleteDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Name" ,this._Name = new jlib.DataFramework.BaseField<string>(this, Contents.Columns.Name, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Xml" ,this._Xml = new jlib.DataFramework.BaseField<string>(this, Contents.Columns.Xml, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Filename" ,this._Filename = new jlib.DataFramework.BaseField<string>(this, Contents.Columns.Filename, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Type" ,this._Type = new jlib.DataFramework.BaseField<int>(this, Contents.Columns.Type, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("SourceID" ,this._SourceID = new jlib.DataFramework.BaseField<int>(this, Contents.Columns.SourceID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("TemplateID" ,this._TemplateID = new jlib.DataFramework.BaseField<int>(this, Contents.Columns.TemplateID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreatedbyID" ,this._CreatedbyID = new jlib.DataFramework.BaseField<int>(this, Contents.Columns.CreatedbyID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ModifiedbyID" ,this._ModifiedbyID = new jlib.DataFramework.BaseField<int>(this, Contents.Columns.ModifiedbyID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("AuxField1" ,this._AuxField1 = new jlib.DataFramework.BaseField<string>(this, Contents.Columns.AuxField1, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("AuxField2" ,this._AuxField2 = new jlib.DataFramework.BaseField<string>(this, Contents.Columns.AuxField2, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.ContentEx();
		}
		
		/// <summary>
		/// Deserialization method for Content
		/// </summary>
		protected Content(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void ContentEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		///		Database Field:		ObjectID;
		///		Database Parameter:	@Objectid;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_content", FieldName: "ObjectID", IsPrimaryKey=true)]
		public virtual int ObjectID
		{
			get 
			{ 
				return this._ObjectID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ObjectID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ObjectID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ObjectID { get; set; }

			
		/// <summary>
		///		CreateDate Property
		///		Database Field:		CreateDate;
		///		Database Parameter:	@Createdate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_content", FieldName: "CreateDate", IsPrimaryKey=true)]
		public virtual DateTime CreateDate
		{
			get 
			{ 
				return this._CreateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CreateDate { get; set; }

			
		/// <summary>
		///		DeleteDate Property
		///		Database Field:		DeleteDate;
		///		Database Parameter:	@Deletedate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_content", FieldName: "DeleteDate", IsPrimaryKey=true)]
		public virtual DateTime DeleteDate
		{
			get 
			{ 
				return this._DeleteDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._DeleteDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._DeleteDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _DeleteDate { get; set; }

			
		/// <summary>
		///		Name Property
		///		Database Field:		Name;
		///		Database Parameter:	@Name;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_content", FieldName: "Name", IsNullable=true)]
		public virtual string Name
		{
			get 
			{ 
				return (string)this._Name.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Name.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Name.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Name { get; set; }

			
		/// <summary>
		///		Xml Property
		///		Database Field:		Xml;
		///		Database Parameter:	@Xml;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_content", FieldName: "Xml", IsNullable=true)]
		public virtual string Xml
		{
			get 
			{ 
				return (string)this._Xml.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Xml.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Xml.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Xml { get; set; }

			
		/// <summary>
		///		Filename Property
		///		Database Field:		filename;
		///		Database Parameter:	@Filename;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_content", FieldName: "filename", IsNullable=true)]
		public virtual string Filename
		{
			get 
			{ 
				return (string)this._Filename.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Filename.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Filename.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Filename { get; set; }

			
		/// <summary>
		///		Type Property
		///		Database Field:		type;
		///		Database Parameter:	@Type;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_content", FieldName: "type")]
		public virtual int Type
		{
			get 
			{ 
				return this._Type.Value; 
			}

			set 
			{ 
				//Set Value
				this._Type.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Type.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _Type { get; set; }

			
		/// <summary>
		///		SourceID Property
		///		Database Field:		source_id;
		///		Database Parameter:	@SourceID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_content", FieldName: "source_id")]
		public virtual int SourceID
		{
			get 
			{ 
				return this._SourceID.Value; 
			}

			set 
			{ 
				//Set Value
				this._SourceID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._SourceID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _SourceID { get; set; }

			
		/// <summary>
		///		TemplateID Property
		///		Database Field:		template_id;
		///		Database Parameter:	@TemplateID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_content", FieldName: "template_id")]
		public virtual int TemplateID
		{
			get 
			{ 
				return this._TemplateID.Value; 
			}

			set 
			{ 
				//Set Value
				this._TemplateID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._TemplateID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _TemplateID { get; set; }

			
		/// <summary>
		///		CreatedbyID Property
		///		Database Field:		CreatedBy;
		///		Database Parameter:	@Createdby;
		///		Primary Key:		No;
		///		Foreign Key:		Yes;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_content", FieldName: "CreatedBy", IsForeignKey=true)]
		public virtual int CreatedbyID
		{
			get 
			{ 
				return this._CreatedbyID.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreatedbyID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreatedbyID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

				//Set Foreign Key Object to Null
				this._CreatedBy = null;
			}
		}
		protected jlib.DataFramework.BaseField<int> _CreatedbyID { get; set; }

		/// <summary>
		///		CreatedBy Foreign Object Property
		///		Foreign Key Field:	CreatedBy
		///		Primary Key Field:	ID
		/// </summary>
		[jlib.DataFramework.Field("d_content", "CreatedBy")]
		public virtual BehindUser CreatedBy
		{
			get
			{
				if(this._CreatedBy == null )
				{
					this._CreatedBy = BehindUsers.GetByField(ID:(int)this.CreatedbyID).Execute().FirstOrDefault();
				}
				
						
				return this._CreatedBy;
			}
			
			set
			{
				this._CreatedBy = value;	
			}
		}
		protected BehindUser _CreatedBy { get; set; }
			
		/// <summary>
		///		ModifiedbyID Property
		///		Database Field:		ModifiedBy;
		///		Database Parameter:	@Modifiedby;
		///		Primary Key:		No;
		///		Foreign Key:		Yes;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_content", FieldName: "ModifiedBy", IsForeignKey=true)]
		public virtual int ModifiedbyID
		{
			get 
			{ 
				return this._ModifiedbyID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ModifiedbyID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ModifiedbyID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

				//Set Foreign Key Object to Null
				this._ModifiedBy = null;
			}
		}
		protected jlib.DataFramework.BaseField<int> _ModifiedbyID { get; set; }

		/// <summary>
		///		ModifiedBy Foreign Object Property
		///		Foreign Key Field:	ModifiedBy
		///		Primary Key Field:	ID
		/// </summary>
		[jlib.DataFramework.Field("d_content", "ModifiedBy")]
		public virtual BehindUser ModifiedBy
		{
			get
			{
				if(this._ModifiedBy == null )
				{
					this._ModifiedBy = BehindUsers.GetByField(ID:(int)this.ModifiedbyID).Execute().FirstOrDefault();
				}
				
						
				return this._ModifiedBy;
			}
			
			set
			{
				this._ModifiedBy = value;	
			}
		}
		protected BehindUser _ModifiedBy { get; set; }
			
		/// <summary>
		///		AuxField1 Property
		///		Database Field:		AuxField1;
		///		Database Parameter:	@Auxfield1;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_content", FieldName: "AuxField1", IsNullable=true)]
		public virtual string AuxField1
		{
			get 
			{ 
				return (string)this._AuxField1.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._AuxField1.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._AuxField1.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _AuxField1 { get; set; }

			
		/// <summary>
		///		AuxField2 Property
		///		Database Field:		AuxField2;
		///		Database Parameter:	@Auxfield2;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_content", FieldName: "AuxField2", IsNullable=true)]
		public virtual string AuxField2
		{
			get 
			{ 
				return (string)this._AuxField2.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._AuxField2.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._AuxField2.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _AuxField2 { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoContent Dto)
		{
			//Set Values
			this._ObjectID.SetChangedValueIfDifferent(Dto.ObjectID);
			this._CreateDate.SetChangedValueIfDifferent(Dto.CreateDate);
			this._DeleteDate.SetChangedValueIfDifferent(Dto.DeleteDate);
			this._Name.SetChangedValueIfDifferent(Dto.Name);
			this._Xml.SetChangedValueIfDifferent(Dto.Xml);
			this._Filename.SetChangedValueIfDifferent(Dto.Filename);
			this._Type.SetChangedValueIfDifferent(Dto.Type);
			this._SourceID.SetChangedValueIfDifferent(Dto.SourceID);
			this._TemplateID.SetChangedValueIfDifferent(Dto.TemplateID);
			this._CreatedbyID.SetChangedValueIfDifferent(Dto.CreatedbyID);
			this._ModifiedbyID.SetChangedValueIfDifferent(Dto.ModifiedbyID);
			this._AuxField1.SetChangedValueIfDifferent(Dto.AuxField1);
			this._AuxField2.SetChangedValueIfDifferent(Dto.AuxField2);
		}
		public virtual DtoContent ToDto()
		{
			return this.ToDto(new DtoContent());
		}
		
		public virtual DtoContent ToDto(DtoContent dto)
		{
			//Set Values
			dto.ObjectID = this.ObjectID;
			dto.CreateDate = this.CreateDate;
			dto.DeleteDate = this.DeleteDate;
			dto.Name = this.Name;
			dto.Xml = this.Xml;
			dto.Filename = this.Filename;
			dto.Type = this.Type;
			dto.SourceID = this.SourceID;
			dto.TemplateID = this.TemplateID;
			dto.CreatedbyID = this.CreatedbyID;
			dto.ModifiedbyID = this.ModifiedbyID;
			dto.AuxField1 = this.AuxField1;
			dto.AuxField2 = this.AuxField2;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				//Create Internal Object 'createdby'
				case "createdby":
					this._CreatedBy = new BehindUser();
					this._CreatedBy.Import(Row, InternalObjectIdentifier);
					break;
				
				//Create Internal Object 'modifiedby'
				case "modifiedby":
					this._ModifiedBy = new BehindUser();
					this._ModifiedBy.Import(Row, InternalObjectIdentifier);
					break;
				
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****

				public static DataRecordType LoadByPk(int ObjectID)
				{
					Context oContext=new Context();
					//Create Sql Connection
					SqlHelper sqlHelper = oContext.GetSqlHelper();
			
					//Load
					DataRecordType item = LoadByPk(sqlHelper, ObjectID, oContext.VersionDate);
			
					//Return
					return item;
				}

		public static DataRecordType LoadByPk(int ObjectID,  DateTime ObjectDate)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ObjectID, ObjectDate);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ObjectID,  DateTime ObjectDate)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_content]  WITH (NOLOCK)
										WHERE
											
											[ObjectID] = @PK_Objectid		AND @ObjectDate BETWEEN [CreateDate] AND [DeleteDate]

											",
											"@PK_Objectid", ObjectID, "@ObjectDate", ObjectDate
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class EmailLogs : EmailLogs<EmailLogs, EmailLog>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates EmailLogs
		/// </summary>
		public EmailLogs() : this(new Context()) {}
		
		/// <summary>
		/// Creates EmailLogs
		/// </summary>
		public EmailLogs(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class EmailLogs<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: EmailLog<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> PostedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "posted_date", PropertyName: "PostedDate", ParameterName: "@PostedDate", PkParameterName: "@PK_PostedDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Subject = new jlib.DataFramework.BaseColumn<string>(FieldName: "subject", PropertyName: "Subject", ParameterName: "@Subject", PkParameterName: "@PK_Subject", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Body = new jlib.DataFramework.BaseColumn<string>(FieldName: "body", PropertyName: "Body", ParameterName: "@Body", PkParameterName: "@PK_Body", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> EmailFrom = new jlib.DataFramework.BaseColumn<string>(FieldName: "email_from", PropertyName: "EmailFrom", ParameterName: "@EmailFrom", PkParameterName: "@PK_EmailFrom", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> EmailTo = new jlib.DataFramework.BaseColumn<string>(FieldName: "email_to", PropertyName: "EmailTo", ParameterName: "@EmailTo", PkParameterName: "@PK_EmailTo", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<bool> IsHtml = new jlib.DataFramework.BaseColumn<bool>(FieldName: "is_html", PropertyName: "IsHtml", ParameterName: "@IsHtml", PkParameterName: "@PK_IsHtml", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> Sent = new jlib.DataFramework.BaseColumn<bool>(FieldName: "sent", PropertyName: "Sent", ParameterName: "@Sent", PkParameterName: "@PK_Sent", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> ErrorMessage = new jlib.DataFramework.BaseColumn<string>(FieldName: "error_message", PropertyName: "ErrorMessage", ParameterName: "@ErrorMessage", PkParameterName: "@PK_ErrorMessage", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> UserID = new jlib.DataFramework.BaseColumn<int>(FieldName: "user_id", PropertyName: "UserID", ParameterName: "@UserID", PkParameterName: "@PK_UserID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> Deleted = new jlib.DataFramework.BaseColumn<bool>(FieldName: "deleted", PropertyName: "Deleted", ParameterName: "@Deleted", PkParameterName: "@PK_Deleted", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Attachments = new jlib.DataFramework.BaseColumn<string>(FieldName: "attachments", PropertyName: "Attachments", ParameterName: "@Attachments", PkParameterName: "@PK_Attachments", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> EmailKey = new jlib.DataFramework.BaseColumn<string>(FieldName: "email_key", PropertyName: "EmailKey", ParameterName: "@EmailKey", PkParameterName: "@PK_EmailKey", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> EmailCc = new jlib.DataFramework.BaseColumn<string>(FieldName: "email_cc", PropertyName: "EmailCc", ParameterName: "@EmailCc", PkParameterName: "@PK_EmailCc", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> EmailBcc = new jlib.DataFramework.BaseColumn<string>(FieldName: "email_bcc", PropertyName: "EmailBcc", ParameterName: "@EmailBcc", PkParameterName: "@PK_EmailBcc", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates EmailLogs
		/// </summary>
		public EmailLogs() : this(new Context()) {}
		
		/// <summary>
		/// Creates EmailLogs
		/// </summary>
		public EmailLogs(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_email_log";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = EmailLogs<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("PostedDate", this.PostedDate = EmailLogs<DataCollectionType, DataRecordType>.Columns.PostedDate);
			base.Columns.Add("Subject", this.Subject = EmailLogs<DataCollectionType, DataRecordType>.Columns.Subject);
			base.Columns.Add("Body", this.Body = EmailLogs<DataCollectionType, DataRecordType>.Columns.Body);
			base.Columns.Add("EmailFrom", this.EmailFrom = EmailLogs<DataCollectionType, DataRecordType>.Columns.EmailFrom);
			base.Columns.Add("EmailTo", this.EmailTo = EmailLogs<DataCollectionType, DataRecordType>.Columns.EmailTo);
			base.Columns.Add("IsHtml", this.IsHtml = EmailLogs<DataCollectionType, DataRecordType>.Columns.IsHtml);
			base.Columns.Add("Sent", this.Sent = EmailLogs<DataCollectionType, DataRecordType>.Columns.Sent);
			base.Columns.Add("ErrorMessage", this.ErrorMessage = EmailLogs<DataCollectionType, DataRecordType>.Columns.ErrorMessage);
			base.Columns.Add("UserID", this.UserID = EmailLogs<DataCollectionType, DataRecordType>.Columns.UserID);
			base.Columns.Add("Deleted", this.Deleted = EmailLogs<DataCollectionType, DataRecordType>.Columns.Deleted);
			base.Columns.Add("Attachments", this.Attachments = EmailLogs<DataCollectionType, DataRecordType>.Columns.Attachments);
			base.Columns.Add("EmailKey", this.EmailKey = EmailLogs<DataCollectionType, DataRecordType>.Columns.EmailKey);
			base.Columns.Add("EmailCc", this.EmailCc = EmailLogs<DataCollectionType, DataRecordType>.Columns.EmailCc);
			base.Columns.Add("EmailBcc", this.EmailBcc = EmailLogs<DataCollectionType, DataRecordType>.Columns.EmailBcc);
			
			//Call the secondary constructor method
			this.EmailLogsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void EmailLogsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> PostedDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> Subject { get; set; }
		public jlib.DataFramework.BaseColumn<string> Body { get; set; }
		public jlib.DataFramework.BaseColumn<string> EmailFrom { get; set; }
		public jlib.DataFramework.BaseColumn<string> EmailTo { get; set; }
		public jlib.DataFramework.BaseColumn<bool> IsHtml { get; set; }
		public jlib.DataFramework.BaseColumn<bool> Sent { get; set; }
		public jlib.DataFramework.BaseColumn<string> ErrorMessage { get; set; }
		public jlib.DataFramework.BaseColumn<int> UserID { get; set; }
		public jlib.DataFramework.BaseColumn<bool> Deleted { get; set; }
		public jlib.DataFramework.BaseColumn<string> Attachments { get; set; }
		public jlib.DataFramework.BaseColumn<string> EmailKey { get; set; }
		public jlib.DataFramework.BaseColumn<string> EmailCc { get; set; }
		public jlib.DataFramework.BaseColumn<string> EmailBcc { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_email_log] WITH (NOLOCK) WHERE [deleted]=0", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_email_log] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_email_log");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_email_log", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, DateTime? PostedDate = null, string Subject = null, string Body = null, string EmailFrom = null, string EmailTo = null, bool? IsHtml = null, bool? Sent = null, string ErrorMessage = null, int? UserID = null, bool? Deleted = null, string Attachments = null, string EmailKey = null, string EmailCc = null, string EmailBcc = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
			if(Deleted == null)
			{
				filterConditions.Add(@"[deleted]=0");
			}
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(PostedDate != null)
			{
				query.Parameters.PostedDate = PostedDate;
				filterConditions.Add(@"[posted_date] = @PostedDate");
			}	
			if(Subject != null)
			{
				query.Parameters.Subject = Subject;
				filterConditions.Add(@"[subject] = @Subject");
			}	
			if(Body != null)
			{
				query.Parameters.Body = Body;
				filterConditions.Add(@"[body] LIKE @Body");
			}	
			if(EmailFrom != null)
			{
				query.Parameters.EmailFrom = EmailFrom;
				filterConditions.Add(@"[email_from] = @EmailFrom");
			}	
			if(EmailTo != null)
			{
				query.Parameters.EmailTo = EmailTo;
				filterConditions.Add(@"[email_to] = @EmailTo");
			}	
			if(IsHtml != null)
			{
				query.Parameters.IsHtml = IsHtml;
				filterConditions.Add(@"[is_html] = @IsHtml");
			}	
			if(Sent != null)
			{
				query.Parameters.Sent = Sent;
				filterConditions.Add(@"[sent] = @Sent");
			}	
			if(ErrorMessage != null)
			{
				query.Parameters.ErrorMessage = ErrorMessage;
				filterConditions.Add(@"[error_message] = @ErrorMessage");
			}	
			if(UserID != null)
			{
				query.Parameters.UserID = UserID;
				filterConditions.Add(@"[user_id] = @UserID");
			}	
			if(Deleted != null)
			{
				query.Parameters.Deleted = Deleted;
				filterConditions.Add(@"[deleted] = @Deleted");
			}	
			if(Attachments != null)
			{
				query.Parameters.Attachments = Attachments;
				filterConditions.Add(@"[attachments] LIKE @Attachments");
			}	
			if(EmailKey != null)
			{
				query.Parameters.EmailKey = EmailKey;
				filterConditions.Add(@"[email_key] = @EmailKey");
			}	
			if(EmailCc != null)
			{
				query.Parameters.EmailCc = EmailCc;
				filterConditions.Add(@"[email_cc] = @EmailCc");
			}	
			if(EmailBcc != null)
			{
				query.Parameters.EmailBcc = EmailBcc;
				filterConditions.Add(@"[email_bcc] = @EmailBcc");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_email_log] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<EmailLogs<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new EmailLogs<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<EmailLogs<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new EmailLogs<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_email_log] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new EmailLogsDataReader(this);
		}
		
		public List<DtoEmailLog> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class EmailLogsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public EmailLogsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoEmailLog
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		PostedDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime PostedDate { get; set; }
		
		/// <summary>
		///		Subject Property
		/// </summary>
		[DataMember]
		public virtual string Subject { get; set; }
		
		/// <summary>
		///		Body Property
		/// </summary>
		[DataMember]
		public virtual string Body { get; set; }
		
		/// <summary>
		///		EmailFrom Property
		/// </summary>
		[DataMember]
		public virtual string EmailFrom { get; set; }
		
		/// <summary>
		///		EmailTo Property
		/// </summary>
		[DataMember]
		public virtual string EmailTo { get; set; }
		
		/// <summary>
		///		IsHtml Property
		/// </summary>
		[DataMember]
		public virtual bool IsHtml { get; set; }
		
		/// <summary>
		///		Sent Property
		/// </summary>
		[DataMember]
		public virtual bool Sent { get; set; }
		
		/// <summary>
		///		ErrorMessage Property
		/// </summary>
		[DataMember]
		public virtual string ErrorMessage { get; set; }
		
		/// <summary>
		///		UserID Property
		/// </summary>
		[DataMember]
		public virtual int UserID { get; set; }
		
		/// <summary>
		///		Deleted Property
		/// </summary>
		[DataMember]
		public virtual bool Deleted { get; set; }
		
		/// <summary>
		///		Attachments Property
		/// </summary>
		[DataMember]
		public virtual string Attachments { get; set; }
		
		/// <summary>
		///		EmailKey Property
		/// </summary>
		[DataMember]
		public virtual string EmailKey { get; set; }
		
		/// <summary>
		///		EmailCc Property
		/// </summary>
		[DataMember]
		public virtual string EmailCc { get; set; }
		
		/// <summary>
		///		EmailBcc Property
		/// </summary>
		[DataMember]
		public virtual string EmailBcc { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class EmailLog : EmailLog<EmailLog>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates EmailLog
		/// </summary>
		public EmailLog() : this(new Context()) {}
		
		/// <summary>
		/// Creates EmailLog
		/// </summary>
		public EmailLog(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for EmailLog
		/// </summary>
		protected EmailLog(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class EmailLog<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: EmailLog<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates EmailLog
		/// </summary>
		public EmailLog() : this(new Context()) {}
		
		/// <summary>
		/// Creates EmailLog
		/// </summary>
		public EmailLog(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_email_log";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, EmailLogs.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostedDate" ,this._PostedDate = new jlib.DataFramework.BaseField<DateTime>(this, EmailLogs.Columns.PostedDate, InitialValue: DateTime.Now, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Subject" ,this._Subject = new jlib.DataFramework.BaseField<string>(this, EmailLogs.Columns.Subject, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Body" ,this._Body = new jlib.DataFramework.BaseField<string>(this, EmailLogs.Columns.Body, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("EmailFrom" ,this._EmailFrom = new jlib.DataFramework.BaseField<string>(this, EmailLogs.Columns.EmailFrom, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("EmailTo" ,this._EmailTo = new jlib.DataFramework.BaseField<string>(this, EmailLogs.Columns.EmailTo, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("IsHtml" ,this._IsHtml = new jlib.DataFramework.BaseField<bool>(this, EmailLogs.Columns.IsHtml, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Sent" ,this._Sent = new jlib.DataFramework.BaseField<bool>(this, EmailLogs.Columns.Sent, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ErrorMessage" ,this._ErrorMessage = new jlib.DataFramework.BaseField<string>(this, EmailLogs.Columns.ErrorMessage, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserID" ,this._UserID = new jlib.DataFramework.BaseField<int>(this, EmailLogs.Columns.UserID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Deleted" ,this._Deleted = new jlib.DataFramework.BaseField<bool>(this, EmailLogs.Columns.Deleted, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Attachments" ,this._Attachments = new jlib.DataFramework.BaseField<string>(this, EmailLogs.Columns.Attachments, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("EmailKey" ,this._EmailKey = new jlib.DataFramework.BaseField<string>(this, EmailLogs.Columns.EmailKey, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("EmailCc" ,this._EmailCc = new jlib.DataFramework.BaseField<string>(this, EmailLogs.Columns.EmailCc, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("EmailBcc" ,this._EmailBcc = new jlib.DataFramework.BaseField<string>(this, EmailLogs.Columns.EmailBcc, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.EmailLogEx();
		}
		
		/// <summary>
		/// Deserialization method for EmailLog
		/// </summary>
		protected EmailLog(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void EmailLogEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		PostedDate Property
		///		Database Field:		posted_date;
		///		Database Parameter:	@PostedDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "posted_date")]
		public virtual DateTime PostedDate
		{
			get 
			{ 
				return this._PostedDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._PostedDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _PostedDate { get; set; }

			
		/// <summary>
		///		Subject Property
		///		Database Field:		subject;
		///		Database Parameter:	@Subject;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "subject", IsNullable=true)]
		public virtual string Subject
		{
			get 
			{ 
				return (string)this._Subject.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Subject.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Subject.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Subject { get; set; }

			
		/// <summary>
		///		Body Property
		///		Database Field:		body;
		///		Database Parameter:	@Body;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "body", IsNullable=true)]
		public virtual string Body
		{
			get 
			{ 
				return (string)this._Body.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Body.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Body.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Body { get; set; }

			
		/// <summary>
		///		EmailFrom Property
		///		Database Field:		email_from;
		///		Database Parameter:	@EmailFrom;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "email_from", IsNullable=true)]
		public virtual string EmailFrom
		{
			get 
			{ 
				return (string)this._EmailFrom.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._EmailFrom.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._EmailFrom.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _EmailFrom { get; set; }

			
		/// <summary>
		///		EmailTo Property
		///		Database Field:		email_to;
		///		Database Parameter:	@EmailTo;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "email_to", IsNullable=true)]
		public virtual string EmailTo
		{
			get 
			{ 
				return (string)this._EmailTo.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._EmailTo.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._EmailTo.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _EmailTo { get; set; }

			
		/// <summary>
		///		IsHtml Property
		///		Database Field:		is_html;
		///		Database Parameter:	@IsHtml;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "is_html")]
		public virtual bool IsHtml
		{
			get 
			{ 
				return this._IsHtml.Value; 
			}

			set 
			{ 
				//Set Value
				this._IsHtml.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._IsHtml.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _IsHtml { get; set; }

			
		/// <summary>
		///		Sent Property
		///		Database Field:		sent;
		///		Database Parameter:	@Sent;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "sent")]
		public virtual bool Sent
		{
			get 
			{ 
				return this._Sent.Value; 
			}

			set 
			{ 
				//Set Value
				this._Sent.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Sent.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _Sent { get; set; }

			
		/// <summary>
		///		ErrorMessage Property
		///		Database Field:		error_message;
		///		Database Parameter:	@ErrorMessage;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "error_message", IsNullable=true)]
		public virtual string ErrorMessage
		{
			get 
			{ 
				return (string)this._ErrorMessage.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._ErrorMessage.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ErrorMessage.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _ErrorMessage { get; set; }

			
		/// <summary>
		///		UserID Property
		///		Database Field:		user_id;
		///		Database Parameter:	@UserID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "user_id")]
		public virtual int UserID
		{
			get 
			{ 
				return this._UserID.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _UserID { get; set; }

			
		/// <summary>
		///		Deleted Property
		///		Database Field:		deleted;
		///		Database Parameter:	@Deleted;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "deleted")]
		public virtual bool Deleted
		{
			get 
			{ 
				return this._Deleted.Value; 
			}

			set 
			{ 
				//Set Value
				this._Deleted.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Deleted.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _Deleted { get; set; }

			
		/// <summary>
		///		Attachments Property
		///		Database Field:		attachments;
		///		Database Parameter:	@Attachments;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "attachments", IsNullable=true)]
		public virtual string Attachments
		{
			get 
			{ 
				return (string)this._Attachments.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Attachments.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Attachments.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Attachments { get; set; }

			
		/// <summary>
		///		EmailKey Property
		///		Database Field:		email_key;
		///		Database Parameter:	@EmailKey;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "email_key", IsNullable=true)]
		public virtual string EmailKey
		{
			get 
			{ 
				return (string)this._EmailKey.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._EmailKey.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._EmailKey.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _EmailKey { get; set; }

			
		/// <summary>
		///		EmailCc Property
		///		Database Field:		email_cc;
		///		Database Parameter:	@EmailCc;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "email_cc", IsNullable=true)]
		public virtual string EmailCc
		{
			get 
			{ 
				return (string)this._EmailCc.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._EmailCc.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._EmailCc.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _EmailCc { get; set; }

			
		/// <summary>
		///		EmailBcc Property
		///		Database Field:		email_bcc;
		///		Database Parameter:	@EmailBcc;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_email_log", FieldName: "email_bcc", IsNullable=true)]
		public virtual string EmailBcc
		{
			get 
			{ 
				return (string)this._EmailBcc.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._EmailBcc.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._EmailBcc.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _EmailBcc { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoEmailLog Dto)
		{
			//Set Values
			this._PostedDate.SetChangedValueIfDifferent(Dto.PostedDate);
			this._Subject.SetChangedValueIfDifferent(Dto.Subject);
			this._Body.SetChangedValueIfDifferent(Dto.Body);
			this._EmailFrom.SetChangedValueIfDifferent(Dto.EmailFrom);
			this._EmailTo.SetChangedValueIfDifferent(Dto.EmailTo);
			this._IsHtml.SetChangedValueIfDifferent(Dto.IsHtml);
			this._Sent.SetChangedValueIfDifferent(Dto.Sent);
			this._ErrorMessage.SetChangedValueIfDifferent(Dto.ErrorMessage);
			this._UserID.SetChangedValueIfDifferent(Dto.UserID);
			this._Deleted.SetChangedValueIfDifferent(Dto.Deleted);
			this._Attachments.SetChangedValueIfDifferent(Dto.Attachments);
			this._EmailKey.SetChangedValueIfDifferent(Dto.EmailKey);
			this._EmailCc.SetChangedValueIfDifferent(Dto.EmailCc);
			this._EmailBcc.SetChangedValueIfDifferent(Dto.EmailBcc);
		}
		public virtual DtoEmailLog ToDto()
		{
			return this.ToDto(new DtoEmailLog());
		}
		
		public virtual DtoEmailLog ToDto(DtoEmailLog dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.PostedDate = this.PostedDate;
			dto.Subject = this.Subject;
			dto.Body = this.Body;
			dto.EmailFrom = this.EmailFrom;
			dto.EmailTo = this.EmailTo;
			dto.IsHtml = this.IsHtml;
			dto.Sent = this.Sent;
			dto.ErrorMessage = this.ErrorMessage;
			dto.UserID = this.UserID;
			dto.Deleted = this.Deleted;
			dto.Attachments = this.Attachments;
			dto.EmailKey = this.EmailKey;
			dto.EmailCc = this.EmailCc;
			dto.EmailBcc = this.EmailBcc;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_email_log] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_email_log]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class Enrollments : Enrollments<Enrollments, Enrollment>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Enrollments
		/// </summary>
		public Enrollments() : this(new Context()) {}
		
		/// <summary>
		/// Creates Enrollments
		/// </summary>
		public Enrollments(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Enrollments<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: Enrollment<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> UserID = new jlib.DataFramework.BaseColumn<int>(FieldName: "user_id", PropertyName: "UserID", ParameterName: "@UserID", PkParameterName: "@PK_UserID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> ReleaseID = new jlib.DataFramework.BaseColumn<int>(FieldName: "release_id", PropertyName: "ReleaseID", ParameterName: "@ReleaseID", PkParameterName: "@PK_ReleaseID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<bool> IsDegreeseeking = new jlib.DataFramework.BaseColumn<bool>(FieldName: "is_degreeseeking", PropertyName: "IsDegreeseeking", ParameterName: "@IsDegreeseeking", PkParameterName: "@PK_IsDegreeseeking", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> IsEnrolled = new jlib.DataFramework.BaseColumn<bool>(FieldName: "is_enrolled", PropertyName: "IsEnrolled", ParameterName: "@IsEnrolled", PkParameterName: "@PK_IsEnrolled", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> OfferingID = new jlib.DataFramework.BaseColumn<int>(FieldName: "offering_id", PropertyName: "OfferingID", ParameterName: "@OfferingID", PkParameterName: "@PK_OfferingID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Enrollments
		/// </summary>
		public Enrollments() : this(new Context()) {}
		
		/// <summary>
		/// Creates Enrollments
		/// </summary>
		public Enrollments(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_enrollments";
			
			//Set Columns
			base.Columns.Add("UserID", this.UserID = Enrollments<DataCollectionType, DataRecordType>.Columns.UserID);
			base.Columns.Add("ReleaseID", this.ReleaseID = Enrollments<DataCollectionType, DataRecordType>.Columns.ReleaseID);
			base.Columns.Add("IsDegreeseeking", this.IsDegreeseeking = Enrollments<DataCollectionType, DataRecordType>.Columns.IsDegreeseeking);
			base.Columns.Add("IsEnrolled", this.IsEnrolled = Enrollments<DataCollectionType, DataRecordType>.Columns.IsEnrolled);
			base.Columns.Add("OfferingID", this.OfferingID = Enrollments<DataCollectionType, DataRecordType>.Columns.OfferingID);
			
			//Call the secondary constructor method
			this.EnrollmentsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void EnrollmentsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> UserID { get; set; }
		public jlib.DataFramework.BaseColumn<int> ReleaseID { get; set; }
		public jlib.DataFramework.BaseColumn<bool> IsDegreeseeking { get; set; }
		public jlib.DataFramework.BaseColumn<bool> IsEnrolled { get; set; }
		public jlib.DataFramework.BaseColumn<int> OfferingID { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_enrollments] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_enrollments] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_enrollments");
		}
		
		
		public static QueryPackage GetByField(int? UserID = null, int? ReleaseID = null, bool? IsDegreeseeking = null, bool? IsEnrolled = null, int? OfferingID = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(UserID != null)
			{
				query.Parameters.UserID = UserID;
				filterConditions.Add(@"[user_id] = @UserID");
			}	
			if(ReleaseID != null)
			{
				query.Parameters.ReleaseID = ReleaseID;
				filterConditions.Add(@"[release_id] = @ReleaseID");
			}	
			if(IsDegreeseeking != null)
			{
				query.Parameters.IsDegreeseeking = IsDegreeseeking;
				filterConditions.Add(@"[is_degreeseeking] = @IsDegreeseeking");
			}	
			if(IsEnrolled != null)
			{
				query.Parameters.IsEnrolled = IsEnrolled;
				filterConditions.Add(@"[is_enrolled] = @IsEnrolled");
			}	
			if(OfferingID != null)
			{
				query.Parameters.OfferingID = OfferingID;
				filterConditions.Add(@"[offering_id] = @OfferingID");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_enrollments] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Enrollments<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Enrollments<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Enrollments<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Enrollments<DataCollectionType, DataRecordType>()));
		}
		*/

		
		public override IDataReader GetDataReader()
		{
			return new EnrollmentsDataReader(this);
		}
		
		public List<DtoEnrollment> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class EnrollmentsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public EnrollmentsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoEnrollment
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		UserID Property
		/// </summary>
		[DataMember]
		public virtual int UserID { get; set; }
		
		/// <summary>
		///		ReleaseID Property
		/// </summary>
		[DataMember]
		public virtual int ReleaseID { get; set; }
		
		/// <summary>
		///		IsDegreeseeking Property
		/// </summary>
		[DataMember]
		public virtual bool IsDegreeseeking { get; set; }
		
		/// <summary>
		///		IsEnrolled Property
		/// </summary>
		[DataMember]
		public virtual bool IsEnrolled { get; set; }
		
		/// <summary>
		///		OfferingID Property
		/// </summary>
		[DataMember]
		public virtual int? OfferingID { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class Enrollment : Enrollment<Enrollment>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Enrollment
		/// </summary>
		public Enrollment() : this(new Context()) {}
		
		/// <summary>
		/// Creates Enrollment
		/// </summary>
		public Enrollment(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for Enrollment
		/// </summary>
		protected Enrollment(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class Enrollment<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: Enrollment<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Enrollment
		/// </summary>
		public Enrollment() : this(new Context()) {}
		
		/// <summary>
		/// Creates Enrollment
		/// </summary>
		public Enrollment(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_enrollments";

			//Declare Fields
			this.Fields.Add("UserID" ,this._UserID = new jlib.DataFramework.BaseField<int>(this, Enrollments.Columns.UserID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ReleaseID" ,this._ReleaseID = new jlib.DataFramework.BaseField<int>(this, Enrollments.Columns.ReleaseID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("IsDegreeseeking" ,this._IsDegreeseeking = new jlib.DataFramework.BaseField<bool>(this, Enrollments.Columns.IsDegreeseeking, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("IsEnrolled" ,this._IsEnrolled = new jlib.DataFramework.BaseField<bool>(this, Enrollments.Columns.IsEnrolled, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("OfferingID" ,this._OfferingID = new jlib.DataFramework.BaseField<int>(this, Enrollments.Columns.OfferingID, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.EnrollmentEx();
		}
		
		/// <summary>
		/// Deserialization method for Enrollment
		/// </summary>
		protected Enrollment(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void EnrollmentEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		UserID Property
		///		Database Field:		user_id;
		///		Database Parameter:	@UserID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_enrollments", FieldName: "user_id", IsPrimaryKey=true)]
		public virtual int UserID
		{
			get 
			{ 
				return this._UserID.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _UserID { get; set; }

			
		/// <summary>
		///		ReleaseID Property
		///		Database Field:		release_id;
		///		Database Parameter:	@ReleaseID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_enrollments", FieldName: "release_id", IsPrimaryKey=true)]
		public virtual int ReleaseID
		{
			get 
			{ 
				return this._ReleaseID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ReleaseID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ReleaseID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ReleaseID { get; set; }

			
		/// <summary>
		///		IsDegreeseeking Property
		///		Database Field:		is_degreeseeking;
		///		Database Parameter:	@IsDegreeseeking;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_enrollments", FieldName: "is_degreeseeking")]
		public virtual bool IsDegreeseeking
		{
			get 
			{ 
				return this._IsDegreeseeking.Value; 
			}

			set 
			{ 
				//Set Value
				this._IsDegreeseeking.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._IsDegreeseeking.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _IsDegreeseeking { get; set; }

			
		/// <summary>
		///		IsEnrolled Property
		///		Database Field:		is_enrolled;
		///		Database Parameter:	@IsEnrolled;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_enrollments", FieldName: "is_enrolled")]
		public virtual bool IsEnrolled
		{
			get 
			{ 
				return this._IsEnrolled.Value; 
			}

			set 
			{ 
				//Set Value
				this._IsEnrolled.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._IsEnrolled.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _IsEnrolled { get; set; }

			
		/// <summary>
		///		OfferingID Property
		///		Database Field:		offering_id;
		///		Database Parameter:	@OfferingID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_enrollments", FieldName: "offering_id", IsNullable=true)]
		public virtual int? OfferingID
		{
			get 
			{ 
				return (int?)this._OfferingID.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._OfferingID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._OfferingID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _OfferingID { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoEnrollment Dto)
		{
			//Set Values
			this._UserID.SetChangedValueIfDifferent(Dto.UserID);
			this._ReleaseID.SetChangedValueIfDifferent(Dto.ReleaseID);
			this._IsDegreeseeking.SetChangedValueIfDifferent(Dto.IsDegreeseeking);
			this._IsEnrolled.SetChangedValueIfDifferent(Dto.IsEnrolled);
			this._OfferingID.SetChangedValueIfDifferent(Dto.OfferingID);
		}
		public virtual DtoEnrollment ToDto()
		{
			return this.ToDto(new DtoEnrollment());
		}
		
		public virtual DtoEnrollment ToDto(DtoEnrollment dto)
		{
			//Set Values
			dto.UserID = this.UserID;
			dto.ReleaseID = this.ReleaseID;
			dto.IsDegreeseeking = this.IsDegreeseeking;
			dto.IsEnrolled = this.IsEnrolled;
			dto.OfferingID = this.OfferingID;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****



		public static DataRecordType LoadByPk(int UserID, int ReleaseID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, UserID, ReleaseID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int UserID, int ReleaseID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_enrollments]  WITH (NOLOCK)
										WHERE
											
											[user_id] = @PK_UserID
											AND [release_id] = @PK_ReleaseID
											",
											"@PK_UserID", UserID, "@PK_ReleaseID", ReleaseID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class Evaluations : Evaluations<Evaluations, Evaluation>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Evaluations
		/// </summary>
		public Evaluations() : this(new Context()) {}
		
		/// <summary>
		/// Creates Evaluations
		/// </summary>
		public Evaluations(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Evaluations<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: Evaluation<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> PostedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "posted_date", PropertyName: "PostedDate", ParameterName: "@PostedDate", PkParameterName: "@PK_PostedDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Comment = new jlib.DataFramework.BaseColumn<string>(FieldName: "comment", PropertyName: "Comment", ParameterName: "@Comment", PkParameterName: "@PK_Comment", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> Score = new jlib.DataFramework.BaseColumn<int>(FieldName: "score", PropertyName: "Score", ParameterName: "@Score", PkParameterName: "@PK_Score", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Username = new jlib.DataFramework.BaseColumn<string>(FieldName: "username", PropertyName: "Username", ParameterName: "@Username", PkParameterName: "@PK_Username", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> UserID = new jlib.DataFramework.BaseColumn<int>(FieldName: "user_id", PropertyName: "UserID", ParameterName: "@UserID", PkParameterName: "@PK_UserID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> CourseCode = new jlib.DataFramework.BaseColumn<string>(FieldName: "course_code", PropertyName: "CourseCode", ParameterName: "@CourseCode", PkParameterName: "@PK_CourseCode", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<short> LessonNumber = new jlib.DataFramework.BaseColumn<short>(FieldName: "lesson_number", PropertyName: "LessonNumber", ParameterName: "@LessonNumber", PkParameterName: "@PK_LessonNumber", DBType: DbType.Int16, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> Reviewed = new jlib.DataFramework.BaseColumn<bool>(FieldName: "Reviewed", PropertyName: "Reviewed", ParameterName: "@Reviewed", PkParameterName: "@PK_Reviewed", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Evaluations
		/// </summary>
		public Evaluations() : this(new Context()) {}
		
		/// <summary>
		/// Creates Evaluations
		/// </summary>
		public Evaluations(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_evaluations";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = Evaluations<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("PostedDate", this.PostedDate = Evaluations<DataCollectionType, DataRecordType>.Columns.PostedDate);
			base.Columns.Add("Comment", this.Comment = Evaluations<DataCollectionType, DataRecordType>.Columns.Comment);
			base.Columns.Add("Score", this.Score = Evaluations<DataCollectionType, DataRecordType>.Columns.Score);
			base.Columns.Add("Username", this.Username = Evaluations<DataCollectionType, DataRecordType>.Columns.Username);
			base.Columns.Add("UserID", this.UserID = Evaluations<DataCollectionType, DataRecordType>.Columns.UserID);
			base.Columns.Add("CourseCode", this.CourseCode = Evaluations<DataCollectionType, DataRecordType>.Columns.CourseCode);
			base.Columns.Add("LessonNumber", this.LessonNumber = Evaluations<DataCollectionType, DataRecordType>.Columns.LessonNumber);
			base.Columns.Add("Reviewed", this.Reviewed = Evaluations<DataCollectionType, DataRecordType>.Columns.Reviewed);
			
			//Call the secondary constructor method
			this.EvaluationsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void EvaluationsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> PostedDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> Comment { get; set; }
		public jlib.DataFramework.BaseColumn<int> Score { get; set; }
		public jlib.DataFramework.BaseColumn<string> Username { get; set; }
		public jlib.DataFramework.BaseColumn<int> UserID { get; set; }
		public jlib.DataFramework.BaseColumn<string> CourseCode { get; set; }
		public jlib.DataFramework.BaseColumn<short> LessonNumber { get; set; }
		public jlib.DataFramework.BaseColumn<bool> Reviewed { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_evaluations] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_evaluations] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_evaluations");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_evaluations", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, DateTime? PostedDate = null, string Comment = null, int? Score = null, string Username = null, int? UserID = null, string CourseCode = null, short? LessonNumber = null, bool? Reviewed = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(PostedDate != null)
			{
				query.Parameters.PostedDate = PostedDate;
				filterConditions.Add(@"[posted_date] = @PostedDate");
			}	
			if(Comment != null)
			{
				query.Parameters.Comment = Comment;
				filterConditions.Add(@"[comment] LIKE @Comment");
			}	
			if(Score != null)
			{
				query.Parameters.Score = Score;
				filterConditions.Add(@"[score] = @Score");
			}	
			if(Username != null)
			{
				query.Parameters.Username = Username;
				filterConditions.Add(@"[username] = @Username");
			}	
			if(UserID != null)
			{
				query.Parameters.UserID = UserID;
				filterConditions.Add(@"[user_id] = @UserID");
			}	
			if(CourseCode != null)
			{
				query.Parameters.CourseCode = CourseCode;
				filterConditions.Add(@"[course_code] = @CourseCode");
			}	
			if(LessonNumber != null)
			{
				query.Parameters.LessonNumber = LessonNumber;
				filterConditions.Add(@"[lesson_number] = @LessonNumber");
			}	
			if(Reviewed != null)
			{
				query.Parameters.Reviewed = Reviewed;
				filterConditions.Add(@"[Reviewed] = @Reviewed");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_evaluations] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Evaluations<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Evaluations<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Evaluations<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Evaluations<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_evaluations] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new EvaluationsDataReader(this);
		}
		
		public List<DtoEvaluation> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class EvaluationsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public EvaluationsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoEvaluation
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		PostedDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime PostedDate { get; set; }
		
		/// <summary>
		///		Comment Property
		/// </summary>
		[DataMember]
		public virtual string Comment { get; set; }
		
		/// <summary>
		///		Score Property
		/// </summary>
		[DataMember]
		public virtual int Score { get; set; }
		
		/// <summary>
		///		Username Property
		/// </summary>
		[DataMember]
		public virtual string Username { get; set; }
		
		/// <summary>
		///		UserID Property
		/// </summary>
		[DataMember]
		public virtual int UserID { get; set; }
		
		/// <summary>
		///		CourseCode Property
		/// </summary>
		[DataMember]
		public virtual string CourseCode { get; set; }
		
		/// <summary>
		///		LessonNumber Property
		/// </summary>
		[DataMember]
		public virtual short LessonNumber { get; set; }
		
		/// <summary>
		///		Reviewed Property
		/// </summary>
		[DataMember]
		public virtual bool Reviewed { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class Evaluation : Evaluation<Evaluation>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Evaluation
		/// </summary>
		public Evaluation() : this(new Context()) {}
		
		/// <summary>
		/// Creates Evaluation
		/// </summary>
		public Evaluation(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for Evaluation
		/// </summary>
		protected Evaluation(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class Evaluation<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: Evaluation<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Evaluation
		/// </summary>
		public Evaluation() : this(new Context()) {}
		
		/// <summary>
		/// Creates Evaluation
		/// </summary>
		public Evaluation(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_evaluations";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, Evaluations.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostedDate" ,this._PostedDate = new jlib.DataFramework.BaseField<DateTime>(this, Evaluations.Columns.PostedDate, InitialValue: DateTime.Now, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Comment" ,this._Comment = new jlib.DataFramework.BaseField<string>(this, Evaluations.Columns.Comment, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Score" ,this._Score = new jlib.DataFramework.BaseField<int>(this, Evaluations.Columns.Score, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Username" ,this._Username = new jlib.DataFramework.BaseField<string>(this, Evaluations.Columns.Username, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserID" ,this._UserID = new jlib.DataFramework.BaseField<int>(this, Evaluations.Columns.UserID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CourseCode" ,this._CourseCode = new jlib.DataFramework.BaseField<string>(this, Evaluations.Columns.CourseCode, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("LessonNumber" ,this._LessonNumber = new jlib.DataFramework.BaseField<short>(this, Evaluations.Columns.LessonNumber, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Reviewed" ,this._Reviewed = new jlib.DataFramework.BaseField<bool>(this, Evaluations.Columns.Reviewed, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.EvaluationEx();
		}
		
		/// <summary>
		/// Deserialization method for Evaluation
		/// </summary>
		protected Evaluation(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void EvaluationEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_evaluations", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		PostedDate Property
		///		Database Field:		posted_date;
		///		Database Parameter:	@PostedDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_evaluations", FieldName: "posted_date")]
		public virtual DateTime PostedDate
		{
			get 
			{ 
				return this._PostedDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._PostedDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _PostedDate { get; set; }

			
		/// <summary>
		///		Comment Property
		///		Database Field:		comment;
		///		Database Parameter:	@Comment;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_evaluations", FieldName: "comment", IsNullable=true)]
		public virtual string Comment
		{
			get 
			{ 
				return (string)this._Comment.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Comment.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Comment.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Comment { get; set; }

			
		/// <summary>
		///		Score Property
		///		Database Field:		score;
		///		Database Parameter:	@Score;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_evaluations", FieldName: "score")]
		public virtual int Score
		{
			get 
			{ 
				return this._Score.Value; 
			}

			set 
			{ 
				//Set Value
				this._Score.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Score.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _Score { get; set; }

			
		/// <summary>
		///		Username Property
		///		Database Field:		username;
		///		Database Parameter:	@Username;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_evaluations", FieldName: "username", IsNullable=true)]
		public virtual string Username
		{
			get 
			{ 
				return (string)this._Username.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Username.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Username.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Username { get; set; }

			
		/// <summary>
		///		UserID Property
		///		Database Field:		user_id;
		///		Database Parameter:	@UserID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_evaluations", FieldName: "user_id")]
		public virtual int UserID
		{
			get 
			{ 
				return this._UserID.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _UserID { get; set; }

			
		/// <summary>
		///		CourseCode Property
		///		Database Field:		course_code;
		///		Database Parameter:	@CourseCode;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_evaluations", FieldName: "course_code", IsNullable=true)]
		public virtual string CourseCode
		{
			get 
			{ 
				return (string)this._CourseCode.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._CourseCode.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CourseCode.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _CourseCode { get; set; }

			
		/// <summary>
		///		LessonNumber Property
		///		Database Field:		lesson_number;
		///		Database Parameter:	@LessonNumber;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_evaluations", FieldName: "lesson_number")]
		public virtual short LessonNumber
		{
			get 
			{ 
				return this._LessonNumber.Value; 
			}

			set 
			{ 
				//Set Value
				this._LessonNumber.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._LessonNumber.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<short> _LessonNumber { get; set; }

			
		/// <summary>
		///		Reviewed Property
		///		Database Field:		Reviewed;
		///		Database Parameter:	@Reviewed;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_evaluations", FieldName: "Reviewed")]
		public virtual bool Reviewed
		{
			get 
			{ 
				return this._Reviewed.Value; 
			}

			set 
			{ 
				//Set Value
				this._Reviewed.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Reviewed.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _Reviewed { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoEvaluation Dto)
		{
			//Set Values
			this._PostedDate.SetChangedValueIfDifferent(Dto.PostedDate);
			this._Comment.SetChangedValueIfDifferent(Dto.Comment);
			this._Score.SetChangedValueIfDifferent(Dto.Score);
			this._Username.SetChangedValueIfDifferent(Dto.Username);
			this._UserID.SetChangedValueIfDifferent(Dto.UserID);
			this._CourseCode.SetChangedValueIfDifferent(Dto.CourseCode);
			this._LessonNumber.SetChangedValueIfDifferent(Dto.LessonNumber);
			this._Reviewed.SetChangedValueIfDifferent(Dto.Reviewed);
		}
		public virtual DtoEvaluation ToDto()
		{
			return this.ToDto(new DtoEvaluation());
		}
		
		public virtual DtoEvaluation ToDto(DtoEvaluation dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.PostedDate = this.PostedDate;
			dto.Comment = this.Comment;
			dto.Score = this.Score;
			dto.Username = this.Username;
			dto.UserID = this.UserID;
			dto.CourseCode = this.CourseCode;
			dto.LessonNumber = this.LessonNumber;
			dto.Reviewed = this.Reviewed;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_evaluations] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_evaluations]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class ExamResultImports : ExamResultImports<ExamResultImports, ExamResultImport>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates ExamResultImports
		/// </summary>
		public ExamResultImports() : this(new Context()) {}
		
		/// <summary>
		/// Creates ExamResultImports
		/// </summary>
		public ExamResultImports(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class ExamResultImports<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: ExamResultImport<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> PostedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "posted_date", PropertyName: "PostedDate", ParameterName: "@PostedDate", PkParameterName: "@PK_PostedDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> UserID = new jlib.DataFramework.BaseColumn<int>(FieldName: "user_id", PropertyName: "UserID", ParameterName: "@UserID", PkParameterName: "@PK_UserID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> OfferingID = new jlib.DataFramework.BaseColumn<int>(FieldName: "offering_id", PropertyName: "OfferingID", ParameterName: "@OfferingID", PkParameterName: "@PK_OfferingID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> SourceID = new jlib.DataFramework.BaseColumn<int>(FieldName: "source_id", PropertyName: "SourceID", ParameterName: "@SourceID", PkParameterName: "@PK_SourceID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> ImportName = new jlib.DataFramework.BaseColumn<string>(FieldName: "import_name", PropertyName: "ImportName", ParameterName: "@ImportName", PkParameterName: "@PK_ImportName", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Topic = new jlib.DataFramework.BaseColumn<string>(FieldName: "topic", PropertyName: "Topic", ParameterName: "@Topic", PkParameterName: "@PK_Topic", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> ExamType = new jlib.DataFramework.BaseColumn<string>(FieldName: "exam_type", PropertyName: "ExamType", ParameterName: "@ExamType", PkParameterName: "@PK_ExamType", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> Credits = new jlib.DataFramework.BaseColumn<int>(FieldName: "credits", PropertyName: "Credits", ParameterName: "@Credits", PkParameterName: "@PK_Credits", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> NamePrefix = new jlib.DataFramework.BaseColumn<string>(FieldName: "name_prefix", PropertyName: "NamePrefix", ParameterName: "@NamePrefix", PkParameterName: "@PK_NamePrefix", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> NameSuffix = new jlib.DataFramework.BaseColumn<string>(FieldName: "name_suffix", PropertyName: "NameSuffix", ParameterName: "@NameSuffix", PkParameterName: "@PK_NameSuffix", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> NameFirst = new jlib.DataFramework.BaseColumn<string>(FieldName: "name_first", PropertyName: "NameFirst", ParameterName: "@NameFirst", PkParameterName: "@PK_NameFirst", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> NameLast = new jlib.DataFramework.BaseColumn<string>(FieldName: "name_last", PropertyName: "NameLast", ParameterName: "@NameLast", PkParameterName: "@PK_NameLast", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> DateOfBirth = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "date_of_birth", PropertyName: "DateOfBirth", ParameterName: "@DateOfBirth", PkParameterName: "@PK_DateOfBirth", DBType: DbType.Date, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Address = new jlib.DataFramework.BaseColumn<string>(FieldName: "address", PropertyName: "Address", ParameterName: "@Address", PkParameterName: "@PK_Address", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Zip = new jlib.DataFramework.BaseColumn<string>(FieldName: "zip", PropertyName: "Zip", ParameterName: "@Zip", PkParameterName: "@PK_Zip", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> City = new jlib.DataFramework.BaseColumn<string>(FieldName: "city", PropertyName: "City", ParameterName: "@City", PkParameterName: "@PK_City", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Country = new jlib.DataFramework.BaseColumn<string>(FieldName: "country", PropertyName: "Country", ParameterName: "@Country", PkParameterName: "@PK_Country", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> PhoneNumber = new jlib.DataFramework.BaseColumn<string>(FieldName: "phone_number", PropertyName: "PhoneNumber", ParameterName: "@PhoneNumber", PkParameterName: "@PK_PhoneNumber", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> MobileNumber = new jlib.DataFramework.BaseColumn<string>(FieldName: "mobile_number", PropertyName: "MobileNumber", ParameterName: "@MobileNumber", PkParameterName: "@PK_MobileNumber", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Email = new jlib.DataFramework.BaseColumn<string>(FieldName: "email", PropertyName: "Email", ParameterName: "@Email", PkParameterName: "@PK_Email", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> ModuleCode = new jlib.DataFramework.BaseColumn<string>(FieldName: "module_code", PropertyName: "ModuleCode", ParameterName: "@ModuleCode", PkParameterName: "@PK_ModuleCode", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Grade = new jlib.DataFramework.BaseColumn<string>(FieldName: "grade", PropertyName: "Grade", ParameterName: "@Grade", PkParameterName: "@PK_Grade", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> GradeComment = new jlib.DataFramework.BaseColumn<string>(FieldName: "grade_comment", PropertyName: "GradeComment", ParameterName: "@GradeComment", PkParameterName: "@PK_GradeComment", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> StartDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "start_date", PropertyName: "StartDate", ParameterName: "@StartDate", PkParameterName: "@PK_StartDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> EndDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "end_date", PropertyName: "EndDate", ParameterName: "@EndDate", PkParameterName: "@PK_EndDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Year = new jlib.DataFramework.BaseColumn<string>(FieldName: "year", PropertyName: "Year", ParameterName: "@Year", PkParameterName: "@PK_Year", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Semester = new jlib.DataFramework.BaseColumn<string>(FieldName: "semester", PropertyName: "Semester", ParameterName: "@Semester", PkParameterName: "@PK_Semester", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates ExamResultImports
		/// </summary>
		public ExamResultImports() : this(new Context()) {}
		
		/// <summary>
		/// Creates ExamResultImports
		/// </summary>
		public ExamResultImports(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_exam_result_imports";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = ExamResultImports<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("PostedDate", this.PostedDate = ExamResultImports<DataCollectionType, DataRecordType>.Columns.PostedDate);
			base.Columns.Add("UserID", this.UserID = ExamResultImports<DataCollectionType, DataRecordType>.Columns.UserID);
			base.Columns.Add("OfferingID", this.OfferingID = ExamResultImports<DataCollectionType, DataRecordType>.Columns.OfferingID);
			base.Columns.Add("SourceID", this.SourceID = ExamResultImports<DataCollectionType, DataRecordType>.Columns.SourceID);
			base.Columns.Add("ImportName", this.ImportName = ExamResultImports<DataCollectionType, DataRecordType>.Columns.ImportName);
			base.Columns.Add("Topic", this.Topic = ExamResultImports<DataCollectionType, DataRecordType>.Columns.Topic);
			base.Columns.Add("ExamType", this.ExamType = ExamResultImports<DataCollectionType, DataRecordType>.Columns.ExamType);
			base.Columns.Add("Credits", this.Credits = ExamResultImports<DataCollectionType, DataRecordType>.Columns.Credits);
			base.Columns.Add("NamePrefix", this.NamePrefix = ExamResultImports<DataCollectionType, DataRecordType>.Columns.NamePrefix);
			base.Columns.Add("NameSuffix", this.NameSuffix = ExamResultImports<DataCollectionType, DataRecordType>.Columns.NameSuffix);
			base.Columns.Add("NameFirst", this.NameFirst = ExamResultImports<DataCollectionType, DataRecordType>.Columns.NameFirst);
			base.Columns.Add("NameLast", this.NameLast = ExamResultImports<DataCollectionType, DataRecordType>.Columns.NameLast);
			base.Columns.Add("DateOfBirth", this.DateOfBirth = ExamResultImports<DataCollectionType, DataRecordType>.Columns.DateOfBirth);
			base.Columns.Add("Address", this.Address = ExamResultImports<DataCollectionType, DataRecordType>.Columns.Address);
			base.Columns.Add("Zip", this.Zip = ExamResultImports<DataCollectionType, DataRecordType>.Columns.Zip);
			base.Columns.Add("City", this.City = ExamResultImports<DataCollectionType, DataRecordType>.Columns.City);
			base.Columns.Add("Country", this.Country = ExamResultImports<DataCollectionType, DataRecordType>.Columns.Country);
			base.Columns.Add("PhoneNumber", this.PhoneNumber = ExamResultImports<DataCollectionType, DataRecordType>.Columns.PhoneNumber);
			base.Columns.Add("MobileNumber", this.MobileNumber = ExamResultImports<DataCollectionType, DataRecordType>.Columns.MobileNumber);
			base.Columns.Add("Email", this.Email = ExamResultImports<DataCollectionType, DataRecordType>.Columns.Email);
			base.Columns.Add("ModuleCode", this.ModuleCode = ExamResultImports<DataCollectionType, DataRecordType>.Columns.ModuleCode);
			base.Columns.Add("Grade", this.Grade = ExamResultImports<DataCollectionType, DataRecordType>.Columns.Grade);
			base.Columns.Add("GradeComment", this.GradeComment = ExamResultImports<DataCollectionType, DataRecordType>.Columns.GradeComment);
			base.Columns.Add("StartDate", this.StartDate = ExamResultImports<DataCollectionType, DataRecordType>.Columns.StartDate);
			base.Columns.Add("EndDate", this.EndDate = ExamResultImports<DataCollectionType, DataRecordType>.Columns.EndDate);
			base.Columns.Add("Year", this.Year = ExamResultImports<DataCollectionType, DataRecordType>.Columns.Year);
			base.Columns.Add("Semester", this.Semester = ExamResultImports<DataCollectionType, DataRecordType>.Columns.Semester);
			
			//Call the secondary constructor method
			this.ExamResultImportsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void ExamResultImportsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> PostedDate { get; set; }
		public jlib.DataFramework.BaseColumn<int> UserID { get; set; }
		public jlib.DataFramework.BaseColumn<int> OfferingID { get; set; }
		public jlib.DataFramework.BaseColumn<int> SourceID { get; set; }
		public jlib.DataFramework.BaseColumn<string> ImportName { get; set; }
		public jlib.DataFramework.BaseColumn<string> Topic { get; set; }
		public jlib.DataFramework.BaseColumn<string> ExamType { get; set; }
		public jlib.DataFramework.BaseColumn<int> Credits { get; set; }
		public jlib.DataFramework.BaseColumn<string> NamePrefix { get; set; }
		public jlib.DataFramework.BaseColumn<string> NameSuffix { get; set; }
		public jlib.DataFramework.BaseColumn<string> NameFirst { get; set; }
		public jlib.DataFramework.BaseColumn<string> NameLast { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> DateOfBirth { get; set; }
		public jlib.DataFramework.BaseColumn<string> Address { get; set; }
		public jlib.DataFramework.BaseColumn<string> Zip { get; set; }
		public jlib.DataFramework.BaseColumn<string> City { get; set; }
		public jlib.DataFramework.BaseColumn<string> Country { get; set; }
		public jlib.DataFramework.BaseColumn<string> PhoneNumber { get; set; }
		public jlib.DataFramework.BaseColumn<string> MobileNumber { get; set; }
		public jlib.DataFramework.BaseColumn<string> Email { get; set; }
		public jlib.DataFramework.BaseColumn<string> ModuleCode { get; set; }
		public jlib.DataFramework.BaseColumn<string> Grade { get; set; }
		public jlib.DataFramework.BaseColumn<string> GradeComment { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> StartDate { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> EndDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> Year { get; set; }
		public jlib.DataFramework.BaseColumn<string> Semester { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_exam_result_imports] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_exam_result_imports] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_exam_result_imports");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_exam_result_imports", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, DateTime? PostedDate = null, int? UserID = null, int? OfferingID = null, int? SourceID = null, string ImportName = null, string Topic = null, string ExamType = null, int? Credits = null, string NamePrefix = null, string NameSuffix = null, string NameFirst = null, string NameLast = null, DateTime? DateOfBirth = null, string Address = null, string Zip = null, string City = null, string Country = null, string PhoneNumber = null, string MobileNumber = null, string Email = null, string ModuleCode = null, string Grade = null, string GradeComment = null, DateTime? StartDate = null, DateTime? EndDate = null, string Year = null, string Semester = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(PostedDate != null)
			{
				query.Parameters.PostedDate = PostedDate;
				filterConditions.Add(@"[posted_date] = @PostedDate");
			}	
			if(UserID != null)
			{
				query.Parameters.UserID = UserID;
				filterConditions.Add(@"[user_id] = @UserID");
			}	
			if(OfferingID != null)
			{
				query.Parameters.OfferingID = OfferingID;
				filterConditions.Add(@"[offering_id] = @OfferingID");
			}	
			if(SourceID != null)
			{
				query.Parameters.SourceID = SourceID;
				filterConditions.Add(@"[source_id] = @SourceID");
			}	
			if(ImportName != null)
			{
				query.Parameters.ImportName = ImportName;
				filterConditions.Add(@"[import_name] = @ImportName");
			}	
			if(Topic != null)
			{
				query.Parameters.Topic = Topic;
				filterConditions.Add(@"[topic] = @Topic");
			}	
			if(ExamType != null)
			{
				query.Parameters.ExamType = ExamType;
				filterConditions.Add(@"[exam_type] = @ExamType");
			}	
			if(Credits != null)
			{
				query.Parameters.Credits = Credits;
				filterConditions.Add(@"[credits] = @Credits");
			}	
			if(NamePrefix != null)
			{
				query.Parameters.NamePrefix = NamePrefix;
				filterConditions.Add(@"[name_prefix] = @NamePrefix");
			}	
			if(NameSuffix != null)
			{
				query.Parameters.NameSuffix = NameSuffix;
				filterConditions.Add(@"[name_suffix] = @NameSuffix");
			}	
			if(NameFirst != null)
			{
				query.Parameters.NameFirst = NameFirst;
				filterConditions.Add(@"[name_first] = @NameFirst");
			}	
			if(NameLast != null)
			{
				query.Parameters.NameLast = NameLast;
				filterConditions.Add(@"[name_last] = @NameLast");
			}	
			if(DateOfBirth != null)
			{
				query.Parameters.DateOfBirth = DateOfBirth;
				filterConditions.Add(@"[date_of_birth] = @DateOfBirth");
			}	
			if(Address != null)
			{
				query.Parameters.Address = Address;
				filterConditions.Add(@"[address] = @Address");
			}	
			if(Zip != null)
			{
				query.Parameters.Zip = Zip;
				filterConditions.Add(@"[zip] = @Zip");
			}	
			if(City != null)
			{
				query.Parameters.City = City;
				filterConditions.Add(@"[city] = @City");
			}	
			if(Country != null)
			{
				query.Parameters.Country = Country;
				filterConditions.Add(@"[country] = @Country");
			}	
			if(PhoneNumber != null)
			{
				query.Parameters.PhoneNumber = PhoneNumber;
				filterConditions.Add(@"[phone_number] = @PhoneNumber");
			}	
			if(MobileNumber != null)
			{
				query.Parameters.MobileNumber = MobileNumber;
				filterConditions.Add(@"[mobile_number] = @MobileNumber");
			}	
			if(Email != null)
			{
				query.Parameters.Email = Email;
				filterConditions.Add(@"[email] = @Email");
			}	
			if(ModuleCode != null)
			{
				query.Parameters.ModuleCode = ModuleCode;
				filterConditions.Add(@"[module_code] = @ModuleCode");
			}	
			if(Grade != null)
			{
				query.Parameters.Grade = Grade;
				filterConditions.Add(@"[grade] = @Grade");
			}	
			if(GradeComment != null)
			{
				query.Parameters.GradeComment = GradeComment;
				filterConditions.Add(@"[grade_comment] = @GradeComment");
			}	
			if(StartDate != null)
			{
				query.Parameters.StartDate = StartDate;
				filterConditions.Add(@"[start_date] = @StartDate");
			}	
			if(EndDate != null)
			{
				query.Parameters.EndDate = EndDate;
				filterConditions.Add(@"[end_date] = @EndDate");
			}	
			if(Year != null)
			{
				query.Parameters.Year = Year;
				filterConditions.Add(@"[year] = @Year");
			}	
			if(Semester != null)
			{
				query.Parameters.Semester = Semester;
				filterConditions.Add(@"[semester] = @Semester");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_exam_result_imports] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<ExamResultImports<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new ExamResultImports<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<ExamResultImports<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new ExamResultImports<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_exam_result_imports] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new ExamResultImportsDataReader(this);
		}
		
		public List<DtoExamResultImport> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class ExamResultImportsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public ExamResultImportsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoExamResultImport
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		PostedDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime PostedDate { get; set; }
		
		/// <summary>
		///		UserID Property
		/// </summary>
		[DataMember]
		public virtual int UserID { get; set; }
		
		/// <summary>
		///		OfferingID Property
		/// </summary>
		[DataMember]
		public virtual int OfferingID { get; set; }
		
		/// <summary>
		///		SourceID Property
		/// </summary>
		[DataMember]
		public virtual int SourceID { get; set; }
		
		/// <summary>
		///		ImportName Property
		/// </summary>
		[DataMember]
		public virtual string ImportName { get; set; }
		
		/// <summary>
		///		Topic Property
		/// </summary>
		[DataMember]
		public virtual string Topic { get; set; }
		
		/// <summary>
		///		ExamType Property
		/// </summary>
		[DataMember]
		public virtual string ExamType { get; set; }
		
		/// <summary>
		///		Credits Property
		/// </summary>
		[DataMember]
		public virtual int? Credits { get; set; }
		
		/// <summary>
		///		NamePrefix Property
		/// </summary>
		[DataMember]
		public virtual string NamePrefix { get; set; }
		
		/// <summary>
		///		NameSuffix Property
		/// </summary>
		[DataMember]
		public virtual string NameSuffix { get; set; }
		
		/// <summary>
		///		NameFirst Property
		/// </summary>
		[DataMember]
		public virtual string NameFirst { get; set; }
		
		/// <summary>
		///		NameLast Property
		/// </summary>
		[DataMember]
		public virtual string NameLast { get; set; }
		
		/// <summary>
		///		DateOfBirth Property
		/// </summary>
		[DataMember]
		public virtual DateTime? DateOfBirth { get; set; }
		
		/// <summary>
		///		Address Property
		/// </summary>
		[DataMember]
		public virtual string Address { get; set; }
		
		/// <summary>
		///		Zip Property
		/// </summary>
		[DataMember]
		public virtual string Zip { get; set; }
		
		/// <summary>
		///		City Property
		/// </summary>
		[DataMember]
		public virtual string City { get; set; }
		
		/// <summary>
		///		Country Property
		/// </summary>
		[DataMember]
		public virtual string Country { get; set; }
		
		/// <summary>
		///		PhoneNumber Property
		/// </summary>
		[DataMember]
		public virtual string PhoneNumber { get; set; }
		
		/// <summary>
		///		MobileNumber Property
		/// </summary>
		[DataMember]
		public virtual string MobileNumber { get; set; }
		
		/// <summary>
		///		Email Property
		/// </summary>
		[DataMember]
		public virtual string Email { get; set; }
		
		/// <summary>
		///		ModuleCode Property
		/// </summary>
		[DataMember]
		public virtual string ModuleCode { get; set; }
		
		/// <summary>
		///		Grade Property
		/// </summary>
		[DataMember]
		public virtual string Grade { get; set; }
		
		/// <summary>
		///		GradeComment Property
		/// </summary>
		[DataMember]
		public virtual string GradeComment { get; set; }
		
		/// <summary>
		///		StartDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime? StartDate { get; set; }
		
		/// <summary>
		///		EndDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime? EndDate { get; set; }
		
		/// <summary>
		///		Year Property
		/// </summary>
		[DataMember]
		public virtual string Year { get; set; }
		
		/// <summary>
		///		Semester Property
		/// </summary>
		[DataMember]
		public virtual string Semester { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class ExamResultImport : ExamResultImport<ExamResultImport>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates ExamResultImport
		/// </summary>
		public ExamResultImport() : this(new Context()) {}
		
		/// <summary>
		/// Creates ExamResultImport
		/// </summary>
		public ExamResultImport(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for ExamResultImport
		/// </summary>
		protected ExamResultImport(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class ExamResultImport<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: ExamResultImport<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates ExamResultImport
		/// </summary>
		public ExamResultImport() : this(new Context()) {}
		
		/// <summary>
		/// Creates ExamResultImport
		/// </summary>
		public ExamResultImport(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_exam_result_imports";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, ExamResultImports.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostedDate" ,this._PostedDate = new jlib.DataFramework.BaseField<DateTime>(this, ExamResultImports.Columns.PostedDate, InitialValue: DateTime.Now, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserID" ,this._UserID = new jlib.DataFramework.BaseField<int>(this, ExamResultImports.Columns.UserID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("OfferingID" ,this._OfferingID = new jlib.DataFramework.BaseField<int>(this, ExamResultImports.Columns.OfferingID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("SourceID" ,this._SourceID = new jlib.DataFramework.BaseField<int>(this, ExamResultImports.Columns.SourceID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ImportName" ,this._ImportName = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.ImportName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Topic" ,this._Topic = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.Topic, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ExamType" ,this._ExamType = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.ExamType, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Credits" ,this._Credits = new jlib.DataFramework.BaseField<int>(this, ExamResultImports.Columns.Credits, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("NamePrefix" ,this._NamePrefix = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.NamePrefix, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("NameSuffix" ,this._NameSuffix = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.NameSuffix, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("NameFirst" ,this._NameFirst = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.NameFirst, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("NameLast" ,this._NameLast = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.NameLast, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("DateOfBirth" ,this._DateOfBirth = new jlib.DataFramework.BaseField<DateTime>(this, ExamResultImports.Columns.DateOfBirth, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Address" ,this._Address = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.Address, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Zip" ,this._Zip = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.Zip, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("City" ,this._City = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.City, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Country" ,this._Country = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.Country, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PhoneNumber" ,this._PhoneNumber = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.PhoneNumber, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("MobileNumber" ,this._MobileNumber = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.MobileNumber, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Email" ,this._Email = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.Email, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ModuleCode" ,this._ModuleCode = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.ModuleCode, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Grade" ,this._Grade = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.Grade, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("GradeComment" ,this._GradeComment = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.GradeComment, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("StartDate" ,this._StartDate = new jlib.DataFramework.BaseField<DateTime>(this, ExamResultImports.Columns.StartDate, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("EndDate" ,this._EndDate = new jlib.DataFramework.BaseField<DateTime>(this, ExamResultImports.Columns.EndDate, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Year" ,this._Year = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.Year, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Semester" ,this._Semester = new jlib.DataFramework.BaseField<string>(this, ExamResultImports.Columns.Semester, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.ExamResultImportEx();
		}
		
		/// <summary>
		/// Deserialization method for ExamResultImport
		/// </summary>
		protected ExamResultImport(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void ExamResultImportEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		PostedDate Property
		///		Database Field:		posted_date;
		///		Database Parameter:	@PostedDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "posted_date")]
		public virtual DateTime PostedDate
		{
			get 
			{ 
				return this._PostedDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._PostedDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _PostedDate { get; set; }

			
		/// <summary>
		///		UserID Property
		///		Database Field:		user_id;
		///		Database Parameter:	@UserID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "user_id")]
		public virtual int UserID
		{
			get 
			{ 
				return this._UserID.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _UserID { get; set; }

			
		/// <summary>
		///		OfferingID Property
		///		Database Field:		offering_id;
		///		Database Parameter:	@OfferingID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "offering_id")]
		public virtual int OfferingID
		{
			get 
			{ 
				return this._OfferingID.Value; 
			}

			set 
			{ 
				//Set Value
				this._OfferingID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._OfferingID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _OfferingID { get; set; }

			
		/// <summary>
		///		SourceID Property
		///		Database Field:		source_id;
		///		Database Parameter:	@SourceID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "source_id")]
		public virtual int SourceID
		{
			get 
			{ 
				return this._SourceID.Value; 
			}

			set 
			{ 
				//Set Value
				this._SourceID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._SourceID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _SourceID { get; set; }

			
		/// <summary>
		///		ImportName Property
		///		Database Field:		import_name;
		///		Database Parameter:	@ImportName;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "import_name", IsNullable=true)]
		public virtual string ImportName
		{
			get 
			{ 
				return (string)this._ImportName.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._ImportName.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ImportName.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _ImportName { get; set; }

			
		/// <summary>
		///		Topic Property
		///		Database Field:		topic;
		///		Database Parameter:	@Topic;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "topic", IsNullable=true)]
		public virtual string Topic
		{
			get 
			{ 
				return (string)this._Topic.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Topic.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Topic.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Topic { get; set; }

			
		/// <summary>
		///		ExamType Property
		///		Database Field:		exam_type;
		///		Database Parameter:	@ExamType;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "exam_type", IsNullable=true)]
		public virtual string ExamType
		{
			get 
			{ 
				return (string)this._ExamType.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._ExamType.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ExamType.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _ExamType { get; set; }

			
		/// <summary>
		///		Credits Property
		///		Database Field:		credits;
		///		Database Parameter:	@Credits;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "credits", IsNullable=true)]
		public virtual int? Credits
		{
			get 
			{ 
				return (int?)this._Credits.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Credits.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Credits.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _Credits { get; set; }

			
		/// <summary>
		///		NamePrefix Property
		///		Database Field:		name_prefix;
		///		Database Parameter:	@NamePrefix;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "name_prefix", IsNullable=true)]
		public virtual string NamePrefix
		{
			get 
			{ 
				return (string)this._NamePrefix.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._NamePrefix.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._NamePrefix.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _NamePrefix { get; set; }

			
		/// <summary>
		///		NameSuffix Property
		///		Database Field:		name_suffix;
		///		Database Parameter:	@NameSuffix;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "name_suffix", IsNullable=true)]
		public virtual string NameSuffix
		{
			get 
			{ 
				return (string)this._NameSuffix.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._NameSuffix.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._NameSuffix.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _NameSuffix { get; set; }

			
		/// <summary>
		///		NameFirst Property
		///		Database Field:		name_first;
		///		Database Parameter:	@NameFirst;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "name_first", IsNullable=true)]
		public virtual string NameFirst
		{
			get 
			{ 
				return (string)this._NameFirst.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._NameFirst.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._NameFirst.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _NameFirst { get; set; }

			
		/// <summary>
		///		NameLast Property
		///		Database Field:		name_last;
		///		Database Parameter:	@NameLast;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "name_last", IsNullable=true)]
		public virtual string NameLast
		{
			get 
			{ 
				return (string)this._NameLast.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._NameLast.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._NameLast.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _NameLast { get; set; }

			
		/// <summary>
		///		DateOfBirth Property
		///		Database Field:		date_of_birth;
		///		Database Parameter:	@DateOfBirth;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "date_of_birth", IsNullable=true)]
		public virtual DateTime? DateOfBirth
		{
			get 
			{ 
				return (DateTime?)this._DateOfBirth.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._DateOfBirth.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._DateOfBirth.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _DateOfBirth { get; set; }

			
		/// <summary>
		///		Address Property
		///		Database Field:		address;
		///		Database Parameter:	@Address;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "address", IsNullable=true)]
		public virtual string Address
		{
			get 
			{ 
				return (string)this._Address.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Address.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Address.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Address { get; set; }

			
		/// <summary>
		///		Zip Property
		///		Database Field:		zip;
		///		Database Parameter:	@Zip;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "zip", IsNullable=true)]
		public virtual string Zip
		{
			get 
			{ 
				return (string)this._Zip.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Zip.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Zip.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Zip { get; set; }

			
		/// <summary>
		///		City Property
		///		Database Field:		city;
		///		Database Parameter:	@City;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "city", IsNullable=true)]
		public virtual string City
		{
			get 
			{ 
				return (string)this._City.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._City.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._City.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _City { get; set; }

			
		/// <summary>
		///		Country Property
		///		Database Field:		country;
		///		Database Parameter:	@Country;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "country", IsNullable=true)]
		public virtual string Country
		{
			get 
			{ 
				return (string)this._Country.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Country.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Country.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Country { get; set; }

			
		/// <summary>
		///		PhoneNumber Property
		///		Database Field:		phone_number;
		///		Database Parameter:	@PhoneNumber;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "phone_number", IsNullable=true)]
		public virtual string PhoneNumber
		{
			get 
			{ 
				return (string)this._PhoneNumber.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._PhoneNumber.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PhoneNumber.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _PhoneNumber { get; set; }

			
		/// <summary>
		///		MobileNumber Property
		///		Database Field:		mobile_number;
		///		Database Parameter:	@MobileNumber;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "mobile_number", IsNullable=true)]
		public virtual string MobileNumber
		{
			get 
			{ 
				return (string)this._MobileNumber.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._MobileNumber.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._MobileNumber.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _MobileNumber { get; set; }

			
		/// <summary>
		///		Email Property
		///		Database Field:		email;
		///		Database Parameter:	@Email;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "email", IsNullable=true)]
		public virtual string Email
		{
			get 
			{ 
				return (string)this._Email.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Email.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Email.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Email { get; set; }

			
		/// <summary>
		///		ModuleCode Property
		///		Database Field:		module_code;
		///		Database Parameter:	@ModuleCode;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "module_code", IsNullable=true)]
		public virtual string ModuleCode
		{
			get 
			{ 
				return (string)this._ModuleCode.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._ModuleCode.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ModuleCode.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _ModuleCode { get; set; }

			
		/// <summary>
		///		Grade Property
		///		Database Field:		grade;
		///		Database Parameter:	@Grade;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "grade", IsNullable=true)]
		public virtual string Grade
		{
			get 
			{ 
				return (string)this._Grade.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Grade.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Grade.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Grade { get; set; }

			
		/// <summary>
		///		GradeComment Property
		///		Database Field:		grade_comment;
		///		Database Parameter:	@GradeComment;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "grade_comment", IsNullable=true)]
		public virtual string GradeComment
		{
			get 
			{ 
				return (string)this._GradeComment.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._GradeComment.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._GradeComment.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _GradeComment { get; set; }

			
		/// <summary>
		///		StartDate Property
		///		Database Field:		start_date;
		///		Database Parameter:	@StartDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "start_date", IsNullable=true)]
		public virtual DateTime? StartDate
		{
			get 
			{ 
				return (DateTime?)this._StartDate.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._StartDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._StartDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _StartDate { get; set; }

			
		/// <summary>
		///		EndDate Property
		///		Database Field:		end_date;
		///		Database Parameter:	@EndDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "end_date", IsNullable=true)]
		public virtual DateTime? EndDate
		{
			get 
			{ 
				return (DateTime?)this._EndDate.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._EndDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._EndDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _EndDate { get; set; }

			
		/// <summary>
		///		Year Property
		///		Database Field:		year;
		///		Database Parameter:	@Year;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "year", IsNullable=true)]
		public virtual string Year
		{
			get 
			{ 
				return (string)this._Year.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Year.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Year.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Year { get; set; }

			
		/// <summary>
		///		Semester Property
		///		Database Field:		semester;
		///		Database Parameter:	@Semester;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_result_imports", FieldName: "semester", IsNullable=true)]
		public virtual string Semester
		{
			get 
			{ 
				return (string)this._Semester.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Semester.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Semester.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Semester { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoExamResultImport Dto)
		{
			//Set Values
			this._PostedDate.SetChangedValueIfDifferent(Dto.PostedDate);
			this._UserID.SetChangedValueIfDifferent(Dto.UserID);
			this._OfferingID.SetChangedValueIfDifferent(Dto.OfferingID);
			this._SourceID.SetChangedValueIfDifferent(Dto.SourceID);
			this._ImportName.SetChangedValueIfDifferent(Dto.ImportName);
			this._Topic.SetChangedValueIfDifferent(Dto.Topic);
			this._ExamType.SetChangedValueIfDifferent(Dto.ExamType);
			this._Credits.SetChangedValueIfDifferent(Dto.Credits);
			this._NamePrefix.SetChangedValueIfDifferent(Dto.NamePrefix);
			this._NameSuffix.SetChangedValueIfDifferent(Dto.NameSuffix);
			this._NameFirst.SetChangedValueIfDifferent(Dto.NameFirst);
			this._NameLast.SetChangedValueIfDifferent(Dto.NameLast);
			this._DateOfBirth.SetChangedValueIfDifferent(Dto.DateOfBirth);
			this._Address.SetChangedValueIfDifferent(Dto.Address);
			this._Zip.SetChangedValueIfDifferent(Dto.Zip);
			this._City.SetChangedValueIfDifferent(Dto.City);
			this._Country.SetChangedValueIfDifferent(Dto.Country);
			this._PhoneNumber.SetChangedValueIfDifferent(Dto.PhoneNumber);
			this._MobileNumber.SetChangedValueIfDifferent(Dto.MobileNumber);
			this._Email.SetChangedValueIfDifferent(Dto.Email);
			this._ModuleCode.SetChangedValueIfDifferent(Dto.ModuleCode);
			this._Grade.SetChangedValueIfDifferent(Dto.Grade);
			this._GradeComment.SetChangedValueIfDifferent(Dto.GradeComment);
			this._StartDate.SetChangedValueIfDifferent(Dto.StartDate);
			this._EndDate.SetChangedValueIfDifferent(Dto.EndDate);
			this._Year.SetChangedValueIfDifferent(Dto.Year);
			this._Semester.SetChangedValueIfDifferent(Dto.Semester);
		}
		public virtual DtoExamResultImport ToDto()
		{
			return this.ToDto(new DtoExamResultImport());
		}
		
		public virtual DtoExamResultImport ToDto(DtoExamResultImport dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.PostedDate = this.PostedDate;
			dto.UserID = this.UserID;
			dto.OfferingID = this.OfferingID;
			dto.SourceID = this.SourceID;
			dto.ImportName = this.ImportName;
			dto.Topic = this.Topic;
			dto.ExamType = this.ExamType;
			dto.Credits = this.Credits;
			dto.NamePrefix = this.NamePrefix;
			dto.NameSuffix = this.NameSuffix;
			dto.NameFirst = this.NameFirst;
			dto.NameLast = this.NameLast;
			dto.DateOfBirth = this.DateOfBirth;
			dto.Address = this.Address;
			dto.Zip = this.Zip;
			dto.City = this.City;
			dto.Country = this.Country;
			dto.PhoneNumber = this.PhoneNumber;
			dto.MobileNumber = this.MobileNumber;
			dto.Email = this.Email;
			dto.ModuleCode = this.ModuleCode;
			dto.Grade = this.Grade;
			dto.GradeComment = this.GradeComment;
			dto.StartDate = this.StartDate;
			dto.EndDate = this.EndDate;
			dto.Year = this.Year;
			dto.Semester = this.Semester;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_exam_result_imports] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_exam_result_imports]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class ExamSections : ExamSections<ExamSections, ExamSection>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates ExamSections
		/// </summary>
		public ExamSections() : this(new Context()) {}
		
		/// <summary>
		/// Creates ExamSections
		/// </summary>
		public ExamSections(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class ExamSections<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: ExamSection<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ObjectID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ObjectID", PropertyName: "ObjectID", ParameterName: "@Objectid", PkParameterName: "@PK_Objectid", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CreateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CreateDate", PropertyName: "CreateDate", ParameterName: "@Createdate", PkParameterName: "@PK_Createdate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> DeleteDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "DeleteDate", PropertyName: "DeleteDate", ParameterName: "@Deletedate", PkParameterName: "@PK_Deletedate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Name = new jlib.DataFramework.BaseColumn<string>(FieldName: "Name", PropertyName: "Name", ParameterName: "@Name", PkParameterName: "@PK_Name", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> ShortName = new jlib.DataFramework.BaseColumn<string>(FieldName: "ShortName", PropertyName: "ShortName", ParameterName: "@Shortname", PkParameterName: "@PK_Shortname", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Text = new jlib.DataFramework.BaseColumn<string>(FieldName: "Text", PropertyName: "Text", ParameterName: "@Text", PkParameterName: "@PK_Text", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates ExamSections
		/// </summary>
		public ExamSections() : this(new Context()) {}
		
		/// <summary>
		/// Creates ExamSections
		/// </summary>
		public ExamSections(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_exam_sections";
			
			//Set Columns
			base.Columns.Add("ObjectID", this.ObjectID = ExamSections<DataCollectionType, DataRecordType>.Columns.ObjectID);
			base.Columns.Add("CreateDate", this.CreateDate = ExamSections<DataCollectionType, DataRecordType>.Columns.CreateDate);
			base.Columns.Add("DeleteDate", this.DeleteDate = ExamSections<DataCollectionType, DataRecordType>.Columns.DeleteDate);
			base.Columns.Add("Name", this.Name = ExamSections<DataCollectionType, DataRecordType>.Columns.Name);
			base.Columns.Add("ShortName", this.ShortName = ExamSections<DataCollectionType, DataRecordType>.Columns.ShortName);
			base.Columns.Add("Text", this.Text = ExamSections<DataCollectionType, DataRecordType>.Columns.Text);
			
			//Call the secondary constructor method
			this.ExamSectionsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void ExamSectionsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ObjectID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CreateDate { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> DeleteDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> Name { get; set; }
		public jlib.DataFramework.BaseColumn<string> ShortName { get; set; }
		public jlib.DataFramework.BaseColumn<string> Text { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_exam_sections] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_exam_sections] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_exam_sections");
		}
		
		
		public static QueryPackage GetByField(int? ObjectID = null, DateTime? CreateDate = null, DateTime? DeleteDate = null, string Name = null, string ShortName = null, string Text = null, DateTime? ObjectDate = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ObjectID != null)
			{
				query.Parameters.ObjectID = ObjectID;
				filterConditions.Add(@"[ObjectID] = @Objectid");
			}	
			if(CreateDate != null)
			{
				query.Parameters.CreateDate = CreateDate;
				filterConditions.Add(@"[CreateDate] = @Createdate");
			}	
			if(DeleteDate != null)
			{
				query.Parameters.DeleteDate = DeleteDate;
				filterConditions.Add(@"[DeleteDate] = @Deletedate");
			}	
			if(Name != null)
			{
				query.Parameters.Name = Name;
				filterConditions.Add(@"[Name] = @Name");
			}	
			if(ShortName != null)
			{
				query.Parameters.ShortName = ShortName;
				filterConditions.Add(@"[ShortName] = @Shortname");
			}	
			if(Text != null)
			{
				query.Parameters.Text = Text;
				filterConditions.Add(@"[Text] LIKE @Text");
			}	
			if(ObjectDate != null || (CreateDate==null && DeleteDate==null))
			{
				Context oContext=new Context();
				query.Parameters.ObjectDate = (ObjectDate==null ? oContext.VersionDate : ObjectDate);
				filterConditions.Add(@"@ObjectDate BETWEEN [CreateDate] AND [DeleteDate]");
			}
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_exam_sections] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<ExamSections<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new ExamSections<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<ExamSections<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new ExamSections<DataCollectionType, DataRecordType>()));
		}
		*/

		
		public override IDataReader GetDataReader()
		{
			return new ExamSectionsDataReader(this);
		}
		
		public List<DtoExamSection> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class ExamSectionsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public ExamSectionsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoExamSection
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		/// </summary>
		[DataMember]
		public virtual int ObjectID { get; set; }
		
		/// <summary>
		///		CreateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime CreateDate { get; set; }
		
		/// <summary>
		///		DeleteDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime DeleteDate { get; set; }
		
		/// <summary>
		///		Name Property
		/// </summary>
		[DataMember]
		public virtual string Name { get; set; }
		
		/// <summary>
		///		ShortName Property
		/// </summary>
		[DataMember]
		public virtual string ShortName { get; set; }
		
		/// <summary>
		///		Text Property
		/// </summary>
		[DataMember]
		public virtual string Text { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class ExamSection : ExamSection<ExamSection>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates ExamSection
		/// </summary>
		public ExamSection() : this(new Context()) {}
		
		/// <summary>
		/// Creates ExamSection
		/// </summary>
		public ExamSection(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for ExamSection
		/// </summary>
		protected ExamSection(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class ExamSection<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: ExamSection<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates ExamSection
		/// </summary>
		public ExamSection() : this(new Context()) {}
		
		/// <summary>
		/// Creates ExamSection
		/// </summary>
		public ExamSection(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_exam_sections";

			//Declare Fields
			this.Fields.Add("ObjectID" ,this._ObjectID = new jlib.DataFramework.BaseField<int>(this, ExamSections.Columns.ObjectID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreateDate" ,this._CreateDate = new jlib.DataFramework.BaseField<DateTime>(this, ExamSections.Columns.CreateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("DeleteDate" ,this._DeleteDate = new jlib.DataFramework.BaseField<DateTime>(this, ExamSections.Columns.DeleteDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Name" ,this._Name = new jlib.DataFramework.BaseField<string>(this, ExamSections.Columns.Name, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ShortName" ,this._ShortName = new jlib.DataFramework.BaseField<string>(this, ExamSections.Columns.ShortName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Text" ,this._Text = new jlib.DataFramework.BaseField<string>(this, ExamSections.Columns.Text, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.ExamSectionEx();
		}
		
		/// <summary>
		/// Deserialization method for ExamSection
		/// </summary>
		protected ExamSection(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void ExamSectionEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		///		Database Field:		ObjectID;
		///		Database Parameter:	@Objectid;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_sections", FieldName: "ObjectID", IsPrimaryKey=true)]
		public virtual int ObjectID
		{
			get 
			{ 
				return this._ObjectID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ObjectID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ObjectID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ObjectID { get; set; }

			
		/// <summary>
		///		CreateDate Property
		///		Database Field:		CreateDate;
		///		Database Parameter:	@Createdate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_sections", FieldName: "CreateDate", IsPrimaryKey=true)]
		public virtual DateTime CreateDate
		{
			get 
			{ 
				return this._CreateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CreateDate { get; set; }

			
		/// <summary>
		///		DeleteDate Property
		///		Database Field:		DeleteDate;
		///		Database Parameter:	@Deletedate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_sections", FieldName: "DeleteDate", IsPrimaryKey=true)]
		public virtual DateTime DeleteDate
		{
			get 
			{ 
				return this._DeleteDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._DeleteDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._DeleteDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _DeleteDate { get; set; }

			
		/// <summary>
		///		Name Property
		///		Database Field:		Name;
		///		Database Parameter:	@Name;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_sections", FieldName: "Name", IsNullable=true)]
		public virtual string Name
		{
			get 
			{ 
				return (string)this._Name.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Name.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Name.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Name { get; set; }

			
		/// <summary>
		///		ShortName Property
		///		Database Field:		ShortName;
		///		Database Parameter:	@Shortname;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_sections", FieldName: "ShortName", IsNullable=true)]
		public virtual string ShortName
		{
			get 
			{ 
				return (string)this._ShortName.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._ShortName.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ShortName.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _ShortName { get; set; }

			
		/// <summary>
		///		Text Property
		///		Database Field:		Text;
		///		Database Parameter:	@Text;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exam_sections", FieldName: "Text", IsNullable=true)]
		public virtual string Text
		{
			get 
			{ 
				return (string)this._Text.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Text.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Text.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Text { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoExamSection Dto)
		{
			//Set Values
			this._ObjectID.SetChangedValueIfDifferent(Dto.ObjectID);
			this._CreateDate.SetChangedValueIfDifferent(Dto.CreateDate);
			this._DeleteDate.SetChangedValueIfDifferent(Dto.DeleteDate);
			this._Name.SetChangedValueIfDifferent(Dto.Name);
			this._ShortName.SetChangedValueIfDifferent(Dto.ShortName);
			this._Text.SetChangedValueIfDifferent(Dto.Text);
		}
		public virtual DtoExamSection ToDto()
		{
			return this.ToDto(new DtoExamSection());
		}
		
		public virtual DtoExamSection ToDto(DtoExamSection dto)
		{
			//Set Values
			dto.ObjectID = this.ObjectID;
			dto.CreateDate = this.CreateDate;
			dto.DeleteDate = this.DeleteDate;
			dto.Name = this.Name;
			dto.ShortName = this.ShortName;
			dto.Text = this.Text;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****

				public static DataRecordType LoadByPk(int ObjectID)
				{
					Context oContext=new Context();
					//Create Sql Connection
					SqlHelper sqlHelper = oContext.GetSqlHelper();
			
					//Load
					DataRecordType item = LoadByPk(sqlHelper, ObjectID, oContext.VersionDate);
			
					//Return
					return item;
				}

		public static DataRecordType LoadByPk(int ObjectID,  DateTime ObjectDate)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ObjectID, ObjectDate);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ObjectID,  DateTime ObjectDate)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_exam_sections]  WITH (NOLOCK)
										WHERE
											
											[ObjectID] = @PK_Objectid		AND @ObjectDate BETWEEN [CreateDate] AND [DeleteDate]

											",
											"@PK_Objectid", ObjectID, "@ObjectDate", ObjectDate
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class Exams : Exams<Exams, Exam>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Exams
		/// </summary>
		public Exams() : this(new Context()) {}
		
		/// <summary>
		/// Creates Exams
		/// </summary>
		public Exams(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Exams<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: Exam<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ObjectID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ObjectId", PropertyName: "ObjectID", ParameterName: "@Objectid", PkParameterName: "@PK_Objectid", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CreateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CreateDate", PropertyName: "CreateDate", ParameterName: "@Createdate", PkParameterName: "@PK_Createdate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> DeleteDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "DeleteDate", PropertyName: "DeleteDate", ParameterName: "@Deletedate", PkParameterName: "@PK_Deletedate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Name = new jlib.DataFramework.BaseColumn<string>(FieldName: "Name", PropertyName: "Name", ParameterName: "@Name", PkParameterName: "@PK_Name", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> IntroText = new jlib.DataFramework.BaseColumn<string>(FieldName: "IntroText", PropertyName: "IntroText", ParameterName: "@Introtext", PkParameterName: "@PK_Introtext", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> ExamText = new jlib.DataFramework.BaseColumn<string>(FieldName: "ExamText", PropertyName: "ExamText", ParameterName: "@Examtext", PkParameterName: "@PK_Examtext", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Language = new jlib.DataFramework.BaseColumn<string>(FieldName: "Language", PropertyName: "Language", ParameterName: "@Language", PkParameterName: "@PK_Language", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> PlayerType = new jlib.DataFramework.BaseColumn<string>(FieldName: "PlayerType", PropertyName: "PlayerType", ParameterName: "@Playertype", PkParameterName: "@PK_Playertype", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> MaxTime = new jlib.DataFramework.BaseColumn<int>(FieldName: "MaxTime", PropertyName: "MaxTime", ParameterName: "@Maxtime", PkParameterName: "@PK_Maxtime", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> Pausable = new jlib.DataFramework.BaseColumn<bool>(FieldName: "Pausable", PropertyName: "Pausable", ParameterName: "@Pausable", PkParameterName: "@PK_Pausable", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> Resumable = new jlib.DataFramework.BaseColumn<bool>(FieldName: "Resumable", PropertyName: "Resumable", ParameterName: "@Resumable", PkParameterName: "@PK_Resumable", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> ReviewAnswers = new jlib.DataFramework.BaseColumn<bool>(FieldName: "ReviewAnswers", PropertyName: "ReviewAnswers", ParameterName: "@Reviewanswers", PkParameterName: "@PK_Reviewanswers", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> ExamID = new jlib.DataFramework.BaseColumn<string>(FieldName: "ExamID", PropertyName: "ExamID", ParameterName: "@Examid", PkParameterName: "@PK_Examid", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Exams
		/// </summary>
		public Exams() : this(new Context()) {}
		
		/// <summary>
		/// Creates Exams
		/// </summary>
		public Exams(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_exams";
			
			//Set Columns
			base.Columns.Add("ObjectID", this.ObjectID = Exams<DataCollectionType, DataRecordType>.Columns.ObjectID);
			base.Columns.Add("CreateDate", this.CreateDate = Exams<DataCollectionType, DataRecordType>.Columns.CreateDate);
			base.Columns.Add("DeleteDate", this.DeleteDate = Exams<DataCollectionType, DataRecordType>.Columns.DeleteDate);
			base.Columns.Add("Name", this.Name = Exams<DataCollectionType, DataRecordType>.Columns.Name);
			base.Columns.Add("IntroText", this.IntroText = Exams<DataCollectionType, DataRecordType>.Columns.IntroText);
			base.Columns.Add("ExamText", this.ExamText = Exams<DataCollectionType, DataRecordType>.Columns.ExamText);
			base.Columns.Add("Language", this.Language = Exams<DataCollectionType, DataRecordType>.Columns.Language);
			base.Columns.Add("PlayerType", this.PlayerType = Exams<DataCollectionType, DataRecordType>.Columns.PlayerType);
			base.Columns.Add("MaxTime", this.MaxTime = Exams<DataCollectionType, DataRecordType>.Columns.MaxTime);
			base.Columns.Add("Pausable", this.Pausable = Exams<DataCollectionType, DataRecordType>.Columns.Pausable);
			base.Columns.Add("Resumable", this.Resumable = Exams<DataCollectionType, DataRecordType>.Columns.Resumable);
			base.Columns.Add("ReviewAnswers", this.ReviewAnswers = Exams<DataCollectionType, DataRecordType>.Columns.ReviewAnswers);
			base.Columns.Add("ExamID", this.ExamID = Exams<DataCollectionType, DataRecordType>.Columns.ExamID);
			
			//Call the secondary constructor method
			this.ExamsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void ExamsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ObjectID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CreateDate { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> DeleteDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> Name { get; set; }
		public jlib.DataFramework.BaseColumn<string> IntroText { get; set; }
		public jlib.DataFramework.BaseColumn<string> ExamText { get; set; }
		public jlib.DataFramework.BaseColumn<string> Language { get; set; }
		public jlib.DataFramework.BaseColumn<string> PlayerType { get; set; }
		public jlib.DataFramework.BaseColumn<int> MaxTime { get; set; }
		public jlib.DataFramework.BaseColumn<bool> Pausable { get; set; }
		public jlib.DataFramework.BaseColumn<bool> Resumable { get; set; }
		public jlib.DataFramework.BaseColumn<bool> ReviewAnswers { get; set; }
		public jlib.DataFramework.BaseColumn<string> ExamID { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_exams] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_exams] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_exams");
		}
		
		
		public static QueryPackage GetByField(int? ObjectID = null, DateTime? CreateDate = null, DateTime? DeleteDate = null, string Name = null, string IntroText = null, string ExamText = null, string Language = null, string PlayerType = null, int? MaxTime = null, bool? Pausable = null, bool? Resumable = null, bool? ReviewAnswers = null, string ExamID = null, DateTime? ObjectDate = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ObjectID != null)
			{
				query.Parameters.ObjectID = ObjectID;
				filterConditions.Add(@"[ObjectId] = @Objectid");
			}	
			if(CreateDate != null)
			{
				query.Parameters.CreateDate = CreateDate;
				filterConditions.Add(@"[CreateDate] = @Createdate");
			}	
			if(DeleteDate != null)
			{
				query.Parameters.DeleteDate = DeleteDate;
				filterConditions.Add(@"[DeleteDate] = @Deletedate");
			}	
			if(Name != null)
			{
				query.Parameters.Name = Name;
				filterConditions.Add(@"[Name] = @Name");
			}	
			if(IntroText != null)
			{
				query.Parameters.IntroText = IntroText;
				filterConditions.Add(@"[IntroText] LIKE @Introtext");
			}	
			if(ExamText != null)
			{
				query.Parameters.ExamText = ExamText;
				filterConditions.Add(@"[ExamText] LIKE @Examtext");
			}	
			if(Language != null)
			{
				query.Parameters.Language = Language;
				filterConditions.Add(@"[Language] = @Language");
			}	
			if(PlayerType != null)
			{
				query.Parameters.PlayerType = PlayerType;
				filterConditions.Add(@"[PlayerType] = @Playertype");
			}	
			if(MaxTime != null)
			{
				query.Parameters.MaxTime = MaxTime;
				filterConditions.Add(@"[MaxTime] = @Maxtime");
			}	
			if(Pausable != null)
			{
				query.Parameters.Pausable = Pausable;
				filterConditions.Add(@"[Pausable] = @Pausable");
			}	
			if(Resumable != null)
			{
				query.Parameters.Resumable = Resumable;
				filterConditions.Add(@"[Resumable] = @Resumable");
			}	
			if(ReviewAnswers != null)
			{
				query.Parameters.ReviewAnswers = ReviewAnswers;
				filterConditions.Add(@"[ReviewAnswers] = @Reviewanswers");
			}	
			if(ExamID != null)
			{
				query.Parameters.ExamID = ExamID;
				filterConditions.Add(@"[ExamID] = @Examid");
			}	
			if(ObjectDate != null || (CreateDate==null && DeleteDate==null))
			{
				Context oContext=new Context();
				query.Parameters.ObjectDate = (ObjectDate==null ? oContext.VersionDate : ObjectDate);
				filterConditions.Add(@"@ObjectDate BETWEEN [CreateDate] AND [DeleteDate]");
			}
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_exams] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Exams<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Exams<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Exams<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Exams<DataCollectionType, DataRecordType>()));
		}
		*/

		
		public override IDataReader GetDataReader()
		{
			return new ExamsDataReader(this);
		}
		
		public List<DtoExam> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class ExamsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public ExamsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoExam
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		/// </summary>
		[DataMember]
		public virtual int ObjectID { get; set; }
		
		/// <summary>
		///		CreateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime CreateDate { get; set; }
		
		/// <summary>
		///		DeleteDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime DeleteDate { get; set; }
		
		/// <summary>
		///		Name Property
		/// </summary>
		[DataMember]
		public virtual string Name { get; set; }
		
		/// <summary>
		///		IntroText Property
		/// </summary>
		[DataMember]
		public virtual string IntroText { get; set; }
		
		/// <summary>
		///		ExamText Property
		/// </summary>
		[DataMember]
		public virtual string ExamText { get; set; }
		
		/// <summary>
		///		Language Property
		/// </summary>
		[DataMember]
		public virtual string Language { get; set; }
		
		/// <summary>
		///		PlayerType Property
		/// </summary>
		[DataMember]
		public virtual string PlayerType { get; set; }
		
		/// <summary>
		///		MaxTime Property
		/// </summary>
		[DataMember]
		public virtual int MaxTime { get; set; }
		
		/// <summary>
		///		Pausable Property
		/// </summary>
		[DataMember]
		public virtual bool Pausable { get; set; }
		
		/// <summary>
		///		Resumable Property
		/// </summary>
		[DataMember]
		public virtual bool Resumable { get; set; }
		
		/// <summary>
		///		ReviewAnswers Property
		/// </summary>
		[DataMember]
		public virtual bool ReviewAnswers { get; set; }
		
		/// <summary>
		///		ExamID Property
		/// </summary>
		[DataMember]
		public virtual string ExamID { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class Exam : Exam<Exam>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Exam
		/// </summary>
		public Exam() : this(new Context()) {}
		
		/// <summary>
		/// Creates Exam
		/// </summary>
		public Exam(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for Exam
		/// </summary>
		protected Exam(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class Exam<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: Exam<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Exam
		/// </summary>
		public Exam() : this(new Context()) {}
		
		/// <summary>
		/// Creates Exam
		/// </summary>
		public Exam(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_exams";

			//Declare Fields
			this.Fields.Add("ObjectID" ,this._ObjectID = new jlib.DataFramework.BaseField<int>(this, Exams.Columns.ObjectID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreateDate" ,this._CreateDate = new jlib.DataFramework.BaseField<DateTime>(this, Exams.Columns.CreateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("DeleteDate" ,this._DeleteDate = new jlib.DataFramework.BaseField<DateTime>(this, Exams.Columns.DeleteDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Name" ,this._Name = new jlib.DataFramework.BaseField<string>(this, Exams.Columns.Name, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("IntroText" ,this._IntroText = new jlib.DataFramework.BaseField<string>(this, Exams.Columns.IntroText, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ExamText" ,this._ExamText = new jlib.DataFramework.BaseField<string>(this, Exams.Columns.ExamText, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Language" ,this._Language = new jlib.DataFramework.BaseField<string>(this, Exams.Columns.Language, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PlayerType" ,this._PlayerType = new jlib.DataFramework.BaseField<string>(this, Exams.Columns.PlayerType, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("MaxTime" ,this._MaxTime = new jlib.DataFramework.BaseField<int>(this, Exams.Columns.MaxTime, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Pausable" ,this._Pausable = new jlib.DataFramework.BaseField<bool>(this, Exams.Columns.Pausable, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Resumable" ,this._Resumable = new jlib.DataFramework.BaseField<bool>(this, Exams.Columns.Resumable, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ReviewAnswers" ,this._ReviewAnswers = new jlib.DataFramework.BaseField<bool>(this, Exams.Columns.ReviewAnswers, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ExamID" ,this._ExamID = new jlib.DataFramework.BaseField<string>(this, Exams.Columns.ExamID, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.ExamEx();
		}
		
		/// <summary>
		/// Deserialization method for Exam
		/// </summary>
		protected Exam(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void ExamEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		///		Database Field:		ObjectId;
		///		Database Parameter:	@Objectid;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exams", FieldName: "ObjectId", IsPrimaryKey=true)]
		public virtual int ObjectID
		{
			get 
			{ 
				return this._ObjectID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ObjectID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ObjectID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ObjectID { get; set; }

			
		/// <summary>
		///		CreateDate Property
		///		Database Field:		CreateDate;
		///		Database Parameter:	@Createdate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exams", FieldName: "CreateDate", IsPrimaryKey=true)]
		public virtual DateTime CreateDate
		{
			get 
			{ 
				return this._CreateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CreateDate { get; set; }

			
		/// <summary>
		///		DeleteDate Property
		///		Database Field:		DeleteDate;
		///		Database Parameter:	@Deletedate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exams", FieldName: "DeleteDate", IsPrimaryKey=true)]
		public virtual DateTime DeleteDate
		{
			get 
			{ 
				return this._DeleteDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._DeleteDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._DeleteDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _DeleteDate { get; set; }

			
		/// <summary>
		///		Name Property
		///		Database Field:		Name;
		///		Database Parameter:	@Name;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exams", FieldName: "Name", IsNullable=true)]
		public virtual string Name
		{
			get 
			{ 
				return (string)this._Name.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Name.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Name.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Name { get; set; }

			
		/// <summary>
		///		IntroText Property
		///		Database Field:		IntroText;
		///		Database Parameter:	@Introtext;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exams", FieldName: "IntroText", IsNullable=true)]
		public virtual string IntroText
		{
			get 
			{ 
				return (string)this._IntroText.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._IntroText.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._IntroText.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _IntroText { get; set; }

			
		/// <summary>
		///		ExamText Property
		///		Database Field:		ExamText;
		///		Database Parameter:	@Examtext;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exams", FieldName: "ExamText", IsNullable=true)]
		public virtual string ExamText
		{
			get 
			{ 
				return (string)this._ExamText.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._ExamText.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ExamText.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _ExamText { get; set; }

			
		/// <summary>
		///		Language Property
		///		Database Field:		Language;
		///		Database Parameter:	@Language;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exams", FieldName: "Language")]
		public virtual string Language
		{
			get 
			{ 
				return this._Language.Value; 
			}

			set 
			{ 
				//Set Value
				this._Language.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Language.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Language { get; set; }

			
		/// <summary>
		///		PlayerType Property
		///		Database Field:		PlayerType;
		///		Database Parameter:	@Playertype;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exams", FieldName: "PlayerType")]
		public virtual string PlayerType
		{
			get 
			{ 
				return this._PlayerType.Value; 
			}

			set 
			{ 
				//Set Value
				this._PlayerType.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PlayerType.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _PlayerType { get; set; }

			
		/// <summary>
		///		MaxTime Property
		///		Database Field:		MaxTime;
		///		Database Parameter:	@Maxtime;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exams", FieldName: "MaxTime")]
		public virtual int MaxTime
		{
			get 
			{ 
				return this._MaxTime.Value; 
			}

			set 
			{ 
				//Set Value
				this._MaxTime.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._MaxTime.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _MaxTime { get; set; }

			
		/// <summary>
		///		Pausable Property
		///		Database Field:		Pausable;
		///		Database Parameter:	@Pausable;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exams", FieldName: "Pausable")]
		public virtual bool Pausable
		{
			get 
			{ 
				return this._Pausable.Value; 
			}

			set 
			{ 
				//Set Value
				this._Pausable.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Pausable.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _Pausable { get; set; }

			
		/// <summary>
		///		Resumable Property
		///		Database Field:		Resumable;
		///		Database Parameter:	@Resumable;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exams", FieldName: "Resumable")]
		public virtual bool Resumable
		{
			get 
			{ 
				return this._Resumable.Value; 
			}

			set 
			{ 
				//Set Value
				this._Resumable.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Resumable.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _Resumable { get; set; }

			
		/// <summary>
		///		ReviewAnswers Property
		///		Database Field:		ReviewAnswers;
		///		Database Parameter:	@Reviewanswers;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exams", FieldName: "ReviewAnswers")]
		public virtual bool ReviewAnswers
		{
			get 
			{ 
				return this._ReviewAnswers.Value; 
			}

			set 
			{ 
				//Set Value
				this._ReviewAnswers.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ReviewAnswers.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _ReviewAnswers { get; set; }

			
		/// <summary>
		///		ExamID Property
		///		Database Field:		ExamID;
		///		Database Parameter:	@Examid;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_exams", FieldName: "ExamID", IsNullable=true)]
		public virtual string ExamID
		{
			get 
			{ 
				return (string)this._ExamID.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._ExamID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ExamID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _ExamID { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoExam Dto)
		{
			//Set Values
			this._ObjectID.SetChangedValueIfDifferent(Dto.ObjectID);
			this._CreateDate.SetChangedValueIfDifferent(Dto.CreateDate);
			this._DeleteDate.SetChangedValueIfDifferent(Dto.DeleteDate);
			this._Name.SetChangedValueIfDifferent(Dto.Name);
			this._IntroText.SetChangedValueIfDifferent(Dto.IntroText);
			this._ExamText.SetChangedValueIfDifferent(Dto.ExamText);
			this._Language.SetChangedValueIfDifferent(Dto.Language);
			this._PlayerType.SetChangedValueIfDifferent(Dto.PlayerType);
			this._MaxTime.SetChangedValueIfDifferent(Dto.MaxTime);
			this._Pausable.SetChangedValueIfDifferent(Dto.Pausable);
			this._Resumable.SetChangedValueIfDifferent(Dto.Resumable);
			this._ReviewAnswers.SetChangedValueIfDifferent(Dto.ReviewAnswers);
			this._ExamID.SetChangedValueIfDifferent(Dto.ExamID);
		}
		public virtual DtoExam ToDto()
		{
			return this.ToDto(new DtoExam());
		}
		
		public virtual DtoExam ToDto(DtoExam dto)
		{
			//Set Values
			dto.ObjectID = this.ObjectID;
			dto.CreateDate = this.CreateDate;
			dto.DeleteDate = this.DeleteDate;
			dto.Name = this.Name;
			dto.IntroText = this.IntroText;
			dto.ExamText = this.ExamText;
			dto.Language = this.Language;
			dto.PlayerType = this.PlayerType;
			dto.MaxTime = this.MaxTime;
			dto.Pausable = this.Pausable;
			dto.Resumable = this.Resumable;
			dto.ReviewAnswers = this.ReviewAnswers;
			dto.ExamID = this.ExamID;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****

				public static DataRecordType LoadByPk(int ObjectID)
				{
					Context oContext=new Context();
					//Create Sql Connection
					SqlHelper sqlHelper = oContext.GetSqlHelper();
			
					//Load
					DataRecordType item = LoadByPk(sqlHelper, ObjectID, oContext.VersionDate);
			
					//Return
					return item;
				}

		public static DataRecordType LoadByPk(int ObjectID,  DateTime ObjectDate)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ObjectID, ObjectDate);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ObjectID,  DateTime ObjectDate)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_exams]  WITH (NOLOCK)
										WHERE
											
											[ObjectId] = @PK_Objectid		AND @ObjectDate BETWEEN [CreateDate] AND [DeleteDate]

											",
											"@PK_Objectid", ObjectID, "@ObjectDate", ObjectDate
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class FinalTestCaches : FinalTestCaches<FinalTestCaches, FinalTestCach>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates FinalTestCaches
		/// </summary>
		public FinalTestCaches() : this(new Context()) {}
		
		/// <summary>
		/// Creates FinalTestCaches
		/// </summary>
		public FinalTestCaches(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class FinalTestCaches<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: FinalTestCach<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Xml = new jlib.DataFramework.BaseColumn<string>(FieldName: "Xml", PropertyName: "Xml", ParameterName: "@Xml", PkParameterName: "@PK_Xml", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CreateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CreateDate", PropertyName: "CreateDate", ParameterName: "@Createdate", PkParameterName: "@PK_Createdate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> FolderID = new jlib.DataFramework.BaseColumn<int>(FieldName: "FolderId", PropertyName: "FolderID", ParameterName: "@Folderid", PkParameterName: "@PK_Folderid", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Checksum = new jlib.DataFramework.BaseColumn<string>(FieldName: "Checksum", PropertyName: "Checksum", ParameterName: "@Checksum", PkParameterName: "@PK_Checksum", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates FinalTestCaches
		/// </summary>
		public FinalTestCaches() : this(new Context()) {}
		
		/// <summary>
		/// Creates FinalTestCaches
		/// </summary>
		public FinalTestCaches(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_final_test_cache";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = FinalTestCaches<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("Xml", this.Xml = FinalTestCaches<DataCollectionType, DataRecordType>.Columns.Xml);
			base.Columns.Add("CreateDate", this.CreateDate = FinalTestCaches<DataCollectionType, DataRecordType>.Columns.CreateDate);
			base.Columns.Add("FolderID", this.FolderID = FinalTestCaches<DataCollectionType, DataRecordType>.Columns.FolderID);
			base.Columns.Add("Checksum", this.Checksum = FinalTestCaches<DataCollectionType, DataRecordType>.Columns.Checksum);
			
			//Call the secondary constructor method
			this.FinalTestCachesEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void FinalTestCachesEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<string> Xml { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CreateDate { get; set; }
		public jlib.DataFramework.BaseColumn<int> FolderID { get; set; }
		public jlib.DataFramework.BaseColumn<string> Checksum { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_final_test_cache] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_final_test_cache] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_final_test_cache");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_final_test_cache", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, string Xml = null, DateTime? CreateDate = null, int? FolderID = null, string Checksum = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(Xml != null)
			{
				query.Parameters.Xml = Xml;
				filterConditions.Add(@"[Xml] = @Xml");
			}	
			if(CreateDate != null)
			{
				query.Parameters.CreateDate = CreateDate;
				filterConditions.Add(@"[CreateDate] = @Createdate");
			}	
			if(FolderID != null)
			{
				query.Parameters.FolderID = FolderID;
				filterConditions.Add(@"[FolderId] = @Folderid");
			}	
			if(Checksum != null)
			{
				query.Parameters.Checksum = Checksum;
				filterConditions.Add(@"[Checksum] = @Checksum");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_final_test_cache] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<FinalTestCaches<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new FinalTestCaches<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<FinalTestCaches<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new FinalTestCaches<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_final_test_cache] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new FinalTestCachesDataReader(this);
		}
		
		public List<DtoFinalTestCach> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class FinalTestCachesDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public FinalTestCachesDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoFinalTestCach
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		Xml Property
		/// </summary>
		[DataMember]
		public virtual string Xml { get; set; }
		
		/// <summary>
		///		CreateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime CreateDate { get; set; }
		
		/// <summary>
		///		FolderID Property
		/// </summary>
		[DataMember]
		public virtual int FolderID { get; set; }
		
		/// <summary>
		///		Checksum Property
		/// </summary>
		[DataMember]
		public virtual string Checksum { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class FinalTestCach : FinalTestCach<FinalTestCach>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates FinalTestCach
		/// </summary>
		public FinalTestCach() : this(new Context()) {}
		
		/// <summary>
		/// Creates FinalTestCach
		/// </summary>
		public FinalTestCach(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for FinalTestCach
		/// </summary>
		protected FinalTestCach(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class FinalTestCach<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: FinalTestCach<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates FinalTestCach
		/// </summary>
		public FinalTestCach() : this(new Context()) {}
		
		/// <summary>
		/// Creates FinalTestCach
		/// </summary>
		public FinalTestCach(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_final_test_cache";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, FinalTestCaches.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Xml" ,this._Xml = new jlib.DataFramework.BaseField<string>(this, FinalTestCaches.Columns.Xml, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreateDate" ,this._CreateDate = new jlib.DataFramework.BaseField<DateTime>(this, FinalTestCaches.Columns.CreateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("FolderID" ,this._FolderID = new jlib.DataFramework.BaseField<int>(this, FinalTestCaches.Columns.FolderID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Checksum" ,this._Checksum = new jlib.DataFramework.BaseField<string>(this, FinalTestCaches.Columns.Checksum, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.FinalTestCachEx();
		}
		
		/// <summary>
		/// Deserialization method for FinalTestCach
		/// </summary>
		protected FinalTestCach(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void FinalTestCachEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_final_test_cache", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		Xml Property
		///		Database Field:		Xml;
		///		Database Parameter:	@Xml;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_final_test_cache", FieldName: "Xml")]
		public virtual string Xml
		{
			get 
			{ 
				return this._Xml.Value; 
			}

			set 
			{ 
				//Set Value
				this._Xml.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Xml.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Xml { get; set; }

			
		/// <summary>
		///		CreateDate Property
		///		Database Field:		CreateDate;
		///		Database Parameter:	@Createdate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_final_test_cache", FieldName: "CreateDate")]
		public virtual DateTime CreateDate
		{
			get 
			{ 
				return this._CreateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CreateDate { get; set; }

			
		/// <summary>
		///		FolderID Property
		///		Database Field:		FolderId;
		///		Database Parameter:	@Folderid;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_final_test_cache", FieldName: "FolderId")]
		public virtual int FolderID
		{
			get 
			{ 
				return this._FolderID.Value; 
			}

			set 
			{ 
				//Set Value
				this._FolderID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._FolderID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _FolderID { get; set; }

			
		/// <summary>
		///		Checksum Property
		///		Database Field:		Checksum;
		///		Database Parameter:	@Checksum;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_final_test_cache", FieldName: "Checksum", IsNullable=true)]
		public virtual string Checksum
		{
			get 
			{ 
				return (string)this._Checksum.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Checksum.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Checksum.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Checksum { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoFinalTestCach Dto)
		{
			//Set Values
			this._Xml.SetChangedValueIfDifferent(Dto.Xml);
			this._CreateDate.SetChangedValueIfDifferent(Dto.CreateDate);
			this._FolderID.SetChangedValueIfDifferent(Dto.FolderID);
			this._Checksum.SetChangedValueIfDifferent(Dto.Checksum);
		}
		public virtual DtoFinalTestCach ToDto()
		{
			return this.ToDto(new DtoFinalTestCach());
		}
		
		public virtual DtoFinalTestCach ToDto(DtoFinalTestCach dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.Xml = this.Xml;
			dto.CreateDate = this.CreateDate;
			dto.FolderID = this.FolderID;
			dto.Checksum = this.Checksum;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_final_test_cache] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_final_test_cache]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class FolderPermissions : FolderPermissions<FolderPermissions, FolderPermission>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates FolderPermissions
		/// </summary>
		public FolderPermissions() : this(new Context()) {}
		
		/// <summary>
		/// Creates FolderPermissions
		/// </summary>
		public FolderPermissions(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class FolderPermissions<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: FolderPermission<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> FolderID = new jlib.DataFramework.BaseColumn<int>(FieldName: "FolderID", PropertyName: "FolderID", ParameterName: "@Folderid", PkParameterName: "@PK_Folderid", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> UserID = new jlib.DataFramework.BaseColumn<int>(FieldName: "UserID", PropertyName: "UserID", ParameterName: "@Userid", PkParameterName: "@PK_Userid", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> UpdateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "UpdateDate", PropertyName: "UpdateDate", ParameterName: "@Updatedate", PkParameterName: "@PK_Updatedate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<DateTime> EndDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "EndDate", PropertyName: "EndDate", ParameterName: "@Enddate", PkParameterName: "@PK_Enddate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> Propagate = new jlib.DataFramework.BaseColumn<bool>(FieldName: "Propagate", PropertyName: "Propagate", ParameterName: "@Propagate", PkParameterName: "@PK_Propagate", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> Read = new jlib.DataFramework.BaseColumn<bool>(FieldName: "Read", PropertyName: "Read", ParameterName: "@Read", PkParameterName: "@PK_Read", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> Write = new jlib.DataFramework.BaseColumn<bool>(FieldName: "Write", PropertyName: "Write", ParameterName: "@Write", PkParameterName: "@PK_Write", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> Publish = new jlib.DataFramework.BaseColumn<bool>(FieldName: "Publish", PropertyName: "Publish", ParameterName: "@Publish", PkParameterName: "@PK_Publish", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> Delete = new jlib.DataFramework.BaseColumn<bool>(FieldName: "Delete", PropertyName: "Delete", ParameterName: "@Delete", PkParameterName: "@PK_Delete", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates FolderPermissions
		/// </summary>
		public FolderPermissions() : this(new Context()) {}
		
		/// <summary>
		/// Creates FolderPermissions
		/// </summary>
		public FolderPermissions(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_folder_permissions";
			
			//Set Columns
			base.Columns.Add("FolderID", this.FolderID = FolderPermissions<DataCollectionType, DataRecordType>.Columns.FolderID);
			base.Columns.Add("UserID", this.UserID = FolderPermissions<DataCollectionType, DataRecordType>.Columns.UserID);
			base.Columns.Add("UpdateDate", this.UpdateDate = FolderPermissions<DataCollectionType, DataRecordType>.Columns.UpdateDate);
			base.Columns.Add("EndDate", this.EndDate = FolderPermissions<DataCollectionType, DataRecordType>.Columns.EndDate);
			base.Columns.Add("Propagate", this.Propagate = FolderPermissions<DataCollectionType, DataRecordType>.Columns.Propagate);
			base.Columns.Add("Read", this.Read = FolderPermissions<DataCollectionType, DataRecordType>.Columns.Read);
			base.Columns.Add("Write", this.Write = FolderPermissions<DataCollectionType, DataRecordType>.Columns.Write);
			base.Columns.Add("Publish", this.Publish = FolderPermissions<DataCollectionType, DataRecordType>.Columns.Publish);
			base.Columns.Add("Delete", this.Delete = FolderPermissions<DataCollectionType, DataRecordType>.Columns.Delete);
			
			//Call the secondary constructor method
			this.FolderPermissionsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void FolderPermissionsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> FolderID { get; set; }
		public jlib.DataFramework.BaseColumn<int> UserID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> UpdateDate { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> EndDate { get; set; }
		public jlib.DataFramework.BaseColumn<bool> Propagate { get; set; }
		public jlib.DataFramework.BaseColumn<bool> Read { get; set; }
		public jlib.DataFramework.BaseColumn<bool> Write { get; set; }
		public jlib.DataFramework.BaseColumn<bool> Publish { get; set; }
		public jlib.DataFramework.BaseColumn<bool> Delete { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_folder_permissions] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_folder_permissions] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_folder_permissions");
		}
		
		
		public static QueryPackage GetByField(int? FolderID = null, int? UserID = null, DateTime? UpdateDate = null, DateTime? EndDate = null, bool? Propagate = null, bool? Read = null, bool? Write = null, bool? Publish = null, bool? Delete = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(FolderID != null)
			{
				query.Parameters.FolderID = FolderID;
				filterConditions.Add(@"[FolderID] = @Folderid");
			}	
			if(UserID != null)
			{
				query.Parameters.UserID = UserID;
				filterConditions.Add(@"[UserID] = @Userid");
			}	
			if(UpdateDate != null)
			{
				query.Parameters.UpdateDate = UpdateDate;
				filterConditions.Add(@"[UpdateDate] = @Updatedate");
			}	
			if(EndDate != null)
			{
				query.Parameters.EndDate = EndDate;
				filterConditions.Add(@"[EndDate] = @Enddate");
			}	
			if(Propagate != null)
			{
				query.Parameters.Propagate = Propagate;
				filterConditions.Add(@"[Propagate] = @Propagate");
			}	
			if(Read != null)
			{
				query.Parameters.Read = Read;
				filterConditions.Add(@"[Read] = @Read");
			}	
			if(Write != null)
			{
				query.Parameters.Write = Write;
				filterConditions.Add(@"[Write] = @Write");
			}	
			if(Publish != null)
			{
				query.Parameters.Publish = Publish;
				filterConditions.Add(@"[Publish] = @Publish");
			}	
			if(Delete != null)
			{
				query.Parameters.Delete = Delete;
				filterConditions.Add(@"[Delete] = @Delete");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_folder_permissions] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<FolderPermissions<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new FolderPermissions<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<FolderPermissions<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new FolderPermissions<DataCollectionType, DataRecordType>()));
		}
		*/

		
		public override IDataReader GetDataReader()
		{
			return new FolderPermissionsDataReader(this);
		}
		
		public List<DtoFolderPermission> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class FolderPermissionsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public FolderPermissionsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoFolderPermission
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		FolderID Property
		/// </summary>
		[DataMember]
		public virtual int FolderID { get; set; }
		
		/// <summary>
		///		UserID Property
		/// </summary>
		[DataMember]
		public virtual int UserID { get; set; }
		
		/// <summary>
		///		UpdateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime UpdateDate { get; set; }
		
		/// <summary>
		///		EndDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime EndDate { get; set; }
		
		/// <summary>
		///		Propagate Property
		/// </summary>
		[DataMember]
		public virtual bool Propagate { get; set; }
		
		/// <summary>
		///		Read Property
		/// </summary>
		[DataMember]
		public virtual bool Read { get; set; }
		
		/// <summary>
		///		Write Property
		/// </summary>
		[DataMember]
		public virtual bool Write { get; set; }
		
		/// <summary>
		///		Publish Property
		/// </summary>
		[DataMember]
		public virtual bool Publish { get; set; }
		
		/// <summary>
		///		Delete Property
		/// </summary>
		[DataMember]
		public virtual bool Delete { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class FolderPermission : FolderPermission<FolderPermission>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates FolderPermission
		/// </summary>
		public FolderPermission() : this(new Context()) {}
		
		/// <summary>
		/// Creates FolderPermission
		/// </summary>
		public FolderPermission(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for FolderPermission
		/// </summary>
		protected FolderPermission(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class FolderPermission<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: FolderPermission<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates FolderPermission
		/// </summary>
		public FolderPermission() : this(new Context()) {}
		
		/// <summary>
		/// Creates FolderPermission
		/// </summary>
		public FolderPermission(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_folder_permissions";

			//Declare Fields
			this.Fields.Add("FolderID" ,this._FolderID = new jlib.DataFramework.BaseField<int>(this, FolderPermissions.Columns.FolderID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserID" ,this._UserID = new jlib.DataFramework.BaseField<int>(this, FolderPermissions.Columns.UserID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UpdateDate" ,this._UpdateDate = new jlib.DataFramework.BaseField<DateTime>(this, FolderPermissions.Columns.UpdateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("EndDate" ,this._EndDate = new jlib.DataFramework.BaseField<DateTime>(this, FolderPermissions.Columns.EndDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Propagate" ,this._Propagate = new jlib.DataFramework.BaseField<bool>(this, FolderPermissions.Columns.Propagate, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Read" ,this._Read = new jlib.DataFramework.BaseField<bool>(this, FolderPermissions.Columns.Read, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Write" ,this._Write = new jlib.DataFramework.BaseField<bool>(this, FolderPermissions.Columns.Write, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Publish" ,this._Publish = new jlib.DataFramework.BaseField<bool>(this, FolderPermissions.Columns.Publish, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Delete" ,this._Delete = new jlib.DataFramework.BaseField<bool>(this, FolderPermissions.Columns.Delete, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.FolderPermissionEx();
		}
		
		/// <summary>
		/// Deserialization method for FolderPermission
		/// </summary>
		protected FolderPermission(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void FolderPermissionEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		FolderID Property
		///		Database Field:		FolderID;
		///		Database Parameter:	@Folderid;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folder_permissions", FieldName: "FolderID", IsPrimaryKey=true)]
		public virtual int FolderID
		{
			get 
			{ 
				return this._FolderID.Value; 
			}

			set 
			{ 
				//Set Value
				this._FolderID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._FolderID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _FolderID { get; set; }

			
		/// <summary>
		///		UserID Property
		///		Database Field:		UserID;
		///		Database Parameter:	@Userid;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folder_permissions", FieldName: "UserID", IsPrimaryKey=true)]
		public virtual int UserID
		{
			get 
			{ 
				return this._UserID.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _UserID { get; set; }

			
		/// <summary>
		///		UpdateDate Property
		///		Database Field:		UpdateDate;
		///		Database Parameter:	@Updatedate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folder_permissions", FieldName: "UpdateDate")]
		public virtual DateTime UpdateDate
		{
			get 
			{ 
				return this._UpdateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._UpdateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UpdateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _UpdateDate { get; set; }

			
		/// <summary>
		///		EndDate Property
		///		Database Field:		EndDate;
		///		Database Parameter:	@Enddate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folder_permissions", FieldName: "EndDate")]
		public virtual DateTime EndDate
		{
			get 
			{ 
				return this._EndDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._EndDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._EndDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _EndDate { get; set; }

			
		/// <summary>
		///		Propagate Property
		///		Database Field:		Propagate;
		///		Database Parameter:	@Propagate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folder_permissions", FieldName: "Propagate")]
		public virtual bool Propagate
		{
			get 
			{ 
				return this._Propagate.Value; 
			}

			set 
			{ 
				//Set Value
				this._Propagate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Propagate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _Propagate { get; set; }

			
		/// <summary>
		///		Read Property
		///		Database Field:		Read;
		///		Database Parameter:	@Read;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folder_permissions", FieldName: "Read")]
		public virtual bool Read
		{
			get 
			{ 
				return this._Read.Value; 
			}

			set 
			{ 
				//Set Value
				this._Read.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Read.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _Read { get; set; }

			
		/// <summary>
		///		Write Property
		///		Database Field:		Write;
		///		Database Parameter:	@Write;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folder_permissions", FieldName: "Write")]
		public virtual bool Write
		{
			get 
			{ 
				return this._Write.Value; 
			}

			set 
			{ 
				//Set Value
				this._Write.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Write.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _Write { get; set; }

			
		/// <summary>
		///		Publish Property
		///		Database Field:		Publish;
		///		Database Parameter:	@Publish;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folder_permissions", FieldName: "Publish")]
		public virtual bool Publish
		{
			get 
			{ 
				return this._Publish.Value; 
			}

			set 
			{ 
				//Set Value
				this._Publish.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Publish.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _Publish { get; set; }

			
		/// <summary>
		///		Delete Property
		///		Database Field:		Delete;
		///		Database Parameter:	@Delete;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folder_permissions", FieldName: "Delete")]
		public virtual bool Delete
		{
			get 
			{ 
				return this._Delete.Value; 
			}

			set 
			{ 
				//Set Value
				this._Delete.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Delete.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _Delete { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoFolderPermission Dto)
		{
			//Set Values
			this._FolderID.SetChangedValueIfDifferent(Dto.FolderID);
			this._UserID.SetChangedValueIfDifferent(Dto.UserID);
			this._UpdateDate.SetChangedValueIfDifferent(Dto.UpdateDate);
			this._EndDate.SetChangedValueIfDifferent(Dto.EndDate);
			this._Propagate.SetChangedValueIfDifferent(Dto.Propagate);
			this._Read.SetChangedValueIfDifferent(Dto.Read);
			this._Write.SetChangedValueIfDifferent(Dto.Write);
			this._Publish.SetChangedValueIfDifferent(Dto.Publish);
			this._Delete.SetChangedValueIfDifferent(Dto.Delete);
		}
		public virtual DtoFolderPermission ToDto()
		{
			return this.ToDto(new DtoFolderPermission());
		}
		
		public virtual DtoFolderPermission ToDto(DtoFolderPermission dto)
		{
			//Set Values
			dto.FolderID = this.FolderID;
			dto.UserID = this.UserID;
			dto.UpdateDate = this.UpdateDate;
			dto.EndDate = this.EndDate;
			dto.Propagate = this.Propagate;
			dto.Read = this.Read;
			dto.Write = this.Write;
			dto.Publish = this.Publish;
			dto.Delete = this.Delete;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****



		public static DataRecordType LoadByPk(int FolderID, int UserID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, FolderID, UserID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int FolderID, int UserID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_folder_permissions]  WITH (NOLOCK)
										WHERE
											
											[FolderID] = @PK_Folderid
											AND [UserID] = @PK_Userid
											",
											"@PK_Folderid", FolderID, "@PK_Userid", UserID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class Folders : Folders<Folders, Folder>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Folders
		/// </summary>
		public Folders() : this(new Context()) {}
		
		/// <summary>
		/// Creates Folders
		/// </summary>
		public Folders(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Folders<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: Folder<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ObjectID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ObjectId", PropertyName: "ObjectID", ParameterName: "@Objectid", PkParameterName: "@PK_Objectid", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CreateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CreateDate", PropertyName: "CreateDate", ParameterName: "@Createdate", PkParameterName: "@PK_Createdate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> DeleteDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "DeleteDate", PropertyName: "DeleteDate", ParameterName: "@Deletedate", PkParameterName: "@PK_Deletedate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Name = new jlib.DataFramework.BaseColumn<string>(FieldName: "Name", PropertyName: "Name", ParameterName: "@Name", PkParameterName: "@PK_Name", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Context_Value = new jlib.DataFramework.BaseColumn<string>(FieldName: "Context", PropertyName: "Context_Value", ParameterName: "@Context_Value", PkParameterName: "@PK_Context_Value", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> AuxField1 = new jlib.DataFramework.BaseColumn<string>(FieldName: "aux_field_1", PropertyName: "AuxField1", ParameterName: "@AuxField1", PkParameterName: "@PK_AuxField1", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> AuxField2 = new jlib.DataFramework.BaseColumn<string>(FieldName: "aux_field_2", PropertyName: "AuxField2", ParameterName: "@AuxField2", PkParameterName: "@PK_AuxField2", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Text = new jlib.DataFramework.BaseColumn<string>(FieldName: "Text", PropertyName: "Text", ParameterName: "@Text", PkParameterName: "@PK_Text", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> CreatedbyID = new jlib.DataFramework.BaseColumn<int>(FieldName: "CreatedBy", PropertyName: "CreatedbyID", ParameterName: "@Createdby", PkParameterName: "@PK_Createdby", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> ModifiedbyID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ModifiedBy", PropertyName: "ModifiedbyID", ParameterName: "@Modifiedby", PkParameterName: "@PK_Modifiedby", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> VisibleStatus = new jlib.DataFramework.BaseColumn<int>(FieldName: "VisibleStatus", PropertyName: "VisibleStatus", ParameterName: "@Visiblestatus", PkParameterName: "@PK_Visiblestatus", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> TNumparents = new jlib.DataFramework.BaseColumn<int>(FieldName: "T_NumParents", PropertyName: "TNumparents", ParameterName: "@TNumparents", PkParameterName: "@PK_TNumparents", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> TNumchildren = new jlib.DataFramework.BaseColumn<int>(FieldName: "T_NumChildren", PropertyName: "TNumchildren", ParameterName: "@TNumchildren", PkParameterName: "@PK_TNumchildren", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Folders
		/// </summary>
		public Folders() : this(new Context()) {}
		
		/// <summary>
		/// Creates Folders
		/// </summary>
		public Folders(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_folders";
			
			//Set Columns
			base.Columns.Add("ObjectID", this.ObjectID = Folders<DataCollectionType, DataRecordType>.Columns.ObjectID);
			base.Columns.Add("CreateDate", this.CreateDate = Folders<DataCollectionType, DataRecordType>.Columns.CreateDate);
			base.Columns.Add("DeleteDate", this.DeleteDate = Folders<DataCollectionType, DataRecordType>.Columns.DeleteDate);
			base.Columns.Add("Name", this.Name = Folders<DataCollectionType, DataRecordType>.Columns.Name);
			base.Columns.Add("Context_Value", this.Context_Value = Folders<DataCollectionType, DataRecordType>.Columns.Context_Value);
			base.Columns.Add("AuxField1", this.AuxField1 = Folders<DataCollectionType, DataRecordType>.Columns.AuxField1);
			base.Columns.Add("AuxField2", this.AuxField2 = Folders<DataCollectionType, DataRecordType>.Columns.AuxField2);
			base.Columns.Add("Text", this.Text = Folders<DataCollectionType, DataRecordType>.Columns.Text);
			base.Columns.Add("CreatedbyID", this.CreatedbyID = Folders<DataCollectionType, DataRecordType>.Columns.CreatedbyID);
			base.Columns.Add("ModifiedbyID", this.ModifiedbyID = Folders<DataCollectionType, DataRecordType>.Columns.ModifiedbyID);
			base.Columns.Add("VisibleStatus", this.VisibleStatus = Folders<DataCollectionType, DataRecordType>.Columns.VisibleStatus);
			base.Columns.Add("TNumparents", this.TNumparents = Folders<DataCollectionType, DataRecordType>.Columns.TNumparents);
			base.Columns.Add("TNumchildren", this.TNumchildren = Folders<DataCollectionType, DataRecordType>.Columns.TNumchildren);
			
			//Call the secondary constructor method
			this.FoldersEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void FoldersEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ObjectID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CreateDate { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> DeleteDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> Name { get; set; }
		public jlib.DataFramework.BaseColumn<string> Context_Value { get; set; }
		public jlib.DataFramework.BaseColumn<string> AuxField1 { get; set; }
		public jlib.DataFramework.BaseColumn<string> AuxField2 { get; set; }
		public jlib.DataFramework.BaseColumn<string> Text { get; set; }
		public jlib.DataFramework.BaseColumn<int> CreatedbyID { get; set; }
		public jlib.DataFramework.BaseColumn<int> ModifiedbyID { get; set; }
		public jlib.DataFramework.BaseColumn<int> VisibleStatus { get; set; }
		public jlib.DataFramework.BaseColumn<int> TNumparents { get; set; }
		public jlib.DataFramework.BaseColumn<int> TNumchildren { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_folders] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_folders] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_folders");
		}
		
		
		public static QueryPackage GetByField(int? ObjectID = null, DateTime? CreateDate = null, DateTime? DeleteDate = null, string Name = null, string Context_Value = null, string AuxField1 = null, string AuxField2 = null, string Text = null, int? CreatedbyID = null, int? ModifiedbyID = null, int? VisibleStatus = null, int? TNumparents = null, int? TNumchildren = null, DateTime? ObjectDate = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ObjectID != null)
			{
				query.Parameters.ObjectID = ObjectID;
				filterConditions.Add(@"[ObjectId] = @Objectid");
			}	
			if(CreateDate != null)
			{
				query.Parameters.CreateDate = CreateDate;
				filterConditions.Add(@"[CreateDate] = @Createdate");
			}	
			if(DeleteDate != null)
			{
				query.Parameters.DeleteDate = DeleteDate;
				filterConditions.Add(@"[DeleteDate] = @Deletedate");
			}	
			if(Name != null)
			{
				query.Parameters.Name = Name;
				filterConditions.Add(@"[Name] = @Name");
			}	
			if(Context_Value != null)
			{
				query.Parameters.Context_Value = Context_Value;
				filterConditions.Add(@"[Context] = @Context_Value");
			}	
			if(AuxField1 != null)
			{
				query.Parameters.AuxField1 = AuxField1;
				filterConditions.Add(@"[aux_field_1] = @AuxField1");
			}	
			if(AuxField2 != null)
			{
				query.Parameters.AuxField2 = AuxField2;
				filterConditions.Add(@"[aux_field_2] = @AuxField2");
			}	
			if(Text != null)
			{
				query.Parameters.Text = Text;
				filterConditions.Add(@"[Text] = @Text");
			}	
			if(CreatedbyID != null)
			{
				query.Parameters.CreatedbyID = CreatedbyID;
				filterConditions.Add(@"[CreatedBy] = @Createdby");
			}	
			if(ModifiedbyID != null)
			{
				query.Parameters.ModifiedbyID = ModifiedbyID;
				filterConditions.Add(@"[ModifiedBy] = @Modifiedby");
			}	
			if(VisibleStatus != null)
			{
				query.Parameters.VisibleStatus = VisibleStatus;
				filterConditions.Add(@"[VisibleStatus] = @Visiblestatus");
			}	
			if(TNumparents != null)
			{
				query.Parameters.TNumparents = TNumparents;
				filterConditions.Add(@"[T_NumParents] = @TNumparents");
			}	
			if(TNumchildren != null)
			{
				query.Parameters.TNumchildren = TNumchildren;
				filterConditions.Add(@"[T_NumChildren] = @TNumchildren");
			}	
			if(ObjectDate != null || (CreateDate==null && DeleteDate==null))
			{
				Context oContext=new Context();
				query.Parameters.ObjectDate = (ObjectDate==null ? oContext.VersionDate : ObjectDate);
				filterConditions.Add(@"@ObjectDate BETWEEN [CreateDate] AND [DeleteDate]");
			}
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_folders] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Folders<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Folders<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Folders<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Folders<DataCollectionType, DataRecordType>()));
		}
		*/

		
		public override IDataReader GetDataReader()
		{
			return new FoldersDataReader(this);
		}
		
		public List<DtoFolder> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class FoldersDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public FoldersDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoFolder
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		/// </summary>
		[DataMember]
		public virtual int ObjectID { get; set; }
		
		/// <summary>
		///		CreateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime CreateDate { get; set; }
		
		/// <summary>
		///		DeleteDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime DeleteDate { get; set; }
		
		/// <summary>
		///		Name Property
		/// </summary>
		[DataMember]
		public virtual string Name { get; set; }
		
		/// <summary>
		///		Context_Value Property
		/// </summary>
		[DataMember]
		public virtual string Context_Value { get; set; }
		
		/// <summary>
		///		AuxField1 Property
		/// </summary>
		[DataMember]
		public virtual string AuxField1 { get; set; }
		
		/// <summary>
		///		AuxField2 Property
		/// </summary>
		[DataMember]
		public virtual string AuxField2 { get; set; }
		
		/// <summary>
		///		Text Property
		/// </summary>
		[DataMember]
		public virtual string Text { get; set; }
		
		/// <summary>
		///		CreatedbyID Property
		/// </summary>
		[DataMember]
		public virtual int CreatedbyID { get; set; }
		
		/// <summary>
		///		ModifiedbyID Property
		/// </summary>
		[DataMember]
		public virtual int ModifiedbyID { get; set; }
		
		/// <summary>
		///		VisibleStatus Property
		/// </summary>
		[DataMember]
		public virtual int VisibleStatus { get; set; }
		
		/// <summary>
		///		TNumparents Property
		/// </summary>
		[DataMember]
		public virtual int TNumparents { get; set; }
		
		/// <summary>
		///		TNumchildren Property
		/// </summary>
		[DataMember]
		public virtual int TNumchildren { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class Folder : Folder<Folder>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Folder
		/// </summary>
		public Folder() : this(new Context()) {}
		
		/// <summary>
		/// Creates Folder
		/// </summary>
		public Folder(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for Folder
		/// </summary>
		protected Folder(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class Folder<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: Folder<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Folder
		/// </summary>
		public Folder() : this(new Context()) {}
		
		/// <summary>
		/// Creates Folder
		/// </summary>
		public Folder(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_folders";

			//Declare Fields
			this.Fields.Add("ObjectID" ,this._ObjectID = new jlib.DataFramework.BaseField<int>(this, Folders.Columns.ObjectID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreateDate" ,this._CreateDate = new jlib.DataFramework.BaseField<DateTime>(this, Folders.Columns.CreateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("DeleteDate" ,this._DeleteDate = new jlib.DataFramework.BaseField<DateTime>(this, Folders.Columns.DeleteDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Name" ,this._Name = new jlib.DataFramework.BaseField<string>(this, Folders.Columns.Name, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Context_Value" ,this._Context_Value = new jlib.DataFramework.BaseField<string>(this, Folders.Columns.Context_Value, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("AuxField1" ,this._AuxField1 = new jlib.DataFramework.BaseField<string>(this, Folders.Columns.AuxField1, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("AuxField2" ,this._AuxField2 = new jlib.DataFramework.BaseField<string>(this, Folders.Columns.AuxField2, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Text" ,this._Text = new jlib.DataFramework.BaseField<string>(this, Folders.Columns.Text, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreatedbyID" ,this._CreatedbyID = new jlib.DataFramework.BaseField<int>(this, Folders.Columns.CreatedbyID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ModifiedbyID" ,this._ModifiedbyID = new jlib.DataFramework.BaseField<int>(this, Folders.Columns.ModifiedbyID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("VisibleStatus" ,this._VisibleStatus = new jlib.DataFramework.BaseField<int>(this, Folders.Columns.VisibleStatus, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("TNumparents" ,this._TNumparents = new jlib.DataFramework.BaseField<int>(this, Folders.Columns.TNumparents, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("TNumchildren" ,this._TNumchildren = new jlib.DataFramework.BaseField<int>(this, Folders.Columns.TNumchildren, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.FolderEx();
		}
		
		/// <summary>
		/// Deserialization method for Folder
		/// </summary>
		protected Folder(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void FolderEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		///		Database Field:		ObjectId;
		///		Database Parameter:	@Objectid;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folders", FieldName: "ObjectId", IsPrimaryKey=true)]
		public virtual int ObjectID
		{
			get 
			{ 
				return this._ObjectID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ObjectID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ObjectID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ObjectID { get; set; }

			
		/// <summary>
		///		CreateDate Property
		///		Database Field:		CreateDate;
		///		Database Parameter:	@Createdate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folders", FieldName: "CreateDate", IsPrimaryKey=true)]
		public virtual DateTime CreateDate
		{
			get 
			{ 
				return this._CreateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CreateDate { get; set; }

			
		/// <summary>
		///		DeleteDate Property
		///		Database Field:		DeleteDate;
		///		Database Parameter:	@Deletedate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folders", FieldName: "DeleteDate", IsPrimaryKey=true)]
		public virtual DateTime DeleteDate
		{
			get 
			{ 
				return this._DeleteDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._DeleteDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._DeleteDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _DeleteDate { get; set; }

			
		/// <summary>
		///		Name Property
		///		Database Field:		Name;
		///		Database Parameter:	@Name;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folders", FieldName: "Name")]
		public virtual string Name
		{
			get 
			{ 
				return this._Name.Value; 
			}

			set 
			{ 
				//Set Value
				this._Name.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Name.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Name { get; set; }

			
		/// <summary>
		///		Context_Value Property
		///		Database Field:		Context;
		///		Database Parameter:	@Context_Value;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folders", FieldName: "Context", IsNullable=true)]
		public virtual string Context_Value
		{
			get 
			{ 
				return (string)this._Context_Value.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Context_Value.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Context_Value.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Context_Value { get; set; }

			
		/// <summary>
		///		AuxField1 Property
		///		Database Field:		aux_field_1;
		///		Database Parameter:	@AuxField1;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folders", FieldName: "aux_field_1", IsNullable=true)]
		public virtual string AuxField1
		{
			get 
			{ 
				return (string)this._AuxField1.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._AuxField1.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._AuxField1.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _AuxField1 { get; set; }

			
		/// <summary>
		///		AuxField2 Property
		///		Database Field:		aux_field_2;
		///		Database Parameter:	@AuxField2;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folders", FieldName: "aux_field_2", IsNullable=true)]
		public virtual string AuxField2
		{
			get 
			{ 
				return (string)this._AuxField2.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._AuxField2.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._AuxField2.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _AuxField2 { get; set; }

			
		/// <summary>
		///		Text Property
		///		Database Field:		Text;
		///		Database Parameter:	@Text;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folders", FieldName: "Text", IsNullable=true)]
		public virtual string Text
		{
			get 
			{ 
				return (string)this._Text.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Text.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Text.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Text { get; set; }

			
		/// <summary>
		///		CreatedbyID Property
		///		Database Field:		CreatedBy;
		///		Database Parameter:	@Createdby;
		///		Primary Key:		No;
		///		Foreign Key:		Yes;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folders", FieldName: "CreatedBy", IsForeignKey=true)]
		public virtual int CreatedbyID
		{
			get 
			{ 
				return this._CreatedbyID.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreatedbyID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreatedbyID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

				//Set Foreign Key Object to Null
				this._CreatedBy = null;
			}
		}
		protected jlib.DataFramework.BaseField<int> _CreatedbyID { get; set; }

		/// <summary>
		///		CreatedBy Foreign Object Property
		///		Foreign Key Field:	CreatedBy
		///		Primary Key Field:	ID
		/// </summary>
		[jlib.DataFramework.Field("d_folders", "CreatedBy")]
		public virtual BehindUser CreatedBy
		{
			get
			{
				if(this._CreatedBy == null )
				{
					this._CreatedBy = BehindUsers.GetByField(ID:(int)this.CreatedbyID).Execute().FirstOrDefault();
				}
				
						
				return this._CreatedBy;
			}
			
			set
			{
				this._CreatedBy = value;	
			}
		}
		protected BehindUser _CreatedBy { get; set; }
			
		/// <summary>
		///		ModifiedbyID Property
		///		Database Field:		ModifiedBy;
		///		Database Parameter:	@Modifiedby;
		///		Primary Key:		No;
		///		Foreign Key:		Yes;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folders", FieldName: "ModifiedBy", IsForeignKey=true)]
		public virtual int ModifiedbyID
		{
			get 
			{ 
				return this._ModifiedbyID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ModifiedbyID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ModifiedbyID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

				//Set Foreign Key Object to Null
				this._ModifiedBy = null;
			}
		}
		protected jlib.DataFramework.BaseField<int> _ModifiedbyID { get; set; }

		/// <summary>
		///		ModifiedBy Foreign Object Property
		///		Foreign Key Field:	ModifiedBy
		///		Primary Key Field:	ID
		/// </summary>
		[jlib.DataFramework.Field("d_folders", "ModifiedBy")]
		public virtual BehindUser ModifiedBy
		{
			get
			{
				if(this._ModifiedBy == null )
				{
					this._ModifiedBy = BehindUsers.GetByField(ID:(int)this.ModifiedbyID).Execute().FirstOrDefault();
				}
				
						
				return this._ModifiedBy;
			}
			
			set
			{
				this._ModifiedBy = value;	
			}
		}
		protected BehindUser _ModifiedBy { get; set; }
			
		/// <summary>
		///		VisibleStatus Property
		///		Database Field:		VisibleStatus;
		///		Database Parameter:	@Visiblestatus;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folders", FieldName: "VisibleStatus")]
		public virtual int VisibleStatus
		{
			get 
			{ 
				return this._VisibleStatus.Value; 
			}

			set 
			{ 
				//Set Value
				this._VisibleStatus.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._VisibleStatus.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _VisibleStatus { get; set; }

			
		/// <summary>
		///		TNumparents Property
		///		Database Field:		T_NumParents;
		///		Database Parameter:	@TNumparents;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folders", FieldName: "T_NumParents")]
		public virtual int TNumparents
		{
			get 
			{ 
				return this._TNumparents.Value; 
			}

			set 
			{ 
				//Set Value
				this._TNumparents.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._TNumparents.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _TNumparents { get; set; }

			
		/// <summary>
		///		TNumchildren Property
		///		Database Field:		T_NumChildren;
		///		Database Parameter:	@TNumchildren;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_folders", FieldName: "T_NumChildren")]
		public virtual int TNumchildren
		{
			get 
			{ 
				return this._TNumchildren.Value; 
			}

			set 
			{ 
				//Set Value
				this._TNumchildren.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._TNumchildren.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _TNumchildren { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoFolder Dto)
		{
			//Set Values
			this._ObjectID.SetChangedValueIfDifferent(Dto.ObjectID);
			this._CreateDate.SetChangedValueIfDifferent(Dto.CreateDate);
			this._DeleteDate.SetChangedValueIfDifferent(Dto.DeleteDate);
			this._Name.SetChangedValueIfDifferent(Dto.Name);
			this._Context_Value.SetChangedValueIfDifferent(Dto.Context_Value);
			this._AuxField1.SetChangedValueIfDifferent(Dto.AuxField1);
			this._AuxField2.SetChangedValueIfDifferent(Dto.AuxField2);
			this._Text.SetChangedValueIfDifferent(Dto.Text);
			this._CreatedbyID.SetChangedValueIfDifferent(Dto.CreatedbyID);
			this._ModifiedbyID.SetChangedValueIfDifferent(Dto.ModifiedbyID);
			this._VisibleStatus.SetChangedValueIfDifferent(Dto.VisibleStatus);
			this._TNumparents.SetChangedValueIfDifferent(Dto.TNumparents);
			this._TNumchildren.SetChangedValueIfDifferent(Dto.TNumchildren);
		}
		public virtual DtoFolder ToDto()
		{
			return this.ToDto(new DtoFolder());
		}
		
		public virtual DtoFolder ToDto(DtoFolder dto)
		{
			//Set Values
			dto.ObjectID = this.ObjectID;
			dto.CreateDate = this.CreateDate;
			dto.DeleteDate = this.DeleteDate;
			dto.Name = this.Name;
			dto.Context_Value = this.Context_Value;
			dto.AuxField1 = this.AuxField1;
			dto.AuxField2 = this.AuxField2;
			dto.Text = this.Text;
			dto.CreatedbyID = this.CreatedbyID;
			dto.ModifiedbyID = this.ModifiedbyID;
			dto.VisibleStatus = this.VisibleStatus;
			dto.TNumparents = this.TNumparents;
			dto.TNumchildren = this.TNumchildren;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				//Create Internal Object 'createdby'
				case "createdby":
					this._CreatedBy = new BehindUser();
					this._CreatedBy.Import(Row, InternalObjectIdentifier);
					break;
				
				//Create Internal Object 'modifiedby'
				case "modifiedby":
					this._ModifiedBy = new BehindUser();
					this._ModifiedBy.Import(Row, InternalObjectIdentifier);
					break;
				
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****

				public static DataRecordType LoadByPk(int ObjectID)
				{
					Context oContext=new Context();
					//Create Sql Connection
					SqlHelper sqlHelper = oContext.GetSqlHelper();
			
					//Load
					DataRecordType item = LoadByPk(sqlHelper, ObjectID, oContext.VersionDate);
			
					//Return
					return item;
				}

		public static DataRecordType LoadByPk(int ObjectID,  DateTime ObjectDate)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ObjectID, ObjectDate);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ObjectID,  DateTime ObjectDate)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_folders]  WITH (NOLOCK)
										WHERE
											
											[ObjectId] = @PK_Objectid		AND @ObjectDate BETWEEN [CreateDate] AND [DeleteDate]

											",
											"@PK_Objectid", ObjectID, "@ObjectDate", ObjectDate
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class Logs : Logs<Logs, Log>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Logs
		/// </summary>
		public Logs() : this(new Context()) {}
		
		/// <summary>
		/// Creates Logs
		/// </summary>
		public Logs(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Logs<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: Log<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> PostedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "posted_date", PropertyName: "PostedDate", ParameterName: "@PostedDate", PkParameterName: "@PK_PostedDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Event = new jlib.DataFramework.BaseColumn<string>(FieldName: "event", PropertyName: "Event", ParameterName: "@Event", PkParameterName: "@PK_Event", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Status = new jlib.DataFramework.BaseColumn<string>(FieldName: "status", PropertyName: "Status", ParameterName: "@Status", PkParameterName: "@PK_Status", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Logs
		/// </summary>
		public Logs() : this(new Context()) {}
		
		/// <summary>
		/// Creates Logs
		/// </summary>
		public Logs(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_log";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = Logs<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("PostedDate", this.PostedDate = Logs<DataCollectionType, DataRecordType>.Columns.PostedDate);
			base.Columns.Add("Event", this.Event = Logs<DataCollectionType, DataRecordType>.Columns.Event);
			base.Columns.Add("Status", this.Status = Logs<DataCollectionType, DataRecordType>.Columns.Status);
			
			//Call the secondary constructor method
			this.LogsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void LogsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> PostedDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> Event { get; set; }
		public jlib.DataFramework.BaseColumn<string> Status { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_log] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_log] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_log");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_log", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, DateTime? PostedDate = null, string Event = null, string Status = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(PostedDate != null)
			{
				query.Parameters.PostedDate = PostedDate;
				filterConditions.Add(@"[posted_date] = @PostedDate");
			}	
			if(Event != null)
			{
				query.Parameters.Event = Event;
				filterConditions.Add(@"[event] = @Event");
			}	
			if(Status != null)
			{
				query.Parameters.Status = Status;
				filterConditions.Add(@"[status] = @Status");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_log] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Logs<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Logs<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Logs<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Logs<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_log] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new LogsDataReader(this);
		}
		
		public List<DtoLog> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class LogsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public LogsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoLog
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		PostedDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime PostedDate { get; set; }
		
		/// <summary>
		///		Event Property
		/// </summary>
		[DataMember]
		public virtual string Event { get; set; }
		
		/// <summary>
		///		Status Property
		/// </summary>
		[DataMember]
		public virtual string Status { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class Log : Log<Log>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Log
		/// </summary>
		public Log() : this(new Context()) {}
		
		/// <summary>
		/// Creates Log
		/// </summary>
		public Log(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for Log
		/// </summary>
		protected Log(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class Log<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: Log<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Log
		/// </summary>
		public Log() : this(new Context()) {}
		
		/// <summary>
		/// Creates Log
		/// </summary>
		public Log(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_log";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, Logs.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostedDate" ,this._PostedDate = new jlib.DataFramework.BaseField<DateTime>(this, Logs.Columns.PostedDate, InitialValue: DateTime.Now, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Event" ,this._Event = new jlib.DataFramework.BaseField<string>(this, Logs.Columns.Event, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Status" ,this._Status = new jlib.DataFramework.BaseField<string>(this, Logs.Columns.Status, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.LogEx();
		}
		
		/// <summary>
		/// Deserialization method for Log
		/// </summary>
		protected Log(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void LogEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_log", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		PostedDate Property
		///		Database Field:		posted_date;
		///		Database Parameter:	@PostedDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_log", FieldName: "posted_date")]
		public virtual DateTime PostedDate
		{
			get 
			{ 
				return this._PostedDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._PostedDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _PostedDate { get; set; }

			
		/// <summary>
		///		Event Property
		///		Database Field:		event;
		///		Database Parameter:	@Event;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_log", FieldName: "event")]
		public virtual string Event
		{
			get 
			{ 
				return this._Event.Value; 
			}

			set 
			{ 
				//Set Value
				this._Event.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Event.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Event { get; set; }

			
		/// <summary>
		///		Status Property
		///		Database Field:		status;
		///		Database Parameter:	@Status;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_log", FieldName: "status", IsNullable=true)]
		public virtual string Status
		{
			get 
			{ 
				return (string)this._Status.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Status.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Status.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Status { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoLog Dto)
		{
			//Set Values
			this._PostedDate.SetChangedValueIfDifferent(Dto.PostedDate);
			this._Event.SetChangedValueIfDifferent(Dto.Event);
			this._Status.SetChangedValueIfDifferent(Dto.Status);
		}
		public virtual DtoLog ToDto()
		{
			return this.ToDto(new DtoLog());
		}
		
		public virtual DtoLog ToDto(DtoLog dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.PostedDate = this.PostedDate;
			dto.Event = this.Event;
			dto.Status = this.Status;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_log] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_log]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class PageViews : PageViews<PageViews, PageView>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates PageViews
		/// </summary>
		public PageViews() : this(new Context()) {}
		
		/// <summary>
		/// Creates PageViews
		/// </summary>
		public PageViews(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class PageViews<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: PageView<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<long> ID = new jlib.DataFramework.BaseColumn<long>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int64, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> UserID = new jlib.DataFramework.BaseColumn<int>(FieldName: "user_id", PropertyName: "UserID", ParameterName: "@UserID", PkParameterName: "@PK_UserID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Referrer = new jlib.DataFramework.BaseColumn<string>(FieldName: "referrer", PropertyName: "Referrer", ParameterName: "@Referrer", PkParameterName: "@PK_Referrer", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Ip = new jlib.DataFramework.BaseColumn<string>(FieldName: "ip", PropertyName: "Ip", ParameterName: "@Ip", PkParameterName: "@PK_Ip", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> PostedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "posted_date", PropertyName: "PostedDate", ParameterName: "@PostedDate", PkParameterName: "@PK_PostedDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Url = new jlib.DataFramework.BaseColumn<string>(FieldName: "url", PropertyName: "Url", ParameterName: "@Url", PkParameterName: "@PK_Url", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<bool> IsNew = new jlib.DataFramework.BaseColumn<bool>(FieldName: "is_new", PropertyName: "IsNew", ParameterName: "@IsNew", PkParameterName: "@PK_IsNew", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> PageTitle = new jlib.DataFramework.BaseColumn<string>(FieldName: "page_title", PropertyName: "PageTitle", ParameterName: "@PageTitle", PkParameterName: "@PK_PageTitle", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<long> ItemID = new jlib.DataFramework.BaseColumn<long>(FieldName: "item_id", PropertyName: "ItemID", ParameterName: "@ItemID", PkParameterName: "@PK_ItemID", DBType: DbType.Int64, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> SessionID = new jlib.DataFramework.BaseColumn<string>(FieldName: "session_id", PropertyName: "SessionID", ParameterName: "@SessionID", PkParameterName: "@PK_SessionID", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> UserAgent = new jlib.DataFramework.BaseColumn<string>(FieldName: "user_agent", PropertyName: "UserAgent", ParameterName: "@UserAgent", PkParameterName: "@PK_UserAgent", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> LoadTime = new jlib.DataFramework.BaseColumn<int>(FieldName: "load_time", PropertyName: "LoadTime", ParameterName: "@LoadTime", PkParameterName: "@PK_LoadTime", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> PostData = new jlib.DataFramework.BaseColumn<string>(FieldName: "post_data", PropertyName: "PostData", ParameterName: "@PostData", PkParameterName: "@PK_PostData", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates PageViews
		/// </summary>
		public PageViews() : this(new Context()) {}
		
		/// <summary>
		/// Creates PageViews
		/// </summary>
		public PageViews(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_page_views";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = PageViews<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("UserID", this.UserID = PageViews<DataCollectionType, DataRecordType>.Columns.UserID);
			base.Columns.Add("Referrer", this.Referrer = PageViews<DataCollectionType, DataRecordType>.Columns.Referrer);
			base.Columns.Add("Ip", this.Ip = PageViews<DataCollectionType, DataRecordType>.Columns.Ip);
			base.Columns.Add("PostedDate", this.PostedDate = PageViews<DataCollectionType, DataRecordType>.Columns.PostedDate);
			base.Columns.Add("Url", this.Url = PageViews<DataCollectionType, DataRecordType>.Columns.Url);
			base.Columns.Add("IsNew", this.IsNew = PageViews<DataCollectionType, DataRecordType>.Columns.IsNew);
			base.Columns.Add("PageTitle", this.PageTitle = PageViews<DataCollectionType, DataRecordType>.Columns.PageTitle);
			base.Columns.Add("ItemID", this.ItemID = PageViews<DataCollectionType, DataRecordType>.Columns.ItemID);
			base.Columns.Add("SessionID", this.SessionID = PageViews<DataCollectionType, DataRecordType>.Columns.SessionID);
			base.Columns.Add("UserAgent", this.UserAgent = PageViews<DataCollectionType, DataRecordType>.Columns.UserAgent);
			base.Columns.Add("LoadTime", this.LoadTime = PageViews<DataCollectionType, DataRecordType>.Columns.LoadTime);
			base.Columns.Add("PostData", this.PostData = PageViews<DataCollectionType, DataRecordType>.Columns.PostData);
			
			//Call the secondary constructor method
			this.PageViewsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void PageViewsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<long> ID { get; set; }
		public jlib.DataFramework.BaseColumn<int> UserID { get; set; }
		public jlib.DataFramework.BaseColumn<string> Referrer { get; set; }
		public jlib.DataFramework.BaseColumn<string> Ip { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> PostedDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> Url { get; set; }
		public jlib.DataFramework.BaseColumn<bool> IsNew { get; set; }
		public jlib.DataFramework.BaseColumn<string> PageTitle { get; set; }
		public jlib.DataFramework.BaseColumn<long> ItemID { get; set; }
		public jlib.DataFramework.BaseColumn<string> SessionID { get; set; }
		public jlib.DataFramework.BaseColumn<string> UserAgent { get; set; }
		public jlib.DataFramework.BaseColumn<int> LoadTime { get; set; }
		public jlib.DataFramework.BaseColumn<string> PostData { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_page_views] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_page_views] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_page_views");
		}
		
		public static void DeleteByPks(List<long> PKs)
		{
			DeleteByPks<long>("d_page_views", "ID", PKs);
		}
		
		public static QueryPackage GetByField(long? ID = null, int? UserID = null, string Referrer = null, string Ip = null, DateTime? PostedDate = null, string Url = null, bool? IsNew = null, string PageTitle = null, long? ItemID = null, string SessionID = null, string UserAgent = null, int? LoadTime = null, string PostData = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(UserID != null)
			{
				query.Parameters.UserID = UserID;
				filterConditions.Add(@"[user_id] = @UserID");
			}	
			if(Referrer != null)
			{
				query.Parameters.Referrer = Referrer;
				filterConditions.Add(@"[referrer] = @Referrer");
			}	
			if(Ip != null)
			{
				query.Parameters.Ip = Ip;
				filterConditions.Add(@"[ip] = @Ip");
			}	
			if(PostedDate != null)
			{
				query.Parameters.PostedDate = PostedDate;
				filterConditions.Add(@"[posted_date] = @PostedDate");
			}	
			if(Url != null)
			{
				query.Parameters.Url = Url;
				filterConditions.Add(@"[url] = @Url");
			}	
			if(IsNew != null)
			{
				query.Parameters.IsNew = IsNew;
				filterConditions.Add(@"[is_new] = @IsNew");
			}	
			if(PageTitle != null)
			{
				query.Parameters.PageTitle = PageTitle;
				filterConditions.Add(@"[page_title] = @PageTitle");
			}	
			if(ItemID != null)
			{
				query.Parameters.ItemID = ItemID;
				filterConditions.Add(@"[item_id] = @ItemID");
			}	
			if(SessionID != null)
			{
				query.Parameters.SessionID = SessionID;
				filterConditions.Add(@"[session_id] = @SessionID");
			}	
			if(UserAgent != null)
			{
				query.Parameters.UserAgent = UserAgent;
				filterConditions.Add(@"[user_agent] = @UserAgent");
			}	
			if(LoadTime != null)
			{
				query.Parameters.LoadTime = LoadTime;
				filterConditions.Add(@"[load_time] = @LoadTime");
			}	
			if(PostData != null)
			{
				query.Parameters.PostData = PostData;
				filterConditions.Add(@"[post_data] LIKE @PostData");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_page_views] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<PageViews<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new PageViews<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<PageViews<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new PageViews<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<long> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<long>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_page_views] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new PageViewsDataReader(this);
		}
		
		public List<DtoPageView> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class PageViewsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public PageViewsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoPageView
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual long ID { get; set; }
		
		/// <summary>
		///		UserID Property
		/// </summary>
		[DataMember]
		public virtual int UserID { get; set; }
		
		/// <summary>
		///		Referrer Property
		/// </summary>
		[DataMember]
		public virtual string Referrer { get; set; }
		
		/// <summary>
		///		Ip Property
		/// </summary>
		[DataMember]
		public virtual string Ip { get; set; }
		
		/// <summary>
		///		PostedDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime PostedDate { get; set; }
		
		/// <summary>
		///		Url Property
		/// </summary>
		[DataMember]
		public virtual string Url { get; set; }
		
		/// <summary>
		///		IsNew Property
		/// </summary>
		[DataMember]
		public virtual bool IsNew { get; set; }
		
		/// <summary>
		///		PageTitle Property
		/// </summary>
		[DataMember]
		public virtual string PageTitle { get; set; }
		
		/// <summary>
		///		ItemID Property
		/// </summary>
		[DataMember]
		public virtual long ItemID { get; set; }
		
		/// <summary>
		///		SessionID Property
		/// </summary>
		[DataMember]
		public virtual string SessionID { get; set; }
		
		/// <summary>
		///		UserAgent Property
		/// </summary>
		[DataMember]
		public virtual string UserAgent { get; set; }
		
		/// <summary>
		///		LoadTime Property
		/// </summary>
		[DataMember]
		public virtual int LoadTime { get; set; }
		
		/// <summary>
		///		PostData Property
		/// </summary>
		[DataMember]
		public virtual string PostData { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class PageView : PageView<PageView>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates PageView
		/// </summary>
		public PageView() : this(new Context()) {}
		
		/// <summary>
		/// Creates PageView
		/// </summary>
		public PageView(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for PageView
		/// </summary>
		protected PageView(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class PageView<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: PageView<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates PageView
		/// </summary>
		public PageView() : this(new Context()) {}
		
		/// <summary>
		/// Creates PageView
		/// </summary>
		public PageView(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_page_views";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<long>(this, PageViews.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserID" ,this._UserID = new jlib.DataFramework.BaseField<int>(this, PageViews.Columns.UserID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Referrer" ,this._Referrer = new jlib.DataFramework.BaseField<string>(this, PageViews.Columns.Referrer, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Ip" ,this._Ip = new jlib.DataFramework.BaseField<string>(this, PageViews.Columns.Ip, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostedDate" ,this._PostedDate = new jlib.DataFramework.BaseField<DateTime>(this, PageViews.Columns.PostedDate, InitialValue: DateTime.Now, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Url" ,this._Url = new jlib.DataFramework.BaseField<string>(this, PageViews.Columns.Url, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("IsNew" ,this._IsNew = new jlib.DataFramework.BaseField<bool>(this, PageViews.Columns.IsNew, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PageTitle" ,this._PageTitle = new jlib.DataFramework.BaseField<string>(this, PageViews.Columns.PageTitle, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ItemID" ,this._ItemID = new jlib.DataFramework.BaseField<long>(this, PageViews.Columns.ItemID, InitialValue: -1L, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("SessionID" ,this._SessionID = new jlib.DataFramework.BaseField<string>(this, PageViews.Columns.SessionID, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserAgent" ,this._UserAgent = new jlib.DataFramework.BaseField<string>(this, PageViews.Columns.UserAgent, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("LoadTime" ,this._LoadTime = new jlib.DataFramework.BaseField<int>(this, PageViews.Columns.LoadTime, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostData" ,this._PostData = new jlib.DataFramework.BaseField<string>(this, PageViews.Columns.PostData, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.PageViewEx();
		}
		
		/// <summary>
		/// Deserialization method for PageView
		/// </summary>
		protected PageView(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void PageViewEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_page_views", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual long ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<long> _ID { get; set; }

			
		/// <summary>
		///		UserID Property
		///		Database Field:		user_id;
		///		Database Parameter:	@UserID;
		///		Primary Key:		No;
		///		Foreign Key:		Yes;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_page_views", FieldName: "user_id", IsForeignKey=true)]
		public virtual int UserID
		{
			get 
			{ 
				return this._UserID.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

				//Set Foreign Key Object to Null
				this._User = null;
			}
		}
		protected jlib.DataFramework.BaseField<int> _UserID { get; set; }

		/// <summary>
		///		User Foreign Object Property
		///		Foreign Key Field:	user_id
		///		Primary Key Field:	ID
		/// </summary>
		[jlib.DataFramework.Field("d_page_views", "user_id")]
		public virtual BehindUser User
		{
			get
			{
				if(this._User == null )
				{
					this._User = BehindUsers.GetByField(ID:(int)this.UserID).Execute().FirstOrDefault();
				}
				
						
				return this._User;
			}
			
			set
			{
				this._User = value;	
			}
		}
		protected BehindUser _User { get; set; }
			
		/// <summary>
		///		Referrer Property
		///		Database Field:		referrer;
		///		Database Parameter:	@Referrer;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_page_views", FieldName: "referrer", IsNullable=true)]
		public virtual string Referrer
		{
			get 
			{ 
				return (string)this._Referrer.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Referrer.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Referrer.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Referrer { get; set; }

			
		/// <summary>
		///		Ip Property
		///		Database Field:		ip;
		///		Database Parameter:	@Ip;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_page_views", FieldName: "ip")]
		public virtual string Ip
		{
			get 
			{ 
				return this._Ip.Value; 
			}

			set 
			{ 
				//Set Value
				this._Ip.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Ip.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Ip { get; set; }

			
		/// <summary>
		///		PostedDate Property
		///		Database Field:		posted_date;
		///		Database Parameter:	@PostedDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_page_views", FieldName: "posted_date")]
		public virtual DateTime PostedDate
		{
			get 
			{ 
				return this._PostedDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._PostedDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _PostedDate { get; set; }

			
		/// <summary>
		///		Url Property
		///		Database Field:		url;
		///		Database Parameter:	@Url;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_page_views", FieldName: "url")]
		public virtual string Url
		{
			get 
			{ 
				return this._Url.Value; 
			}

			set 
			{ 
				//Set Value
				this._Url.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Url.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Url { get; set; }

			
		/// <summary>
		///		IsNew Property
		///		Database Field:		is_new;
		///		Database Parameter:	@IsNew;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_page_views", FieldName: "is_new")]
		public virtual bool IsNew
		{
			get 
			{ 
				return this._IsNew.Value; 
			}

			set 
			{ 
				//Set Value
				this._IsNew.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._IsNew.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _IsNew { get; set; }

			
		/// <summary>
		///		PageTitle Property
		///		Database Field:		page_title;
		///		Database Parameter:	@PageTitle;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_page_views", FieldName: "page_title", IsNullable=true)]
		public virtual string PageTitle
		{
			get 
			{ 
				return (string)this._PageTitle.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._PageTitle.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PageTitle.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _PageTitle { get; set; }

			
		/// <summary>
		///		ItemID Property
		///		Database Field:		item_id;
		///		Database Parameter:	@ItemID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_page_views", FieldName: "item_id")]
		public virtual long ItemID
		{
			get 
			{ 
				return this._ItemID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ItemID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ItemID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<long> _ItemID { get; set; }

			
		/// <summary>
		///		SessionID Property
		///		Database Field:		session_id;
		///		Database Parameter:	@SessionID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_page_views", FieldName: "session_id", IsNullable=true)]
		public virtual string SessionID
		{
			get 
			{ 
				return (string)this._SessionID.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._SessionID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._SessionID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _SessionID { get; set; }

			
		/// <summary>
		///		UserAgent Property
		///		Database Field:		user_agent;
		///		Database Parameter:	@UserAgent;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_page_views", FieldName: "user_agent", IsNullable=true)]
		public virtual string UserAgent
		{
			get 
			{ 
				return (string)this._UserAgent.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._UserAgent.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserAgent.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _UserAgent { get; set; }

			
		/// <summary>
		///		LoadTime Property
		///		Database Field:		load_time;
		///		Database Parameter:	@LoadTime;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_page_views", FieldName: "load_time")]
		public virtual int LoadTime
		{
			get 
			{ 
				return this._LoadTime.Value; 
			}

			set 
			{ 
				//Set Value
				this._LoadTime.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._LoadTime.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _LoadTime { get; set; }

			
		/// <summary>
		///		PostData Property
		///		Database Field:		post_data;
		///		Database Parameter:	@PostData;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_page_views", FieldName: "post_data", IsNullable=true)]
		public virtual string PostData
		{
			get 
			{ 
				return (string)this._PostData.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._PostData.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostData.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _PostData { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoPageView Dto)
		{
			//Set Values
			this._UserID.SetChangedValueIfDifferent(Dto.UserID);
			this._Referrer.SetChangedValueIfDifferent(Dto.Referrer);
			this._Ip.SetChangedValueIfDifferent(Dto.Ip);
			this._PostedDate.SetChangedValueIfDifferent(Dto.PostedDate);
			this._Url.SetChangedValueIfDifferent(Dto.Url);
			this._IsNew.SetChangedValueIfDifferent(Dto.IsNew);
			this._PageTitle.SetChangedValueIfDifferent(Dto.PageTitle);
			this._ItemID.SetChangedValueIfDifferent(Dto.ItemID);
			this._SessionID.SetChangedValueIfDifferent(Dto.SessionID);
			this._UserAgent.SetChangedValueIfDifferent(Dto.UserAgent);
			this._LoadTime.SetChangedValueIfDifferent(Dto.LoadTime);
			this._PostData.SetChangedValueIfDifferent(Dto.PostData);
		}
		public virtual DtoPageView ToDto()
		{
			return this.ToDto(new DtoPageView());
		}
		
		public virtual DtoPageView ToDto(DtoPageView dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.UserID = this.UserID;
			dto.Referrer = this.Referrer;
			dto.Ip = this.Ip;
			dto.PostedDate = this.PostedDate;
			dto.Url = this.Url;
			dto.IsNew = this.IsNew;
			dto.PageTitle = this.PageTitle;
			dto.ItemID = this.ItemID;
			dto.SessionID = this.SessionID;
			dto.UserAgent = this.UserAgent;
			dto.LoadTime = this.LoadTime;
			dto.PostData = this.PostData;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				//Create Internal Object 'user'
				case "user":
					this._User = new BehindUser();
					this._User.Import(Row, InternalObjectIdentifier);
					break;
				
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_page_views] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(long ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, long ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_page_views]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class PaymentAttempts : PaymentAttempts<PaymentAttempts, PaymentAttempt>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates PaymentAttempts
		/// </summary>
		public PaymentAttempts() : this(new Context()) {}
		
		/// <summary>
		/// Creates PaymentAttempts
		/// </summary>
		public PaymentAttempts(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class PaymentAttempts<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: PaymentAttempt<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CreateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CreateDate", PropertyName: "CreateDate", ParameterName: "@Createdate", PkParameterName: "@PK_Createdate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<decimal> Amount = new jlib.DataFramework.BaseColumn<decimal>(FieldName: "Amount", PropertyName: "Amount", ParameterName: "@Amount", PkParameterName: "@PK_Amount", DBType: DbType.Currency, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Curency = new jlib.DataFramework.BaseColumn<string>(FieldName: "Curency", PropertyName: "Curency", ParameterName: "@Curency", PkParameterName: "@PK_Curency", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> NetsTransactionID = new jlib.DataFramework.BaseColumn<string>(FieldName: "NetsTransactionID", PropertyName: "NetsTransactionID", ParameterName: "@Netstransactionid", PkParameterName: "@PK_Netstransactionid", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> UserEmail = new jlib.DataFramework.BaseColumn<string>(FieldName: "UserEmail", PropertyName: "UserEmail", ParameterName: "@Useremail", PkParameterName: "@PK_Useremail", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> UserName = new jlib.DataFramework.BaseColumn<string>(FieldName: "UserName", PropertyName: "UserName", ParameterName: "@Username", PkParameterName: "@PK_Username", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> ChageStatus = new jlib.DataFramework.BaseColumn<int>(FieldName: "ChageStatus", PropertyName: "ChageStatus", ParameterName: "@Chagestatus", PkParameterName: "@PK_Chagestatus", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates PaymentAttempts
		/// </summary>
		public PaymentAttempts() : this(new Context()) {}
		
		/// <summary>
		/// Creates PaymentAttempts
		/// </summary>
		public PaymentAttempts(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_payment_attempts";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = PaymentAttempts<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("CreateDate", this.CreateDate = PaymentAttempts<DataCollectionType, DataRecordType>.Columns.CreateDate);
			base.Columns.Add("Amount", this.Amount = PaymentAttempts<DataCollectionType, DataRecordType>.Columns.Amount);
			base.Columns.Add("Curency", this.Curency = PaymentAttempts<DataCollectionType, DataRecordType>.Columns.Curency);
			base.Columns.Add("NetsTransactionID", this.NetsTransactionID = PaymentAttempts<DataCollectionType, DataRecordType>.Columns.NetsTransactionID);
			base.Columns.Add("UserEmail", this.UserEmail = PaymentAttempts<DataCollectionType, DataRecordType>.Columns.UserEmail);
			base.Columns.Add("UserName", this.UserName = PaymentAttempts<DataCollectionType, DataRecordType>.Columns.UserName);
			base.Columns.Add("ChageStatus", this.ChageStatus = PaymentAttempts<DataCollectionType, DataRecordType>.Columns.ChageStatus);
			
			//Call the secondary constructor method
			this.PaymentAttemptsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void PaymentAttemptsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CreateDate { get; set; }
		public jlib.DataFramework.BaseColumn<decimal> Amount { get; set; }
		public jlib.DataFramework.BaseColumn<string> Curency { get; set; }
		public jlib.DataFramework.BaseColumn<string> NetsTransactionID { get; set; }
		public jlib.DataFramework.BaseColumn<string> UserEmail { get; set; }
		public jlib.DataFramework.BaseColumn<string> UserName { get; set; }
		public jlib.DataFramework.BaseColumn<int> ChageStatus { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_payment_attempts] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_payment_attempts] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_payment_attempts");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_payment_attempts", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, DateTime? CreateDate = null, decimal? Amount = null, string Curency = null, string NetsTransactionID = null, string UserEmail = null, string UserName = null, int? ChageStatus = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(CreateDate != null)
			{
				query.Parameters.CreateDate = CreateDate;
				filterConditions.Add(@"[CreateDate] = @Createdate");
			}	
			if(Amount != null)
			{
				query.Parameters.Amount = Amount;
				filterConditions.Add(@"[Amount] = @Amount");
			}	
			if(Curency != null)
			{
				query.Parameters.Curency = Curency;
				filterConditions.Add(@"[Curency] = @Curency");
			}	
			if(NetsTransactionID != null)
			{
				query.Parameters.NetsTransactionID = NetsTransactionID;
				filterConditions.Add(@"[NetsTransactionID] = @Netstransactionid");
			}	
			if(UserEmail != null)
			{
				query.Parameters.UserEmail = UserEmail;
				filterConditions.Add(@"[UserEmail] = @Useremail");
			}	
			if(UserName != null)
			{
				query.Parameters.UserName = UserName;
				filterConditions.Add(@"[UserName] = @Username");
			}	
			if(ChageStatus != null)
			{
				query.Parameters.ChageStatus = ChageStatus;
				filterConditions.Add(@"[ChageStatus] = @Chagestatus");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_payment_attempts] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<PaymentAttempts<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new PaymentAttempts<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<PaymentAttempts<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new PaymentAttempts<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_payment_attempts] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new PaymentAttemptsDataReader(this);
		}
		
		public List<DtoPaymentAttempt> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class PaymentAttemptsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public PaymentAttemptsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoPaymentAttempt
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		CreateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime CreateDate { get; set; }
		
		/// <summary>
		///		Amount Property
		/// </summary>
		[DataMember]
		public virtual decimal Amount { get; set; }
		
		/// <summary>
		///		Curency Property
		/// </summary>
		[DataMember]
		public virtual string Curency { get; set; }
		
		/// <summary>
		///		NetsTransactionID Property
		/// </summary>
		[DataMember]
		public virtual string NetsTransactionID { get; set; }
		
		/// <summary>
		///		UserEmail Property
		/// </summary>
		[DataMember]
		public virtual string UserEmail { get; set; }
		
		/// <summary>
		///		UserName Property
		/// </summary>
		[DataMember]
		public virtual string UserName { get; set; }
		
		/// <summary>
		///		ChageStatus Property
		/// </summary>
		[DataMember]
		public virtual int ChageStatus { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class PaymentAttempt : PaymentAttempt<PaymentAttempt>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates PaymentAttempt
		/// </summary>
		public PaymentAttempt() : this(new Context()) {}
		
		/// <summary>
		/// Creates PaymentAttempt
		/// </summary>
		public PaymentAttempt(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for PaymentAttempt
		/// </summary>
		protected PaymentAttempt(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class PaymentAttempt<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: PaymentAttempt<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates PaymentAttempt
		/// </summary>
		public PaymentAttempt() : this(new Context()) {}
		
		/// <summary>
		/// Creates PaymentAttempt
		/// </summary>
		public PaymentAttempt(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_payment_attempts";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, PaymentAttempts.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreateDate" ,this._CreateDate = new jlib.DataFramework.BaseField<DateTime>(this, PaymentAttempts.Columns.CreateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Amount" ,this._Amount = new jlib.DataFramework.BaseField<decimal>(this, PaymentAttempts.Columns.Amount, InitialValue: 0.00M, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Curency" ,this._Curency = new jlib.DataFramework.BaseField<string>(this, PaymentAttempts.Columns.Curency, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("NetsTransactionID" ,this._NetsTransactionID = new jlib.DataFramework.BaseField<string>(this, PaymentAttempts.Columns.NetsTransactionID, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserEmail" ,this._UserEmail = new jlib.DataFramework.BaseField<string>(this, PaymentAttempts.Columns.UserEmail, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserName" ,this._UserName = new jlib.DataFramework.BaseField<string>(this, PaymentAttempts.Columns.UserName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ChageStatus" ,this._ChageStatus = new jlib.DataFramework.BaseField<int>(this, PaymentAttempts.Columns.ChageStatus, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.PaymentAttemptEx();
		}
		
		/// <summary>
		/// Deserialization method for PaymentAttempt
		/// </summary>
		protected PaymentAttempt(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void PaymentAttemptEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payment_attempts", FieldName: "ID", IsPrimaryKey=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		CreateDate Property
		///		Database Field:		CreateDate;
		///		Database Parameter:	@Createdate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payment_attempts", FieldName: "CreateDate")]
		public virtual DateTime CreateDate
		{
			get 
			{ 
				return this._CreateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CreateDate { get; set; }

			
		/// <summary>
		///		Amount Property
		///		Database Field:		Amount;
		///		Database Parameter:	@Amount;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payment_attempts", FieldName: "Amount")]
		public virtual decimal Amount
		{
			get 
			{ 
				return this._Amount.Value; 
			}

			set 
			{ 
				//Set Value
				this._Amount.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Amount.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<decimal> _Amount { get; set; }

			
		/// <summary>
		///		Curency Property
		///		Database Field:		Curency;
		///		Database Parameter:	@Curency;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payment_attempts", FieldName: "Curency")]
		public virtual string Curency
		{
			get 
			{ 
				return this._Curency.Value; 
			}

			set 
			{ 
				//Set Value
				this._Curency.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Curency.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Curency { get; set; }

			
		/// <summary>
		///		NetsTransactionID Property
		///		Database Field:		NetsTransactionID;
		///		Database Parameter:	@Netstransactionid;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payment_attempts", FieldName: "NetsTransactionID", IsNullable=true)]
		public virtual string NetsTransactionID
		{
			get 
			{ 
				return (string)this._NetsTransactionID.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._NetsTransactionID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._NetsTransactionID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _NetsTransactionID { get; set; }

			
		/// <summary>
		///		UserEmail Property
		///		Database Field:		UserEmail;
		///		Database Parameter:	@Useremail;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payment_attempts", FieldName: "UserEmail")]
		public virtual string UserEmail
		{
			get 
			{ 
				return this._UserEmail.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserEmail.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserEmail.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _UserEmail { get; set; }

			
		/// <summary>
		///		UserName Property
		///		Database Field:		UserName;
		///		Database Parameter:	@Username;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payment_attempts", FieldName: "UserName")]
		public virtual string UserName
		{
			get 
			{ 
				return this._UserName.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserName.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserName.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _UserName { get; set; }

			
		/// <summary>
		///		ChageStatus Property
		///		Database Field:		ChageStatus;
		///		Database Parameter:	@Chagestatus;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payment_attempts", FieldName: "ChageStatus")]
		public virtual int ChageStatus
		{
			get 
			{ 
				return this._ChageStatus.Value; 
			}

			set 
			{ 
				//Set Value
				this._ChageStatus.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ChageStatus.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ChageStatus { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoPaymentAttempt Dto)
		{
			//Set Values
			this._ID.SetChangedValueIfDifferent(Dto.ID);
			this._CreateDate.SetChangedValueIfDifferent(Dto.CreateDate);
			this._Amount.SetChangedValueIfDifferent(Dto.Amount);
			this._Curency.SetChangedValueIfDifferent(Dto.Curency);
			this._NetsTransactionID.SetChangedValueIfDifferent(Dto.NetsTransactionID);
			this._UserEmail.SetChangedValueIfDifferent(Dto.UserEmail);
			this._UserName.SetChangedValueIfDifferent(Dto.UserName);
			this._ChageStatus.SetChangedValueIfDifferent(Dto.ChageStatus);
		}
		public virtual DtoPaymentAttempt ToDto()
		{
			return this.ToDto(new DtoPaymentAttempt());
		}
		
		public virtual DtoPaymentAttempt ToDto(DtoPaymentAttempt dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.CreateDate = this.CreateDate;
			dto.Amount = this.Amount;
			dto.Curency = this.Curency;
			dto.NetsTransactionID = this.NetsTransactionID;
			dto.UserEmail = this.UserEmail;
			dto.UserName = this.UserName;
			dto.ChageStatus = this.ChageStatus;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_payment_attempts] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_payment_attempts]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class Payments : Payments<Payments, Payment>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Payments
		/// </summary>
		public Payments() : this(new Context()) {}
		
		/// <summary>
		/// Creates Payments
		/// </summary>
		public Payments(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Payments<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: Payment<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CreateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CreateDate", PropertyName: "CreateDate", ParameterName: "@Createdate", PkParameterName: "@PK_Createdate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> UserID = new jlib.DataFramework.BaseColumn<int>(FieldName: "UserID", PropertyName: "UserID", ParameterName: "@Userid", PkParameterName: "@PK_Userid", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> CustomerID = new jlib.DataFramework.BaseColumn<int>(FieldName: "CustomerID", PropertyName: "CustomerID", ParameterName: "@Customerid", PkParameterName: "@PK_Customerid", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> PaymentMethodID = new jlib.DataFramework.BaseColumn<int>(FieldName: "PaymentMethodID", PropertyName: "PaymentMethodID", ParameterName: "@Paymentmethodid", PkParameterName: "@PK_Paymentmethodid", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> IsTest = new jlib.DataFramework.BaseColumn<bool>(FieldName: "IsTest", PropertyName: "IsTest", ParameterName: "@Istest", PkParameterName: "@PK_Istest", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> Success = new jlib.DataFramework.BaseColumn<bool>(FieldName: "Success", PropertyName: "Success", ParameterName: "@Success", PkParameterName: "@PK_Success", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> UserEmail = new jlib.DataFramework.BaseColumn<string>(FieldName: "UserEmail", PropertyName: "UserEmail", ParameterName: "@Useremail", PkParameterName: "@PK_Useremail", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> UserFirstName = new jlib.DataFramework.BaseColumn<string>(FieldName: "UserFirstName", PropertyName: "UserFirstName", ParameterName: "@Userfirstname", PkParameterName: "@PK_Userfirstname", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> UserLastName = new jlib.DataFramework.BaseColumn<string>(FieldName: "UserLastName", PropertyName: "UserLastName", ParameterName: "@Userlastname", PkParameterName: "@PK_Userlastname", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> UserPhone = new jlib.DataFramework.BaseColumn<string>(FieldName: "UserPhone", PropertyName: "UserPhone", ParameterName: "@Userphone", PkParameterName: "@PK_Userphone", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Company = new jlib.DataFramework.BaseColumn<string>(FieldName: "Company", PropertyName: "Company", ParameterName: "@Company", PkParameterName: "@PK_Company", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> AddressStreet = new jlib.DataFramework.BaseColumn<string>(FieldName: "AddressStreet", PropertyName: "AddressStreet", ParameterName: "@Addressstreet", PkParameterName: "@PK_Addressstreet", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> AddressZip = new jlib.DataFramework.BaseColumn<string>(FieldName: "AddressZip", PropertyName: "AddressZip", ParameterName: "@Addresszip", PkParameterName: "@PK_Addresszip", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> AddressCity = new jlib.DataFramework.BaseColumn<string>(FieldName: "AddressCity", PropertyName: "AddressCity", ParameterName: "@Addresscity", PkParameterName: "@PK_Addresscity", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> AddressCountry = new jlib.DataFramework.BaseColumn<string>(FieldName: "AddressCountry", PropertyName: "AddressCountry", ParameterName: "@Addresscountry", PkParameterName: "@PK_Addresscountry", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> InvoiceReference = new jlib.DataFramework.BaseColumn<string>(FieldName: "InvoiceReference", PropertyName: "InvoiceReference", ParameterName: "@Invoicereference", PkParameterName: "@PK_Invoicereference", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> TransactionID = new jlib.DataFramework.BaseColumn<string>(FieldName: "TransactionID", PropertyName: "TransactionID", ParameterName: "@Transactionid", PkParameterName: "@PK_Transactionid", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> QueryString = new jlib.DataFramework.BaseColumn<string>(FieldName: "QueryString", PropertyName: "QueryString", ParameterName: "@Querystring", PkParameterName: "@PK_Querystring", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> OrderDescription = new jlib.DataFramework.BaseColumn<string>(FieldName: "OrderDescription", PropertyName: "OrderDescription", ParameterName: "@Orderdescription", PkParameterName: "@PK_Orderdescription", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<decimal> OrderAmount = new jlib.DataFramework.BaseColumn<decimal>(FieldName: "OrderAmount", PropertyName: "OrderAmount", ParameterName: "@Orderamount", PkParameterName: "@PK_Orderamount", DBType: DbType.Currency, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> OrderCurrency = new jlib.DataFramework.BaseColumn<string>(FieldName: "OrderCurrency", PropertyName: "OrderCurrency", ParameterName: "@Ordercurrency", PkParameterName: "@PK_Ordercurrency", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> CCResponseCode = new jlib.DataFramework.BaseColumn<string>(FieldName: "CCResponseCode", PropertyName: "CCResponseCode", ParameterName: "@Ccresponsecode", PkParameterName: "@PK_Ccresponsecode", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> CCResponseSource = new jlib.DataFramework.BaseColumn<string>(FieldName: "CCResponseSource", PropertyName: "CCResponseSource", ParameterName: "@Ccresponsesource", PkParameterName: "@PK_Ccresponsesource", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> CCResponseText = new jlib.DataFramework.BaseColumn<string>(FieldName: "CCResponseText", PropertyName: "CCResponseText", ParameterName: "@Ccresponsetext", PkParameterName: "@PK_Ccresponsetext", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> CCAuthorizationID = new jlib.DataFramework.BaseColumn<string>(FieldName: "CCAuthorizationID", PropertyName: "CCAuthorizationID", ParameterName: "@Ccauthorizationid", PkParameterName: "@PK_Ccauthorizationid", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CCExecutionTime = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CCExecutionTime", PropertyName: "CCExecutionTime", ParameterName: "@Ccexecutiontime", PkParameterName: "@PK_Ccexecutiontime", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> GoogleCookieID = new jlib.DataFramework.BaseColumn<string>(FieldName: "GoogleCookieID", PropertyName: "GoogleCookieID", ParameterName: "@Googlecookieid", PkParameterName: "@PK_Googlecookieid", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> SiteCode = new jlib.DataFramework.BaseColumn<string>(FieldName: "SiteCode", PropertyName: "SiteCode", ParameterName: "@Sitecode", PkParameterName: "@PK_Sitecode", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Payments
		/// </summary>
		public Payments() : this(new Context()) {}
		
		/// <summary>
		/// Creates Payments
		/// </summary>
		public Payments(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_payments";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = Payments<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("CreateDate", this.CreateDate = Payments<DataCollectionType, DataRecordType>.Columns.CreateDate);
			base.Columns.Add("UserID", this.UserID = Payments<DataCollectionType, DataRecordType>.Columns.UserID);
			base.Columns.Add("CustomerID", this.CustomerID = Payments<DataCollectionType, DataRecordType>.Columns.CustomerID);
			base.Columns.Add("PaymentMethodID", this.PaymentMethodID = Payments<DataCollectionType, DataRecordType>.Columns.PaymentMethodID);
			base.Columns.Add("IsTest", this.IsTest = Payments<DataCollectionType, DataRecordType>.Columns.IsTest);
			base.Columns.Add("Success", this.Success = Payments<DataCollectionType, DataRecordType>.Columns.Success);
			base.Columns.Add("UserEmail", this.UserEmail = Payments<DataCollectionType, DataRecordType>.Columns.UserEmail);
			base.Columns.Add("UserFirstName", this.UserFirstName = Payments<DataCollectionType, DataRecordType>.Columns.UserFirstName);
			base.Columns.Add("UserLastName", this.UserLastName = Payments<DataCollectionType, DataRecordType>.Columns.UserLastName);
			base.Columns.Add("UserPhone", this.UserPhone = Payments<DataCollectionType, DataRecordType>.Columns.UserPhone);
			base.Columns.Add("Company", this.Company = Payments<DataCollectionType, DataRecordType>.Columns.Company);
			base.Columns.Add("AddressStreet", this.AddressStreet = Payments<DataCollectionType, DataRecordType>.Columns.AddressStreet);
			base.Columns.Add("AddressZip", this.AddressZip = Payments<DataCollectionType, DataRecordType>.Columns.AddressZip);
			base.Columns.Add("AddressCity", this.AddressCity = Payments<DataCollectionType, DataRecordType>.Columns.AddressCity);
			base.Columns.Add("AddressCountry", this.AddressCountry = Payments<DataCollectionType, DataRecordType>.Columns.AddressCountry);
			base.Columns.Add("InvoiceReference", this.InvoiceReference = Payments<DataCollectionType, DataRecordType>.Columns.InvoiceReference);
			base.Columns.Add("TransactionID", this.TransactionID = Payments<DataCollectionType, DataRecordType>.Columns.TransactionID);
			base.Columns.Add("QueryString", this.QueryString = Payments<DataCollectionType, DataRecordType>.Columns.QueryString);
			base.Columns.Add("OrderDescription", this.OrderDescription = Payments<DataCollectionType, DataRecordType>.Columns.OrderDescription);
			base.Columns.Add("OrderAmount", this.OrderAmount = Payments<DataCollectionType, DataRecordType>.Columns.OrderAmount);
			base.Columns.Add("OrderCurrency", this.OrderCurrency = Payments<DataCollectionType, DataRecordType>.Columns.OrderCurrency);
			base.Columns.Add("CCResponseCode", this.CCResponseCode = Payments<DataCollectionType, DataRecordType>.Columns.CCResponseCode);
			base.Columns.Add("CCResponseSource", this.CCResponseSource = Payments<DataCollectionType, DataRecordType>.Columns.CCResponseSource);
			base.Columns.Add("CCResponseText", this.CCResponseText = Payments<DataCollectionType, DataRecordType>.Columns.CCResponseText);
			base.Columns.Add("CCAuthorizationID", this.CCAuthorizationID = Payments<DataCollectionType, DataRecordType>.Columns.CCAuthorizationID);
			base.Columns.Add("CCExecutionTime", this.CCExecutionTime = Payments<DataCollectionType, DataRecordType>.Columns.CCExecutionTime);
			base.Columns.Add("GoogleCookieID", this.GoogleCookieID = Payments<DataCollectionType, DataRecordType>.Columns.GoogleCookieID);
			base.Columns.Add("SiteCode", this.SiteCode = Payments<DataCollectionType, DataRecordType>.Columns.SiteCode);
			
			//Call the secondary constructor method
			this.PaymentsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void PaymentsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CreateDate { get; set; }
		public jlib.DataFramework.BaseColumn<int> UserID { get; set; }
		public jlib.DataFramework.BaseColumn<int> CustomerID { get; set; }
		public jlib.DataFramework.BaseColumn<int> PaymentMethodID { get; set; }
		public jlib.DataFramework.BaseColumn<bool> IsTest { get; set; }
		public jlib.DataFramework.BaseColumn<bool> Success { get; set; }
		public jlib.DataFramework.BaseColumn<string> UserEmail { get; set; }
		public jlib.DataFramework.BaseColumn<string> UserFirstName { get; set; }
		public jlib.DataFramework.BaseColumn<string> UserLastName { get; set; }
		public jlib.DataFramework.BaseColumn<string> UserPhone { get; set; }
		public jlib.DataFramework.BaseColumn<string> Company { get; set; }
		public jlib.DataFramework.BaseColumn<string> AddressStreet { get; set; }
		public jlib.DataFramework.BaseColumn<string> AddressZip { get; set; }
		public jlib.DataFramework.BaseColumn<string> AddressCity { get; set; }
		public jlib.DataFramework.BaseColumn<string> AddressCountry { get; set; }
		public jlib.DataFramework.BaseColumn<string> InvoiceReference { get; set; }
		public jlib.DataFramework.BaseColumn<string> TransactionID { get; set; }
		public jlib.DataFramework.BaseColumn<string> QueryString { get; set; }
		public jlib.DataFramework.BaseColumn<string> OrderDescription { get; set; }
		public jlib.DataFramework.BaseColumn<decimal> OrderAmount { get; set; }
		public jlib.DataFramework.BaseColumn<string> OrderCurrency { get; set; }
		public jlib.DataFramework.BaseColumn<string> CCResponseCode { get; set; }
		public jlib.DataFramework.BaseColumn<string> CCResponseSource { get; set; }
		public jlib.DataFramework.BaseColumn<string> CCResponseText { get; set; }
		public jlib.DataFramework.BaseColumn<string> CCAuthorizationID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CCExecutionTime { get; set; }
		public jlib.DataFramework.BaseColumn<string> GoogleCookieID { get; set; }
		public jlib.DataFramework.BaseColumn<string> SiteCode { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_payments] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_payments] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_payments");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_payments", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, DateTime? CreateDate = null, int? UserID = null, int? CustomerID = null, int? PaymentMethodID = null, bool? IsTest = null, bool? Success = null, string UserEmail = null, string UserFirstName = null, string UserLastName = null, string UserPhone = null, string Company = null, string AddressStreet = null, string AddressZip = null, string AddressCity = null, string AddressCountry = null, string InvoiceReference = null, string TransactionID = null, string QueryString = null, string OrderDescription = null, decimal? OrderAmount = null, string OrderCurrency = null, string CCResponseCode = null, string CCResponseSource = null, string CCResponseText = null, string CCAuthorizationID = null, DateTime? CCExecutionTime = null, string GoogleCookieID = null, string SiteCode = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(CreateDate != null)
			{
				query.Parameters.CreateDate = CreateDate;
				filterConditions.Add(@"[CreateDate] = @Createdate");
			}	
			if(UserID != null)
			{
				query.Parameters.UserID = UserID;
				filterConditions.Add(@"[UserID] = @Userid");
			}	
			if(CustomerID != null)
			{
				query.Parameters.CustomerID = CustomerID;
				filterConditions.Add(@"[CustomerID] = @Customerid");
			}	
			if(PaymentMethodID != null)
			{
				query.Parameters.PaymentMethodID = PaymentMethodID;
				filterConditions.Add(@"[PaymentMethodID] = @Paymentmethodid");
			}	
			if(IsTest != null)
			{
				query.Parameters.IsTest = IsTest;
				filterConditions.Add(@"[IsTest] = @Istest");
			}	
			if(Success != null)
			{
				query.Parameters.Success = Success;
				filterConditions.Add(@"[Success] = @Success");
			}	
			if(UserEmail != null)
			{
				query.Parameters.UserEmail = UserEmail;
				filterConditions.Add(@"[UserEmail] = @Useremail");
			}	
			if(UserFirstName != null)
			{
				query.Parameters.UserFirstName = UserFirstName;
				filterConditions.Add(@"[UserFirstName] = @Userfirstname");
			}	
			if(UserLastName != null)
			{
				query.Parameters.UserLastName = UserLastName;
				filterConditions.Add(@"[UserLastName] = @Userlastname");
			}	
			if(UserPhone != null)
			{
				query.Parameters.UserPhone = UserPhone;
				filterConditions.Add(@"[UserPhone] = @Userphone");
			}	
			if(Company != null)
			{
				query.Parameters.Company = Company;
				filterConditions.Add(@"[Company] = @Company");
			}	
			if(AddressStreet != null)
			{
				query.Parameters.AddressStreet = AddressStreet;
				filterConditions.Add(@"[AddressStreet] = @Addressstreet");
			}	
			if(AddressZip != null)
			{
				query.Parameters.AddressZip = AddressZip;
				filterConditions.Add(@"[AddressZip] = @Addresszip");
			}	
			if(AddressCity != null)
			{
				query.Parameters.AddressCity = AddressCity;
				filterConditions.Add(@"[AddressCity] = @Addresscity");
			}	
			if(AddressCountry != null)
			{
				query.Parameters.AddressCountry = AddressCountry;
				filterConditions.Add(@"[AddressCountry] = @Addresscountry");
			}	
			if(InvoiceReference != null)
			{
				query.Parameters.InvoiceReference = InvoiceReference;
				filterConditions.Add(@"[InvoiceReference] = @Invoicereference");
			}	
			if(TransactionID != null)
			{
				query.Parameters.TransactionID = TransactionID;
				filterConditions.Add(@"[TransactionID] = @Transactionid");
			}	
			if(QueryString != null)
			{
				query.Parameters.QueryString = QueryString;
				filterConditions.Add(@"[QueryString] = @Querystring");
			}	
			if(OrderDescription != null)
			{
				query.Parameters.OrderDescription = OrderDescription;
				filterConditions.Add(@"[OrderDescription] = @Orderdescription");
			}	
			if(OrderAmount != null)
			{
				query.Parameters.OrderAmount = OrderAmount;
				filterConditions.Add(@"[OrderAmount] = @Orderamount");
			}	
			if(OrderCurrency != null)
			{
				query.Parameters.OrderCurrency = OrderCurrency;
				filterConditions.Add(@"[OrderCurrency] = @Ordercurrency");
			}	
			if(CCResponseCode != null)
			{
				query.Parameters.CCResponseCode = CCResponseCode;
				filterConditions.Add(@"[CCResponseCode] = @Ccresponsecode");
			}	
			if(CCResponseSource != null)
			{
				query.Parameters.CCResponseSource = CCResponseSource;
				filterConditions.Add(@"[CCResponseSource] = @Ccresponsesource");
			}	
			if(CCResponseText != null)
			{
				query.Parameters.CCResponseText = CCResponseText;
				filterConditions.Add(@"[CCResponseText] = @Ccresponsetext");
			}	
			if(CCAuthorizationID != null)
			{
				query.Parameters.CCAuthorizationID = CCAuthorizationID;
				filterConditions.Add(@"[CCAuthorizationID] = @Ccauthorizationid");
			}	
			if(CCExecutionTime != null)
			{
				query.Parameters.CCExecutionTime = CCExecutionTime;
				filterConditions.Add(@"[CCExecutionTime] = @Ccexecutiontime");
			}	
			if(GoogleCookieID != null)
			{
				query.Parameters.GoogleCookieID = GoogleCookieID;
				filterConditions.Add(@"[GoogleCookieID] = @Googlecookieid");
			}	
			if(SiteCode != null)
			{
				query.Parameters.SiteCode = SiteCode;
				filterConditions.Add(@"[SiteCode] = @Sitecode");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_payments] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Payments<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Payments<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Payments<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Payments<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_payments] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new PaymentsDataReader(this);
		}
		
		public List<DtoPayment> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class PaymentsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public PaymentsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoPayment
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		CreateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime CreateDate { get; set; }
		
		/// <summary>
		///		UserID Property
		/// </summary>
		[DataMember]
		public virtual int UserID { get; set; }
		
		/// <summary>
		///		CustomerID Property
		/// </summary>
		[DataMember]
		public virtual int CustomerID { get; set; }
		
		/// <summary>
		///		PaymentMethodID Property
		/// </summary>
		[DataMember]
		public virtual int PaymentMethodID { get; set; }
		
		/// <summary>
		///		IsTest Property
		/// </summary>
		[DataMember]
		public virtual bool IsTest { get; set; }
		
		/// <summary>
		///		Success Property
		/// </summary>
		[DataMember]
		public virtual bool Success { get; set; }
		
		/// <summary>
		///		UserEmail Property
		/// </summary>
		[DataMember]
		public virtual string UserEmail { get; set; }
		
		/// <summary>
		///		UserFirstName Property
		/// </summary>
		[DataMember]
		public virtual string UserFirstName { get; set; }
		
		/// <summary>
		///		UserLastName Property
		/// </summary>
		[DataMember]
		public virtual string UserLastName { get; set; }
		
		/// <summary>
		///		UserPhone Property
		/// </summary>
		[DataMember]
		public virtual string UserPhone { get; set; }
		
		/// <summary>
		///		Company Property
		/// </summary>
		[DataMember]
		public virtual string Company { get; set; }
		
		/// <summary>
		///		AddressStreet Property
		/// </summary>
		[DataMember]
		public virtual string AddressStreet { get; set; }
		
		/// <summary>
		///		AddressZip Property
		/// </summary>
		[DataMember]
		public virtual string AddressZip { get; set; }
		
		/// <summary>
		///		AddressCity Property
		/// </summary>
		[DataMember]
		public virtual string AddressCity { get; set; }
		
		/// <summary>
		///		AddressCountry Property
		/// </summary>
		[DataMember]
		public virtual string AddressCountry { get; set; }
		
		/// <summary>
		///		InvoiceReference Property
		/// </summary>
		[DataMember]
		public virtual string InvoiceReference { get; set; }
		
		/// <summary>
		///		TransactionID Property
		/// </summary>
		[DataMember]
		public virtual string TransactionID { get; set; }
		
		/// <summary>
		///		QueryString Property
		/// </summary>
		[DataMember]
		public virtual string QueryString { get; set; }
		
		/// <summary>
		///		OrderDescription Property
		/// </summary>
		[DataMember]
		public virtual string OrderDescription { get; set; }
		
		/// <summary>
		///		OrderAmount Property
		/// </summary>
		[DataMember]
		public virtual decimal OrderAmount { get; set; }
		
		/// <summary>
		///		OrderCurrency Property
		/// </summary>
		[DataMember]
		public virtual string OrderCurrency { get; set; }
		
		/// <summary>
		///		CCResponseCode Property
		/// </summary>
		[DataMember]
		public virtual string CCResponseCode { get; set; }
		
		/// <summary>
		///		CCResponseSource Property
		/// </summary>
		[DataMember]
		public virtual string CCResponseSource { get; set; }
		
		/// <summary>
		///		CCResponseText Property
		/// </summary>
		[DataMember]
		public virtual string CCResponseText { get; set; }
		
		/// <summary>
		///		CCAuthorizationID Property
		/// </summary>
		[DataMember]
		public virtual string CCAuthorizationID { get; set; }
		
		/// <summary>
		///		CCExecutionTime Property
		/// </summary>
		[DataMember]
		public virtual DateTime? CCExecutionTime { get; set; }
		
		/// <summary>
		///		GoogleCookieID Property
		/// </summary>
		[DataMember]
		public virtual string GoogleCookieID { get; set; }
		
		/// <summary>
		///		SiteCode Property
		/// </summary>
		[DataMember]
		public virtual string SiteCode { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class Payment : Payment<Payment>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Payment
		/// </summary>
		public Payment() : this(new Context()) {}
		
		/// <summary>
		/// Creates Payment
		/// </summary>
		public Payment(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for Payment
		/// </summary>
		protected Payment(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class Payment<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: Payment<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Payment
		/// </summary>
		public Payment() : this(new Context()) {}
		
		/// <summary>
		/// Creates Payment
		/// </summary>
		public Payment(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_payments";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, Payments.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreateDate" ,this._CreateDate = new jlib.DataFramework.BaseField<DateTime>(this, Payments.Columns.CreateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserID" ,this._UserID = new jlib.DataFramework.BaseField<int>(this, Payments.Columns.UserID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CustomerID" ,this._CustomerID = new jlib.DataFramework.BaseField<int>(this, Payments.Columns.CustomerID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PaymentMethodID" ,this._PaymentMethodID = new jlib.DataFramework.BaseField<int>(this, Payments.Columns.PaymentMethodID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("IsTest" ,this._IsTest = new jlib.DataFramework.BaseField<bool>(this, Payments.Columns.IsTest, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Success" ,this._Success = new jlib.DataFramework.BaseField<bool>(this, Payments.Columns.Success, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserEmail" ,this._UserEmail = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.UserEmail, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserFirstName" ,this._UserFirstName = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.UserFirstName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserLastName" ,this._UserLastName = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.UserLastName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserPhone" ,this._UserPhone = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.UserPhone, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Company" ,this._Company = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.Company, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("AddressStreet" ,this._AddressStreet = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.AddressStreet, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("AddressZip" ,this._AddressZip = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.AddressZip, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("AddressCity" ,this._AddressCity = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.AddressCity, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("AddressCountry" ,this._AddressCountry = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.AddressCountry, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("InvoiceReference" ,this._InvoiceReference = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.InvoiceReference, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("TransactionID" ,this._TransactionID = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.TransactionID, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("QueryString" ,this._QueryString = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.QueryString, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("OrderDescription" ,this._OrderDescription = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.OrderDescription, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("OrderAmount" ,this._OrderAmount = new jlib.DataFramework.BaseField<decimal>(this, Payments.Columns.OrderAmount, InitialValue: 0.00M, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("OrderCurrency" ,this._OrderCurrency = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.OrderCurrency, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CCResponseCode" ,this._CCResponseCode = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.CCResponseCode, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CCResponseSource" ,this._CCResponseSource = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.CCResponseSource, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CCResponseText" ,this._CCResponseText = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.CCResponseText, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CCAuthorizationID" ,this._CCAuthorizationID = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.CCAuthorizationID, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CCExecutionTime" ,this._CCExecutionTime = new jlib.DataFramework.BaseField<DateTime>(this, Payments.Columns.CCExecutionTime, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("GoogleCookieID" ,this._GoogleCookieID = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.GoogleCookieID, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("SiteCode" ,this._SiteCode = new jlib.DataFramework.BaseField<string>(this, Payments.Columns.SiteCode, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.PaymentEx();
		}
		
		/// <summary>
		/// Deserialization method for Payment
		/// </summary>
		protected Payment(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void PaymentEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		CreateDate Property
		///		Database Field:		CreateDate;
		///		Database Parameter:	@Createdate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "CreateDate")]
		public virtual DateTime CreateDate
		{
			get 
			{ 
				return this._CreateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CreateDate { get; set; }

			
		/// <summary>
		///		UserID Property
		///		Database Field:		UserID;
		///		Database Parameter:	@Userid;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "UserID")]
		public virtual int UserID
		{
			get 
			{ 
				return this._UserID.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _UserID { get; set; }

			
		/// <summary>
		///		CustomerID Property
		///		Database Field:		CustomerID;
		///		Database Parameter:	@Customerid;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "CustomerID")]
		public virtual int CustomerID
		{
			get 
			{ 
				return this._CustomerID.Value; 
			}

			set 
			{ 
				//Set Value
				this._CustomerID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CustomerID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _CustomerID { get; set; }

			
		/// <summary>
		///		PaymentMethodID Property
		///		Database Field:		PaymentMethodID;
		///		Database Parameter:	@Paymentmethodid;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "PaymentMethodID")]
		public virtual int PaymentMethodID
		{
			get 
			{ 
				return this._PaymentMethodID.Value; 
			}

			set 
			{ 
				//Set Value
				this._PaymentMethodID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PaymentMethodID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _PaymentMethodID { get; set; }

			
		/// <summary>
		///		IsTest Property
		///		Database Field:		IsTest;
		///		Database Parameter:	@Istest;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "IsTest")]
		public virtual bool IsTest
		{
			get 
			{ 
				return this._IsTest.Value; 
			}

			set 
			{ 
				//Set Value
				this._IsTest.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._IsTest.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _IsTest { get; set; }

			
		/// <summary>
		///		Success Property
		///		Database Field:		Success;
		///		Database Parameter:	@Success;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "Success")]
		public virtual bool Success
		{
			get 
			{ 
				return this._Success.Value; 
			}

			set 
			{ 
				//Set Value
				this._Success.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Success.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _Success { get; set; }

			
		/// <summary>
		///		UserEmail Property
		///		Database Field:		UserEmail;
		///		Database Parameter:	@Useremail;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "UserEmail", IsNullable=true)]
		public virtual string UserEmail
		{
			get 
			{ 
				return (string)this._UserEmail.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._UserEmail.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserEmail.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _UserEmail { get; set; }

			
		/// <summary>
		///		UserFirstName Property
		///		Database Field:		UserFirstName;
		///		Database Parameter:	@Userfirstname;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "UserFirstName", IsNullable=true)]
		public virtual string UserFirstName
		{
			get 
			{ 
				return (string)this._UserFirstName.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._UserFirstName.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserFirstName.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _UserFirstName { get; set; }

			
		/// <summary>
		///		UserLastName Property
		///		Database Field:		UserLastName;
		///		Database Parameter:	@Userlastname;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "UserLastName", IsNullable=true)]
		public virtual string UserLastName
		{
			get 
			{ 
				return (string)this._UserLastName.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._UserLastName.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserLastName.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _UserLastName { get; set; }

			
		/// <summary>
		///		UserPhone Property
		///		Database Field:		UserPhone;
		///		Database Parameter:	@Userphone;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "UserPhone", IsNullable=true)]
		public virtual string UserPhone
		{
			get 
			{ 
				return (string)this._UserPhone.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._UserPhone.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserPhone.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _UserPhone { get; set; }

			
		/// <summary>
		///		Company Property
		///		Database Field:		Company;
		///		Database Parameter:	@Company;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "Company", IsNullable=true)]
		public virtual string Company
		{
			get 
			{ 
				return (string)this._Company.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Company.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Company.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Company { get; set; }

			
		/// <summary>
		///		AddressStreet Property
		///		Database Field:		AddressStreet;
		///		Database Parameter:	@Addressstreet;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "AddressStreet", IsNullable=true)]
		public virtual string AddressStreet
		{
			get 
			{ 
				return (string)this._AddressStreet.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._AddressStreet.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._AddressStreet.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _AddressStreet { get; set; }

			
		/// <summary>
		///		AddressZip Property
		///		Database Field:		AddressZip;
		///		Database Parameter:	@Addresszip;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "AddressZip", IsNullable=true)]
		public virtual string AddressZip
		{
			get 
			{ 
				return (string)this._AddressZip.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._AddressZip.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._AddressZip.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _AddressZip { get; set; }

			
		/// <summary>
		///		AddressCity Property
		///		Database Field:		AddressCity;
		///		Database Parameter:	@Addresscity;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "AddressCity", IsNullable=true)]
		public virtual string AddressCity
		{
			get 
			{ 
				return (string)this._AddressCity.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._AddressCity.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._AddressCity.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _AddressCity { get; set; }

			
		/// <summary>
		///		AddressCountry Property
		///		Database Field:		AddressCountry;
		///		Database Parameter:	@Addresscountry;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "AddressCountry", IsNullable=true)]
		public virtual string AddressCountry
		{
			get 
			{ 
				return (string)this._AddressCountry.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._AddressCountry.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._AddressCountry.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _AddressCountry { get; set; }

			
		/// <summary>
		///		InvoiceReference Property
		///		Database Field:		InvoiceReference;
		///		Database Parameter:	@Invoicereference;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "InvoiceReference", IsNullable=true)]
		public virtual string InvoiceReference
		{
			get 
			{ 
				return (string)this._InvoiceReference.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._InvoiceReference.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._InvoiceReference.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _InvoiceReference { get; set; }

			
		/// <summary>
		///		TransactionID Property
		///		Database Field:		TransactionID;
		///		Database Parameter:	@Transactionid;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "TransactionID", IsNullable=true)]
		public virtual string TransactionID
		{
			get 
			{ 
				return (string)this._TransactionID.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._TransactionID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._TransactionID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _TransactionID { get; set; }

			
		/// <summary>
		///		QueryString Property
		///		Database Field:		QueryString;
		///		Database Parameter:	@Querystring;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "QueryString", IsNullable=true)]
		public virtual string QueryString
		{
			get 
			{ 
				return (string)this._QueryString.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._QueryString.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._QueryString.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _QueryString { get; set; }

			
		/// <summary>
		///		OrderDescription Property
		///		Database Field:		OrderDescription;
		///		Database Parameter:	@Orderdescription;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "OrderDescription", IsNullable=true)]
		public virtual string OrderDescription
		{
			get 
			{ 
				return (string)this._OrderDescription.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._OrderDescription.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._OrderDescription.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _OrderDescription { get; set; }

			
		/// <summary>
		///		OrderAmount Property
		///		Database Field:		OrderAmount;
		///		Database Parameter:	@Orderamount;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "OrderAmount")]
		public virtual decimal OrderAmount
		{
			get 
			{ 
				return this._OrderAmount.Value; 
			}

			set 
			{ 
				//Set Value
				this._OrderAmount.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._OrderAmount.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<decimal> _OrderAmount { get; set; }

			
		/// <summary>
		///		OrderCurrency Property
		///		Database Field:		OrderCurrency;
		///		Database Parameter:	@Ordercurrency;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "OrderCurrency")]
		public virtual string OrderCurrency
		{
			get 
			{ 
				return this._OrderCurrency.Value; 
			}

			set 
			{ 
				//Set Value
				this._OrderCurrency.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._OrderCurrency.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _OrderCurrency { get; set; }

			
		/// <summary>
		///		CCResponseCode Property
		///		Database Field:		CCResponseCode;
		///		Database Parameter:	@Ccresponsecode;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "CCResponseCode", IsNullable=true)]
		public virtual string CCResponseCode
		{
			get 
			{ 
				return (string)this._CCResponseCode.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._CCResponseCode.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CCResponseCode.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _CCResponseCode { get; set; }

			
		/// <summary>
		///		CCResponseSource Property
		///		Database Field:		CCResponseSource;
		///		Database Parameter:	@Ccresponsesource;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "CCResponseSource", IsNullable=true)]
		public virtual string CCResponseSource
		{
			get 
			{ 
				return (string)this._CCResponseSource.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._CCResponseSource.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CCResponseSource.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _CCResponseSource { get; set; }

			
		/// <summary>
		///		CCResponseText Property
		///		Database Field:		CCResponseText;
		///		Database Parameter:	@Ccresponsetext;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "CCResponseText", IsNullable=true)]
		public virtual string CCResponseText
		{
			get 
			{ 
				return (string)this._CCResponseText.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._CCResponseText.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CCResponseText.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _CCResponseText { get; set; }

			
		/// <summary>
		///		CCAuthorizationID Property
		///		Database Field:		CCAuthorizationID;
		///		Database Parameter:	@Ccauthorizationid;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "CCAuthorizationID", IsNullable=true)]
		public virtual string CCAuthorizationID
		{
			get 
			{ 
				return (string)this._CCAuthorizationID.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._CCAuthorizationID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CCAuthorizationID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _CCAuthorizationID { get; set; }

			
		/// <summary>
		///		CCExecutionTime Property
		///		Database Field:		CCExecutionTime;
		///		Database Parameter:	@Ccexecutiontime;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "CCExecutionTime", IsNullable=true)]
		public virtual DateTime? CCExecutionTime
		{
			get 
			{ 
				return (DateTime?)this._CCExecutionTime.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._CCExecutionTime.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CCExecutionTime.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CCExecutionTime { get; set; }

			
		/// <summary>
		///		GoogleCookieID Property
		///		Database Field:		GoogleCookieID;
		///		Database Parameter:	@Googlecookieid;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "GoogleCookieID", IsNullable=true)]
		public virtual string GoogleCookieID
		{
			get 
			{ 
				return (string)this._GoogleCookieID.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._GoogleCookieID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._GoogleCookieID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _GoogleCookieID { get; set; }

			
		/// <summary>
		///		SiteCode Property
		///		Database Field:		SiteCode;
		///		Database Parameter:	@Sitecode;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_payments", FieldName: "SiteCode", IsNullable=true)]
		public virtual string SiteCode
		{
			get 
			{ 
				return (string)this._SiteCode.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._SiteCode.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._SiteCode.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _SiteCode { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoPayment Dto)
		{
			//Set Values
			this._CreateDate.SetChangedValueIfDifferent(Dto.CreateDate);
			this._UserID.SetChangedValueIfDifferent(Dto.UserID);
			this._CustomerID.SetChangedValueIfDifferent(Dto.CustomerID);
			this._PaymentMethodID.SetChangedValueIfDifferent(Dto.PaymentMethodID);
			this._IsTest.SetChangedValueIfDifferent(Dto.IsTest);
			this._Success.SetChangedValueIfDifferent(Dto.Success);
			this._UserEmail.SetChangedValueIfDifferent(Dto.UserEmail);
			this._UserFirstName.SetChangedValueIfDifferent(Dto.UserFirstName);
			this._UserLastName.SetChangedValueIfDifferent(Dto.UserLastName);
			this._UserPhone.SetChangedValueIfDifferent(Dto.UserPhone);
			this._Company.SetChangedValueIfDifferent(Dto.Company);
			this._AddressStreet.SetChangedValueIfDifferent(Dto.AddressStreet);
			this._AddressZip.SetChangedValueIfDifferent(Dto.AddressZip);
			this._AddressCity.SetChangedValueIfDifferent(Dto.AddressCity);
			this._AddressCountry.SetChangedValueIfDifferent(Dto.AddressCountry);
			this._InvoiceReference.SetChangedValueIfDifferent(Dto.InvoiceReference);
			this._TransactionID.SetChangedValueIfDifferent(Dto.TransactionID);
			this._QueryString.SetChangedValueIfDifferent(Dto.QueryString);
			this._OrderDescription.SetChangedValueIfDifferent(Dto.OrderDescription);
			this._OrderAmount.SetChangedValueIfDifferent(Dto.OrderAmount);
			this._OrderCurrency.SetChangedValueIfDifferent(Dto.OrderCurrency);
			this._CCResponseCode.SetChangedValueIfDifferent(Dto.CCResponseCode);
			this._CCResponseSource.SetChangedValueIfDifferent(Dto.CCResponseSource);
			this._CCResponseText.SetChangedValueIfDifferent(Dto.CCResponseText);
			this._CCAuthorizationID.SetChangedValueIfDifferent(Dto.CCAuthorizationID);
			this._CCExecutionTime.SetChangedValueIfDifferent(Dto.CCExecutionTime);
			this._GoogleCookieID.SetChangedValueIfDifferent(Dto.GoogleCookieID);
			this._SiteCode.SetChangedValueIfDifferent(Dto.SiteCode);
		}
		public virtual DtoPayment ToDto()
		{
			return this.ToDto(new DtoPayment());
		}
		
		public virtual DtoPayment ToDto(DtoPayment dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.CreateDate = this.CreateDate;
			dto.UserID = this.UserID;
			dto.CustomerID = this.CustomerID;
			dto.PaymentMethodID = this.PaymentMethodID;
			dto.IsTest = this.IsTest;
			dto.Success = this.Success;
			dto.UserEmail = this.UserEmail;
			dto.UserFirstName = this.UserFirstName;
			dto.UserLastName = this.UserLastName;
			dto.UserPhone = this.UserPhone;
			dto.Company = this.Company;
			dto.AddressStreet = this.AddressStreet;
			dto.AddressZip = this.AddressZip;
			dto.AddressCity = this.AddressCity;
			dto.AddressCountry = this.AddressCountry;
			dto.InvoiceReference = this.InvoiceReference;
			dto.TransactionID = this.TransactionID;
			dto.QueryString = this.QueryString;
			dto.OrderDescription = this.OrderDescription;
			dto.OrderAmount = this.OrderAmount;
			dto.OrderCurrency = this.OrderCurrency;
			dto.CCResponseCode = this.CCResponseCode;
			dto.CCResponseSource = this.CCResponseSource;
			dto.CCResponseText = this.CCResponseText;
			dto.CCAuthorizationID = this.CCAuthorizationID;
			dto.CCExecutionTime = this.CCExecutionTime;
			dto.GoogleCookieID = this.GoogleCookieID;
			dto.SiteCode = this.SiteCode;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_payments] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_payments]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	//public partial class PhoenixSessions : PhoenixSessions<PhoenixSessions, PhoenixSession>
	//{
	//	#region ***** CONSTRUCTORS *****
	//	/// <summary>
	//	/// Creates PhoenixSessions
	//	/// </summary>
	//	public PhoenixSessions() : this(new Context()) {}
		
	//	/// <summary>
	//	/// Creates PhoenixSessions
	//	/// </summary>
	//	public PhoenixSessions(Context Context) : base(Context) {}

	//	#endregion
	//}
	
	////Generic Collection
	//public partial class PhoenixSessions<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
	//	where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
	//	where DataRecordType: PhoenixSession<DataRecordType>, new()
	//{
		
	//	#region ***** ENUM / CONSTANTS *****
	//	#endregion
		
	//	#region ***** COLUMNS *****
	//	public static new class Columns
	//	{
	//		//Properties
	//		public static jlib.DataFramework.BaseColumn<string> PhoenixCookie = new jlib.DataFramework.BaseColumn<string>(FieldName: "phoenix_cookie", PropertyName: "PhoenixCookie", ParameterName: "@PhoenixCookie", PkParameterName: "@PK_PhoenixCookie", DBType: DbType.AnsiString, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
	//		public static jlib.DataFramework.BaseColumn<string> Username = new jlib.DataFramework.BaseColumn<string>(FieldName: "username", PropertyName: "Username", ParameterName: "@Username", PkParameterName: "@PK_Username", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
	//		public static jlib.DataFramework.BaseColumn<DateTime> PostedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "posted_date", PropertyName: "PostedDate", ParameterName: "@PostedDate", PkParameterName: "@PK_PostedDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
	//	}
	//	#endregion

	//	#region ***** CONSTRUCTORS *****
	//	/// <summary>
	//	/// Creates PhoenixSessions
	//	/// </summary>
	//	public PhoenixSessions() : this(new Context()) {}
		
	//	/// <summary>
	//	/// Creates PhoenixSessions
	//	/// </summary>
	//	public PhoenixSessions(Context Context) : base(Context) 
	//	{
	//		//Set Table_Name
	//		this.TABLE_NAME = "d_phoenix_sessions";
			
	//		//Set Columns
	//		base.Columns.Add("PhoenixCookie", this.PhoenixCookie = PhoenixSessions<DataCollectionType, DataRecordType>.Columns.PhoenixCookie);
	//		base.Columns.Add("Username", this.Username = PhoenixSessions<DataCollectionType, DataRecordType>.Columns.Username);
	//		base.Columns.Add("PostedDate", this.PostedDate = PhoenixSessions<DataCollectionType, DataRecordType>.Columns.PostedDate);
			
	//		//Call the secondary constructor method
	//		this.PhoenixSessionsEx();
	//	}

	//	/// <summary>
	//	/// Secondary Class Constructor Method
	//	/// </summary>
	//	protected virtual void PhoenixSessionsEx() {}
		
	//	#endregion
		
	//	#region ***** PROPERTIES *****
	//	public jlib.DataFramework.BaseColumn<string> PhoenixCookie { get; set; }
	//	public jlib.DataFramework.BaseColumn<string> Username { get; set; }
	//	public jlib.DataFramework.BaseColumn<DateTime> PostedDate { get; set; }
		
		
	//	#endregion

	//	#region ***** STATIC LOAD METHODS *****
	//	public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
	//	{
	//		//If Cache is not supplied, load from previous method attribute
	//		CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

	//		return GetAll(@"SELECT * FROM [d_phoenix_sessions] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
	//	}
		
	//	public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
	//	{
	//		//If Cache is not supplied, load from previous method attribute
	//		CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

	//		return GetAll(@"SELECT * FROM [d_phoenix_sessions] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
	//	}
		
	//	public static void DeleteAll()
	//	{
	//		DeleteAll("d_phoenix_sessions");
	//	}
		
	//	public static void DeleteByPks(List<string> PKs)
	//	{
	//		DeleteByPks<string>("d_phoenix_sessions", "phoenix_cookie", PKs);
	//	}
		
	//	public static QueryPackage GetByField(string PhoenixCookie = null, string Username = null, DateTime? PostedDate = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
	//	{
	//		//If Cache is not supplied, load from previous method attribute
	//		CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

	//		//Build Package
	//		QueryPackage query = new QueryPackage();

	//		//Get Filter Conditions
	//		List<string> filterConditions = new List<string>();	
		
	//		if(PhoenixCookie != null)
	//		{
	//			query.Parameters.PhoenixCookie = PhoenixCookie;
	//			filterConditions.Add(@"[phoenix_cookie] = @PhoenixCookie");
	//		}	
	//		if(Username != null)
	//		{
	//			query.Parameters.Username = Username;
	//			filterConditions.Add(@"[username] = @Username");
	//		}	
	//		if(PostedDate != null)
	//		{
	//			query.Parameters.PostedDate = PostedDate;
	//			filterConditions.Add(@"[posted_date] = @PostedDate");
	//		}	
	//		//Provider: SQL Server Schema Provider
	//		//Create Statement
	//		query.Statement = @"SELECT * FROM [d_phoenix_sessions] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
	//		query.IsStoredProcedure = false;
	//		query.CacheOptions = CacheOptions;
	//		query.PagingOptions = PagingOptions;

	//		//Return Query Package
	//		return query;

	//	}

	//	public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
	//	{
	//		return GetAll().Where(Condition);
	//	}
	//	public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
	//	{
	//		return GetAll(PagingOptions: PagingOptions).Where(Condition);
	//	}
	//	/*
	//	public static QueryPackage Where(Func<PhoenixSessions<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
	//	{
	//		return GetAll().Where(fnCondition(new PhoenixSessions<DataCollectionType, DataRecordType>()));
	//	}
	//	public static QueryPackage Where(PagingOptions PagingOptions, Func<PhoenixSessions<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
	//	{
	//		return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new PhoenixSessions<DataCollectionType, DataRecordType>()));
	//	}
	//	*/

	//	public static QueryPackage GetByPKs(List<string> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
	//	{
	//		//If Cache is not supplied, load from previous method attribute
	//		CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

	//		if(PKs == null) PKs = new List<string>();

	//		//Build Package
	//		QueryPackage query = new QueryPackage();

	//		//Get IN Conditions
	//		List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
	//		if(pkClause.Count==0) pkClause.Add("-100");
	//		string pkStringClause = pkClause.Join(",");

	//		//Create Statement
	//		query.Statement = @"SELECT * FROM [d_phoenix_sessions] WITH (NOLOCK) WHERE [phoenix_cookie] IN (" + pkStringClause + ")";
	//		query.IsStoredProcedure = false;
	//		query.CacheOptions = CacheOptions;
	//		query.PagingOptions = PagingOptions;

	//		//Return Query Package
	//		return query;

	//	}
		
	//	public override IDataReader GetDataReader()
	//	{
	//		return new PhoenixSessionsDataReader(this);
	//	}
		
	//	public List<DtoPhoenixSession> ToDtos()
	//	{
	//		return this.Select(x=>x.ToDto()).ToList();
	//	}
	//	#endregion
		
	//	#region ***** INTERNAL CLASSES *****
	//	public class PhoenixSessionsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
	//	{
	//		//Constructor
	//		public PhoenixSessionsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
	//	}
	//	#endregion
		
	//}

	////Typed Dto Record
	//[Serializable]
	//[DataContract]
	//public partial class DtoPhoenixSession
	//{
	//	#region ***** PROPERTIES *****
	//	/// <summary>
	//	///		PhoenixCookie Property
	//	/// </summary>
	//	[DataMember]
	//	public virtual string PhoenixCookie { get; set; }
		
	//	/// <summary>
	//	///		Username Property
	//	/// </summary>
	//	[DataMember]
	//	public virtual string Username { get; set; }
		
	//	/// <summary>
	//	///		PostedDate Property
	//	/// </summary>
	//	[DataMember]
	//	public virtual DateTime PostedDate { get; set; }
		
	//	#endregion
	//}

	////Typed Record
	//public partial class PhoenixSession : PhoenixSession<PhoenixSession>
	//{
	//	#region ***** CONSTRUCTORS *****
	//	/// <summary>
	//	/// Creates PhoenixSession
	//	/// </summary>
	//	public PhoenixSession() : this(new Context()) {}
		
	//	/// <summary>
	//	/// Creates PhoenixSession
	//	/// </summary>
	//	public PhoenixSession(Context Context) : base(Context) {}
		
	//	/// <summary>
	//	/// Deserialization method for PhoenixSession
	//	/// </summary>
	//	protected PhoenixSession(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
	//	#endregion
	//}

	////Generic Record
	//public partial class PhoenixSession<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: PhoenixSession<DataRecordType>, new()
	//{
	//	#region ***** CONSTRUCTORS *****
	//	/// <summary>
	//	/// Creates PhoenixSession
	//	/// </summary>
	//	public PhoenixSession() : this(new Context()) {}
		
	//	/// <summary>
	//	/// Creates PhoenixSession
	//	/// </summary>
	//	public PhoenixSession(Context Context) : base(Context)
	//	{
	//		//Set Table_Name
	//		this.TABLE_NAME = "d_phoenix_sessions";

	//		//Declare Fields
	//		this.Fields.Add("PhoenixCookie" ,this._PhoenixCookie = new jlib.DataFramework.BaseField<string>(this, PhoenixSessions.Columns.PhoenixCookie, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
	//		this.Fields.Add("Username" ,this._Username = new jlib.DataFramework.BaseField<string>(this, PhoenixSessions.Columns.Username, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
	//		this.Fields.Add("PostedDate" ,this._PostedDate = new jlib.DataFramework.BaseField<DateTime>(this, PhoenixSessions.Columns.PostedDate, InitialValue: DateTime.Now, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
	//		//Call the secondary constructor method
	//		this.PhoenixSessionEx();
	//	}
		
	//	/// <summary>
	//	/// Deserialization method for PhoenixSession
	//	/// </summary>
	//	protected PhoenixSession(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
 //       {
 //           if (SerializationInfo == null)
 //               throw new ArgumentNullException("SerializationInfo");

	//		//Loop through fields setting values
	//		this.Fields
	//			.ToList()
	//			.ForEach(field =>
	//			{
	//				//Set Value
	//				field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
	//			});
 //       }
		
	//	/// <summary>
	//	/// Secondary Class Constructor Method
	//	/// </summary>
	//	protected virtual void PhoenixSessionEx() {}
	//	#endregion

	//	#region ***** PROPERTIES *****
	//	/// <summary>
	//	///		PhoenixCookie Property
	//	///		Database Field:		phoenix_cookie;
	//	///		Database Parameter:	@PhoenixCookie;
	//	///		Primary Key:		Yes;
	//	///		Foreign Key:		No;
	//	///		IsIdentity:			No;
	//	///		IsColumnNullable:	No;
	//	///		ColumnHasDefault:	No;
	//	/// </summary>
	//	[jlib.DataFramework.Field(Table: "d_phoenix_sessions", FieldName: "phoenix_cookie", IsPrimaryKey=true)]
	//	public virtual string PhoenixCookie
	//	{
	//		get 
	//		{ 
	//			return this._PhoenixCookie.Value; 
	//		}

	//		set 
	//		{ 
	//			//Set Value
	//			this._PhoenixCookie.SetChangedValue(value); 

	//			//Set RowState
	//			if(this.RecordState != DataRowAction.Add && this._PhoenixCookie.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

	//		}
	//	}
	//	protected jlib.DataFramework.BaseField<string> _PhoenixCookie { get; set; }

			
	//	/// <summary>
	//	///		Username Property
	//	///		Database Field:		username;
	//	///		Database Parameter:	@Username;
	//	///		Primary Key:		No;
	//	///		Foreign Key:		No;
	//	///		IsIdentity:			No;
	//	///		IsColumnNullable:	No;
	//	///		ColumnHasDefault:	No;
	//	/// </summary>
	//	[jlib.DataFramework.Field(Table: "d_phoenix_sessions", FieldName: "username")]
	//	public virtual string Username
	//	{
	//		get 
	//		{ 
	//			return this._Username.Value; 
	//		}

	//		set 
	//		{ 
	//			//Set Value
	//			this._Username.SetChangedValue(value); 

	//			//Set RowState
	//			if(this.RecordState != DataRowAction.Add && this._Username.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

	//		}
	//	}
	//	protected jlib.DataFramework.BaseField<string> _Username { get; set; }

			
	//	/// <summary>
	//	///		PostedDate Property
	//	///		Database Field:		posted_date;
	//	///		Database Parameter:	@PostedDate;
	//	///		Primary Key:		No;
	//	///		Foreign Key:		No;
	//	///		IsIdentity:			No;
	//	///		IsColumnNullable:	No;
	//	///		ColumnHasDefault:	Yes;
	//	/// </summary>
	//	[jlib.DataFramework.Field(Table: "d_phoenix_sessions", FieldName: "posted_date")]
	//	public virtual DateTime PostedDate
	//	{
	//		get 
	//		{ 
	//			return this._PostedDate.Value; 
	//		}

	//		set 
	//		{ 
	//			//Set Value
	//			this._PostedDate.SetChangedValue(value); 

	//			//Set RowState
	//			if(this.RecordState != DataRowAction.Add && this._PostedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

	//		}
	//	}
	//	protected jlib.DataFramework.BaseField<DateTime> _PostedDate { get; set; }

			

	//	#endregion
		
	//	#region ***** METHODS *****
	//	public virtual void ImportDto(DtoPhoenixSession Dto)
	//	{
	//		//Set Values
	//		this._PhoenixCookie.SetChangedValueIfDifferent(Dto.PhoenixCookie);
	//		this._Username.SetChangedValueIfDifferent(Dto.Username);
	//		this._PostedDate.SetChangedValueIfDifferent(Dto.PostedDate);
	//	}
	//	public virtual DtoPhoenixSession ToDto()
	//	{
	//		return this.ToDto(new DtoPhoenixSession());
	//	}
		
	//	public virtual DtoPhoenixSession ToDto(DtoPhoenixSession dto)
	//	{
	//		//Set Values
	//		dto.PhoenixCookie = this.PhoenixCookie;
	//		dto.Username = this.Username;
	//		dto.PostedDate = this.PostedDate;
				
	//		//Return
	//		return dto;
	//	}
	//	protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
	//	{
	//		switch(InternalObjectIdentifier.ToLower())
	//		{
				
	//			default:
	//				break;
	//		}
			
	//		//Call Base Method
	//		base.ImportInternalObject(InternalObjectIdentifier, Row);
	//	}
	//	#endregion
		
	//	#region ***** STATIC LOAD METHODS *****



	//	public static DataRecordType LoadByPk(string PhoenixCookie)
	//	{
	//		//Create Sql Connection
	//		SqlHelper sqlHelper = new Context().GetSqlHelper();
			
	//		//Load
	//		DataRecordType item = LoadByPk(sqlHelper, PhoenixCookie);
			
	//		//Return
	//		return item;
	//	}
	//	public static DataRecordType LoadByPk(SqlHelper SqlHelper, string PhoenixCookie)
	//	{
	//		//Execute Command
	//		DataTable dataTable = SqlHelper.ExecuteSql(@"
	//									SELECT
	//										*
	//									FROM
	//										[d_phoenix_sessions]  WITH (NOLOCK)
	//									WHERE
											
	//										[phoenix_cookie] = @PK_PhoenixCookie
	//										",
	//										"@PK_PhoenixCookie", PhoenixCookie
	//										);
											
	//		//If Row returned
	//		if (dataTable.Rows.Count > 0)
	//		{
	//			//Create New Object
	//			DataRecordType item = new DataRecordType();
	//			item.Context = new Context();
				
	//			//Import
	//			item.Import(dataTable.Rows[0]);

	//			//Return Row
	//			return item;
	//		}

	//		//Else, Return null
	//		else
	//			return null;

	//	}

	//	#endregion
	//}

	//Typed Collection
	public partial class Posts : Posts<Posts, Post>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Posts
		/// </summary>
		public Posts() : this(new Context()) {}
		
		/// <summary>
		/// Creates Posts
		/// </summary>
		public Posts(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Posts<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: Post<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ObjectID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ObjectId", PropertyName: "ObjectID", ParameterName: "@Objectid", PkParameterName: "@PK_Objectid", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CreateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CreateDate", PropertyName: "CreateDate", ParameterName: "@Createdate", PkParameterName: "@PK_Createdate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> DeleteDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "DeleteDate", PropertyName: "DeleteDate", ParameterName: "@Deletedate", PkParameterName: "@PK_Deletedate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Subject = new jlib.DataFramework.BaseColumn<string>(FieldName: "Subject", PropertyName: "Subject", ParameterName: "@Subject", PkParameterName: "@PK_Subject", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Body = new jlib.DataFramework.BaseColumn<string>(FieldName: "Body", PropertyName: "Body", ParameterName: "@Body", PkParameterName: "@PK_Body", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> CreatedbyID = new jlib.DataFramework.BaseColumn<int>(FieldName: "CreatedBy", PropertyName: "CreatedbyID", ParameterName: "@Createdby", PkParameterName: "@PK_Createdby", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> ModifiedbyID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ModifiedBy", PropertyName: "ModifiedbyID", ParameterName: "@Modifiedby", PkParameterName: "@PK_Modifiedby", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<DateTime> ResolvedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "ResolvedDate", PropertyName: "ResolvedDate", ParameterName: "@Resolveddate", PkParameterName: "@PK_Resolveddate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> ParentID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ParentID", PropertyName: "ParentID", ParameterName: "@Parentid", PkParameterName: "@PK_Parentid", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Posts
		/// </summary>
		public Posts() : this(new Context()) {}
		
		/// <summary>
		/// Creates Posts
		/// </summary>
		public Posts(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_posts";
			
			//Set Columns
			base.Columns.Add("ObjectID", this.ObjectID = Posts<DataCollectionType, DataRecordType>.Columns.ObjectID);
			base.Columns.Add("CreateDate", this.CreateDate = Posts<DataCollectionType, DataRecordType>.Columns.CreateDate);
			base.Columns.Add("DeleteDate", this.DeleteDate = Posts<DataCollectionType, DataRecordType>.Columns.DeleteDate);
			base.Columns.Add("Subject", this.Subject = Posts<DataCollectionType, DataRecordType>.Columns.Subject);
			base.Columns.Add("Body", this.Body = Posts<DataCollectionType, DataRecordType>.Columns.Body);
			base.Columns.Add("CreatedbyID", this.CreatedbyID = Posts<DataCollectionType, DataRecordType>.Columns.CreatedbyID);
			base.Columns.Add("ModifiedbyID", this.ModifiedbyID = Posts<DataCollectionType, DataRecordType>.Columns.ModifiedbyID);
			base.Columns.Add("ResolvedDate", this.ResolvedDate = Posts<DataCollectionType, DataRecordType>.Columns.ResolvedDate);
			base.Columns.Add("ParentID", this.ParentID = Posts<DataCollectionType, DataRecordType>.Columns.ParentID);
			
			//Call the secondary constructor method
			this.PostsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void PostsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ObjectID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CreateDate { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> DeleteDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> Subject { get; set; }
		public jlib.DataFramework.BaseColumn<string> Body { get; set; }
		public jlib.DataFramework.BaseColumn<int> CreatedbyID { get; set; }
		public jlib.DataFramework.BaseColumn<int> ModifiedbyID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> ResolvedDate { get; set; }
		public jlib.DataFramework.BaseColumn<int> ParentID { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_posts] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_posts] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_posts");
		}
		
		
		public static QueryPackage GetByField(int? ObjectID = null, DateTime? CreateDate = null, DateTime? DeleteDate = null, string Subject = null, string Body = null, int? CreatedbyID = null, int? ModifiedbyID = null, DateTime? ResolvedDate = null, int? ParentID = null, DateTime? ObjectDate = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ObjectID != null)
			{
				query.Parameters.ObjectID = ObjectID;
				filterConditions.Add(@"[ObjectId] = @Objectid");
			}	
			if(CreateDate != null)
			{
				query.Parameters.CreateDate = CreateDate;
				filterConditions.Add(@"[CreateDate] = @Createdate");
			}	
			if(DeleteDate != null)
			{
				query.Parameters.DeleteDate = DeleteDate;
				filterConditions.Add(@"[DeleteDate] = @Deletedate");
			}	
			if(Subject != null)
			{
				query.Parameters.Subject = Subject;
				filterConditions.Add(@"[Subject] = @Subject");
			}	
			if(Body != null)
			{
				query.Parameters.Body = Body;
				filterConditions.Add(@"[Body] = @Body");
			}	
			if(CreatedbyID != null)
			{
				query.Parameters.CreatedbyID = CreatedbyID;
				filterConditions.Add(@"[CreatedBy] = @Createdby");
			}	
			if(ModifiedbyID != null)
			{
				query.Parameters.ModifiedbyID = ModifiedbyID;
				filterConditions.Add(@"[ModifiedBy] = @Modifiedby");
			}	
			if(ResolvedDate != null)
			{
				query.Parameters.ResolvedDate = ResolvedDate;
				filterConditions.Add(@"[ResolvedDate] = @Resolveddate");
			}	
			if(ParentID != null)
			{
				query.Parameters.ParentID = ParentID;
				filterConditions.Add(@"[ParentID] = @Parentid");
			}	
			if(ObjectDate != null || (CreateDate==null && DeleteDate==null))
			{
				Context oContext=new Context();
				query.Parameters.ObjectDate = (ObjectDate==null ? oContext.VersionDate : ObjectDate);
				filterConditions.Add(@"@ObjectDate BETWEEN [CreateDate] AND [DeleteDate]");
			}
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_posts] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Posts<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Posts<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Posts<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Posts<DataCollectionType, DataRecordType>()));
		}
		*/

		
		public override IDataReader GetDataReader()
		{
			return new PostsDataReader(this);
		}
		
		public List<DtoPost> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class PostsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public PostsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoPost
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		/// </summary>
		[DataMember]
		public virtual int ObjectID { get; set; }
		
		/// <summary>
		///		CreateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime CreateDate { get; set; }
		
		/// <summary>
		///		DeleteDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime DeleteDate { get; set; }
		
		/// <summary>
		///		Subject Property
		/// </summary>
		[DataMember]
		public virtual string Subject { get; set; }
		
		/// <summary>
		///		Body Property
		/// </summary>
		[DataMember]
		public virtual string Body { get; set; }
		
		/// <summary>
		///		CreatedbyID Property
		/// </summary>
		[DataMember]
		public virtual int CreatedbyID { get; set; }
		
		/// <summary>
		///		ModifiedbyID Property
		/// </summary>
		[DataMember]
		public virtual int ModifiedbyID { get; set; }
		
		/// <summary>
		///		ResolvedDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime? ResolvedDate { get; set; }
		
		/// <summary>
		///		ParentID Property
		/// </summary>
		[DataMember]
		public virtual int ParentID { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class Post : Post<Post>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Post
		/// </summary>
		public Post() : this(new Context()) {}
		
		/// <summary>
		/// Creates Post
		/// </summary>
		public Post(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for Post
		/// </summary>
		protected Post(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class Post<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: Post<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Post
		/// </summary>
		public Post() : this(new Context()) {}
		
		/// <summary>
		/// Creates Post
		/// </summary>
		public Post(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_posts";

			//Declare Fields
			this.Fields.Add("ObjectID" ,this._ObjectID = new jlib.DataFramework.BaseField<int>(this, Posts.Columns.ObjectID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreateDate" ,this._CreateDate = new jlib.DataFramework.BaseField<DateTime>(this, Posts.Columns.CreateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("DeleteDate" ,this._DeleteDate = new jlib.DataFramework.BaseField<DateTime>(this, Posts.Columns.DeleteDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Subject" ,this._Subject = new jlib.DataFramework.BaseField<string>(this, Posts.Columns.Subject, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Body" ,this._Body = new jlib.DataFramework.BaseField<string>(this, Posts.Columns.Body, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreatedbyID" ,this._CreatedbyID = new jlib.DataFramework.BaseField<int>(this, Posts.Columns.CreatedbyID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ModifiedbyID" ,this._ModifiedbyID = new jlib.DataFramework.BaseField<int>(this, Posts.Columns.ModifiedbyID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ResolvedDate" ,this._ResolvedDate = new jlib.DataFramework.BaseField<DateTime>(this, Posts.Columns.ResolvedDate, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ParentID" ,this._ParentID = new jlib.DataFramework.BaseField<int>(this, Posts.Columns.ParentID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.PostEx();
		}
		
		/// <summary>
		/// Deserialization method for Post
		/// </summary>
		protected Post(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void PostEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		///		Database Field:		ObjectId;
		///		Database Parameter:	@Objectid;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_posts", FieldName: "ObjectId", IsPrimaryKey=true)]
		public virtual int ObjectID
		{
			get 
			{ 
				return this._ObjectID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ObjectID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ObjectID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ObjectID { get; set; }

			
		/// <summary>
		///		CreateDate Property
		///		Database Field:		CreateDate;
		///		Database Parameter:	@Createdate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_posts", FieldName: "CreateDate", IsPrimaryKey=true)]
		public virtual DateTime CreateDate
		{
			get 
			{ 
				return this._CreateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CreateDate { get; set; }

			
		/// <summary>
		///		DeleteDate Property
		///		Database Field:		DeleteDate;
		///		Database Parameter:	@Deletedate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_posts", FieldName: "DeleteDate", IsPrimaryKey=true)]
		public virtual DateTime DeleteDate
		{
			get 
			{ 
				return this._DeleteDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._DeleteDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._DeleteDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _DeleteDate { get; set; }

			
		/// <summary>
		///		Subject Property
		///		Database Field:		Subject;
		///		Database Parameter:	@Subject;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_posts", FieldName: "Subject", IsNullable=true)]
		public virtual string Subject
		{
			get 
			{ 
				return (string)this._Subject.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Subject.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Subject.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Subject { get; set; }

			
		/// <summary>
		///		Body Property
		///		Database Field:		Body;
		///		Database Parameter:	@Body;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_posts", FieldName: "Body", IsNullable=true)]
		public virtual string Body
		{
			get 
			{ 
				return (string)this._Body.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Body.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Body.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Body { get; set; }

			
		/// <summary>
		///		CreatedbyID Property
		///		Database Field:		CreatedBy;
		///		Database Parameter:	@Createdby;
		///		Primary Key:		No;
		///		Foreign Key:		Yes;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_posts", FieldName: "CreatedBy", IsForeignKey=true)]
		public virtual int CreatedbyID
		{
			get 
			{ 
				return this._CreatedbyID.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreatedbyID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreatedbyID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

				//Set Foreign Key Object to Null
				this._CreatedBy = null;
			}
		}
		protected jlib.DataFramework.BaseField<int> _CreatedbyID { get; set; }

		/// <summary>
		///		CreatedBy Foreign Object Property
		///		Foreign Key Field:	CreatedBy
		///		Primary Key Field:	ID
		/// </summary>
		[jlib.DataFramework.Field("d_posts", "CreatedBy")]
		public virtual BehindUser CreatedBy
		{
			get
			{
				if(this._CreatedBy == null )
				{
					this._CreatedBy = BehindUsers.GetByField(ID:(int)this.CreatedbyID).Execute().FirstOrDefault();
				}
				
						
				return this._CreatedBy;
			}
			
			set
			{
				this._CreatedBy = value;	
			}
		}
		protected BehindUser _CreatedBy { get; set; }
			
		/// <summary>
		///		ModifiedbyID Property
		///		Database Field:		ModifiedBy;
		///		Database Parameter:	@Modifiedby;
		///		Primary Key:		No;
		///		Foreign Key:		Yes;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_posts", FieldName: "ModifiedBy", IsForeignKey=true)]
		public virtual int ModifiedbyID
		{
			get 
			{ 
				return this._ModifiedbyID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ModifiedbyID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ModifiedbyID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

				//Set Foreign Key Object to Null
				this._ModifiedBy = null;
			}
		}
		protected jlib.DataFramework.BaseField<int> _ModifiedbyID { get; set; }

		/// <summary>
		///		ModifiedBy Foreign Object Property
		///		Foreign Key Field:	ModifiedBy
		///		Primary Key Field:	ID
		/// </summary>
		[jlib.DataFramework.Field("d_posts", "ModifiedBy")]
		public virtual BehindUser ModifiedBy
		{
			get
			{
				if(this._ModifiedBy == null )
				{
					this._ModifiedBy = BehindUsers.GetByField(ID:(int)this.ModifiedbyID).Execute().FirstOrDefault();
				}
				
						
				return this._ModifiedBy;
			}
			
			set
			{
				this._ModifiedBy = value;	
			}
		}
		protected BehindUser _ModifiedBy { get; set; }
			
		/// <summary>
		///		ResolvedDate Property
		///		Database Field:		ResolvedDate;
		///		Database Parameter:	@Resolveddate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_posts", FieldName: "ResolvedDate", IsNullable=true)]
		public virtual DateTime? ResolvedDate
		{
			get 
			{ 
				return (DateTime?)this._ResolvedDate.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._ResolvedDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ResolvedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _ResolvedDate { get; set; }

			
		/// <summary>
		///		ParentID Property
		///		Database Field:		ParentID;
		///		Database Parameter:	@Parentid;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_posts", FieldName: "ParentID")]
		public virtual int ParentID
		{
			get 
			{ 
				return this._ParentID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ParentID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ParentID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ParentID { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoPost Dto)
		{
			//Set Values
			this._ObjectID.SetChangedValueIfDifferent(Dto.ObjectID);
			this._CreateDate.SetChangedValueIfDifferent(Dto.CreateDate);
			this._DeleteDate.SetChangedValueIfDifferent(Dto.DeleteDate);
			this._Subject.SetChangedValueIfDifferent(Dto.Subject);
			this._Body.SetChangedValueIfDifferent(Dto.Body);
			this._CreatedbyID.SetChangedValueIfDifferent(Dto.CreatedbyID);
			this._ModifiedbyID.SetChangedValueIfDifferent(Dto.ModifiedbyID);
			this._ResolvedDate.SetChangedValueIfDifferent(Dto.ResolvedDate);
			this._ParentID.SetChangedValueIfDifferent(Dto.ParentID);
		}
		public virtual DtoPost ToDto()
		{
			return this.ToDto(new DtoPost());
		}
		
		public virtual DtoPost ToDto(DtoPost dto)
		{
			//Set Values
			dto.ObjectID = this.ObjectID;
			dto.CreateDate = this.CreateDate;
			dto.DeleteDate = this.DeleteDate;
			dto.Subject = this.Subject;
			dto.Body = this.Body;
			dto.CreatedbyID = this.CreatedbyID;
			dto.ModifiedbyID = this.ModifiedbyID;
			dto.ResolvedDate = this.ResolvedDate;
			dto.ParentID = this.ParentID;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				//Create Internal Object 'createdby'
				case "createdby":
					this._CreatedBy = new BehindUser();
					this._CreatedBy.Import(Row, InternalObjectIdentifier);
					break;
				
				//Create Internal Object 'modifiedby'
				case "modifiedby":
					this._ModifiedBy = new BehindUser();
					this._ModifiedBy.Import(Row, InternalObjectIdentifier);
					break;
				
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****

				public static DataRecordType LoadByPk(int ObjectID)
				{
					Context oContext=new Context();
					//Create Sql Connection
					SqlHelper sqlHelper = oContext.GetSqlHelper();
			
					//Load
					DataRecordType item = LoadByPk(sqlHelper, ObjectID, oContext.VersionDate);
			
					//Return
					return item;
				}

		public static DataRecordType LoadByPk(int ObjectID,  DateTime ObjectDate)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ObjectID, ObjectDate);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ObjectID,  DateTime ObjectDate)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_posts]  WITH (NOLOCK)
										WHERE
											
											[ObjectId] = @PK_Objectid		AND @ObjectDate BETWEEN [CreateDate] AND [DeleteDate]

											",
											"@PK_Objectid", ObjectID, "@ObjectDate", ObjectDate
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class QuestionOptions : QuestionOptions<QuestionOptions, QuestionOption>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates QuestionOptions
		/// </summary>
		public QuestionOptions() : this(new Context()) {}
		
		/// <summary>
		/// Creates QuestionOptions
		/// </summary>
		public QuestionOptions(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class QuestionOptions<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: QuestionOption<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ObjectID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ObjectID", PropertyName: "ObjectID", ParameterName: "@Objectid", PkParameterName: "@PK_Objectid", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CreateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CreateDate", PropertyName: "CreateDate", ParameterName: "@Createdate", PkParameterName: "@PK_Createdate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> DeleteDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "DeleteDate", PropertyName: "DeleteDate", ParameterName: "@Deletedate", PkParameterName: "@PK_Deletedate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> IsCorrect = new jlib.DataFramework.BaseColumn<int>(FieldName: "IsCorrect", PropertyName: "IsCorrect", ParameterName: "@Iscorrect", PkParameterName: "@PK_Iscorrect", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Text = new jlib.DataFramework.BaseColumn<string>(FieldName: "Text", PropertyName: "Text", ParameterName: "@Text", PkParameterName: "@PK_Text", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Reason = new jlib.DataFramework.BaseColumn<string>(FieldName: "Reason", PropertyName: "Reason", ParameterName: "@Reason", PkParameterName: "@PK_Reason", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates QuestionOptions
		/// </summary>
		public QuestionOptions() : this(new Context()) {}
		
		/// <summary>
		/// Creates QuestionOptions
		/// </summary>
		public QuestionOptions(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_question_options";
			
			//Set Columns
			base.Columns.Add("ObjectID", this.ObjectID = QuestionOptions<DataCollectionType, DataRecordType>.Columns.ObjectID);
			base.Columns.Add("CreateDate", this.CreateDate = QuestionOptions<DataCollectionType, DataRecordType>.Columns.CreateDate);
			base.Columns.Add("DeleteDate", this.DeleteDate = QuestionOptions<DataCollectionType, DataRecordType>.Columns.DeleteDate);
			base.Columns.Add("IsCorrect", this.IsCorrect = QuestionOptions<DataCollectionType, DataRecordType>.Columns.IsCorrect);
			base.Columns.Add("Text", this.Text = QuestionOptions<DataCollectionType, DataRecordType>.Columns.Text);
			base.Columns.Add("Reason", this.Reason = QuestionOptions<DataCollectionType, DataRecordType>.Columns.Reason);
			
			//Call the secondary constructor method
			this.QuestionOptionsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void QuestionOptionsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ObjectID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CreateDate { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> DeleteDate { get; set; }
		public jlib.DataFramework.BaseColumn<int> IsCorrect { get; set; }
		public jlib.DataFramework.BaseColumn<string> Text { get; set; }
		public jlib.DataFramework.BaseColumn<string> Reason { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_question_options] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_question_options] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_question_options");
		}
		
		
		public static QueryPackage GetByField(int? ObjectID = null, DateTime? CreateDate = null, DateTime? DeleteDate = null, int? IsCorrect = null, string Text = null, string Reason = null, DateTime? ObjectDate = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ObjectID != null)
			{
				query.Parameters.ObjectID = ObjectID;
				filterConditions.Add(@"[ObjectID] = @Objectid");
			}	
			if(CreateDate != null)
			{
				query.Parameters.CreateDate = CreateDate;
				filterConditions.Add(@"[CreateDate] = @Createdate");
			}	
			if(DeleteDate != null)
			{
				query.Parameters.DeleteDate = DeleteDate;
				filterConditions.Add(@"[DeleteDate] = @Deletedate");
			}	
			if(IsCorrect != null)
			{
				query.Parameters.IsCorrect = IsCorrect;
				filterConditions.Add(@"[IsCorrect] = @Iscorrect");
			}	
			if(Text != null)
			{
				query.Parameters.Text = Text;
				filterConditions.Add(@"[Text] LIKE @Text");
			}	
			if(Reason != null)
			{
				query.Parameters.Reason = Reason;
				filterConditions.Add(@"[Reason] LIKE @Reason");
			}	
			if(ObjectDate != null || (CreateDate==null && DeleteDate==null))
			{
				Context oContext=new Context();
				query.Parameters.ObjectDate = (ObjectDate==null ? oContext.VersionDate : ObjectDate);
				filterConditions.Add(@"@ObjectDate BETWEEN [CreateDate] AND [DeleteDate]");
			}
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_question_options] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<QuestionOptions<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new QuestionOptions<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<QuestionOptions<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new QuestionOptions<DataCollectionType, DataRecordType>()));
		}
		*/

		
		public override IDataReader GetDataReader()
		{
			return new QuestionOptionsDataReader(this);
		}
		
		public List<DtoQuestionOption> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class QuestionOptionsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public QuestionOptionsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoQuestionOption
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		/// </summary>
		[DataMember]
		public virtual int ObjectID { get; set; }
		
		/// <summary>
		///		CreateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime CreateDate { get; set; }
		
		/// <summary>
		///		DeleteDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime DeleteDate { get; set; }
		
		/// <summary>
		///		IsCorrect Property
		/// </summary>
		[DataMember]
		public virtual int IsCorrect { get; set; }
		
		/// <summary>
		///		Text Property
		/// </summary>
		[DataMember]
		public virtual string Text { get; set; }
		
		/// <summary>
		///		Reason Property
		/// </summary>
		[DataMember]
		public virtual string Reason { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class QuestionOption : QuestionOption<QuestionOption>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates QuestionOption
		/// </summary>
		public QuestionOption() : this(new Context()) {}
		
		/// <summary>
		/// Creates QuestionOption
		/// </summary>
		public QuestionOption(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for QuestionOption
		/// </summary>
		protected QuestionOption(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class QuestionOption<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: QuestionOption<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates QuestionOption
		/// </summary>
		public QuestionOption() : this(new Context()) {}
		
		/// <summary>
		/// Creates QuestionOption
		/// </summary>
		public QuestionOption(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_question_options";

			//Declare Fields
			this.Fields.Add("ObjectID" ,this._ObjectID = new jlib.DataFramework.BaseField<int>(this, QuestionOptions.Columns.ObjectID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreateDate" ,this._CreateDate = new jlib.DataFramework.BaseField<DateTime>(this, QuestionOptions.Columns.CreateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("DeleteDate" ,this._DeleteDate = new jlib.DataFramework.BaseField<DateTime>(this, QuestionOptions.Columns.DeleteDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("IsCorrect" ,this._IsCorrect = new jlib.DataFramework.BaseField<int>(this, QuestionOptions.Columns.IsCorrect, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Text" ,this._Text = new jlib.DataFramework.BaseField<string>(this, QuestionOptions.Columns.Text, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Reason" ,this._Reason = new jlib.DataFramework.BaseField<string>(this, QuestionOptions.Columns.Reason, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.QuestionOptionEx();
		}
		
		/// <summary>
		/// Deserialization method for QuestionOption
		/// </summary>
		protected QuestionOption(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void QuestionOptionEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		///		Database Field:		ObjectID;
		///		Database Parameter:	@Objectid;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_question_options", FieldName: "ObjectID", IsPrimaryKey=true)]
		public virtual int ObjectID
		{
			get 
			{ 
				return this._ObjectID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ObjectID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ObjectID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ObjectID { get; set; }

			
		/// <summary>
		///		CreateDate Property
		///		Database Field:		CreateDate;
		///		Database Parameter:	@Createdate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_question_options", FieldName: "CreateDate", IsPrimaryKey=true)]
		public virtual DateTime CreateDate
		{
			get 
			{ 
				return this._CreateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CreateDate { get; set; }

			
		/// <summary>
		///		DeleteDate Property
		///		Database Field:		DeleteDate;
		///		Database Parameter:	@Deletedate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_question_options", FieldName: "DeleteDate", IsPrimaryKey=true)]
		public virtual DateTime DeleteDate
		{
			get 
			{ 
				return this._DeleteDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._DeleteDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._DeleteDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _DeleteDate { get; set; }

			
		/// <summary>
		///		IsCorrect Property
		///		Database Field:		IsCorrect;
		///		Database Parameter:	@Iscorrect;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_question_options", FieldName: "IsCorrect")]
		public virtual int IsCorrect
		{
			get 
			{ 
				return this._IsCorrect.Value; 
			}

			set 
			{ 
				//Set Value
				this._IsCorrect.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._IsCorrect.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _IsCorrect { get; set; }

			
		/// <summary>
		///		Text Property
		///		Database Field:		Text;
		///		Database Parameter:	@Text;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_question_options", FieldName: "Text", IsNullable=true)]
		public virtual string Text
		{
			get 
			{ 
				return (string)this._Text.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Text.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Text.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Text { get; set; }

			
		/// <summary>
		///		Reason Property
		///		Database Field:		Reason;
		///		Database Parameter:	@Reason;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_question_options", FieldName: "Reason", IsNullable=true)]
		public virtual string Reason
		{
			get 
			{ 
				return (string)this._Reason.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Reason.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Reason.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Reason { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoQuestionOption Dto)
		{
			//Set Values
			this._ObjectID.SetChangedValueIfDifferent(Dto.ObjectID);
			this._CreateDate.SetChangedValueIfDifferent(Dto.CreateDate);
			this._DeleteDate.SetChangedValueIfDifferent(Dto.DeleteDate);
			this._IsCorrect.SetChangedValueIfDifferent(Dto.IsCorrect);
			this._Text.SetChangedValueIfDifferent(Dto.Text);
			this._Reason.SetChangedValueIfDifferent(Dto.Reason);
		}
		public virtual DtoQuestionOption ToDto()
		{
			return this.ToDto(new DtoQuestionOption());
		}
		
		public virtual DtoQuestionOption ToDto(DtoQuestionOption dto)
		{
			//Set Values
			dto.ObjectID = this.ObjectID;
			dto.CreateDate = this.CreateDate;
			dto.DeleteDate = this.DeleteDate;
			dto.IsCorrect = this.IsCorrect;
			dto.Text = this.Text;
			dto.Reason = this.Reason;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****

				public static DataRecordType LoadByPk(int ObjectID)
				{
					Context oContext=new Context();
					//Create Sql Connection
					SqlHelper sqlHelper = oContext.GetSqlHelper();
			
					//Load
					DataRecordType item = LoadByPk(sqlHelper, ObjectID, oContext.VersionDate);
			
					//Return
					return item;
				}

		public static DataRecordType LoadByPk(int ObjectID,  DateTime ObjectDate)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ObjectID, ObjectDate);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ObjectID,  DateTime ObjectDate)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_question_options]  WITH (NOLOCK)
										WHERE
											
											[ObjectID] = @PK_Objectid		AND @ObjectDate BETWEEN [CreateDate] AND [DeleteDate]

											",
											"@PK_Objectid", ObjectID, "@ObjectDate", ObjectDate
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class Questions : Questions<Questions, Question>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Questions
		/// </summary>
		public Questions() : this(new Context()) {}
		
		/// <summary>
		/// Creates Questions
		/// </summary>
		public Questions(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Questions<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: Question<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ObjectID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ObjectID", PropertyName: "ObjectID", ParameterName: "@Objectid", PkParameterName: "@PK_Objectid", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CreateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CreateDate", PropertyName: "CreateDate", ParameterName: "@Createdate", PkParameterName: "@PK_Createdate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> DeleteDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "DeleteDate", PropertyName: "DeleteDate", ParameterName: "@Deletedate", PkParameterName: "@PK_Deletedate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> QuestionType = new jlib.DataFramework.BaseColumn<string>(FieldName: "QuestionType", PropertyName: "QuestionType", ParameterName: "@Questiontype", PkParameterName: "@PK_Questiontype", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Text = new jlib.DataFramework.BaseColumn<string>(FieldName: "Text", PropertyName: "Text", ParameterName: "@Text", PkParameterName: "@PK_Text", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> SubText = new jlib.DataFramework.BaseColumn<string>(FieldName: "SubText", PropertyName: "SubText", ParameterName: "@Subtext", PkParameterName: "@PK_Subtext", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Reason = new jlib.DataFramework.BaseColumn<string>(FieldName: "Reason", PropertyName: "Reason", ParameterName: "@Reason", PkParameterName: "@PK_Reason", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<decimal> Points = new jlib.DataFramework.BaseColumn<decimal>(FieldName: "Points", PropertyName: "Points", ParameterName: "@Points", PkParameterName: "@PK_Points", DBType: DbType.Currency, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Hint = new jlib.DataFramework.BaseColumn<string>(FieldName: "Hint", PropertyName: "Hint", ParameterName: "@Hint", PkParameterName: "@PK_Hint", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> ForeignKey = new jlib.DataFramework.BaseColumn<string>(FieldName: "ForeignKey", PropertyName: "ForeignKey", ParameterName: "@Foreignkey", PkParameterName: "@PK_Foreignkey", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Reference = new jlib.DataFramework.BaseColumn<string>(FieldName: "Reference", PropertyName: "Reference", ParameterName: "@Reference", PkParameterName: "@PK_Reference", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Domain = new jlib.DataFramework.BaseColumn<string>(FieldName: "Domain", PropertyName: "Domain", ParameterName: "@Domain", PkParameterName: "@PK_Domain", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> KnowledgeArea = new jlib.DataFramework.BaseColumn<string>(FieldName: "KnowledgeArea", PropertyName: "KnowledgeArea", ParameterName: "@Knowledgearea", PkParameterName: "@PK_Knowledgearea", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Questions
		/// </summary>
		public Questions() : this(new Context()) {}
		
		/// <summary>
		/// Creates Questions
		/// </summary>
		public Questions(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_questions";
			
			//Set Columns
			base.Columns.Add("ObjectID", this.ObjectID = Questions<DataCollectionType, DataRecordType>.Columns.ObjectID);
			base.Columns.Add("CreateDate", this.CreateDate = Questions<DataCollectionType, DataRecordType>.Columns.CreateDate);
			base.Columns.Add("DeleteDate", this.DeleteDate = Questions<DataCollectionType, DataRecordType>.Columns.DeleteDate);
			base.Columns.Add("QuestionType", this.QuestionType = Questions<DataCollectionType, DataRecordType>.Columns.QuestionType);
			base.Columns.Add("Text", this.Text = Questions<DataCollectionType, DataRecordType>.Columns.Text);
			base.Columns.Add("SubText", this.SubText = Questions<DataCollectionType, DataRecordType>.Columns.SubText);
			base.Columns.Add("Reason", this.Reason = Questions<DataCollectionType, DataRecordType>.Columns.Reason);
			base.Columns.Add("Points", this.Points = Questions<DataCollectionType, DataRecordType>.Columns.Points);
			base.Columns.Add("Hint", this.Hint = Questions<DataCollectionType, DataRecordType>.Columns.Hint);
			base.Columns.Add("ForeignKey", this.ForeignKey = Questions<DataCollectionType, DataRecordType>.Columns.ForeignKey);
			base.Columns.Add("Reference", this.Reference = Questions<DataCollectionType, DataRecordType>.Columns.Reference);
			base.Columns.Add("Domain", this.Domain = Questions<DataCollectionType, DataRecordType>.Columns.Domain);
			base.Columns.Add("KnowledgeArea", this.KnowledgeArea = Questions<DataCollectionType, DataRecordType>.Columns.KnowledgeArea);
			
			//Call the secondary constructor method
			this.QuestionsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void QuestionsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ObjectID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CreateDate { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> DeleteDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> QuestionType { get; set; }
		public jlib.DataFramework.BaseColumn<string> Text { get; set; }
		public jlib.DataFramework.BaseColumn<string> SubText { get; set; }
		public jlib.DataFramework.BaseColumn<string> Reason { get; set; }
		public jlib.DataFramework.BaseColumn<decimal> Points { get; set; }
		public jlib.DataFramework.BaseColumn<string> Hint { get; set; }
		public jlib.DataFramework.BaseColumn<string> ForeignKey { get; set; }
		public jlib.DataFramework.BaseColumn<string> Reference { get; set; }
		public jlib.DataFramework.BaseColumn<string> Domain { get; set; }
		public jlib.DataFramework.BaseColumn<string> KnowledgeArea { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_questions] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_questions] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_questions");
		}
		
		
		public static QueryPackage GetByField(int? ObjectID = null, DateTime? CreateDate = null, DateTime? DeleteDate = null, string QuestionType = null, string Text = null, string SubText = null, string Reason = null, decimal? Points = null, string Hint = null, string ForeignKey = null, string Reference = null, string Domain = null, string KnowledgeArea = null, DateTime? ObjectDate = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ObjectID != null)
			{
				query.Parameters.ObjectID = ObjectID;
				filterConditions.Add(@"[ObjectID] = @Objectid");
			}	
			if(CreateDate != null)
			{
				query.Parameters.CreateDate = CreateDate;
				filterConditions.Add(@"[CreateDate] = @Createdate");
			}	
			if(DeleteDate != null)
			{
				query.Parameters.DeleteDate = DeleteDate;
				filterConditions.Add(@"[DeleteDate] = @Deletedate");
			}	
			if(QuestionType != null)
			{
				query.Parameters.QuestionType = QuestionType;
				filterConditions.Add(@"[QuestionType] = @Questiontype");
			}	
			if(Text != null)
			{
				query.Parameters.Text = Text;
				filterConditions.Add(@"[Text] LIKE @Text");
			}	
			if(SubText != null)
			{
				query.Parameters.SubText = SubText;
				filterConditions.Add(@"[SubText] LIKE @Subtext");
			}	
			if(Reason != null)
			{
				query.Parameters.Reason = Reason;
				filterConditions.Add(@"[Reason] LIKE @Reason");
			}	
			if(Points != null)
			{
				query.Parameters.Points = Points;
				filterConditions.Add(@"[Points] = @Points");
			}	
			if(Hint != null)
			{
				query.Parameters.Hint = Hint;
				filterConditions.Add(@"[Hint] = @Hint");
			}	
			if(ForeignKey != null)
			{
				query.Parameters.ForeignKey = ForeignKey;
				filterConditions.Add(@"[ForeignKey] = @Foreignkey");
			}	
			if(Reference != null)
			{
				query.Parameters.Reference = Reference;
				filterConditions.Add(@"[Reference] = @Reference");
			}	
			if(Domain != null)
			{
				query.Parameters.Domain = Domain;
				filterConditions.Add(@"[Domain] = @Domain");
			}	
			if(KnowledgeArea != null)
			{
				query.Parameters.KnowledgeArea = KnowledgeArea;
				filterConditions.Add(@"[KnowledgeArea] = @Knowledgearea");
			}	
			if(ObjectDate != null || (CreateDate==null && DeleteDate==null))
			{
				Context oContext=new Context();
				query.Parameters.ObjectDate = (ObjectDate==null ? oContext.VersionDate : ObjectDate);
				filterConditions.Add(@"@ObjectDate BETWEEN [CreateDate] AND [DeleteDate]");
			}
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_questions] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Questions<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Questions<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Questions<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Questions<DataCollectionType, DataRecordType>()));
		}
		*/

		
		public override IDataReader GetDataReader()
		{
			return new QuestionsDataReader(this);
		}
		
		public List<DtoQuestion> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class QuestionsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public QuestionsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoQuestion
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		/// </summary>
		[DataMember]
		public virtual int ObjectID { get; set; }
		
		/// <summary>
		///		CreateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime CreateDate { get; set; }
		
		/// <summary>
		///		DeleteDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime DeleteDate { get; set; }
		
		/// <summary>
		///		QuestionType Property
		/// </summary>
		[DataMember]
		public virtual string QuestionType { get; set; }
		
		/// <summary>
		///		Text Property
		/// </summary>
		[DataMember]
		public virtual string Text { get; set; }
		
		/// <summary>
		///		SubText Property
		/// </summary>
		[DataMember]
		public virtual string SubText { get; set; }
		
		/// <summary>
		///		Reason Property
		/// </summary>
		[DataMember]
		public virtual string Reason { get; set; }
		
		/// <summary>
		///		Points Property
		/// </summary>
		[DataMember]
		public virtual decimal Points { get; set; }
		
		/// <summary>
		///		Hint Property
		/// </summary>
		[DataMember]
		public virtual string Hint { get; set; }
		
		/// <summary>
		///		ForeignKey Property
		/// </summary>
		[DataMember]
		public virtual string ForeignKey { get; set; }
		
		/// <summary>
		///		Reference Property
		/// </summary>
		[DataMember]
		public virtual string Reference { get; set; }
		
		/// <summary>
		///		Domain Property
		/// </summary>
		[DataMember]
		public virtual string Domain { get; set; }
		
		/// <summary>
		///		KnowledgeArea Property
		/// </summary>
		[DataMember]
		public virtual string KnowledgeArea { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class Question : Question<Question>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Question
		/// </summary>
		public Question() : this(new Context()) {}
		
		/// <summary>
		/// Creates Question
		/// </summary>
		public Question(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for Question
		/// </summary>
		protected Question(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class Question<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: Question<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Question
		/// </summary>
		public Question() : this(new Context()) {}
		
		/// <summary>
		/// Creates Question
		/// </summary>
		public Question(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_questions";

			//Declare Fields
			this.Fields.Add("ObjectID" ,this._ObjectID = new jlib.DataFramework.BaseField<int>(this, Questions.Columns.ObjectID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreateDate" ,this._CreateDate = new jlib.DataFramework.BaseField<DateTime>(this, Questions.Columns.CreateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("DeleteDate" ,this._DeleteDate = new jlib.DataFramework.BaseField<DateTime>(this, Questions.Columns.DeleteDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("QuestionType" ,this._QuestionType = new jlib.DataFramework.BaseField<string>(this, Questions.Columns.QuestionType, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Text" ,this._Text = new jlib.DataFramework.BaseField<string>(this, Questions.Columns.Text, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("SubText" ,this._SubText = new jlib.DataFramework.BaseField<string>(this, Questions.Columns.SubText, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Reason" ,this._Reason = new jlib.DataFramework.BaseField<string>(this, Questions.Columns.Reason, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Points" ,this._Points = new jlib.DataFramework.BaseField<decimal>(this, Questions.Columns.Points, InitialValue: 0.00M, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Hint" ,this._Hint = new jlib.DataFramework.BaseField<string>(this, Questions.Columns.Hint, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ForeignKey" ,this._ForeignKey = new jlib.DataFramework.BaseField<string>(this, Questions.Columns.ForeignKey, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Reference" ,this._Reference = new jlib.DataFramework.BaseField<string>(this, Questions.Columns.Reference, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Domain" ,this._Domain = new jlib.DataFramework.BaseField<string>(this, Questions.Columns.Domain, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("KnowledgeArea" ,this._KnowledgeArea = new jlib.DataFramework.BaseField<string>(this, Questions.Columns.KnowledgeArea, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.QuestionEx();
		}
		
		/// <summary>
		/// Deserialization method for Question
		/// </summary>
		protected Question(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void QuestionEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		///		Database Field:		ObjectID;
		///		Database Parameter:	@Objectid;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_questions", FieldName: "ObjectID", IsPrimaryKey=true)]
		public virtual int ObjectID
		{
			get 
			{ 
				return this._ObjectID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ObjectID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ObjectID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ObjectID { get; set; }

			
		/// <summary>
		///		CreateDate Property
		///		Database Field:		CreateDate;
		///		Database Parameter:	@Createdate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_questions", FieldName: "CreateDate", IsPrimaryKey=true)]
		public virtual DateTime CreateDate
		{
			get 
			{ 
				return this._CreateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CreateDate { get; set; }

			
		/// <summary>
		///		DeleteDate Property
		///		Database Field:		DeleteDate;
		///		Database Parameter:	@Deletedate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_questions", FieldName: "DeleteDate", IsPrimaryKey=true)]
		public virtual DateTime DeleteDate
		{
			get 
			{ 
				return this._DeleteDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._DeleteDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._DeleteDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _DeleteDate { get; set; }

			
		/// <summary>
		///		QuestionType Property
		///		Database Field:		QuestionType;
		///		Database Parameter:	@Questiontype;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_questions", FieldName: "QuestionType")]
		public virtual string QuestionType
		{
			get 
			{ 
				return this._QuestionType.Value; 
			}

			set 
			{ 
				//Set Value
				this._QuestionType.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._QuestionType.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _QuestionType { get; set; }

			
		/// <summary>
		///		Text Property
		///		Database Field:		Text;
		///		Database Parameter:	@Text;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_questions", FieldName: "Text", IsNullable=true)]
		public virtual string Text
		{
			get 
			{ 
				return (string)this._Text.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Text.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Text.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Text { get; set; }

			
		/// <summary>
		///		SubText Property
		///		Database Field:		SubText;
		///		Database Parameter:	@Subtext;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_questions", FieldName: "SubText", IsNullable=true)]
		public virtual string SubText
		{
			get 
			{ 
				return (string)this._SubText.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._SubText.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._SubText.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _SubText { get; set; }

			
		/// <summary>
		///		Reason Property
		///		Database Field:		Reason;
		///		Database Parameter:	@Reason;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_questions", FieldName: "Reason", IsNullable=true)]
		public virtual string Reason
		{
			get 
			{ 
				return (string)this._Reason.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Reason.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Reason.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Reason { get; set; }

			
		/// <summary>
		///		Points Property
		///		Database Field:		Points;
		///		Database Parameter:	@Points;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_questions", FieldName: "Points")]
		public virtual decimal Points
		{
			get 
			{ 
				return this._Points.Value; 
			}

			set 
			{ 
				//Set Value
				this._Points.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Points.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<decimal> _Points { get; set; }

			
		/// <summary>
		///		Hint Property
		///		Database Field:		Hint;
		///		Database Parameter:	@Hint;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_questions", FieldName: "Hint", IsNullable=true)]
		public virtual string Hint
		{
			get 
			{ 
				return (string)this._Hint.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Hint.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Hint.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Hint { get; set; }

			
		/// <summary>
		///		ForeignKey Property
		///		Database Field:		ForeignKey;
		///		Database Parameter:	@Foreignkey;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_questions", FieldName: "ForeignKey", IsNullable=true)]
		public virtual string ForeignKey
		{
			get 
			{ 
				return (string)this._ForeignKey.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._ForeignKey.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ForeignKey.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _ForeignKey { get; set; }

			
		/// <summary>
		///		Reference Property
		///		Database Field:		Reference;
		///		Database Parameter:	@Reference;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_questions", FieldName: "Reference", IsNullable=true)]
		public virtual string Reference
		{
			get 
			{ 
				return (string)this._Reference.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Reference.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Reference.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Reference { get; set; }

			
		/// <summary>
		///		Domain Property
		///		Database Field:		Domain;
		///		Database Parameter:	@Domain;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_questions", FieldName: "Domain", IsNullable=true)]
		public virtual string Domain
		{
			get 
			{ 
				return (string)this._Domain.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Domain.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Domain.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Domain { get; set; }

			
		/// <summary>
		///		KnowledgeArea Property
		///		Database Field:		KnowledgeArea;
		///		Database Parameter:	@Knowledgearea;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_questions", FieldName: "KnowledgeArea", IsNullable=true)]
		public virtual string KnowledgeArea
		{
			get 
			{ 
				return (string)this._KnowledgeArea.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._KnowledgeArea.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._KnowledgeArea.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _KnowledgeArea { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoQuestion Dto)
		{
			//Set Values
			this._ObjectID.SetChangedValueIfDifferent(Dto.ObjectID);
			this._CreateDate.SetChangedValueIfDifferent(Dto.CreateDate);
			this._DeleteDate.SetChangedValueIfDifferent(Dto.DeleteDate);
			this._QuestionType.SetChangedValueIfDifferent(Dto.QuestionType);
			this._Text.SetChangedValueIfDifferent(Dto.Text);
			this._SubText.SetChangedValueIfDifferent(Dto.SubText);
			this._Reason.SetChangedValueIfDifferent(Dto.Reason);
			this._Points.SetChangedValueIfDifferent(Dto.Points);
			this._Hint.SetChangedValueIfDifferent(Dto.Hint);
			this._ForeignKey.SetChangedValueIfDifferent(Dto.ForeignKey);
			this._Reference.SetChangedValueIfDifferent(Dto.Reference);
			this._Domain.SetChangedValueIfDifferent(Dto.Domain);
			this._KnowledgeArea.SetChangedValueIfDifferent(Dto.KnowledgeArea);
		}
		public virtual DtoQuestion ToDto()
		{
			return this.ToDto(new DtoQuestion());
		}
		
		public virtual DtoQuestion ToDto(DtoQuestion dto)
		{
			//Set Values
			dto.ObjectID = this.ObjectID;
			dto.CreateDate = this.CreateDate;
			dto.DeleteDate = this.DeleteDate;
			dto.QuestionType = this.QuestionType;
			dto.Text = this.Text;
			dto.SubText = this.SubText;
			dto.Reason = this.Reason;
			dto.Points = this.Points;
			dto.Hint = this.Hint;
			dto.ForeignKey = this.ForeignKey;
			dto.Reference = this.Reference;
			dto.Domain = this.Domain;
			dto.KnowledgeArea = this.KnowledgeArea;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****

				public static DataRecordType LoadByPk(int ObjectID)
				{
					Context oContext=new Context();
					//Create Sql Connection
					SqlHelper sqlHelper = oContext.GetSqlHelper();
			
					//Load
					DataRecordType item = LoadByPk(sqlHelper, ObjectID, oContext.VersionDate);
			
					//Return
					return item;
				}

		public static DataRecordType LoadByPk(int ObjectID,  DateTime ObjectDate)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ObjectID, ObjectDate);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ObjectID,  DateTime ObjectDate)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_questions]  WITH (NOLOCK)
										WHERE
											
											[ObjectID] = @PK_Objectid		AND @ObjectDate BETWEEN [CreateDate] AND [DeleteDate]

											",
											"@PK_Objectid", ObjectID, "@ObjectDate", ObjectDate
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class Releases : Releases<Releases, Release>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Releases
		/// </summary>
		public Releases() : this(new Context()) {}
		
		/// <summary>
		/// Creates Releases
		/// </summary>
		public Releases(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Releases<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: Release<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CreateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CreateDate", PropertyName: "CreateDate", ParameterName: "@Createdate", PkParameterName: "@PK_Createdate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<bool> Deleted = new jlib.DataFramework.BaseColumn<bool>(FieldName: "Deleted", PropertyName: "Deleted", ParameterName: "@Deleted", PkParameterName: "@PK_Deleted", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Language = new jlib.DataFramework.BaseColumn<string>(FieldName: "Language", PropertyName: "Language", ParameterName: "@Language", PkParameterName: "@PK_Language", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> MaxTime = new jlib.DataFramework.BaseColumn<int>(FieldName: "MaxTime", PropertyName: "MaxTime", ParameterName: "@Maxtime", PkParameterName: "@PK_Maxtime", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Type = new jlib.DataFramework.BaseColumn<string>(FieldName: "Type", PropertyName: "Type", ParameterName: "@Type", PkParameterName: "@PK_Type", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Name = new jlib.DataFramework.BaseColumn<string>(FieldName: "Name", PropertyName: "Name", ParameterName: "@Name", PkParameterName: "@PK_Name", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> ObjectID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ObjectID", PropertyName: "ObjectID", ParameterName: "@Objectid", PkParameterName: "@PK_Objectid", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> ObjectDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "ObjectDate", PropertyName: "ObjectDate", ParameterName: "@Objectdate", PkParameterName: "@PK_Objectdate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<bool> IsResumable = new jlib.DataFramework.BaseColumn<bool>(FieldName: "IsResumable", PropertyName: "IsResumable", ParameterName: "@Isresumable", PkParameterName: "@PK_Isresumable", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> IlearnOfferingID = new jlib.DataFramework.BaseColumn<int>(FieldName: "IlearnOfferingID", PropertyName: "IlearnOfferingID", ParameterName: "@Ilearnofferingid", PkParameterName: "@PK_Ilearnofferingid", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<DateTime> StartDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "StartDate", PropertyName: "StartDate", ParameterName: "@Startdate", PkParameterName: "@PK_Startdate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> EndDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "EndDate", PropertyName: "EndDate", ParameterName: "@Enddate", PkParameterName: "@PK_Enddate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> UrlKey = new jlib.DataFramework.BaseColumn<string>(FieldName: "UrlKey", PropertyName: "UrlKey", ParameterName: "@Urlkey", PkParameterName: "@PK_Urlkey", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Releases
		/// </summary>
		public Releases() : this(new Context()) {}
		
		/// <summary>
		/// Creates Releases
		/// </summary>
		public Releases(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_releases";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = Releases<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("CreateDate", this.CreateDate = Releases<DataCollectionType, DataRecordType>.Columns.CreateDate);
			base.Columns.Add("Deleted", this.Deleted = Releases<DataCollectionType, DataRecordType>.Columns.Deleted);
			base.Columns.Add("Language", this.Language = Releases<DataCollectionType, DataRecordType>.Columns.Language);
			base.Columns.Add("MaxTime", this.MaxTime = Releases<DataCollectionType, DataRecordType>.Columns.MaxTime);
			base.Columns.Add("Type", this.Type = Releases<DataCollectionType, DataRecordType>.Columns.Type);
			base.Columns.Add("Name", this.Name = Releases<DataCollectionType, DataRecordType>.Columns.Name);
			base.Columns.Add("ObjectID", this.ObjectID = Releases<DataCollectionType, DataRecordType>.Columns.ObjectID);
			base.Columns.Add("ObjectDate", this.ObjectDate = Releases<DataCollectionType, DataRecordType>.Columns.ObjectDate);
			base.Columns.Add("IsResumable", this.IsResumable = Releases<DataCollectionType, DataRecordType>.Columns.IsResumable);
			base.Columns.Add("IlearnOfferingID", this.IlearnOfferingID = Releases<DataCollectionType, DataRecordType>.Columns.IlearnOfferingID);
			base.Columns.Add("StartDate", this.StartDate = Releases<DataCollectionType, DataRecordType>.Columns.StartDate);
			base.Columns.Add("EndDate", this.EndDate = Releases<DataCollectionType, DataRecordType>.Columns.EndDate);
			base.Columns.Add("UrlKey", this.UrlKey = Releases<DataCollectionType, DataRecordType>.Columns.UrlKey);
			
			//Call the secondary constructor method
			this.ReleasesEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void ReleasesEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CreateDate { get; set; }
		public jlib.DataFramework.BaseColumn<bool> Deleted { get; set; }
		public jlib.DataFramework.BaseColumn<string> Language { get; set; }
		public jlib.DataFramework.BaseColumn<int> MaxTime { get; set; }
		public jlib.DataFramework.BaseColumn<string> Type { get; set; }
		public jlib.DataFramework.BaseColumn<string> Name { get; set; }
		public jlib.DataFramework.BaseColumn<int> ObjectID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> ObjectDate { get; set; }
		public jlib.DataFramework.BaseColumn<bool> IsResumable { get; set; }
		public jlib.DataFramework.BaseColumn<int> IlearnOfferingID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> StartDate { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> EndDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> UrlKey { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_releases] WITH (NOLOCK) WHERE [Deleted]=0", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_releases] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_releases");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_releases", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, DateTime? CreateDate = null, bool? Deleted = null, string Language = null, int? MaxTime = null, string Type = null, string Name = null, int? ObjectID = null, DateTime? ObjectDate = null, bool? IsResumable = null, int? IlearnOfferingID = null, DateTime? StartDate = null, DateTime? EndDate = null, string UrlKey = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
			if(Deleted == null)
			{
				filterConditions.Add(@"[Deleted]=0");
			}
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(CreateDate != null)
			{
				query.Parameters.CreateDate = CreateDate;
				filterConditions.Add(@"[CreateDate] = @Createdate");
			}	
			if(Deleted != null)
			{
				query.Parameters.Deleted = Deleted;
				filterConditions.Add(@"[Deleted] = @Deleted");
			}	
			if(Language != null)
			{
				query.Parameters.Language = Language;
				filterConditions.Add(@"[Language] = @Language");
			}	
			if(MaxTime != null)
			{
				query.Parameters.MaxTime = MaxTime;
				filterConditions.Add(@"[MaxTime] = @Maxtime");
			}	
			if(Type != null)
			{
				query.Parameters.Type = Type;
				filterConditions.Add(@"[Type] = @Type");
			}	
			if(Name != null)
			{
				query.Parameters.Name = Name;
				filterConditions.Add(@"[Name] = @Name");
			}	
			if(ObjectID != null)
			{
				query.Parameters.ObjectID = ObjectID;
				filterConditions.Add(@"[ObjectID] = @Objectid");
			}	
			if(ObjectDate != null)
			{
				query.Parameters.ObjectDate = ObjectDate;
				filterConditions.Add(@"[ObjectDate] = @Objectdate");
			}	
			if(IsResumable != null)
			{
				query.Parameters.IsResumable = IsResumable;
				filterConditions.Add(@"[IsResumable] = @Isresumable");
			}	
			if(IlearnOfferingID != null)
			{
				query.Parameters.IlearnOfferingID = IlearnOfferingID;
				filterConditions.Add(@"[IlearnOfferingID] = @Ilearnofferingid");
			}	
			if(StartDate != null)
			{
				query.Parameters.StartDate = StartDate;
				filterConditions.Add(@"[StartDate] = @Startdate");
			}	
			if(EndDate != null)
			{
				query.Parameters.EndDate = EndDate;
				filterConditions.Add(@"[EndDate] = @Enddate");
			}	
			if(UrlKey != null)
			{
				query.Parameters.UrlKey = UrlKey;
				filterConditions.Add(@"[UrlKey] = @Urlkey");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_releases] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Releases<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Releases<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Releases<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Releases<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_releases] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new ReleasesDataReader(this);
		}
		
		public List<DtoRelease> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class ReleasesDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public ReleasesDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoRelease
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		CreateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime CreateDate { get; set; }
		
		/// <summary>
		///		Deleted Property
		/// </summary>
		[DataMember]
		public virtual bool Deleted { get; set; }
		
		/// <summary>
		///		Language Property
		/// </summary>
		[DataMember]
		public virtual string Language { get; set; }
		
		/// <summary>
		///		MaxTime Property
		/// </summary>
		[DataMember]
		public virtual int MaxTime { get; set; }
		
		/// <summary>
		///		Type Property
		/// </summary>
		[DataMember]
		public virtual string Type { get; set; }
		
		/// <summary>
		///		Name Property
		/// </summary>
		[DataMember]
		public virtual string Name { get; set; }
		
		/// <summary>
		///		ObjectID Property
		/// </summary>
		[DataMember]
		public virtual int ObjectID { get; set; }
		
		/// <summary>
		///		ObjectDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime ObjectDate { get; set; }
		
		/// <summary>
		///		IsResumable Property
		/// </summary>
		[DataMember]
		public virtual bool IsResumable { get; set; }
		
		/// <summary>
		///		IlearnOfferingID Property
		/// </summary>
		[DataMember]
		public virtual int IlearnOfferingID { get; set; }
		
		/// <summary>
		///		StartDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime? StartDate { get; set; }
		
		/// <summary>
		///		EndDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime? EndDate { get; set; }
		
		/// <summary>
		///		UrlKey Property
		/// </summary>
		[DataMember]
		public virtual string UrlKey { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class Release : Release<Release>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Release
		/// </summary>
		public Release() : this(new Context()) {}
		
		/// <summary>
		/// Creates Release
		/// </summary>
		public Release(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for Release
		/// </summary>
		protected Release(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class Release<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: Release<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Release
		/// </summary>
		public Release() : this(new Context()) {}
		
		/// <summary>
		/// Creates Release
		/// </summary>
		public Release(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_releases";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, Releases.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreateDate" ,this._CreateDate = new jlib.DataFramework.BaseField<DateTime>(this, Releases.Columns.CreateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Deleted" ,this._Deleted = new jlib.DataFramework.BaseField<bool>(this, Releases.Columns.Deleted, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Language" ,this._Language = new jlib.DataFramework.BaseField<string>(this, Releases.Columns.Language, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("MaxTime" ,this._MaxTime = new jlib.DataFramework.BaseField<int>(this, Releases.Columns.MaxTime, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Type" ,this._Type = new jlib.DataFramework.BaseField<string>(this, Releases.Columns.Type, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Name" ,this._Name = new jlib.DataFramework.BaseField<string>(this, Releases.Columns.Name, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ObjectID" ,this._ObjectID = new jlib.DataFramework.BaseField<int>(this, Releases.Columns.ObjectID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ObjectDate" ,this._ObjectDate = new jlib.DataFramework.BaseField<DateTime>(this, Releases.Columns.ObjectDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("IsResumable" ,this._IsResumable = new jlib.DataFramework.BaseField<bool>(this, Releases.Columns.IsResumable, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("IlearnOfferingID" ,this._IlearnOfferingID = new jlib.DataFramework.BaseField<int>(this, Releases.Columns.IlearnOfferingID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("StartDate" ,this._StartDate = new jlib.DataFramework.BaseField<DateTime>(this, Releases.Columns.StartDate, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("EndDate" ,this._EndDate = new jlib.DataFramework.BaseField<DateTime>(this, Releases.Columns.EndDate, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UrlKey" ,this._UrlKey = new jlib.DataFramework.BaseField<string>(this, Releases.Columns.UrlKey, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.ReleaseEx();
		}
		
		/// <summary>
		/// Deserialization method for Release
		/// </summary>
		protected Release(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void ReleaseEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_releases", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		CreateDate Property
		///		Database Field:		CreateDate;
		///		Database Parameter:	@Createdate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_releases", FieldName: "CreateDate")]
		public virtual DateTime CreateDate
		{
			get 
			{ 
				return this._CreateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CreateDate { get; set; }

			
		/// <summary>
		///		Deleted Property
		///		Database Field:		Deleted;
		///		Database Parameter:	@Deleted;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_releases", FieldName: "Deleted")]
		public virtual bool Deleted
		{
			get 
			{ 
				return this._Deleted.Value; 
			}

			set 
			{ 
				//Set Value
				this._Deleted.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Deleted.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _Deleted { get; set; }

			
		/// <summary>
		///		Language Property
		///		Database Field:		Language;
		///		Database Parameter:	@Language;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_releases", FieldName: "Language", IsNullable=true)]
		public virtual string Language
		{
			get 
			{ 
				return (string)this._Language.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Language.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Language.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Language { get; set; }

			
		/// <summary>
		///		MaxTime Property
		///		Database Field:		MaxTime;
		///		Database Parameter:	@Maxtime;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_releases", FieldName: "MaxTime")]
		public virtual int MaxTime
		{
			get 
			{ 
				return this._MaxTime.Value; 
			}

			set 
			{ 
				//Set Value
				this._MaxTime.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._MaxTime.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _MaxTime { get; set; }

			
		/// <summary>
		///		Type Property
		///		Database Field:		Type;
		///		Database Parameter:	@Type;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_releases", FieldName: "Type")]
		public virtual string Type
		{
			get 
			{ 
				return this._Type.Value; 
			}

			set 
			{ 
				//Set Value
				this._Type.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Type.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Type { get; set; }

			
		/// <summary>
		///		Name Property
		///		Database Field:		Name;
		///		Database Parameter:	@Name;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_releases", FieldName: "Name", IsNullable=true)]
		public virtual string Name
		{
			get 
			{ 
				return (string)this._Name.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Name.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Name.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Name { get; set; }

			
		/// <summary>
		///		ObjectID Property
		///		Database Field:		ObjectID;
		///		Database Parameter:	@Objectid;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_releases", FieldName: "ObjectID")]
		public virtual int ObjectID
		{
			get 
			{ 
				return this._ObjectID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ObjectID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ObjectID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ObjectID { get; set; }

			
		/// <summary>
		///		ObjectDate Property
		///		Database Field:		ObjectDate;
		///		Database Parameter:	@Objectdate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_releases", FieldName: "ObjectDate")]
		public virtual DateTime ObjectDate
		{
			get 
			{ 
				return this._ObjectDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._ObjectDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ObjectDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _ObjectDate { get; set; }

			
		/// <summary>
		///		IsResumable Property
		///		Database Field:		IsResumable;
		///		Database Parameter:	@Isresumable;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_releases", FieldName: "IsResumable")]
		public virtual bool IsResumable
		{
			get 
			{ 
				return this._IsResumable.Value; 
			}

			set 
			{ 
				//Set Value
				this._IsResumable.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._IsResumable.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _IsResumable { get; set; }

			
		/// <summary>
		///		IlearnOfferingID Property
		///		Database Field:		IlearnOfferingID;
		///		Database Parameter:	@Ilearnofferingid;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_releases", FieldName: "IlearnOfferingID")]
		public virtual int IlearnOfferingID
		{
			get 
			{ 
				return this._IlearnOfferingID.Value; 
			}

			set 
			{ 
				//Set Value
				this._IlearnOfferingID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._IlearnOfferingID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _IlearnOfferingID { get; set; }

			
		/// <summary>
		///		StartDate Property
		///		Database Field:		StartDate;
		///		Database Parameter:	@Startdate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_releases", FieldName: "StartDate", IsNullable=true)]
		public virtual DateTime? StartDate
		{
			get 
			{ 
				return (DateTime?)this._StartDate.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._StartDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._StartDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _StartDate { get; set; }

			
		/// <summary>
		///		EndDate Property
		///		Database Field:		EndDate;
		///		Database Parameter:	@Enddate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_releases", FieldName: "EndDate", IsNullable=true)]
		public virtual DateTime? EndDate
		{
			get 
			{ 
				return (DateTime?)this._EndDate.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._EndDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._EndDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _EndDate { get; set; }

			
		/// <summary>
		///		UrlKey Property
		///		Database Field:		UrlKey;
		///		Database Parameter:	@Urlkey;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_releases", FieldName: "UrlKey", IsNullable=true)]
		public virtual string UrlKey
		{
			get 
			{ 
				return (string)this._UrlKey.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._UrlKey.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UrlKey.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _UrlKey { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoRelease Dto)
		{
			//Set Values
			this._CreateDate.SetChangedValueIfDifferent(Dto.CreateDate);
			this._Deleted.SetChangedValueIfDifferent(Dto.Deleted);
			this._Language.SetChangedValueIfDifferent(Dto.Language);
			this._MaxTime.SetChangedValueIfDifferent(Dto.MaxTime);
			this._Type.SetChangedValueIfDifferent(Dto.Type);
			this._Name.SetChangedValueIfDifferent(Dto.Name);
			this._ObjectID.SetChangedValueIfDifferent(Dto.ObjectID);
			this._ObjectDate.SetChangedValueIfDifferent(Dto.ObjectDate);
			this._IsResumable.SetChangedValueIfDifferent(Dto.IsResumable);
			this._IlearnOfferingID.SetChangedValueIfDifferent(Dto.IlearnOfferingID);
			this._StartDate.SetChangedValueIfDifferent(Dto.StartDate);
			this._EndDate.SetChangedValueIfDifferent(Dto.EndDate);
			this._UrlKey.SetChangedValueIfDifferent(Dto.UrlKey);
		}
		public virtual DtoRelease ToDto()
		{
			return this.ToDto(new DtoRelease());
		}
		
		public virtual DtoRelease ToDto(DtoRelease dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.CreateDate = this.CreateDate;
			dto.Deleted = this.Deleted;
			dto.Language = this.Language;
			dto.MaxTime = this.MaxTime;
			dto.Type = this.Type;
			dto.Name = this.Name;
			dto.ObjectID = this.ObjectID;
			dto.ObjectDate = this.ObjectDate;
			dto.IsResumable = this.IsResumable;
			dto.IlearnOfferingID = this.IlearnOfferingID;
			dto.StartDate = this.StartDate;
			dto.EndDate = this.EndDate;
			dto.UrlKey = this.UrlKey;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_releases] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_releases]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class Revisions : Revisions<Revisions, Revision>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Revisions
		/// </summary>
		public Revisions() : this(new Context()) {}
		
		/// <summary>
		/// Creates Revisions
		/// </summary>
		public Revisions(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Revisions<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: Revision<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> PostedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "posted_date", PropertyName: "PostedDate", ParameterName: "@PostedDate", PkParameterName: "@PK_PostedDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> UserID = new jlib.DataFramework.BaseColumn<int>(FieldName: "user_id", PropertyName: "UserID", ParameterName: "@UserID", PkParameterName: "@PK_UserID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> TableName = new jlib.DataFramework.BaseColumn<string>(FieldName: "table_name", PropertyName: "TableName", ParameterName: "@TableName", PkParameterName: "@PK_TableName", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> PkID = new jlib.DataFramework.BaseColumn<int>(FieldName: "pk_id", PropertyName: "PkID", ParameterName: "@PKID", PkParameterName: "@PK_PKID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Data = new jlib.DataFramework.BaseColumn<string>(FieldName: "data", PropertyName: "Data", ParameterName: "@Data", PkParameterName: "@PK_Data", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Revisions
		/// </summary>
		public Revisions() : this(new Context()) {}
		
		/// <summary>
		/// Creates Revisions
		/// </summary>
		public Revisions(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_revisions";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = Revisions<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("PostedDate", this.PostedDate = Revisions<DataCollectionType, DataRecordType>.Columns.PostedDate);
			base.Columns.Add("UserID", this.UserID = Revisions<DataCollectionType, DataRecordType>.Columns.UserID);
			base.Columns.Add("TableName", this.TableName = Revisions<DataCollectionType, DataRecordType>.Columns.TableName);
			base.Columns.Add("PkID", this.PkID = Revisions<DataCollectionType, DataRecordType>.Columns.PkID);
			base.Columns.Add("Data", this.Data = Revisions<DataCollectionType, DataRecordType>.Columns.Data);
			
			//Call the secondary constructor method
			this.RevisionsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void RevisionsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> PostedDate { get; set; }
		public jlib.DataFramework.BaseColumn<int> UserID { get; set; }
		public jlib.DataFramework.BaseColumn<string> TableName { get; set; }
		public jlib.DataFramework.BaseColumn<int> PkID { get; set; }
		public jlib.DataFramework.BaseColumn<string> Data { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_revisions] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_revisions] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_revisions");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_revisions", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, DateTime? PostedDate = null, int? UserID = null, string TableName = null, int? PkID = null, string Data = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(PostedDate != null)
			{
				query.Parameters.PostedDate = PostedDate;
				filterConditions.Add(@"[posted_date] = @PostedDate");
			}	
			if(UserID != null)
			{
				query.Parameters.UserID = UserID;
				filterConditions.Add(@"[user_id] = @UserID");
			}	
			if(TableName != null)
			{
				query.Parameters.TableName = TableName;
				filterConditions.Add(@"[table_name] = @TableName");
			}	
			if(PkID != null)
			{
				query.Parameters.PkID = PkID;
				filterConditions.Add(@"[pk_id] = @PKID");
			}	
			if(Data != null)
			{
				query.Parameters.Data = Data;
				filterConditions.Add(@"[data] LIKE @Data");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_revisions] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Revisions<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Revisions<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Revisions<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Revisions<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_revisions] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new RevisionsDataReader(this);
		}
		
		public List<DtoRevision> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class RevisionsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public RevisionsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoRevision
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		PostedDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime PostedDate { get; set; }
		
		/// <summary>
		///		UserID Property
		/// </summary>
		[DataMember]
		public virtual int UserID { get; set; }
		
		/// <summary>
		///		TableName Property
		/// </summary>
		[DataMember]
		public virtual string TableName { get; set; }
		
		/// <summary>
		///		PkID Property
		/// </summary>
		[DataMember]
		public virtual int PkID { get; set; }
		
		/// <summary>
		///		Data Property
		/// </summary>
		[DataMember]
		public virtual string Data { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class Revision : Revision<Revision>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Revision
		/// </summary>
		public Revision() : this(new Context()) {}
		
		/// <summary>
		/// Creates Revision
		/// </summary>
		public Revision(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for Revision
		/// </summary>
		protected Revision(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class Revision<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: Revision<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Revision
		/// </summary>
		public Revision() : this(new Context()) {}
		
		/// <summary>
		/// Creates Revision
		/// </summary>
		public Revision(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_revisions";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, Revisions.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostedDate" ,this._PostedDate = new jlib.DataFramework.BaseField<DateTime>(this, Revisions.Columns.PostedDate, InitialValue: DateTime.Now, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserID" ,this._UserID = new jlib.DataFramework.BaseField<int>(this, Revisions.Columns.UserID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("TableName" ,this._TableName = new jlib.DataFramework.BaseField<string>(this, Revisions.Columns.TableName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PkID" ,this._PkID = new jlib.DataFramework.BaseField<int>(this, Revisions.Columns.PkID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Data" ,this._Data = new jlib.DataFramework.BaseField<string>(this, Revisions.Columns.Data, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.RevisionEx();
		}
		
		/// <summary>
		/// Deserialization method for Revision
		/// </summary>
		protected Revision(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void RevisionEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_revisions", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		PostedDate Property
		///		Database Field:		posted_date;
		///		Database Parameter:	@PostedDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_revisions", FieldName: "posted_date")]
		public virtual DateTime PostedDate
		{
			get 
			{ 
				return this._PostedDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._PostedDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _PostedDate { get; set; }

			
		/// <summary>
		///		UserID Property
		///		Database Field:		user_id;
		///		Database Parameter:	@UserID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_revisions", FieldName: "user_id")]
		public virtual int UserID
		{
			get 
			{ 
				return this._UserID.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _UserID { get; set; }

			
		/// <summary>
		///		TableName Property
		///		Database Field:		table_name;
		///		Database Parameter:	@TableName;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_revisions", FieldName: "table_name")]
		public virtual string TableName
		{
			get 
			{ 
				return this._TableName.Value; 
			}

			set 
			{ 
				//Set Value
				this._TableName.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._TableName.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _TableName { get; set; }

			
		/// <summary>
		///		PkID Property
		///		Database Field:		pk_id;
		///		Database Parameter:	@PKID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_revisions", FieldName: "pk_id")]
		public virtual int PkID
		{
			get 
			{ 
				return this._PkID.Value; 
			}

			set 
			{ 
				//Set Value
				this._PkID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PkID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _PkID { get; set; }

			
		/// <summary>
		///		Data Property
		///		Database Field:		data;
		///		Database Parameter:	@Data;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_revisions", FieldName: "data")]
		public virtual string Data
		{
			get 
			{ 
				return this._Data.Value; 
			}

			set 
			{ 
				//Set Value
				this._Data.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Data.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Data { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoRevision Dto)
		{
			//Set Values
			this._PostedDate.SetChangedValueIfDifferent(Dto.PostedDate);
			this._UserID.SetChangedValueIfDifferent(Dto.UserID);
			this._TableName.SetChangedValueIfDifferent(Dto.TableName);
			this._PkID.SetChangedValueIfDifferent(Dto.PkID);
			this._Data.SetChangedValueIfDifferent(Dto.Data);
		}
		public virtual DtoRevision ToDto()
		{
			return this.ToDto(new DtoRevision());
		}
		
		public virtual DtoRevision ToDto(DtoRevision dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.PostedDate = this.PostedDate;
			dto.UserID = this.UserID;
			dto.TableName = this.TableName;
			dto.PkID = this.PkID;
			dto.Data = this.Data;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_revisions] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_revisions]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class Settings : Settings<Settings, Setting>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Settings
		/// </summary>
		public Settings() : this(new Context()) {}
		
		/// <summary>
		/// Creates Settings
		/// </summary>
		public Settings(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Settings<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: Setting<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ObjectID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ObjectId", PropertyName: "ObjectID", ParameterName: "@Objectid", PkParameterName: "@PK_Objectid", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CreateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CreateDate", PropertyName: "CreateDate", ParameterName: "@Createdate", PkParameterName: "@PK_Createdate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> DeleteDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "DeleteDate", PropertyName: "DeleteDate", ParameterName: "@Deletedate", PkParameterName: "@PK_Deletedate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> TableName = new jlib.DataFramework.BaseColumn<string>(FieldName: "TableName", PropertyName: "TableName", ParameterName: "@Tablename", PkParameterName: "@PK_Tablename", DBType: DbType.AnsiString, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> KeyName = new jlib.DataFramework.BaseColumn<string>(FieldName: "KeyName", PropertyName: "KeyName", ParameterName: "@Keyname", PkParameterName: "@PK_Keyname", DBType: DbType.AnsiString, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Value = new jlib.DataFramework.BaseColumn<string>(FieldName: "Value", PropertyName: "Value", ParameterName: "@Value", PkParameterName: "@PK_Value", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Settings
		/// </summary>
		public Settings() : this(new Context()) {}
		
		/// <summary>
		/// Creates Settings
		/// </summary>
		public Settings(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_settings";
			
			//Set Columns
			base.Columns.Add("ObjectID", this.ObjectID = Settings<DataCollectionType, DataRecordType>.Columns.ObjectID);
			base.Columns.Add("CreateDate", this.CreateDate = Settings<DataCollectionType, DataRecordType>.Columns.CreateDate);
			base.Columns.Add("DeleteDate", this.DeleteDate = Settings<DataCollectionType, DataRecordType>.Columns.DeleteDate);
			base.Columns.Add("TableName", this.TableName = Settings<DataCollectionType, DataRecordType>.Columns.TableName);
			base.Columns.Add("KeyName", this.KeyName = Settings<DataCollectionType, DataRecordType>.Columns.KeyName);
			base.Columns.Add("Value", this.Value = Settings<DataCollectionType, DataRecordType>.Columns.Value);
			
			//Call the secondary constructor method
			this.SettingsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void SettingsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ObjectID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CreateDate { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> DeleteDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> TableName { get; set; }
		public jlib.DataFramework.BaseColumn<string> KeyName { get; set; }
		public jlib.DataFramework.BaseColumn<string> Value { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_settings] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_settings] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_settings");
		}
		
		
		public static QueryPackage GetByField(int? ObjectID = null, DateTime? CreateDate = null, DateTime? DeleteDate = null, string TableName = null, string KeyName = null, string Value = null, DateTime? ObjectDate = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ObjectID != null)
			{
				query.Parameters.ObjectID = ObjectID;
				filterConditions.Add(@"[ObjectId] = @Objectid");
			}	
			if(CreateDate != null)
			{
				query.Parameters.CreateDate = CreateDate;
				filterConditions.Add(@"[CreateDate] = @Createdate");
			}	
			if(DeleteDate != null)
			{
				query.Parameters.DeleteDate = DeleteDate;
				filterConditions.Add(@"[DeleteDate] = @Deletedate");
			}	
			if(TableName != null)
			{
				query.Parameters.TableName = TableName;
				filterConditions.Add(@"[TableName] = @Tablename");
			}	
			if(KeyName != null)
			{
				query.Parameters.KeyName = KeyName;
				filterConditions.Add(@"[KeyName] = @Keyname");
			}	
			if(Value != null)
			{
				query.Parameters.Value = Value;
				filterConditions.Add(@"[Value] = @Value");
			}	
			if(ObjectDate != null || (CreateDate==null && DeleteDate==null))
			{
				Context oContext=new Context();
				query.Parameters.ObjectDate = (ObjectDate==null ? oContext.VersionDate : ObjectDate);
				filterConditions.Add(@"@ObjectDate BETWEEN [CreateDate] AND [DeleteDate]");
			}
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_settings] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Settings<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Settings<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Settings<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Settings<DataCollectionType, DataRecordType>()));
		}
		*/

		
		public override IDataReader GetDataReader()
		{
			return new SettingsDataReader(this);
		}
		
		public List<DtoSetting> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class SettingsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public SettingsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoSetting
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		/// </summary>
		[DataMember]
		public virtual int ObjectID { get; set; }
		
		/// <summary>
		///		CreateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime CreateDate { get; set; }
		
		/// <summary>
		///		DeleteDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime DeleteDate { get; set; }
		
		/// <summary>
		///		TableName Property
		/// </summary>
		[DataMember]
		public virtual string TableName { get; set; }
		
		/// <summary>
		///		KeyName Property
		/// </summary>
		[DataMember]
		public virtual string KeyName { get; set; }
		
		/// <summary>
		///		Value Property
		/// </summary>
		[DataMember]
		public virtual string Value { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class Setting : Setting<Setting>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Setting
		/// </summary>
		public Setting() : this(new Context()) {}
		
		/// <summary>
		/// Creates Setting
		/// </summary>
		public Setting(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for Setting
		/// </summary>
		protected Setting(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class Setting<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: Setting<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Setting
		/// </summary>
		public Setting() : this(new Context()) {}
		
		/// <summary>
		/// Creates Setting
		/// </summary>
		public Setting(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_settings";

			//Declare Fields
			this.Fields.Add("ObjectID" ,this._ObjectID = new jlib.DataFramework.BaseField<int>(this, Settings.Columns.ObjectID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreateDate" ,this._CreateDate = new jlib.DataFramework.BaseField<DateTime>(this, Settings.Columns.CreateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("DeleteDate" ,this._DeleteDate = new jlib.DataFramework.BaseField<DateTime>(this, Settings.Columns.DeleteDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("TableName" ,this._TableName = new jlib.DataFramework.BaseField<string>(this, Settings.Columns.TableName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("KeyName" ,this._KeyName = new jlib.DataFramework.BaseField<string>(this, Settings.Columns.KeyName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Value" ,this._Value = new jlib.DataFramework.BaseField<string>(this, Settings.Columns.Value, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.SettingEx();
		}
		
		/// <summary>
		/// Deserialization method for Setting
		/// </summary>
		protected Setting(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void SettingEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ObjectID Property
		///		Database Field:		ObjectId;
		///		Database Parameter:	@Objectid;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_settings", FieldName: "ObjectId", IsPrimaryKey=true)]
		public virtual int ObjectID
		{
			get 
			{ 
				return this._ObjectID.Value; 
			}

			set 
			{ 
				//Set Value
				this._ObjectID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ObjectID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ObjectID { get; set; }

			
		/// <summary>
		///		CreateDate Property
		///		Database Field:		CreateDate;
		///		Database Parameter:	@Createdate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_settings", FieldName: "CreateDate", IsPrimaryKey=true)]
		public virtual DateTime CreateDate
		{
			get 
			{ 
				return this._CreateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CreateDate { get; set; }

			
		/// <summary>
		///		DeleteDate Property
		///		Database Field:		DeleteDate;
		///		Database Parameter:	@Deletedate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_settings", FieldName: "DeleteDate", IsPrimaryKey=true)]
		public virtual DateTime DeleteDate
		{
			get 
			{ 
				return this._DeleteDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._DeleteDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._DeleteDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _DeleteDate { get; set; }

			
		/// <summary>
		///		TableName Property
		///		Database Field:		TableName;
		///		Database Parameter:	@Tablename;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_settings", FieldName: "TableName", IsPrimaryKey=true)]
		public virtual string TableName
		{
			get 
			{ 
				return this._TableName.Value; 
			}

			set 
			{ 
				//Set Value
				this._TableName.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._TableName.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _TableName { get; set; }

			
		/// <summary>
		///		KeyName Property
		///		Database Field:		KeyName;
		///		Database Parameter:	@Keyname;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_settings", FieldName: "KeyName", IsPrimaryKey=true)]
		public virtual string KeyName
		{
			get 
			{ 
				return this._KeyName.Value; 
			}

			set 
			{ 
				//Set Value
				this._KeyName.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._KeyName.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _KeyName { get; set; }

			
		/// <summary>
		///		Value Property
		///		Database Field:		Value;
		///		Database Parameter:	@Value;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_settings", FieldName: "Value", IsNullable=true)]
		public virtual string Value
		{
			get 
			{ 
				return (string)this._Value.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Value.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Value.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Value { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoSetting Dto)
		{
			//Set Values
			this._ObjectID.SetChangedValueIfDifferent(Dto.ObjectID);
			this._CreateDate.SetChangedValueIfDifferent(Dto.CreateDate);
			this._DeleteDate.SetChangedValueIfDifferent(Dto.DeleteDate);
			this._TableName.SetChangedValueIfDifferent(Dto.TableName);
			this._KeyName.SetChangedValueIfDifferent(Dto.KeyName);
			this._Value.SetChangedValueIfDifferent(Dto.Value);
		}
		public virtual DtoSetting ToDto()
		{
			return this.ToDto(new DtoSetting());
		}
		
		public virtual DtoSetting ToDto(DtoSetting dto)
		{
			//Set Values
			dto.ObjectID = this.ObjectID;
			dto.CreateDate = this.CreateDate;
			dto.DeleteDate = this.DeleteDate;
			dto.TableName = this.TableName;
			dto.KeyName = this.KeyName;
			dto.Value = this.Value;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****

				public static DataRecordType LoadByPk(int ObjectID, string TableName, string KeyName)
				{
					Context oContext=new Context();
					//Create Sql Connection
					SqlHelper sqlHelper = oContext.GetSqlHelper();
			
					//Load
					DataRecordType item = LoadByPk(sqlHelper, ObjectID, TableName, KeyName, oContext.VersionDate);
			
					//Return
					return item;
				}

		public static DataRecordType LoadByPk(int ObjectID, string TableName, string KeyName,  DateTime ObjectDate)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ObjectID, TableName, KeyName, ObjectDate);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ObjectID, string TableName, string KeyName,  DateTime ObjectDate)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_settings]  WITH (NOLOCK)
										WHERE
											
											[ObjectId] = @PK_Objectid		AND @ObjectDate BETWEEN [CreateDate] AND [DeleteDate]

											AND [TableName] = @PK_Tablename
											AND [KeyName] = @PK_Keyname
											",
											"@PK_Objectid", ObjectID, "@PK_Tablename", TableName, "@PK_Keyname", KeyName, "@ObjectDate", ObjectDate
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class Ssns : Ssns<Ssns, Ssn>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Ssns
		/// </summary>
		public Ssns() : this(new Context()) {}
		
		/// <summary>
		/// Creates Ssns
		/// </summary>
		public Ssns(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Ssns<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: Ssn<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> UserID = new jlib.DataFramework.BaseColumn<int>(FieldName: "user_id", PropertyName: "UserID", ParameterName: "@UserID", PkParameterName: "@PK_UserID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Ssn = new jlib.DataFramework.BaseColumn<string>(FieldName: "ssn", PropertyName: "Ssn", ParameterName: "@Ssn", PkParameterName: "@PK_Ssn", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Ssns
		/// </summary>
		public Ssns() : this(new Context()) {}
		
		/// <summary>
		/// Creates Ssns
		/// </summary>
		public Ssns(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_ssn";
			
			//Set Columns
			base.Columns.Add("UserID", this.UserID = Ssns<DataCollectionType, DataRecordType>.Columns.UserID);
			base.Columns.Add("Ssn", this.Ssn = Ssns<DataCollectionType, DataRecordType>.Columns.Ssn);
			
			//Call the secondary constructor method
			this.SsnsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void SsnsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> UserID { get; set; }
		public jlib.DataFramework.BaseColumn<string> Ssn { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_ssn] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_ssn] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_ssn");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_ssn", "user_id", PKs);
		}
		
		public static QueryPackage GetByField(int? UserID = null, string Ssn = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(UserID != null)
			{
				query.Parameters.UserID = UserID;
				filterConditions.Add(@"[user_id] = @UserID");
			}	
			if(Ssn != null)
			{
				query.Parameters.Ssn = Ssn;
				filterConditions.Add(@"[ssn] = @Ssn");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_ssn] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Ssns<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Ssns<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Ssns<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Ssns<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_ssn] WITH (NOLOCK) WHERE [user_id] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new SsnsDataReader(this);
		}
		
		public List<DtoSsn> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class SsnsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public SsnsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoSsn
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		UserID Property
		/// </summary>
		[DataMember]
		public virtual int UserID { get; set; }
		
		/// <summary>
		///		Ssn Property
		/// </summary>
		[DataMember]
		public virtual string Ssn_Value { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class Ssn : Ssn<Ssn>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Ssn
		/// </summary>
		public Ssn() : this(new Context()) {}
		
		/// <summary>
		/// Creates Ssn
		/// </summary>
		public Ssn(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for Ssn
		/// </summary>
		protected Ssn(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class Ssn<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: Ssn<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Ssn
		/// </summary>
		public Ssn() : this(new Context()) {}
		
		/// <summary>
		/// Creates Ssn
		/// </summary>
		public Ssn(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_ssn";

			//Declare Fields
			this.Fields.Add("UserID" ,this._UserID = new jlib.DataFramework.BaseField<int>(this, Ssns.Columns.UserID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Ssn" ,this._Ssn = new jlib.DataFramework.BaseField<string>(this, Ssns.Columns.Ssn, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.SsnEx();
		}
		
		/// <summary>
		/// Deserialization method for Ssn
		/// </summary>
		protected Ssn(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void SsnEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		UserID Property
		///		Database Field:		user_id;
		///		Database Parameter:	@UserID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_ssn", FieldName: "user_id", IsPrimaryKey=true)]
		public virtual int UserID
		{
			get 
			{ 
				return this._UserID.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _UserID { get; set; }

			
		/// <summary>
		///		Ssn Property
		///		Database Field:		ssn;
		///		Database Parameter:	@Ssn;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_ssn", FieldName: "ssn")]
		public virtual string Ssn_Value
		{
			get 
			{ 
				return this._Ssn.Value; 
			}

			set 
			{ 
				//Set Value
				this._Ssn.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Ssn.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Ssn { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoSsn Dto)
		{
			//Set Values
			this._UserID.SetChangedValueIfDifferent(Dto.UserID);
			this._Ssn.SetChangedValueIfDifferent(Dto.Ssn_Value);
		}
		public virtual DtoSsn ToDto()
		{
			return this.ToDto(new DtoSsn());
		}
		
		public virtual DtoSsn ToDto(DtoSsn dto)
		{
			//Set Values
			dto.UserID = this.UserID;
			dto.Ssn_Value = this.Ssn_Value;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****



		public static DataRecordType LoadByPk(int UserID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, UserID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int UserID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_ssn]  WITH (NOLOCK)
										WHERE
											
											[user_id] = @PK_UserID
											",
											"@PK_UserID", UserID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class TempDatas : TempDatas<TempDatas, TempData>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates TempDatas
		/// </summary>
		public TempDatas() : this(new Context()) {}
		
		/// <summary>
		/// Creates TempDatas
		/// </summary>
		public TempDatas(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class TempDatas<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: TempData<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<string> TableName = new jlib.DataFramework.BaseColumn<string>(FieldName: "table_name", PropertyName: "TableName", ParameterName: "@TableName", PkParameterName: "@PK_TableName", DBType: DbType.AnsiString, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> PkID = new jlib.DataFramework.BaseColumn<int>(FieldName: "pk_id", PropertyName: "PkID", ParameterName: "@PKID", PkParameterName: "@PK_PKID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> KeyName = new jlib.DataFramework.BaseColumn<string>(FieldName: "key_name", PropertyName: "KeyName", ParameterName: "@KeyName", PkParameterName: "@PK_KeyName", DBType: DbType.AnsiString, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Value = new jlib.DataFramework.BaseColumn<string>(FieldName: "value", PropertyName: "Value", ParameterName: "@Value", PkParameterName: "@PK_Value", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> PostedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "posted_date", PropertyName: "PostedDate", ParameterName: "@PostedDate", PkParameterName: "@PK_PostedDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<bool> IsTemporary = new jlib.DataFramework.BaseColumn<bool>(FieldName: "is_temporary", PropertyName: "IsTemporary", ParameterName: "@IsTemporary", PkParameterName: "@PK_IsTemporary", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<byte[]> BinaryValue = new jlib.DataFramework.BaseColumn<byte[]>(FieldName: "binary_value", PropertyName: "BinaryValue", ParameterName: "@BinaryValue", PkParameterName: "@PK_BinaryValue", DBType: DbType.Binary, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates TempDatas
		/// </summary>
		public TempDatas() : this(new Context()) {}
		
		/// <summary>
		/// Creates TempDatas
		/// </summary>
		public TempDatas(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_temp_data";
			
			//Set Columns
			base.Columns.Add("TableName", this.TableName = TempDatas<DataCollectionType, DataRecordType>.Columns.TableName);
			base.Columns.Add("PkID", this.PkID = TempDatas<DataCollectionType, DataRecordType>.Columns.PkID);
			base.Columns.Add("KeyName", this.KeyName = TempDatas<DataCollectionType, DataRecordType>.Columns.KeyName);
			base.Columns.Add("Value", this.Value = TempDatas<DataCollectionType, DataRecordType>.Columns.Value);
			base.Columns.Add("PostedDate", this.PostedDate = TempDatas<DataCollectionType, DataRecordType>.Columns.PostedDate);
			base.Columns.Add("IsTemporary", this.IsTemporary = TempDatas<DataCollectionType, DataRecordType>.Columns.IsTemporary);
			base.Columns.Add("BinaryValue", this.BinaryValue = TempDatas<DataCollectionType, DataRecordType>.Columns.BinaryValue);
			
			//Call the secondary constructor method
			this.TempDatasEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void TempDatasEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<string> TableName { get; set; }
		public jlib.DataFramework.BaseColumn<int> PkID { get; set; }
		public jlib.DataFramework.BaseColumn<string> KeyName { get; set; }
		public jlib.DataFramework.BaseColumn<string> Value { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> PostedDate { get; set; }
		public jlib.DataFramework.BaseColumn<bool> IsTemporary { get; set; }
		public jlib.DataFramework.BaseColumn<byte[]> BinaryValue { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_temp_data] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_temp_data] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_temp_data");
		}
		
		
		public static QueryPackage GetByField(string TableName = null, int? PkID = null, string KeyName = null, string Value = null, DateTime? PostedDate = null, bool? IsTemporary = null, byte[] BinaryValue = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(TableName != null)
			{
				query.Parameters.TableName = TableName;
				filterConditions.Add(@"[table_name] = @TableName");
			}	
			if(PkID != null)
			{
				query.Parameters.PkID = PkID;
				filterConditions.Add(@"[pk_id] = @PKID");
			}	
			if(KeyName != null)
			{
				query.Parameters.KeyName = KeyName;
				filterConditions.Add(@"[key_name] = @KeyName");
			}	
			if(Value != null)
			{
				query.Parameters.Value = Value;
				filterConditions.Add(@"[value] LIKE @Value");
			}	
			if(PostedDate != null)
			{
				query.Parameters.PostedDate = PostedDate;
				filterConditions.Add(@"[posted_date] = @PostedDate");
			}	
			if(IsTemporary != null)
			{
				query.Parameters.IsTemporary = IsTemporary;
				filterConditions.Add(@"[is_temporary] = @IsTemporary");
			}	
			if(BinaryValue != null)
			{
				query.Parameters.BinaryValue = BinaryValue;
				filterConditions.Add(@"[binary_value] = @BinaryValue");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_temp_data] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<TempDatas<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new TempDatas<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<TempDatas<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new TempDatas<DataCollectionType, DataRecordType>()));
		}
		*/

		
		public override IDataReader GetDataReader()
		{
			return new TempDatasDataReader(this);
		}
		
		public List<DtoTempData> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class TempDatasDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public TempDatasDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoTempData
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		TableName Property
		/// </summary>
		[DataMember]
		public virtual string TableName { get; set; }
		
		/// <summary>
		///		PkID Property
		/// </summary>
		[DataMember]
		public virtual int PkID { get; set; }
		
		/// <summary>
		///		KeyName Property
		/// </summary>
		[DataMember]
		public virtual string KeyName { get; set; }
		
		/// <summary>
		///		Value Property
		/// </summary>
		[DataMember]
		public virtual string Value { get; set; }
		
		/// <summary>
		///		PostedDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime PostedDate { get; set; }
		
		/// <summary>
		///		IsTemporary Property
		/// </summary>
		[DataMember]
		public virtual bool IsTemporary { get; set; }
		
		/// <summary>
		///		BinaryValue Property
		/// </summary>
		[DataMember]
		public virtual byte[] BinaryValue { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class TempData : TempData<TempData>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates TempData
		/// </summary>
		public TempData() : this(new Context()) {}
		
		/// <summary>
		/// Creates TempData
		/// </summary>
		public TempData(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for TempData
		/// </summary>
		protected TempData(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class TempData<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: TempData<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates TempData
		/// </summary>
		public TempData() : this(new Context()) {}
		
		/// <summary>
		/// Creates TempData
		/// </summary>
		public TempData(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_temp_data";

			//Declare Fields
			this.Fields.Add("TableName" ,this._TableName = new jlib.DataFramework.BaseField<string>(this, TempDatas.Columns.TableName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PkID" ,this._PkID = new jlib.DataFramework.BaseField<int>(this, TempDatas.Columns.PkID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("KeyName" ,this._KeyName = new jlib.DataFramework.BaseField<string>(this, TempDatas.Columns.KeyName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Value" ,this._Value = new jlib.DataFramework.BaseField<string>(this, TempDatas.Columns.Value, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostedDate" ,this._PostedDate = new jlib.DataFramework.BaseField<DateTime>(this, TempDatas.Columns.PostedDate, InitialValue: DateTime.Now, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("IsTemporary" ,this._IsTemporary = new jlib.DataFramework.BaseField<bool>(this, TempDatas.Columns.IsTemporary, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("BinaryValue" ,this._BinaryValue = new jlib.DataFramework.BaseField<byte[]>(this, TempDatas.Columns.BinaryValue, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.TempDataEx();
		}
		
		/// <summary>
		/// Deserialization method for TempData
		/// </summary>
		protected TempData(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void TempDataEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		TableName Property
		///		Database Field:		table_name;
		///		Database Parameter:	@TableName;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_temp_data", FieldName: "table_name", IsPrimaryKey=true)]
		public virtual string TableName
		{
			get 
			{ 
				return this._TableName.Value; 
			}

			set 
			{ 
				//Set Value
				this._TableName.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._TableName.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _TableName { get; set; }

			
		/// <summary>
		///		PkID Property
		///		Database Field:		pk_id;
		///		Database Parameter:	@PKID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_temp_data", FieldName: "pk_id", IsPrimaryKey=true)]
		public virtual int PkID
		{
			get 
			{ 
				return this._PkID.Value; 
			}

			set 
			{ 
				//Set Value
				this._PkID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PkID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _PkID { get; set; }

			
		/// <summary>
		///		KeyName Property
		///		Database Field:		key_name;
		///		Database Parameter:	@KeyName;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_temp_data", FieldName: "key_name", IsPrimaryKey=true)]
		public virtual string KeyName
		{
			get 
			{ 
				return this._KeyName.Value; 
			}

			set 
			{ 
				//Set Value
				this._KeyName.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._KeyName.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _KeyName { get; set; }

			
		/// <summary>
		///		Value Property
		///		Database Field:		value;
		///		Database Parameter:	@Value;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_temp_data", FieldName: "value", IsNullable=true)]
		public virtual string Value
		{
			get 
			{ 
				return (string)this._Value.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Value.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Value.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Value { get; set; }

			
		/// <summary>
		///		PostedDate Property
		///		Database Field:		posted_date;
		///		Database Parameter:	@PostedDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_temp_data", FieldName: "posted_date")]
		public virtual DateTime PostedDate
		{
			get 
			{ 
				return this._PostedDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._PostedDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _PostedDate { get; set; }

			
		/// <summary>
		///		IsTemporary Property
		///		Database Field:		is_temporary;
		///		Database Parameter:	@IsTemporary;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_temp_data", FieldName: "is_temporary")]
		public virtual bool IsTemporary
		{
			get 
			{ 
				return this._IsTemporary.Value; 
			}

			set 
			{ 
				//Set Value
				this._IsTemporary.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._IsTemporary.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _IsTemporary { get; set; }

			
		/// <summary>
		///		BinaryValue Property
		///		Database Field:		binary_value;
		///		Database Parameter:	@BinaryValue;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_temp_data", FieldName: "binary_value", IsNullable=true)]
		public virtual byte[] BinaryValue
		{
			get 
			{ 
				return (byte[])this._BinaryValue.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._BinaryValue.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._BinaryValue.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<byte[]> _BinaryValue { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoTempData Dto)
		{
			//Set Values
			this._TableName.SetChangedValueIfDifferent(Dto.TableName);
			this._PkID.SetChangedValueIfDifferent(Dto.PkID);
			this._KeyName.SetChangedValueIfDifferent(Dto.KeyName);
			this._Value.SetChangedValueIfDifferent(Dto.Value);
			this._PostedDate.SetChangedValueIfDifferent(Dto.PostedDate);
			this._IsTemporary.SetChangedValueIfDifferent(Dto.IsTemporary);
			this._BinaryValue.SetChangedValueIfDifferent(Dto.BinaryValue);
		}
		public virtual DtoTempData ToDto()
		{
			return this.ToDto(new DtoTempData());
		}
		
		public virtual DtoTempData ToDto(DtoTempData dto)
		{
			//Set Values
			dto.TableName = this.TableName;
			dto.PkID = this.PkID;
			dto.KeyName = this.KeyName;
			dto.Value = this.Value;
			dto.PostedDate = this.PostedDate;
			dto.IsTemporary = this.IsTemporary;
			dto.BinaryValue = this.BinaryValue;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****



		public static DataRecordType LoadByPk(string TableName, int PkID, string KeyName)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, TableName, PkID, KeyName);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, string TableName, int PkID, string KeyName)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_temp_data]  WITH (NOLOCK)
										WHERE
											
											[table_name] = @PK_TableName
											AND [pk_id] = @PK_PKID
											AND [key_name] = @PK_KeyName
											",
											"@PK_TableName", TableName, "@PK_PKID", PkID, "@PK_KeyName", KeyName
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class UrlCaches : UrlCaches<UrlCaches, UrlCach>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UrlCaches
		/// </summary>
		public UrlCaches() : this(new Context()) {}
		
		/// <summary>
		/// Creates UrlCaches
		/// </summary>
		public UrlCaches(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class UrlCaches<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: UrlCach<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Url = new jlib.DataFramework.BaseColumn<string>(FieldName: "url", PropertyName: "Url", ParameterName: "@Url", PkParameterName: "@PK_Url", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Method = new jlib.DataFramework.BaseColumn<string>(FieldName: "method", PropertyName: "Method", ParameterName: "@Method", PkParameterName: "@PK_Method", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> PostData = new jlib.DataFramework.BaseColumn<string>(FieldName: "post_data", PropertyName: "PostData", ParameterName: "@PostData", PkParameterName: "@PK_PostData", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> PostDataChecksum = new jlib.DataFramework.BaseColumn<int>(FieldName: "post_data_checksum", PropertyName: "PostDataChecksum", ParameterName: "@PostDataChecksum", PkParameterName: "@PK_PostDataChecksum", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Response = new jlib.DataFramework.BaseColumn<string>(FieldName: "response", PropertyName: "Response", ParameterName: "@Response", PkParameterName: "@PK_Response", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> PostedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "posted_date", PropertyName: "PostedDate", ParameterName: "@PostedDate", PkParameterName: "@PK_PostedDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Username = new jlib.DataFramework.BaseColumn<string>(FieldName: "username", PropertyName: "Username", ParameterName: "@Username", PkParameterName: "@PK_Username", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UrlCaches
		/// </summary>
		public UrlCaches() : this(new Context()) {}
		
		/// <summary>
		/// Creates UrlCaches
		/// </summary>
		public UrlCaches(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_url_cache";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = UrlCaches<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("Url", this.Url = UrlCaches<DataCollectionType, DataRecordType>.Columns.Url);
			base.Columns.Add("Method", this.Method = UrlCaches<DataCollectionType, DataRecordType>.Columns.Method);
			base.Columns.Add("PostData", this.PostData = UrlCaches<DataCollectionType, DataRecordType>.Columns.PostData);
			base.Columns.Add("PostDataChecksum", this.PostDataChecksum = UrlCaches<DataCollectionType, DataRecordType>.Columns.PostDataChecksum);
			base.Columns.Add("Response", this.Response = UrlCaches<DataCollectionType, DataRecordType>.Columns.Response);
			base.Columns.Add("PostedDate", this.PostedDate = UrlCaches<DataCollectionType, DataRecordType>.Columns.PostedDate);
			base.Columns.Add("Username", this.Username = UrlCaches<DataCollectionType, DataRecordType>.Columns.Username);
			
			//Call the secondary constructor method
			this.UrlCachesEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void UrlCachesEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<string> Url { get; set; }
		public jlib.DataFramework.BaseColumn<string> Method { get; set; }
		public jlib.DataFramework.BaseColumn<string> PostData { get; set; }
		public jlib.DataFramework.BaseColumn<int> PostDataChecksum { get; set; }
		public jlib.DataFramework.BaseColumn<string> Response { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> PostedDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> Username { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_url_cache] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_url_cache] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_url_cache");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_url_cache", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, string Url = null, string Method = null, string PostData = null, int? PostDataChecksum = null, string Response = null, DateTime? PostedDate = null, string Username = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(Url != null)
			{
				query.Parameters.Url = Url;
				filterConditions.Add(@"[url] = @Url");
			}	
			if(Method != null)
			{
				query.Parameters.Method = Method;
				filterConditions.Add(@"[method] = @Method");
			}	
			if(PostData != null)
			{
				query.Parameters.PostData = PostData;
				filterConditions.Add(@"[post_data] LIKE @PostData");
			}	
			if(PostDataChecksum != null)
			{
				query.Parameters.PostDataChecksum = PostDataChecksum;
				filterConditions.Add(@"[post_data_checksum] = @PostDataChecksum");
			}	
			if(Response != null)
			{
				query.Parameters.Response = Response;
				filterConditions.Add(@"[response] LIKE @Response");
			}	
			if(PostedDate != null)
			{
				query.Parameters.PostedDate = PostedDate;
				filterConditions.Add(@"[posted_date] = @PostedDate");
			}	
			if(Username != null)
			{
				query.Parameters.Username = Username;
				filterConditions.Add(@"[username] = @Username");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_url_cache] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<UrlCaches<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new UrlCaches<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<UrlCaches<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new UrlCaches<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_url_cache] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new UrlCachesDataReader(this);
		}
		
		public List<DtoUrlCach> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class UrlCachesDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public UrlCachesDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoUrlCach
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		Url Property
		/// </summary>
		[DataMember]
		public virtual string Url { get; set; }
		
		/// <summary>
		///		Method Property
		/// </summary>
		[DataMember]
		public virtual string Method { get; set; }
		
		/// <summary>
		///		PostData Property
		/// </summary>
		[DataMember]
		public virtual string PostData { get; set; }
		
		/// <summary>
		///		PostDataChecksum Property
		/// </summary>
		[DataMember]
		public virtual int PostDataChecksum { get; set; }
		
		/// <summary>
		///		Response Property
		/// </summary>
		[DataMember]
		public virtual string Response { get; set; }
		
		/// <summary>
		///		PostedDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime? PostedDate { get; set; }
		
		/// <summary>
		///		Username Property
		/// </summary>
		[DataMember]
		public virtual string Username { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class UrlCach : UrlCach<UrlCach>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UrlCach
		/// </summary>
		public UrlCach() : this(new Context()) {}
		
		/// <summary>
		/// Creates UrlCach
		/// </summary>
		public UrlCach(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for UrlCach
		/// </summary>
		protected UrlCach(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class UrlCach<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: UrlCach<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UrlCach
		/// </summary>
		public UrlCach() : this(new Context()) {}
		
		/// <summary>
		/// Creates UrlCach
		/// </summary>
		public UrlCach(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_url_cache";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, UrlCaches.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Url" ,this._Url = new jlib.DataFramework.BaseField<string>(this, UrlCaches.Columns.Url, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Method" ,this._Method = new jlib.DataFramework.BaseField<string>(this, UrlCaches.Columns.Method, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostData" ,this._PostData = new jlib.DataFramework.BaseField<string>(this, UrlCaches.Columns.PostData, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostDataChecksum" ,this._PostDataChecksum = new jlib.DataFramework.BaseField<int>(this, UrlCaches.Columns.PostDataChecksum, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Response" ,this._Response = new jlib.DataFramework.BaseField<string>(this, UrlCaches.Columns.Response, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostedDate" ,this._PostedDate = new jlib.DataFramework.BaseField<DateTime>(this, UrlCaches.Columns.PostedDate, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Username" ,this._Username = new jlib.DataFramework.BaseField<string>(this, UrlCaches.Columns.Username, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.UrlCachEx();
		}
		
		/// <summary>
		/// Deserialization method for UrlCach
		/// </summary>
		protected UrlCach(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void UrlCachEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_url_cache", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		Url Property
		///		Database Field:		url;
		///		Database Parameter:	@Url;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_url_cache", FieldName: "url", IsNullable=true)]
		public virtual string Url
		{
			get 
			{ 
				return (string)this._Url.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Url.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Url.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Url { get; set; }

			
		/// <summary>
		///		Method Property
		///		Database Field:		method;
		///		Database Parameter:	@Method;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_url_cache", FieldName: "method", IsNullable=true)]
		public virtual string Method
		{
			get 
			{ 
				return (string)this._Method.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Method.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Method.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Method { get; set; }

			
		/// <summary>
		///		PostData Property
		///		Database Field:		post_data;
		///		Database Parameter:	@PostData;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_url_cache", FieldName: "post_data", IsNullable=true)]
		public virtual string PostData
		{
			get 
			{ 
				return (string)this._PostData.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._PostData.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostData.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _PostData { get; set; }

			
		/// <summary>
		///		PostDataChecksum Property
		///		Database Field:		post_data_checksum;
		///		Database Parameter:	@PostDataChecksum;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_url_cache", FieldName: "post_data_checksum")]
		public virtual int PostDataChecksum
		{
			get 
			{ 
				return this._PostDataChecksum.Value; 
			}

			set 
			{ 
				//Set Value
				this._PostDataChecksum.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostDataChecksum.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _PostDataChecksum { get; set; }

			
		/// <summary>
		///		Response Property
		///		Database Field:		response;
		///		Database Parameter:	@Response;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_url_cache", FieldName: "response", IsNullable=true)]
		public virtual string Response
		{
			get 
			{ 
				return (string)this._Response.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Response.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Response.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Response { get; set; }

			
		/// <summary>
		///		PostedDate Property
		///		Database Field:		posted_date;
		///		Database Parameter:	@PostedDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_url_cache", FieldName: "posted_date", IsNullable=true)]
		public virtual DateTime? PostedDate
		{
			get 
			{ 
				return (DateTime?)this._PostedDate.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._PostedDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _PostedDate { get; set; }

			
		/// <summary>
		///		Username Property
		///		Database Field:		username;
		///		Database Parameter:	@Username;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_url_cache", FieldName: "username", IsNullable=true)]
		public virtual string Username
		{
			get 
			{ 
				return (string)this._Username.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Username.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Username.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Username { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoUrlCach Dto)
		{
			//Set Values
			this._Url.SetChangedValueIfDifferent(Dto.Url);
			this._Method.SetChangedValueIfDifferent(Dto.Method);
			this._PostData.SetChangedValueIfDifferent(Dto.PostData);
			this._PostDataChecksum.SetChangedValueIfDifferent(Dto.PostDataChecksum);
			this._Response.SetChangedValueIfDifferent(Dto.Response);
			this._PostedDate.SetChangedValueIfDifferent(Dto.PostedDate);
			this._Username.SetChangedValueIfDifferent(Dto.Username);
		}
		public virtual DtoUrlCach ToDto()
		{
			return this.ToDto(new DtoUrlCach());
		}
		
		public virtual DtoUrlCach ToDto(DtoUrlCach dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.Url = this.Url;
			dto.Method = this.Method;
			dto.PostData = this.PostData;
			dto.PostDataChecksum = this.PostDataChecksum;
			dto.Response = this.Response;
			dto.PostedDate = this.PostedDate;
			dto.Username = this.Username;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_url_cache] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_url_cache]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class UserBackups : UserBackups<UserBackups, UserBackup>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UserBackups
		/// </summary>
		public UserBackups() : this(new Context()) {}
		
		/// <summary>
		/// Creates UserBackups
		/// </summary>
		public UserBackups(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class UserBackups<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: UserBackup<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<long> UserID = new jlib.DataFramework.BaseColumn<long>(FieldName: "user_id", PropertyName: "UserID", ParameterName: "@UserID", PkParameterName: "@PK_UserID", DBType: DbType.Int64, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> PostedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "posted_date", PropertyName: "PostedDate", ParameterName: "@PostedDate", PkParameterName: "@PK_PostedDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Username = new jlib.DataFramework.BaseColumn<string>(FieldName: "username", PropertyName: "Username", ParameterName: "@Username", PkParameterName: "@PK_Username", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Email = new jlib.DataFramework.BaseColumn<string>(FieldName: "email", PropertyName: "Email", ParameterName: "@Email", PkParameterName: "@PK_Email", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<byte[]> Data = new jlib.DataFramework.BaseColumn<byte[]>(FieldName: "data", PropertyName: "Data", ParameterName: "@Data", PkParameterName: "@PK_Data", DBType: DbType.Binary, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UserBackups
		/// </summary>
		public UserBackups() : this(new Context()) {}
		
		/// <summary>
		/// Creates UserBackups
		/// </summary>
		public UserBackups(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_user_backup";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = UserBackups<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("UserID", this.UserID = UserBackups<DataCollectionType, DataRecordType>.Columns.UserID);
			base.Columns.Add("PostedDate", this.PostedDate = UserBackups<DataCollectionType, DataRecordType>.Columns.PostedDate);
			base.Columns.Add("Username", this.Username = UserBackups<DataCollectionType, DataRecordType>.Columns.Username);
			base.Columns.Add("Email", this.Email = UserBackups<DataCollectionType, DataRecordType>.Columns.Email);
			base.Columns.Add("Data", this.Data = UserBackups<DataCollectionType, DataRecordType>.Columns.Data);
			
			//Call the secondary constructor method
			this.UserBackupsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void UserBackupsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<long> UserID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> PostedDate { get; set; }
		public jlib.DataFramework.BaseColumn<string> Username { get; set; }
		public jlib.DataFramework.BaseColumn<string> Email { get; set; }
		public jlib.DataFramework.BaseColumn<byte[]> Data { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_user_backup] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_user_backup] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_user_backup");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_user_backup", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, long? UserID = null, DateTime? PostedDate = null, string Username = null, string Email = null, byte[] Data = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(UserID != null)
			{
				query.Parameters.UserID = UserID;
				filterConditions.Add(@"[user_id] = @UserID");
			}	
			if(PostedDate != null)
			{
				query.Parameters.PostedDate = PostedDate;
				filterConditions.Add(@"[posted_date] = @PostedDate");
			}	
			if(Username != null)
			{
				query.Parameters.Username = Username;
				filterConditions.Add(@"[username] = @Username");
			}	
			if(Email != null)
			{
				query.Parameters.Email = Email;
				filterConditions.Add(@"[email] = @Email");
			}	
			if(Data != null)
			{
				query.Parameters.Data = Data;
				filterConditions.Add(@"[data] = @Data");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_user_backup] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<UserBackups<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new UserBackups<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<UserBackups<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new UserBackups<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_user_backup] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new UserBackupsDataReader(this);
		}
		
		public List<DtoUserBackup> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class UserBackupsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public UserBackupsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoUserBackup
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		UserID Property
		/// </summary>
		[DataMember]
		public virtual long UserID { get; set; }
		
		/// <summary>
		///		PostedDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime PostedDate { get; set; }
		
		/// <summary>
		///		Username Property
		/// </summary>
		[DataMember]
		public virtual string Username { get; set; }
		
		/// <summary>
		///		Email Property
		/// </summary>
		[DataMember]
		public virtual string Email { get; set; }
		
		/// <summary>
		///		Data Property
		/// </summary>
		[DataMember]
		public virtual byte[] Data { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class UserBackup : UserBackup<UserBackup>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UserBackup
		/// </summary>
		public UserBackup() : this(new Context()) {}
		
		/// <summary>
		/// Creates UserBackup
		/// </summary>
		public UserBackup(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for UserBackup
		/// </summary>
		protected UserBackup(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class UserBackup<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: UserBackup<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UserBackup
		/// </summary>
		public UserBackup() : this(new Context()) {}
		
		/// <summary>
		/// Creates UserBackup
		/// </summary>
		public UserBackup(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_user_backup";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, UserBackups.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserID" ,this._UserID = new jlib.DataFramework.BaseField<long>(this, UserBackups.Columns.UserID, InitialValue: -1L, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostedDate" ,this._PostedDate = new jlib.DataFramework.BaseField<DateTime>(this, UserBackups.Columns.PostedDate, InitialValue: DateTime.Now, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Username" ,this._Username = new jlib.DataFramework.BaseField<string>(this, UserBackups.Columns.Username, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Email" ,this._Email = new jlib.DataFramework.BaseField<string>(this, UserBackups.Columns.Email, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Data" ,this._Data = new jlib.DataFramework.BaseField<byte[]>(this, UserBackups.Columns.Data, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.UserBackupEx();
		}
		
		/// <summary>
		/// Deserialization method for UserBackup
		/// </summary>
		protected UserBackup(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void UserBackupEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_backup", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		UserID Property
		///		Database Field:		user_id;
		///		Database Parameter:	@UserID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_backup", FieldName: "user_id")]
		public virtual long UserID
		{
			get 
			{ 
				return this._UserID.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<long> _UserID { get; set; }

			
		/// <summary>
		///		PostedDate Property
		///		Database Field:		posted_date;
		///		Database Parameter:	@PostedDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_backup", FieldName: "posted_date")]
		public virtual DateTime PostedDate
		{
			get 
			{ 
				return this._PostedDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._PostedDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _PostedDate { get; set; }

			
		/// <summary>
		///		Username Property
		///		Database Field:		username;
		///		Database Parameter:	@Username;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_backup", FieldName: "username")]
		public virtual string Username
		{
			get 
			{ 
				return this._Username.Value; 
			}

			set 
			{ 
				//Set Value
				this._Username.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Username.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Username { get; set; }

			
		/// <summary>
		///		Email Property
		///		Database Field:		email;
		///		Database Parameter:	@Email;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_backup", FieldName: "email")]
		public virtual string Email
		{
			get 
			{ 
				return this._Email.Value; 
			}

			set 
			{ 
				//Set Value
				this._Email.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Email.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Email { get; set; }

			
		/// <summary>
		///		Data Property
		///		Database Field:		data;
		///		Database Parameter:	@Data;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_backup", FieldName: "data", IsNullable=true)]
		public virtual byte[] Data
		{
			get 
			{ 
				return (byte[])this._Data.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Data.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Data.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<byte[]> _Data { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoUserBackup Dto)
		{
			//Set Values
			this._UserID.SetChangedValueIfDifferent(Dto.UserID);
			this._PostedDate.SetChangedValueIfDifferent(Dto.PostedDate);
			this._Username.SetChangedValueIfDifferent(Dto.Username);
			this._Email.SetChangedValueIfDifferent(Dto.Email);
			this._Data.SetChangedValueIfDifferent(Dto.Data);
		}
		public virtual DtoUserBackup ToDto()
		{
			return this.ToDto(new DtoUserBackup());
		}
		
		public virtual DtoUserBackup ToDto(DtoUserBackup dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.UserID = this.UserID;
			dto.PostedDate = this.PostedDate;
			dto.Username = this.Username;
			dto.Email = this.Email;
			dto.Data = this.Data;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_user_backup] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_user_backup]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class UserRestoreErrors : UserRestoreErrors<UserRestoreErrors, UserRestoreError>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UserRestoreErrors
		/// </summary>
		public UserRestoreErrors() : this(new Context()) {}
		
		/// <summary>
		/// Creates UserRestoreErrors
		/// </summary>
		public UserRestoreErrors(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class UserRestoreErrors<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: UserRestoreError<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> BackupID = new jlib.DataFramework.BaseColumn<int>(FieldName: "backup_id", PropertyName: "BackupID", ParameterName: "@BackupID", PkParameterName: "@PK_BackupID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> PostedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "posted_date", PropertyName: "PostedDate", ParameterName: "@PostedDate", PkParameterName: "@PK_PostedDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<byte[]> Data = new jlib.DataFramework.BaseColumn<byte[]>(FieldName: "data", PropertyName: "Data", ParameterName: "@Data", PkParameterName: "@PK_Data", DBType: DbType.Binary, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Description = new jlib.DataFramework.BaseColumn<string>(FieldName: "description", PropertyName: "Description", ParameterName: "@Description", PkParameterName: "@PK_Description", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UserRestoreErrors
		/// </summary>
		public UserRestoreErrors() : this(new Context()) {}
		
		/// <summary>
		/// Creates UserRestoreErrors
		/// </summary>
		public UserRestoreErrors(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_user_restore_errors";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = UserRestoreErrors<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("BackupID", this.BackupID = UserRestoreErrors<DataCollectionType, DataRecordType>.Columns.BackupID);
			base.Columns.Add("PostedDate", this.PostedDate = UserRestoreErrors<DataCollectionType, DataRecordType>.Columns.PostedDate);
			base.Columns.Add("Data", this.Data = UserRestoreErrors<DataCollectionType, DataRecordType>.Columns.Data);
			base.Columns.Add("Description", this.Description = UserRestoreErrors<DataCollectionType, DataRecordType>.Columns.Description);
			
			//Call the secondary constructor method
			this.UserRestoreErrorsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void UserRestoreErrorsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<int> BackupID { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> PostedDate { get; set; }
		public jlib.DataFramework.BaseColumn<byte[]> Data { get; set; }
		public jlib.DataFramework.BaseColumn<string> Description { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_user_restore_errors] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_user_restore_errors] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_user_restore_errors");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_user_restore_errors", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, int? BackupID = null, DateTime? PostedDate = null, byte[] Data = null, string Description = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(BackupID != null)
			{
				query.Parameters.BackupID = BackupID;
				filterConditions.Add(@"[backup_id] = @BackupID");
			}	
			if(PostedDate != null)
			{
				query.Parameters.PostedDate = PostedDate;
				filterConditions.Add(@"[posted_date] = @PostedDate");
			}	
			if(Data != null)
			{
				query.Parameters.Data = Data;
				filterConditions.Add(@"[data] = @Data");
			}	
			if(Description != null)
			{
				query.Parameters.Description = Description;
				filterConditions.Add(@"[description] LIKE @Description");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_user_restore_errors] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<UserRestoreErrors<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new UserRestoreErrors<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<UserRestoreErrors<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new UserRestoreErrors<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_user_restore_errors] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new UserRestoreErrorsDataReader(this);
		}
		
		public List<DtoUserRestoreError> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class UserRestoreErrorsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public UserRestoreErrorsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoUserRestoreError
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		BackupID Property
		/// </summary>
		[DataMember]
		public virtual int BackupID { get; set; }
		
		/// <summary>
		///		PostedDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime PostedDate { get; set; }
		
		/// <summary>
		///		Data Property
		/// </summary>
		[DataMember]
		public virtual byte[] Data { get; set; }
		
		/// <summary>
		///		Description Property
		/// </summary>
		[DataMember]
		public virtual string Description { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class UserRestoreError : UserRestoreError<UserRestoreError>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UserRestoreError
		/// </summary>
		public UserRestoreError() : this(new Context()) {}
		
		/// <summary>
		/// Creates UserRestoreError
		/// </summary>
		public UserRestoreError(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for UserRestoreError
		/// </summary>
		protected UserRestoreError(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class UserRestoreError<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: UserRestoreError<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UserRestoreError
		/// </summary>
		public UserRestoreError() : this(new Context()) {}
		
		/// <summary>
		/// Creates UserRestoreError
		/// </summary>
		public UserRestoreError(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_user_restore_errors";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, UserRestoreErrors.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("BackupID" ,this._BackupID = new jlib.DataFramework.BaseField<int>(this, UserRestoreErrors.Columns.BackupID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostedDate" ,this._PostedDate = new jlib.DataFramework.BaseField<DateTime>(this, UserRestoreErrors.Columns.PostedDate, InitialValue: DateTime.Now, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Data" ,this._Data = new jlib.DataFramework.BaseField<byte[]>(this, UserRestoreErrors.Columns.Data, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Description" ,this._Description = new jlib.DataFramework.BaseField<string>(this, UserRestoreErrors.Columns.Description, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.UserRestoreErrorEx();
		}
		
		/// <summary>
		/// Deserialization method for UserRestoreError
		/// </summary>
		protected UserRestoreError(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void UserRestoreErrorEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_restore_errors", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		BackupID Property
		///		Database Field:		backup_id;
		///		Database Parameter:	@BackupID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_restore_errors", FieldName: "backup_id")]
		public virtual int BackupID
		{
			get 
			{ 
				return this._BackupID.Value; 
			}

			set 
			{ 
				//Set Value
				this._BackupID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._BackupID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _BackupID { get; set; }

			
		/// <summary>
		///		PostedDate Property
		///		Database Field:		posted_date;
		///		Database Parameter:	@PostedDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_restore_errors", FieldName: "posted_date")]
		public virtual DateTime PostedDate
		{
			get 
			{ 
				return this._PostedDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._PostedDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _PostedDate { get; set; }

			
		/// <summary>
		///		Data Property
		///		Database Field:		data;
		///		Database Parameter:	@Data;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_restore_errors", FieldName: "data")]
		public virtual byte[] Data
		{
			get 
			{ 
				return this._Data.Value; 
			}

			set 
			{ 
				//Set Value
				this._Data.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Data.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<byte[]> _Data { get; set; }

			
		/// <summary>
		///		Description Property
		///		Database Field:		description;
		///		Database Parameter:	@Description;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_restore_errors", FieldName: "description")]
		public virtual string Description
		{
			get 
			{ 
				return this._Description.Value; 
			}

			set 
			{ 
				//Set Value
				this._Description.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Description.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Description { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoUserRestoreError Dto)
		{
			//Set Values
			this._BackupID.SetChangedValueIfDifferent(Dto.BackupID);
			this._PostedDate.SetChangedValueIfDifferent(Dto.PostedDate);
			this._Data.SetChangedValueIfDifferent(Dto.Data);
			this._Description.SetChangedValueIfDifferent(Dto.Description);
		}
		public virtual DtoUserRestoreError ToDto()
		{
			return this.ToDto(new DtoUserRestoreError());
		}
		
		public virtual DtoUserRestoreError ToDto(DtoUserRestoreError dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.BackupID = this.BackupID;
			dto.PostedDate = this.PostedDate;
			dto.Data = this.Data;
			dto.Description = this.Description;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_user_restore_errors] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_user_restore_errors]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class UserSettings : UserSettings<UserSettings, UserSetting>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UserSettings
		/// </summary>
		public UserSettings() : this(new Context()) {}
		
		/// <summary>
		/// Creates UserSettings
		/// </summary>
		public UserSettings(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class UserSettings<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: UserSetting<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> UserID = new jlib.DataFramework.BaseColumn<int>(FieldName: "user_id", PropertyName: "UserID", ParameterName: "@UserID", PkParameterName: "@PK_UserID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> CompanyID = new jlib.DataFramework.BaseColumn<int>(FieldName: "company_id", PropertyName: "CompanyID", ParameterName: "@CompanyID", PkParameterName: "@PK_CompanyID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> MasterCompanyID = new jlib.DataFramework.BaseColumn<int>(FieldName: "master_company_id", PropertyName: "MasterCompanyID", ParameterName: "@MasterCompanyID", PkParameterName: "@PK_MasterCompanyID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<int> UserTypeID = new jlib.DataFramework.BaseColumn<int>(FieldName: "user_type_id", PropertyName: "UserTypeID", ParameterName: "@UserTypeID", PkParameterName: "@PK_UserTypeID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> KeyName = new jlib.DataFramework.BaseColumn<string>(FieldName: "key_name", PropertyName: "KeyName", ParameterName: "@KeyName", PkParameterName: "@PK_KeyName", DBType: DbType.AnsiString, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> SubKey = new jlib.DataFramework.BaseColumn<string>(FieldName: "sub_key", PropertyName: "SubKey", ParameterName: "@SubKey", PkParameterName: "@PK_SubKey", DBType: DbType.AnsiString, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Value = new jlib.DataFramework.BaseColumn<string>(FieldName: "value", PropertyName: "Value", ParameterName: "@Value", PkParameterName: "@PK_Value", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> PostedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "posted_date", PropertyName: "PostedDate", ParameterName: "@PostedDate", PkParameterName: "@PK_PostedDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UserSettings
		/// </summary>
		public UserSettings() : this(new Context()) {}
		
		/// <summary>
		/// Creates UserSettings
		/// </summary>
		public UserSettings(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_user_settings";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = UserSettings<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("UserID", this.UserID = UserSettings<DataCollectionType, DataRecordType>.Columns.UserID);
			base.Columns.Add("CompanyID", this.CompanyID = UserSettings<DataCollectionType, DataRecordType>.Columns.CompanyID);
			base.Columns.Add("MasterCompanyID", this.MasterCompanyID = UserSettings<DataCollectionType, DataRecordType>.Columns.MasterCompanyID);
			base.Columns.Add("UserTypeID", this.UserTypeID = UserSettings<DataCollectionType, DataRecordType>.Columns.UserTypeID);
			base.Columns.Add("KeyName", this.KeyName = UserSettings<DataCollectionType, DataRecordType>.Columns.KeyName);
			base.Columns.Add("SubKey", this.SubKey = UserSettings<DataCollectionType, DataRecordType>.Columns.SubKey);
			base.Columns.Add("Value", this.Value = UserSettings<DataCollectionType, DataRecordType>.Columns.Value);
			base.Columns.Add("PostedDate", this.PostedDate = UserSettings<DataCollectionType, DataRecordType>.Columns.PostedDate);
			
			//Call the secondary constructor method
			this.UserSettingsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void UserSettingsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<int> UserID { get; set; }
		public jlib.DataFramework.BaseColumn<int> CompanyID { get; set; }
		public jlib.DataFramework.BaseColumn<int> MasterCompanyID { get; set; }
		public jlib.DataFramework.BaseColumn<int> UserTypeID { get; set; }
		public jlib.DataFramework.BaseColumn<string> KeyName { get; set; }
		public jlib.DataFramework.BaseColumn<string> SubKey { get; set; }
		public jlib.DataFramework.BaseColumn<string> Value { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> PostedDate { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_user_settings] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_user_settings] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_user_settings");
		}
		
		
		public static QueryPackage GetByField(int? ID = null, int? UserID = null, int? CompanyID = null, int? MasterCompanyID = null, int? UserTypeID = null, string KeyName = null, string SubKey = null, string Value = null, DateTime? PostedDate = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(UserID != null)
			{
				query.Parameters.UserID = UserID;
				filterConditions.Add(@"[user_id] = @UserID");
			}	
			if(CompanyID != null)
			{
				query.Parameters.CompanyID = CompanyID;
				filterConditions.Add(@"[company_id] = @CompanyID");
			}	
			if(MasterCompanyID != null)
			{
				query.Parameters.MasterCompanyID = MasterCompanyID;
				filterConditions.Add(@"[master_company_id] = @MasterCompanyID");
			}	
			if(UserTypeID != null)
			{
				query.Parameters.UserTypeID = UserTypeID;
				filterConditions.Add(@"[user_type_id] = @UserTypeID");
			}	
			if(KeyName != null)
			{
				query.Parameters.KeyName = KeyName;
				filterConditions.Add(@"[key_name] = @KeyName");
			}	
			if(SubKey != null)
			{
				query.Parameters.SubKey = SubKey;
				filterConditions.Add(@"[sub_key] = @SubKey");
			}	
			if(Value != null)
			{
				query.Parameters.Value = Value;
				filterConditions.Add(@"[value] = @Value");
			}	
			if(PostedDate != null)
			{
				query.Parameters.PostedDate = PostedDate;
				filterConditions.Add(@"[posted_date] = @PostedDate");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_user_settings] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<UserSettings<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new UserSettings<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<UserSettings<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new UserSettings<DataCollectionType, DataRecordType>()));
		}
		*/

		
		public override IDataReader GetDataReader()
		{
			return new UserSettingsDataReader(this);
		}
		
		public List<DtoUserSetting> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class UserSettingsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public UserSettingsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoUserSetting
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		UserID Property
		/// </summary>
		[DataMember]
		public virtual int UserID { get; set; }
		
		/// <summary>
		///		CompanyID Property
		/// </summary>
		[DataMember]
		public virtual int CompanyID { get; set; }
		
		/// <summary>
		///		MasterCompanyID Property
		/// </summary>
		[DataMember]
		public virtual int MasterCompanyID { get; set; }
		
		/// <summary>
		///		UserTypeID Property
		/// </summary>
		[DataMember]
		public virtual int UserTypeID { get; set; }
		
		/// <summary>
		///		KeyName Property
		/// </summary>
		[DataMember]
		public virtual string KeyName { get; set; }
		
		/// <summary>
		///		SubKey Property
		/// </summary>
		[DataMember]
		public virtual string SubKey { get; set; }
		
		/// <summary>
		///		Value Property
		/// </summary>
		[DataMember]
		public virtual string Value { get; set; }
		
		/// <summary>
		///		PostedDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime PostedDate { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class UserSetting : UserSetting<UserSetting>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UserSetting
		/// </summary>
		public UserSetting() : this(new Context()) {}
		
		/// <summary>
		/// Creates UserSetting
		/// </summary>
		public UserSetting(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for UserSetting
		/// </summary>
		protected UserSetting(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class UserSetting<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: UserSetting<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates UserSetting
		/// </summary>
		public UserSetting() : this(new Context()) {}
		
		/// <summary>
		/// Creates UserSetting
		/// </summary>
		public UserSetting(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_user_settings";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, UserSettings.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserID" ,this._UserID = new jlib.DataFramework.BaseField<int>(this, UserSettings.Columns.UserID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CompanyID" ,this._CompanyID = new jlib.DataFramework.BaseField<int>(this, UserSettings.Columns.CompanyID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("MasterCompanyID" ,this._MasterCompanyID = new jlib.DataFramework.BaseField<int>(this, UserSettings.Columns.MasterCompanyID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UserTypeID" ,this._UserTypeID = new jlib.DataFramework.BaseField<int>(this, UserSettings.Columns.UserTypeID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("KeyName" ,this._KeyName = new jlib.DataFramework.BaseField<string>(this, UserSettings.Columns.KeyName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("SubKey" ,this._SubKey = new jlib.DataFramework.BaseField<string>(this, UserSettings.Columns.SubKey, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Value" ,this._Value = new jlib.DataFramework.BaseField<string>(this, UserSettings.Columns.Value, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostedDate" ,this._PostedDate = new jlib.DataFramework.BaseField<DateTime>(this, UserSettings.Columns.PostedDate, InitialValue: DateTime.Now, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.UserSettingEx();
		}
		
		/// <summary>
		/// Deserialization method for UserSetting
		/// </summary>
		protected UserSetting(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void UserSettingEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_settings", FieldName: "ID", IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		UserID Property
		///		Database Field:		user_id;
		///		Database Parameter:	@UserID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_settings", FieldName: "user_id", IsPrimaryKey=true)]
		public virtual int UserID
		{
			get 
			{ 
				return this._UserID.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _UserID { get; set; }

			
		/// <summary>
		///		CompanyID Property
		///		Database Field:		company_id;
		///		Database Parameter:	@CompanyID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_settings", FieldName: "company_id", IsPrimaryKey=true)]
		public virtual int CompanyID
		{
			get 
			{ 
				return this._CompanyID.Value; 
			}

			set 
			{ 
				//Set Value
				this._CompanyID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CompanyID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _CompanyID { get; set; }

			
		/// <summary>
		///		MasterCompanyID Property
		///		Database Field:		master_company_id;
		///		Database Parameter:	@MasterCompanyID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_settings", FieldName: "master_company_id", IsPrimaryKey=true)]
		public virtual int MasterCompanyID
		{
			get 
			{ 
				return this._MasterCompanyID.Value; 
			}

			set 
			{ 
				//Set Value
				this._MasterCompanyID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._MasterCompanyID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _MasterCompanyID { get; set; }

			
		/// <summary>
		///		UserTypeID Property
		///		Database Field:		user_type_id;
		///		Database Parameter:	@UserTypeID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_settings", FieldName: "user_type_id", IsPrimaryKey=true)]
		public virtual int UserTypeID
		{
			get 
			{ 
				return this._UserTypeID.Value; 
			}

			set 
			{ 
				//Set Value
				this._UserTypeID.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UserTypeID.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _UserTypeID { get; set; }

			
		/// <summary>
		///		KeyName Property
		///		Database Field:		key_name;
		///		Database Parameter:	@KeyName;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_settings", FieldName: "key_name", IsPrimaryKey=true)]
		public virtual string KeyName
		{
			get 
			{ 
				return this._KeyName.Value; 
			}

			set 
			{ 
				//Set Value
				this._KeyName.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._KeyName.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _KeyName { get; set; }

			
		/// <summary>
		///		SubKey Property
		///		Database Field:		sub_key;
		///		Database Parameter:	@SubKey;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_settings", FieldName: "sub_key", IsPrimaryKey=true)]
		public virtual string SubKey
		{
			get 
			{ 
				return this._SubKey.Value; 
			}

			set 
			{ 
				//Set Value
				this._SubKey.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._SubKey.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _SubKey { get; set; }

			
		/// <summary>
		///		Value Property
		///		Database Field:		value;
		///		Database Parameter:	@Value;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_settings", FieldName: "value", IsNullable=true)]
		public virtual string Value
		{
			get 
			{ 
				return (string)this._Value.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Value.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Value.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Value { get; set; }

			
		/// <summary>
		///		PostedDate Property
		///		Database Field:		posted_date;
		///		Database Parameter:	@PostedDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_user_settings", FieldName: "posted_date")]
		public virtual DateTime PostedDate
		{
			get 
			{ 
				return this._PostedDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._PostedDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _PostedDate { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoUserSetting Dto)
		{
			//Set Values
			this._UserID.SetChangedValueIfDifferent(Dto.UserID);
			this._CompanyID.SetChangedValueIfDifferent(Dto.CompanyID);
			this._MasterCompanyID.SetChangedValueIfDifferent(Dto.MasterCompanyID);
			this._UserTypeID.SetChangedValueIfDifferent(Dto.UserTypeID);
			this._KeyName.SetChangedValueIfDifferent(Dto.KeyName);
			this._SubKey.SetChangedValueIfDifferent(Dto.SubKey);
			this._Value.SetChangedValueIfDifferent(Dto.Value);
			this._PostedDate.SetChangedValueIfDifferent(Dto.PostedDate);
		}
		public virtual DtoUserSetting ToDto()
		{
			return this.ToDto(new DtoUserSetting());
		}
		
		public virtual DtoUserSetting ToDto(DtoUserSetting dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.UserID = this.UserID;
			dto.CompanyID = this.CompanyID;
			dto.MasterCompanyID = this.MasterCompanyID;
			dto.UserTypeID = this.UserTypeID;
			dto.KeyName = this.KeyName;
			dto.SubKey = this.SubKey;
			dto.Value = this.Value;
			dto.PostedDate = this.PostedDate;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_user_settings] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int UserID, int CompanyID, int MasterCompanyID, int UserTypeID, string KeyName, string SubKey)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, UserID, CompanyID, MasterCompanyID, UserTypeID, KeyName, SubKey);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int UserID, int CompanyID, int MasterCompanyID, int UserTypeID, string KeyName, string SubKey)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_user_settings]  WITH (NOLOCK)
										WHERE
											
											[user_id] = @PK_UserID
											AND [company_id] = @PK_CompanyID
											AND [master_company_id] = @PK_MasterCompanyID
											AND [user_type_id] = @PK_UserTypeID
											AND [key_name] = @PK_KeyName
											AND [sub_key] = @PK_SubKey
											",
											"@PK_UserID", UserID, "@PK_CompanyID", CompanyID, "@PK_MasterCompanyID", MasterCompanyID, "@PK_UserTypeID", UserTypeID, "@PK_KeyName", KeyName, "@PK_SubKey", SubKey
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class Views : Views<Views, View>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Views
		/// </summary>
		public Views() : this(new Context()) {}
		
		/// <summary>
		/// Creates Views
		/// </summary>
		public Views(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Views<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: View<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: true, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<bool> Deleted = new jlib.DataFramework.BaseColumn<bool>(FieldName: "deleted", PropertyName: "Deleted", ParameterName: "@Deleted", PkParameterName: "@PK_Deleted", DBType: DbType.Boolean, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<DateTime> PostedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "posted_date", PropertyName: "PostedDate", ParameterName: "@PostedDate", PkParameterName: "@PK_PostedDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<DateTime> UpdatedDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "updated_date", PropertyName: "UpdatedDate", ParameterName: "@UpdatedDate", PkParameterName: "@PK_UpdatedDate", DBType: DbType.DateTime, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<long> CreatedBy = new jlib.DataFramework.BaseColumn<long>(FieldName: "created_by", PropertyName: "CreatedBy", ParameterName: "@CreatedBy", PkParameterName: "@PK_CreatedBy", DBType: DbType.Int64, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<long> UpdatedBy = new jlib.DataFramework.BaseColumn<long>(FieldName: "updated_by", PropertyName: "UpdatedBy", ParameterName: "@UpdatedBy", PkParameterName: "@PK_UpdatedBy", DBType: DbType.Int64, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
			public static jlib.DataFramework.BaseColumn<string> Name = new jlib.DataFramework.BaseColumn<string>(FieldName: "name", PropertyName: "Name", ParameterName: "@Name", PkParameterName: "@PK_Name", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> ReportUrl = new jlib.DataFramework.BaseColumn<string>(FieldName: "report_url", PropertyName: "ReportUrl", ParameterName: "@ReportUrl", PkParameterName: "@PK_ReportUrl", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Xml = new jlib.DataFramework.BaseColumn<string>(FieldName: "xml", PropertyName: "Xml", ParameterName: "@Xml", PkParameterName: "@PK_Xml", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: true, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Views
		/// </summary>
		public Views() : this(new Context()) {}
		
		/// <summary>
		/// Creates Views
		/// </summary>
		public Views(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "d_views";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = Views<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("Deleted", this.Deleted = Views<DataCollectionType, DataRecordType>.Columns.Deleted);
			base.Columns.Add("PostedDate", this.PostedDate = Views<DataCollectionType, DataRecordType>.Columns.PostedDate);
			base.Columns.Add("UpdatedDate", this.UpdatedDate = Views<DataCollectionType, DataRecordType>.Columns.UpdatedDate);
			base.Columns.Add("CreatedBy", this.CreatedBy = Views<DataCollectionType, DataRecordType>.Columns.CreatedBy);
			base.Columns.Add("UpdatedBy", this.UpdatedBy = Views<DataCollectionType, DataRecordType>.Columns.UpdatedBy);
			base.Columns.Add("Name", this.Name = Views<DataCollectionType, DataRecordType>.Columns.Name);
			base.Columns.Add("ReportUrl", this.ReportUrl = Views<DataCollectionType, DataRecordType>.Columns.ReportUrl);
			base.Columns.Add("Xml", this.Xml = Views<DataCollectionType, DataRecordType>.Columns.Xml);
			
			//Call the secondary constructor method
			this.ViewsEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void ViewsEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<bool> Deleted { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> PostedDate { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> UpdatedDate { get; set; }
		public jlib.DataFramework.BaseColumn<long> CreatedBy { get; set; }
		public jlib.DataFramework.BaseColumn<long> UpdatedBy { get; set; }
		public jlib.DataFramework.BaseColumn<string> Name { get; set; }
		public jlib.DataFramework.BaseColumn<string> ReportUrl { get; set; }
		public jlib.DataFramework.BaseColumn<string> Xml { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_views] WITH (NOLOCK) WHERE [deleted]=0", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_views] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("d_views");
		}
		
		public static void DeleteByPks(List<int> PKs)
		{
			DeleteByPks<int>("d_views", "ID", PKs);
		}
		
		public static QueryPackage GetByField(int? ID = null, bool? Deleted = null, DateTime? PostedDate = null, DateTime? UpdatedDate = null, long? CreatedBy = null, long? UpdatedBy = null, string Name = null, string ReportUrl = null, string Xml = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
			if(Deleted == null)
			{
				filterConditions.Add(@"[deleted]=0");
			}
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(Deleted != null)
			{
				query.Parameters.Deleted = Deleted;
				filterConditions.Add(@"[deleted] = @Deleted");
			}	
			if(PostedDate != null)
			{
				query.Parameters.PostedDate = PostedDate;
				filterConditions.Add(@"[posted_date] = @PostedDate");
			}	
			if(UpdatedDate != null)
			{
				query.Parameters.UpdatedDate = UpdatedDate;
				filterConditions.Add(@"[updated_date] = @UpdatedDate");
			}	
			if(CreatedBy != null)
			{
				query.Parameters.CreatedBy = CreatedBy;
				filterConditions.Add(@"[created_by] = @CreatedBy");
			}	
			if(UpdatedBy != null)
			{
				query.Parameters.UpdatedBy = UpdatedBy;
				filterConditions.Add(@"[updated_by] = @UpdatedBy");
			}	
			if(Name != null)
			{
				query.Parameters.Name = Name;
				filterConditions.Add(@"[name] = @Name");
			}	
			if(ReportUrl != null)
			{
				query.Parameters.ReportUrl = ReportUrl;
				filterConditions.Add(@"[report_url] = @ReportUrl");
			}	
			if(Xml != null)
			{
				query.Parameters.Xml = Xml;
				filterConditions.Add(@"[xml] LIKE @Xml");
			}	
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_views] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<Views<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new Views<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<Views<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new Views<DataCollectionType, DataRecordType>()));
		}
		*/

		public static QueryPackage GetByPKs(List<int> PKs, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			if(PKs == null) PKs = new List<int>();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get IN Conditions
			List<string> pkClause = PKs.Select(x=>"'" + x.ToString() + "'").ToList();
			if(pkClause.Count==0) pkClause.Add("-100");
			string pkStringClause = pkClause.Join(",");

			//Create Statement
			query.Statement = @"SELECT * FROM [d_views] WITH (NOLOCK) WHERE [ID] IN (" + pkStringClause + ")";
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		
		public override IDataReader GetDataReader()
		{
			return new ViewsDataReader(this);
		}
		
		public List<DtoView> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class ViewsDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public ViewsDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoView
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		Deleted Property
		/// </summary>
		[DataMember]
		public virtual bool Deleted { get; set; }
		
		/// <summary>
		///		PostedDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime PostedDate { get; set; }
		
		/// <summary>
		///		UpdatedDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime UpdatedDate { get; set; }
		
		/// <summary>
		///		CreatedBy Property
		/// </summary>
		[DataMember]
		public virtual long CreatedBy { get; set; }
		
		/// <summary>
		///		UpdatedBy Property
		/// </summary>
		[DataMember]
		public virtual long UpdatedBy { get; set; }
		
		/// <summary>
		///		Name Property
		/// </summary>
		[DataMember]
		public virtual string Name { get; set; }
		
		/// <summary>
		///		ReportUrl Property
		/// </summary>
		[DataMember]
		public virtual string ReportUrl { get; set; }
		
		/// <summary>
		///		Xml Property
		/// </summary>
		[DataMember]
		public virtual string Xml { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class View : View<View>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates View
		/// </summary>
		public View() : this(new Context()) {}
		
		/// <summary>
		/// Creates View
		/// </summary>
		public View(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for View
		/// </summary>
		protected View(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class View<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: View<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates View
		/// </summary>
		public View() : this(new Context()) {}
		
		/// <summary>
		/// Creates View
		/// </summary>
		public View(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "d_views";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, Views.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Deleted" ,this._Deleted = new jlib.DataFramework.BaseField<bool>(this, Views.Columns.Deleted, InitialValue: false, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("PostedDate" ,this._PostedDate = new jlib.DataFramework.BaseField<DateTime>(this, Views.Columns.PostedDate, InitialValue: DateTime.Now, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UpdatedDate" ,this._UpdatedDate = new jlib.DataFramework.BaseField<DateTime>(this, Views.Columns.UpdatedDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreatedBy" ,this._CreatedBy = new jlib.DataFramework.BaseField<long>(this, Views.Columns.CreatedBy, InitialValue: -1L, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("UpdatedBy" ,this._UpdatedBy = new jlib.DataFramework.BaseField<long>(this, Views.Columns.UpdatedBy, InitialValue: -1L, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Name" ,this._Name = new jlib.DataFramework.BaseField<string>(this, Views.Columns.Name, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ReportUrl" ,this._ReportUrl = new jlib.DataFramework.BaseField<string>(this, Views.Columns.ReportUrl, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Xml" ,this._Xml = new jlib.DataFramework.BaseField<string>(this, Views.Columns.Xml, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.ViewEx();
		}
		
		/// <summary>
		/// Deserialization method for View
		/// </summary>
		protected View(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void ViewEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			Yes;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_views", FieldName: "ID", IsPrimaryKey=true, IsIdentity=true)]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}

		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }

			
		/// <summary>
		///		Deleted Property
		///		Database Field:		deleted;
		///		Database Parameter:	@Deleted;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_views", FieldName: "deleted")]
		public virtual bool Deleted
		{
			get 
			{ 
				return this._Deleted.Value; 
			}

			set 
			{ 
				//Set Value
				this._Deleted.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Deleted.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<bool> _Deleted { get; set; }

			
		/// <summary>
		///		PostedDate Property
		///		Database Field:		posted_date;
		///		Database Parameter:	@PostedDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_views", FieldName: "posted_date")]
		public virtual DateTime PostedDate
		{
			get 
			{ 
				return this._PostedDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._PostedDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._PostedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _PostedDate { get; set; }

			
		/// <summary>
		///		UpdatedDate Property
		///		Database Field:		updated_date;
		///		Database Parameter:	@UpdatedDate;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_views", FieldName: "updated_date")]
		public virtual DateTime UpdatedDate
		{
			get 
			{ 
				return this._UpdatedDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._UpdatedDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UpdatedDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _UpdatedDate { get; set; }

			
		/// <summary>
		///		CreatedBy Property
		///		Database Field:		created_by;
		///		Database Parameter:	@CreatedBy;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_views", FieldName: "created_by")]
		public virtual long CreatedBy
		{
			get 
			{ 
				return this._CreatedBy.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreatedBy.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreatedBy.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<long> _CreatedBy { get; set; }

			
		/// <summary>
		///		UpdatedBy Property
		///		Database Field:		updated_by;
		///		Database Parameter:	@UpdatedBy;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_views", FieldName: "updated_by")]
		public virtual long UpdatedBy
		{
			get 
			{ 
				return this._UpdatedBy.Value; 
			}

			set 
			{ 
				//Set Value
				this._UpdatedBy.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._UpdatedBy.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<long> _UpdatedBy { get; set; }

			
		/// <summary>
		///		Name Property
		///		Database Field:		name;
		///		Database Parameter:	@Name;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_views", FieldName: "name", IsNullable=true)]
		public virtual string Name
		{
			get 
			{ 
				return (string)this._Name.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Name.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Name.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Name { get; set; }

			
		/// <summary>
		///		ReportUrl Property
		///		Database Field:		report_url;
		///		Database Parameter:	@ReportUrl;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_views", FieldName: "report_url", IsNullable=true)]
		public virtual string ReportUrl
		{
			get 
			{ 
				return (string)this._ReportUrl.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._ReportUrl.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ReportUrl.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _ReportUrl { get; set; }

			
		/// <summary>
		///		Xml Property
		///		Database Field:		xml;
		///		Database Parameter:	@Xml;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	Yes;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_views", FieldName: "xml", IsNullable=true)]
		public virtual string Xml
		{
			get 
			{ 
				return (string)this._Xml.RawValue; 
			}

			set 
			{ 
				//Set Value
				this._Xml.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Xml.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Xml { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoView Dto)
		{
			//Set Values
			this._Deleted.SetChangedValueIfDifferent(Dto.Deleted);
			this._PostedDate.SetChangedValueIfDifferent(Dto.PostedDate);
			this._UpdatedDate.SetChangedValueIfDifferent(Dto.UpdatedDate);
			this._CreatedBy.SetChangedValueIfDifferent(Dto.CreatedBy);
			this._UpdatedBy.SetChangedValueIfDifferent(Dto.UpdatedBy);
			this._Name.SetChangedValueIfDifferent(Dto.Name);
			this._ReportUrl.SetChangedValueIfDifferent(Dto.ReportUrl);
			this._Xml.SetChangedValueIfDifferent(Dto.Xml);
		}
		public virtual DtoView ToDto()
		{
			return this.ToDto(new DtoView());
		}
		
		public virtual DtoView ToDto(DtoView dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.Deleted = this.Deleted;
			dto.PostedDate = this.PostedDate;
			dto.UpdatedDate = this.UpdatedDate;
			dto.CreatedBy = this.CreatedBy;
			dto.UpdatedBy = this.UpdatedBy;
			dto.Name = this.Name;
			dto.ReportUrl = this.ReportUrl;
			dto.Xml = this.Xml;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		public static DataRecordType LoadById(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadById(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadById(SqlHelper SqlHelper, int ID)
		{
			
		
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
									SELECT
										*
									FROM
										[d_views] WITH (NOLOCK)
									WHERE
										[ID] = @PK_ID
									",
									
									"@PK_ID", ID
									
									);
									
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;
		}



		public static DataRecordType LoadByPk(int ID)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[d_views]  WITH (NOLOCK)
										WHERE
											
											[ID] = @PK_ID
											",
											"@PK_ID", ID
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}

	//Typed Collection
	public partial class RowLinks : RowLinks<RowLinks, RowLink>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates RowLinks
		/// </summary>
		public RowLinks() : this(new Context()) {}
		
		/// <summary>
		/// Creates RowLinks
		/// </summary>
		public RowLinks(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class RowLinks<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseTableCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: RowLink<DataRecordType>, new()
	{
		
		#region ***** ENUM / CONSTANTS *****
		#endregion
		
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID1 = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID1", PropertyName: "ID1", ParameterName: "@Id1", PkParameterName: "@PK_Id1", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> ID2 = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID2", PropertyName: "ID2", ParameterName: "@Id2", PkParameterName: "@PK_Id2", DBType: DbType.Int32, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Table1 = new jlib.DataFramework.BaseColumn<string>(FieldName: "Table1", PropertyName: "Table1", ParameterName: "@Table1", PkParameterName: "@PK_Table1", DBType: DbType.AnsiString, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Table2 = new jlib.DataFramework.BaseColumn<string>(FieldName: "Table2", PropertyName: "Table2", ParameterName: "@Table2", PkParameterName: "@PK_Table2", DBType: DbType.AnsiString, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> CreateDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "CreateDate", PropertyName: "CreateDate", ParameterName: "@Createdate", PkParameterName: "@PK_Createdate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<DateTime> DeleteDate = new jlib.DataFramework.BaseColumn<DateTime>(FieldName: "DeleteDate", PropertyName: "DeleteDate", ParameterName: "@Deletedate", PkParameterName: "@PK_Deletedate", DBType: DbType.DateTime, IsPrimaryKey: true, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> Sequence = new jlib.DataFramework.BaseColumn<int>(FieldName: "Sequence", PropertyName: "Sequence", ParameterName: "@Sequence", PkParameterName: "@PK_Sequence", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: true);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates RowLinks
		/// </summary>
		public RowLinks() : this(new Context()) {}
		
		/// <summary>
		/// Creates RowLinks
		/// </summary>
		public RowLinks(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.TABLE_NAME = "l_row_links";
			
			//Set Columns
			base.Columns.Add("ID1", this.ID1 = RowLinks<DataCollectionType, DataRecordType>.Columns.ID1);
			base.Columns.Add("ID2", this.ID2 = RowLinks<DataCollectionType, DataRecordType>.Columns.ID2);
			base.Columns.Add("Table1", this.Table1 = RowLinks<DataCollectionType, DataRecordType>.Columns.Table1);
			base.Columns.Add("Table2", this.Table2 = RowLinks<DataCollectionType, DataRecordType>.Columns.Table2);
			base.Columns.Add("CreateDate", this.CreateDate = RowLinks<DataCollectionType, DataRecordType>.Columns.CreateDate);
			base.Columns.Add("DeleteDate", this.DeleteDate = RowLinks<DataCollectionType, DataRecordType>.Columns.DeleteDate);
			base.Columns.Add("Sequence", this.Sequence = RowLinks<DataCollectionType, DataRecordType>.Columns.Sequence);
			
			//Call the secondary constructor method
			this.RowLinksEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void RowLinksEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID1 { get; set; }
		public jlib.DataFramework.BaseColumn<int> ID2 { get; set; }
		public jlib.DataFramework.BaseColumn<string> Table1 { get; set; }
		public jlib.DataFramework.BaseColumn<string> Table2 { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> CreateDate { get; set; }
		public jlib.DataFramework.BaseColumn<DateTime> DeleteDate { get; set; }
		public jlib.DataFramework.BaseColumn<int> Sequence { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [l_row_links] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [l_row_links] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static void DeleteAll()
		{
			DeleteAll("l_row_links");
		}
		
		
		public static QueryPackage GetByField(int? ID1 = null, int? ID2 = null, string Table1 = null, string Table2 = null, DateTime? CreateDate = null, DateTime? DeleteDate = null, int? Sequence = null, DateTime? ObjectDate = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID1 != null)
			{
				query.Parameters.ID1 = ID1;
				filterConditions.Add(@"[ID1] = @Id1");
			}	
			if(ID2 != null)
			{
				query.Parameters.ID2 = ID2;
				filterConditions.Add(@"[ID2] = @Id2");
			}	
			if(Table1 != null)
			{
				query.Parameters.Table1 = Table1;
				filterConditions.Add(@"[Table1] = @Table1");
			}	
			if(Table2 != null)
			{
				query.Parameters.Table2 = Table2;
				filterConditions.Add(@"[Table2] = @Table2");
			}	
			if(CreateDate != null)
			{
				query.Parameters.CreateDate = CreateDate;
				filterConditions.Add(@"[CreateDate] = @Createdate");
			}	
			if(DeleteDate != null)
			{
				query.Parameters.DeleteDate = DeleteDate;
				filterConditions.Add(@"[DeleteDate] = @Deletedate");
			}	
			if(Sequence != null)
			{
				query.Parameters.Sequence = Sequence;
				filterConditions.Add(@"[Sequence] = @Sequence");
			}	
			if(ObjectDate != null || (CreateDate==null && DeleteDate==null))
			{
				Context oContext=new Context();
				query.Parameters.ObjectDate = (ObjectDate==null ? oContext.VersionDate : ObjectDate);
				filterConditions.Add(@"@ObjectDate BETWEEN [CreateDate] AND [DeleteDate]");
			}
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [l_row_links] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		/*
		public static QueryPackage Where(Func<RowLinks<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll().Where(fnCondition(new RowLinks<DataCollectionType, DataRecordType>()));
		}
		public static QueryPackage Where(PagingOptions PagingOptions, Func<RowLinks<DataCollectionType, DataRecordType>, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(fnCondition(new RowLinks<DataCollectionType, DataRecordType>()));
		}
		*/

		
		public override IDataReader GetDataReader()
		{
			return new RowLinksDataReader(this);
		}
		
		public List<DtoRowLink> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class RowLinksDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public RowLinksDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoRowLink
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID1 Property
		/// </summary>
		[DataMember]
		public virtual int ID1 { get; set; }
		
		/// <summary>
		///		ID2 Property
		/// </summary>
		[DataMember]
		public virtual int ID2 { get; set; }
		
		/// <summary>
		///		Table1 Property
		/// </summary>
		[DataMember]
		public virtual string Table1 { get; set; }
		
		/// <summary>
		///		Table2 Property
		/// </summary>
		[DataMember]
		public virtual string Table2 { get; set; }
		
		/// <summary>
		///		CreateDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime CreateDate { get; set; }
		
		/// <summary>
		///		DeleteDate Property
		/// </summary>
		[DataMember]
		public virtual DateTime DeleteDate { get; set; }
		
		/// <summary>
		///		Sequence Property
		/// </summary>
		[DataMember]
		public virtual int Sequence { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class RowLink : RowLink<RowLink>
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates RowLink
		/// </summary>
		public RowLink() : this(new Context()) {}
		
		/// <summary>
		/// Creates RowLink
		/// </summary>
		public RowLink(Context Context) : base(Context) {}
		
		/// <summary>
		/// Deserialization method for RowLink
		/// </summary>
		protected RowLink(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : base(SerializationInfo, StreamingContext) {}
		#endregion
	}

	//Generic Record
	public partial class RowLink<DataRecordType> : Phoenix.LearningPortal.Data.BaseTableRecordImpl<Context> where DataRecordType: RowLink<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates RowLink
		/// </summary>
		public RowLink() : this(new Context()) {}
		
		/// <summary>
		/// Creates RowLink
		/// </summary>
		public RowLink(Context Context) : base(Context)
		{
			//Set Table_Name
			this.TABLE_NAME = "l_row_links";

			//Declare Fields
			this.Fields.Add("ID1" ,this._ID1 = new jlib.DataFramework.BaseField<int>(this, RowLinks.Columns.ID1, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("ID2" ,this._ID2 = new jlib.DataFramework.BaseField<int>(this, RowLinks.Columns.ID2, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Table1" ,this._Table1 = new jlib.DataFramework.BaseField<string>(this, RowLinks.Columns.Table1, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Table2" ,this._Table2 = new jlib.DataFramework.BaseField<string>(this, RowLinks.Columns.Table2, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CreateDate" ,this._CreateDate = new jlib.DataFramework.BaseField<DateTime>(this, RowLinks.Columns.CreateDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("DeleteDate" ,this._DeleteDate = new jlib.DataFramework.BaseField<DateTime>(this, RowLinks.Columns.DeleteDate, InitialValue: DateTime.MinValue, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Sequence" ,this._Sequence = new jlib.DataFramework.BaseField<int>(this, RowLinks.Columns.Sequence, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.RowLinkEx();
		}
		
		/// <summary>
		/// Deserialization method for RowLink
		/// </summary>
		protected RowLink(SerializationInfo SerializationInfo, StreamingContext StreamingContext) : this(new Context())
        {
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					field.Value.SetLoadedValue(SerializationInfo.GetValue(field.Value.PropertyName, field.Value.ObjectType));
				});
        }
		
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void RowLinkEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID1 Property
		///		Database Field:		ID1;
		///		Database Parameter:	@Id1;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "l_row_links", FieldName: "ID1", IsPrimaryKey=true)]
		public virtual int ID1
		{
			get 
			{ 
				return this._ID1.Value; 
			}

			set 
			{ 
				//Set Value
				this._ID1.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ID1.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ID1 { get; set; }

			
		/// <summary>
		///		ID2 Property
		///		Database Field:		ID2;
		///		Database Parameter:	@Id2;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "l_row_links", FieldName: "ID2", IsPrimaryKey=true)]
		public virtual int ID2
		{
			get 
			{ 
				return this._ID2.Value; 
			}

			set 
			{ 
				//Set Value
				this._ID2.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._ID2.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _ID2 { get; set; }

			
		/// <summary>
		///		Table1 Property
		///		Database Field:		Table1;
		///		Database Parameter:	@Table1;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "l_row_links", FieldName: "Table1", IsPrimaryKey=true)]
		public virtual string Table1
		{
			get 
			{ 
				return this._Table1.Value; 
			}

			set 
			{ 
				//Set Value
				this._Table1.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Table1.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Table1 { get; set; }

			
		/// <summary>
		///		Table2 Property
		///		Database Field:		Table2;
		///		Database Parameter:	@Table2;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "l_row_links", FieldName: "Table2", IsPrimaryKey=true)]
		public virtual string Table2
		{
			get 
			{ 
				return this._Table2.Value; 
			}

			set 
			{ 
				//Set Value
				this._Table2.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Table2.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<string> _Table2 { get; set; }

			
		/// <summary>
		///		CreateDate Property
		///		Database Field:		CreateDate;
		///		Database Parameter:	@Createdate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "l_row_links", FieldName: "CreateDate", IsPrimaryKey=true)]
		public virtual DateTime CreateDate
		{
			get 
			{ 
				return this._CreateDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._CreateDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._CreateDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _CreateDate { get; set; }

			
		/// <summary>
		///		DeleteDate Property
		///		Database Field:		DeleteDate;
		///		Database Parameter:	@Deletedate;
		///		Primary Key:		Yes;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	No;
		/// </summary>
		[jlib.DataFramework.Field(Table: "l_row_links", FieldName: "DeleteDate", IsPrimaryKey=true)]
		public virtual DateTime DeleteDate
		{
			get 
			{ 
				return this._DeleteDate.Value; 
			}

			set 
			{ 
				//Set Value
				this._DeleteDate.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._DeleteDate.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<DateTime> _DeleteDate { get; set; }

			
		/// <summary>
		///		Sequence Property
		///		Database Field:		Sequence;
		///		Database Parameter:	@Sequence;
		///		Primary Key:		No;
		///		Foreign Key:		No;
		///		IsIdentity:			No;
		///		IsColumnNullable:	No;
		///		ColumnHasDefault:	Yes;
		/// </summary>
		[jlib.DataFramework.Field(Table: "l_row_links", FieldName: "Sequence")]
		public virtual int Sequence
		{
			get 
			{ 
				return this._Sequence.Value; 
			}

			set 
			{ 
				//Set Value
				this._Sequence.SetChangedValue(value); 

				//Set RowState
				if(this.RecordState != DataRowAction.Add && this._Sequence.FieldStatus == FieldStatuses.Modified) this.RecordState = DataRowAction.Change;

			}
		}
		protected jlib.DataFramework.BaseField<int> _Sequence { get; set; }

			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoRowLink Dto)
		{
			//Set Values
			this._ID1.SetChangedValueIfDifferent(Dto.ID1);
			this._ID2.SetChangedValueIfDifferent(Dto.ID2);
			this._Table1.SetChangedValueIfDifferent(Dto.Table1);
			this._Table2.SetChangedValueIfDifferent(Dto.Table2);
			this._CreateDate.SetChangedValueIfDifferent(Dto.CreateDate);
			this._DeleteDate.SetChangedValueIfDifferent(Dto.DeleteDate);
			this._Sequence.SetChangedValueIfDifferent(Dto.Sequence);
		}
		public virtual DtoRowLink ToDto()
		{
			return this.ToDto(new DtoRowLink());
		}
		
		public virtual DtoRowLink ToDto(DtoRowLink dto)
		{
			//Set Values
			dto.ID1 = this.ID1;
			dto.ID2 = this.ID2;
			dto.Table1 = this.Table1;
			dto.Table2 = this.Table2;
			dto.CreateDate = this.CreateDate;
			dto.DeleteDate = this.DeleteDate;
			dto.Sequence = this.Sequence;
				
			//Return
			return dto;
		}
		protected override void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			switch(InternalObjectIdentifier.ToLower())
			{
				
				default:
					break;
			}
			
			//Call Base Method
			base.ImportInternalObject(InternalObjectIdentifier, Row);
		}
		#endregion
		
		#region ***** STATIC LOAD METHODS *****

				public static DataRecordType LoadByPk(int ID1, int ID2, string Table1, string Table2)
				{
					Context oContext=new Context();
					//Create Sql Connection
					SqlHelper sqlHelper = oContext.GetSqlHelper();
			
					//Load
					DataRecordType item = LoadByPk(sqlHelper, ID1, ID2, Table1, Table2, oContext.VersionDate);
			
					//Return
					return item;
				}

		public static DataRecordType LoadByPk(int ID1, int ID2, string Table1, string Table2,  DateTime ObjectDate)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			//Load
			DataRecordType item = LoadByPk(sqlHelper, ID1, ID2, Table1, Table2, ObjectDate);
			
			//Return
			return item;
		}
		public static DataRecordType LoadByPk(SqlHelper SqlHelper, int ID1, int ID2, string Table1, string Table2,  DateTime ObjectDate)
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											*
										FROM
											[l_row_links]  WITH (NOLOCK)
										WHERE
											
											[ID1] = @PK_Id1
											AND [ID2] = @PK_Id2
											AND [Table1] = @PK_Table1
											AND [Table2] = @PK_Table2		AND @ObjectDate BETWEEN [CreateDate] AND [DeleteDate]

											",
											"@PK_Id1", ID1, "@PK_Id2", ID2, "@PK_Table1", Table1, "@PK_Table2", Table2, "@ObjectDate", ObjectDate
											);
											
			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				DataRecordType item = new DataRecordType();
				item.Context = new Context();
				
				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}

		#endregion
	}


	//Typed Collection
	public partial class Users : Users<Users, User>
	{
		#region ***** CONSTANTS *****
		public const string Users_SQL = @"SELECT * FROM [d_users] WITH (NOLOCK)";
		#endregion
	
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Sql Users
		/// </summary>
		public Users() : this(new Context()) {}
		
		/// <summary>
		/// Creates Users
		/// </summary>
		public Users(Context Context) : base(Context) {}

		#endregion
	}
	
	//Generic Collection
	public partial class Users<DataCollectionType, DataRecordType> : jlib.DataFramework.BaseViewCollection<DataCollectionType, DataRecordType, Context> 
		where DataCollectionType: jlib.DataFramework.BaseViewCollection<DataCollectionType, DataRecordType, Context>, new() 
		where DataRecordType: User<DataRecordType>, new()
	{
				
		#region ***** COLUMNS *****
		public static new class Columns
		{
			//Properties
			public static jlib.DataFramework.BaseColumn<int> ID = new jlib.DataFramework.BaseColumn<int>(FieldName: "ID", PropertyName: "ID", ParameterName: "@ID", PkParameterName: "@PK_ID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Email = new jlib.DataFramework.BaseColumn<string>(FieldName: "Email", PropertyName: "Email", ParameterName: "@Email", PkParameterName: "@PK_Email", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> Username = new jlib.DataFramework.BaseColumn<string>(FieldName: "Username", PropertyName: "Username", ParameterName: "@Username", PkParameterName: "@PK_Username", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> FirstName = new jlib.DataFramework.BaseColumn<string>(FieldName: "First_Name", PropertyName: "FirstName", ParameterName: "@FirstName", PkParameterName: "@PK_FirstName", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> LastName = new jlib.DataFramework.BaseColumn<string>(FieldName: "Last_name", PropertyName: "LastName", ParameterName: "@LastName", PkParameterName: "@PK_LastName", DBType: DbType.String, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<int> CompanyID = new jlib.DataFramework.BaseColumn<int>(FieldName: "Company_ID", PropertyName: "CompanyID", ParameterName: "@CompanyID", PkParameterName: "@PK_CompanyID", DBType: DbType.Int32, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
			public static jlib.DataFramework.BaseColumn<string> CompanyName = new jlib.DataFramework.BaseColumn<string>(FieldName: "Company_name", PropertyName: "CompanyName", ParameterName: "@CompanyName", PkParameterName: "@PK_CompanyName", DBType: DbType.AnsiString, IsPrimaryKey: false, IsIdentity: false, IsNullable: false, HasDbDefault: false);
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates Users
		/// </summary>
		public Users() : this(new Context()) {}
		
		/// <summary>
		/// Creates Users
		/// </summary>
		public Users(Context Context) : base(Context) 
		{
			//Set Table_Name
			this.VIEW_NAME = "d_users";
			
			//Set Columns
			base.Columns.Add("ID", this.ID = Users<DataCollectionType, DataRecordType>.Columns.ID);
			base.Columns.Add("Email", this.Email = Users<DataCollectionType, DataRecordType>.Columns.Email);
			base.Columns.Add("Username", this.Username = Users<DataCollectionType, DataRecordType>.Columns.Username);
			base.Columns.Add("FirstName", this.FirstName = Users<DataCollectionType, DataRecordType>.Columns.FirstName);
			base.Columns.Add("LastName", this.LastName = Users<DataCollectionType, DataRecordType>.Columns.LastName);
			base.Columns.Add("CompanyID", this.CompanyID = Users<DataCollectionType, DataRecordType>.Columns.CompanyID);
			base.Columns.Add("CompanyName", this.CompanyName = Users<DataCollectionType, DataRecordType>.Columns.CompanyName);
			
			//Call the secondary constructor method
			this.UsersEx();
		}

		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void UsersEx() {}
		
		#endregion
		
		#region ***** PROPERTIES *****
		public jlib.DataFramework.BaseColumn<int> ID { get; set; }
		public jlib.DataFramework.BaseColumn<string> Email { get; set; }
		public jlib.DataFramework.BaseColumn<string> Username { get; set; }
		public jlib.DataFramework.BaseColumn<string> FirstName { get; set; }
		public jlib.DataFramework.BaseColumn<string> LastName { get; set; }
		public jlib.DataFramework.BaseColumn<int> CompanyID { get; set; }
		public jlib.DataFramework.BaseColumn<string> CompanyName { get; set; }
		
		
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_users] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		public static QueryPackage GetAllWithDeleted(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			return GetAll(@"SELECT * FROM [d_users] WITH (NOLOCK)", CacheOptions, PagingOptions);
			
		}
		
		
		public static QueryPackage GetByField(int? ID = null, string Email = null, string Username = null, string FirstName = null, string LastName = null, int? CompanyID = null, string CompanyName = null,  CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = new QueryPackage();

			//Get Filter Conditions
			List<string> filterConditions = new List<string>();	
		
			if(ID != null)
			{
				query.Parameters.ID = ID;
				filterConditions.Add(@"[ID] = @ID");
			}	
			if(Email != null)
			{
				query.Parameters.Email = Email;
				filterConditions.Add(@"[Email] = @Email");
			}	
			if(Username != null)
			{
				query.Parameters.Username = Username;
				filterConditions.Add(@"[Username] = @Username");
			}	
			if(FirstName != null)
			{
				query.Parameters.FirstName = FirstName;
				filterConditions.Add(@"[First_Name] = @FirstName");
			}	
			if(LastName != null)
			{
				query.Parameters.LastName = LastName;
				filterConditions.Add(@"[Last_name] = @LastName");
			}	
			if(CompanyID != null)
			{
				query.Parameters.CompanyID = CompanyID;
				filterConditions.Add(@"[Company_ID] = @CompanyID");
			}	
			if(CompanyName != null)
			{
				query.Parameters.CompanyName = CompanyName;
				filterConditions.Add(@"[Company_name] = @CompanyName");
			}	
			
			//Provider: SQL Server Schema Provider
			//Create Statement
			query.Statement = @"SELECT * FROM [d_users] WITH (NOLOCK) WHERE " + filterConditions.Join("\n\tAND ");
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		public static QueryPackage Where(jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll().Where(Condition);
		}
		public static QueryPackage Where(PagingOptions PagingOptions, jlib.DataFramework.QueryBuilder.BaseCondition Condition)
		{
			return GetAll(PagingOptions: PagingOptions).Where(Condition);
		}
		
		public override IDataReader GetDataReader()
		{
			return new UsersDataReader(this);
		}
		
		public List<DtoUser> ToDtos()
		{
			return this.Select(x=>x.ToDto()).ToList();
		}
		#endregion
		
		#region ***** INTERNAL CLASSES *****
		public class UsersDataReader : BaseCollectionDataReader<DataCollectionType, DataRecordType, Context> 
		{
			//Constructor
			public UsersDataReader(IEnumerable<DataRecordType> items) : base(items) {}
		}
		#endregion
		
	}

	//Typed Dto Record
	[Serializable]
	[DataContract]
	public partial class DtoUser
	{
		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		/// </summary>
		[DataMember]
		public virtual int ID { get; set; }
		
		/// <summary>
		///		Email Property
		/// </summary>
		[DataMember]
		public virtual string Email { get; set; }
		
		/// <summary>
		///		Username Property
		/// </summary>
		[DataMember]
		public virtual string Username { get; set; }
		
		/// <summary>
		///		FirstName Property
		/// </summary>
		[DataMember]
		public virtual string FirstName { get; set; }
		
		/// <summary>
		///		LastName Property
		/// </summary>
		[DataMember]
		public virtual string LastName { get; set; }
		
		/// <summary>
		///		CompanyID Property
		/// </summary>
		[DataMember]
		public virtual int CompanyID { get; set; }
		
		/// <summary>
		///		CompanyName Property
		/// </summary>
		[DataMember]
		public virtual string CompanyName { get; set; }
		
		#endregion
	}

	//Typed Record
	public partial class User : User<User>
	{
	}

	//Generic Record
	public partial class User<DataRecordType> : jlib.DataFramework.BaseViewRecord<Context>
		where DataRecordType: User<DataRecordType>, new()
	{
		#region ***** CONSTRUCTORS *****
		/// <summary>
		/// Creates User
		/// </summary>
		public User() : this(new Context()) {}
		
		/// <summary>
		/// Creates User
		/// </summary>
		public User(Context Context) : base(Context)
		{
			//Set Table_Name
			this.VIEW_NAME = "d_users";

			//Declare Fields
			this.Fields.Add("ID" ,this._ID = new jlib.DataFramework.BaseField<int>(this, Users.Columns.ID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Email" ,this._Email = new jlib.DataFramework.BaseField<string>(this, Users.Columns.Email, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("Username" ,this._Username = new jlib.DataFramework.BaseField<string>(this, Users.Columns.Username, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("FirstName" ,this._FirstName = new jlib.DataFramework.BaseField<string>(this, Users.Columns.FirstName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("LastName" ,this._LastName = new jlib.DataFramework.BaseField<string>(this, Users.Columns.LastName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CompanyID" ,this._CompanyID = new jlib.DataFramework.BaseField<int>(this, Users.Columns.CompanyID, InitialValue: -1, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			this.Fields.Add("CompanyName" ,this._CompanyName = new jlib.DataFramework.BaseField<string>(this, Users.Columns.CompanyName, InitialValue: null, FieldStatus: jlib.DataFramework.FieldStatuses.NotSet));
			
			//Call the secondary constructor method
			this.UserEx();
		}
		/// <summary>
		/// Secondary Class Constructor Method
		/// </summary>
		protected virtual void UserEx() {}
		#endregion

		#region ***** PROPERTIES *****
		/// <summary>
		///		ID Property
		///		Database Field:		ID;
		///		Database Parameter:	@ID;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_users", FieldName: "ID")]
		public virtual int ID
		{
			get 
			{ 
				return this._ID.Value; 
			}
		}
		protected jlib.DataFramework.BaseField<int> _ID { get; set; }
			
		/// <summary>
		///		Email Property
		///		Database Field:		Email;
		///		Database Parameter:	@Email;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_users", FieldName: "Email")]
		public virtual string Email
		{
			get 
			{ 
				return (string)this._Email.RawValue; 
			}
		}
		protected jlib.DataFramework.BaseField<string> _Email { get; set; }
			
		/// <summary>
		///		Username Property
		///		Database Field:		Username;
		///		Database Parameter:	@Username;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_users", FieldName: "Username")]
		public virtual string Username
		{
			get 
			{ 
				return (string)this._Username.RawValue; 
			}
		}
		protected jlib.DataFramework.BaseField<string> _Username { get; set; }
			
		/// <summary>
		///		FirstName Property
		///		Database Field:		First_Name;
		///		Database Parameter:	@FirstName;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_users", FieldName: "First_Name")]
		public virtual string FirstName
		{
			get 
			{ 
				return (string)this._FirstName.RawValue; 
			}
		}
		protected jlib.DataFramework.BaseField<string> _FirstName { get; set; }
			
		/// <summary>
		///		LastName Property
		///		Database Field:		Last_name;
		///		Database Parameter:	@LastName;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_users", FieldName: "Last_name")]
		public virtual string LastName
		{
			get 
			{ 
				return (string)this._LastName.RawValue; 
			}
		}
		protected jlib.DataFramework.BaseField<string> _LastName { get; set; }
			
		/// <summary>
		///		CompanyID Property
		///		Database Field:		Company_ID;
		///		Database Parameter:	@CompanyID;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_users", FieldName: "Company_ID")]
		public virtual int CompanyID
		{
			get 
			{ 
				return this._CompanyID.Value; 
			}
		}
		protected jlib.DataFramework.BaseField<int> _CompanyID { get; set; }
			
		/// <summary>
		///		CompanyName Property
		///		Database Field:		Company_name;
		///		Database Parameter:	@CompanyName;
		/// </summary>
		[jlib.DataFramework.Field(Table: "d_users", FieldName: "Company_name")]
		public virtual string CompanyName
		{
			get 
			{ 
				return (string)this._CompanyName.RawValue; 
			}
		}
		protected jlib.DataFramework.BaseField<string> _CompanyName { get; set; }
			

		#endregion
		
		#region ***** METHODS *****
		public virtual void ImportDto(DtoUser Dto)
		{
			//Set Values
			this._ID.SetChangedValueIfDifferent(Dto.ID);
			this._Email.SetChangedValueIfDifferent(Dto.Email);
			this._Username.SetChangedValueIfDifferent(Dto.Username);
			this._FirstName.SetChangedValueIfDifferent(Dto.FirstName);
			this._LastName.SetChangedValueIfDifferent(Dto.LastName);
			this._CompanyID.SetChangedValueIfDifferent(Dto.CompanyID);
			this._CompanyName.SetChangedValueIfDifferent(Dto.CompanyName);
		}
		public virtual DtoUser ToDto()
		{
			return this.ToDto(new DtoUser());
		}
		
		public virtual DtoUser ToDto(DtoUser dto)
		{
			//Set Values
			dto.ID = this.ID;
			dto.Email = this.Email;
			dto.Username = this.Username;
			dto.FirstName = this.FirstName;
			dto.LastName = this.LastName;
			dto.CompanyID = this.CompanyID;
			dto.CompanyName = this.CompanyName;
				
			//Return
			return dto;
		}
	
		#endregion
		
		#region ***** STATIC LOAD METHODS *****
		protected static DataRecordType LoadByPk(string PkField, object PkId)
		{
			//Create Sql Connection
			SqlHelper sqlHelper = new Context().GetSqlHelper();
			
			return LoadByPk(sqlHelper, PkField, PkId);
		}
		protected static DataRecordType LoadByPk(jlib.db.SqlHelper SqlHelper, string PkField, object PkId)
		{
			return jlib.DataFramework.BaseViewRecord<Context>.LoadByPk<DataRecordType>(SqlHelper, "d_users", PkField, PkId);
		}

		#endregion
	}






}


