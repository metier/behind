using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using jlib.db;
using jlib.functions;
using jlib.helpers;
using jlib.DataFramework;
using jlib.DataFramework.Cache;
using jlib.DataFramework.Cache.Attributes;
using jlib.DataFramework.Paging;

namespace Phoenix.LearningPortal.Data {

	public partial class Folders {
        public static QueryPackage GetForCMSList(int FolderID, int UserID) {
            jlib.DataFramework.BaseContext Context = new Context();
            return GetForCMSList(FolderID, UserID, Context.VersionDate);
        }
        public static QueryPackage GetForCMSList(int FolderID, int UserID, DateTime RevisionDate) {
            QueryPackage query = new QueryPackage();
            query.Statement = @"           
	select 
        CreatedByUser.username as CreatedByUserName, isnull(Content.UserName, ModifiedByUser.username) as ModifiedByUserName, isnull(ContentModifyDate, d_folders.CreateDate) as ModifiedDate, d_folders.*, Original.CreateDate as OriginalCreateDate, l_row_links.Sequence,
        case when d_folders.context='content' then (select count(*) from l_row_links a, l_row_links b where a.id1=d_folders.ObjectID and a.Table1='d_folders' and a.id2=b.id2 and a.Table2=b.Table2 and @RevisionDate between a.CreateDate and a.DeleteDate and @RevisionDate between b.CreateDate and b.DeleteDate) else T_NumParents end as Numlinks
    from 
        l_row_links,
        d_folders cross apply (select top 1 CreateDate from d_folders Original where Original.ObjectID=d_folders.ObjectID order by CreateDate) Original        
        left outer join d_users CreatedByUser on CreatedByUser.ID=d_folders.CreatedBy
        left outer join d_users ModifiedByUser on ModifiedByUser.ID=d_folders.ModifiedBy
        outer apply (select top 1 d_content.CreateDate as ContentModifyDate, ModifiedByUser.Username from l_row_links, d_content left outer join d_users ModifiedByUser on ModifiedByUser.ID=d_content.ModifiedBy where l_row_links.id1=d_folders.ObjectID and l_row_links.Table1='d_folders' and l_row_links.Table2='d_content' and l_row_links.id2=d_content.ObjectID order by d_content.DeleteDate desc) Content
    where 
        " + (UserID >0 ? "l_row_links.id2 in (SELECT FolderID from t_permissions where UserID=@UserID) AND " : "" ) + 
        @"l_row_links.table1='d_folders' and l_row_links.table2='d_folders' and l_row_links.id2=d_folders.ObjectID and l_row_links.id1=@FolderID 
        and @RevisionDate between l_row_links.CreateDate and l_row_links.DeleteDate
        and @RevisionDate between d_folders.CreateDate and d_folders.DeleteDate";
            query.Parameters.FolderID = FolderID;
            query.Parameters.RevisionDate = RevisionDate;
            query.Parameters.UserID = UserID;
            return query;
        }
        public static QueryPackage GetNestedFolders(int FolderID) {
            jlib.DataFramework.BaseContext Context=new Context();
            return GetNestedFolders(FolderID, Context.VersionDate);
        }
        public static QueryPackage GetNestedFolders(int FolderID, DateTime RevisionDate) {
            QueryPackage query = new QueryPackage();
            query.Statement = @"           
	select 
        d_folders.*, a.Level, a.Sort, a.ParentID, a.ParentTableName
    from 
        d_folders,
        dbo.fn_get_links(@FolderID,'d_folders',@RevisionDate,0,'') a
    where 
        a.ObjectID=d_folders.ObjectID
        and @RevisionDate between d_folders.CreateDate and d_folders.DeleteDate";
            query.Parameters.FolderID = FolderID;
            query.Parameters.RevisionDate = RevisionDate;
            return query;
        }	
	}
    
    //public partial class jlib.DataFramework.BaseTableRecord<Context> {

    //}

    public partial class Folder 
	{
		#region ***** ENUMS *****
			
		#endregion
	
		#region ***** CONSTRUCTORS *****
			
		#endregion
	
		#region ***** PROPERTIES *****

        
		#endregion
	
		#region ***** METHODS *****
        public Folder CloneFolder(bool Recursive, bool Settings, bool LinkContent, int AuthorID) {            
            Folder Clone = this.Clone( DataRowVersion.Current, true) as Folder;
            Clone.ObjectID = 0;
            Clone.Fields[Folders.Columns.ObjectID.PropertyName].FieldStatus = FieldStatuses.NotSet;
            Clone.RecordState =  DataRowAction.Add;
            Clone.CreateDate = DateTime.Now;
            Clone.CreatedbyID = AuthorID;
            Clone.ModifiedbyID = AuthorID;
            if (Recursive) {     
           
                this.GetChildren<Folder>().ForEach(x => {
                    if(x.Name.IndexOf("Final Test",  StringComparison.CurrentCultureIgnoreCase)==-1) 
                        Clone.addChild(x.CloneFolder(Recursive, Settings, LinkContent, AuthorID) as Folder);
                });
                this.GetChildren<Content>().ForEach(x => {
                    if (LinkContent) Clone.addChild(x);
                    else {
                        Content Content = x.CloneContent();                        
                        Content.Save();
                        Clone.addChild(Content);
                    }
                });
            }
            if (Settings) {
                this.Settings.ForEach(x => {
                    Clone.setSetting(x.KeyName, x.Value);
                });
            }
            Clone.setSetting("CopiedFrom", this.ObjectID.Str());
            if (AuthorID > -1)
            {
                Clone.Save(AuthorID);
                if (Recursive)
                {
                    //Update Display Mode setting
                    var originalChildren = this.GetChildren<Content>();
                    var copyChildren = Clone.GetChildren<Content>();
                    for (var x = 0; x < originalChildren.Count; x++)
                    {
                        var original = originalChildren[x];
                        var copy = copyChildren[x];
                        original.Settings.Where(setting => setting.KeyName.StartsWith("_Folder:" + this.ObjectID)).ToList()
                        .ForEach(setting => { copy.setSetting(setting.KeyName.Replace("_Folder:" + this.ObjectID, "_Folder:" + Clone.ObjectID), setting.Value); });

                        copy.Save(AuthorID);
                    }
                }
                
            }
            return Clone;
        }
        protected abstract class ItemCache {
            public DateTime CreateDate { get; set; }
        }
        protected class FileCache: ItemCache {
            public FileCache(List<KeyValuePair<string, string>> Files):base() {
                this.Files = Files;
                this.CreateDate = DateTime.Now;
            }            
            public List<KeyValuePair<string, string>> Files { get; private set; }
        }
        protected class FolderCache: ItemCache {
            public FolderCache(List<KeyValuePair<string, Folder>> Folders): base() {
                this.Folders = Folders;             
            }
            public List<KeyValuePair<string, Folder>> Folders { get; private set; }
        }
        protected class FolderCacheJSON: ItemCache {
            public FolderCacheJSON(List<object> Folders) : base() {
                this.Folders = Folders;                
            }
            public List<object> Folders { get; private set; }
        }
        private static Dictionary<int, FileCache> FileCacheList = new Dictionary<int, FileCache>();
        private static Dictionary<int, FolderCache> FolderCacheList = new Dictionary<int, FolderCache>();
        private static Dictionary<int, FolderCacheJSON> FolderCacheListJSON = new Dictionary<int, FolderCacheJSON>();
        protected void AddFiles(List<KeyValuePair<string, string>> Files, string Path, int MaxRecursions) {
            this.Children.ForEach(x=>{
                if (x.Table2 == "d_content" && !(x.Object2 as Content).Filename.IsNullOrEmpty()) Files.Add(new KeyValuePair<string,string>((x.Object2 as Content).Filename, Path + "/" + (x.Object2 as Content).Name));
                else if (x.Table2 == "d_folders" && MaxRecursions > 0) (x.Object2 as Folder).AddFiles(Files, Path + "/" + (x.Object2 as Folder).Name, MaxRecursions - 1);
            });
        }
        protected void AddFolders(List<KeyValuePair<string, Folder>> Files, string Path, int MaxRecursions) {
            this.Children.ForEach(x => {
                if (x.Table2 == "d_folders") {
                    Folder child=(x.Object2 as Folder);
                    if(child!=null){
                        Files.Add(new KeyValuePair<string, Folder>(Path + "/" + child.Name, child));
                        if (MaxRecursions > 0) (x.Object2 as Folder).AddFolders(Files, Path + "/" + (x.Object2 as Folder).Name, MaxRecursions - 1);
                    }
                }
            });
        }
        protected List<object> AddFoldersJSON(List<object> Folders, string Path, int MaxRecursions) {
        //    Func<Phoenix.LearningPortal.Data.Folder, List<Folder>, object> a = delegate(Phoenix.LearningPortal.Data.Folder Folder, List<Folder> Parents) { return ""; };
            if (Folders == null) Folders = new List<object>();
            this.Children.ForEach(x => {
                if (x.Table2 == "d_folders" && MaxRecursions > 0) {
                    Folder F = (x.Object2 as Folder);
                    Folders.Add(new { ObjectID= F.ObjectID, Name= F.Name, Path= Path });
                    F.AddFoldersJSON(Folders, convert.cIfValue(Path,""," > " , "") + F.Name, MaxRecursions - 1);                                        
                }
            });
            return Folders;
        }
        public List<object> GetFoldersJSON(bool bForceReload) {
            if (!bForceReload && FolderCacheListJSON.ContainsKey(this.ObjectID) && FolderCacheListJSON[this.ObjectID].CreateDate > DateTime.Now.AddMinutes(-60)) return FolderCacheListJSON[this.ObjectID].Folders;
            List<object> Folders = AddFoldersJSON(null, "", 5);

            FolderCacheListJSON[this.ObjectID] = new FolderCacheJSON(Folders);
            return Folders;
        }
        
        public List<KeyValuePair<string,string>> GetFiles(bool bForceReload){
            if(!bForceReload && FileCacheList.ContainsKey(this.ObjectID) && FileCacheList[this.ObjectID].CreateDate>DateTime.Now.AddMinutes(-60)) return FileCacheList[this.ObjectID].Files;
            List<KeyValuePair<string, string>> Files = new List<KeyValuePair<string, string>>();
            AddFiles(Files,this.Name,5);
            
            FileCacheList[this.ObjectID] = new FileCache(Files);
            return Files;
        }
        public List<KeyValuePair<string, Folder>> GetFolders(bool bForceReload) {
            if (!bForceReload && FolderCacheList.ContainsKey(this.ObjectID) && FolderCacheList[this.ObjectID].CreateDate > DateTime.Now.AddMinutes(-60)) return FolderCacheList[this.ObjectID].Folders;
            List<KeyValuePair<string, Folder>> Folders = new List<KeyValuePair<string, Folder>>();
            AddFolders(Folders, this.Name, 5);

            FolderCacheList[this.ObjectID] = new FolderCache(Folders);
            return Folders;
        }
        public List<Folder> GetCMSPages(string sType) {
            Folder Lesson = null;
            List<Folder> Pages = new List<Folder>();
            RowLink Link=null;
            if (this.Context_Value == "content") {
                Link = this.Parents.FirstOrDefault(x => x.Table1 == "d_folders" && (x.Object1 as Folder).Context_Value == "lesson");
                if (Link != null) Lesson = Link.Object1 as Folder;
            } else if (this.Context_Value == "lesson") {
                Lesson = this;
            }
            if (Lesson != null) {
                Lesson.Children.ForEach(x=>{
                    if (x.Table2 == "d_folders" && x.Object2 != null && (x.Object2 as Folder).Context_Value == "content" && (x.Object2 as Folder).AuxField1 == sType)
                    Pages.Add((x.Object2 as Folder));
                });
            }
            return Pages;
        }
        public Folder GetCMSPage(string sType) {
            return GetCMSPages(sType).SafeGetValue(0);            
        }
        //public override void Save(SqlHelper SqlHelper, int AuthorID) {
        //    if (this.ObjectID == 0) this.ObjectID = Phoenix.LearningPortal.Data.Context.nextObjectID();
        //    if (this.Fields[Folders.Columns.DeleteDate.PropertyName].RawValue == null) DeleteDate = DateTime.MaxValue;
        //    if (this.Fields[Folders.Columns.CreateDate.PropertyName].RawValue == null) CreateDate = DateTime.Now;
        //    base.Save(SqlHelper, AuthorID);
            
        //}
		#endregion
	}

}