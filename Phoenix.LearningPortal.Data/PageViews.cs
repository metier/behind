
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Serialization;

using jlib.db;
using jlib.functions;
using jlib.helpers;
using jlib.DataFramework;
using jlib.DataFramework.Cache;
using jlib.DataFramework.Cache.Attributes;
using jlib.DataFramework.Paging;


namespace Phoenix.LearningPortal.Data
{
	public partial class PageViews
	{
        public static QueryPackage GetAllForGrid() {
            QueryPackage query = new QueryPackage();
            query.Statement = @"
            				select 
                                d_page_views.*, isnull(d_users.username,'')+' (ID: '+ cast(d_page_views.user_id as varchar(100)) + ')' as UserSortKey
                            from 
                                d_page_views
                                left outer join d_users on d_users.id=d_page_views.user_id                            
            ";

            return query;
        }
	}

	public partial class PageView
	{
		#region ***** ENUMS *****
			
		#endregion
	
		#region ***** CONSTRUCTORS *****        
        public PageView(jlib.components.webpage WebPage, System.Web.HttpRequest Request, System.Web.SessionState.HttpSessionState Session) {
            this.Ip = WebPage.IP;
            this.Referrer = WebPage.Referrer;
            this.UserAgent = WebPage.UserAgent;
            this.Url = Request.Url.PathAndQuery.Substr(0,800);
            this.PostData = (System.Configuration.ConfigurationManager.AppSettings["page.logging.mode"] == "full" ? convert.cStreamToString(Request.InputStream, Request.ContentEncoding) : "");
            this.IsNew = Session.IsNewSession;
        }
		#endregion
	
		#region ***** PROPERTIES *****

		#endregion
	
		#region ***** METHODS *****
			
		#endregion
	}

}