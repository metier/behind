
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using jlib.db;
using jlib.functions;
using jlib.helpers;
using jlib.DataFramework;
using jlib.DataFramework.Cache;
using jlib.DataFramework.Cache.Attributes;
using jlib.DataFramework.Paging;


namespace Phoenix.LearningPortal.Data
{
	public partial class Settings
	{
        // This method has some issues, since it will only get a setting with an exact Date
//        public static QueryPackage GetByDate(string tableName, int objectID, DateTime date) {
//            QueryPackage qp = new QueryPackage();
//            qp.Statement = @"
//        SELECT
//            d_settings.*
//        FROM 
//            d_settings,        
//            (
//                SELECT
//                    ObjectID, max(DeleteDate) AS DeleteDate
//                FROM
//                    d_settings
//                WHERE
//                    ObjectID=@ObjectID AND
//                    TableName=@TableName AND
//                    CreateDate>=@Date AND
//                    DeleteDate<=@Date
//                GROUP 
//                BY ObjectID
//            ) A
//        WHERE
//            d_settings.ObjectID=@ObjectID AND
//            d_settings.TableName=@TableName AND
//            d_settings.DeleteDate=A.DeleteDate           
//";
//            qp.Parameters.ObjectID = objectID;
//            qp.Parameters.TableName = tableName;
//            qp.Parameters.Date = date;
//            return qp;
//        }
    }

	public partial class Setting
	{
		#region ***** ENUMS *****
			
		#endregion
	
		#region ***** CONSTRUCTORS *****
			
		#endregion
	
		#region ***** PROPERTIES *****
        BaseTableRecordImpl<Phoenix.LearningPortal.Data.Context> m_oObject;
        public BaseTableRecordImpl<Context> Object {
            get {
                if (m_oObject == null)
                    m_oObject = RowLink.getDataConstructor(TableName).Invoke(RowLink.getDataType(TableName).GetType(), new object[] { ObjectID }) as BaseTableRecordImpl<Phoenix.LearningPortal.Data.Context>;

                return m_oObject;
            }
            set {
                this.ObjectID = value._CurrentObjectID;
                this.TableName = value.TABLE_NAME;
                m_oObject = value;
            }
        }
		#endregion
	
		#region ***** METHODS *****
			
		#endregion
	}

}