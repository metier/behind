if exists(select * from sysobjects where name like 'd_companies' and xType='U')
	DROP table d_companies
GO

if exists(select * from sysobjects where name like 'v_companies' and xType='V')
	Drop view [v_companies]
GO

Create view [dbo].[v_companies]
as

WITH CTE AS
(   SELECT  id, name,Parentid, 0 [Level]
    FROM    Phoenix..Customers c
    UNION ALL
    SELECT  CTE.ID, CTE.Name, Customers.ParentId, Level + 1
    FROM    CTE
            INNER JOIN Phoenix..Customers
                ON CTE.ParentId = Customers.Id
    WHERE   Customers.ParentId IS NOT NULL
)

SELECT  c.ID, c.Name, pc.ParentId as Parent_id, 0 as is_root, c.ParentId as top_parent_id
FROM    (   SELECT  *, MAX([Level]) OVER (PARTITION BY id) [MaxLevel]
            FROM    CTE
        ) c
		inner join Phoenix..Customers pc on pc.Id=c.Id
WHERE   MaxLevel = Level


GO

if exists(select * from sysobjects where name like 'd_users' and xType='U')		
	exec sp_rename 'd_users', 'd_behind_users'	
GO
delete from d_behind_users where id>0

if exists(select * from sysobjects where name like 'd_users' and xType='V')
	Drop view d_users
GO

Create view [dbo].[d_users]
as
select u.ID, mu.Email, MembershipUserId as Username, FirstName as First_Name, LastName as Last_name, CustomerId as Company_ID, c.Name as Company_name
from Phoenix..Users u
inner join Phoenix..Customers c on c.Id=u.CustomerId
inner join Phoenix..[vw_aspnet_MembershipUsers] mu on mu.UserName=u.MembershipUserId
union select * from d_behind_users