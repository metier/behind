﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.components;
using jlib.db;
using jlib.functions;
using System.Text;
using System.Linq;
using System.Security.Cryptography;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal
{
    public partial class Library : jlib.components.webpage
    {

        private Util.Classes.user m_oActiveUser;

        public void Page_Load(object sender, EventArgs e)
        {
            m_oActiveUser = (Page.Master as Inc.Master.MyMetier).ActiveUser;
            if (m_oActiveUser.FileFolderIDs.Count == 0) Response.Redirect("index.aspx");
            (Page.Master as Inc.Master.MyMetier).Section = Inc.Master.MyMetier.SiteSection.Files;
            m_oActiveUser.FileFolderIDs.ForEach(FolderID =>
            {
                AttackData.Folder Folder = AttackData.Folder.LoadByPk(FolderID);
                if (Folder != null)
                {
                    Control FileSection = jlib.helpers.control.cloneObject(pDownloadSectionTemplate) as Control;
                    List<jlib.components.iControl> FileSectionControls = jlib.helpers.control.getDynamicControls(FileSection, null);
                    lFileSections.Controls.Add(FileSection);
                    jlib.helpers.control.populateValue(FileSectionControls, "heading", Folder.AuxField1);
                    label FileContainer = (jlib.helpers.control.findControlsByDataMember(FileSectionControls, "list")[0] as label);
                    Folder.GetChildren<AttackData.Content>().ForEach(File =>
                    {

                        Control FileControl = jlib.helpers.control.cloneObject(pDownloadItemTemplate) as Control;
                        FileContainer.Controls.Add(FileControl);
                        string sFileType = System.IO.Path.GetExtension(File.Filename).ToLower();
                        if (sFileType == ".doc" || sFileType == ".docx") sFileType = "Microsoft Word Document";
                        else sFileType = parse.replaceAll(sFileType, ".", "").ToUpper() + " Document";

                        List<jlib.components.iControl> oControls = jlib.helpers.control.getDynamicControls(FileControl, null);
                        jlib.helpers.control.populateValue(oControls, "title", File.Name);
                        jlib.helpers.control.populateValue(oControls, "details", sFileType);
                        hyperlink oLink = (jlib.helpers.control.findControlsByDataMember(oControls, "link")[0] as hyperlink);
                        oLink.NavigateUrl = "https://mymetier.net" + File.Filename;

                    });
                }
            });

            pDownloadSectionTemplate.Controls.Clear();
            pDownloadItemTemplate.Controls.Clear();
        }
    }
}