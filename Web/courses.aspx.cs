﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Data.Common;
using System.Web;
using System.Web;
using System.Linq;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.components;
using jlib.functions;
using System.Text;
using System.Security.Cryptography;

namespace Phoenix.LearningPortal
{
    public partial class Courses : jlib.components.webpage
    {

        public void Page_Init(object sender, EventArgs e)
        {
            Util.Classes.user activeUser = (this.Master as Inc.Master.MyMetier).ActiveUser;

            if (!activeUser.AllowEnroll) Response.Redirect("index.aspx?user_id=" + this.QueryString("user_id"));
            (Page.Master as Inc.Master.MyMetier).Section = Inc.Master.MyMetier.SiteSection.Catalog;


            List<Util.Classes.ActivitySet> oAvailableCourses = activeUser.AvailableCourses;
            pTermsContainer.Visible = parse.stripHTML(activeUser.Organization.TermsAndConditions).Length > 5;

            if (pTermsContainer.Visible)
                ClientScript.RegisterClientScriptBlock(this.GetType(), "fnDisplayTerms", "function fnDisplayTermsDialog(){$('#terms-dialog').detach();$(\"<div id='terms-dialog'>" + parse.replaceAll(activeUser.Organization.TermsAndConditions, "\"", "&quot;", "\r", "", "\n", "") + "</div>\").appendTo(document.body).dialog({ modal: true, 'height': 400, 'width': 400, position: ['center', 'center'] }).dialog('open').dialog('option', 'title', \"" + parse.replaceAll(jlib.helpers.translation.translate(this.Language, "", "enroll.aspx", Page, "terms-dialog-title"), "\"", "&quot;") + "\");};", true);


            List<Util.Classes.ActivitySet> oRows = new List<Util.Classes.ActivitySet>();
            for (int x = 0; x < oAvailableCourses.Count; x++)
            {
                if (oAvailableCourses[x].Activities.FirstOrDefault(activity => !Util.MyMetier.IsPrince2SpecialHandling(activity.VersionNumber)) == null)
                {
                    oAvailableCourses.RemoveAt(x);
                    x--;
                }
                else
                {
                    oRows.Add(oAvailableCourses[x]);
                }
            }
            int iNumFeatured = 0;
            if (!this.IsAjaxPostback)
            {
                oRows.Shuffle();
                for (int x = 0; x < oRows.Count && iNumFeatured < 3; x++)
                {
                    if (oRows[x].Activities.Count > oRows[x].GetActivityByType(Util.Data.MetierClassTypes.Exam).Count)
                    {
                        List<List<Util.Classes.Activity>> oGroupedOfferings = new List<List<Util.Classes.Activity>>() { new List<Util.Classes.Activity>(), new List<Util.Classes.Activity>(), new List<Util.Classes.Activity>(), new List<Util.Classes.Activity>(), new List<Util.Classes.Activity>() };

                        bool bHasAccommodation = false;
                        for (int y = 0; y < oRows[x].Activities.Count; y++)
                        {
                            if (oRows[x].Activities[y].ArticleType.Int() < oGroupedOfferings.Count)
                                oGroupedOfferings[oRows[x].Activities[y].ArticleType.Int()].Add(oRows[x].Activities[y]);
                            if (oRows[x].Activities[y].ArticleType == Util.Data.MetierClassTypes.Accommodation)
                                bHasAccommodation = true;
                        }

                        Control oControl = jlib.helpers.control.cloneObject(pFeaturedTemplate) as Control;
                        oControl.ID = "pFeaturedTemplate_" + x;
                        List<jlib.components.iControl> oControls = jlib.helpers.control.getDynamicControls(oControl, null);
                        if (iNumFeatured == 0) (jlib.helpers.control.findControlById(Page, oControl.Controls, "lContainer")[0] as label).CssClass += " first";
                        jlib.helpers.control.populateValue(oControls, "heading", oRows[x].Name);

                        jlib.helpers.control.populateValue(oControls, "course-type", (oRows[x].Activities.Count == 1 ? (oGroupedOfferings[(int)Util.Data.MetierClassTypes.Exam].Count == 1 ? jlib.helpers.translation.translate(this.Language, "", this, "exam") : oGroupedOfferings[(int)Util.Data.MetierClassTypes.Classroom].Count == 1 ? jlib.helpers.translation.translate(this.Language, "", this, "classroom") : jlib.helpers.translation.translate(this.Language, "", this, "elearning")) : parse.stripEndingCharacter((oGroupedOfferings[(int)Util.Data.MetierClassTypes.Exam].Count > 1 ? jlib.helpers.translation.translate(this.Language, "", this, "exam") + "+" : "") + (oGroupedOfferings[(int)Util.Data.MetierClassTypes.Classroom].Count > 1 ? jlib.helpers.translation.translate(this.Language, "", this, "classroom") + "+" : "") + (oGroupedOfferings[(int)Util.Data.MetierClassTypes.ELearning].Count > 1 ? jlib.helpers.translation.translate(this.Language, "", this, "elearning") + "+" : ""), "+")));
                        int numLessons = (oGroupedOfferings[(int)Util.Data.MetierClassTypes.ELearning].Count > 0 && oGroupedOfferings[(int)Util.Data.MetierClassTypes.ELearning][0].ELearnCourseObject != null ? oGroupedOfferings[(int)Util.Data.MetierClassTypes.ELearning][0].ELearnCourseObject.Lessons.Count : 0);
                        jlib.helpers.control.populateValue(oControls, "course-lessons", (numLessons > 0 ? numLessons + " " + jlib.helpers.translation.translate(this.Language, "", this, "lesson-" + (numLessons == 1 ? "singular" : "plural")) : ""));

                        if (activeUser.Organization.IsShowPricesToUserOnPortal && oRows[x].Price > 0) jlib.helpers.control.populateValue(oControls, "price", oRows[x].Price + " " + oRows[x].PrimaryActivity.Currency.ToLower() + (bHasAccommodation ? convert.cIfValue(jlib.helpers.translation.translate(this.Language, "", this, "price-accommodation-extra"), "<br /><span style='font-size:10px;'><i>", "</i></span>", "") : ""));
                        else (jlib.helpers.control.findControlsByDataMember(oControls, "price")[0] as label).Visible = false;


                        jlib.helpers.control.populateValue(oControls, "description", convert.cEllipsis(oRows[x].PrimaryActivity.GetDescription(), 250, true, true, true));

                        Util.Classes.Activity oClassroomImage = convert.cValue(oGroupedOfferings[(int)Util.Data.MetierClassTypes.ELearning].SafeGetValue(0), oGroupedOfferings[(int)Util.Data.MetierClassTypes.Classroom].SafeGetValue(0), oRows[x].Activities[0]);
                        (jlib.helpers.control.findControlById(Page, oControl.Controls, "iCourseImage")[0] as image).ImageUrl = "./inc/library/exec/thumbnail.aspx?z=1&u=" + System.Web.HttpUtility.UrlEncode(Util.MyMetier.getCourseImage(activeUser.Organization == null ? 0 : convert.cInt(activeUser.Organization.ParentCompanyID, activeUser.Organization.ID), oClassroomImage)) + "&w=240&h=120";
                        (jlib.helpers.control.findControlById(Page, oControl.Controls, "lCourseInfo")[0] as hyperlink).NavigateUrl = "navigator.aspx?ilm_id=" + oRows[x].Id;
                        hyperlink enroll = (jlib.helpers.control.findControlsByDataMember(oControls, "enroll")[0] as hyperlink);
                        enroll.OnClientClick = "fnEnroll(" + oRows[x].Id + ");";
                        if ((oGroupedOfferings[(int)Util.Data.MetierClassTypes.Exam].Count > 0 && !activeUser.AllowExamEnroll)
                            || (oGroupedOfferings[(int)Util.Data.MetierClassTypes.Exam].Count == 0 && !activeUser.AllowCourseEnroll)
                            )
                            enroll.Visible = false;

                        lFeaturedCourses.Controls.Add(oControl);
                        iNumFeatured++;
                    }
                }
                pFeaturedContainer.Visible = iNumFeatured > 0;
                pFeaturedTemplate.Controls.Clear();
            }
            iSortName.ImageUrl = String.Format(iSortName.ImageUrl, (hCurrentSort.Value.Contains("name") ? (hCurrentSort.Value.Contains("desc") ? "desc" : "asc") : "inactive"));
            iSortType.ImageUrl = String.Format(iSortType.ImageUrl, (hCurrentSort.Value.Contains("name") ? "inactive" : (hCurrentSort.Value.Contains("desc") ? "desc" : "asc")));

            List<Util.Classes.ActivitySet> oSearchResult = oAvailableCourses.FindAll(course => { return (tSearchName.Text.Trim().IsNullOrEmpty() || course.Name.IndexOf(tSearchName.Text, StringComparison.CurrentCultureIgnoreCase) > -1) && (hFilterType.Value.IsNullOrEmpty() || (convert.cInt(hFilterType.Value) == 4 && course.GetActivityByType(Util.Data.MetierClassTypes.ELearning).Count > 0 && course.GetActivityByType(Util.Data.MetierClassTypes.Classroom).Count > 0) || course.GetActivityByType(hFilterType.Value.Int()).Count > 0); });
            oSearchResult.Sort((a, b) => (hCurrentSort.Value.Contains("name") ? a.Name.Str().CompareTo(b.Name.Str()) : a.Activities[0].ArticleType.CompareTo(b.Activities[0].ArticleType)) * (hCurrentSort.Value.Contains("desc") ? -1 : 1));

            if (iNumFeatured == oSearchResult.Count && tSearchName.Text + hFilterType.Value == "")
            {
                aContainer.Visible = false;
            }
            else
            {
                int iPageNum = Math.Max(0, convert.cInt(hCurrentPage.Value)), iNumEntries = 0, iPageSize = 20, iNumPages = 0;
                for (int x = 0; x < oSearchResult.Count; x++)
                {
                    if ((iPageNum * iPageSize) <= iNumEntries && ((iPageNum + 1) * iPageSize) > iNumEntries)
                    {
                        List<List<Util.Classes.Activity>> oGroupedOfferings = new List<List<Util.Classes.Activity>>() { new List<Util.Classes.Activity>(), new List<Util.Classes.Activity>(), new List<Util.Classes.Activity>(), new List<Util.Classes.Activity>(), new List<Util.Classes.Activity>() };
                        for (int y = 0; y < oSearchResult[x].Activities.Count; y++)
                        {
                            if (oSearchResult[x].Activities[y].ArticleType.Int() < oGroupedOfferings.Count)
                            {
                                oGroupedOfferings[oSearchResult[x].Activities[y].ArticleType.Int()].Add(oSearchResult[x].Activities[y]);
                            }
                        }
                        Control oControl = jlib.helpers.control.cloneObject(pCourseTableListing, new string[] { "ID" }) as Control;
                        oControl.ID = "pCourseTableListing_" + x;
                        List<jlib.components.iControl> oControls = jlib.helpers.control.getDynamicControls(oControl, null);
                        jlib.helpers.control.populateValue(oControls, "type", parse.stripEndingCharacter((oGroupedOfferings[(int)Util.Data.MetierClassTypes.ELearning].Count > 0 ? "E+" : "") + (oGroupedOfferings[(int)Util.Data.MetierClassTypes.Classroom].Count > 0 ? "K+" : "") + (oGroupedOfferings[(int)Util.Data.MetierClassTypes.Exam].Count > 0 ? "M+" : ""), "+"));
                        jlib.helpers.control.populateValue(oControls, "title", oSearchResult[x].Name);
                        (jlib.helpers.control.findControlsByDataMember(oControls, "title")[0] as hyperlink).NavigateUrl = "navigator.aspx?ilm_id=" + oSearchResult[x].Id;
                        (jlib.helpers.control.findControlsByDataMember(oControls, "info")[0] as hyperlink).NavigateUrl = "navigator.aspx?ilm_id=" + oSearchResult[x].Id;
                        hyperlink enroll = (jlib.helpers.control.findControlsByDataMember(oControls, "enroll")[0] as hyperlink);
                        enroll.OnClientClick = "fnEnroll(" + convert.cLong(oSearchResult[x].Id) + ");";
                        if ((oGroupedOfferings[(int)Util.Data.MetierClassTypes.Exam].Count > 0 && !activeUser.AllowExamEnroll)
                            || (oGroupedOfferings[(int)Util.Data.MetierClassTypes.Exam].Count == 0 && !activeUser.AllowCourseEnroll)
                            )
                            enroll.Visible = false;

                        lCourseTable.Controls.Add(oControl);
                    }
                    iNumEntries++;
                }
                iNumPages = Math.Max(0, 1 + ((iNumEntries - 1) / iPageSize));
                lPagination1.Text = String.Format(jlib.helpers.translation.translate(this.Language, "", this, "paging"), (iPageNum * iPageSize) + 1, Math.Min(iNumEntries, (iPageSize * (iPageNum + 1))), iNumEntries);
                pCourseTableListing.Controls.Clear();

                lFilterElearn.Visible = activeUser.AvailableCourseTypes[(int)Util.Data.MetierClassTypes.ELearning] > 0;
                lFilterClassroom.Visible = activeUser.AvailableCourseTypes[(int)Util.Data.MetierClassTypes.Classroom] > 0;
                lFilterEandC.Visible = activeUser.AvailableCourseTypes[(int)Util.Data.MetierClassTypes.IntegratedLearning] > 0;
                lFilterExam.Visible = activeUser.AvailableCourseTypes[(int)Util.Data.MetierClassTypes.Exam] > 0;
                lFilterType.Visible = hFilterType.Value != "";
                lFilterClear.Text = (convert.cInt(hFilterType.Value) == 4 ? jlib.helpers.translation.translate(this.Language, "", this, "filter-ec") : (convert.cInt(hFilterType.Value) == (int)Util.Data.MetierClassTypes.Classroom ? jlib.helpers.translation.translate(this.Language, "", this, "filter-classroom") : (convert.cInt(hFilterType.Value) == (int)Util.Data.MetierClassTypes.Exam ? jlib.helpers.translation.translate(this.Language, "", this, "filter-exam") : jlib.helpers.translation.translate(this.Language, "", this, "filter-elearn"))));
                if (iNumPages > 1)
                {
                    hPagePrev.OnClientClick = "$('.current-page').val('" + Math.Max(0, iPageNum - 1) + "');AJAXControlAction($(this).attr('name'),null,null);";
                    hPageNext.OnClientClick = "$('.current-page').val('" + Math.Min(iNumPages - 1, iPageNum + 1) + "');AJAXControlAction($(this).attr('name'),null,null);";
                    int iPaginationStart = Math.Max(0, iPageNum - (iPageNum > 0 && iPageNum == iNumPages - 1 ? 2 : 1));
                    hPage1.Text = (iPaginationStart + 1).ToString();
                    hPage2.Text = (iPaginationStart + 2).ToString();
                    hPage3.Text = (iPaginationStart + 3).ToString();
                    hPage1.CssClass = (iPaginationStart == iPageNum ? "active" : "");
                    hPage2.CssClass = (iPaginationStart + 1 == iPageNum ? "active" : "");
                    hPage3.CssClass = (iPaginationStart + 2 == iPageNum ? "active" : "");
                    hPage2.Visible = (iPaginationStart + 1 < iNumPages);
                    hPage3.Visible = (iPaginationStart + 2 < iNumPages);
                }
                else
                {
                    pPagination.Visible = false;
                }
            }
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "popup-title", "var sDialogTitle=\"" + parse.replaceAll(jlib.helpers.translation.translate(this.Language, "", "/enroll.aspx", Page, "details-heading"), "\"", "'") + "\";", true);

            if (lCourseTable.Controls.Count == 0 && lFeaturedCourses.Controls.Count == 0)
            {
                aContainer.Visible = false;
                lNoAvailableCourses.Visible = true;
            }
        }
    }
}