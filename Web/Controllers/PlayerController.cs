﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttackData = Phoenix.LearningPortal.Data;
using jlib.functions;
using System.Runtime.Caching;

namespace Phoenix.LearningPortal.Controllers
{
    public class PlayerController : ApiController
    {

        // GET api/<controller>/5
        private static MemoryCache Cache = new MemoryCache("Course");
        public static void ResetLessonCache(int CourseId, int LessonId)
        {
            var keyPrefix = "/course/" + CourseId + "/lesson/" + LessonId;
            Cache.ToList().ForEach(x => {
                if (x.Key.StartsWith(keyPrefix)) Cache.Remove(x.Key);
            });            
        }
        [HttpGet]
        [Route("api/player/course/{CourseId:int}/lesson/{LessonId:int}")]
        [Route("api/player/course/{CourseId:int}/lesson/{LessonId:int}/page/{PageId:int}")]
        [Route("api/player/course/{CourseId:int}/lesson/{LessonId:int}/{Preview}")]
        public object GetLesson(int CourseId, int LessonId, int? PageId=0, bool? Preview=false, bool Printing = false, bool IncludeAllLessons = false)
        {            
            string key = "/course/" + CourseId + "/lesson/" + LessonId+"/page/"+ PageId + (IncludeAllLessons ? "?IncludeAllLessons=true" : "");

			lock (Cache)
            {
                if (!Preview.Bln() && Cache.Contains(key)) return Cache[key];
                var manager = new Managers.PlayerManager(CourseId, LessonId, PageId.Value, false, Printing, IncludeAllLessons);
                object value = null;
                if (PageId > 0) value = manager.GetPage();
                else value = manager.GetLesson();
                if(!Preview.Bln() && !manager.IsFinalTest && !IncludeAllLessons) Cache.Set(key, value, DateTime.Now.AddMinutes(30));
                return value;
            }            
        }

        [HttpPost]
        [Route("api/player/print")]
        public Models.PrintResponse Print([FromBody]Models.PrintRequest PrintRequest)
        {
            var manager = new Managers.PlayerManager(0, PrintRequest.LessonId, PrintRequest.PageId);
            return manager.PrintLesson(PrintRequest);            
        }

        [HttpGet]
        [Route("api/permission/cms/partial/{UserId:int}")]
        public bool HasPartialCMSAccess(int UserId)
        {
            var hasPartialAccess = (new jlib.db.ado_helper("sql.dsn.cms")).Execute_SQL("select top 1 UserId from d_folder_permissions where UserID=@user_id", "@user_id", UserId).Rows.Count > 0;
            if(hasPartialAccess) new jlib.db.SqlHelper(System.Configuration.ConfigurationManager.AppSettings["sql.dsn.cms"]).ExecuteSproc("sp_recalc_permissions", "@user_id", UserId);
            return hasPartialAccess;
        }
        [HttpPost]
        [Route("api/user/{UserId:int}/activity/{CurrentActivityId:int}/activityset/{CurrentActivitySetId:int}/switch/to/{ToActivitysetId:int}")]
        public UserActivityDetails PerformExamSwitch(int UserId, int CurrentActivityId, int CurrentActivitySetId, int ToActivitysetId)
        {
            System.Web.HttpContext.Current.Items["DisableLoginRedirect"] = true;
            var user = Util.Classes.user.getUser(UserId);
            var details = GetUserActivityDetails(UserId, CurrentActivityId, CurrentActivitySetId, true);
            var toActivitySetFoundation = details.P2FoundationExamDates.FirstOrDefault(x => x.Key == ToActivitysetId);
            var toActivitySetPractitioner = details.P2PractitionerExamDates.FirstOrDefault(x => x.Key == ToActivitysetId);

            if (toActivitySetFoundation.IsDefault() && toActivitySetPractitioner.IsDefault()) throw new Exception($"{ToActivitysetId} is not a valid activity set to switch to with the P2 Exam Switcher.");

            var fromActivitySetId = (!toActivitySetFoundation.IsDefault() ? details.FoundationExamActivitysetId : details.PractitionerExamActivitysetId);            
            if (fromActivitySetId > 0)
            {
                var unenrollResult = user.PhoenixSession.PhoenixPut(String.Format(Util.Phoenix.session.Queries.ActivitySetUnenroll, fromActivitySetId, UserId));
                if (unenrollResult.Error) throw new Exception($"An error occurred trying to unenroll from activityset {fromActivitySetId}.");
            }

            var result = new Util.Phoenix.session(true).PhoenixPut("ActivitySets/" + ToActivitysetId + "/enroll?userId=" + UserId);

            if (result.Error)
            {
                throw new Exception($"An error occurred trying to enroll in activityset {ToActivitysetId}.");
            }
            else
            {
                //if the activitySet the user enrolled in is take-home exam (has prevent unenrollment), send an email to Sumbal 
                //var newActivitySet = new Util.Classes.ActivitySet(user.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ActivitySetGet, ToActivitysetId)).DataObject, user);
                //if (!newActivitySet.CanUnenroll)
                 //   Util.Email.sendEmail("noreply@mymetier.net", "sumbal.akhtar@metier.no;charlotte.wikdal.halle@metier.no", "jorgen@bids.no", "", "[mymetier.net] A user just switched to a P2 take-home exam?", "The user " + user.Username + " just enrolled in " + newActivitySet.Name + ". This activityset is set as prevent unenrollment, which probably means it's a P2 take-home exam.", true, null);

                user.PhoenixSession.PhoenixClearCache();
                System.Web.HttpContext.Current.Items["DisableLoginRedirect"] = true;
                user = Util.Classes.user.getUser(user.ID, true);

                return GetUserActivityDetails(UserId, CurrentActivityId, ToActivitysetId, true);
            }
        }

        [HttpGet]
        [Route("api/user/{UserId:int}/activity/{ActivityId:int}/activityset/{ActivitySetId:int}/details")]
        [Route("api/user/{UserId:int}/activity/{ActivityId:int}/activityset/{ActivitySetId:int}/details/{*IsEnrolled}")]
        public UserActivityDetails GetUserActivityDetails(int UserId, int ActivityId, int ActivitySetId, bool? IsEnrolled = false)
        {
            var details = new UserActivityDetails { };

            //Calculate PRINCE2 exam swticher
            //if (IsEnrolled.Bln()) PopulatePRINCE2ExamSwitcher(details, UserId, ActivitySetId);

            if (AttackData.Settings.GetByField(ObjectID: ActivityId, TableName: "Activities", KeyName: "Type", Value: "webinar").Execute().FirstOrDefault() != null) details.Webinar = true;
            AttackData.RowLinks.GetByField(Table1: "Activities", Table2: "d_folders", ID1: ActivityId).Execute().ForEach(link =>
            {
                if (link.Object2 as AttackData.Folder != null && link.Object2.GetParents<AttackData.Folder>().SafeGetValue(0).ObjectID == (IsEnrolled.Bln() ? 119598 : 119599))
                {
                    foreach (var content in (link.Object2 as AttackData.Folder).GetChildren<AttackData.Content>())
                    {
                        details.PageContent += "<div class='cms-content'>" + content.Xml + "</div>";
                    }                    
                }
            });


            return details;
        }

        private class Codes
        {
            public static string FExam = "EPM22";
            public static string PExam = "EPM23";
            public static string FElearn = "PM22E";
            public static string PElearn = "PM23E";
            public static string FPElearn = "PM24E";
        }
        private void PopulatePRINCE2ExamSwitcher(UserActivityDetails Details, int UserId, int ActivitySetId)
        {
            System.Web.HttpContext.Current.Items["DisableLoginRedirect"] = true;
            var user = Util.Classes.user.getUser(UserId);
            if (user == null || user.DistributorId != 1194 || user.OrganizationID == 4369) return;

            //Not enrolled
            if (!user.Enrollments.Any(x => x.ActivitySetId == ActivitySetId)) return;


            //If not arbitrary, return unless explicitly turned on for customer 
            if (!user.Organization.IsArbitrary &&
                AttackData.Settings.GetByField(ObjectID: user.OrganizationID, TableName: "Customers", KeyName: "ExamSwitcher", Value: "show").Execute().FirstOrDefault() == null)
                return;

            //If not arbitrary, show unless explicitly hidden for customer
            if (user.Organization.IsArbitrary &&
                AttackData.Settings.GetByField(ObjectID: user.OrganizationID, TableName: "Customers", KeyName: "ExamSwitcher", Value: "hide").Execute().FirstOrDefault() != null)
                return;

            //Hide for activitySet if explcitly hidden
            if (AttackData.Settings.GetByField(ObjectID: ActivitySetId, TableName: "ActivitySets", KeyName: "ExamSwitcher", Value: "hide").Execute().FirstOrDefault() != null)
                return;

            var phoenixSession = new Util.Phoenix.session(true);

            var activitySet = new Util.Classes.ActivitySet(phoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ActivitySetGet, ActivitySetId), true, true).DataObject, user);
            var examActivities = activitySet.Activities.Where(x => x.IsCaseExam || x.IsProjectAssignment || x.IsMPCExam || x.IsExternalCertification).ToList();

            var isViewingExamActivity = activitySet.Activities.Any(x => x.ArticleNumber.StartsWith(Codes.FExam) || x.ArticleNumber.StartsWith(Codes.PExam));

            if (isViewingExamActivity)
            {
                //if viewing an exam, we need to check enrollment in the corresponding P2 course
                var course = user.Enrollments.FirstOrDefault(enrollment => { return enrollment.ArticleNumber.StartsWith(Codes.FPElearn) || enrollment.ArticleNumber.StartsWith(examActivities[0].ArticleNumber.Substr(1, 4) + "E"); });
                if (course != null)
                {
                    var IsFoundationEligble = examActivities[0].ArticleNumber.StartsWith(Codes.FExam);
                    var IsPractitionerEligble = examActivities[0].ArticleNumber.StartsWith(Codes.PExam);
                    if (activitySet.CanUnenroll && examActivities[0].Participation.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Enrolled)
                    {
                        if (IsFoundationEligble)
                        {
                            Details.CanSwitchFoundation = true;
                            Details.FoundationExamActivitysetId = activitySet.Id;
                            Details.FoundationExamActivityId = examActivities[0].Id;
                            Details.FoundationExamActivitysetName = activitySet.Name;
                        }
                        if (IsPractitionerEligble)
                        {
                            Details.CanSwitchPractitioner = true;
                            Details.PractitionerExamActivitysetId = activitySet.Id;
                            Details.PractitionerExamActivityId = examActivities[0].Id;
                            Details.PractitionerExamActivitysetName = activitySet.Name;
                        }
                    }
                }
            }
            else
            {
                //if not viewing an exam, make sure we're viewing a P2 elearning course
                var course = activitySet.Activities.FirstOrDefault(activity => { return activity.ArticleNumber == Codes.FElearn || activity.ArticleNumber == Codes.PElearn || activity.ArticleNumber == Codes.FPElearn; });
                if (course == null) return;

                var IsFoundationEligble = course.ArticleNumber == Codes.FElearn || (course.ArticleNumber == Codes.FPElearn && activitySet != null && activitySet.Name.IndexOf("Foundation", StringComparison.CurrentCultureIgnoreCase) > -1);
                var IsPractitionerEligble = course.ArticleNumber == Codes.PElearn || course.ArticleNumber == Codes.FPElearn;

                var currentFExam = user.Enrollments.FirstOrDefault(enrollment => { return enrollment.ArticleNumber == Codes.FExam && enrollment.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Enrolled; });
                var currentPExam = user.Enrollments.FirstOrDefault(enrollment => { return enrollment.ArticleNumber == Codes.PExam && enrollment.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Enrolled; });

                var startedFExams = user.Enrollments.FindAll(enrollment => { return enrollment.ArticleNumber == Codes.FExam && enrollment.ParticipantStatusCodeValue != Util.Classes.Enrollment.ParticipantStatusCodes.Enrolled; });
                var startedPExams = user.Enrollments.FindAll(enrollment => { return enrollment.ArticleNumber == Codes.PExam && enrollment.ParticipantStatusCodeValue != Util.Classes.Enrollment.ParticipantStatusCodes.Enrolled; });

                Details.CanSwitchFoundation = IsFoundationEligble && startedFExams.Count < 2 && (currentFExam == null || currentFExam.ActivitySetObject.CanUnenroll);
                Details.CanSwitchPractitioner = IsPractitionerEligble && startedPExams.Count < 2 && (currentPExam == null || currentPExam.ActivitySetObject.CanUnenroll);

                if (currentFExam != null)
                {
                    Details.FoundationExamActivitysetId = currentFExam.ActivitySetObject.Id;
                    Details.FoundationExamActivityId = currentFExam.ActivityId;
                    Details.FoundationExamActivitysetName = currentFExam.ActivitySetObject.Name;
                }
                if (currentPExam != null)
                {
                    Details.PractitionerExamActivitysetId = currentPExam.ActivitySetObject.Id;
                    Details.PractitionerExamActivityId = currentPExam.ActivityId;
                    Details.PractitionerExamActivitysetName = currentPExam.ActivitySetObject.Name;
                }
            }


            var onlineCustomer = Util.Classes.user.getOrganization(2338);
            lock (onlineCustomer)
            {
                //Only include ActivitySets that are either take-home (!x.CanUnenroll) or in the future
                List<Util.Classes.ActivitySet> sortedCalendar = onlineCustomer.CourseCalendar.Where(x=>x.Activities.Count == 1 && (x.Activities[0].ActivityStart.Date() > DateTime.Now || !x.CanUnenroll )).ToList();
                sortedCalendar.Sort((x, y) => x.Activities.Count() > 0 && y.Activities.Count() > 0 ? x.Activities[0].ActivityStart.Date().CompareTo(y.Activities[0].ActivityStart.Date()) : 0);
                if (Details.CanSwitchFoundation)
                {
                    sortedCalendar.ForEach(set =>
                    {
                        if (set.Activities.Count == 1 && set.Activities.FirstOrDefault(activity => { return activity.ArticleNumber == Codes.FExam; }) != null) Details.P2FoundationExamDates.Add(new KeyValuePair<int, string>(set.Id, set.Name));
                    });
                    
                }

                if (Details.CanSwitchPractitioner)
                {                    
                    sortedCalendar.ForEach(set =>
                    {
                        if (set.Activities.Count == 1 && set.Activities.FirstOrDefault(activity => { return activity.ArticleNumber == Codes.PExam; }) != null) Details.P2PractitionerExamDates.Add(new KeyValuePair<int, string>(set.Id, set.Name));
                    });
                    
                }
            }
        }

        public class UserActivityDetails
        {
            public List<KeyValuePair<int, string>> P2FoundationExamDates { get; set; } = new List<KeyValuePair<int, string>>();
            public List<KeyValuePair<int, string>> P2PractitionerExamDates { get; set; } = new List<KeyValuePair<int, string>>();
            public bool Webinar { get; set; }
            public string PageContent { get; set; }
            public bool CanSwitchFoundation { get; set; }
            public bool CanSwitchPractitioner { get; set; }
            public int FoundationExamActivityId { get; set; }
            public int PractitionerExamActivityId { get; set; }
            public int FoundationExamActivitysetId { get; set; }
            public int PractitionerExamActivitysetId { get; set; }
            public string FoundationExamActivitysetName { get; set; }
            public string PractitionerExamActivitysetName { get; set; }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}