﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttackData = Phoenix.LearningPortal.Data;
using jlib.functions;
using System.Runtime.Caching;

namespace Phoenix.LearningPortal.Controllers
{
    public class BadgeController : ApiController
    {

        private string GetHostAndSchema()
        {
            return $"{this.Request.RequestUri.Scheme}://{this.Request.RequestUri.Host}";
        }

        [HttpGet]
        [Route("api/badge/activity/{ActivityId:int}/user/{UserId:int}")]        
        public Models.Badge GetBadge(int ActivityId, int UserId)
        {

            var user = Util.Classes.user.getUser(UserId);
            user.PhoenixSession = new Util.Phoenix.session(true);

            var activity = new Util.Classes.Activity(user.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ActivityGet, ActivityId), true, true).DataObject, user);
            //var customerArticle = new Util.Classes.CustomerArticle(phoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.CustomerArticleGet, activity.CustomerArticleId), true, true).DataObject, null);
            //var description = parse.stripHTML(parse.replaceAll(activity.GetDescription() ?? "", "</div><div>","\n","<br>","\n", "<br />", "\n"));
            var description = parse.stripHTML(parse.replaceAll(activity.GetDescription() ?? "", "</div><div>", " ", "<br>", "\n", "<br />", "\n"));



            return new Models.Badge 
            {
                Image = $"{GetHostAndSchema()}{Util.MyMetier.GetBadgeImageUrl(activity)}",
                //Criteria = activity.LanguageId == 45 ? "Fullført og bestått" : "Passed course", 
                //Criteria = $"https://mymetier.net/learningportal/api/badge/criteria/activity/{ActivityId}/user/{UserId}",
                Criteria = $"{GetHostAndSchema()}/learningportal/badges/criteria.aspx?activityid={ActivityId}&userid={UserId}",
                Description = description,
                Name = activity.ArticleCopyObject.Title
            };
        }

        [HttpGet]
        [Route("api/badge/award/activity/{ActivityId:int}/user/{UserId:int}")]
        public Models.BadgeAward AwardBadge(int ActivityId, int UserId)
        {

            var user = Util.Classes.user.getUser(UserId);
            user.PhoenixSession = new Util.Phoenix.session(true);

            var participant = user.Enrollments.Where(e => e.ActivityId == ActivityId).OrderBy(e => e.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Completed ? 0 : 1).FirstOrDefault();

            if (participant == null) throw new Exception($"Activity {ActivityId} not found for user {UserId} -- maybe they have been unenrolled?");
            if (participant.ParticipantStatusCodeValue != Util.Classes.Enrollment.ParticipantStatusCodes.Completed) throw new Exception($"Activity {ActivityId} for user {UserId} (participant {participant.Id}) is not completed.");
            
            

            //var phoenixSession = new Util.Phoenix.session(true);
            
            

            //var activity = new Util.Classes.Activity(phoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ActivityGet, ActivityId), true, true).DataObject, phoenixSession);
            //var user = new Util.Classes.pa(phoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ParticipantsGet, ActivityId), true, true).DataObject, phoenixSession);
            return new Models.BadgeAward
            {
                Uid = participant.Id.ToString(),
                Recipient = new Models.BadgeAwardRecipient 
                { 
                    Identity = user.Email
                },
                //1388534400                
                IssuedOn = (Int32)((participant.CompletedDate ?? DateTime.UtcNow.Date).Subtract(new DateTime(1970, 1, 1))).TotalSeconds,
                Badge = $"{GetHostAndSchema()}/learningportal/api/badge/activity/{ActivityId}/user/{UserId}",
                Verify = new Models.BadgeAwardVerification 
                { 
                    Url = $"{GetHostAndSchema()}/learningportal/api/badge/award/activity/{ActivityId}/user/{UserId}"
                }
                
            };

        }

        //[HttpGet]
        //[Route("api/badge/criteria/activity/{ActivityId:int}/user/{UserId:int}")]
        //public HttpResponseMessage GetBadgeCriteria(int ActivityId, int UserId)
        //{

        //    var user = Util.Classes.user.getUser(UserId);
        //    user.PhoenixSession = new Util.Phoenix.session(true);

        //    var activity = new Util.Classes.Activity(user.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ActivityGet, ActivityId), true, true).DataObject, user);
        //    //var customerArticle = new Util.Classes.CustomerArticle(phoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.CustomerArticleGet, activity.CustomerArticleId), true, true).DataObject, null);
        //    //var description = parse.stripHTML(parse.replaceAll(activity.GetDescription() ?? "", "</div><div>","\n","<br>","\n", "<br />", "\n"));
        //    var description = parse.stripHTML(parse.replaceAll(activity.GetDescription() ?? "", "</div><div>", " ", "<br>", "\n", "<br />", "\n"));

        //    var response = new HttpResponseMessage();
        //    response.Content = new StringContent($"<html><body><h1>{System.Web.HttpUtility.HtmlEncode(activity.ArticleCopyObject.Title)}</h1><div>{System.Web.HttpUtility.HtmlEncode(activity.GetDescription())}</div><div>{System.Web.HttpUtility.HtmlEncode(activity.GetCourseInfo())}</div></body></html>");
        //    response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/html");
        //    return response;
        //}
    }
}