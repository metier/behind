﻿<%@ Page Title="myMetier Participants" Language="C#" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Participants" MasterPageFile="~/inc/master/mymetier.master" Codebehind="participants.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">
    <script language="javascript" src="./inc/js/MetaGrid.js"></script>
<div id="wrapper-introduction">
<common:label runat="server" TranslationKey="heading" Tag="h1" />   
<common:label runat="server" TranslationKey="sub-heading" Tag="p" />     
</div>
      <style>
            .paging div{
                float:left;
            }
            .metagrid a.sort-column{
                background-repeat:no-repeat;
                background:url(gfx/icon-arrow-sort-inactive.gif);    
                display:inline-block;
                width:9px;
                height:8px;          
                margin-left:10px;  
            }
          .metagrid a.sort-column.active {
              background:url(gfx/icon-arrow-sort-asc.gif);    
          }
          .metagrid a.sort-column.active.desc {
              background:url(gfx/icon-arrow-sort-desc.gif);    
          }
        </style>
<div class="supertable">

    <div id="UserSelector" class="metagrid">
        <div class="header nine-rounded">
            <common:label runat="server" Tag="h1" TranslationKey="filter-name" />   		
		    <div class="search">
			    <p><div class="search-container"></div></p>
		    </div>
		    <div class="clear"></div>
	    </div>
        <div class="templates">
            <div class="facet-container">{0}</div>
            <li class="facet"><a href="javascript:void(0)" class="facet-item">{0}</a></li>
            <!--<div class="facet-undo"><a href="javascript:void(0)" class="facet-item">&lt; See all</a></div>-->
            <table>
            <tr class="row">
                <td class="first left-rounded blue">#FName#</td><td>#LName#</td><td id="template-points">#Points#</td> <td class="last right-rounded dark-blue"><a href="user.aspx?id=#Id#">Informasjon</a></td>               
            </tr>
                </table>
            <div class="title">Viser {0} - {1} av {2} deltakere</div>
            <div class="sort"><a href="javascript:void(0)" class="sort-item">{0}</a></div>
            <div class="paging">
                <div class="prev"></div>
                <div class="page-number"><a href="javascript:void(0)">1</a></div>
                <div class="next"></div>
            </div>
            <div class="search">
                <input type="text" class="search search_input"  />
            </div>
        </div>

        <div class="supertableleft">
		<div class="status"> <p class="title"></p></div>

		<div class="sorter nine-rounded">
        <common:label runat="server" Tag="p" TranslationKey="improve-your-search" />   					
            <common:hidden runat="server" ID="hFilterIlm" CssClass="ilm-filter" />
            <common:hidden runat="server" ID="hFilterClass" CssClass="class-filter" />
            
                <ul class="filter-remove"></ul>
                <common:label runat="server" Tag="h3" TranslationKey="courses" />   								
				<ul>                
                    <div class="facets"></div>				
				</ul>
		</div>
	</div>                                
            <table>
                <thead>
			<tr>
				<th><common:label runat="server" TranslationKey="first-name" /><a href="#" class="sort-column active" sort-function="return row['FName'];"></a></th>
				<th><common:label runat="server" TranslationKey="last-name" /><a href="#" class="sort-column" sort-function="return row['LName'];"></a></th>
                <th id="header-points"><common:label runat="server" TranslationKey="points" /><a href="#" class="sort-column" sort-function="return row['Points'];"></a></th>
				<th></th>
			</tr>
		</thead>
		<tbody class="list">     
            </tbody>
                <tfoot>
                    <tr>                
                        <td colspan="4" style="text-align:center" class="pagination"><a href="javascript:void(0)" class="prev">&lt;</a><a href="javascript:void(0)" class="next">&gt;</a></td>
                    </tr>
                </tfoot>
            </table>            
    </div>

</div>
  
<script language="javascript">
    $("form").submit(function () { return false; });
    $(".search_input").keydown(function (e) { if (e.which == 13) $(this).blur(); });    
    $(function () {
                
        if (participants.length > 0 && participants[0]["Points"] == -1) $("#template-points, #header-points").detach();
        var container = $("#UserSelector").on("renderEnd", function (e,grid) {            
            container.find(".pagination a").not(".prev, .next").detach();
            var index = Math.max(0, container.data("Grid").PageIndex - 1);
            for (var x = 0; x < index + 3 && x < container.data("Grid").CalculatePageCount() ; x++) {
                (container).find(".pagination a.next").before($("<a href='javascript:void(0)'/>").html(x + 1).data("index", x).click(function () { container.data("Grid").PageIndex = $(this).data("index"); container.data("Grid").Render(); }).toggleClass("active", x == container.data("Grid").PageIndex));                
            }
            container.find(".facet.active").detach();
            container.find(".filter-remove").html("");
            $.each(grid.Filters, function () {
                var $filter = this;
                if (this.Facet) container.find(".filter-remove").append($("<li><a href='javascript:void(0)'>" + this.Value + "</a></li>").click(function () { grid.Filters.removeByValue(null, $filter); grid.Render(); }));
            });
            container.find(".row").addClass("highlight");
            try { fnEnableJSEffects(); } catch (e) { }
        });
        var grid = new MetaGrid(
            {
                Container: container,
                FacetColumns: [{ ID: "Courses" }],
                FacetOptions: { Sort: function (a,b) {return (a.Count==b.Count?0:a.Count>b.Count?-1:1) },MaxItems:5},
                SortColumn:0,
                SortColumns: [],
                PageSize:15,
                SearchColumns: [{ ID: "Main", Method: function (row, grid) { return row["FName"] + " " + row["LName"]; }, SearchOnBlur: true }],
                Data: participants
            });
        container.find(".prev").click(function () { if (grid.PageIndex > 0) { grid.PageIndex--; grid.Render(); } });
        container.find(".next").click(function () { if (grid.PageIndex < grid.CalculatePageCount()-1) { grid.PageIndex++; grid.Render(); } });
        grid.Render();        
    });
   
</script>    
</asp:Content>