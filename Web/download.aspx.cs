﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.functions;

namespace Phoenix.LearningPortal
{
    public partial class Download : jlib.components.webpage
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (!Request.Browser.Type.ToUpper().Contains("IE") || Request.Browser.MajorVersion > 8) Response.Cache.SetCacheability(HttpCacheability.NoCache);            
            (Page.Master as Phoenix.LearningPortal.Inc.Master.Default).clearWrapper().DisableLoginRedirect = true;            
        }
        protected override void OnInit(EventArgs e)
        {
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (this.QueryString("file") != "")
            {
                Response.ContentType = jlib.net.HTTP.lookupMimeType(this.QueryString("file"));
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + System.Web.HttpUtility.UrlEncode(this.QueryString("file")));
                Response.WriteFile(System.Configuration.ConfigurationManager.AppSettings["temp.path"] + "\\" + parse.replaceAll(this.QueryString("file"), "\\", "", "//", ""));
                Response.End();
            }
            base.OnInit(e);


            Response.Clear();
            if (Request["FileId"].Int() > 0 && parse.replaceAll(Request["secret"], " ", "") == parse.replaceAll(Util.Permissions.encryptString(Request["FileId"] + System.Configuration.ConfigurationManager.AppSettings["phoenix.api.token"]), "+", ""))
            {
                Util.Phoenix.session.RequestResult result = new Util.Phoenix.session().PhoenixRequest("GET", String.Format(Util.Phoenix.session.Queries.FileGet, Request["FileId"]), null, false, false, true);
                Response.ContentType = result.WebResponse.ContentType;
                if (result.WebResponse.Headers["Content-Disposition"] != null)
                {
                    string contentDisposition = result.WebResponse.Headers["Content-Disposition"];
                    string extension = System.IO.Path.GetExtension(parse.replaceAll(parse.inner_substring(contentDisposition + ";", "filename=", null, null, ";"), "\"", "").Trim()).ToLower();
                    Response.ContentType = jlib.net.HTTP.lookupMimeType(extension);
                    if (extension == ".mp4" || extension == ".ppt" || extension == ".pptx" || extension == ".pdf" || extension == ".doc" || extension == ".docx" || extension == ".xls" || extension == ".xlsx") contentDisposition = parse.replaceAll(contentDisposition, "attachment; ", "");
                    Response.AddHeader("Content-Disposition", contentDisposition);
                }
                Response.BinaryWrite(convert.cStreamToByteArray(result.WebResponse.GetResponseStream()));
                if (Request["Log"].Bln() && Request["ParticipantId"].Int() > 0)
                {
                    new Util.Phoenix.session().PhoenixRequest("PUT", String.Format(Util.Phoenix.session.Queries.ParticipantSetStatus, Request["ParticipantId"], "INPROGRESS"), null, false, false, true);
                }
            }
            Response.End();
        }
    }    
}