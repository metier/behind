﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AttackData = Phoenix.LearningPortal.Data;
using jlib.functions;
using System.Runtime;

namespace Phoenix.LearningPortal.Managers
{
    public class PlayerManager
    {
        private Util.Classes.user m_oActiveUser = null;
        public bool IsFinalTest { get; set; }
        public bool IsPrinting { get; set; }
		public bool IncludeAllLessons { get; set; }

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Util.Classes.user ActiveUser
        {
            get
            {
                if (m_oActiveUser == null)
                {
                    System.Web.HttpContext.Current.Items["DisableLoginRedirect"] = true;
                    m_oActiveUser = Util.Classes.user.getUser(new Util.Phoenix.session().GetUserId());
                }
                return m_oActiveUser;
            }
        }
        public PlayerManager(int CourseId, int LessonId, int PageId, bool Preview=false, bool Printing = false, bool IncludeAllLessons = false)
        {
            this.SiteId = "";
            this.CompanyId = "";
            this.Preview = Preview;
            this.IsPrinting = Printing;
			this.IncludeAllLessons = IncludeAllLessons;

			if (CourseId > 0) CourseObject = AttackData.Folder.LoadByPk(CourseId);
            if (LessonId> 0) LessonObject = AttackData.Folder.LoadByPk(LessonId);
            if (PageId > 0) PageObject = AttackData.Folder.LoadByPk(PageId);
            if (PageObject == null && LessonObject != null) PageObject = LessonObject.GetChildren<AttackData.Folder>().FirstOrDefault(x => { return x.Context_Value == "content"; });
            if (PageObject == null)
            {
                LessonObject = CourseObject.GetChildren<AttackData.Folder>().FirstOrDefault();
                if(LessonObject!=null)
                    PageObject = LessonObject.GetChildren<AttackData.Folder>().FirstOrDefault(x => { return x.Context_Value == "content"; });
            }
            if (PageObject != null && LessonObject == null)
            {
                if (CourseObject != null)
                {
                    LessonObject = PageObject.GetParentUntil<AttackData.Folder>(10, new Func<AttackData.Folder, bool>(item => CourseObject.GetChildren<AttackData.Folder>().FirstOrDefault(x => { return x.ObjectID == item.ObjectID; }) != null));
                }
                if (LessonObject == null) LessonObject = PageObject.GetParents<AttackData.Folder>().FirstOrDefault();
            }
            if (CourseObject == null && LessonObject != null)
            {
                if (LessonObject.Context_Value == "course") CourseObject = LessonObject;
                else CourseObject = LessonObject.GetParentUntil<AttackData.Folder>(10, new Func<AttackData.Folder, bool>(item => item.Context_Value == "course"));
            }

            this.Language = convert.cStr(CourseObject.SafeGet().AuxField2, "en");
            if (ActiveUser != null)
            {
                this.SiteId = ActiveUser.DistributorId.Str();
                if (ActiveUser.Organization != null)
                    this.CompanyId = ActiveUser.Organization.MasterCompanyID.Str();
            }            
        }
        private AttackData.Folder CourseObject { get; set; }
        private AttackData.Folder LessonObject { get; set; }
        private AttackData.Folder PageObject { get; set; }
        private bool Preview { get; set; }
        private string Language { get; set; }
        private string SiteId { get; set; }
        private string CompanyId { get; set; }
        public object GetLesson()
        {
            var objectWithLessons = (CourseObject != null ? CourseObject : LessonObject);
            if (objectWithLessons != null)
            {
                var folders = BuildFolders(objectWithLessons, this.IncludeAllLessons);
                var course = new
				{
					Name = objectWithLessons.Name,
					Folders = folders,
					CourseCode = objectWithLessons.AuxField1,
					LanguageCode = objectWithLessons.AuxField2,
					ObjectID = objectWithLessons.ObjectID,
					Settings = MinifyJSON(objectWithLessons.getSetting("PlayerSettings")),
					UnlockTestScore =objectWithLessons.getSetting("Unlock Test"),
					Weight = (objectWithLessons.getSetting("Weight").IsNullOrEmpty() ? 1 : objectWithLessons.getSetting("Weight").Int())
				};
                
                return course;     
            }
            return null;
        }
        public object GetPage()
        {            
            if (PageObject != null)                
            {
                var parent = (LessonObject == null ? CourseObject : LessonObject);
                return BuildContentObject(parent, PageObject,true);                
            }
            return null;
        }
        private List<object> BuildFolders(AttackData.Folder folder, bool outputContent)
        {
			var folders = new List<object>();
            var subFolders = new List<object>();

            var children = folder.GetChildren<AttackData.Folder>();
            if (folder.AuxField1 == "finaltest" && children.Count > 0 && !IsPrinting)
            {
                folders.Add(BuildContentObject(folder, children[0], outputContent));
                //Add all non quiz-types
                children.ForEach(child => {
                    if (child.AuxField1 != "quiz")
                    {
                        folders.Add(BuildContentObject(folder, child, true));
                    }
                });
            }
            else
            {
                for (int x = 0; x < children.Count; x++)
                {
                    if (children[x].Context_Value == "content")
                    {
                        folders.Add(BuildContentObject(folder, children[x],
                            (!Preview && (children[x].AuxField1 == "global"))
                            || (PageObject != null && PageObject.ObjectID == children[x].ObjectID) ? true : outputContent));
                    }
                    else
                    {
                        subFolders = BuildFolders(children[x], outputContent == true || (LessonObject != null && LessonObject.ObjectID == children[x].ObjectID) == true);
                        //FolderCount = (subFolders.Count > 0 ? subFolders.Count : children[x].GetChildren<AttackData.Folder>().Count()), 
                        folders.Add(new { Name = children[x].Name, ObjectID = children[x].ObjectID, AuxField1 = children[x].AuxField1, Folders = subFolders, Settings = MinifyJSON(children[x].getSetting("PlayerSettings")), Weight = (children[x].getSetting("Weight").IsNullOrEmpty() ? 1 : children[x].getSetting("Weight").Int()) });
                    }
                }
            }

			//Lookup course dictionary
			if(folder.Context_Value == "course")
			{
				var dictionary = this.GetCourseDictionary();
				if (dictionary != null)
					folders.Add(dictionary);
			}
            return folders;
        }
        private object BuildContentObject(AttackData.Folder parentFolder, AttackData.Folder page, bool outputContent)
        {
            List<object> content = BuildContent(parentFolder, page, outputContent);
            return new
			{
				Name = page.Name,
				ObjectID = page.ObjectID,
				AuxField1 = page.AuxField1,
				Content = content,
				Title = convert.cStr(jlib.helpers.translation.translate(this.Language, "", "/inc/master/courseplayer.master", null, page.AuxField1 + "-heading", this.SiteId, this.CompanyId), page.Name),
				Settings = MinifyJSON(page.getSetting("PlayerSettings"))
			};
        }
        private List<object> BuildContent(AttackData.Folder parentFolder, AttackData.Folder page, bool outputContent)
        {
            List<object> contentObjects = new List<object>();
            if (outputContent)
            {
                List<AttackData.Content> contents = null;

                //if (LessonObject != null && LessonObject.AuxField1 == "finaltest")
                if (parentFolder.AuxField1 == "finaltest" && page.AuxField1 == "quiz" && !IsPrinting)
                {
                    IsFinalTest = true;
                    contents = new List<Data.Content>();

                    
                    var cache = Data.UserSettings.GetByField(KeyName: "cache.finaltest", UserID: this.ActiveUser.ID).Where(x => x.SubKey.StartsWith(parentFolder.ObjectID + ":")).Select();
                    foreach (var item in cache)
                    {
						//Use Filename property for ObjectID. This is so that when we load cached Xmls, the player is able to map content to answers
						var contentLink = Data.RowLinks.GetByField(ID1: item.SubKey.SafeSplit(":", 1).Int(), Table1: "d_folders", Table2: "d_content").Where(x => x.DeleteDate > item.PostedDate).Select().FirstOrDefault();
						contents.Add(new Data.Content
						{
							Name = "Finaltest",
							ObjectID = (contentLink == null ? item.SubKey.SafeSplit(":", 1).Int() : contentLink.ID2),
							Xml = item.Value,
							Type = (int)AttackData.Contents.Types.XML,
							AuxField1 = "quiz",
							Filename = item.SubKey.SafeSplit(":", 1).Str()
						});
                    }

                    if (contents.Count < 3)
                    {
                        var children = parentFolder.GetChildren<AttackData.Folder>().Where(x => x.AuxField1 == "quiz").ToList().Shuffle();
                        for (int x = 0; x < children.Count; x++)
                        {
                            if (contents.Count > 2) break;
                            bool Found = contents.Any(y => y.ObjectID == children[x].ObjectID);
                            if (!Found)
                            {
                                var content = children[x].GetChildren<AttackData.Content>().FirstOrDefault();
                                if (content != null)
                                {
									//ObjectId should be d_content.ObjectID
									//Filename should be d_folder.ObjectID (ie. parent -- the page)
									content.Filename = children[x].ObjectID.Str();

									contents.Add(content);
                                    new Data.UserSetting
                                    {
                                        KeyName = "cache.finaltest",
                                        UserID = this.ActiveUser.ID,
                                        SubKey = parentFolder.ObjectID + ":" + children[x].ObjectID,
                                        Value = content.Xml
                                    }.Save();
                                }
                            }
                        }
                    }
                }
                else
                {
                    contents = page.GetChildren<AttackData.Content>();
                }

                //If no content, add at least one page
                if (contents.FirstOrDefault(x => { return !x.AuxField1.IsNullOrEmpty() && x.Type == (int)AttackData.Contents.Types.HTML; }) == null && (page.AuxField1 == "splash" || page.AuxField1 == "complete"))
                    contents.Add(new AttackData.Content() { Name = page.AuxField1.ToCamelCase(), AuxField1 = page.AuxField1, Type = (int)AttackData.Contents.Types.HTML });

                contents.ForEach(x => { if (x.AuxField1 == "splash" && x.Type == (int)AttackData.Contents.Types.HTML) x.Filename = WriteSplashImage(page); });

                for (int x = 0; x < contents.Count; x++)
                {
                    var content = contents[x];
                    //if ((this.QueryString("ContentID").Int() == 0 || this.QueryString("ContentID").Int() == content.ObjectID))
                    //{
                    //if (!Request["preview-data-" + content.ObjectID].IsNullOrEmpty())
                    //    content.Xml = Request["preview-data-" + content.ObjectID];

                    contentObjects.Add(new { Name = content.Name, ObjectID = content.ObjectID, Xml = content.Xml, AuxField1 = (content.Type == (int)AttackData.Contents.Types.FileUrl ? "file" : content.AuxField1), Filename = (content.Filename.Str().EndsWith(".htm") ? "" : content.Filename), Mode = content.getSetting("_Folder:" + page.ObjectID + "|Mode") });
                    //}
                }
                //}
            }
            return contentObjects;
        }
        private string WriteSplashImage(AttackData.Folder page)
        {
            var userId = (ActiveUser == null ? 0 : ActiveUser.ID);
            var cacheKey = "SplashImage:" + userId + ":" + page.ObjectID;
            string SplashImage = System.Runtime.Caching.MemoryCache.Default.Get(cacheKey).Str();
            //string SplashImage = System.Web.HttpContext.Current.Request.Cookies["splash-image." + page.ObjectID].Str();
            if (SplashImage == "")
            {
                int iLesson = -1;
                AttackData.Folder lesson = page.GetParents<AttackData.Folder>().FirstOrDefault();
                if (lesson != null)
                {
                    AttackData.Folder course = lesson.GetParents<AttackData.Folder>().FirstOrDefault();
                    if (course != null)
                    {
                        lesson = course.GetChildren<AttackData.Folder>().Where(x => x.ObjectID == lesson.ObjectID).FirstOrDefault();
                        if (lesson != null) iLesson = course.GetChildren<AttackData.Folder>().IndexOf(lesson) + 1;
                    }
                }
                int orgId=0, masterOrgId = 0;
                if(ActiveUser!= null && ActiveUser.Organization != null)
                {
                    orgId = ActiveUser.Organization.ID;
                    masterOrgId = ActiveUser.Organization.MasterCompanyID;
                }
                IEnumerable<KeyValuePair<string, string>> SplashImagesFiltered = Util.MyMetier.GetSplashImages(CourseObject.AuxField1.Str(), masterOrgId, orgId, iLesson, null, new List<string>() { "logo.", "next-lesson." });

                if (SplashImagesFiltered.Count() == 0) SplashImagesFiltered = AttackData.Folder.LoadByPk(69595).GetFiles(false);
                SplashImage = "https://mymetier.net/learningportal/inc/library/exec/thumbnail.aspx?w=1002&h=564&z=1&u=" + System.Web.HttpUtility.UrlEncode(SplashImagesFiltered.Count() > 0 ? SplashImagesFiltered.ToList().Shuffle()[0].Key : System.Web.HttpContext.Current.Request.ApplicationPath + "/player/gfx/splash-image.jpg");
                //System.Web.HttpContext.Current.Response.Cookies.Set(new HttpCookie("splash-image." + page.ObjectID, SplashImage));
                System.Runtime.Caching.MemoryCache.Default.Set(cacheKey, SplashImage, DateTimeOffset.Now.AddHours(2));
            }
            return SplashImage;
        }
        private string MinifyJSON(string JSON)
        {
            return parse.replaceAll(JSON, "\n", "", "\r", "");
        }
        public Models.PrintResponse PrintLesson(Models.PrintRequest PrintRequest)
        {
            log.Info("Print request");
            var response = new Models.PrintResponse();

            EvoPdf.Document Doc = new EvoPdf.Document();
            Doc.LicenseKey = "72FyYHVwYHBgd25wYHNxbnFybnl5eXlgcA==";//"31FCUEVAUEFHQVBGXkBQQ0FeQUJeSUlJSVBA";
            string copyrightMessage = CourseObject.getSetting("Copyright");
            int footerHeight = Util.MyMetier.GetPDFFooterHeight(copyrightMessage);
            List<Util.MyMetier.PrintPage> PrintJobs = new List<Util.MyMetier.PrintPage>();

            
            List<AttackData.Folder> LessonPages = null;
			if (PrintRequest.PageId > 0) LessonPages = AttackData.Folder.LoadByPk(PrintRequest.PageId).GetParents<AttackData.Folder>().FirstOrDefault().GetChildren<AttackData.Folder>();
			else
			{
				LessonPages = AttackData.Folder.LoadByPk(PrintRequest.LessonId).GetChildren<AttackData.Folder>();
				PrintRequest.PageId = LessonPages.Select(p => p.ObjectID).FirstOrDefault();
			}

			PrintRequest.BaseUrl = System.Configuration.ConfigurationManager.AppSettings["print.base.url"];
			PrintRequest.ExcludedPageTypes = new List<string>();
			PrintRequest.Source = "LP";
			if(PrintRequest.PageTypes == null) PrintRequest.PageTypes = new List<string>();

			dynamic settingsDynamic = jlib.functions.json.DynamicJson.Parse(convert.cStr(CourseObject.getSetting("PlayerSettings"), "{}"));
            foreach (var item in convert.SafeGetList(settingsDynamic, "Print", "ExcludePageTypes"))
            {
				PrintRequest.ExcludedPageTypes.Add(convert.cStr(item));
            }

			//var pageTypesToPrint = (PrintRequest.Command == "Checklist" ? new List<string> { "checklist" } : new List<string> { });

			//var request = new Models.PrintRequest
			//{
			//	PageId = Lessons[0].GetChildren<AttackData.Folder>().Where(p => p.Context_Value == "content").Select(p => p.ObjectID).FirstOrDefault().Int(),
			//	LessonId = Lessons[0].ObjectID,
			//	Command = "Page",
			//	Options = new List<string>(),
			//	ScormData = "",
			//	ToEmail = "",
			//	PageTypes = pageTypesToPrint,
			//	FromLesson = Math.Max(0, Request["_From"].Int() - 1),
			//	ToLesson = Math.Max(0, (Request["_To"].Int() == 0 ? 99 : Request["_To"].Int() - 1)),
			//	//Source = "CMS",
			//	//BaseUrl = System.Configuration.ConfigurationManager.AppSettings["print.base.url"]
			//};



			var url = convert.cStr(System.Configuration.ConfigurationManager.AppSettings["print.base.url"], System.Configuration.ConfigurationManager.AppSettings["site.url"]) + "/portal2/#/course/" + CourseObject.ObjectID + "/lesson/" + LessonObject.ObjectID + "/page/" + PrintRequest.PageId + "?PrintRequest=" + Uri.EscapeDataString(Newtonsoft.Json.JsonConvert.SerializeObject(PrintRequest));

            log.Info("Print request URL "+ url);

            var printPage = new Util.MyMetier.PrintPage(url, null, null, footerHeight, HttpVerb: "GET", ContentType: null, AutoTOC: false);
			printPage.work();


            //LessonPages.ForEach(Nav =>
            //         {
            //             if (PrintRequest.Command == "Page") {
            //                 if (Nav.ObjectID != PrintRequest.PageId) return;
            //             } else if (!PrintRequest.FileList.IsNullOrEmpty())
            //             {
            //                 if (!PrintRequest.FileList.Contains(Nav.AuxField1.Str())) return;
            //             }
            //             else if (PrintRequest.Command == "Checklist")
            //             {
            //                 if (Nav.AuxField1 != "checklist") return;
            //             }else
            //             {
            //                 if (excludedPageTypes.Contains(Nav.AuxField1)) return;
            //                 if (Nav.AuxField1 == "splash" || Nav.AuxField1 == "complete" || Nav.AuxField1 == "global" || Nav.AuxField1 == "disclaimer") return;                    
            //             }


            //             string Url = System.Configuration.ConfigurationManager.AppSettings["site.url"] + "/portal2/#/course/" + CourseObject.ObjectID + "/lesson/" + LessonObject.ObjectID + "/page/" + Nav.ObjectID + "?PrintRequest=" + Uri.EscapeDataString(PrintRequest.SerializeToJson());
            //             //        string PostDat = "__SCORM=" + System.Web.HttpUtility.UrlEncode(PrintRequest.ScormData)
            //             //+ "&__OPTIONS=" + System.Web.HttpUtility.UrlEncode(PrintRequest.Options)
            //             //+ "&__COMMAND=" + System.Web.HttpUtility.UrlEncode(PrintRequest.Command);
            //             if (DuplicateURLChecker.Contains(Url))
            //             {
            //                 string Email = "I was about to print this page twice: " + Url + "<br /> Requested URL: " + PrintRequest.SerializeToJson() + "<br />DuplicateURLChecker value: " + DuplicateURLChecker + "<br />Nav count: " + LessonPages.Count;
            //                 LessonPages.ForEach(x => { Email += "<br /> - " + x.Name + " / " + x.ObjectID; });
            //                 Util.Email.sendEmail("noreply@lp.mymetier.net", "jorgen@bids.no", "", "", "Duplicate lesson page print detected", Email, true, null);
            //             }
            //             else
            //             {
            //                 DuplicateURLChecker += ";" + Url;
            //		var printJob = new Util.MyMetier.PrintPage(Url, null, Nav.AuxField1, footerHeight, "GET");
            //		PrintJobs.Add(printJob);
            //		printJob.Thread = new System.Threading.Thread(printJob.work);
            //		printJob.Thread.Start();
            //             }                    
            //       });
            //       int Timeout = 60;
            //       while (Timeout > 0)
            //       {
            //           System.Threading.Thread.Sleep(1000);
            //           Timeout--;
            //           bool Completed = true;
            //           foreach (Util.MyMetier.PrintPage printJob in PrintJobs)
            //           {
            //               if (!printJob.Completed) Completed = false;
            //           }
            //           if (Completed)
            //           {					
            ////string printLog = "==================================================\nPDF Started for user ID: " + (Page as jlib.components.webpage).QueryStringLong("user_id") + " -- " +System.DateTime.Now + "\n" ;
            //for (int x = 0; x < PrintJobs.Count; x++)
            //               {
            //	//printLog+=x + " URL: " + PrintPages[x].Url + "\n";						
            //	if (PrintJobs[x].Document != null && PrintJobs[x].Document.Pages.Count > 0)
            //                   {
            //                       Doc.AppendDocument(PrintJobs[x].Document);
            //                       //  printLog += PrintPages[x].HTML;
            //                   }
            //	else
            //	{
            //		//printLog += "TIMEOUT!!!";
            //		//if (System.Configuration.ConfigurationManager.AppSettings["player2.error.log"].IsNotNullOrEmpty())
            //		//	io.write_file(System.Configuration.ConfigurationManager.AppSettings["player2.error.log"], DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString() + ": PDF Conversion timeout " + PrintJobs[x].Url + "\n\n ---------- \n\n", true);
            //	}

            //	//printLog += "\n\n-------------------------------------------------\n";
            //}
            //               break;
            //           }
            //       }

            //Cancel thread if still running			
            //PrintJobs.ForEach(p => 
            //{
            //	try
            //	{
            //		p.Thread.Abort();
            //	}
            //	catch (Exception) { }
            //});				


            //if (Doc.Pages.Count == 0)
            //         {
            //             string Email = "PDF timeout! __COMMAND: " + PrintRequest.Command + "<Br />__OPTIONS: " + PrintRequest.Command + "<br />";
            //             foreach (Util.MyMetier.PrintPage PrintStatus in PrintJobs)
            //                 Email += (PrintStatus.Document != null ? "Completed" : "NOT COMPLETED") + " -- " + PrintStatus.Url + "<br />";
            //             Util.Email.sendEmail("noreply@lp.mymetier.net", "jorgen@bids.no", "", "", "PDF Conversion timeout!!", Email, true, null);
            //         }

            if ((printPage.Document?.Pages?.Count ?? 0) != 0)
            {
                log.Info("Appending Document ");
                Doc.AppendDocument(printPage.Document);
            } else
                log.Info("NO appending Document ");

            printPage = null;
			GC.Collect();

			Doc.DocumentInformation.Author = "Metier OEC AS";
			Doc.DocumentInformation.CreationDate = DateTime.Now;
			Doc.DocumentInformation.Title = CourseObject.Name + " [" + CourseObject.AuxField1 + "]";
			Util.MyMetier.PDFAddFooter(Doc, false, copyrightMessage, true, true);

            //if (oMaster.ActiveUser != null && oMaster.ActiveUser.DistributorId > 0)
            //{
            //    var settings = Util.Customization.Settings.GetSettings(oMaster.ActiveUser);
            //    if (settings.PDFWatermark.IsNotNullOrEmpty())
            //        Util.MyMetier.AddWatermark(Doc, settings.PDFWatermark);
            //}

            if (PrintRequest.ToEmail.IsNotNullOrEmpty())
            {
                Util.Email.sendEmail("kurssporsmal@metier.no", PrintRequest.ToEmail, "", "", String.Format(jlib.helpers.translation.translate(this.Language, "", "/inc/master/courseplayer.master", null, "email-subject", this.SiteId, this.CompanyId), this.LessonObject.Name, this.CourseObject.Name), jlib.helpers.translation.translate(this.Language, "", "/inc/master/courseplayer.master", null, "email-body",this.SiteId,this.CompanyId), true, new List<KeyValuePair<string, byte[]>>() { new KeyValuePair<string, byte[]>(this.LessonObject.Name + ".pdf", Doc.Save()) });
                response.Message = string.Format(jlib.helpers.translation.translate(this.Language, "", "/inc/master/courseplayer.master", null, "email-sent",this.SiteId, this.CompanyId), PrintRequest.ToEmail);
                response.MessageIcon = "email";
            }
            else
            {
                string sFileName = io.getUniqueFileName(System.Configuration.ConfigurationManager.AppSettings["temp.path"] + "\\" + String.Format("{0:yyyy-MM-dd HH-mm-ss}", DateTime.Now) + "-ELearning.pdf");
                Doc.Save(sFileName);

                log.Info("File request" + sFileName); 
                response.Url = HttpContext.Current.Request.ApplicationPath + "/download.aspx?file=" + parse.replaceAll(sFileName, System.Configuration.ConfigurationManager.AppSettings["temp.path"] + "\\");
                response.Message = jlib.helpers.translation.translate(this.Language, "", "/inc/master/courseplayer.master", null, "download-button", this.SiteId, this.CompanyId);
                log.Info("Print response url "  + response.Url);
                log.Info("Print response message " + response.Message);
            }

            return response;

        }
		//public void PrintCourse
		public object GetCourseDictionary()
		{
			var cacheKey = "dictionary.id-" + this.CourseObject.ObjectID;
			var dictionaryId = System.Runtime.Caching.MemoryCache.Default.Get(cacheKey).Int();			
			if (dictionaryId == 0)
			{
				int orgId = 0, masterOrgId = 0;
				if (ActiveUser != null && ActiveUser.Organization != null)
				{
					orgId = ActiveUser.Organization.ID;
					masterOrgId = ActiveUser.Organization.MasterCompanyID;
				}

				dictionaryId = Phoenix.LearningPortal.Util.MyMetier.GetDictionaryForCourse(CourseObject.AuxField1.Str(), masterOrgId);
				System.Runtime.Caching.MemoryCache.Default.Set(cacheKey, (dictionaryId == 0 ? -1 : dictionaryId), DateTimeOffset.Now.AddHours(2));				
			}

			if (dictionaryId > 0)
			{
				AttackData.Folder Dictionary = AttackData.Folder.LoadByPk(dictionaryId);
				if (Dictionary != null)
				{
					AttackData.Content DictionaryContent = Dictionary.GetChildren<AttackData.Content>().FirstOrDefault();
					if (DictionaryContent != null)
					{
						var xmlEntries = xml.loadXml(DictionaryContent.Xml).SelectNodes("dictionary/entry");
						List<string[]> DictionaryEntries = new List<string[]>();
						for (int x = 0; x < xmlEntries.Count; x++)
							if (!xml.getXmlNodeValue(xmlEntries[x], "key").Trim().IsNullOrEmpty()) DictionaryEntries.Add(new string[] { xml.getXmlNodeValue(xmlEntries[x], "key"), parse.replaceAll(xml.getXmlNodeValue(xmlEntries[x], "value"), "\n", "<br />") });

						return new
						{
							AuxField1 ="dictionary",
							ObjectID = dictionaryId,
							Title = "Dictionary",
							Settings = "",
							Name = "Dictionary",
							Content = new List<object>
							{
								new { Xml = jlib.functions.json.DynamicJson.Serialize(DictionaryEntries) }
							}
						};						
					}
				}
			}
			return null;
		}
        
    }
}