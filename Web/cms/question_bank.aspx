﻿<%@ Page Title="Metier Content Studio" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="true" Inherits="Phoenix.LearningPortal.Cms.QuestionBank" Codebehind="question_bank.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" DisableWrapper="true">
<script src="../inc/library/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript" src="../inc/library/ckeditor/adapters/jquery.js"></script>

<h2>Question Bank</h2>

<table class="form-table">
<tr><td class="first">Name: </td><td><common:textbox width="250px" runat="server" id="tName" /></td></tr>
<tr><td>Language: </td><td><common:dropdown width="250px" runat="server" id="dLanguage"><asp:listitem>English</asp:listitem></common:dropdown></td></tr>
</table>
<br />

<b>Questions</b>
<jgrid:jGridAdo runat="server" id="gSettingsGrid" Pagination="false" DataViewFiltering="false" CaseInsensitiveFilter="true" BindDropdownByID="false" style="width:956px;" SortOrder="asc" JSName="oSettingsGrid" AutoFilter="true" PageSize="50" SortCol="1" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets' style='border-top:0;margin-bottom:0'" DeleteConfirmation="Are you sure you wish to delete this portal?" DisplayFilter="true" DisplayAdd="false">
    <Cols>        
        <jgrid:col runat="server" id="cID" HText="ID" ColType="Hide" PrimaryKey="true" FormatString="#pk_id#|#table_name#|#key_name#" SortField="pk_id, table_name, key_name" FPartialMatch="true" />
        <jgrid:col runat="server" id="cText" HText="Text" ColType="Text" FormatString="key_name" SortField="key_name" FField="key_name" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cSection" HText="Section" ColType="Text" FormatString="key_name" SortField="key_name" FField="key_name" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cType" HText="Type" ColType="Text" FormatString="value" SortField="value" FField="value" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cAlt1" HText="Alt 1" ColType="Text" FormatString="value" SortField="value" FField="value" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cAlt2" HText="Alt 2" ColType="Text" FormatString="value" SortField="value" FField="value" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cAlt3" HText="Alt 3" ColType="Text" FormatString="value" SortField="value" FField="value" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cAlt4" HText="Alt 4" ColType="Text" FormatString="value" SortField="value" FField="value" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cAlt5" HText="Alt 5" ColType="Text" FormatString="value" SortField="value" FField="value" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cAlt6" HText="Alt 6" ColType="Text" FormatString="value" SortField="value" FField="value" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cAlt7" HText="Alt 7" ColType="Text" FormatString="value" SortField="value" FField="value" FPartialMatch="true" FInputAtt="class='text'" />

        <jgrid:col id="cOptions" HAtt="class='device-info'" CAtt="class='functions'" runat="server" HText="" ColType="Options" CText="<a href='javascript:void(0);' onclick='fnJGridRowEdit(this);'><img src='../inc/images/icons/pencil1.png' alt='Edit this setting' title='Edit this setting' /></a>  <a href='javascript:void(0);' onclick='fnJGridRowDel(this);'><img src='../inc/images/icons/cancel.png' alt='Delete this setting'  title='Delete this setting' /></a>" />
</Cols>
</jgrid:jGridAdo>

<script language="javascript">    //CKEDITOR.basePath = '/inc/library/ckeditor/'; $(".content_editor").ckeditor({ skin: 'office2003', toolbar: 'Lexicon' }); $(".content_editor_small").ckeditor({ skin: 'office2003', toolbar: 'Lexicon', height: '100px' });</script>  
</asp:Content>