﻿<%@ Page Title="Metier Content Studio" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Cms.Relationships" Codebehind="relationships.aspx.cs" %>
<%@ Register TagPrefix="CMS" TagName="Tree" Src="~/inc/controls/tree.ascx" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" DisableWrapper="true">

 <style>
    .relatives { list-style-type: none; margin: 10px; width: 60%; border:3px solid silver; }
    .relatives li { margin: 5px; padding: 5px; font-size: 1.2em; height: 1.5em; background-color:#eee; cursor:move; }
    html>body #sortable li { height: 1.5em; line-height: 1.2em; }
    .ui-state-highlight { height: 1.5em; line-height: 1.2em; }
    h1{font-size:20px; line-height:25px;}
    body{margin:20px;}
    </style>
    

<h1>Parents <button>Add new</button></h1>
<CMS:Tree runat="server" id="cTree" ItemTypes="d_folders,d_content" />
<div id="allowed-parent-types"></div>
<ul id="parents" class="relatives"></ul>
<h1>Children <button>Add new</button></h1>
<div id="allowed-children-types"></div>
<ul id="children" class="relatives"></ul>

<script type="text/javascript">   
    
    <common:label runat="server" id="lScript" />
    $(function () {
        
        var __Relationships=$(document.body).data("__relationships");
        var Folder=$(document.body).data("__data").Folder;
        var s="";
        $.each(fnCMSAllowableParents(Folder.Context_Value),function(i,o){s+=(i==0?"":", ") + CMSRules.ContextTypes[this];});
        $("#allowed-parent-types").html("Allowed types: " + s);
        $.each(__Relationships.Parents,function(){
            $("<li>" + fnCMSBuildPath(this.PathID, this.PathContext, this.Path) + "</li>").appendTo($("#parents"));
        });
    
        
        $.each(fnCMSAllowableChildren(Folder.Context_Value),function(i,o){s=(i==0?"":s+", ") + CMSRules.ContextTypes[this];});
        $("#allowed-children-types").html("Allowed types: " + s);

        $(".relatives").sortable({
                placeholder: "ui-state-highlight"
            });
            $(".relatives").disableSelection();
        });
	</script>    
</asp:Content>