﻿<%@ Page Language="C#" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Cms.Browser"  MasterPageFile="~/inc/master/default.master" Codebehind="browser.aspx.cs" %>
<%@ Register TagPrefix="CMS" TagName="Tree" Src="~/inc/controls/tree.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
        <div style="height:500px; overflow:scroll">
     <CMS:Tree runat="server" id="cTree" ItemTypes="d_folders" />
            <common:label runat="server" ID="lTreeOptions"/>
    </div>
    <input type="button" id="accept" value="Accept"/> <input type="button" id="cancel" value="Cancel"/>
    <script language="javascript">
        if (!$(document).data("cancel-function"))
            $(document).data("cancel-function", function () {
                window.close();
            });
        
        if (!$(document).data("accept-function"))
            $(document).data("accept-function", function () {
                window.close();
            });
        
        $("#cancel").click(function () {
            $(document).data("cancel-function")();
        });
        $("#accept").click(function () {
            $(document).data("accept-function")();            
        });

        $("#tree").bind("select_node.jstree", function (a, b) {            
            var AllowedContext = false;
            if (b.rslt.obj && b.rslt.obj.attr("context")) {
                var AllowedContexts = getQuerystring("context","");
                AllowedContext = (AllowedContexts == "" || ("," + AllowedContexts + ",").indexOf("," + b.rslt.obj.attr("context") + ",") > -1);
                $.cookie("browser.aspx-SelectedID", b.rslt.obj.attr("id"));
            }
            $("#accept").prop("disabled", !AllowedContext);            
        });

    </script>
</asp:Content>