﻿$(document).ready(function () {    
    $(document.body).find("._content").mouseover(function () { $(this).addClass("_hover"); }).mouseout(function () { $(this).removeClass("_hover"); }).each(function () {
        var key="section-visible-" + getQuerystring("PageID");
        var val = ","+$(this).attr("content_id")+",";        
        $("<div class='_cms-controls'><a href='#'><span class='toggle icon" + (cStr($.cookie(key)).indexOf(val) > -1 ? "" : " min") + "' /></a><a href='#'><span class='edit icon' /></a><a href='#'><span class='delete icon' /></a><a class='move' href='#'><span class='move icon' /></a></div>").appendTo(this).find(".toggle").click(function () {
            var b = !$(this).hasClass("min");
            $.cookie(key, (b ? cStr($.cookie(key)) + val : cStr($.cookie(key)).split(val).join("")));
            $(this).toggleClass("min", b).closest("._content").children().filter(function () { return !$(this).hasClass("_cms-controls") && !$(this).hasClass("title-minimized") && !$(this).hasClass("print-details") && !$(this).hasClass("question-template"); }).toggle(b);
            $(this).closest("._content").find(".title-minimized").toggle(!b);
        }).click().end();
    });
    $(document.body).find(".content-container").sortable({ items: "._content", axis: "y", containment: "parent", cursor: "move", forceHelperSize: true, forcePlaceholderSize: true, scrollSensitivity: 100, scrollSpeed: 80 }).disableSelection();
});