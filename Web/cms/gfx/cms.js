﻿/* CKEditor Start */

function fnBuildQuoteHTML(Quote, Author) {
  return (
    "<div class='intro-quote' contentEditable='false'>" +
    "<div class='image'></div>" +
    "<div class='top'></div>" +
    "<div class='container'><div style='width:370px;'><div class='top'></div><div class='fill'><div class='text' contentEditable='true'>" +
    Quote +
    "</div></div><div class='bottom'></div></div><div class='author' contentEditable='true'>" +
    Author +
    "</div></div></div>"
  );
}

//CKEDITOR.basePath = '../inc/library/ckeditor/';
//CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
CKEDITOR.config.extraPlugins =
  "lineutils,widget,metierbox,metierquestion,metierheading,metierblock";
CKEDITOR.on("dialogDefinition", function (ev) {
  var dialogName = ev.data.name;
  if (dialogName != "image") return;

  var Tab = ev.data.definition.getContents("info");
  Tab.get("basic").children = [
    {
      type: "hbox",
      padding: 1,
      children: [
        {
          id: "txtWidth",
          label: "Width",
          type: "text",
          width: "40px",
          setup: function (type, element) {
            this.setValue(element.getAttribute("normal-width"));
          },
          commit: function (type, element) {
            if (this.getValue() || this.isChanged()) {
              element.setAttribute("normal-width", this.getValue());
              element.setAttribute("width", this.getValue());
              if (!element.getAttribute("normal-height"))
                element.setAttribute("height", "");
            }
          },
        },
        {
          id: "txtHeight",
          label: "Height",
          type: "text",
          width: "40px",
          setup: function (type, element) {
            this.setValue(element.getAttribute("normal-height"));
          },
          commit: function (type, element) {
            if (this.getValue() || this.isChanged()) {
              element.setAttribute("normal-height", this.getValue());
              element.setAttribute("height", this.getValue());
              if (!element.getAttribute("normal-width"))
                element.setAttribute("width", "");
            }
          },
        },
      ],
    },
    {
      type: "hbox",
      padding: 1,
      children: [
        {
          id: "txtWidth1",
          label: "Zoomed<br />Width",
          type: "text",
          width: "40px",
          setup: function (type, element) {
            this.setValue(element.getAttribute("zoom-width"));
          },
          commit: function (type, element) {
            if (this.getValue() || this.isChanged()) {
              element.setAttribute("zoom-width", this.getValue());
            }
          },
        },
        {
          id: "txtHeight1",
          label: "Zoomed<br />Height",
          type: "text",
          width: "40px",
          setup: function (type, element) {
            this.setValue(element.getAttribute("zoom-height"));
          },
          commit: function (type, element) {
            if (this.getValue() || this.isChanged()) {
              element.setAttribute("zoom-height", this.getValue());
            }
          },
        },
      ],
    },
  ];
});

var MetierToolbarCMS = [
  ["Save", "SaveCopy", "Preview"],
  ["Source", "Maximize"],
  //'Cut', 'Copy',
  ["PasteText", "PasteFromWord", "Undo", "Redo"],
  [
    "Bold",
    "Italic",
    "Underline",
    "ShowBlocks",
    "Format",
    "Styles",
    "RemoveFormat",
  ],
  ["Find", "Replace"],
  ["JustifyLeft", "JustifyCenter"],
  ["NumberedList", "BulletedList", "-", "Outdent", "Indent"],
  ["Image", "Table"],
  [
    "Link",
    "Unlink",
    "Anchor",
    "Templates",
    "cms-split",
    "metierbox",
    "metierquestion",
    "metierheading",
    "metierblock",
  ],
];

var MetierToolbarSmall = [
  ["PasteText", "PasteFromWord", "Undo", "Redo"],
  ["Bold", "Italic", "Underline", "ShowBlocks", "Format", "RemoveFormat"],
  ["JustifyLeft", "JustifyCenter"],
  ["NumberedList", "BulletedList", "-", "Outdent", "Indent"],
  ["Link", "Unlink", "Anchor"],
];

CKEDITOR.plugins.registered["preview"] = {
  init: function (editor) {
    var command = editor.addCommand("preview", {
      modes: { wysiwyg: 1, source: 1 },
      exec: function (editor) {
        $(".bPreview").click();
      },
    });
    editor.ui.addButton("Preview", { label: "Preview", command: "preview" });
  },
};
CKEDITOR.plugins.registered["save"] = {
  init: function (editor) {
    var command = editor.addCommand("save", {
      modes: { wysiwyg: 1, source: 1 },
      exec: function (editor) {
        Data._Saving = true;
        $(".bSave").click();
        setTimeout(function () {
          Data._Saving = false;
        }, 1000);
      },
    });
    editor.ui.addButton("Save", { label: "Save", command: "save" });
    if (typeof gAlsoUsedIn !== "undefined" && gAlsoUsedIn.Data.length > 1) {
      editor.addCommand("savecopy", {
        modes: { wysiwyg: 1, source: 1 },
        exec: function (editor) {
          Data._Saving = true;
          $(".bSaveAsCopy").click();
          setTimeout(function () {
            Data._Saving = false;
          }, 1000);
        },
      });
      editor.ui.addButton("SaveCopy", {
        label: "Save as a new page",
        command: "savecopy",
        icon: "../../../Cms/gfx/save-as-copy.png",
      });
    }
    /*
        editor.addCommand('comment', {
            modes: { wysiwyg: 1, source: 1 },
            exec: function (editor) {
                var ID = -(new Date()).getTime();
                var R = editor.getSelection().getRanges()[0];
                var S = R.startContainer.$, E = R.endContainer.$;
                var SO = R.startOffset, EO = R.endOffset;
                var Selection = document.createTextNode(S.nodeValue.substring(SO));
                $(document.createElement("ms")).attr("cid", ID).insertAfter(S).after(Selection);
                S.nodeValue = S.nodeValue.substring(0, SO);
                if (S == E) {
                    EO -= SO;
                    E = Selection;
                }
                $(document.createElement("me")).attr("cid", ID).insertBefore(E).before(document.createTextNode(E.nodeValue.substring(0, EO)));
                E.nodeValue = E.nodeValue.substring(EO);
                var c = fnBuildComment(ID, 0);
                Comments.push(c);
                ResetHighlights();
                fnCMSShowComments(ID);
            }
        }
           );
        editor.ui.addButton('Comment', { label: 'Insert comment', command: 'comment', icon: 'gfx/comment.gif' });
        */
    editor.addCommand("cms-split", {
      modes: { wysiwyg: 1, source: 1 },
      exec: function (editor) {
        var newElement = CKEDITOR.dom.element.createFromHtml(
          '<div class="cms-split"></div>',
          editor.document
        );
        editor.insertElement(newElement);
        //var R = editor.getSelection().getRanges()[0];
        //$(document.createElement("div")).attr("class", "cms-split").insertBefore(R.startContainer.$);
      },
    });
    editor.ui.addButton("cms-split", {
      label: "Split Content",
      command: "cms-split",
      icon: "plugins/metiercut/cut.png",
    });
  },
};

+CKEDITOR.stylesSet.add("MetierStyles", [
  { name: "Heading", element: "h2", styles: {} },
  { name: "Heading 2", element: "h3", styles: {} },
  { name: "Heading 3", element: "h4", styles: {} },
  { name: "Heading 4", element: "h5", styles: {} },
  { name: "Heading, no-nav", element: "h2", attributes: { class: "nonav" } },
  { name: "Heading, bold nav", element: "h2", attributes: { class: "l0" } },
  { name: "Heading 2, bold nav", element: "h3", attributes: { class: "l0" } },
  { name: "Heading 3, bold nav", element: "h4", attributes: { class: "l0" } },
  { name: "Heading 4, bold nav", element: "h5", attributes: { class: "l0" } },
  { name: "Red", element: "span", attributes: { class: "red" } },
  { name: "Green", element: "span", attributes: { class: "green" } },
  { name: "Blue", element: "span", attributes: { class: "blue" } },
  { name: "Yellow", element: "span", attributes: { class: "yellow" } },
]);

CKEDITOR.config.stylesSet = "MetierStyles";
CKEDITOR.config.format_tags = "p";

CKEDITOR.config.filebrowserImageBrowseUrl =
  "files.aspx?ObjectID=125&revision=12/31/9999%2011%3A59%3A59%20PM&parent_id=d_folders:125&context=file-folder";
CKEDITOR.config.filebrowserImageUploadUrl =
  "edit.aspx?action=upload&_url=" + escape(window.location.href);
CKEDITOR.config.filebrowserUploadUrl =
  "edit.aspx?action=upload&mode=file&_url=" + escape(window.location.href);
//CKEDITOR.config.filebrowserUploadUrl="files.aspx?action=upload&_url="+escape(window.location.href);
CKEDITOR.config.templates_replaceContent = false;
CKEDITOR.config.contentsCss = ["../player/css/tempstyle.css?9"];

//CKEDITOR.config.autoGrow_onStartup = true;
CKEDITOR.addTemplates("MetierContent", {
  // The name of the subfolder that contains the preview images of the templates.
  imagesPath: CKEDITOR.getUrl(
    CKEDITOR.plugins.getPath("templates") + "templates/images/"
  ),

  // Template definitions.
  templates: [
    {
      title: "Metier Quote",
      /*image: 'template1.gif',*/
      description: "Description of My Template 1.",
      html:
        //'<p class="quote">[Quote goes here...]<br /><span>[Author here...]</span></p>'
        fnBuildQuoteHTML("[Quote goes here...]", "[Author here...]"),
    },

    {
      title: "Metier Question",
      /*image: 'template1.gif',*/
      description: "Description of My Template 1.",
      html:
        "<div class='cms-question' contentEditable='false'>" +
        "<div class='title' contentEditable='true'></div>" +
        "<div class='text' contentEditable='true'>[Enter question here...]</div></div>",
    },
  ],
});
CKEDITOR.config.templates = "MetierContent";
/* CKEditor END */

var CMSRules = {
  ChildTypes: {
    node: "*",
    "file-folder": "file-folder",
    content: "content",
    assessment: "",
    master_course: "",
    lesson: "content|lesson",
    course: "lesson|content",
    exam: "",
    examcontainer: "exam",
    "question-bank": "",
    tag: "tag",
  },
  ContextTypes: {
    node: "Folder",
    "file-folder": "File Folder",
    content: "Page",
    lesson: "Lesson",
    course: "Course",
    exam: "Exam",
    tag: "Tag",
    examcontainer: "Exam Container",
  },
  Languages: [
    ["da", "Danish"],
    ["nl", "Dutch"],
    ["en", "English"],
    ["fr", "French"],
    ["de", "German"],
    ["it", "Italian"],
    ["no", "Norwegian"],
    ["pl", "Polish"],
    ["pt", "Portugese (Brazil)"],
    ["es", "Spanish"],
    ["sv", "Swedish"],
  ],
  CustomSettings: {
    examcontainer: [
      {
        Name: "Multiple-attempts",
        Type: "Dropdown",
        Options: [
          [1, "Yes"],
          [0, "No"],
        ],
      },
    ],
    lesson: [
      { Name: "Weight", Type: "Text" },
      { Name: "PlayerSettings", Type: "Textarea" },
      { Name: "CustomCSS", Type: "Textarea" },
    ],
    course: [
      { Name: "Article", Type: "Text" },
      { Name: "Unlock Test", Type: "Text" },
      { Name: "Gold Score", Type: "Text" },
      { Name: "Silver Score", Type: "Text" },
      { Name: "Revision date to publish", Type: "Date" },
      {
        Name: "Stage",
        Type: "Dropdown",
        Options: [
          [0, "N/A"],
          [98, "Planned"],
          [30, "Course Authoring"],
          [1, "In Review"],
          [11, "Published, in production"],
          [99, "Archived"],
        ],
        Container: 1,
      },
      { Name: "CustomJS", Type: "Textarea", Container: 1 },
      { Name: "CustomCSS", Type: "Textarea", Container: 1 },
      { Name: "PlayerSettings", Type: "Textarea", Container: 1 },
      { Name: "Copyright", Type: "Text", Container: 1 },
    ],
  },
};
$.extend(CMSRules, {
  AuxFieldTypes: {
    course: [
      "Version",
      { Name: "Language", Type: "Dropdown", Options: CMSRules.Languages },
    ],
    content: ["PageType"],
  },
});
function fnCMSBuildPath(sIDs, sContexts, sPath) {
  $.each(sPath.split(">"), function (i, o) {
    if (cStr(sIDs.split(">")[i]) != "")
      sPath =
        (i == 0 ? "" : sPath + " ") +
        "<a href='" +
        sIDs.split(">")[i] +
        "'><img src='gfx/" +
        sContexts.split(">")[i] +
        ".png' /> " +
        this +
        "</a>";
  });
  return sPath;
}

function fnCMSAllowableChildren(Context) {
  var o = cStr(CMSRules.ChildTypes[Context]).split("|");
  if (o[0] == "*") {
    o = [];
    for (var s in CMSRules.ContextTypes) o.push(s);
  }
  return o;
}
function fnCMSAllowableParents(Context) {
  var Arr = [];
  $.each(CMSRules.ChildTypes, function (i, o) {
    if (o == "*" || ("|" + o + "|").indexOf("|" + Context + "|") > -1)
      Arr.push(i);
  });
  return Arr;
}
function fnCMSGetSetting(Obj, Key) {
  if (Obj && Obj.Settings) {
    for (var x = 0; x < Obj.Settings.Records.length; x++)
      if (Obj.Settings.Records[x].KeyName == Key)
        return Obj.Settings.Records[x].Value;
  }
  return "";
}
$(function () {
  var o = $(document.body).data("__data");
  if (o && o.Folder && o.Folder.Parent)
    $("#parent-folder").click(function () {
      window.parent.fnUpdateEditor(
        "d_folders:" + $(document.body).data("__data").Folder.Parent.ObjectID,
        $(document.body).data("__data").Folder.Parent.Context_Value,
        0
      );
    });
  else $("#parent-folder").hide();

  if (
    $(document.body).data("__data") &&
    $(document.body).data("__data").Folder
  ) {
    var Folder = $(document.body).data("__data").Folder;
    $("#title").prepend("<img src='gfx/" + Folder.Context_Value + ".png'/>");

    var CustomSettings = CMSRules.CustomSettings[Folder.Context_Value];
    var fnRenderInput = function (Name, Options, Value) {
      if (Options.Type == "Text" || Options.Type == "Date") {
        var T = $(
          "<input type='text' name='" +
            Name +
            "' value='" +
            Value +
            "' class='ajax-postback' style='width:250px' />"
        );
        if (Options.Type == "Date") T.css("width", "100px").datepicker();
      } else if (Options.Type == "Textarea") {
        var T = $(
          "<textarea name='" +
            Name +
            "' style='width:250px;height:50px' class='ajax-postback'>" +
            Value +
            "</textarea>"
        );
      } else if (Options.Type == "Dropdown") {
        var T = $(
          "<select name='" +
            Name +
            "' class='ajax-postback' style='width:250px' />"
        );
        $.each(Options.Options, function (i, o) {
          if (Object.prototype.toString.call(o) === "[object Array]")
            $(
              "<option value='" +
                o[0] +
                "'" +
                (o[0] == Value ? " selected" : "") +
                ">" +
                o[1] +
                "</option>"
            ).appendTo(T);
          else
            $(
              "<option value='" +
                o +
                "'" +
                (o == Value ? " selected" : "") +
                ">" +
                o +
                "</option>"
            ).appendTo(T);
        });
      }
      return T;
    };
    if (CustomSettings) {
      $.each(CustomSettings, function (i, o) {
        var C = $(
          "<div style='padding-top:10px;clear:left' class='Attribute-" +
            o.Name +
            "'><div style='float:left;width:90px; padding-top:3px'>" +
            o.Name +
            ":</div></div>"
        ).appendTo(
          $(".dynamic-control-container:eq(" + cInt(o.Container) + ")")
        );
        fnRenderInput(
          "Attribute-" + o.Name,
          o,
          fnCMSGetSetting(Folder, o.Name)
        ).appendTo(C);
      });
    }

    $(".dynamic-control-container").each(function () {
      if ($(this).children().length == 0) $(this).hide();
    });
    $.each(fnCMSAllowableChildren(Folder.Context_Value), function (i, o) {
      if (this != "")
        $("#add-new select").append(
          "<option value='" +
            this +
            "'>" +
            CMSRules.ContextTypes[this] +
            "</option>"
        );
    });

    var AuxFields = CMSRules.AuxFieldTypes[Folder.Context_Value];
    $.each([1, 2], function (i, o) {
      $(".tAuxField" + o)
        .toggle(AuxFields && AuxFields[i] != "")
        .find("div")
        .html(
          AuxFields && AuxFields[i] ? AuxFields[i].Name || AuxFields[i] : ""
        );
      if (AuxFields && AuxFields[i] && AuxFields[i].Type) {
        var C = $(".tAuxField" + o).find("input, textarea, select");
        C.replaceWith(fnRenderInput(C.attr("name"), AuxFields[i], C.val()));
      }
    });

    if ($(document.body).data("__data").Folder.Parent != null) {
      var sTemp = $(".ajax-postback.context").val();
      $(".ajax-postback.context").html("");
      $.each(
        fnCMSAllowableChildren(Folder.Parent.Context_Value),
        function (i, o) {
          if (this != "")
            $(".ajax-postback.context").append(
              "<option value='" +
                this +
                "'" +
                (sTemp == this ? " selected" : "") +
                ">" +
                CMSRules.ContextTypes[this] +
                "</option>"
            );
        }
      );
    }

    if ($("#add-new select option").length == 0 || Folder.ObjectID == 0)
      $("#add-new").hide();
    $("#add-new button").click(function () {
      window.parent.fnUpdateEditor(
        "d_folders:0",
        $("#add-new select").val(),
        Folder.ObjectID
      );
    });
    $("input.ajax-postback, select.ajax-postback, textarea.ajax-postback")
      .on("mouseenter focus", function () {
        $(this).addClass("hover");
      })
      .on("mouseleave blur", function () {
        $(this).removeClass("hover");
      })
      .on("change keyup click", function () {
        $(this).css(
          "background-color",
          $(this).val() != $(this).data("_value") ? "#e9dada" : "transparent"
        );
        var bChanged;
        $(
          "input.ajax-postback, select.ajax-postback, textarea.ajax-postback"
        ).each(function () {
          if ($(this).val() != $(this).data("_value")) bChanged = true;
        });
        if (bChanged) {
          if (
            $("#ajax-postback-controls").show().find("img").hide().length == 0
          ) {
            $(".dynamic-control-container:last").after(
              "<div id='ajax-postback-controls' style='clear:both'><button class='save' type='button'>Save</button> <button type='button' class='revert'>Revert</button><img src='../inc/images/loading.gif' style='width:16px;position:relative; top:3px; padding:3px;display:none'/></div>"
            );
          }
        } else {
          $("#ajax-postback-controls").hide();
        }
      })
      .each(function () {
        $(this).data("_value", $(this).val());
      });
    $(document.body).on(
      "click",
      "#ajax-postback-controls .save, a.ajax-postback",
      function () {
        $("#ajax-postback-controls img").show();
        //onsuccess> need to support redirect if page is saved for first Time
        //need to reset data("_value") if success
        //dataType:script,
        var oRequest = $.ajax($("form").attr("action"), {
          data:
            $("form").serialize() +
            "&mode=ajax-postback&ajax-control=" +
            escape("" + $(this).attr("id")),
          cache: false,
          type: "post",
          error: function (oRequest, sStatus, oError) {
            alert("An error occurred attempting your save. Please try again.");
          },
          complete: function () {
            $("#ajax-postback-controls img").hide();
          },
          success: function (oRequest, sStatus, oError) {
            if (oRequest.indexOf("//redirect:") > -1) {
              window.location = trim(
                oRequest.safeSplit("//redirect:", 1).safeSplit("\r", 0)
              );
            } else if (oRequest.indexOf("//ok") > -1) {
              $(".ajax-postback").each(function () {
                $(this).data("_value", $(this).val()).change();
              });
            }
          },
        });
      }
    );
    $(document.body).on(
      "click",
      "#ajax-postback-controls .revert",
      function () {
        $(
          "input.ajax-postback, select.ajax-postback, textarea.ajax-postback"
        ).each(function () {
          $(this).val($(this).data("_value")).change();
        });
      }
    );
    $(".ajax-postback.context, .ilearning-id")
      .change(function () {
        $("#update-ilearning").toggle(
          $(".ajax-postback.context").val() == "course" &&
            $(".ilearning-id").val() != ""
        );
      })
      .change();

    $("#path")
      .html(
        "<b>Path: </b><u>" +
          cStr(Folder.Path)
            .split(">")
            .remove(0, 0)
            .join("</u>&nbsp;&gt;&nbsp;<u>") +
          "</u>"
      )
      .show();
    if (window.parent && window.parent.GetTree) {
      var tree = window.parent.GetTree();

      var arr = cStr(Folder.PathID).split(">");
      $.each(arr, function (i, s) {
        if (trim(this)) tree.ExpandNode("d_folders\\:" + this);
      });
      tree.SelectNode("d_folders\\:" + Folder.ObjectID);
    }
  }
  $("#diff-compare, #diff-merge").click(function () {
    var oRequest = $.ajax($("form").attr("action"), {
      data:
        $("#diff-window input").serialize() +
        "&mode=ajax-postback&ajax-control=" +
        escape("" + $(this).attr("id")),
      cache: false,
      type: "post",
      error: function (oRequest, sStatus, oError) {
        alert("An error occurred attempting your operation. Please try again.");
      },
      success: function (oRequest, sStatus, oError) {},
    });
  });
});

$("document").ready(function () {
  $("#container").append('<div id="push" />');
  $(
    '#main-nav li a[href*="' +
      window.location.href
        .substring(window.location.href.indexOf("/", 8))
        .split("?")[0] +
      '"]'
  )
    .parents("ul")
    .show(); // Show current subnavigation

  //$('#main-nav > li > a
  $('#main-nav > li > a[href="#"]').click(
    // Click!
    function () {
      $(this).parent().siblings().children("a").removeClass("current"); // Remove .current class from all tabs
      $(this).addClass("current"); // Add class .current
      $(this).parent().siblings().children("ul").fadeOut(150); // Hide all subnavigation
      $(this).parent().children("ul").fadeIn(150); // Show current subnavigation
      return false;
    }
  );
  var subtitle = $("#content > h2");
  $("#header").append(subtitle);
  $("#content > h2").remove();
  $(".content-box-header ul li:first-child a").addClass("current"); // Add .current to the first class
  $(".content-box .tab-content").hide(); // Hide all .tab-content divs
  $(".content-box .tab-content:first-child").show(); // Show default tabs

  $("tbody tr").removeClass("alt-row"); // Remove all .alt-row classes
  $("tbody tr:even").addClass("alt-row"); // Add .alt-row to even table rows

  $("ul li a.current").parents("ul:eq(0)").siblings("a").addClass("current");
  $("#main-nav .drop-menu a").each(function () {
    $(this).click(function (e) {
      $(this).parent().find("ul").toggle();
      e.stopPropagation();
    });
  });
  $(document.body).click(function () {
    $("#main-nav .drop-menu ul").hide();
  });
  $("select.language-enumeration").each(function () {
    var t = this;
    $.each(CMSRules.Languages, function () {
      $(t).append('<option value="' + this[0] + '">' + this[1] + "</option>");
    });
  });
});

function fnJTreeSerialize(o) {
  var oObj = $(o).jstree("");
  if (oObj.data.core.refreshing) return;
  oObj.save_opened();
  oObj.save_selected();
  ($("#" + $(o).attr("id") + "_open").length == 0
    ? $(
        "<input type='hidden' id='" +
          $(o).attr("id") +
          "_open' name='" +
          $(o).attr("id") +
          "_open' />"
      ).appendTo("form:eq(0)")
    : $("#" + $(o).attr("id") + "_open")
  ).val(oObj.data.core.to_open.join(","));
}

function fnContextMenu(sFunction, oObj, oNode, sID, sContext) {
  if (oObj == null) oObj = $("#tree").jstree("");
  if (typeof oNode == "string")
    oNode = $("#" + oNode.replace(/(:|\.)/g, "\\$1"));
  if (oNode == null) {
    var s = sID.split(":").remove(2, 2).join(":");
    alert(s);
    oNode = $("#" + s.replace(/(:|\.)/g, "\\$1"));
  }
  if (!sContext) sContext = $(oNode).attr("context");
  if (!sID) sID = $(oNode).attr("id");

  if (sFunction.indexOf("diff-") > -1) {
    var s =
      "<img src='" +
      oNode
        .find("a ins")
        .css("background-image")
        .safeSplit("(", 1)
        .safeSplit(")", 0)
        .split('"')
        .join("") +
      "'>&nbsp;";
    $(oObj.get_path(oNode)).each(function (i, o) {
      s += (i == 0 ? "" : "&gt;") + "<u>" + this + "</u>";
    });
    $("#diff-window")
      .show()
      .find("[name='" + sFunction + "']")
      .val(sID)
      .end()
      .find("#c-" + sFunction)
      .html(s);
  } else if (sFunction == "edit") {
    //if (sContext == "content")
    fnUpdateEditor(sID, sContext);
  } else if (sFunction == "copy") {
    if (",lesson,course,content,".indexOf("," + sContext + ",") == -1) {
      alert("Copying of '" + sContext + "' is currently not supported");
      return;
    }
    oObj.copy(oNode);
  } else if (sFunction == "cut") {
    oObj.cut(oNode);
  } else if (sFunction.indexOf("paste") == 0) {
    if (!oObj.data.crrm.cp_nodes || oObj.data.crrm.cp_nodes.length == 0) {
      alert("Nothing to past");
    } else {
      var Source = $(oObj.data.crrm.cp_nodes[0]);
      var ParentTypes = fnCMSAllowableParents(Source.attr("context"));
      if (
        $.inArray(sContext, ParentTypes) == -1 &&
        sFunction.indexOf("overwrite") == -1
      ) {
        alert(
          "A node of type '" +
            Source.attr("context") +
            "' can't have a parent of type '" +
            sContext +
            "'"
        );
      } else {
        var _tree = oObj;
        //alert("Dest: " + sID + " (" + sContext + ")" + "\nSource: " + Source.attr("id") + " (" + Source.attr("context") + ")");
        var oRequest = $.ajax($("form").attr("action").split("#")[0], {
          data:
            "action=" +
            sFunction +
            "&source=" +
            escape(Source.attr("id")) +
            "&dest=" +
            escape(sID),
          cache: false,
          type: "get",
          error: function (oRequest, sStatus, oError) {
            alert(
              "An error occurred while pasting your data. Please try again."
            );
          },
          complete: function () {},
          success: function (oRequest, sStatus, oError) {
            if (oRequest.indexOf("//ok") > -1) {
              _tree.refresh(oNode);
            }
          },
        });
      }
    }
  } else if (sFunction == "remove") {
    oObj.remove(oNode);
  } else if (sFunction == "remove-by-id") {
    //non-tree delete
    if (window.confirm("Are you sure you wish to delete this item?")) {
      var oRequest = $.ajax("index.aspx", {
        data: "action=delete&node=" + escape(sID),
        async: false,
        cache: false,
        type: "get",
        error: function (oRequest, sStatus, oError) {
          alert("An error occurred while pasting your data. Please try again.");
        },
        complete: function () {},
        success: function (oRequest, sStatus, oError) {},
      });
      if (oRequest && oRequest.responseText.indexOf("//ok") > -1) return true;
    }
  } else if (sFunction == "rename") {
    oObj.rename(oNode);
  } else if (sFunction == "refresh") {
    oObj.refresh(oNode);
  } else if (sFunction == "export-course" || sFunction == "export-json") {
    window.open(
      "edit.aspx?action=" + sFunction + "&ObjectID=" + sID.split(":")[1]
    );
  } else if (sFunction.indexOf("GET:") == 0) {
    var oRequest = $.ajax($("form").attr("action").split("#")[0], {
      data: "action=" + sFunction.substring(4) + "&Object=" + sID,
      cache: false,
      type: "get",
      error: function (oRequest, sStatus, oError) {
        alert(
          "An error occurred while executing this function. Please try again.\n\n" +
            oRequest.responseText
        );
      },
      complete: function () {},
      success: function (oRequest, sStatus, oError) {
        if (oRequest.indexOf("//refresh") > -1)
          setTimeout(function () {
            $("#tree").jstree("refresh", "#" + sID.replace(/(:|\.)/g, "\\$1"));
          }, 100);
      },
    });
  } else if (sFunction == "preview") {
    if (window.localStorage) {
      window.localStorage["CMS:ContentId"] = "";
      window.localStorage["CMS:Content"] = "";
    }
    window.open(
      "../player/player.aspx?preview=true&LessonID=" + sID.split(":")[1]
    );
    //$.post($(this).parents("form:eq(0)").attr("action") || window.location.href, "action=preview&object_id=" + sID, function (data) { });
  } else if (sFunction == "publish") {
    $.post(
      $(this).parents("form:eq(0)").attr("action") || window.location.href,
      "action=publish&object_id=" + sID,
      function (data) {}
    );
  } else if (sFunction.indexOf("popup-") == 0) {
    var d = $("#" + sFunction);
    if (sFunction == "popup-course-upgrade") {
      var code = $.trim(oNode.text().split(" - ")[0]);
      //if (code.split("-").length > 2)
      //{
      //    alert("Customer-specific courses can't be upgraded yet.");
      //    return;
      //}
      var main = code.split("-")[1];
      var number = main.substring(0, 4);
      if (!isNaN(Number(number))) {
        var newCode = code.split(number).join(number.substring(0, 2) + "10");
        d.find("[name='AuxField1']").val(newCode);
      }
    }
    d.find(".please-wait")
      .detach()
      .end()
      .find(".error-message")
      .hide()
      .end()
      .find("input, select, textarea")
      .each(function () {
        if (!d.hasClass("noreset")) $(this).val("").attr("checked", false);
      })
      .end()
      .dialog({
        modal: true,
        buttons: [
          {
            text: "OK",
            click: function () {
              fnJTreePopupSubmit(this, sFunction, sID);
            },
            id: "btnDialogOk",
          },
          {
            text: "Cancel",
            click: function () {
              $(this).dialog("close");
            },
          },
        ],
      })
      .dialog("open")
      .dialog("option", "title", "Action Prompt");
  } else if (sFunction.indexOf("new-") == 0) {
    var Option = sFunction.substring(4);
    if (Option == "file-folder") {
      var s = cStr(
        window.prompt("Enter name of " + Option.split("-").join(" "), "")
      );
      if (s != "")
        $.post(
          $(this).parents("form:eq(0)").attr("action") || window.location.href,
          "action=" +
            escape(sFunction) +
            "&object_id=" +
            sID +
            "&name=" +
            escape(s),
          function (data) {
            if (data != "") eval(data);
            oObj.refresh(oNode);
            oObj.open_node(oNode, false, true);
          }
        );
      return;
    }
    fnUpdateEditor(Option + ":0", sFunction.substring(4), sID);
    //
  }
}

function fnJTreePopupSubmit(Dialog, Function, ID) {
  $("#btnDialogOk").hide();
  var Controls = $(Dialog).find(
    "input[required='true'][type='text'], textarea[required='true'], select[required='true']"
  );
  var Validation = "";
  Controls.each(function () {
    if (trim($(this).val()) == "")
      Validation +=
        "* " + $(this).parents("td:eq(0)").prev().html() + " is required\n";
  });
  if (Validation != "") {
    alert(
      "Validation failed. Please correct the following before submitting again:\n" +
        Validation
    );
    return false;
  }
  $(
    "<div class='please-wait' style='text-align:center;background-color:white;position:absolute;left:0;top:0;right:0;bottom:0'><br /><br /><br/><br /><img src='../inc/images/icons/ajax-loader2_white.gif' /><b> Please wait...</b></div>"
  ).appendTo($(Dialog));
  var oRequest = $.ajax($("form").attr("action"), {
    data:
      $(Dialog).find("input, textarea, select").serialize() +
      "&ObjectID=" +
      ID +
      "&mode=popup-postback&function=" +
      escape(Function),
    cache: false,
    type: "post",
    error: function (oRequest, sStatus, oError) {
      alert("An error occurred attempting your save. Please try again.");
    },
    complete: function () {
      $("#ajax-postback-controls img").hide();
    },
    success: function (oRequest, sStatus, oError) {
      if (oRequest.indexOf("redirect:") == 0) {
        $(Dialog).dialog("close");
        window.location.href = oRequest.split("\n")[0].split("redirect:")[1];
      } else if (oRequest.indexOf("//ok") > -1) {
        $(Dialog).dialog("close");
        setTimeout(function () {
          $("#tree").jstree("refresh");
        }, 100);
      } else {
        $(Dialog).find(".please-wait").detach();
        setTimeout(function () {
          $("#tree").jstree("refresh");
        }, 100);
        eval(oRequest);
      }
    },
  });
  //alert(Function);
}

function fnUpdateEditor(sID, sContext, sParentID) {
  var sURL,
    oldURL = cStr($("#cContentFrame").show()[0].src);
  if (sContext == "question-bank") {
    //$($("#cContentFrame")[0].document).html("Please wait")
    //alert($("#cContentFrame").html());
    sURL =
      "question_bank.aspx?cms=true&id=" +
      sID +
      "&revision=" +
      escape(_RevisionDate) +
      "&parent_id=" +
      cStr(sParentID) +
      "&context=" +
      sContext;
  } else if (sContext == "exam" || sContext == "assessment") {
    //(sContext == "exam" || sContext=="assessment") {
    sURL = "exam.aspx?";
  } else if (sContext == "file-folder") {
    sURL = "files.aspx?";
  } else if (
    sContext == "lesson" ||
    sContext == "course" ||
    sContext == "node" ||
    sContext == "tag" ||
    sContext == "examcontainer"
  ) {
    sURL = "list.aspx?";
  } else {
    sURL = "edit.aspx?";
  }
  if (sURL) {
    if (
      oldURL.indexOf(sURL) > -1 &&
      oldURL.indexOf("ObjectID=" + sID.split(":")[1]) > -1
    )
      return;
    $("#cContentFrame").show()[0].src =
      sURL +
      "cms=true&ObjectID=" +
      sID.split(":")[1] +
      "&revision=" +
      escape(_RevisionDate) +
      "&parent_id=" +
      cStr(sParentID) +
      "&context=" +
      sContext;
  }
}

//-1==Cancel/Close
function fnJTreeDialogButton(d, i) {
  var oMove = $.data(document.body, "__JTREE");
  if (oMove) {
    if (i < 0) {
      $.jstree.rollback(oMove.rlbk);
    } else {
      var oObj = {
        index: oMove.rslt.cp,
        parent: oMove.rslt.np.attr("id"),
        node: oMove.rslt.o.attr("id"),
        reference: oMove.rslt.r.attr("id"),
        position: oMove.rslt.p,
        button: i,
      };
      if (
        i == 3 &&
        fnSplitIndex(oObj.parent, "-", 0) +
          "-" +
          fnSplitIndex(oObj.parent, "-", 1) ==
          fnSplitIndex(oObj.node, "-", 2) +
            "-" +
            fnSplitIndex(oObj.node, "-", 3)
      ) {
        alert(
          "A node can't be linked to the same parent more than once. Try copying instead."
        );
        return;
      }
      var s =
        $(this).parents("form:eq(0)").attr("action") || window.location.href;
      $.post(
        s.split("#")[0] + (s.indexOf("?") == -1 ? "?" : "&") + "action=move",
        JSON.stringify(oObj),
        function (data) {
          if (data != "") {
            alert(data);
            $.jstree.rollback(oMove.rlbk);
          } else {
            if (i == 1) {
              //move
            } else if (i == 2 || i == 3) {
              //copy or link
              //need to refresh target parent on copy/link
              oMove.rt.refresh(oMove.rslt.np);
            }
          }
        }
      );
    }
  }
  if (i != -100) $(d).dialog("close");
}
function fnJTreeDialog(oData, sHTML, oButtons) {
  $.data(document.body, "__JTREE", oData);
  if ($("#dialogWindow").length == 0)
    $("form:eq(0)").append(
      "<div id='dialogWindow' style='display:none'></div>"
    );
  $("#dialogWindow")
    .dialog({
      modal: true,
      width: 400,
      height: 150,
      buttons: oButtons,
      open: function (ev, ui) {},
      close: function (ev, ui) {
        fnJTreeDialogButton(this, -100);
      },
    })
    .dialog("open")
    .dialog("option", "title", "Action Prompt")
    .html(
      "<div style='padding:10px 0 0 10px;color:black'> " + sHTML + "</div>"
    );
}
