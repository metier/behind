﻿<%@ Page Title="Metier Content Studio" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Cms.Exam" Codebehind="exam.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" DisableWrapper="true">
<script src="../inc/library/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript" src="../inc/library/ckeditor/adapters/jquery.js"></script>

<div id="tabs">
<ul>
    <li><a href="#tab-window-1"><span class="editor-type">Exam</span> Editor</a></li>
    <li><a href="#tab-window-2">Releases</a></li>
    <li><a href="#tab-window-3">Version History</a></li>
    <li><a href="#tab-window-4">Also used in</a></li>
</ul>
    
<div id="tab-window-1" class="tab-window">
    <common:label runat="server" ID="lFormError" Visible="false" CssClass="form_error" Tag="div" />
    <div class="section" id="exam">
        <table class="form-table">
            <tr><td class="first"><span class="editor-type">Exam</span> name: </td><td><common:textbox width="250px" runat="server" JDataMember="Name" JFieldDesc="Exam name" JRequired="true" /></td></tr>
            <tr class="dynamic-display prince_2p prince_2f"><td>Alloted time: </td><td><common:textbox width="50px" runat="server" JDataMember="MaxTime" /> (seconds)</td></tr>
            <tr><td>Player type: </td><td><select style="width:250px" onchange="fnLoadExam(cExam, true);" id="dExamType" JRequired="true" JFieldDesc="Player type" JDataMember="PlayerType"><option value="">(Please select)</option><option value="prince_2p">Prince 2 Practitioner</option><option value="prince_2f">Prince 2 Foundation</option><option value="take-home">Take-home</option><option value="assessment">Assessment</option><option value="assessment-dnv">Assessment DNV Kema</option><option value="case-test">Case</option></select></td></tr>
            <tr><td>Features:</td><td>
            <span class="dynamic-display assessment">
            Role 1: <input type="text" JDataMember="Settings.Role_1" /><br />
            Role 2: <input type="text" JDataMember="Settings.Role_2" /><br />
            Role 3: <input type="text" JDataMember="Settings.Role_3" /><br />
            Role 4: <input type="text" JDataMember="Settings.Role_4" /><br />
            Role 5: <input type="text" JDataMember="Settings.Role_5" /><br />
            Role 6: <input type="text" JDataMember="Settings.Role_6" /><br />
            </span>
            <span class="dynamic-display prince_2p prince_2f">
            <input type="checkbox" JDataMember="Pausable" />Exam can be paused
            <br /><input type="checkbox" JDataMember="Resumable" />Exam can be resumed
            <br /><input type="checkbox" JDataMember="ReviewAnswers" />User can review answers
            
                <br /><input type="checkbox" JDataMember="Settings.disable-time-limit" />Disable time limits                
                <br /><input type="checkbox" JDataMember="Settings.sequential" />Sequential playback only (no back and forth)
                </span>
                <span class="dynamic-display prince_2f assessment-dnv">
                <br /><input type="checkbox" JDataMember="Settings.randomize-questions" />Randomize Questions
                </span>                
                <span class="dynamic-display case-test">
                <br /><input type="checkbox" JDataMember="Settings.inline-answer-display" />User can review answers while taking assessment
                </span>
                </td></tr>
            <tr><td>UI:</td>
            <td>                
            <span class="dynamic-display prince_2p prince_2f case-test">
                <input type="checkbox" JDefault="1" JDataMember="Settings.ui-notes" />Display Notes<br />
                <input type="checkbox" JDefault="1" JDataMember="Settings.ui-bookmarks" />Display Bookmarks<br />
                <input type="checkbox" JDefault="1" JDataMember="Settings.ui-text" />Display Texts<br />
            </span>
            </td></tr>

            <tr class="dynamic-display assessment-dnv prince_2f">
                <td>Limit # of MC questions: </td><td><input type="text" JDataMember="Settings.limit-mc-questions" /></td>
            </tr>

            <tr class="dynamic-display assessment-dnv assessment"><td>Logo URL: </td><td><input type="text" JDataMember="Settings.Logo"  style="width:250px"/></td></tr>
            <tr><td>Starting URL: </td><td><common:textbox width="250px" runat="server" id="tStartingUrl" Enabled="false" DataMember="d_exams.starting_url" /> (read-only)</td></tr>
            <tr><td>Language: </td><td><common:dropdown width="250px" runat="server" id="dLanguage" JRequired="true" JFieldDesc="Exam language" JDataMember="Language">
            <asp:listitem value="">(Please select)</asp:listitem>
            <asp:listitem value="no">Norwegian</asp:listitem>
            <asp:listitem value="en">English</asp:listitem>
            <asp:listitem value="da">Danish</asp:listitem>
            <asp:listitem value="sv">Swedish</asp:listitem>
            <asp:listitem value="nl">Dutch</asp:listitem>
            <asp:listitem value="fr">French</asp:listitem>
            <asp:listitem value="de">German</asp:listitem>
            <asp:listitem value="pl">Polish</asp:listitem>
            <asp:listitem value="es">Spanish</asp:listitem>
            <asp:listitem value="pt">Portugese</asp:listitem>
            </common:dropdown></td></tr>
            <tr id="intro-text"><td style="vertical-align:top;">Intro text:</td><td>
                <common:textbox runat="server" TextMode="MultiLine" CssClass="content_editor_small" Height="100px" Width="600px" JDataMember="IntroText" />
            </td></tr>
            <tr class="dynamic-display assessment-dnv"><td style="vertical-align:top;">DNV assessment pass:</td><td>
                <common:textbox runat="server" TextMode="MultiLine" CssClass="content_editor_small" Height="100px" Width="600px" JDataMember="Settings.DNVPass" />
            </td></tr>
            <tr class="dynamic-display assessment-dnv"><td style="vertical-align:top;">DNV assessment fail:</td><td>
                <common:textbox runat="server" TextMode="MultiLine" CssClass="content_editor_small" Height="100px" Width="600px" JDataMember="Settings.DNVFail" />
            </td></tr>
            <tr><td style="vertical-align:top;"><span class="editor-type">Exam</span> text:</td><td>
                <common:textbox runat="server" TextMode="MultiLine" CssClass="content_editor_small" Height="100px" Width="600px" JDataMember="ExamText" />
            </td></tr>
            <tr><td style="vertical-align:top;">In-line CSS:</td><td>
                <common:textbox runat="server" TextMode="MultiLine" Height="100px" Width="600px" JDataMember="Settings.css" />
            </td></tr>
            </table>
        <br />
    </div>
    
    <div class="dynamic-display prince_2p prince_2f assessment case-test assessment-dnv">
    <a href='javascript:void(0);' onclick="showImportDialog();"><img src='../inc/images/icons/page_excel.png' alt='Import from Excel' title='Import from Excel' /></a>    
        <div class="section" id="exam-sections">
            <h3 style="display:inline"><b>Sections</b></h3>
            <ul class="menu"><li class="ui-state-default">Add new</li></ul>
            <br style="clear:both" />    
            <div class="content">
                <table class="form-table">
                    <tr><td class="first">Section name: </td><td><common:textbox width="250px" runat="server" JDataMember="Name" /></td></tr>
                    <tr><td class="first">Short name: </td><td><common:textbox width="250px" runat="server" JDataMember="ShortName" /></td></tr>
                    <!--<tr><td>Section Time: </td><td><common:textbox JDataMember="MaxTime" width="80px" runat="server" />(leave blank for no limit)</td></tr>-->
                    <tr><td style="vertical-align:top;">Section text: </td><td><common:textbox runat="server" TextMode="MultiLine" CssClass="content_editor_small" Height="70px" Width="600px" JDataMember="Text"></common:textbox></td></tr>
                    <!--
                    <tr><td>Section Type: </td><td><common:dropdown width="250px" runat="server"><asp:listitem>Select questions randomly from question bank</asp:listitem></common:dropdown>
                    <br />Question bank: <common:textbox ID="Textbox3" width="80px" runat="server" />
                    <br />Num questions: <common:textbox ID="Textbox2" width="80px" runat="server" />
                    </td></tr>
                    -->
                </table>
                <br /><br />
            </div>
        </div>
        <div class="section" id="section-sections">
            <h3 style="display:inline"><b>Sub-Section</b></h3> 
            <ul class="menu"><li class="ui-state-default">Add new</li></ul>
            <br style="clear:both" />
            <div class="content">
                <table class="form-table">
                <tr><td class="first">Section name: </td><td><common:textbox width="250px" runat="server" JDataMember="Name" /></td></tr>
                <tr><td class="first">Features: </td><td><input type="checkbox" JDataMember="Settings.parent-additional-info" />Display parent's Section Text as Additional Info</td></tr>
                <tr><td style="vertical-align:top;">Section text: </td><td><common:textbox runat="server" TextMode="MultiLine" CssClass="content_editor_small" Height="70px" Width="600px" JDataMember="Text"></common:textbox></td></tr>
                </table>
                <br /><br />
            </div>
        </div>
        
        <div class="section" id="question-sections">
            <h3 style="display:inline"><b><span class="editor-question-type">Question</span>-Section</b></h3> 
            <ul class="menu"><li class="ui-state-default">Add new</li></ul>
            <br style="clear:both" />
            <div class="content">

                <table class="form-table">
                    <tr><td class="first">Question section name: </td><td><common:textbox width="250px" runat="server" JDataMember="Name" JRequired="false" JFieldDesc="Question section name" /></td></tr>
                    <tr id="question-section-text"><td style="vertical-align:top;">Question section text: </td><td><common:textbox runat="server" TextMode="MultiLine" CssClass="content_editor_small" Height="70px" Width="600px" JDataMember="Text"></common:textbox></td></tr>
                </table>
                <b>Questions</b> <a href='javascript:void(0);' onclick="fnPasteQuestion();"><img src='../inc/images/icons/paste_plain.png' alt='Paste question from Clipboard' title='Paste question from Clipboard' /></a> <a href='javascript:void(0);' onclick="showImportDialog();"><img src='../inc/images/icons/page_excel.png' alt='Import from Excel' title='Import from Excel' /></a>
                <jgrid:jGridAdo runat="server"  DisplayFilter="false" DisplayAdd="true" id="gGrid" Pagination="false" DataViewFiltering="false" BindDropdownByID="true" style="width:956px;" SortOrder="asc" JSName="oGrid" AutoFilter="true" PageSize="50" SortCol="1" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets' style='border-top:0;margin-bottom:0'" DeleteConfirmation="Are you sure you wish to delete this question?">
                    <Cols>        
                        <jgrid:col runat="server" id="cID" HText="ID" ColType="Hide" PrimaryKey="true" FormatString="ID" SortField="ID" />
                        <jgrid:col runat="server" id="cText" HText="Text" ColType="Text" SortField="#" FPartialMatch="true" CInputAtt="class='text question_text'" CAtt="class='question-col-1'" />
                        <jgrid:col runat="server" id="cSubText" HText="Text" ColType="Text" SortField="#" CInputAtt="class='text question_subtext'" />
                        <jgrid:col runat="server" id="cReason" HText="Text" ColType="Text" SortField="#" CInputAtt="class='text question_reason'" />
                        <jgrid:col runat="server" id="cHint" HText="Text" ColType="Text" SortField="#" CInputAtt="class='text question_hint'" />
                        <jgrid:col runat="server" id="cForeignKey" HText="Text" ColType="Text" SortField="#" CInputAtt="class='text foreign_key'" />
                        <jgrid:col runat="server" id="cReference" HText="Text" ColType="Text" SortField="#" CInputAtt="class='text reference'" />
                        <jgrid:col runat="server" id="cDomain" HText="Text" ColType="Text" SortField="#" CInputAtt="class='text domain'" />
                        <jgrid:col runat="server" id="cKnowledgeArea" HText="Text" ColType="Text" SortField="#" CInputAtt="class='text knowledge_area'" />
                        <jgrid:col runat="server" id="cType" HText="Type" ColType="JCombo" CInputAtt="watermark='' onclientchange='var o=fnJGridGetData(oCombo.div).Index;if(o.Index>=0)oGrid.Data[o.Index].HTML=&#34;&#34;;oGrid.render();' class='JCombo text' width='250px' listwidth='250px' arrow='true' data='JComboQuestionTypes' multi='false' Required='false' listfilter='false' max=1000" SortField="#" />
                        <jgrid:col runat="server" id="cPoints" HText="Points" ColType="Text" SortField="#" />
                        <jgrid:col runat="server" id="cQuestion" HText="Correct" ColType="Text" SortField="#" />                            
                        <jgrid:col id="cOptions" HAtt="class='device-info'" CAtt="class='functions'" runat="server" HText="" ColType="Options" CText="<a href='javascript:void(0);' onclick='fnJGridRowEdit(this);'><img src='../inc/images/icons/pencil1.png' alt='Edit this question' title='Edit this question' /></a>  <a href='javascript:void(0);' onclick='fnJGridRowDel(this);'><img src='../inc/images/icons/cancel.png' alt='Delete this question'  title='Delete this question' /></a><a href='javascript:void(0);' onclick='var o=fnJGridGetData(this);if(o.Index!=0){var a=o.Grid.Data[o.Index];o.Grid.Data[o.Index]=o.Grid.Data[o.Index-1];o.Grid.Data[o.Index-1]=a;a.HTML=\'\';o.Grid.Data[o.Index].HTML=\'\';o.Grid.raiseEvent(\'GridRowAfterSave\',o);o.Grid.render();}'><img src='../inc/images/icons/bullet_arrow_up.png' alt='Move Up'  title='Move Up' /></a><a href='javascript:void(0);' onclick='var o=fnJGridGetData(this);if(o.Index!=o.Grid.Data.length-1){var a=o.Grid.Data[o.Index];o.Grid.Data[o.Index]=o.Grid.Data[o.Index+1];o.Grid.Data[o.Index+1]=a;a.HTML=\'\';o.Grid.Data[o.Index].HTML=\'\';o.Grid.raiseEvent(\'GridRowAfterSave\',o);o.Grid.render();}'><img src='../inc/images/icons/bullet_arrow_down.png' alt='Move Down'  title='Move Down' /></a><a href='javascript:void(0);' onclick='var o=fnJGridGetData(this);var a=$.extend(true, [], o.Grid.Data[o.Index]);a[0]=\'_\'+(new Date().getTime());a.HTML=\'\';o.Grid.Data.push(a);o.Grid.raiseEvent(\'GridRowAfterSave\',o);o.Grid.render();'><img src='../inc/images/icons/page_white_copy.png' alt='Duplicate' title='Duplicate' /></a><a href='javascript:void(0);' onclick='var o=fnJGridGetData(this);var a=$.extend(true, [], o.Grid.Data[o.Index]);a[0]=\'_\'+(new Date().getTime());a.HTML=\'\';window.clipboardData.setData(\'Text\', JSON.stringify(a));'><img src='../inc/images/icons/page_copy.png' alt='Copy to Clipboard' title='Copy to Clipboard' /></a>" />
                    </Cols>
                </jgrid:jGridAdo>            
            </div>
        </div>  
    </div>          
    
    <common:button.button runat="server" id="bSave" buttonmode="SubmitButton" text="Save" />
</div>
<div id="tab-window-2" class="tab-window">

<jgrid:jGridAdo runat="server" DisplayFilter="true" DisplayAdd="true" id="gOfferings" Pagination="false" DataViewFiltering="false" BindDropdownByID="true" style="width:956px;" SortOrder="desc" JSName="gOfferings" AutoFilter="true" SortCol="0" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets' style='border-top:0;margin-bottom:0'" DeleteConfirmation="Are you sure you wish to delete this release?">
    <Cols>        
        <jgrid:col runat="server" id="cOID" HText="ID" DataMember="#" ColType="NoEdit" PrimaryKey="true" FormatString="ID" SortField="ID" />
        <jgrid:col runat="server" id="cOName" HText="Name" DataMember="Name" ColType="Text" FormatString="name" SortField="name" FField="name" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cOCreateDate" DataMember="CreateDate" HText="Create Date" ColType="Text" FormatString="{0:d} {0:t}" FormatStringArguments="CreateDate" SortField="CreateDate" FField="CreateDate" FPartialMatch="true" CInputAtt="class='text daterangepicker'" />
        <jgrid:col runat="server" id="cOObjectDate" DataMember="ObjectDate" HText="Version Date" ColType="Text" FormatString="VersionDate" SortField="ObjectDate" FField="ObjectDate" FPartialMatch="true" CInputAtt="class='text daterangepicker'" />
        <jgrid:col runat="server" id="cOAllotedTime" DataMember="MaxTime" HText="Alloted Time (seconds)" ColType="Text" FormatString="MaxTime" SortField="MaxTime" FField="MaxTime" FPartialMatch="true" CInputAtt="class='text'" />
        <jgrid:col runat="server" id="cOIlearnOfferingID" DataMember="IlearnOfferingID" HText="Ilearn Offering (optional)" ColType="Text" FormatString="IlearnOfferingID" SortField="IlearnOfferingID" FField="IlearnOfferingID" FPartialMatch="true" CInputAtt="class='text'" />
                                    
        <jgrid:col id="cOOptions" HAtt="class='device-info'" CAtt="class='functions'" runat="server" HText="" ColType="Options" FormatString="<a href='javascript:void(0);' onclick='var o=fnJGridGetData(this);if(o.ID>-1){gOfferings.addRequest(gOfferings.buildRequest(&quot;custom&quot;, &quot;Type&quot;,&quot;export-responses|&quot;+o.ID));}'><img src='../inc/images/icons/page_excel.png' alt='Play Exam' title='Export Answer-attempts to XLS' /></a>  <a href='javascript:void(0);' onclick='window.open(&quot;/learningportal/exam/player.aspx?release_id=#ID#&quot;);'><img src='../inc/images/icons/control_play.png' alt='Play Exam' title='Play Exam' /></a>  <a href='javascript:void(0);' onclick='fnDialogShow(0,false,&quot;Enrollment List&quot;,&quot;This pop-up will show the enrollment list for this release. Here you can add/remove users, track progress etc.&quot;);'><img src='../inc/images/icons/group.png' alt='Enrollment'  title='Enrollment' /></a> <a href='javascript:void(0);' onclick='fnJGridRowEdit(this);'><img src='../inc/images/icons/pencil1.png' alt='Edit this release'  title='Edit this release' /></a> <a href='javascript:void(0);' onclick='fnJGridRowDel(this);'><img src='../inc/images/icons/cancel.png' alt='Archive this release'  title='Archive this release' /></a>" />
    </Cols>
</jgrid:jGridAdo>  
</div>
<div id="tab-window-3" class="tab-window"></div>
<div id="tab-window-4" class="tab-window"></div>
</div>

<style>
	#exam-sections .menu, #section-sections .menu, #question-sections .menu { list-style-type: none; margin: 0; padding: 0;  }
	#exam-sections .menu li, #section-sections .menu li, #question-sections .menu li { width:auto; float:left; margin-right:20px; cursor:pointer; padding:5px; }	
	#exam-sections .menu li.selected, #section-sections .menu li.selected, #question-sections .menu li.selected { background:Orange; color:Green; }	
	.validate-error{
	    border:1px solid red;
	    font-weight:bold;
	}
	</style>
	<script type="text/javascript">

    CKEDITOR.config.filebrowserImageBrowseUrl="../cms/files.aspx?ObjectID=125&revision=12/31/9999%2011%3A59%3A59%20PM&parent_id=d_folders:125&context=file-folder";
CKEDITOR.config.filebrowserImageUploadUrl="../cms/edit.aspx?action=upload&crop=true&_url="+escape(window.location.href);
CKEDITOR.config.filebrowserUploadUrl="../cms/files.aspx?action=upload&crop=true&_url="+escape(window.location.href);

        var JComboQuestionTypes=[["mc_single","Multiple Choice (single correct)"],["mc_multi","Multiple Choice (multiple correct)"],["mc_single_p2","Multiple Choice Practitioner (single correct)"],["tf","True/False"],["text","Free Text"],["blank","Fill in the blank"]];
	    $(function () {
	        $("#exam-sections .menu, #section-sections .menu, #question-sections .menu").sortable({ items: 'li.drag', update: function (event, ui) {
	            var oObject=$.data($(event.target).parents(".section:eq(0)")[0], "object");
                 oObject.Sections= [];
	            $(event.target).find("li.drag").each(function (i, o) {
	                oObject.Sections.push($.data(o, "section"));
	            });
	        }
	        });
	        $("#exam-sections .menu, #section-sections .menu, #question-sections .menu").disableSelection();
            var oList=$("#exam, #exam-sections, #section-sections, #question-sections");
            oList.each(function (i,o){if(i<oList.length-1)$.data(o,"child-section",oList[i+1]);if(i>0)$.data(o,"parent-section",oList[i-1]);});
            fnLoadExam(cExam);
            $(document).submit(function(e){
                if(!fnSaveSection($("#exam")))return false;
                if ($("#cExam_Data").length == 0) $("form").append("<input type='hidden' id='cExam_Data' name='cExam_Data'/>"); $("#cExam_Data").val(JSON.stringify(cExam));
            });
	    });

   $(function () {
       $('#tabs').tabs({       
            load: function (event, ui) {
                $('a', ui.panel).click(function () {
                    $(ui.panel).load(this.href);
                    return false;
                });
                //alert(ui.panel.html());
            },
            select: function (event, ui) {
                var isValid = true; //... // form validation returning true or false
                //alert(ui.index);
                return isValid;
            },
            show: function (event, ui) {
                var oFrame = $("iframe", ui.panel);
                if (cStr(oFrame.attr("src")) == "" && oFrame.attr("load-src") != "") oFrame.attr("src", oFrame.attr("load-src"));
            }            
        });
    });
    <common:label runat="server" id="lScript" />

    function fnReloadMenu(oContainer){
        var oSection=$.data(oContainer[0],"object");
        if(oSection&&oContainer.find(".menu li").each(function(i,o){if($(o).parent().find("li").length>1)$(o).detach();}).end().find(".menu").length>0)$.each(oSection.Sections, function(i,o){$.data($("<li class='ui-state-default drag" + ($.data(oContainer[0],"active")==o? " selected":"") + "' id='" + cInt(o.ObjectID) + "'>"+(o.Name?o.Name:"&nbsp;")+"</li>").insertBefore(oContainer.find(".menu li:last"))[0],"section",o);});
    }
    function fnLoadSection(oContainer, oSection, oActive){        
        if(oActive==undefined)oActive=null;
        oContainer.toggle(oSection!=null || oActive!=null);
        if(oSection=="new")oSection=null;
        
        if(oSection)$.data(oContainer[0],"object",oSection);
        else oSection=$.data(oContainer[0],"object");

        if(oActive=="new"){
            oActive=$.extend(true,{},{x:cEmptySection}).x;
            oSection.Sections.push(oActive);            
        }
        
        $.data(oContainer[0],"active",oActive);
        if(oSection&&oSection.Sections==null)oSection.Sections=[];            
        
        fnReloadMenu(oContainer);
        
        if(oActive){                    
            oContainer.find("input[JDataMember], textarea[JDataMember], select[JDataMember]").each(function(i,o){                                        
                fnSetFieldValue(o,cStr($(o).attr("JDataMember").indexOf("Settings.")==0?oActive.Settings[$(o).attr("JDataMember").substring(9)]: oActive[$(o).attr("JDataMember")]),true);                
            });                    
            oContainer.find(".content").show();        
            //if(oSection.Sections.length>0)fnLoadHTMLSection(oContainer.next("div.section"),oSection.Sections[0]
            
        }else{
            oContainer.find("input[JDataMember], textarea[JDataMember], select[JDataMember]").each(function(i,o){fnSetFieldValue(o,"",true); });                    
            oContainer.find(".content").hide();
            oContainer.nextAll("div.section").hide();
        }
        return {"Section":oSection,"Active":oActive};
    }
    function fnSetFieldValue(o,sValue,bUseDefault){
        if(sValue==""&&bUseDefault)sValue=cStr($(o).attr("JDefault"));
        if(o.type.toLowerCase()=="checkbox") $(o).prop("checked",sValue==true||sValue=="1"||sValue=="true");
        else $(o).val(sValue);
    }
    function fnGetFieldValue(o){        
        if(o.type.toLowerCase()=="checkbox") return ($(o).is(":checked")?"1":"0");
        else return $(o).val();
    }
    function fnSaveSection(oContainer, bNoValidate){
        if(!bNoValidate)$(".validate-error").removeClass("validate-error");        
        var oActive=$.data(oContainer[0],"active");
        var bReturn=true;
        if(oActive!=null){
            if($.data(oContainer[0],"child-section")!=null)bReturn=fnSaveSection($($.data(oContainer[0],"child-section")), bNoValidate);            
            if(!bReturn && !bNoValidate)return false;
            oContainer.find("input[JDataMember], textarea[JDataMember], select[JDataMember]").each(function(i,o){
                var v=fnGetFieldValue(o); 
                if($(o).attr("JDataMember").indexOf("Settings.")==0) oActive.Settings[$(o).attr("JDataMember").substring(9)]=v; 
                else oActive[$(o).attr("JDataMember")]=v; 
                if($(o).attr("JRequired")=="true"&&v==""){bReturn=false; if(!bNoValidate){$(o).addClass("validate-error"); alert($(o).attr("JFieldDesc") +" must be filled out.");return;}} 
            });            
        }
        return bReturn;
    }

    
    function fnLoadExam(oSection, bNoValidate){                        
        if(!fnSaveSection($("#exam"), bNoValidate)&&!bNoValidate)return false;        
        $(".editor-type").html("Exam");
        $(".editor-question-type").html("Question");
        $("#intro-text, #question-section-text").toggle(oSection.PlayerType!="case-test");        
        
        var oResult=fnLoadSection($("#exam"),oSection,oSection);
        if(oSection.Sections==null)oSection.Sections=[];
        //exam-sections
        $(".dynamic-display").hide();
        if(oSection.PlayerType!="")$("."+oSection.PlayerType).show();
        if(oSection.PlayerType=="prince_2p"||oSection.PlayerType=="assessment"||oSection.PlayerType=="assessment-dnv"){
            fnLoadHTMLSection($("#exam-sections"),oSection, oSection.Sections[0]);
            if(oSection.PlayerType=="assessment")$(".editor-type, .editor-question-type").html("Assessment");
            else if(oSection.PlayerType=="assessment-dnv"){
                $(".editor-type").html("DNV Assessment");                
                $("#section-sections").hide(); 
            }
                       
        }else if(oSection.PlayerType=="prince_2f" || oSection.PlayerType=="case-test"){
            $("#exam-sections, #section-sections").hide();            
            for(var x=0;x<oSection.Sections.length;x++)
                if(oSection.Sections[x].Sections&&oSection.Sections[x].Sections.length>0)oSection.Sections.remove(x,x--);                 
            if($.inArray($("#question-sections").data("active"),oSection.Sections)==-1)$("#question-sections").data("active",null);
            fnLoadHTMLSection($("#exam-sections"));            
            fnLoadHTMLSection($("#section-sections"));
            
            fnLoadHTMLSection($("#question-sections"),oSection, oSection.Sections[0]);
        }else if(oSection.PlayerType=="take-home"){

        }
    }
        
    function fnLoadHTMLSection(oHTMLContainer, oSection, oActive){
        if(!fnSaveSection(oHTMLContainer))return false;        
        if(oActive==null&&oSection&&oSection.Sections&&oSection.Sections.length>0)oActive=oSection.Sections[0];
        var bChanged=oActive!=$.data(oHTMLContainer[0],"active");
        var oResult=fnLoadSection(oHTMLContainer,oSection,oActive);
        
        if(!$.data(oHTMLContainer[0],"child-section")){
            if(oResult.Active){
                var oData=[];
                if(oResult.Active.Questions==null)oResult.Active.Questions=[];
                $.each(oResult.Active.Questions,function(i,o){oData.push([o.ObjectID,o.Text,o.SubText,o.Reason,o.Hint, o.ForeignKey,o.Reference,o.Domain,o.KnowledgeArea, o.QuestionType,o.Points,o.Answers]);});
                oGrid.Data=oData;
                oGrid.render();            
            }
        }else if(bChanged){
            fnLoadHTMLSection($($.data(oHTMLContainer[0],"child-section")),oResult.Active);
        }
    }

    oGrid.registerEventListener("GridRowAfterSave", function(oEvent) {   
        var oActive=$.data($("#question-sections")[0],"active");
        if(oActive!=null){
            oActive.Questions=[];
            if(oEvent.Data&&oEvent.Data.ID==c_Add)oGrid.Data.push(oEvent.Data.Values);                       
            $.each(oGrid.Data, function(counter,o){
                var i=0;
                oActive.Questions.push({"ObjectID":o[i++],"Text":o[i++],"SubText":o[i++],"Reason":o[i++],"Hint":o[i++],"ForeignKey":o[i++],"Reference":o[i++],"Domain":o[i++],"KnowledgeArea":o[i++],"QuestionType":o[i++],"Points":o[i++],"Answers":o[i++]});
            });            
        }
        oEvent.Cancel=true;
        oGrid.render();        
    });            
    
    oGrid.registerEventListener("GridRenderStart", function(oEvent) {    
        oGrid.getCol("cPoints").Type=(cExam.PlayerType=="assessment" || cExam.PlayerType=="case-test"?"Hide":"Text");        
        oGrid.getCol("cType").Type=(cExam.PlayerType=="case-test"?"Hide":"JCombo");                
        if($(oGrid.Table).find(".cke_editor").length>0)$(oGrid.Table).find("textarea").ckeditor(function(){ this.destroy();});
        
    });
    oGrid.registerEventListener("GridRenderEnd", function(oEvent) {    
    if(cExam.PlayerType=="case-test"){
        $(oGrid.Table).find("textarea").ckeditor({ skin: 'kama', toolbar: 'Lexicon', height: '100px', width: '800px' });
        //alert($(oGrid.Table).find("table.cke_editor").find("td").length);
    }    
    //.css("background-color","red")
    //if(cExam.PlayerType=="case-test")$(oGrid.Table).find("textarea").ckeditor({ skin: 'kama', toolbar: 'Lexicon', height: '100px', width: '800px' });
    });
    oGrid.registerEventListener("GridAjaxBeforeStart", function(oEvent) {    
        if(oEvent.Data._request.Action=="refresh"){
            if($.data($("#question-sections")[0],"active")) fnLoadHTMLSection($("#question-sections"), null,$.data($("#question-sections")[0],"active"));            
        }else if(oEvent.Data._request.Action=="delete"){
            for( var x=0;x<oGrid.Data.length;x++)
                if(cStr(oGrid.Data[x][oGrid.MetaData.PKCol])==cStr(oEvent.Data._request.pk))oGrid.Data.remove(x,x);    
            oGrid.raiseEvent("GridRowAfterSave", oEvent);
        }else{
            alert(oEvent.Data._request.Action);
        }
        oEvent.Cancel=true;
    });    
    
    
    oGrid.registerEventListener("GridRowCollected", function(oEvent) {    
        //if($(oEvent.Data.Row).find('#cQuestion').length==0){
        if($(oEvent.Data.Row).find('textarea.question_text').length==0){
            if(oGrid.Data[oEvent.Data.Index])oEvent.Data.Values=oGrid.Data[oEvent.Data.Index];
        }else{
            if(oEvent.Data.Index==c_Add||oEvent.Data.Index>-1){
                if(oEvent.Data.Index==c_Add)oEvent.Data.Values[oGrid.getCol("cID").Index]="_" + (new Date().getTime());
                oEvent.Data.Values[oGrid.getCol("cText").Index]=cStr($(oEvent.Data.Row).find('textarea.question_text').val());                        
                oEvent.Data.Values[oGrid.getCol("cSubText").Index]=cStr($(oEvent.Data.Row).find('textarea.question_subtext').val());                
                oEvent.Data.Values[oGrid.getCol("cReason").Index]=cStr($(oEvent.Data.Row).find('textarea.question_reason').val());
                oEvent.Data.Values[oGrid.getCol("cHint").Index]=cStr($(oEvent.Data.Row).find('textarea.question_hint').val());
                oEvent.Data.Values[oGrid.getCol("cForeignKey").Index]=cStr($(oEvent.Data.Row).find('input.foreign_key').val());                  
                oEvent.Data.Values[oGrid.getCol("cReference").Index]=cStr($(oEvent.Data.Row).find('input.reference').val());                  
                oEvent.Data.Values[oGrid.getCol("cDomain").Index]=cStr($(oEvent.Data.Row).find('input.domain').val());                  
                oEvent.Data.Values[oGrid.getCol("cKnowledgeArea").Index]=cStr($(oEvent.Data.Row).find('input.knowledge_area').val());                  
                oEvent.Data.Values[oGrid.getCol("cType").Index]=cStr(oEvent.Data.Values[oGrid.getCol("cType").Index]).split("\n")[0];
                                
                var oOriginalData=(oEvent.Data.Index==c_Add?[]: oGrid.Data[oEvent.Data.Index][oGrid.getCol("cQuestion").Index]);                
                var sType=oEvent.Data.Values[oGrid.getCol("cType").Index];
                if(sType.indexOf("mc_single")==0 || sType.indexOf("mc_multi")==0 || sType.indexOf("tf")==0){
                    var oCell=$(oEvent.Data.Row).find('#cQuestion');
                    var iCounter=0;
                    oOriginalData=$.extend(true,{},{x:oOriginalData}).x;
                    oCell.find(".answer_response").each(function(i,o){if(this.value!=''){if(oOriginalData.length<=iCounter)oOriginalData[iCounter]={"Settings":{}};oOriginalData[iCounter].Text=this.value;oOriginalData[iCounter].IsCorrect=oCell.find(".answer_correct:eq("+iCounter+"):checked").length;oOriginalData[iCounter].Reason=cStr(oCell.find(".answer_reason:eq("+iCounter+")").val());if(!oOriginalData[iCounter].Settings)oOriginalData[iCounter].Settings={};oOriginalData[iCounter].Settings["Role"]=cStr(oCell.find(".answer_role:eq("+iCounter+")").val());iCounter++;}});
                    oOriginalData.length=iCounter;
                }else{
                    
                }
                oEvent.Data.Values[oGrid.getCol("cQuestion").Index]=oOriginalData;
                if(cExam.PlayerType=="case-test"){
                    oEvent.Data.Values[oGrid.getCol("cText").Index]=oEvent.Data.Values[oGrid.getCol("cText").Index].split("\n").join("").split("\r").join("");
                    oEvent.Data.Values[oGrid.getCol("cReason").Index]=oEvent.Data.Values[oGrid.getCol("cReason").Index].split("\n").join("").split("\r").join("");
                    if(cStr(oEvent.Data.Values[oGrid.getCol("cType").Index])=="")oEvent.Data.Values[oGrid.getCol("cType").Index]="text";
                }
            }
            
        }        
    });    
    oGrid.registerEventListener("GridRenderCell", function(oEvent) {
        if(oEvent.Data.ColObject.ID=="cSubText"||oEvent.Data.ColObject.ID=="cReason"||oEvent.Data.ColObject.ID=="cHint"||oEvent.Data.ColObject.ID=="cForeignKey"||oEvent.Data.ColObject.ID=="cReference"||oEvent.Data.ColObject.ID=="cDomain"||oEvent.Data.ColObject.ID=="cKnowledgeArea"){
            oEvent.Data.HTML="";
            oEvent.Data.TagStart="";
            oEvent.Data.TagEnd="";
        }
        if(!oEvent.Data.Editing && oEvent.Data.Index>-1) oEvent.Data.HTML=cStr(oEvent.Data.HTML).split("\n").join("<br />");
        if(oEvent.Data.ColObject.ID=="cText"){
            if(oEvent.Data.Index==c_Add||oEvent.Data.Editing){
                oEvent.Data.HTML="<div style='font-weight:bold;"+ (oEvent.Data.Index==c_Add?"color:white":"")+ "'>Question:<br /><textarea class='question_text'>"+cStr(oGrid.getEditingData(oEvent.Data.ID,oEvent.Data.ColObject.Index))+"</textarea>"
                +(cExam.PlayerType=="assessment"? "<br />Sub-Text:<textarea class='question_subtext'>"+cStr(oGrid.getEditingData(oEvent.Data.ID,"cSubText"))+"</textarea>" : "" ) 
                +(cExam.PlayerType=="assessment"?"": "<br />Reason:<textarea class='question_reason'>"+cStr(oGrid.getEditingData(oEvent.Data.ID,"cReason"))+"</textarea>")
                +(cExam.PlayerType=="assessment"?"": "<br />Hint:<textarea class='question_hint'>"+cStr(oGrid.getEditingData(oEvent.Data.ID,"cHint"))+"</textarea>")
                +(cExam.PlayerType=="assessment"?"": "<br />Foreign key:<input class='foreign_key' value='"+cStr(oGrid.getEditingData(oEvent.Data.ID,"cForeignKey")) + "'/>")
                +(cExam.PlayerType=="assessment"?"": "<br />Reference:<input class='reference' value='"+cStr(oGrid.getEditingData(oEvent.Data.ID,"cReference")) + "'/>")
                +(cExam.PlayerType=="assessment"?"": "<br />Domain:<input class='domain' value='"+cStr(oGrid.getEditingData(oEvent.Data.ID,"cDomain")) + "'/>")
                +(cExam.PlayerType=="assessment"?"": "<br />Knowledge Area:<input class='knowledge_area' value='"+cStr(oGrid.getEditingData(oEvent.Data.ID,"cKnowledgeArea")) + "'/>")
                + "</div>";
            }else if(oEvent.Data.Index>-1){
                oEvent.Data.HTML="<b>Question: </b>"+oEvent.Data.HTML
                +(oGrid.getData(oEvent.Data.Index,"cSubText")==""?"":"<br /><b>Sub-text: </b>"+oGrid.getData(oEvent.Data.Index,"cSubText"))
                +(oGrid.getData(oEvent.Data.Index,"cReason")==""?"":"<br /><b>Reason: </b>"+oGrid.getData(oEvent.Data.Index,"cReason"))
                +(oGrid.getData(oEvent.Data.Index,"cHint")==""?"":"<br /><b>Hint: </b>"+oGrid.getData(oEvent.Data.Index,"cHint"))
                +(oGrid.getData(oEvent.Data.Index,"cForeignKey")==""?"":"<br /><b>Foreign key: </b>"+oGrid.getData(oEvent.Data.Index,"cForeignKey"))
                +(oGrid.getData(oEvent.Data.Index,"cReference")==""?"":"<br /><b>Reference: </b>"+oGrid.getData(oEvent.Data.Index,"cReference"))
                +(oGrid.getData(oEvent.Data.Index,"cDomain")==""?"":"<br /><b>Domain: </b>"+oGrid.getData(oEvent.Data.Index,"cDomain"))
                +(oGrid.getData(oEvent.Data.Index,"cKnowledgeArea")==""?"":"<br /><b>Knowledge area: </b>"+oGrid.getData(oEvent.Data.Index,"cKnowledgeArea"));

            }
        }else if(oEvent.Data.ColObject.ID=="cQuestion"){
            var sHTML="";
            if(oEvent.Data.Index==c_Add||oEvent.Data.Editing){                
                var oRowValues=oGrid.PersistedData.EditingArr[oEvent.Data.ID];
                var sType=oRowValues[oGrid.getCol("cType").Index];
                var oValue=(typeof(oRowValues[oEvent.Data.ColObject.Index])=="string"?[]:$.extend(true,{},{x:oRowValues[oEvent.Data.ColObject.Index]}).x);                
                if(sType.indexOf("mc_single")==0 || sType.indexOf("mc_multi")==0 || sType.indexOf("tf")==0){
                    sHTML="<table id='cQuestion' style='font-weight:bold;"+ (oEvent.Data.Index==c_Add?"color:white":"")+ "'><tr>"+(cExam.PlayerType=="assessment"?"":"<td><b>Correct</b></td>")+"<td><b>Alternative</b></td>"+(cExam.PlayerType=="assessment"?"<td><b>Role(s)</b></td>":"<td><b>Reason</b></td>")+"</tr>";
                    if(sType=="tf"){
                        oValue.length=2;
                        if(oValue[0]==null)oValue[0]={};
                        if(oValue[1]==null)oValue[1]={};
                        oValue[0].Text="True";oValue[1].Text="False";
                    }else{
                        while(oValue.length<10)oValue.push({});
                    }
                    $.each(oValue,function(i,o){sHTML+="<tr>"+(cExam.PlayerType=="assessment"?"":"<td><input " +(o.IsCorrect?"checked='checked' ":"")+ "type='"+(sType.indexOf("mc_multi")==0?"checkbox":"radio")+"' class='answer_correct' name='answer_correct_"+oEvent.Data.ColObject.Index+"' value='"+i+"'>" + (sType.indexOf("mc_")==0?"<br /><img src='../inc/images/icons/remove.gif' onclick='if(window.confirm(&#34;Are you sure you want to remove this answer?&#34;))$(this).parents(&#34;tr:eq(0)&#34;).detach();'/>" : "" )+"</td>")+"<td>" + (sType=="tf"?o.Text+"<input type='hidden' value='"+o.Text + "' class='answer_response'>":"<textarea class='answer_response' name='answer_response_"+oEvent.Data.ColObject.Index+"_"+i + "'>" + cStr(o.Text) + "</textarea>")+"</td>"+(cExam.PlayerType=="assessment"?"<td><input type='text' class='answer_role' value='" + (o.Settings? cStr(o.Settings["Role"]) : "") + "' name='answer_role_" + oEvent.Data.ColObject.Index+"_"+i + "'></td>":"<td><textarea class='answer_reason' name='answer_reason_"+oEvent.Data.ColObject.Index+"_"+i + "'>" + cStr(o.Reason) + "</textarea></td>")+"</tr>";});
                    sHTML+="</table>";
                }else if(sType=="text"){
                    //sHTML=
                }
            }else if(oEvent.Data.Index>-1){
                var oValues=oGrid.Data[oEvent.Data.Index];
                var sType=oValues[oGrid.getCol("cType").Index];                
                if(sType.indexOf("mc_single")==0 || sType.indexOf("mc_multi")==0 || sType.indexOf("tf")==0){
                    $.each(oValues[oEvent.Data.ColObject.Index],function(i,o){sHTML+="<div style='padding-bottom:10px'><span style='font-weight:bold;"+ (o.IsCorrect? "color:green":"") +"'>" + o.Text + (cExam.PlayerType=="assessment"?" <i>(<u>Role: " + (cStr(o.Settings["Role"])==""?"None":o.Settings["Role"]) +  "</u>)</i>":"") +"</span>" + (o.Reason==""?"": "<br /><i>" + o.Reason + "</i>") + "</div>";});                    
                }
            }        
            oEvent.Data.HTML=sHTML;
        }            
    });   
    
    
	    $(document).on("click","#exam-sections .menu li, #section-sections .menu li, #question-sections .menu li", function(){fnLoadHTMLSection($(this).parents(".section:eq(0)"),null,$.data(this,"section")?$.data(this,"section"):"new");})
        .on("contextmenu","#exam-sections .menu li, #section-sections .menu li, #question-sections .menu li",
            function(){
                if($.data(this,"section")&&window.confirm("Are you sure you wish to remove this section?")){
                    var oParent=$(this).parents(".section:eq(0)");
                    var oSection=$.data(oParent[0],"object");var oActive=$.data(this,"section");
                    $.each(oSection.Sections,function(j,p){if(oActive==p){oSection.Sections.remove(j,j);j--;}});
                    if(oActive==$.data(oParent[0],"active"))$.data(oParent[0],"active",null);
                    fnLoadHTMLSection(oParent,oSection,$.data(oParent[0],"active"));
                }
                return false;
            }
        )
        .on("change","input[JDataMember='Name']:not(#exam input)", function(){fnSaveSection($(this).parents(".section:eq(0)"));fnReloadMenu($(this).parents(".section:eq(0)"));});

    function showImportDialog(){                
        var oFrame = $("#hImportPopup");            
        oFrame.dialog({ modal: true, buttons: { "Close": function() { $(this).dialog("close"); } } }).dialog("open").dialog("option", "title", "Import Questions").dialog("option","position",[20, 20]).dialog("option","height",$(window).height()-40).dialog("option","width",$(window).width()-40).dialog("close");            
        if(fnGetIEVersion()>0){
            oFrame.children("div").css("width","100%").css("height","100%").css("overflow","auto");
            oFrame.css("overflow","hidden");
        }
        oFrame.parent().appendTo($("form:first"));            
        oFrame.dialog("open");
        $(".import-reset-link").click();            
    }

    function fnPasteQuestion(){
    
        try{
        var s=window.clipboardData.getData('Text');
        }catch(e){}
        $("#hPasteWindow").dialog({ modal: true, width:600, height:500, buttons: { "Cancel": function() { $(this).dialog("close"); }, "Ok": function() { 
        var a=$(this).find("textarea").val();
        var s=$("#hPasteWindow").find("input:checked").attr("id");
        if(s.indexOf("CopiedQuestion")>-1){        
            alert(a);
            a=$.parseJSON(a);oGrid.Data.push(a);                    
        }else if(s.indexOf("FoundationExam")>-1){
            //we're pasting a Prince2 Foundation m-c Exam
            var s=a.split("\n");
            for(var x=0;x<s.length;x++){
                if(cInt(s[x].split(" ")[0])>0&&s[x].split(" ").length>1 && trim(s[x].split(" ")[1])!=""){
                    var sQuestion=s[x].substring(s[x].indexOf(" ")+1);
                    for(x++;x<s.length;x++){
                        if(trim(sQuestion)!=""){
                            if(s[x].indexOf(")")==1)break;
                            sQuestion+=" " + s[x];
                        }
                    }
                    var b=[];
                    for( var y=x;y<s.length;y++){
                        alert(s[y]);
                        if(s[y].substring(1,2)!=")")break;
                        b.push({"Reason":"","Text":trim(s[y].substring(2))});
                    }
                    a=["_" + (new Date().getTime()),sQuestion, "mc_single",1,b];
                    oGrid.Data.push(a);
                }
            }
        }else if(s.indexOf("PasteExcel")>-1){
            var sCols=[{Name:"Question"},{Name:"Option A"},{Name:"Option B"},{Name:"Option C"},{Name:"Option D"},{Name:"Option E"},{Name:"Option F"},{Name:"Answer"},{Name:"Type"},{Name:"Reason A"},{Name:"Reason B"},{Name:"Reason C"},{Name:"Reason D"},{Name:"Reason E"},{Name:"Reason F"}];        
        }    
        oGrid.raiseEvent('GridRowAfterSave',null);oGrid.render();

         } }}).dialog("open").dialog("option","title","Paste content").find("textarea").val(cStr(s));
                        
    }
</script>
<div id="hPasteWindow" style="display:none">
<div class="page-1">
<h3>Select type of content you're pasting:</h3>
<input type="radio" checked="checked" name="content-type" id="hPasteCopiedQuestion" /><label for="hPasteCopiedQuestion">Copied Question</label> <input type="radio" name="content-type" id="hPasteFoundationExam" /><label for="hPasteFoundationExam">Foundation Exam</label> <input type="radio" name="content-type" id="hPasteExcel" /><label for="hPasteExcel">Excel Sheet</label>
<br />
<textarea style="width:520px;height:320px"></textarea>
</div>
<div class="page-2" style="display:none">
<input type="checkbox" id="hPasteExcelColHeading" /><label for="hPasteExcelColHeading">Contains Column Headings</label><br />
<table id="hPasteExcelData"></table>
<button>Process Data</button>
</div>
</div>
<div style="display:none;" id="hImportPopup">
    <div>
    <common:importer DSN="sql.dsn.cms" HeaderRow="true" AllowDataReplace="false" DisableColumnPreset="true" OnRenderScript="if($('#hJImportCurrentTab').val()=='2'){$('#hImportPopup').dialog('close');oGrid.refresh();};$('.ui-dialog-content, .ui-dialog-content>div').scrollTop(0);" AjaxMode="true" AllowUpload="false" template="question-import" runat="server" id="oImporter" keyname="import.data" keytable="d_users" xmlfile="~/tools/templates.xml" />
    </div>
    <br style="clear:both" />
</div>
<script language="javascript" type="text/javascript">    CKEDITOR.config.toolbarStartupExpanded = false; CKEDITOR.config.uiColor = '#eee'; $(".content_editor").ckeditor({ skin: 'kama', toolbar: 'Lexicon' }); $(".content_editor_small").ckeditor({ skin: 'kama', toolbar: 'Lexicon', height: '100px', width: '700px' });    CKEDITOR.on("instanceReady", function (e) { $("#cke_" + e.editor.name).find("td").css("background-color", "transparent"); });    </script>  
</asp:Content>