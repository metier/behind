﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using jlib.functions;
using jlib.net;
using jlib.db;

namespace Phoenix.LearningPortal.Cms
{

    public partial class QuestionBank : jlib.components.webpage
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["phoenix.admin.url"] + "/content");
            ado_helper oData = new ado_helper("sql.dsn.cms");
            (Page.Master as Inc.Master.Default).DisableWrapper = true;
        }
    }
}