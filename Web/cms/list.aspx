﻿<%@ Page Title="Metier Content Studio" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Cms.List" EnableViewState="false" EnableEventValidation="false" Codebehind="list.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" DisableWrapper="true">

<div style="background-color:#ddd; border:1px solid silver; padding:20px; position:relative;">
    <div style="float:left">
        <div id="title" style="font-weight:bold; font-size:14px;">&nbsp;<common:textbox runat="server" ID="tTitle" CssClass="ajax-postback" width="550px" style="font-weight:bold; font-size:14px;" /></div>
        <div style="padding-top:15px;" class="tAuxField1"><div style="float:left;width:90px; padding-top:3px">Details 1:</div><common:textbox runat="server" ID="tAuxField1" CssClass="ajax-postback" Width="350px" /></div>
        <div style="padding-top:15px;" class="tAuxField2"><div style="float:left;width:90px; padding-top:3px">Details 2:</div><common:textbox runat="server" ID="tAuxField2" CssClass="ajax-postback" Width="350px" /></div>
        <div style="padding:5px 0;"><div style="float:left;width:90px; padding-top:3px">Context:</div><common:dropdown runat="server" ID="dContext" CssClass="ajax-postback context" /></div>
        <!-- <div style="padding:5px 0;"><div style="float:left;width:90px; padding-top:3px">Sort Index:</div><common:textbox runat="server" ID="tSortIndex" CssClass="ajax-postback" /></div> -->        
        <div style="position:absolute;top:5px;right:5px"><a href="javascript:void(0)" id="parent-folder"><img src="../inc/images/icons/folder_up.png" style="position:relative;top:4px" /> Parent Folder</a></div>
        <div class="dynamic-control-container"></div>        
    </div>
    <div class="dynamic-control-container" style="float:left;padding-left:50px"></div>
        <br style="clear:both" />    
<div id="tabs">
<ul>
<li><a href="#tab-window-0">Items</a></li>
<li><a href="#tab-window-1">Settings</a></li>
</ul>
    
  <div id="tab-window-0" class="tab-window">
  <div id="cStatus" style="visibility:hidden;"><img src='https://mymetier.net/learningportal/inc/images/icons/accept.gif'/>&nbsp;Refresh complete</div>
<br style="clear:both" /><br style="clear:both" />

<div id="add-new">Add new: <select></select><button type="button">Go</button></div>


<jgrid:jGridData runat="server" id="gFiles" Pagination="false" DataViewFiltering="false" BindDropdownByID="false" style="width:956px;" SortOrder="asc" JSName="gFiles" AutoFilter="true" SortCol="0" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets' style='border-bottom:1px solid silver;'" DeleteConfirmation="Are you sure you wish to delete this item?" DisplayFilter="true" DisplayAdd="false">
    <Cols>        
        <jgrid:col runat="server" id="cID" HText="ID" ColType="Hide" PrimaryKey="true" FormatString="#ObjectID#" SortField="Sequence, Name" FPartialMatch="true" />
        <jgrid:col runat="server" id="cContext" HText="" ColType="NoEdit" PrimaryKey="false" FormatString="Context_Value" SortField="Context_Value" FField="Context_Value" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cNumLinks" HText="" ColType="Hide" PrimaryKey="false" FormatString="Numlinks" SortField="Numlinks" FField="Numlinks" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cName" HText="Name" ColType="Text" PrimaryKey="false" FormatString="Name" SortField="Name" FField="Name" FPartialMatch="true" FInputAtt="class='text'" />
        
        <jgrid:col runat="server" id="cAuxField1" HText="Details" ColType="Text" PrimaryKey="false" FormatString="AuxField1" SortField="AuxField1" FField="AuxField1" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cCreated" HText="Created Date" ColType="Text" PrimaryKey="false" FormatString="OriginalCreateDate" SortField="OriginalCreateDate" FField="OriginalCreateDate" FPartialMatch="true" FInputAtt="class='text daterangepicker'" />
        <jgrid:col runat="server" id="cUpdated" HText="Updated Date" ColType="Text" PrimaryKey="false" FormatString="ModifiedDate" SortField="ModifiedDate" FField="ModifiedDate" FPartialMatch="true" FInputAtt="class='text daterangepicker'" />
        <jgrid:col runat="server" id="cCreatedBy" HText="Created By" ColType="Text" PrimaryKey="false" FormatString="CreatedByUserName" SortField="CreatedByUserName" FField="CreatedByUserName" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cUpdatedBy" HText="Updated By" ColType="Text" PrimaryKey="false" FormatString="ModifiedByUserName" SortField="ModifiedByUserName" FField="ModifiedByUserName" FPartialMatch="true" FInputAtt="class='text'" />
        
</Cols>
</jgrid:jGridData>
<jgrid:col id="cOptions" HAtt="class='device-info'" CAtt="class='functions'"  HText="" ColType="Options" CText="<a href='javascript:void(0);' onclick='fnJGridRowEdit(this);'><img src='../inc/images/icons/pencil1.png' alt='Edit this setting' title='Edit this setting' /></a>  <a href='javascript:void(0);' onclick='fnJGridRowDel(this);'><img src='../inc/images/icons/cancel.png' alt='Delete this setting'  title='Delete this setting' /></a>" />
</div>

<div id="tab-window-1" class="tab-window">
<jgrid:jGridData runat="server" id="gSettingsGrid" Pagination="false" DataViewFiltering="false" CaseInsensitiveFilter="true" BindDropdownByID="false" style="width:956px;" SortOrder="asc" JSName="oSettingsGrid" AutoFilter="true" PageSize="50" SortCol="1" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets' style='border-top:0;margin-bottom:0'" DeleteConfirmation="Are you sure you wish to delete this portal?" DisplayFilter="true" DisplayAdd="false">
    <Cols>        
        <jgrid:col runat="server" id="cSID" HText="ID" ColType="Hide" PrimaryKey="true" FormatString="#ObjectID#|#TableName#|#KeyName#" SortField="ObjectID, TableName, KeyName" FPartialMatch="true" />
        <jgrid:col runat="server" id="cSKey" HText="Key" ColType="Text" PrimaryKey="false" FormatString="KeyName" SortField="KeyName" FField="KeyName" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cSValue" HText="Value" ColType="Text" PrimaryKey="false" FormatString="Value" SortField="Value" FField="Value" FPartialMatch="true" FInputAtt="class='text'" />

        <jgrid:col id="cSOptions" HAtt="class='device-info'" CAtt="class='functions'" runat="server" HText="" ColType="Options" CText="<a href='javascript:void(0);' onclick='fnJGridRowEdit(this);'><img src='../inc/images/icons/pencil1.png' alt='Edit this setting' title='Edit this setting' /></a>  <a href='javascript:void(0);' onclick='fnJGridRowDel(this);'><img src='../inc/images/icons/cancel.png' alt='Delete this setting'  title='Delete this setting' /></a>" />
</Cols>
</jgrid:jGridData>
</div>


</div>
    </div>
<script language="javascript">
    gFiles.registerEventListener("GridRenderCell", function (oEvent) {
        if (oEvent.Data.Index > -1) {
            if (oEvent.Data.ColObject.ID == "cName") oEvent.Data.HTML = "<a href='javascript:void(0)' title='d_folders:" + gFiles.getData(oEvent.Data.Index,"cID") + " (" + gFiles.getData(oEvent.Data.Index,"cContext") + ")' onclick='window.parent.fnUpdateEditor(\"d_folders:" + gFiles.getData(oEvent.Data.Index,"cID") + "\",\"" + gFiles.getData(oEvent.Data.Index,"cContext") + "\"," + $(document.body).data("__data").Folder.ObjectID + ");'>" + oEvent.Data.HTML + "</a>";
            else if (oEvent.Data.ColObject.ID == "cContext"){
                oEvent.Data.HTML = "<img src='" + (gFiles.getData(oEvent.Data.Index,"cNumLinks") > 1 ? "../inc/library/exec/thumbnail.aspx?u=" + escape("cms/gfx/" + oEvent.Data.HTML + ".png") + "&o=" + escape("cms/gfx/shortcut-overlay.png") : "gfx/" + oEvent.Data.HTML + ".png") + "' alt='" + oEvent.Data.HTML + "' />";                            
            }
        }
    });
    fnGridAttach(gFiles);
    <common:label runat="server" id="lScript" />
    
      $(function () {       
    
     $('#tabs').tabs({
            load: function (event, ui) {
                $('a', ui.panel).click(function () {
                    $(ui.panel).load(this.href);
                    return false;
                });
                //alert(ui.panel.html());
            },
            select: function (event, ui) {
                var isValid = true; //... // form validation returning true or false
                //alert(ui.index);
                return isValid;
            },
            show: function (event, ui) {
                var oFrame = $("iframe", ui.panel);
                if (cStr(oFrame.attr("src")) == "" && oFrame.attr("load-src") != "") oFrame.attr("src", oFrame.attr("load-src"));
            }
        });
    });
    
    //$("add-new-button")
</script>

</asp:Content>