﻿//Need an editor that supports custom tags.
//http://www.voofie.com/content/2/ckeditor-plugin-development/

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Linq;
using System.Collections;
using System.Collections.Specialized;
using jlib.functions;
using jlib.net;
using jlib.db;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Cms
{
    public partial class Index : jlib.components.webpage
    {
        private DateTime m_oRevisionDate = DateTime.MaxValue;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Init(object sender, EventArgs e)
        {

            if (convert.cStr(Request["action"]).StartsWith("new-"))
            {
                AttackData.Folder Folder = new AttackData.Folder();
                Folder.Name = Request["name"];
                Folder.Context_Value = convert.cStr(Request["action"]).Substring(4);
                Folder.addParent(AttackData.BaseTableRecordImpl<AttackData.Context>.instantiateObject(Request["object_id"].SafeSplit(":", 0), Request["object_id"].SafeSplit(":", 1).Int()));
                Folder.Save();
                Response.Clear();
                Response.Write("fnUpdateEditor('" + Folder.TABLE_NAME + ":" + Folder.ObjectID + "','" + Folder.Context_Value + "','" + Request["object_id"] + "');");
                Response.End();
            }
            else if (Request["mode"] == "ajax-postback")
            {
                Response.ContentType = "text/javascript";
                string HTML = @"<html>
    <link type=""text/css"" rel=""stylesheet"" href=""https://mymetier.net/learningportal/player/css/tempstyle.css"" />
    <style>
        ins {background-color: #cfc; text-decoration: none; }
        del { color: #999; background-color:#FEC8C8; } 
        ins img{border:4px solid #cfc;}
        del img{border:4px solid #FEC8C8;}
    </style>
    <body>";
                AttackData.Folder Old1 = null, Old2 = null, New1 = null;
                AttackData.Content COld1 = null, COld2 = null, CNew1 = null;
                List<string> Cleaned = new List<string>();

                if (!Request["diff-old1"].IsNullOrEmpty())
                {
                    AttackData.BaseTableRecordImpl<AttackData.Context> Obj = AttackData.Folder.instantiateObject(Request["diff-old1"]);
                    Old1 = Obj as AttackData.Folder;
                    if (Old1 != null)
                    {
                        COld1 = Old1.GetChildren<AttackData.Content>().SafeGetValue(0);
                    }
                    else
                    {
                        COld1 = Obj as AttackData.Content;
                    }
                }
                if (!Request["diff-old2"].IsNullOrEmpty())
                {
                    AttackData.BaseTableRecordImpl<AttackData.Context> Obj = AttackData.Folder.instantiateObject(Request["diff-old2"]);
                    Old2 = Obj as AttackData.Folder;
                    if (Old2 != null)
                    {
                        COld2 = Old2.GetChildren<AttackData.Content>().SafeGetValue(0);
                    }
                    else
                    {
                        COld2 = Obj as AttackData.Content;
                    }
                }
                if (!Request["diff-new1"].IsNullOrEmpty())
                {
                    AttackData.BaseTableRecordImpl<AttackData.Context> Obj = AttackData.Folder.instantiateObject(Request["diff-new1"]);
                    New1 = Obj as AttackData.Folder;
                    if (New1 != null)
                    {
                        CNew1 = New1.GetChildren<AttackData.Content>().SafeGetValue(0);
                    }
                    else
                    {
                        CNew1 = Obj as AttackData.Content;
                    }
                }

                if (Old1 != null && (Old1.Context_Value == "course" || Old1.Context_Value == "lesson"))
                {
                    if (Old2 == null || Old2.Context_Value != Old1.Context_Value)
                    {
                        HTML += "When comparing a " + Old1.Context_Value + " you can only compare it to an object of the same type";
                    }
                    else
                    {
                        HTML += "<h2 style=\"color:black\">Comparison report: " + Old1.getPath().Path + " vs. " + Old2.getPath().Path + "</h2>";
                        List<AttackData.Folder> Children1 = Old1.GetChildren<AttackData.Folder>();
                        List<AttackData.Folder> Children2 = Old2.GetChildren<AttackData.Folder>();
                        int lastLessonIndex = -1;
                        for (int x = 0; x < Children1.Count; x++)
                        {
                            if (Children1[x].Context_Value == "lesson")
                            {
                                var lesson2 = Children2.SafeGetValue(++lastLessonIndex);
                                //If lesson2.Name is different from Children1[x].Name, see if there is another lesson with a similar name in this course
                                if (lesson2 == null || parse.removeXChars(parse.inner_substring(lesson2.Name, " ", null, null, null).ToLower(), "abcdefghijklmnopqrstuvwxyz") != parse.removeXChars(parse.inner_substring(Children1[x].Name, " ", null, null, null).ToLower(), "abcdefghijklmnopqrstuvwxyz"))
                                {
                                    for (int y = 0; y < Children2.Count; y++)
                                    {
                                        if (parse.removeXChars(parse.inner_substring(Children2[y].Name, " ", null, null, null).ToLower(), "abcdefghijklmnopqrstuvwxyz") == parse.removeXChars(parse.inner_substring(Children1[x].Name, " ", null, null, null).ToLower(), "abcdefghijklmnopqrstuvwxyz"))
                                        {
                                            lesson2 = Children2[y];
                                            lastLessonIndex = y;
                                            break;
                                        }
                                    }
                                }
                                HTML += CompareLessons(x, Children1[x], lesson2);

                            }
                            else if (Children1[x].Context_Value == "content")
                            {
                                HTML += CompareContent(Children1[x], Children2.SafeGetValue(x));
                            }
                        }
                    }
                }
                else
                {
                    HTML += CompareContent(convert.cStr(Old1 == null ? null : Old1.Name, Old2 == null ? null : Old2.Name), COld1 == null ? null : COld1.Xml, COld2 == null ? null : COld2.Xml, CNew1 == null ? null : CNew1.Xml);
                }


                Response.Write("var w=window.open('');w.document.write('" + (HTML + "</body></html>").Replace("\n", "\\n").Replace("\r", "").Replace("'", "&#039;") + "');");
                //Response.Write("alert(\"" + HTML.Replace("\"", "").Replace("'", "").Replace("\n", "").Replace("\r", "") + "\");");//.dialog();");
                //Response.Write("alert(\"test\");");
                Response.End();
                //if (tTitle.Text.Trim() == "") {
                //    Response.Write("alert('You need to give this folder a title');");
                //} else {
                //}
            }
        }
        private string CompareLessons(int x, AttackData.Folder Child1, AttackData.Folder Child2)
        {
            string HTML = "<h2 style=\"color:black\">Lesson #" + (x + 1) + ": " + Child1.Name + " vs. " + (Child2 == null ? "<span style='color:red'>Doesn't exist!</span>" : Child2.Name) + "</h2>";
            List<AttackData.Folder> Children1 = Child1.GetChildren<AttackData.Folder>();
            List<AttackData.Folder> Children2 = (Child2 == null ? new List<AttackData.Folder>() : Child2.GetChildren<AttackData.Folder>());
            for (int y = 0; y < Children1.Count; y++)
            {
                if (Children1[y].Context_Value == "lesson")
                    HTML += CompareLessons(x, Children1[y], Children2.SafeGetValue(y));
                else if (Children1[y].Context_Value == "content")
                    HTML += CompareContent(Children1[y], Children2.SafeGetValue(y));
            }
            return HTML;
        }
        private string CompareContent(AttackData.Folder Old1, AttackData.Folder Old2)
        {
            string PageTitle = convert.cStr(Old1 == null ? "" : Old1.Name, Old2 == null ? "" : Old2.Name);
            string HTML = (PageTitle.IsNullOrEmpty() ? "" : "<h2 style=\"color:black\">Comparing: " + PageTitle + "</h2>");
            if (Old1 == null || Old2 == null)
            {
                return HTML + "<span style='color:red'>" + (Old1 == null ? "Old Master" : "Old Customer Tailored") + " file doesn't exist</span>";
            }
            else if (Old1.Context_Value != Old2.Context_Value)
            {
                return HTML + "<span style='color:red'>Can't compare a " + Old1.Context_Value + " and a " + Old2.Context_Value + "</span>";
            }
            AttackData.Content C1 = Old1.GetChildren<AttackData.Content>().SafeGetValue(0);
            AttackData.Content C2 = Old2.GetChildren<AttackData.Content>().SafeGetValue(0);
            return HTML + CompareContent("", C1 == null ? "" : C1.Xml, C2 == null ? "" : C2.Xml, "");
        }
        private string CompareContent(string sPageTitle, string sOld1, string sOld2, string sNew1)
        {
            string HTML = (sPageTitle.IsNullOrEmpty() ? "" : "<h2 style=\"color:black\">Comparing: " + sPageTitle + "</h2>");
            if (sOld1 == sOld2)
            {
                HTML += "<span style='color:red'>The pages are identical</span>";
                return HTML;
            }
            List<string> Cleaned = new List<string>();
            if (!sOld1.IsNullOrEmpty()) Cleaned.Add(sOld1);
            if (!sOld2.IsNullOrEmpty()) Cleaned.Add(sOld2);
            if (!sNew1.IsNullOrEmpty()) Cleaned.Add(sNew1);

            if (Cleaned.Count < 2)
            {
                HTML += "<span style='color:red'>Missing left or right side, so can't compare</span>";
                return HTML;
            }

            int EscapeChar = 44032;
            Dictionary<string, string> HTMLTags = new Dictionary<string, string>();
            for (int x = 0; x < Cleaned.Count; x++)
            {
                Cleaned[x] = System.Text.RegularExpressions.Regex.Replace(parse.replaceAll(Cleaned[x], "\r", "", "\n", ""), "<(S*?)[^>]*>.*?|<.*?\\/>", delegate (System.Text.RegularExpressions.Match m)
                {
                    if (!HTMLTags.ContainsKey(m.Value))
                    {
                        HTMLTags[m.Value] = ((char)EscapeChar).ToString();
                        EscapeChar++;
                    }
                    return HTMLTags[m.Value];
                });
            }
            Util.Diff.DiffMatchPatch.diff_match_patch Diff = new Util.Diff.DiffMatchPatch.diff_match_patch();
            //Diff.Diff_Timeout = 0;
            List<Util.Diff.DiffMatchPatch.Diff> Changes = Diff.diff_main(Cleaned[0], Cleaned[1]);
            Diff.diff_cleanupSemantic(Changes);
            List<Util.Diff.DiffMatchPatch.Patch> Patches = Diff.patch_make(Changes);
            if (Cleaned.Count > 2)
            {
                Cleaned[2] = Diff.patch_apply(Patches, Cleaned[2])[0].Str();
                foreach (KeyValuePair<string, string> Tag in HTMLTags) Cleaned[2] = Cleaned[2].Replace(Tag.Value, Tag.Key);
            }

            //List<DiffMatchPatch.Diff> HTMLChanges = Changes.FindAll(x => x.operation != DiffMatchPatch.Operation.INSERT || x.text != "\n");
            HTML += Diff.diff_prettyHtml(Changes);
            //string HTML = Diff.diff_text2(Changes);
            foreach (KeyValuePair<string, string> Tag in HTMLTags) HTML = HTML.Replace(Tag.Value, Tag.Key);

            //if (Request["ajax-control"]=="diff-merge"){

            //} else if (Request["ajax-control"] == "diff-compare") {

            //}
            //Response.Write("var d=$('#popup'); if(d.length==0)d=$('<div id=\"popup\" />').appendTo(document.body);d.html('" + HTML.Replace("\n", "").Replace("\r", "").Replace("'","&#039;") + "').dialog().dialog('open').dialog('option', 'title', 'Action Prompt');");
            HTML = @"<html>" + HTML
            + (Cleaned.Count > 2 ? "<hr /><h1>Merged:</h1>" + Cleaned[2] : "")
            + "</html>";

            XmlDocument xmlDoc = jlib.net.sgmlparser.SgmlReader.getXmlDoc(HTML);
            XmlNodeList xmlImages = xmlDoc.SelectNodes("//img");
            for (int x = 0; x < xmlImages.Count; x++)
            {
                string[] Styles = xml.getXmlAttributeValue(xmlImages[x], "style").Split(';');
                for (int y = 0; y < Styles.Length; y++)
                    if (Styles[y].IndexOf("border") > -1) Styles[y] = "";
                xml.setXmlAttributeValue(xmlImages[x], "style", Styles.Join(";"));
            }

            return xmlDoc.SelectSingleNode("html").InnerXml;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (convert.cStr(Request["tree_open"]) != "") ClientScript.RegisterHiddenField("tree_open", Request["tree_open"]);
            lScript.Text = "var _RevisionDate='" + m_oRevisionDate.ToString() + "';";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            log.Info("Logs on index.aspx IN BEHIND");

        }
    }

}