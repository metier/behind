﻿<%@ Page Title="Metier Content Studio" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Cms.Index" Codebehind="index.aspx.cs" %>
<%@ Register TagPrefix="CMS" TagName="Tree" Src="~/inc/controls/tree.ascx" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
<script src="../inc/library/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript" src="../inc/library/ckeditor/adapters/jquery.js"></script>

<h2 class="page-title"><img src="../inc/images/icons/tools_32.png" alt="Backup and Restore Data" />Content Builder</h2>
<div style="position:absolute;width:550px;top:20px;left:300px;background-color:White;border:4px solid silver;padding:7px;line-height:20px;display:none" id="diff-window">
<input type="hidden" name="diff-old1" /><input type="hidden" name="diff-old2" /><input type="hidden" name="diff-new1" />
<h3>Compare</h3>
<table>
<tr>
<td><b>Old Generic: </b></td><td> <span id="c-diff-old1"></span></td>
</tr>
<tr>
<td><b>Old Customer Tailored: </b></td><td><span id="c-diff-old2"></span></td>
</tr>
<tr>
<td><b>New: </b></td><td><span id="c-diff-new1"></span></td>
</tr>
</table>
<a href="javascript:void(0)" id="diff-compare">Compare now</a>
<a href="javascript:void(0)" id="diff-merge">Merge now</a>
</div>
<script type="text/javascript">
    
    $(function () {        
        $(window).resize(function () {
            if ($.data(window, "resize") == null) $.data(window, "resize", [$("#cContentFrame"), $(".course-browser>div")]);
            for (var x = 0; x < $.data(window, "resize").length; x++) $.data(window, "resize")[x].css("height", Math.max(100, $(window).height() - 200) + "px");
        });
        $(window).resize();        
    });

    <common:label runat="server" id="lScript" />
	</script>
    
    <style>
    .draggable-divider{cursor:e-resize;width:5px;} 
    .collapsed{border-left:3px solid silver;}   
    </style>
        <table style="width:100%; height:100%;"><tr><td class="course-browser" style="width:300px"><div>
        <CMS:Tree runat="server" id="cTree" ItemTypes="d_folders" />
        </div>
        </td><td class="draggable-divider" style="vertical-align:middle"><img src="gfx/collapse-h.gif" class="toggle-tree" /></td><td style="vertical-align:top;border:3px solid silver; " id="cContent">
        <iframe id="cContentFrame" style="overflow:scroll" frameborder="0" width="100%" height="100%" src="list.aspx?ObjectID=1&revision=12/31/9999%2011%3A59%3A59%20PM&parent_id=&context=node"></iframe>                  

</td></tr></table>
<script>
    //var s = $(this).siblings("eq:0"); s.width((e.pageX - s.offset().left) + "px");
    
    var _DragControllerImpl = function () {
        var _doc = document;        
        this.mousedown = function (e) { $.data(_doc, "draggable", e.srcElement); };
        this.mouseup = function () { $.data(_doc, "draggable", null); };
        this.mousemove = function (e,iOffset) { if ($.data(_doc, "draggable") && e.which == 0) $.data(_doc, "draggable", null); if ($.data(_doc, "draggable")) { var s = $($.data(_doc, "draggable")).siblings(":eq(0)"); s.width((e.pageX - s.offset().left + cInt(iOffset)) + "px"); } };
    };
    var _DragController = new _DragControllerImpl();
    $(".draggable-divider").mousedown(function (e) { _DragController.mousedown(e); });
    $(document).mouseup(function (e) { _DragController.mouseup(e); }).mousemove(function (e) { _DragController.mousemove(e); });

    $("#cContentFrame").load(function () {
        $($("#cContentFrame")[0].contentDocument).mouseup(function (e) { _DragController.mouseup(e); }).mousemove(function (e) { _DragController.mousemove(e, $(window).width()); });
    });
    var fnToggleTree = function (Initial) { var b = $.cookie("cms-tree-visible") == "0"; if (Initial) b = !b; if (Initial && b) return; $.cookie("cms-tree-visible", (b ? 1 : 0)); $(".course-browser").toggle(b); $(".toggle-tree").attr("src", "gfx/" + (b?"collapse":"expand") + "-h.gif");$(".draggable-divider").toggleClass("collapsed", !b); };
    $(".toggle-tree").click(function () { fnToggleTree(); })
    fnToggleTree(true);    
    
    //$($("#cContentFrame")[0]).mousemove(function () { alert('move'); });
//onmousedown="this.dragging=true" onmouseup="this.dragging=false"; onmousemove="if(this.dragging)$(this).siblings('#course-browser').width('px');"
</script>
</asp:Content>