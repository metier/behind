﻿<%@ Page Title="Metier Content Studio" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Cms.Files" EnableViewState="false" EnableEventValidation="false" Codebehind="files.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" DisableWrapper="true">
<script type="text/javascript" src="../inc/js/uploader/js/vendor/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../inc/js/uploader/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="../inc/js/uploader/js/jquery.fileupload.js"></script>

<style>
    
    
    .toolbar-button img
    {
        position:relative;
        top:3px;
    }
    a.toolbar-button
    {
        margin:0 10px;
        padding:0px 6px 6px 6px;
        border:1px solid silver;
        border-radius:6px;
        background-color:#e0e0e0;
        height:15px;
        display:block;
        float:left;
        text-decoration:none;
    }
    a.toolbar-button.disabled{
        /*color:silver;*/
        background-color:#c0c0c0;
        cursor:default;
        opacity:.50;
filter:alpha(opacity=50);
    }
    a.toolbar-button.hover
    {
        background-color:#eee;
        border:1px solid #555;        
    }
    #drag-and-drop{
        height:15px; width:150px;margin-left:20px; background-color: #9ce4ff; border: 3px dashed #41ccff; text-align:center; vertical-align:middle; display:none;
        border-radius:6px;
    }
    #drag-and-drop.dragover
    {
        background-color:#9cfff2;
    }
    #file-toolbar div
    {
        float:left;
    }
    table
    {
        background-color:#e0e0e0;
    }
    #file-toolbar
    {
        padding:4px 0;
    }
#file-toolbar, #file-filter, #file-list
{        
    border:2px solid #d3d3d3;
    border-radius: 6px;
    margin:5px 10px;
}
#file-filter
{
    background-color:White;    
}
#file-filter div
{
    margin-left:10px;
}
#file-list img
{
    padding-bottom:10px;
}
#file-list .filename
{
    padding-bottom:5px;
}
#file-list .file
{
    text-align:center;
    background-color:White;
    width:100px;
    margin:5px;
    border:1px solid #d3d3d3;
    padding:10px;
    float:left;
    height:136px;
    word-wrap:break-word; 
    cursor:pointer;   
}
#file-list .file.hover
{
    border-color:#99ccff;
    background-color:#dff1ff;   
}
#file-list .file.selected
{
    border-color:#6565fe;
    background-color:#b4d9ff;
}

</style>
<common:hidden runat="server" ID="hFolderOverride" CssClass="hFolderOverride" />

<asp:PlaceHolder runat="server" ID="pFolderHeading">
<div style="background-color:#ddd; border:1px solid silver; padding:20px; position:relative;">
<div id="title" style="font-weight:bold; font-size:14px;">&nbsp;<common:textbox runat="server" ID="tTitle" CssClass="ajax-postback" width="550px" style="font-weight:bold; font-size:14px;" /></div>
<div style="padding-top:15px;"><div style="float:left;width:90px; padding-top:3px">Details:</div><common:textbox runat="server" ID="tAuxField1" CssClass="ajax-postback" Width="350px" /></div>
<div style="padding:5px 0;"><div style="float:left;width:90px; padding-top:3px">Context:</div><common:dropdown runat="server" ID="dContext" CssClass="ajax-postback context" /></div>
    <div class="dynamic-control-container" style="float:left;padding-left:50px"></div>
<div id="add-new" style="padding:15px 0 5px 0;"><div style="float:left;width:90px; padding-top:3px"><b>Add new: </b></div><select></select><button type="button">Go</button></div>

<div style="position:absolute;top:5px;right:5px"><a href="javascript:void(0)" id="parent-folder"><img src="../inc/images/icons/folder_up.png" style="position:relative;top:4px" /> Parent Folder</a></div>
</div>
<br />
</asp:PlaceHolder>
<table style="width:100%;min-height:100%" cellspacing="0" cellpadding="0">
<tr>
<asp:PlaceHolder runat="server" ID="pTree">
<td valign="top" style="width:200px;border:10px solid #e0e0e0; background-color:White"><b>Select folder</b><ul style="padding-top:5px" id="tree" class="filetree"></ul></td>
</asp:PlaceHolder>
<td valign="top">

<div id="file-toolbar"><a class="highlight toolbar-button" href="javascript:void(0)" onclick="fnRefreshFiles()"><img src="../inc/images/icons/arrow_refresh.png" /> Refresh</a>
<a class="highlight toolbar-button upload" href="javascript:void(0)" onclick="$('#drag-and-drop').toggle(fnGetIEVersion()<1);$('#file-upload').toggle(fnGetIEVersion()>0);" ><img src="../inc/images/icons/add.png" /> Upload</a>

 <div id="drag-and-drop">Drag and drop files here</div>
<div id="file-upload" style="display:none">
Locate file: <input id="fileupload" type="file" name="files[]" multiple>
</div>
<br style="clear:both" />

</div>
<div id="file-filter">
<div>Filter: <common:textbox runat="server" cssclass="name" id="tFilenameFilter" /><button type="button" onclick="fnRefreshFiles()">Apply</button></div>
</div>
<div id="file-list"></div>
</td>
</tr>
</table>

<script type="text/javascript">   
fnEnableJSEffects();
$("#drag-and-drop").bind("dragover",function(){$(this).addClass("dragover"); clearTimeout($(this).data("timer")); $(this).data("timer", setTimeout(function(){$("#drag-and-drop").mouseleave();},1000)); }).bind("drop dragout mouseleave",function(){$(this).removeClass("dragover");});

function fnRefreshFiles(){    
    $.ajax( {
        type: "POST",
        url: $("form").attr( 'action' ),
        data: $("form").serialize()+"&__mode=ajax",
        success: function( response ) {
        //console.log( response );                
        }
    } );
}
function fnPopulateFiles(_Data){   
    $("#file-list").children().detach();
        var _Files=_Data.Files;
        if(_Files.length==0)$("<div style='padding:10px 0 0 10px'>No files to display. Why don't you <a href='javascript:void(0)' onclick='$(\".upload\").click();'>upload</a> some?</div>").appendTo($("#file-list"));
        $(_Files).each(function(i,o){
            if(cStr(o[5]).indexOf("d_folders:")>-1){
                $("<div id='" + o[5] + "' class='file highlight'><img src='/learningportal/inc/library/exec/thumbnail.aspx?f=1&u=/inc/library/media/icons/filetypes/512px/folder.png&w=100&h=75'/><div class='filename'>" + cEllipsis(o[0],24,false)  + "</div><div class='size'></div></div>").data("data",o).appendTo($("#file-list")).click(function(){ $(".hFolderOverride").val($(this).data("data")[5]); fnRefreshFiles(); });   
            }else{
                 var sExtension=o[0].split(".").safeGetValue(o[0].split(".").length-1);
                 var sFileName=stripEndingStr(o[0], "."+sExtension);                 
                 $("<div id='" + o[5] + "' class='file highlight' ondblclick='window.open(\"" + o[1] + "\");'><img src='/learningportal/inc/library/exec/thumbnail.aspx?f=1&u=" + escape(o[1]) + "&w=100&h=75'/><div class='filename'>" + cEllipsis(sFileName,20,false) + "." + sExtension  + "</div><div class='size'>" + cDecimal(o[2]/1024,1) + "kb</div></div>").data("data",o).appendTo($("#file-list"));   
            }
        });
        $("<br style='clear:both' />").appendTo($("#file-list"));        
        $("#file-list .file").mousedown(function(e){
            if(e.which==3){                
                e.preventDefault();
                e.stopPropagation();
                $.vakata.context.show(
                    {"remove":{ "label": "Delete", "action": function (obj) { if(fnContextMenu("remove-by-id", this, obj)) fnRefreshFiles(); }, "separator_before": false, "separator_after": true, "icon": "../inc/images/icons/delete-16.png" }}
                    
                    , $(e.currentTarget), e.pageX, e.pageY, null, e.currentTarget);    
                $.vakata.context.cnt.attr("class", "jstree-default-context");
                return false;
            }else{
                if(!_Options.MultiSelect || !e.ctrlKey)$("#file-list .selected").removeClass("selected");
                $(this).addClass("selected");
                $("#file-select").addClass("highlight").removeClass("disabled");        
            }
        });        
        $("#file-select").removeClass("highlight").addClass("disabled");
}

var _Options={MultiSelect: false};
    $(function () {               
        fnPopulateFiles($(document.body).data("__data"));

        $('#fileupload').fileupload({
                /*sequentialUploads:true,*/
              dataType: 'json', 
              fileInput: $('input:file'),
              /*url: 'server/php/',*/
              done: function (e, data) {
                fnPopulateFiles(data.result);                                                  
              }
          });
        

    });

    <common:label runat="server" id="lScript" />

$(function () {

    $(document.body).on("contextmenu",".jstree-default-context", function(e){                    
            return false;        
    });
    //alert("b");
    //$("#file-list .file").bind("contextmenu",function(){return false;});

    $.jstree._themes = "/inc/css/tree-themes/";
        $("#tree").bind("loaded.jstree", function (event, data) {
        setTimeout(function(){
            var o=$("#tree").find("li:eq(0)")[0];
            if(o)$("#tree").jstree("select_node",o);
        },100);
    }).jstree({
            "core": { "initially_open": ($("#tree_open").length == 0 ? [] : $("#tree_open").val().split(",")), "html_titles": true },
            "themes": { "theme": "default" },
            "json_data": {
                "progressive_render": true,
                "ajax": { "url":
                    function (oNode) {
                        return "/cms/index.aspx?mode=mymetier&root=" + (oNode == -1 ? "d_folders:125" : $(oNode).attr("id"));
                    }
                    /*,"success": function (new_data) {
                    alert(new_data);
                    return new_data;
                }*/
                }
            },            
            "plugins": ["themes", "json_data", "ui", "contextmenu", "dnd", "hotkeys", "crrm"]
        }).bind("select_node.jstree", function (a, b) { b.inst.toggle_node(b.rslt.obj[0]); $(".hFolderOverride").val(b.rslt.obj[0].id); fnRefreshFiles();  });
        
    });

            
	</script>
    <asp:PlaceHolder runat="server" ID="pCKButtonContainer">
        <br />
        <div style="float:right">
        <a class="highlight toolbar-button" id="file-select" href="javascript:void(0)" onclick="var o=$('.file.selected');if(o.length==0)return; window.opener.CKEDITOR.tools.callFunction(window.location.href.split('&CKEditorFuncNum=')[1].split('&')[0], o.data('data')[1] ); window.close();"><img src="../inc/images/icons/accept.png" /> Select</a>
        <a class="highlight toolbar-button" href="javascript:void(0)" onclick="window.close();"><img src="../inc/images/icons/door_open.png" /> Cancel</a>
        </div>
    </asp:PlaceHolder>
</asp:Content>