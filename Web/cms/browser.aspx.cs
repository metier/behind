﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.functions;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Cms
{
    public partial class Browser : jlib.components.webpage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            (Page.Master as Inc.Master.Default).DisableWrapper = true;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string SelectedNode = System.Web.HttpUtility.HtmlDecode(jlib.net.HTTP.getCookieValue(Request.Cookies["browser.aspx-SelectedID"]).Str());
            if (!this.IsPostBack && !SelectedNode.IsNullOrEmpty())
            {
                lTreeOptions.Text = "<input type=\"hidden\" id=\"tree_select\" value=\"" + parse.replaceAll(SelectedNode, ":", "\\:") + "\"/>";
                AttackData.BaseTableRecordImpl<AttackData.Context> Node =
                    AttackData.BaseTableRecordImpl<AttackData.Context>.instantiateObject(SelectedNode);

                string OpenedNodes = "";
                while (Node.GetParents<AttackData.Folder>().Count > 0)
                {
                    Node = Node.GetParents<AttackData.Folder>()[0];
                    OpenedNodes += (OpenedNodes == "" ? "" : ",") + Node.TABLE_NAME + "\\:" + Node._CurrentObjectID;
                }
                lTreeOptions.Text += "<input type=\"hidden\" id=\"tree_open\" value=\"" + OpenedNodes + "\"/>";
            }

            //<input type="hidden" id="tree_open" value="d_folders\:1,d_folders\:82856"/>    
            //        <input type="hidden" id="tree_select" value="d_folders\:82856"/>    

        }
    }
}