﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Linq;
using System.Collections;
using System.Collections.Specialized;
using jlib.functions;
using jlib.net;
using jlib.db;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using AttackData = Phoenix.LearningPortal.Data;

//Need an editor that supports custom tags.
//http://www.voofie.com/content/2/ckeditor-plugin-development/

namespace Phoenix.LearningPortal.Cms
{
    public partial class Edit : jlib.components.webpage
    {
        private DateTime m_oRevisionDate = DateTime.MaxValue;
        public AttackData.Folder PageObject { get; set; }
        //public AttackData.Content Content { get; set; }
        //public List<AttackData.Content> Contents { get; set; }
        private AttackData.Folder m_oLessonObject, m_oCourseObject;
        public AttackData.Folder LessonObject
        {
            get
            {
                if (m_oLessonObject == null)
                {
                    if (PageObject == null) return new AttackData.Folder();
                    m_oLessonObject = PageObject.GetParents<AttackData.Folder>().SafeGetValue(0);
                }
                return m_oLessonObject;
            }
            set { m_oLessonObject = value; }
        }
        public AttackData.Folder CourseObject
        {
            get
            {
                if (m_oCourseObject == null)
                {
                    if (LessonObject == null) return new AttackData.Folder();
                    m_oCourseObject = LessonObject.GetParents<AttackData.Folder>().SafeGetValue(0);
                }
                return m_oCourseObject;
            }
            set
            {
                m_oCourseObject = value;
            }
        }
        private Util.Classes.user m_oUser;

        public void InitializeVariables(int PageID)
        {
            if (PageID == 0) pBody.Visible = false;
            PageObject = AttackData.Folder.LoadByPk(PageID);
            if (PageObject == null)
            {
                string ParentID = Request["parent_id"];
                if (!ParentID.Contains(":")) ParentID = "d_folders:" + ParentID;
                PageObject = new AttackData.Folder() { Context_Value = "content" };
                PageObject.addParent(AttackData.Folder.instantiateObject(ParentID) as AttackData.Folder);
            }
            List<AttackData.Content> children = PageObject.GetChildren<AttackData.Content>();

        }
        private void postProcessContent()
        {
            if (PageObject.AuxField1 == "theory")
            {
                List<XmlNode> questions = new List<XmlNode>();
                List<dynamic> theoryContent = new List<dynamic>();
                PageObject.GetChildren<AttackData.Content>().ForEach(x =>
                {
                    if (x.getSetting("_Folder:" + PageObject.ObjectID + "|Mode") != "hide")
                    {
                        XmlDocument xmlDoc = jlib.net.sgmlparser.SgmlReader.getXmlDoc(convert.cStr(x.Xml.Str().Trim(), "<html />"));
                        theoryContent.Add(new { Text = xmlDoc.OuterXml, XmlDoc = xmlDoc, Content = x });
                        questions.AddRange(xmlDoc.SelectNodes("//div[@class='cms-question']").ToList());
                    }
                });

                //dont parse reflection questions if this page has none and there is more than one theory page
                if (questions.Count == 0 || (PageObject.GetParents<AttackData.Folder>().Count > 0 && PageObject.GetParents<AttackData.Folder>()[0].GetCMSPages("theory").Count > 1))
                {

                }
                else
                {
                    string sReflection = "", sIDList = ",";
                    foreach (XmlNode xmlNode in questions) sIDList += xml.getXmlAttributeValue(xmlNode, "id") + ",";
                    foreach (XmlNode xmlNode in questions)
                    {
                        if (xmlNode.Attributes["id"] == null)
                        {
                            int i = 0;
                            while (sIDList.IndexOf(",question_" + i + ",") > -1) i++;
                            xml.setXmlAttributeValue(xmlNode, "id", "question_" + i);
                            sIDList += "question_" + i + ",";
                        }
                        //var reader = new XElement("data", System.Web.HttpUtility.HtmlDecode(xml.getXmlNodeValue(xmlNode, ".//*[@class='text']"))).CreateReader();
                        //reader.MoveToContent();
                        //sReflection += "<li target=\"" + xml.getXmlAttributeValue(xmlNode, "id") + "\">" + reader.ReadInnerXml() + "</li>";
                        sReflection += "<li target=\"" + xml.getXmlAttributeValue(xmlNode, "id") + "\">" + xml.getXmlNodeValue(xmlNode, ".//*[@class='text']") + "</li>";
                    }

                    if (sReflection != "")
                    {
                        AttackData.Folder Reflection = LessonObject.GetCMSPage("reflection");
                        if (Reflection == null)
                        {
                            Reflection = new AttackData.Folder()
                            {
                                Context_Value = "content",
                                AuxField1 = "reflection",
                                Name = "Questions"
                            };
                            Reflection.Save(Session["user.id"].Int());
                            LessonObject.addChild(Reflection, LessonObject.GetChildren<AttackData.Folder>().IndexOf(LessonObject.GetCMSPage("theory")));
                            LessonObject.Save(Session["user.id"].Int());
                        }

                        int CopiedFrom = 0;
                        AttackData.Content ReflectionContent = Reflection.GetChildren<AttackData.Content>().SafeGetValue(0);

                        if (ReflectionContent != null)
                        {
                            //bool SaveReflectionAsCopy = (PageObject.GetChildren<AttackData.Content>().First().GetParents<AttackData.Folder>().Count != ReflectionContent.GetParents<AttackData.Folder>().Count);
                            //Reflection should never be shared
                            bool SaveReflectionAsCopy = (ReflectionContent.GetParents<AttackData.Folder>().Count > 1);
                            if (SaveReflectionAsCopy)
                            {
                                Reflection.Children.ForEach(x => { if (x.Table2 == "d_content") x.TupleDelete(Session["user.id"].Int()); });
                                CopiedFrom = ReflectionContent.ObjectID;
                                ReflectionContent = null;
                            }
                        }
                        if (ReflectionContent == null)
                        {
                            ReflectionContent = new AttackData.Content()
                            {
                                Name = Reflection.Name,
                                AuxField1 = Reflection.AuxField1,
                                Type = (int)AttackData.Contents.Types.HTML
                            };
                            ReflectionContent.addParent(Reflection);
                            if (CopiedFrom > 0) ReflectionContent.setSetting("CopiedFrom", CopiedFrom.Str());
                        }
                        XmlDocument xmlReflection = jlib.net.sgmlparser.SgmlReader.getXmlDoc(convert.cStr(ReflectionContent.Xml.Str().Trim(), "<html />"));
                        XmlNode xmlList = xmlReflection.SelectSingleNode("//ul");
                        if (xmlList == null) xmlList = xml.addXmlElement(xmlReflection.DocumentElement, "ul");
                        xmlList.InnerXml = sReflection;
                        ReflectionContent.Xml = xmlReflection.SelectSingleNode("html").InnerXml;
                        ReflectionContent.Save(Session["user.id"].Int());
                    }
                    theoryContent.ForEach(x =>
                    {
                        if (x.Text != x.XmlDoc.OuterXml)
                        {
                            System.Web.HttpContext.Current.Items["versioning"] = false;
                            x.Content.Xml = x.XmlDoc.OuterXml;
                            x.Content.Save();
                        }
                    });
                }
            }
        }
        private void saveContent(AttackData.Content content, XmlDocument xmlDoc, int sequence)
        {
            if (content.AuxField1 == "quiz")
                content.Type = (int)AttackData.Contents.Types.XML;
            else if (content.Type!= (int)AttackData.Contents.Types.FileUrl && Phoenix.LearningPortal.Inc.Controls.Tree.AllPageTypes.Contains(content.AuxField1))
                content.Type = (int)AttackData.Contents.Types.HTML;

            XmlNodeList xmlTags;

            //dynamic JSON = jlib.functions.json.DynamicJson.Parse(Request["__PageState"]);
            //for (int x = 0; x < JSON.Count(); x++) {
            //    Dictionary<string, object> Properties = (JSON[x] as System.Dynamic.DynamicObject).GetMembers();
            //    if (Properties.SafeGetValue("Modified").Bln() || Properties.SafeGetValue("Deleted").Bln() || Properties.SafeGetValue("ID").Int() < 0) {
            //        AttackData.Post Post = (Properties.SafeGetValue("ID").Int() > 0 ? AttackData.Post.LoadByPk(Properties.SafeGetValue("ID").Int()) : null);
            //        if (Post == null && Properties.SafeGetValue("Deleted").Bln()) continue;
            //        if (Post == null) {
            //            Post = new AttackData.Post() { CreatedbyID = m_oUser.ID.Int(), ParentID = Properties.SafeGetValue("ParentID").Int() };
            //            if (Post.ParentID == 0) Post.addParent(Content);
            //        }
            //        Post.ResolvedDate = Properties.SafeGetValue("ResolvedDate").Date();
            //        Post.Body = Properties.SafeGetValue("Body").Str();
            //        Post.SetTags(parse.split(Properties.SafeGetValue("Tags"), "|").ToList());

            //        Post.Save(m_oUser.ID.Int());
            //        if (Properties.SafeGetValue("ID").Int() < 0) {
            //            xmlTags = xmlDoc.SelectNodes("//ms[@cid='" + Properties.SafeGetValue("ID") + "'] | //me[@cid='" + Properties.SafeGetValue("ID") + "']");
            //            foreach (XmlNode xmlTag in xmlTags) xmlTag.Attributes["cid"].Value = Post.ObjectID.Str();
            //        }
            //    }
            //}

            //xmlTags = xmlDoc.SelectNodes("//ms | //me");
            //foreach (XmlNode xmlTag in xmlTags) {
            //    if (!xmlTag.InnerXml.IsNullOrEmpty()) {
            //        if (xmlTag.Name == "ms") {
            //            //if (xmlTag.NextSibling == null) 
            //            xmlTag.ParentNode.InsertAfter(xmlDoc.CreateTextNode(xmlTag.InnerText), xmlTag);
            //            //else xmlTag.NextSibling.InnerXml = xmlTag.InnerXml + xmlTag.NextSibling.InnerXml;
            //        } else if (xmlTag.Name == "me") {
            //            //if (xmlTag.PreviousSibling == null) 
            //            xmlTag.ParentNode.InsertBefore(xmlDoc.CreateTextNode(xmlTag.InnerText), xmlTag);
            //            //else xmlTag.PreviousSibling.InnerXml = xmlTag.PreviousSibling.InnerXml + xmlTag.InnerXml;
            //        }
            //        xmlTag.InnerXml = "";
            //    }
            //}
            //xmlTags = xmlDoc.SelectNodes("//span[contains(@class, \"cms-comment\")]");
            //foreach (XmlNode xmlTag in xmlTags) {
            //    while (xmlTag.ChildNodes.Count > 0)
            //        xmlTag.ParentNode.InsertBefore(xmlTag.ChildNodes[0], xmlTag);
            //    xmlTag.ParentNode.RemoveChild(xmlTag);
            //}
            XmlNodeList xmlAddIDs = xmlDoc.SelectNodes("//div[@class='metierquestion'] | //div[@class='metierbox']");
            foreach (XmlNode node in xmlAddIDs)
            {
                if (xml.getXmlAttributeValue(node, "id").IsNullOrEmpty()) xml.setXmlAttributeValue(node, "id", jlib.helpers.general.getRandomString("abcdefghijklmnopqrstuvwxyz", 7));
            }
            XmlNodeList xmlImages = xmlDoc.SelectNodes("//img");
            foreach (XmlNode xmlImage in xmlImages)
            {
                string ImageUrl = xml.getXmlAttributeValue(xmlImage, "src").Str();
                string ImageAlt = xml.getXmlAttributeValue(xmlImage, "alt").SafeSplit("|", 0);
                if (ImageUrl.StartsWith("/")) xml.setXmlAttributeValue(xmlImage, "src", System.Configuration.ConfigurationManager.AppSettings["site.url"] + xml.getXmlAttributeValue(xmlImage, "src"));
                else if (ImageUrl.StartsWith("data:"))
                {
                    string Extension = parse.inner_substring(ImageUrl, "data:", null, ",", null);
                    Extension = parse.inner_substring(Extension, null, null, ";", null);
                    string FileUrl = Util.Cms.SaveFile(CourseObject, LessonObject, PageObject, Extension, System.Convert.FromBase64String(parse.inner_substring(ImageUrl, ",", null, null, null)), ImageAlt, false, "");
                    if (!FileUrl.IsNullOrEmpty()) xml.setXmlAttributeValue(xmlImage, "src", FileUrl);
                }
                ImageUrl = xml.getXmlAttributeValue(xmlImage, "src").Str();

                //Auto-Update Name of uploaded file based on Image Alt
                if (ImageUrl.Contains("/inc/library/exec/thumbnail.aspx?")) ImageUrl = parse.inner_substring(ImageUrl + "&", "u=", null, null, "&");
                if (!ImageUrl.Contains("file:///") && !ImageAlt.IsNullOrEmpty() && !parse.removeXChars(ImageUrl, "abcdefghijkl,mopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ").Contains(parse.removeXChars(ImageAlt, "abcdefghijkl,mopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")))
                {                    
                    AttackData.Content File = AttackData.Contents.GetByField(Filename: Server.MapPath(parse.replaceAll(ImageUrl.Contains("://") ? "/" + parse.inner_substring(ImageUrl, "://", "/", null, null) : ImageUrl,"?","",":",""))).Execute().FirstOrDefault();
                    if (File != null && parse.removeXChars(ImageUrl, "abcdefghijkl,mopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ").Contains(parse.removeXChars(File.Name, "abcdefghijkl,mopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")))
                    {
                        File.Name = ImageAlt + System.IO.Path.GetExtension(ImageUrl);
                        File.Type = (int)AttackData.Contents.Types.FileUrl;
                        File.Save(Session["user.id"].Int());
                    }
                }
            }
            content.Xml = xml.getXmlNodeValue(xmlDoc, "html");
            if (content.AuxField1 == "quiz")
            {
                if (xmlDoc != null && xmlDoc.SelectSingleNode("html") != null)
                    content.Xml = System.Xml.Linq.XElement.Parse(xmlDoc.SelectSingleNode("html").InnerXml).ToString();
            }
            else if (PageObject.AuxField1 == "intro")
            {
                XmlNodeList xmlNodes = xmlDoc.SelectNodes("//div[@class='intro-quote']");
                foreach (XmlNode xmlNode in xmlNodes)
                {

                    XmlElement xmlQuote = xml.setXmlAttributeValue(xmlDoc.CreateElement("p"), "id", "quote") as XmlElement;
                    xmlQuote.InnerXml = parse.stripWhiteChars(xml.getXmlNodeValue(xmlNode, ".//div[@class='text']"));
                    XmlElement xmlWriter = xml.setXmlAttributeValue(xmlDoc.CreateElement("span"), "id", "writer") as XmlElement;
                    xmlWriter.InnerXml = parse.stripWhiteChars(xml.getXmlNodeValue(xmlNode, ".//div[@class='author']"));
                    xmlQuote.AppendChild(xmlWriter);
                    xmlNode.ParentNode.InsertAfter(xmlQuote, xmlNode);
                    xmlNode.ParentNode.RemoveChild(xmlNode);
                }
                if (xmlDoc.SelectSingleNode("html") != null) content.Xml = xmlDoc.SelectSingleNode("html").InnerXml;
                //Remove quote markup
            }
            if (content.Type != 2 && content.AuxField1 != "dictionary") content.Xml = parse.replaceAll(content.Xml, "&amp;", "&");

            bool xmlIsModified = content.Fields[AttackData.Contents.Columns.Xml.FieldName].FieldStatus == jlib.DataFramework.FieldStatuses.Modified;
            content.Save(Session["user.id"].Int());

            if (xmlIsModified && content.AuxField1 == "quiz" && (LessonObject == null || LessonObject.AuxField1 != "finaltest"))
            {
                content.GetParents<AttackData.Folder>().ForEach(x =>
                {
                    AttackData.Folder course = x.GetParentUntil<AttackData.Folder>(10, new Func<AttackData.Folder, bool>(item => item.Context_Value == "course"));
                    if (course != null)
                    {
                        Util.Cms.GenerateFinalTest(course, false);
                    }
                });
            }
        }

        private void addChildToAllLinkedParents(AttackData.Content content, AttackData.Content afterContent)
        {
            PageObject.addChild(content, (afterContent == null ? 0 : PageObject.GetChildIndex(afterContent) + 1));
            AttackData.Content child = PageObject.GetChildren<AttackData.Content>().FirstOrDefault(x => { return x.ObjectID != content.ObjectID; });
            if (child != null)
            {
                child.GetParents<AttackData.Folder>().ForEach(x =>
                {
                    if (x.ObjectID != PageObject.ObjectID)
                    {
                        x.addChild(content, (afterContent == null ? 0 : x.GetChildIndex(afterContent) + 1));
                        x.Save(Session["user.id"].Int());
                    }
                });
            }
        }
        private void ResetCourseCache()
        {
            if(PageObject==null) PageObject = AttackData.Folder.LoadByPk(this.QueryString("PageID").Int());
            if (PageObject != null && CourseObject.ObjectID > 0)            
                Controllers.PlayerController.ResetLessonCache(CourseObject.ObjectID, LessonObject.ObjectID);            
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (convert.isDate(Request["revision"])) System.Web.HttpContext.Current.Items["version.date"] = Request["revision"].Dte();

            if (this.QueryString("action") == "addsection")
            {
                System.Web.HttpContext.Current.Items["versioning"] = false;
                PageObject = AttackData.Folder.LoadByPk(this.QueryString("PageID").Int());
                AttackData.Content afterContent = null;
                if (this.QueryString("location").Contains("after:"))
                    afterContent = AttackData.Content.LoadByPk(this.QueryString("location").SafeSplit(":", 1).Int());

                AttackData.Content Content = null;
                if (this.QueryString("existing-id").Int() > 0)
                {
                    Content = AttackData.Content.LoadByPk(this.QueryString("existing-id").Int());
                    Content.setSetting("_Folder:" + PageObject.ObjectID + "|Mode", "nohead");
                    Content.Save(Session["user.id"].Int());
                }
                else
                {
                    Content = new AttackData.Content()
                    {
                        Name = "Section " + (afterContent == null ? 1 : PageObject.GetChildIndex(afterContent) + 2),
                        CreatedbyID = Session["user.id"].Int(),
                        AuxField1 = convert.cStr(PageObject.AuxField1, "theory"),
                        Type = (int)AttackData.Contents.Types.HTML
                    };
                    Content.setSetting("_Folder:" + PageObject.ObjectID + "|Mode", "nohead");
                    Content.Save(Session["user.id"].Int());
                }


                if (this.QueryString("apply-to") == "all")
                {
                    addChildToAllLinkedParents(Content, afterContent);
                }
                else
                {
                    PageObject.addChild(Content, (afterContent == null ? 0 : PageObject.GetChildIndex(afterContent) + 1));
                }
                PageObject.Save(Session["user.id"].Int());

                Response.ContentType = "application/javascript";
                Response.Write("fnEditContent(" + Content.ObjectID + ");");
                ResetCourseCache();
                Response.End();
            }
            else if (this.QueryString("action") == "deletecontent")
            {
                AttackData.Content contentObject = AttackData.Content.LoadByPk(this.QueryString("ContentID").Int());
                PageObject = AttackData.Folder.LoadByPk(this.QueryString("PageID").Int());

                int parentsDeleted = 0;
                contentObject.Parents.ForEach(link =>
                {
                    if (this.QueryString("apply-to") == "all" || PageObject.ObjectID == link.ID1)
                    {
                        link.TupleDelete(Session["user.id"].Int());
                        parentsDeleted++;
                    }
                });
                if (contentObject.Parents.Count == parentsDeleted) contentObject.TupleDelete(Session["user.id"].Int());
                ResetCourseCache();
                Response.End();
            }
            else if (this.QueryString("action") == "setcontentorder")
            {
                System.Web.HttpContext.Current.Items["versioning"] = false;
                PageObject = AttackData.Folder.LoadByPk(this.QueryString("PageID").Int());
                List<string> order = Request["Order"].Split(',').ToList();
                PageObject.Children.ForEach(x =>
                {
                    if (x.Table2 == "d_content" && order.Contains(x.ID2.Str()))
                        x.Sequence = order.IndexOf(x.ID2.Str());
                });
                PageObject.Save(Session["user.id"].Int());
                ResetCourseCache();
                Response.End();
            }
            else if (this.QueryString("action") == "gettempcontent" || this.QueryString("action") == "savetempcontent")
            {
                System.Web.HttpContext.Current.Items["versioning"] = true;
                PageObject = AttackData.Folder.LoadByPk(this.QueryString("PageID").Int());
                AttackData.Content currentContent = (this.QueryString("ObjectID") == "firstchild" ? PageObject.GetChildren<AttackData.Content>().SafeGetValue(0) : AttackData.Content.LoadByPk(this.QueryString("ObjectID").Int()));
                if (currentContent != null && (currentContent.ModifiedbyID == Session["user.id"].Int() && currentContent.CreateDate.AddMinutes(5) > DateTime.Now))
                {
                    System.Web.HttpContext.Current.Items["versioning"] = false;
                }
                //do we have temporary user-specific content stored for this user
                Response.ContentType = "application/json";
                if (currentContent == null)
                {
                    currentContent = new AttackData.Content()
                    {
                        ModifiedbyID = Session["user.id"].Int(),
                        CreatedbyID = Session["user.id"].Int(),
                        AuxField1 = PageObject.AuxField1,
                        Name = PageObject.AuxField1.ToCamelCase() + " 1",
                        Type = (int)(PageObject.AuxField1 == "quiz" ? AttackData.Contents.Types.XML : AttackData.Contents.Types.HTML)
                    };
                }
                if (this.QueryString("action") == "savetempcontent")
                {
                    if (this.QueryString("saveascopy").Bln())
                    {
                        //JVT TODO                    
                        //List<AttackData.Content> 
                        List<AttackData.RowLink> children = PageObject.Children.ToList();
                        children.ForEach(x =>
                        {
                            if ((x.Object2 as AttackData.Content) != null)
                            {
                                AttackData.Content oldContent = x.Object2 as AttackData.Content;
                                AttackData.Content newContent = oldContent.Clone(DataRowVersion.Current, true) as AttackData.Content;
                                newContent.Fields[AttackData.Contents.Columns.ObjectID.FieldName].FieldStatus = jlib.DataFramework.FieldStatuses.NotSet;
                                if (currentContent.ObjectID == oldContent.ObjectID) currentContent = newContent;
                                newContent.setSetting("CopiedFrom", oldContent.ObjectID.Str());
                                newContent.Save(Session["user.id"].Int());
                                x.TupleDelete(Session["user.id"].Int());
                                PageObject.Children.Remove(x);
                                PageObject.addChild(newContent);
                            }
                        });
                        PageObject.Save(Session["user.id"].Int());
                    }
                    dynamic data = jlib.functions.json.DynamicJson.Parse(Request["__data"]);
                    dynamic content = data.Content;
                    currentContent.Name = content.Name;

                    currentContent.AuxField1 = content.AuxField1;
                    currentContent.Xml = content.Xml;
                    currentContent.setSetting("_Folder:" + PageObject.ObjectID + "|Mode", content.Mode);
                    bool isAdding = (currentContent.RecordState == DataRowAction.Add);
                    XmlDocument xmlDoc = jlib.net.sgmlparser.SgmlReader.getXmlDoc(System.Text.RegularExpressions.Regex.Replace(content.Xml, " jquery\\d+=\"\\d+\"", ""));
                    XmlNodeList splits = xmlDoc.SelectNodes("//div[@class='cms-split']");

                    List<XmlDocument> docs = new List<XmlDocument>() { xmlDoc };
                    if (splits.Count > 0) docs = xmlDoc.SplitOnNodes(splits);
                    for (int x = 0; x < docs.Count; x++)
                    {
                        xml.ExpandEmptyHTMLTags(docs[x]);
                        AttackData.Content prevContent = null;
                        if (x > 0)
                        {
                            prevContent = currentContent;
                            currentContent = currentContent.Clone(DataRowVersion.Current, true) as AttackData.Content;
                            currentContent.Fields[AttackData.Contents.Columns.ObjectID.FieldName].FieldStatus = jlib.DataFramework.FieldStatuses.NotSet;
                            currentContent.setSetting("_Folder:" + PageObject.ObjectID + "|Mode", "nohead");
                        }
                        saveContent(currentContent, docs[x], x);
                        if (isAdding || x > 0)
                        {
                            addChildToAllLinkedParents(currentContent, prevContent);
                            PageObject.Save(Session["user.id"].Int());
                        }
                    }
                    postProcessContent();
                }
                Response.Write("{\"Code\":200, \"Content\":" + currentContent.ToJson().Substr(0, -1) + ",\"Mode\":\"" + currentContent.getSetting("_Folder:" + PageObject.ObjectID + "|Mode") + "\",\"ModifiedBy\":\"" + (currentContent.ModifiedBy == null ? "" : currentContent.ModifiedBy.Username) + "\"}}");
                ResetCourseCache();
                Response.End();
            }
            gSettingsGrid.QuerySource = AttackData.Settings.GetByField(TableName: "d_folders", ObjectID: Request["ObjectID"].Int());
            gVersionsGrid.GridRowPrerender += new jlib.controls.jgrid.OnGridRowPrerenderHandler(gVersionsGrid_GridRowPrerender);
            (Page.Master as Inc.Master.Default).DisableWrapper = true;
            m_oUser = (Page.Master as Inc.Master.Default).ActiveUser;

            if (this.QueryString("action") == "upload")
            {
                int FolderID = parse.inner_substring(System.Web.HttpUtility.UrlDecode(this.QueryString("_url").SafeSplit("#", 0)) + "&", "ObjectID=", null, "&", null).Int();
                InitializeVariables(FolderID);
                for (int x = 0; x < Request.Files.Count; x++)
                {
                    string FileName = Util.Cms.SaveFile(CourseObject, LessonObject, PageObject, convert.cStr(System.IO.Path.GetExtension(Request.Files[x].FileName), Request.Files[x].ContentType), convert.cStreamToByteArray(Request.Files[x].InputStream), System.IO.Path.GetFileNameWithoutExtension(Request.Files[x].FileName), this.QueryString("crop").Bln(), this.QueryString("mode"));
                    Response.Write("<script>/(2,'" + FileName + "');</script>/\n");
                }
                Response.End();
            }
        }



        void gVersionsGrid_GridRowPrerender(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            AttackData.Content Content = e.DBRow as AttackData.Content;
            e.setColValue(cVersionID, "d_content:" + Content.ObjectID + ":" + Content.DeleteDate.Ticks);
        }
        private void RenderObjectDiff()
        {
            string[] IDs = this.QueryString("ids").Split(',');
            List<AttackData.Content> Files = new List<AttackData.Content>();
            foreach (string s in IDs)
            {
                if (s.Trim() != "")
                {
                    AttackData.Content File = AttackData.Contents.GetByField(ObjectID: s.SafeSplit(":", 1).Int(), DeleteDate: new DateTime(s.SafeSplit(":", 2).Lng())).Execute().FirstOrDefault();
                    if (File != null) Files.Add(File);
                }
            }

            if (Files.Count > 1)
            {
                Files = Files.OrderBy(x => x.DeleteDate).ToList();
                Response.Write(
                @"<html>
<link type='text/css' rel='stylesheet' href='" + System.Configuration.ConfigurationManager.AppSettings["site.url"] + ResolveClientUrl("~/player/css/tempstyle.css") + @"' />
<style>
ins {
    background-color: #cfc;
    text-decoration: none;
}
del {
    color: #999;
    background-color:#FEC8C8;
}
</style>
<body>
                "
                + Util.Diff.HtmlDiff.Execute(Files[0].Xml, Files[1].Xml)
                + "</body></html>");
            }
            else
            {
                Response.Write("Not enough files to diff. Two files required");
            }
            Response.End();
        }
        private void RenderExportCourse()
        {

            StringBuilder SB = new StringBuilder(@"
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />        
<link type='text/css' rel='stylesheet' href='" + System.Configuration.ConfigurationManager.AppSettings["site.url"] + @"/player/css/tempstyle.css' />
<link type='text/css' rel='stylesheet' href='" + System.Configuration.ConfigurationManager.AppSettings["site.url"] + @"/css/metier-common.css?4' />
<style>pre{font-size:12px}</style> 
<link rel='stylesheet' type='text/css' media='screen' href='" + System.Configuration.ConfigurationManager.AppSettings["site.url"] + @"/inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.css' />				</head>
 <body>");

            AttackData.Folder Course = AttackData.Folder.LoadByPk(Request["ObjectID"].Int());
            SB.Append("<h1><u>" + Course.Name + "</u></h1>");
            Course.GetChildren<AttackData.Folder>().ForEach(x =>
            {
                if (x.Context_Value == "lesson" && x.AuxField1 != "finaltest")
                {
                    SB.Append("<h1>" + x.Name + "</h1>");
                    x.GetChildren<AttackData.Folder>().ForEach(y =>
                    {
                        if (y.Context_Value == "lesson")
                        {
                            SB.Append("<h1><u>" + y.Name + "</u></h1>");
                            y.GetChildren<AttackData.Folder>().ForEach(z =>
                            {
                                if (z.Context_Value == "content" && !",splash,complete,".Contains("," + z.AuxField1 + ","))
                                {
                                    List<AttackData.Content> PageContent = z.GetChildren<AttackData.Content>();
                                    SB.Append("<div style='border-top:3px dashed silver'><h1>" + z.Name + "</h1>");
                                    PageContent.ForEach(c =>
                                    {
                                        if (!",splash,complete,".Contains("," + c.AuxField1 + ",") && c.getSetting("_Folder:" + z.ObjectID + "|Mode") != "hide")
                                            SB.Append(c.AuxField1 == "quiz" ? "<pre>" + System.Web.HttpUtility.HtmlEncode(c.Xml) + "</pre>" : "<div>" + ResizeLargeImages(c.Xml) + "</div>");
                                    });
                                    SB.Append("</div>");
                                }
                            });
                        }
                        else if (y.Context_Value == "content" && !",splash,complete,".Contains("," + y.AuxField1 + ","))
                        {
                            List<AttackData.Content> PageContent = y.GetChildren<AttackData.Content>();
                            SB.Append("<div style='border-top:3px dashed silver'><h1>" + y.Name + "</h1>");
                            PageContent.ForEach(c =>
                            {
                                if (!",splash,complete,".Contains("," + c.AuxField1 + ",") && c.getSetting("_Folder:" + y.ObjectID + "|Mode") != "hide")
                                    SB.Append(c.AuxField1 == "quiz" ? "<pre>" + System.Web.HttpUtility.HtmlEncode(c.Xml) + "</pre>" : "<div>" + ResizeLargeImages(c.Xml) + "</div>");
                            });
                            SB.Append("</div>");
                        }
                    });
                }
            });
            Response.Write(SB.ToString() + "</body></html>");
            Response.End();
        }
        private string ResizeLargeImages(string Xml, int MaxWidth = 500, int MaxHeight=1500) {
            XmlDocument xmlDoc = jlib.net.sgmlparser.SgmlReader.getXmlDoc(convert.cStr(Xml.Str().Trim(), "<html />"));
            var images = xmlDoc.SelectNodes("//img[@src]");
            images.ToList().ForEach(img => {
                var imageUrl = xml.getXmlAttributeValue(img, "src");
                if (imageUrl.Contains("/inc/library/exec/thumbnail.aspx?")) imageUrl = parse.inner_substring(imageUrl + "&", "u=", null, null, "&");
                imageUrl = jlib.helpers.general.getThumbnailUrl(imageUrl, MaxWidth, MaxHeight, true, true, null,  jlib.helpers.general.FileStatus.None);
                xml.setXmlAttributeValue(img, "src", imageUrl);
            });
            return xmlDoc.OuterXml;
        }
        //private void SerializeObject()
        private void RenderExportJSON()
        {
            AttackData.Folder Folder = AttackData.Folder.LoadByPk(Request["ObjectID"].Int());
            Response.Write(Folder.ToJson(true, null, null, null, 10));
            Response.End();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.QueryString("action") == "diff")
            {
                //AttackData.Content a = AttackData.Content.instantiateObject("d_content:37745:634856355559130000") as AttackData.Content;
                RenderObjectDiff();
            }
            else if (this.QueryString("action") == "export-course")
            {
                RenderExportCourse();
            }
            else if (this.QueryString("action") == "export-json")
            {
                RenderExportJSON();
            }

            System.Web.HttpContext.Current.Items["versioning"] = true;

            InitializeVariables(Request["ObjectID"].Int());

            if (!this.IsPostBack)
            {
                tName.Text = PageObject.Name;
                dIconType.setValue(PageObject.AuxField1, true);
                tPlayerSettings.setValue(PageObject.getSetting("PlayerSettings"));                
            }
            else if (Request["mode"] == "ajax-postback")
            {
                PageObject.Name = tName.Text;
                PageObject.AuxField1 = dIconType.SelectedValue;
                PageObject.setSetting("PlayerSettings", tPlayerSettings.Text);                
                PageObject.Save(Session["user.id"].Int());
                if (Request["mode"] == "ajax-postback" && Request["ObjectID"].Int() == 0)
                {
                    AttackData.Content content = new AttackData.Content()
                    {
                        Name = PageObject.Name,
                        AuxField1 = PageObject.AuxField1,
                        Type = (int)(PageObject.AuxField1 == "quiz" ? AttackData.Contents.Types.XML : AttackData.Contents.Types.HTML)
                    };
                    content.addParent(PageObject);
                    content.Save();
                    Response.Write("//redirect: " + parse.replaceAll(Request.Url.PathAndQuery, "ObjectID=0", "ObjectID=" + PageObject.ObjectID));
                }
                Response.Write("//ok");
                Response.End();
            }
            List<int> contentIDs = new List<int>();
            PageObject.GetChildren<AttackData.Content>().ForEach(x => { contentIDs.Add(x.ObjectID); });

            gVersionsGrid.QuerySource = AttackData.Contents.GetRevisions(contentIDs);
            if (PageObject.GetChildren<AttackData.Content>().Count > 0)
            {
                //PageObject.GetChildren<AttackData.Content>()[0].getPaths().cl
                gAlsoUsedIn.DataSource = PageObject.GetChildren<AttackData.Content>()[0].getPaths();
            }

            lScript.Text = "$(document.body).data('__data', {\"Tags\":" + jlib.functions.json.DynamicJson.Serialize(AttackData.Folder.LoadByPk(5).GetFoldersJSON(false)) + ",\"UserID\":" + Session["user.id"].Int() + ",\"UserName\":\"" + (m_oUser == null ? "" : m_oUser.Username) + "\", \"Folder\":$.extend(" + PageObject.ToJson() + ",{Path:'" + (PageObject.GetParents<AttackData.Folder>().Count == 0 ? "" : parse.replaceAll(parse.stripEndingCharacter(PageObject.GetParents<AttackData.Folder>()[0].getPath().Path, ">"), "'", "&#39")) + "',PathID:'" + (PageObject.GetParents<AttackData.Folder>().Count == 0 ? "" : parse.replaceAll(parse.stripEndingCharacter(PageObject.GetParents<AttackData.Folder>()[0].getPath().ID, ">"), "'", "&#39")) + "',Parent:" + (PageObject.Parents.Count == 0 || PageObject.Parents[0].ID1 == 0 || PageObject.Parents[0].Object1 == null ? "null" : PageObject.Parents[0].Object1.ToJson()) + "}),Course:" + (CourseObject == null ? "null" : CourseObject.ToJson()) + "});";

            if (PageObject != null)
                hPreview.Attributes["src"] = "../player/player.aspx?preview=true&PageID=" + PageObject.ObjectID + "&editing=true&revision=" + this.QueryString("revision");
        }

    }
}