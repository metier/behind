﻿<%@ Page Title="Metier Oracle Backup and Restore" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Cms.Import" Codebehind="import.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
<h2 class="page-title"><img src="../inc/images/icons/tools_32.png" alt="Backup and Restore Data" />Import Content</h2>
<common:label runat="server" ID="lErrorLabel" Tag="div" Visible="false" style="border:3px dotted red; color:Red; font-weight:bold" />
<!--
<common:button.button runat="server" text="Clear database" id="bClearDatabase" buttonmode="SubmitButton" /><br />
-->
<b>Import</b><br />
File pattern: <common:textbox runat="server" ID="tXmlName" Text="pm-0201no-forsvaret.xml"  /> (optional)<br />
Existing Course ID: <common:textbox runat="server" ID="tExistingCourseID" /> (optional)<br />
        <common:radiobutton runat="server" text="Never overwrite" GroupName="overwrite-options" /><br />

    <common:radiobutton runat="server" ID="cOverwriteExistingContent" text="Overwrite existing content" Checked="true" GroupName="overwrite-options" /><br />
    <common:radiobutton runat="server" ID="cOverwriteExistingContentNonLinked" text="Overwrite existing content IF NOT USED IN OTHER COURSES" GroupName="overwrite-options" /><br />

    <common:checkbox runat="server" ID="Checkbox1" text="Overwrite existing content IF NOT USED IN " Checked="true" /><br />
<common:button.button runat="server" text="Import Data" id="bImportData" buttonmode="SubmitButton" /><br />
<hr />

<common:button.button runat="server" text="Strip Leadning and Trailing empty P-tags" id="bClearEmptyPs" buttonmode="SubmitButton" /><br />
<common:button.button runat="server" text="Parse Tankevekkere" id="bParseReflection" buttonmode="SubmitButton" /><br />


<common:button.button runat="server" text="Import P2 P" id="bImportP2P" buttonmode="SubmitButton" /><br />
    <common:button.button runat="server" text="Fix Quiz" id="bFixQuiz" buttonmode="SubmitButton" /><br />
<br />

    <common:button.button runat="server" text="Replace and download quiz backgrounds" id="bDownloadQuizAttachments" buttonmode="SubmitButton" /><br />
    
    <common:label runat="server" ID="lCourseList" />
        <br />
        <br />
        <br />
       
</asp:Content>