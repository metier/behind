﻿<%@ Page Title="Metier Quiz Merge" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Cms.QuizMerge" Codebehind="quizmerge.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server">
    <style>
        .processed a
        {
            text-decoration:line-through;
        }
        .followup a
        {
            color:red;
            font-weight:bold;            
        }
        
        .qcontainer
        {
            border:2px solid silver;
        }
        .bSave
        {
            font-size:16px;
            font-weight:bold;
            color:green;
        }
        .bExitAndFollowUp
        {
            font-size: 16px;
            font-weight: bold;
            color: red;
        }
    </style>
<h2><img src="../inc/images/icons/tools_32.png" alt="Other Reports" />Quiz Merger</h2>
			
<br style="clear: both;" />
<common:label runat="server" ID="lContent" />
 <asp:PlaceHolder runat="server" ID="pDetails">
       <br /><br />
    <table>
        <tr>
            <td><b style="font-size:16px">Old questions</b></td>
            <td></td>
            <td><b style="font-size:16px">Current questions</b></td>
        </tr>
        <tr>
            <td valign="top" class="left sortable" style="min-width:600px;padding:10px; border:3px dashed yellow;"></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td valign="top" class="right sortable" style="min-width:600px;padding:10px; border:3px dashed  green;"></td>
        </tr>
    </table>
    <br /><br /><br /><br />
    <button type="button" class="bSave">Save and mark as cleaned</button>
    <button type="button" class="bExit">Return</button>
    <button type="button" class="bExitAndFollowUp">Exit and mark for follow-up</button>
<script language="javascript">
    <common:label runat="server" ID="lScript" />
for(var x=0;x<Questions.length;x++){
    var c=$("<div class='qcontainer'><iframe style='width:700px;height:600px'></iframe><br /><button type='button' class='toggle'>Move</button></div>").appendTo($("table .left")).data("xml", Questions[x]);
    c.find("iframe").attr("src","/learningportal/player/player.aspx?header=hide&preview=true&PageID=36359&ContentID=33358&preview-data="+encodeURIComponent("<quiz><questions>"+Questions[x]+"</questions></quiz>"));        
}
var XML= $($.parseXML(replaceAll(CurrentContent.Xml, "&quot;", "\"")));
var oQuestions = XML.find("question")
for (var x = 0; x < oQuestions.length; x++) {
    var c=$("<div class='qcontainer'><iframe style='width:700px;height:600px'></iframe><br /><button type='button' class='toggle'>Move</button></div>").appendTo($("table .right")).data("xml", fnSerializeXML(oQuestions[x]));
    c.find("iframe").attr("src","/learningportal/player/player.aspx?header=hide&preview=true&PageID=36359&ContentID=33358&preview-data="+encodeURIComponent("<quiz><questions>"+fnSerializeXML(oQuestions[x])+"</questions></quiz>"));        
}
$("button.toggle").click(function(){
    $(this).parents(".qcontainer").appendTo($(this).parents(".left").length==0?$(".left"):$(".right"));
});

$( ".sortable" ).sortable({ connectWith: ".sortable" });
$( ".sortable" ).disableSelection();
$(".bSave").click(function(){
    var xml="<quiz><questions>";
    $(".right .qcontainer").each(function(){
        xml+=$(this).data("xml");
    });    
    $("form").append("<input type='hidden' name='action' value='save' />").append($("<input type='hidden' name='data' />").val(xml+"</questions></quiz>")).submit();
});
$(".bExit").click(function(){window.location='quizmerge.aspx';});
$(".bExitAndFollowUp").click(function(){$("form").append($("<input type='hidden' name='action' value='followup' />")).submit();});

</script>
</asp:PlaceHolder>
        
</asp:Content>

