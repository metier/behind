﻿//insert newline before question regex: \t\d+ (?=\w)
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using jlib.functions;
using jlib.net;
using jlib.db;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Cms
{
    public partial class Exam : jlib.components.webpage
    {
        private AttackData.Folder ExamContainer;
        private AttackData.Exam ExamObject;
        protected void Page_Init(object sender, EventArgs e)
        {
            this.ErrorLabel = lFormError;
            ado_helper oData = new ado_helper("sql.dsn.cms");
            (Page.Master as Inc.Master.Default).DisableWrapper = true;
            //m_oExam = sqlbuilder.executeSelect(oData, "d_exams", "object_id", this.QueryStringInt("id"), new condition("'" + convert.cDate(this.QueryString("revision")) + "' between create_date and delete_date"));
            //if (!this.IsPostBack && m_oExam.Rows.Count > 0) {
            //    this.populateValues("d_exams", m_oExam.Rows[0]);
            //}


            gOfferings.GridBeforeDatabind += new jlib.controls.jgrid.OnGridBeforeDatabindHandler(gOfferings_GridBeforeDatabind);
            gOfferings.GridRowClientDel += new jlib.controls.jgrid.OnGridRowClientDelHandler(gOfferings_GridRowClientDel);
            gOfferings.GridRowClientUpdate += new jlib.controls.jgrid.OnGridRowClientUpdateHandler(gOfferings_GridRowClientUpdate);
            gOfferings.GridCustomData += new jlib.controls.jgrid.OnGridCustomDataHandler(gOfferings_GridCustomData);
            gOfferings.DataHelper = oData;
            //oImporter.KeyID = this.CurrentUserID;
            new Util.Import().attach(oImporter);

            ExamContainer = AttackData.Folder.LoadByPk(this.QueryStringInt("ObjectID"));
            if (ExamContainer != null) ExamObject = ExamContainer.GetChildren<AttackData.Exam>().SafeGetValue(0);
        }

        void gOfferings_GridCustomData(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            if (e.Type.StartsWith("export-responses"))
            {
                int iReleaseID = parse.splitValue(e.Type, "|", 1).Int();
                if (iReleaseID > 0)
                {
                    AttackData.Releases Releases = AttackData.Releases.GetByField(ID: iReleaseID).Execute();
                    string sFileName = Util.Export.exportExamResponses(Releases, Session["user.id"].Int());
                    e.ClientJSAfter = "$.infoBar(\"Download file from <a href='../download.aspx?file=" + System.Web.HttpUtility.UrlEncode(System.IO.Path.GetFileName(sFileName)) + "' target='_blank'>here</a>\", { ID: \"alert-once\", type: $.infoBar.type.ok});";
                    e.Return = true;
                    //DataTable oAnswers=sqlbuilder.getDataTable(gOfferings.DataHelper, "select * from ")

                }
            }
        }

        void gOfferings_GridRowClientUpdate(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            if (ExamObject != null)
            {
                DataTable oDT = sqlbuilder.executeSelect(gOfferings.DataHelper, "d_releases", "id", e.PK);
                if (oDT.Rows.Count == 0)
                {
                    oDT.Rows.Add(sqlbuilder.setRowValues(oDT.NewRow(), "Type", "exam", "ObjectID", ExamObject.ObjectID));
                }
                e.setRowValues(oDT.Rows[0]);
                if (convert.cStr(oDT.Rows[0]["CreateDate"]) == "") oDT.Rows[0]["CreateDate"] = DateTime.Now;
                if (convert.cStr(oDT.Rows[0]["ObjectDate"]) == "" || e.getColValue(cOObjectDate).ToLower().Contains("latest")) oDT.Rows[0]["ObjectDate"] = DateTime.MaxValue;
                if (e.getColValue(cOAllotedTime) == "") oDT.Rows[0]["MaxTime"] = convert.cInt(sqlbuilder.executeSelectValue(gOfferings.DataHelper, "d_exams", "MaxTime", "ObjectID", oDT.Rows[0]["ObjectID"], new condition("'" + convert.cDate(oDT.Rows[0]["ObjectDate"]) + "' BETWEEN CreateDate and DeleteDate")));

                ado_helper.update(oDT);
            }
        }


        void gOfferings_GridRowClientDel(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            sqlbuilder.executeUpdate(gOfferings.DataHelper, "d_releases", "id", e.PK, "deleted", 1);
        }

        void gOfferings_GridBeforeDatabind(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            if (ExamObject != null)
            {
                gOfferings.setDataSourcePaged("*, CASE WHEN ObjectDate>GetDate() THEN 'Latest' ELSE cast(ObjectDate as varchar(255)) END AS VersionDate", "d_releases", "deleted=0 and ObjectID=" + ExamObject.ObjectID);
            }
        }
        private void ExtractInlineImages(Util.Exam.exam_section section)
        {
            section.Sections.ForEach(x => ExtractInlineImages(x));
            if (!section.Text.IsNullOrEmpty() && section.Text.IndexOf("<img ", StringComparison.CurrentCultureIgnoreCase) > -1)
            {
                XmlDocument content = jlib.net.sgmlparser.SgmlReader.getXmlDoc(section.Text);
                XmlNodeList images = content.SelectNodes("//img");

                foreach (XmlNode image in images)
                {
                    string ImageUrl = xml.getXmlAttributeValue(image, "src").Str();
                    if (ImageUrl.StartsWith("data:"))
                    {
                        string ImageAlt = xml.getXmlAttributeValue(image, "alt").SafeSplit("|", 0);
                        string Extension = parse.inner_substring(ImageUrl, "data:", null, ",", null);
                        Extension = parse.inner_substring(Extension, null, null, ";", null);
                        string FileUrl = Util.Cms.SaveFile(ExamContainer, ExamContainer, ExamContainer, Extension, System.Convert.FromBase64String(parse.inner_substring(ImageUrl, ",", null, null, null)), ImageAlt, false, "");
                        if (!FileUrl.IsNullOrEmpty()) xml.setXmlAttributeValue(image, "src", FileUrl);
                    }
                }
                section.Text = content.OuterXml;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsAjaxPostback)
            {
                lScript.Text += "var cEmptySection=" + jlib.functions.json.JsonMapper.ToJson(new Util.Exam.exam_section()) + ";\n"
                    + "CKEDITOR.basePath = '/learningportal/inc/library/ckeditor/';\n";
                if (bSave.IsClicked)
                {
                    jlib.functions.json.JsonData oJSONData = jlib.functions.json.JsonMapper.ToObject(Page.Request.Form["cExam_Data"]);
                    Util.Exam.exam m_oExam = jlib.functions.json.JsonMapper.ToObject<Util.Exam.exam>(oJSONData.ToJson());
                    m_oExam.validate();
                    if (ExamObject == null)
                    {
                        AttackData.Folder ParentFolder = AttackData.Folder.LoadByPk(this.QueryString("parent_id").Int());
                        ExamContainer = new AttackData.Folder()
                        {
                            Context_Value = "exam",
                            Name = m_oExam.Name
                        };
                        ExamContainer.Save();
                        ParentFolder.addChild(ExamContainer);
                        ParentFolder.Save();
                    }

                    m_oExam.Sections.ForEach(x => ExtractInlineImages(x));

                    m_oExam.save(DateTime.Now);
                    if (ExamObject == null)
                        ExamObject = AttackData.Exam.LoadByPk(m_oExam.ObjectID);

                    if (ExamContainer.Children.Count == 0)
                    {

                        ExamContainer.addChild(ExamObject);
                        ExamContainer.Save();

                        Response.Redirect("exam.aspx?ObjectID=" + ExamContainer.ObjectID);
                    }
                    else
                    {
                        ExamContainer.Name = m_oExam.Name;
                        ExamContainer.Save();
                    }
                    lScript.Text += "var cExam=" + jlib.functions.json.JsonMapper.ToJson(m_oExam);
                }
                else
                {
                    if (convert.cStr(Page.Request.Form["cExam_Data"]) != "")
                    {
                        lScript.Text += "var cExam=" + Page.Request.Form["cExam_Data"] + ";";
                    }
                    else
                    {
                        Util.Exam.exam m_oExam = new Util.Exam.exam();
                        m_oExam.populate(ExamObject == null ? 0 : ExamObject.ObjectID, (this.QueryString("revision") == "" ? DateTime.MaxValue : convert.cDate(this.QueryString("revision"))));
                        if (m_oExam.ObjectID == 0 && this.QueryString("context") == "assessment")
                        {
                            m_oExam.PlayerType = this.QueryString("context");
                        }
                        lScript.Text += "var cExam=" + jlib.functions.json.JsonMapper.ToJson(m_oExam);
                    }
                }
                if (this.validateForm())
                {
                    //if (m_oExam.Rows.Count == 0) {
                    //    m_oExam.Rows.Add(sqlbuilder.setRowValues(m_oExam.NewRow(), "object_id", inc_master_default.nextObjectID(), "create_date", DateTime.Now, "delete_date", DateTime.MaxValue));
                    //    bAdded = true;
                    //}
                    //this.collectValues("d_exams", m_oExam.Rows[0]);
                    //ado_helper.update(m_oExam);
                    //if (bAdded) Response.Redirect("exam.aspx?id=" + m_oExam.Rows[0]["object_id"] + "&revision=" + DateTime.MaxValue.AddSeconds(-1));
                }
            }
        }
    }
}