﻿//Need an editor that supports custom tags.
//http://www.voofie.com/content/2/ckeditor-plugin-development/

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Linq;
using System.Collections;
using System.Collections.Specialized;
using jlib.functions;
using jlib.net;
using jlib.db;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Cms
{
    public partial class Files : jlib.components.webpage
    {
        AttackData.Folder FolderObject;

        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Cache.SetNoStore();

            pTree.Visible = !Request["cms"].Bln();
            pFolderHeading.Visible = !pTree.Visible;
            //gSettingsGrid.GridBeforeDatabind += new jlib.controls.jgrid.OnGridBeforeDatabindHandler(gSettingsGrid_GridBeforeDatabind);
            //ado_helper oData = new ado_helper("sql.dsn.cms");
            (Page.Master as Inc.Master.Default).DisableWrapper = true;
            if (convert.isDate(Request["revision"])) System.Web.HttpContext.Current.Items["version.date"] = Request["revision"].Dte();
            FolderObject = AttackData.Folder.LoadByPk(convert.cInt(hFolderOverride.Value.SafeSplit(":", 1), Request["ObjectID"]));
            if (FolderObject == null)
            {
                FolderObject = new AttackData.Folder() { Context_Value = Request["context"] };
                FolderObject.addParent(AttackData.Folder.LoadByPk(Request["parent_id"].Int()));
            }
            if (FolderObject.Context_Value != "") dContext.Items.Add(FolderObject.Context_Value);

            if (Request["mode"] == "ajax-postback")
            {
                Response.ContentType = "text/javascript";
                if (tTitle.Text.Trim() == "")
                {
                    Response.Write("alert('You need to give this folder a title');");
                }
                else
                {
                    FolderObject.Name = tTitle.Text;
                    FolderObject.AuxField1 = tAuxField1.Text;
                    FolderObject.Context_Value = dContext.SelectedValue;
                    FolderObject.Save(Session["user.id"].Int());
                    if (Request["ObjectID"].Int() != FolderObject.ObjectID) Response.Write("window.location.href='files.aspx?" + this.QueryStringStripKeys("ObjectID") + "&ObjectID=" + FolderObject.ObjectID + "';");
                    else Response.Write("//ok");
                }
                Response.End();
            }

            if (!this.IsPostBack)
            {
                tTitle.Text = FolderObject.Name;
                tAuxField1.Text = FolderObject.AuxField1;
                dContext.setValue(FolderObject.Context_Value);
            }



            //RowLink Link = PageObject.Children.Find(x => x.Table2 == "d_content");
            //Content Content = (Link==null? null: Link.Object2 as Content);
            //if (Content != null) {
            //    if (!this.IsPostBack) {

            //        tContent.Text = Content.Xml;

            //        //if(tContent.Text=="" || tContent.Text.IndexOf("<")>-1)

            //    } else if (bSave.IsClicked) {
            //        Content.Xml = tContent.Text;
            //        Content.Save(Session["user.id"].Int());
            //    }
            //}
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {


            for (int x = 0; x < Request.Files.Count; x++)
            {
                HttpPostedFile File = Request.Files[x];
                string sFolder = System.Configuration.ConfigurationManager.AppSettings["cms.upload.path"] + "\\uploads\\"; //parse.replaceAll(Request.ApplicationPath + "/images/uploads/","//","/");
                string sFileName = io.getUniqueFileName(sFolder + File.FileName);
                File.SaveAs(sFileName);
                AttackData.Content FileObject = new AttackData.Content() { Type = (int)AttackData.Contents.Types.FileUrl };
                FileObject.Filename = System.Web.HttpContext.Current.Request.ApplicationPath + "/images/uploads/" + System.IO.Path.GetFileName(sFileName); //this should be a relative path)
                FileObject.Name = System.IO.Path.GetFileName(File.FileName);
                FileObject.setSetting("Size", File.ContentLength.Str());
                FileObject.addParent(FolderObject);
                FileObject.Save(Session["user.id"].Int());
            }

            List<object> Files = new List<object>();

            FolderObject.GetChildren<AttackData.Folder>().Where(
                Folder => Folder.Name.IndexOf(parse.stripCharacter(tFilenameFilter.Text.Str(), "*"), StringComparison.CurrentCultureIgnoreCase) > -1
            ).OrderBy(
                Folder => Folder.Name
            ).ToList().ForEach(Folder =>
            {
                Files.Add(new string[] { Folder.Name, "", "0", Folder.CreateDate.Str(), (Folder.CreatedBy == null ? "" : Folder.CreatedBy.Username), "d_folders:" + Folder.ObjectID, Folder.Context_Value });
            });

            FolderObject.GetChildren<AttackData.Content>().Where(
                Content => Content.Name.IndexOf(parse.stripCharacter(tFilenameFilter.Text.Str(), "*"), StringComparison.CurrentCultureIgnoreCase) > -1
            ).OrderBy(
                Content => Content.Name
            ).ToList().ForEach(Content =>
            {
                Files.Add(new string[] { Content.Name, Content.Filename, Content.getSetting("Size"), Content.CreateDate.Str(), (Content.CreatedBy == null ? "" : Content.CreatedBy.Username), "d_content:" + Content.ObjectID });
            });

            dynamic JSON = new jlib.functions.json.DynamicJson();
            JSON.Files = Files;
            if (Request["__mode"] == "ajax" || Request.Files.Count > 0)
            {
                Response.Clear();
                if (Request["__mode"] == "ajax")
                {
                    Response.ContentType = "text/javascript";
                    Response.Write("fnPopulateFiles(" + JSON.ToString() + ");");
                }
                else
                {
                    Response.Write(JSON.ToString());
                }
                Response.End();
            }

            lScript.Text = "$(document.body).data('__data', $.extend(" + JSON.ToString() + ",{\"Folder\":" + FolderObject.ToJson() + "}));$(document.body).data('__data').Folder.Parent=" + (FolderObject.Parents.Count == 0 || FolderObject.Parents[0].ID1 == 0 ? "null" : FolderObject.Parents[0].Object1.ToJson()) + ";";
            pCKButtonContainer.Visible = Request["CKEditorFuncNum"].Str() != "";
        }
    }

}