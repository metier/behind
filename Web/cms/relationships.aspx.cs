﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using jlib.functions;
using jlib.net;
using jlib.db;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Cms
{

    public partial class Relationships : jlib.components.webpage
    {
        private DateTime m_oRevisionDate = DateTime.MaxValue;

        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            ado_helper oData = new ado_helper("sql.dsn.cms");
            (Page.Master as Inc.Master.Default).DisableWrapper = true;

            if (convert.isDate(Request["revision"])) System.Web.HttpContext.Current.Items["version.date"] = Request["revision"].Dte();
            System.Web.HttpContext.Current.Items["versioning"] = true;


            AttackData.Folder PageObject = AttackData.Folder.LoadByPk(Request["ObjectID"].Int());
            if (PageObject == null)
            {
                PageObject = new AttackData.Folder() { Context_Value = "content" };
                PageObject.addParent(AttackData.Folder.LoadByPk(Request["parent_id"].Int()));
            }

            dynamic JSON = new jlib.functions.json.DynamicJson();
            List<object> Parents = new List<object>(), Children = new List<object>();
            PageObject.Parents.ForEach(x =>
            {
                Parents.Add(new { ObjectID = x.Table1 + ":" + x._CurrentObjectID, Name = x.Fields.SafeGetValue("Name"), Path = x.Object1.getPath().Path, PathID = x.Object1.getPath().ID, PathContext = x.Object1.getPath().Context });
            });
            PageObject.Children.ForEach(x =>
            {
                Children.Add(new { ObjectID = x.Table1 + ":" + x._CurrentObjectID, Name = x.Fields.SafeGetValue("Name"), Path = x.Object1.getPath().Path, PathID = x.Object1.getPath().ID, PathContext = x.Object1.getPath().Context });
            });

            JSON.Parents = Parents;
            JSON.Children = Children;
            lScript.Text += "$(document.body).data('__relationships', " + JSON.ToString() + ");";
            lScript.Text += "$(document.body).data('__data', {\"Folder\":" + PageObject.ToJson() + "});$(document.body).data('__data').Folder.Parent=" + (PageObject.Parents.Count == 0 || PageObject.Parents[0].ID1 == 0 ? "null" : PageObject.Parents[0].Object1.ToJson()) + ";";


            //if (!this.IsPostBack) {
            //    tName.Text = PageObject.Name;
            //    dTemplate.setValue(PageObject.AuxField1, true);
            //    //if (PageObject.AuxField1 == "quiz") Content.Xml = parse.replaceAll(Content.Xml, "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>", "");
            //    //tContent.Text = parse.stripWhiteChars(Content.Xml);


            //    //if(tContent.Text=="" || tContent.Text.IndexOf("<")>-1)

            //} else if (bSave.IsClicked) {

            //    PageObject.Name = tName.Text;
            //    PageObject.AuxField1 = dTemplate.SelectedValue;
            //    PageObject.Save(Session["user.id"].Int());
            //    if (Request["ObjectID"].Int() != PageObject.ObjectID) Response.Redirect("edit.aspx?" + this.QueryStringStripKeys("ObjectID") + "&ObjectID=" + PageObject.ObjectID);
            //}

        }

    }
}