﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using jlib.functions;
using jlib.net;
using jlib.db;
using System.Text;
using System.Globalization;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Cms
{
    public partial class Import : jlib.components.webpage
    {
        private string GetNodeText(List<XmlNode> TextNodes, ref int x)
        {
            string Text = TextNodes[x].InnerText;
            while (x < TextNodes.Count - 1 && TextNodes[x].ParentNode.ParentNode == TextNodes[x + 1].ParentNode.ParentNode)
            {
                Text += " " + TextNodes[++x].InnerText;
                XmlNode xmlDiv = TextNodes[x].ParentNode.ParentNode;
                if (xmlDiv.ParentNode != null) xmlDiv.ParentNode.RemoveChild(xmlDiv);
            }
            return Text;
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //string s = "szczeg[polsk-liten-o]łowy";

            //Response.Write(s + "<br />" + string.Concat(s.Normalize(NormalizationForm.FormD).Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark))
            //    + "<br />" + Encoding.ASCII.GetString(Encoding.Unicode.GetBytes(s)));


            //string unicodeString = s;// "This string contains the unicode character Pi(\u03a0)";

            //// Create two different encodings.
            //Encoding ascii = Encoding.ASCII;
            //Encoding unicode = Encoding.Unicode;

            //// Convert the string into a byte[].
            //byte[] unicodeBytes = unicode.GetBytes(unicodeString);

            //// Perform the conversion from one encoding to the other.
            //byte[] asciiBytes = Encoding.Convert(unicode, ascii, unicodeBytes);

            //// Convert the new byte[] into a char[] and then into a string.
            //// This is a slightly different approach to converting to illustrate
            //// the use of GetCharCount/GetChars.
            //char[] asciiChars = new char[ascii.GetCharCount(asciiBytes, 0, asciiBytes.Length)];
            //ascii.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
            //string asciiString = new string(asciiChars);

            //// Display the strings created before and after the conversion.
            //Console.WriteLine("Original string: {0}", unicodeString);
            //Console.WriteLine("Ascii converted string: {0}", asciiString);

            //Response.End();
            ado_helper oData = new ado_helper("sql.dsn.cms");
            if (bDownloadQuizAttachments.IsClicked)
            {
                Dictionary<string, string> attachmentMapping = new Dictionary<string, string>();
                DataTable contents = sqlbuilder.getDataTable(oData, "select  * from d_content where xml like '%https://mymetier.net/content/public/oilcourses/%'");
                foreach (DataRow content in contents.Rows)
                {
                    string[] arr = parse.split(content["xml"], "https://mymetier.net/content/public/oilcourses/", true, true);
                    for (int x = 1; x < arr.Length; x++)
                    {
                        string separator = arr[x - 1].Substr(-1);
                        string url = arr[x].Substring(0, arr[x].IndexOf(separator));
                        HTTP http = new HTTP();
                        if (!attachmentMapping.ContainsKey(url.ToLower()))
                        {
                            string sFolder = parse.replaceAll(System.Configuration.ConfigurationManager.AppSettings["cms.upload.path"] + "\\OldCourseImages\\", "\\\\", "\\");
                            string filename = io.getUniqueFileName(sFolder + "\\" + System.IO.Path.GetFileName(url));

                            if (http.SaveBinary(parse.replaceAll(url, "https://mymetier.net/", "https://metierdemo.net/"), filename, null))
                            {
                                attachmentMapping[url.ToLower()] = System.Configuration.ConfigurationManager.AppSettings["site.url"] + "/images/OldCourseImages/" + System.IO.Path.GetFileName(filename);
                            }
                        }
                        if (attachmentMapping.ContainsKey(url.ToLower())) arr[x] = parse.replaceAll(arr[x], url, attachmentMapping[url.ToLower()]);
                    }
                    content["xml"] = arr.Join("");
                }
                ado_helper.update(contents);
                Response.Redirect("import.aspx?");
            }
            else if (bFixQuiz.IsClicked)
            {
                System.Web.HttpContext.Current.Items["versioning"] = false;
                AttackData.Contents quizes = AttackData.Contents.GetByField(Type: 2).Execute();
                quizes.ForEach(x =>
                {
                    if (!x.Xml.IsNullOrEmpty())
                    {
                        AttackData.Folder course = x.GetParentUntil<AttackData.Folder>(10, new Func<AttackData.Folder, bool>(item => item.Context_Value == "course"));
                        string originalXml = x.Xml;
                        string newXml = Util.Cms.FixImportedQuiz(course, originalXml);

                        if (originalXml != newXml)
                        {
                            x.Xml = newXml;
                            x.Save(11536);
                        }
                    }
                });

            }
            else if (bImportP2P.IsClicked)
            {
                string Data = io.read_file("c:\\a\\SE - PRINCE2 Practitioner Sample Paper EX02 - August 2012 - Swedish1.html", System.Text.Encoding.UTF8);
                string CSS = parse.inner_substring(Data, "<style type=\"text/css\">", "<!--", "--!>", null);
                XmlDocument Test = jlib.net.sgmlparser.SgmlReader.getXmlDoc(Data);
                //XmlNodeList TextNodes = Test.SelectNodes("//div/span/text()");
                List<XmlNode> TextNodes = Test.SelectNodes("//div/span/text()").Cast<XmlNode>().ToList();
                XmlNode LastTable = null;
                for (int x = 0; x < TextNodes.Count; x++)
                {
                    XmlNode xmlDiv = TextNodes[x].ParentNode.ParentNode;
                    bool TableMode = (x == 0 ? false : Math.Abs(parse.inner_substring(xml.getXmlAttributeValue(xmlDiv, "style"), "top:", null, "px", null).Dbl() - parse.inner_substring(xml.getXmlAttributeValue(TextNodes[x - 1].ParentNode.ParentNode, "style"), "top:", null, "px", null).Dbl()) < 5)
                        || (x == TextNodes.Count - 1 ? false : Math.Abs(parse.inner_substring(xml.getXmlAttributeValue(xmlDiv, "style"), "top:", null, "px", null).Dbl() - parse.inner_substring(xml.getXmlAttributeValue(TextNodes[x + 1].ParentNode.ParentNode, "style"), "top:", null, "px", null).Dbl()) < 5);
                    if (TableMode)
                    {
                        if (LastTable == null)
                        {
                            LastTable = xml.setXmlAttributeValue(Test.SelectSingleNode("html/body").AppendChild(Test.CreateElement("table")), "style", "border:1px solid black", "border", 1);

                            xml.addXmlElement(LastTable, "tr");
                        }
                        else if (Math.Abs(parse.inner_substring(xml.getXmlAttributeValue(xmlDiv, "style"), "top:", null, "px", null).Dbl() - parse.inner_substring(xml.getXmlAttributeValue(TextNodes[x - 1].ParentNode.ParentNode, "style"), "top:", null, "px", null).Dbl()) > 5)
                        {
                            xml.addXmlElement(LastTable, "tr");
                        }
                        string Text = GetNodeText(TextNodes, ref x);


                        XmlNode Sibling = xmlDiv.NextSibling;
                        while (Sibling != null)
                        {
                            if (parse.inner_substring(xml.getXmlAttributeValue(Sibling, "style"), "left:", null, "px", null).Dbl() == parse.inner_substring(xml.getXmlAttributeValue(xmlDiv, "style"), "left:", null, "px", null).Dbl() && parse.inner_substring(xml.getXmlAttributeValue(Sibling, "style"), "top:", null, "px", null).Dbl() - parse.inner_substring(xml.getXmlAttributeValue(xmlDiv, "style"), "top:", null, "px", null).Dbl() < 15)
                            {
                                XmlNodeList Nodes = Sibling.SelectNodes("span/text()");
                                foreach (XmlNode Node in Nodes)
                                {
                                    Text += " " + Node.InnerText;
                                    TextNodes.Remove(Node);
                                }
                                xmlDiv = Sibling;
                            }
                            Sibling = Sibling.NextSibling;
                        }




                        xml.addXmlElement(LastTable.SelectSingleNode("tr[last()]"), "td", Text);
                    }
                    else
                    {
                        LastTable = null;
                    }
                    if (xmlDiv.ParentNode != null) xmlDiv.ParentNode.RemoveChild(xmlDiv);
                }
                Response.Write(Test.OuterXml);
                Response.End();
            }

            System.Web.HttpContext.Current.Items["versioning"] = false;
            //jlib.helpers.structures.collection_aux oLanguages = new jlib.helpers.structures.collection_aux();
            //oLanguages.Add(1,"NO");
            //oLanguages.Add(2,"EN");
            //oLanguages.Add(3,"FR");
            //oLanguages.Add(4,"NL");
            //oLanguages.Add(5,"DE");
            //oLanguages.Add(6,"DA");
            //oLanguages.Add(7,"SE");
            //oLanguages.Add(8,"ES");
            //oLanguages.Add(9,"PL");
            //oLanguages.Add(10,"PTBR");       


            //if (bClearDatabase.IsClicked || bImportData.IsClicked) oData.Execute_SQL(@"truncate table d_folders; truncate table d_settings; truncate table l_row_links; truncate table d_revisions; truncate table d_releases; truncate table d_content;");//truncate table d_lessons; truncate table d_courses;
            //        if (bImportData.IsClicked || bClearDatabase.IsClicked) {
            //            oData.Execute_SQL(@"delete from d_folders where objectid>125;
            //delete from d_settings where objectid>125;
            //delete from d_content where objectid>125;
            //delete from l_row_links where id1>125 or id2>125;");
            //        }
            if (bImportData.IsClicked)
            {

                System.IO.DirectoryInfo oDir = new System.IO.DirectoryInfo(System.Configuration.ConfigurationManager.AppSettings["course.automake.path"] + "\\oilcourses\\");
                System.IO.FileInfo[] oFiles = oDir.GetFiles(tXmlName.Text); // oDir.GetFiles("pm-0203en.xml"); // oDir.GetFiles("*.xml");  //oDir.GetFiles("pm-0202dk-teliasonera.xml");  //oDir.GetFiles("*.xml");  // oDir.GetFiles("pm-0202dk*.xml"); //pm-0203en.xml
                if (oFiles.Length == 0)
                {
                    lCourseList.Text = "No XML-files found in '" + System.Configuration.ConfigurationManager.AppSettings["course.automake.path"] + "\\oilcourses\\'";
                }
                else
                {
                    for (int x = 0; x < oFiles.Length; x++)
                    {
                        int iCourseID = tExistingCourseID.Text.Int();
                        if (tExistingCourseID.Text == "")
                        {
                            DataTable CourseID = (new AttackData.Setting()).Context.GetSqlHelper().Execute("select * from d_settings where tablename=@table and value like @path and deletedate>getdate()", "@table", "d_folders", "@path", "%pma\\oilcourses\\" + oFiles[x].Name);
                            iCourseID = CourseID.SafeGetValue(0, "ObjectID").Int();
                        }
                        importCourseFromXml(oFiles[x].FullName, AttackData.Folder.LoadByPk(iCourseID));
                    }
                }

            }
            else if (bClearEmptyPs.IsClicked)
            {
                AttackData.Contents Contents = AttackData.Contents.GetAll().Execute();// .GetByField(ObjectID: 25161).Execute();
                Contents.ForEach(Content =>
                {
                    Content.Xml = stripStartAndEndPTags(Content.Xml);
                });
                Contents.Save();
                //} else if (bParseCase.IsClicked) {
                //    System.Web.HttpContext.Current.Items["versioning"] = false;
                //    AttackData.Folders Folders = AttackData.Folders.GetByField(AuxField1:"case").Execute();
                //    Folders.ForEach(Folder => {
                //        AttackData.Content Content = Folder.Children.FirstOrDefault(x => x.Table2 == "d_content").Object2 as AttackData.Content;

                //        if (Content != null){// && Content.Xml.Trim()!="") {
                //            cleanContent(Content,io.read_file(System.Configuration.ConfigurationManager.AppSettings["course.automake.path"] + "\\lessons\\" + Content.getSetting("original.path"), System.Text.Encoding.Default), Content.Parents[0].Object1 as AttackData.Folder, null);


                //        }
                //    });
            }
            else if (bParseReflection.IsClicked)
            {
                string sErrorList = "";
                System.Web.HttpContext.Current.Items["versioning"] = true;
                //System.Web.HttpContext.Current.Items["version.date"] = DateTime.Now.AddDays(-1);
                //AttackData.Folders Folders = AttackData.Folders.GetByField(AuxField1:"theory").Execute();
                AttackData.Folder Course = AttackData.Folder.LoadByPk(tExistingCourseID.Text.Int());
                Course.GetChildren<AttackData.Folder>().ForEach(Lesson =>
                {
                    List<AttackData.Folder> Folders = Lesson.GetCMSPages("theory");
                    //AttackData.Folders.GetByField(AuxField1: "theory", ObjectID: 35702).Execute();
                    Folders.ForEach(Folder =>
                    {
                        if (Folder.TNumparents > 0) sErrorList += ParseReflection(Folder);
                    });
                });
                if (sErrorList != "")
                {
                    Response.Write(sErrorList);
                    Response.End();
                }
            }
            if (lErrorLabel.Text != "") lErrorLabel.Visible = true;
        }



        private void importCourseFromXml(string FileName, AttackData.Folder Course)
        {
            XmlDocument oDoc = new XmlDocument();
            oDoc.Load(FileName);
            AttackData.Folder Parent;
            if (Course == null)
            {
                Parent = AttackData.Folder.LoadByPk(ObjectID: 1);
                Course = new AttackData.Folder();
                Course.addParent(Parent);
            }
            else
            {
                Parent = Course.GetParents<AttackData.Folder>()[0];
            }
            Course.Context_Value = "course";
            Course.Name = xml.getXmlNodeValue(oDoc, "makefile/general/title");
            Course.AuxField1 = xml.getXmlNodeValue(oDoc, "makefile/general/coursecode");
            Course.AuxField2 = Course.AuxField1.Substr(-2, 2);

            string[] sSettings = new string[] { "//destdir", "//glossaryURL", "//templatedir", "//points/max_total", "//points/max_normal", "//points/max_final", "//points/unlock_final", "//points/silver", "//points/gold", "//points/questions_final", "//endquiz/random", "//endquiz/questions", "//endquiz/idstart", "//endquiz/dir" };
            for (int y = 0; y < sSettings.Length; y++)
                Course.setSetting(parse.stripLeadingCharacter(sSettings[y], "/"), xml.getXmlNodeTextValue(oDoc, sSettings[y]));

            Course.setSetting("MakeFile", FileName);

            XmlNodeList xmlLessons = oDoc.SelectNodes("//structure/lesson");
            for (int y = 0; y < xmlLessons.Count; y++)
            {
                AttackData.Folder Lesson = importLessonFromXml(xmlLessons[y], oDoc, y, Course, Course);
                XmlNodeList xmlSubLessons = xmlLessons[y].SelectNodes("lesson");
                for (int z = 0; z < xmlSubLessons.Count; z++)
                {
                    importLessonFromXml(xmlSubLessons[z], oDoc, z, Course, Lesson);
                }
            }

            //Import final tests
            string sDestDir = Course.getSetting("destdir");
            if (sDestDir != "") sDestDir = System.Configuration.ConfigurationManager.AppSettings["course.automake.path"] + "\\" + sDestDir + "\\oppgaver\\";
            if (sDestDir != "" && System.IO.Directory.Exists(sDestDir))
            {
                System.IO.FileInfo[] FinalTests = new System.IO.DirectoryInfo(sDestDir).GetFiles("quiz_100*.xml");
                if (FinalTests.Length > 0)
                {

                    AttackData.Folder Lesson = Course.GetChildren<AttackData.Folder>().FirstOrDefault(x => x.AuxField1 == "finaltest");
                    //.GetCMSPage("finaltest");
                    if (Lesson == null)
                    {
                        Lesson = new AttackData.Folder()
                        {
                            Name = "Final Test",
                            Context_Value = "lesson",
                            AuxField1 = "finaltest"
                        };
                    }

                    for (int y = 0; y < FinalTests.Length; y++)
                    {
                        if (Lesson.GetChildren<AttackData.Folder>().Count < y)
                        {
                            System.IO.FileInfo FinalTest = FinalTests[y];
                            //foreach (System.IO.FileInfo FinalTest in FinalTests) {
                            AttackData.Folder Page = new AttackData.Folder()
                            {
                                Context_Value = "content",
                                Name = FinalTest.Name,
                                AuxField1 = "quiz"
                            };
                            Lesson.addChild(Page);

                            AttackData.Content Content = new AttackData.Content()
                            {
                                Filename = "oppgaver\\" + FinalTest.Name,
                                Name = "Finaltest",
                                Type = 2,
                                Xml = Util.Cms.FixImportedQuiz(Course, io.read_file(FinalTest.FullName, System.Text.Encoding.Default))
                            };

                            Content.Save();
                            Page.addChild(Content);
                            Page.setSetting("_Url", sDestDir + "\\" + Content.Filename);

                            Page.Save();
                        }
                    }
                    Lesson.Save();
                    Course.addChild(Lesson);
                    Course.Save();
                }
            }

            Course.Save();
        }

        private AttackData.Folder importLessonFromXml(XmlNode xmlLesson, XmlDocument xmlDoc, int Index, AttackData.Folder Course, AttackData.Folder ParentFolder)
        {
            AttackData.Folder Lesson = ParentFolder.GetChildren<AttackData.Folder>().FirstOrDefault(x => x.getSetting("_LessonNumber") == xml.getXmlAttributeValue(xmlLesson, "seq"));
            if (Lesson == null)
            {
                Lesson = new AttackData.Folder();
                ParentFolder.addChild(Lesson);
            }
            Lesson.Name = xml.getXmlAttributeValue(xmlLesson, "name");
            Lesson.Context_Value = "lesson";
            Lesson.setSetting("_LessonNumber", xml.getXmlAttributeValue(xmlLesson, "seq"));

            string[] sSettings = new string[] { "scored" };//, "weight"
            for (int z = 0; z < sSettings.Length; z++)
                Lesson.setSetting(sSettings[z], xml.getXmlAttributeValue(xmlLesson, sSettings[z]));

            Lesson.Save();
            ParentFolder.Save();

            XmlNodeList xmlObjects = xmlDoc.SelectNodes("//file[@l_id='" + xml.getXmlAttributeValue(xmlLesson, "id") + "']");
            for (int z = 0; z < xmlObjects.Count; z++)
            {
                if (xml.getXmlAttributeValue(xmlObjects[z], "template") == "t_startlesson") continue;
                importLessonPageFromXml(xmlObjects[z], xmlDoc, Course, Lesson);
            }

            xmlObjects = xmlDoc.SelectNodes("//resources/resource[@type='quiz'][@id='" + parse.replaceAll(xml.getXmlAttributeValue(xmlLesson, "id"), "l_", "r_") + "_q']");
            if (xmlObjects.Count > 0)
            {
                importQuiz(xmlObjects[0], Course, Lesson, Lesson.GetCMSPage("quiz"));
            }

            //Add Splash
            AttackData.Folder Page = new AttackData.Folder();
            if (Lesson.GetCMSPage("splash") == null)
            {
                Page.Name = "Forside";
                Page.AuxField1 = "splash";
                Page.Context_Value = "content";
                Page.Save();
                Lesson.addChild(Page, 0);
            }
            //Add Completed-page
            if (Lesson.GetCMSPage("complete") == null)
            {
                Page = new AttackData.Folder();
                Page.Name = "Fullfør leksjon";
                Page.AuxField1 = "complete";
                Page.Context_Value = "content";
                Page.Save();
                Lesson.addChild(Page);
                Lesson.Save();
            }

            List<AttackData.Folder> Folders = Lesson.GetCMSPages("theory");
            Folders.ForEach(Folder =>
            {
                ParseReflection(Folder);
            });

            return Lesson;
        }

        private AttackData.Folder importLessonPageFromXml(XmlNode xmlLessonPage, XmlDocument xmlDoc, AttackData.Folder Course, AttackData.Folder Lesson)
        {
            XmlNode xmlFile = xmlLessonPage.SelectSingleNode("resource[@r_id]");
            if (xmlFile != null) xmlFile = xmlDoc.SelectSingleNode("//resources/resource[@id='" + xml.getXmlAttributeValue(xmlFile, "r_id") + "']");

            string sURL = xml.getXmlAttributeValue(xmlLessonPage, "dest");
            DataTable ExistingPageObject = Course.Context.GetSqlHelper().ExecuteSql("select d_settings.objectid from d_settings, l_row_links where d_settings.tableName='d_folders' and d_settings.KeyName='_Url' and d_settings.value like @url and l_row_links.id2=d_settings.objectid and l_row_links.table2='d_folders' and l_row_links.table1='d_folders' and l_row_links.id1=@id", "@id", Lesson.ObjectID, "@url", sURL);

            AttackData.Folder Page = null;
            if (ExistingPageObject.Rows.Count > 0)
                Page = AttackData.Folder.LoadByPk(ExistingPageObject.Rows[0]["objectid"].Int());
            if (Page == null) Page = new AttackData.Folder();
            //Pages not supported in new design> eksempler, referanser
            Page.AuxField1 = (sURL.Contains("case") ? "case" : (sURL.Contains("eksempler") ? "examples" : (sURL.Contains("fallgruver") ? "pitfalls" : (sURL.Contains("innledning") ? "intro" : (sURL.Contains("maal") ? "goals" : (sURL.Contains("referanser") ? "global" : (sURL.Contains("sjekkliste") ? "checklist" : (sURL.Contains("tanke") ? "reflection" : (sURL.Contains("teori") ? "theory" : (sURL.Contains("quiz") ? "quiz" : ""))))))))));

            Page.setSetting("_Url", sURL);
            string sNavigation = Lesson.getSetting("_Pages");
            string sPageName = convert.cStr(parse.inner_substring(sNavigation, sURL.SafeSplit("\\", 1), ":", "[", "]", null), convert.cCapitalizedStr(sURL.SafeSplit("\\", 1).Replace(".htm", "")));
            string[] sArr = parse.split(sNavigation, "url:");
            int iPageIndex = -1;
            for (int iCounter = 0; iCounter < sArr.Length; iCounter++) if (sArr[iCounter].IndexOf(sPageName, StringComparison.CurrentCultureIgnoreCase) > -1) iPageIndex = iCounter - 1;

            Page.Name = parse.replaceAll(sPageName, "Practical Examples", "Examples");
            Page.Context_Value = "content";

            AttackData.Setting ContentLocation = null;
            //Link to existing content?
            if (xmlFile != null)
            {
                ContentLocation = AttackData.Settings.GetByField(TableName: "d_content", KeyName: "original.path", Value: xml.getXmlAttributeValue(xmlFile, "src")).Execute().FirstOrDefault();
                if (ContentLocation != null && ContentLocation.Object == null) ContentLocation = null;
            }

            //if (ContentLocation == null) {
            string sData = io.read_file(System.Configuration.ConfigurationManager.AppSettings["course.automake.path"] + "/" + xml.getXmlNodeTextValue(xmlDoc, "//templatedir") + "/" + xml.getXmlAttributeValue(xmlDoc.SelectSingleNode("//resources/resource[@type='template'][@id='" + xml.getXmlAttributeValue(xmlLessonPage, "template") + "']"), "src"), System.Text.Encoding.Default).Trim();
            if (!sData.IsNullOrEmpty())
            {
                bool importContent = (cOverwriteExistingContent.Checked || ContentLocation == null || (ContentLocation.Object as AttackData.Content).Xml.Str().Trim() == "");
                //if this content is not linked to from an active course
                if (!importContent && cOverwriteExistingContentNonLinked.Checked && ContentLocation != null)
                {
                    AttackData.Content Content = ContentLocation.Object as AttackData.Content;
                    List<AttackData.Folder> activeParents = Content.GetParents<AttackData.Folder>().Where(x =>
                    {
                        List<AttackData.Folder> allParents = x.GetAllParents<AttackData.Folder>(7).ToList();
                        bool isCurrentCourse = allParents.FirstOrDefault(y => { return y.ObjectID == Course.ObjectID; }) != null;
                        bool isDeletedCourse = allParents.FirstOrDefault(y => { return y.ObjectID == 1; }) == null;
                        bool isObsoleteCourse = allParents.FirstOrDefault(y => { return y.ObjectID == 95159; }) == null;
                        if (isCurrentCourse || isDeletedCourse || isObsoleteCourse) return false;
                        return true;
                    }).ToList();
                    if (activeParents.Count == 0) importContent = true;

                }
                if (importContent)
                {
                    AttackData.Content Content = new AttackData.Content();
                    if (ContentLocation != null) Content = ContentLocation.Object as AttackData.Content;
                    Content.setSetting("original.path", xml.getXmlAttributeValue(xmlFile, "src"));
                    Content.Type = 1;
                    Content.Name = xml.getXmlAttributeValue(xmlLessonPage, "dest");
                    Content.AuxField1 = Page.AuxField1;
                    Content.SourceID = 0;
                    Content.Filename = "https://mymetier.net/content/public/" + Course.getSetting("destdir") + "/" + parse.replaceAll(Page.getSetting("_Url"), "\\", "/");
                    XmlNodeList xmlResources = xmlLessonPage.SelectNodes("resource");
                    for (int i = 0; i < xmlResources.Count; i++)
                    {
                        string sReplacement = "";
                        if (xml.getXmlAttributeValue(xmlResources[i], "r_id") != "") sReplacement = io.read_file(System.Configuration.ConfigurationManager.AppSettings["course.automake.path"] + "\\lessons\\" + xml.getXmlAttributeValue(xmlDoc, "//resources/resource[@id='" + xml.getXmlAttributeValue(xmlResources[i], "r_id") + "']", "src"), System.Text.Encoding.Default);
                        else if (xml.getXmlAttributeValue(xmlResources[i], "type") == "globalvalue") sReplacement = xml.getXmlNodeTextValue(xmlDoc, xml.getXmlNodeTextValue(xmlResources[i]));
                        else if (xml.getXmlAttributeValue(xmlResources[i], "type") == "text") sReplacement = xml.getXmlNodeTextValue(xmlResources[i]);
                        else throw new Exception("Unrecognized replacement type: " + xml.getXmlAttributeValue(xmlResources[i], "type"));

                        if (xml.getXmlAttributeValue(xmlResources[i], "type") == "body" && sReplacement.IndexOf("<body", StringComparison.CurrentCultureIgnoreCase) > -1)
                        {
                            if (sReplacement.IndexOf("</body>", StringComparison.CurrentCultureIgnoreCase) == -1) sReplacement += "</body>";
                            sReplacement = parse.inner_substring(ref sReplacement, "<body", ">", "</body>", null, false, false, false);
                        }
                        sData = parse.replaceAll(sData, xml.getXmlAttributeValue(xmlResources[i], "match"), sReplacement);
                        if (xml.getXmlAttributeValue(xmlResources[i], "match") == "{{content-title}}") Content.Name = sReplacement;
                    }
                    cleanContent(Content, sData, Page, xmlDoc);


                    //oObject.Rows[0]["title"] = xml.getXmlNodeValue(xmlDoc1, "//" + sNameSpacePrefix + "title", oNS);                                    

                    List<string> oNav = new List<string>();
                    if (Content.Filename.IndexOf("lessonfs.htm", StringComparison.CurrentCultureIgnoreCase) > -1)
                    {
                        sNavigation = parse.replaceAll(parse.inner_substring(sData, "sPages", "=", ";", null), false, true, "\"", "", "\t", "", "\n", "", "\r", "", "+ § +", "\n", "+§+", "\n");
                        Lesson.setSetting("_LanguageID", parse.inner_substring(sData, "getMetierTrack().LanguageID", "=", ";", null));
                        Lesson.setSetting("_PrintFrames", parse.replaceAll(parse.inner_substring(sData, "sPrintFrames", "=", ";", null), "\"", ""));
                        Lesson.setSetting("_Pages", sNavigation);
                        Lesson.setSetting("_Control", parse.replaceAll(parse.inner_substring(sData, "sControl", "=", "\";", null), false, true, "\"", "", "\t", "", "\n", "", "\r", "", "+ § +", "\n", "+§+", "\n")); //{{control_links}} in <file section                                        
                        return Lesson;
                    }

                    string[] sSettings = new string[] { "template" }; //sSettings = new string[] { "template", "dest" };
                    for (int i = 0; i < sSettings.Length; i++)
                    {
                        Page.setSetting(sSettings[i], xml.getXmlAttributeValue(xmlLessonPage, sSettings[i]));
                        if (sSettings[i] == "template")
                        {
                            string sTemplateUrl = xml.getXmlNodeTextValue(xmlDoc, "//templatedir") + "/" + xml.getXmlAttributeValue(xmlDoc, "//resources/resource[@id='" + xml.getXmlAttributeValue(xmlLessonPage, sSettings[i]) + "']", "src");
                            Page.setSetting("template_url", sTemplateUrl);

                        }
                    }
                    Content.Save();
                    Page.addChild(Content);
                }
            }
            //}
            Lesson.addChild(Page, iPageIndex);
            if (ContentLocation != null) Page.addChild(ContentLocation.Object);

            Page.Save();
            return Lesson;
        }

        private AttackData.Content importQuiz(XmlNode xmlObject, AttackData.Folder Course, AttackData.Folder Lesson, AttackData.Folder Page)
        {
            if (Page == null)
            {
                Page = new AttackData.Folder();
                Page.Context_Value = "content";
                Page.Name = "Quiz";
                Page.AuxField1 = "quiz";
                Lesson.addChild(Page);
            }
            AttackData.Setting ContentLocation = AttackData.Settings.GetByField(TableName: "d_content", KeyName: "original.path", Value: xml.getXmlAttributeValue(xmlObject, "src")).Execute().FirstOrDefault();
            AttackData.Content Object = Page.GetChildren<AttackData.Content>().FirstOrDefault();
            if (Object == null && ContentLocation != null && ContentLocation.Object != null) Object = ContentLocation.Object as AttackData.Content;
            if (Object == null) Object = new AttackData.Content();

            Page.addChild(Object);
            //if (ContentLocation == null) {        
            if (Object.Xml.IsNullOrEmpty() || cOverwriteExistingContent.Checked)
            {
                Object.Type = 2;
                Object.Name = "Quiz";
                string url = System.Configuration.ConfigurationManager.AppSettings["course.automake.path"] + "/lessons/" + xml.getXmlAttributeValue(xmlObject, "src");
                string quiz = io.read_file(url, System.Text.Encoding.Default);
                if (quiz.Str().IndexOf("\"utf-8\"", StringComparison.CurrentCultureIgnoreCase) > -1) quiz = io.read_file(url, System.Text.Encoding.UTF8);
                Object.Xml = Util.Cms.FixImportedQuiz(Course, quiz);
                Object.Filename = xml.getXmlAttributeValue(xmlObject.OwnerDocument.SelectSingleNode("//copy/file[@r_id='" + xml.getXmlAttributeValue(xmlObject, "id") + "']"), "dest");
                Object.setSetting("original.path", xml.getXmlAttributeValue(xmlObject, "src"));
                Object.Save();
            }

            //}
            Page.Save();
            if (ContentLocation != null && ContentLocation.Object != null) ContentLocation.Object.addParent(Page);
            else Object.addParent(Page);

            if (Object.Xml.Str().Contains("connecttheboxes") || Object.Xml.Str().Contains("background="))
            {
                lErrorLabel.Text += "Quiz in lesson " + Lesson.Name + " contains the words 'connecttheboxes' or 'background'.<br />";
            }
            return Object;
        }

        private string stripStartAndEndPTags(string Data)
        {
            Data = Data.Str().Trim();
            if (Data.IndexOf("<p") > -1)
            {
                try
                {
                    XmlDocument xmlDoc = xml.loadXml("<data>" + Data + "</data>");
                    stripStartAndEndPTags(xmlDoc.SelectSingleNode("data"));

                    Data = xmlDoc.SelectSingleNode("data").InnerXml.Trim();
                }
                catch (Exception)
                {
                }
            }
            return Data;
        }

        private XmlNode stripStartAndEndPTags(XmlNode xmlParentNode)
        {
            if (xmlParentNode != null)
            {
                foreach (XmlNode xmlNode in xmlParentNode.SelectNodes(".//comment()")) xmlNode.ParentNode.RemoveChild(xmlNode);

                XmlNodeList xmlNodes = xmlParentNode.SelectNodes("./*");
                List<XmlNode> xmlNodeList = new List<XmlNode>();
                for (int x = 0; x < xmlNodes.Count; x++)
                {
                    if (xmlNodes[x].Name != "p") break;
                    xmlNodeList.Add(xmlNodes[x]);
                }
                for (int x = xmlNodes.Count - 1; x >= 0; x--)
                {
                    if (xmlNodes[x].Name != "p") break;
                    xmlNodeList.AddIfNotExist(xmlNodes[x]);
                }
                xmlNodeList.ForEach(x =>
                {
                    if (x.InnerXml.IndexOf("<img") == -1 && parse.stripWhiteChars(parse.replaceAll(x.InnerText, " ", "", "&nbsp;", "")) == "")
                    {
                        x.ParentNode.RemoveChild(x);
                    }
                });
            }
            return xmlParentNode;
        }

        //General method for scrubbing all content
        private void cleanContent(AttackData.Content Content, string sData, AttackData.Folder Page, XmlDocument oDoc)
        {
            if (Page.AuxField1 == "case") sData = parse.replaceAll(sData, "<td><span id=\"divAnswer1\"></span></td>", "<td><span id=\"divAnswer1\"></span>");
            sData = parse.replaceAll(sData, "[polsk-stor-o]", "&#211;", "[polsk-liten-o]", "&#243;");
            XmlDocument oDoc1 = jlib.net.sgmlparser.SgmlReader.getXmlDoc(sData);
            XmlNamespaceManager oNS = xml.createNameSpaceManager(oDoc1);

            string sNameSpacePrefix = (oNS.HasNamespace("default-namespace") ? "default-namespace:" : "");

            XmlNodeList xmlNodes = oDoc1.SelectNodes("//" + sNameSpacePrefix + "script | //" + sNameSpacePrefix + "textarea", oNS);
            foreach (XmlNode xmlNode in xmlNodes)
            {
                if (xmlNode.ParentNode.Name == "p" && xmlNode.ParentNode.ChildNodes.Count == 1)
                    xmlNode.ParentNode.ParentNode.RemoveChild(xmlNode.ParentNode);
                else
                    xmlNode.ParentNode.RemoveChild(xmlNode);
            }

            xmlNodes = oDoc1.SelectNodes("//" + sNameSpacePrefix + "*[@lang]", oNS);
            foreach (XmlNode xmlNode in xmlNodes) xmlNode.Attributes.Remove(xmlNode.Attributes["lang"]);

            if (Page.AuxField1 == "goals" || Page.AuxField1 == "checklist" || Page.AuxField1 == "pitfalls" || Page.AuxField1 == "reflection")
            {
                XmlNode xmlNode = oDoc1.SelectSingleNode("//" + sNameSpacePrefix + "ul", oNS);
                if (xmlNode == null) xmlNode = oDoc1.SelectSingleNode("//" + sNameSpacePrefix + "ol", oNS);
                if (xmlNode != null)
                {
                    Content.Xml = parse.replaceAll(xmlNode.OuterXml, " xmlns=\"http://www.w3.org/1999/xhtml\"", "", "ol>", "ul>");
                    xml.removeXmlNode(xmlNode);
                }
                if (Page.AuxField1 == "reflection") Content.Xml = xml.getXmlNodeValue(oDoc, "//txtThoughtDesc") + Content.Xml;
                else if (Page.AuxField1 == "goals")
                {
                    XmlNode xmlGoalPreText = oDoc1.SelectSingleNode("//" + sNameSpacePrefix + "*[@id='txtContent']", oNS);
                    if (xmlGoalPreText != null)
                    {
                        if (xmlGoalPreText.ChildNodes.Count == 1 && xmlGoalPreText.ChildNodes[0].Name == "p") xmlGoalPreText = xmlGoalPreText.ChildNodes[0];
                        Content.Xml = parse.replaceAll(xmlGoalPreText.InnerXml, " xmlns=\"http://www.w3.org/1999/xhtml\"") + Content.Xml;
                    }
                }
            }
            else
            {

                string[] sStyleCheck = new string[] { "border", "border-left", "border-right", "border-top", "border-bottom" };
                List<XmlNode> ExcludedTables = new List<XmlNode>();
                XmlNodeList xmlTables = oDoc1.SelectNodes("//" + sNameSpacePrefix + "table", oNS);
                foreach (XmlNode xmlTable in xmlTables)
                {
                    foreach (string sStyle in sStyleCheck)
                    {
                        string sValue = parse.inner_substring(xml.getXmlAttributeValue(xmlTable, "style") + ";", sStyle + ":", null, ";", null);
                        if (sValue != "" && ((sValue.Contains("px") && !sValue.Contains("0px")) || (sValue.Contains("pt") && !sValue.Contains("0pt"))))
                        {
                            ExcludedTables.Add(xmlTable);
                            break;
                        }
                    }
                    if (!ExcludedTables.Contains(xmlTable) && xml.getXmlAttributeValue(xmlTable, "border").Int() > 0) ExcludedTables.Add(xmlTable);
                    if (ExcludedTables.Contains(xmlTable)) continue;
                    xmlNodes = xmlTable.SelectNodes(sNameSpacePrefix + "tbody/" + sNameSpacePrefix + "tr/" + sNameSpacePrefix + "td", oNS);
                    if (xmlNodes.Count == 0) xmlNodes = xmlTable.SelectNodes(sNameSpacePrefix + "tr/" + sNameSpacePrefix + "td", oNS);
                    foreach (XmlNode xmlNodeTD in xmlNodes)
                    {
                        foreach (string sStyle in sStyleCheck)
                        {
                            string sValue = parse.inner_substring(xml.getXmlAttributeValue(xmlNodeTD, "style") + ";", sStyle + ":", null, ";", null);
                            if (sValue != "" && ((sValue.Contains("px") && !sValue.Contains("0px")) || (sValue.Contains("pt") && !sValue.Contains("0pt"))))
                            {
                                ExcludedTables.Add(xmlTable);
                                break;
                            }

                        }
                        if (ExcludedTables.Contains(xmlTable)) break;
                    }
                }

                xmlNodes = oDoc1.SelectNodes("//" + sNameSpacePrefix + "h1 | //" + sNameSpacePrefix + "h2 | //" + sNameSpacePrefix + "h3 | //" + sNameSpacePrefix + "h4", oNS);
                foreach (XmlNode xmlNode in xmlNodes) xmlNode.ParentNode.RemoveChild(xmlNode);

                xmlNodes = oDoc1.SelectNodes("//" + sNameSpacePrefix + "td", oNS);
                foreach (XmlNode xmlNode in xmlNodes)
                {
                    XmlNode xmlParent = xmlNode;
                    while (xmlParent.Name == "tr" || xmlParent.Name == "td" || xmlParent.Name == "th" || xmlParent.Name == "tbody") xmlParent = xmlParent.ParentNode;
                    if (ExcludedTables.Contains(xmlParent)) continue;
                    while (xmlNode.ChildNodes.Count > 0)
                        xmlParent.ParentNode.InsertBefore(xmlNode.ChildNodes[0], xmlParent);
                }

                foreach (XmlNode xmlNode in xmlTables)
                {
                    if (!ExcludedTables.Contains(xmlNode)) xmlNode.ParentNode.RemoveChild(xmlNode);
                }

                //Fix relative image links
                xmlNodes = oDoc1.SelectNodes("//" + sNameSpacePrefix + "img", oNS);
                foreach (XmlNode xmlNode in xmlNodes)
                {
                    if (xml.getXmlAttributeValue(xmlNode, "src").StartsWith(".."))
                    {
                        string sTempUrl = parse.stripEndingCharacter(Content.Filename.Substring(0, Content.Filename.LastIndexOf("/")), "/");
                        int iNumRelativeLevels = parse.split(xml.getXmlAttributeValue(xmlNode, "src"), "../").Length - 1;

                        for (int iCounter = 0; iCounter < iNumRelativeLevels && sTempUrl.IndexOf("/", 7) > -1; iCounter++)
                            sTempUrl = sTempUrl.Substring(0, sTempUrl.LastIndexOf("/"));

                        xml.setXmlAttributeValue(xmlNode, "src", sTempUrl + "/" + parse.stripLeadingCharacter(xml.getXmlAttributeValue(xmlNode, "src"), "/", ".."));
                    }
                    else if (xml.getXmlAttributeValue(xmlNode, "src").StartsWith("/"))
                    {
                        xml.setXmlAttributeValue(xmlNode, "src", Content.Filename.Substring(0, Content.Filename.LastIndexOf("/")) + "/" + xml.getXmlAttributeValue(xmlNode, "src"));
                    }
                    else if (xml.getXmlAttributeValue(xmlNode, "src").StartsWith("http://cdserver1.z37.no.tconet.net"))
                    {
                        xml.setXmlAttributeValue(xmlNode, "src", parse.replaceAll(xml.getXmlAttributeValue(xmlNode, "src"), "http://cdserver1.z37.no.tconet.net/pma/", "https://mymetier.net/content/public/"));
                    }

                }
                Content.Xml = parse.replaceAll(xml.getXmlNodeValue(oDoc1, "//" + sNameSpacePrefix + "body", oNS), " xmlns=\"http://www.w3.org/1999/xhtml\"", "", " class=\"MsoNormal\"", "", "https://mymetier.net/content/images/", "https://mymetier.net/content/public/images/");

                if (Page.AuxField1 == "case")
                {
                    XmlDocument xmlDoc = jlib.net.sgmlparser.SgmlReader.getXmlDoc("<data>" + Content.Xml + "</data>");
                    xmlNodes = xmlDoc.SelectNodes("//a[contains(@href, 'fnCaseToEval')] | //a[contains(@href, 'fnCaseToQuestions')]");
                    foreach (XmlNode xmlNode in xmlNodes) xmlNode.ParentNode.RemoveChild(xmlNode);
                    xmlNodes = xmlDoc.SelectNodes("//form | //div[@id='divWrong']");
                    foreach (XmlNode xmlNode in xmlNodes)
                    {
                        while (xmlNode.ChildNodes.Count > 0) xmlNode.ParentNode.InsertBefore(xmlNode.ChildNodes[0], xmlNode);
                        xmlNode.ParentNode.RemoveChild(xmlNode);
                    }
                    xmlNodes = xmlDoc.SelectNodes("//input");
                    foreach (XmlNode xmlNode in xmlNodes) xmlNode.ParentNode.InsertBefore(xmlDoc.CreateElement("br"), xmlNode);

                    XmlNode xmlNode1 = xmlDoc.SelectSingleNode("//div[@id='part1']");
                    if (xmlNode1 != null)
                    {
                        xmlNode1.Attributes.RemoveAll();
                        xml.setXmlAttributeValue(xmlNode1, "class", "case-question");
                        stripStartAndEndPTags(xmlNode1);
                    }
                    xmlNode1 = xmlDoc.SelectSingleNode("//div[@id='part2']");
                    if (xmlNode1 != null)
                    {
                        xmlNode1.Attributes.RemoveAll();
                        xml.setXmlAttributeValue(xmlNode1, "class", "case-answer");
                    }
                    for (int x = 0; x < 10; x++)
                    {
                        xmlNode1 = xmlDoc.SelectSingleNode("//*[@id='divAnswer" + x + "']");
                        if (xmlNode1 != null)
                        {
                            if (xmlNode1.PreviousSibling != null && xmlNode1.PreviousSibling.Name == "b") xmlNode1.ParentNode.RemoveChild(xmlNode1.PreviousSibling);
                            if (xmlNode1.PreviousSibling != null && xmlNode1.PreviousSibling.Name == "b") xmlNode1.ParentNode.RemoveChild(xmlNode1.PreviousSibling);
                            if (xmlNode1.PreviousSibling != null && xmlNode1.PreviousSibling.Name == "b") xmlNode1.ParentNode.RemoveChild(xmlNode1.PreviousSibling);
                            //if (xmlNode1.NextSibling != null && (xmlNode1.NextSibling.InnerText.Contains("Suggested solution") || xmlNode1.NextSibling.InnerText.Contains("Løsningsforslag"))) xmlNode1.ParentNode.RemoveChild(xmlNode1.NextSibling);
                            xmlNode1.ParentNode.RemoveChild(xmlNode1);
                        }
                    }
                    xmlNodes = xmlDoc.SelectNodes("//b[contains(text(),'Suggested solution')] | //b[contains(text(),'Løsningsforslag')] | //b[contains(text(),'Ditt svar')] | //b[contains(text(),'Your answer')] | //p[contains(text(),'Suggested solution')]");
                    for (int x = 0; x < xmlNodes.Count; x++)
                    {
                        if (xmlNodes[x].InnerText.Length < 22)
                        {
                            if (xmlNodes[x].ParentNode.Name == "p" && xmlNodes[x].ParentNode.InnerText == xmlNodes[x].InnerText) xmlNodes[x].ParentNode.ParentNode.RemoveChild(xmlNodes[x].ParentNode);
                            else xmlNodes[x].ParentNode.RemoveChild(xmlNodes[x]);
                        }
                    }
                    stripStartAndEndPTags(xmlDoc.SelectSingleNode("//div[@class='case-answer']"));

                    Content.Xml = stripStartAndEndPTags(xmlDoc.SelectSingleNode("html/data")).InnerXml.Trim();
                }
            }
            Content.Xml = parse.stripCharacter(Content.Xml, "\n", "\r", "\t", " ", "&nbsp;");
        }

        //Method to parse reflection-page from theory page.
        private string ParseReflection(AttackData.Folder TheoryPage)
        {
            AttackData.RowLink ContentLink = TheoryPage.Children.FirstOrDefault(x => x.Table2 == "d_content");
            AttackData.Content Content = (ContentLink == null ? null : ContentLink.Object2 as AttackData.Content);
            if (Content != null)
            {
                try
                {
                    XmlDocument xmlTheory = jlib.net.sgmlparser.SgmlReader.getXmlDoc(Content.Xml);
                    AttackData.Folder ReflectionPage = TheoryPage.GetCMSPage("reflection");
                    AttackData.Content ReflectionContent = ReflectionPage.Children.FirstOrDefault(x => x.Table2 == "d_content").Object2 as AttackData.Content;
                    XmlDocument xmlReflection = jlib.net.sgmlparser.SgmlReader.getXmlDoc(ReflectionContent.Xml);
                    XmlNodeList xmlList = xmlReflection.SelectNodes("//ul/li");

                    string sReflection = "";
                    XmlNodeList xmlTargets = xmlTheory.SelectNodes("//a[contains(@name,'target')]");
                    if (xmlTargets.Count > 0)
                    {
                        for (int x = 0; x < xmlTargets.Count; x++)
                        {
                            XmlNode xmlNode = xmlTargets[x];
                            xmlNode.ParentNode.InsertAfter(
                                xmlTheory.ImportNode(convert.cXmlNode("<div class=\"cms-question\" contenteditable=\"false\" id=\"question_" + x + "\"><div class=\"title\" contenteditable=\"true\">&#160;</div><div class=\"text\" contenteditable=\"true\">" + xml.getXmlNodeValue(xmlList[x]) + "</div></div>"), true)
                                , xmlNode);

                            xmlNode.ParentNode.RemoveChild(xmlNode);
                            sReflection += "<li target=\"question_" + x + "\">" + xml.getXmlNodeValue(xmlList[x]) + "</li>";
                        }

                        xmlReflection.SelectSingleNode("//ul").InnerXml = sReflection;

                        ReflectionContent.Xml = xmlReflection.SelectSingleNode("html").InnerXml;
                        ReflectionContent.Save(Session["user.id"].Int());
                        Content.Xml = xmlTheory.SelectSingleNode("html").InnerXml;
                        Content.Save(Session["user.id"].Int());
                    }
                }
                catch (Exception ex)
                {
                    return "<b>" + TheoryPage.getPath().Path + " (" + TheoryPage.getPath().ID + ")</b><br />" + ex.Message + "<br />" + parse.replaceAll(ex.StackTrace, "\n", "<br />") + "<br /><br />";
                }
            }
            return "";
        }
    }
}