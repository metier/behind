﻿<%@ Page Title="Metier Content Studio" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Cms.Edit" Codebehind="edit.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" DisableWrapper="true">
<link type="text/css" rel="stylesheet" href="../player/css/tempstyle.css?10" />
<link type="text/css" rel="stylesheet" href="../css/metier-common.css?4" />
    <link type="text/css" rel="stylesheet" href="gfx/cms.css" />
<div style="background-color:#ddd; border:1px solid silver; padding:20px; position:relative;" id="editor-container">
<div style="position:absolute;top:5px;right:5px; text-align:right"><a href="javascript:void(0)" id="parent-folder"><img src="../inc/images/icons/folder_up.png" style="position:relative;top:4px" /> Parent Folder</a>
<br /><br /><div id="path"></div><div id="modified"></div><div id="deleted" style="color:Red;"></div></div>
<div style="font-weight:bold; font-size:14px;">
<label style="width:75px;display:block;float:left">Title:</label><common:textbox runat="server" ID="tName" CssClass="text ajax-postback" Width="450px" style="float:left" /><br style="clear:left" />
<label style="width:75px;display:block;float:left">Icon type:</label>
    <common:dropdown runat="server" ID="dIconType" style="float:left" CssClass="ajax-postback">
        <asp:listitem value="splash">Splash</asp:listitem>
        <asp:listitem value="intro">Intro</asp:listitem>
        <asp:listitem value="case">Case</asp:listitem>
        <asp:listitem value="examples">Examples</asp:listitem>        
        <asp:listitem value="global">Global Page</asp:listitem>        
        <asp:listitem value="goals">Goals</asp:listitem>
        <asp:listitem value="checklist">Checklist</asp:listitem>
        <asp:listitem value="pitfalls">Pitfalls</asp:listitem>
        <asp:listitem value="reflection">Reflection</asp:listitem>
        <asp:listitem value="theory">Theory</asp:listitem>
        <asp:listitem value="quiz">Quiz</asp:listitem>
        <asp:listitem value="complete">Complete</asp:listitem>
        <asp:listitem value="dictionary">Dictionary</asp:listitem>        
        <asp:listitem value="disclaimer">Disclaimer</asp:listitem>
        </common:dropdown>    
    <br style="clear:left" />
    <a href="#" onclick="$(this).hide();$('#advanced-settings').show();">Show advanced settings</a>
    <div id="advanced-settings" style="display:none">
        <label style="width:120px;display:block;float:left">Player settings:</label>
        <common:textbox TextMode="MultiLine" runat="server" id="tPlayerSettings" style="float:left;height:50px;width:400px" CssClass="ajax-postback" />        
        <br style="clear:left" />
    </div>
    <div class="dynamic-control-container"></div> 
</div>
    <div class="dynamic-control-container" style="float:left;padding-left:50px"></div>
        <br style="clear:both" />
</div>
    <asp:PlaceHolder runat="server" ID="pBody">
    <table style="width:100%" id="content-section">
        <tr>
    <td class="_editor">
        <div id="main-tabs">
<ul>
    <li><a href="#tab-main-data">Section-details</a></li>
    <li><a href="#tab-window-2">Settings</a></li>
    <li><a href="#tab-window-3">Version History</a></li>
    <li id="tab-also-used-in"><a href="#tab-window-4">Also used in</a></li>    
    </ul>
    <div id="tab-main-data" class="tab-window">
    <label style="width:75px;display:block;float:left">Name:</label><input type="text" class="tSectionName" style="float:left" />
    <label style="width:75px;display:block;float:left;padding-left:20px">Template:</label><select class="dTemplate" style="float:left" onchange="fnUpdatePageType(this.value);">
        <option value="splash">Splash</option>
        <option value="intro">Intro</option>
        <option value="case">Case</option>
        <option value="examples">Examples</option>
        <option value="global">Global Page</option>
        <option value="goals">Goals</option>
        <option value="checklist">Checklist</option>
        <option value="pitfalls">Pitfalls</option>
        <option value="reflection">Reflection</option>
        <option value="theory">Theory</option>
        <option value="quiz">Quiz</option>
        <option value="complete">Complete</option>
        <option value="dictionary">Dictionary</option>        
        </select>   
        <label style="width:75px;display:block;float:left;padding-left:20px">Mode:</label><select id="dMode" style="float:left">
        <option value="">Display</option>
        <option value="nohead">Display w/o heading</option>
        <option value="hide">Hide</option>
        <option value="popup">Popup with link</option>        
        <option value="slidedown">Slidedown with link</option>        
        </select>   
              
              <br style="clear:left" />

    <div id="tabs">
<ul>

<li><a href="#tab-window-case">Case</a></li>
<li><a href="#tab-window-quiz">Quiz</a></li>
<li><a href="#tab-window-pitfalls">Pitfalls</a></li>
<li><a href="#tab-window-checklist">Checklist</a></li>
<li><a href="#tab-window-dictionary">Dictionary</a></li>

<li><a href="#tab-window-1">Content</a></li>

<li><a href="#tab-window-5">Notes/Tags</a></li>

</ul>

  <div id="tab-window-case" class="tab-window">
  <h1>Case Intro</h1>
  <textarea target-class="case-intro"></textarea>
    <div class="sort-container">
  <fieldset id="case-template" style="display:none" class="block">  
  <div style="float:right"><img src="../inc/images/icons/bullet_arrow_up.png" class="icon up" /><img src="../inc/images/icons/bullet_arrow_down.png" class="icon down" /><img src="../inc/images/icons/delete.png" class="icon delete" /></div>
  <h2 style="float:left">Case Question</h2><br style="clear:both" />
  <textarea target-class="case-question"></textarea>
  <h2>Case Answer</h2>
  <textarea target-class="case-answer"></textarea>
  </fieldset>
  </div>
  <a href="javascript:void(0)" class="add icon"><img src="../inc/images/icons/plus.png" /> Add Another Case Question</a>
  </div>  

  <div id="tab-window-quiz" class="tab-window">
  <h1>Quiz</h1> <input type="checkbox" id="instant-preview" onchange="$('.instant-preview').toggle(this.checked);" />Instant preview
  <div class="sort-container"> 
  <fieldset id="quiz-template" style="display:none" class="block xmlbind">
  <div style="float:right"><img src="../inc/images/icons/bullet_arrow_up.png" class="icon up" /><img src="../inc/images/icons/bullet_arrow_down.png" class="icon down" /><img src="../inc/images/icons/delete.png" class="icon delete" /></div>  
  <div style="float:left;width:500px">
      <b>Question:</b><br /><textarea xmlattr="questiontext"></textarea>
      <div class="quiz-aux-fields">
        <b>Introduction:</b><br /><textarea xmlattr="introduction" style="height:20px"></textarea>      
        <br /><b>Content reference:</b><br /><textarea xmlattr="reference" style="height:20px"></textarea>
        <br /><b>Reason:</b><br /><textarea xmlattr="reason" style="height:20px"></textarea>            
        <br /><b>Bottom text:</b><br /><textarea xmlattr="bottomtext" style="height:20px"></textarea>            
    </div>
</div>
  <div style="float:left;width:120px">
      <b>Type:</b><br /><select class="type" xmlattr="@type"><option value="multiplechoice">Multiple Choice</option><option value="draganddrop">Drag And Drop</option><option value="objectpositioning">Object Positioning</option><option value="connecttheboxes">Connect the Boxes</option><option value="fillintheblanks">Fill in the blanks</option></select>
      <br /><b>Background:</b><br /><input type="text" xmlattr="@background" style="width:120px" />
      <br /><b>Answer time (sec):</b><br /><input type="text" xmlattr="@timer" style="width:120px" />
      <input type="hidden" xmlattr="@version" />
  </div>
  
  <br style="clear:both" />
<div style="float:left;width:700px" class="rendering-unsupported"> <b>Rendering of this question is unsupported. Please edit XML instead</b><br /><textarea style="height:200px;width:100%"></textarea></div>
  <br style="clear:both" />
  <div class="mc-container container xmlbind group">
      <div class="text">Text</div><div class="q-group">Group</div>&nbsp;<b>Correct</b>
      <div style="width:600px">
          <div class="options-mc">
              <div id="quiz-mc-template" class="template"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><input type="text" class="text" /><input type="text" class="q-group" /><input type="checkbox" class="correct"/> <img src="../inc/images/icons/delete.png" class="icon delete option" style="float:right" /><br style="clear:both" /></div>      
          </div>
          <a href="javascript:void(0)" class="add-option-mc icon"><img src="../inc/images/icons/plus.png" /> Add Another Option</a>
      </div>
  </div>
    <div class="fb-container container xmlbind">
        <div style="width:600px">
            <div>Align answers: <select xmlattr="text@align"><option value="left">Left</option><option value="top">Top</option></select></div>
            <div>Re-use answers: <select xmlattr="text@reuse-answers"><option value="">No</option><option value="1">Yes</option></select></div>
            <div>Size: <select xmlattr="text@size"><option value="regular">Regular</option><option value="small">Small</option></select></div>
            <b>Text</b><br />
            <textarea style="width:600px;height:120px" xmlattr="text"></textarea>
            <a href="javascript:void(0)" class="add-option-fb icon"><img src="../inc/images/icons/plus.png" /> Add Another Blank</a>
            <br /><br />
            <div class="text">Incorrect options</div>
            <br style="clear:both" />
            <div style="width:600px">
                <div class="options-fb">
                    <div id="quiz-fb-template" class="template group"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><input type="text" class="text" /> <img src="../inc/images/icons/delete.png" class="icon delete option" style="float:right" /></div>      
                </div>
                <a href="javascript:void(0)" class="add-incorrect-fb icon"><img src="../inc/images/icons/plus.png" /> Add Another Option</a>
            </div>
        </div>
  </div>
  <div class="dd-container container xmlbind">
      <div style="width:350px;border-right:2px dashed silver;float:left;padding-right:10px">
          <div class="text" style="width:240px">Drags</div><div class="text" style="width:50px">Target</div><br style="clear:both" />
          <div>Align: <select xmlattr="drags@align"><option value="left">Left</option><option value="top">Top</option></select></div>
          <div>Override Width: <input type="text" xmlattr="drags@width" style="width:20px" /> / Height: <input type="text" xmlattr="drags@height" style="width:20px" /></div>
          <div class="options-dd">
              <div id="quiz-dd-template" class="template"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><input type="text" class="text" /><input type="text" class="target"/> <img src="../inc/images/icons/delete.png" class="icon delete option" style="float:right" /><br style="clear:both" /></div>                          
          </div>
          <a href="javascript:void(0)" class="add-option-dd icon"><img src="../inc/images/icons/plus.png" /> Add Another Option</a>
      </div>
      <div style="width:430px;float:left;padding-left:10px">
        <div class="text">Drops</div><br style="clear:both" />
          <div>Position: <select xmlattr="targets@position" onchange="$(this).parents('.xmlbind:eq(0)').find('.coordinates').toggle(this.value=='absolute');"><option value="auto">Auto</option><option value="absolute">Absolute</option></select> &nbsp;Size: <select xmlattr="targets@size"><option value="">Normal</option><option value="compact">Compact</option></select></div>
          <div class="options-target">
            <div id="quiz-target-template" class="template xmlbind"><span style="float:left" class="number">1. </span><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><input type="text" class="text" xmlattr="" />&nbsp;&nbsp;<span class="coordinates">x:<input type="text" xmlattr="@x" style="width:25px" /> y:<input type="text" xmlattr="@y" style="width:25px" /> w:<input type="text" xmlattr="@width" style="width:25px" /> h:<input type="text" xmlattr="@height" style="width:25px" /></span><img src="../inc/images/icons/delete.png" class="icon delete option" style="float:right" /><br style="clear:both" /></div>      
          </div>
          <a href="javascript:void(0)" class="add-option-dd icon"><img src="../inc/images/icons/plus.png" /> Add Another Option</a>
      </div>
  </div>    
    <div class="cb-container container xmlbind">
        <div><b>Arrange:</b> <select xmlattr="boxes@arrange" onchange="$(this).parents('.xmlbind:eq(0)').find('.coordinates').toggle(this.value=='absolute');"><option value="auto">Auto</option><option value="absolute">Absolute</option></select> &nbsp;&nbsp;<b>Draw border:</b> <select xmlattr="boxes@draw"><option value=""></option><option value="false">False</option></select></div>
        <br />
      <div style="width:470px;border-right:2px dashed silver;float:left;padding-right:10px">
          <div class="text" style="width:240px">Sources</div><div class="text" style="width:50px">Target</div><br style="clear:both" />
          <div class="options-cb">
              <div id="quiz-cb-template" class="template xmlbind"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><input type="text" class="text" xmlattr="" /><input type="text" class="target"/> &nbsp;&nbsp;<span class="coordinates">x:<input type="text" xmlattr="@x" style="width:25px" /> y:<input type="text" xmlattr="@y" style="width:25px" /></span><img src="../inc/images/icons/delete.png" class="icon delete option" style="float:right" /><br style="clear:both" /></div>                          
          </div>
          <a href="javascript:void(0)" class="add-option-cb icon"><img src="../inc/images/icons/plus.png" /> Add Another Option</a>
      </div>
      <div style="width:340px;float:left;padding-left:10px">
        <div class="text">Targets</div><br style="clear:both" />          
          <div class="options-cb-target">
            <div id="quiz-cb-target-template" class="template xmlbind"><span style="float:left" class="number">1. </span><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><input type="text" class="text" xmlattr="" />&nbsp;&nbsp;<span class="coordinates">x:<input type="text" xmlattr="@x" style="width:25px" /> y:<input type="text" xmlattr="@y" style="width:25px" /></span><img src="../inc/images/icons/delete.png" class="icon delete option" style="float:right" /><br style="clear:both" /></div>      
          </div>
          <a href="javascript:void(0)" class="add-option-cb icon"><img src="../inc/images/icons/plus.png" /> Add Another Option</a>
      </div>
  </div>  
      <br style="clear:both" />
            
  <iframe class="instant-preview" style="clear:both;width:800px;height:300px">
                
  </iframe>
  </fieldset>
  </div>
  <a href="javascript:void(0)" class="add icon"><img src="../inc/images/icons/plus.png" /> Add Another Question</a>
  </div>

  <div id="tab-window-pitfalls" class="tab-window">
  <h1>Pitfalls</h1>  
  
  <div class="sort-container">
  <fieldset id="pitfalls-template" style="display:none" class="block">
  <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
  <div style="float:right"><img src="../inc/images/icons/bullet_arrow_up.png" class="icon up" /><img src="../inc/images/icons/bullet_arrow_down.png" class="icon down" /><img src="../inc/images/icons/delete.png" class="icon delete" /></div>  
  <textarea style="float:left"></textarea>
  </fieldset>
  </div>
  <a href="javascript:void(0)" class="add icon"><img src="../inc/images/icons/plus.png" /> Add Another Pitfall</a>
  </div>

<div id="tab-window-checklist" class="tab-window">
  <h1>Checklist</h1>  
    <div class="sort-container">
  <fieldset id="checklist-template" style="display:none" class="block">
  <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
  <div style="float:right"><img src="../inc/images/icons/bullet_arrow_up.png" class="icon up" /><img src="../inc/images/icons/bullet_arrow_down.png" class="icon down" /><img src="../inc/images/icons/delete.png" class="icon delete" /></div>  
  <textarea style="float:left"></textarea>
  </fieldset>
  </div>
  <a href="javascript:void(0)" class="add icon"><img src="../inc/images/icons/plus.png" /> Add Another Checklist Item</a>
  </div>
<div id="tab-window-dictionary" class="tab-window">
  <h1>Dictionary</h1>  
    
  <fieldset id="dictionary-template" style="display:none" class="block">    
      <div style="float:right"><img src="../inc/images/icons/delete.png" class="icon delete" /></div>  
      <div style="float:left;width:500px; "><textarea style=" display:inline;width:150px; height:50px"></textarea> <div style="display:inline;position:relative;top:-20px">==&gt; </div><textarea style="display:inline;width:250px;height:50px"></textarea></div>
  </fieldset>
  
  <a href="javascript:void(0)" class="add icon"><img src="../inc/images/icons/plus.png" /> Add Another Dictionary Item</a>
  </div>        

  <div id="tab-window-1" class="tab-window">
      

          <div class="content">
                <textarea style="height:600px;" class="content_editor" name="content_editor" id="content_editor"></textarea>
              </div>
 
  </div>
  
<div id="tab-window-5" class="tab-window">
    <h1>Notes/Tags</h1>
</div>
        </div>
            </div>

            <div id="tab-window-2" class="tab-window">Settings will display relevant to the object type selected.
  <br />Tabs will only display if they're relevant for the selcted object.
  <br />For template files, we will have setting: copy to destination (which will copy the template file to the destination directly). Or, maybe we should have a link between the template file and the d_courses object?
  <br /><br />
  <jgrid:jGridData runat="server" id="gSettingsGrid" Pagination="false" DataViewFiltering="false" CaseInsensitiveFilter="true" BindDropdownByID="false" style="width:956px;" SortOrder="asc" JSName="oSettingsGrid" AutoFilter="true" PageSize="50" SortCol="1" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets' style='border-top:0;margin-bottom:0'" DeleteConfirmation="Are you sure you wish to delete this portal?" DisplayFilter="true" DisplayAdd="false">
    <Cols>        
        <jgrid:col runat="server" id="cID" HText="ID" ColType="Hide" PrimaryKey="true" FormatString="#ObjectID#|#TableName#|#KeyName#" SortField="ObjectID, TableName, KeyName" FPartialMatch="true" />
        <jgrid:col runat="server" id="cKey" HText="Key" ColType="Text" PrimaryKey="false" FormatString="KeyName" SortField="KeyName" FField="KeyName" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cValue" HText="Value" ColType="Text" PrimaryKey="false" FormatString="Value" SortField="Value" FField="Value" FPartialMatch="true" FInputAtt="class='text'" />

        <jgrid:col id="cOptions" HAtt="class='device-info'" CAtt="class='functions'" runat="server" HText="" ColType="Options" CText="<a href='javascript:void(0);' onclick='fnJGridRowEdit(this);'><img src='../inc/images/icons/pencil1.png' alt='Edit this setting' title='Edit this setting' /></a>  <a href='javascript:void(0);' onclick='fnJGridRowDel(this);'><img src='../inc/images/icons/cancel.png' alt='Delete this setting'  title='Delete this setting' /></a>" />
</Cols>
</jgrid:jGridData>
  </div> 
            <div id="tab-window-3" class="tab-window">
  <button type="button" onclick="window.open('edit.aspx?action=diff&ids=' + escape(gVersionsGrid.getSelection()));">Diff selected versions</button>
  <jgrid:jGridData runat="server" id="gVersionsGrid" Pagination="false" DataViewFiltering="false" CaseInsensitiveFilter="true" BindDropdownByID="false" style="width:956px;" SortOrder="asc" JSName="gVersionsGrid" AutoFilter="true" PageSize="50" SortCol="1" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets' style='border-top:0;margin-bottom:0'" DeleteConfirmation="Are you sure you wish to delete this portal?" DisplayFilter="true" DisplayAdd="false">
    <Cols>        
        <jgrid:col runat="server" id="cSelect" HText="" ColType="Select" PrimaryKey="false" FormatString="value" SortField="value" FField="value" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cVersionID" HText="ID" ColType="Hide" PrimaryKey="true" SortField="ObjectID, CreateDate, DeleteDate" FPartialMatch="true" />
        <jgrid:col runat="server" id="cDate" HText="Date Modified" ColType="Text" PrimaryKey="false" FormatString="CreateDate" SortField="CreateDate" FField="CreateDate" FPartialMatch="true" FInputAtt="class='text daterangepicker'" />
        <jgrid:col runat="server" id="cUser" HText="User" ColType="Text" PrimaryKey="false" FormatString="#CreatedBy.Username#" SortField="#CreatedBy.Username#" FField="#CreatedBy.Username#" FPartialMatch="true" FInputAtt="class='text'" />

<jgrid:col runat="server" id="cVersionOptions" HAtt="class='device-info'" CAtt="class='functions'" HText="" ColType="Options" FormatString="<a href='javascript:void(0);' onclick=&quot;window.open('edit.aspx?cms=true&ObjectID=' + getQuerystring('ObjectID') + '&revision=' + escape('#DeleteDate#') + '&parent_id=33144&context=content');&quot;><img src='../inc/images/icons/pencil1.png' alt='View this version' title='View this version' /></a> | Comp: <a href='javascript:void(0);' onclick=&quot; var d=fnJGridGetData(this); window.parent.fnContextMenu('diff-old1',null,'d_folders:' + getQuerystring('ObjectID'),d.Grid.getData(d.Index,'cVersionID'),null);&quot;>L</a>, <a href='javascript:void(0);' onclick=&quot; var d=fnJGridGetData(this); window.parent.fnContextMenu('diff-old2',null,'d_folders:' + getQuerystring('ObjectID'),d.Grid.getData(d.Index,'cVersionID'),null);&quot;>R</a>, <a href='javascript:void(0);' onclick=&quot; var d=fnJGridGetData(this); window.parent.fnContextMenu('diff-new1',null,'d_folders:' + getQuerystring('ObjectID'),d.Grid.getData(d.Index,'cVersionID'),null);&quot;>N</a>" />                                
</Cols>
</jgrid:jGridData>

<jgrid:col id="cPerformedActivity" HText="User" ColType="Text" PrimaryKey="false" FormatString="Content Review/Final Customer Check/Revision" SortField="value" FField="value" FPartialMatch="true" FInputAtt="class='text'" />
  </div>     
              <div id="tab-window-4" class="tab-window">
   <jgrid:jGridAdo runat="server" id="gAlsoUsedIn" Pagination="false" DataViewFiltering="true" CaseInsensitiveFilter="true" BindDropdownByID="false" style="width:956px;" SortOrder="asc" JSName="gAlsoUsedIn" AutoFilter="true" PageSize="50" SortCol="1" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets' style='border-top:0;margin-bottom:0'" DeleteConfirmation="Are you sure you wish to delete this portal?" DisplayFilter="true" DisplayAdd="false">
    <Cols>        
        <jgrid:col runat="server" id="cAlsoID" HText="ID" ColType="Text" PrimaryKey="true" FormatString="ID" SortField="ID" FPartialMatch="true" />
        <jgrid:col runat="server" id="cAlsoPath" HText="Key" ColType="Text" PrimaryKey="false" FormatString="Path" SortField="Path" FField="Path" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cAlsoContext" HText="Value" ColType="Text" PrimaryKey="false" FormatString="Context" SortField="Context" FField="Context" FPartialMatch="true" FInputAtt="class='text'" />
</Cols>
</jgrid:jGridAdo>
    
</div> 

    </div>
        

        <div class="buttons" style="padding:5px;margin:10px 0;border:1px solid silver">
    <common:button.button runat="server" buttonmode="buttonbutton" id="bPreview" text="Preview" cssclass="bPreview" />
  <common:button.button runat="server" buttonmode="buttonbutton" id="bSave" text="Save" cssclass="bSave" onclientclick="fnSaveContent();" />
  <common:button.button runat="server" buttonmode="buttonbutton" id="bSaveAsCopy" text="Save As New Page" cssclass="bSaveAsCopy" onclientclick="fnSaveContent(true);" />
</div>

        </td>

            <td valign="top" style="width:280px;padding:5px; border:4px solid silver">
        <h1 style="background-color:#ddd">Sections</h1>
    <div style="overflow:scroll;width:280px;height:750px; line-height:1.8">
        <table><tr>
            <td valign="top">
                Add Id:<br />
                At:
            </td>
            <td>
                <input type="text" id="existing-id" style="width:50px" /> (blank for new)<br />
                <select id="insert-section"><option value="beginning">At Beginning</option></select><br />
                <select id="apply-section-to"><option value="all">Apply to all linked pages</option><option value="current">Apply to current page only</option>
                </select>
            </td><td valign="top"><br /><button type="button" id="insert-section-go">Go</button></td>
        </tr></table>         
        <br />[ <a href="javascript:void(0)" class="preview-toggle">Collapse All</a> | <a href="javascript:void(0)" class="preview-toggle expand">Expand All</a> ]
        <div style="overflow:hidden" id="preview-container">
            <iframe class="preview" src="" style="width:1100px;" runat="server" id="hPreview"></iframe>
        </div>
        </div>
            </td>

        </tr>
    </table>                
        <script type="text/javascript">   
    gAlsoUsedIn.registerEventListener("GridRenderCell", function (oEvent) {
    if(oEvent.Data.Index>-1){
        oEvent.Data.HTML=stripEndingStr(oEvent.Data.HTML,">");
        if(oEvent.Data.ColObject.ID == "cAlsoPath")oEvent.Data.HTML="<a style='text-decoration:underline' href='edit.aspx?cms=true&ObjectID=" + gAlsoUsedIn.getData(oEvent.Data.Index,"cAlsoID").safeSplit(">",-2) + "&revision=" + getQuerystring("revision") + "&context=" + gAlsoUsedIn.getData(oEvent.Data.Index,"cAlsoContext").safeSplit(">",-2) + "' target='_blank'>" + oEvent.Data.HTML + "</a>";        
    }
});
gAlsoUsedIn.registerEventListener("GridCallbackEnd", function (oEvent) {    
    if(!gAlsoUsedIn.Data.length || gAlsoUsedIn.Data.length<2){
        $("#tab-window-4, #tab-also-used-in, .bSaveAsCopy").hide();
    }else{
        $("#editor-container").css("background-color","#f9eded");
    }        
});
    </script>
    </asp:PlaceHolder>

<script type="text/javascript">   
    //if($("._editor").length>0) $(".ajax-postback").removeClass("ajax-postback");
//Fix to prevent IE to display "Are you sure you want to navigate away from this page every time a A-link with javascript:void(0) is clicked"
if(navigator.userAgent.indexOf("MSIE ")>-1) $(document.body).on("click","a",function(){if(!Data._Saving){Data._Saving=true;setTimeout(function(){Data._Saving=false;},1);}});

$(document).on("click","#tab-window-case .icon, #tab-window-pitfalls .icon, #tab-window-checklist .icon, #tab-window-quiz .icon, #tab-window-dictionary .icon",function(e){var o=$(e.target);
    if(o.hasClass("delete") && window.confirm("Are you sure you wish to remove this item?")){o.parents(o.hasClass("option")? "div:eq(0)": "fieldset:eq(0)").detach();}
    else if(o.hasClass("up")){ if(o.parents("fieldset:eq(0)")[0]==o.parents(".tab-window").find("fieldset:visible:first")[0])return; o.parents("fieldset:eq(0)").insertBefore(o.parents("fieldset:eq(0)").prev());}
    else if(o.hasClass("down")){ if(o.parents("fieldset:eq(0)")[0]==o.parents(".tab-window").find("fieldset:visible:last")[0])return; o.parents("fieldset:eq(0)").insertAfter(o.parents("fieldset:eq(0)").next());}
    else if(o.hasClass("add")){fnAddItem();}
    else if(o.hasClass("add-option-mc")){$("#quiz-mc-template").clone().attr("id","").removeClass("template").appendTo($(o).parents("fieldset").find(".options-mc"));}
    else if(o.hasClass("add-option-dd")||o.hasClass("add-option-cb")){$(o).parent().find(".template").clone().attr("id","").removeClass("template").appendTo($(o).prev());}
    else if(o.hasClass("add-option-fb")){$(o).parent().find("textarea").val($(o).parent().find("textarea").val()+" {word}");}
    else if(o.hasClass("add-incorrect-fb")){$("#quiz-fb-template").clone().attr("id","").removeClass("template").appendTo($(o).parents("fieldset").find(".options-fb"));}
}).on("load","iframe.preview",function(){
    alert(this);
});

function fnAddItem(Q,A){

    function populateFromXmlNode(el,xml){
        var parent=$(el).closest(".xmlbind");
        $(el).find("[xmlattr]").filter(function(){
            return $(this).closest(".xmlbind")[0]==parent[0];
        }).each(function(i,o){
            var x=$(o).attr("xmlattr").split("@")[0];
            var a=$(o).attr("xmlattr").split("@")[1];
            var n=$(x? $(xml).find(x.split("/").join(" ")) : xml);
            if(a)$(o).val(n.attr(a));                                
            else $(o).val(n.text());                                
        });                
        return xml;
    }

    var s=$("#tabs").data("active");
    if(s=="dictionary" && Q && Q.nodeName=="entry"){
        A=$(Q).find("value").text();
        Q=$(Q).find("key").text();                
    }
    var o=$("#" + s + "-template").clone().attr("id","").show().find("textarea:eq(0)").val(cStr(Q)).end().find("textarea:eq(1)").val(cStr(A)).end().insertAfter($("#tab-window-" + s + " fieldset:last")).show();
    if(s=="case"){        
        o.find("textarea").ckeditor({toolbar:toolbar = MetierToolbarSmall, skin: 'moonocolor', width:'80%'} );
        if(!$("[target-class='case-intro']").is(".editor"))$("[target-class='case-intro']").ckeditor({toolbar:toolbar = MetierToolbarSmall, skin: 'moonocolor', width:'80%'} );            
    }else if(s=="quiz"){        
        var s=Q && ($(Q).attr("type")=="draganddrop" || $(Q).attr("type")=="objectpositioning"?"dd":($(Q).attr("type")=="connecttheboxes"?"cb":($(Q).attr("type")=="fillintheblanks"?"fb":"mc"))); 
        var unsupported=false;//$(Q).attr("type")=="connecttheboxes";
        if(Q){
            $(Q).find("answer").each(function(){
                if(s=="mc")$("#quiz-mc-template").clone().attr("id","").removeClass("template").find("input:eq(0)").val($(this).text()).end().find("input:eq(1)").val($(this).attr("group")).end().find("input:eq(2)").attr("checked",($(this).attr("correct")=="yes"?"checked":null)).end().appendTo(o.find(".options-mc"));
                else if(s=="fb")$("#quiz-fb-template").clone().attr("id","").removeClass("template").find("input:eq(0)").val($(this).text()).end().appendTo(o.find(".options-fb"));
            });
            $(Q).find("target").each(function(){
                populateFromXmlNode($("#quiz-target-template").clone().attr("id","").removeClass("template").appendTo(o.find(".options-target")), this);
            });
            $(Q).find("drag").each(function(){                
                var p=$("#quiz-dd-template").clone().attr("id","").removeClass("template").find("input:eq(0)").val($(this).text()).end().appendTo(o.find(".options-dd")), 
                    s=$(this).attr("correct"),
                    x=$(Q).find("target[id='" + s + "']");
                if(s && x) p.find(".target").val(cStr(x.index()+1));                                                    
                if(cStr(s).indexOf("multi:")==0)unsupported=true;
            });
            $(Q).find("box").each(function(){                
                var c=o.find(".options-cb" + ($(this).attr("texton")=="right"?"-target":""));
                var p= populateFromXmlNode(c.find(".template").clone().attr("id","").removeClass("template").find("input:eq(0)").val($(this).text()).end().appendTo(c).data("xml-id",$(this).attr("id")),this);                    
            });
            
        }       
        if(!Q)
            Q=$($.parseXML('<x><question version="1.1" /></x>').documentElement).find("question")[0];            
                
        populateFromXmlNode(o,Q);
        
        o.find(".rendering-unsupported").toggle(unsupported).find("textarea").val(unsupported?fnSerializeXML(Q):"");

        o.find("select.type").change(function(){ if(!o.find(".rendering-unsupported").is(":visible")) {$(o).find(".container").removeClass("show").filter("."+($(this).val()=="draganddrop"|| $(this).val()=="objectpositioning"?"dd":($(this).val()=="connecttheboxes"?"cb":($(this).val()=="fillintheblanks"?"fb":"mc")))+"-container").addClass("show");}}).change();        
        
        populateFromXmlNode(o.find(".container.show"),Q);        
        
        var f=function(){ setTimeout(function(){ o.find(".options-target div .number, .options-cb-target div .number").filter(":visible").each(function(i,o){i++;$(this).html(i+". ").data("index",i);});},10); };
        o.find(".options-mc, .options-dd, .options-target, .options-cb, .options-cb-target").sortable({stop:
                function(){
                    var t= o.find(".options-dd .target, .options-cb .target").data("dirty",1);                
                    o.find(".options-target div .number, .options-cb-target div .number").each(function(i,o){ if(i>0){ t.each(function(){ if($(this).data("dirty") && $(this).val()==$(o).data("index"))$(this).val(i).data("dirty",null); }); };}); f();
                }
        });//.disableSelection();                
        o.find(".add-option-dd, .options-target .delete, .options-cb-target .delete, .add-option-cb").click(f);
        f();

        if(s=="cb"){
            var f=function(id){return o.find("div").filter(function(){return $(this).data("xml-id")==id;})[0];};
            $(Q).find("correctconnections connection").each(function(){
                var a=f($(this).attr("id1")), b=f($(this).attr("id2"));                                
                if(a && b){
                    if($(a).parents(".options-cb").length==0){
                        var c=b;b=a;a=c;
                    }
                    $(a).find(".target").val(cIfValue($(a).find(".target").val(),"",",","")+(o.find(".options-cb-target > div:visible").index(b)+1));
                }
            });
        }
    }
}


    var Data={}, Folder, Content, Course;
    $(function () {       
        $(window).resize(function () {
            if(CKEDITOR.instances[$(".content_editor").attr("id")] && CKEDITOR.instances[$(".content_editor").attr("id")].document) CKEDITOR.instances[$(".content_editor").attr("id")].resize(null,$(window).height()-250);            
        }); 
                
        Data=$(document.body).data("__data");
        Folder=Data.Folder;
        Course=Data.Course;
        
        if(Folder){            
            $("#path").html("<b>Path: </b><u>" + cStr(Folder.Path).split(">").remove(0,0).join("</u>&nbsp;&gt;&nbsp;<u>")+"</u>").show();                        
        }
                
        var _D=[];
        $.each(Data.Tags,function(){_D.push([this.ObjectID,cIfValue(this.Path,""," > ","") + this.Name]);});
        Data.Tags=_D.sort(function(A,B){return (A[1] == B[1]) ? 0 : (A[1] > B[1]) ? 1 : -1;});
        

        //Init tabs
        $("#main-tabs").tabs({});
        
        $('#tabs').tabs({
            load: function (event, ui) {                
                $('a', ui.panel).click(function () {
                    $(ui.panel).load(this.href);
                    return false;
                });                
            },
            create: function(event, ui){
                if(!$("#tabs").data("active") && ui.panel)
                    $("#tabs").data("active",$(ui.panel).attr("id").safeSplit("-",2));                
            },activate: function (event, ui) {                
                var oFrame = $("iframe", ui.newPanel);
                $("#tabs").data("prev-active",cStr($("#tabs").data("active"))).data("active",$(ui.newPanel).attr("id").safeSplit("-",2));                
                if (cStr(oFrame.attr("src")) == "" && oFrame.attr("load-src") != "") oFrame.attr("src", oFrame.attr("load-src"));
                setTimeout(function(){ fnUpdatePageType($(".dTemplate").val()); },1);
            }
        });
        $("#tabs>ul>li>a").each(function(){
            $(this).parent().toggle(!isNaN(Number($(this).attr("href").safeSplit("-",2))));
        });
    });


    <common:label runat="server" id="lScript" />

  $(".bPreview").click(function(){    
    fnSaveContextEditor();
    var s=CKEDITOR.instances[$(".content_editor").attr("id")]?CKEDITOR.instances[$(".content_editor").attr("id")].getData():$(".content_editor").val();
    if(window.localStorage){
        window.localStorage["CMS:ContentId"] = EditorData.Content.ObjectID;
        window.localStorage["CMS:Content"] = s;
    }
    $("<form target='lesson-preview' method='post' action='../player/player.aspx?preview=true&PageID="+Data.Folder.ObjectID + "'><input type='hidden' name='preview-data-" + EditorData.Content.ObjectID + "' /></form>").find("input").val(s).end().appendTo(document.body).submit().remove();
});      

  
    window.onbeforeunload = function(e) {
        //Do we need to save content to editor        
        if(fnContentHasChanged()){
            return "You have unsaved changes. Are you sure you want to navigate away from this page?";
        }         
    };    

    function fnSaveContextEditor(){
        if(cInt($("#tabs").data("active"))==0){
            $("#tabs").data("prev-active", $("#tabs").data("active"));
            fnUpdatePageType($("#tabs").data("active"));         
        }
    }

    function fnQuizPreviewQuestion(el){
        var c=$(el).parents(".block:eq(0)");
        var doc=fnQuizSerializeQuestion(null,c);
        var frame=c.find("iframe");
        var answers="";
        if(frame.attr("src") && frame[0].contentWindow.Quiz){
            var Q=frame[0].contentWindow.Quiz.Quiz;
            Q.save();            
            answers="q0;"+Q.Settings.Data[0].Answers;
        }
        frame.attr("src","../player/player.aspx?header=hide&preview=true&PageID="+Data.Folder.ObjectID+ "&ContentID=" + EditorData.Content.ObjectID +"&preview-data-"+EditorData.Content.ObjectID+"="+encodeURIComponent(fnSerializeXML(doc))+"&answers=" + encodeURIComponent(answers));        
    }
    function fnQuizSerializeQuestion(doc,el){
        function saveToXmlNode(el,xml){
            var parent=$(el).closest(".xmlbind");
            $(el).find("[xmlattr]").filter(function(){
                return $(this).closest(".xmlbind")[0]==parent[0];
            }).each(function(i,o){
                var x=$(o).attr("xmlattr").split("@")[0];
                var a=$(o).attr("xmlattr").split("@")[1];
            
                var n=$(x? $(xml).find(x.split("/").join(" ")) : xml);
                if(x && n.length==0){
                    n=$(xml);
                    for(var i=0;i<x.split("/").length;i++){
                        if(n.find(x.split("/")[i]).length==0) n=$(fnCreateXmlNode(n,x.split("/")[i]));
                        else n=n.find(x.split("/")[i]);
                    }
                }
                if(a)fnSetXmlAttribute(n,a,$(o).val());
                else n.text($(o).val());                    
            });
            return xml;
        }

        if(!doc)doc=$.parseXML("<quiz />");
        node=$(doc).find("questions")[0];
        if(!node)node=fnCreateXmlNode(doc,"questions");        
        
        if($(el).find(".rendering-unsupported textarea").val()){
            var q=$.parseXML($(el).find(".rendering-unsupported textarea").val()).documentElement;
            if(q){
                node.appendChild(q);            
                var q=fnSetXmlAttribute(q,"type",$(el).find("select").val(),"weight",1);
                $($(q).find("questiontext")).text($(el).find("textarea").val());
            }
        }else{        
            var q=fnSetXmlAttribute(fnCreateXmlNode(node,"question"),"type",$(el).find("select").val(),"weight",1);
            $(fnCreateXmlNode(q,"questiontext")).text($(el).find("textarea").val());
            var type=$(el).find("select.type").val();
            if(type=="draganddrop" || type=="objectpositioning"){
                var t=fnCreateXmlNode(q,"targets"), d=fnCreateXmlNode(q,"drags");
                $(el).find(".options-dd > div").not(".template").each(function(){
                    //if($(this).find("input:eq(0)").val()){
                    saveToXmlNode(this,$(fnSetXmlAttribute(fnCreateXmlNode(d,"drag"),"correct", cIfValue($(this).find("input:eq(1)").val(),"target","","") )).text($(this).find("input:eq(0)").val()));                
                    //$(fnSetXmlAttribute(fnCreateXmlNode(d,"drag"),"x",$(this).find(".").val(),"y",$(this).find(".").val(),"correct", cIfValue($(this).find("input:eq(1)").val(),"target","","") )).text($(this).find("input:eq(0)").val());
                    //}
                });                        
                $(el).find(".container.show .options-target > div").not(".template").each(function(i,o){
                    //if($(this).find("input:eq(0)").val()){                    
                    saveToXmlNode(this,$(fnSetXmlAttribute(fnCreateXmlNode(t,"target"),"id", "target"+(i+1) )).text($(this).find("input:eq(0)").val()));
                    //}
                });
            }else if(type=="connecttheboxes"){
                var b=saveToXmlNode(q,fnCreateXmlNode(q,"boxes"))
                    , c=fnCreateXmlNode(q,"correctconnections");
                $(el).find(".options-cb > div, .options-cb-target > div").not(".template").each(function(i,o){                    
                    $(this).data("xml-id","id" + i);                    
                    saveToXmlNode(this,$(fnSetXmlAttribute(fnCreateXmlNode(b,"box"),"id",$(this).data("xml-id"),"texton",$(this).parents(".options-cb").length==0?"right":"left")));                
                });                
                $(el).find(".options-cb > div .target").each(function(i,o){                    
                    $.each($(this).val().split(","),function(j,p){
                        if(trim(p)){                            
                            var t=$(el).find(".number").filter(function(){return $(this).data("index")==p;}).parent();
                            if(t.data("xml-id")) fnSetXmlAttribute(fnCreateXmlNode(c,"connection"),"id1",$(o).parent().data("xml-id"),"id2",t.data("xml-id"));
                        }
                    });
                });
            }else{
                var a=fnCreateXmlNode(q,"answers");
                $(el).find(".options-mc > div").not(".template").each(function(){
                    if($(this).find("input:eq(0)").val()){
                        $(fnSetXmlAttribute(fnCreateXmlNode(a,"answer"),"group",$(this).find("input:eq(1)").val(),"correct",$(this).find("input:eq(2):checked").length>0?"yes":"no")).text($(this).find("input:eq(0)").val());
                    }
                });
                $(el).find(".options-fb > div").not(".template").each(function(){
                    if($(this).find("input:eq(0)").val()){
                        $(fnCreateXmlNode(a,"answer")).text($(this).find("input:eq(0)").val());
                    }
                });
            }            
            saveToXmlNode($(el).find(".container.show"),q);
            saveToXmlNode(el,q);        
        }

        return doc;
    }

    function fnUpdatePageType(Type){            
        var p=$("#tabs").data("prev-active"), c=$("#tabs").data("active");        
        //if(p==c && c==Type)return;
                
        var Editor=$("#tab-window-"+p);
        //Do we need to change editor type? textarea vs. CKEDITOR
        if((Type=="quiz" || Type=="dictionary") && CKEDITOR.instances[$(".content_editor").attr("id")]!=null)CKEDITOR.instances[$(".content_editor").attr("id")].destroy();
        else if(!(Type=="quiz" || Type=="dictionary") && CKEDITOR.instances[$(".content_editor").attr("id")]==null)fnCreateCKEDITOR(".content_editor", false);
        //save previous to main HTML editor
        if(p && isNaN(Number(p))){
            if(p=="quiz"){
                var doc = $.parseXML("<quiz />");
                Editor.find(".block:gt(0)").each(function(){
                    fnQuizSerializeQuestion(doc,this);                    
                });                                
                $(".content_editor").val(fnSerializeXML(doc));
            }else if(p=="dictionary"){
                var doc = $.parseXML("<dictionary />");
                Editor.find(".block:gt(0)").each(function(){
                    var q=fnCreateXmlNode(doc,"entry");
                    $(fnCreateXmlNode(q,"key")).text($(this).find("textarea:eq(0)").val());
                    $(fnCreateXmlNode(q,"value")).text($(this).find("textarea:eq(1)").val());
                });                
                $(".content_editor").val(fnSerializeXML(doc));
            }else{
                var Content="",Controls=Editor.find("input, textarea");
                Controls.each(function(){                
                    var s=$(this).val().replace(/^(<p><\/p>\s*)+|((<p><\/p>\s*)+)$/,"");                    
                    if(s){
                         if(p=="case") Content+="<div class='" + $(this).attr("target-class") + "'>" + s + "</div>";
                         else Content+="<li>"+s+"</li>";
                    }
                });
                $("#tabs").data("prev-active",null);
                if(p=="pitfalls" || p=="checklist")Content="<ul>"+Content+"</ul>";
                CKEDITOR.instances[$(".content_editor").attr("id")].setData(Content);
                $(".content_editor").val(Content);
            }
        }            
        if(p==c && (!Type || c!=null))return;        
        //Only show tabs applicable to this content type
        $("#tabs>ul>li>a").each(function(){var s=$(this).attr("href").safeSplit("-",2); if(!isNaN(Number(s)))return;  $(this).parent().toggle(Type==s); $(this).next().toggle(Type==s); });

        //Restore tab previusly viewed for this content type
        if($("#tabs").data("type")!=Type && $.cookie("CMS.preference."+Type) && $("#tabs>ul>li>a[href='#tab-window-"+$.cookie("CMS.preference."+Type)+"']").is(":visible") && $.cookie("CMS.preference."+Type)!=c){
            $("#tabs").data("type",Type).data("active",null);
            $("#tabs>ul>li>a[href='#tab-window-"+$.cookie("CMS.preference."+Type)+"']").click();
            return;
        }
        $("#tabs").data("type",Type);
                    
        //Make sure window current on display is available for this content type        
        if(isNaN(c) && !$("#tabs>ul>li>a[href='#tab-window-"+c+"']").is(":visible")){    
            $("#tabs").data("active",null);
            $("#tabs>ul>li>a:visible:eq(0)").click();
            return;
        }
        $.cookie("CMS.preference."+Type,c);
        
        Editor=$("#tab-window-"+c);
        //populate HTML editor to context-sensitive editor
        if(isNaN(Number(c))){        
            var Content=null, s;
            if($(".content_editor").data("editor-loaded")) Content=CKEDITOR.instances[$(".content_editor").attr("id")];            
            s=$("<div>" + (Content? Content.getData():$(".content_editor").val())+"</div>");            
            var B=Editor.find("fieldset:visible");
            if(c=="case"){            
                Editor.find("[target-class='case-intro']").val(s.find(".case-intro").html());                
                for(var x=0;x<s.find(".case-question").length || x==0;x++){  
                    var q=s.find(".case-question").eq(x);
                    var a=q.next().is(".case-answer") ? q.next() : null;                    
                    if(B.length-1<x) fnAddItem(q.html(),a?a.html():"");
                    else{
                        B.eq(x).find("textarea:eq(0)").val(q.html());
                        B.eq(x).find("textarea:eq(1)").val(a?a.html():"");
                    }
                }                
                while(Editor.find("fieldset:visible").length>s.find(".case-question").length)Editor.find("fieldset:visible:last").detach()
            }else if(c=="pitfalls" || c=="checklist"){            
                for(var x=0;x<s.find("li").length || x==0;x++){  
                    if(B.length-1<x) fnAddItem(s.find("li").eq(x).text());
                    else B.eq(x).find("textarea:eq(0)").val(s.find("li").eq(x).text());
                }                
            }else if(c=="quiz"){      
                $( "#tab-window-quiz fieldset:gt(0)").detach();
                if($(".content_editor").val()) $($.parseXML($(".content_editor").val())).find("question").each(function(){
                    fnAddItem(this);
                });                
            }else if(c=="dictionary"){      
                $("#tab-window-dictionary fieldset:gt(0)").detach();
                if($(".content_editor").val()) $($.parseXML($(".content_editor").val())).find("entry").each(function(){
                    fnAddItem(this);
                });
            }
            $( "#tab-window-"+ c +" .sort-container").sortable();//.disableSelection();
        }
        $(".buttons").toggle(cInt(c)==0 || CKEDITOR.instances[$(".content_editor").attr("id")]==null);        
    }
    function fnCreateCKEDITOR(Selector, ResetContent){        
        //if(!CKEDITOR.dom.element.get($(Selector).attr("name")) || !CKEDITOR.dom.element.get($(Selector).attr("name")).getEditor())                
        if(Course){
            var url = "../player/css.aspx?CourseID="+Course.ObjectID;
            if(CKEDITOR.config.contentsCss.indexOf(url)==-1)CKEDITOR.config.contentsCss.push(url);            
        }
                
        CKEDITOR.replace( $(Selector).attr("name"),{toolbar:toolbar = MetierToolbarCMS, skin: 'moonocolor'} );/*uiColor: '#9AB8F3', , width:'600px'*/
        //$(Selector).ckeditor({ skin: 'office2003', toolbar: 'Metier2', height: '600px' });
            
            CKEDITOR.instances[$(Selector).attr("id")].on("instanceReady",function(ev){                                
                var Doc=$(CKEDITOR.instances[$(Selector).attr("id")].document.$.documentElement);
                Doc.find("#quote").each(function(){
                    var s=cStr($(this).find("#writer").html());
                    $(this).find("#writer").detach();
                    $(this).after(fnBuildQuoteHTML($(this).html(),s)).detach();
                });
                                
                if(ResetContent) Data._Original=CKEDITOR.instances[$(Selector).attr("id")].getData().replace(/jquery\d+="\d+"/,"");            
                $(Selector).data("editor-loaded",true);

                ev.editor.on('paste', function (evt) {
                    var editor = evt.editor;
                    var s=cStr(evt.data.dataValue);
                    if(s.indexOf("msocom")>-1 || s.indexOf("msoanchor")>-1){
                        evt.stop();
                        var c=$(cStr("<div>"+evt.data.dataValue+"</div>"));
                        c.find("a[href*='msocom']").each(function(){
                            //$(this).next().detach();                            
                            $(this).detach();
                        });
                        c.find("a[href*='msoanchor']").each(function(){
                            $(this).parent().parent().detach();
                        });
                        editor.insertHtml( c.html() );                        
                    }
                    
                    // show loader that blocks editor changes
                    /*
                    $.post('clean.php', {html: evt.data.html}, function (data) {
                         // text will be inserted at correct place            
                    }, 'json');
                    */
                }); 
                $(window).resize();
            });                      						
    }

    $("#tab-window-quiz").on("change","input, textarea, select",function(e){
        if($("#instant-preview:checked").length>0){
            fnQuizPreviewQuestion(e.target);
            //var c=$(e.target).parentsUntil(".block");
            //alert(e.target);
        }else{
            $(".instant-preview").hide();
        }       
    });
    
    $("#insert-section-go").click(function(){
        fnToggleLoadingScreen(true);       
        $.ajax({
            url: "?action=addsection&apply-to=" + $("#apply-section-to").val()+ "&existing-id=" + $("#existing-id").val() + "&location=" + $("#insert-section").val() +"&PageID=" + Data.Folder.ObjectID+"&revision=" + getQuerystring("revision"),
            type: "POST", data: {},
            error: function (oRequest, sStatus, oError) { alert("An error occurred while adding the section. Please try again"); },
            success: function (oRequest, sStatus, oError) {                
                $("iframe.preview")[0].contentWindow.location.reload(); 
                _CMSContentOrder=null;
            },complete:function(){                
                fnToggleLoadingScreen(false);                
            }
        });           
    });
    $(".preview-toggle").click(function(){
        var b=$(this).hasClass("expand");                
        $($("iframe.preview")[0].contentDocument).find("._cms-controls .toggle").filter(function(){return $(this).hasClass("min") !== b;}).click();
    });
    $("iframe.preview").load(function() {
        if(Folder && (Folder.AuxField1=="dictionary" || (Folder.Parent && Folder.Parent.AuxField1=="finaltest"))){
            fnEditContent("firstchild");
        }else{
        var win=this;
        var resizeIframe=function(){
            var h=$(win).contents().find(":last").offset().top+$(win).contents().find(":last").height();            
            $(win).height( $(win).contents().find("body").height() );
            $("#preview-container").height(Math.max(200,$(win).contents().find("body").height()/4));            
        };        
        var loaded=function (){
            if(!EditorData){
                var o=$(win.contentDocument).find("._content[content_id]:eq(0)");                
                if(cInt(o.attr("content_id"))>0){
                    fnEditContent(cInt(o.addClass("_cmsediting").attr("content_id")));                        
                }else if(Folder){
                        if(Folder.Parent && Folder.Parent.AuxField1=="finaltest")
                            fnEditContent("firstchild");
                        else{
                    $(".tSectionName").val(Folder.Name);
                    $(".dTemplate").val(Folder.AuxField1);                    
                }
            }
                }
            resizeIframe();
            // $(win.contentDocument).on('selectstart dragstart',function(e){                
            // e.preventDefault(); return false;
            //});
            if(EditorData)$(win.contentDocument).find("._content[content_id="+EditorData.Content.ObjectID+"]").addClass("_cmsediting");
            
            $("#insert-section option:gt(0)").detach();
            $(win.contentDocument).find("._content[content_id]").each(function(){
                $("#insert-section").append($("<option value='after:" + $(this).attr("content_id") + "'>After '" + $(this).find(".title-minimized").text().split("[")[0] + "'</option>"));
            });
            
            $(win.contentDocument).find("._cms-controls .toggle").click(function(){
                setTimeout(resizeIframe,1000);
            });
            $(win.contentDocument).find("._cms-controls .edit").click(function(e){
                $(win.contentDocument).find("._cmsediting").removeClass("_cmsediting");
                var c=$(this).closest("._content");
                $(c).addClass("_cmsediting");
                fnEditContent($(c).attr("content_id"));
                e.stopPropagation();
                return false; 
            });            
                $(win.contentDocument).find("._cms-controls .delete").click(function(e){
                    if(window.confirm("Are you sure you want to delete this section?")){                        
                        var c=$(this).closest("._content");                        
                        fnToggleLoadingScreen(true);                
                        
                        $.ajax({
                            url: "?action=deletecontent&apply-to=" + $("#apply-section-to").val() + "&ContentID=" + $(c).attr("content_id")+"&PageID=" + Data.Folder.ObjectID,
                            type: "POST", data: {},
                            error: function (oRequest, sStatus, oError) { alert("An error occurred deleting the content. Please try again"); },
                            success: function (oRequest, sStatus, oError) {                            
                                if(EditorData && EditorData.Content && EditorData.Content.ObjectID==$(c).attr("content_id")){
                                    EditorData={};
                                    fnLoadEditor(EditorData);
                                }                                        
                                $("#insert-section").find("option[value='after:"+$(c).attr("content_id") + "']").detach();
                                c.detach();
                            },complete:function(){                
                                fnToggleLoadingScreen(false);  
                            }            
                        }); 
                                                
                        e.stopPropagation();
                        return false; 
                    }
                }); 
        };
        var ready=function(){return win.contentWindow && win.contentWindow._Player && win.contentWindow._Player.Player && win.contentWindow._Player.Player.Loaded;}
        function executeWhenReady(ready,execute,delay){
            if(ready()){
                execute();
            }else{
                setTimeout(function(){ executeWhenReady(ready,execute,delay); },delay);
            }
        }
        executeWhenReady(ready,loaded,1);
        }
    });
    var _CMSContentOrder;
    setInterval(function(){  
        var win=$("iframe.preview")[0];
        if(win && win.contentDocument){
            var c=[];
            $(win.contentDocument).find("._content[content_id]").each(function(){
                c.push($(this).attr("content_id"));
            });
            if(c.length>0){                
                if(_CMSContentOrder){
                    var o="",n="";
                    $.each(_CMSContentOrder,function(){o+=this+",";});
                    $.each(c,function(){n+=this+",";});
                    if(n!=o){
                        fnToggleLoadingScreen(true);                
                        $.ajax({
                            url: "?action=setcontentorder&Order=" + escape(n)+"&PageID=" + Data.Folder.ObjectID,
                            type: "POST", data: {},
                            error: function (oRequest, sStatus, oError) { alert("An error occurred getting the content. Please try again"); },
                            success: function (oRequest, sStatus, oError) {                            
                            },complete:function(){                
                                fnToggleLoadingScreen(false);                
                            }            
                        });       
                    }
                }
                _CMSContentOrder=c;
            }
        }
    },1000);
    function fnToggleLoadingScreen(toggle, msg){
        if(toggle) $("<div id='content-loading' style='position:absolute;background-color:rgba(255,255,255,0.5);z-index:100;width:100%;height:100%;top:0;left:0;right:0;bottom:0;text-align:center;padding:400px 0'><h2>"+cStr(msg,"<img src='../inc/images/loading.gif' /><br />Processing, please wait") + "</h2></div>").appendTo(document.body);        
        else $("#content-loading").detach();
    }
    function fnEditContent(id){
        fnToggleLoadingScreen(true);
        $.ajax({
            url: "?action=gettempcontent&ObjectID=" + id+"&PageID=" + Data.Folder.ObjectID+"&revision=" + getQuerystring("revision"),
            type: "POST", data: {},
            error: function (oRequest, sStatus, oError) { alert("An error occurred getting the content. Please try again"); },
            success: function (oRequest, sStatus, oError) {
                fnLoadEditor(oRequest);                
            },complete:function(){                
                fnToggleLoadingScreen(false);                
            }
        });        
    }
    function fnGetContent(){
        if(EditorData && EditorData.Content){
            if(CKEDITOR.instances[$(".content_editor").attr("id")]) CKEDITOR.instances[$(".content_editor").attr("id")].updateElement();            
            fnSaveContextEditor();
            return {
                AuxField1:$(".dTemplate").val(),
                Name:$(".tSectionName").val(),
                Xml:$(".content_editor").val(),
                Mode:$("#dMode").val()
            };            
        }
        return null;
    }
    function fnContentHasChanged(){
        var changed=false;
        if(EditorData && EditorData.Content){
            var newContent=fnGetContent();
            for(var prop in newContent)
                if(prop!="Xml" && newContent[prop]!=EditorData.Content[prop])changed=true;

            if(!changed) changed=newContent.Xml.replace(/\s|\"|'/g, '')!= cStr(EditorData._Original).replace(/\s|\"|'/g, '');
        }
        return changed;
    }    
    function fnSaveContent(bAsCopy){               
        if(!EditorData)EditorData={};
        if(!EditorData.Content)EditorData.Content={};
        if(fnContentHasChanged() || bAsCopy){         
            var newContent=fnGetContent();
            var err=(newContent.AuxField1==""?"\n * Template":"")
                + (newContent.Name==""?"\n * Name":"");
            if(err){
                alert("Your edits could not be saved. Please correct the following:" + err);
                return false;
            }

            if(!bAsCopy && Course && Course.AuxField1.split("-").length>2 && gAlsoUsedIn.Data.length>2){
                if(!window.confirm("You are about to save changes to a file that is used in other courses. Are you sure you wish to continue?")) return false;
            }else if(bAsCopy&& Course && Course.AuxField1.split("-").length==2){
                if(!window.confirm("You are about to save a file used in a Generic Course as a new copy. This will break its connection to other courses. Are you sure you wish to continue?")) return false;
            }    
            if($(".content_editor").val().indexOf("mail-attachment.")>-1){
                alert("The content contains the string 'mail-attachment.'. This is probably because you've pasted an image from Gmail. You will first need to save this image to your computer before pasting it. Page will not be saved until this is fixed.");
                return false;
            }

            $.extend(EditorData.Content,newContent);             
            fnToggleLoadingScreen(true);                
            $.ajax({
                url: "?saveascopy="+(bAsCopy?1:0)+"&action=savetempcontent&ObjectID=" + EditorData.Content.ObjectID+"&PageID=" + Data.Folder.ObjectID,
                type: "POST", data: {__data:  JSON.stringify({Content:EditorData.Content })},
                error: function (oRequest, sStatus, oError) { alert("An error occurred getting the content. Please try again"); },
                success: function (oRequest, sStatus, oError) {              
                    EditorData._Original=EditorData.Content.Xml;
                    if(oRequest.Redirect)window.location.href=oRequest.Redirect;
                    fnLoadEditor(oRequest);
                    $("iframe.preview")[0].contentWindow.location.reload();                
                },complete:function(){                
                    fnToggleLoadingScreen(false);                
                }                
            });    
        } else{
            fnToggleLoadingScreen(true,"<img src='../inc/images/256-alert-red.png'/> <br />No changes since last save");                
            setTimeout(function(){fnToggleLoadingScreen(false);},4000);
        }
        return true;
    }
    var EditorData=null;
    function fnToggleEditor(b){
        $("._editor .hider").detach();
        if(!b)$("._editor").prepend($("<div class='hider' style='position:absolute;background-color:#f7f7f7;z-index:1000;'/>").width($("._editor").width()).height($("._editor").height()));
    }
    function fnLoadEditor(data){   
        if(fnContentHasChanged() && !window.confirm("Your latest changes have not been saved. Do you want to continue?")) return;
        //if(!fnSaveContent())return;
        EditorData=data;
        if(CKEDITOR.instances[$(".content_editor").attr("id")]!=null)CKEDITOR.instances[$(".content_editor").attr("id")].destroy();
        
        fnToggleEditor(EditorData.Content!=null);
        if(!EditorData.Content)return;
                 
        $(".content_editor").val(EditorData.Content?EditorData.Content.Xml:"");
        
        $("#tabs").data("prev-active",null);//.data("active",null);
        if(EditorData.Content==null){
            $("#tab-window-1").hide();            
        }else{
            $("#dMode").val(EditorData.Content.Mode);
            //$("#tabs>ul>li>a[href='#tab-window-1']").click();                        
            $(".dTemplate").val(EditorData.Content.AuxField1);
            $(".tSectionName").val(EditorData.Content.Name);            
            fnUpdatePageType($(".dTemplate").val());
        }        
        setTimeout(function(){
            if(CKEDITOR.instances[$(".content_editor").attr("id")]) CKEDITOR.instances[$(".content_editor").attr("id")].updateElement();      
            EditorData._Original=$(".content_editor").val();            
        },1000);

        if(EditorData.Content!=null){
            $("#modified").html("<b>Modified: </b>" + EditorData.Content.CreateDate + " by <u>" + EditorData.Content.ModifiedBy+"</u>").show();
            var d=new Date(EditorData.Content.DeleteDate);
            if(d.getYear()<9999) $("#deleted").html("<b>Deleted: </b>" + EditorData.Content.DeleteDate).show();            
        }        
    }
	</script>   
</asp:Content>