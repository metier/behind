﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.controls.jgrid;
using jlib.functions;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Cms
{

    public partial class QuizMerge : jlib.components.webpage
    {
        public void Page_Init(object sender, EventArgs e)
        {

        }

        public void Page_Load(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = xml.loadXml(io.read_file(Server.MapPath("/learningportal/cms/quizmerge.xml")));
            AttackData.Context context = new AttackData.Context();

            if (Request["action"] == "exit") Response.Redirect("quizmerge.aspx");
            else if (Request["action"] == "followup")
            {
                xml.setXmlAttributeValue(xmlDoc.SelectSingleNode("quizes/quiz[id/text()='" + this.QueryString("id") + "']"), "followup", "true");
                io.write_file(Server.MapPath("/learningportal/cms/quizmerge.xml"), xmlDoc.OuterXml);
                Response.Redirect("quizmerge.aspx");
            }
            else if (Request["action"] == "save")
            {
                xml.setXmlAttributeValue(xmlDoc.SelectSingleNode("quizes/quiz[id/text()='" + this.QueryString("id") + "']"), "processed", "true");
                io.write_file(Server.MapPath("/learningportal/cms/quizmerge.xml"), xmlDoc.OuterXml);
                AttackData.Content content = AttackData.Content.LoadByPk(this.QueryString("id").Int());
                content.Xml = Request["data"];
                content.Save(11536);
                Response.Redirect("quizmerge.aspx");
            }
            if (this.QueryString("id").Int() > 0)
            {
                AttackData.Content content = AttackData.Content.LoadByPk(this.QueryString("id").Int());
                lContent.Text += "<b>" + content.getPath().Path + "</b><br />";

                DataTable dtContent = context.GetSqlHelper().Execute("select * from d_content where objectid=" + content.ObjectID + " and deletedate<getdate() order by deletedate");
                string s = dtContent.SafeGetValue(0, "xml");
                for (int x = 0; x < dtContent.Rows.Count; x++)
                {
                    if (parse.split(s, "connecttheboxes").Length + parse.split(s, "background=").Length < parse.split(dtContent.Rows[x]["xml"], "connecttheboxes").Length + parse.split(dtContent.Rows[x]["xml"], "background=").Length)
                        s = dtContent.Rows[x]["xml"].Str();
                }
                if (parse.split(s, "connecttheboxes").Length + parse.split(s, "background=").Length < parse.split(content.Xml, "connecttheboxes").Length + parse.split(content.Xml, "background=").Length)
                {
                    lContent.Text += "<b>There is an equal number of instances of conntecttheboxes and background= in old and current quiz XML</b>";
                }
                else
                {
                    XmlDocument xmlOldQuiz = xml.loadXml(s);
                    XmlNodeList xmlQuestions = xmlOldQuiz.SelectNodes("//question[@background and string-length(@background)!=0] | //question[@type='connecttheboxes']");
                    List<string> lstQuestions = new List<string>();
                    foreach (XmlNode node in xmlQuestions) lstQuestions.Add(Util.Cms.FixImportedQuiz(content.GetParentUntil<AttackData.Folder>(10, new Func<AttackData.Folder, bool>(item => item.Context_Value == "course")), node.OuterXml));
                    lScript.Text += "var Questions=" + jlib.functions.json.DynamicJson.Serialize(lstQuestions) + ";var CurrentContent=" + content.ToJson() + ";";
                }


                lContent.Text += "<a href='quizmerge.aspx'>Go Back</a>";
            }
            else
            {

                pDetails.Visible = false;
                XmlNodeList xmlNodes = xmlDoc.SelectNodes("quizes/quiz");

                if (Request["action"] == "clean")
                {
                    foreach (XmlNode node in xmlNodes)
                    {
                        DataTable dtContent = context.GetSqlHelper().Execute("select * from d_content where objectid=" + xml.getXmlValue(node, "id").Int() + " and deletedate<getdate() order by deletedate");
                        if (dtContent.Rows.Count < 1)
                        {
                            node.ParentNode.RemoveChild(node);
                        }
                    }
                    xmlNodes = xmlDoc.SelectNodes("quizes/quiz");
                    io.write_file(Server.MapPath("/learningportal/cms/quizmerge.xml"), xmlDoc.OuterXml);
                }


                List<int> notFound = new List<int>();
                foreach (XmlNode node in xmlNodes)
                {
                    //Response.Write(xml.getXmlValue(node, "id").Int());
                    //Response.Flush();
                    AttackData.Content content = AttackData.Content.LoadByPk(xml.getXmlValue(node, "id").Int());
                    if (content == null) notFound.Add(xml.getXmlValue(node, "id").Int());
                    else lContent.Text += "<li class='" + (xml.getXmlAttributeValue(node, "processed").Bln() ? "processed " : "") + (xml.getXmlAttributeValue(node, "followup").Bln() ? "followup " : "") + "'><a href='quizmerge.aspx?id=" + content.ObjectID + "'>" + content.getPath().Path + "</a></li>";
                }
                lContent.Text = "<ul>" + lContent.Text + "</ul>";
                if (notFound.Count > 0) lContent.Text += "Not found: " + notFound.Join("<br />");
            }

        }
    }
}