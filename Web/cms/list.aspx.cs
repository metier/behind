﻿//Need an editor that supports custom tags.
//http://www.voofie.com/content/2/ckeditor-plugin-development/

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Linq;
using System.Collections;
using System.Collections.Specialized;
using jlib.functions;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Cms
{
    public partial class List : jlib.components.webpage
    {
        AttackData.Folder FolderObject;
        public Util.Classes.user ActiveUser
        {
            get
            {
                return (Page.Master as Inc.Master.Default).ActiveUser;
            }
        }

        private bool IsStageServer()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["MyMetier.dsn"].ConnectionString.Contains("TSTMET");
        }
        private string GetServerHost()
        {
            return "https://" + (IsStageServer() ? "staging." : "") + "mymetier.net";
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            gSettingsGrid.QuerySource = AttackData.Settings.GetByField(TableName: "d_folders", ObjectID: Request["ObjectID"].Int());
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            (Page.Master as Inc.Master.Default).DisableWrapper = true;
            //todo
            //lIlearningID.Visible = User.ID == 11536;
            if (convert.isDate(Request["revision"])) System.Web.HttpContext.Current.Items["version.date"] = Request["revision"].Dte();
            FolderObject = AttackData.Folder.LoadByPk(Request["ObjectID"].Int());
            if (FolderObject == null)
            {
                FolderObject = new AttackData.Folder() { Context_Value = Request["context"] };
                FolderObject.addParent(AttackData.Folder.LoadByPk(Request["parent_id"].Int()));
            }
            if (FolderObject.ObjectID > 0) gFiles.QuerySource = AttackData.Folders.GetForCMSList(FolderObject.ObjectID, (ActiveUser.IsBehindSuperAdmin || ActiveUser.IsBehindContentAdmin ? 0 : ActiveUser.ID).Int());
            if (FolderObject.Context_Value != "") dContext.Items.Add(FolderObject.Context_Value);
            if (!this.IsPostBack)
            {
                tTitle.Text = FolderObject.Name;
                tAuxField1.Text = FolderObject.AuxField1;
                tAuxField2.Text = FolderObject.AuxField2;
                dContext.setValue(FolderObject.Context_Value);
                //tWeight.setValue(FolderObject.getSetting("Weight"));
            }

            if (Request["mode"] == "ajax-postback")
            {
                Response.ContentType = "text/javascript";
                if (tTitle.Text.Trim() == "")
                {
                    Response.Write("alert('You need to give this folder a title');");
                }
                else
                {
                    FolderObject.Name = tTitle.Text;
                    FolderObject.AuxField1 = tAuxField1.Text;
                    FolderObject.AuxField2 = tAuxField2.Text;
                    FolderObject.Context_Value = dContext.SelectedValue;

                    foreach (string s in Request.Form.Keys)
                    {
                        if (s.StartsWith("Attribute-")) FolderObject.setSetting(s.Substring(10), Request.Form[s]);
                    }
                    FolderObject.Save(Session["user.id"].Int());
                    //Recalculate permissions
                    Util.Cms.RecalculateUserPermission( this.ActiveUser );
                    if (Request["ObjectID"].Int() != FolderObject.ObjectID) Response.Write("window.location.href='list.aspx?" + this.QueryStringStripKeys("ObjectID") + "&ObjectID=" + FolderObject.ObjectID + "';");
                    else Response.Write("//ok");
                }
                Response.End();
            }
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            lScript.Text = "$(document.body).data('__data', {\"UserID\":" + ActiveUser.ID + ",\"UserName\":\"" + ActiveUser.Username + "\",\"Folder\":$.extend(" + FolderObject.ToJson(true) + ",{Path:'" + (FolderObject.GetParents<AttackData.Folder>().Count == 0 ? "" : parse.replaceAll(parse.stripEndingCharacter(FolderObject.GetParents<AttackData.Folder>()[0].getPath().Path, ">"), "'", "&#39")) + "',PathID:'" + (FolderObject.GetParents<AttackData.Folder>().Count == 0 ? "" : parse.replaceAll(parse.stripEndingCharacter(FolderObject.GetParents<AttackData.Folder>()[0].getPath().ID, ">"), "'", "&#39")) + "',Parent:" + (FolderObject.Parents.Count == 0 || FolderObject.Parents[0].ID1 == 0 ? "null" : FolderObject.Parents[0].Object1.ToJson()) + "})});";
        }
    }

}