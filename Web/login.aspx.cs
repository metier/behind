﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.functions;
using System.Text;
using System.Security.Cryptography;
using session = Phoenix.LearningPortal.Util.Phoenix.session;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal
{
    public partial class Login : jlib.components.webpage
    {
        public void Page_PreRender(object sender, EventArgs e)
        {
            int distributorId = Util.Customization.Settings.GetDistributorIdForDomain(convert.cStr(Request.ServerVariables["HTTP_HOST"], Request.ServerVariables["SERVER_NAME"]));
            if (distributorId > 0)
            {
                var loginUrl = Util.Customization.Settings.GetSettings(distributorId, null).GetSetting("LoginUrl");
                if (loginUrl.IsNotNullOrEmpty()) Response.Redirect(loginUrl);
            }
            if (Request.Cookies["MasterCompanyId"] != null)
            {
                var masterCompanyId = Request.Cookies["MasterCompanyId"].Value.Int();
                var loginUrl = Util.Customization.Settings.GetSettings(null, masterCompanyId).GetSetting("LoginUrl");
                if (loginUrl.IsNotNullOrEmpty()) Response.Redirect(loginUrl);
            }
            if (!this.IsPostBack && Request.QueryString["invalid-credentials"].Bln())
                lWrongUsernamePassword.Visible = true;

            if (System.Configuration.ConfigurationManager.AppSettings["site.portal2"].Bln())
                Response.Redirect("portal2/#/portal/login");
        }
        public void Page_PreInit(object sender, EventArgs e)
        {
            (Page.Master as Inc.Master.MyMetier).clearWrapper().DisableLoginRedirect = true;

            if (this.QueryString("language").Str() != "")
            {
                this.Language = this.QueryString("language").Str();
                HttpCookie oCookie = new System.Web.HttpCookie("language", this.Language);
                oCookie.Expires = DateTime.Now.AddYears(1);
                Response.SetCookie(oCookie);

                oCookie = new System.Web.HttpCookie("session-language", this.Language);
                Response.SetCookie(oCookie);
            }
            else
            {
                this.Language = convert.cStr(Request.Cookies["language"] == null ? "" : Request.Cookies["language"].Value, Util.MyMetier.getUserLanguage(null));
            }
        }

        public void Page_Init(object sender, EventArgs e)
        {
            session session = new session();
            if (!this.IsPostBack)
            {
                if (Request["action"] == "logout")
                {
                    Util.Classes.user activeUser = Util.Classes.user.getUser(new Util.Phoenix.session().GetUserId());
                    string redirectUrl = "";
                    if (activeUser.ID > 0)
                    {
                        redirectUrl = Util.MyMetier.GetOrgSettings(activeUser.Organization.MasterCompanyID, activeUser.Organization.ID).SafeGetValue("Logout Redirect");
                    }
                    session.PhoenixPost(session.Queries.UserLogout, null);
                    Util.Phoenix.Helpers.DeletePhoenixSession(Request, Response);
                    if (!redirectUrl.IsNullOrEmpty()) Response.Redirect(redirectUrl);


                }
                if (!Request["token"].IsNullOrEmpty())
                {
                    tResetToken.Value = Request["token"];
                }
            }
            lPasswordReset.Visible = !Request["token"].IsNullOrEmpty();
            if (Request["username"].Str() != "" && !this.IsAjaxPostback)
            {

                AttackData.PageView PageView = (this.Master as Inc.Master.MyMetier).PageView;
                if (PageView == null) PageView = new AttackData.PageView(Page as jlib.components.webpage, Request, Session);
                PageView.PostData = convert.cStreamToString(Request.InputStream, Request.ContentEncoding);
                PageView.Save();

                session.RequestResult result = session.PhoenixPost(String.Format(session.Queries.UserLogon, System.Web.HttpUtility.UrlEncode(Request["username"])), new { Password = Request["password"] });
                bool accountExpired = convert.cInt(convert.SafeGetProperty(result.HttpErrorJSON, "ErrorCode")) == 3001;
                bool validUser = result.HttpResponseCode == 200;
                Util.Classes.user user = null;
                if (validUser)
                {
                    session.PhoenixClearCache();
                    user = Util.Classes.user.getUser(session.GetUserId(), true);
                    if (user.ID == 0) accountExpired = true;
                    else if (!user.UserRoles.Contains("LearningPortalUser") && !user.UserRoles.Contains("LearningPortalSuperUser")) accountExpired = true;
                }
                if (accountExpired)
                {
                    lAccountExpired.Visible = true;
                    Util.Phoenix.Helpers.DeletePhoenixSession(Request, Response);
                }
                else if (validUser)
                {
                    //Set some cookies
                    Response.SetCookie(new HttpCookie("CompanyId", user.OrganizationID.Str()) { Expires = DateTime.Now.AddYears(1) });
                    Response.SetCookie(new HttpCookie("MasterCompanyId", user.Organization.MasterCompanyID.Str()) { Expires = DateTime.Now.AddYears(1) });
                    Response.SetCookie(new HttpCookie("DistributorId", user.Organization.DistributorId.Str()) { Expires = DateTime.Now.AddDays(1) });
                    //redirect user to correct domain if using domain of wrong distributor
                    if (!System.Configuration.ConfigurationManager.AppSettings["site.dev"].Bln())
                    {
                        var domain = Util.Customization.Settings.GetSettings(user).GetSetting("LearningPortalDomain");
                        if (domain.IsNotNullOrEmpty() && convert.cStr(Request.ServerVariables["HTTP_HOST"], Request.ServerVariables["SERVER_NAME"]).ToLower().IndexOf(domain.ToLower()) == -1)
                        {
                            Response.StatusCode = 307;
                            Response.AddHeader("Location", "https://" + domain + jlib.net.HTTP.stripQueryStringParameters(Request.Url.PathAndQuery, "referrer"));
                            Response.End();
                        }                        
                    }

                    if (System.Configuration.ConfigurationManager.AppSettings["site.portal2"].Bln())
                        Response.Redirect("portal2/#/portal/login?login=true");

                    //clear cached queries for user                
                    if (!Request["referrer"].IsNullOrEmpty() && Request["referrer"].IndexOf("login.aspx") == -1 && Request["referrer"].IndexOf("learningportal/index.aspx") == -1) Response.Redirect(Request["referrer"] + hHashValue.Value);
                    Response.Redirect(System.Web.HttpContext.Current.Request.ApplicationPath + "/index.aspx" + (user.NagUserForExamCompetence ? "?NagUserForExamCompetence=true" : ""));
                }
                else
                {
                    //{"Message":"Could not authenticate","ValidationErrors":["User does not exist or password is wrong."]}
                    lWrongUsernamePassword.Visible = true;
                    if (Request.QueryString["username"].IsNotNullOrEmpty()) Response.Redirect("login.aspx?invalid-credentials=true");
                }
            }
            pTrackingScript.Visible = !System.Configuration.ConfigurationManager.AppSettings["site.dev"].Bln();
            dLanguage.setValue(convert.cStr(this.Language, "en"), true);

            //invalid username/password            

            lTimeout.Visible = this.QueryString("timeout") == "true";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            if (this.IsAjaxPostback)
            {
                if (!tPasswordUsername.Text.IsNullOrEmpty())
                {
                    session.RequestResult result = session.PhoenixPut(String.Format(session.Queries.UserPasswordReset, tPasswordUsername.Text));
                    if (result.Error)
                    {
                        lUsernameNotFound.Visible = true;
                    }
                    else
                    {
                        lPasswordReminderSent.Visible = true;
                        lPasswordUsernameLabel.Visible = false;
                        tPasswordUsername.Visible = false;
                        hPasswordReminderClose.Visible = true;
                        hPasswordReminderSubmit.Visible = false;
                    }
                }
                else
                {
                    session.RequestResult result = session.PhoenixRequest("PUT", String.Format(session.Queries.UserPasswordResetFromToken, tResetEmail.Text, tResetToken.Value, tResetPassword.Text), "", false, false);
                    if (result.Error)
                    {
                        lTokenNotFound.Visible = true;
                    }
                    else
                    {
                        lResetSuccess.Visible = true;
                        Session["alert.once"] = lResetSuccess.Text;
                        Response.Write("window.location.href='login.aspx';");
                        Response.End();
                        //Response.Redirect("login.aspx");
                    }
                }
            }
            if (!this.IsPostBack && this.QueryString("password-lookup").Bln()) ClientScript.RegisterStartupScript(this.GetType(), "password-lookup", "$('.passwordreclaim').click();", true);
        }
    }
}