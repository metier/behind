﻿<%@ Page Title="Metier Diploma Generator" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Diploma" Codebehind="diploma.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">
<style>
#diploma-details label
{
    width:100px;
    float:left;
}
</style>
<h2><img src="./inc/images/icons/tools_32.png" alt="Diploma Generator" /><common:label runat="server" ID="lTitle">Diploma Generator</common:label></h2>

<div class="clear"></div>
	<common:label runat="server" ID="lFormError" Visible="false" CssClass="form_error" Tag="div" style="clear:both" />    
	<div class="content-box">
		<div class="content-box-header">
			<common:label runat="server" ID="lTitle1" Tag="h3">Diploma Generator</common:label>
		</div>
					
		<div class="content-box-content">			    
				<fieldset id="diploma-details">
					<p>
						<label>Student Name *</label>
                        <common:textbox runat="server" ID="tStudentName" CssClass="small" Required="true" FieldDesc="Student Name" />												
					</p>
					<p>
						<label>Course Name *</label>
                        <common:textbox runat="server" ID="tCourseName" CssClass="small" Required="true" FieldDesc="Course Name" />												
					</p>
					<p>
						<label>PDU</label>
                        <common:textbox runat="server" ID="tPDU" CssClass="small" />												
					</p>
                    <p>
						<label>Grade</label>
                        <common:textbox runat="server" ID="tGrade" CssClass="small" />												
					</p>
                    <common:label runat="server" Tag="p" ID="lObjectives">
						<label>Objectives</label>
                        <common:textbox runat="server" ID="tObjectives" CssClass="small" TextMode="MultiLine" Height="100px" />												
					</common:label>
                    <p>
                        <label>Diploma Date</label>
                        <common:textbox runat="server" ID="tDiplomaDate" CssClass="small" ValidateAs="date1" />												
                    </p>
					
                     <common:label runat="server" ID="lDiplomaFields">                   	
					<p>
						<label>Language</label>
						<common:dropdown runat="server" ID="dLanguage">
                        <asp:ListItem Value="en">English</asp:ListItem>
                        <asp:ListItem Value="no">Norwegian</asp:ListItem>
                        <asp:ListItem Value="fr">French</asp:ListItem>
                        <asp:ListItem Value="nl">Dutch</asp:ListItem>
                        <asp:ListItem Value="de">Deutsch</asp:ListItem>
                        <asp:ListItem Value="da">Danish</asp:ListItem>
                        <asp:ListItem Value="sv">Swedish</asp:ListItem>
                        <asp:ListItem Value="es">Spanish</asp:ListItem>
                        <asp:ListItem Value="pl">Polish</asp:ListItem>
                        <asp:ListItem Value="pt">Portugese</asp:ListItem>

                        </common:dropdown>							
					</p>
                    
                    <p>
                        <label>Level</label>
                        <common:dropdown runat="server" ID="dLevel">
                        <asp:ListItem Selected="True" Value="b" Text="Bronze" />
                        <asp:ListItem Value="s" Text="Silver" />
                        <asp:ListItem Value="g" Text="Gold" />
                        </common:dropdown>
                    </p>
                    <p>
                        <label>Distributor</label>
                        <common:dropdown runat="server" ID="dDistributor">
                        <asp:ListItem Selected="True" Value="Metier" Text="Metier" />
                        <asp:ListItem Value="HSE" Text="Proactima" />                        
                        </common:dropdown>
                    </p>
                    </common:label>
				</fieldset>
						
				<input type="submit" value="Generate Diploma" runat="server" id="bSubmit" />
			
		</div>
	</div>

    <!-- NEW STANDARD DIPLOMA -->
    
<common:label runat="server" ID="lDiplomaRegular" visible="false" Tag="div"><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Diploma</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style type="text/css">
            * {
                padding: 0;
                margin: 0;
                border: 0;
                font-family: Calibri;
                color: #00205B;
            }    
            @font-face {
                  font-family: "URW Impact W01 Bold";
                  src: url("/learningportal/diploma/regular/urw-impact-w01-condensed-bold/a45c767ddaa0d1d8299e1fed1a3610f9.eot"); /* IE9*/
                  src: url("/learningportal/diploma/regular/urw-impact-w01-condensed-bold/a45c767ddaa0d1d8299e1fed1a3610f9.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
                  url("/learningportal/diploma/regular/urw-impact-w01-condensed-bold/a45c767ddaa0d1d8299e1fed1a3610f9.woff2") format("woff2"), /* chrome、firefox */
                  url("/learningportal/diploma/regular/urw-impact-w01-condensed-bold/a45c767ddaa0d1d8299e1fed1a3610f9.woff") format("woff"), /* chrome、firefox */
                  url("/learningportal/diploma/regular/urw-impact-w01-condensed-bold/a45c767ddaa0d1d8299e1fed1a3610f9.ttf") format("truetype"), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
                  url("/learningportal/diploma/regular/urw-impact-w01-condensed-bold/a45c767ddaa0d1d8299e1fed1a3610f9.svg#URW Impact W01 Bold") format("svg"); /* iOS 4.1- */
            }
	        #lesson-list
	        {
		        width:100%;
		        font-size:14px;
		        padding: 0 50px 0 150px;

	        }
	        #lesson-list td
	        {		
		        vertical-align: top;
                width:50px;
	        }	
        </style>
    </head>
    <body style="width: 1000px;border: 0px solid black;height: 1471px;padding: 20px;" id="diploma-rendered">
     <div>

        

<div style="position:relative;height:1122px">
                                                                       


<div style="border: 1px solid #00205B;height: 1431px;">

<div id="header" style="padding: 44px 65px 0 80px">
	<div style="background-image:url(#diploma-logo#); background-size: contain;background-repeat:no-repeat;background-position: left bottom;width: 230px;height: 144px;"></div>
</div>
<table style="text-align: center;width: 1000px;border:0px solid black;">
<tbody><tr><td style="line-height:52px;height:589px;padding: 60px 65px 0 65px">
<div style="font-size: 127px;color: #00205B;font-family: 'URW Impact W01 Bold' !important;line-height: 1;"><translate key="diploma"></translate></div><div style="font-size:35px;font-family: 'Tajawal', sans-serif;"><translate key="certification-confirmation"></translate></div><div style="font-size:35px;margin: 50px 0 10px 0;">#user.name#</div>
<div style="font-size:25px;border-top: 1px solid #00205B;padding-top: 20px;">
	
	<asp:PlaceHolder runat="server" id="hSuccessMessageElearningRegular">
		<translate key="success-message">has successfully completed the e-learning course</translate>
	</asp:PlaceHolder>
	<asp:PlaceHolder runat="server" id="hSuccessMessageClassroomRegular">
		<translate key="success-message-classroom">has successfully completed the course</translate>
	</asp:PlaceHolder>
	
</div>
    
	
<div style="font-size:40px;color: #006472;font-weight: bold;font-family: 'URW Impact W01 Bold' !important;">#rco.title#</div>
    <div style="font-size:18px;line-height:normal;padding: 15px 0;">#additional-info#</div>
<div style="font-size:25px;"><translate key="certifying-organization"></translate></div>
    
</td></tr>
</tbody></table>


<div class="diplom" style="text-align: center;width: 1000px;top:1066px;position:absolute;font-size:19px;font-weight:600;height: 340px;">
<img src="#signature#" />
<br>
    <div>#principal#<br />
#principal.title# #academy#<br />
<br />#date#</div>
<div style="position: absolute;width: 1000px;bottom: 0;">
<img src="/learningportal/diploma/prince2/metier-logo.png" style="width: 170px;">
</div></div>


</div></div>
</div></body></html>
</common:label>

<!-- SKEMA COURSE ATTENDANCE -->
<common:label runat="server" ID="lCourseAttendanceSkema" Visible="false" Tag="div">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Course Attendance</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            *{
                padding:0;margin:0;border:0;
                font-family:arial;
                color: black;
            }        
        </style>
    </head>
    <body style="width:1040px;height: 1471px;border: 0px solid black;" id="diploma-rendered">
        <div>
            <img alt="Metier Logo" src="/learningportal/diploma/prince2/metier-logo.png" style="max-width:204px;padding: 65px 85px 0 0;float:right" />
            <div id="header" style="padding: 200px 0 0 0 ;border-bottom: 2px solid black;margin: 0 85px 0 85px;">#date#</div>
            <table>
                <tbody>
                    <tr>
                        <td style="height:700px;padding: 0 85px 0 85px">
                            <div style="font-size:36px;color: #00205B;font-weight: bold;text-transform:uppercase;font-family: 'Arial Black';">Letter of course attendance</div>
                            <div style="width: 230px;height: 100px;"></div>
                            <div>This letter is to confirm that <span style="font-weight: bold">#user.name#</span> has completed and completed the <span style="font-weight: bold">#rco.title#</span> e-learning course. The course was available online and completed at <span>#date#</span></div>
                            <div style="width: 230px;height: 150px;"></div>
                            <div>On behalf of the Organisation,</div>
                            <img alt="Signature Halvard Kilde, CEO Metier" style="padding: 35px 0 35px 0" src="#signature#" />

                            <div>#principal#<br />
                                #principal.title# #academy#<br />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
</common:label>


<!-- NEW PRINCE2 DIPLOMA (letter of course attendace) -->
<common:label runat="server" ID="lCourseAttendancePrince2" Visible="false" Tag="div">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Course Attendance</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            *{
                padding:0;margin:0;border:0;
                font-family:arial;
                color: black;
            }        
        </style>
    </head>
    <body style="width:1040px;height: 1471px;border: 0px solid black;" id="diploma-rendered">
        <div>
            <img alt="Metier Logo" src="/learningportal/diploma/prince2/metier-logo.png" style="max-width:204px;padding: 65px 85px 0 0;float:right" />
            <div id="header" style="padding: 200px 0 0 0 ;border-bottom: 2px solid black;margin: 0 85px 0 85px;">#date#</div>
            <table>
                <tbody>
                    <tr>
                        <td style="height:700px;padding: 0 85px 0 85px">
                            <div style="font-size:36px;color: #00205B;font-weight: bold;text-transform:uppercase;font-family: 'Arial Black';">Letter of course attendance</div>
                            <div style="width: 230px;height: 75px;"></div>
                            <div style="font-weight: bold;">This is a letter confirming course attendance only and is not a document demonstrating or certifying the achievement of any qualification in the subject matter of the training course</div>
                            <div style="width: 230px;height: 100px;"></div>
                            <div>This letter is to verify that <span style="font-weight: bold">#user.name#</span> has attended and completed the <span style="font-weight: bold">#rco.title#</span> e-learning course. The course was available online and completed at <span>#date#</span></div>
                            <div style="width: 230px;height: 150px;"></div>
                            <div>On behalf of the Organisation,</div>
                            <img alt="Signature Halvard Kilde, CEO Metier" style="padding: 35px 0 35px 0" src="#signature#" />

                            <div>#principal#<br />
                                #principal.title# #academy#<br />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div style="font-size: 11px;font-weight: light;position:absolute;padding: 10px; margin: 0 85px;top:1386px;border-top: 2px solid black;min-width: 850px;">PRINCE2® is a (registered) Trade Mark of AXELOS Limited, used under permission of AXELOS Limited. All rights reserved.</div>
        </div>
    </body>
</html>
</common:label>

    
    
    <!-- OLD PRINCE2 DIPLOMA -->
<common:label runat="server" ID="lDiplomaPrince2" Visible="false" Tag="div"><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>    
</head><body style="width:1040px;height: 1471px;border: 0px solid black;" id="diploma-rendered"><div>


<style>
    *{
        padding:0;margin:0;border:0;
        font-family:arial;
        color: #000000;
    }        
	#lesson-list
	{
		width:100%;
		font-size:14px;
		padding: 0 50px 0 80px;

	}
	#lesson-list td
	{
		width:50%;
		vertical-align: top;
	}	
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">


<div id="header" style="padding: 48px 20px 0 20px;border-bottom: 2px solid black;margin: 0 45px 0 40px;">
	<img src="/learningportal/diploma/prince2/axelos-logo.PNG" style="max-width:204px;padding:0 0 0 0;float:left">
<img src="/learningportal/diploma/prince2/metier-logo.png" style="max-width:204px;padding:0 0 0 0;float:right">
	<div style="width: 230px;height: 124px;"></div>
</div>
<table style="">
<tbody><tr><td style="line-height:52px;height:789px;padding: 60px 65px 0 105px"><div style="font-size:47px;color: #00205B;font-weight: bold;"><translate key="participation"></translate></div><div><translate key="prince2-confirmation"></translate></div>

<div style="font-size:37px;color: #27245a;font-weight: bold;padding-top: 50px;">#user.name#</div>
<div style="font-size:24px;padding: 5px 0;">
    <asp:PlaceHolder runat="server" id="hSuccessMessageElearningPrince2">
		<translate key="prince2-success-message">Has successfully completed the e-learning course:</translate>
	</asp:PlaceHolder>
	<asp:PlaceHolder runat="server" id="hSuccessMessageClassroomPrince2">
		<translate key="prince2-success-message-classroom">Has successfully completed the course:</translate>
	</asp:PlaceHolder>
</div>
	
    <div style="font-size: 25px;text-transform:uppercase;background-color: #00205B;color: white;position: relative;left: -20px;padding: 6px 20px;font-weight: bold;">#rco.title#</div>
    <div style="font-size:18px;line-height:normal;padding-top:39px">#additional-info#</div>
    
</td></tr>
</tbody></table>


    <img src="/learningportal/diploma/prince2/prince2-logo.jpg" style="max-width:234px;position:absolute;left:85px;top:1276px">

<div style="font-size: 11px;position:absolute;left: 55px;padding: 10px;text-align: center;top:1386px;border-top: 2px solid black;width: 920px;">PRINCE2® is a (registered) Trade Mark of AXELOS Limited, used under permission of AXELOS Limited. All rights reserve. The Swirl logoTM is a trade mark of AXELOS Limited.</div>
        

</div></body></html>
</common:label>
				
    <!-- ORIGINAL DIPLOMA -->

<common:label runat="server" ID="lDiplomaOriginal" Visible="false" Tag="div"><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<style>
    *{
        padding:0;margin:0;border:0;
        font-family:arial;
        color: #515c71;
    }    
    .prince2-disclaimer{
        padding-top:50px;
    }
    .prince2-disclaimer table{
        border:0px solid silver;padding:10px;
        text-align:justify;
        font-style:italic;
        font-size:13px;
    }
    .prince2-disclaimer .logo {
        text-align: center;
        padding-bottom:15px;
    }
	#lesson-list
	{
		width:100%;
		font-size:14px;
		padding: 0 50px 0 100px;

	}
	#lesson-list td
	{
		width:50%;
		vertical-align: top;
	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body style="width:1040px;" id="diploma-rendered">
<img src="https://new.mymetier.net/#background-image#" style="top:0px; left: 0px; z-index:-1;width:1040px;height:1471px;position:absolute;">
<div id="header" style="padding: 98px 65px 0 80px">
	<img src="#logo#" style="max-width:234px;padding:0 0 0 0;float:right" />
	<div style="background-image:url(#diploma-logo#); background-size: contain;background-repeat:no-repeat;background-position: left bottom;width: 230px;height: 144px;"></div>
</div>
<table style="text-align: center;width:1040px; border:0px solid black">
<tr><td style="line-height:52px;height:789px;padding: 60px 65px 0 65px">
<div style="font-size:47px">#user.name#</div>
<div style="font-size:18px;text-transform:uppercase" runat="server">
	<asp:PlaceHolder runat="server" id="hSuccessMessageElearningOld">
		<translate  key="success-message">has successfully completed the e-learning course</translate>
	</asp:PlaceHolder>
	<asp:PlaceHolder runat="server" id="hSuccessMessageClassroomOld">
		<translate  key="success-message-classroom">has successfully completed the course</translate>
	</asp:PlaceHolder>
</div>
	
<div style="font-size:40px">#rco.title#</div>
<div style="font-size:18px;text-transform:uppercase"><translate key="completed"></translate></div>
<div style="font-size:18px;line-height:normal;padding-top:39px">#additional-info#</div>
    <div class="prince2-disclaimer">#prince2-disclaimer#</div>
</td></tr>
</table>


<div class="diplom" style="text-align: center;width:1040px; top:1066px;  position:absolute;font-size:19px; font-weight:600">
<img src="#signature#" />
<br />
<div>#principal#<br />
#principal.title# #academy#<br />
<br />#date#</div>
</div>
#medal#

    <div style="font-size:13px;position:absolute;left:85px;top:1386px"><translate key="footer-left"><span style="color:#54a646">Metier AS</span><br />Hoffsveien 70 C</translate></div>
    <div style="font-size:13px;position:absolute;left:460px;top:1386px"><translate key="footer-center">Pb 118, Smestad<br />0309 Oslo</translate></div>
    <div style="font-size:13px;position:absolute;right:84px;top:1386px"><translate key="footer-right">www.metieracademy.com<br />Tel.: +47 24 12 45 00</translate></div>
</body>
</html>
</common:label>

<common:label runat="server" Visible="false" ID="lTranscriptHTML"><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<style>
    *{
        padding:0;margin:0;border:0;
        font-family:arial;
        color:#515c71;
        font-size:26px;
        font-weight:200;
    }    
    div
    {
        position:absolute;
    }
    
    .name{left:146px;}
    .today{left:887px;}
    .title{left:73px;}
    .date{left:527px;}
    .pdu{left:712px;}
    .grade{left:839px;}
    .first-line{position:relative; top:462px}
    .details-line{position:relative; top:696px}
    
    .transcript-skema-no .name, .transcript-hio-no .name{left:109px;}
    .transcript-skema-no .pdu, .transcript-hio-no .pdu{left:702px}
    .transcript-skema-no .details-line, .transcript-hio-no .details-line{top:720px;}
    
    .transcript-forsvaret-no-page-1 .first-line, .transcript-forsvaret-no-page-1 .details-line
    {
        position:static;
    }
    .transcript-forsvaret-no-page-1 .name{
        top:541px;
        left:0;
        right:0;        
        text-align:center;
        color:Black;
        font-family:  "Monotype Corsiva", Corsiva, "Lucida Handwriting", "Script MT Bold", "Brush Script MT", "Comic Sans MS", "Comic Sans", cursive;
        font-size:36px;
    }
    .transcript-forsvaret-no-page-1 .details-line div.date{
        top:980px;
        left:520px;
        color:Black;
        font-family: 'Monotype Corsiva', Corsiva, 'Lucida Handwriting', 'Script MT Bold', 'Brush Script MT', 'Comic Sans MS', 'Comic Sans', cursive;
        font-size:44px;
        display:block;
    }
    .transcript-forsvaret-no-page-1 .today, 
    .transcript-forsvaret-no-page-1 .details-line div
    {
        display:none;
    }
    .transcript-forsvaret-no-page-2 div
    {
        display:none;
    }
    
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body style="width:1040px;">
<img src="https://new.mymetier.net#background-image#" style="top:0px; left: 0px; z-index:-1;width:1040px;height:1459px;position:absolute;">
<common:label runat="server" ID="lTranscriptContainer" Tag="div" style="position:relative">
<div class="first-line">
<div class="name">#user.name#</div>
<div class="today">#today#</div>
</div>
<div class="details-line">
<div class="title">#rco.title#</div>
<div class="date">#date#</div>
<div class="pdu">#pdu#</div>
<div class="grade">#grade#</div>
</div>
<!--
<div style="position:absolute;left:120px; top:353px">#user.name#</div>
-->
</common:label>

</body>
</html>
</common:label>

</asp:Content>

