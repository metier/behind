﻿<%@ Page Title="Metier Reports" Language="C#" AutoEventWireup="true" Inherits="Phoenix.LearningPortal.Reports" MasterPageFile="~/inc/master/mymetier.master" Codebehind="reports.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">
<script type="text/javascript" src="./inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="~/inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.css" />				
<div id="wrapper-introduction">
  <common:label runat="server" TranslationKey="heading" Tag="h1" />   
  <common:label runat="server" ID="lNoAvailableReports" Tag="h3" style="font-style:italic" visible="false" TranslationKey="no-reports-available"/>
</div>
    
    <common:ajaxpane runat="server" id="aContainer">
        <common:hidden runat="server" ID="hCurrentPage" CssClass="current-page" />
        <common:hidden runat="server" ID="hCurrentSort" CssClass="current-sort" />
<div class="supertable">
	<div class="header nine-rounded">
        <common:label runat="server" TranslationKey="table-heading" Tag="h1" />				
		<div class="clear"></div>
	</div>
    <br />
    <div class="header nine-rounded" runat="server" id="hPEICEReport">
        <common:label runat="server" tag="h2" TranslationKey="peice-progress-report">PEICE progress report</common:label><br />        
        <br /><br /><button type="button" onclick="window.open('reports.aspx?action=download-peice');"><common:label runat="server" TranslationKey="download-to-excel">Download to Excel</common:label></button>
    </div>
    <div class="header nine-rounded">
        <common:label runat="server" tag="h2" TranslationKey="progress-report">Progress report</common:label><br />
        <common:label runat="server" tag="b" TranslationKey="progress-select-product">:</common:label>&nbsp;&nbsp;<select id="productId"></select>
        <br /><br /><button type="button" onclick="downloadFile();"><common:label runat="server" TranslationKey="download-to-excel">Download to Excel</common:label></button>
    </div>
</div>
	

<script language="javascript">
    fnEnableJSEffects();
</script>
</common:ajaxpane>
<script language="javascript">   
    $("form").submit(function () { return false; });
    $("#productId").append("<option value=''>(All)</option>")    
    $.each(productList, function () {
        $("#productId").append("<option value='" + this.Key + "'>" + this.Value + "</option>")
    });
    function downloadFile() {
        window.open("reports.aspx?action=download&report=progress&productId=" + $("#productId").val());
    }
</script>
</asp:Content>