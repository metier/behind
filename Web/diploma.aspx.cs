﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.controls.jgrid;
using jlib.functions;
using EvoPdf;
namespace Phoenix.LearningPortal
{
    public partial class Diploma : jlib.components.webpage
    {
        Util.Classes.user m_oActiveUser = null;
		Util.Classes.organization m_oCustomer = null;
		Util.Classes.Enrollment oClassroom = null; 
		private string PhoenixUrl;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            protected void Page_PreInit(object sender, EventArgs e)
        {
            //if (System.Configuration.ConfigurationManager.AppSettings["mymetier.ips"].Contains(";" + Request.ServerVariables["REMOTE_ADDR"] + ";")) (Page.Master as Inc.Master.Default).clearWrapper().DisableLoginRedirect = true;
            m_oActiveUser = (Page.Master as Inc.Master.Default).ActiveUser;
            if (m_oActiveUser.UserObject != null)
            {
                (Page.Master as Inc.Master.Default).DisableLoginRedirect = true;
                if (!m_oActiveUser.IsBehindAdmin || !Request["manual"].Bln()) (Page.Master as Inc.Master.Default).clearWrapper();
            }

			m_oCustomer = m_oActiveUser.Organization;
            //m_oCustomer = Util.Classes.user.getOrganization(7956);


            if (m_oActiveUser != null && m_oActiveUser.IsPhoenixUser)
			{
				if (this.QueryString("customer_id").Int() > 0)
				{
					m_oCustomer = Util.Classes.user.getOrganization(this.QueryString("customer_id").Int(), true);


					var result = new Util.Phoenix.session().PhoenixGet("participants?activityId=11640412&limit=1");
					foreach (dynamic enrollment in result.DataObject.Items)
					{
						oClassroom = new Util.Classes.Enrollment(enrollment, m_oActiveUser);
					}
				}
			}

			PhoenixUrl = System.Configuration.ConfigurationManager.AppSettings["phoenix.url"];
		}
        public void Page_Init(object sender, EventArgs e)
        {
            this.ErrorLabel = lFormError;
        }
        public void Page_Load(object sender, EventArgs e)
        {
            try
            {
                log.Info("Starting with diploma generation");
                DownloadAndGenerateDiploma();
            }
            catch (Exception ex)
            {

                log.Error("Error on Diploma generation: " + ex.Message, ex);
            }
        }

        private void DownloadAndGenerateDiploma()
        {
            string sLanguage = convert.cStr(dLanguage.getValue()),
                            sUserName = tStudentName.Text,
                            sVersion = dDistributor.Text,
                            sMedal = dLevel.Text,
                            sRCOTitle = tCourseName.Text,
                            sPDU = tPDU.Text,
                            classroomDays = "",
                            sclassroomStartDate = null,
                            sclassroomEndDate = null,
                            sGrade = tGrade.Text,
                            sObjectives = tObjectives.Text,
                            sDiplomaLogo = "",
                            DiplomaUrl = null,
                            DiplomaHTML = null,
                            Prince2Disclaimer = null;

            // var diplomaContainer = (this.QueryString("template") == "prince2" ? lCourseAttendancePrince2 : this.QueryString("template") == "skemaCompletion" ? lCourseAttendanceSkema : lDiplomaRegular);
            var diplomaContainer = getDiplomaTemplate(this.QueryString("template"));

            var DistributorId = 0;
            var isEnd = false;
            string sBackgroundImage = "";
            if (sBackgroundImage.IsNullOrEmpty()) sBackgroundImage = jlib.helpers.translation.translate(sLanguage, "/learningportal/diploma/certificate_a4_en.png", this, "png-name");

            var sCompletedDate = String.Format("{0:yyyy-MM-dd}", convert.cDate(tDiplomaDate.Text, true));


            Util.Classes.PhoenixObject CourseObject = null;
            if (m_oActiveUser != null && this.QueryStringLong("classroom_id") > 0)
            {
                if (oClassroom == null)
                {
                    oClassroom = m_oActiveUser.Enrollments.FirstOrDefault(enrollment => enrollment.ActivityId == this.QueryString("classroom_id").Int());
                    if (oClassroom == null)
                    {
                        Response.Clear();
                        Response.Write($"The user {m_oActiveUser.Username} is not enrolled in ActivityId {this.QueryString("classroom_id")}.");
                        if(!isEnd)
                        {
                            isEnd = true;
                            Response.End();
                        }
                        
                    }
                }
                if (oClassroom.ELearnCourseObject != null && oClassroom.ELearnCourseObject.Course != null)
                {
                    CourseObject = oClassroom.ELearnCourseObject.Course as Util.Classes.PhoenixObject;
                    sVersion = convert.cStr(convert.SafeGetProperty(CourseObject.Data, "Version"));
                    if (oClassroom.RcoCourseId > 0)
                    {
                        string PlayerSettings = Phoenix.LearningPortal.Data.Folder.LoadByPk(oClassroom.RcoCourseId).getSetting("PlayerSettings");
                        if (PlayerSettings.IsNotNullOrEmpty())
                        {
                            DiplomaUrl = convert.SafeGetProperty(jlib.functions.json.DynamicJson.Parse(PlayerSettings), "Diploma", "Url");
                            if (DiplomaUrl.IsNotNullOrEmpty())
                            {
                                DiplomaUrl = Server.MapPath(Request.ApplicationPath + DiplomaUrl);
                                if (!System.IO.File.Exists(DiplomaUrl))
                                    DiplomaUrl = null;
                            }
                        }
                    }
                }
                var prince2Articles = new List<string> { "PM20", "PM22", "PM23", "PM24", "PM28", "PM29" };
                if (prince2Articles.Any(x => oClassroom.ArticleNumber.Str().StartsWith(x)))
                    Prince2Disclaimer = @"<table border='0' class='prince2-disclaimer'>
<tbody>
<tr>
<td class='logo'><img src='https://mymetier.net/learningportal/images/uploads/prince2logo.png' /></td>
</tr>
<td>PRINCE2® is a registered trade mark of AXELOS Limited, used under permission of AXELOS Limited. All rights reserved. The Swirl logo™ is a trade mark of AXELOS Limited, used under permission of AXELOS Limited. All rights reserved.</td>
</tr>
</tbody>
</table>";
                sLanguage = convert.cStr(Util.MyMetier.getUserLanguage(oClassroom.LanguageId.Str()), m_oActiveUser.UserLanguage);
                sRCOTitle = oClassroom.ArticleCopyObject.Title;
                ////todo
                if (oClassroom.ArticleType == Util.Data.MetierClassTypes.ELearning && oClassroom.ArticleCopyObject.PDU > 0)
                {
                    sPDU = String.Format(jlib.helpers.translation.translate(sLanguage, "PDU: {0}", this, "pdu"), oClassroom.ArticleCopyObject.PDU);
                }

                if (oClassroom.ArticleType == Util.Data.MetierClassTypes.Classroom)
                {
                    hSuccessMessageElearningOld.Visible = false;
                    hSuccessMessageElearningRegular.Visible = false;
                    hSuccessMessageElearningPrince2.Visible = false;
                }
                else
                {
                    hSuccessMessageClassroomOld.Visible = false;
                    hSuccessMessageClassroomRegular.Visible = false;
                    hSuccessMessageClassroomPrince2.Visible = false;
                }


                IEnumerable<KeyValuePair<string, string>> DiplomaLogo = Util.MyMetier.GetSplashImages(sVersion, m_oActiveUser.Organization.MasterCompanyID, m_oActiveUser.Organization.ID, -1, new List<string>() { "diploma-logo." }, null);

                //if (DiplomaLogo.Count() > 0) sDiplomaLogo = "https://mymetier.net/learningportal/inc/library/exec/thumbnail.aspx?w=234&h=130&q=100&u=" + DiplomaLogo.ToList()[0].Key;                
                if (DiplomaLogo.Count() > 0) sDiplomaLogo = "https://mymetier.net/" + DiplomaLogo.ToList()[0].Key;

                int classroomDayCount = 0;

                m_oActiveUser.Enrollments.ForEach(enrollment =>
                {
                    if (enrollment.ActivitySetId > 0 && enrollment.ActivitySetId == Request["ilm_id"].Int() && enrollment.ArticleType == Util.Data.MetierClassTypes.Classroom && enrollment.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Completed)
                    {
                        enrollment.ParticipantAvailability.ActivityPeriods.ForEach(period =>
                        {
                            if (!period.Start.IsNullOrEmpty() && !period.End.IsNullOrEmpty())
                            {
                                classroomDayCount += Math.Ceiling(period.End.Value.Subtract(period.Start.Value).TotalDays).Int();
                                sclassroomStartDate = String.Format("{0:dd-MM-yyyy}", period.Start.Value);
                                sclassroomEndDate = String.Format("{0:dd-MM-yyyy}", period.End.Value);
                            };

                        });
                    }
                });
                if (classroomDayCount > 0) classroomDays = String.Format(jlib.helpers.translation.translate(sLanguage, "Number of days: {0}", this, "daycount"), classroomDayCount);
                //sGrade = "";
                if (oClassroom.ELearnCourseObject != null)
                    sMedal = (oClassroom.MedalType == 0 ? "g" : (oClassroom.MedalType == 1 ? "s" : (oClassroom.MedalType == 2 ? "b" : "")));

                //If classroom, grab start 
                if (oClassroom.ArticleType == Util.Data.MetierClassTypes.Classroom && oClassroom.ActivityPeriods != null)
                {
                    sCompletedDate = "";
                    foreach (var group in oClassroom.ActivityPeriods.Where(x => x.Start.HasValue).GroupBy(x => x.Start.Value.Date))
                        sCompletedDate += (sCompletedDate.IsNullOrEmpty() ? "" : "<br />") + String.Format("{0:yyyy-MM-dd}", group.First().Start.Value.Date) + (group.First().End.HasValue && group.First().End.Value.Date != group.First().Start.Value.Date ? " - " + String.Format("{0:yyyy-MM-dd}", group.First().End.Value.Date) : "");
                }
                else
                {
                    sCompletedDate = String.Format("{0:yyyy-MM-dd}", convert.cDate(oClassroom.CompletedDate.Dte(), true));
                }

                //if(Request.ServerVariables["REMOTE_ADDR"]=="79.171.80.68" || Request.IsLocal){
                diplomaContainer.Visible = true;
                sUserName = m_oActiveUser.FirstName + " " + m_oActiveUser.LastName;

                DistributorId = m_oActiveUser.DistributorId;
            }

            if (DiplomaUrl.IsNotNullOrEmpty())
            {
                DateTime? completedDate = null;
                var finalTest = oClassroom.ELearnCourseObject.Lessons.FirstOrDefault(lesson => lesson.IsFinalTest);

                if (oClassroom.CompletedDate.HasValue)
                    completedDate = oClassroom.CompletedDate.Dte();
                else if (oClassroom.ELearnCourseObject != null)
                {
                    if (finalTest != null)
                        completedDate = finalTest.UserLessonCompletedDate.Dte();
                }
                int? finalTestScore = null;
                if (finalTest != null) finalTestScore = finalTest.UserNumPoints;

                DiplomaHTML = jlib.helpers.Razor.RenderRazorTemplate(DiplomaUrl, new { Activity = oClassroom, User = m_oActiveUser, CompletedDate = completedDate, FinalTestScore = finalTestScore });
                if (!sLanguage.IsNullOrEmpty()) DiplomaHTML = jlib.helpers.translation.translateHTML(DiplomaHTML, "translate", sLanguage, parse.replaceAll(DiplomaUrl, Server.MapPath(Request.ApplicationPath), "", "\\", "/"));
            }
            else if ((this.IsPostBack && this.validateForm()) || (sUserName != "" && !this.IsPostBack))
            {
                string[] sBackgroundImages = parse.split(sBackgroundImage, "|");
                for (int x = 0; x < sBackgroundImages.Length; x++)
                {
                    HtmlTextWriter oWriter = new HtmlTextWriter(new System.IO.StringWriter());
                    diplomaContainer.RenderControl(oWriter);

                    string sHTML1 = oWriter.InnerWriter.ToString();
                    if (DiplomaHTML.Str().StartsWith("<span")) DiplomaHTML = DiplomaHTML.Substr(DiplomaHTML.IndexOf(">") + 1, DiplomaHTML.LastIndexOf("</") - 7);
                    var signatureUrl = "https://mymetier.net/learningportal/diploma/" + (sVersion.Contains("HSE") ? "signature_oppedal.png" : jlib.helpers.translation.translate(sLanguage, "signature_kilde.png", this, "signature-file"));
                    var signatureText = (sVersion.Contains("HSE") ? "Henriette Aashaug" : jlib.helpers.translation.translate(sLanguage, "Erik Aursnes Dammen", this, "principal-name"));
                    var signatureTitle = (sVersion.Contains("HSE") ? "" : jlib.helpers.translation.translate(sLanguage, "Principal", this, "principal-title"));
                    var logoUrl = "https://mymetier.net/learningportal/diploma/" + (sVersion.Contains("HSE") ? "proactima_learning_logo_.png" : (DistributorId == 3448 || DistributorId == 2062 ? "metierlogo.png" : "metieroec-logo.png"));


                    if (m_oCustomer != null)
                    {
                        if (m_oCustomer.DiplomaSignatureAttachmentUrl.IsNotNullOrEmpty() && m_oCustomer.DiplomaShowCustomSignature)
                        {
                            if (m_oCustomer.DiplomaCustomSignatureText.IsNotNullOrEmpty())
                            {
                                signatureText = m_oCustomer.DiplomaCustomSignatureText;
                                signatureTitle = m_oCustomer.DiplomaCustomSignatureTitle;
                            }
                            signatureUrl = m_oCustomer.DiplomaSignatureAttachmentUrl;
                        }

                        if (m_oCustomer.DiplomaLogoAttachmentUrl.IsNotNullOrEmpty() && m_oCustomer.DiplomaShowCustomerLogo)
                        {
                            sDiplomaLogo = m_oCustomer.DiplomaLogoAttachmentUrl;
                        }
                        if (m_oCustomer.DiplomaShowElearningLessons && oClassroom?.ELearnCourseObject != null && oClassroom.ELearnCourseObject.Lessons.Count > 1)
                        {
                            //sObjectives
                            sObjectives += (sObjectives.IsNotNullOrEmpty() ? "<br /><br />" : "") + "<table id='lesson-list'><tr><td><ul>";
                            var colIndex = 0;
                            for (int lessonIndex = 0; lessonIndex < oClassroom.ELearnCourseObject.Lessons.Count; lessonIndex++)
                            {
                                if (lessonIndex >= oClassroom.ELearnCourseObject.Lessons.Count / 2 && colIndex == 0)
                                {
                                    colIndex++;
                                    sObjectives += "</ul></td><td><ul>";
                                }
                                var name = oClassroom.ELearnCourseObject.Lessons[lessonIndex].Name.Str().Trim();
                                if (convert.isNumeric(name.SafeSplit(".", 0)))
                                    name = name.Substring(name.IndexOf(".") + 1).Trim();

                                sObjectives += $"<li>{name}</li>";
                            }
                            sObjectives += "</ul></td></tr></table>";
                        }
                    }

                    sHTML1 =
                        jlib.helpers.translation.translateHTML(
                        parse.replaceAll(sHTML1,
                        "#background-image#", sBackgroundImages[x],
                        "#logo#", logoUrl,
                        "#medal#", "",// (sMedal.IsNullOrEmpty() ? "" : "<img src=\"https://mymetier.net/learningportal/diploma/" + sMedal + "_medal.png\" style=\"position:absolute;top:1151px;left:793px;width:182px\">"),
                        "#signature#", signatureUrl,
                        "#date#", String.Format("{0:dd-MM-yyyy}", sCompletedDate),
                        "#startDate", sclassroomStartDate,
                        "#endDate,", sclassroomEndDate,
                        "#today#", String.Format("{0:yyyy-MM-dd}", DateTime.Now),
                        "#user.name#", sUserName,
                        "#rco.title#", sRCOTitle,
                        "#academy#", "", //(sVersion.Contains("HSE") ? "Proactima Learning" : "Metier Academy"),
                        "#principal#", signatureText,
                        "#principal.title#", signatureTitle,
                        "#additional-info#", convert.cIfValue(sPDU, "", "<br /><br />", "<br />") + convert.cIfValue(classroomDays, "", "<br /><br />", "") + parse.replaceAll(sObjectives, "\n", "<br />"),
                        "#pdu#", sPDU,
                        "#prince2-disclaimer#", Prince2Disclaimer,
                        "#grade#", sGrade,
                        "#diploma-logo#", sDiplomaLogo
                        ), "translate", sLanguage, "/diploma.aspx");
                    string sPageHTML = parse.inner_substring(sHTML1, "<body", ">", "</body>", null);
                    DiplomaHTML = parse.replaceAll((DiplomaHTML.Str() == "" ? parse.replaceAll(sHTML1, sPageHTML, "<div style='position:relative;height:1122px'>" + sPageHTML + "</div>") : DiplomaHTML.Substring(0, DiplomaHTML.LastIndexOf("</body>")) + "<div style='page-break-before:always;position:relative;height:1122px'>" + sPageHTML + "</div>" + DiplomaHTML.Substring(DiplomaHTML.LastIndexOf("</body>"))), "http://phoenix1.vontangen.com", "https://mymetier.net");
                }
            }
            if (DiplomaHTML.IsNotNullOrEmpty())
            {
                var cssUrl = Util.Customization.Settings.GetSettings(DistributorId, m_oActiveUser?.OrganizationID).LearningPortalCSSUrl;
                if (cssUrl.IsNotNullOrEmpty())
                    DiplomaHTML = DiplomaHTML.Replace("<head>", $"<head><link href=\"{this.Request.Url.AbsoluteUri.Substring(0, this.Request.Url.AbsoluteUri.IndexOf("/", 10))}/learningportal{cssUrl}\" rel=\"stylesheet\"/>");

                Dictionary<string, string> ParsedTokens = new Dictionary<string, string>();
                DiplomaHTML = ParseAndStripTokens(DiplomaHTML, ref ParsedTokens);
                if (this.QueryString("action") == "html") Response.Write(DiplomaHTML);
                else WriteHtmlAsPdf(DiplomaHTML, ParsedTokens, sRCOTitle);
                if (!isEnd)
                {
                    isEnd = true;
                    Response.End();
                }
                //Response.End();
            }
        }

        private jlib.components.label getDiplomaTemplate(string queryString)
        {
            if(queryString == "prince2")
            {
                return lCourseAttendancePrince2;
            }

            if (queryString == "skemaCompletion")
            {
                return lCourseAttendanceSkema;
            }

            return lDiplomaRegular;
        }
        private string ParseAndStripTokens(string HTML, ref Dictionary<string, string> ParsedTokens)
        {
            List<string> TokensToStrip = new List<string>() { "PdfPageSize:", "PdfPageOrientation:", "FitToWidth:" };    
            foreach (var Token in TokensToStrip) {             
                if (HTML.IndexOf(Token) > -1)
                {
                    ParsedTokens[Token] = parse.replaceAll(parse.inner_substring(HTML, Token, null, "\n", null), "\r", "");
                    HTML = parse.replaceAll(HTML, Token + ParsedTokens[Token], "");
                }
            }
            return HTML;
        }
        private void WriteHtmlAsPdf(string HTML, Dictionary<string,string> ParsedTokens, string sRCOTitle)
        {                       
            var PageSize = PdfPageSize.A4;
            var PageOrientation = PdfPageOrientation.Portrait;
            var FitToWidth = true;
            if (ParsedTokens.ContainsKey("PdfPageSize:"))
                PageSize = typeof(EvoPdf.PdfPageSize).GetField(ParsedTokens["PdfPageSize:"]).GetValue(null) as EvoPdf.PdfPageSize;

            if (ParsedTokens.ContainsKey("PdfPageOrientation:"))
                Enum.TryParse<PdfPageOrientation>(ParsedTokens["PdfPageOrientation:"], out PageOrientation);

            if (ParsedTokens.ContainsKey("FitToWidth:"))
                FitToWidth = convert.cBool(ParsedTokens["FitToWidth:"]);

            var pdfDocument = new Document();
            
            pdfDocument.DocumentInformation.Title = sRCOTitle + " Diploma";
            pdfDocument.DocumentInformation.Author = "LearningPortal";
            pdfDocument.DocumentInformation.Subject = sRCOTitle + "Diploma";
            pdfDocument.DocumentInformation.Keywords = "Diploma" + sRCOTitle;
            pdfDocument.DocumentInformation.CreationDate = DateTime.Now;
            pdfDocument.JpegCompressionEnabled = false;
            //pdfDocument.JpegCompressionLevel = 20;
            pdfDocument.CompressionLevel = PdfCompressionLevel.NoCompression;
            //pdfDocument.CompressCrossReference = false;


            var pdfPage = pdfDocument.AddPage(pageSize: PageSize, pageMargins: new Margins(0, 0, 0, 0), pageOrientation: PageOrientation);
            pdfPage.ShowHeader = false;
            pdfPage.ShowFooter = false;
            

            //oConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;


            //oConverter.PdfDocumentOptions.SinglePage = true;

            pdfDocument.LicenseKey = "72FyYHVwYHBgd25wYHNxbnFybnl5eXlgcA==";//"31FCUEVAUEFHQVBGXkBQQ0FeQUJeSUlJSVBA";

            var htmlToImageElement = new HtmlToImageElement(HTML, "https://mymetier.net");
            htmlToImageElement.FitWidth = FitToWidth;            
            //htmlToImageElement


            //htmlToImageElement.ConversionDelay 
            pdfPage.AddElement(htmlToImageElement);            
            while (pdfDocument.Pages.Count > 1) pdfDocument.RemovePage(1);
            var oPDF = pdfDocument.Save();

            Response.Clear();
            Response.AddHeader("Content-Type", "application/pdf");
            Response.AddHeader("Content-Disposition", "attachment; filename=" + sRCOTitle.Replace(",", "_") + "_Diploma.pdf; size=" + oPDF.Length.ToString()); 
            Response.BinaryWrite(oPDF);
        }
    }
}