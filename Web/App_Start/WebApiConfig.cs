﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
//using System.Web.Http.Cors;

namespace Phoenix.LearningPortal.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            RegisterWebApi(config);
        }

        public static void RegisterWebApi(HttpConfiguration config)
        {
            // Attribute Routing
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "API Default",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
            
            //Configure CamelCase Notation for JSON
            var jsonFormatter = GlobalConfiguration.Configuration.Formatters.JsonFormatter;            
            jsonFormatter.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Local;
            jsonFormatter.SerializerSettings.Culture = new System.Globalization.CultureInfo(string.Empty)
            {
                NumberFormat = new System.Globalization.NumberFormatInfo
                {

                }
            };

            //UrlForm Formatting
            var formPostFormatter = new System.Net.Http.Formatting.FormUrlEncodedMediaTypeFormatter();
            formPostFormatter.SupportedMediaTypes.Add(new System.Net.Http.Headers.MediaTypeHeaderValue("text/html"));
            formPostFormatter.SupportedMediaTypes.Add(new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-www-form-urlencoded"));

            //Multi-part forms
            //var multiPartFormFormatter = new MultiFormDataMediaTypeFormatter();
            


            //Clear others and set json as the default
            GlobalConfiguration.Configuration.Formatters.Clear();
            GlobalConfiguration.Configuration.Formatters.Add(jsonFormatter);
            GlobalConfiguration.Configuration.Formatters.Add(formPostFormatter);


            ///TODO: Add Response Log Handler
            ///
        }


    }
}