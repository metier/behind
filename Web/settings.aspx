﻿<%@ Page Language="C#" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Settings" MasterPageFile="~/inc/master/mymetier.master" Title="myMetier Settings" Codebehind="settings.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">
<link rel="stylesheet" media="screen" href="./inc/library/controls/jcombo/media/jcombo.css" type="text/css" />
<script type="text/javascript" src="./inc/library/controls/jcombo/media/jcombo.js"></script>
<script type="text/javascript" src="./inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<input type="text" style="display: none">
<input type="password" style="display: none">
<div id="wrapper-introduction">
<common:label runat="server" TranslationKey="heading" CssClass="standalone" Tag="h1" /> 
 <common:label runat="server" ID="lFormError" Visible="false" CssClass="form_error" />
</div>
<div id="wrapper-settings">
<div class="settingsform">
 <table>
	<tr>
		<td rowspan="5" class="headercolumn"><common:label runat="server" TranslationKey="user-info" /></td>
		<common:label runat="server" CssClass="fieldcolumn largetext" Tag="td" id="lUsername" databind="userSettings.User.MembershipUserId"></common:label>
		<td class="helptextcolumn"></td>
	</tr>
	<tr>
		<td class="dividersmall" colspan="3"></td>
	</tr>
	<tr>

		<td class="fieldcolumn"><common:label runat="server" CssClass="textlabel" Tag="label" TranslationKey="first-name" /><common:textbox runat="server" Required="true" FieldDesc="Fornavn" id="tFirstname" cssclass="textinput required" databind="userSettings.User.FirstName" /></td>
		<td class="helptextcolumn"></td>
	</tr>
	<tr>
		<td class="dividersmall" colspan="3"></td>
	</tr>	
	<tr>
		<td class="fieldcolumn"><common:label runat="server" CssClass="textlabel" Tag="label" TranslationKey="last-name" /><common:textbox runat="server" Required="true" FieldDesc="Etternavn" id="tLastname" cssclass="textinput required" databind="userSettings.User.LastName" /></td>
		<td class="helptextcolumn"></td>
	</tr>	
    <tr>
		<td class="dividersmall" colspan="3"></td>
	</tr>	
	<tr>
		<td class="headercolumn"></td>
		<td class="fieldcolumn"><common:label runat="server" CssClass="textlabel" Tag="label" TranslationKey="email" /><common:textbox runat="server" Required="true" FieldDesc="E-post" ID="tEmail" ValidateAs="email" cssclass="textinput required email" databind="userSettings.Email" /></td>
		<td class="helptextcolumn"></td>
	</tr>
    <tr>
		<td class="dividersmall" colspan="3"></td>
	</tr>	
	<tr>
		<td class="headercolumn"></td>
		<td class="fieldcolumn"><common:label runat="server" CssClass="textlabel" Tag="label" TranslationKey="phone-number" /><common:textbox runat="server" FieldDesc="Telefonnummer" id="tPhoneNumber" cssclass="textinput requiredCompetency" databind="userSettings.User.PhoneNumber" /></td>
		<td class="helptextcolumn"></td>
	</tr>
     
    <tr id="addressDivider">
		<td class="dividerlarge" colspan="3"></td>
	</tr>	
	<tr>
		<td class="headercolumn"></td>
        <common:label runat="server" CssClass="fieldcolumn largetext" Tag="td" TranslationKey="address" />		
		<td class="helptextcolumn"></td>
	</tr>
    
	<tr>
		<td class="dividersmall" colspan="3"></td>
	</tr>

	<tr>
        <td class="headercolumn"></td>
		<td class="fieldcolumn"><common:label runat="server" CssClass="textlabel" Tag="label" TranslationKey="street-address" /><common:textbox runat="server" FieldDesc="Gateadresse" id="tAddress1" cssclass="textinput requiredCompetency" databind="userSettings.User.StreetAddress" /></td>
		<td class="helptextcolumn"></td>
	</tr>
	<tr>
		<td class="dividersmall" colspan="3"></td>
	</tr>	
	<tr>
        <td class="headercolumn"></td>
		<td class="fieldcolumn">
			<div class="left"><common:label runat="server" CssClass="textlabel" Tag="label" TranslationKey="zip" /><common:textbox runat="server" FieldDesc="Postnummer" id="tZip" cssclass="textinput-zip requiredCompetency" databind="userSettings.User.ZipCode" /></div>
			<div class="right"><common:label runat="server" CssClass="textlabel" Tag="label" TranslationKey="city" /><common:textbox runat="server" FieldDesc="Sted" id="tCity" cssclass="textinput-city requiredCompetency" databind="userSettings.User.City" /></div>
		
		</td>
		<td class="helptextcolumn"></td>
	</tr>
    <tr>
		<td class="dividersmall" colspan="3"></td>
	</tr>	
    <tr>
		<td class="headercolumn"></td>
		<td class="fieldcolumn"><common:label runat="server" CssClass="textlabel" Tag="label" TranslationKey="country" /><select class="textinput requiredCompetency ISO3166CountryCodes" id="dCountry" databind="userSettings.User.Country" style="width:370px"><option></option></select></td>
		<td class="helptextcolumn"></td>
	</tr>    
    <tr>
		<td class="dividerlarge" colspan="3"></td>
	</tr>
    
	<tr>
		<td rowspan="2" class="headercolumn"><common:label runat="server" TranslationKey="heading-image" /></td>
		<td class="fieldcolumn avatarcell">
			<div class="avatarupload">
				<div class="avatarleft">                    
                    <common:image runat="server" Resize="false" ID="iImage" Width="90px" Height="95px" CssClass="thumbnail" oncontextmenu="if(window.confirm('Are you sure you want to remove this image')){$(this).hide(); userSettings.User.Attachment=null;userSettings.User.AttachmentId=null; }return false;" BlankPixelIfNotFound="true" />					
				</div>
				<div class="avatarright">
					<div class="actionbutton"></div>                    
                    <asp:FileUpload runat="server" ID="fUpload" Width="180px" />                                                                
                    <br /><br />                    
                    <common:label runat="server" Tag="i" TranslationKey="image-disclaimer" />			
			    </div>
            </div>
		</td>
		<td class="helptextcolumn"></td>
	</tr>
	<tr>
		<td class="dividermedium"></td>
	</tr>	
	<tr>
        <common:label runat="server" Tag="td" CssClass="headercolumn" TranslationKey="language" />		
		<td class="fieldcolumn">
            <common:label runat="server" Tag="label" CssClass="selectlabel" TranslationKey="preferred-language" />		
            <common:dropdown runat="server" ID="dLanguage" CssClass="required language" databind="userSettings.User.PreferredLanguageId">            
            </common:dropdown>			
		</td>
		<td class="helptextcolumn"></td>
	</tr>
	<tr>
		<td class="dividermedium" colspan="3"></td>
	</tr>	
	<tr>
        <common:label runat="server" Tag="td" CssClass="headercolumn" TranslationKey="password" />				
		<td class="fieldcolumn"><common:label runat="server" Tag="label" CssClass="textlabel" TranslationKey="new-password" /><common:textbox runat="server" ID="tPassword1" cssclass="textinput password" TextMode="Password" FieldDesc="Passord" MinLength="4" /></td>
		<td class="helptextcolumn"></td>
	</tr>
	<tr>
		<td class="dividersmall" colspan="3"></td>
	</tr>	
	<tr>
		<td class="headercolumn"></td>
		<td class="fieldcolumn"><common:label runat="server" Tag="label" CssClass="textlabel" TranslationKey="new-password-repeat" /><common:textbox runat="server" ID="tPassword2" cssclass="textinput password" TextMode="Password" /></td>
		<td class="helptextcolumn"></td>
	</tr>
     
     	<tr>
		<td class="dividermedium" colspan="3"></td>
	</tr>
     <asp:PlaceHolder runat="server" ID="pCompetencyContainer">
     <tr>
         <td class="headercolumn" valign="top">
            <common:label runat="server" TranslationKey="competence-heading" Tag="b">University credit<br />eligibility:</common:label>
            <br /><br /><div style="font-size:14px; font-style:italic" class="competency-missing"><img src="./inc/images/icons/stop.png" /> <common:label runat="server" TranslationKey="competence-required" Tag="b">Required information</common:label></div>                     
         </td>
		<td class="fieldcolumn" id="competencyForm">
            <div style="background-color:#FFFDFE;border:2px solid silver; border-radius: 3px 4px;padding:10px; width:350px" class="competency">
                <div style="background-color:white;border:2px solid silver; border-radius: 3px 4px;padding:4px">
                    <table><tr><td valign="top"><img src="./inc/images/icons/stop.png" class="competency-missing" /><img src="./inc/images/icons/accept.png" class="competency-accepted" /></td>
                        <td>
                            <div onclick="$(this).parents('.competency:eq(0)').find('.body').toggle();" style="cursor:pointer">
                                <common:label runat="server" TranslationKey="competence-missing" Tag="b" CssClass="competency-missing">Eligibility not documented</common:label>
                                <common:label runat="server" TranslationKey="competence-completed" Tag="b" CssClass="competency-accepted">Eligibility documented</common:label>
                                &nbsp; [<a href="javascript:void(0)" style="font-size:14px" name="competency">+</a>]
                            </div>
                            <div style="font-size:11px;font-style:italic; padding-top:5px">
                                <common:label runat="server" TranslationKey="competence-missing-why" Tag="span" CssClass="competency-missing">In order to take an exam, you will need to fill in all the fields in the enrollment form.</common:label>
                                <common:label runat="server" TranslationKey="competence-completed-thanks" Tag="span" CssClass="competency-accepted">Thank you for confirming your university credit elegibility.</common:label>
                            </div>    
                        </td>
                           </tr></table> 
                </div>
                <div class="body competency-missing">
                    <br />
               
                <common:label runat="server" Tag="label" CssClass="textlabel" TranslationKey="competence-salutation" Text="Salutation" />
                <common:dropdown runat="server" cssclass="textinput-city requiredCompetency" FieldDesc="Salutation" databind="userCompetency.Title">
                    <asp:ListItem></asp:ListItem>
                    <asp:ListItem Value="Mr">Mr</asp:ListItem>
                    <asp:ListItem Value="Mrs">Mrs</asp:ListItem>
                    <asp:ListItem Value="Ms">Ms</asp:ListItem>
                </common:dropdown>
                                
                <br />
                <br /><common:checkbox runat="server" TranslationKey="competence-citizenship" Text="I am a Norwegian citizen" CssClass="citizenship" />                
                <br /><br />
                <div id="ssnContainer">
                    <common:label runat="server" Tag="label" CssClass="textlabel" TranslationKey="competence-ssn" Text="Social security number" style="display:inline" />
                    <img style="margin-top:4px" src="./inc/images/icons/help.png" onclick="fnShowMessage('<b>Ditt personnummer lagres kryptert i vår database.</b>');" />
                    <common:textbox runat="server" cssclass="textinput-city ssn requiredCompetency" FieldDesc="Social security number" databind="userCompetency.SocialSecurityNumber" />
                    
                </div>
                <div id="dobContainer">
                    <common:label runat="server" Tag="label" CssClass="textlabel doblabel" TranslationKey="competence-dob" Text="Date of birth" />
                    <select id="dobYear" style="width:80px"><option value=""><common:label runat="server" TranslationKey="competence-year">(Year)</common:label></option></select> - <select id="dobMonth" style="width:80px"><option value=""><common:label runat="server" TranslationKey="competence-month">(Month)</common:label></option></select> - <select id="dobDay" style="width:80px"><option value=""><common:label runat="server" TranslationKey="competence-day">(Day)</common:label></option></select>                    
                    <br /><br />
                    <common:label runat="server" Tag="label" CssClass="textlabel" TranslationKey="competence-nationality" Text="Nationality" />
                    <common:textbox runat="server" ID="Textbox4" cssclass="textinput-city nationality requiredCompetency" FieldDesc="Nationality" databind="userCompetency.Nationality" />                                    
                </div>
                <br />
                    <common:label runat="server" Tag="label" CssClass="textlabel" TranslationKey="competence-birthcountry" Text="Country of birth" />
                    <select class="textinput-city requiredCompetency ISO3166CountryCodes" databind="userCompetency.CountryOfBirth" ><option></option></select>                    
                    <br /><br />
                <common:label runat="server" Tag="label" CssClass="textlabel" TranslationKey="competence-highest-education" Text="Highest completed degree" />                
                <common:dropdown runat="server" cssclass="textinput-city requiredCompetency edLevel" FieldDesc="Education Level" databind="userCompetency.EducationType" width="350px">
                    <asp:ListItem></asp:ListItem>
                    <asp:ListItem Value="1" TranslationKey="competence-level-university">University</asp:ListItem>
                    <asp:ListItem Value="2" TranslationKey="competence-level-college">University College</asp:ListItem>
                    <asp:ListItem Value="3" TranslationKey="competence-level-secondary">General study competence from high school/upper secondary school *</asp:ListItem>
                    <asp:ListItem Value="0" TranslationKey="competence-level-none">None of the above alternatives apply to me</asp:ListItem>
                </common:dropdown>
                <br style="clear:both" />
                <common:label Tag="i" runat="server" TranslationKey="competence-ed-level-disclaimer" Text="* General study competence: Completion of a 3-year secondary study program" style="font-size:12px" />
                <br /><br />
                <div id="edDescription">                    
                    <common:label runat="server" Tag="label" CssClass="textlabel" TranslationKey="competence-highest-ed" Text="Please specify your highest completed grade/level of education" />
                    <common:textbox runat="server" ID="Textbox5" cssclass="textinput-city requiredCompetency" databind="userCompetency.EducationLevel" />
                     <br />
                    <common:label runat="server" Tag="label" CssClass="textlabel" TranslationKey="competence-graduation-year" Text="Year of graduation/exam" />
                    <common:textbox runat="server" ID="Textbox6" cssclass="textinput-city requiredCompetency" databind="userCompetency.GraduationYear" />
                    <br />
                    <common:label runat="server" Tag="label" CssClass="textlabel" TranslationKey="competence-institution" Text="Name of school/college/university" />
                    <common:textbox runat="server" ID="Textbox7" cssclass="textinput-city requiredCompetency" databind="userCompetency.InstitutionName" />
                    <br />
                    <common:label runat="server" Tag="i" style="font-size:12px" TranslationKey="competence-university-upload-instructions">Please upload your diploma documenting the information given above</common:label>
                    <br /><br />
                </div>
                <div id="noEdDescription">
                    <common:label runat="server" Tag="i" CssClass="textlabel" TranslationKey="competence-requirements-label" style="font-size:12px" Text="<u>Accredited Practical Competence:</u> You must be a minimum 23 years of age and have 5 years of full-time work experience. Care-giving and compulsory military service/civil service may also be counted as work experience." />
                    
                    <br /><br /><input type="radio" name="accreditedPracticalCompetence" value="true" id="accreditedPracticalCompetenceTrue" style="float:left" /><common:label for="accreditedPracticalCompetenceTrue" runat="server" Tag="label" style="float:left;width:310px;padding:0 0 10px 10px" CssClass="textlabel" TranslationKey="competence-accredit-pass" Text="I satisfy the requirements for “accredited” practical competence" />
                    <br style="clear:both" /><input type="radio" name="accreditedPracticalCompetence" value="false" id="accreditedPracticalCompetenceFalse" style="float:left" /><common:label for="accreditedPracticalCompetenceFalse" runat="server" Tag="label" style="float:left;width:310px;padding:0 0 15px 10px" CssClass="textlabel" TranslationKey="competence-accredit-fail" Text="I do not satisfy the requirements for “accredited” practical competence" />
                    <br style="clear:both" />
                    <common:label runat="server" Tag="i" style="font-size:12px" TranslationKey="competence-practical-upload-instructions">Please upload your Curriculum Vitae including the following information: Birth date, Education, Work Experience</common:label>
                    <br /><br />
                    <common:label runat="server" Tag="label" CssClass="textlabel" TranslationKey="competence-work-experience" Text="Describe your 5 most recent years of work experience and name of employer, years of employment and position" />
                    <common:textbox runat="server" TextMode="MultiLine" cssclass="textinput-city requiredCompetency" databind="userCompetency.PracticalCompetenceDescription" Width="350px" />
                    <br /><br />                    
                </div>
                    
                    <common:label runat="server" Tag="b" TranslationKey="competence-attachments">List of attachments (diploma, resume)</common:label>
                    <br />
                    <div style="padding-top:15px">
                        <common:label runat="server" TranslationKey="competence-upload">Upload a new file</common:label><br />
                        <table>
                            <tr>
                                <td><asp:FileUpload runat="server" ID="fCompetenceUpload" /></td>
                                <td><div class="actionbutton"><common:hyperlink runat="server" navigateurl="javascript:void(0)" id="lCompetencyAgree" OnClientClick="fnUploadCompetenceFile();" TranslationKey="competence-upload-button" text="Upload" /></div></td>
                            </tr>

                        </table>
                    </div>
                    <div style="height:2px; margin:25px 70px; border-top:2px dotted silver; "></div>
                    
                         <common:label runat="server" Tag="b" TranslationKey="competence-existing-files">Files you've uploaded</common:label>
                    <ol id="competency-files" class="empty">
                        <li><common:label runat="server" Tag="i" TranslationKey="competence-no-files">You haven't uploaded any files</common:label></li>
                    </ol>
                    <common:label runat="server" TranslationKey="competence-custom-text" />                                        
                    <br />
                    <input type="checkbox" id="competency-agree" databind="userCompetency.IsTermsAccepted" /><label for="competency-agree"> <common:label runat="server" Tag="i" TranslationKey="competence-agree">I confirm the information entered in the form is correct and I accept that the information registered will be stored in a database until I've completed my certification. The information will be shared with the organization cerifying your university credits.</common:label></label>
                    <br /><br />
                    <div class="actionbutton grayscale" id="competence-submit"><common:hyperlink runat="server" navigateurl="javascript:void(0)" TranslationKey="competence-submit" Text="Submit competency form" /></div>

                    </div>                    
            </div>
		</td>
		<td class="helptextcolumn"></td>
	</tr>
	</asp:PlaceHolder>


	<tr>
		<td class="dividerlarge" colspan="3"></td>
	</tr>	
	<tr>
		<td class="headercolumn"></td>
		<td class="fieldcolumn"><div class="actionbutton"><common:hyperlink runat="server" navigateurl="javascript:void(0)" id="hSubmit" OnClientClick="$('form').submit();" TranslationKey="button-save" /></div></td>
		<td class="helptextcolumn"></td>
	</tr>
	</table>
  </div>
  </div>

  <script language="javascript">      
      <common:label runat="server" id="lScripts" />   
        $.each(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],function(i,o){
            $("#dobMonth").append("<option value='" + (i+1) + "'>" + this + "</option>");
        });
      
      for(var x=0;x<95;x++) $("#dobYear").append("<option value='" + ((new Date()).getFullYear()-100+x) + "'>" + ((new Date()).getFullYear()-100+x) + "</option>");
      for(var x=0;x<31;x++) $("#dobDay").append("<option value='" + (x+1) + "'>" + (x+1) + "</option>");          

      if(userCompetency.DateOfBirth){
          $("#dobYear").val(cStr(userCompetency.DateOfBirth).safeSplit("-",0));
          $("#dobMonth").val(stripLeadingStr(cStr(userCompetency.DateOfBirth).safeSplit("-",1),"0"));
          $("#dobDay").val(stripLeadingStr(cStr(userCompetency.DateOfBirth).safeSplit("-",2).safeSplit("T",0),"0"));
      }
      $(".ISO3166CountryCodes").each(function(i,o){
          var l=(myMetierUserDetails.Language=="no"?ISO3166CountryCodesNO:ISO3166CountryCodes);
          $.each(l,function(){
              $(o).append("<option value='" + this[0] + "'>" + this[1]+"</option>");
          });
      });
      $("#competence-submit").click(function(){
          var error=validateCompetency();
          if(!$(this).hasClass("grayscale")){
              $("input[name='submit-source']").detach();
              $('form').append("<input type='hidden' name='submit-source' value='competence' />")
              $('form').submit();
          }else{
              alert(cStr(_Translations["competence-submit-error"], "Form could not be submitted.")+"\n " + error);
          }       
      });
                
          //loading
      $(".competency-missing").toggle(NagUserForExamCompetence);
      $(".competency-accepted").toggle(!NagUserForExamCompetence);
          
        $("[databind]").each(function(){              
            var s=eval($(this).attr("databind"));
            if(s==undefined)s="";
            if($(this).is("input:checkbox"))$(this).attr("checked",s).data("original",s);
            else if($(this).is("input, select, textarea")) $(this).val(s).data("original",s);
            else $(this).html(s);              
        });

        if(userCompetency.IsAccreditedPracticalCompetenceSatisfied)$("#accreditedPracticalCompetenceTrue").attr("checked","checked");
        else $("#accreditedPracticalCompetenceFalse").attr("checked","checked");

        if(userCompetencyAttachments.length>0){
            $("#competency-files").html("").removeClass("empty");            
        }
      for(var x=0;x<userCompetencyAttachments.length;x++){
          $("#competency-files").append("<li><a target='_blank' href='" + userCompetencyAttachments[x].Url + "'>" + userCompetencyAttachments[x].Title + "</a></li>");
      }
      $.each(companySettings.CustomLeadingTexts,function(i,o){
          if(this.IsVisible){
              var input = $('<tr><td class="dividersmall" colspan="3"></td></tr><tr><td class="headercolumn"></td><td class="fieldcolumn"><label class="textlabel"/><input type="text" class="textinput customtext"/></td></tr>').insertBefore($("#addressDivider")).find("label").html(this.Text).end().find("input").data("leadingTextId",o.LeadingTextId).toggleClass("required",this.IsMandatory);
              $.each(userSettings.User.UserInfoElements,function(){
                  if(this.LeadingTextId==o.LeadingTextId)input.val(this.InfoText).data("original",this.InfoText);
              });              
          }
        }          
      );
    
      $("#"+$(".citizenship label").attr("for")).attr("checked",!userCompetency.DateOfBirth).change(function(){          
          $("#ssnContainer").toggle(this.checked);
          $("#dobContainer").toggle(!this.checked);
          if(this.checked){
              $("#dobContainer select").val("");
              $("#dobContainer input.nationality").val("Norwegian");              
          }
      }).change();
      $(".edLevel").change(function(){          
          var none=$(this).val()=="0";
          $("#edDescription").toggle(!none && $(this).val()!="");
          $("#noEdDescription").toggle(none);           
      }).change();
      
      
      //saving
      function validateCompetency(){
          if($("#competencyForm").length==0)return;
          var error="";    
          $("label.missingCompetency").removeClass("missingCompetency");
          userCompetency.DateOfBirth="";
          if($("#dobContainer").is(":visible")){
              $("#dobContainer select").each(function(){
                  userCompetency.DateOfBirth+=(userCompetency.DateOfBirth?"-":"")+$(this).val();
                  if($(this).val()=="" && !$(".doblabel").hasClass("missingCompetency")){
                      error+="\n * " + $(".doblabel").html() + " " + cStr(_Translations["competence-required-field"],"is required");                                                
                      $(".doblabel").addClass("missingCompetency");
                  }
              });
          }
                                    
          $(".requiredCompetency:visible").each(function(){
              var label=$(this).prevAll("label").length>0 ? $(this).prevAll("label").eq(0) : $(this).parents("tr:eq(0)").find("td:eq(0)");
              if($(this).val()==""){                  
                  error+="\n * " + label.addClass("missingCompetency").html() + " " + cStr(_Translations["competence-required-field"],"is a required field");                                                
              }else{
                  label.removeClass("missingCompetency");
              }
          });
          if($("#accreditedPracticalCompetenceTrue").is(":visible") && $("input[name='accreditedPracticalCompetence']:checked").length==0){
              $("input[name='accreditedPracticalCompetence']").addClass("missingCompetency");
              error+="\n * " + cStr(_Translations["competence-verify-accredited"],"You must indicate if you meet the requirements of 'Accredited Practical Competence'");                                                
          }
          if($("#ssnContainer").is(":visible")){
              if(replaceAll($("#ssnContainer input").val(),".","",",","","-","")!=$("#ssnContainer input").val()) $("#ssnContainer input").val(replaceAll($("#ssnContainer input").val(),".","",",","","-",""));              
              if(removeXChars($("#ssnContainer input").val(),"0123456789*")!=$("#ssnContainer input").val() || $("#ssnContainer input").val().length!=11){                  
                  error+="\n * " + cStr(_Translations["competence-ssn-validation"],"Social security number must be 11 digits");
                  $("#ssnContainer label").eq(0).addClass("missingCompetency");
              }
          }
          if($('#competency-agree:checked').length==0){
              error+="\n * " + cStr(_Translations["competence-verify-info"],"You need to confirm the information you have entered is correct");
              $('#competency-agree').next().addClass("missingCompetency");              
          }                    
          if(userCompetencyAttachments.length==0 && !$("#competencyForm").data("file"))
              if($("#edDescription").is(":visible")) error+="\n * " + cStr(_Translations["competence-upload-diploma"],"You need to upload a copy of your university diploma");
              else error+="\n * " + cStr(_Translations["competence-upload-resume"],"You need to upload a copy of your resume");
          
          $("#competence-submit").toggleClass("grayscale",error!="");
          return error;
      }

        //userSettings
      //companySettings
      var changedObjects={};
      $(document).ready(function () {
          $("form").submit(function(){
              var error="";changedObjects={};
              $(".required").each(function(){
                  var label=$(this).prevAll("label").length>0 ? $(this).prevAll("label").eq(0) : $(this).parents("tr:eq(0)").find("td:eq(0)");
                  if($(this).val()==""){
                      error+="\n * " + label.addClass("missing").html() + " " + cStr(_Translations["competence-required-field"],"is a required field");                                            
                  }else{
                      label.removeClass("missing");
                  }
              });          
              if($(".password").val() && (($(".password:eq(0)").val() != $(".password:eq(1)").val()) || $(".password:eq(0)").val().length<3)){
                  error+="\n * " + $(".password:eq(0)").prevAll("label").eq(0).html() + " doesn't match " + $(".password:eq(1)").prevAll("label").eq(0).html() + " or " + $(".password:eq(0)").prevAll("label").eq(0).html() + " is less than 4 characters long";
              }
              if(error){
                  alert(cStr(_Translations["competence-save-failed"],"Your changes could not be saved. Please correct the following:") + error);
                  return false;
              }
              $("input[databind], select[databind], textarea[databind]").each(function(){
                  var val=($(this).is("input:checkbox") ? this.checked :$(this).val());
                  if(val!=$(this).data("original")) changedObjects[$(this).attr("databind").split(".")[0]]=true;
                  eval($(this).attr("databind")+'="' + replaceAll(val,'"','&quot;','\n','\\n','\r','') + '";');                  
              });
              $("input.customtext").each(function(i,o){
                  var leadingTextId=$(this).data("leadingTextId");
                  if($(o).val()!=cStr($(o).data("original")))changedObjects["userSettings"]=true;
                  var found=false;
                  $.each(userSettings.User.UserInfoElements,function(){
                      if(this.LeadingTextId==leadingTextId){
                          this.InfoText=$(o).val();
                          found=true;
                      }
                  });          
                  if(!found)userSettings.User.UserInfoElements.push({ "UserId" : userSettings.User.Id, "InfoText" : $(o).val(), "LeadingTextId" : leadingTextId });                                    
              });
              userCompetency.IsAccreditedPracticalCompetenceSatisfied=$("#accreditedPracticalCompetenceTrue").is(":checked");
              if(userCompetency.IsTermsAccepted && validateCompetency()!="")userCompetency.IsTermsAccepted=false;
              
              
              var toSerialize=["userSettings","companySettings","userCompetency","changedObjects"];
              $.each(toSerialize,function(i,o){
                  var hidden=$("[name='"+o+"']");  
                  if(hidden.length==0)hidden=$("<input type='hidden' name='" + o + "' />").appendTo($("form"));                  
                  hidden.val(JSON.stringify(eval(o)));
              });
              
          });
      });
          
      $(".email, .username").change(function () { $(this).val($(this).val().split(" ").join("").split("\t").join()); $(".email").val($(".email").val().toLowerCase()); if ($(".username").length == 1 && $(".username").val() == "") $(".username").val($(".email").val()); });
      $("#competencyForm input:file").change(function(){
          $("#competencyForm").data("file",$(this).val());
      });
      function fnUploadCompetenceFile(){
          $("input[name='submit-source']").detach();
          $('form').append("<input type='hidden' name='submit-source' value='competence-file' />")
          $('form').submit();
      }
      
  </script>  
    <asp:PlaceHolder runat="server" id="pCompetencyScriptContainer">
        <script>
            validateCompetency();
            $("#competencyForm, .requiredCompetency:visible").on( "click change keypress", function() {
                validateCompetency();
            });
        </script>
    </asp:PlaceHolder>
</asp:Content>