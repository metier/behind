﻿<%@ Page Title="myMetier Achievements" Language="C#" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.User" MasterPageFile="~/inc/master/mymetier.master" Codebehind="user.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">
<div id="wrapper-introduction">
<common:label runat="server" TranslationKey="heading" CssClass="standalone" Tag="h1" />
</div>
<div class="achievementspanel nine-rounded">
	<div class="panelleft">
    <common:image runat="server" Width="150px" Height="150px" Resize="false" ID="iUserImage" />
		
		<common:label runat="server" tag="h2" ID="lUserName" />
        <common:label runat="server" Tag="p" CssClass="poeng" ID="lUserPoints" TranslationKey="user-points" />
	</div>
    <asp:PlaceHolder runat="server" ID="pRankingTemplate">
        <common:label runat="server" Tag="li" DataMember="li"><common:hyperlink runat="server" DataMember="user-link"><common:label runat="server" DataMember="rank" CssClass="rank" Tag="span" /><common:image runat="server" datamember="image" Width="40px" Height="40px" Resize="false" /><common:label runat="server" DataMember="text" cssclass="text" /></common:hyperlink></common:label>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="pRankingContainer">
	<div class="panelright">
		<ul class="ranking">
            <common:label runat="server" ID="lRanking" />			
		</ul>
	</div>
    </asp:PlaceHolder>
	<div class="clear"></div>
    <asp:PlaceHolder runat="server" ID="pMedalsPane">
	<div class="panelfooter">
		<div class="medalcontainer-3">
			<div class="medalbox"><div class="medal gold"></div><div class="multiply"></div><common:label runat="server" Tag="div" cssclass="count" ID="lMedalsGold" /></div>
			<div class="medalbox"><div class="medal silver"></div><div class="multiply"></div><common:label runat="server" Tag="div" cssclass="count" ID="lMedalsSilver" /></div>
			<div class="medalbox"><div class="medal bronze"></div><div class="multiply"></div><common:label runat="server" Tag="div" cssclass="count" ID="lMedalsBronze" /></div>
		</div>
	</div>
	<div class="clear"></div>
    </asp:PlaceHolder>
</div>

<asp:PlaceHolder runat="server" ID="pCourseTemplate">
  <tr class="highlight">
    <td width="548" class="first left-rounded green"><common:hyperlink runat="server" CssClass="name" datamember="name" NavigateUrl="javascript:void(0)" /></td>
    <td><common:label runat="server" Tag="div" CssClass="date" DataMember="course_last_accessed" /></td>
	<common:label runat="server" Tag="td" CssClass="pdu right-rounded dark-blue" DataMember="download-pdu"><common:hyperlink runat="server" CssClass="pdu" datamember="pdu-link" TranslationKey="pdu-link" NavigateUrl="javascript:void(0)">PDU Info</common:hyperlink></common:label>
    <common:label runat="server" Tag="td" CssClass="last diploma right-rounded dark-blue" DataMember="download-diploma"><common:hyperlink runat="server" CssClass="diploma" datamember="diploma-link" Target="_blank" TranslationKey="diploma-link" /></common:label>
  </tr>
  <tr>
    <td colspan="3" class="divider"></td>
  </tr>
</asp:PlaceHolder>
    <div class="achievementslist">
    <table>
      <tr>
        <td colspan="3"><common:label runat="server" TranslationKey="completed-courses" Tag="h1" /></td>
      </tr>
      <common:label runat="server" ID="lCourses" />    
      <asp:PlaceHolder runat="server" ID="pCoursesNoShow">
      <tr>
        <td colspan="3"><common:label runat="server" TranslationKey="no-show-courses-heading" Tag="h1" /></td>
      </tr>
      <common:label runat="server" ID="lCoursesNoShow" />    
      </asp:PlaceHolder>
      <asp:PlaceHolder runat="server" ID="pTranscriptDownload" Visible="false">
      <tr>
        <td colspan="3"><br /><common:label runat="server" TranslationKey="transcript-download-heading" Tag="h1" /></td>
      </tr>
      <common:label runat="server" ID="lTranscriptDownload" />    
      </asp:PlaceHolder>
    </table>
</div>
</asp:Content>