﻿<%@ Page Title="myMetier" Language="C#" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Navigator" MasterPageFile="~/inc/master/mymetier.master" Codebehind="navigator.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">

    <script type="text/javascript" src="./player/flowplayer/flowplayer-3.2.11.min.js"></script>    
<div id="wrapper-introduction">
  <div id="container-programpicture"> <common:image runat="server" ID="iCourseImage" Resize="false" /></div>
  <common:label runat="server" Tag="h1" ID="lCourseTitle" CssClass="coursedetails" />
    <asp:PlaceHolder runat="server" ID="pUenrolledDetails">
    <div id="unenrolled-course-details"></div>
        
    <script type="text/html" id="activityset_template">

        {% for(i=0;i<Current.Activities.length;i++) { %}
            {% if(Current.Activities[i].ActivityType=="elearning" || Current.Activities[i].ActivityType=="classroom" || Current.Activities[i].ActivityType=="exam"){ %}
            <div class="activity">
                <h2>{%=Current.Activities[i].Name%}</h2>
                {% for(j=0;j<Current.Activities[i].Locations.length;j++) { %}
                <div class="activity-detail"><b translationkey="classroom-location">Location</b><b>:</b> {%=Current.Activities[i].Locations[j].Name%}</div>
                {% } %}
                {% for(j=0;j<Current.Activities[i].Ecoaches.length;j++) { %}
                <div class="activity-detail"><b translationkey="ecoach">Ecoach</b><b>:</b> {%=Current.Activities[i].Ecoaches[j].Name%} - <a href="mailto:{%=cStr(Current.Activities[i].Ecoaches[j].ContactEmail,"kurssporsmal@metier.no")%}" translationkey="send-request">Contact</a>                    
                </div>
                {% } %}
                {% if((Current.Activities[i].ActivityType=="classroom" || Current.Activities[i].ActivityType=="exam") && Current.Activities[i].Periods.length>0) { %}                    
                <table class="activity-detail"><tr><td valign="top"><b translationkey="classroom-popup-when">When</b><b>:</b> &nbsp;</td><td>
                    {% for(j=0;j<Current.Activities[i].Periods.length;j++) { %}
                        {%=Current.Activities[i].Periods[j]%}
                    {% } %}
                </td></tr></table>
                {% } %}
            </div>
                <br />            
            {% } %}
        {% } %}
        {% if(Price){ %}        
            {%=Price%}
            <br /><br />            
        {% } %}
        {% if(AllowEnroll){ %}        
            <span class="actionbutton"><a href="javascript:void(0)" onclick="fnEnroll({%=Current.Id%});" translationkey="button-enroll{%=Current.Activities[0].ActivityType=="exam"?"-exam":""%}">Enroll</a></span>
        {% } %}
        {% if(EnrollmentPriceAccommodationDisclaimer){ %}        
            <br /><br />            
            {%=EnrollmentPriceAccommodationDisclaimer%}            
        {% } %}
  </script>
    <script>
        $("#unenrolled-course-details").html(tmpl("activityset_template", __Courses));
    </script>
</asp:PlaceHolder>

    <common:label runat="server" ID="lCMSDescriptions" />
  <common:label runat="server" ID="lCourseDescription" />
  

  <common:label runat="server" Tag="table" ID="lElearnExpiredContainer" class="course-elearn-expired" Visible="false">
  <tr>
  <td><img src="./inc/images/icons/delete-32.png" alt="Stop" /></td>
  <common:label runat="server" ID="lElearnExpired" Tag="td" />
  </tr>
  </common:label>
   
  
  <div class="clear"></div>
</div>
    <common:label runat="server" Tag="div" ID="lFoundationExamSwitcher" class="programpanel nine-rounded prince2-exam-switcher">  
        <div style="padding:20px 20px">
            <common:label runat="server" Tag="h2" TranslationKey="exam-switch-foundation-heading">Foundation exam</common:label>    
        <common:label runat="server" id="lFoundationExamSwitchPossible">
            <common:label runat="server" TranslationKey="exam-switch-info" Tag="div" ID="lFoundationExamSwitchNotBooked" CssClass="exam-switch-info">Changing exam date can be performed up until 10 days before the exam</common:label>
            <div class="changer">
                <common:dropdown runat="server" ID="dFoundationExams" DataTextField="Text" DataValueField="Value" /><common:button.button runat="server" buttonmode="submitbutton" text="Bekreft" id="bFoundationExamConfirm" translationkey="exam-switch-confirm" />
            </div>
            <common:label runat="server" Tag="div" ID="lFoundationExamSwitchNotBooked1" TranslationKey="exam-switch-info1">Dersom du senere ønsker å endre dato står du fritt til å gjøre dette frem til 10 dager før valgt eksamendato.</common:label>
            <br />
            <common:label runat="server" TranslationKey="exam-switch-take-home" />
    </common:label>
    </div>
  </common:label>
    <common:label runat="server" Tag="div" ID="lPractitionerExamSwitcher" class="programpanel nine-rounded prince2-exam-switcher">
      <div style="padding:20px 20px">
        <common:label runat="server" Tag="h2" TranslationKey="exam-switch-practitioner-heading">Practitioner exam</common:label>
          <common:label runat="server" id="lPractitionerExamSwitchPossible">
            <common:label runat="server" TranslationKey="exam-switch-info" Tag="div" ID="lPractitionerExamSwitchNotBooked" CssClass="exam-switch-info">Changing exam date can be performed up until 10 days before the exam</common:label>
          <div class="changer">
            <common:dropdown runat="server" ID="dPractitionerExams" DataTextField="Text" DataValueField="Value" /><common:button.button runat="server" buttonmode="submitbutton" id="bPractitionerExamConfirm" translationkey="exam-switch-confirm" />
          </div>
            <common:label runat="server" Tag="div" ID="lPractitionerExamSwitchNotBooked1" TranslationKey="exam-switch-info1">Dersom du senere ønsker å endre dato står du fritt til å gjøre dette frem til 10 dager før valgt eksamendato.</common:label>
            <br />
              <common:label runat="server" TranslationKey="exam-switch-take-home" Tag="div" /><br />
              <common:label runat="server" TranslationKey="exam-switch-info-practitioner" Tag="div"/>
        </common:label>
    </div>
  </common:label>
<asp:PlaceHolder runat="server" ID="pProgramPanelContainer">
<div class="programpanel nine-rounded">

  <ul>
  <asp:PlaceHolder runat="server" ID="pDiplomaContainer">    
    <li id="hDiplomaLink" class="button-diploma">
        <common:label runat="server" Tag="h3" TranslationKey="diploma-heading" />
        <p translationkey="diploma-details" id="hDiplomaDetails"></p>
    </li>    
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="pFinalTestContainer">    
    <li class="button-endtest" id="hFinalTestLink">
        <common:label runat="server" Tag="h3" TranslationKey="finaltest-heading" />      
        <common:label runat="server" ID="lFinalTestUnlock" Tag="p" TranslationKey="finaltest-details" />
    </li>    
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="pClassroomContainer">
    <common:hyperlink cssclass="button-classroom" ID="hClassroomLink" runat="server">
    <common:hidden runat="server" ID="hClassroomPopupContent" />
        <li>
        <common:label runat="server" ID="lClassroomMonth" Tag="div" CssClass="month" />
        <common:label runat="server" ID="lClassroomDay" Tag="div" CssClass="date" />            
      <common:label runat="server" Tag="h3" TranslationKey="classroom-heading" ID="lClassroomHeading" />      
      <common:label runat="server" ID="lClassroomSmall" Tag="p"/>      
    </li>    
    <u><common:label runat="server" TranslationKey="classroom-details" Tag="i" ID="lClassroomDetails" /></u>
    </common:hyperlink>
    </asp:PlaceHolder>
  </ul>
   
  
  <asp:PlaceHolder runat="server" ID="pElearnProgress">
  <table border="0" cellpadding="0" cellspacing="0" id="progress-container">
  <asp:PlaceHolder runat="server" ID="pEnrollmentContainer">
  <tr>
		<td class="enroll">
			<p><common:label runat="server" id="lEnrollmentPrice" Text="Price: {0}" TranslationKey="price" /><br />
			<span class="actionbutton"><common:hyperlink NavigateUrl="javascript:void(0)" runat="server" ID="hEnroll" TranslationKey="button-enroll" /></span>
            <common:label runat="server" Tag="i" style="font-size:10px; display:block" id="lEnrollmentPriceAccommodationDisclaimer" visible="false" TranslationKey="price-accommodation-extra">* Dagpakkekostnad kommer i tillegg til kursavgiften</common:label></p>
		</td>
    </tr>
    </asp:PlaceHolder>
  <asp:PlaceHolder runat="server" ID="pEnrolledProgress">
    <tr>    
      <td>
      <table style="width:100%">
      <tr><td><div class="programpoints"></div></td>
      <td><div class="progressbar large">            
          <div class="progressnumber-total"></div>
          <common:label runat="server" Tag="div" CssClass="percentage-taken" Visible="false" ID="lPercentageTaken">100%</common:label>
          <div class="progresscolor-taken">                    
            <div class="progressnumber-taken"></div>
          </div>
          <div class="rt" />
        </div></td>
        <td class="lContinueCourseContainer" style="float:right"><div class="actionbutton"><a href="javascript:void(0)" id="lContinueCourse"></a></div></td>
      </tr>
      </table>
      
        </td>
    </tr>
    <tr>
      <td><div class="statusmessage">
          <p></p>
        </div></td>
    </tr>
    </asp:PlaceHolder>
  </table>
  </asp:PlaceHolder>

  <div class="clear"></div>
</div>
</asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="pFileUploadContainer" Visible="false">
        
        <div style="width:950px;margin:0 auto">
            <common:label style="margin-bottom:10px" runat="server" Tag="h1" TranslationKey="heading-uploads">Last opp din eksamensbesvarelse</common:label>        
        </div>        
        <div class="filepanel nine-rounded">
            <div style="padding:20px">                
            <common:label runat="server" ID="lUploadFilesDescription" TranslationKey="upload-description" Text="Use the area below to upload your files for this assignment. If you need to upload more files than is supported by the form, you will need to perform the upload multiple times." />            
            <br />    <br />            
                <asp:PlaceHolder runat="server" ID="pUploadNewFileContainer" Visible="false">                
        <table style="width:100%">
            <thead>
                <tr>                    
                    <td><common:label runat="server" Tag="b" TranslationKey="upload-title">Title</common:label></td>
                    <td><common:label runat="server" Tag="b" TranslationKey="upload-file">File</common:label></td>
                </tr>
            </thead>
            <tbody>
                <tr>                    
                    <td><common:textbox runat="server" ID="tFileTitle1" /></td>
                    <td><asp:FileUpload runat="server" ID="fUpload1" /></td>
                </tr>                
                <tr>
                    <td colspan="3"><div class="actionbutton"><br /><br /><common:hyperlink runat="server" TranslationKey="upload-button" Text="Perform Upload" NavigateUrl="javascript:void(0)" OnClientClick="$('form').submit();" /></div></td>
                </tr>
            </tbody>            
        </table>
                <br /><br />
                    </asp:PlaceHolder>
                <common:label runat="server" Tag="h3" TranslationKey="heading-files-uploaded">Files you have uploaded so far</common:label>
                <common:label runat="server" id="lUploadedFiles" TranslationKey="no-files-uploaded" Text="No files have been uploaded" />
            <div class="clear"></div>
                </div>
            </div>
    </asp:PlaceHolder>
    
<asp:PlaceHolder runat="server" ID="pELearnLessonList">
    <div class="largelessonslist">
    <table width="990" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td><common:label runat="server" TranslationKey="lessons" Tag="h1" ID="lLessonsHeading" /></td>
        <td class="info"><common:label runat="server" TranslationKey="progress-score">Score</common:label></td>
      </tr>
        </table>
    <table width="990" border="0" cellspacing="0" cellpadding="0" id="lessonsTable">
    
    </table>
    </div>

    <script type="text/html" id="lessonlist_template">
        
        
        {% 
        var LessonList=$.grep(Lessons,function(itm){return itm.Type=="lesson";});
        for(i=0;i<LessonList.length;i++) { %}            
            <tr class="{%= (window.parent.LessonData.PlayerSettings.Player.SequentialPlayback && i>0 && LessonList[i-1].Status!='C' ? 'disabled' : (LessonList[i].Url?'highlight clickable':'')) %}" onclick="{%=LessonList[i].Url ? "fnMetierPlayRCO("+ActivityId+"," +LessonList[i].RCOID +",'" +LessonList[i].Url + "', null);":"" %}">
                <td class="first left-rounded green"><span class="name">{%=LessonList[i].Title%}</span>
                <div class="clear"></div></td>
                <td><div class="date">{%=LessonList[i].CompletedDate%}</div></td>
                {% if (LessonList[i].NumSubLessons == 0) { %}
                    <td title="{%=LessonList[i].Score+'%'%}" class="last right-rounded green"><div class="progressbar"><div class="taken" style="width:{%=LessonList[i].Score%}%;position:absolute;"></div><span style="z-index:2;text-align:center;position:absolute;width:100%; font-weight:normal">{%=_Translations[(LessonList[i].Status == "C" ? "progressbar-completed" : (LessonList[i].Status == "I" ? "progressbar-in-progress" : "progressbar-not-started"))] + replaceAll(LessonList[i].Weight>0?_Translations["progressbar-percentage"]:"","{0}",LessonList[i].Score)%}</span></div></td>
                {% } %}
            </tr>
            <tr>
                <td colspan="3" class="divider"></td>
            </tr>            
        {% } %}
  </script>
    <script>                        
        window.parent.LessonData.PlayerSettings = $.extend(true,{},{'Player':{},'Quiz':{}},$.parseJSON(window.parent.LessonData.PlayerSettings));        
        $("#lessonsTable").html(tmpl("lessonlist_template", window.parent.LessonData));        
    </script>
  
</asp:PlaceHolder>


<div id="wrapper-supplemental">
  <div class="programdownloads left">
    <asp:PlaceHolder runat="server" Visible="false" ID="pCourseInfoContainer">    
    <common:label runat="server" Tag="div" ID="lCourseInfo" CssClass="course-info" />
    </asp:PlaceHolder>    

      <asp:PlaceHolder runat="server" ID="pClassroomDownloadTemplate">
          <tr>
          <td>
    <common:hyperlink runat="server" CssClass="downloads highlight" DataMember="link" Target="_blank">    
      <common:label runat="server" tag="h3" DataMember="title" />
      <common:label runat="server" tag="p" DataMember="details" CssClass="infotext" />                    
      </common:hyperlink>
              </td>
      </tr>
    </asp:PlaceHolder>

      <asp:PlaceHolder runat="server" Visible="false" ID="pCaseExamContainer">    
        <common:label runat="server" Tag="h1" translationkey="case-exam-download" />    
        <common:label runat="server" ID="lCaseExamDownloads" Tag="table" />    
          <br />
    </asp:PlaceHolder>

    <asp:PlaceHolder runat="server" Visible="false" ID="pClassroomDownloads">
    
    <common:label runat="server" Tag="h1" ID="lDownloadFilesHeading" />    
    <common:label runat="server" ID="lClassroomDownloads" Tag="table" />    
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="pECoach">
    <br />        
        <common:label runat="server" TranslationKey="ecoach" Tag="h1" />    
        <table width="450" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td width="105"><common:image runat="server" ID="iECoachImage" CssClass="avatar" width="90px" height="95px" Resize="false" /></td>
            <td class="programsupport" width="345" valign="top"><common:label runat="server" Tag="h3" ID="lECoachName" />
            <common:label ID="lECoachCourse" runat="server" TranslationKey="ecoach-title" Tag="p" CssClass="infotext" />                  
              <p><common:hyperlink runat="server" ID="lECoachEmail" TranslationKey="send-request" Target="_blank" /></p>
              <common:label runat="server" ID="lECoachPhone" Tag="p" />
              </td>
          </tr>
        </tbody></table>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="pInstructor" Visible="false">
    <br />
    <common:label runat="server" TranslationKey="instructor" Tag="h1" />            
        <table width="450" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td width="105"><common:image runat="server" ID="iInstructorImage" CssClass="avatar" width="90px" height="95px" Resize="false" /></td>
            <td class="programsupport" width="345" valign="top"><common:label runat="server" Tag="h3" ID="lInstructorName" />
            <common:label ID="lInstructorCourse" runat="server" TranslationKey="instructor-title" Tag="p" CssClass="infotext" />                  
                            
          </tr>
        </tbody></table>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="pStudyAdvisor" Visible="false">
    <br />
    <common:label runat="server" TranslationKey="mentor" Tag="h1" />            
        <table width="450" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td width="105"><common:image runat="server" ID="iStudyAdvisorImage" CssClass="avatar" width="90px" height="95px" Resize="false" /></td>
            <td class="programsupport" width="345" valign="top"><common:label runat="server" Tag="h3" ID="lStudyAdvisorName" />
            <common:label ID="lStudyAdvisorCourse" runat="server" TranslationKey="mentor-title" Tag="p" CssClass="infotext" />                  
            <p><common:hyperlink runat="server" ID="lStudyAdvisorEmail" TranslationKey="send-request" Target="_blank" /></p>                                                    
          </tr>
        </tbody></table>
    </asp:PlaceHolder>
  </div>

  <div class="programstudents right">
<asp:PlaceHolder runat="server" ID="pUnenrolledELearnLessonList" Visible="false">
    <asp:PlaceHolder runat="server" ID="pUnenrolledLessonTemplate">
    <tr class="highlight">
        <td width="280" class="first left-rounded green"><common:label runat="server" DataMember="name" /></td>
        <td width="142" class="last right-rounded green"><div class="progressbar small"><div class="taken" style="width: 0%;"></div></div></td>
      </tr>
      <tr>
        <td colspan="3" class="divider"></td>
      </tr>        
    </asp:PlaceHolder>

<common:label runat="server" TranslationKey="lessons" Tag="h1" />  
    <table class="smalllessonslist">
        <common:label runat="server" ID="lUnenrolledLessons" />      
    </table>
  <br />
</asp:PlaceHolder>
  
  <asp:PlaceHolder runat="server" ID="pTop3Template">    
    <common:label runat="server" Tag="td" DataMember="container">          
        <common:label runat="server" Tag="div" CssClass="ranknumber" DataMember="ranknumber" />
        <common:image runat="server" Width="90" Height="95" CssClass="avatar" ID="iAvatar" Resize="false" />        
        <common:label runat="server" Tag="div" CssClass="surname" DataMember="surname" />
        <common:label runat="server" Tag="div" CssClass="familyname" DataMember="familyname" />
        <common:label runat="server" Tag="div" CssClass="number" DataMember="points" />      
    </common:label>    
  </asp:PlaceHolder>
  <asp:PlaceHolder runat="server" ID="pBottom7Template">
    <tr class="smallstudentslist highlight">
        <td class="first left-rounded green"><common:label runat="server" Tag="div" CssClass="ranknumber" DataMember="ranknumber" /></td>
        <td><common:hyperlink runat="server" CssClass="name" DataMember="name" /></td>
        <td class="last right-rounded green">        
            <common:label runat="server" DataMember="progress-bar" Tag="div" CssClass="progressbar">
                <common:label runat="server" Tag="div" CssClass="number" DataMember="number" />
                <common:label runat="server" Tag="div" CssClass="taken" DataMember="progress" />
              </common:label></td>
      </tr>
      <tr class="smallstudentslist">
        <td colspan="3" class="divider"></td>
      </tr>
    </asp:PlaceHolder>

  <asp:PlaceHolder runat="server" ID="pParticipantContainer">
  
  <common:label runat="server" TranslationKey="participants" Tag="h1" />    
    <div class="top3 nine-rounded-dark">
    <table style="border-collapse:collapse">
    <tr>
    <asp:PlaceHolder runat="server" ID="pTop3Students" />    
    </tr>
    </table>
    </div>
    <table class="smallstudentslist" width="450" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="36" class="divider">&nbsp;</td>
        <td width="270" class="divider">&nbsp;</td>
        <td width="144" class="divider">&nbsp;</td>
      </tr>
      <asp:PlaceHolder runat="server" ID="pBottom7Students" />           
    </table>    
    <common:hyperlink runat="server" ID="lViewParticipants" NavigateUrl="participants.aspx?" Visible="false" style="font-style:italic; margin-top:10px; display:block" />
  </asp:PlaceHolder>
  </div>
  <div class="clear"></div>
</div>


</common:form>
<script language="javascript">
    function fnMetierPlayRCO(iClass, iRCO, sUrl, iType, sScormMode, bInternal) {
        window.parent.fnMetierPlayRCO(iClass, iRCO, sUrl, iType, sScormMode, bInternal);
    }
    $(document).ready(function () {    
        $(".prince2-exam-switcher select").each(function(){
            if($(this).find("option:selected").val()>0){
                $(this).parents(".changer").hide();
                $("<div class='booked-notice'>" + replaceAll(_Translations["exam-switch-already-booked"],"{0}", " <i>" + $(this).find("option:selected").text() + "</i>") + "<br /><br />" + _Translations["exam-switch-already-instructions"] + " <a href='javascript:void(0)'><u>" + _Translations["exam-switch-confirm-change"] + "</u></a></div>").insertAfter($(this).parents(".changer")).find("a").click(function(){$(this).parent().hide().prev().show();});
            }
        });
    });
</script>

<script language="javascript">    
       
        
    <common:label runat="server" id="lScript" />
        window.parent.document.title=document.title;
    
    if(window.parent==window)window.location.href=window.location.href.split("frame=").join("");     
    
</script>
    
    <script language="javascript">

        
        $("a").each(function(){
            if(cStr($(this).attr("href")).indexOf(".mp4")>-1){
                $(this).click(function(e){
                    new clsVideoPlayer([$(this).attr("href")+"&"]).play();
                    e.cancel=true;
                    return false;
                });
            }
        });        
        $(document).ready(function () { 
            function GetTempVal(Key,Default){
                var V = (typeof (Storage) !== "undefined" ? localStorage.getItem(Key):$.cookies(Key));
                if(!V)V=Default;
                return V;
            }
            var Values={MaxPoints:0, Points:0,LessonCount:0,CompletedLessonCount:0,LessonPoints:0,StartedLessonWeights:0,MaxLessonPoints:0};
            
            $.each(window.parent.LessonData.Lessons,function(I,Item){
                if(iUserID==0){
                    Item.Score=GetTempVal(Item.RCOID+"-cmi.score.raw",0);
                    Item.Status=GetTempVal(Item.RCOID+"-cmi.completion_status","N");                    
                }
                if(Item.NumSubLessons==0){
                    if(Item.Score>0&&Item.Status=="N")Item.Status="I";
                    Values.MaxPoints+=(Item.Weight*100);
                    Values.Points+=(Item.Weight*Item.Score);
                    if(Item.Type=='lesson'){
                        Values.LessonPoints+=(Item.Weight*Item.Score);
                        Values.MaxLessonPoints+=(Item.Weight*100);
                        if(Item.Status!="N")Values.StartedLessonWeights+=Item.Weight;
                        Values.LessonCount++;
                        if(Item.Status=="C")Values.CompletedLessonCount++;
                    }
                }
            });
            $.extend(window.parent.LessonData,Values);
            $(".programpoints").html(Values.MaxPoints==0?"100%": replaceAll(_Translations["course-point-progress"],"{0}",Values.Points,"{1}",Values.MaxPoints,"{2}",Values.LessonPoints,"{3}",Values.MaxLessonPoints));
            $(".progressnumber-total").html(Values.LessonCount);
            $(".progressnumber-taken").html(Values.CompletedLessonCount);
            $(".progresscolor-taken").css("width",(Values.LessonCount==0?"100": (Values.CompletedLessonCount*100)/ Values.LessonCount)+"%");
            if(Values.MaxPoints==0)$(".statusmessage p").html(_Translations["course-completed"]);
            else if(window.parent.LessonData.IsMockExam)$(".statusmessage p").html(cStr(_Translations["mockexam-status"]).format(Values.CompletedLessonCount, Values.LessonCount, Values.Points));
            else $(".statusmessage p").html(
                cStr(_Translations["elearn-status"]).format(Values.CompletedLessonCount, Values.LessonCount, Values.Points, window.parent.LessonData.GoldPoints, window.parent.LessonData.UnlockTest,SafeGetProperty(window.parent.LessonData.PlayerSettings,"Diploma","FinalTestMinScore"),(Values.StartedLessonWeights>0?Math.round(Values.LessonPoints / Values.StartedLessonWeights):0))                
                + (window.parent.LessonData.Rank?(" "+_Translations["elearn-status-competition"]).format(window.parent.LessonData.Rank, window.parent.LessonData.ParticipantCount):"")
                );                        

            if($("#lessonlist_template").length>0)
                fnTranslateControl($("#lessonsTable").html(tmpl("lessonlist_template", window.parent.LessonData)));        
            
            //Handle Start course / Resume course button
            var ResumeLesson=$.grep(window.parent.LessonData.Lessons,function(item){return item.Status!="C" && item.Url && item.Type == "lesson";})[0];
            if(!ResumeLesson)ResumeLesson=$.grep(window.parent.LessonData.Lessons,function(item){return item.Status=="C" && item.Url && item.Type == "lesson";}).reverse()[0];
            if(ResumeLesson){
                var FirstCourseLesson=$.grep(window.parent.LessonData.Lessons,function(item){return item.Url && item.Type == "lesson";})[0];
                $("#lContinueCourse").html(_Translations[FirstCourseLesson == ResumeLesson && ResumeLesson.Status=="N" ? "button-start" : "button-continue"]).click(function(){
                    fnMetierPlayRCO(window.parent.LessonData.ActivityId.Id,ResumeLesson.RCOID,ResumeLesson.Url);
                });
            }else{
                $("#lContinueCourseContainer").hide();
            }
            if(window.parent.LessonData.DiplomaUrl){
                $("#hDiplomaLink").addClass("active").click(function(){window.open(window.parent.LessonData.DiplomaUrl);});
                $("#hDiplomaDetails").html(_Translations["diploma-download"]);                						                
            }else{
                var FinalTestMinScore = SafeGetProperty(window.parent.LessonData.PlayerSettings,"Diploma","FinalTestMinScore");
                if(FinalTestMinScore>0)
                    $("#hDiplomaDetails").html(replaceAll(_Translations["diploma-passing-score"],"{0}",FinalTestMinScore));                						                                
            }
            if(window.parent.LessonData.FinalTestUrl){
                $("#hFinalTestLink").addClass(window.parent.LessonData.FinalTestClass).attr("onclick",window.parent.LessonData.FinalTestUrl).attr("target","_blank");                
            }
        });        
    </script>
    <style>
        .activity .activity-detail{
            padding-top:5px;
        }
        .activity{
            padding-bottom:20px;
        }
    </style>
</asp:Content>