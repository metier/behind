﻿using System;
using System.Xml;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.components;
using jlib.functions;
using System.Text;
using session = Phoenix.LearningPortal.Util.Phoenix.session;
namespace Phoenix.LearningPortal
{
    public partial class Settings : jlib.components.webpage
    {

        public void Page_Init(object sender, EventArgs e)
        {
            dLanguage.Items.Clear();
            Util.Classes.user activeUser = (this.Master as Inc.Master.MyMetier).ActiveUser;
            Util.Phoenix.session.RequestResult result = activeUser.PhoenixSession.PhoenixRequest("GET", session.Queries.LanguagesGet, null, true, true);
            foreach (var language in result.DataObject) dLanguage.Items.Add(new ListItem(convert.cStr(convert.SafeGetProperty(language, "Name")), convert.cStr(convert.SafeGetProperty(language, "Id"))));

            //copy translation from enroll.aspx
            List<iControl> controls = jlib.helpers.control.getDynamicControls(Page, null);
            jlib.helpers.translation.translate(this.Language, controls, "/enroll.aspx");
            (this.Master as Inc.Master.MyMetier).SubscribeTranslation("/enroll.aspx");
        }
        public void Page_Load(object sender, EventArgs e)
        {
            Util.Classes.user activeUser = (this.Master as Inc.Master.MyMetier).ActiveUser;
            this.ErrorLabel = lFormError;
            dynamic userSettings = jlib.functions.json.DynamicJson.Parse((Request["userSettings"].IsNullOrEmpty() ? convert.cStr(activeUser.PhoenixSession.PhoenixRequest("GET", session.Queries.UserGetCurrent, "", false, false).Data, "{}") : Request["userSettings"]));
            dynamic originalUserCompetency = (activeUser.ExamCompetence != null ? activeUser.ExamCompetence.Data : null);
            dynamic userCompetency = (parse.replaceAll(Request["userCompetency"], "{}").IsNullOrEmpty() && originalUserCompetency != null ? jlib.functions.json.DynamicJson.Parse(originalUserCompetency.ToString()) : jlib.functions.json.DynamicJson.Parse(convert.cStr(Request["userCompetency"], "{}")));
            dynamic changedObjects = jlib.functions.json.DynamicJson.Parse(convert.cStr(Request["changedObjects"], "{}"));

            if (fCompetenceUpload.HasFile)
            {
                dynamic image = Util.Phoenix.session.UploadFile(fCompetenceUpload.FileName, fCompetenceUpload.PostedFile.ContentType, fCompetenceUpload.FileBytes);
                dynamic attachment = new jlib.functions.json.DynamicJson();
                foreach (var member in image.GetDynamicMemberNames())
                    attachment[member] = image[member];

                List<string> attachments = new List<string>();
                if (userCompetency.Attachments())
                {
                    foreach (dynamic att in userCompetency.Attachments)
                        attachments.Add(att.ToString());
                }
                attachments.Add(attachment.ToString());
                userCompetency.Attachments = null;
                userCompetency = jlib.functions.json.DynamicJson.Parse(parse.replaceAll(userCompetency.ToString(), "\"Attachments\":null", "\"Attachments\":[" + attachments.Join(",") + "]"));
            }
            if (fUpload.HasFile)
            {
                dynamic image = Util.Phoenix.session.UploadFile(fUpload.FileName, fUpload.PostedFile.ContentType, fUpload.FileBytes);
                if (userSettings.User.Attachment == null) userSettings.User.Attachment = new jlib.functions.json.DynamicJson();
                foreach (var member in image.GetDynamicMemberNames())
                {
                    userSettings.User.Attachment[member] = image[member];
                }
            }

            if (this.IsPostBack)
            {
                //should se save exam competency?
                if (fCompetenceUpload.HasFile || convert.HasProperty(changedObjects, "userCompetency"))
                {
                    userCompetency.FirstName = userSettings.User.FirstName;
                    userCompetency.LastName = userSettings.User.LastName;
                    userCompetency.Email = userSettings.Email;
                    userCompetency.StreetAddress = userSettings.User.StreetAddress;
                    userCompetency.ZipCode = userSettings.User.ZipCode;
                    userCompetency.Country = userSettings.User.Country;
                    userCompetency.SocialSecurityNumber = parse.replaceAll(userCompetency.SocialSecurityNumber, " ", "");
                    if (convert.cStr(userCompetency.SocialSecurityNumber).EndsWith("*****")) userCompetency.SocialSecurityNumber = convert.SafeGetProperty(originalUserCompetency, "SocialSecurityNumber");
                    if (convert.cStr(userCompetency.SocialSecurityNumber).Length != 11) userCompetency.SocialSecurityNumber = null;

                    if (convert.cStr(convert.SafeGetProperty(userCompetency, "DateOfBirth")) != "") userCompetency.DateOfBirth = convert.cDate(userCompetency.DateOfBirth);

                    List<string> nullifyIfEmpty = new List<string>() { "SocialSecurityNumber", "EducationType", "GraduationYear" };
                    nullifyIfEmpty.ForEach(field =>
                    {
                        if (convert.cStr(convert.SafeGetProperty(userCompetency, field)) == "") userCompetency[field] = null;
                    });
                    if (Request["submit-source"] == "competence-file") userCompetency.IsTermsAccepted = false;

                    Util.Phoenix.session.RequestResult competencyResult = activeUser.PhoenixSession.PhoenixRequest((convert.HasProperty(userCompetency, "Id") ? "PUT" : "POST"), String.Format(session.Queries.UserCompetenceGetPutPost, activeUser.ID), userCompetency.ToString(), false, false);

                    activeUser.PhoenixSession.PhoenixClearCache(String.Format(session.Queries.UserCompetenceGetPutPost, activeUser.ID));
                    if (Request["submit-source"] == "competence" || Request["submit-source"] == "competence-file")
                    {
                        activeUser.PhoenixSession.PhoenixClearCache();
                        activeUser = Util.Classes.user.getUser(activeUser.ID, true);
                    }

                    userCompetency = (activeUser.ExamCompetence != null ? jlib.functions.json.DynamicJson.Parse(activeUser.ExamCompetence.Data.ToString()) : jlib.functions.json.DynamicJson.Parse("{}"));
                    if (userCompetency != null && convert.cStr(convert.SafeGetProperty(userCompetency, "EducationType")) == "0" && convert.cBool(convert.SafeGetProperty(userCompetency, "IsTermsAccepted"))
                        //&& convert.cBool(userCompetency.IsAccreditedPracticalCompetenceSatisfied)
                        && (originalUserCompetency == null || !convert.cBool(originalUserCompetency.IsTermsAccepted)))
                    {
                        Session["alert.once"] = jlib.helpers.translation.translate(this.Language, "", "/enroll.aspx", Page, "competence-work-experience-check");
                        if (!convert.cBool(userCompetency.IsAccreditedPracticalCompetenceSatisfied))
                        {
                            List<Util.Classes.Resource> examEcoaches = new List<Util.Classes.Resource>();
                            activeUser.Enrollments.ForEach(enrollment =>
                            {
                                if (enrollment.ArticleType == Util.Data.MetierClassTypes.Exam)
                                    examEcoaches.AddRange(enrollment.ExtendedActivityObject.GetResourcesByType(Util.Data.ResourceTypes.ECoach));
                            });

                            Util.Email.SendRazorEmail(new { User = activeUser.UserObject, ECoach = (examEcoaches.Count == 0 ? null : examEcoaches[0].Data) }, "email-user-without-work-experience", "email-user-without-work-experience", "", true, "en");
                        }
                    }

                }

                //if (Request["submit-source"] != "competence") {
                Util.Phoenix.session.RequestResult passwordResult = null;
                Util.Phoenix.session.RequestResult userResult = activeUser.PhoenixSession.PhoenixRequest("PUT", session.Queries.UserPut, userSettings.ToString(), false, false);
                if (!tPassword1.Text.IsNullOrEmpty())
                {
                    passwordResult = activeUser.PhoenixSession.PhoenixPost(String.Format(session.Queries.UserUpdatePassword, System.Web.HttpUtility.UrlEncode(activeUser.Username)), new { NewPassword = tPassword1.Text });
                }
                activeUser.PhoenixSession.PhoenixClearCache();
                activeUser = Util.Classes.user.getUser(activeUser.ID, true);
                if (userResult.Error || (passwordResult != null && passwordResult.Error))
                {
                    Session["alert.once"] = (userResult.Error ? "Your data could not be saved. " : "") + (passwordResult != null && passwordResult.Error ? "Your password could not be updated." : "") + convert.cIfValue(Session["alert.once"], "<br />", "", "");
                    if (userResult.Error) userSettings = activeUser.PhoenixSession.PhoenixRequest("GET", session.Queries.UserGetCurrent, "", false, false).DataObject;
                }
                else
                {
                    if (Request["submit-source"] != "competence-file")
                        Session["alert.once"] = jlib.helpers.translation.translate(this.Language, "", this, "save-confirmation") + convert.cIfValue(Session["alert.once"], "<br />", "", "");
                    Response.Redirect("settings.aspx?" + convert.cIfValue(this.QueryStringStripKeys("id"), "", "&", "") + "id=" + activeUser.ID);
                }
                //}
            }

            List<object> competencyAttachments = new List<object>();
            if (convert.HasProperty(userCompetency, "Attachments"))
                foreach (dynamic attachment in userCompetency.Attachments)
                    competencyAttachments.Add(new Util.Classes.Attachment(attachment));
            lCompetencyAgree.OnClientClick = parse.replaceAll(lCompetencyAgree.OnClientClick, "{0}", jlib.helpers.translation.translate(this.Language, "You need to accept the terms before you can continue", "/enroll.aspx", Page, "competence-accept-terms"));

            iImage.ImageUrl = jlib.helpers.general.getThumbnailUrl(convert.cStr(activeUser.ImageUrl, "gfx/avatar.jpg"), (int)iImage.Width.Value, (int)iImage.Height.Value, true, jlib.helpers.general.FileStatus.None);
            pCompetencyContainer.Visible = ((activeUser.IsExamCompetenceInitiated) || activeUser.NagUserForExamCompetence);
            pCompetencyScriptContainer.Visible = pCompetencyContainer.Visible;
            if (!this.IsPostBack && pCompetencyContainer.Visible && activeUser.ExamCompetence == null) userCompetency.IsTermsAccepted = false;
            //if (pCompetencyContainer.Visible) {
            //    tAddress1.CssClass += " required";
            //    tZip.CssClass += " required";
            //    tCity.CssClass += " required";
            //    tCountry.CssClass += " required";
            //}

            string dob = convert.SafeGetProperty(userCompetency, "DateOfBirth");
            if (!dob.IsNullOrEmpty()) userCompetency.DateOfBirth = dob.SafeSplit("T", 0);
            string ssn = convert.SafeGetProperty(userCompetency, "SocialSecurityNumber");
            if (!ssn.IsNullOrEmpty()) userCompetency.SocialSecurityNumber = ssn.Substr(0, 6) + "*****";
            lScripts.Text += "var userSettings=" + userSettings.ToString() + ";\n"
            + "var companySettings=" + convert.cStr(Request["companySettings"], activeUser.PhoenixSession.PhoenixGet(String.Format(session.Queries.CustomerGet, activeUser.OrganizationID)).Data, "{}") + ";\n"
            + "var userCompetency=" + userCompetency.ToString() + ";\n"
            + "var userCompetencyAttachments=" + jlib.functions.json.DynamicJson.Serialize(competencyAttachments) + ";\n"
            + "var NagUserForExamCompetence=" + activeUser.NagUserForExamCompetence.Str().ToLower() + ";\n"
            ;

        }
    }
}