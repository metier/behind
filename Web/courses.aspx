﻿<%@ Page Title="myMetier Catalog" Language="C#" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Courses" MasterPageFile="~/inc/master/mymetier.master" Codebehind="courses.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">
<script type="text/javascript" src="./inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="./inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.css" />				
<div id="wrapper-introduction">
  <common:label runat="server" TranslationKey="heading" Tag="h1" />   
  <common:label runat="server" ID="lNoAvailableCourses" Tag="h3" style="font-style:italic" visible="false" TranslationKey="no-courses-available"/>
</div>
    <asp:PlaceHolder runat="server" ID="pFeaturedTemplate">
        <common:label runat="server" Tag="div" cssclass="coursebox" ID="lContainer">
            <common:image runat="server" ID="iCourseImage" width="223px" height="121px" Resize="false" ImageUrl="gfx/icon-course-02.png" />            
			<common:label runat="server" Tag="h1" DataMember="heading" />
			<div class="details"><common:label runat="server" Tag="span" cssclass="textleft" DataMember="course-type" /><common:label datamember="course-lessons" runat="server" Tag="span" cssclass="textright" /><div class="clear"></div></div>
			<common:label runat="server" tag="div" cssclass="details" DataMember="price"><common:label runat="server" Tag="span" cssclass="textleft" /><common:label runat="server" Tag="span" cssclass="textright" DataMember="price" /><div class="clear"></div></common:label>
			<common:label runat="server" Tag="p" DataMember="description" />
			<div class="enroll"><span class="textleft"><common:hyperlink runat="server" ID="lCourseInfo" cssclass="courseinfo" href="#" TranslationKey="course-info" /></span><span class="textright"><div class="actionbutton small"><common:hyperlink runat="server" DataMember="enroll" NavigateUrl="javascript:void(0)" TranslationKey="button-enroll"/></div></span><div class="clear"></div></div>
		</common:label>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="pFeaturedContainer">
	    <div class="featuredcourseswrapper">
            <common:label runat="server" ID="lFeaturedCourses" />		
		    <div class="clear"></div>
	    </div>
    </asp:PlaceHolder>
    
    <common:ajaxpane runat="server" id="aContainer">
<div class="supertable">
	<div class="header nine-rounded">
        <common:label runat="server" TranslationKey="table-heading" Tag="h1" />		
		<div class="search">
			<p><common:textbox runat="server" id="tSearchName" cssclass="search_input" OnClientChange="$('.current-page').val(0);AJAXControlAction($(this).attr('name'),null,null);" /></p>
		</div>
		<div class="clear"></div>
	</div>
	<div class="supertableleft">
		<div class="status">  <common:label runat="server" ID="lPagination1" Tag="p" /></div>

		<div class="sorter nine-rounded">
            <common:label runat="server" Tag="p" TranslationKey="improve-search" />
            <common:hidden runat="server" ID="hFilterType" CssClass="type-filter" />
            <common:hidden runat="server" ID="hCurrentPage" CssClass="current-page" />
            <common:hidden runat="server" ID="hCurrentSort" CssClass="current-sort" />
            <common:label runat="server" ID="lFilterType" Tag="ul" CssClass="filter-remove"><li><common:label runat="server" id="lFilterClear" Tag="a" href="javascript:void(0);" onclick="$('.current-page').val(0);$('.type-filter').val('');AJAXControlAction($(this).attr('name'),null,null);" /></li></common:label>
			<common:label runat="server" Tag="h3" TranslationKey="course-types" />
				<ul>
					<common:label runat="server" Tag="li" ID="lFilterElearn"><a href="javascript:void(0);" onclick="$('.current-page').val(0);$('.type-filter').val(0);AJAXControlAction('type-filter',null,null);"><common:label runat="server" TranslationKey="filter-elearn" /></a></common:label>
					<common:label runat="server" Tag="li" ID="lFilterClassroom"><a href="javascript:void(0);" onclick="$('.current-page').val(0);$('.type-filter').val(1);AJAXControlAction('type-filter',null,null);"><common:label runat="server" TranslationKey="filter-classroom" /></a></common:label>
					<common:label runat="server" Tag="li" ID="lFilterEandC"><a href="javascript:void(0);" onclick="$('.current-page').val(0);$('.type-filter').val(4);AJAXControlAction('type-filter',null,null);"><common:label runat="server" TranslationKey="filter-ec" /></a></common:label>
					<common:label runat="server" Tag="li" ID="lFilterExam"><a href="javascript:void(0);" onclick="$('.current-page').val(0);$('.type-filter').val(3);AJAXControlAction('type-filter',null,null);"><common:label runat="server" TranslationKey="filter-exam" /></a></common:label>
				</ul>
		</div>
	</div>
    <asp:PlaceHolder runat="server" ID="pCourseTableListing">
        <tr class="highlight">			
            <common:label runat="server" Tag="td" DataMember="type" CssClass="first left-rounded blue" />
			<td><common:hyperlink runat="server" DataMember="title" NavigateUrl="#" /></td>
            <td><common:hyperlink runat="server" DataMember="info" NavigateUrl="#" TranslationKey="link-information"/></td>
			<td class="last right-rounded dark-blue"><common:hyperlink runat="server" DataMember="enroll" NavigateUrl="javascript:void(0)" TranslationKey="link-enroll"/></td>
		</tr>
		<tr>
			<td colspan="3" class="divider"></td>
		</tr>
    </asp:PlaceHolder>
		<table>
			<thead>
				<tr>
					<th style="width:60px"><common:label runat="server" TranslationKey="sort-type" /><a href="#" class="sort asc" onclick="$('.current-sort').val('type ' + ($('.current-sort').val().indexOf('desc')>-1?'asc':'desc'));AJAXControlAction();"><common:image ID="iSortType" runat="server" ImageUrl="~/gfx/icon-arrow-sort-{0}.gif" /></a></th>
					<th><common:label runat="server" TranslationKey="sort-course-name" /><a href="#" class="sort asc" onclick="$('.current-sort').val('name ' + ($('.current-sort').val().indexOf('desc')>-1?'asc':'desc'));AJAXControlAction();"><common:image ID="iSortName" runat="server" ImageUrl="~/gfx/icon-arrow-sort-{0}.gif" /></a></th>
					<th colspan="2"></th>
				</tr>
			</thead>
			<tbody>
                <common:label runat="server" ID="lCourseTable" />				
                <tr>
                <td></td>
                <td colspan="2" style="text-align:center" class="pagination"><asp:placeholder runat="server" ID="pPagination">
                <common:hyperlink runat="server" ID="hPagePrev" NavigateUrl="javascript:void(0)" Text="&lt;" CssClass="prev" />
                <common:hyperlink runat="server" ID="hPage1" NavigateUrl="javascript:void(0)" onclientclick="$('.current-page').val(Number($(this).html())-1);AJAXControlAction($(this).attr('name'),null,null);" />
                <common:hyperlink runat="server" ID="hPage2" NavigateUrl="javascript:void(0)" onclientclick="$('.current-page').val(Number($(this).html())-1);AJAXControlAction($(this).attr('name'),null,null);" />
                <common:hyperlink runat="server" ID="hPage3" NavigateUrl="javascript:void(0)" onclientclick="$('.current-page').val(Number($(this).html())-1);AJAXControlAction($(this).attr('name'),null,null);" />
                <common:hyperlink runat="server" ID="hPageNext" NavigateUrl="javascript:void(0)" Text="&gt;" CssClass="next" />
                </asp:placeholder>
                </td>
                </tr>
                <asp:PlaceHolder runat="server" ID="pTermsContainer">
                <tr>
                <td></td>
                <td colspan="2" style="text-align:center;font-weight:normal">
                <br /><br />
                &bull;&nbsp;&nbsp;<common:hyperlink runat="server" TranslationKey="terms-link" NavigateUrl="javascript:void(0)" OnClientClick="fnDisplayTermsDialog();" />&nbsp;&nbsp;&bull;                
                </td>
                </tr>
                </asp:PlaceHolder>
			</tbody>
		</table>                
</div>
        <br style="clear:both" />
<script language="javascript">
    fnEnableJSEffects();
</script>
</common:ajaxpane>
<script language="javascript">

    $("form").submit(function () { return false; });
    $(document).on("keydown", ".search_input", function (e) { if (e.which == 13) $(this).blur(); });
</script>
</asp:Content>