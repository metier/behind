﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Linq;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.components;
using jlib.functions;
using System.Text;
using System.Security.Cryptography;
namespace Phoenix.LearningPortal
{
    public partial class User : jlib.components.webpage
    {

        public void Page_Load(object sender, EventArgs e)
        {
            Util.Classes.user currentUser = (this.Master as Inc.Master.MyMetier).ActiveUser;
            List<object> pduInfoList = new List<object>();

            (Page.Master as Inc.Master.MyMetier).Section = Inc.Master.MyMetier.SiteSection.Merits;
            Util.Classes.user activeUser = Util.Classes.user.getUser(convert.cInt(this.QueryString("id"), currentUser.ID));
            if ((currentUser.ID != activeUser.ID && activeUser.Organization.CompetitionMode == 2) || (convert.cInt(activeUser.Organization.ParentCompanyID, activeUser.OrganizationID) != convert.cInt(currentUser.Organization.ParentCompanyID, currentUser.OrganizationID))) Response.Redirect("error.aspx");

            lUserName.setValue(activeUser.FirstName + " " + activeUser.LastName);
            iUserImage.ImageUrl = jlib.helpers.general.getThumbnailUrl(convert.cStr(activeUser.ImageUrl, "gfx/avatar.jpg"), (int)iUserImage.Width.Value, (int)iUserImage.Height.Value, true, jlib.helpers.general.FileStatus.None);
            if (activeUser.Organization.CompetitionMode == 2)
            {
                pRankingContainer.Visible = false;
            }
            else
            {

                for (int x = 0; x < activeUser.Colleagues.Count && x < 4; x++)
                {
                    Control oControl = jlib.helpers.control.cloneObject(pRankingTemplate) as Control;
                    List<jlib.components.iControl> oControls = jlib.helpers.control.getDynamicControls(oControl, null);
                    if (x == 3 && currentUser.UserRank > 3)
                    {
                        x = currentUser.UserRank - 1;
                        (jlib.helpers.control.findControlsByDataMember(oControls, "li")[0] as label).CssClass += "last";
                    }
                    jlib.helpers.control.populateValue(oControls, "rank", (activeUser.Organization.CompetitionMode == 0 ? (x + 1).Str() : ""));
                    jlib.helpers.control.populateValue(oControls, "text", String.Format(jlib.helpers.translation.translate(this.Language, "", this, "ranking" + (activeUser.Organization.CompetitionMode == 0 ? "-competition" : "")), activeUser.Colleagues[x].FirstName + " " + activeUser.Colleagues[x].LastName, (activeUser.Colleagues[x].Id == activeUser.ID ? activeUser.PointCount : activeUser.Colleagues[x].TotalScore)));
                    (jlib.helpers.control.findControlsByDataMember(oControls, "image")[0] as image).ImageUrl = jlib.helpers.general.getThumbnailUrl(convert.cStr(activeUser.Colleagues[x].ImageUrl, "gfx/avatar.jpg"), (int)iUserImage.Width.Value, (int)iUserImage.Height.Value, true, jlib.helpers.general.FileStatus.None);
                    (jlib.helpers.control.findControlsByDataMember(oControls, "image")[0] as image).AlternateText = activeUser.Colleagues[x].FirstName + " " + activeUser.Colleagues[x].LastName;
                    (jlib.helpers.control.findControlsByDataMember(oControls, "user-link")[0] as hyperlink).NavigateUrl = "user.aspx?id=" + activeUser.Colleagues[x].Id;

                    lRanking.Controls.Add(oControl);
                }
            }
            pRankingTemplate.Controls.Clear();
            List<int> iProcessedClassrooms = new List<int>();
            int[] iCounter = new int[2];


            for (int x = 0; x < activeUser.Enrollments.Count; x++)
            {

                if (!iProcessedClassrooms.Contains(activeUser.Enrollments[x].ActivityId) || activeUser.Enrollments[x].ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.NoShow)
                {

                    List<Util.Classes.Enrollment> oILM = new List<Util.Classes.Enrollment>() { activeUser.Enrollments[x] };
                    List<Util.Classes.Enrollment> oELearnSession = new List<Util.Classes.Enrollment>();

                    bool bChanged = false;
                    if (activeUser.Enrollments[x].ActivitySetGroupingId > 0)
                    {
                        oILM = activeUser.Enrollments.FindAll(enrollment => { return enrollment.ActivitySetGroupingId == activeUser.Enrollments[x].ActivitySetGroupingId; });
                        oILM.Sort((a, b) => b.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.NoShow ? 0 : 1);
                        for (int y = 0; y < oILM.Count; y++)
                        {
                            if (oILM[y].ArticleType == Util.Data.MetierClassTypes.Exam || oILM[y].ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.NoShow)
                            {
                                oILM[y].ActivitySetGroupingId = 0;
                                if (oILM.Count > 1)
                                {
                                    Util.Classes.Enrollment oCurrentRow = oILM[y];
                                    oILM.RemoveAt(y);
                                    y--;
                                    bChanged = true;
                                    if (oILM.Count > 0 && oCurrentRow.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.NoShow) oILM[0].ActivitySetName = oILM[0].ActivityName;
                                    oCurrentRow.ActivitySetName = oCurrentRow.ActivityName;
                                }
                            }
                        }
                    }
                    if (bChanged && activeUser.Enrollments[x].ActivitySetGroupingId == 0) oILM = new List<Util.Classes.Enrollment>() { activeUser.Enrollments[x] };
                    for (int y = 0; y < oILM.Count; y++) iProcessedClassrooms.AddIfNotExist(oILM[y].ActivityId);

                    //Remove NoShow-course if user is enrolled in a new activity with same article #
                    if (oILM[0].ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.NoShow)
                    {
                        if (activeUser.Enrollments.FirstOrDefault(enrollment => enrollment.ParticipantStatusCodeValue != Util.Classes.Enrollment.ParticipantStatusCodes.NoShow && enrollment.ArticleNumber == oILM[0].ArticleNumber) != null) continue;
                    }

                    int iLevel = 1, pduCount = 0, classroomDayCount = 0;
                    for (int y = 0; y < oILM.Count; y++)
                    {
                        if (oILM[y].ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Completed || oILM[y].ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Passed)
                            pduCount += oILM[y].ArticleCopyObject.PDU;
                        if (oILM[y].ArticleType == Util.Data.MetierClassTypes.ELearning)
                        {
                            oELearnSession.Add(oILM[y]);
                        }
                        else if (oILM[y].ArticleType == Util.Data.MetierClassTypes.Classroom)
                        {
                            if (oILM[y].ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Completed)
                                oILM[y].ActivityPeriods.ForEach(period => { if (!period.Start.IsNullOrEmpty() && !period.End.IsNullOrEmpty()) classroomDayCount += Math.Ceiling(period.End.Value.Subtract(period.Start.Value).TotalDays).Int(); });
                        }
                        if (oILM[y].ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.NoShow) iLevel = 2;
                        else if (oILM[y].ArticleType != Util.Data.MetierClassTypes.Accommodation && ((oILM[y].ArticleType == Util.Data.MetierClassTypes.Classroom && ((oILM[y].ActivityEnd.Dte() >= DateTime.Now || oILM[y].ActivityStart.Dte() >= DateTime.Now) && oILM[y].ParticipantStatusCodeValue != Util.Classes.Enrollment.ParticipantStatusCodes.Completed)) || (oILM[y].ArticleType != Util.Data.MetierClassTypes.Classroom && oILM[y].ParticipantStatusCodeValue != Util.Classes.Enrollment.ParticipantStatusCodes.Completed))) iLevel = 0;
                    }

                    if (iLevel == 1 || iLevel == 2)
                    {
                        Control oControl = jlib.helpers.control.cloneObject(pCourseTemplate) as Control;
                        List<jlib.components.iControl> oControls = jlib.helpers.control.getDynamicControls(oControl, null);
                        jlib.helpers.control.populateValue(oControls, "course_last_accessed", convert.cDate(activeUser.Enrollments[x].CompletedDate).Day + " " + Util.MyMetier.getUserCulture(this.Language).DateTimeFormat.MonthNames[(int)convert.cDate(activeUser.Enrollments[x].CompletedDate).Month - 1].Substring(0, 3).ToLower() + " " + convert.cDate(activeUser.Enrollments[x].CompletedDate).Year);
                        hyperlink oCourseLink = (jlib.helpers.control.findControlsByDataMember(oControls, "name")[0] as hyperlink);
                        //+ (pduCount > 0 ? String.Format(" " + jlib.helpers.translation.translate(this.Language, "", this, "pdu"), pduCount) : "") 
                        oCourseLink.setValue(convert.cStr(activeUser.Enrollments[x].ActivitySetName, activeUser.Enrollments[x].ActivityName) + (classroomDayCount > 0 ? String.Format(" " + jlib.helpers.translation.translate(this.Language, "", this, "daycount"), classroomDayCount) : ""));


                        hyperlink oDiplomaLink = (jlib.helpers.control.findControlsByDataMember(oControls, "diploma-link")[0] as hyperlink),
                        pduLink = (jlib.helpers.control.findControlsByDataMember(oControls, "pdu-link")[0] as hyperlink);

                        oCourseLink.NavigateUrl = "navigator.aspx?class_id=" + activeUser.Enrollments[x].ActivityId;

                        if (currentUser.ID == activeUser.ID)
                        {
                            if (oELearnSession.Count > 0 && iLevel == 1) oDiplomaLink.NavigateUrl = "diploma.aspx?classroom_id=" + oELearnSession[0].ActivityId + "&ilm_id=" + oELearnSession[0].ActivitySetGroupingId;
                            else oDiplomaLink.Visible = false;

                            if (pduCount > 0 && iLevel == 1)
                            {
                                pduLink.OnClientClick = "ShowPDUInfo(" + activeUser.Enrollments[x].ActivityId + ");";
                                pduInfoList.Add(new
                                {
                                    Id = activeUser.Enrollments[x].ActivityId,
                                    Name = convert.cStr(activeUser.Enrollments[x].ActivitySetName, activeUser.Enrollments[x].ActivityName),
                                    PDU = pduCount,
                                    ClassroomDayCount = classroomDayCount,
                                    ProviderNumber = (oELearnSession.Count > 0 ? oELearnSession[0].ArticleCopyObject.ProviderNumber : ""),
                                    ActivityNumber = (oELearnSession.Count > 0 ? oELearnSession[0].ArticleCopyObject.ActivityNumber : ""),
                                    ProviderName = (oELearnSession.Count > 0 ? oELearnSession[0].ArticleCopyObject.ProviderName : ""),
                                    ActivityName = (oELearnSession.Count > 0 ? oELearnSession[0].ActivityName : "")
                                });
                            }
                            else pduLink.Visible = false;
                        }
                        else
                        {
                            (jlib.helpers.control.findControlsByDataMember(oControls, "download-diploma")[0] as label).CssClass = "right-rounded green last";
                            oDiplomaLink.Visible = false;
                            (jlib.helpers.control.findControlsByDataMember(oControls, "download-pdu")[0] as label).Visible = false;
                        }

                        if (iLevel == 1) lCourses.Controls.Add(oControl);
                        else lCoursesNoShow.Controls.Add(oControl);
                    }
                }
            }

            if (lCourses.Controls.Count == 0) lCourses.Text = "<td style='padding-left:30px'>" + String.Format(jlib.helpers.translation.translate(this.Language, "", this, (currentUser.ID == activeUser.ID ? "no-completed-courses-you" : "no-completed-courses-user")), activeUser.FirstName + " " + activeUser.LastName) + "</td>";

            pCoursesNoShow.Visible = lCoursesNoShow.Controls.Count > 0 && currentUser.ID == activeUser.ID;
            pCourseTemplate.Controls.Clear();
            if (currentUser.ID == activeUser.ID || activeUser.Organization.CompetitionMode == 0)
            {
                lUserPoints.Text = String.Format(lUserPoints.Text, activeUser.PointCount, (activeUser.MedalCount[0] + activeUser.MedalCount[1] + activeUser.MedalCount[2]));
            }
            else
            {
                lUserPoints.Visible = false;
                pMedalsPane.Visible = false;
            }

            lMedalsBronze.setValue(activeUser.MedalCount[2]);
            lMedalsSilver.setValue(activeUser.MedalCount[1]);
            lMedalsGold.setValue(activeUser.MedalCount[0]);
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "pdu-info", "var __pduInfo=" + jlib.functions.json.DynamicJson.Serialize(pduInfoList) + ";", true);
        }
    }
}