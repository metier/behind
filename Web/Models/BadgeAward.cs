﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace Phoenix.LearningPortal.Models
{
    public class BadgeAward
    {
        [JsonProperty(PropertyName = "uid")]
        public string Uid{get;set;}
        [JsonProperty(PropertyName = "recipient")]
        public BadgeAwardRecipient Recipient { get;set;}
        [JsonProperty(PropertyName = "issuedOn")]
        public long IssuedOn { get; set; }
        [JsonProperty(PropertyName = "badge")]
        public string Badge { get; set; }
        [JsonProperty(PropertyName = "verify")]
        public BadgeAwardVerification Verify { get; set; }
    }
    public class BadgeAwardRecipient
    {
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; } = "email";
        [JsonProperty(PropertyName = "identity")]
        public string Identity { get; set; }
        [JsonProperty(PropertyName = "hashed")]
        public bool Hashed { get; set; } = false;        
    }
    public class BadgeAwardVerification
    {
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; } = "hosted";
        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }        
    }
}