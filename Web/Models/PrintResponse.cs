﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.LearningPortal.Models
{
    public class PrintResponse
    {
        public string Message{get;set;}        
        public string Url {get;set;}
        public string MessageIcon { get; set; }
    }
}