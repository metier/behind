﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace Phoenix.LearningPortal.Models
{
    public class Badge
    {
        [JsonProperty(PropertyName = "name")]
        public string Name{get;set;}
        [JsonProperty(PropertyName = "description")] 
        public string Description {get;set;}
        [JsonProperty(PropertyName = "image")]
        public string Image { get; set; } = "https://mymetier.net/learningportal/badges/t2.png";
        [JsonProperty(PropertyName = "criteria")] 
        public string Criteria { get; set; }
        [JsonProperty(PropertyName = "issuer")]
        public string Issuer { get; set; } = "https://mymetier.net/learningportal/badges/issuer-metieroec.json";
    }
}