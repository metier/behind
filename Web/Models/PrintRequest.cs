﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.LearningPortal.Models
{
    public class PrintRequest
    {
        public int PageId{get;set;}
        public int LessonId { get; set; }
        public string Command { get;set;}
        public List<string> Options {get;set;}
        public string FileList {get;set;}
        public string ScormData {get;set;}
        public string ToEmail {get;set;}
		public List<string> PageTypes { get; set; }
		public List<string> ExcludedPageTypes { get; set; }
		public int FromLesson { get; set; }
		public int ToLesson { get; set; }
		public string Source { get; set; } //CMS, LMS
		public string BaseUrl { get; set; } //CMS, LMS
		public string LanguageCode { get; set; } //CMS, LMS

	}
}