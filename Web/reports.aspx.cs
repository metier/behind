﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Data.Common;
using System.Web;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using jlib.db;
using jlib.components;
using jlib.functions;
using System.Text;
using System.Security.Cryptography;
namespace Phoenix.LearningPortal
{
    public partial class Reports : jlib.components.webpage
    {

        public void Page_Init(object sender, EventArgs e)
        {

            (Page.Master as Inc.Master.MyMetier).Section = Inc.Master.MyMetier.SiteSection.Reporting;            

            Util.Classes.user activeUser = (Page.Master as Inc.Master.MyMetier).ActiveUser;
            if (!activeUser.IsLearningPortalSuperUser) Response.Redirect("error.aspx");

            hPEICEReport.Visible = activeUser.DistributorId == 10000;

            if (Request["action"] == "download")
            {
                //http://phoenix/api/reports/progressstatus/xlsx?customerId=2339&lessonStatus=N&lessonStatus=C&lessonStatus=F&productId=5&productId=4
                string url = "reports/progressstatus/xlsx?customerId=" + activeUser.OrganizationID + (Request["productId"].IsNullOrEmpty() ? "" : "&productId=" + Request["productId"]);
                Util.Phoenix.session.RequestResult result = activeUser.PhoenixSession.PhoenixRequest("GET", url, null, false, false, true);
                Response.ContentType = result.WebResponse.ContentType;
                if (result.WebResponse.Headers["Content-Disposition"] != null)
                    Response.AddHeader("Content-Disposition", result.WebResponse.Headers["Content-Disposition"]);
                Response.BinaryWrite(convert.cStreamToByteArray(result.WebResponse.GetResponseStream()));
                Response.End();
            }else if (hPEICEReport.Visible && Request["action"] == "download-peice")
            {
                var ado = new ado_helper("sql.dsn.cms");
                var sql = @"select * from v_PEICE_User_Progress";
                var table = sqlbuilder.getDataTable(ado, sql);

                SpreadsheetGear.IWorkbook WorkbookFile = SpreadsheetGear.Factory.GetWorkbook();
                int SheetNumber = 0, RowCounter = 0;
                WorkbookFile.Worksheets[SheetNumber].Name = "Participants";
                var oValues = (SpreadsheetGear.Advanced.Cells.IValues)WorkbookFile.Worksheets[SheetNumber];

                for (int y = 0; y < table.Columns.Count; y++)
                {
                    WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, y].Font.Size = 12;
                    WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, y].Interior.Color = System.Drawing.Color.FromArgb(91, 0, 0);
                    WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, y].Font.Color = System.Drawing.Color.White;
                    WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, y].Font.Bold = true;
                    oValues.SetText(RowCounter, y, table.Columns[y].Caption);
                }
                RowCounter++;
                foreach (DataRow row in table.Rows)
                {
                    for (int y = 0; y < table.Columns.Count; y++)
                    {
                        if (convert.isNumeric(row[y]))
                            oValues.SetNumber(RowCounter, y, row[y].Dbl());
                        else
                            oValues.SetText(RowCounter, y, row[y].Str());
                    }
                    RowCounter++;
                }

                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment; filename=\"PEICE%20Progress%20Report.xlsx\"");
                WorkbookFile.SaveToStream(Response.OutputStream, SpreadsheetGear.FileFormat.OpenXMLWorkbook);
                Response.End();
            }

            Dictionary<int, string> products = new Dictionary<int, string>();

            activeUser.AvailableCoursesRaw.ForEach(x =>
            {
                x.Activities.ForEach(y =>
                {
                    if (!products.ContainsKey(convert.cInt(y.ArticleCopyObject.Data.Product.Id)))
                        products.Add(convert.cInt(y.ArticleCopyObject.Data.Product.Id), convert.cStr(y.ArticleCopyObject.Data.Product.Title) + " (" + y.ArticleCopyObject.Data.Product.ProductPath + ")");
                });
            });
            //var session = new Util.Phoenix.session().PhoenixGet(Phoenix.session.Queries.DistributorsGet, true, true).DataObject;

            Util.Phoenix.session.RequestResult customerCoursePrograms = activeUser.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.CustomerCoursePrograms, activeUser.OrganizationID), true, true);
            foreach (var program in customerCoursePrograms.DataObject)
            {
                Util.Phoenix.session.RequestResult programProducts = activeUser.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ProductsInCoursePrograms, program.Id), true, true);
                foreach (var product in programProducts.DataObject)
                {
                    if (!products.ContainsKey(convert.cInt(product.Id)))
                        products.Add(convert.cInt(product.Id), convert.cStr(product.Title + " (" + product.ProductPath + ")"));
                }
            }
            var productList = products.ToList();
            productList.Sort((a, b) => a.Value.CompareTo(b.Value));
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "courses", "var productList=" + jlib.functions.json.DynamicJson.Serialize(productList) + ";", true);

        }
    }
}