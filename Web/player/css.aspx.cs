﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.functions;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Player
{

    public partial class CSS : jlib.components.webpage
    {

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Response.ContentType = "text/css";
            Response.Clear();
            if (Request.QueryString["CourseID"].Int() > 0)
            {
                AttackData.Folder courseObject = AttackData.Folder.LoadByPk(Request.QueryString["CourseID"].Int());
                if (courseObject != null) Response.Write(courseObject.getSetting("CustomCSS"));
            }
            Response.Cache.SetExpires(DateTime.Now.AddHours(1));
            Response.End();
        }
    }
}