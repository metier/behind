﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.functions;

namespace Phoenix.LearningPortal.Player
{
    public partial class Scorm : jlib.components.webpage
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {

        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Util.Classes.user activeUser = Util.Classes.user.getUser(new Util.Phoenix.session().GetUserId());
            Response.ContentType = "application/json";
            object disableLoginRedirect = System.Web.HttpContext.Current.Items["DisableLoginRedirect"];
            System.Web.HttpContext.Current.Items["DisableLoginRedirect"] = true;
            Util.Phoenix.session.RequestResult result = null;
            for (int x = 0; x < 3; x++)
            {
                string payload = (Request.ContentEncoding == null ? convert.cStreamToString(Request.InputStream) : convert.cStreamToString(Request.InputStream, Request.ContentEncoding));
                result = activeUser.PhoenixSession.PhoenixRequest(Request.HttpMethod, Util.Phoenix.session.Queries.ScormServer + Request["url"], payload, false, false);
                if (result.HttpResponseCode != 500) break;
                //Util.Email.sendEmail("noreply@mymetier.net", "jorgen@vontangen.com", "", "", result.HttpResponseCode + " / Attempt: " + x + " - SCORM API Error - " + DateTime.Now.ToLongTimeString() + " (" + activeUser.Username + ") - " + Request.UserHostAddress + " / " + activeUser.PhoenixSession.GetPhoenixCookie(), "URL: " + Request.HttpMethod + " " + Util.Phoenix.session.Queries.ScormServer + Request["url"] + "<br />" + convert.cStreamToString(Request.InputStream) + "<br /><br />" + result.HttpResponseCode + " - " + result.HttpErrorMessage + "<br /><br />" + result.Data + "<br /><br />" + Request.UserAgent, true, null);
            }
            System.Web.HttpContext.Current.Items["DisableLoginRedirect"] = disableLoginRedirect;
            if (result.HttpResponseCode != 200)
            {
                Response.StatusCode = (result.HttpResponseCode == 401 ? 403 : result.HttpResponseCode);
                Response.Write(result.HttpResponseCode == 401 ? "Your session has timed out." : result.HttpErrorMessage);
                //if (result.HttpResponseCode != 500) Util.Email.sendEmail("noreply@mymetier.net", "jorgen@vontangen.com", "", "", result.HttpResponseCode + " - SCORM API Error - " + DateTime.Now.ToLongTimeString() + " (" + activeUser.Username + ") - " + Request.UserHostAddress + " / " + activeUser.PhoenixSession.GetPhoenixCookie(), "URL: " + Request.HttpMethod + " " + Util.Phoenix.session.Queries.ScormServer + Request["url"] + "<br />" + convert.cStreamToString(Request.InputStream) + "<br /><br />" + result.HttpResponseCode + " - " + result.HttpErrorMessage + "<br /><br />" + result.Data + "<br /><br />" + Request.UserAgent, true, null);
            }
            Response.Write(result.Data);
            Response.End();
        }
    }
}