﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using jlib.functions;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using AttackData = Phoenix.LearningPortal.Data;
namespace Phoenix.LearningPortal.Player
{


    public partial class Player : jlib.components.webpage
    {
        Inc.Master.Courseplayer oMaster;        
        private string PlayerSettings;
        AttackData.Folder CourseObject=null, LessonObject = null, PageObject = null;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_PreInit(object sender, EventArgs e)
        {
            oMaster = (Page.Master as Inc.Master.Courseplayer);
            oMaster.DisableLoginRedirect = true;
            oMaster.Page_PreInit(sender, e);
            //(Page.Master as Inc.Master.Courseplayer).DisableWrapper = true;            
        }
        
        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (this.CourseObject != null)
                {
                    int DictionaryID = Session["dictionary.id-" + this.CourseObject.ObjectID].Int();
                    if (DictionaryID == 0)
                    {
                        DictionaryID = Phoenix.LearningPortal.Util.MyMetier.GetDictionaryForCourse(CourseObject.AuxField1.Str(), oMaster.CurrentOrg.MasterCompanyID);
                        Session["dictionary.id-" + this.CourseObject.ObjectID] = (DictionaryID == 0 ? -1 : DictionaryID);
                    }

                    if (DictionaryID > 0)
                    {
                        AttackData.Folder Dictionary = AttackData.Folder.LoadByPk(DictionaryID);
                        if (Dictionary != null)
                        {
                            AttackData.Content DictionaryContent = Dictionary.GetChildren<AttackData.Content>().FirstOrDefault();
                            if (DictionaryContent != null)
                            {
                                XmlNodeList xmlEntries = xml.loadXml(DictionaryContent.Xml).SelectNodes("dictionary/entry");
                                List<string[]> DictionaryEntries = new List<string[]>();
                                for (int x = 0; x < xmlEntries.Count; x++)
                                    if (!xml.getXmlNodeValue(xmlEntries[x], "key").Trim().IsNullOrEmpty()) DictionaryEntries.Add(new string[] { xml.getXmlNodeValue(xmlEntries[x], "key"), parse.replaceAll(xml.getXmlNodeValue(xmlEntries[x], "value"), "\n", "<br />") });

                                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "dictionary", "$(document).data(\"__dictionary\"," + jlib.functions.json.DynamicJson.Serialize(DictionaryEntries) + ");", true);
                            }
                        }
                    }
                    string customJS = CourseObject.getSetting("CustomJS");
                    if (!customJS.IsNullOrEmpty()) Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CustomJS", customJS, true);
                    string customCSS = CourseObject.getSetting("CustomCSS");
                    if (LessonObject != null) customCSS += LessonObject.getSetting("CustomCSS");
                    if (!customCSS.IsNullOrEmpty()) Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CustomCSS", "<style> " + customCSS + "</style>", false);
                    object nextLessonImage = Session["NextLessonImage-" + (this.LessonObject == null ? 0 : this.LessonObject.ObjectID)];
                    if (nextLessonImage == null)
                    {
                        //Lookup to see if there is a master-image defined for this course
                        IEnumerable<KeyValuePair<string, string>> SplashImagesFiltered = Util.MyMetier.GetSplashImages(CourseObject.AuxField1.Str(), oMaster.CurrentOrg.MasterCompanyID, oMaster.CurrentOrg.ID, -1, new List<string>() { "next-lesson." }, null);
                        if (SplashImagesFiltered.Count() > 0) nextLessonImage = SplashImagesFiltered.ToList()[0].Key;

                        //Get splash image associated with next lessons
                        if (nextLessonImage == null && LessonObject != null)
                        {
                            var lessons = CourseObject.GetChildren<AttackData.Folder>();
                            var lesson = lessons.FirstOrDefault(x => x.ObjectID == LessonObject.ObjectID);
                            if (lesson != null && lessons.IndexOf(lesson) < lessons.Count - 1)
                            {
                                var nextLesson = lessons[lessons.IndexOf(lesson) + 1];
                                var splashPage = nextLesson.GetChildren<AttackData.Folder>().FirstOrDefault(x => x.AuxField1 == "splash");
                                if (splashPage != null)
                                {
                                    var contentObjects = splashPage.GetChildren<AttackData.Content>();
                                    var match = contentObjects.FirstOrDefault(x => x.AuxField1 == "file" && (x.Filename.Str().EndsWith(".png") || x.Filename.Str().EndsWith(".jpg") || x.Filename.Str().EndsWith(".jpeg")));
                                    if (match != null) nextLessonImage = match.Filename;
                                    else
                                    {
                                        match = contentObjects.FirstOrDefault(x => x.AuxField1 == "splash" && x.Xml.Str().IndexOf("<img", StringComparison.CurrentCultureIgnoreCase) > -1);
                                        if (match != null)
                                            nextLessonImage = xml.getXmlAttributeValue(jlib.net.sgmlparser.SgmlReader.getXmlDoc(match.Xml).SelectSingleNode("//img"), "src");
                                    }
                                }
                            }
                            if (nextLessonImage.Str().IndexOf("thumbnail.aspx") > -1)
                                nextLessonImage = parse.inner_substring(nextLessonImage + "&", "&u=", null, "&", null);
                        }
                        Session["NextLessonImage-" + (this.LessonObject == null ? 0 : this.LessonObject.ObjectID)] = nextLessonImage.Str();
                    }
                    if (!nextLessonImage.IsNullOrEmpty()) lNextLessonImage.Style["background-image"] = "url('" + "https://mymetier.net/learningportal/inc/library/exec/thumbnail.aspx?z=1&w=108&h=58&q=100&u=" + nextLessonImage.Str() + "');";
                }

                dynamic settingsDynamic = jlib.functions.json.DynamicJson.Parse(convert.cStr(PlayerSettings, "{}"));

                Dictionary<string, string> translations = jlib.helpers.translation.getEntries(this.Language, Page, new List<string>() { "/player/splash.aspx", "/player/intro.aspx", "/player/pitfalls.aspx", "/player/reflection.aspx", "/player/theory.aspx", "/player/goals.aspx", "/player/references.aspx", "/player/checklist.aspx", "/player/case.aspx", "/player/quiz.aspx", "/player/complete.aspx", Page.Request.Url.AbsolutePath, "/inc/master/courseplayer.master" }, convert.SafeGetProperty(settingsDynamic, "Translation", "SiteId"), convert.SafeGetProperty(settingsDynamic, "Translation", "CompanyId"));
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "translation", "$(document).data(\"__translation\"," + translations.ToJSON() + ");", true);

                if (!convert.cBool(System.Configuration.ConfigurationManager.AppSettings["site.dev"])) Page.ClientScript.RegisterStartupScript(this.GetType(), "js-debug", "try{window.onerror=fnHandleWindowError;}catch(e){}", true);
                if (oMaster.ActiveUser != null & oMaster.ActiveUser.ID > 0)
                {
                    var settings = Util.Customization.Settings.GetSettings(oMaster.ActiveUser);
                    if (settings.PageTitlePrefix != null) Page.Title = settings.PageTitlePrefix;
                }
            }
            catch (Exception ex)
            {

                log.Error(ex);
            }
        }
        
        public void PrintCourse()
        {
            var options = new
            {
                PageId = Request["PageID"].Int(),
                LessonId = Request["LessonID"].Int(),
                Command = Request["__COMMAND"],
                Options = Request["__OPTIONS"],
                FileList = Request["__FILELIST"],
                ScormData = Request["__SCORM"],
                ToEmail = Request["__EMAIL"]
            };

            EvoPdf.Document Doc = new EvoPdf.Document();
            Doc.LicenseKey = "72FyYHVwYHBgd25wYHNxbnFybnl5eXlgcA==";//"31FCUEVAUEFHQVBGXkBQQ0FeQUJeSUlJSVBA";
            string copyrightMessage = CourseObject.getSetting("Copyright");
            int footerHeight = Util.MyMetier.GetPDFFooterHeight(copyrightMessage);
            List<Util.MyMetier.PrintPage> PrintPages = new List<Util.MyMetier.PrintPage>();
            log.Info("Getting lessons ");

            string DuplicateURLChecker = "";
            List<AttackData.Folder> LessonPages = null;
            if (options.PageId > 0) LessonPages = AttackData.Folder.LoadByPk(options.PageId).GetParents<AttackData.Folder>().FirstOrDefault().GetChildren<AttackData.Folder>();
            else LessonPages = AttackData.Folder.LoadByPk(options.LessonId).GetChildren<AttackData.Folder>();
            log.Info("Gotten lessons: " + LessonPages.Count + "  ");

            var excludedPageTypes = new List<string>();
            dynamic settingsDynamic = jlib.functions.json.DynamicJson.Parse(convert.cStr(CourseObject.getSetting("PlayerSettings"), "{}"));
            foreach (var item in convert.SafeGetList(settingsDynamic, "Print", "ExcludePageTypes"))
            {
                excludedPageTypes.Add(convert.cStr(item));
            }

            LessonPages.ForEach(Nav =>
            {
                if ((!excludedPageTypes.Contains(Nav.AuxField1) && Nav.AuxField1 != "splash" && Nav.AuxField1 != "complete" && Nav.AuxField1 != "global" && Nav.AuxField1 != "disclaimer" && (Nav.AuxField1 == "checklist" || options.Command != "download-checklist"))
                    || (!options.FileList.IsNullOrEmpty() && options.FileList.Contains(Nav.AuxField1.Str()))
                    )
                {

                    string Url = System.Configuration.ConfigurationManager.AppSettings["site.url"] + "/player/player.aspx?PageID=" + Nav.ObjectID + "&print=true";
                    string PostData = "__SCORM=" + System.Web.HttpUtility.UrlEncode(options.ScormData)
            + "&__OPTIONS=" + System.Web.HttpUtility.UrlEncode(options.Options)
            + "&__COMMAND=" + System.Web.HttpUtility.UrlEncode(options.Command);
                    if (DuplicateURLChecker.Contains(Url))
                    {
                        string Email = "I was about to print this page twice: " + Url + "<br /> Requested URL: " + Request.Url.AbsoluteUri + "<br />DuplicateURLChecker value: " + DuplicateURLChecker + "<br />Nav count: " + LessonPages.Count;
                        LessonPages.ForEach(x => { Email += "<br /> - " + x.Name + " / " + x.ObjectID; });
                        Util.Email.sendEmail("noreply@lp.mymetier.net", "jose.luis.suarez@metieroec.no", "", "", "Duplicate lesson page print detected", Email, true, null);
                    }
                    else
                    {
                        DuplicateURLChecker += ";" + Url;
                        PrintPages.Add(new Util.MyMetier.PrintPage(Url, PostData, Nav.AuxField1, footerHeight));
                        System.Threading.Thread Thread = new System.Threading.Thread(new System.Threading.ThreadStart(PrintPages[PrintPages.Count - 1].work));
                        Thread.Start();
                    }
                }
            });
            int Timeout = 60;
            while (Timeout > 0)
            {
                System.Threading.Thread.Sleep(1000);
                Timeout--;
                bool Completed = true;
                foreach (Util.MyMetier.PrintPage PrintStatus in PrintPages)
                {
                    if (!PrintStatus.Completed) Completed = false;
                }
                if (Completed)
                {
                    //string printLog = "==================================================\nPDF Started for user ID: " + (Page as jlib.components.webpage).QueryStringLong("user_id") + " -- " +System.DateTime.Now + "\n" ;
                    for (int x = 0; x < PrintPages.Count; x++)
                    {
                        //printLog+=x + " URL: " + PrintPages[x].Url + "\n";
                        if (PrintPages[x].Document != null)
                        {
                            Doc.AppendDocument(PrintPages[x].Document);
                            //  printLog += PrintPages[x].HTML;
                        }
                        //else
                        //{
                        //    printLog += "TIMEOUT!!!";
                        //}
                        //printLog += "\n\n-------------------------------------------------\n";
                    }
                    break;
                }
            }
            if (Doc.Pages.Count == 0)
            {
                log.Info("TIMEOUT for creating documents ");
                string Email = "PDF timeout! __COMMAND: " + options.Command + "<Br />__OPTIONS: " + options.Command + "<br />";
                foreach (Util.MyMetier.PrintPage PrintStatus in PrintPages)
                    Email += (PrintStatus.Document != null ? "Completed" : "NOT COMPLETED") + " -- " + PrintStatus.Url + "<br />";
                Util.Email.sendEmail("noreply@lp.mymetier.net", "jose.luis.suarez@metieroec.no", "", "", "PDF Conversion timeout!!", Email, true, null);
            }
            Util.MyMetier.PDFAddFooter(Doc, false, copyrightMessage, true, true);

            if (oMaster.ActiveUser!=null && oMaster.ActiveUser.DistributorId > 0)
            {
                var settings = Util.Customization.Settings.GetSettings(oMaster.ActiveUser);
                if (settings.PDFWatermark.IsNotNullOrEmpty())
                    Util.MyMetier.AddWatermark(Doc, settings.PDFWatermark);                
            }
            
            if (options.Str().Contains(",email,"))
            {
                Util.Email.sendEmail("kurssporsmal@metier.no", options.ToEmail, "", "", String.Format(jlib.helpers.translation.translate(this.Language, "", "/inc/master/courseplayer.master", Page, "email-subject"), this.LessonObject.Name, this.CourseObject.Name), jlib.helpers.translation.translate(this.Language, "", "/inc/master/courseplayer.master", Page, "email-body"), true, new List<KeyValuePair<string, byte[]>>() { new KeyValuePair<string, byte[]>(this.LessonObject.Name + ".pdf", Doc.Save()) });
                Response.Write("<div style='font-weight:bold; position:relative' id='email-sent-message'><img src='" + Request.ApplicationPath + "/inc/images/icons/email_go.png' style='position:absolute;left:-10px' />" + String.Format(jlib.helpers.translation.translate(this.Language, "", "/inc/master/courseplayer.master", Page, "email-sent"), options.ToEmail) + "</div>");
            }
            else
            {
                string sFileName = io.getUniqueFileName(System.Configuration.ConfigurationManager.AppSettings["temp.path"] + "\\" + String.Format("{0:yyyy-MM-dd HH-mm-ss}", DateTime.Now) + "-ELearning.pdf");
                Doc.Save(sFileName);
                Response.Write("<a href='" + Request.ApplicationPath + "/download.aspx?file=" + parse.replaceAll(sFileName, System.Configuration.ConfigurationManager.AppSettings["temp.path"] + "\\") + "' target='_blank'>" + jlib.helpers.translation.translate(this.Language, "", "/inc/master/courseplayer.master", Page, "download-button") + "</a>");
            }
            Response.End();
        }

        public void WriteFinaltestData(List<object> contentObjects)
        {
            string previewKey = Request.Form.AllKeys.FirstOrDefault(x => { return x.StartsWith("preview-data-"); });
            List<QuizData> QuizData = new List<QuizData>();
            dynamic sessionStatus = oMaster.ActiveUser.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ScormSessionGet, Request["scorm_attempt"]), false, false).DataObject;
            string suspendData = convert.cStr(convert.SafeGetProperty(sessionStatus, "SuspendData"));

            //todo get suspenddata for LessonID        
            if (!suspendData.IsNullOrEmpty())
            {
                List<string> arr = new List<string>();
                if (suspendData.Contains("quiz:q")) arr = parse.split(suspendData, "quiz:q").ToList();
                else
                {
                    parse.split(suspendData, "quiz-").ToList().ForEach(x =>
                    {
                        if (convert.isNumeric(x.SafeSplit(":", 0))) arr.Add(x.SafeSplit(":q", 1));
                    });
                }
                arr.ForEach(x =>
                {
                    if (!x.IsNullOrEmpty()) QuizData.Add(new QuizData(x.SafeSplit(";", 0), "", parse.inner_substring(x, ";", null, null, null, null)));
                });
            }
            for (int x = 0; x < QuizData.Count; x++)
            {
                //Only load from cache if we have saved suspend data on finaltest
                if (!QuizData[x].Answers.IsNullOrEmpty())
                {
                    AttackData.UserSetting QuizCache = AttackData.UserSettings.GetByField(UserID: oMaster.ActiveUser.ID.Int(), KeyName: "cache.finaltest", SubKey: LessonObject.ObjectID + ":" + QuizData[x].QuizID).Execute().FirstOrDefault();
                    if (QuizCache != null) QuizData[x].Xml = QuizCache.Value;

                }
            }
            //Make sure we have three unique quizzes in QuizIDs
            //jvt
            if (QuizData.Count < 3)
            {
                IList<AttackData.RowLink> QuizLinks = LessonObject.Children.FindAll(x => x.Table2 == "d_folders" && (x.Object2 as AttackData.Folder).AuxField1 == "quiz").Shuffle();
                for (int x = 0; x < QuizLinks.Count; x++)
                {
                    if (QuizData.Count > 2) break;
                    bool Found = false;
                    QuizData.ForEach(y => { if (y.QuizID == QuizLinks[x].ID2.Str()) Found = true; });
                    if (!Found) QuizData.Add(new QuizData(QuizLinks[x].ID2.Str(), "", ""));
                }
            }
            //jvt
            //Get XML for the three QuizIDs
            for (int x = 0; x < QuizData.Count; x++)
            {
                if (QuizData[x].Xml.IsNullOrEmpty())
                {
                    AttackData.Folder QuizObject = AttackData.Folder.LoadByPk(QuizData[x].QuizID.Int());
                    if (QuizObject != null)
                    {
                        AttackData.Content QuizContent = QuizObject.GetChildren<AttackData.Content>().SafeGetValue(0);
                        if (QuizContent != null)
                        {
                            QuizData[x].Xml = QuizContent.Xml;
                            if (oMaster.ActiveUser.ID > 0)
                            {
                                AttackData.UserSetting QuizCache = AttackData.UserSettings.GetByField(UserID: oMaster.ActiveUser.ID.Int(), KeyName: "cache.finaltest", SubKey: LessonObject.ObjectID + ":" + QuizData[x].QuizID).Execute().FirstOrDefault();

                                if (QuizCache == null)
                                    QuizCache = new AttackData.UserSetting()
                                    {
                                        UserID = oMaster.ActiveUser.ID.Int(),
                                        KeyName = "cache.finaltest",
                                        SubKey = LessonObject.ObjectID + ":" + QuizData[x].QuizID
                                    };
                                QuizCache.Value = QuizData[x].Xml;
                                QuizCache.Save();
                            }
                        }
                    }
                }
            }

            QuizData.ForEach(x =>
            {
                if (previewKey != null) x.Xml = Request[previewKey];
                contentObjects.Add(new { Name = "Final test", ObjectID = x.QuizID.Int(), Xml = x.Xml, AuxField1 = "quiz", Filename = "", Mode = "", Answers = x.Answers, Score = x.Score });
            });
            oMaster.DisableWrapper = true;
        }
        public class QuizData
        {
            public QuizData(string QuizID, string Xml, string Answers)
            {
                this.QuizID = QuizID;
                this.Xml = Xml;
                this.Answers = Answers;
            }
            public string QuizID { get; set; }
            public string Xml { get; set; }
            public int Score { get; set; }
            private string m_sAnswers;
            public string Answers
            {
                get { return m_sAnswers; }
                set
                {
                    Score = parse.inner_substring(value + ";", "score:", null, ";", null).Int();
                    m_sAnswers = "";
                    value.Split(';').ToList().ForEach(x => { if (!x.StartsWith("score:")) m_sAnswers += x + ";"; });
                    m_sAnswers = parse.stripEndingCharacter(m_sAnswers, ";");
                }
            }
        }

        private string WriteSplashImage(AttackData.Folder page)
        {

            string SplashImage = Session["splash-image." + page.ObjectID].Str();
            if (SplashImage == "")
            {
                int iLesson = -1;
                AttackData.Folder lesson = page.GetParents<AttackData.Folder>().FirstOrDefault();
                if (lesson != null)
                {
                    AttackData.Folder course = lesson.GetParents<AttackData.Folder>().FirstOrDefault();
                    if (course != null)
                    {
                        lesson = course.GetChildren<AttackData.Folder>().Where(x => x.ObjectID == lesson.ObjectID).FirstOrDefault();
                        if (lesson != null) iLesson = course.GetChildren<AttackData.Folder>().IndexOf(lesson) + 1;
                    }
                }

                IEnumerable<KeyValuePair<string, string>> SplashImagesFiltered = Util.MyMetier.GetSplashImages(CourseObject.AuxField1.Str(), oMaster.CurrentOrg.MasterCompanyID, oMaster.CurrentOrg.ID, iLesson, null, new List<string>() { "logo.", "next-lesson." });

                if (SplashImagesFiltered.Count() == 0) SplashImagesFiltered = AttackData.Folder.LoadByPk(69595).GetFiles(false);
                SplashImage = "https://mymetier.net/learningportal/inc/library/exec/thumbnail.aspx?w=1002&h=564&z=1&u=" + System.Web.HttpUtility.UrlEncode(SplashImagesFiltered.Count() > 0 ? SplashImagesFiltered.ToList().Shuffle()[0].Key : Request.ApplicationPath + "/player/gfx/splash-image.jpg");
                Session["splash-image." + page.ObjectID] = SplashImage;
            }
            return SplashImage;
        }
        private List<object> BuildContent(AttackData.Folder parentFolder, AttackData.Folder page, bool outputContent)
        {
            List<object> contentObjects = new List<object>();
            if (outputContent)
            {
                List<AttackData.Content> contents = page.GetChildren<AttackData.Content>();

                //if (LessonObject != null && LessonObject.AuxField1 == "finaltest")
                if (parentFolder.AuxField1 == "finaltest" && page.AuxField1 == "quiz")
                {
                    WriteFinaltestData(contentObjects);

                    //override for when previewing FinalTest in CMS
                    if (Request["preview"].Bln()) page.ObjectID = Request["PageID"].Int();
                }
                else
                {
                    if (contents.FirstOrDefault(x => { return !x.AuxField1.IsNullOrEmpty() && x.Type == (int)AttackData.Contents.Types.HTML; }) == null && (page.AuxField1 == "splash" || page.AuxField1 == "complete"))
                        contents.Add(new AttackData.Content() { Name = page.AuxField1.ToCamelCase(), AuxField1 = page.AuxField1, Type = (int)AttackData.Contents.Types.HTML });

                    contents.ForEach(x => { if (x.AuxField1 == "splash" && x.Type == (int)AttackData.Contents.Types.HTML) x.Filename = WriteSplashImage(page); });

                    for (int x = 0; x < contents.Count; x++)
                    {
                        AttackData.Content content = contents[x];
                        if ((this.QueryString("ContentID").Int() == 0 || this.QueryString("ContentID").Int() == content.ObjectID))
                        {
                            if (!Request["preview-data-" + content.ObjectID].IsNullOrEmpty())
                                content.Xml = Request["preview-data-" + content.ObjectID];

                            contentObjects.Add(new { Name = content.Name, ObjectID = content.ObjectID, Xml = content.Xml, AuxField1 = (content.Type == (int)AttackData.Contents.Types.FileUrl ? "file" : content.AuxField1), Filename = (content.Filename.Str().EndsWith(".htm") ? "" : content.Filename), Mode = content.getSetting("_Folder:" + page.ObjectID + "|Mode") });
                        }
                    }
                }
            }
            return contentObjects;
        }
        private object BuildContentObject(AttackData.Folder parentFolder, AttackData.Folder page, bool outputContent)
        {
            List<object> content = BuildContent(parentFolder, page, outputContent);
            return new { Name = page.Name, ObjectID = page.ObjectID, AuxField1 = page.AuxField1, Content = content, Title = convert.cStr(jlib.helpers.translation.translate(this.Language, "", this, page.AuxField1 + "-heading"), page.Name), Settings = MinifyJSON(page.getSetting("PlayerSettings")) };
        }
        private List<object> BuildFolders(AttackData.Folder folder, bool outputContent)
        {
            List<object> folders = new List<object>(),
                subFolders = new List<object>();

            List<AttackData.Folder> children = folder.GetChildren<AttackData.Folder>();
            if (folder.AuxField1 == "finaltest" && children.Count > 0)
            {
                folders.Add(BuildContentObject(folder, children[0], outputContent));
                //Add all non quiz-types
                children.ForEach(child=> {
                    if (child.AuxField1 != "quiz")
                    {
                        folders.Add(BuildContentObject(folder, child, true));                        
                    }
                });
            }
            else
            {
                for (int x = 0; x < children.Count; x++)
                {
                    if (children[x].Context_Value == "content")
                    {
                        folders.Add(BuildContentObject(folder, children[x],
                            (!Request["preview"].Bln() && (children[x].AuxField1 == "global"))
                            || (PageObject != null && PageObject.ObjectID == children[x].ObjectID) ? true : outputContent));
                    }
                    else
                    {
                        subFolders = BuildFolders(children[x], outputContent == true || (LessonObject != null && LessonObject.ObjectID == children[x].ObjectID) == true);
                        folders.Add(new { Name = children[x].Name, ObjectID = children[x].ObjectID, AuxField1 = children[x].AuxField1, Folders = subFolders, Settings = MinifyJSON(children[x].getSetting("PlayerSettings")) });
                    }
                }
            }
            return folders;
        }

        private string MinifyJSON(string JSON)
        {
            return parse.replaceAll(JSON, "\n", "", "\r", "");
        }
        

        protected void Page_Init(object sender, EventArgs e)
        {
            log.Info("Showing player for Page_Init for course: " + Request.QueryString["CourseID"]);

            try
            {
                if (Request.QueryString["PageID"].Int() == -1) Response.End();

                if (convert.isDate(Request["revision"])) System.Web.HttpContext.Current.Items["version.date"] = Request["revision"].Dte();

                oMaster.LookupCourseObjects(out CourseObject, out LessonObject, out PageObject);


                log.Info("PRINTING DOCUMENT - Revision ");

                log.Info(Request["revision"]);
                if (!convert.isDate(Request["revision"]))
                {
                    string revisionDate = CourseObject.getSetting("Revision date to publish");
                    if (revisionDate.IsNotNullOrEmpty() && convert.cDate(revisionDate) < DateTime.Now && !Request["preview"].Bln())
                    {
                        System.Web.HttpContext.Current.Items["version.date"] = convert.cDate(revisionDate);
                        oMaster.LookupCourseObjects(out CourseObject, out LessonObject, out PageObject);
                    }
                }

                if (CourseObject == null && PageObject != null) CourseObject = PageObject.GetParentUntil<AttackData.Folder>(10, new Func<AttackData.Folder, bool>(item => item.Context_Value == "course"));
                if (CourseObject != null)
                {
                    log.Info("CourseObject != null ");
                    PlayerSettings = CourseObject.getSetting("PlayerSettings");
                    if (Request["preview"].Bln() && !Request["editing"].Bln() && PlayerSettings.IsNotNullOrEmpty())
                    {
                        log.Info("Preview and different than editing");

                        dynamic settingsDynamic = jlib.functions.json.DynamicJson.Parse(convert.cStr(PlayerSettings, "{}"));
                        if (convert.cStr(convert.SafeGetProperty(settingsDynamic, "Player", "Id")) == "2")
                        {
                            var url = "/learningportal/portal2/#/course/" + CourseObject.ObjectID + "/lesson/" + LessonObject.ObjectID + (PageObject == null ? "" : "/page/" + PageObject.ObjectID) + "?Preview=true";
                            log.Info("Redirecting to " + url);

                            Response.Redirect(url);
                        };
                    }
                }

                if (Request["print"].Bln())
                {
                    log.Info("PRINTING BLN?");
                    dynamic JSON = new jlib.functions.json.DynamicJson();
                    JSON.PrintSCORM = Request["__SCORM"];
                    JSON.PrintOptions = Request["__OPTIONS"];
                    JSON.PrintCommand = Request["__COMMAND"];

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "menu", "$(document).data(\"__data\"," + JSON.ToString() + ");", true);

                }
                else if (Request["__PDF"].Bln())
                {
                    log.Info("PRINTING pdf?");

                    log.Info("PRINTING CONFIRMED");

                    PrintCourse();
                }
                log.Info("NO PDF, NO PRINT");
                if (Request.QueryString["editing"].Bln()) pEditingScript.Visible = true;
                AttackData.Folder objectWithLessons = (CourseObject != null ? CourseObject : LessonObject);
                if (objectWithLessons != null)
                {
                    dynamic json = new jlib.functions.json.DynamicJson();

                    List<object> folders = BuildFolders(objectWithLessons, false);
                    var course = new { Name = objectWithLessons.Name, Folders = folders, CourseCode = objectWithLessons.AuxField1, ObjectID = objectWithLessons.ObjectID };
                    json.Course = course;

                    if (Request.QueryString["format"] == "json")
                    {
                        Response.Clear();
                        Response.ContentType = "application/json";
                        Response.Write(json.ToString());
                        Response.End();
                    }

                    lContent.Text = "var _course=" + json.ToString() + ";"
                           + "var _playerSettings=" + "$.extend(true,{},{'Player':{'AutoZoomImages':true},'Quiz':{'MinimumRequiredScore':0}}," + convert.cStr(MinifyJSON(PlayerSettings), "{}") + ");";

                }

            }
            catch (Exception ex)
            {
                log.Error(ex);

            }
            
        }
    }
}