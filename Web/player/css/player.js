﻿var _Player = {};
if (window.location.href.indexOf("print=true") == -1) document.write("<div style='z-index:10000;background-color:white;position:absolute;width:100%;height:100%;' id='loading-screen'></div>");
$(document).ready(function () {
    if (typeof _course != 'undefined') {
        var fnAddParent = function (parent) {
            if (parent.Folders) {
                for (var x = 0; x < parent.Folders.length; x++) {
                    parent.Folders[x].Parent = parent;
                    fnAddParent(parent.Folders[x]);
                }
            }
            if (parent.Content)
                $.each(parent.Content, function (i, o) { o.Parent = parent;});
        };
        fnAddParent(_course.Course);
        _Player.Player = new clsCoursePlayer();
        _Player.Player.Load(cStr(getQuerystring("LessonID"), getHashstring("LessonID")), cStr(getQuerystring("PageID"), getHashstring("PageID")));
    }
});
$(window).bind("contextmenu", function (e) {
    if (!_Player.Player.Preview) e.preventDefault();
});
function clsCoursePlayer() {    
    this.Settings = _playerSettings;
    this.Course = _course.Course;    
    this.Preview = (window.location.href.indexOf("preview=true") > -1 || window.location.href.indexOf("demo=true") > -1);
    this.Editing = getQuerystring("editing") == "true";
    this.Printing = getQuerystring("print") == "true";
    this.FindPage = function (LessonId, PageId) { return fnFindPage(this.Course, LessonId, PageId); }
    this.MaskContent = function () {
        this.MaskContentCount = cInt(this.MaskContentCount) + 1;
        if ($("#contentMask").length == 0) $("<div id='contentMask' style='background-color:white;z-index:10000;position:absolute;left:0;right:0;bottom:0;'></div>").appendTo(document.body);        
        $("#contentMask").css("top",($("#wrapper-globalmenu").height()+1)+"px" );                    
    }
    this.UnMaskContent = function () {
        if (this.MaskContentCount > 0) this.MaskContentCount--;
        if (this.MaskContentCount == 0) $("#contentMask").detach();
    }
    this.FindFirstPage = function (Lesson) {
        if (this.ShouldAddToPriNav(Lesson)) return Lesson;
        if (Lesson.Folders)
            for (var z = 0; z < Lesson.Folders.length; z++)
                if (this.FindFirstPage(Lesson.Folders[z])) return this.FindFirstPage(Lesson.Folders[z]);
    }
    this.ScrollTo = function (Element, ScrollTop) {
        var PageBlock = $(Element);
        if (PageBlock.length == 0) return;
        var Blocks = $(".page-block");
        if(Blocks.length>0){
            if(!PageBlock.is(".page-block")) PageBlock = PageBlock.parents(".page-block");
            if (!PageBlock.hasClass("show")) {
                Blocks.removeClass("show");
                $(".page-block[data-page-block='" + PageBlock.attr("data-page-block") + "']").hide().addClass("show").fadeIn(_Player.Player.Loaded?500:0);
            }
            var Index = $(_Player.Player.PageBlocks).index($.grep(_Player.Player.PageBlocks, function (Item) { return Item.Id == PageBlock.attr("data-page-block"); })[0]);
            var LBlock = _Player.Player.PageBlocks[Index - 1];
            $("#page-block-nav .left").toggle(LBlock != null);
            if (LBlock)
                $("#page-block-nav .left").html("Prev: " + LBlock.Name).off("click.player").on("click.player", function () { _Player.Player.ScrollTo(Blocks.filter("[data-page-block='" + LBlock.Id + "']"), 0); });
                        
            var RBlock = _Player.Player.PageBlocks[Index + 1];
            $("#page-block-nav .right").toggle(RBlock!=null);            
            if (RBlock)
                $("#page-block-nav .right").html("Next: " + RBlock.Name).off("click.player").on("click.player", function () { _Player.Player.ScrollTo(Blocks.filter("[data-page-block='" + RBlock.Id + "']"), 0); });
        }
        ScrollTop = ($(Element).is(".page-block") ? ScrollTop : $(Element).position().top);
        if (!isNaN(ScrollTop) && cStr(ScrollTop)!="") {
            var diff = Math.abs(ScrollTop - $(window).scrollTop())
            $('html, body').animate({
                scrollTop: Math.max(0, ScrollTop - 5)
            }, Math.min(1000, diff));
        }
        $(window).scroll();
    }
    this.CleanupLessonChanged = function (NewLesson,NewPage) {
        $.extend(this, { AllowedPages: [], PagesWithCompletedQuizes: [], PagesWhereScrolledToBottom: [], LessonQuizes: null });
        var DisclaimerPagesLesson = $.grep(NewLesson.Folders, function (folder) { return folder.AuxField1 == "disclaimer"; });
        if (DisclaimerPagesLesson.length > 0) fnDisplayPageAsDialog(DisclaimerPagesLesson[0], {            
            dialogClass: "disclaimer disclaimer-" + NewLesson.AuxField1, closeOnEscape: false, modal: true, buttons: [{text: "Accept",click: function () {$(this).dialog("close");}}]
        });
    }
    this.Cleanup = function (Terminate) {
        $("#metier-msg").detach();
        if (this.Loaded && this.Page) {
            $("body").removeClass("page-" + this.Page.AuxField1);
            this.Page.ScrollTop = $(window).scrollTop();
            this.Page.ScrollBlock = cStr($(".page-block.show").attr("data-page-block"));
            if (this.Settings.Player.RestoreLastPosition)
                _Player.SCORM.setSuspendData("scrollposition-"+this.Page.ObjectID,this.Page.ScrollTop+"|"+this.Page.ScrollBlock);
        }
        this.Loaded = false;
        this.SuppressFirstH2 = false;
        this.SavePlayerValues(Terminate);
        $(window).detach("resize");
        $("#content-wrapper").html($("#content-container-template").html());
        $(".mainmenu-maximized tr, #nav-primary tbody, .popmenu-body ul").html("");
        if (_Player.Highlighter) _Player.Highlighter.Unload();
        _Player.Highlighter = null;
        this.PageNavMap = {};      
    }
    this.GetSCORMLesson = function (LessonId, PageId) {
        var _lessons = (_Player.SCORM.Win ? _Player.SCORM.Win.LessonData : null);
        if (_lessons && _lessons.Lessons) {
            for (var x = 0; x < _lessons.Lessons.length; x++) {
                var o = _lessons.Lessons[x];
                if (o.Url) {
                    var sTest = (o.Url.indexOf("PageID=") > -1 ? "PageID=" + PageId : (o.Url.indexOf("LessonID=") > -1 ? "LessonID=" + LessonId : o.URL));
                    if (o.Url.indexOf(sTest) > -1) {
                        return o;
                    }
                }
            }
        }
    }
    this.BuildSettings = function (Lesson, Page) {
        return $.extend(true, {}, _playerSettings, Lesson.Settings ? $.parseJSON(Lesson.Settings) : {}, Page.Settings ? $.parseJSON(Page.Settings) : {});
    }
    this.PrepareContent = function (Content) {
        $(Content).find("a[href]").not("[data-dialog]").not("[data-target]").not("[target]").each(function () {
            var h = $(this).attr("href").toLowerCase();
            if (h.indexOf("://") > -1 && h.indexOf("/learningportal/player/player.aspx") == -1) $(this).attr("target", "_blank");
        });
        $(Content).find("[data-dialog]").click(function () {
            var content = $($(this).attr("data-dialog"));
            if (content.length > 0) {
                fnDisplayDialog(content.html(), { title: content.find("h1").text() });
                return false;
            }
        });
        $(Content).find("map area, [data-target='dialog']").click(function () {
            var href = $(this).attr("href"), title = $(this).attr("alt");
            if (!title) title = $(this).attr("data-title");
            if (!$(this).attr("target") && !$(this).attr("onclick") && href && href.indexOf("#") != 0 && href.indexOf("javascript:") == -1) {
                var opts = { title: title };
                if (href.toLowerCase().indexOf(".png") > -1 || href.toLowerCase().indexOf(".jpg") > -1 || href.toLowerCase().indexOf(".gif") > -1 || href.toLowerCase().indexOf(".jpeg") > -1) {
                    fnDisplayDialog("<h1>" + title + "</h1><img src='" + href + "' alt='" + title + "' />", opts);
                } else {
                    if (href.indexOf("http") > -1 && href.indexOf(window.location.protocol + window.location.hostname) == -1) {
                        fnDisplayUrl(href, title);
                    } else {
                        $.ajax({
                            url: href,
                            cache: false,
                            success: function (data) {
                                if ($(data).find(".wrapper.main-text").length > 0)
                                    fnDisplayDialog($(data).find(".wrapper.main-text"), opts);
                                else
                                    fnDisplayDialog(data, opts);
                            }
                        });
                    }
                }
                return false;
            }
        });
    }
    this.SetLessonAndPageIDs = function (LessonID, PageID) {
        var o = fnFindPage(this.Course, LessonID, PageID);
        if (!o) o = fnFindPage(this.Course, LessonID);
        if (!o) o = fnFindPage(this.Course);
        if (!this.Lesson || this.Lesson != o.Lesson) this.CleanupLessonChanged(o.Lesson,o.Page);
        this.Lesson = o.Lesson;
        this.Page = o.Page;        
        this.Settings = this.BuildSettings(this.Lesson, this.Page);
                    
        this.PageID = this.Page.ObjectID;
        this.LessonID = this.Lesson.ObjectID;
        this.PaginationTag = !this.Printing && !this.Editing && (",theory,examples,intro,".indexOf("," + this.Page.AuxField1 + ",") > -1 ? _Player.Player.Settings.Player.PaginationTag : "");
        this.NextPageID = (this.GetNextPage(this.PageID) ? this.GetNextPage(this.PageID).ObjectID : -1);
        this.AllowedPages = [this.PageID];
        if (this.FindFirstPage(this.Lesson)) this.AddAllowedPage(this.FindFirstPage(this.Lesson).ObjectID);
    }
    this.ShouldAddToPriNav = function (Page) {
        return Page && Page.AuxField1 != "global" && Page.AuxField1 != "disclaimer" && Page.Content;
    }
    this._GetPage = function (PageID, WithinCurrentLessonOnly, Step) {
        var o = fnFindPage(this.Course, null, PageID);
        var NextPage;
        if (o && o.PageIndex > -1 && o.Lesson) {
            var Lesson = o.Lesson;
            var Index = o.PageIndex + Step;
            while (!NextPage && Lesson.Folders) {
                for (var x = Index; (Step>0 ? x < Lesson.Folders.length : x>=0); x += Step) {
                    if (!NextPage && this.ShouldAddToPriNav(Lesson.Folders[x]))
                        NextPage = Lesson.Folders[x];
                }
                if (NextPage != null || WithinCurrentLessonOnly) return NextPage;
                var NextLesson;
                if (Lesson.Parent) {
                    NextLesson = Lesson.Parent.Folders[$(Lesson.Parent.Folders).index(Lesson) + Step];
                    if (!NextLesson && Lesson.Parent.Parent) NextLesson = Lesson.Parent.Parent.Folders[$(Lesson.Parent.Parent.Folders).index(Lesson) + Step];
                }
                if (!NextLesson) return;
                Lesson = NextLesson;
                Index = (Step>0?0:Lesson.Folders.length-1);
            }
        }
    }
    this.GetPrevPage = function(PageID, WithinCurrentLessonOnly){
        return this._GetPage(PageID, WithinCurrentLessonOnly,-1);
    }
    this.GetNextPage = function (PageID, WithinCurrentLessonOnly) {
        return this._GetPage(PageID, WithinCurrentLessonOnly, 1);
    }
    this.InitializeSCORM = function () {        
        if (_Player.SCORM) {
            var iCurrentRCO = (_Player.SCORM.Win ? cInt(_Player.SCORM.Win.iCurrentRCO) : 0);
            _Player.SCORM.Lesson = this.GetSCORMLesson(this.LessonID, this.Lesson.Folders[0].ObjectID);
            if (_Player.SCORM.Win && _Player.SCORM.Win.iCurrentRCO == _Player.SCORM.Lesson.RCOID) return;
        }
        //Create SCORM
        _Player.SCORM = new clsScorm(this);
        var iCurrentRCO = (_Player.SCORM.Win ? cInt(_Player.SCORM.Win.iCurrentRCO) : 0);
        _Player.SCORM.Lesson = this.GetSCORMLesson(this.LessonID, this.Lesson.Folders[0].ObjectID);        
        if (!this.Printing) {
            if (!_Player.SCORM.API) {
                if (!_Player.Player.Preview) alert("SCORM object not found. Saving your progress will not be possible. #1");
            } else {

                if (!_Player.SCORM.Lesson) {
                    alert("SCORM object not found. Saving your progress will not be possible. #2");
                } else {
                    //Initialize a new attempt
                    if (_Player.SCORM.Win.iCurrentRCO != _Player.SCORM.Lesson.RCOID) {
                        if (_Player.SCORM.Win.fnMetierPlayRCO) _Player.SCORM.Win.fnMetierPlayRCO(_Player.SCORM.Win.iClassroomID, _Player.SCORM.Lesson.RCOID, null, 0, _Player.SCORM.Lesson.ScormMode, true);
                        _Player.SCORM.reset();
                    }
                    _Player.SCORM.init();
                }
            }
        }
    }
    this.InitializeLessonQuizes = function () {
        if (!this.LessonQuizes) {
            this.LessonQuizes = [];
            var Quizes = fnGetContentsByType(this.Lesson, "quiz", true);
            $.each(Quizes, function () {
                if (this.Mode != "hide") {
                    var s=_Player.Player.BuildSettings(this.Parent.Parent, this.Parent).Quiz;
                    var W = s.Weight, M = s.Multiplier, NumQuestions = cStr(this.Xml).split("</question>").length - 1;
                    _Player.Player.LessonQuizes.push({ Object: this, TotalScore: cInt(cStr(cStr(_Player.SCORM.getSuspendData("quiz-" + this.ObjectID)).split("score:")[1]).split(";")[0]) * NumQuestions, Weight: NumQuestions, QuizWeight: (W == undefined ? 1 : W), Multiplier: (M == undefined ? 1 : M) });
                }
            })            
        }
    };
    this.RefreshCourseStatus = function () {
        this.Status = { Completed: 0, Score: 0, MaxScore: 0, NumLessons: 0 };
        var countLesson = function (Lesson) {
            if (Lesson.AuxField1 != "finaltest") {
                if (Lesson.ScormLesson) {
                    _Player.Player.Status.MaxScore += (Lesson.ScormLesson.Weight * 100);
                    if (Lesson.ScormLesson.Status == "C" && Lesson.ScormLesson.Type == "lesson") _Player.Player.Status.Completed++;
                    _Player.Player.Status.Score += cInt(Lesson.ScormLesson.Weight * Lesson.ScormLesson.Score);
                }
                _Player.Player.Status.NumLessons++;
            }
        }
        $(this.Course.Folders).each(function () {
            if (this.Folders) {
                countLesson(this);                
                if (this.Folders) $(this.Folders).each(function () { if (!this.Content) countLesson(this); });
            }
        });
        $(".progress-points").html(this.Status.Score + " / " + this.Status.MaxScore);
        $(".progressnumber-total").html(this.Status.NumLessons);
        $(".progressnumber-taken").html(this.Status.Completed);
        $(".progresscolor-taken").css("width", this.Status.Completed * 100 / Math.max(1, this.Status.NumLessons) + "%");
        $("#complete-details").html(replaceAll(_Translations["completion-status"], "{0}", cStr(this.Status.Completed), "{1}", cStr(this.Status.NumLessons), "{2}", cStr(this.Status.Score), "{3}", cStr(this.Status.MaxScore)));
    }
    this.Load = function (lessonID, pageID) {
        this.MaskContent();        
        this.RenderLessonNav = function (o) {
            if (o.AuxField1 != "finaltest") {
                var lessonPage = fnFindPage(_Player.Player.Course, o.ObjectID, null);
                if (o.ObjectID == _Player.Player.Lesson.ObjectID) {
                    $("#primary-nav-container .pri-nav-item").html(o.Name);
                    _Player._LessonOrderIndex = _Player._LessonOrder.length;
                }
                var lessonHasContentPages = o.Folders && $.grep(o.Folders, function (folder) { return folder.Content; }).length > 0;                
                _Player._LessonOrder.push(o);                
                o.ScormLesson = (lessonHasContentPages ? this.GetSCORMLesson(lessonPage.Lesson.ObjectID, lessonPage.Page.ObjectID) : null);                
                if (this.ScormLesson && this.ScormLesson.Status && this.ScormLesson.Status != "N") _Player.Player.AllowedPages.push(lessonPage.Page.ObjectID)

                var nav = $('<tr class="highlight"><td width="280" class="first left-rounded green"></td><td width="142" class="last right-rounded green"><div class="progressbar tiny"><div class="taken" style="width:' + (this.ScormLesson ? (this.ScormLesson.Status == "C" ? 100 : this.ScormLesson.Score) : 0) + '%;"></div></div></td></tr>').appendTo($("#nav-primary tbody"));
                fnAddNav(nav, lessonPage.Page).find('td:eq(0)').html(o.Name);
                $("#nav-primary tbody").append('<tr><td colspan="3" class="divider"></td></tr>');
                
            }
        };
        this.AddAllowedPage = function (Id) {
            Id = cInt(Id);
            if ($(this.AllowedPages).index(Id) == -1) this.AllowedPages.push(Id);
        }
        this.UnlockNextPageIfAvailable = function () {
            if (this.Settings.Player.SequentialPlayback && $(this.PagesWhereScrolledToBottom).index(this.PageID) > -1 && this.NextPageID > 0 && $(this.AllowedPages).index(this.NextPageID) == -1) {
                var IncompleteQuiz;
                $.each(this.LessonQuizes, function (i, o) {
                    if (!IncompleteQuiz && o.Object.Parent.ObjectID == _Player.Player.PageID) {
                        if (cInt(_Player.Player.Settings.Quiz.MinimumRequiredScore) > 0 && (o.TotalScore / _Player.Player.Settings.Quiz.MinimumRequiredScore) < o.Weight) IncompleteQuiz = o;
                        else {
                            if (this.Questions)
                                $.each(this.Questions, function () {
                                    if (!IncompleteQuiz && !this.IsCompleted()) IncompleteQuiz = o;
                                });                                
                        }                            
                    }
                });
                
                if (!IncompleteQuiz) {
                    if(this.PageNavMap[this.NextPageID]) $.each(this.PageNavMap[this.NextPageID], function () { $(this).removeClass("disabled"); });
                    this.AddAllowedPage(this.NextPageID);
                    _Player.SCORM.setSuspendData("allowed-pages", this.AllowedPages.join());                    
                }
            }
        };
        this.InitializeWindow = function () {
            if (this.Initialized) return;
            $(window).bind("hashchange", function () {
                if (window.location.hash.indexOf("PageID=" + _Player.Player.PageID) == -1 && getHashstring("PageID")) _Player.Player.Load(null, getHashstring("PageID"));
            });
            if (this.Settings.Player.SequentialPlayback) {
                $(window).scroll(function () {
                    if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                        var LastPageBlock = $(".page-block:last");
                        if (LastPageBlock.length == 0 || LastPageBlock.is(".show")) {
                            if ($(_Player.Player.PagesWhereScrolledToBottom).index(_Player.Player.PageID) == -1) _Player.Player.PagesWhereScrolledToBottom.push(_Player.Player.PageID);
                            _Player.Player.UnlockNextPageIfAvailable();
                        }
                    }
                });
            }
            if (this.Printing) $(document.body).addClass("print");
            $.each(["#download-checklist", "#cIncludeNotesContainer", "#cIncludeHighlightsContainer", "#cIncludeAddlCheckpointsContainer"], function (i, o) {                
                $(o).toggle($(o).text() != "");
            });
            $("#toggler").click(function () { fnShowHideMenu(!$('#myContent').is(':visible')); $.cookie('menu-visible', b ? 0 : 1, { expires: 10 }); });
            $("#pop-kompendium input").each(function () { if ($(this).attr("type") == "radio") { var s = $.cookie("print-" + $(this).attr("name")); if (s == $(this).attr("value")) $(this).prop("checked", true); } else { var s = $.cookie("print-" + $(this).attr("class")); if (cStr(s) != "") $(this).prop("checked", s == "1"); } }).change(function () { if (!$(this).is(":checked")) return; if ($(this).attr("type") == "radio") { $.cookie("print-" + $(this).attr("name"), $(this).attr("value")); } else { $.cookie("print-" + $(this).attr("class"), $(this).is(":checked") ? 1 : 0); } });
            $("#pop-kompendium input[type='radio']").change(function () { var o = $(this); if (!o.is(":checked")) return; $("#download-checklist, #download-lecture").each(function () { $(this).html("" + _Translations[o.attr("value") + "-" + $(this).attr("id").split("-")[1]]); }); }).change();
            $("#download-checklist, #download-lecture").click(function () {
                var Email = (_Player.SCORM.API ? _Player.SCORM.Win.LessonData.UserEmail : "");
                if (Email == "" && $("#print-email:checked").length > 0) {
                    alert("Can't send requested email, since SCORM tracking is inactive or learner's profile does not contain a valid email address.");
                    return;
                }
                $("#pop-kompendium .options, #pop-kompendium .actions").hide();
                $("#pop-kompendium .status").html("<div class='loader' />" + _Translations["download-please-wait"]);
                var s = "";
                $("#pop-kompendium input").each(function () { if ($(this).is(":checked")) s += ($(this).attr("type") == "radio" ? $(this).attr("value") : $(this).attr("class")) + ","; });
                _Player.Player.SavePlayerValues();
                $.ajax({
                    type: "POST",
                    data: { __COMMAND: $(this).attr("id"), __OPTIONS: s, __SCORM: _Player.SCORM.SuspendDataString, __EMAIL: Email, __PAGEID: _Player.Player.PageID, __PDF: true },
                    error: function (oRequest, sStatus, oError) { $("#pop-kompendium .status").html("An error occurred preparing your download. Please try again"); },
                    success: function (oRequest, sStatus, oError) { $("#pop-kompendium .status").html("<br /><div style='text-align:center;margin-left:15px'><div class='actionbutton'>" + oRequest + "</div></div>"); },
                    complete: function () { $("#pop-kompendium .options, #pop-kompendium .actions").show(); }
                });
            });

            if (this.Settings.Player.RestoreLastPosition) {
                var p = cInt(_Player.SCORM.getSuspendData("lastpage"));
                if (p > 0 > 0 && this.PageID != p) {
                    if ($.grep(this.Lesson.Folders, function (o) { return o.ObjectID == p; })[0]) {
                        this.SetLessonAndPageIDs(this.LessonID, p);
                    }
                }
                $.each(_Player.SCORM.getSuspendData("scrollposition-*"), function (Idx, Itm) {
                    var Page = $.grep(_Player.Player.Lesson.Folders, function (o) { return o.ObjectID == cStr(Itm[0]).split("-")[1]; })[0];
                    if (Page) {
                        Page.ScrollTop = cInt(cStr(Itm[1]).split("|")[0]);
                        Page.ScrollBlock = cStr(Itm[1]).split("|")[1];
                    }
                });
            }
            this.Initialized = true;
        };
        this.InitializeGlobalPages = function () {
            var globalPages = $.grep(this.Course.Folders, function (folder) { return folder.AuxField1 == "global"; })
            var globalPagesLesson = $.grep(this.Lesson.Folders, function (folder) { return folder.AuxField1 == "global"; });
            $.each(globalPagesLesson, function (i, n) {
                var match = $.grep(globalPages, function (m) { return m.Name == n.Name })[0];
                if (match) globalPages[$(globalPages).index(match)] = n;
                else globalPages.push(n);
            });
            $(".topmenu-item.temp").remove();
            $.each(globalPages, function (index, item) {
                var nav = $('<li class="topmenu-item temp"><a href="javascript:void(0)">' + item.Name + '</a></li>').click(function () { fnDisplayPageAsDialog(item); });
                $("#globalmenu").append(nav);
            });            
        };
        this.InitializeContent = function () {
            //todo> move content initialize code
        };

        this.Cleanup();
        this.SetLessonAndPageIDs(lessonID, pageID);
        
        if ($(document).data("__data") != null) {
            _Player = $.extend(_Player, $(document).data("__data"));
        }
        _Player = $.extend(_Player, { Loaded: true });

        this.InitializeSCORM();
        this.InitializeLessonQuizes();
        this.InitializeWindow();
        this.InitializeGlobalPages();
        this.InitializeContent();

        
        window.location.hash = "#PageID=" + this.PageID;
        $.each(cStr(_Player.SCORM.getSuspendData("allowed-pages")).split(","), function () {
            _Player.Player.AddAllowedPage(this);                
        });

        this.LessonIndex = (this.Lesson && this.Lesson.Parent ? $.inArray(this.Lesson, this.Lesson.Parent.Folders) : 0);
        $("body").addClass("page-" + this.Page.AuxField1);
        //load content        
        for (var iPage = 0; iPage < this.Page.Content.length; iPage++) {
            var c = this.Page.Content[iPage];            
            if (c.AuxField1 && c.AuxField1 !="file" && (c.Mode != "hide" || this.Editing)) {
                var t = (this.Printing && c.AuxField1 == "pitfalls" ? "theory" : c.AuxField1);
                var s = $("#" + t + "-template").html().split("{");
                for (var y = 1; y < s.length; y++)
                    s[y] = cStr(c[s[y].safeSplit("}", 0)]) + s[y].safeSplit("}", 1);

                s = fnTranslateControl($(s.join("")).attr("content_id", c.ObjectID).addClass(c.AuxField1).addClass("_mode-" + c.Mode));                
                if (c.AuxField1 == "reflection") {
                    var content = fnGetContentsByType(this.Lesson, "theory", true);
                    s.find("li").each(function (i,o) {
                        var id = $(this).attr("target");
                        $.each(content, function () {
                            if (cStr(this.Xml).indexOf(id) > -1 && this.Mode == "hide") s.find("li[target='" + id + "']").detach();
                        });
                    });
                }
                s.find("h1:not(.course-title):eq(0)").html(c.Name).toggle(c.Mode != "nohead" && c.Mode != "slidedown");

                $("<div class='title-minimized'><h1>" + c.Name + " [" + c.AuxField1 + "]<br />Mode: " + cStr(c.Mode, "Normal") + "<br />Id: " + c.ObjectID + "</h1></div>").appendTo(s);
                $(".content-container").append(s.data("index", iPage));
                if (!this.Editing) {
                    if (c.Mode == "popup") 
                        $("<div class='wrapper.empty'><a href='javascript:void(0)'>Click to read about " + c.Name + "</a></div>").appendTo($(".content-container")).find("a").data("content", s).click(function () { fnDisplayDialog($(this).data("content").show(), { width: 830, height: Math.max(510, $(window).height() - 250), title: c.Name }); });
                    else if (c.Mode == "slidedown")
                        $("<div class='wrapper.empty metierbox slidedown'><div class='metierbox-content' style='background-color:#fff'><a href='javascript:void(0)' class='toggler'>" + c.Name + "</a><div class='slide-content' /></div></div>").appendTo($(".content-container")).find("a").click(function () { $(this).parents(".slidedown").toggleClass('active'); }).end().find('.slide-content').html(s);
                    
                }
                if (s.find(".menu").length > 0) {
                    s.data("menu", s.find(".menu"));
                    $("#wrapper-submenu").append(s.find(".menu").removeClass("menu"));
                }
                if (this.Lesson.AuxField1 == "finaltest") break;
            }
        }
        
        $(".course-title").html(this.Course.Name);
        $(".lesson-title").html(this.Lesson.Name);
        document.title = document.title + " - " + this.Course.Name + " >> " + this.Lesson.Name;
        window.parent.document.title = document.title;

        this.PageBlocks = [];
        if (this.PaginationTag) {            
            //New code to split on H2s
            var SplitAttr1 = "data-" + Math.floor(Math.random() * 1000000000).toString();
            $("._content.theory, ._content.examples, ._content.intro").find(".print-details").remove().end().find(".wrapper").each(function (Idx, Container) {
                var Blocks = [];                
                var IncludeH1=Idx == 0;
                $(Container).find((IncludeH1 ? "h1:first, " : "") + _Player.Player.PaginationTag).not(".nosection").each(function (i, o) {                    
                    if (replaceAll($(this).text()," ","","&nbsp;","") != "") {                        
                        if (IncludeH1 && i == 1 && $(this).prev().is("h1")) _Player.Player.SuppressFirstH2 = true;
                        else $(this).attr(SplitAttr1, "A");
                    }
                });
                var Arr = ("<div>" + $(Container).html() + "</div>").split(SplitAttr1);                                
                for (var x = 1; x < Arr.length; x++) {
                    if (Idx > 0 && x == 1 && _Player.Player.PageBlocks.length > 0) {
                        var t=Arr[x - 1].substring(0,Arr[x - 1].lastIndexOf("<"));                        
                        if($("<div>" + t + "</div>").text()!="")
                            Blocks.push("<div class='page-block show' data-page-block='" + _Player.Player.PageBlocks[_Player.Player.PageBlocks.length - 1].Id + "'>" + $("<div>" + t + "</div>").html() + "</div>");                                                    
                    }
                    var S = Arr[x - 1].substring(Arr[x - 1].lastIndexOf("<")) + Arr[x].substring(4, Arr[x].lastIndexOf("<") );                    
                    var Id = Idx + "-" + x;                    
                    Blocks.push("<div class='page-block show' data-page-block='" + Id + "'>" + $("<div>" + S + "</div>").html() + "</div>");
                    _Player.Player.PageBlocks.push({ Id: Id, Name: Arr[x].substring(Arr[x].indexOf(">")+1, Arr[x].indexOf("<")) });                    
                };
                if (Arr.length == 1 && _Player.Player.PageBlocks.length>0 && $("<div>" + $(Container).html() + "</div>").text() != "")
                    Blocks.push("<div class='page-block show' data-page-block='" + _Player.Player.PageBlocks[_Player.Player.PageBlocks.length - 1].Id + "'>" + $(Container).html() + "</div>");
                $(Container).html(Blocks.join(""));
            });
            //Wrap non-theory content in the preceding/next page-block
            $("._content").not(".theory, .examples, .intro").each(function (Idx, Container) {                
                var BlockToAddTo = $(Container).prevAll().has(".page-block").eq(0).find(".page-block:last");
                if (BlockToAddTo.length == 0) BlockToAddTo = $(Container).nextAll().has(".page-block").eq(0).find(".page-block:first");
                if (BlockToAddTo.length > 0) $(Container).wrapInner("<div class='page-block show' data-page-block='" + BlockToAddTo.attr("data-page-block") + "' />");
            });
            //End new code
            if (this.PageBlocks.length>1) $("._content:last").after($("<div id='page-block-nav' class='wrapper empty'><a class='left'></a><a class='right'></a><br /></div>"));
        }

        var b = $.cookie("menu-visible") == "1" && $("._content.splash").length == 0 && $("._editing").length == 0;
        $('#myContent').toggle(b);
        $("#global-menu-filler").height(b ? 131 : 20);

                
        function fnAddNav(element, page) {
            if (!page || (page.Parent && !page.Parent.ScormLesson && _Player.SCORM.Win && _Player.SCORM.Win.LessonData.LessonNotAvailableMessage)) {
                $(element).each(function () {
                    $(this).off("click").on("click", function () { alert(cStr((_Player.SCORM.Win ? _Player.SCORM.Win.LessonData.LessonNotAvailableMessage : ""), _Player.Player.Settings.Player.LessonNotAvailableMessage, "Lesson is not available")); });
                });
            } else {
                if (!page.Content) page.Content = [];
                var Map = _Player.Player.PageNavMap[page.ObjectID];
                if (!Map) {
                    Map = [];
                    _Player.Player.PageNavMap[page.ObjectID] = Map;
                }                
                $(element).each(function () {
                    Map.push(this);
                    if (_Player.Player.Settings.Player.SequentialPlayback)$(this).toggleClass("disabled", $(_Player.Player.AllowedPages).index(page.ObjectID) == -1);
                    $(this).off("click");
                    if (!_Player.Player.Preview && page.Content.length > 0) $(this).on("click", function () { if($(this).is(".disabled")) return; _Player.Player.Load(null, page.ObjectID); });                    
                    if ($(this).is("a")) {				    
                        $(this).attr("href", (!_Player.Player.Preview && page.Content.length > 0 ? "javascript:void(0)" : fnMetierUrl("player.aspx?PageID=" + page.ObjectID)));                                        
                    } else if (_Player.Player.Preview || page.Content.length == 0) {                    
                        $(this).on("click", function () { if ($(this).is(".disabled")) return; window.location = fnMetierUrl("player.aspx?PageID=" + page.ObjectID); });
                    }
                });
            }
            return element;
        }
        
        //Variable used to keep order of lessons (ignoring nesting)
        _Player._LessonOrder = [];
        _Player._LessonOrderIndex = -1;

        $(this.Course.Folders).each(function () {
            if (this.Folders) {
                _Player.Player.RenderLessonNav(this);
                if (this.Folders) $(this.Folders).each(function () { if (!this.Content) _Player.Player.RenderLessonNav(this); });
            }
        });
        this.RefreshCourseStatus();
        //render page nav
        if (_Player.Player.Lesson.Folders) {
            $("#quicknav-left, #quicknav-right").hide();
            var Counter = {};
            $(_Player.Player.Lesson.Folders).each(function (i, o) {
                if (_Player.Player.ShouldAddToPriNav(o)) {
                    var bActive = (_Player.Player.PageID == o.ObjectID);
                    var li = $("<li class='popmenu-listing'><span class='left-rounded green highlight right-rounded'>" + o.Title + "</span></li>").appendTo($(".popmenu-body ul"));
                    fnAddNav(li, o);

                    if (bActive) {
                        if (!_Player.Player.Printing) {                            
                            var prev = _Player.Player.GetPrevPage(o.ObjectID, true);
                            var next=_Player.Player.GetNextPage(o.ObjectID, true);
                            if (prev != null) fnAddNav($("#quicknav-left").show(), prev);
                            if (next != null) fnAddNav($("#quicknav-right").show(), next);
                            $("#secondary-nav-container .pri-nav-item").html(o.Title);
                        }                                                
                    }
                    if (Counter[o.AuxField1]==undefined) Counter[o.AuxField1] = 0;
                    else Counter[o.AuxField1]+=1;
                    var td = $("<td class='count-"+Counter[o.AuxField1]+ " index-"+i+ " nav-" + o.AuxField1 + " highlight" + (bActive ? " active" : "") + "'><div class='img'></div><div class='title'>" + o.Title + "</div></td>").appendTo($(".mainmenu-maximized tr"));
                    fnAddNav(td, o);					
                }                
            });            
        }
                
        if (_Player.Player.Printing) _Player.SCORM.setSuspendDataString(_Player.PrintSCORM);

        if (_Player.SCORM.Win) {
            var F, D;
            $(_Player.SCORM.Win.LessonData.Lessons).each(function (i) {
                if (this.Type == "finaltest") F = this;
                else if (this.Type == "diploma") D = this;
            });

            if (F) {
                $("#lFinalTestUnlock").html(replaceAll(_Translations[F.Status == "C" ? "finaltest-completed" : "finaltest-details"], "{0}", _Player.SCORM.Win.LessonData.UnlockTest));
                if (F.Status != "C" 
                    && this.Status.Score >= _Player.SCORM.Win.LessonData.UnlockTest
                    && (_Player.Player.Status.Completed==_Player.Player.Status.NumLessons++ || !$.extend({FinalTest:{Unlock:{AllLessonsCompleted:false}}},_Player.Player.Settings).FinalTest.Unlock.AllLessonsCompleted)                    
                    ) $(".button-endtest").addClass("active").attr("href", F.Url);

                $(".button-endtest").show();
            }
            if (D) {
                $(".button-diploma").show();
                if ((F && F.Status == "C") || (!F && this.Status.Score >= _Player.SCORM.Win.LessonData.UnlockTest)) {
                    $(".button-diploma").addClass("active").attr("href", D.Url);
                    $("#lDiplomaDetails").html("" + _Translations["diploma-download"]);
                } else {
                    $("#lDiplomaDetails").html("" + _Translations["diploma-details"]);
                }
            }
        }
        
        $(".mainmenu-maximized").on("mouseenter", ".highlight:not(.disabled)", function () { $(this).addClass("manual-hover"); if (!$(this).hasClass("active")) $(this).find(".img").animate({ height: 58, width: 63, top: 4 }, { duration: 300, queue: false }).end().find(".title").animate({ top: 8 }, { duration: 300 }); }).on("mouseleave", ".manual-hover", function () { $(this).removeClass("manual-hover"); if (!$(this).hasClass("active")) $(this).find(".img").animate({ height: 48, width: 51, top: 10 }, { duration: 300 }).end().find(".title").animate({ top: 10 }, { duration: 300 }); });
        $("#primary-nav-container, #secondary-nav-container").mouseover(function () { $(this).addClass("manual-hover"); }).mouseleave(function () { $(this).removeClass("manual-hover"); });

        $("._content.intro").each(function () {
            $(this).find("ol").each(function (i, o) { $(o).css("list-style-type", "none").children("li").each(function (j, k) { $("<div class='bullet'>" + (j + 1) + "</div>").prependTo($(k)); }) });
            $(this).find("p#quote, p.quote").each(function (i, o) {
                $(o).before($(
                "<div class='intro-quote'>"
                + "<div class='image'></div>"
                + "<div class='top'></div>"
                + "<div class='container'><div style='width:370px;'><div class='top'></div><div class='fill'><div class='text'></div></div><div class='bottom'></div></div><div class='author'/></div></div>").find(".text").html($(o).clone().find("span").remove().end().html()).end().find(".author").html(stripLeadingStr($(o).find("span").html(), "-", " ")).end()).remove();
            });
        });

        if ($("._content.theory, ._content.examples, ._content.intro").length > 0) {
            _Player.Highlighter = new clsHighlighter();
            if ($(document).data("__dictionary") && $("._content.theory, ._content.examples, ._content.intro, ._content.case").length > 0) new clsDictionary(_Player.Highlighter.Words, $(document).data("__dictionary"));
        }

        $("._content.splash").each(function (index, section) {
            section = $(section);
            $(function () {
                var a = [];
                $.each(_Player.Player.Page.Content, function () { if (this.AuxField1 == "file") a.push(escape(this.Filename)); });
                new clsVideoPlayer(a);
            });
            
            fnAddNav(section.find(".start"), _Player.Player.GetNextPage(_Player.Player.PageID));
            var content=$.grep(_Player.Player.Page.Content, function (o) { return cStr(o.AuxField1) == "splash"; })[0];
            var showAsImage=cStr(content.Xml).length<10;
            section.find(".image").toggle(showAsImage);
            if(showAsImage){
                section.find(".image").attr("src", content.Filename);
            }else{
                section.find(".content").html(content.Xml);
            }                        
        });
        $("._content").each(function (index, section) {            
            if (replaceAll($(section).css("background-image"),"none","") && (index == 0 || _Player.Player.Editing)) {                
                if (fnGetIEVersion() > 0 && fnGetIEVersion() < 9 && !_Player.Player.Printing) {
                    var img=$("<img style='position:fixed;top:0;left:0;z-index:-1;' src='" + $(section).css("background-image").substring(5, $(section).css("background-image").length - 2) + "' />");                    
                    img.load(function () {                        
                        var $bg = $(this),
                        fAspectRatio = $bg.width() / $bg.height();

                        function fnResizeBackground() {
                            var dim = { w: Math.max($(window).width(), $("body").width()), h: Math.max($(window).height(), $("body").height()) };
                            if ((dim.w / dim.h) < fAspectRatio) {
                                $bg.css("width", "auto").css("height", "100%").css("left", (-(fAspectRatio - (dim.w / dim.h)) * dim.h / 2) + "px");
                            } else {
                                $bg.css("height", "auto").css("width", "100%").css("left", 0 + "px");
                            }
                        }
                        $(window).resize(function () { fnResizeBackground(); }).trigger("resize");
                    });
                    img.prependTo($(section).css("background-image", "none").css("background", "transparent"));                                
                } else if ($(section).parents("._editing").length == 0 && !_Player.Player.Printing) {
                    $(window).resize(function () {                        
                        $(section).css("min-height", "").css("min-height", Math.max(0, $(window).height() - $("body").height() + $(section).height()));
                    }).resize();    
                }
            }
        });
        $("._content.goals").each(function (index, section) {
            section = $(section);
            if (index > 0 && !_Player.Player.Editing) {
                section.find(".goals-content li").each(function (i, o) { if(trim($(o).html())) $("<div class='goal'><div class='arrow'></div><div class='text'>" + $(o).html() + "</div><br style='clear:both' /></div>").appendTo($(".goals-list:eq(0)")); });
                section.detach();
            }else{                
                
                section.find(".goals-content li").each(function (i, o) { if (trim($(o).html())) $("<div class='goal'><div class='arrow'></div><div class='text'>" + $(o).html() + "</div><br style='clear:both' /></div>").appendTo(section.find(".goals-list")); });
                $("<div />").appendTo(section.find(".goals-list"));
                section.find(".goals-content ul").detach();
            }
        });        
        $("._content.theory").each(function () {
            $(this).find(".cms-question").each(function () {
                var s = cStr(_Player.SCORM.getSuspendData("reflection-" + $(this).attr("id")), _Player.Player.Preview ? " " : "");
                if (s != "") {
                    var t = (trim(replaceAll($(this).find(".title").text(), String.fromCharCode(160), " ")) == "" ? "" : trim(replaceAll($(this).find(".title").html(), "&nbsp;", " ")));
                    if ($(this).hasClass("wide")) {
                        var o = $('<div class="metierbox" id="vtprcgv"><div class="metierbox-title">' + cStr(t, replaceAll(_Translations["question-box-title"], "{0}", stripEndingStr(trim($(this).find(".text").text()), ":"))) + '</div><div class="metierbox-content" style="border-width: 1px; background-color: rgb(233, 254, 186);">' + s + '</div></div>');
                    } else {
                        var o = $('<div class="chat-dialog user-answer" style="float:right;padding:10px"><div class="legend">' + cStr(t, replaceAll(_Translations["question-box-title"], "{0}", stripEndingStr(trim($(this).find(".text").text()), ":"))) + '</div><div class="right"><div class="container"><div class="top"></div><div class="fill"><div class="text"><div class="textarea">' + s + '</div></div></div></div><div class="bottom"></div></div></div>');
                    }
                    o.insertAfter($(this));
                }
                $(this).remove();
            });
            //Hack to force a break in FireFox since it is not correctly floating when user-answer is next to a wide table
            if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
                $(this).find(".chat-dialog").each(function () {
                    var o = $(this).next();
                    while (o.length > 0 && replaceAll(o.html(), " ", "") == "") o = o.next();
                    while (o.length > 0) {
                        if (o[0].nodeName.toLowerCase() == "table") {
                            $(this).after($("<br style='clear:both' />"));
                        }
                        o = o.children();
                    }
                });
            }
        });
        $("._content.pitfalls").each(function (index, section) {
            section = $(section);
            function fnCalculateTopPadding(i) {
                if (i == 0) return -50; //18
                if (i % 5 == 0) return 70; //70
                if (i % 5 == 1) return 50; //50
                if (i % 5 == 2) return 150; //150
                if (i % 5 == 3) return 50; //50
                if (i % 5 == 4) return 90; //90
            }
            function fnCalculateShape(i) {
                if (i % 5 == 0 || i % 5 == 2 || i % 5 == 3) return "wide";
                return "narrow";
            }
            function fnCalculateHAlignment(i) {
                if (i % 5 == 0 || i % 5 == 2) return "lean-left";
                if (i % 5 == 1) return "lean-even-more-right";
                if (i % 5 == 3) return "lean-right";
                return "";
            }
            function AddPitfalls(section, addToSection) {
                fnRemoveEmptyTags(section.find(".pitfalls-content > ul > li"));
                section.find(".pitfalls-content > ul > li").each(function (i, o) {
                    i = addToSection.find(".pitfall-item").length;
                    if (fnShouldAddToNav(section))
                        $("<li><a class='roundbutton' href='#anchor" + i + "'>" + replaceAll(_Translations["pitfall-title"], "{0}", (i + 1)) + "</a></li>").appendTo($("#wrapper-submenu ul.subnav"));

                    $("<div style='margin-top:" + fnCalculateTopPadding(i) + "px' class='pitfall-item'><div class='anchoradjustment'><a name='anchor" + i + "' id='anchor" + i + "'>&nbsp;</a></div>" +
                "<div class='pitfall-" + fnCalculateShape(i) + "-top " + fnCalculateHAlignment(i) + "'><div class='pitfall-" + fnCalculateShape(i) + "-number'>" + (i + 1) + "</div><div class='pitfall-" + fnCalculateShape(i) + "-text'><p" + (i > 8 ? " style='margin-left:35px'" : "") + ">" + $(o).html() + "</p></div><div class='clear'></div></div>" +
                "<div class='pitfall-" + fnCalculateShape(i) + "-bottom " + fnCalculateHAlignment(i) + "'></div></div>"
            ).appendTo(addToSection.find(".wrapper.main"));
                });
            }
            if (index > 0 && !_Player.Player.Editing) {
                AddPitfalls(section, $("._content.pitfalls:eq(0)"));
                section.detach();
            } else {                
                if (!_Player.Player.Printing) {
                    AddPitfalls(section, section);                    
                    section.find(".pitfalls-content ul").remove();                    
                    if (trim(replaceAll(section.find(".pitfalls-content").text(), "\r", "", "\n", "", "\u00a0", ""))) {
                        section.find(".wrapper.intro-fallgruver:eq(0)").append("<div class='intro'>" + section.find(".pitfalls-content").html() + "</div>");
                        section.find(".pitfalls-content").html("");
                    }
                }
            }
        });
        $("._content.checklist").each(function (index, section) {
            section = $(section);
            var menu, addToSection;
            function fnResizeItems() {
                menu.find(".subnav li").detach();
                var a = $("._content.checklist .item");
                a.each(function (i, o) {
                    var iStep = 30;
                    $(this).css("min-height", 0).css("min-height", (cInt($(this).height() / iStep) + ($(this).height() % iStep == 0 ? 0 : 1)) * iStep).toggleClass("last", i == a.length - 1);
                    if (fnShouldAddToNav(section))
                        $('<li><a class="checkpointbutton" href="#check-' + $(this).data("item-counter") + '">' + replaceAll(_Translations["checkitem-title"], "{0}", (i + 1)) + '</a></li>').appendTo(menu.find(".subnav"));
                });
                section.find("textarea").autosize();
            }
            function fnBuildCheckItem(sText, bCustom, bNew, bChecked) {
                if (sText == "") sText = _Translations["write-here"];
                $(addToSection).data("item-counter", cInt(addToSection.data("item-counter")) + 1);
                return $('<div class="item"><a name="check-' + addToSection.data("item-counter") + '"><div class="lock" /><div class="delete" /></a><div class="separator"></div><div class="inner-item"><div class="checkbox ' + (bChecked ? "checked" : "") + '"></div>' + (bCustom ? '<textarea class="text"></textarea>' : '<div class="text"></div>') + '</div><br style="clear:both" /><div class="separator last"></div></div>').toggleClass("new", bNew).toggleClass("custom", bCustom).find(".text").val(sText).html(sText.split("\n").join("<br />")).end().data("item-counter", addToSection.data("item-counter"));
            }
            function AddChecklist(section, addToSection) {
                fnRemoveEmptyTags(section.find(".checklist-content li"));
                section.find(".checklist-content li:not(li li)").each(function (i, o) {
                    var c=fnBuildCheckItem($(o).html(), false, false, _Player.SCORM.getSuspendData("checklist-" + section.attr("content_id") + "-checked-" + i) == "1").data("key",section.attr("content_id")+"-"+i);
                    if (addToSection.find("div.checklist .content .custom").length > 0) c.insertBefore(addToSection.find("div.checklist .content .custom:eq(0)"));
                    else c.appendTo(addToSection.find("div.checklist .content"));
                });

                if (!_Player.Player.Printing || cStr(_Player.PrintOptions).indexOf("cIncludeAddlCheckpoints") > -1) {
                    $(_Player.SCORM.getSuspendData("checklist-" + section.attr("content_id") + "-text-*")).each(function () { fnBuildCheckItem(this[1], true, false, _Player.SCORM.getSuspendData("checklist-" + section.attr("content_id") + "-checked-" + this[0].substring(this[0].lastIndexOf("-") + 1)) == "1").appendTo(addToSection.find("div.checklist .content")); });
                    //for legacy support, restore old Page-linked checklist items
                    if (index == 0) $(_Player.SCORM.getSuspendData("checklist-" + _Player.Player.PageID + "-text-*")).each(function () { fnBuildCheckItem(this[1], true, false, _Player.SCORM.getSuspendData("checklist-" + _Player.Player.PageID + "-checked-" + this[0].substring(this[0].lastIndexOf("-") + 1)) == "1").appendTo(section.find("div.checklist .content")); });
                }                
                addToSection.find(".checklist .item.new").detach();
            }
            if (index > 0 && !_Player.Player.Editing) {
                addToSection = $("._content.checklist:eq(0)");
                menu = addToSection.data("menu");
                AddChecklist(section, addToSection);
                section.detach();
                $("#wrapper-submenu .content:gt(0)").detach();
            } else {
                menu = section.data("menu");
                addToSection = section;
                AddChecklist(section, section);
                section.find(".checklist-content ul").remove();
                if (trim(replaceAll(section.find(".checklist-content").text(), "\r", "", "\n", "", "\u00a0", ""))) section.find("div.checklist .top").append("<div class='intro'>" + section.find(".checklist-content").html() + "</div>");

            menu.find(".addcheckpoint").click(function () { fnBuildCheckItem('', true, true).hide().appendTo(section.find('div.checklist .content')).fadeIn(1000); $(window).scrollTop(section.find('.checklist .content .item:last').position().top); fnResizeItems(); });

            section.find("div.checklist").on("mouseover", ".item", function () { $(this).addClass("hover"); }).on("mouseleave", ".item", function () { $(this).removeClass("hover"); });
            section.find("div.checklist").on(fnIsTouchDevice() ? "touchend" : "click", ".checkbox", function () { $(this).toggleClass("checked").parents(".item:eq(0)").removeClass("new"); });
            section.find("div.checklist").on(fnIsTouchDevice() ? "touchend" : "click", ".delete", function () { $(this).parents(".item:eq(0)").detach(); fnResizeItems(); });
            section.find("div.checklist").on("change", "textarea", function (e) { if (this.value == '') this.value = _Translations["write-here"]; $(this).parents(".item:eq(0)").toggleClass("new", this.value == _Translations["write-here"]); });
            section.find("div.checklist").on("focus", "textarea", function () { if (this.value == _Translations["write-here"]) { $(this).val(''); if (section.find('.checklist .content .item:last')[0] == $(this).parents(".item")[0]) fnBuildCheckItem('', true, true).hide().appendTo(section.find('div.checklist .content')).fadeIn(1000); fnResizeItems(); } });

            section.find(".checklist-content").remove();

            $(section).keyup(function () { fnResizeItems(); });

            if ($(section).parents("._editing").length == 0 && !_Player.Player.Printing) {
                $(section).css("min-height", $(window).height() + "px");
            }
            }
            if (!_Player.Player.Printing) fnBuildCheckItem("", true, true).appendTo(addToSection.find(".content"));
            fnResizeItems();
        });

        var _quizSections = $("._content.quiz");
        _quizSections.each(function (index, section) {            
            section = $(section);
            var finalTest = _Player.Player.Lesson.AuxField1 == "finaltest";
            var printMode = window.location.href.indexOf("print=true") > -1;            
            if (printMode && _Player.Player.Preview) finalTest = false;
            var c = (finalTest ? _Player.Player.Page.Content : [_Player.Player.Page.Content[section.data("index")]]);
            var o = { Data: [], FinalTest: finalTest, KeyMode: window.location.href.indexOf("answers=true") > -1 && !finalTest, CMSMode: _Player.Player.Preview };
            $.extend(true, o, _Player.Player.Settings.Quiz);
            var restorePrevAnswers = !finalTest || !(_Player.SCORM.API && _Player.SCORM.API.GetValue("metier.cmi.completion_status") == "N");
            $.each(c, function (i, x) {
                if (x.AuxField1 == "quiz") o.Data.push({ Answers: (restorePrevAnswers && x.Answers ? decodeURI(x.Answers) : ""), Score: x.Score, Xml: x.Xml, QuizID: x.ObjectID });
            });

            if ((_Player.Player.Printing || _Player.SCORM.API) && !finalTest) o.Data[0].Answers = cStr(_Player.SCORM.getSuspendData("quiz-" + section.attr("content_id")), _Player.SCORM.getSuspendData("quiz"));
            var _prevQuiz = (index == 0 ? null : $(_quizSections[index - 1]).data("quiz").Quiz);
            o.QuestionOffset = (_prevQuiz ? cInt(_prevQuiz.Settings.QuestionOffset) + _prevQuiz.Questions.length : 0);

            o.Quiz = new clsQuiz(o, section,_Player.Player);
            section.data("quiz", o)
            .bind("changed", function (A, B, C, Score, TotalScore) {
                _Player.Player.UnlockNextPageIfAvailable();                
            })
            .bind("save", function () { _Player.Player.SavePlayerValues(); }) // calling terminate here will prevent a user from recording multiple attempts
            .bind("close", function () { if (window.opener && window.opener.fnRestoreNavigator)window.opener.fnRestoreNavigator(window); else window.parent.fnRestoreNavigator(); });
        });

        $("._content.reflection").each(function (index, section) {
            section = $(section);
            var SavedValues = _Player.SCORM.getSuspendData("reflection-*");
            section.find("li").each(function (i, o) {

                var sQ = "<td class='left'><div class='container'><div class='top'></div><div class='fill'><div class='text'><span>" + $(o).html() + (o.A ? "<br /><div class='expand' /><div class='answer'>" + o.A + "</div>" : "") + "</span></div></div><div class='bottom'></div></div></td><td class='mid'></td>";
                var sA = "<td class='right'><div class='container'><div class='top'></div><div class='fill'><div class='text'><textarea target='" + $(o).attr("target") + "'>" + cStr(SavedValues["reflection-" + $(o).attr("target")], window.location.href.indexOf("preview=true") == -1 ? "" : " ") + "</textarea></div></div><div class='bottom'></div></div></td>";

                $("<tr>" + sQ + "<td></td>" + "</tr>").appendTo(section.find(".chat-dialog"));
                $("<tr><td></td><td></td>" + sA + "</tr>").appendTo(section.find(".chat-dialog"));


                $("<tr><td colspan='3' class='separator'>&nbsp;<br />" + (section.find("li").length - 1 == i ? "" : "<div class='more-questions'></div>") + "</td></tr>").appendTo(section.find(".chat-dialog"));
                if (section.find(".chat-dialog").find("textarea:last").val()) {
                    section.find(".chat-dialog").find("textarea:last").data("status", 1).parents("tr:eq(0)").next("tr").find("td.separator .more-questions").hide();
                }
            });
            section.find("ul").remove();

            section.find("textarea").autosize().change(function (e) { if (this.value == '') this.value = _Translations["write-here"]; $(this).css("color", (this.value == _Translations["write-here"] ? "#569020" : "#243543")); }).change().focus(function () { if (this.value == _Translations["write-here"]) { var h = $(this); setTimeout(function () { h.select(); }, 100); } }).keyup(function (e) {
                var _this = $(this);
                if (this.value == "" || this.value == _Translations["write-here"]) {
                    _this.data("status", 0);
                    $(this).parents("tr:eq(0)").nextAll("tr:eq(0)").find("td.separator .more-questions").toggle(!_Player.Player.Printing).end().end().nextAll("tr:eq(1), tr:eq(2)").find(".container").toggle(_Player.Player.Printing).end().find("textarea").keyup();
                } else {
                    if (_this.data("status") != "1") {
                        _this.data("status", 1);
                        setTimeout(function () {
                            if (_this.data("status") == "1") {
                                var o = $(_this).parents("tr:eq(0)").nextAll("tr:eq(0)").find("td.separator .more-questions").hide().end().end().nextAll("tr:eq(1), tr:eq(2)").find(".container").hide().find("textarea").keyup().end();
                                o.fadeIn(3000);
                            }
                        }, 500);
                    }
                }
            }).keyup();
        });

        $("._content.complete").each(function (index, section) {
            section = $(section);
            function fnEvalBarChange(i, bUIOnly) {
                if (i == $("#lesson-rating").data("value")) return;
                $("#lesson-rating").data("value", i);
                $("#lesson-rating .knob").css("left", 1 + (64 * i));
                $("#lesson-rating .bar").css("width", 13 + 1 + (64 * i));
                if (!bUIOnly) {
                    clearTimeout(iSubmitEvalHandle);
                    iSubmitEvalHandle = setTimeout(fnSubmitEval, 1000);
                    if (_Player.SCORM.API) _Player.SCORM.setSuspendData("lesson-rating", i);
                }
            }

            $("#completion-button").mouseenter(function () { $(this).addClass("manual-hover"); }).mouseleave(function () { $(this).removeClass("manual-hover"); });
            $("#reset-complete-status").click(function () { $("#lesson-in-progress, #lesson-completed").toggle(); if (_Player.SCORM.API) _Player.SCORM.LessonStatus = "I"; _Player.SCORM.Lesson.Status = "I"; _Player.Player.RefreshCourseStatus(); });

            $("#comment-text").val(_Player.SCORM.API ? _Player.SCORM.getSuspendData("complete-" + _Player.Player.PageID + "-comment") : "").autosize().change(function (e) { if (this.value == '') this.value = _Translations["write-here"]; $(this).css("color", (this.value == _Translations["write-here"] ? "#569020" : "#243543")); }).change().focus(function () { if (this.value == _Translations["write-here"]) $(this).val(""); })
            if (_Player.SCORM.API) {
                if (cInt(_Player.SCORM.getSuspendData("lesson-rating")) > 0) fnEvalBarChange(cInt(_Player.SCORM.getSuspendData("lesson-rating")), true);
                if (_Player.SCORM.LessonStatus == "C") {
                    $("#completion-button").click();
                } else $("#completion-button").click(function () {
                    _Player.SCORM.LessonStatus = "C"; _Player.SCORM.Lesson.Status = "C"; 
                    //Check if no quiz and SetFullScoreOnCompleteIfNoQuiz==true
                    if (_Player.Player.Settings.Quiz && _Player.Player.Settings.Quiz.SetFullScoreOnCompleteIfNoQuiz && _Player.Player.LessonQuizes.length == 0) {
                        _Player.SCORM.Lesson.Score = _Player.SCORM.Score = 100;                            
                        _Player.SCORM.writeValues();
                    }
                    _Player.Player.RefreshCourseStatus();
                });
            }

            var iSubmitEvalHandle = 0;
            var fnSubmitEval = function () {
                $.ajax({
                    url: "../tools/sendmail.aspx?action=eval",
                    async: false,
                    type: "POST",
                    data: { comment: $("#comment-text").val(), score: $("#lesson-rating").data("value"), username: _Player.SCORM.StudentID, course_code: _Player.Player.Course.CourseCode, CourseId: _Player.Player.Course.ObjectID, LessonId: _Player.Player.LessonID, lesson_number: (_Player.Player.LessonIndex + 1) }
                });
            };

            $("#lesson-rating").bind("mousemove click mousedown touchstart touchmove", function (e) {
                if ((e.type == "mousedown" && e.which == 1) || e.type == "touchstart") $("#lesson-rating").data("state", "left");
                if ($("#lesson-rating").data("state") == "left" || e.type == "click") {
                    var i = Math.round(((e.pageX | e.originalEvent.pageX) - $("#lesson-rating").offset().left - 13) / 64);
                    if (i >= 0 && i <= 6) fnEvalBarChange(i);
                }
            });
            $("#lesson-rating").bind("mouseleave mouseup touchend", function (e) { $("#lesson-rating").data("state", null); });
            $(".knob").bind('dragstart', function (e) { e.preventDefault(); });
            
            
            var NextPage = _Player.Player.GetNextPage(_Player.Player.PageID);            
            if(NextPage){                
                fnAddNav($(".next-lesson"), NextPage).find(".lesson-name").html(NextPage.Parent.Name);
            } else {                
                $(".next-lesson").hide();
            }
            
            $("#lesson-rating-container .submit").click(function () {
                if ($("#comment-text").val() == "" || $("#comment-text").val() == _Translations["write-here"]) {
                    alert(_Translations["comment-required"]);
                    return;
                }
                $("#lesson-rating-container .loader").show();                
                clearTimeout(iSubmitEvalHandle);
                fnSubmitEval();
                if (_Player.SCORM.API) _Player.SCORM.setSuspendData("complete-" + _Player.Player.PageID + "-comment", $("#comment-text").val());
                $("#lesson-rating-container .comment > .contents").replaceWith("<div><br />" + _Translations["comment-received"] + "</div>");
                $("#lesson-rating-container .loader").hide();
            });
        })
        $("._content.case").each(function (index, section) {
            section = $(section);
            var SavedValues = _Player.SCORM.getSuspendData("case-" + _Player.Player.PageID + "-text-*");
            section.find(".case-content .case-question").each(function (i, o) {
                var Answer = $(this).nextAll("div:eq(0)");
                if (Answer.length == 0 || !Answer.hasClass("case-answer")) Answer = null;
                $("<div class='left question'><div class='container'><div class='top'></div><div class='fill'><div class='text'>" + $(this).html() + "</div></div><div class='bottom'></div></div></div>").appendTo(section.find(".chat-dialog"));
                $("<div class='right response'><div class='container'><div class='top'></div><div class='fill'><div class='text'><textarea></textarea></div></div></div><div class='bottom'></div></div>").appendTo(section.find(".chat-dialog")).find("textarea").val(cStr(SavedValues["case-" + _Player.Player.PageID + "-text-" + i]));
                if (Answer && Answer.html()) $("<div class='left suggestion'><div class='container'><div class='top'></div><div class='fill'><div class='text'><div class='expand'><span>" + _Translations["show-suggested-answer"] + "</span></div><div class='answer'>" + (Answer ? Answer.html() : "") + "</div></div></div><div class='bottom'></div></div></div>").appendTo(section.find(".chat-dialog"));
                $(this).detach();
                if (Answer) Answer.detach();
            });
            if (trim(replaceAll(section.find(".intro").html(), "\r", "", "\n", "")) == "") section.find(".intro").detach();
            if (trim(section.find(".case-content").html()) == "") section.find(".case-content").detach();
            section.find(".chat-dialog .expand").click(function () { $(this).toggleClass("contract").siblings(".answer").toggle(); });
            section.find("textarea").autosize().change(function (e) { if (this.value == '') this.value = _Translations["write-here"]; $(this).css("color", (this.value == _Translations["write-here"] ? "#569020" : "#243543")); }).change().focus(function () { if (this.value == _Translations["write-here"]) { var h = $(this); setTimeout(function () { h.select(); }, 100); } });
            SavedValues = _Player.SCORM.getSuspendData("case-" + _Player.Player.PageID + "-input-*");
            if (SavedValues.length > 0) section.find("input").each(function (i, o) { $(this).val(cStr(SavedValues[i])); });
            if (_Player.Player.Printing) section.find(".chat-dialog .answer").show();
        });
        this.PrepareContent($("._content"));                     

        if (fnIsTouchDevice()) {
            $.fn.nodoubletapzoom = function () {
                $(this).bind('touchstart', function preventZoom(e) {
                    var t2 = e.timeStamp
                    , t1 = $(this).data('lastTouch') || t2
                    , dt = t2 - t1
                    , fingers = e.originalEvent.touches.length;
                    $(this).data('lastTouch', t2);
                    if (!dt || dt > 500 || fingers > 1) return;
                    e.preventDefault();
                    $(this).trigger('click').trigger('click');
                });
            };
            $("body").nodoubletapzoom();

            //fix virtual keyboard issue on iPads            
            var fn = function () { $(document.body).prepend($("#wrapper-globalmenu").css("position", "absolute").css("top", $(window).scrollTop()).detach()); };
            $(document).bind("touchmove", function () {
                if ($(document).data("scroll-fix")) {
                    clearTimeout($("#wrapper-globalmenu").hide().data("show"));
                    $("#wrapper-globalmenu").data("show", setTimeout(function () { $("#wrapper-globalmenu").show(); }, 200));
                }
            });
            $(document).bind("scroll", function (e) {
                if ($(document).data("scroll-fix")) {
                    $("#wrapper-globalmenu").show();
                    fn();
                }
            });
            $(document).on("focus", "input, textarea", function (e) {
                $(document).data("scroll-fix", true);
                if ($('#myContent').is(':visible')) {
                    $("#myContent").data("ipad-hide", true).hide();
                }
            });
            $(document).on("blur", "input, textarea", function (e) {
                $(document).data("scroll-fix", null);
                if (!$('#myContent').is(':visible') && $("#myContent").data("ipad-hide")) {
                    $("#myContent").data("ipad-hide", null).show();
                }
                $("#wrapper-globalmenu").css("position", "fixed").css("top", 0);
            });            
        }

        $(".close-lesson").off("click.player").on("click.player", function () {
            _Player.Player.Cleanup(true);
            if (window.opener && window.opener.fnRestoreNavigator)window.opener.fnRestoreNavigator(window); else window.parent.fnRestoreNavigator();
        });

        $(".embedded-video").each(function () {
            var Files = [];
            $(this).find("a, img").each(function () { Files.push(cStr($(this).attr("src"), $(this).attr("href"))); });            
            $(this).html('<DIV style="DISPLAY: block" id=button-open-video><DIV class=frame><DIV class=image><A class=play></A></DIV></DIV></DIV><br style="clear:both" />');
            clsVideoPlayer(Files, $(this).attr("width"), $(this).attr("height"));
        });

        if (!_Player.Player.Printing && this.Settings.Player.GlobalDisclaimer)
        {
            $(".content-container").append($("<div class='_content wrapper main-text disclaimer' style=''></div>").html("<hr/>"+this.Settings.Player.GlobalDisclaimer));
        }

        fnEnableJSEffects();
        function fnShowHideMenu(Show, ForceResize) {            
            $("#addressline-right").toggle(!Show);
            if ((Show && !$('#myContent').is(":visible"))
                ||(!Show && $('#myContent').is(":visible")))
                $('#myContent').stop(true).slideToggle(function () { $(window).resize(); });

            var iTopOffset = Show ? 180 : 60;
            var o = $("#wrapper-submenu");
            var bInitial = o.data("initialized") == null;
            o.data("initialized", 1).css("position", "fixed");
            var p = $("#wrapper-submenu .container:eq(1)");
            if ($("#in-page-nav li").length < 2) {
                p.hide();
            } else {
                p.toggleClass("hide", p.height() >= $(window).height() - iTopOffset - 110);
            }
            if (o.data("__animate_to") != iTopOffset || ForceResize) {
                o.data("__animate_to", iTopOffset);
                if (bInitial) o.css("top", iTopOffset);
                else o.animate({ "top": iTopOffset });
            }
            var Filler = $("#global-menu-filler");
            if (Filler.data("__animate_to") != iTopOffset || ForceResize) {
                Filler.data("__animate_to", iTopOffset).animate({ "height": Show ? 131 : 20 });
            }

            if (bInitial) {
                var _options = {
                    position: {
                        adjust: { x: -20, y: 0 },
                        corner: {
                            tooltip: 'topLeft',
                            target: 'bottomRight'
                        }
                    }, style: {
                        border: { width: 5, radius: 10 },
                        tip: { corner: true },
                        classes: 'qtip-rounded qtip-shadow'
                    },
                    show: {
                        event: "mouseover",
                        effect: { type: "fade", length: 100 }
                    },
                    hide: { event: "mouseout" }
                };
                if (_Player.Player.Page.AuxField1 != "splash" && $.cookie("menu-visible") == null) {
                    $.cookie("menu-visible", 1);
                    $('#myContent').slideDown(1000);
                    fnShowHideMenu(true);
                    if (_Translations["menu-tooltip"] && !_Player.Player.Printing) {
                        setTimeout(function () {
                            $('#toggler').qtip($.extend(_options, { content: { text: _Translations["menu-tooltip"], title: _Translations["menu-tooltip-head"] }, show: { ready: true, event: false }, position: { adjust: { y: 0, x: 0 } }, hide: { event: false } }));
                        }, 1000);

                        setTimeout(function () { $('#toggler').qtip("hide"); }, 10000);
                    }
                }
                if (!fnIsTouchDevice()) {
                    if (_Translations["note-tooltip"]) $(".addnote").qtip($.extend(_options, { content: { text: _Translations["note-tooltip"], title: _Translations["note-tooltip-head"] }, position: { adjust: { x: -100, y: 0 } } }));
                    if (_Translations["highlight-tooltip"]) $(".markerswitch").qtip($.extend(_options, { content: { text: _Translations["highlight-tooltip"], title: _Translations["highlight-tooltip-head"] }, position: { adjust: { x: -100, y: 0 } } }));
                    if (_Translations["highlight-menu-tooltip"]) $(".markeroptions").qtip($.extend(_options, { content: { text: _Translations["highlight-menu-tooltip"], title: _Translations["highlight-menu-tooltip-head"] }, position: { adjust: { x: 0, y: 30 }, corner: { tooltip: 'topRight', target: 'bottomLeft' } } }));
                }
            }
        }
        function fnResizeWindow(ForceResize) {
            if (!$.cookie || window.location.href.indexOf("header=hide") > -1 || _Player.Player.Printing) return;
            $(".mainmenu-maximized").css("padding-right", $(window).width() > 1024 ? Math.min(140, $(window).width() - 1024) : 0);
			if(!_Player.Player.Settings.Player.DynamicContentWidth)
				$(".wrapper.main, .wrapper.main-text, .wrapper.main-wide, .wrapper.empty").each(function () {
					if (fnShouldAddToNav(this)) {                    
						var i = Math.max(0, ($(window).width() - $(this).width()) / 2);
						var Level = 250 + ($(this).parent().hasClass("checklist") > 0 ? -200 : 0);
						if (i < Level && $("#wrapper-submenu:visible li").length > 0) i = Math.max(0, i - (Level - i));
						$(this).css("margin-left", i);
					}
				});                  
        }
		
		new WOW().init();
        $(window).resize(function () { fnResizeWindow(false); })
        fnResizeWindow(true);        
        fnShowHideMenu(($.cookie("menu-visible") == null || cInt($.cookie("menu-visible")) > 0) && $("._content.splash").length == 0 && $("._editing").length == 0);

        if (fnGetIEVersion() > 0 && fnGetIEVersion() < 9) $("#quicknav-left, #quicknav-right").addClass("ie");
        $("table.standard").each(function (i, o) { if ($(this).find("tr").length > 1 && $(this).find("thead tr, th").length == 0) { var h = $("<thead />").prependTo(this); $(this).find("tr:eq(0) td").each(function (i, o) { h.append(o); }); } });        
        $("#loading-screen").hide();
        if (window.location.href.indexOf("header=hide") > -1) {
            $("#quicknav-left, #quicknav-right, #wrapper-submenu, #global-menu-filler, #wrapper-globalmenu").hide();
            $(document.body).addClass("nohead");
        }
        $(".metierbox-answer").each(function () {            
            $(this).html('<div class="img" /><textarea></textarea><br />').find("textarea").val(cStr(_Player.SCORM.getSuspendData("metierbox-" + _Player.Player.PageID+"-" + $(this).parents(".metierquestion:eq(0)").attr("id")))).autosize().on("change blur", function (e) { if (this.value == '') this.value = _Translations["write-here"]; $(this).keyup(); }).keyup(function () { var b = trim(this.value) == _Translations["write-here"]; $(this).css("color", (b ? "#838383" : "#243543")); }).change().focus(function () { if (trim(this.value) == _Translations["write-here"]) { $(this).val("").keyup(); } }).parents(".metierquestion:eq(0)").find(".metierbox-content").wrapInner("<div class='text'/>").prepend("<div class='img'/>");
        });
        $(".metierbox-title").each(function () {
            if (!trim($(this).html())) $(this).addClass("empty").html("");
        })
		$(".saveable-form").each(function (i,o) {					
			$(this).find("input,textarea,select").each(function(j,p){
				if(!$(this).attr("name"))$(this).attr("name","field-"+j);
			});
			//var s=_Player.SCORM.getSuspendData("saveable-form-" + _Player.Player.PageID);
			var s="field-0=xx&field-1=yy&field-2=zz&field-3=&field-11=on";
			if(s)$(this).find("input,select,textarea").deserialize(s);
        });
		$(".fullwidth").each(function(){
			$(this).wrapInner("<div class='inner' />");
		});
		
        if (window.PIE) $(".metierbox-content, .metierbox-answer").each(function () { PIE.attach(this); });
        $(".datatable:eq(0)").each(function (i, o) {
            var CreateDatatable = function () {
                $(".datatable").DataTable({
                      //"language": {"url": "datatables/datatables.nb.json"}
                  });
            };
            if (!$(this).DataTable) {
                fnLoadCSS("datatables/datatables.css");
                fnLoadJS("datatables/datatables.js", CreateDatatable);
            }
        });
        $(".lightslider:eq(0)").each(function (i, o) {
            var CreateSlider = function () {
                $(".lightslider li").each(function () {
                    var Url = $(this).find("img:eq(0)").attr("src"),
                    Alt = $(this).find("img:eq(0)").attr("alt");
                    if (!$(this).attr("data-thumb")) $(this).attr("data-thumb", "../inc/library/exec/thumbnail.aspx?w=133&h=100&z=1&d=1&u=" + escape(Url));
                    if (!$(this).attr("data-src")) $(this).attr("data-src", Url);
                    if (Alt && !$(this).attr("data-sub-html")) $(this).attr("data-sub-html", Alt);
                });                
                $(".lightslider").each(function (i, o) {
                    _Player.Player.MaskContent();
                    var Block = $(this).parents(".page-block");
                    var BlockVisible = Block.hasClass("show");
                    if (!BlockVisible)Block.addClass("show");
                    $(this).lightSlider({
                        gallery: true,
                        item: 1,                        
                        loop: false,
                        slideMargin: 10,
                        thumbItem: 5,
                        galleryMargin:0,
                        enableDrag: false,
                        adaptiveHeight: true,
                        /*thumbMargin:4,
                        autoWidth:true,                         
                            */
                        slideMove: 1,
                        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                        speed: 600,
                        onSliderLoad: function (el) {
                            el.lightGallery({                                
                                selector: '.lightslider .lslide',
                                download:0
                            });
                        }
                    });
                    setTimeout(function () {
                        if (!BlockVisible) Block.removeClass("show");
                        _Player.Player.UnMaskContent();
                    }, 1);
                    
                });
            }
            if (!$(this).lightSlider) {
                fnLoadCSS("lightslider/lightslider.min.css");
                fnLoadCSS("lightslider/lightgallery.min.css");
                fnLoadJS("lightslider/lightslider.min.js", function () { fnLoadJS("lightslider/lightgallery.min.js", CreateSlider); });                
            } else {
                CreateSlider();
            }
        });
        $(".page-block").removeClass("show");
        this.ScrollTo($(".page-block:eq(0)"));
        if (this.Page.ScrollBlock) this.ScrollTo($(".page-block[data-page-block='" + this.Page.ScrollBlock + "']"));
        
        $(window).scrollTop(cInt(_Player.Player.Page.ScrollTop));
        var setScrollTimer = function () {
            setTimeout(function () {
                if (_Player.Player.NonLoadedImages > 0) setScrollTimer();
                else {
                    $(window).scrollTop(cInt(_Player.Player.Page.ScrollTop));
                    $(window).scroll();
                }
            }, 100);
        }
        setScrollTimer();
        $("a[href^='#']").click(function (e) {
            var t = $("a[name='" + $(this).attr("href").substring(1) + "']");
            if (t.length > 0) {
                _Player.Player.ScrollTo(t[0], e);
                e.cancelBubble = true;
                return false;
            }
        });
        $("._content:first").filter(".theory, .intro, .quiz, .examples, .reflection, .splash, .complete, .case, .empty").css("padding-top", 40);
        $("._content, #page-block-nav").filter(":last").filter(".theory, .intro, .quiz, .examples, .reflection, .splash, .complete, .case").css("padding-bottom", 40);        
        setTimeout(function () {            
            _Player.Player.UnMaskContent();			
        }, 1);
        this.Loaded = true;
    }
    function fnGetContentsByType(parent, type, deep, arr) {
        if (!arr) arr = [];
        if (parent.Content) $.each(parent.Content, function () { if (!type || type == this.AuxField1) arr.push(this); });
        if (deep && parent.Folders) $.each(parent.Folders, function () { fnGetContentsByType(this, type, deep, arr); });
        return arr;
    }
    function fnFindPage(parentLesson, lessonID, pageID) {
        var lesson, page;
        var ret = function (lesson, page) {            
            return { Lesson: lesson, Page: page, HasPageContent: (page && page.Content && page.Content.length > 0), PageIndex: (lesson.Folders ? $(lesson.Folders).index(page) : -1) };
        }
        if (parentLesson.Folders) {
            for (var x = 0; x < parentLesson.Folders.length; x++) {
                //check pages
                if (cInt(pageID)>0 && parentLesson.Folders[x].ObjectID == pageID) {
                    return ret(parentLesson, parentLesson.Folders[x]);
                }
                if (!pageID && !lesson) {
                    if (lessonID == parentLesson.Folders[x].ObjectID || !lessonID) {                        
                        page = _Player.Player.FindFirstPage(parentLesson.Folders[x]);
                        if (!lesson) lesson = parentLesson.Folders[x];
                    }
                    if (lesson) return ret(lesson, page);
                }
                var o = fnFindPage(parentLesson.Folders[x], lessonID, pageID);
                if (o) return o;
            }
        }
    }

    this.SavePlayerValues = function(terminate) {        
        if (!_Player.SCORM) return;
        var clearedSections = {};
        $("._content.checklist div.checklist .content .item").each(function (i, o) {
            o=$(o);
            var key = o.data("key");
            if (!key && o.hasClass("custom") && trim($(this).find("textarea").val())!="") 
                key = $(this).parents("._content.checklist:eq(0)").attr("content_id") + "-" + i;            
            if(key){
                var sectionID = key.split("-")[0];
                var index = key.split("-")[1];                
                if(!clearedSections[sectionID]){
                    _Player.SCORM.clearSuspendData("checklist-" + sectionID+ "-*");
                    clearedSections[sectionID]=1;
                }
                        
                if ($(this).find(".checkbox").hasClass("checked")) _Player.SCORM.setSuspendData("checklist-" + sectionID + "-checked-" + index, "1");
                if ($(this).hasClass("custom")) { var s = $(this).find("textarea").val(); if (s != "" && s != _Translations["write-here"]) _Player.SCORM.setSuspendData("checklist-" + sectionID + "-text-" + index, s); }
            }
        });

        $("._content.reflection .chat-dialog textarea").each(function (i, o) {
            if (i == 0) _Player.SCORM.clearSuspendData("reflection-*");
            if ($(o).val() != "" && $(o).val() != _Translations["write-here"]) _Player.SCORM.setSuspendData("reflection-" + $(this).attr("target"), $(o).val());
        });

        $("._content.case .chat-dialog").each(function (i, o) {
            if (i == 0) _Player.SCORM.clearSuspendData("case-" + _Player.Player.PageID + "-*");
            _Player.SCORM.setSuspendData("case-" + _Player.Player.PageID + "-text-" + i, $(this).find("textarea").val());
            _Player.SCORM.setSuspendData("case-" + _Player.Player.PageID + "-input-" + i, $(this).find("input").val());
        });
        
        $(".metierbox-answer textarea").each(function () {
            _Player.SCORM.setSuspendData("metierbox-" + _Player.Player.PageID + "-" + $(this).parents(".metierquestion:eq(0)").attr("id"), $(this).val());
        });
        $(".saveable-form").each(function (i,o) {					
            _Player.SCORM.setSuspendData("saveable-form-" + _Player.Player.PageID, $(this).find("input,select,textarea").serialize());
        });
        var isFinalTest = $.grep($("._content.quiz"), function (item) { var o = $(item).data("quiz"); return o && o.Quiz.Settings.FinalTest;})[0]!=null;
        $("._content.quiz").each(function (index, section) {
            section = $(section);
            var o = section.data("quiz");
            if (o && o.Quiz.Questions) {
                o.Quiz.save();
                var Answer = "", MaxScore = 0;
                $(o.Quiz.Settings.Data).each(function () {
                    if (cStr(this.Answers) != "") {
                        var LessonQuiz = o.Quiz.LessonQuiz;
                        Answer += (Answer ? ";quiz:" : "") + "q" + this.QuizID + ";" + this.Answers + (this.Answers.indexOf("score:") == -1 ? ";score:" + this.Score + ";" : "");                                                
                        if (LessonQuiz && this.Score > LessonQuiz.TotalScore) LessonQuiz.MaxScore = this.Score;
                        if (this.Score > MaxScore) MaxScore = this.Score;
                    }
                });                
                if (o.Quiz.Settings.FinalTest && Answer) {
                    _Player.SCORM.LessonStatus = "C";
                    _Player.SCORM.Score = MaxScore;
                }
                _Player.SCORM.setSuspendData("quiz" + (o.Quiz.Settings.FinalTest?"":"-" + section.attr("content_id")), Answer);                                    
            }
        });
        if (!isFinalTest && $("._content.quiz").length > 0) {
            var S = 0, Q = 0;
            $.each(_Player.Player.LessonQuizes, function () {
                S += Math.max(cInt(this.MaxScore), this.TotalScore) * this.QuizWeight*this.Multiplier;
                Q += this.Weight * this.QuizWeight;
            });
            S = cInt((Q? S / Q : 0));
            _Player.SCORM.Score = Math.min(100, Math.max(S, cInt(_Player.SCORM.Score)));
            if(_Player.SCORM.Lesson)_Player.SCORM.Lesson.Score = _Player.SCORM.Score;
            _Player.Player.RefreshCourseStatus();
        }        
        if (this.Settings.Player.RestoreLastPosition) _Player.SCORM.setSuspendData("lastpage", _Player.Player.PageID);

        if (_Player.SCORM.Lesson) {
            if(_Player.SCORM.Score) _Player.SCORM.Lesson.Score = _Player.SCORM.Score;
            _Player.SCORM.Lesson.Status = (_Player.SCORM.LessonStatus + "i").substring(0, 1).toUpperCase();
            _Player.SCORM.writeValues();
            if (terminate) {                
                _Player.SCORM.API.LMSFinish();
                _Player.SCORM.Win.iCurrentRCO = -1;
            }
        }
    }
    $(window).on("unload", function () { _Player.Player.Cleanup(true); });
    function fnDisplayPageAsDialog(page,opts) {
        if (page) {
            opts = $.extend({}, { title: page.Name }, opts);
            if (page.Content && page.Content.length > 0)
                if (page.Content[0].AuxField1 == "file")
                    window.open(page.Content[0].Filename)
                else
                    fnDisplayDialog("<h1>" + page.Content[0].Name + "</h1>" + page.Content[0].Xml, opts);
            else {
                $.ajax({
                    url: fnMetierUrl("player.aspx?PageID=" + page.ObjectID),
                    cache: false,
                    success: function (data) {
                        fnDisplayDialog($(data).find(".wrapper.main-text"), opts);
                    }
                });
            }
        }
    }

}

function clsHighlighter() {    
    this.Unload = function () {
        _this.Words.off("touchend.hl mouseup.hl");
        $(document.body).off("mouseup.hl touchend.hl mousemove.hl touchmove.hl");
        _this.Words = null;
        _this.Highlights = null;
        _this.Notes = null;
        if(_this.SaveTimer) clearInterval(_this.SaveTimer);        
    }
    if ($("#in-page-nav").length == 0) $("#wrapper-submenu").prepend('<div class="container">' + _Translations["learning-tools"] + '<ul class="tools"></ul></div><br /><div class="container">' + _Translations["in-page-nav"] + '<ul id="in-page-nav"></ul></div>');
    $(document.body).on("mouseup.hl touchend.hl", function (e) { var o = $(document).data("dragging-note"); if (o) { var Element = fnGetNearestWord(e.clientY); if (Element) { var Key = fnGetKey(Element, Element); $(o).data("note").Key = Key.Instance + "§" + Key.Text; } $(document).data("dragging-note", null); fnSaveSuspendData(); } })
    .on("mousemove.hl touchmove.hl", function (e) { var o = $(document).data("dragging-note"); if (o) { e.preventDefault(); $(o).data("pos", { y: e.clientY | (e.originalEvent && e.originalEvent.touches && e.originalEvent.touches.length > 0 ? e.originalEvent.touches[0].clientY : 0) }); var Element = fnGetNearestWord($(o).data("pos").y); if (Element) $(Element).after(o); if (window.getSelection) window.getSelection().removeAllRanges(); else document.selection.empty(); } });
    this.Selection = {};
    this.Highlights=[];
    this.Notes=[];
    var _this = this;
    _Player.Player.NonLoadedImages = 0;

    this.PrepareImg = function (Imgs) {
        var fnZoomEnabled = function (Img) {
            return !$(Img).is(".no-zoom") && (_Player.Player.Settings.Player.AutoZoomImages || $(Img).is(".do-zoom"));
        }
        Imgs.each(function (i, o) {
            var Alt = $(o).attr("alt");
            var Max = { w: $(o).attr("zoom-width"), h: $(o).attr("zoom-height") };
            var Min = { w: $(o).attr("normal-width"), h: $(o).attr("normal-height") };
            if ($(o).is("iframe")) {
                var Alt = $(o).attr("title");
                if (Alt) {
                    iImageCounter++;
                    if ($(o).next().is("br")) $(o).next().remove();
                    $(o).wrap("<div class='image-caption' />").wrap("<div class='img' />");
                    $("<div class='title caption'/>").html((_Translations["image-title"]?replaceAll(_Translations["image-title"], "{0}", iImageCounter) + " - " : "")+ Alt).appendTo($(o).parent());
                }
            }else if ($(o).is("img")) {
                if (fnZoomEnabled(o))$(o).css("width", "auto").css("height", "auto");
                if (Alt) iImageCounter++;

                var fnFixImage = function (Alt) {
                    if ($(o).next().is("br")) $(o).next().remove();
                    $(o).wrap("<div class='image-caption " + replaceAll(" "+$(o).attr("class")," border"," ") + "' />").wrap("<div class='img' />");
                    if ($(o).is(".right, .left")) {
                        var w = $(o).attr("width") || $(o).css("width");
                        if (w) $(o).parents(".image-caption:eq(0)").css("width", w)
                    }
                    
                    if (fnZoomEnabled(o)) {
                        var _w = $(o).css("width").split("px").join("");
                        var _h = $(o).css("height").split("px").join("");
                        if (_w > 600 || _h > 400 || Max.w || Max.h || Min.w || Min.h) {
                            if (cInt(Max.w, Max.h) == 0 && (_w > 900 || _h > 900)) {
                                if (_w / 900 > _h / 900) Max.w = 900;
                                else Max.h = 900;
                            }
                            if (cInt(Max.w, Max.h) > 0) {
                                if (cInt(Max.h) > 0 && cInt(Max.w) > 0) {
                                    _w = cInt(Max.w);
                                    _h = cInt(Max.h);
                                } else if (cInt(Max.h) > 0) {
                                    _w = _w * (cInt(Max.h) / _h);
                                    _h = Max.h;
                                } else if (cInt(Max.w) > 0) {
                                    _h = _h * (cInt(Max.w) / _w);
                                    _w = Max.w;
                                }
                            }
                            var shrink = { w: cStr(Min.w, "auto"), h: cStr(Min.h, "auto") };
                            if (cInt(Min.w, Min.h) == 0) {
                                //if(_h > 350)shrink.h=Math.min(_h,350);
                                if (_w <= 600 && _h > 400) shrink.h = Math.min(_h, 400);
                                else shrink.w = Math.min(_w, 600);
                            }
                            var _resize = function (Shrink) {
                                if (Shrink) $(o).css("width", shrink.w).css("height", shrink.h).parent().css("left", "0");
                                else $(o).css("width", cStr(Max.w, "auto")).css("height", cStr(Max.h, "auto")).parent();
                                if ($(o).width() > 700) $(o).parent().css("left", (-$(o).width() + 700) / 2);
                                $(o).data("expanded", Shrink ? 0 : 1);
                                $(o).parent().parent().find(".fa").toggleClass("fa-search-plus", Shrink).toggleClass("fa-search-minus", !Shrink);
                            }

                            if (!(cInt(Min.w, Min.h) > 0 && Max.w == Min.w && Max.h == Min.h || (Math.abs(_w - shrink.w) < 40 || Math.abs(_h - shrink.h) < 40))) {                                                                
                                $(o).parent().parent().addClass("auto-expand").click(function () {
                                    var i = $(o).data("expanded");
                                    if (i == 1) {
                                        _resize(true);
                                    } else {
                                        _resize(false);
                                    }
                                });
                            }
                            _resize(true);
                        }
                    }
                    if (Alt || $(o).parents(".auto-expand").length > 0) {
                        var title = $("<div class='title'/>").appendTo($(o).parent().parent());
                        if ($(o).parents(".auto-expand").length > 0) title.append("<i class='fa fa-search-plus'></i>");
                        if (Alt) title.addClass("caption").append("<div>" + (_Translations["image-title"] ? replaceAll(_Translations["image-title"], "{0}", $(o).data("image-index")) + " - " : "") + Alt + "</div>");
                    }
					
                };
                $(o).data("image-index", iImageCounter);
				
                if (!$(o).prop('complete') && (fnGetIEVersion() < 10 || cInt(replaceAll($(o).css("width"), "px", "")) == 0)) {
                    $(o).css("visibility","hidden");
					_Player.Player.NonLoadedImages++;					
                    $(o).load(function () {						
                        fnFixImage(Alt);
                        _Player.Player.NonLoadedImages--;						
						$(o).css("visibility","");
						if(_Player.Player.Settings.Player.ImageFadeIn)
							$(o).addClass("wow fadeIn");
                    });
                } else {
                    fnFixImage(Alt);
                }
            }
        });
    }    
    this.PrepareTable=function(Tbls){
        Tbls.each(function (i, o) {
            if ($(o).is("[summary][summary!='']"))
                $("<div style='border-top:1px dotted black; margin:20px 0; padding-top:3px; font-size:12px; text-align:left' />").html((_Translations["table-title"]?replaceAll(_Translations["table-title"], "{0}", ++iTableCounter) + " - " : "") + $(o).attr("summary")).insertAfter($(o));
        });
    }
    this.PrepareHead=function(Heads){
        Heads.each(function (i, o) {
            var Title = replaceAll($(o).is("h1") && _Player.Player.SuppressFirstH2 ? $(o).next().text() : $(o).text(), "&nbsp;", " ");
            if (!$(o).hasClass("nonav") && !$(o).hasClass("lesson-title") && Title) {
                if ($(o).is("h2") && _Player.Player.SuppressFirstH2 && !bFirstH2Found) {
                    bFirstH2Found = true;
                } else {                    
                    if ($(o).hasClass("l0")) Title = "<b>" + Title + "</b>";
                    $("<li><a class='roundbutton' href='javascript:void(0)'>" + Title + "</a></li>").appendTo($("#in-page-nav")).click(function () { _Player.Player.ScrollTo(o); });
                }
            }
        });
    }
  

    var iImageCounter = 0, iTableCounter = 0, bFirstH2Found;
    if (!_Player.Player.Editing){
        var Ctrls = $(".wrapper.main, .wrapper.main-text, .wrapper.main-wide")
            .filter(function () { return $(this).closest(".splash").length == 0 && fnShouldAddToNav(this); });
        this.PrepareImg(Ctrls.find("img:not(.no-process), iframe:not(.no-process)"));
        this.PrepareTable(Ctrls.find("table"));
        this.PrepareHead(Ctrls.find((_Player.Player.PaginationTag?"h1:first:visible,":"") + "h2"));
    }        

    if (!fnIsTouchDevice()) {
        $("body").mousemove(function (e) {
            if ($("body").hasClass("cursor-add-note")) {
                $("#add-new-note").detach();
                var Element = fnGetNearestWord(e.clientY);
                if (Element && Element.className.indexOf("word") > -1) {
                    $(Element).after("<div id='add-new-note' />");
                }
            }
        });
        $(".wrapper.main-text").on("mouseup.hl", function (e) { var s = cStr($("body").attr("class")); fnMouseUp(s); if (s.indexOf("cursor-hl-") > -1) fnHighlight(_this.Selection.Key, s); });
    } else {
        _this.SaveTimer = setInterval(function () { fnSaveSelection(); }, 2000);
    }
    function fnGetNearestWord(y) {
        var StartX = $(".wrapper.main-text").offset().left;
        var Width = $(".wrapper.main-text").width();
        var checker = function (y) {
            for (var x = StartX; x < StartX + Width; x = x + 10) {
                var oElement = document.elementFromPoint(x, y);
                if (oElement && oElement.className.indexOf("word") > -1) return oElement;
            }
        };
        var Element = null;
        var InitialY = y;
        while (y < InitialY + 400 && (!Element || Element.className.indexOf("word") == -1)) {
            Element = checker(y);
            y = y + 15;
        }
        return Element;
    }
    function fnHighlight(oKey, sClass) {
        var sColor = (" " + sClass + " ").split("cursor-hl-")[1].split(" ")[0];
        $(oKey.Controls).each(function () { $(this).removeClass().addClass(sColor + " word"); });
        var sKey = sColor + "§" + oKey.Instance + "§" + oKey.Length + "§" + oKey.Text;

        _this.Highlights.push(sKey);
        $(oKey.Controls[0]).data("highlight", sKey);
        fnSaveSuspendData();

        if (window.getSelection) window.getSelection().removeAllRanges();
        else if (document.selection) document.selection.empty();
    }

    function fnSaveSelection() {
        var S = fnGetSelection();
        if (!S) {
            if (fnIsTouchDevice()) $("#pop-farger").hide();
            return;
        }
        _this.Selection.Range = S.Range;
        var oKey = fnGetKey(S.Start, S.End);
        _this.Selection.Key = oKey;
        _this.Selection.Time = new Date();
        if (fnIsTouchDevice()) $("#pop-farger").toggle(oKey.Controls.length > 0 && !$(document).data("scroll-fix"));
    }
    function fnMouseUp(sClass) {
        if (sClass.indexOf("cursor-add-note") > -1) {
            $("body").removeClass("cursor-add-note");
            fnAddNote($("#add-new-note").prev("span.word").length == 0 ? $("#add-new-note").next("span.word") : $("#add-new-note").prev("span.word"), "");
            $("#add-new-note").detach();
        } else if (sClass.indexOf("cursor-hl-") > -1) {
            fnSaveSelection();
        }
    }

    function fnGetKey(oStart, oEnd) {
        var fn = function (o, t) {
            var i = -1;
            while (o != null) {
                i = _this.Words.index(o);
                if (i == -1) {
                    var p = $(o).find("span.word");
                    for (var x = 0; x < p.length && i < 0; x++) {
                        i = _this.Words.index(p[x]);
                        if (i > 0 && t == 1 && fnIsTouchDevice()) i--;
                    }
                }
                if (i > -1) return i;
                o = $(o).next()[0];
            }
        };
        var iStart = fn(oStart, 0);
        var iEnd = fn(oEnd, 1);
        //For some reason, _this.Words sometimes references other SPANs than those in document
        if (typeof iStart === "undefined" && typeof iEnd === "undefined") {
            _this.ScanWords();
            iStart = fn(oStart, 0);
            iEnd = fn(oEnd, 1);
        }
        var oReturn = { "Controls": [], "Instance": 0, "Length": (iEnd - iStart + 1), "Start": oStart, "End": oEnd, "iStart": iStart, "iEnd": iEnd };
        oReturn.Text = $(_this.Words[iStart]).text() + (_this.Words.length > iStart + 1 ? $(_this.Words[iStart + 1]).text() : "");

        for (var x = 0; x < iStart; x++) {
            if ($(_this.Words[x]).text() + (_this.Words.length > x + 1 ? $(_this.Words[x + 1]).text() : "") == oReturn.Text) oReturn.Instance++;
        }

        for (var x = iStart; x <= iEnd; x++) oReturn.Controls.push(_this.Words[x]);
        return oReturn;
    }

    function fnGetControls(sStr, iInstance, iCount) {
        for (var x = 0; x < _this.Words.length; x++) {
            var s = sStr;
            var oReturn = [];
            for (; s.length > 0 && x < _this.Words.length; x++) {
                if (s.indexOf($(_this.Words[x]).text()) != 0) break;
                s = s.substring($(_this.Words[x]).text().length);
                if (oReturn.length < iCount) oReturn.push(_this.Words[x]);
            }
            if (s.length == 0) {
                if (iInstance > 0) iInstance--;
                else {
                    while (iCount - oReturn.length > 0) oReturn.push(_this.Words[x++]);
                    return oReturn;
                }
            }
        }
        return [];
    }

    function fnLoadSuspendData() {
        _this.Highlights.length = 0;
        _this.Notes.length = 0;
        if (!_Player.Player.Printing || cStr(_Player.PrintOptions).indexOf("cIncludeHighlights") > -1) {
            var Data = _Player.SCORM.getSuspendData("hl-" + _Player.Player.PageID + "-*");
            $(Data).each(function (i, o) {
                var sKey = o[1];
                var o = fnGetControls(sKey.split("§")[3], sKey.split("§")[1], sKey.split("§")[2]);
                if (o.length > 0) {
                    $(o).each(function (i, o) { $(o).addClass(sKey.split("§")[0]); });
                    $(o[0]).data("highlight", sKey);
                    _this.Highlights.push(sKey);
                }
            });
        }
        if (!_Player.Player.Printing || cStr(_Player.PrintOptions).indexOf("cIncludeNotes") > -1) {
            var Data = _Player.SCORM.getSuspendData("note-" + _Player.Player.PageID + "-*");
            $(Data).each(function (i, o) {
                var sKey = o[0];
                var Ctrl = fnGetControls(sKey.split("§")[2], sKey.split("§")[1], 1);
                if (Ctrl.length > 0) {
                    fnAddNote(Ctrl[0], o[1]);
                }
            });
        }
    }
    function fnSaveSuspendData() {
        _Player.SCORM.clearSuspendData("hl-" + _Player.Player.PageID + "-*");
        _Player.SCORM.clearSuspendData("note-" + _Player.Player.PageID + "-*");

        $.each(_this.Highlights, function (i, o) { if (o) _Player.SCORM.setSuspendData("hl-" + _Player.Player.PageID + "-" + i, this); });
        $.each(_this.Notes, function (i, o) { if (o) _Player.SCORM.setSuspendData("note-" + _Player.Player.PageID + "-" + i + "§" + this.Key, $(this.Obj).find("textarea").val()); });
    }

    function fnAddNote(oCtrl, sText) {
        $(".markerswitch.active>a").click();
        var oObj = $("<span class='user-note'><div class='corner'></div><div class='dragger'></div><div class='remove'></div><textarea></textarea></span>").insertBefore(oCtrl).find("textarea").val(sText).autosize().change(function (e) { if (this.value == '') this.value = _Translations["write-here"]; $(this).css("color", (this.value == _Translations["write-here"] ? "#996633" : "#3d3b33")); }).change().change(function () { fnSaveSuspendData(); }).focus(function () { if (this.value == _Translations["write-here"]) { if (fnIsTouchDevice()) { var d = this; setTimeout(function () { d.setSelectionRange(0, 9999); }, 100); } $(this).select(); } }).focus().parents(".user-note").click(function () { $(this).find("textarea").focus(); }).find(".remove").click(function () { var k = $(this).parents(".user-note:eq(0)").data("note"); $(_this.Notes).each(function (i, o) { if (o && o.Key == k.Key) { _this.Notes[i] = null; } }); $(this).parents(".user-note:eq(0)").detach(); fnSaveSuspendData(); }).parents(".user-note");
        //.find(".dragger")
        oObj.on("mousedown touchstart", function (e) { if ((e.which == 1 || e.type == "touchstart")&& !$(e.target).is("textarea")) { $(document).data("dragging-note", oObj); } });

        var oKey = fnGetKey(oCtrl, oCtrl);
        var sKey = oKey.Instance + "§" + oKey.Text;
        var oSettings = { "Key": sKey, "Obj": oObj };
        _this.Notes.push(oSettings);
        oObj.data("note", oSettings);
        var sel = window.getSelection ? window.getSelection() : document.selection;
        if (sel) {
            if (sel.removeAllRanges) {
                sel.removeAllRanges();
            } else if (sel.empty) {
                sel.empty();
            }
        }
    }
    function fnSetBodyClass(sClass) {
        if ($(document.body).data("__current-class")) $(document.body).removeClass($(document.body).data("__current-class"));
        if (sClass) $(document.body).addClass(sClass).data("__current-class", sClass);
    }

    function fnRemoveHighlight(o, s) {
        if (s == null) s = fnGetHighlightColor(o);
        var i = _this.Words.index(o);
        for (var x = i; x >= 0 && $(_this.Words[x]).hasClass(s) ; x--) {
            $(_this.Words[x]).removeClass(s);
            if (cStr($(_this.Words[x]).data("highlight")).indexOf(s) > -1) _this.Highlights.removeByValue(null, $(_this.Words[x]).data("highlight"));

        }
        for (var x = i + 1; x < _this.Words.length && $(_this.Words[x]).hasClass(s) ; x++) {
            $(_this.Words[x]).removeClass(s);
            if (cStr($(_this.Words[x]).data("highlight")).indexOf(s) > -1) _this.Highlights.removeByValue(null, $(_this.Words[x]).data("highlight"));
        }

        fnSaveSuspendData();
    }
    function fnGetHighlightColor(o) {
        if ($(o).hasClass("yellow")) return "yellow";
        if ($(o).hasClass("pink")) return "pink";
        else if ($(o).hasClass("blue")) return "blue";
        else if ($(o).hasClass("green")) return "green";
        return "";
    }

    //Initialize
    var text_nodes = $(".wrapper.main-text").contents().filter(function () {
        return this.nodeType == 3 && this.nodeValue.match(/[a-zA-Z]{2,}/);
    });
    text_nodes.replaceWith(function () {
        return "<span>" + $(this).text() + "</span>";        
    });
    var text_nodes = $(".wrapper.main-text *:not(iframe)").contents().filter(function () {
        return this.nodeType == 3 && this.nodeValue.match(/[a-zA-Z]{2,}/);
    });
    if (text_nodes.length > 0) {
        text_nodes.replaceWith(function (i) {
            var t = $(this).text();
            var s = trim(t.split("\n").join(" ").split("\r").join(" ").split("\t").join("").split("        ").join(" ").split("    ").join(" ").split("  ").join(" ").split("  ").join(" ").split("  ").join(" ").split("  ").join(" ").split("  ").join(" ").split("  ").join(" ").split("  ").join(" "));
            return "<span class='word'>" + (t.substring(0, 1) == " " ? " " : "") + s.split(" ").join(" </span><span class='word'>") + (t.substring(t.length - 1) == " " ? " " : "") + "</span>";
        });
    }
    this.ScanWords=function() {
        _this.Words=$("span.word");        
    }
    this.ScanWords();
    fnLoadSuspendData();
    
    _this.Words.on(fnIsTouchDevice() ? "touchend.hl" : "mouseup.hl", function (e) {
        var s = fnGetHighlightColor(this);
        if (s != "") {
            if (!fnIsTouchDevice() && $("body").hasClass("cursor-erase")) fnRemoveHighlight(this, s);
            if (fnIsTouchDevice()) {
                var i = _this.Words.index(this);
                var oTopMostWord = this, oRightMostWord = this;
                for (var x = i; x >= 0 && $(_this.Words[x]).hasClass(s) ; x--) {
                    oTopMostWord = _this.Words[x];
                    if ($(oRightMostWord).offset().left > $(_this.Words[x]).offset().left) oRightMostWord = _this.Words[x];
            }
                for (var x = i + 1; x < _this.Words.length && $(_this.Words[x]).hasClass(s) ; x++) {
                    if ($(oRightMostWord).offset().left + $(oRightMostWord).width() < $(_this.Words[x]).offset().left + $(_this.Words[x]).width()) oRightMostWord = _this.Words[x];
            }
                var _this = this;
                $(".delete-highlight").detach();
                $("<div class='delete-highlight' />").click(function () { fnRemoveHighlight(_this, s); $(".delete-highlight").detach(); }).css("left", $(oRightMostWord).offset().left + $(oRightMostWord).width() - 8).css("top", $(oTopMostWord).offset().top - 15).appendTo($("body"));
        }
    }
    });

    $("#wrapper-submenu .tools:eq(0)").append($("<li><a class='addnote' href='javascript:void(0)'>" + _Translations["add-note"] + "</a></li>").find("a").click(
        function (e) {
            if (fnIsTouchDevice()) {
                var Element = fnGetNearestWord(e.originalEvent && e.originalEvent.touches && e.originalEvent.touches.length > 0 ? e.originalEvent.touches[0].y : e.clientY);
                fnAddNote(Element ? $(Element) : $("span.word:eq(60)"), "");
            } else
                fnSetBodyClass($(document.body).hasClass("cursor-add-note") ? null : "cursor-add-note");
    }
    ).end())
.append($("<li class='markeroptions'><a href='javascript:void(0);'></a></li>").find("a").click(function () { $("#pop-farger").toggle(); }).end())
.append($("<li class='markerswitch'><a class='yellow'>" + _Translations["add-highlight"] + "</a><div id='pop-farger' class='drop-shadow pop-menu'><div class='head'>" + _Translations["highlight-colors"] + "<div class='close' onclick='$(this).parent().parent().hide();'>x</div></div></div></li>").find("a").click(function () { $(this).parent().toggleClass("active"); fnSetBodyClass($(this).parent().hasClass("active") ? cStr($(this).data("__class"), "cursor-hl-yellow") : null); }).end());

    var sColors = ["cursor-hl-yellow", "cursor-hl-pink", "cursor-hl-green", "cursor-hl-blue"];
    if (!fnIsTouchDevice()) sColors.push("cursor-erase");
    var sNames = [_Translations["highlight-yellow"], _Translations["highlight-pink"], _Translations["highlight-green"], _Translations["highlight-blue"], _Translations["highlight-eraser"]];

    $(sColors).each(function (i, s) { $("#pop-farger").append("<div class='color' highlight-class='" + this + "'><div class='box " + replaceAll(this, "cursor-", "", "hl-", "") + "'></div><div class='label'>" + sNames[i] + "</div><br /></div>"); });

    $("#pop-farger .color").mouseenter(function () { $(this).addClass("hover"); }).mouseleave(function () { $(this).removeClass("hover"); }).click(function () {
        $(".markerswitch").addClass("active"); $(".markerswitch a").removeClass().addClass(replaceAll($(this).attr("highlight-class"), "cursor-", "", "hl-", "")).data("__class", $(this).attr("highlight-class")); fnSetBodyClass($(this).attr("highlight-class")); $("#pop-farger").hide();
    }).on("click.hl", function (e) { if (fnIsTouchDevice() && _this.Selection.Key != null) { fnHighlight(_this.Selection.Key, $(this).attr("highlight-class")); } });
}


function fnSendEmail(sTo, sFrom, sSubject, bHTML, sBody) {
    $.ajax({
        url: "../tools/sendmail.aspx",
        async: false,
        type: "POST",
        data: { to: sTo, from: sFrom, subject: sSubject, html: (bHTML ? '1' : '0'), body: sBody }
    });
}
function fnGoToPage(PageId) {
    var Page = _Player.Player.FindPage(null, PageId);
    if (!_Player.Player.Preview && Page && Page.HasPageContent) _Player.Player.Load(null, PageId);
    else window.location = fnMetierUrl("player.aspx?PageID=" + PageId);        
}
function fnMetierUrl(sUrl) {
    sUrl = sUrl.split("#")[0];
    sUrl += (sUrl.indexOf("?") == -1 ? "?" : (sUrl.lastIndexOf("&") == sUrl.length - 1 ? "" : "&"));
    $.each(["header", "demo", "preview", "CourseID", "RCOID"], function (i, s) {
        if (sUrl.indexOf(s) == -1 && (getQuerystring(s)))
            sUrl += s + "=" + getQuerystring(s) + "&";
    });
    sUrl = stripEndingStr(sUrl, "&", "?");    
    return sUrl;
}
function fnShouldAddToNav(ctrl) {
    return $(ctrl).closest("#references-dialog").length == 0 && ($(ctrl).closest("._mode-popup, ._mode-slidedown").length == 0 || $(ctrl).closest("._editing").length > 0);
}
function fnShowContactUs() {
    fnShowMessage("<b>" + _Translations["contact-heading"] + "</b><br />" + _Translations["contact-body"] + "<br /><br /><textarea style='width:350px;height:100px'></textarea>", "<div class='left actionbutton'><a href='javascript:void(0)'>" + _Translations["button-send"] + "</a></div><div class='right cancelbutton'><a href='javascript:void(0)'>" + _Translations["button-cancel"] + "</a></div>").find(".cancelbutton a").click(function () { $("#metier-msg").detach(); }).end().find(".actionbutton a").click(function () { var s = trim($("#metier-msg textarea").val()); if (s == "") { } else { var e = cStr((_Player.SCORM.API ? cStr(_Player.SCORM.Win.LessonData.EcoachEmail, _Player.SCORM.Win.LessonData.DistributorEmail) : ""), "kurssporsmal@metier.no"); fnSendEmail(e, (_Player.SCORM.API ? _Player.SCORM.Win.LessonData.UserEmail : "kurssporsmal@metier.no"), "Question from course participant! Lesson: " + _Player.SCORM.Lesson.Title + " / Course: " + _Player.Player.Course.Name, false, "Participant: " + (_Player.SCORM.API ? _Player.SCORM.Win.LessonData.UserEmail : "N/A") + "\nQuestion: " + s); fnShowMessage("<b>" + _Translations["contact-heading"] + "</b><br /><br />" + _Translations["contact-success"]); } });
}
function clsScorm(player) {
    var c_VariableSep = "||";
    var c_KeyValueSep = ":";
    this._search = function (win) {
        if (player.Preview) return false;
        while ((!win.API_1484_11 && !win.API) && win.parent && win.parent != win) win = win.parent;
        if (win.API_1484_11 || win.API) {
            this.Win = win;
            this.API = (win.API_1484_11 ? win.API_1484_11 : win.API);
            this.ScormMode = (win.API_1484_11 ? "3" : "C");
            if (this.ScormMode == "3") {
                this.API.LMSInitialize = this.API.Initialize;
                this.API.LMSFinish = this.API.Terminate;
                this.API.LMSGetValue = this.API.GetValue;
                this.API.LMSSetValue = this.API.SetValue;
                this.API.LMSGetAutoCommit = this.API.GetAutoCommit;
                this.API.LMSSetAutoCommit = this.API.SetAutoCommit;
                this.API.LMSCommit = this.API.Commit;
                this.API.LMSGetLastError = this.API.GetLastError;
                this.API.LMSGetErrorString = this.API.GetErrorString;
                this.API.LMSGetDiagnostic = this.API.GetDiagnostic;
                this.APIFields = ["cmi.session_time", "cmi.location", "cmi.score.raw", "cmi.suspend_data", "cmi.completion_status", "cmi.entry", "cmi.learner_name", "cmi.learner_id", "cmi.credit", "cmi.total_time"];

            } else {
                this.APIFields = ["cmi.core.session_time", "cmi.core.lesson_location", "cmi.core.score.raw", "cmi.suspend_data", "cmi.core.lesson_status", "cmi.core.entry", "cmi.core.student_name", "cmi.core.student_id", "cmi.core.credit", "cmi.core.total_time"];
                this.SuspendDataMax = 4000;
            }
            return true;
        }
        return false;
    };
    this._find = function (IgnoreSearched) {
        if (IgnoreSearched || !this.Searched) {
            this.Searched = true;
            this.API = null;
            if (window.parent && window.parent != window) this._search(window.parent);
            if (this.API == null && window.top.opener) this._search(window.top.opener);
            if (this.API == null && window.LessonData) this.Win = window;
        }
        return this.API;
    };
    this.reset = function () {
        this.API = null;
        this.Win = null;
        this.Searched = false;
        this.SuspendDataMax = 64000;
        this.SuspendData = {};
        this._find();
    };
    this.setSuspendDataString = function (Data) {
        this.SuspendDataString = Data;
        this.SuspendData = {};
        if (cStr(this.SuspendDataString) != "") {
            var oArr = this.SuspendDataString.split(c_VariableSep);
            for (var x = 0; x < oArr.length; x++) {
                var i = oArr[x].indexOf(c_KeyValueSep);
                this.setSuspendData(oArr[x].substr(0, i), decodeURI(oArr[x].substr(i + 1)));
            }
        }
    };
    this.setSuspendData = function (sKey, sVal) { this.SuspendData[sKey] = sVal; };
    this.clearSuspendData = function (sKey) { for (s in this.SuspendData) { if (sKey == null || sKey == s || (sKey.lastIndexOf("*") == sKey.length - 1 && s.indexOf(sKey.substring(0, sKey.length - 1)) == 0)) this.SuspendData[s] = null; } };
    this.getSuspendData = function (sKey) { if (sKey != null && sKey.lastIndexOf("*") != sKey.length - 1) return this.SuspendData[sKey]; var o = []; for (s in this.SuspendData) { if (sKey == null || sKey == s || (sKey.lastIndexOf("*") == sKey.length - 1 && s.indexOf(sKey.substring(0, sKey.length - 1)) == 0)) { o.push([s, this.SuspendData[s]]); o[s] = this.SuspendData[s]; } } return o; };

    this.init = function () {
        this.API.LMSInitialize("");        
                                    
        this.CurrentValues = [];
        for (var x = 0; x < 10; x++)
            this.CurrentValues[x] = this.API.LMSGetValue(this.APIFields[x]);

        //In player.js, we use the single character statuses: P, F, C, I, B, N
        this.CurrentValues[4] = replaceAll(this.CurrentValues[4], "passed", "P", "failed", "F", "completed", "C", "incomplete", "I", "browsed", "B", "not attempted", "N");

        this.StudentName = this.CurrentValues[6];
        this.StudentID = this.CurrentValues[7];
        this.Credit = this.CurrentValues[8];
        this.TotalTime = this.CurrentValues[9];
        this.LessonStatus = replaceAll(this.CurrentValues[4],"N","I");
        this.Score = this.CurrentValues[2];
        this.Entry = this.CurrentValues[5];
        this.setSuspendDataString(this.CurrentValues[3]);
        this.API.LMSSetAutoCommit(false);
    };
    /*
    this.CreateNewScormSessionIfTerminated=function(){
        if(this.API.LMSGetLastError()==104)
            return this.CreateNewScormSession();
        
        return false;
    }
    */
    this.CreateNewScormSession = function (LessonId, PageId) {
        if (!this.CreateNewScormSessionFailure) {
            if (LessonId || PageId) {
                var lesson = _Player.Player.GetSCORMLesson(LessonId, PageId);
                if (lesson) _Player.SCORM.Lesson = lesson;
            }
            _Player.SCORM.Win.fnMetierPlayRCO(_Player.SCORM.Win.iClassroomID, _Player.SCORM.Lesson.RCOID, null, 0, _Player.SCORM.Lesson.ScormMode, true)
            this._find(true);
            if (!this.API || this.API.LMSInitialize("") != "true"){
                this.CreateNewScormSessionFailure = true;
                alert("Your data could not be saved. This is probably because you've been logged out.\n\nPlease try clicking 'Save & Close' in the upper right corner, and re-opening the course player again.");                
            }
        }
        return !this.CreateNewScormSessionFailure;
    }
    this._SetSCORMValues = function () {
        var maps = [[this.Score, 2], [this.SuspendDataString, 3], [this.LessonStatus, 4]];
        for (var x = 0; x < maps.length; x++) {
            var map = maps[x];
            if (map[0] && cStr(map[0]) != cStr(this.CurrentValues[map[1]])) {
                if (this.API.LMSSetValue(this.APIFields[map[1]], map[0]) == "false") return false;
                else this.CurrentValues[map[1]] = map[0];
            }
        }
        return true;
    }
    this.writeValues = function () {
        this.SuspendDataString = "";

        // Suspend data - high priority items
        for (sKey in this.SuspendData) {
            if (sKey != null && (sKey.indexOf("quiz") != -1 || sKey.indexOf("answer") != -1)) {
                var sVal = cStr(this.SuspendData[sKey]);

                if (sVal.length > 0 && sVal != "undefined") {
                    var sItem = sKey + c_KeyValueSep + encodeURI(sVal) + c_VariableSep;
                    if ((this.SuspendDataString.length + sItem.length) < this.SuspendDataMax) this.SuspendDataString += sItem;
                }
            }
        }

        // Suspend data - lower priority items
        for (sKey in this.SuspendData) {
            if (sKey != null && (sKey.indexOf("quiz") == -1 && sKey.indexOf("answer") == -1)) {
                var sVal = cStr(this.SuspendData[sKey]);
                if (sVal.length > 0 && sVal != "undefined") {
                    var sItem = sKey + c_KeyValueSep + encodeURI(sVal) + c_VariableSep;
                    if ((this.SuspendDataString.length + sItem.length) < this.SuspendDataMax) this.SuspendDataString += sItem;
                }
            }
        }

        this.SuspendDataString = this.SuspendDataString.substr(0, this.SuspendDataString.length - 2);

        if (this.APIFields) {
            if(!this._SetSCORMValues()){
                this.CreateNewScormSession();
                if(!this._SetSCORMValues())
                    fnShowMessage("<h2>Your data could not be saved.</h2><br />This is probably because you've been logged out.<br /><br />Please try clicking 'Save & Close' in the upper right corner, and re-opening the course player again.", "<div class='middle actionbutton acknowledge'><a>OK</a></div>");
            }                         

            this.API.LMSCommit();
        }
    };
    this.reset();
}
function clsDictionary(_Words, _Dictionary) {
    $.expr[':'].noparents = function (a, i, m) {
        return $(a).parents(m[3]).length < 1;
    };
    _Words = _Words.filter(":noparents(.no-dict)");
    function fnReplaceStr(s, pairs) {
        for (var x = 0; x < pairs.length; x = x + 2) s = s.replace(new RegExp(pairs[x], 'gi'), pairs[x + 1]);
        return s;
    }
    function fnRemoveNonChars(s) {
        return removeXChars(s, "abcdefghijklmnopqrstuvwxyzæøå0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ");
    }
    //Returns {"V":lower case cleaned, "O": Original word, "C": is case sensitive dictionary word, "E": is exact dictionary word}
    function fnSingularize(s, c, dictionaryEntry) {//s=string, c=caseSensitive
        if (s.O != null) return s;
        var o = { O: cStr(s), C: 0, V: "", W: function () { if (!this._W) this._W = fnRemoveNonChars(this.O); return this._W; } };
        if (!s) return o;
        if (!dictionaryEntry) s = fnReplaceStr(s, ["!", " "]);
        s = fnReplaceStr(s,['"'," ","\\.", " ", ",", " ", "-", " ", "\\?", " ", " a ", "", " the ", "", " en ", "", " et ", "", " ei ", "", " har ", "", " å ", ""]);        
        s = $.trim(s);
        if (!s) return "";
        if (s.indexOf(" ") > -1) {
            var t = s.split(" ");
            $.each(t, function (i, o) { s = (i == 0 ? "" : s) + fnSingularize(this, c, dictionaryEntry).V; });
        } else {
            s = " " + s + " ";
            R = ["'s ", " ", "’s ", " ", "ene ", "", "et ", "", "er ", "", "t ", "", "e ", "", "en ", "", "s ", "", "ies ", "y "];
            for (var x = 0; x < R.length; x = x + 2) s = s.replace(new RegExp(R[x], 'gi'), R[x + 1]);                
            s = fnRemoveNonChars(s);
        }
        o.V = (c ? s : s.toLowerCase());
        if (dictionaryEntry) {
            var markers = ["^", "!"];//^=Case Sensitive, !=Exact match
            var markerAttributes = ["C", "E"];
            var index = 0;
            while (index > -1 && o.O.length > 1) {
                index = markers.indexOf(o.O.substring(0, 1));
                if (index > -1) {
                    o[markerAttributes[index]] = 1;
                    o.O = o.O.substring(1);
                }
            }
        }
        return o;
    }
    var start = new Date();

    var _Singularized = [];
    for (var x = 0; x < _Dictionary.length; x++) _Dictionary[x][0] = fnSingularize(_Dictionary[x][0],0,true);
    for (var x = 0; x < _Words.length; x++) {
        var s=$.trim(cStr(_Words[x].innerText, _Words[x].textContent));
        if (s) {
            s = s.split(String.fromCharCode(160)).join(" ").split(" ");//Replace &nbsp;
            if (s.length == 1) {
                var o = fnSingularize(s[0], 0, false);
                o.Word = _Words[x];
                _Singularized.push(o);
            } else $.each(s, function () {
                var o = fnSingularize(this, 0, false);
                o.Word = _Words[x];
                _Singularized.push(o);                
            });
        }
    }
    for (var x = 0; x < _Singularized.length; x++) {
        for (var y = 0; y < _Dictionary.length; y++) {
            var Match = _Singularized[x].V;
            var Original=_Singularized[x].O;
            if (Match) {
                var Counter = 0, WordsMatched=0;
                while (_Dictionary[y][0].V.indexOf(Match) == 0) {
                    if (_Dictionary[y][0].V == Match) {
                        WordsMatched = Counter;
                        while (Counter >= 0) {                           
                            if ($(_Words[x + Counter]).parents(".title").length == 0
                               && (!_Dictionary[y][0].C || fnSingularize(_Dictionary[y][0].O, 1).V == fnSingularize(_Singularized[x].O, 1).V)
                               && (!_Dictionary[y][0].E || _Dictionary[y][0].W().toLowerCase() == _Singularized[x].W().toLowerCase())
                               ) {                                
                                if (!(_Dictionary[y][0].E && _Dictionary[y][0].C && _Dictionary[y][0].W() != _Singularized[x].W())) {
                                    $(_Singularized[x + Counter].Word).data("dict-index", y).wrap("<span class='dict-entry'></span>");
                                }
                            }
                            Counter--;
                        }                       
                        x += WordsMatched;
                        break;
                    } else {
                        var v = _Singularized[x + (++Counter)];
                        if (!v) break;
                        Match += v.V;
                    }
                }
            }
        }
    }
    var _DisplayTimer;
    $(".dict-entry").bind("mouseover", function (e) {
        if (_DisplayTimer) clearTimeout(_DisplayTimer);
        var fnShowDict = function (Element) {
            var D = $(".dict-popup");            
            D.detach();
            var E = $("<div class='dict-popup'><div class='frame'>" + (fnIsTouchDevice() ? "<div class='close' />" : "") + "<div class='lt'></div><div class='lb'></div><div class='rt'></div><div class='rb'></div><div class='top'></div><div class='bottom'></div><div class='left'></div><div class='right'></div><div class='trunk'></div>" + _Dictionary[Element.data("dict-index")][1] + "</div></div>").appendTo($(document.body));
            E.css("left", Element.offset().left - 140 + Element.width() / 2).css("top", Element.offset().top - 30 - E.height()).data("curr-dict-index", Element.data("dict-index")).find(".close").click(function () { $(".dict-popup").detach(); });
        };
        var Word = ($(e.target).is(".dict-entry") ? $(e.target).find(".word") : ($(e.target).is(".word") ? $(e.target) : null));
        if (Word) fnShowDict(Word);
    }).bind("mouseout", function () {
        $(".dict-popup").detach();
    });
    //alert("Page load took " + (new Date() - start) + "milliseconds");    
}

function fnDisplayUrl(url, title, settings) {
    if (!settings) settings = {};
    if (title) settings.title = title;
    settings = $.extend({}, { modal: true, hide: 'fold', show: { effect: 'slide', direction: "up" }, width: 930, height: 510, resizable: true }, settings);
    $("#references-dialog").remove();
    var tag = $("<div id='references-dialog' style='padding:0;margin:0' />");
    tag.html($("<iframe style='width:100%;height:100%;' frameborder='0' />").load(function () {
        $(this).contents().find(".wrapper.main, .wrapper.main-text, .wrapper.main-wide, .wrapper.empty").css("margin-left", 50);
    }).attr("src", url)).dialog(settings).dialog('open');
}
function fnDisplayContent(id, title, settings) {
    fnDisplayUrl("/learningportal/player/player.aspx?PageID=" + id + "&header=hide&preview=true", title, settings);
}
function fnDisplayDialog(html, settings) {
    settings = $.extend({}, { modal: true, hide: 'fold', show: { effect: 'slide', direction: "up" }, width: 830, height: 510, resizable: true }, settings);
    $("#references-dialog").remove();
    var tag = $("<div id='references-dialog'>Loading...</div>");        
    tag.html(html).dialog(settings).dialog('open');
    _Player.Player.PrepareContent(tag);
}
