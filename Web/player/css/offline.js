﻿var appCache = window.applicationCache;

if (appCache) {

    var cacheStates = ["UNCACHED (numeric value 0) -- The ApplicationCache object's cache host is not associated with an application cache at this time.",
    "IDLE (numeric value 1) -- The ApplicationCache object's cache host is associated with an application cache whose application cache group's update status is idle, and that application cache is the newest cache in its application cache group, and the application cache group is not marked as obsolete.",
    "CHECKING (numeric value 2) -- The ApplicationCache object's cache host is associated with an application cache whose application cache group's update status is checking.",
    "DOWNLOADING (numeric value 3) -- The ApplicationCache object's cache host is associated with an application cache whose application cache group's update status is downloading.",
    "UPDATEREADY (numeric value 4) -- The ApplicationCache object's cache host is associated with an application cache whose application cache group's update status is idle, and whose application cache group is not marked as obsolete, but that application cache is not the newest cache in its group.",
    "OBSOLETE (numeric value 5) -- The ApplicationCache object's cache host is associated with an application cache whose application cache group is marked as obsolete."];

    

    function logEvent(s) {
        var win = $("#async-msg");
        if (win.length == 0) $("<div id='async-msg' style='width:200px;padding:10px;min-height:20px;background-color:white;border:3px solid red' />").appendTo(document.body);
        win.html(s);
    }

    $(appCache).bind("checking", function (event) {
        logEvent("Checking for manifest");
    }
    );

    // This gets fired if there is no update to the manifest file
    // that has just been checked.
    $(appCache).bind("noupdate",function (event) {
        logEvent("No cache updates");
    }
    );

    // This gets fired when the browser is downloading the files
    // defined in the cache manifest.
    $(appCache).bind("downloading",function (event) {
        logEvent("Downloading cache");
        // Get the total number of files in our manifest.
        //getTotalFiles();
    }
    );

    // This gets fired for every file that is downloaded by the
    // cache update.
    $(appCache).bind("progress",function (event) {
        logEvent("File downloaded");
        // Show the download progress.
        //displayProgress();
        }
    );

    // This gets fired when all cached files have been
    // downloaded and are available to the application cache.
    $(appCache).bind("cached",function (event) {
        logEvent("All files downloaded");
    }
    );

    // This gets fired when new cache files have been downloaded
    // and are ready to replace the *existing* cache. The old
    // cache will need to be swapped out.
    $(appCache).bind("updateready",function (event) {
        logEvent("New cache available");
        // Swap out the old cache.
        //appCache.swapCache();
    }
    );

    // This gets fired when the cache manifest cannot be found.
    $(appCache).bind("obsolete",function (event) {
        logEvent("Manifest cannot be found");
        }
    );

    // This gets fired when an error occurs
    $(appCache).bind("error",function (event) {
        logEvent("An error occurred");
        }
    );

    var events = "checking,error,noupdate,downloading,progress,updateready,cached,obsolete".split(',');
    function updateCacheStatus() {
        document.querySelector('#status').innerHTML = cacheStates[window.applicationCache.status];
    }
}