﻿//Question.Position - refers to position in Quiz.Questions[] array
//Question.Index refers to position in Xml -- this should only be used when saving and loading answers

function clsQuiz(Settings, Section, Player) {
    //Our own, private random number function
    var seed = Math.random();
    function QuizRandom() {
        var x = Math.sin(seed++) * 10000;
        return x - Math.floor(x);
    }

    Settings = $.extend(true, { MaxFinalTestAttempts: 3, Inline: Settings.FinalTest, Sequential: false, ShowAnswersScore: 0 }, Settings);
    if (Settings.FinalTest) Settings = $.extend(true, {}, Settings, { ShowAnswersScore: 101 });
    var _Quiz = this;
    var _Section = $(Section);
    Settings.Quiz = this;
    Settings.QuestionOffset = cInt(Settings.QuestionOffset);   
    $.extend(this, { Settings: Settings, Player: Player, Initialize: fnInitialize, Questions: [], render: fnRenderQuestion, save: fnSaveQuestion });

    this.CanGotoQuestion = function (Index) {
        if (!this.Settings.Sequential || Index == 0 || this.Settings.FinalTest) return this.Questions[Index]!=null;
        return (this.Questions[Index] && this.Questions[Index - 1].IsCompleted());
    }

    if (Settings.KeyMode == null) Settings.KeyMode = false;
    if (window.location.href.indexOf("&answers=true") > -1) this.Settings.KeyMode = true;    
    this.Menu = $(".quiz-menu");
    if (this.Menu.length == 0) this.Menu = $('<div class="container quiz-menu"><ul></ul>' + (this.Settings.FinalTest ? '' : '<ul class="tools"><li class="status"><div class="bar"></div><div class="text"><strong>' + _Translations["total-correct"] + '</strong>&nbsp;&nbsp;<span>17%</span></div></li></ul>') + '</div>').appendTo($("#wrapper-submenu"));

    var Data = this.Settings.Data;
    console.log("Loading Data", Data);
    this.AlertFinalTest=function(){    
        var Resume = "", NextQuiz = 0, Score=0;
        $(Data).each(function (i, o) {
            if (o.Answers) {
                Resume += "<li><a class='resume_" + i + "' href='javascript:void(0)'>" + replaceAll(_Translations["show-attempt"], "{0}", (i + 1), "{1}", o.Score) + "</a></li>";
                if (i == NextQuiz) NextQuiz++;
                if (Score < o.Score) Score = o.Score;
            }
        });
        this.Settings.Alerted = true;
        if (NextQuiz < Data.length) this.Data = $($.parseXML(Data[NextQuiz].Xml));
        var IsDone = NextQuiz > this.Settings.MaxFinalTestAttempts - 1;
        if (this.Settings.FinalTestPassingScore > 0 && this.Settings.FinalTestPassingScore <= Score) IsDone = true;
        var Box = fnShowMessage(
            replaceAll(_Translations["finaltest-" + (IsDone ? "done" : "welcome")], "{0}", (this.Data == null ? "" : this.Data.find("question").length), "{1}", this.Settings.MaxFinalTestAttempts)
            + (this.Settings.FinalTestPassingScore > 0 ? replaceAll(_Translations["finaltest-minscore"], "{0}", this.Settings.FinalTestPassingScore) : "")
            + (IsDone && this.Settings.FinalTestPassingScore > 0 ? replaceAll(_Translations["finaltest-minscore-" + (Score >= this.Settings.FinalTestPassingScore ? "pass" : "fail")], "{0}", Score) : "")
            + (Resume ? "<ul>" + Resume + "</ul>" : ""),
            [(IsDone ? null : [_Translations["finaltest-start"], function () { Settings.KeyMode = false; Settings.Index = NextQuiz; _Quiz.Initialize(); }]), [_Translations["finaltest-exit"], function ()
            {
                if (window.parent.fnRestoreNavigator) window.parent.fnRestoreNavigator();
                else window.parent.opener.fnRestoreNavigator();
            }]], { NoClose: true });
        $(Data).each(function (i, o) {
            if (this.Answers) Box.find(".resume_" + i).click(function () { Settings.KeyMode = true; Settings.Index = i; _Quiz.Initialize(); $("#metier-msg").detach(); });
        });        
    }
    if (this.Settings.FinalTest && !this.Settings.Alerted) {
        this.AlertFinalTest();
        return;
    }
    this.Initialize();
    function fnInitialize() {
        this.Questions = [];
        this.CurrentQuestion = "";
        if (cStr(Settings.Index) == "") Settings.Index = 0;
        if (Player) {
            $.each(Player.LessonQuizes, function (i, o) {
                if (o.Object.ObjectID == _Quiz.Settings.Data[_Quiz.Settings.Index].QuizID) _Quiz.LessonQuiz = o;
            });
        }
        Settings.Answers = Data[Settings.Index].Answers;
        try {
            this.Data = $($.parseXML(replaceAll(Data[Settings.Index].Xml, "&quot;", "\"")));
        } catch (e) { this.Data = $($.parseXML("<node />")); }
        
        var oQuestions = this.Data.find("question");
        var s = cStr(Settings.Answers);
        if (s.indexOf(";score:") > -1) s = s.split(";score:")[0] + s.substring((s + ";").indexOf(";", s.split(";score:").length));
        if (s.indexOf("q") == 0) s = s.substring(s.indexOf(";") + 1);
        //s = "2§1§5;§0;2§1";
        Settings.Answers = s.split(";");
        for (var x = 0; x < oQuestions.length; x++) {
            var oQuestion = new clsQuestion(oQuestions[x], this, x, Settings.Answers[x]);
            this.Questions.push(oQuestion);
        }
        if (this.Settings.RandomizeQuestions) {
            this.Questions = this.Questions.sort(function () {
                return Math.round(QuizRandom()) - 0.5;
            });
        }
        for (var x = 0; x < this.Questions.length; x++) {
            this.Questions[x].Position = x;
            this.render(x, !this.Settings.Inline);
            this.save(x, true, false);
        }

        if (this.Settings.Inline && this.Questions.length > 0) this.render(0, !this.Settings.Inline);
        if (this.Settings.FinalTest && !this.Settings.KeyMode && _Translations["finaltest-end-prompt"])
            $(window).on('beforeunload.finaltest', function () {
                return _Translations["finaltest-end-prompt"];
            });
    }
    function fnSaveQuestion(iIndex, bScoreOnly, bDisplayKey) {
        if (bDisplayKey == null) bDisplayKey = false;
        if (iIndex == null) {
            iIndex = this.CurrentQuestion;
            if (bScoreOnly == null) bScoreOnly = true;
        }
        if (cStr(iIndex) == "") return;
        if (this.Settings.KeyMode) bDisplayKey = true;
        var oQuestionObject = this.Questions[iIndex];
        var oQuestion = _Section.find(".question-" + iIndex);
        var iScore = oQuestionObject.Score, sAnswer = oQuestionObject.Answer;
        if (!oQuestionObject.KeyMode && !bScoreOnly) oQuestionObject.CheckAnswerCounter++;
        if (oQuestion.length > 0) {
            sAnswer = "";
            iScore = 0;
            var iNumCorrect = oQuestion.find(".bottomtext input.c[type='checkbox']").length;
            $.each(oQuestion.find(".bottomtext input[type='checkbox']"), function (i, o) {
                if ($(o).is(":checked")) iScore += ($(o).hasClass("c") ? 1 : -1);
                $(o).parent().toggleClass("key",bDisplayKey).toggleClass("correct", bDisplayKey && $(o).hasClass("c"));
            });
            if (oQuestionObject.ProblemType.MultipleChoice) {
                var Answers = oQuestionObject.Xml.find("answer");
                oQuestion.find(".question").each(function (i, o) {
                    var questionIndex = $(o).data("index");
                    $(o).toggleClass("key", bDisplayKey && $(Answers[questionIndex]).attr("correct") == "yes");
                    if ($(o).hasClass("selected")) {
                        iScore += ($(Answers[questionIndex]).attr("correct") == "yes" ? 1 : -1);
                        sAnswer += (sAnswer == "" ? "" : ",") + questionIndex;
                    }
                    if ($(Answers[questionIndex]).attr("correct") == "yes") iNumCorrect++;
                });                
                iScore = (iNumCorrect == 0 ? (oQuestionObject.CheckAnswerCounter == 0 || iScore!=0 ? 0 : 100) : Math.max(0, iScore) * 100 / iNumCorrect);
            } else if (oQuestionObject.ProblemType.ObjectPositioning) {
                var Drags = oQuestionObject.Xml.find("drag");
                var CorrectDrags = $.grep(Drags, function (o) { return cStr($(o).attr("correct")) != ""; }).length;
                var Drops = oQuestionObject.Xml.find("target");
                $(Drags).each(function (i) {
                    var Element = oQuestion.find(".question").filter(function () { return $(this).data("index") == i; });                    
                    var pos = { y: $(Element).position().top - oQuestionObject.VShiftCalc(), x: $(Element).position().left - oQuestionObject.HShiftCalc(), w: $(Element).width(), h: $(Element).height() }                    
                    var center = { x: (pos.w / 2) + pos.x, y: (pos.h / 2) + pos.y };
                    var c = $(this).attr("correct");
                    var bCorrect = false;
                    if (!c) {
                        bCorrect = (pos.y < 0 && oQuestionObject.Xml.find("drags").attr("align") == "top" || pos.x < -10);
                    } else {
                        var target = oQuestionObject.Xml.find("target[id='" + c + "']");
                        var rect = { x: cInt($(target).attr("x")), y: cInt($(target).attr("y")), w: cInt($(target).attr("width")), h: cInt($(target).attr("height")) };
                        bCorrect = center.x > rect.x && center.y > rect.y && center.x < rect.x + rect.w && center.y < rect.y + rect.h;
                    }                    
                    iScore += (bCorrect ? 1 : -1);                    
                    sAnswer += Element.data("index") + ":" + pos.x + "x" + pos.y + ",";

                    if (oQuestionObject.KeyMode != bDisplayKey || _Quiz.Settings.FinalTest) {
                        Element.toggleClass("key", bDisplayKey).toggleClass("incorrect", bDisplayKey && !bCorrect);
                        if (bDisplayKey) {
                            Element.data("area", $("<div style='border:4px solid green;position:absolute;left:" + (rect.x + oQuestionObject.HShiftCalc()) + "px;top:" + (rect.y + oQuestionObject.VShiftCalc()) + "px;width:" + rect.w + "px;height:" + rect.h + "px;'></div>").appendTo(oQuestion.find(".quiz-DD")));
                        } else {
                            $(Element.data("area")).detach();
                        }
                        
                        if ((_Quiz.Settings.KeyMode && _Quiz.Settings.CMSMode)) Element.removeClass("incorrect");
                    }
                });
                iScore = (CorrectDrags == 0 ? 0 : iScore / CorrectDrags) * 100;
            } else if (oQuestionObject.ProblemType.DragAndDrop) {                
                var Drags = oQuestionObject.Xml.find("drag");
                var CorrectDrags = $.grep(Drags, function (o) { return cStr($(o).attr("correct")) != ""; }).length;
                var Drops = oQuestionObject.Xml.find("target");
                var GetCurrentMultiExpression = function (exp) {
                    exp = exp.split("multi:").join("").split("|");
                    for (var x = 0; x < exp.length; x++) {
                        var f = function (id) { return oQuestion.find(".answer").filter(function () { return $($(this).data("node")).attr("id") == id; })[0]; };
                        var c = f(replaceAll(exp[x].split("=")[0], "[", "", "]", ""));
                        if (c && $(c).find(".question:contains(" + exp[x].split("=")[1].split(">")[0] + ")").length > 0) {
                            return exp[x].split(">")[1];
                        }
                    }
                };
                
                $(Drags).each(function (i) {
                    var Element = oQuestion.find(".question").filter(function () { return $(this).data("index") == i; });
                    var Container = Element.parents(".answer")[0];
                    var Drag = this;
                    var bCorrect = false;
                    if (Container != null) {
                        var exp = $(Drag).attr("correct");
                        if (exp && exp.indexOf("multi:") == 0) {
                            exp = GetCurrentMultiExpression(exp);
                            //jvt
                            bCorrect = cStr(exp).indexOf("[" + $(Drops[$(Container).data("index")]).attr("id") + "]") > -1;
                        } else {
                            bCorrect = $(Drops[$(Container).data("index")]).attr("id") == exp;//.hasClass(exp);
                        }
                        iScore += (bCorrect ? 1 : -1);
                        sAnswer += Element.data("index") + ":" + $(Container).data("index") + ",";
                    }
                    if (oQuestionObject.KeyMode != bDisplayKey || _Quiz.Settings.FinalTest) {                        
                        Element.toggleClass("key", bDisplayKey).toggleClass("incorrect", bDisplayKey && !bCorrect);
                        if (!bCorrect || Element.data("previous-container") != null) {
                            if (bDisplayKey) {//move to correct container
                                Element.data("previous-container", Element.parent());
                                var c = $(Drag).attr("correct");
                                if (c && c.indexOf("multi:") > -1) {
                                    var exp = cStr(GetCurrentMultiExpression(c)).split("],[");
                                    for (var x = 0; x < exp.length; x++) {
                                        c = _Section.find(".answer_" + iIndex + "_" + replaceAll(exp[x], "[", "", "]", ""));
                                        if (c.parents(".answer").find(".question").length == 0 || x == exp.length - 1) {
                                            Element.detach().appendTo(c.parents(".answer"));
                                            break;
                                        }
                                    }
                                } else {
                                    Element.detach().appendTo(_Section.find(".answer_" + iIndex + "_" + c).parents(".answer"));
                                }
                            } else {//move back
                                Element.detach().appendTo(Element.data("previous-container")).data("previous-container", null);
                            }
                        }
                        if ((_Quiz.Settings.KeyMode && _Quiz.Settings.CMSMode)) Element.removeClass("incorrect");
                    }
                });
                iScore = (CorrectDrags == 0 ? 0 : iScore / CorrectDrags) * 100;
                oQuestionObject.AnswerRecalc();
            } else if (oQuestionObject.ProblemType.FillInTheBlanks) {
                //For blank fill-in areas, we score -1 when they are filled in. 0 when they are blank
                oQuestion.find(".answers .answer").each(function () {                    
                    if ($(".question", this).length > 0)
                        iScore += ($(".question", this).text().toLowerCase() == $(this).data("answer").toLowerCase() ? ($(this).data("answer")=="" ?0:1) : -1);
                    
                    sAnswer += $(".question", this).text() + "|";
                });
                if (oQuestion.find(".question").length > 0)
                    iScore = iScore * 100 / oQuestion.find(".answer").filter(function () { return $(this).data("answer") != "";}).length;

                if (oQuestionObject.KeyMode != bDisplayKey || _Quiz.Settings.FinalTest) {
                    if (!bDisplayKey) {
                        oQuestion.find(".question.key").detach();
                        oQuestion.find(".answer:not(:has(.question))").removeClass("full");
                        oQuestion.find(".answer").removeClass("correct incorrect");
                    } else if (oQuestion.find(".answers .answer .question.key").length == 0) {
                        oQuestion.find(".answers .answer").each(function () {
                            var correct = $(".question", this).text().toLowerCase() == $(this).data("answer").toLowerCase();
                            $("<span class='question key' />").html($(this).data("answer")).appendTo(this);

                            $(this).addClass("full " + (correct ? "correct" : "incorrect"));
                        });
                    }
                }

            } else if (oQuestionObject.ProblemType.ConnectTheBoxes) {
                if (oQuestionObject.jsPlumb) {
                    var _jsPlumb = oQuestionObject.jsPlumb;
                    var processed = {};
                    oQuestion.find(".questions .question").each(function (i, q) {
                        var conn = [];
                        _jsPlumb.select({ source: q }).each(function (o) { conn.push(o); });
                        _jsPlumb.select({ target: q }).each(function (o) { conn.push(o); });
                        $.each(conn, function (i, o) {
                            if (!processed[o.id]) {
                                var id1 = $(o.source == q ? o.source : o.target).data("boxid"),
                                    id2 = $(o.source == q ? o.target : o.source).data("boxid");
                                if (id1 != null && id2 != null) {
                                    iScore += (oQuestionObject.Xml.find("connection[id1=" + id1 + "][id2=" + id2 + "]").length + oQuestionObject.Xml.find("connection[id1=" + id2 + "][id2=" + id1 + "]").length > 0 ? 1 : -1);
                                    sAnswer += id1 + ":" + id2 + ",";
                                    processed[o.id] = o;
                                }
                            }
                        });
                    });
                    if (oQuestionObject.Xml.find("correctconnections connection").length > 0)
                        iScore = iScore * 100 / oQuestionObject.Xml.find("correctconnections connection").length;

                    if (oQuestionObject.KeyMode != bDisplayKey) {
                        oQuestion.find(".question").each(function () {
                            _jsPlumb.removeAllEndpoints(this);
                        });
                        //_jsPlumb.deleteEveryEndpoint();
                        if (!bDisplayKey) {
                            oQuestionObject.CreateEndpoints();
                            oQuestionObject.LoadAnswers();

                        } else {
                            $.each(oQuestionObject.Xml.find("correctconnections connection"), function (index, node) {
                                var correct = cStr(oQuestionObject.Answer).indexOf($(node).attr("id1") + ":" + $(node).attr("id2")) > -1;
                                _jsPlumb.connect({
                                    source: oQuestion.find(".question").filter(function () { return $(this).data("boxid") == $(node).attr("id1"); })[0],
                                    target: oQuestion.find(".question").filter(function () { return $(this).data("boxid") == $(node).attr("id2"); })[0],
                                    paintStyle: { lineWidth: 10, strokeStyle: (correct ? '#72c028' : '#f56932') },
                                    endpointStyle: { fillStyle: (correct ? '#72c028' : '#f56932') },
                                    deleteEndpointsOnDetach: true
                                });
                            });
                        }
                    }
                } else {
                    sAnswer = oQuestionObject.Answer;
                }
            }
        }
        iScore = Math.round(Math.max(0, iScore));
        if (oQuestionObject.CheckAnswerCounter == 0 && !_Quiz.Settings.FinalTest) iScore = 0;
        if (!oQuestionObject.KeyMode) {            
            sAnswer += "§";
            oQuestion.find(".bottomtext input, .bottomtext textarea").each(function (i) {
                var a = ($(this).is("[type=checkbox]") ? ($(this).is(":checked") ? 1 : 0) : $(this).val());
                sAnswer += (i == 0 ? "" : "|") + a; oQuestionObject.Bottomtexts[i] = a;
            });
            sAnswer += "§" + oQuestionObject.CheckAnswerCounter + "§" + iScore + "§" + oQuestionObject.RemainingTime;

            oQuestionObject.Score = iScore;
            var changed = oQuestionObject.Answer != sAnswer;
            oQuestionObject.Answer = sAnswer;
            var iQuizScore = 0, Answers=[];
            $(_Quiz.Questions).each(function (i, o) { iQuizScore += o.Score; Answers[o.Index] = o.Answer; });

            var iPageScore = iQuizScore, iPageQuestions = _Quiz.Questions.length;
            
            if (_Quiz.Player) {
                $.each(_Quiz.Player.LessonQuizes, function (i, o) {
                    if (o.Object.ObjectID == _Quiz.Settings.Data[_Quiz.Settings.Index].QuizID) {
                        o.TotalScore = iQuizScore;
                        o.Weight = _Quiz.Questions.length;
                        o.Questions = _Quiz.Questions;
                    }else if (o.Object.Parent.ObjectID == _Quiz.Player.PageID) {
                        iPageScore += o.TotalScore;
                        iPageQuestions += o.Weight;
                    }
                });
            }
            iQuizScore = Math.round(iQuizScore / _Quiz.Questions.length);
            $.extend(_Quiz.Settings.Data[_Quiz.Settings.Index], { Answers: Answers.join(";"), Score: iQuizScore });            
            iPageScore = Math.round(iPageScore / iPageQuestions);
                        
            if (!this.Settings.FinalTest || bDisplayKey) {
                
                _Quiz.Menu.find(".status span").html(iPageScore + "%").end().find(".status .bar").css("width", iPageScore + "%");
                oQuestionObject.UpdateUI();                
            }
            oQuestionObject.Nav.find("span").html(this.Settings.FinalTest && !bDisplayKey ? "" : oQuestionObject.Score + "%").end().find(".bar").css("width", (this.Settings.FinalTest ? (sAnswer ? 100 : 0) : oQuestionObject.Score) + "%");
            if (changed) _Section.trigger("changed", [oQuestionObject, _Quiz, iQuizScore, iPageScore]);
        }

        oQuestionObject.KeyMode = bDisplayKey;
        oQuestion.toggleClass("keymode", bDisplayKey);
    }

    function fnRenderQuestion(iIndex, bAppend) {
        if (this.Settings.Inline && !this.Settings.KeyMode && cStr(this.CurrentQuestion) != "") this.save(this.CurrentQuestion, true);
        this.CurrentQuestion = iIndex;

        var ExistingQuestion = _Section.find(".question-" + iIndex);
        var PrevScroll = $(window).scrollTop();
        if (!bAppend && ExistingQuestion.length == 0) _Section.find(".wrapper.main").html("");
        var oQuestionObject = this.Questions[iIndex];
        if (cStr(oQuestionObject.Answer).indexOf("§") > -1) oQuestionObject.ParseAnswer(oQuestionObject.Answer);
        var oContainer = _Section.find(".question-template").clone().show()
            .find(".check-answers").click(function (e) {
                if ($(this).parents().is(".disabled")) return;
                _Quiz.save(iIndex);
                $(this).parent().find(".progressbar").css({ "opacity": "0", "background": "#f00" }).animate({ opacity: 1 }, 100).animate({ "backgroundColor": "#aab8c4" }, 1000);                
                oQuestionObject.UpdateUI();
                
            }).end()            
            .find(".display-key").mousedown(function (e) { if (!oQuestionObject.KeyMode && $(this).hasClass("disabled")) return; _Quiz.save(iIndex, false, !oQuestionObject.KeyMode); e.stopPropagation(); e.stopImmediatePropagation(); return false; }).end()
            .find(".anchor").attr("name", "quiz_" + (iIndex + this.Settings.QuestionOffset)).end()
            .find(".prev-button").click(function () { if (_Quiz.CurrentQuestion > 0 && !$(this).hasClass("disabled")) _Quiz.render(_Quiz.CurrentQuestion - 1, false); }).end()
            .find(".next-button").click(function () { if (_Quiz.CurrentQuestion < _Quiz.Questions.length - 1 && !$(this).hasClass("disabled")) _Quiz.render(_Quiz.CurrentQuestion + 1, false); var q = _Quiz.Questions[_Quiz.CurrentQuestion]; if (_Quiz.Settings.Inline && q.DisplayTimerMessage()) q.StartTimer(); }).end()
            .find(".new-attempt").click(function () { _Quiz.Settings.Alerted = false; $(".quiz-menu").detach(); _Quiz.AlertFinalTest(); }).end()
            .find(".submit-button").click(function () {
                if (_Quiz.Settings.KeyMode) {
                    _Section.trigger("save", []);
                    _Section.trigger("close", []);
                } else {
                    var Submit = function () {
                        if (_Quiz.Settings.FinalTest) $(window).off('beforeunload.finaltest');
                            
                        _Quiz.Settings.KeyMode = true;
                        $.each(_Quiz.Questions, function (i) { _Quiz.save(i); });

                        _Section.trigger("save", []);
                        var e = {};
                        _Section.trigger("submitted", e);
                        if (e.cancel) return;
                        _Quiz.render(0, false);

                        if (_Quiz.Settings.FinalTest) {
                            fnShowMessage(replaceAll(_Translations["test-submitted"], "{0}", _Quiz.Settings.Data[_Quiz.Settings.Index].Score, "{1}", _Quiz.Settings.MaxFinalTestAttempts));
                        }
                    };
                    if (_Quiz.Settings.FinalTest)
                        fnShowMessage(_Translations["submit-test-prompt"], [[_Translations["Yes"], function () { $("#metier-msg").detach(); Submit(); }], [_Translations["No"], function () { $("#metier-msg").detach(); }]], { NoAutoClose: true });
                    else
                        Submit();
                }
            }).end()
            .find(".reset").click(function () {
                if ($(this).parents().is(".disabled")) return;
                oQuestionObject.reset();
            }).end();
        if (ExistingQuestion.length > 0)
            ExistingQuestion.after(oContainer).detach();
        else
            oContainer.appendTo(_Section.find(".wrapper.main").append(iIndex == 0 || !bAppend ? "" : "<div><br /><br /><br /></div>"));        
        
        if (this.Settings.Inline) {                        
            if (this.Settings.FinalTest) {
                oContainer.find(".check-answers, .progressbar, .quiz-buttons").remove();
                if (_Quiz.Settings.KeyMode) oContainer.find(".submit-button").removeClass("submit-button").find(".text").html(_Translations["close-button"]);
                oContainer.find(".new-attempt").toggle(_Quiz.Settings.KeyMode && $.grep(_Quiz.Settings.Data, function (n, i) { return n.Answers == ""; }).length > 0);
                if (_Quiz.Settings.Index == _Quiz.Settings.MaxFinalTestAttempts - 1) oContainer.find(".new-attempt .text").html(_Translations["show-attempt-summary"]);
            } else {
                oContainer.find(".finaltest-buttons td:gt(0)").detach().end().find("td").css("border-top", "1px solid #d0d6dc");                
            }
        } else oContainer.find(".finaltest-buttons").remove();

        if (oQuestionObject.Nav == null) oQuestionObject.Nav = $('<li><a href="javascript:void(0)" class="roundbutton progress"><div class="bar"></div><div class="text"><strong>' + (iIndex + 1 + this.Settings.QuestionOffset) + '</strong>&nbsp;&nbsp;<span></span></div></a></li>').appendTo(_Quiz.Menu.find("ul:eq(0)")).find("a").click(function () { if (_Quiz.Settings.Inline && !$(this).hasClass("disabled")) { _Quiz.render(iIndex, false); } if(oQuestionObject.Container) _Quiz.Player.ScrollTo($(oQuestionObject.Container));  });
        var contentHeight = 0, version = Number(cStr(oQuestionObject.Xml.attr("version"), "1"));
        if (oQuestionObject.ProblemType.ConnectTheBoxes && version < 1.1) version = 1.1;

        var oQuestion = oContainer.attr("class", "quiz-outer question-" + iIndex + " m" + cStr(version).safeSplit(".", 0) + " v" + replaceAll(version, ".", "-")).find(oQuestionObject.ProblemType.MultipleChoice ? ".quiz-MC" : (oQuestionObject.ProblemType.DragAndDrop || oQuestionObject.ProblemType.ObjectPositioning ? ".quiz-DD" : (oQuestionObject.ProblemType.ConnectTheBoxes ? ".quiz-CL" : (oQuestionObject.ProblemType.FillInTheBlanks ? ".quiz-FB" : null))));
        oContainer.find(".quiz-area").children().each(function (i, o) {
            if (o != oQuestion[0]) $(o).remove();
        });
        oContainer.find(".counter").html(iIndex + 1 + this.Settings.QuestionOffset).end().find(".problem").html(oQuestionObject.Xml.find("questiontext").text()).end().mousedown(function (e) {
            if (oQuestionObject.KeyMode && !oQuestionObject.Quiz.Settings.FinalTest) { _Quiz.save(iIndex, false, !oQuestionObject.KeyMode); }
        });
        oQuestionObject.Container = oContainer;
        $.extend(oQuestionObject,{NonLoadedImages: [], ImagesLoadedHandlers: []});        
        oQuestionObject.SetBackground = fnSetBackground;
        var intro = oQuestionObject.Xml.find("introduction");
        if (trim(intro.text())) oQuestion.before($("<div class='intro' />").html(intro.text()));
        if (oQuestionObject.ProblemType.MultipleChoice) {
            var hasBG = cStr(oQuestionObject.Xml.attr("background")) != "";
            if (hasBG) oQuestion.find(".questions").addClass("background");
            var groups = [], answers = oQuestionObject.Xml.find("answer");
            var imageNodes = $.grep(answers, function (o) { return $(o).text().indexOf("<img ") > -1; });
            var imageMode = imageNodes.length > 0;
            var textMode = (answers.length - imageNodes.length) > 0;
            answers.each(function (i, o) {
                var groupName=cStr($(o).attr("group"));
                var group=$.grep(groups,function(o){return o.Name==groupName;})[0];
                if(!group){
                    group={Name:groupName, List:[]};
                    groups.push(group);
                }
                var text = $(o).text();
                group.List.push($("<div style='" + (text.indexOf("<img ")==-1 && imageMode && textMode ? "clear:both" : "") + "' class='" + (hasBG ? "" : "auto highlight") + (text.indexOf("<img ") > -1 ? " image" : "") + " question" + ($(o).attr("correct") == "yes" ? " c" : "") + (cStr(oQuestionObject.Answer) != "" && (("," + oQuestionObject.Answer + ",").indexOf("," + cStr(i) + ",") > -1) ? " selected" : "") + "'><div class='checkbox' /><div class='text' /><br /></div>").data("index", i).find(".text").html(text).end());
            });
            $.each(groups, function(i,o){
                if (_Quiz.Settings.RandomizeOptions) {
                    o.List.sort(function () {
                        return (Math.round(Math.random()) - 0.5)
                    });
                }
                var group = $("<div class='group'>" + (o.Name?"<div class='title'>"+o.Name+"</div>":"") + "</div>").appendTo(oQuestion.find(".questions"));
                $.each(o.List, function () {
                    this.appendTo(group);
                });
            });
            oQuestion.toggle(groups.length > 0);
            oQuestion.find(".question").click(function (e) { if ($(this).parents().is(".disabled")) return; $(this).toggleClass("selected"); if ($(this).hasClass("selected") && _Quiz.Settings.MCSingleSelect && $(this).siblings().addBack().filter(".question.c").length == 1) $(this).siblings(".question").removeClass("selected"); });
            oQuestionObject.SetBackground(version, cStr(oQuestionObject.Xml.attr("background")), oQuestion, 0, 0);
                
        } else if (oQuestionObject.ProblemType.DragAndDrop || oQuestionObject.ProblemType.ObjectPositioning) {
            var qAlign = oQuestionObject.Xml.find("drags").attr("align");
            var qWidth = cStr(oQuestionObject.Xml.find("drags").attr("width"), "225");
            var qHeight = oQuestionObject.Xml.find("drags").attr("height");
            var aPosition = oQuestionObject.Xml.find("targets").attr("position");
            if (qAlign == "top") oQuestion.find(".questions").addClass("top");
            oQuestion.find(".answers").addClass(cStr(oQuestionObject.Xml.find("targets").attr("size")));

            oQuestionObject.AnswerRecalc = function () {
                if (!this.Container) return;
                var Answer = $(this.Container).find(".quiz-DD .answer");
                $.each($(Answer), function () {
                    if ($(this).find(".question").length == 0) {
                        $(this).removeClass("has-items");
                    } else {
                        $(this).append($(this).find(".drop-area").height("6px").detach()).addClass("has-items").find(".question").css("position", "static");
                    }
                });
                var Questions = $(this.Container).find(".question");
                $.each(Questions, function () {
                    var p = $(this).parents(".answer");                    
                    $(this).find("table").css("border-right", (p.length == 0 || !$(p.data("node")).attr("color") ? "" : "3px solid " + $(p.data("node")).attr("color")));
                    if(!$(this).hasClass("image")) $(this).css("height", "auto").css("width", (p.length == 0 ? qWidth : (p.width() - 13)) + "px")
                });
                if (!qHeight || qAlign == "top") {
                    var maxQuestionHeight = 0;
                    $(this.Container).find(".questions .question").each(function () { $(this).css("height", "auto"); maxQuestionHeight = Math.max(maxQuestionHeight, $(this).height(),cInt(qHeight)); });
                    $(this.Container).find(".questions .question").each(function () { $(this).height(maxQuestionHeight + "px"); });
                }
                $(this.Container).find(".question.image").each(function () {
                    if ($.grep($(this).find("img"), function (item) { return !$(item).prop('complete'); }).length == 0) {
                        if ($(this).parents(".answer").length > 0 && !$(this).is(".shrink")) {
                            $(this).addClass("shrink").css("width", $(this).width() * .5).css("height", $(this).height() * .5);
                        } else if ($(this).parents(".answer").length == 0 && $(this).is(".shrink")) {
                            $(this).removeClass("shrink").css("width", "").css("height", "");
                        }
                    }
                });
            }
            oQuestionObject.ImagesLoadedHandlers.push(oQuestionObject.AnswerRecalc);

            oQuestionObject.Xml.find("drag").each(function (i, o) {
                var bImg = $(o).text().indexOf("<img")>-1;
                var q = $("<div style='" + (bImg ? "" : (qWidth ? "width:" + qWidth + "px;" : "") + (qHeight ? "height:" + qHeight + "px" : "")) + "' class='question" + (bImg ? " image" : "") + "'><table><tr><td class='rubber'></td><td class='text'></td></tr></table></div>").data("index", i).find(".text").html($(o).text()).end().appendTo(oQuestion.find(".questions"));
                if (oQuestionObject.Xml.find("drags").attr("align") == "absolute") {
                    oQuestion.find(".questions").addClass("absolute");
                    q.css("left", $(this).attr("x") + "px").css("top", $(this).attr("y") + "px");
                }
                $.each(q.find("img"), function (j, p) { if (!$(p).prop('complete')) oQuestionObject.NonLoadedImages.push(p); });
            });
            
            if (this.Settings.RandomizeOptions && oQuestionObject.Xml.find("drags").attr("align") != "absolute") {
                oQuestion.find(".questions .question").sort(function () {
                    return (Math.round(Math.random()) - 0.5)
                }).appendTo(oQuestion.find(".questions"));
            }

            oQuestionObject.AnswerRecalc();
            oQuestionObject.VShiftCalc = function () { return (qAlign == "top" && qAlign != "absolute" ? oQuestion.find(".questions").height() + 2 : 0); }
            oQuestionObject.HShiftCalc = function () { return (qAlign != "top" && qAlign != "absolute" ? oQuestion.find(".questions").width() : 0); }
            var vShift = oQuestionObject.VShiftCalc();
            var hShift = oQuestionObject.HShiftCalc();
            if (oQuestionObject.ProblemType.DragAndDrop) {
                oQuestionObject.Xml.find("target").each(function (i, o) {
                    var a = $("<div class='answer group'><span class='text' /><div class='drop-area answer_" + iIndex + "_" + $(o).attr("id") + "'></div></div>").data("index", i).data("node", o).find(".text").html($(o).text()).end().appendTo(oQuestion.find(".answers"));
                    if (aPosition == "absolute") {
                        var hAdj = (version > 1 ? hShift : 0);
                        a.css("position", "absolute").css("left", (cInt($(o).attr("x")) - 1 + hAdj) + "px").css("top", (cInt($(o).attr("y")) - 1) + "px").css("width", $(o).attr("width") + "px").css("height", $(o).attr("height") + "px");
                        contentHeight = Math.max(contentHeight, cInt($(o).attr("y")) + cInt($(o).attr("height")));
                    } else if (aPosition == "vertical") {
                        oQuestion.find(".answers").addClass("vertical");
                        if (cStr($(o).attr("color")))
                            a.find(".text").css("color", $(o).attr("color"));
                        a.width((700 / oQuestionObject.Xml.find("target").length) - 10);
                    } else {
                        oQuestion.addClass("visible");
                    }
                });
                if (this.Settings.RandomizeOptions && aPosition != "absolute") {
                    oQuestion.find(".answers .answer").sort(function () {
                        return (Math.round(Math.random()) - 0.5)
                    }).appendTo(oQuestion.find(".answers"));
                }                
                oQuestion.find(".questions .question").each(function (index, q) {
                    var i = $(q).data("index");
                    if (("," + oQuestionObject.Answer).indexOf("," + i + ":") > -1) {                        
                        var AnswerNode = oQuestionObject.Xml.find("target:eq(" + ("," + oQuestionObject.Answer).split("," + i + ":")[1].split(",")[0] + ")");
                        if (AnswerNode) _Section.find(".answer_" + iIndex + "_" + AnswerNode.attr("id")).parent().append(q);                                                
                    }
                });
            } else if(oQuestionObject.ProblemType.ObjectPositioning){
                //load answers
                oQuestionObject.ImagesLoadedHandlers.push(function () {
                    oQuestion.find(".questions .question").each(function (index, q) {
                        var i = $(q).data("index");
                        var a = cStr(cStr(("," + oQuestionObject.Answer).split("," + i + ":")[1]).split(",")[0]);
                        var d = { x: cInt(a.split("x")[0]), y: cInt(a.split("x")[1]) };
                        if (d.y!=0) $(q).css("top", (oQuestionObject.VShiftCalc()+ d.y) + "px").css("left", (oQuestionObject.HShiftCalc()+ d.x) + "px");                                                    
                    });
                });
            }

            oQuestionObject.AnswerRecalc();

            if (qAlign != "absolute" && oQuestion.find(".question").length > 0) {
                var i = Math.max(cInt(oQuestion.find(".answers").height()), cInt(oQuestion.find(".question:last").height()) + cInt(oQuestion.find(".question:last").position().top)) - vShift - 27;
                contentHeight = Math.max(i, contentHeight);
            }

            if (contentHeight > 0) oQuestion.height(contentHeight + vShift + 27);
            if (oQuestionObject.ProblemType.DragAndDrop){
                oQuestion.find(".answer").sortable({
                    connectWith: ".quiz-DD .answer, .questions",
                    items: "> .question",
                    placeholder: "active",
                    over: function (event, ui) {
                        $(this).addClass("active").find(".drop-area").animate({ height: ui.helper.height() + "px" }, 200);
                    },
                    out: function (event, ui) {
                        $(this).removeClass("active").find(".drop-area").height("6px");
                    },
                    receive: function (event, ui) {
                        oQuestionObject.AnswerRecalc(this, ui.item);
                    },
                    remove: function (event, ui) {
                        oQuestionObject.AnswerRecalc(this);
                    },
                    start: function (e, ui) { ui.helper.addClass("dragging"); $(document).data("disable-swipe", true); },
                    stop: function (e, ui) { ui.item.removeClass("dragging"); $(document).data("disable-swipe", null); }
                });
                oQuestion.find(".questions").sortable({
                    connectWith: ".quiz-DD .answer",
                    placeholder: "active",
                    revert: 200,
                    start: function (e, ui) { ui.helper.addClass("dragging"); $(document).data("disable-swipe", true); },
                    stop: function (e, ui) { ui.item.removeClass("dragging"); $(document).data("disable-swipe", null); },
                    receive: function (event, ui) {
                        oQuestionObject.AnswerRecalc();
                    }
                });
            } else {

                oQuestion.find(".question").each(function (i, o) {
                    $(o).draggable({                        
                        start: function (e, ui) { ui.helper.addClass("dragging"); $(document).data("disable-swipe", true); },
                        stop: function (e, ui) { ui.helper.removeClass("dragging"); $(document).data("disable-swipe", null); },
                        receive: function (event, ui) {
                            //oQuestionObject.AnswerRecalc();
                        }
                    });
                });
            }
            oQuestion.find(".questions, .answers").disableSelection();

            var fnSetBackgroundWhenReady = function () {                                
                vShift = oQuestionObject.VShiftCalc();
                if (aPosition == "absolute") {
                    if (version == 1) oQuestion.find(".answers").css("top", vShift + 57);
                    else oQuestion.find(".answers").css("top", vShift);
                    oQuestion.addClass("invisible").removeClass("dynamic").find(".answers").css("position", "absolute").css("left", 0);
                }                
            }            
            oQuestionObject.ImagesLoadedHandlers.push(fnSetBackgroundWhenReady);
            oQuestionObject.SetBackground(version, cStr(oQuestionObject.Xml.attr("background")), oQuestion, hShift);
            
        } else if (oQuestionObject.ProblemType.FillInTheBlanks) {
            var textObj = oQuestionObject.Xml.find("text");
            var qAlign = cStr(textObj.attr("align"), "left");
            oQuestion.addClass(cStr(textObj.attr("size"), "regular"));
            oQuestion.find(".questions").addClass(qAlign);
            oQuestion.find(".answers").addClass(qAlign);
            var text = textObj.text();            
            if(text.indexOf("<")==-1)text=replaceAll(text, "\n", "<br />");
            var re = /({)[^}]*(})/g, alternatives = [];
            var arr, lastIndex = 0, html = '';
            while ((arr = re.exec(text)) !== null) {
                var answer = arr[0].substring(1, arr[0].length - 1);
                html += text.substring(lastIndex, re.lastIndex - arr[0].length) + '<span class="answer" data-answer="' + answer + '">&nbsp;</span>';
                lastIndex = re.lastIndex;                
                if (answer) {
                    var q = $("<div class='question'></div>").html(answer);
                    alternatives.push(q);
                }
            }
            html += text.substring(lastIndex);
            $("<div />").html(html).appendTo(oQuestion.find(".answers")).find("[data-answer]").each(function (i, o) {
                $(this).data("answer", $(this).attr("data-answer")).removeAttr("data-answer");
            });

            oQuestionObject.Xml.find("answer").each(function (i, o) {
                alternatives.push($("<div class='question'></div>").html($(o).text()));                
            });

            $.each(alternatives.sort(function () {
                return Math.round(QuizRandom()) - 0.5;
            }), function () { $(this).appendTo(oQuestion.find(".questions")); });

            if (qAlign == "top") oQuestion.find(".questions").height(oQuestion.find(".questions").height());
            else oQuestion.find(".questions").height(oQuestion.height());            

            $.each(cStr(oQuestionObject.Answer).split("|"), function (i, s) {
                if (s) {
                    var q = oQuestion.find(".questions .question").filter(function(){return $(this).text() === s}).eq(0);
                    if (q.length > 0) oQuestion.find(".answer").eq(i).append(q).addClass("full");
                }
            });

            function RecalcReUsableAnswers() {
                if (textObj.attr("reuse-answers") == "1") {
                    var processedMatches = [];
                    oQuestion.find(".question").each(function () {
                        var text = $(this).html();
                        if (processedMatches.indexOf(text) == -1) {
                            processedMatches.push(text);
                            var unusedMatch;
                            oQuestion.find(".question").filter(function () { return $(this).text() === text; }).each(function () {
                                if ($(this).parent().is(".questions")) {
                                    if (unusedMatch) $(this).remove();
                                    else unusedMatch = this;
                                }
                            });
                            if (!unusedMatch)
                                $("<div class='question'></div>").html(text).appendTo(oQuestion.find(".questions"));
                        }
                    });
                }
            }
            oQuestion.find(".answer").sortable({
                connectWith: ".quiz-FB .answer, .questions",
                items: "> .question",
                over: function (event, ui) {
                    $(this).addClass("active");
                },
                out: function (event, ui) {
                    $(this).removeClass("active");
                },
                receive: function (event, ui) {
                    if ($(this).hasClass("full")) $(ui.sender).sortable('cancel');
                    $(this).addClass("full");
                    RecalcReUsableAnswers();
                },
                remove: function (event, ui) {
                    $(this).removeClass("full");
                    RecalcReUsableAnswers();
                }
            });

            oQuestion.find(".questions").sortable({
                connectWith: ".quiz-FB .answer"
            });

            oQuestion.find(".questions, .answers").disableSelection();            
            RecalcReUsableAnswers();           

        } else if (oQuestionObject.ProblemType.ConnectTheBoxes) {
            var arrange = oQuestionObject.Xml.find("boxes").attr("arrange");                        
            oQuestionObject.Xml.find("box").each(function (i, o) {
                var q = $("<div class='question'><div class='text' /></div>").toggleClass("empty", $(o).text() == "").data("index", i).find(".text").html($(o).text()).end().appendTo(oQuestion.find($(this).attr("texton") == "left" ? ".questions" : ".answers")).data("boxid", $(this).attr("id"));
                if (arrange != "auto") {
                    q.css("position", "absolute").css("left", ($(this).attr("x") - ($(this).attr("texton") == "left" ? (q).width() : 0)) + "px").css("top", ($(this).attr("y") - 20) + "px");
                    contentHeight = Math.max(contentHeight, cInt($(o).attr("y")) + 50);
                }
                $.each(q.find("img"), function (j, p) { if (!$(p).prop('complete')) oQuestionObject.NonLoadedImages.push(p); });
            });
            
            var fnLoadQuestion = function () {
                var _jsPlumb;
                if (_Quiz.Settings.RandomizeOptions && arrange == "auto") {
                    oQuestion.find(".questions .question").sort(function () {
                        return (Math.round(Math.random()) - 0.5)
                    }).appendTo(oQuestion.find(".questions"));
                    oQuestion.find(".answers .question").sort(function () {
                        return (Math.round(Math.random()) - 0.5)
                    }).appendTo(oQuestion.find(".answers"));
                }
                oQuestionObject.LoadAnswers = function () {
                    $.each(cStr(oQuestionObject.Answer).split(","), function (index, str) {
                        if (trim(str) != "") {
                            _jsPlumb.connect({
                                source: oQuestion.find(".question").filter(function () { return $(this).data("boxid") == str.split(":")[0]; })[0],
                                target: oQuestion.find(".question").filter(function () { return $(this).data("boxid") == str.split(":")[1]; })[0]
                            });
                        }
                    });
                }
                oQuestionObject.CreateEndpoints = function () {
                    _jsPlumb.addEndpoint(oQuestion.find(".questions .question"), {
                        isSource: true, isTarget: true, anchor: "RightMiddle", maxConnections: -1, deleteEndpointsOnDetach: false
                    });

                    _jsPlumb.addEndpoint(oQuestion.find(".answers .question"), {
                        paintStyle: { width: 25, height: 21, fillStyle: '#666' },
                        isSource: true, isTarget: true,
                        anchor: "LeftMiddle",
                        deleteEndpointsOnDetach: false,
                        maxConnections: -1/*,
                    dropOptions: { activeClass: "active" },
                    hoverClass: "active",
                    cssClass:"active"*/
                    });
                    _jsPlumb.makeSource(oQuestion.find(".questions .question"), { isSource: true, isTarget: true, anchor: "RightMiddle", maxConnections: -1, deleteEndpointsOnDetach: false });

                    _jsPlumb.makeTarget(oQuestion.find(".answers .question"),
                        { isSource: true, isTarget: true, deleteEndpointsOnDetach: false }
                      );
                };
                jsPlumb.ready(function () {
                    oQuestionObject.jsPlumb = jsPlumb.getInstance();
                    _jsPlumb = oQuestionObject.jsPlumb;
                    _jsPlumb.importDefaults({
                        PaintStyle: { lineWidth: 10, strokeStyle: '#456' },
                        Connector: ["Straight", { curviness: 150 }],
                        Anchors: ["RightMiddle", "LeftMiddle"],
                        ConnectionsDetachable: true
                    });
                    oQuestionObject.CreateEndpoints();

                    _jsPlumb.bind("connection", function (info) {
                        $([info.source, info.target]).addClass("active");

                        info.connection.bind("click", function (conn, e) {
                            _jsPlumb.detach(conn);
                        });

                    });
                    _jsPlumb.bind("beforeDrop", function (info) {
                        //Prevent same line from being drawn twice
                        return _jsPlumb.getConnections({ target: info.targetId, source: info.sourceId }).length==0;                        
                    });

                    _jsPlumb.bind("connectionDetached", function (info) {
                        if (_jsPlumb.getConnections({ source: info.source }).length == 0) $(info.source).removeClass("active");
                        if (_jsPlumb.getConnections({ source: info.target }).length == 0) $(info.target).removeClass("active");
                    });
                    oQuestionObject.LoadAnswers();
                    var prevKeyMode = oQuestionObject.KeyMode == true;
                    oQuestionObject.KeyMode = false;
                    _Quiz.save(iIndex, true, prevKeyMode);
                });                
                if (contentHeight > 0) oQuestion.height(contentHeight + 27);
                if (oQuestionObject.Xml.find("boxes").attr("draw") == "false") oQuestion.addClass("invisible");
                                
            };
            
            oQuestionObject.ImagesLoadedHandlers.push(fnLoadQuestion);            
            oQuestionObject.SetBackground(version, cStr(oQuestionObject.Xml.attr("background")), oQuestion, 0, 0);            
        }
        oQuestionObject.ImagesLoadedHandlers.push(function () {
            $(window).scrollTop(PrevScroll);
        });
        
        fnExecuteWhenAllComplete(oQuestionObject);
       
        var bottomtext = oQuestionObject.Xml.find("bottomtext");
        var text = trim(bottomtext.text());
        if (text) {
            var re = /(\[)[^}]*?(\])/g;
            var arr, lastIndex = 0, html = '',i=0;
            while ((arr = re.exec(text)) !== null) {
                var answer = arr[0].substring(1, arr[0].length - 1);
                html += text.substring(lastIndex, re.lastIndex - arr[0].length) + '<input type="text" class="answer auto" data-answer="' + answer + '">';
                lastIndex = re.lastIndex;
                i++;
            }
            html += text.substring(lastIndex);
            $("<div class='bottomtext' />").html(html).appendTo(oContainer.find(".quiz-area"))
            .find("[data-answer]").each(function (i, o) {
                $(this).data("answer", $(this).attr("data-answer")).removeAttr("data-answer");
            }).end().find("input, textarea").each(function (i, o) {
                if ($(o).is("[type=checkbox]"))
                    $(o).attr("checked",oQuestionObject.Bottomtexts[i]==1);
                else
                    $(o).val(oQuestionObject.Bottomtexts[i]);
            });
        }
        if (oQuestionObject.DisplayTimerMessage()) {        
            oContainer.find(".quiz-container").css("visibility", "hidden").parent().append($("<div class='timed-quiz'><h1>This is a timed quiz</h1>You have " + oQuestionObject.RemainingTime + " seconds to complete it. When you're ready, hit the start button below. A clock will appear to help you manage your time. When the time is up, you will no longer be able to change your response.<br /><br /><div class='start big-button'><i class='fa fa-play-circle'></i><div class='text'>Start</div></div><br style='clear:both' /> </div>").find(".big-button").click(function () {
                oQuestionObject.StartTimer();
            }).end());            
        }
        oContainer.find(".display-key").toggleClass("hide", this.Settings.ShowAnswersScore>100);
        
        var reason = oQuestionObject.Xml.find("reason");
        var t = trim(reason.text());
        oContainer.find(".display-reason").toggleClass("hide",t.length == 0).on("click", function () { if ($(this).is(".disabled")) return; fnShowMessage(t.indexOf("<") == -1 ? replaceAll(t, "\n", "<br />") : t); });        
        oQuestionObject.UpdateUI();        
        //Display correct answers
        if (this.Settings.FinalTest && this.Settings.KeyMode) this.save(this.CurrentQuestion);
        _Section.trigger("render", [oQuestionObject]);
        return oContainer;
    }
    
    function fnSetBackground(version, src, oQuestion, offsetX, offsetY) {
        if (src) {
            var _this = this;
            var i = new Image();
            i.src = src;
            _this.NonLoadedImages.push(i);

            _this.ImagesLoadedHandlers.push(function () {
                var dim = { x: offsetX, y: offsetY };
                if (dim.y == null && _this.VShiftCalc) dim.y = _this.VShiftCalc();
                if (version == 1) {
                    dim.x += 11;
                    dim.y -= 3;
                }
                var p = oQuestion.parents(".quiz-container");

                oQuestion.css("background", "url('" + src + "') no-repeat " + dim.x + "px " + dim.y + "px");
                dim.x = this.width + dim.x;
                if (dim.x > p.width()) {
                    p.css("position", "relative").css("left", (p.width() - dim.x) / 2).css("width", dim.x);
                }

                dim.y = i.height + dim.y;
                if (dim.y > oQuestion.height()) {
                    oQuestion.css("height", dim.y);                    
                }
            });            
        }
    }
    function fnExecuteWhenAllComplete(QuestionObject) {
        var RunAll = function () {
            $.each(QuestionObject.ImagesLoadedHandlers, function (i, fn) {
                fn();
            });            
        }        
        if ($.grep(QuestionObject.NonLoadedImages, function (item) { return !$(item).prop('complete'); }).length == 0) 
            RunAll();
        else
            var t = setInterval(function () {
                if ($.grep(QuestionObject.NonLoadedImages, function (item) { return !$(item).prop('complete'); }).length == 0) {
                    RunAll();
                    clearInterval(t);
                }
            }, 100);
    }
    function clsQuestion(xmlQuestion, Quiz, Index, Answer) {
        var _question = this;
        this.Xml = $(xmlQuestion);
        this.Quiz = Quiz;
        this.Index = Index;        
        this.ProblemType = {
            DragAndDrop: this.Xml.attr("type") == "draganddrop",
            ObjectPositioning: this.Xml.attr("type") == "objectpositioning",
            MultipleChoice: this.Xml.attr("type") == "multiplechoice",
            ConnectTheBoxes: this.Xml.attr("type") == "connecttheboxes",
            FillInTheBlanks: this.Xml.attr("type") == "fillintheblanks",
            TrueOrFalse: this.Xml.attr("type") == "trueorfalse"
        };
        this.reset = function (bInternal) {
            this.Answer = "";
            this.KeyMode = false;
            this.Bottomtexts = [];
            if(cInt(this.Quiz.Settings.MaxAttempts)==0)this.CheckAnswerCounter = 0;
            this.Score = 0;
            this.Time = cInt(this.Xml.attr("timer"));            
            this.RemainingTime = this.Time;
            if (!bInternal) {
                this.Container = Quiz.render(this.Position);
                Quiz.save(this.Position, true, false);
            }
        };
        this.reset(true);
        this.ParseAnswer=function(Answer){
            Answer = cStr(Answer).split("§");
            this.Answer = cStr(Answer[0]);
            this.Bottomtexts = cStr(Answer[1]).split("|");            
            this.CheckAnswerCounter = cInt(Answer[2]);
            if(cStr(Answer[2])==="" && this.Answer!=="")this.CheckAnswerCounter++;
            this.Score = cInt(Answer[3]);
            if(cStr(Answer[4])!=="")this.RemainingTime = cInt(Answer[4]);
        };
        this.ParseAnswer(Answer);
        this.IsCompleted = function () {
            return (this.CheckAnswerCounter > 0 && (_Quiz.Settings.MinimumRequiredScore == 0 || this.Score >= _Quiz.Settings.MinimumRequiredScore))
                || this.IsDisabled();
        }
        this.AllowShowAnswers = function () {
            return (this.CheckAnswerCounter > 0 && this.Score >= _Quiz.Settings.ShowAnswersScore) || this.IsDisabled();
        }
        this.DisplayTimerMessage = function(){
            return !this.IsCompleted() && this.RemainingTime > 0;
        }            
        this.IsDisabled = function () {
            if (cInt(_Quiz.Settings.MaxAttempts) > 0 && this.Quiz.Settings.MaxAttempts <= this.CheckAnswerCounter) return true;
            if (this.Time > 0 && this.RemainingTime < 1) return true;
            return false;
        }
        this.StartTimer = function () {
            _question.Container.find(".quiz-container").css("visibility", "visible").parent().find(".timed-quiz").hide();
            this._TimerObject = setInterval(function () {
                _question.RemainingTime--;
                _question.Container.find(".problem-container .clock").show().html(Math.floor(_question.RemainingTime / 60) + ":" + ("0" + (_question.RemainingTime % 60)).slice(-2));
                if (_question.RemainingTime < 1) {
                    clearInterval(_question._TimerObject);
                    _question.Container.find(".check-answers").click();
                    _question.Container.find(".problem-container .clock").hide();                
                }
            }, 1000);
        }
        this.StopTimer = function () {
            if (this._TimerObject) {
                clearInterval(this._TimerObject);
                this._TimerObject = null;
            }
        };
        this.UpdateUI = function () {
            this.Container.find(".quiz-container").toggleClass("disabled", this.IsDisabled());

            this.Container.find(".display-key").toggleClass("disabled", !this.AllowShowAnswers()).find("span:eq(0)").html(replaceAll(_Translations["display-answers"], "{0}", "").split("(")[0]);
            this.Container.find(".display-reason").toggleClass("disabled", !this.IsCompleted());
            this.Container.find(".display-key-container").toggle(this.Container.find(".display-key, .display-reason").not(".hide").length > 0);
            this.Container.find(".progresscolor-taken").css("width", this.Score + "%").find(".progressnumber-taken").html(replaceAll(_Translations["percent-correct"], "{0}", this.Score))
            this.Container.find(".prev-button").toggleClass("disabled", this.Position == 0);
            this.Container.find(".next-button").toggleClass("disabled", !this.Quiz.CanGotoQuestion(this.Position + 1));
            for (var x = this.Position + 1; x < this.Quiz.Questions.length; x++) {
                var next = this.Quiz.Questions[x];
                if (next && next.Nav)
                    next.Nav.toggleClass("disabled", !this.Quiz.CanGotoQuestion(x));
            }
            if (this.IsCompleted()) this.StopTimer();
            if (this.IsDisabled())
                this.Container.find(".question.ui-draggable").draggable('disable');
        }
    }
}
