﻿<%@ Page Language="C#" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Player.Player" MasterPageFile="~/inc/master/courseplayer.master" Title="myMetier" Codebehind="player.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">    
        <script src="../inc/js/dom.jsPlumb-1.7.5-min.js"></script>
		<script src="css/wow.min.js"></script>        
		
    <script src="css/quiz.js?1"></script>
    <asp:PlaceHolder runat="server" ID="pEditingScript" Visible="false">
        <script src="../cms/gfx/cms-content.js"></script>        
        <link type="text/css" rel="stylesheet" href="../cms/gfx/cms-content.css" />
		<link type="text/css" rel="stylesheet" href="css/animate.min.css" />
    </asp:PlaceHolder>
	
<div id="content-wrapper"></div>
<script type="text/template" id="content-container-template">    
<div style="margin: 0 auto;" class="content-container">
    <div id="wrapper-submenu">
      
  <br />
  <ul class="subnav"></ul>
    </div>
</div>
</script>
<script type="text/template" id="splash-template">    
    <div class="_content group">
        <div class="wrapper main-wide" style="width:1024px">
    <div style="border:1px solid #6e777e;padding:10px; position:relative">
        <img class="image" />
        <div class="content"></div>
        <div id="button-open-video"><div class="frame"><div class="image"><a class="play"></a></div></div></div>    
    </div>
    <div style="padding:5px 20px 5px 20px">
    <div class="actionbutton" style="float:right;padding-top:30px"><a href="javascript:void(0)" class="start"><span translationkey="start-lecture">Start leksjon</span> <i class="fa fa-chevron-circle-right" style="padding-left: 5px;position: relative;top: 1px;"></i></a></div>
    <h1 class="lesson-title"></h1>
    <br style="clear:left" />
    <h2 class="course-title"></h2>
    </div>
    </div>
    <br style="clear:both" />

    <div style="display:none; overflow:hidden" id="video-player">
    </div>
</div>
</script>

<script type="text/template" id="theory-template">    
    <div class="_content group">
    <div class="wrapper main-text"><h1></h1>
        {Xml}
    </div>
    </div>
</script>
<script type="text/template" id="reflection-template">    
    <div class="_content group">
    <div class="wrapper main-text"><h1></h1>
        {Xml}
        <br />
        <br />
        <table class="chat-dialog reflection" style="padding-left:21px"></table>
    </div>
    </div>
</script>
<script type="text/template" id="examples-template">    
    <div class="_content group">
    <div class="wrapper main-text"><h1></h1>
        {Xml}
    </div>
    </div>
</script>
<script type="text/template" id="global-template">    
    <div class="_content group">
    <div class="wrapper main-text"><h1></h1>
        {Xml}
    </div>
    </div>
</script>
<script type="text/template" id="pitfalls-template">
    <div class="_content group">
        <div class="wrapper intro-fallgruver"><h1>{Name}</h1></div>
    
        <div class="wrapper main"></div>
        <div class="pitfalls-content">{Xml}</div>
</div>
</script>    
<script type="text/template" id="checklist-template">
    <div class="_content group">
        <div class="menu content">
            <ul class="tools">
                <li><a class="addcheckpoint" href="javascript:void(0)" translationkey="add-new"></a></li>    
              </ul>
              <br />
              <ul class="subnav">
              </ul>
        </div>
        <div class="wrapper main" style="padding-top:0">
            <div class="checklist">    
            <div>        
                <div class="top"><h1>{Name}</h1></div>
                <div class="content"></div>
                <div class="bottom"></div>
            </div>
          </div>  
        </div>        
        <div class="checklist-content">{Xml}</div>
    </div>
</script>    
<script type="text/template" id="goals-template">
    <div class="_content group">
        <div class="goals-sign wrapper">
        <div class="group">
            <div class="top-l"></div>
            <div class="top-m"></div>
            <div class="top-r"></div>
        </div>
            <div class="goals-list"><div class="goals-heading"><h1>{Name}</h1>
                <div class="goals-content">{Xml}</div>            
            </div></div>
            <div class="group">
                <div class="bottom-l"></div>
                <div class="bottom-m"></div>
                <div class="bottom-r"></div>
            </div>
        </div>
    </div>
</script>
<script type="text/template" id="case-template">
    <div class="_content group">
    <div class="wrapper main">    
        <h1>{Name}</h1>    
        <p class="intro">
            <div class="case-content">{Xml}</div>
        </p>
        <br />    
        <div class="chat-dialog wide" style="padding-left:21px"></div>
    </div>
    </div>
</script>
<script type="text/template" id="intro-template">
    <div class="_content group">
<div class="wrapper main-text">
    <div class="print-details">
  <h1 class="course-title" style="font-size:35px;float:left;"></h1>
<br style="clear:left" />
<h2 class="lesson-title" style="float:left;line-height:10px"></h2>
<br />
<br /><br /><br />
</div>

<h1></h1>
{Xml}
</div>
</div>
</script>

    <script type="text/template" id="quiz-template">
<div class="_content group">
    
    <div class="question-template quiz-outer" style="display:none;">
<a href="javascript:void(0)" class="anchor"></a>
<div class="nine-rounded quiz-container">
<div class="head">
    <div class="counter">1</div>
    <div class="problem-container">
    <div class="problem"></div>
        <div class="clock"></div>
    </div>
    <br style="clear:both" />
</div>
<table style="width:100%" cellspacing="0">
<tr>
<td colspan="4" class="quiz-area">

<div class="quiz-DD dynamic">
<div class="questions group"></div>

<div class="answers group"></div>
</div>

<div class="quiz-CL">
<div class="questions"></div>

<div class="answers"></div>
<br style="clear:both" />
</div>

<div class="quiz-FB">
<div class="questions"></div>

<div class="answers"></div>
<br style="clear:both" />
</div>

<div class="quiz-MC">
    <div class="questions"></div>
</div>

</td>
</tr>

<tr class="quiz-buttons">
<td colspan="2" style="padding:3px 0">
    <div class="check-answers big-button"><i class="fa fa-check-circle"></i><div class="text" translationkey="check-answer"></div></div> 
    <div class="progressbar large" style="width:150px">            
          <div class="progresscolor-taken" style="display:inline-block;"><div class="progressnumber-taken" style="width:150px"></div></div>
          <div class="rt"></div>
    </div>
</td>

    <td class="display-key-container">
        <div class="display-key clickable"><i class="fa fa-eye"></i><span translationkey="display-answers">Vis fasit <span>(-2)</span></span></div>
        <div class="display-reason clickable"><i class="fa fa-book"></i><span translationkey="display-reason">Display reasoning</span></div>
    </td>
    <td class="reset-container"><a href="javascript:void(0)" class="reset"><i class="fa fa-repeat"></i><span translationkey="start-over">Begynn på nytt</span></a></td>
</tr>

<tr class="finaltest-buttons">
<td colspan="3" style="height:30px"><div class="highlight prev-button player-button"><div class="left"></div><div class="right"></div><div class="text button-text-shadow"><i class="fa fa-angle-double-left"></i> &nbsp;<span translationkey="prev-button">Previous</span></div></div> 
<div class="highlight next-button player-button"><div class="left"></div><div class="right"></div><div class="text button-text-shadow"><span translationkey="next-button">Next</span> &nbsp;<i class="fa fa-angle-double-right"></i></div></div>

</td>
<td style="float:right"><div class="highlight submit-button player-button"><div class="left"></div><div class="right"></div><div class="text button-text-shadow" translationkey="submit-button">Submit Test</div></div><div class="highlight new-attempt player-button"><div class="left"></div><div class="right"></div><div class="text button-text-shadow" translationkey="new-attempt">Submit Test</div></div></td>
</tr>

</table>
</div>
</div>

<div class="wrapper main" style="width:720px">
</div>
<br /><br />

</div>
</script>

<script type="text/template" id="complete-template">    
    <div class="_content group">
<div class="wrapper main page-complete-lesson">
<div class="nine-rounded" id="lesson-in-progress">
<table cellspacing="0">
<tr>
<td valign="top" style="width:413px;padding:20px 35px"><h1 translationkey="title"></h1>
<p class="intro" translationkey="sub-title"></p>
<p translationkey="review-later"></p>
</td>
<td style="background-color:#f0f3f6;text-align:center;vertical-align:middle; border-left:1px solid #d0d6dc; border-radius: 0 9px 9px 0">
<table id="completion-button" onclick="$('#lesson-in-progress, #lesson-completed').toggle();">
<tr><td class="inner"><div class="text button-text-shadow" translationkey="complete-button">Fullfør</div></td></tr>
</table>
</td>
</tr>
</table>

</div>

<div style="display:none" id="lesson-completed">
    <div class="nine-rounded">
        <table cellspacing="0">
        <tr>
        <td valign="top" style="width:413px;padding:20px 35px"><h1 translationkey="lesson-completed"></h1>
        <p translationkey="review-later"></p>
        </td>
        <td style="padding:20px 0 20px 100px">
        <div class="checkmark"></div>
        </td>
        </tr>
        <tr>
        <td style="border-top:1px solid #d0d6dc;"></td>
        <td style="text-align:center;border-top:1px solid #d0d6dc;border-left:1px solid #d0d6dc;font-size:12px"><p><a href="javascript:void(0)" id="reset-complete-status" translationkey="reset-status"></a></p></td>
        </tr>
        </table>
    </div>
    
    <br />
    <div class="nine-rounded">
        <table cellspacing="0" id="lesson-rating-container">
        <tr>
        <td valign="top" style="width:413px;padding:10px 35px"><h2 translationkey="rate-lesson-heading"></h2>
        <p translationkey="rate-lesson-instructions"></p>
        <div id="lesson-rating"><div class="knob"></div><div class="bar"></div></div>
        <div class="label" translationkey="rate-lesson-bad"></div>
        <div class="label" style="float:right;" translationkey="rate-lesson-good"></div>
         
        </td>
        <td style="padding:10px 15px 20px 25px;border-left:1px solid #d0d6dc" class="comment">
        <h2 translationkey="comment-lesson">Kommentér leksjonen</h2>
            <div class="contents">
              <div class="chat-dialog user-answer small" style="padding-top:10px"><div class="right"><div class="container"><div class="top"></div><div class="fill"><div class='text'><textarea id="comment-text" style="height:40px"></textarea></div></div></div><div class="bottom"></div></div></div>
              <div style="margin:20px 0 0 45px"><div class="highlight player-button submit"><div class="left"></div><div class="right"></div><div class="text button-text-shadow" translationkey="send-comment"></div></div></div><div class="loader"></div>
          </div>
        </td>
        </tr>
        </table>
    </div>

    <br />
    <div class="nine-rounded">
        <table cellspacing="0">
        <tr>
        <td valign="top" style="width:423px;padding:20px 25px 20px 35px;">
        <div style="font-size:18px;float:left;padding:5px 40px 20px 0;" class="progress-points"></div>
        <div class="progressbar large" style="width:250px">            
          <div class="progressnumber-total"></div><div class="progresscolor-taken" style="display:inline-block;width:92%;"><div class="progressnumber-taken"></div></div>
          <div class="rt">
        </div></div>
        <br style="clear:both" />
          <p id="complete-details"></p>
        </td>
        <td class="information-button highlight close-lesson">          
          <div class="image" ></div>          
          <p style="font-size:14px" translationkey="course-overview"></p>  
        </td>
        <td class="information-button highlight next-lesson" style="padding:20px 15px;">
            <div class="frame">
            <common:label runat="server" ID="lNextLessonImage" Tag="div" cssclass="image"></common:label>
            </div>
            <div style="font-size:14px" translationkey="next-lesson"></div>
            <div style="font-size:12px; color:#95aec3" class="lesson-name">Kunde og leverandør</div>
        </td>
        </tr>
        </table>
    </div>
</div>

</div>
</div>
</script>
    <script language="javascript">
    <common:label runat="server" ID="lContent" />
        </script>
</asp:Content>