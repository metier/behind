﻿<%@ Page Title="myMetier Overview" Language="C#" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Index" MasterPageFile="~/inc/master/mymetier.master" Codebehind="index.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">

  
<div id="wrapper-introduction">
  <div id="container-merits" class="nine-rounded">
    <ul class="merits-mini">
      <common:label runat="server" Tag="li" CssClass="gold" ID="lMedalsGold" />
      <common:label runat="server" Tag="li" CssClass="silver" ID="lMedalsSilver" />
      <common:label runat="server" Tag="li" CssClass="bronze" ID="lMedalsBronze" />
    </ul>
    <div class="clear"></div>
    <ul class="goto">
      <li><common:hyperlink runat="server" TranslationKey="view-medals" NavigateUrl="user.aspx"/> </li>
    </ul>
  </div>
  <common:label runat="server" ID="lHeading" Tag="h1" />    
  <common:label runat="server" ID="lSubHeading" Tag="h3" />
  <div class="clear"></div>
</div>

     <script>
         
         $(function () {
             $("#courses-ongoing").toggle(__Courses.Current.length > 0);
             $("#courses-completed").toggle(__Courses.Completed.length > 0);
             $("#courses-noshow").toggle(__Courses.NoShow.Activities.length > 0);
             
                          
             $.each(__Courses.Current, function () {
                 $("#courses-ongoing .accordion").append(tmpl("activityset_template", this));
             });
             $.each(__Courses.Completed, function () {
                 $("#courses-completed .accordion").append(tmpl("activityset_template", this));
             });
             
             $("#courses-noshow .courses").append(tmpl("activityset_template", __Courses.NoShow));
                    
             //active 
             $.each(["#courses-ongoing", "#courses-completed"], function () {
                 var key = this;                                  
                 $(key + " .accordion").accordion({
                     heightStyle: "content",
                     collapsible:true,
                     active: Math.max(0, $(key + " .activityset").index($(key + " #" + $.cookie(key + "-Active")))),
                     activate: function (e, u) {                         
                         $.cookie(key + "-Active", $(u.newPanel).find(".activityset").attr("id"), { expires: 365 });                         
                     }
                 });
             });
         });
  </script>
    
<script type="text/html" id="activityset_template">
    <h3>{%=Name%}</h3>
    <div>
    <table class="activityset" id="ActivitySet-{%=Id%}">
        {% for(i=0;i<Activities.length;i++) { %}
        {%=i%3 == 0 ? "<tr class='miniprogrambox'>":""%}
        <td class="nine-rounded">
            
            <div class="main" onclick="window.location='navigator.aspx?activity_ids={%=Activities[i].Id%}&ilm_id={%=Id%}'">
            <div class="progressbar" style="{%=Activities[i].ActivityType=="elearning"?"":"display:none"%}">
              <div class="number-total">{%=Activities[i].ELessons%}</div>
                <div style="{%=Activities[i].ELessons==0||Activities[i].ELessonsCompleted==0?"display:none":""%}" class="percentage-taken">{%=Activities[i].ELessons==0?0:cInt(100*Activities[i].ELessonsCompleted/Activities[i].ELessons)%}%</div>
              <div class="taken" style="width:{%=Activities[i].ELessons==0?100:(100*Activities[i].ELessonsCompleted/Activities[i].ELessons)%}%;">              
                <div class="number-taken">{%=Activities[i].ELessonsCompleted%}</div>
              </div>
          
            </div>
             <div class="symbol-classroom" style="display:{%=Activities[i].ActivityType=="classroom"?"":"none"%}">
              <div class="month">{%=Activities[i].CMonth%}</div>
              <div class="date">{%=Activities[i].CDay%}</div>
            </div>
            <div class="symbol-exam" style="display:{%=Activities[i].ActivityType=="exam"?"":"none"%}"></div>
            <div class="programthumb"><img src="{%=Activities[i].ImageUrl%}" alt="Program" /></div>
        
            <div class="clear"></div>        
            <h2>{%=Activities[i].Name%}</h2>
        
            </div>
            <div class="actionbutton"><a href="navigator.aspx?activity_ids={%=Activities[i].Id%}&ilm_id={%=Id%}">{%=Activities[i].ButtonTitle%}</a></div>                    
            {%=Activities[i].Completed ? "<div class='completed'></div>":""%}
            {%=Activities[i].Status=="NOSHOW" ? "<div class='noshow'></div>":""%}
        </td>            
        {%=i%3 == 2 || i == Activities.length-1 ? "</tr>":"<td class='divider'></td>"%}
        {%=i%3 == 2 && i < Activities.length-1 ? "<tr><td style='line-height:10px'>&nbsp;</td></tr><tr>":""%}
        {% } %}
    </table>
        </div>
  </script>

<div class="wrapper-miniprogramboxes" id="courses-ongoing">    
<common:label runat="server" tag="h1" TranslationKey="current-courses-heading" />  
     <div class="accordion"></div>        
    <br style="clear:both" />
    <common:hyperlink runat="server" NavigateUrl="courses.aspx" CssClass="lCourseCatalog" ID="lCourseCatalog"></common:hyperlink>    
</div>


<div class="wrapper-miniprogramboxes" id="courses-completed">  
  <common:label runat="server" tag="h1" TranslationKey="completed-courses-heading" />  
    <div class="accordion"></div>        
</div>

<div class="wrapper-miniprogramboxes" id="courses-noshow">  
  <common:label runat="server" tag="h1" TranslationKey="no-show-courses-heading" />  
    <div class="courses"></div>   
</div>

    <asp:PlaceHolder runat="server" ID="pNagDialog">
    <div id="nag-dialog">
        <common:label runat="server" TranslationKey="competency-nag-head" Tag="h1">University credit eligibility</common:label>
        <common:label runat="server" TranslationKey="competency-nag-body">
        You're enrolled in an exam that awards university credits. In order for you to be eligibile to receive these credits, you need to document that you meet the requirements of general study competence.
        <br /><br />This can be done on your Settings-page, in the section titled <i>University credit eligibility</i>.</common:label>
        <br /><br />
        <button type="button" onclick="window.location='settings.aspx#competency'"><common:label runat="server" TranslationKey="competency-nag-goto-form">Go to credit eligibility form</common:label></button>
        &nbsp;&nbsp;<button type="button" onclick="window.location='index.aspx'"><common:label runat="server" TranslationKey="competency-nag-postpone">Postpone</common:label></button>
    </div>
        </asp:PlaceHolder>
<script language="javascript">
    if (fnGetIEVersion() > 0) {
        $(".nine-rounded").each(function (i, o) {            
            $(o).append("<div class='lt' />").append("<div class='rt' />").append("<div class='lb' />").append("<div class='rb' />");
        });
    }    
    
    $('#nag-dialog').dialog({ modal: true, "height": 400, "width": 600, position: ['center', 'center'] }).dialog('open').dialog('option', 'title', $('#nag-dialog').find("h1").html());
</script>
</asp:Content>