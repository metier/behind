﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.components;
using jlib.db;
using jlib.functions;
using System.Text;
using System.Linq;
using System.Security.Cryptography;

namespace Phoenix.LearningPortal
{
    public partial class NavigatorFrame : jlib.components.webpage
    {
        private Util.Classes.user m_oActiveUser;
        public void Page_PreInit(object sender, EventArgs e)
        {
            (Page.Master as Inc.Master.MyMetier).DisableWrapper = true;
        }
        public void Page_Init(object sender, EventArgs e)
        {
            m_oActiveUser = (Page.Master as Inc.Master.MyMetier).ActiveUser;

            if (System.Configuration.ConfigurationManager.AppSettings["site.portal2"].Bln() && m_oActiveUser != null && m_oActiveUser.DistributorId > 0 && m_oActiveUser.DistributorId != 10000)
                Response.Redirect("portal2/#/portal/home");

            string URL = (this.QueryString("test").Bln() ? "/player/theory.aspx" : "navigator.aspx?" + parse.stripLeadingCharacter(jlib.net.HTTP.stripQueryStringParameters(Request.Url.Query + "&frame=true", "top", "__url"), "?"));
            lIFrame.Text = "<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen id=\"content\" marginwidth=\"0\" marginheight=\"0\" frameborder=\"0\" vspace=\"0\" hspace=\"0\" src=\"" + URL + "\"></iframe>";
            lScript.Text += "var iUserID=" + m_oActiveUser.ID + ";\n var sFrameURL='" + URL + "'; var sSCORMServer='" + Request.ApplicationPath + "/player/scorm.aspx';";
            //(Page.FindControl("cBody") as System.Web.UI.HtmlControls.HtmlContainerControl).Style["height"]
        }
        public void Page_PreRender(object sender, EventArgs e)
        {
            //if (ExamSession.Count > 0) 
            if ((Page.Master as Inc.Master.MyMetier).HelloBarScriptVisibility)
            {
                List<int> ids = new List<int>();
                Request["class_id"].Str().Split(',').ToList().ForEach(x => ids.AddIfNotExist(x.Int()));
                Request["activity_ids"].Str().Split(',').ToList().ForEach(x => ids.AddIfNotExist(x.Int()));
                for (int i = 0; i < ids.Count; i++)
                {
                    if (ids[i] > 0)
                    {
                        Util.Phoenix.session.RequestResult result = m_oActiveUser.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ActivityGet, ids[i]), true, true);
                        if (!result.Error && new Util.Classes.Activity(result.DataObject, m_oActiveUser).ArticleType == Util.Data.MetierClassTypes.Exam)
                        {
                            (Page.Master as Inc.Master.MyMetier).HelloBarScriptVisibility = false;
                            break;
                        }
                    }

                }
            }

        }


    }
}