﻿using System;
using System.Xml;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.components;
using jlib.db;
using jlib.functions;
using System.Text;
using System.Security.Cryptography;
using session = Phoenix.LearningPortal.Util.Phoenix.session;

namespace Phoenix.LearningPortal
{

    public partial class Index : jlib.components.webpage
    {
        Util.Classes.user m_oActiveUser;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void Page_PreInit(object sender, EventArgs e)
        {
            base.Page_PreInit(sender, e);

            m_oActiveUser = (Page.Master as Inc.Master.MyMetier).ActiveUser;
            if (System.Configuration.ConfigurationManager.AppSettings["site.portal2"].Bln() && m_oActiveUser != null && m_oActiveUser.DistributorId > 0 && m_oActiveUser.DistributorId != 10000)
                Response.Redirect("portal2/#/portal/home");

            if (jlib.net.HTTP.getCookieValue(Request.Cookies["session-language"]) != ""
                )
            {
                this.Language = jlib.net.HTTP.getCookieValue(Request.Cookies["session-language"]);

                session.RequestResult result = m_oActiveUser.PhoenixSession.PhoenixGet(session.Queries.UserGetCurrent);
                if (!result.Error && Util.MyMetier.getLanguageID(this.Language) != null)
                {
                    if (Util.MyMetier.getLanguageID(this.Language).Value != convert.cInt(result.DataObject.User.PreferredLanguageId))
                    {
                        result.DataObject.User.PreferredLanguageId = Util.MyMetier.getLanguageID(this.Language).Value;
                        m_oActiveUser.PhoenixSession.PhoenixRequest("PUT", session.Queries.UserPut, result.DataObject.ToString(), false, false);
                        m_oActiveUser.PhoenixSession.PhoenixClearCache();
                        m_oActiveUser = Util.Classes.user.getUser(m_oActiveUser.ID, true);
                    }
                }
                Response.Cookies["session-language"].Expires = DateTime.Now.AddYears(-10);
            }
            if (m_oActiveUser.ID.Int() > 0 && jlib.net.HTTP.getCookieValue(Request.Cookies["user.id"]).Int() != m_oActiveUser.ID.Int()) Response.Cookies.Set(new System.Web.HttpCookie("user.id", m_oActiveUser.ID.Str()));
        }

        private class ActivityGroupingRule
        {
            public List<string> RequiredArticleCodes = new List<string>();
            public List<string> OptionalArticleCodes = new List<string>();
            private List<string> m_oArticleCodeOrder;
            public string Title = "";

            //If ArticleCodeOrder is null, then we sort by RequiredArticleCodes, OptionalArticleCodes. If it's an empty list, we don't override default sort
            public List<string> ArticleCodeOrder
            {
                get
                {
                    if (m_oArticleCodeOrder == null)
                    {
                        m_oArticleCodeOrder = new List<string>();
                        m_oArticleCodeOrder.AddRange(RequiredArticleCodes);
                        m_oArticleCodeOrder.AddRange(OptionalArticleCodes);
                    }
                    return m_oArticleCodeOrder;
                }
            }

            public ActivityGroupingRule(List<string> requiredCodes, List<string> optionalCodes, List<string> codeOrder, string title)
            {
                RequiredArticleCodes = requiredCodes;
                OptionalArticleCodes = optionalCodes;
                m_oArticleCodeOrder = codeOrder;
                Title = title;
            }
        }
        private List<Util.Classes.Enrollment> GetEnrollmentsForCodes(List<Util.Classes.Enrollment> enrollmentsToCheck, List<string> codes)
        {
            List<double> processedClassrooms = null;
            return GetEnrollmentsForCodes(enrollmentsToCheck, codes, ref processedClassrooms);
        }
        private List<Util.Classes.Enrollment> GetEnrollmentsForCodes(List<Util.Classes.Enrollment> enrollmentsToCheck, List<string> codes, ref List<double> processedClassrooms)
        {
            List<Util.Classes.Enrollment> enrollments = new List<Util.Classes.Enrollment>();
            for (int x = 0; x < enrollmentsToCheck.Count; x++)
            {
                if ((processedClassrooms == null || !processedClassrooms.Contains(enrollmentsToCheck[x].ActivityId)) && (codes.FirstOrDefault(code => code == enrollmentsToCheck[x].VersionNumber || (code.EndsWith("*") && enrollmentsToCheck[x].VersionNumber.Str().StartsWith(parse.stripEndingCharacter(code, "*")))) != null))
                {
                    if (processedClassrooms != null) processedClassrooms.Add(enrollmentsToCheck[x].ActivityId);
                    enrollments.Add(enrollmentsToCheck[x]);
                }
            }
            return enrollments;
        }

        public void Page_Init(object sender, EventArgs e)
        {

            log.Info("Logs on index.aspx");
            lCourseCatalog.Visible = m_oActiveUser.AllowEnroll && m_oActiveUser.AvailableCourses.Count > 0 && jlib.helpers.translation.translate(this.Language, "", this, "more-courses-available").IsNotNullOrEmpty();
            lCourseCatalog.Text = jlib.helpers.translation.translate(this.Language, "", this, "more-courses-available") + " - " + jlib.helpers.translation.translate(this.Language, "", this, "more-courses-add");


            List<ActivityGroupingRule> activityGroupingRules = new List<ActivityGroupingRule>();
            //ITPP
            activityGroupingRules.Add(new ActivityGroupingRule(new List<string>() { "PM26C*", "PM26E*", "EPM22EC*", "PM27C*", "PM27E*", "PM22E*" }, new List<string>() { "TPM22", "ECE01CX*" }, null, "ITPP"));
            //PMP Prep
            activityGroupingRules.Add(new ActivityGroupingRule(new List<string>() { "CE10*", "TPM27*" }, new List<string>() { }, null, "CAPM+PMP"));

            lHeading.Text = String.Format(jlib.helpers.translation.translate(this.Language, "", this, "heading"), m_oActiveUser.FirstName);
            (Page.Master as Inc.Master.MyMetier).Section = Inc.Master.MyMetier.SiteSection.Overview;


            var noShowActivities = new List<Util.Classes.ActivityBase>();

            //Get distinct list of all Sets user is enrolled
            List<Util.Classes.EnrollmentSet> enrolledSets = m_oActiveUser.Enrollments.Select(enrollment => enrollment.ActivitySetId).Distinct().Select(id =>
            {
                return new Util.Classes.EnrollmentSet(m_oActiveUser.Enrollments.FindAll(enrollment => enrollment.ActivitySetId == id).Select(enrollment => { return enrollment as Util.Classes.ActivityBase; }).ToList());
            }).ToList();

            enrolledSets.ForEach(set =>
            {

                //Remove all Imported History
                set.ActivityBases.RemoveAll(activity => activity.IsImportedHistory);

                //Remove all un-supported activityTypes
                set.ActivityBases.RemoveAll(activity => activity.ArticleType != Util.Data.MetierClassTypes.Exam && activity.ArticleType != Util.Data.MetierClassTypes.Classroom && activity.ArticleType != Util.Data.MetierClassTypes.ELearning);

                //Remove all activities user is not enrolled in
                set.ActivityBases.RemoveAll(activity => m_oActiveUser.Enrollments.FirstOrDefault(enrollment => enrollment.EnrollmentId == activity.EnrollmentId) == null);

                //remove no-shows from enrolled Sets (in case user is enrolled in the same activity through multiple sets
                set.ActivityBases.RemoveAll(activity => noShowActivities.FirstOrDefault(enrollment => enrollment.EnrollmentId == activity.EnrollmentId) != null);

                //Add all no-shows to list
                noShowActivities.AddRange(set.ActivityBases.Where(activity => { return m_oActiveUser.Enrollments.First(enrollment => enrollment.EnrollmentId == activity.EnrollmentId).ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.NoShow; }));

                //remove no-shows from enrolled Sets
                set.ActivityBases.RemoveAll(activity => noShowActivities.FirstOrDefault(enrollment => enrollment.EnrollmentId == activity.EnrollmentId) != null);
            });

            //Remove no-show activities where user has been enrolled in new activity
            noShowActivities.RemoveAll(activity => m_oActiveUser.Enrollments.FirstOrDefault(enrollment => enrollment.ArticleNumber == activity.ArticleNumber && enrollment.ParticipantStatusCodeValue != Util.Classes.Enrollment.ParticipantStatusCodes.NoShow) != null);

            //Sort sets based on rule where set with activity with oldest startdate goes first
            enrolledSets.Sort((a, b) =>
            {
                if (a.ActivityBases.Count == 0) return 1;
                if (b.ActivityBases.Count == 0) return -1;
                var aFirstDate = a.ActivityBases.Min(activity => activity.ActivityStart == null ? DateTime.MaxValue : activity.ActivityStart.Dte());
                var bFirstDate = b.ActivityBases.Min(activity => activity.ActivityStart == null ? DateTime.MaxValue : activity.ActivityStart.Dte());
                return aFirstDate.CompareTo(bFirstDate);
            });

            //Remove all sets that don't contain activities
            enrolledSets.RemoveAll(set => set.ActivityBases.Count == 0);

            //Convert to DisplaySets and DisplayActivities
            var displaySets = enrolledSets.Select(set => { return new Util.Classes.DisplaySet(set).Populate(m_oActiveUser, this); }).ToList();

            List<Util.Classes.DisplaySet> currentSets = displaySets.Where(set => set.Activities.Where(activity => !activity.Completed).FirstOrDefault() != null).ToList();
            List<Util.Classes.DisplaySet> completedSets = displaySets.Where(set => !currentSets.Contains(set)).ToList();
            var noShowFakeSet = new Util.Classes.EnrollmentSet(null);
            noShowFakeSet.ActivityBases = noShowActivities;

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "var __Courses=" + jlib.functions.json.DynamicJson.Serialize(new { Current = currentSets, Completed = completedSets, NoShow = new Util.Classes.DisplaySet(noShowFakeSet).Populate(m_oActiveUser, this) }) + ";", true);


            lMedalsBronze.setValue(m_oActiveUser.MedalCount[2]);
            lMedalsSilver.setValue(m_oActiveUser.MedalCount[1]);
            lMedalsGold.setValue(m_oActiveUser.MedalCount[0]);

            lSubHeading.Text = String.Format(jlib.helpers.translation.translate(this.Language, "", this, "sub-heading") + (m_oActiveUser.UserRank > 0 ? jlib.helpers.translation.translate(this.Language, "", this, "sub-heading-competition") : ""), m_oActiveUser.ElearnEnrollments, m_oActiveUser.ElearnLessonsCompleted, m_oActiveUser.ElearnLessonsAvailable, m_oActiveUser.MedalCount[0] + m_oActiveUser.MedalCount[1] + m_oActiveUser.MedalCount[2], m_oActiveUser.UserRank, m_oActiveUser.MedalCount[0] + m_oActiveUser.MedalCount[1] + m_oActiveUser.MedalCount[2] + m_oActiveUser.ElearnEnrollments - m_oActiveUser.ElearnEnrollmentsCompleted);
            pNagDialog.Visible = Request["NagUserForExamCompetence"].Bln();
        }
    }
}