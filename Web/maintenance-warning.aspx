﻿<%@ Page Title="myMetier Overview" Language="C#" AutoEventWireup="true" Inherits="Phoenix.LearningPortal.MaintenanceWarning" MasterPageFile="~/inc/master/mymetier.master" Codebehind="maintenance-warning.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">

<div id="wrapper-introduction">  
  <common:label runat="server" ID="lHeading" Tag="h1">The site is unavailable</common:label>
  <common:label runat="server" ID="lSubHeading" Tag="h3">We are performing a scheduled upgrade, and the system will be unavailable until 8pm CET on December 16th. Please check back then.</common:label>
  <div class="clear"></div>
</div>


<script language="javascript">
    if (fnGetIEVersion() > 0) {
        $(".nine-rounded").each(function (i, o) {
            $(o).append("<div class='lt' />").append("<div class='rt' />").append("<div class='lb' />").append("<div class='rb' />");
        });
    }
    if (fnGetFFVersion() > 0) $(".miniprogrambox .empty").css("top", "-33px");
    
</script>
</asp:Content>