﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Linq;
using System.Collections;
using System.Collections.Specialized;
using jlib.functions;
using jlib.net;
using jlib.db;

namespace Phoenix.LearningPortal.Behind
{

    public partial class Index : jlib.components.webpage
    {
        public void Page_Load(object sender, EventArgs e)
        {
            Util.Classes.user m_oActiveUser = (Page.Master as Inc.Master.Default).ActiveUser;
            if (m_oActiveUser != null) Util.Cms.RecalculateUserPermission(m_oActiveUser);
        }
    }

}