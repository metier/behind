﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.functions;
using System.Text;
using System.Security.Cryptography;
using session = Phoenix.LearningPortal.Util.Phoenix.session;
namespace Phoenix.LearningPortal
{
    public partial class MaintenanceWarning : jlib.components.webpage
    {
        public void Page_PreInit(object sender, EventArgs e)
        {
            (Page.Master as Inc.Master.MyMetier).DisableLoginRedirect = true;
            if (!System.Configuration.ConfigurationManager.AppSettings["maintenance.warning"].Bln()) Response.Redirect("login.aspx");
        }

        public void Page_Init(object sender, EventArgs e)
        {

        }
    }
}