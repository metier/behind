﻿<%@ Page Title="myMetier Login" Language="C#" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Login" MasterPageFile="~/inc/master/mymetier.master" Codebehind="login.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>myMetier - Log in</title>
<link type="text/css" rel="stylesheet" href="~/css/metier-lms.css" />
<link type="text/css" rel="stylesheet" href="~/css/metier-common.css?4" />
<script src="./inc/js/jquery.js?3" type="text/javascript"></script>
<script src="./inc/js/functions.js?14" type="text/javascript"></script>
<script type="text/javascript" src="./inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="~/inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.css" />
</head>

<body class="login">
<common:form runat="server" ID="frmMain">


<div class="logo"></div>
<common:label runat="server" ID="lChooseLanguage" cssclass="choose-language" Tag="div"><common:dropdown runat="server" ID="dLanguage" OnClientChange="window.location=(window.location.href+'?').split('?')[0]+'?language='+this.value;">
<asp:ListItem Value="en" Text="English" />
<asp:ListItem Value="no" Text="Norsk" />
<asp:ListItem Value="da" Text="Dansk" />
<asp:ListItem Value="sv" Text="Svenska" />
<asp:ListItem Value="fr" Text="Français" />
<asp:ListItem Value="de" Text="Deutsch" />
<asp:ListItem Value="pt" Text="Portuguese" />
<asp:ListItem Value="it" Text="Italiano" />
<asp:ListItem Value="es" Text="Español" />
<asp:ListItem Value="pl" Text="Polski" />
<asp:ListItem Value="nl" Text="Nederlands" />
</common:dropdown>
</common:label>
<div class="clear"></div>

<div id="wrapper-loginbox">
  <div class="container-login">
  <common:label runat="server" TranslationKey="heading" Tag="h1" />    
    <common:label runat="server" ID="lWrongUsernamePassword" Tag="div" style="color:red; font-style:italic" TranslationKey="wrong-credentials" Visible="false" />
    <common:label runat="server" ID="lAccountExpired" Tag="div" style="color:red; font-style:italic" TranslationKey="account-expired" Visible="false" />
    <common:label runat="server" ID="lTimeout" Tag="div" style="color:red; font-style:italic" TranslationKey="timeout" />    
    <common:label runat="server" TranslationKey="username" Tag="p" />        
    <p>
      <label for="textfield"></label>
      <input name="username" type="text" class="formfield-login" id="textfield" tabindex="1" />
    </p>
    <p><common:label runat="server" TranslationKey="password" /> <a class="passwordreclaim" href="javascript:void(0)" onclick="fnPasswordReminderInitialize();" tabindex="4"><common:label runat="server" TranslationKey="password-forgotten" /></a></p>
    <p>
      <input type="password" name="password" class="formfield-login" id="textfield2" tabindex="2" />
    </p>
  </div>
  <div class="container-loginbutton actionbutton">  
    <a href="javascript:void(0)" onclick="$(document.forms[0]).submit()" tabindex="3"><common:label runat="server" TranslationKey="button" /></a>   
  </div>
</div>
<div class="reflection-loginbox"></div>
<div id="footer"><common:label runat="server" TranslationKey="disclaimer" /></div>

<common:label runat="server" CssClass="password-reminder" Tag="div" id="lPasswordReminder" style="display:none">  
<common:ajaxpane runat="server" id="aContainer">
    <common:hidden runat="server" ID="hHashValue" CssClass="urlhash" />
  <common:label runat="server" TranslationKey="password-reminder-heading" Tag="h1" Text="Password reminder" />    
    <common:label runat="server" ID="lUsernameNotFound" Tag="div" style="color:red; font-style:italic;padding-bottom:10px" TranslationKey="password-reminder-not-found" Visible="false" />    
    <common:label runat="server" ID="lPasswordReminderSent" Tag="div" style="color:red; font-style:italic;padding-bottom:10px" TranslationKey="password-reminder-sent" Visible="false" />    
    <common:label runat="server" TranslationKey="password-reminder-username" Tag="p" ID="lPasswordUsernameLabel" />        
    <p>      
      <common:textbox runat="server" id="tPasswordUsername" cssclass="formfield-login password-username" />
    </p>
      
  <div class="actionbutton">  
    <a href="javascript:void(0)" runat="server" id="hPasswordReminderSubmit" class="password-reminder-submit" onclick="AJAXControlAction($('.password-username').attr('name'),null,null);" onclick1="$.get(window.location.href.split('?')[0]+'?action=reminder&username='+escape($('#password-username').val()));" tabindex="10"><common:label ID="Label3" runat="server" TranslationKey="password-reminder-submit" /></a>    
    <a href="javascript:void(0)" runat="server" id="hPasswordReminderClose" visible="false" class="password-reminder-close" onclick="$('.ui-dialog-content').dialog().dialog('close');" tabindex="10">OK</a>    
  </div>
  <script>
     
      $(function () {
          fnEnableJSEffects();
          $(document).on("keypress", ".password-reset input", function (e) { if (e.which == 13) { $(".password-reset-submit").click(); return false; } });
          $(document).on("keypress", ".password-reminder input", function (e) { if (e.which == 13) { $(".password-reminder-submit").click(); return false; } });          
          var hashVal = cStr($("input.urlhash").val());
          if (hashVal && !window.location.hash) window.location.hash = hashVal;          
      });
      $("form").submit(function () {          
          $("input.urlhash").val(window.location.hash);
      });

      <common:label runat="server" id="lPasswordReminderSuccess" />
</script>  
  </common:ajaxpane>
</common:label>

<common:label runat="server" CssClass="password-reset" Tag="div" id="lPasswordReset">  
<common:ajaxpane runat="server" id="Ajaxpane1">
  <common:label runat="server" TranslationKey="password-reset-heading" Tag="h1" Text="Password reset" />    
    <common:label runat="server" ID="lTokenNotFound" Tag="div" style="color:red; font-style:italic;padding-bottom:10px" TranslationKey="token-not-found" Visible="false" />    
    <common:label runat="server" ID="lResetSuccess" Tag="div" style="color:red; font-style:italic;padding-bottom:10px" TranslationKey="password-reset-success" Visible="false" />    
    <common:label runat="server" TranslationKey="password-reminder-username" Tag="p" />        
    <p>      
      <common:textbox runat="server" id="tResetEmail" cssclass="formfield-login email" />
    </p>     
      <common:hidden runat="server" ID="tResetToken" cssclass="formfield-login reset-token" />       
    <common:label runat="server" TranslationKey="password-reset-password" Tag="p" />        
    <p>      
      <common:textbox runat="server" ID="tResetPassword1" cssclass="formfield-login password" TextMode="Password" />
    </p>

    <common:label runat="server" TranslationKey="password-reset-password-retype" Tag="p" />        
    <p>      
      <common:textbox runat="server" ID="tResetPassword" cssclass="formfield-login password" TextMode="Password" />
    </p>
      
  <div class="actionbutton">  
    <a href="javascript:void(0)" class="password-reset-submit" onclick="if($('.password-reset').find('.password').val().length<4 || $('.password-reset').find('.password:eq(0)').val()!=$('.password-reset').find('.password:eq(1)').val()){ alert('Password must be at least 4 characters, and the two password fields must be the same.'); return false; } AJAXControlAction($('.reset-email').attr('name'),null,null);"  tabindex="10"><common:label ID="Label5" runat="server" TranslationKey="password-reset-submit" /></a>    
  </div>
  <script>
      $(function () {
          fnEnableJSEffects();
          $(".reset-email, .reset-token").keypress(function (e) { if (e.which == 13) { $(".password-reset-submit").click(); return false; } });
      });
</script>
  </script>
  </common:ajaxpane>
</common:label>

</common:form>
</body>
<script language="javascript">
    function fnPasswordReminderInitialize() {
        $('.password-reminder').dialog({ modal: true, 'height': 350, 'width': 600, position: {
            my: "center",
            at: "center",
            of: window
        } }).dialog('open').dialog('option', 'title', $(this).html());
        $('.password-reminder').parent().appendTo($('form:first'));
        $('.password-reminder-submit').show();
        $('.password-username').parent().show();
        $('.password-reminder-close').hide();
    }
    
    $('.password-reset').dialog({ modal: true, 'height': 350, 'width': 600, position: {
        my: "center",
        at: "center",
        of: window
    } }).dialog('open').dialog('option', 'title', $(this).html()); $('.password-reset').parent().appendTo($('form:first'));

    if (fnGetIEVersion()>0) { 
        if (fnGetIEVersion()< 9) {
            $("#wrapper-loginbox").css("top", "120px").append("<div class='top'/>");
        }
    }
    if (window.top != window) {
        var oParent = window.top;
        while (oParent != oParent.top) oParent = oParent.top;
        oParent.location.href = window.location.href;
    }

    $(function () {
        fnEnableJSEffects();
        $(document).keypress(function (e) { if (e.which == 13) $(document.forms[0]).submit(); });
        $("#textfield").focus();
        /*
        if ($.cookie('maintenance-alert') == null)
        {
            $("#alert-dialog").dialog({ width: 800, height: 400, title: "Planned Outage: October 4" });
            $.cookie('maintenance-alert', 'hide', { expires: 1 });
        }
        */
        
    });
</script>
<div id="alert-dialog" style="display:none">

<link rel="stylesheet" type="text/css" href="https://mymetier.net/content/public/courseplayer2/splashalert/splashalert.css"/>

<h1 class="alert">Planned Outage: October 4</h1>
<p>
On <b>Wednesday October 4th at 04:00-06:00 CEST</b> (<a style="text-decoration: underline;color: #1d73cc;" href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Metier+Planned+Outage&iso=20171004T02&ah=2" _target="_blank">see this in your timezone</a>), we’re performing major infrastructure upgrades to myMetier. During this period, myMetier.net will unavailable or unstable, so please plan to do your coursework outside of this 2-hour period.

 <br /><br /> 
 Should you have any questions regarding the maintenance, please do not hesitate to contact us at <a style="text-decoration: underline;color: #1d73cc;" href="mailto:contact@metieracademy.com">contact@metieracademy.com</a>.
    <br /><br /> 
</p>
<input style="margin:0 auto 0 auto;" class="knapp" type="reset" name="check" value="Affirmative!" onclick="$('#alert-dialog').dialog('close');">

</div>
<asp:placeholder runat="server" id="pTrackingScript">
<script language="javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-744700-52']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
</asp:placeholder>
</html>
</asp:Content>