﻿<%@ Page Title="myMetier Error" Language="C#" AutoEventWireup="true" Inherits="Phoenix.LearningPortal.Error" MasterPageFile="~/inc/master/mymetier.master" Codebehind="error.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">
<div id="wrapper-introduction">
  <h1 class="standalone">Error</h1>
</div>
<div id="wrapper-settings">
<div class="settingsform">
An error has occurred. Possibly due to a lack of permission to perform a certain activity.
</div>
</div>
</asp:Content>