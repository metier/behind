﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.components;
using jlib.functions;
using System.Text;
using System.Security.Cryptography;

namespace Phoenix.LearningPortal
{
    public partial class Error : jlib.components.webpage
    {
        public void Page_PreInit(object sender, EventArgs e)
        {
            (Page.Master as Inc.Master.MyMetier).DisableLoginRedirect = true;
        }
        public void Page_Load(object sender, EventArgs e)
        {
            if (this.QueryString("debug") == "true")
            {
                Response.Clear();
                Response.Write("IP: " + (Page as jlib.components.webpage).IP + "</br>");
                Response.Write("Request: " + Request.Url.AbsoluteUri + "</br></br>");
                Response.Write("Query String:<ul>");
                foreach (string s in HttpContext.Current.Request.QueryString) Response.Write("<li>" + s + ": " + HttpContext.Current.Request.QueryString[s] + "</li>");
                Response.Write("</ul><br />Form Variables:<ul>");
                foreach (string s in HttpContext.Current.Request.Form) Response.Write("<li>" + s + ": " + HttpContext.Current.Request.Form[s] + "</li>");
                Response.Write("</ul><br />Server Variables:<ul>");
                foreach (string s in HttpContext.Current.Request.ServerVariables) Response.Write("<li>" + s + ": " + HttpContext.Current.Request.ServerVariables[s] + "</li>");

                Response.Write("</ul>");
                Response.Write("<br />Request Input Stream: " + jlib.functions.convert.cStreamToString(HttpContext.Current.Request.InputStream) + "<br />");

                Response.End();
            }
        }
    }
}