﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using jlib.functions;
using jlib.net;
using jlib.db;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Inc.Controls
{
    public partial class Tree : System.Web.UI.UserControl
    {

        private DateTime m_oRevisionDate = DateTime.Now;
        private string m_sItemTypes = "d_folders";

        public static string[] LessonPageTypes = new[] { "splash", "intro", "goals", "reflection", "theory", "examples", "pitfalls", "checklist", "case", "quiz", "complete" };
        public static string[] AllPageTypes = new[] { "splash", "intro", "goals", "reflection", "theory", "examples", "pitfalls", "checklist", "case", "quiz", "complete", "global" };
        public static string[] GetLessonPages(string Language)
        {
            if (Language == "no") return new[] { "Forside", "Innledning", "Mål", "Tankevekkere", "Teori", "Eksempler", "Fallgruver", "Sjekkliste", "Case", "Quiz", "Fullfør leksjon" };
            if (Language == "de") return new[] { "Front Page", "Introduction", "Objectives", "Questions", "In Practice", "Examples", "Stolpersteine", "Checklist", "Fallbeispiel", "Quiz", "Abschluss" };
            if (Language == "da") return new[] { "Forside", "Introduktion", "Mål", "Spørgsmål", "Teori", "Eksempler", "Faldgruber", "Checkliste", "Case", "Quiz", "Fuldfør lektion" };
            if (Language == "sv") return new[] { "Försida", "Inledning", "Mål", "Tankeställare", "Teori", "Exempel", "Fallgropar", "Checklista", "Case", "Quiz", "Slutför lektionen" };
            if (Language == "es") return new[] { "Comienzo", "Introducción", "Objetivos", "Preguntas", "En Teoría", "Ejemplos", "Escollos", "Checklist", "Estudio de caso", "Quiz", "Completar" };
            if (Language == "fr") return new[] { "Début", "Introduction", "Objectifs", "Questions", "En pratique", "Exemples", "Ecueils", "Check-list", "Cas pratique", "Quiz", "Compléter" };
            if (Language == "nl") return new[] { "Voorpagina", "Introductie", "Leerdoel", "Vragen", "In de praktijk", "Praktijkvoorbeelden", "Valkuilen", "Checklist", "Casus", "Quiz", "Einde" };
            if (Language == "pt") return new[] { "Página inicial", "Introdução", "Objetivos", "Perguntas", "Na prática", "Exemplos", "Armadilhas", "Lista de verificação", "Estudo de caso", "Quiz", "Lição em andamento" };
            return new[] { "Front Page", "Introduction", "Objectives", "Questions", "In Practice", "Examples", "Pitfalls", "Checklist", "Case Study", "Quiz", "Complete" };
        }
        public static string GetReferncePageName(string Language)
        {
            if (Language == "no") return "Referanser";
            if (Language == "de") return "References";
            if (Language == "da") return "Referencer";
            if (Language == "sv") return "Referenser";
            if (Language == "es") return "Referencias";
            if (Language == "fr") return "Références";
            if (Language == "nl") return "Referenties";
            return "References";
        }
        public string GetNewVersionCode(string OldVersionCode, int NewVersion)
        {
            OldVersionCode = parse.stripEndingCharacter(OldVersionCode, "-OBSOLETE");
            var newVersionCode = OldVersionCode.SafeSplit("-", 0) + "-" + OldVersionCode.SafeSplit("-", 1).Substring(0, 2) + convert.cPadString(NewVersion, 2, "0", true);            
            return newVersionCode + OldVersionCode.Substring(newVersionCode.Length);                
        }
        public string ItemTypes
        {
            get
            {
                return m_sItemTypes;
            }
            set
            {
                m_sItemTypes = value;
            }
        }
        private Util.Classes.user m_oActiveUser = null;
        public Util.Classes.user ActiveUser
        {
            get
            {
                if (m_oActiveUser == null) m_oActiveUser = Util.Classes.user.getUser(new Util.Phoenix.session().GetUserId());
                return m_oActiveUser;
            }
        }
        public class jtree
        {
            public System.Collections.Generic.List<node> Children = new List<node>();
            public System.Collections.Generic.List<string> ChildIDs = new List<string>();
        }
        public class node
        {
            public node(string sName, string sTableName, string sID, string sContext, bool bHasChildren, int iNumLinks, string sAuxField1)
            {
                Name = sName;
                TableName = sTableName;
                ID = sID;
                Context = sContext;
                HasChildren = bHasChildren;
                NumLinks = iNumLinks;
                AuxField1 = sAuxField1;
            }
            private bool m_bExpanded = false, m_bHasChildren = false;
            private System.Collections.Generic.List<node> m_oChildren = new List<node>();

            public string Name { get; set; }
            public string Context { get; set; }
            public string TableName { get; set; }
            public string AuxField1 { get; set; }


            public jtree Tree { get; set; }
            public node Parent { get; set; }
            public int NumLinks { get; set; }
            public string ID { get; private set; }

            public bool Expanded
            {
                get
                {
                    return m_bExpanded;
                }
                set
                {
                    m_bExpanded = value;
                    if (value)
                    {
                        node oParent = Parent;
                        while (oParent != null)
                        {
                            oParent.Expanded = true;
                            oParent = oParent.Parent;
                        }
                    }
                }
            }
            public bool HasChildren
            {
                get
                {
                    return m_bHasChildren || m_oChildren.Count > 0;
                }
                set
                {
                    m_bHasChildren = value;
                }
            }

            public void addChild(node oNode)
            {
                m_oChildren.Add(oNode);
                oNode.Parent = this;
                oNode.Tree = this.Tree;
                Tree.Children.Add(oNode);
                Tree.ChildIDs.Add(oNode.ID);
            }
            public node getChild(int iIndex)
            {
                if (iIndex < 0 || iIndex >= m_oChildren.Count) return null;
                return m_oChildren[iIndex];
            }
            public int loadHierarchicalData(DataTable oDT, string sTableNameCol, string sIDCol, string sParentCol, string sNameCol, string sContext, string sHasChildrenCol, string sNumLinksCol, string sAuxField1Col)
            {
                sTableNameCol = convert.cStr(sTableNameCol).Trim();
                sHasChildrenCol = convert.cStr(sHasChildrenCol).Trim();
                sParentCol = convert.cStr(sParentCol).Trim();
                sContext = convert.cStr(sContext).Trim();
                sNumLinksCol = sNumLinksCol.Str().Trim();
                sAuxField1Col = sAuxField1Col.Str().Trim();
                int iCounter = 0;
                for (int x = 0; x < oDT.Rows.Count; x++)
                {
                    node oNode = new node(convert.cStr(oDT.Rows[x][sNameCol]), (sTableNameCol == "" ? "" : oDT.Rows[x][sTableNameCol].Str()), convert.cStr(oDT.Rows[x][sIDCol]),
                        sContext == "" ? "" : sContext.StartsWith("~") ? sContext.Substring(1) : convert.cStr(oDT.Rows[x][sContext]),
                        sHasChildrenCol == "" ? false : sHasChildrenCol.StartsWith("~") ? convert.cBool(sHasChildrenCol.Substring(1)) : convert.cBool(oDT.Rows[x][sHasChildrenCol]),
                        sNumLinksCol == "" ? 1 : sNumLinksCol.StartsWith("~") ? sNumLinksCol.Substring(1).Int() : oDT.Rows[x][sNumLinksCol].Int(),
                        sAuxField1Col == "" ? "" : sAuxField1Col.StartsWith("~") ? sAuxField1Col.Substring(1).Str() : oDT.Rows[x][sAuxField1Col].Str());
                    int i = (sParentCol == "" ? -1 : Tree.ChildIDs.IndexOf(convert.cStr(oDT.Rows[x][sParentCol])));
                    if (i == -1) this.addChild(oNode);
                    else Tree.Children[i].addChild(oNode);
                }
                return iCounter;
            }

            public dynamic toJSONObject()
            {
                if (ID == "") throw new Exception("ID Not set for node '" + Name + "'");
                return new
                {
                    data = Name,
                    numlinks = NumLinks,
                    //icon = Context,
                    attr = new { id = convert.cIfValue(TableName, "", ":", "") + ID, context = Context, auxfield1 = AuxField1 },
                    state = (HasChildren ? (Expanded ? "open" : "closed") : null),
                    children = m_oChildren.Count > 0 ? toJSONChildren(null) : null
                };
            }

            public List<object> toJSONChildren(List<object> Children)
            {
                if (Children == null) Children = new List<object>();
                if (m_oChildren.Count == 0) return Children;

                for (int x = 0; x < m_oChildren.Count; x++)
                    Children.Add(m_oChildren[x].toJSONObject());

                return Children;
            }
        }
        private void loadNode(node oNode, string sID)
        {
            if (sID.Contains(":")) sID = sID.Substring(sID.IndexOf(":") + 1);
            ado_helper oData = new ado_helper("sql.dsn.cms");
            DataTable oDT = sqlbuilder.getDataTable(oData, @"with Hierachy(table_name, context, Sequence, ObjectID, parent_id, Name, Aux_field_1, Level, NumParents, NumChildren)as
(           select 'd_folders', c.context, d.sequence, c.ObjectID, d.id1, cast( case when isnull(c.Aux_field_1,'')='' then c.Name when c.context='course' THEN c.Aux_field_1 + ' - ' + c.Name when c.context='content' THEN c.Name + ' [' + c.Aux_field_1 + ']' else c.Name END as varchar(255)) as Name, c.Aux_field_1, 0 as Level, T_NumParents, T_NumChildren from d_folders c, l_row_links d where d.id1=@ID and d.id2=c.ObjectID and d.table1='d_folders' and d.table2='d_folders' and @RevisionDate between d.CreateDate and d.DeleteDate and @RevisionDate between c.CreateDate and c.DeleteDate 
union all   select 'd_folders', c.context, d.sequence, c.ObjectID, d.id1, cast( case when isnull(c.Aux_field_1,'')='' then c.Name when c.context='course' THEN c.Aux_field_1 + ' - ' + c.Name when c.context='content' THEN c.Name + ' [' + c.Aux_field_1 + ']' else c.Name END as varchar(255)) as Name, c.Aux_field_1, ch.Level + 1, T_NumParents, T_NumChildren from d_folders c, l_row_links d inner join Hierachy ch on d.id1 = ch.ObjectID and d.table1='d_folders' and d.table2='d_folders' where d.id2=c.ObjectID  and @RevisionDate between d.CreateDate and d.DeleteDate and @RevisionDate between c.CreateDate and c.DeleteDate
" + (ItemTypes.Contains("d_exams") ? "union all select 'd_exams', 'd_exams', d.sequence, c.ObjectID, d.id1, c.Name, '', ch.Level + 1, 1,0 from d_exams c, l_row_links d inner join Hierachy ch on ch.table_name='d_folders' and d.id1 = ch.ObjectID and d.table1='d_folders' and d.table2='d_exams' where d.id2=c.ObjectID  and @RevisionDate between d.CreateDate and d.DeleteDate and @RevisionDate between c.CreateDate and c.DeleteDate" : "") + @"
" //+ (ItemTypes.Contains("d_content") ? "union all select 'd_content', 'd_content', d.sequence, c.ObjectID, d.id1, c.Name, '', ch.Level + 1, 1,0 from d_content c, l_row_links d inner join Hierachy ch on ch.table_name='d_folders' and d.id1 = ch.ObjectID and d.table1='d_folders' and d.table2='d_content' where d.id2=c.ObjectID  and @RevisionDate between d.CreateDate and d.DeleteDate and @RevisionDate between c.CreateDate and c.DeleteDate" : "") 
    + @"
)
select table_name, ObjectID, parent_id, Name, Aux_field_1, Level, case when NumChildren=0 then 'false ' else 'true' end as hasChildren, context, case when context='content' then (select count(*) from l_row_links a, l_row_links b where a.id1=ObjectID and a.Table1=table_name and a.id2=b.id2 and a.Table2=b.Table2 and @RevisionDate between a.CreateDate and a.DeleteDate and @RevisionDate between b.CreateDate and b.DeleteDate) else NumParents end as numLinks from Hierachy 
where level<2
"
    + (ActiveUser.IsBehindSuperAdmin || ActiveUser.IsBehindContentAdmin ? "" : " AND ObjectID in (SELECT FolderID from t_permissions where UserID=" + ActiveUser.ID + ") ")
    + " order by level, sequence, name", "@ID", sID.Int(), "@RevisionDate", m_oRevisionDate);
            oNode.loadHierarchicalData(oDT, "table_name", "ObjectID", "parent_id", "name", "context", "hasChildren", "numLinks", "Aux_field_1");

        }        
        private void PrintLessonPlayer1(EvoPdf.Document Doc, AttackData.Folder Course, AttackData.Folder Lesson, jlib.helpers.structures.OrderedDictionary<string, int> TOC, int FooterHeight)
        {
            List<string> PagesToPrint = Request["_Pages"].Split(',').ToList();
            List<Util.MyMetier.PrintPage> PrintPages = new List<Util.MyMetier.PrintPage>();
            var FolderList = Lesson.GetChildren<AttackData.Folder>();
            
            FolderList.ForEach(Nav => {
                if (Nav.Context_Value == "lesson" && (Lesson.AuxField1 != "finaltest" || PagesToPrint.FirstOrDefault(PageName => PageName.Contains("FinalTest")) != null)) PrintLessonPlayer1(Doc, Course, Nav, TOC, FooterHeight);
                else if (Nav.Context_Value == "content")
                {
                    bool FinalTestMode = Lesson.AuxField1 == "finaltest";
                    if (FinalTestMode)
                    {
                        if (PagesToPrint.Contains("firstFinalTest") || PagesToPrint.Contains("allFinalTests")) PagesToPrint.AddIfNotExist("quiz");
                        if (PagesToPrint.Contains("firstFinalTestAnswers") || PagesToPrint.Contains("allFinalTestAnswers")) PagesToPrint.AddIfNotExist("quizAnswers");
                    }
                    if (!Nav.AuxField1.IsNullOrEmpty() &&
                        //If FinalTestMode, only print first instance if 'allFinalTest' not found
                        ((!FinalTestMode || PagesToPrint.FirstOrDefault(PageName => PageName.Contains("allFinalTest")) != null) || FolderList.IndexOf(Nav) == FolderList.IndexOf(FolderList.First(f => f.AuxField1 == "quiz"))) &&
                        (PagesToPrint.Contains(Nav.AuxField1) || (Nav.AuxField1 == "quiz" && PagesToPrint.Contains("quizAnswers"))))
                    {
                        string Template = convert.cStr(parse.replaceAll(Nav.AuxField1, "examples", "", "references", "", "pitfalls", ""), "theory");
                                   
                        string Url = System.Configuration.ConfigurationManager.AppSettings["site.url"] + "/player/player.aspx?PageID=" + Nav.ObjectID + "&print=true&CMS=true&preview=true";
                        string PostData = "";
                        if (Nav.AuxField1 != "quiz" || PagesToPrint.Contains("quiz"))
                        {
                            PrintPages.Add(new Util.MyMetier.PrintPage(Url, PostData, Template, FooterHeight));
                            System.Threading.Thread Thread = new System.Threading.Thread(new System.Threading.ThreadStart(PrintPages[PrintPages.Count - 1].work));
                            Thread.Start();
                        }
                        if (Nav.AuxField1 == "quiz" && PagesToPrint.Contains("quizAnswers"))
                        {
                            PrintPages.Add(new Util.MyMetier.PrintPage(Url + "&answers=true", PostData, Template, FooterHeight));
                            System.Threading.Thread Thread = new System.Threading.Thread(new System.Threading.ThreadStart(PrintPages[PrintPages.Count - 1].work));
                            Thread.Start();
                        }                        
                    }
                }            
            });
            int Timeout = 120;
            while (Timeout > 0)
            {
                System.Threading.Thread.Sleep(1000);
                Timeout--;
                bool Completed = true;
                foreach (Util.MyMetier.PrintPage PrintStatus in PrintPages)
                {
                    if (!PrintStatus.Completed) Completed = false;
                }
                if (Completed)
                {
                    string printLog = "==================================================\nPDF Started for user ID: " + (Page as jlib.components.webpage).QueryStringLong("user_id") + " -- " + System.DateTime.Now + "\n";
                    int pageCounter = 0;

                    for (int x = 0; x < PrintPages.Count; x++)
                    {
                        printLog += x + " URL: " + PrintPages[x].Url + "\n";
                        if (PrintPages[x].Document != null && PrintPages[x].Document.Pages.Count > 0)
                        {

                            Doc.AppendDocument(PrintPages[x].Document);
                            pageCounter += PrintPages[x].Document.Pages.Count;
                            printLog += PrintPages[x].HTML;
                        }
                        else
                        {
                            printLog += "TIMEOUT!!!";
                        }
                        printLog += "\n\n-------------------------------------------------\n";
                    }
                    io.write_file(Server.MapPath("/pdf-print-log-admin.txt"), printLog, true);
                    TOC.Add(Lesson.Name, pageCounter);
                    break;
                }
            }
            PrintPages = null;
            GC.Collect();
        }

		//private void PrintLessonPlayer2(EvoPdf.Document Doc, AttackData.Folder Course, AttackData.Folder Lesson, jlib.helpers.structures.OrderedDictionary<string, int> TOC, int FooterHeight, List<string> PagesToPrint)
		//private void PrintLessonPlayer2(EvoPdf.Document Doc, AttackData.Folder Course, int FromLesson, int ToLesson, int FooterHeight, List<string> PagesToPrint)
		//{
		//	Util.MyMetier.PrintPage printPage = null;
		//	var FolderList = Lesson.GetChildren<AttackData.Folder>();

		//	//Print all nested lessons
		//	FolderList.Where(f => f.Context_Value == "lesson").ToList().ForEach(f => PrintLessonPlayer2(Doc, Course, f, TOC, FooterHeight, PagesToPrint));

		//	//Print nested pages. If lesson doesn't contain any pages in PagesToPrint, exit
		//	if ((PagesToPrint?.Count ?? 0) == 0 || !FolderList.Any(f => f.Context_Value == "content" && !PagesToPrint.Contains(f.AuxField1))) return;

		//	var request = new Models.PrintRequest
		//	{
		//		PageId = FolderList.First(f => f.Context_Value == "content").ObjectID,
		//		LessonId = Lesson.ObjectID,
		//		Command = "Page",
		//		Options = new List<string>(),
		//		ScormData = "",
		//		ToEmail = "",
		//		PageTypes = PagesToPrint
		//	};

		//	var url = System.Configuration.ConfigurationManager.AppSettings["site.url"] + "/portal2/#/course/" + Course.ObjectID + "/lesson/" + Lesson.ObjectID + "/page/" + request.PageId + "?PrintRequest=" + Uri.EscapeDataString(Newtonsoft.Json.JsonConvert.SerializeObject(request));
		//	printPage = new Util.MyMetier.PrintPage(url, null, null, FooterHeight, HttpVerb: "GET", ContentType: null);
		//	System.Threading.Thread Thread = new System.Threading.Thread(new System.Threading.ThreadStart(printPage.work));
		//	Thread.Start();

		//	//FolderList.ForEach(Nav => {
		//	//	if (Nav.Context_Value == "lesson" && (Lesson.AuxField1 != "finaltest" || PagesToPrint.FirstOrDefault(PageName => PageName.Contains("FinalTest")) != null)) PrintLessonPlayer2(Doc, Course, Nav, TOC, FooterHeight, PagesToPrint);
		//	//	else if (Nav.Context_Value == "content")
		//	//	{
		//	//		bool FinalTestMode = Lesson.AuxField1 == "finaltest";
		//	//		if (FinalTestMode)
		//	//		{
		//	//			if (PagesToPrint.Contains("firstFinalTest") || PagesToPrint.Contains("allFinalTests")) PagesToPrint.AddIfNotExist("quiz");
		//	//			if (PagesToPrint.Contains("firstFinalTestAnswers") || PagesToPrint.Contains("allFinalTestAnswers")) PagesToPrint.AddIfNotExist("quizAnswers");
		//	//		}
		//	//		if (!Nav.AuxField1.IsNullOrEmpty() &&
		//	//			//If FinalTestMode, only print first instance if 'allFinalTest' not found
		//	//			((!FinalTestMode || PagesToPrint.FirstOrDefault(PageName => PageName.Contains("allFinalTest")) != null) || FolderList.IndexOf(Nav) == FolderList.IndexOf(FolderList.First(f => f.AuxField1 == "quiz"))) &&
		//	//			(PagesToPrint.Contains(Nav.AuxField1) || (Nav.AuxField1 == "quiz" && PagesToPrint.Contains("quizAnswers"))))
		//	//		{
		//	//			string Template = convert.cStr(parse.replaceAll(Nav.AuxField1, "examples", "", "references", "", "pitfalls", ""), "theory");


						
		//	//			if (Nav.AuxField1 != "quiz" || PagesToPrint.Contains("quiz"))
		//	//			{
		//	//				var url = System.Configuration.ConfigurationManager.AppSettings["site.url"] + "/portal2/#/course/" + Course.ObjectID + "/lesson/" + Lesson.ObjectID + "/page/" + Nav.ObjectID + "?PrintRequest=" + Uri.EscapeDataString(Newtonsoft.Json.JsonConvert.SerializeObject(request));
		//	//				PrintPages.Add(new Util.MyMetier.PrintPage(url, null, Template, FooterHeight, HttpVerb: "GET", ContentType: null));
		//	//				System.Threading.Thread Thread = new System.Threading.Thread(new System.Threading.ThreadStart(PrintPages[PrintPages.Count - 1].work));
		//	//				Thread.Start();
		//	//			}
		//	//			if (Nav.AuxField1 == "quiz" && PagesToPrint.Contains("quizAnswers"))
		//	//			{
		//	//				request.Options.Add("Answers");
		//	//				var url = System.Configuration.ConfigurationManager.AppSettings["site.url"] + "/portal2/#/course/" + Course.ObjectID + "/lesson/" + Lesson.ObjectID + "/page/" + Nav.ObjectID + "?PrintRequest=" + Uri.EscapeDataString(Newtonsoft.Json.JsonConvert.SerializeObject(request));
		//	//				PrintPages.Add(new Util.MyMetier.PrintPage(url, null, Template, FooterHeight, HttpVerb: "GET", ContentType: null));
		//	//				System.Threading.Thread Thread = new System.Threading.Thread(new System.Threading.ThreadStart(PrintPages[PrintPages.Count - 1].work));
		//	//				Thread.Start();
		//	//				request.Options.Remove("Answers");
		//	//			}
		//	//		}
		//	//	}
		//	//});
		//	int timeoutMS = 120000;
		//	var startDate = DateTime.Now;
		//	while (timeoutMS > 0)
		//	{
		//		System.Threading.Thread.Sleep(100);
		//		timeoutMS-=100;
		//		//bool Completed = true;
		//		//foreach (Util.MyMetier.PrintPage PrintStatus in PrintPages)
		//		//{
		//		//	if (!PrintStatus.Completed) Completed = false;
		//		//}
		//		if (printPage.Completed)
		//		{
		//			string printLog = "==================================================\nPDF Conversion completed for user ID: " + (Page as jlib.components.webpage).QueryStringLong("user_id") + " -- " + System.DateTime.Now + " (took " + DateTime.Now.Subtract(startDate).TotalMilliseconds + " ms)\n";
		//			int pageCounter = 0;

		//			Doc.AppendDocument(printPage.Document);
		//			pageCounter += printPage.Document.Pages.Count;
		//			printLog += " URL: " + printPage.Url + "\n";

		//			//for (int x = 0; x < PrintPages.Count; x++)
		//			//{
		//			//	printLog += x + " URL: " + PrintPages[x].Url + "\n";
		//			//	if (PrintPages[x].Document != null && PrintPages[x].Document.Pages.Count > 0)
		//			//	{

		//			//		Doc.AppendDocument(PrintPages[x].Document);
		//			//		pageCounter += PrintPages[x].Document.Pages.Count;
		//			//		printLog += PrintPages[x].HTML;
		//			//	}
		//			//	else
		//			//	{
		//			//		printLog += "TIMEOUT!!!";
		//			//	}
		//			//	printLog += "\n\n-------------------------------------------------\n";
		//			//}
		//			io.write_file(Server.MapPath("/pdf-print-log-admin.txt"), printLog, true);
		//			TOC.Add(Lesson.Name, pageCounter);
		//			break;
		//		}
		//	}
		//	printPage = null;
		//	GC.Collect();
		//}
		private void ApplyDynamicToObject(AttackData.BaseTableRecordImpl<AttackData.Context> ParentObject, dynamic Values, AttackData.BaseTableRecordImpl<AttackData.Context> Object, bool LinkToExistingContent)
        {
            foreach (var Field in Object.Fields)
            {
                if (!new List<string>() { "Deleted", "ObjectID", "CreateDate", "DeleteDate" }.Contains(Field.Value.PropertyName) && convert.HasProperty(Values, Field.Value.PropertyName))
                {
                    if (convert.cStr(Values[Field.Value.PropertyName]) != convert.cStr(Field.Value.RawValue))
                        Field.Value.SetChangedValue(Values[Field.Value.PropertyName]);
                }
            }
            var SettingsToDelete = Object.Settings.Select(Setting => Setting as AttackData.Setting).ToList();
            if (Values.IsDefined("Settings"))
            {
                foreach (var DynamicSetting in Values.Settings.Records)
                {
                    string keyName = convert.cStr(DynamicSetting.KeyName);
                    if (ParentObject!=null && keyName.StartsWith("_Folder:"))
                    {
                        keyName = "_Folder:" + ParentObject._CurrentObjectID + keyName.Substring(keyName.IndexOf("|"));
                    }                        
                    var Setting = SettingsToDelete.FirstOrDefault(x => x.KeyName == keyName);
                    if (Setting != null) SettingsToDelete.Remove(Setting);
                    Object.setSetting(keyName, DynamicSetting.Value);
                }
            }
            SettingsToDelete.ForEach(SettingToDelete => SettingToDelete.Delete());

            List<dynamic> DynamicChildren = convert.SafeGetList(Values.Children);

            //Unlink children with IDs that are not linked to course being imported
            var ChildrenToDelete = Object.Children.Select(Child => Child).ToList();

            for (var ChildIndex = 0; ChildIndex < DynamicChildren.Count; ChildIndex++)
            {
                {
                    var DynamicChild = DynamicChildren[ChildIndex];
                    //var CurrentChild = Object.Children.Select(Child => Child.Object2).FirstOrDefault(Child => Child._CurrentObjectID == DynamicChild.ObjectID && Child.GetType().Name == DynamicChild._Type);

                    //Try to match by Id and Type
                    var CurrentChild = ChildrenToDelete.Select(Child => Child.Object2).FirstOrDefault(Child => Child._CurrentObjectID == DynamicChild.ObjectID && Child.GetType().Name == DynamicChild._Type);

                    //If LinkToExistingContent, try to link to existing content (instead of creating new content objects)
                    if (LinkToExistingContent && DynamicChild._Type == "Content" && Data.Content.LoadByPk(convert.cInt(DynamicChild.ObjectID))!= null)
                    {
                        var LinkedContent = Data.Content.LoadByPk(convert.cInt(DynamicChild.ObjectID));
                        Object.addChild(LinkedContent, ChildIndex);                        
                    }
                    else
                    {
                        //Try to match by Type only
                        if (CurrentChild == null)
                            CurrentChild = ChildrenToDelete.Select(Child => Child.Object2).FirstOrDefault(Child => Child.GetType().Name == DynamicChild._Type);

                        if (CurrentChild == null)
                        {
                            if (DynamicChild._Type == "Folder")
                                CurrentChild = new AttackData.Folder();
                            else if (DynamicChild._Type == "Content")
                                CurrentChild = new AttackData.Content();
                        }
                        else
                        {
                            //Remove from deletion list
                            ChildrenToDelete.Remove(ChildrenToDelete.FirstOrDefault(Link => Link.ID2 == CurrentChild._CurrentObjectID));
                        }
                        Object.addChild(CurrentChild, ChildIndex);
                        ApplyDynamicToObject(Object, DynamicChild, CurrentChild, LinkToExistingContent);
                    }
                }
            }
            ChildrenToDelete.RemoveAll(Child => DynamicChildren.FirstOrDefault(DynamicChild => DynamicChild.ObjectID == Child.Object2._CurrentObjectID && DynamicChild._Type == Child.Object2.GetType().Name) != null);
            ChildrenToDelete.ForEach(Child => Child.Delete());
            Object.Save();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            ado_helper oData = new ado_helper("sql.dsn.cms");

            if (Request["mode"] == "popup-postback")
            {

                if (Request["function"] == "popup-import-json")
                {
                    try
                    {
                        AttackData.Folder Object = AttackData.Folder.LoadByPk(Request["ObjectID"].SafeSplit(":", 1).Int());
                        var JSON = jlib.functions.json.DynamicJson.Parse(Request["_JSON"].Str());
                        ApplyDynamicToObject(null, JSON, Object, Request.Form["_TryContentLinking"] != null);

                        Response.Write("//ok");
                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex.Message + "\n" + ex.StackTrace);
                    }
                }
                else if (Request["function"] == "popup-course-from-template")
                {
                    AttackData.Folder ParentFolder = AttackData.Folder.LoadByPk(Request["ObjectID"].SafeSplit(":", 1).Int());
                    AttackData.Folder Course = new AttackData.Folder()
                    {
                        Context_Value = "course"
                    };
                    Course.addParent(ParentFolder);
                    foreach (string Key in Request.Form)
                    {
                        if (Key != "ObjectID")
                        {
                            if (Course.Fields.ContainsPropertyName(Key))
                            {
                                Course.Fields[Key].SetChangedValue(Request.Form[Key]);
                            }
                            else if (Key.StartsWith("Attribute-"))
                            {
                                Course.setSetting(Key.Substring(10), Request.Form[Key]);
                            }
                        }
                    }
                    string[] LessonPages = GetLessonPages(Course.AuxField2);
                    int NumLessons = Request["_NumberOfLessons"].Int();
                    Course.setSetting("Gold Score", Util.MyMetier.CalculateGoldScore(NumLessons).Str());
                    Course.setSetting("Silver Score", Util.MyMetier.CalculateSilverScore(NumLessons).Str());
                    Course.setSetting("Unlock Test", Util.MyMetier.CalculateUnlockTest(NumLessons).Str());
                    for (int x = 0; x < NumLessons; x++)
                    {
                        AttackData.Folder Lesson = new AttackData.Folder()
                        {
                            Name = (x + 1) + ". ",
                            Context_Value = "lesson"
                        };
                        Course.addChild(Lesson);
                        for (int y = 0; y < LessonPageTypes.Length; y++)
                        {
                            AttackData.Folder Page = new AttackData.Folder()
                            {
                                Name = LessonPages[y],
                                AuxField1 = LessonPageTypes[y],
                                Context_Value = "content"
                            };
                            Lesson.addChild(Page);
                            Page.Save();
                        }
                        Lesson.Save();
                    }

                    if (Request.Form["_HasFinalTest"] != null)
                    {
                        AttackData.Folder Lesson = new AttackData.Folder()
                        {
                            Name = "Final Test",
                            AuxField1 = "finaltest",
                            Context_Value = "lesson"
                        };
                        Lesson.setSetting("Weight", Util.MyMetier.CalculateFinalTestWeight(NumLessons).Str());

                        AttackData.Folder Page = new AttackData.Folder()
                        {
                            Name = "quiz_1000.xml",
                            AuxField1 = "quiz",
                            Context_Value = "content"
                        };
                        Lesson.addChild(Page);
                        Page.Save();

                        Course.addChild(Lesson);
                        Lesson.Save();
                    }
                    AttackData.Folder References = new AttackData.Folder()
                    {
                        Name = GetReferncePageName(Course.AuxField2),
                        AuxField1 = "global",
                        Context_Value = "content"
                    };
                    AttackData.Content ReferencesContent = new AttackData.Content()
                    {
                        Name = References.Name,
                        AuxField1 = "global",
                        Type = 1

                    };
                    ReferencesContent.Save();
                    References.addChild(ReferencesContent);
                    References.Save();
                    Course.addChild(References);
                    /*
                    if (Request.Form["_HasDiploma"] != null) {
                        AttackData.Folder Lesson = new AttackData.Folder() {
                            Name = "Diploma",
                            AuxField1="diploma",
                            Context_Value = "lesson"
                        };
                     * Course.addChild(Lesson);                      
                        Lesson.Save();
                    }*/
                    Course.Save();
                    Response.Write("//ok");
                }
                else if (Request["function"] == "popup-course-upgrade")
                {
                    //Todo oppgradering av kundespesifikke kurs
                    //Ved oppgradering av generisk, lag det helt frittsående
                    //Ved oppgradering av kundespesifikk, link de sidene fra oppgradert generisk som gammel kundespesifikk er linket til. Sidene som er frittstående i gammel kundespesifikk skal være frittstående i ny kundespesifikk

                    var Course = AttackData.Folder.LoadByPk(Request["ObjectID"].SafeSplit(":", 1).Int());                 
                    var hasFinalTest = Course.GetChildren<AttackData.Folder>().Any(Lesson => Lesson.Context_Value == "lesson" && Lesson.AuxField1 == "finaltest");

                    var upgradeLog = "<b>Upgrading course (" + Course.ObjectID + ") from [" + Course.AuxField1 + "] to [" + Request["AuxField1"] + "]</b>\n";

                    if (Request["_UpgradeCourse"].Bln())
                    {
                        if (Request["AuxField1"].Str().Trim().IsNullOrEmpty())
                        {
                            Response.Write("alert('Version code must be filled out!')");
                            Response.End();
                        }

                        if (Data.Folders.GetByField(AuxField1: Request["AuxField1"].Str().Trim()).Select().Any())
                        {
                            Response.Write("alert('There is already a course with version code " + Request["AuxField1"] + "!')");
                            Response.End();
                        }


                        AttackData.Folder newCourse;
                        var viewModeMap = new List<ViewModeSettings>();

                        if (Request["_NoNewVersion"].Bln())
                        {
                            newCourse = Course;
                            upgradeLog += "\n* <b>Reusing existing course for upgrade.</b>\n";
                        }
                        else
                        {
                            //Customer specific course
                            if (Course.AuxField1.Str().Split('-').Length > 2)
                            {
                                //If the version is PM-0203NO-BA-RAMBØLL, we should use PM-0203NO-BA as source
                                var parts = Course.AuxField1.Split('-').ToList();
                                parts.RemoveAt(parts.Count - 1);
                                var sourceCourseVersionCode = string.Join("-", parts);
                                var courseMatches = AttackData.Folders.GetByField(AuxField1: sourceCourseVersionCode + "-OBSOLETE").Select();
                                if(courseMatches.Count == 0) courseMatches = AttackData.Folders.GetByField(AuxField1: sourceCourseVersionCode).Select();
                                if (courseMatches.Count > 1)
                                {
                                    Response.Write("alert('I found multiple courses with the course code " + sourceCourseVersionCode + ". Normal upgrade is not possible!')");
                                    Response.End();
                                }
                                var sourceCourse = courseMatches.FirstOrDefault();
                                //var sourceCourse = Course;
                                //while (sourceCourse != null && parse.stripEndingCharacter(sourceCourse.AuxField1, "-OBSOLETE").Split('-').Length != 2)
                                //{
                                //    sourceCourse = AttackData.Folder.LoadByPk(sourceCourse == null ? Course.getSetting("CopiedFrom").Int() : sourceCourse.getSetting("CopiedFrom").Int());
                                //    if (sourceCourse == null) break;
                                //}
                                if (sourceCourse == null)
                                {
                                    Response.Write("alert('I can\\'t find the course with course code " + sourceCourseVersionCode + " to upgrade from. Normal upgrade is not possible!')");
                                    Response.End();
                                }
                                var targetVersionCode = GetNewVersionCode(sourceCourse.AuxField1, 10);
                                var targetCourse = AttackData.Folders.GetByField(AuxField1: targetVersionCode).Select().FirstOrDefault();
                                if (targetCourse == null)
                                {
                                    Response.Write("alert('I can\\'t find the course that " + sourceCourse.AuxField1 + " was converted to. No course exists with version code " + targetVersionCode + "!')");
                                    Response.End();
                                }
                                newCourse = Course.CloneFolder(false, true, false, this.ActiveUser.ID);
                                newCourse.AuxField1 = GetNewVersionCode(Course.AuxField1, 10);

                                var courseLessonMap = new Dictionary<AttackData.Folder, AttackData.Folder>();
                                var coursePageMap = new Dictionary<AttackData.Folder, AttackData.Folder>();
                                var sourceLessons = sourceCourse.GetChildren<AttackData.Folder>();
                                sourceLessons.ForEach(lesson =>
                                {
                                    var targetLesson = targetCourse.GetChildren<AttackData.Folder>().FirstOrDefault(x => x.Name == lesson.Name);
                                    //if (targetLesson == null) targetLesson = targetCourse.GetChildren<AttackData.Folder>()[sourceLessons.IndexOf(lesson)];
                                    courseLessonMap[lesson] = targetLesson;
                                    var pages = lesson.GetChildren<AttackData.Folder>();
                                    pages.ForEach(page => {                                        
                                        if (lesson.AuxField1 == "finaltest")
                                        {
                                            //coursePageMap[page] = courseLessonMap[lesson].GetChildren<AttackData.Folder>()[pages.IndexOf(page)];
                                        }
                                        else
                                        {
                                            if (targetLesson == null)
                                            {
                                                Response.Write("alert('The upgraded generic course " + targetCourse.AuxField1 + " does not contain a lesson called " + lesson.Name + "')");
                                                Response.End();                                                
                                            }
                                            var targetPages = targetLesson.GetCMSPages(page.AuxField1);
                                            if (targetPages.Count == 0 || targetPages.Count > 1) targetPages = targetLesson.GetChildren<AttackData.Folder>().Where(x => x.Name == page.Name).ToList();
                                            if (targetPages.Count == 0 && !new List<string> { "goals", "references", "global" }.Contains(page.AuxField1)) throw new Exception("Can't find page " + page.Name + " under lesson " + lesson.Name);
                                            coursePageMap[page] = targetPages.FirstOrDefault();
                                        }                                        
                                    });
                                });

                                Course.GetChildren<AttackData.Folder>().ForEach(lesson =>
                                {
                                    if (lesson.AuxField1 == "finaltest") return;

                                    var newLesson = lesson.CloneFolder(false, true, false, this.ActiveUser.ID);
                                    var pages = lesson.GetChildren<AttackData.Folder>();
                                    var isIntroAndGoalsLinked = false;

                                    //Todo: Special handling of Intro and Goals page
                                    if (pages.Any(x => x.AuxField1 == "intro") && pages.Any(x => x.AuxField1 == "goals"))
                                    {
                                        var oldGoalsAndIntroContent = lesson.GetChildren<AttackData.Folder>().Where(x => x.AuxField1 == "intro" || x.AuxField1 == "goals").SelectMany(x => x.GetChildren<AttackData.Content>()).ToList();
                                        if (oldGoalsAndIntroContent.All(x =>
                                        {
                                            var path = x.GetPathUntil<AttackData.Folder>(10, new Func<AttackData.Folder, bool>(item => item.ObjectID == sourceCourse.ObjectID));
                                            return (path != null && path.Count > 0);
                                        }))
                                            isIntroAndGoalsLinked = true;
                                    }

                                    pages.ForEach(page =>
                                    {

                                        if (isIntroAndGoalsLinked && page.AuxField1 == "goals")
                                            return;

                                        if (isIntroAndGoalsLinked && page.AuxField1 == "intro")
                                        {                                            
                                            //Get page in old generic course that this page in old customer specific corresponds to. Use shared content for identification
                                            var path = page.GetChildren<AttackData.Content>().First().GetPathUntil<AttackData.Folder>(10, new Func<AttackData.Folder, bool>(item => item.ObjectID == sourceCourse.ObjectID));
                                            var targetPage = coursePageMap.First(x => x.Key.ObjectID == path.Last().ObjectID).Value;
                                            var newPage1 = targetPage.CloneFolder(true, true, true, this.ActiveUser.ID);
                                            newLesson.addChild(newPage1);
                                            return;
                                        }

                                        var newPage = page.CloneFolder(false, true, false, this.ActiveUser.ID);                                        
                                        var pageContent = page.GetChildren<AttackData.Content>();
                                        pageContent.ForEach(content =>
                                        {
                                            AttackData.Content newContent;
                                            //sourceCourse.GetChildren<AttackData.Folder>().Any(x=>x.GetChildren<AttackData.Folder>().Any(y=>y.ObjectID == content.GetParents<AttackData.fol>))
                                            //AttackData.Folder course = content.GetParentUntil<AttackData.Folder>(10, new Func<AttackData.Folder, bool>(item => item.Context_Value == "course"));                                            
                                            //if(allParents.Any(x=>x.ObjectID == sourceCourse.ObjectID))
                                            //var allParents = content.GetAllParents<AttackData.Folder>(5);

                                            //Is this piece of content also in the sourceCourse? If so, link to same version in targetCourse                                            
                                            var path = content.GetPathUntil<AttackData.Folder>(10, new Func<AttackData.Folder, bool>(item => item.ObjectID == sourceCourse.ObjectID));

                                            //Only link content if it is linked between original course and new course AND (page <> { "goals", "intro" } OR isIntroAndGoalsLinked)
                                            if (page.AuxField1 != "reflection" && (path != null && path.Count > 0) && (isIntroAndGoalsLinked || !new List<string> { "goals", "intro" }.Contains(page.AuxField1) ))
                                            {
                                                //var sourceFolder = sourceCourse;
                                                //var targetFolder = targetCourse;
                                                //for (int x = 1; x < path.Count; x++)
                                                //{
                                                //    sourceFolder  = sourceFolder.GetChildren<AttackData.Folder>().First(y => y.ObjectID == path[x].ObjectID);
                                                //    targetFolder = targetFolder.GetChildren<AttackData.Folder>().First(y=>y.Name == sourceFolder.Name);                                                    
                                                //}

                                                var targetFolder = coursePageMap.First(x => x.Key.ObjectID == path.Last().ObjectID).Value;

                                                //If page is references, it might have been removed from the upgraded generic course. If so, don't bring it over.
                                                if (targetFolder == null && new List<string> { "goals", "references", "global" }.Contains(page.AuxField1)) return;
                                                if (targetFolder == null) throw new Exception("Can't find target page for " + lesson.Name + " > " + page.Name);
                                                var targetPageContent = targetFolder.GetChildren<AttackData.Content>();

                                                if (targetPageContent.Count <= pageContent.IndexOf(content))
                                                    throw new Exception("Can't find target content with index " + pageContent.IndexOf(content) + " (" + lesson.Name + " > " + page.Name + " > " + content.Name + ")" );

                                                newContent = targetPageContent[pageContent.IndexOf(content)];
                                                //Locate content in new targetCourse that corresponds to this content from sourceCourse
                                                //var sourceLessonIndex = sourceCourse.GetChildren<AttackData.Folder>().IndexOf()
                                                //var targetLesson = targetCourse.GetChildren<AttackData.Folder>()[]
                                                //newContent = 
                                            }
                                            else
                                            {
                                                newContent = content.CloneContent();
                                                newContent.Save(this.ActiveUser.ID);
                                            }
                                            newPage.addChild(newContent);

                                            //Copy viewmode
                                            var viewSetting = content.Settings.Where(setting => setting.KeyName.StartsWith("_Folder:" + page.ObjectID)).FirstOrDefault();
                                            if (viewSetting != null && viewSetting.Value.IsNotNullOrEmpty()) {
                                                viewModeMap.Add(new ViewModeSettings {
                                                    ViewModeKey = viewSetting.KeyName.Replace(page.ObjectID.Str(), "xxxxxx"),
                                                    ViewModeValue = viewSetting.Value,
                                                    Content = newContent,
                                                    Parent = newPage
                                               });                                                                                                    
                                            }
                                            
                                        });
                                        //Only add if page has content OR if not references
                                        if (newPage.Children.Count > 0 || !new List<string> { "goals", "references", "global" }.Contains(newPage.AuxField1))
                                        {
                                            newLesson.addChild(newPage);
                                            newPage.Save(this.ActiveUser.ID);
                                        }
                                    });
                                    newLesson.Save(this.ActiveUser.ID);
                                    newCourse.addChild(newLesson);
                                });
                            }
                            else
                            {
                                //Request["_LinkContent"].Bln()
                                var linkToContentInOldVersion = false;
                                newCourse = Course.CloneFolder(true, true, linkToContentInOldVersion, this.ActiveUser.ID);
                            }
                            newCourse.addParent(Course.GetParents<Data.Folder>().First());
                            newCourse.setSetting("Player2UpgradedFrom", Course.ObjectID.Str());

                            //Move existing course to Obsolete-folder
                            var obsoleteFolder = Course.GetParents<AttackData.Folder>().ToList().SelectMany(f => f.GetChildren<AttackData.Folder>().Where(x => x.Name.ContainsI("obsolete") && x.Context_Value == "node")).FirstOrDefault();
                            if (obsoleteFolder == null)
                            {
                                obsoleteFolder = new AttackData.Folder
                                {
                                    Name = "~Obsolete",
                                    Context_Value = "node"
                                };
                                obsoleteFolder.addParent(Course.GetParents<AttackData.Folder>().First());
                                obsoleteFolder.Save();
                            }
                            Course.Parents.ForEach(p => p.Delete());
                            Course.addParent(obsoleteFolder);
                            Course.AuxField1 += "-OBSOLETE";
                            Course.Save(this.ActiveUser.ID);
                        }
                        upgradeLog += "\n* <b>Processing goals content</b>\n";
                        newCourse.AuxField1 = Request["AuxField1"].Str();

                        //Settings
                        var settings = Newtonsoft.Json.JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(convert.cStr(newCourse.getSetting("PlayerSettings"), "{}"));
                        var existingPlayerProperty = settings.Properties().Where(x => x.Name == "Player").Select(x => x.Value).FirstOrDefault() as Newtonsoft.Json.Linq.JObject;
                        if (existingPlayerProperty == null)
                        {
                            existingPlayerProperty = new Newtonsoft.Json.Linq.JObject();
                            settings.Add("Player", existingPlayerProperty);
                        }
                        existingPlayerProperty.Remove("Id");
                        existingPlayerProperty.Add("Id", new Newtonsoft.Json.Linq.JValue(2));
                        

                        newCourse.setSetting("PlayerSettings", settings.ToString());

                        
                        //Enumerate through all goals and remove this page type
                        newCourse.GetChildren<AttackData.Folder>().ForEach(x =>
                        {
                            var introPage = x.GetCMSPage("intro");
                            if (introPage == null) return;
                            var introContent = introPage.GetChildren<AttackData.Content>().LastOrDefault();
                            if (introContent == null) return;

                            x.GetCMSPages("goals").ForEach(page =>
                            {

                                page.GetChildren<AttackData.Content>().ForEach(content =>
                                {
                                    introContent.Xml += "<h2>" + page.Name + "</h2>" + content.Xml;
                                    //introPage.addChild(content);
                                });

                                //Todo - check if this page is shared. If so, see if there is a course where the intro page has already been upgraded
                                //page.GetChildren<AttackData.Content>().ForEach(content =>
                                //{
                                //    var newContent = content.Clone() as AttackData.Content;
                                //    newContent.AuxField1 = "intro";
                                //    newContent.setSetting("Player2UpgradedFrom", content.ObjectID.Str());
                                //    newContent.Save();
                                //    introPage.addChild(newContent);
                                //});

                                page.Parents
                                .Where(link => link.ID1 == x.ObjectID && link.Table1 == "d_folders")
                                .ToList()
                                .ForEach(link =>
                                {
                                    link.TupleDelete(this.ActiveUser.ID);
                                });
                            });
                            introContent.Save(this.ActiveUser.ID);

                            //upgradeLog += "** Processing lesson " + x.Name + "\n";
                            //introPage.Save(this.ActiveUser.ID);
                        });



                        newCourse.Save();

                        //Generate final test if creating new course version
                        if (!Request["_NoNewVersion"].Bln() && hasFinalTest)
                            Util.Cms.GenerateFinalTest(newCourse, true);

                        //Check 
                        
                        viewModeMap.ForEach(x => {
                            x.Content.setSetting(x.ViewModeKey.Replace("xxxxxx", x.Parent.ObjectID.Str()),x.ViewModeValue);
                            x.Content.Save(this.ActiveUser.ID);
                        });

                        //throw new Exception("Need to process viewModeMap. For each map, we need to copy key starting with _Folder: -- to get viewmode");
                        //viewModeMap.ToList().ForEach(x => {
                        //    if (x.Value.IsNotNullOrEmpty())
                        //    {
                        //        x.Key.setSetting(x.Value.Replace("xxxxxx", ))
                        //    }
                        //        original.Settings.Where(setting => setting.KeyName.StartsWith("_Folder:" + this.ObjectID)).ToList()
                        //.ForEach(setting => { copy.setSetting(setting.KeyName.Replace("_Folder:" + this.ObjectID, "_Folder:" + Clone.ObjectID), setting.Value); });
                        //});
                    }

                    if (Request["_QuizCompatibilityCheck"].Bln())
                    {
                        upgradeLog += "\n* <b>Processing quiz content</b>\n<div style='color:red'>";
                        Course.GetChildren<AttackData.Folder>()
                            .Where(lesson => lesson.AuxField1 != "finaltest")
                            .ToList()
                            .ForEach(lesson =>
                            {

                                lesson.GetCMSPages("quiz").ForEach(page =>
                                {
                                    page.GetChildren<AttackData.Content>().ForEach(content =>
                                    {


                                        var xmlQuiz = xml.loadXml(content.Xml);
                                        var questions = xmlQuiz.SelectNodes("//question").ToList();

                                        //Check for connect the lines
                                        var questionsWithConnectTheLines = xmlQuiz.SelectNodes("//question[@type='connecttheboxes']").ToList();

                                        //Check for background images
                                        var questionsWithBackgroundImages = xmlQuiz.SelectNodes("//question[@background and string-length(@background)!=0]").ToList();

                                        //Check for absolute positioning    
                                        var questionsWithAbsolutePositioning = xmlQuiz.SelectNodes("//question[@type='draganddrop']/targets[@position='absolute']/..").ToList();

                                        var questionsWithIssues = new List<XmlNode>();
                                        questionsWithIssues.AddRange(questionsWithConnectTheLines);
                                        questionsWithIssues.AddRange(questionsWithBackgroundImages);
                                        questionsWithIssues.AddRange(questionsWithAbsolutePositioning);

                                        if (questionsWithIssues.Count > 0)
                                        {
                                            upgradeLog += "** Lesson: " + lesson.Name + "\n";
                                            questions.ForEach(question =>
                                            {
                                                if (questionsWithIssues.Contains(question))
                                                {
                                                    upgradeLog += "*** Question " + (questions.IndexOf(question) + 1) + ": ";
                                                    if (questionsWithConnectTheLines.Contains(question))
                                                        upgradeLog += "Connect the lines, ";

                                                    if (questionsWithBackgroundImages.Contains(question))
                                                        upgradeLog += "Background image, ";

                                                    if (questionsWithAbsolutePositioning.Contains(question))
                                                        upgradeLog += "Drag and drop with absolute positioning";

                                                    upgradeLog += "\n";
                                                }

                                            });
                                        }
                                    });

                                });
                            });
                        upgradeLog += "</div>";
                    }
                    

                    upgradeLog += "Upgrading course completed\n\n";

                    io.write_file(Server.MapPath("/course-upgrade-log.txt"), DateTime.Now + " :: " + parse.stripHTML(upgradeLog), true);
                    Response.Write("$('#_UpgradeCourseError').show().html('" + parse.replaceAll(upgradeLog,"\n","<br />","\r","","'","\"") + "');");
                }
                else if (Request["function"] == "popup-export-course")
                {
                    AttackData.Folder Course = AttackData.Folder.LoadByPk(Request["ObjectID"].SafeSplit(":", 1).Int());
                    string copyrightMessage = Course.getSetting("Copyright");

                    int footerHeight = Util.MyMetier.GetPDFFooterHeight(copyrightMessage);
                    var playerVersion = 1;

                    playerVersion = convert.cInt(convert.SafeGetProperty(jlib.functions.json.DynamicJson.Parse(convert.cStr(Course.getSetting("PlayerSettings") ,"{}")),"Player","Id"),1);


                    List<AttackData.Folder> Lessons = Course.GetChildren<AttackData.Folder>();
                    EvoPdf.Document Doc = new EvoPdf.Document();
                    Doc.LicenseKey = "72FyYHVwYHBgd25wYHNxbnFybnl5eXlgcA==";//"31FCUEVAUEFHQVBGXkBQQ0FeQUJeSUlJSVBA"
                    jlib.helpers.structures.OrderedDictionary<string, int> TOC = new jlib.helpers.structures.OrderedDictionary<string, int>();
                    bool HasPrintedFinalTest = false;
                    var FinalTestLesson = Lessons.FirstOrDefault(Lesson => Lesson.Context_Value == "lesson" && Lesson.AuxField1 == "finaltest");
					if (playerVersion == 1)
					{
						for (int x = Math.Max(0, Request["_From"].Int() - 1); x < (Request["_To"].Int() > 0 ? Math.Min(Request["_To"].Int(), Lessons.Count) : Lessons.Count); x++)
						{
							if (Lessons[x].Context_Value != "content")
							{
								if (Lessons[x].AuxField1 != "finaltest" || Request["_Pages"].Str().Contains("FinalTest"))
								{
									PrintLessonPlayer1(Doc, Course, Lessons[x], TOC, footerHeight);
									if (Lessons[x].AuxField1 == "finaltest") HasPrintedFinalTest = true;
								}
							}
						}
						if (!HasPrintedFinalTest && FinalTestLesson != null && Request["_Pages"].Str().Contains("FinalTest"))						
							PrintLessonPlayer1(Doc, Course, FinalTestLesson, TOC, footerHeight);
							
					}
					else if(playerVersion == 2)
					{
						var pagesToPrint = Request["_Pages"].Str().Split(',').ToList();

						var request = new Models.PrintRequest
						{						
							PageId = Lessons[0].GetChildren<AttackData.Folder>().Where(p=>p.Context_Value == "content").Select(p=>p.ObjectID).FirstOrDefault().Int(),
							LessonId = Lessons[0].ObjectID,
							Command = "Page",
							Options = new List<string>(),
							ScormData = "",
							ToEmail = "",
							PageTypes = pagesToPrint,
							FromLesson = Math.Max(0, Request["_From"].Int() - 1),
							ToLesson = Math.Max(0, (Request["_To"].Int() == 0 ? 99 : Request["_To"].Int() -1 )),
							Source = "CMS",
							BaseUrl = System.Configuration.ConfigurationManager.AppSettings["print.base.url"]
						};



						var url = convert.cStr(System.Configuration.ConfigurationManager.AppSettings["print.base.url"], System.Configuration.ConfigurationManager.AppSettings["site.url"]) + "/portal2/#/course/" + Course.ObjectID + "/lesson/" + request.LessonId + "/page/" + request.PageId + "?PrintRequest=" + Uri.EscapeDataString(Newtonsoft.Json.JsonConvert.SerializeObject(request));
						var printPage = new Util.MyMetier.PrintPage(url, null, null, footerHeight, HttpVerb: "GET", ContentType: null, AutoTOC: pagesToPrint.Contains("toc"));
						printPage.work();
						

						if ((printPage.Document?.Pages?.Count ?? 0) != 0)
							Doc.AppendDocument(printPage.Document);
						
						printPage = null;
						GC.Collect();

					}

					if (playerVersion == 1 && ("," + Request["_Pages"] + ",").Contains(",toc,"))
                    {
                        var oConverter = new EvoPdf.PdfConverter();
                        oConverter.PdfDocumentOptions.PdfPageSize = EvoPdf.PdfPageSize.A4;
                        string HTML = "<html><head><link type='text/css' rel='stylesheet' href='https://mymetier.net/learningportal/player/css/tempstyle.css' /></head><body><div style='margin:200px auto;width:600px'><table style='width:600px'><tr><td colspan='2'><h1>Table of contents</h1></td></tr>";
                        int pageCounter = 2;
                        for (int x = 0; x < TOC.Count; x++)
                        {
                            HTML += "<tr><td>" + TOC.ToList()[x].Key + "</td><td style='text-align:right'>" + pageCounter + "</td></tr>";
                            int prevPageCounter = pageCounter;
                            pageCounter += TOC[x];
                            TOC[x] = prevPageCounter;
                        }
                        oConverter.HtmlElementsMappingOptions.HtmlElementSelectors = new[] { "tr" };
                        Doc.InsertPage(0, oConverter.GetPdfDocumentObjectFromHtmlString(HTML + "</table></div></body></html>").Pages[0]);
                        for (int x = 1; x < oConverter.HtmlElementsMappingOptions.HtmlElementsMappingResult.Count; x++)
                        {
                            var map = oConverter.HtmlElementsMappingOptions.HtmlElementsMappingResult[x];
                            if (Doc.Pages.Count > 0 && map.PdfRectangles.Length > 0 && (TOC[x - 1] - 1) > -1 && (TOC[x - 1] - 1) < Doc.Pages.Count)
                                Doc.Pages[0].AddElement(new EvoPdf.InternalLinkElement(map.PdfRectangles[0].Rectangle, new EvoPdf.ExplicitDestination(Doc.Pages[TOC[x - 1] - 1])));
                        }
                    }
                    Doc.DocumentInformation.Author = "Metier OEC AS";
                    Doc.DocumentInformation.CreationDate = DateTime.Now;
                    Doc.DocumentInformation.Title = Course.Name + " [" + Course.AuxField1 + "]";
                    //Util.MyMetier.PDFAddFooter(Doc, ("," + Request["_Pages"] + ",").Contains(",toc,"), copyrightMessage, true, true);
                    Util.MyMetier.PDFAddFooter(Doc, false, copyrightMessage, true, true);
                    string sFileName = io.getUniqueFileName(System.Configuration.ConfigurationManager.AppSettings["temp.path"] + "\\" + String.Format("{0:yyyy-MM-dd HH-mm-ss}", DateTime.Now) + "MyMetier.pdf");
                    Doc.Save(sFileName);
                    Response.Write("redirect:" + Request.ApplicationPath + "/download.aspx?file=" + sFileName.Substring(sFileName.LastIndexOf("\\") + 1));
                }
                Response.End();
            }
            else if (Request["action"] == "lesson-from-template")
            {
                Response.ContentType = "application/x-javascript";
                AttackData.Folder Course = AttackData.Folder.instantiateObject(Request["Object"]) as AttackData.Folder;
                string[] LessonPages = GetLessonPages(Course.AuxField2);
                AttackData.Folder Lesson = new AttackData.Folder()
                {
                    Name = (Course.GetChildren<AttackData.Folder>().Count + 1) + ". ",
                    Context_Value = "lesson"
                };
                Course.addChild(Lesson);
                for (int y = 0; y < LessonPageTypes.Length; y++)
                {
                    AttackData.Folder Page = new AttackData.Folder()
                    {
                        Name = LessonPages[y],
                        AuxField1 = LessonPageTypes[y],
                        Context_Value = "content"
                    };
                    Lesson.addChild(Page);
                    Page.Save();
                }
                Lesson.Save();
                Course.Save();
                Response.Write("$('#tree').jstree('refresh', $('#" + parse.replaceAll(Request["Object"], ":", "\\\\:") + "'));");
                Response.End();
            }
            else if (Request["action"] == "publish-to-cache")
            {
                Response.ContentType = "application/x-javascript";
                if (System.Configuration.ConfigurationManager.AppSettings["page.cache.path"].IsNullOrEmpty())
                {
                    Response.Write("alert('Page caching is not enabled')");
                }
                else
                {
                    AttackData.Folder Course = AttackData.Folder.instantiateObject(Request["Object"]) as AttackData.Folder;
                    List<string> URLs = new List<string>();
                    Course.GetChildren<AttackData.Folder>().ForEach(Lesson =>
                    {
                        Lesson.GetChildren<AttackData.Folder>().ForEach(Page =>
                        {
                            if (Page.Context_Value == "lesson")
                            {
                                Page.GetChildren<AttackData.Folder>().ForEach(Page1 =>
                                {
                                    if (Page1.Context_Value == "content" && !",splash,quiz,".Contains(Page.AuxField1)) URLs.Add(System.Configuration.ConfigurationManager.AppSettings["site.url"] + "/player/" + convert.cStr(parse.replaceAll(Page1.AuxField1, "examples", "", "global", "", "disclaimer", ""), "theory") + ".aspx?write-cache=true&PageID=" + Page1.ObjectID);
                                });
                            }
                            else if (Page.Context_Value == "content" && !",splash,quiz,".Contains(Page.AuxField1))
                            {
                                URLs.Add(System.Configuration.ConfigurationManager.AppSettings["site.url"] + "/player/" + convert.cStr(parse.replaceAll(Page.AuxField1, "examples", "", "global", "", "disclaimer", ""), "theory") + ".aspx?write-cache=true&PageID=" + Page.ObjectID);
                            }
                        });
                    });

                    URLs.ForEach(x => HTTP.HtmlGet(x));

                    //Course

                    Response.Write("alert('Course succesfully written to cache')");
                }
                Response.End();
            }
            else if (Request["action"] == "renumber-lessons")
            {
                Response.ContentType = "application/x-javascript";
                AttackData.Folder Folder = AttackData.Folder.instantiateObject(Request["Object"]) as AttackData.Folder;
                List<AttackData.Folder> Children = Folder.GetChildren<AttackData.Folder>().FindAll(x => { return x.Context_Value == "lesson"; });
                if (Children.Count == 0)
                {
                    Response.Write("alert('No lessons found to re-number');");
                }
                else
                {
                    bool useNumber = Folder.Context_Value == "course";
                    Children.ForEach(x =>
                    {
                        if (x.Name.IndexOf("final test", StringComparison.CurrentCultureIgnoreCase) == -1)
                        {
                            if (x.Name.Contains(". ")) x.Name = x.Name.Substring(x.Name.IndexOf(". ") + 2);
                            x.Name = (useNumber ? (Children.IndexOf(x) + 1).Str() : Folder.Name.SafeSplit(".", 0) + ('a' + Children.IndexOf(x))) + ". " + x.Name;
                            x.Save();
                        }
                    });

                    Response.Write("//ok//refresh");
                }
                Response.End();
            }
            else if (Request["action"] == "sort-folders")
            {
                Response.ContentType = "application/x-javascript";
                AttackData.Folder Folder = AttackData.Folder.instantiateObject(Request["Object"]) as AttackData.Folder;
                List<AttackData.Folder> Children = Folder.GetChildren<AttackData.Folder>();
                Children.Sort((x, y) =>
                {
                    string a = (x.Context_Value == "lesson" && x.Name.Substr(1, 1) == "." ? "0" : (x.Context_Value == "course" ? x.AuxField1 + " - " : "")) + x.Name;
                    string b = (y.Context_Value == "lesson" && y.Name.Substr(1, 1) == "." ? "0" : (y.Context_Value == "course" ? y.AuxField1 + " - " : "")) + y.Name;
                    return string.Compare(a, b);
                });
                Children.ForEach(x =>
                {
                    Folder.Children.First(y => { return y.ID1 == Folder.ObjectID && y.Table1 == Folder.TABLE_NAME && y.ID2 == x.ObjectID && y.Table2 == x.TABLE_NAME; }).Sequence = Children.IndexOf(x);
                });
                Folder.Save();
                Response.Write("//ok//refresh");
                Response.End();
            }
            else if (Request["action"] == "finaltest-generate")
            {
                Response.ContentType = "application/x-javascript";
                AttackData.Folder Course = AttackData.Folder.instantiateObject(Request["Object"]) as AttackData.Folder;
                var Result = Util.Cms.GenerateFinalTest(Course, true);

                Response.Write("alert('" + Result.Item2 + "');" + (Result.Item1 ? "//refresh" : ""));
                Response.End();
            }
            else if (Request["action"] == "paste" || Request["action"] == "paste-as-new" || Request["action"] == "paste-and-overwrite" || Request["action"] == "paste-as-new-and-overwrite")
            {
                AttackData.Folder Source = AttackData.Folder.LoadByPk(Request["source"].SafeSplit(":", 1).Int());
                AttackData.Folder Dest = AttackData.Folder.LoadByPk(Request["dest"].SafeSplit(":", 1).Int());
                if (Request["action"] == "paste-and-overwrite" || Request["action"] == "paste-as-new-and-overwrite")
                {                    
                    //var JSON = jlib.functions.json.DynamicJson.Parse(Source.ToJson(true, null, null, null, 10));
                    //ApplyDynamicToObject(JSON, Dest, Request["action"] == "paste-and-overwrite");

                    Dest.Children.ForEach(x => x.TupleDelete());
                    Dest.Children.Clear();
                    Dest.addChildren(Source.CloneChildren(true, true, Request["action"] == "paste-and-overwrite", 0));
                    //if (Request["action"] == "paste-and-overwrite")
                    //{
                    //    foreach (AttackData.RowLink Child in Source.Children)
                    //    {
                    //        Dest.addChild(Child.Table2, Child.ID2);
                    //    }
                    //}
                    //else
                    //{

                    //}
                }
                else
                {
                    AttackData.Folder Clone = Source.CloneFolder(true, true, Request["action"] == "paste", 0);
                    Dest.addChild(Clone);
                    if (Clone.Context_Value == "course" && Source.GetChildren<AttackData.Folder>().FirstOrDefault(folder => folder.Name.IndexOf("Final Test", StringComparison.CurrentCultureIgnoreCase) == -1) != null) Util.Cms.GenerateFinalTest(Clone, true);
                }
                Dest.Save();
                Response.Write("//ok");
                Response.End();
            }
            else if (Request["action"] == "move")
            {
                AttackData.Folder Source = AttackData.Folder.LoadByPk(Request["source"].SafeSplit(":", 1).Int());
                AttackData.Folder Dest = AttackData.Folder.LoadByPk(Request["dest"].SafeSplit(":", 1).Int());
                int previousParentPosition = -1;
                Source.Parents.ForEach(x =>
                {
                    if (x.Table1 == "d_folders" && x.ID1 == Dest.ObjectID) previousParentPosition = x.Sequence;
                    x.TupleDelete();
                });
                Source.Parents.Clear();
                System.Threading.Thread.Sleep(100);
                //Request["position"] contains node being moved in count (ie. if 3rd node is moved to 4th position, "position" will be 1 more than it should
                Dest.addChild(Source, Request["position"].Int() - (previousParentPosition > -1 && previousParentPosition < Request["position"].Int() ? 1 : 0));
                Dest.Save();
                Response.Write("//ok");
                Response.End();
            }
            else if (Request["action"] == "rename")
            {
                DataTable oNode = sqlbuilder.getDataTable(oData, "select * from " + ado_helper.PrepareDB(parse.splitValue(Request["node"], ":", 0)) + " where ObjectID=@ObjectID and @RevisionDate between CreateDate and DeleteDate", "@ObjectID", parse.splitValue(Request["node"], ":", 1).Int(), "@RevisionDate", m_oRevisionDate);

                oNode.Rows.Add(oNode.NewRow());
                ado_helper.cloneDataRow(oNode.Rows[0], oNode.Rows[1], "id,CreateDate");
                oNode.Rows[0]["DeleteDate"] = DateTime.Now;
                oNode.Rows[1]["name"] = Request["name"];
                oNode.Rows[1]["CreateDate"] = DateTime.Now;
                ado_helper.update(oNode);
                //createVersion(convert.cInt(oNode.Rows[0]["id"]), getTable(Request["node"]), convert.cInt(oNode.Rows[1]["id"]));
                //sqlbuilder.executeInsert(oData, "d_revisions", "table_name", getTable(Request["node"]), "pk_id", oNode.Rows[1]["id"], "action", "rename", "user_id", 0);
                //oData.Execute_SQLF("insert into l_row_links(sequence, table1,table2,id1,id2) select sequence, table1, table2, id1, " + oNode.Rows[1]["id"] + " from l_row_links where (id2={0} and table2='{1}'); insert into l_row_links(sequence, table1,table2,id2,id1) select sequence, table1, table2, id2, " + oNode.Rows[1]["id"] + " from l_row_links where (id1={0} and table1='{1}')", oNode.Rows[0]["id"], getTable(Request["node"]));
                Response.Clear();
                Response.End();
            }
            else if (Request["action"] == "delete")
            {
                sqlbuilder.getDataTable(oData, "update " + ado_helper.PrepareDB(parse.splitValue(Request["node"], ":", 0)) + " set DeleteDate=GetDate() where ObjectID=@ObjectID and @RevisionDate between CreateDate and DeleteDate", "@ObjectID", parse.splitValue(Request["node"], ":", 1).Int(), "@RevisionDate", m_oRevisionDate);
                sqlbuilder.getDataTable(oData, "update l_row_links set DeleteDate=GetDate() where Table2=@Table2 and id2=@ObjectID and @RevisionDate between CreateDate and DeleteDate", "@ObjectID", parse.splitValue(Request["node"], ":", 1).Int(), "@RevisionDate", m_oRevisionDate, "@Table2", parse.splitValue(Request["node"], ":", 0));
                //sqlbuilder.executeUpdate(oData, getTable(Request["node"]), "id", convert.cInt(parse.splitValue(Request["node"], "-", 1)), "DeleteDate", DateTime.Now);
                //sqlbuilder.executeInsert(oData, "d_revisions", "table_name", getTable(Request["node"]), "pk_id", convert.cInt(parse.splitValue(Request["node"], "-", 1)), "action", "delete", "user_id", 0);
                Response.Clear();
                Response.Write("//ok");
                Response.End();
                //}else if (Request["action"] == "move") {            
                //    jlib.functions.json.JsonData oJSON = jlib.functions.json.JsonMapper.ToObject(convert.cStreamToString(Request.InputStream));
                //    if (convert.cInt(oJSON["button"]) > 0) {
                //        DataTable oParent = sqlbuilder.executeSelect(oData, getTable(oJSON["parent"]) , "id", convert.cInt(parse.splitValue(oJSON["parent"], "-", 1)));
                //        DataTable oOldParent = sqlbuilder.executeSelect(oData, getTable(parse.splitValue(oJSON["node"], "-", 2)), "id", convert.cInt(parse.splitValue(oJSON["node"], "-", 3)));
                //        DataTable oReference = sqlbuilder.executeSelect(oData, getTable(oJSON["reference"]), "id", convert.cInt(parse.splitValue(oJSON["reference"], "-", 1)));
                //        DataTable oNode = sqlbuilder.executeSelect(oData, getTable(oJSON["node"]), "id", convert.cInt(parse.splitValue(oJSON["node"], "-", 1)));
                //        oNode.Rows.Add(oNode.NewRow());
                //        ado_helper.cloneDataRow(oNode.Rows[0], oNode.Rows[1], "id,CreateDate,DeleteDate");

                //        if (convert.cInt(oJSON["button"]) == 3) {//link
                //            oNode.Rows[1]["id"] = oNode.Rows[0]["id"];
                //        } else if (convert.cInt(oJSON["button"]) == 2) {//copy                    
                //            Response.Write("Error: Deep copy not yet supported");
                //            Response.End();

                //            oData.Execute_SQLF("update " + getTable(oJSON["node"]) + " set original_id=id where id=" + oNode.Rows[1]["id"]);
                //            createVersion(convert.cInt(oNode.Rows[0]["id"]), getTable(oJSON["node"]), convert.cInt(oNode.Rows[1]["id"]));
                //            sqlbuilder.executeInsert(oData, "d_revisions", "table_name", getTable(oJSON["node"]), "pk_id", oNode.Rows[1]["id"], "action", "copy", "user_id", 0);
                //        }else if (convert.cInt(oJSON["button"]) == 1) {//move
                //            ado_helper.update(oNode);                    
                //            createVersion(convert.cInt(oNode.Rows[0]["id"]), getTable(oJSON["node"]), convert.cInt(oNode.Rows[1]["id"]));
                //            sqlbuilder.executeInsert(oData, "d_revisions", "table_name", getTable(oJSON["node"]), "pk_id", oNode.Rows[0]["id"], "action", "move", "user_id", 0);

                //        }
                //        oData.Execute_SQLF(
                //            (convert.cInt(oJSON["button"]) == 3 ? "" : "insert into l_row_links (id1, table1, id2, table2, sequence) select id1, table1, {1}, table2, sequence from l_row_links where table2='{3}' and id2=" + oNode.Rows[0]["id"] + " and table1<>'" + getTable(parse.splitValue(oJSON["node"], "-", 2)) + "' and id1<>" + oOldParent.Rows[0]["id"] + ";")
                //                + (convert.cStr(oJSON["position"]) == "last" ? "" : "update l_row_links set sequence=sequence+1 where id1={0} and table1='{2}' and table2='{3}'" + (convert.cStr(oJSON["position"]) == "first" ? "" : " and sequence" + (convert.cStr(oJSON["position"]) == "before" ? ">=" : ">") + "(select sequence from l_row_links where id1={0} and table1='{2}' and table2='{3}' and id2=" + oReference.Rows[0]["id"] + ")") + ";")
                //                + "insert into l_row_links(id1,id2,table1,table2,sequence) values({0},{1},'{2}','{3}'," + (convert.cStr(oJSON["position"]) == "first" ? "0" : "(select " + (convert.cStr(oJSON["position"]) == "last" ? "max(sequence)+1" : (convert.cStr(oJSON["position"]) == "before" ? "sequence-1" : "sequence+1")) + " from l_row_links where id1={0} and table1='{2}' and table2='{3}'" + (",before,after,".IndexOf("," + oJSON["position"] + ",") > -1 ? " and id2=" + oReference.Rows[0]["id"] : "") + ")") + ")", oParent.Rows[0]["id"], oNode.Rows[1]["id"], getTable(oJSON["parent"]), getTable(oJSON["node"]));

                //    }
                //    Response.Clear();
                //    Response.End();
            }
            else if (Request["action"] == "publish")
            {
                //SshTransferProtocolBase oTransfer = new Sftp("met-login01.osl.basefarm.net", "llbakken", "sfmdCk%x");
                //oTransfer.Connect();
                //oTransfer.Get("htaccess", "C:\\proj\\Clients\\metier\\ExamParser\\www\\publish\\htaccess");            
                //string s = "te";
                //oTransfer.Close();
            }
            else if (Request["action"] == "preview")
            {
                AttackData.Folder Lesson = AttackData.Folder.LoadByPk(Request["object_id"].SafeSplit(":", 1).Int());


                //DataTable oObjects = sqlbuilder.getDataTable(oData, "select o.*, (select xml from d_content where [type]=3 and o.template_id=d_content.id) as template_html from d_content o, l_row_links l where id in (select max(id) from d_content group by original_id) and CreateDate<='" + m_oRevisionDate + "' AND DeleteDate>'" + m_oRevisionDate + "' and [type]=1 and l.id2=o.id and l.table2='d_content' and l.table1='d_lessons' and l.id1=" + oLesson.Rows[0]["id"]);
                //string sDir="C:\\proj\\Clients\\metier\\ExamParser\\www\\publish\\";
                //io.write_file(sDir                 
                //should lessonfs be its own file in the tree, or should it be linked to d_courses
                //how can we facilitate builing a course with different templates? using fixed template file names, perhaps?
                //we also want to support copying of template files directly, w/o modifying them (ie. eval.htm)
                //these must show in nav
                //should link command copy all lessons/objects EXCEPT for the actual content -- which would be stored in a separate table? The goal being that we can have customized settings for each page
                //maybe we can store settings for d_content on the l_row_link object?
                //io.createDirectory(sDir + oLesson

                Response.Clear();
                Response.End();
            }
            else if (Request["mode"] == "mymetier")
            {
                jlib.functions.JSONConverter oJSON = new JSONConverter();
                Response.Cache.SetNoStore();
                Response.Clear();
                Response.ContentType = "application/x-javascript";

                jtree oTree = new jtree();
                node oNode = new node("__internal", "", "", "source", false, 1, "");
                oNode.Tree = oTree;
                loadNode(oNode, Request.QueryString["root"]);

                Response.Write(jlib.functions.json.DynamicJson.Serialize(oNode.toJSONChildren(null)));

                Response.End();
                //} else if (Request["action"] == "content") {
                //    AttackData.Folder Page = AttackData.Folder.LoadByPk(Request["object_id"].SafeSplit(":",1).Int());
                //    AttackData.RowLink Link = Page.Children.Find(x => x.Table2 == "d_content");
                //    if (Link != null) { 
                //    AttackData.Content Content = Link.Object2 as AttackData.Content;
                //    if (Content!=null) Response.Write(Content.Xml);
                //        }
                //    Response.End();
            }
        }
        private string getTable(object oPrefix)
        {
            string sPrefix = convert.cStr(oPrefix);
            return (sPrefix.StartsWith("content") ? "d_content" : (sPrefix.StartsWith("d_exams") ? "d_exams" : "d_folders"));
        }
        private void createVersion(int iOldID, string sTable, int iNewID)
        {
            ado_helper oData = new ado_helper("sql.dsn.cms");
            //copy l_row_links (both ways)

            //copy d_settings
            oData.Execute_SQLF("insert into d_settings(pk_id,table_name,key_name,value)select {0}, table_name,key_name,value from d_settings where pk_id={1} and table_name='{2}';", iNewID, iOldID, sTable);
        }
    }    
    class ViewModeSettings
    {
        public AttackData.Folder Parent { get; set; }
        public AttackData.Content Content { get; set; }
        public string ViewModeKey { get; set; }
        public string ViewModeValue { get; set; }
    }
}