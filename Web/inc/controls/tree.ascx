﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Inc.Controls.Tree" Codebehind="tree.ascx.cs" %>
<!--
<input type="button" onclick='GetTree().ExpandNode(["d_folders\\:1", "d_folders\\:61623", "d_folders\\:61623", "d_folders\\:61532"]); GetTree().SelectNode("d_folders\\:61542");' value="ttt" />
-->
<ul id="tree" class="filetree"></ul>
<script language="javascript">
    var _tree = null;
    function GetTree() {
        if (!_tree.ExpandNode) {
            _tree.ExpandNode = function (sID) {                
                if (!$.isArray(sID)) sID = [sID];
                $.each(sID, function (i, o) {
                    if (o) {
                        sID[i] = null;
                        if (!_tree.jstree("is_open", "#" + this)) {
                            _tree.jstree("open_node", "#" + this, function () {
                                _tree.ExpandNode(sID);
                            });
                            return false;
                            //alert(o);
                        }
                    }
                    _tree.SelectNode();

                });                                
            }
            _tree.SelectNode = function (sID) {                
                if (cStr(sID)=="")sID=cStr(_tree.__select_node);
                if (cStr(sID)=="")return;
                _tree.__select_node = null;
                if (_tree.jstree("get_selected").is("#" + sID)) return;
                _tree.jstree("deselect_all");                
                if (!_tree.jstree("select_node", "#" + sID)) _tree.__select_node = sID;                
            }
        }
        return _tree;
    }
    $(function () {
        $.jstree._themes = "../inc/css/tree-themes/";
        _tree= $("#tree").jstree({
            "core": { "initially_open": ($("#tree_open").length == 0 ? [] : $("#tree_open").val().split(",")), "html_titles": true },
            "ui": { "initially_select": ($("#tree_select").length == 0 ? [] : $("#tree_select").val().split(",")) },
            "themes": { "theme": "default" },
            "json_data": {
                "progressive_render": true,
                "ajax": { "url":
                    function (oNode) {
                        return window.location.href.split("?")[0].split("#")[0] + "?mode=mymetier&root=" + (oNode == -1 ? "source" : $(oNode).attr("id"));
                    }
                    , "success": function (data) {
                        var a = function (o) {
                            $.each(o, function (i, p) {
                                //if(p.attr.auxfield1)p.data=p.attr.auxfield1 + " - " + p.data;                                
                                p.attr.title = p.attr.id + " (" + p.attr.context + ")";
                                p.icon = (p.numlinks > 1 ? "../inc/library/exec/thumbnail.aspx?u=" + escape("cms/gfx/" + p.attr.context + ".png") + "&o=" + escape("cms/gfx/shortcut-overlay.png") : "gfx/" + p.attr.context + ".png");
                                if (p.children) a(p.children);
                            });
                        }
                        a(data);
                    }
                }
            },
            "contextmenu": {
                "items": function (oNode) {
                    var oMenu = {};
                    if (oNode.attr("id").indexOf("c") == 0) oMenu["publish"] = { "label": "Publish", "action": function (obj) { fnContextMenu("publish", this, obj); }, "separator_after": false, "icon": "../inc/images/icons/application_go.png" };
                    if (oNode.attr("context") == "lesson") oMenu["preview"] = { "label": "Preview Lesson", "action": function (obj) { fnContextMenu("preview", this, obj); }, "separator_after": false, "icon": "../inc/images/icons/application_view_tile.png" };

                    oMenu["new"] = { "label": "New", "separator_before": false, "separator_after": true, "icon": "../inc/images/icons/add.png", "submenu": {
                        "exam": { "label": "Exam", "action": function (obj) { fnContextMenu("new-d_exams", this, obj); }, "separator_before": false, "separator_after": false, "icon": "../inc/images/icons/application_form_add.png" },
                        "file-folder": { "label": "File Folder", "action": function (obj) { fnContextMenu("new-file-folder", this, obj); }, "separator_before": false, "separator_after": false, "icon": "../inc/images/icons/folder_add.png" }
                        /*
                        "qustion-bank": { "label": "Question Bank", "action": function (obj) { fnContextMenu("new-question-bank", this, obj); }, "separator_before": false, "separator_after": false, "icon": "../inc/images/icons/database_add.png" },
                        "assessment": { "label": "Assessment", "action": function (obj) { fnContextMenu("new-d_exams", this, obj); }, "separator_before": true, "separator_after": false, "icon": "../inc/images/icons/awaiting-customer-response.png" }
                        */
                    }
                    };
                    if (oNode.attr("context") == "node") oMenu["new"]["submenu"]["course-template"] = { "label": "Course from template", "action": function (obj) { fnContextMenu("popup-course-from-template", this, obj); }, "separator_before": false, "separator_after": false, "icon": "../inc/images/icons/folder_add.png" };
                    if (oNode.attr("context") == "course") {
                        oMenu["new"]["submenu"]["course-template"] = { "label": "Lesson from template", "action": function (obj) { fnContextMenu("GET:lesson-from-template", this, obj); }, "separator_before": false, "separator_after": false, "icon": "../inc/images/icons/folder_add.png" };
                        oMenu["export-pdf"] = { "label": "Export to PDF", "action": function (obj) { fnContextMenu("popup-export-course", this, obj); }, "separator_before": false, "separator_after": false, "icon": "../inc/images/icons/PDF.gif" };
                        oMenu["export"] = { "label": "Export to Word", "action": function (obj) { fnContextMenu("export-course", this, obj); }, "separator_before": false, "separator_after": false, "icon": "../inc/images/icons/page_white_word.png" };
                        oMenu["publish"] = { "label": "Publish to cache", "action": function (obj) { fnContextMenu("GET:publish-to-cache", this, obj); }, "separator_before": false, "separator_after": true, "icon": "../inc/images/icons/page_white_gear.png" };
                        oMenu["finaltest-generate"] = { "label": "Generate Finaltest", "action": function (obj) { fnContextMenu("GET:finaltest-generate", this, obj); }, "separator_before": false, "separator_after": true, "icon": "../inc/images/icons/page_white_gear.png" };
                        oMenu["upgrade-course"] = { "label": "Convert to new Player Format", "action": function (obj) { fnContextMenu("popup-course-upgrade", this, obj); }, "separator_before": false, "separator_after": false, "icon": "../inc/images/icons/folder_add.png" };
                    }
                    if (oNode.attr("context") == "lesson" || oNode.attr("context") == "course") {
                        oMenu["renumber-lessons"] = { "label": "Re-number Lessons", "action": function (obj) { fnContextMenu("GET:renumber-lessons", this, obj); }, "separator_before": false, "separator_after": true, "icon": "../inc/images/icons/text_list_numbers.png" };
                        oMenu["export-json"] = { "label": "Export to JSON", "action": function (obj) { fnContextMenu("export-json", this, obj); }, "separator_before": false, "separator_after": false, "icon": "../inc/images/icons/script_code_red.png" };
                        oMenu["import-json"] = { "label": "Import from JSON", "action": function (obj) { fnContextMenu("popup-import-json", this, obj); }, "separator_before": false, "separator_after": true, "icon": "../inc/images/icons/script_code_red.png" };
                    }
                    if (oNode.attr("context") == "content" || oNode.attr("context") == "lesson" || oNode.attr("context") == "course") oMenu["compare"] = { "label": "Comparison", "separator_before": false, "separator_after": true, "icon": "../inc/images/icons/shape_move_back.png", "submenu": {
                        "old1": { "label": "Add Old Master", "action": function (obj) { fnContextMenu("diff-old1", this, obj); }, "separator_before": false, "separator_after": false, "icon": "../inc/images/icons/shape_align_left.png" },
                        "old2": { "label": "Add Old Customer Tailored", "action": function (obj) { fnContextMenu("diff-old2", this, obj); }, "separator_before": false, "separator_after": false, "icon": "../inc/images/icons/shape_align_left.png" },
                        "new1": { "label": "Add New Master", "action": function (obj) { fnContextMenu("diff-new1", this, obj); }, "separator_before": false, "separator_after": false, "icon": "../inc/images/icons/shape_align_right.png" }
                    }
                    };

                    oMenu["open"] = { "label": "Edit", "action": function (obj) { fnContextMenu("edit", this, obj); }, "separator_after": true, "icon": "../inc/images/icons/image_edit.png" };
                    oMenu["copy"] = { "label": "Copy", "action": function (obj) { fnContextMenu("copy", this, obj); }, "icon": "../inc/images/icons/page_copy.png" };
                    oMenu["cut"] = { "label": "Cut", "action": function (obj) { fnContextMenu("cut", this, obj); }, "icon": "../inc/images/icons/cut.png" };
                    oMenu["paste"] = { "label": "Paste", "icon": "../inc/images/icons/page_paste.png", "separator_after": true, "submenu": {
                        "paste": { "label": "Paste", "action": function (obj) { fnContextMenu("paste", this, obj); }, "icon": "../inc/images/icons/page_paste.png" },
                        "paste-as-new": { "label": "Paste as un-linked", "action": function (obj) { fnContextMenu("paste-as-new", this, obj); }, "icon": "../inc/images/icons/paste_plain.png" }
                    }
                    };
                    oMenu["paste-overwrite"] = {
                        "label": "Paste and overwrite", "icon": "../inc/images/icons/page_white_paste.png", "submenu": {
                            "paste-and-overwrite": { "label": "Paste", "action": function (obj) { fnContextMenu("paste-and-overwrite", this, obj); }, "icon": "../inc/images/icons/page_white_paste.png" },
                            "paste-as-new-and-overwrite": { "label": "Paste as un-linked", "action": function (obj) { fnContextMenu("paste-as-new-and-overwrite", this, obj); }, "icon": "../inc/images/icons/page_white_paste.png" }
                        }
                    };
                    
                    if (oNode.attr("id") != "d_folders:1" && oNode.attr("id") != "d_folders:3" && oNode.attr("id") != "d_folders:5" && oNode.attr("id") != "d_folders:117" && oNode.attr("id") != "d_folders:125" && oNode.attr("id") != "d_folders:101357") {
                        oMenu["delete"] = { "label": "Delete", "action": function (obj) { fnContextMenu("remove", this, obj); }, "separator_before": false, "separator_after": true, "icon": "../inc/images/icons/delete-16.png" };
                        oMenu["sort"] = { "label": "Re-sort", "action": function (obj) { fnContextMenu("GET:sort-folders", this, obj); }, "separator_before": false, "separator_after": true, "icon": "../inc/images/icons/icon-sort-asc.gif" };
                        oMenu["rename"] = { "label": "Rename", "action": function (obj) { fnContextMenu("rename", this, obj); }, "separator_before": false, "separator_after": true, "icon": "../inc/images/icons/pencil1.png" };
                        oMenu["refresh"] = { "label": "Refresh", "action": function (obj) { fnContextMenu("refresh", this, obj); }, "separator_before": false, "separator_after": true, "icon": "../inc/images/icons/arrow_refresh.png" };
                    }

                    return oMenu;
                }
            },
            "crrm": {
                "move": {
                    "check_move": function (m) {
                        //if (m.o.attr("context") == "course") return;
                        var ParentTypes = fnCMSAllowableParents(m.o.attr("context"));
                        return $.inArray(m.np.attr("context"), ParentTypes) > -1;
                    }
                }
            },
            "plugins": ["themes", "json_data", "ui", "contextmenu", "dnd", "hotkeys", "crrm"]
        }).bind("select_node.jstree", function (a, b) {
            _tree.__select_node = null; b.inst.toggle_node(b.rslt.obj[0]);
            //only update right-hand window when tree is viewed through main CMS window
            if (window.location.href.indexOf("index.aspx") > -1 || window.location.href.indexOf(".aspx") == -1) fnContextMenu("edit", null, b.rslt.obj);            
        })
        .bind("create_node.jstree", function (a, b) { alert('s'); })
        .bind("move_node.jstree", function (e, data) {
            //fnJTreeDialog(b, "Do you want to move, copy or create a link?", { "Move": function () { fnJTreeDialogButton(this, 1); }, "Copy": function () { fnJTreeDialogButton(this, 2); }, "Link": function () { fnJTreeDialogButton(this, 3); }, "Cancel": function () { fnJTreeDialogButton(this, -1); } }); 

            fnJTreeDialog(data, "Are you sure you want to perform this move?", { "Yes": function () {
                data.rslt.o.each(function (i) {
                    $.ajax({
                        async: false,
                        type: 'GET',
                        cache: false,
                        url: $("form:eq(0)").attr("action").split("#")[0],
                        data: {
                            "action": "move",
                            "source": $(this).attr("id"),
                            "dest": data.rslt.cr === -1 ? "" : data.rslt.np.attr("id"),
                            "position": data.rslt.cp + i,
                            "title": data.rslt.name,
                            "copy": data.rslt.cy ? 1 : 0
                        },
                        success: function (r) {
                            //data.inst.refresh(data.inst._get_parent(data.rslt.oc));
                            //data.inst.refresh(this);
                            /*
                            if (!r.status) {
                            $.jstree.rollback(data.rlbk);
                            }
                            else {
                            $(data.rslt.oc).attr("id", "node_" + r.id);
                            if (data.rslt.cy && $(data.rslt.oc).children("UL").length) {
                            data.inst.refresh(data.inst._get_parent(data.rslt.oc));
                            }
                            } 
                            */
                        }
                    });
                });
                $.data(document.body, "__JTREE", null);
                fnJTreeDialogButton(this, -1);
                //$('#dialogWindow').parents(".ui-dialog").detach();                
            }, "Cancel": function () { fnJTreeDialogButton(this, -1); }
            });

        })
        .bind("rename_node.jstree", function (a, b) {
            var s = $(this).parents("form:eq(0)").attr("action") || window.location.href;
            $.post(s.split("#")[0] + (s.indexOf("?") == -1 ? "?" : "&") + "action=rename&node=" + escape(b.rslt.obj.attr("id")) + "&context=" + escape(b.rslt.obj.attr("context")) + "&name=" + escape(b.rslt.name));
        }).bind("delete_node.jstree", function (a, b) {
            if (window.confirm("Are you sure you want to delete this node?")) {
                var s = $(this).parents("form:eq(0)").attr("action") || window.location.href;
                $.post(s.split("#")[0] + (s.indexOf("?") == -1 ? "?" : "&") + "action=delete&node=" + escape(b.rslt.obj.attr("id")));
            }
        });                

        $("form").submit(function () { fnJTreeSerialize($("#tree")[0]); });
        $.each(["toc|Table of contents", "intro|Intro", "goals|Goals", "reflection|Reflection", "theory|Theory", "examples|Examples", "pitfalls|Pitfalls", "checklist|Checklist", "case|Case", "references|References", "quiz|Quiz", "quizAnswers|Quiz w/Answers", "firstFinalTest|First Final Test", "firstFinalTestAnswers|First Final Test w/Answers"], //"allFinalTests|All Final Tests", "allFinalTestAnswers|All Final Tests w/Answers"
            function () {
                $('<div><input checked="checked" type="checkbox" name="_Pages" value="' + this.split("|")[0] + '" id="_PDF_' + this.split("|")[0] + '" /><label for="_PDF_' + this.split("|")[0] + '">'+this.split("|")[1]+'</label></div>').appendTo($("#popup-export-course .options"));
            }
        );        
    });
        
</script>
<div style="display:none" id="popup-course-from-template">
<h2>Create new course</h2><br />
<table>
<tr><td>Course Name:</td><td><input type="text" required="true" name="Name" /></td></tr>
<tr><td>Version Code:</td><td><input type="text" required="true" name="AuxField1" /></td></tr>
<tr><td>Article #:</td><td><input type="text" required="true" name="Attribute-Article" /></td></tr>
<tr><td>Language:</td><td><select name="AuxField2" class="language-enumeration"><option></option></select></td></tr>
<tr><td>Number of Lessons:</td><td><input type="text" required="true" name="_NumberOfLessons" /></td></tr>
<tr><td colspan="2"><input type="checkbox" name="_HasFinalTest" id="_HasFinalTest" /><label for="_HasFinalTest">Has Finaltest</label></td></tr>
<!--
<tr><td colspan="2"><input type="checkbox" name="_HasDiploma" id="_HasDiploma" /><label for="_HasDiploma">Has Diploma</label></td></tr>
-->
</table>
</div>
<div style="display:none" id="popup-course-upgrade" class="noreset">
<h2>Upgrade course to new course player</h2><br />
<table>
<tr><td colspan="2">
    <input type="checkbox" name="_UpgradeCourse" id="_UpgradeCourse" checked="checked" /><label for="_UpgradeCourse">Upgrade Course</label> <br />
    <!--
    <input type="checkbox" name="_LinkContent" id="_LinkContent" /><label for="_LinkContent">Link to content in old version</label> <br />
    -->
    <input type="checkbox" name="_NoNewVersion" id="_NoNewVersion" /><label for="_NoNewVersion">Don't create a new version</label> <br />
    <input type="checkbox" name="_QuizCompatibilityCheck" id="_QuizCompatibilityCheck" checked="checked" /><label for="_UpgradeCourse">Run quiz compatibility check</label>
</td></tr>
<tr><td>New Version Code:</td><td><input type="text" name="AuxField1" /></td></tr>
</table>
<div id="_UpgradeCourseError" class="error-message" style="background-color:rgba(182, 255, 221, 0.56);padding:20px; display:none"></div>
</div>
<div style="display:none" id="popup-export-course" class="noreset">
<h2>Export Course to PDF</h2><br />
<b>Limit to Lesson(s):</b> <br />From: <input type="text" name="_From" style="width:20px" /> To: <input type="text" name="_To" style="width:20px" />  (blank for all)
<br /><br />
    <b>Page types:</b><div class="options"></div>
(Please note: this operation can take several minutes)
</div>

<div style="display:none" id="popup-import-json">
<h2>Import course from JSON</h2><br />
<b>JSON:</b> <br />
    <textarea name="_JSON"></textarea><br />
(Please note: this operation will overwrite the existing course)<br />
    <input type="checkbox" name="_TryContentLinking" value="1" />Link to existing content (instead of creating new)<br />
    (Don't check this option if you don't know the implications of it).
</div>