using System;
using System.Drawing;
using System.Drawing.Imaging;	
using System.Drawing.Drawing2D;
using System.IO;
using System.Net;
using jlib.functions;
using System.Text;

namespace jlib.exec
{
    public class font_image : System.Web.UI.Page {

        protected override void OnInit(EventArgs e) {
            Response.Clear();
            //Image oBitmap1 = Code128Code.MakeBarcodeImage(Request["q"], 1, false);

            //MemoryStream oStream1 = new MemoryStream();
            //oBitmap1.Save(oStream1, ImageFormat.Png);
            //oBitmap1.Dispose();
            //Response.ContentType = "image/png";
            //oStream1.WriteTo(Response.OutputStream);
            //Response.End();
            //Font oFont = new Font(convert.cStr(, "Arial"), convert.cInt(, 20), , System.Drawing.GraphicsUnit.Pixel);
            Response.ContentType = "image/png";
            Response.BinaryWrite(jlib.helpers.graphics.RenderFontImage(Request["q"],Request["font"], Request["size"].Int(), (convert.cBool(Request["bold"]) ? System.Drawing.FontStyle.Bold : System.Drawing.FontStyle.Regular), Request["height"].Int(), Request["barcode"].Bln()));
            Response.End();
        }
    }    
}
