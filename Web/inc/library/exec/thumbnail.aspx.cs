using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
//using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;

using jlib.functions;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Configuration;
using System.Net;

namespace jlib.exec {
    /// <summary>
    /// Summary description for index.
    /// </summary>
    public class thumbnail : System.Web.UI.Page //: jlib.components.webpage.general
    {


        #region CONSTRUCTORS
        private void Page_Load(object sender, System.EventArgs e) {
            System.Drawing.Imaging.ImageFormat oFormat = null;
            System.Drawing.Bitmap oOutput = null;

            Response.Clear();

            Response.Cache.SetCacheability(HttpCacheability.Public);
            Response.Cache.SetExpires(DateTime.Now.AddDays(1));

            System.Collections.Hashtable oRequest = new Hashtable();
            string[] sRequest = parse.split(Request.QueryString.ToString(), "&");
            for (int x = 0; x < sRequest.Length; x++) {
                string[] sPair = parse.split(sRequest[x], "=");
                string sValue = (sPair.Length == 1 ? "" : Server.UrlDecode(sPair[1]));
                if (sValue.StartsWith("amp;")) sValue = sValue.Substring(4);
                oRequest.Add(sPair[0], sValue);
            }

            //Collect Parameters
            int iWidth = convert.cInt(oRequest["w"]);
            int iHeight = convert.cInt(oRequest["h"]);
            string sUri = convert.cStr(convert.cStr(oRequest["u"]) == "" ? oRequest["url"] : oRequest["u"]);
            bool bForceDefinedSize = convert.cBool(oRequest["d"]);

            int iCropLeft = (parse.split(convert.cStr(oRequest["t"]), "-").Length > 0 ? convert.cInt(parse.split(convert.cStr(oRequest["t"]), "-")[0]) : 0);
            int iCropTop = (parse.split(convert.cStr(oRequest["t"]), "-").Length > 1 ? convert.cInt(parse.split(convert.cStr(oRequest["t"]), "-")[1]) : 0);
            int iCropRight = (parse.split(convert.cStr(oRequest["t"]), "-").Length > 2 ? convert.cInt(parse.split(convert.cStr(oRequest["t"]), "-")[2]) : 0);
            int iCropBottom = (parse.split(convert.cStr(oRequest["t"]), "-").Length > 3 ? convert.cInt(parse.split(convert.cStr(oRequest["t"]), "-")[3]) : 0);
            bool bCrop=convert.cBool(oRequest["z"]);
            bool bFileIconFallback = oRequest["f"].Bln();

            //scale upward if the new dimensions are greater than original
            bool bNoResizeIfFit = convert.cBool(oRequest["n"]);

            //blank 1 px if not found
            bool bBlankPixelIfNotFound = convert.cBool(oRequest["b"]);

            bool bUseCache = (convert.cStr(oRequest["c"]) == "" || convert.cBool(oRequest["c"]));

            //are dimensions provided to be considered a "bounding box"?
            bool bMaxMode = (bCrop?false: (convert.cStr(oRequest["m"]) == "" ? true : convert.cBool(oRequest["m"])));

            //should the resize be performed proportionally, or stretch image?
            bool bResizeProportional = (convert.cStr(oRequest["p"]) == "" ? true : convert.cBool(oRequest["p"]));

            string sWatermarkFile = "";
            if (convert.cStr(oRequest["e"]) != "") sWatermarkFile = convert.cStr(oRequest["e"]);


            string sOverlayFile = "";
            if (convert.cStr(oRequest["o"]) != "") sOverlayFile = convert.cStr(oRequest["o"]);

            //JPEG Quality
            int iQuality = convert.cInt(oRequest["q"]);

            if (iWidth + iHeight == 0) iWidth = 200;


            if (sUri.IndexOf("://") == -1 && convert.cStr(ConfigurationManager.AppSettings["default.image.path"]) != "")
                sUri = ConfigurationManager.AppSettings["default.image.path"] + "\\" + parse.replaceAll(sUri, "/", "\\");
            else if (convert.cBool(ConfigurationManager.AppSettings["disable.remote.image.url"]) && sUri.ToLower().IndexOf(ConfigurationManager.AppSettings["default.image.url"]) == -1)
                return;

            //Resize
            Stream oImageStream = null;
            string sTempFile = String.Format("{0}{1}x{2}-{3}-{4}.{5}.{6}-{7}.{8}.{9}.{10}.{11}.{12}.{13}.{14}.{15}.{16}" + convert.cIfValue(oRequest["effect"], "-", "", ""), ConfigurationManager.AppSettings["temp.path"] + "\\thumbnails\\" + Math.Abs(sUri.GetHashCode()).ToString().Substring(0, 2) + "\\", iWidth, iHeight, iQuality, iCropLeft, iCropTop, iCropRight, iCropBottom, bResizeProportional ? 1 : 0, bNoResizeIfFit ? 1 : 0, bMaxMode ? 1 : 0, jlib.functions.parse.removeXChars(System.IO.Path.GetFileName(sUri), "-. "), sUri.GetHashCode(), sWatermarkFile.GetHashCode(), sOverlayFile.GetHashCode(), (bCrop?"1":"0"), bForceDefinedSize ? 1 : 0);
            jlib.functions.io.createDirectory(System.IO.Path.GetDirectoryName(sTempFile));

            if (System.IO.Path.GetExtension(sUri).ToLower() == ".ico")
                sTempFile += ".png";
            else
                sTempFile += System.IO.Path.GetExtension(sUri);

            if (System.IO.File.Exists(sTempFile) && !bUseCache) jlib.functions.io.delete_file(sTempFile);

            if (System.IO.File.Exists(sTempFile) && bUseCache) {

                if (System.IO.Path.GetExtension(sUri).ToLower() == ".gif") {
                    Response.ContentType = "image/gif";
                } else if (System.IO.Path.GetExtension(sUri).ToLower() == ".png" || System.IO.Path.GetExtension(sUri).ToLower() == ".ico") {
                    Response.ContentType = "image/png";
                } else {
                    Response.ContentType = "image/jpeg";
                }

                System.IO.BufferedStream oReader = new BufferedStream(File.OpenRead(sTempFile));
                int iPointer = 0;
                int iRead = 0;
                byte[] bArr = new byte[10240];
                while (true) {
                    iRead = oReader.Read(bArr, iPointer, bArr.Length);
                    if (iRead == 0) break;
                    Response.OutputStream.Write(bArr, 0, iRead);
                }

                oReader.Close();
                Response.End();

            }            
            try {
                if (sUri.Trim() == "") throw new Exception("Image URL is blank");
                if (bFileIconFallback && ",.png,.gif,.jpg,.jpeg,.ico,.bmp,".IndexOf("," + System.IO.Path.GetExtension(sUri).ToLower() + ",") == -1) oImageStream = getExtensionStream(sUri);
                else {
                    try {
                        oImageStream = jlib.functions.io.getStream(sUri);
                    } catch (Exception ex) {
                        if (bFileIconFallback) oImageStream = getExtensionStream(sUri);
                        else throw ex;
                    }
                }
                if (convert.cStr(oRequest["effect"]) == "mirror") {
                    Size MaxSize = new Size(394, 333);
                    int SkewV = 40, SkewH = 10, MaxReflectionLength = 83, FrameWidth = 7;
                    ReflectedThumbnail rt = new ReflectedThumbnail(MaxSize, SkewV, SkewH, FrameWidth, MaxReflectionLength);
                    Bitmap oImage = new Bitmap((Bitmap)System.Drawing.Image.FromStream(oImageStream));
                    rt.create(oImage);
                    Response.ContentType = "image/png";
                    try {
                        if (bUseCache) rt.ThumbnailImage.Save(sTempFile);
                    } catch (Exception) { }
                    rt.ThumbnailImage.Save(Response.OutputStream, ImageFormat.Png);
                    return;
                }

            } catch (Exception) {
                if (bBlankPixelIfNotFound || System.Configuration.ConfigurationManager.AppSettings["image.not.found.url"] == null) {
                    oOutput = new Bitmap(1, 1);
                    oOutput.SetPixel(0, 0, Color.White);
                    Response.ContentType = "image/jpeg";
                    oOutput.Save(Response.OutputStream, ImageFormat.Jpeg);
                    return;
                }
                oImageStream = jlib.functions.io.getStream(System.Web.HttpContext.Current.Request.ApplicationPath + convert.cStr(System.Configuration.ConfigurationManager.AppSettings["image.not.found.url"], "/inc/m/img_not_avail.jpg"));
                bUseCache = false;
            }
            oOutput = generateThumbnail(oImageStream, ref oFormat, iWidth, iHeight, sUri, iCropLeft, iCropTop, iCropRight, iCropBottom, bMaxMode, bNoResizeIfFit, bResizeProportional, iQuality, bBlankPixelIfNotFound, ref bUseCache, bCrop, bForceDefinedSize);
            if (sOverlayFile != "") {

                Stream oOverlayStream = jlib.functions.io.getStream(sOverlayFile);
                System.Drawing.Imaging.ImageFormat oTempFormat = null;
                System.Drawing.Bitmap oOverlay = generateThumbnail(oOverlayStream, ref oTempFormat, iWidth, iHeight, sOverlayFile, 0, 0, 0, 0, true, true, true, iQuality, bBlankPixelIfNotFound, ref bUseCache, bCrop, bForceDefinedSize);

                if (sUri.Substring(sUri.Length - 4).ToLower() == ".ico" || sUri.Substring(sUri.Length - 4).ToLower() == ".gif") {

                    Bitmap oBMP = new Bitmap(oOutput.Width, oOutput.Height);
                    Graphics gBMP = Graphics.FromImage(oBMP);
                    gBMP.DrawImage(oOutput, new Rectangle(0, 0, oBMP.Width, oBMP.Height), 0, 0, oOutput.Width, oOutput.Height, GraphicsUnit.Pixel);
                    gBMP.Dispose();
                    oOutput = oBMP;

                }
                Graphics gOutput = Graphics.FromImage(oOutput);
                //gOutput.SmoothingMode		= System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                gOutput.DrawImage(oOverlay, oOutput.Width - oOverlay.Width, oOutput.Height - oOverlay.Height);

                oOverlayStream.Close();
                gOutput.Dispose();
                oOverlay.Dispose();
                if (!oFormat.Equals(System.Drawing.Imaging.ImageFormat.Png)) oOutput = ConvertTo256Colors(oOutput);
            }


            oImageStream.Close();

            //Watermark
            if (sWatermarkFile != "") {
                oOutput = addWatermark(oOutput, Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath + sWatermarkFile));
            }

            // Temporary addition for animated GIF's
            bool bMultiFrame = false;
            if (oOutput.FrameDimensionsList.Length > 0) {
                FrameDimension oDimension = new FrameDimension(oOutput.FrameDimensionsList[0]);
                if (oOutput.GetFrameCount(oDimension) > 1)
                    bMultiFrame = true;
            }

            try {
                if (bUseCache && !bMultiFrame)
                    oOutput.Save(sTempFile);
            } catch (Exception) { }

            //Output Image
            if (oFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif)) {
                Response.ContentType = "image/gif";
            } else if (oFormat.Equals(System.Drawing.Imaging.ImageFormat.Png)) {
                Response.ContentType = "image/png";
            } else {
                Response.ContentType = "image/jpeg";
            }

            if (sUri.Substring(sUri.Length - 4).ToLower() == ".ico") {

                MemoryStream oStream = new MemoryStream();
                oOutput.Save(oStream, oFormat);
                Response.BinaryWrite(oStream.ToArray());

            } else {
                //jlib.graphics.OctreeQuantizer oQuantizer = new OctreeQuantizer( 255, 8 );
                //Bitmap oBitmap = oQuantizer.Quantize( oOutput );

                if (oFormat.Equals(ImageFormat.Bmp)) oFormat = ImageFormat.Jpeg;

                if (bMultiFrame) {
                    oImageStream = jlib.functions.io.getStream(sUri);
                    Response.BinaryWrite(jlib.functions.convert.cStreamToByteArray(oImageStream));
                } else
                    oOutput.Save(Response.OutputStream, oFormat);

            }



            //Dispose            
            oOutput.Dispose();

        }

        public Stream getExtensionStream(string sUri){
            string sIconPath = System.Web.HttpContext.Current.Request.ApplicationPath + "/inc/library/media/icons/filetypes/512px/";
            string sExtension=parse.stripLeadingCharacter(parse.replaceAll(System.IO.Path.GetExtension(sUri).ToLower(),".docx",".doc",".jpeg",".jpg",".xlsx",".xls",".pptx",".ppt"),".");
            if (System.IO.File.Exists(Server.MapPath(sIconPath + sExtension + ".png"))) return jlib.functions.io.getStream(Server.MapPath(sIconPath + sExtension + ".png"));
            return jlib.functions.io.getStream(Server.MapPath(sIconPath + "_blank.png"));
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

        #region PROPERTIES

        #endregion

        #region METHODS
        public Size calculateThumbSize(int iCurrentWidth, int iCurrentHeight, int iNewWidth, int iNewHeight, bool bMaxMode, bool bNoResizeIfFit, bool bResizeProportional) {
            if (!bResizeProportional)
                return new Size(iNewWidth, iNewHeight);

            double dFactor = 0d;

            if (bNoResizeIfFit && iCurrentWidth < iNewWidth && iCurrentHeight < iNewHeight) {
                return new Size((int)(iCurrentWidth), (int)(iCurrentHeight));
            }

            if (bMaxMode) {
                iNewHeight = iNewHeight == 0 ? iCurrentHeight : iNewHeight;
                iNewWidth = iNewWidth == 0 ? iCurrentWidth : iNewWidth;

                if (((double)iCurrentWidth / (double)iNewWidth) > ((double)iCurrentHeight / (double)iNewHeight)) {
                    dFactor = (double)iNewWidth / (double)iCurrentWidth;
                } else {
                    dFactor = (double)iNewHeight / (double)iCurrentHeight;
                }
            } else {
                if (iNewHeight > iNewWidth) {
                    dFactor = (double)iNewHeight / (double)iCurrentHeight;
                } else {
                    dFactor = (double)iNewWidth / (double)iCurrentWidth;
                }
            }

            return new Size((int)Math.Round(iCurrentWidth * dFactor), (int)Math.Round(iCurrentHeight * dFactor));

        }

        private Bitmap ConvertTo256Colors(Bitmap oBMPSource) {

            helpers.graphics.OctreeQuantizer oQuantizer = new helpers.graphics.OctreeQuantizer(255, 8);
            Bitmap oBMPQuantized = oQuantizer.Quantize(oBMPSource);

            System.Drawing.Color cT = Color.Black;
            System.Drawing.Color[] Clrs = oBMPQuantized.Palette.Entries;
            for (int x = 0; x < 255; x++) {

                System.Drawing.Color oColor = Clrs[x];
                if (oColor.R == Color.Black.R && oColor.G == Color.Black.G && oColor.B == Color.Black.B) { //match 
                    oBMPQuantized.Palette.Entries[x] = Color.FromArgb(0, oColor.R, oColor.G, oColor.B); //trans 
                } else {
                    oBMPQuantized.Palette.Entries[x] = Color.FromArgb(255, oColor.R, oColor.G, oColor.B); //opaic 
                }
            }

            return oBMPQuantized;
        }

        private bool IsIndexedImageType(Image image)
        {
            return (image.PixelFormat == PixelFormat.Indexed || image.PixelFormat == PixelFormat.Format1bppIndexed || image.PixelFormat == PixelFormat.Format4bppIndexed || image.PixelFormat == PixelFormat.Format8bppIndexed);
        }
        private Image ConvertToNonIndexedIfIndexed(Image image, ref System.Drawing.Imaging.ImageFormat oFormat)
        {
            if (!IsIndexedImageType(image)) return image;
            oFormat = ImageFormat.Jpeg;
            Bitmap tempBitmap = new Bitmap(image.Width, image.Height, PixelFormat.Format32bppArgb );
            Graphics.FromImage(tempBitmap).DrawImage(image, 0, 0);
            return tempBitmap;
        }

        public Bitmap generateThumbnail(System.IO.Stream oImageStream, ref System.Drawing.Imaging.ImageFormat oFormat, int iWidth, int iHeight, string sUri, int iCropLeft, int iCropTop, int iCropRight, int iCropBottom, bool bMaxMode, bool bNoResizeIfFit, bool bResizeProportional, int iQuality, bool bBlankPixelIfNotFound, ref bool bUseCache, bool bCrop, bool bForceDefinedSize) {
            Image oImage;
            try {
                if (sUri.Substring(sUri.Length - 4).ToLower() == ".ico") {

                    helpers.graphics.iconhelper oIcons = new helpers.graphics.iconhelper(oImageStream);

                    Icon oIcon = oIcons.FindIcon(iWidth, iHeight);
                    Bitmap oBitmap = oIcon.ToBitmap();

                    if (oBitmap.Width - iCropLeft - iCropRight <= 0) { iCropLeft = 0; iCropRight = 0; }
                    if (oBitmap.Height - iCropTop - iCropBottom <= 0) { iCropTop = 0; iCropBottom = 0; }

                    oFormat = System.Drawing.Imaging.ImageFormat.Png;

                    if (oIcon.Width == iWidth && oIcon.Height == iHeight) {
                        return ConvertTo256Colors(oBitmap);
                    }

                    Bitmap oBitmap1 = new Bitmap(iWidth, iHeight);
                    Graphics oGraphics = Graphics.FromImage(oBitmap1);
                    oGraphics.DrawImage(oBitmap, new Rectangle(0, 0, oBitmap1.Width, oBitmap1.Height), -iCropLeft, -iCropTop, oBitmap.Width - iCropRight, oBitmap.Height - iCropBottom, GraphicsUnit.Pixel);

                    oIcon.Dispose();
                    oGraphics.Dispose();
                    oBitmap.Dispose();


                    return ConvertTo256Colors(oBitmap1);
                }

                oImage = System.Drawing.Image.FromStream(oImageStream);
            } catch (Exception) {
                if (bBlankPixelIfNotFound || System.Configuration.ConfigurationManager.AppSettings["image.not.found.url"] == "") {
                    System.Drawing.Bitmap oOutput = new Bitmap(1, 1);
                    oOutput.SetPixel(0, 0, Color.White);
                    Response.ContentType = "image/jpeg";
                    oOutput.Save(Response.OutputStream, ImageFormat.Jpeg);
                    Response.End();
                }
                oImageStream = jlib.functions.io.getStream(System.Web.HttpContext.Current.Request.ApplicationPath + convert.cStr(System.Configuration.ConfigurationManager.AppSettings["image.not.found.url"], "/inc/m/img_not_avail.jpg"));
                oImage = System.Drawing.Image.FromStream(oImageStream);
                bUseCache = false;
            }
            oImageStream.Close();
            oFormat = oImage.RawFormat;           

            if (oImage.Width - iCropLeft - iCropRight <= 0) { iCropLeft = 0; iCropRight = 0; }
            if (oImage.Height - iCropTop - iCropBottom <= 0) { iCropTop = 0; iCropBottom = 0; }
            
            //Should the image be clipped and zoomed (so that it meets width and height parameters)
            if (bForceDefinedSize && iWidth > 0 && iHeight > 0)
            {
                int iNetWidth = oImage.Width - iCropLeft - iCropRight;
                int iNetHeight = oImage.Height - iCropTop - iCropBottom;
                if (iNetWidth > 0 && iNetHeight > 0)
                {
                    double dNewImageRatio = (iHeight.Dbl() / iWidth);
                    double dOriginalImageRatio = (iNetHeight.Dbl() / iNetWidth);
                    //Image is taller relative to width -- crop top/bottom
                    if (dOriginalImageRatio > dNewImageRatio)
                    {
                        iCropTop += ((iNetHeight - dNewImageRatio * iNetWidth) / 2).Int();
                        iCropBottom += ((iNetHeight - dNewImageRatio * iNetWidth) / 2).Int();
                    }
                    else
                    {
                        iCropLeft += ((iNetWidth - iNetHeight / dNewImageRatio) / 2).Int();
                        iCropRight += ((iNetWidth - iNetHeight / dNewImageRatio) / 2).Int();
                    }
                }
            }

            if (iCropTop > 0 || iCropLeft > 0 || iCropRight > 0 || iCropBottom > 0) {
                oImage = ConvertToNonIndexedIfIndexed(oImage, ref oFormat);
                Bitmap oBitmap = new Bitmap(oImage.Width - iCropLeft - iCropRight, oImage.Height - iCropTop - iCropBottom, oImage.PixelFormat);
                Graphics.FromImage(oBitmap).DrawImage(oImage, new Rectangle(0, 0, oBitmap.Width, oBitmap.Height), new Rectangle(iCropLeft, iCropTop, oBitmap.Width, oBitmap.Height), GraphicsUnit.Pixel);
                oImage = oBitmap;
            }
            
            Size oSize = calculateThumbSize(oImage.Width, oImage.Height, iWidth, iHeight, bMaxMode, bNoResizeIfFit, bResizeProportional);

            if (oSize.Width == oImage.Width && oSize.Height == oImage.Height) return new Bitmap(oImage); //return (Bitmap)oImage; (this was causing a generic GDI+ error when saving it to the stream)

            thumbmaker oThumbMaker = new thumbmaker((Bitmap)oImage);
            MemoryStream oStream = new MemoryStream();

            //PNG OVERRIDE. For some reason resizing to PNG doesn't always work
            if (oFormat.Equals(ImageFormat.Png)) oFormat = ImageFormat.Jpeg;

            if (oFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif)) {
                oThumbMaker.ResizeToGif(oSize.Width, oSize.Height, oStream);
            } else if (oFormat.Equals(System.Drawing.Imaging.ImageFormat.Png)) {
                oThumbMaker.ResizeToPng(oSize.Width, oSize.Height, oStream);
            } else {
                if (iQuality > 0)
                    oThumbMaker.ResizeToJpeg(oSize.Width, oSize.Height, iQuality, oStream);
                else
                    oThumbMaker.ResizeToJpeg(oSize.Width, oSize.Height, oStream);
            }
            oImage=Image.FromStream(oStream);
            if (bCrop && (oImage.Width > iWidth || oImage.Height > iHeight)) {
                Bitmap oBitmap = new Bitmap(iWidth, iHeight, oImage.PixelFormat);
                Graphics.FromImage(oBitmap).DrawImage(oImage, new Rectangle(0, 0, oBitmap.Width, oBitmap.Height), new Rectangle((oImage.Width - oBitmap.Width) / 2, (oImage.Height - oBitmap.Height) / 2, oBitmap.Width, oBitmap.Height), GraphicsUnit.Pixel);
                oImage = oBitmap;
            }

            return (Bitmap)oImage;// new Bitmap( oStream );

        }

        public Bitmap addWatermark(Bitmap oImage, string sFilename) {
            System.Drawing.Image imgWatermark = System.Drawing.Image.FromFile(sFilename);

            Bitmap oWatermark = new Bitmap(oImage);
            oWatermark.SetResolution(oImage.HorizontalResolution, oImage.VerticalResolution);

            Graphics gWatermark = Graphics.FromImage(oWatermark);
            gWatermark.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            ImageAttributes imageAttributes = new ImageAttributes();
            ColorMap colorMap = new ColorMap();

            //colorMap.OldColor	= Color.FromArgb(255, 255, 255, 255);
            //colorMap.NewColor	= Color.FromArgb(0, 0, 0, 0);
            //ColorMap[] remapTable = {colorMap};

            //imageAttributes.SetRemapTable(remapTable, ColorAdjustType.Bitmap);

            float[][] colorMatrixElements = { 
												new float[] {1.0f,  0.0f,  0.0f,  0.0f, 0.0f},
												new float[] {0.0f,  1.0f,  0.0f,  0.0f, 0.0f},
												new float[] {0.0f,  0.0f,  1.0f,  0.0f, 0.0f},
												new float[] {0.0f,  0.0f,  0.0f,  0.3f, 0.0f},
												new float[] {0.0f,  0.0f,  0.0f,  0.0f, 1.0f}
											};

            ColorMatrix wmColorMatrix = new ColorMatrix(colorMatrixElements);

            imageAttributes.SetColorMatrix(wmColorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

            //gWatermark.FillRegion( System.Drawing.Brushes.Blue, new System.Drawing.Region( new System.Drawing.Rectangle(0,0, oImage.Width, oImage.Height)) );
            gWatermark.DrawImage(imgWatermark, new Rectangle(oImage.Width - imgWatermark.Width - 5, oImage.Height - imgWatermark.Height - 5, imgWatermark.Width, imgWatermark.Height), 0, 0, imgWatermark.Width, imgWatermark.Height, GraphicsUnit.Pixel, imageAttributes);

            return oWatermark;

        }
        #endregion

        #region ReflectedThumbnail
        public class ReflectedThumbnail : IDisposable {
            public Bitmap ThumbnailImage;
            public int ImageBottom;

            public Size MaxSize;
            public int SkewH;
            public int SkewV;
            public int FrameWidth;
            public int MaxReflectionLength;

            //public Pen BorderPen = Pens.Gray;
            public Pen BorderPen = new Pen(Brushes.Transparent, 200);
            public Brush FrameBrush = Brushes.White;

            private struct Pixel {
                public byte B;
                public byte G;
                public byte R;
                public byte A;
            }

            public ReflectedThumbnail()
                : this(
                    new Size(128, 128),
                    20, 20,
                    3,
                    80) {
            }

            public ReflectedThumbnail(Size maxSize, int skewV, int skewH, int frameWidth, int maxReflectionLength) {
                MaxSize = maxSize;
                SkewV = skewV;
                SkewH = skewH;
                FrameWidth = frameWidth;
                MaxReflectionLength = maxReflectionLength;
            }

            public void Dispose() {
                DisposeBitmap();
            }

            public void DisposeBitmap() {
                if (ThumbnailImage != null) {
                    ThumbnailImage.Dispose();
                    ThumbnailImage = null;
                }
            }

            public void create(
                Bitmap image) {
                DisposeBitmap();

                Size sz = adaptProportionalSize(
                    new Size(MaxSize.Width - FrameWidth * 2, MaxSize.Height - FrameWidth * 2),
                    image.Size);
                sz.Width += FrameWidth * 2;
                sz.Height += FrameWidth * 2;

                ImageBottom = sz.Height;

                int reflectionLength = Math.Min(sz.Height, MaxReflectionLength);
                ThumbnailImage = new Bitmap(
                    sz.Width,
                    sz.Height + reflectionLength + SkewV);

                using (Bitmap
                    bmpFramed = createFramedBitmap(image, sz),
                    bmpReflection = createReflectedBitmap(bmpFramed, reflectionLength)) {

                    //using (Graphics g = Graphics.FromImage(bmpReflection)) {
                    //    g.ResetTransform();
                    //    g.ResetClip();
                    //    g.DrawRectangle(new Pen(Brushes.White, 6), 3, 3, bmpReflection.Width-6, bmpReflection.Height-12);
                    //}

                    using (Graphics g = Graphics.FromImage(ThumbnailImage)) {
                        Bitmap bmpFramed2 = shearBitmap(bmpFramed, SkewV, SkewH, sz.Width, sz.Height, false);
                        g.DrawImage(bmpFramed2, Point.Empty);

                        Bitmap bmpFramed1 = shearBitmap(bmpReflection, SkewV, SkewH / 3, sz.Width, reflectionLength, true);
                        g.DrawImage(bmpFramed1, 0, bmpFramed2.Height - (SkewV / 2) - 2);

                    }
                }
            }

            protected virtual Bitmap shearBitmap(Bitmap bBitmap, int iSkewV, int iSkewH, int iWidth, int iHeight, bool bReversedShear) {
                Bitmap bBitmap1 = new Bitmap(iWidth, iHeight, PixelFormat.Format32bppArgb);
                Bitmap bBitmap2 = new Bitmap(iWidth, iHeight, PixelFormat.Format32bppArgb);
                using (Graphics g1 = Graphics.FromImage(bBitmap1)) {
                    for (int y = 0; y < bBitmap.Height; y++) {
                        float row_skew = iSkewH * (float)(bReversedShear ? y : bBitmap.Height - y) / bBitmap.Height;
                        g1.DrawImage(
                        bBitmap,
                        new RectangleF(row_skew * 1.5F, y + 1, bBitmap.Width - (row_skew * 0.4F), 1),
                        new RectangleF(0, y, bBitmap.Width, 1),
                        GraphicsUnit.Pixel);
                    }
                }
                using (Graphics g1 = Graphics.FromImage(bBitmap2)) {
                    for (int x = 0; x < bBitmap1.Width; x++) {
                        float col_skew = SkewV * (float)(bReversedShear ? x : bBitmap1.Width - x) / bBitmap1.Width;
                        g1.DrawImage(
                        bBitmap1,
                        new RectangleF(x, col_skew / 2, 1, bBitmap1.Height - col_skew),
                        new RectangleF(x, 0, 1, bBitmap1.Height),
                        GraphicsUnit.Pixel);
                    }
                }
                return bBitmap2;
            }

            protected virtual Bitmap createFramedBitmap(Bitmap bmpSource, Size szFull) {
                int iWidth = szFull.Width - SkewV;
                int iHeight = szFull.Height;
                Bitmap bmp = new Bitmap(iWidth, iHeight, PixelFormat.Format32bppArgb);
                using (Graphics g = Graphics.FromImage(bmp)) {
                    g.FillRectangle(FrameBrush, 1, 1, iWidth - 2, iHeight - 2);
                    g.DrawRectangle(BorderPen, 0, 0, iWidth, iHeight);
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.DrawImage(
                        bmpSource,
                        new Rectangle(FrameWidth + 1, FrameWidth + 1, iWidth - FrameWidth * 2 - 2, iHeight - FrameWidth * 2 - 2),
                        new Rectangle(Point.Empty, bmpSource.Size),
                        GraphicsUnit.Pixel);
                }
                return bmp;
            }

            private static unsafe byte gaussBlur(byte* p, int width) {
                unchecked {
                    return (byte)((p[-width - 4] + 2 * p[-width] + p[-width + 4] +
                                        2 * p[-4] + 4 * p[0] + 2 * p[4] +
                                        p[width - 4] + 2 * p[width] + p[width + 4]) / 16);
                }
            }

            protected virtual double calculateAlphaFallout(double f) {
                return f * f * 0.8;
            }

            protected virtual Bitmap createReflectedBitmap(Bitmap bmpFramed, int height) {
                Bitmap bmpResult = new Bitmap(bmpFramed.Width, height, PixelFormat.Format32bppArgb);

                BitmapData bdS = bmpFramed.LockBits(new Rectangle(Point.Empty, bmpFramed.Size), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
                BitmapData bdD = bmpResult.LockBits(new Rectangle(Point.Empty, bmpResult.Size), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
                unchecked {
                    unsafe {
                        int nWidthInPixels = bdD.Width * 4;
                        for (int y = height - 1; y >= 0; y--) {
                            byte alpha = (byte)(255 * calculateAlphaFallout((double)(height - y) / height));
                            Pixel* pS = (Pixel*)bdS.Scan0.ToPointer() + bdS.Width * (bdS.Height - y - FrameWidth - 1);
                            Pixel* pD = (Pixel*)bdD.Scan0.ToPointer() + bdD.Width * y;
                            for (int x = bdD.Width; x > 0; x--, pD++, pS++) {
                                int R = gaussBlur(&pS->R, nWidthInPixels);
                                int G = gaussBlur(&pS->G, nWidthInPixels);
                                int B = gaussBlur(&pS->B, nWidthInPixels);
                                pD->R = (byte)R;// ((R * 3 + G * 2 + B * 2) / 7);
                                pD->G = (byte)G;// ((R * 2 + G * 3 + B * 2) / 7);
                                pD->B = (byte)B;// ((R * 2 + G * 2 + B * 3) / 7);
                                pD->A = alpha;
                            }
                        }
                    }
                }
                bmpFramed.UnlockBits(bdD);
                bmpResult.UnlockBits(bdD);

                Bitmap bmpReflection1 = new Bitmap(bmpResult.Width + 10, bmpResult.Height + 10, PixelFormat.Format32bppArgb);
                bmpReflection1.MakeTransparent();
                using (Graphics g = Graphics.FromImage(bmpReflection1)) g.DrawImage(bmpResult, new Rectangle(5, 5, bmpResult.Width - 10, bmpResult.Height - 10), new Rectangle(5, 5, bmpResult.Width - 10, bmpResult.Height - 10), GraphicsUnit.Pixel);
                bmpResult = bmpReflection1;

                return bmpResult;
            }

            public static Size adaptProportionalSize(
                Size szMax,
                Size szReal) {
                int nWidth;
                int nHeight;
                double sMaxRatio;
                double sRealRatio;

                if (szMax.Width < 1 || szMax.Height < 1 || szReal.Width < 1 || szReal.Height < 1)
                    return Size.Empty;

                sMaxRatio = (double)szMax.Width / (double)szMax.Height;
                sRealRatio = (double)szReal.Width / (double)szReal.Height;

                if (sMaxRatio < sRealRatio) {
                    nWidth = Math.Min(szMax.Width, szReal.Width);
                    nHeight = (int)Math.Round(nWidth / sRealRatio);
                } else {
                    nHeight = Math.Min(szMax.Height, szReal.Height);
                    nWidth = (int)Math.Round(nHeight * sRealRatio);
                }

                return new Size(nWidth, nHeight);
            }

        }
        #endregion
    }
}
