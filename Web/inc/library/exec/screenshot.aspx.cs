using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Text;

using jlib.functions;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Configuration;
using System.Net;
using System.Diagnostics;


namespace jlib.exec
{
	/// <summary>
	/// Summary description for index.
	/// </summary>
	public class screenshot : System.Web.UI.Page //jlib.components.webpage.general
	{
		#region DECLARATIONS
		#endregion

		#region CONSTRUCTORS
		private void Page_Load(object sender, System.EventArgs e)
		{

			Response.Cache.SetCacheability(HttpCacheability.Public);
			Response.Cache.SetExpires(DateTime.Now.AddDays(1));

            System.Collections.Hashtable oRequest = new Hashtable();
            //string[] sRequest = parse.split( Request.QueryString.ToString(), "&amp%3b" );
            string[] sRequest = parse.split( Request.QueryString.ToString(), "&" );
            for ( int x = 0; x < sRequest.Length; x++ ) {
                string[] sPair = parse.split( sRequest[x], "=" );
                oRequest.Add( sPair[0], ( sPair.Length == 1 ? "" : Server.UrlDecode( sPair[1] ) ) );
            }


			//Clear cache
            bool bClearCache = convert.cBool( oRequest["d"] );

			//Website URl            
			string sUrl = convert.cStr(convert.cStr(oRequest["u"]) == "" ? oRequest["url"] : oRequest["u"]);
            if (sUrl.StartsWith("/")) sUrl = "http://localhost" + sUrl;

			//Thumbnail Size
            int iWidth = convert.cInt( oRequest["w"] );
            int iHeight = convert.cInt( oRequest["h"] );

			//scale upward if the new dimensions are greater than original
            bool bNoResizeIfFit = convert.cBool( oRequest["n"] );
			
			//are dimensions provided to be considered a "bounding box"?
            bool bMaxMode = ( convert.cStr( oRequest["m"] ) == "" ? true : convert.cBool( oRequest["m"] ) );
			
			//should the resize be performed proportionally, or stretch image?
            bool bResizeProportional = ( convert.cStr( oRequest["p"] ) == "" ? true : convert.cBool( oRequest["p"] ) );

			//Watermark
			string sWatermarkFile = "";
            if ( convert.cStr( oRequest["e"] ) != "" ) sWatermarkFile = convert.cStr( oRequest["e"] );
			

			//Screenshot TempFile
            //string sTempFile = String.Format( "/inc/temp/thumbnails/{0}.jpg", sUrl.GetHashCode() );
			string sTempPath = convert.cStr( ConfigurationManager.AppSettings["default.image.path"], ConfigurationManager.AppSettings["temp.path"]) + "\\" + Math.Abs(sUrl.GetHashCode()) + ".jpg";

			//Chaching
			if ( File.Exists(sTempPath) && File.GetCreationTime(sTempPath).AddMonths(1) > DateTime.Now && !bClearCache )
			{
				//Use Cached Version
				//We still need to resize it
				//Response.ContentType = "image/jpeg";	
				//Response.WriteFile(sTempPath);
				//Response.End();									

			}
			else
			{
				////Create New Thumbnail
				//Process oProcess 							= new Process();
				//oProcess.EnableRaisingEvents 				= false ;
				//oProcess.StartInfo.RedirectStandardOutput 	= false;
				//oProcess.StartInfo.UseShellExecute 			= true;
				//oProcess.StartInfo.WorkingDirectory 		= Server.MapPath("/inc/temp/thumbnails/");	
				////oProcess.StartInfo.FileName 				= Server.MapPath("./WebThumbnail.exe");
				////oProcess.StartInfo.Arguments = String.Format( "\"-url={0}\" -size=auto \"-filename={1}\"", sUrl, System.IO.Path.GetFileName( sTempPath ));

				//oProcess.StartInfo.FileName 				= Server.MapPath("./IECapt.exe");
				//oProcess.StartInfo.Arguments = String.Format( "\"{0}\" \"{1}\"", sUrl, System.IO.Path.GetFileName( sTempPath ));

				//oProcess.Start();
				//oProcess.WaitForExit( 30000 );
				////jlib.functions.io.move_file( Server.MapPath( "temp/" + System.IO.Path.GetFileName( sTempPath ) ), sTempPath );
				//jlib.functions.io.move_file(oProcess.StartInfo.WorkingDirectory + System.IO.Path.GetFileName(sTempPath), sTempPath);
                
				//oProcess.Dispose();


				//Create New Thumbnail
				Process oProcess = new Process();
				oProcess.EnableRaisingEvents = false;
				oProcess.StartInfo.RedirectStandardOutput = false;
				oProcess.StartInfo.UseShellExecute = true;
				oProcess.StartInfo.WorkingDirectory = Server.MapPath("/inc/temp/thumbnails/");
				//oProcess.StartInfo.FileName 				= Server.MapPath("./WebThumbnail.exe");
				//oProcess.StartInfo.Arguments = String.Format( "\"-url={0}\" -size=auto \"-filename={1}\"", sUrl, System.IO.Path.GetFileName( sTempPath ));

				oProcess.StartInfo.FileName = Server.MapPath("./webshotcmd.exe");
				oProcess.StartInfo.Arguments = String.Format("/url \"{0}\" /out \"{1}\" /waitdoc 3 /timeoutpg 30 /timeout 80 -debugoff", sUrl, System.IO.Path.GetFileName(sTempPath));
				
				oProcess.Start();
				oProcess.WaitForExit(30000);
				//jlib.functions.io.move_file( Server.MapPath( "temp/" + System.IO.Path.GetFileName( sTempPath ) ), sTempPath );
				oProcess.Dispose();
				jlib.functions.io.move_file(oProcess.StartInfo.WorkingDirectory + System.IO.Path.GetFileName(sTempPath), sTempPath);
				//Response.Write(jlib.functions.io.move_file(oProcess.StartInfo.WorkingDirectory + System.IO.Path.GetFileName(sTempPath), sTempPath) + "<br />");
				//Response.Write(oProcess.StartInfo.WorkingDirectory + System.IO.Path.GetFileName(sTempPath) + "<br />");
				//Response.Write(sTempPath + "<br />");
				//Response.End();
				



				//thumbnailGenerator oGenerator = new thumbnailGenerator();
				//oGenerator.Url = sUrl;
				
				//System.Threading.Thread oThread = new System.Threading.Thread(new System.Threading.ThreadStart(oGenerator.work));				
				//oThread.Priority = System.Threading.ThreadPriority.AboveNormal;
				//oThread.SetApartmentState(System.Threading.ApartmentState.STA);
				//oThread.Start();
				//int iMaxSleep = 90000;
				//for (int x = 0; x < iMaxSleep; x = x + 2000) {
				//    System.Threading.Thread.Sleep(2000);
				//    if (!oGenerator.Running) break;
				//}

				//if (oGenerator.Running) oThread.Abort();
				//if (!oGenerator.Success)
				//    sTempPath = "";
				//else if (!jlib.functions.io.move_file(oGenerator.FileName, sTempPath)) sTempPath = "";

				//if (oGenerator.oException != null) throw new Exception("ThumbErr", oGenerator.oException);
			}
			
			//Product Thumbnail, Redirect User
            try {
				//if( sTempPath != "" ) 
				Server.Execute(jlib.helpers.general.getThumbnailUrl(ConfigurationManager.AppSettings["default.image.url"] + "//" + Math.Abs(sUrl.GetHashCode()) + ".jpg", iWidth, iHeight, bNoResizeIfFit, bMaxMode, bResizeProportional, sWatermarkFile, jlib.helpers.general.FileStatus.Normal, false, 0, 0, 0, 0));
            } catch (Exception) { }
	
		}




		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

		#region PROPERTIES
		
		#endregion

		#region METHODS

		#endregion

		//private class thumbnailGenerator {
		//    public bool Running = false;
		//    public bool Success = false;
		//    public string Url = "";
		//    public string FileName = "";
		//    public thumbnailGenerator() { }
		//    public Exception oException = null;
		//    public void work() {
		//        try {
		//            int WebShotHandle = 0;
		//            Success = false;
		//            Running = true;
		//            //WebShot.DllInit("debug.log", WebShot.DEBUG_FLAGWINDOW | WebShot.DEBUG_FLAGFILE);
		//            WebShot.DllInit("", WebShot.DEBUG_FLAGDISABLED);

		//            WebShot.Create(ref WebShotHandle);
		//            //WebShot.SetVerbose(WebShotHandle, 1);
		//            WebShot.SetDisableScripts(WebShotHandle, 0);
		//            WebShot.SetDisableActiveX(WebShotHandle, 0);
		//            WebShot.SetDocumentTimeout(WebShotHandle, 3);
		//            WebShot.SetImageQuality(WebShotHandle, 100);
		//            //WebShot.SetOutputPath(WebShotHandle, System.Configuration.ConfigurationManager.AppSettings["temp.path"]);
		//            WebShot.SetOutputPath(WebShotHandle, System.AppDomain.CurrentDomain.BaseDirectory);
		//            if (WebShot.Open(WebShotHandle, Url) == 0) {
						
		//            } else {
		//                StringBuilder oSB = new StringBuilder(4000);
		//                WebShot.GetImageFilename(WebShotHandle, oSB, 4000);
		//                this.FileName = oSB.ToString();
		//                Success = true;							
		//            }
		//            WebShot.Destroy(ref WebShotHandle);

		//            WebShot.DllUninit();
		//        } catch (Exception ex) {
		//            Success = false;
		//            oException = ex;
		//        }
		//        Running = false;
		//    }
		//}
	}

	
}
