using System;
using System.Drawing;
using System.Drawing.Imaging;	
using System.Drawing.Drawing2D;
using System.IO;
using System.Net;


namespace jlib.exec
{

//	ThumbMaker thumbMaker = new ThumbMaker(@"c:\Image.gif");
//	thumbMaker.ResizeToPng(100, 0, @"c:\ScaledImage.png");


	public class thumbmaker
	{
		Double xFactor;
		Double yFactor;
		System.IntPtr sourceScan0;
		int sourceStride;
		Bitmap scaledBitmap, bitmap;

	
		public thumbmaker(Bitmap oOriginal) {
			bitmap = oOriginal;
		}
		
		
//		void AdjustSizes(ref int xSize, ref int ySize)
//		{
//			if (xSize != 0 && ySize == 0)
//				ySize = Math.Abs((int) (xSize * bitmap.Height / bitmap.Width));
//			else if (xSize == 0 && ySize != 0)
//				xSize = Math.Abs((int) (ySize * bitmap.Width / bitmap.Height));
//			else if (xSize == 0 && ySize == 0)
//			{
//				xSize = bitmap.Width;
//				ySize = bitmap.Height;
//			}
//		}

		//Internal resize for indexed colored images
        void IndexedRezise( int xSize, int ySize) {
            IndexedRezise( xSize, ySize, 0 );
        }
		void IndexedRezise(int xSize, int ySize, int iFrameID)
		{	
			BitmapData sourceData;
			BitmapData targetData;
            Bitmap oCurrBitmap = bitmap;
			
			//AdjustSizes(ref xSize, ref ySize);

            scaledBitmap = new Bitmap( xSize, ySize, oCurrBitmap.PixelFormat );
            scaledBitmap.Palette = oCurrBitmap.Palette;
            sourceData = oCurrBitmap.LockBits( new Rectangle( 0, 0, oCurrBitmap.Width, oCurrBitmap.Height ), ImageLockMode.ReadOnly, oCurrBitmap.PixelFormat );

            if ( oCurrBitmap.FrameDimensionsList.Length > 0 ) {
                FrameDimension oDimension = new FrameDimension( oCurrBitmap.FrameDimensionsList[0] );
                if ( iFrameID > 0 && oCurrBitmap.GetFrameCount( oDimension ) > 1 ) {
                    if ( iFrameID >= oCurrBitmap.GetFrameCount( oDimension ) - 1 )
                        iFrameID = 0;

                    if ( iFrameID > 0 )
                        oCurrBitmap.SelectActiveFrame( oDimension , iFrameID );

                }
            }

            try
			{
				targetData = scaledBitmap.LockBits(new Rectangle(0, 0, xSize, ySize), ImageLockMode.WriteOnly, scaledBitmap.PixelFormat);
				try
				{
                    xFactor = (Double)oCurrBitmap.Width / (Double)scaledBitmap.Width;
                    yFactor = (Double)oCurrBitmap.Height / (Double)scaledBitmap.Height;
					sourceStride = sourceData.Stride; 
					sourceScan0 = sourceData.Scan0;
					int targetStride = targetData.Stride; 
					System.IntPtr targetScan0 = targetData.Scan0;
					unsafe 
					{ 
						byte * p = (byte *)(void *)targetScan0;
						int nOffset = targetStride - scaledBitmap.Width; 
						int nWidth = scaledBitmap.Width;
						for(int y=0;y < scaledBitmap.Height;++y)
						{
							for(int x=0; x < nWidth; ++x )
							{
								
								p[0] = GetSourceByteAt(x, y);
								++p;
								
							}
							p += nOffset;
						}
					}
				}
				finally
				{
					scaledBitmap.UnlockBits(targetData);
				}
			}
			finally
			{
                oCurrBitmap.UnlockBits( sourceData );
			}       
		}
		
		//This gets the color index on the source image for coords x, y on the resized target image
		byte GetSourceByteAt(int x, int y)
		{
			try
			{
				unsafe
				{
					return ((byte*) ((Int64)sourceScan0 + (Int64) (Math.Floor(y * yFactor) * sourceStride) +
						(Int64) Math.Floor(x * xFactor)))[0];
				}
			}
			catch(Exception e)
			{
				throw new Exception(String.Format("Error at GetSourceByteAt({0}, {1}) with sourceScan0={2}, y={1}, yFactor={3}, sourceStride={4}, x={0}, xFactor={5}", x, y, sourceScan0, yFactor, sourceStride, xFactor), e);
			}
		}

		//Internal resize for RGB colored images
		void RGBRezise(int xSize, int ySize)
		{
			//AdjustSizes(ref xSize, ref ySize);
			scaledBitmap = new Bitmap(xSize, ySize, PixelFormat.Format24bppRgb);
			Graphics g = Graphics.FromImage(scaledBitmap);
			//Rectangle destRect = new Rectangle(0, 0, xSize, ySize);
			g.InterpolationMode = InterpolationMode.HighQualityBicubic;
			g.SmoothingMode = SmoothingMode.AntiAlias;            
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            //g.CompositingQuality = CompositingQuality.HighQuality;
            //g.CompositingMode = CompositingMode.SourceCopy;
            

            //g.DrawImage( bitmap, -1, -1, jlib.functions.convert.cFlt( 1 + xSize * 1.01 ), jlib.functions.convert.cFlt( 1 + ySize * 1.01 ) );// destRect);
			g.FillRectangle(System.Drawing.Brushes.White, 0, 0, xSize , ySize );				
			g.DrawImage(bitmap, 0, 0, xSize , ySize );
		}

		void Save(string fileName, ImageFormat format)
		{
			scaledBitmap.Save(fileName, format);
		}

		void Save(string fileName, long jQuality, ImageFormat format)
		{	 
			ImageCodecInfo jpegCodecInfo =	GetEncoderInfo("image/jpeg");	
			Encoder qualityEncoder = Encoder.Quality;			 
			EncoderParameters encoderParams = new EncoderParameters(1);
			EncoderParameter qualityEncoderParam = new EncoderParameter(qualityEncoder, jQuality);
			encoderParams.Param[0] = qualityEncoderParam;
			scaledBitmap.Save(fileName, jpegCodecInfo, encoderParams);
		}

		void Save(Stream stream, ImageFormat format)
		{
			scaledBitmap.Save(stream, format);
		}

		void Save(Stream stream, long jQuality, ImageFormat format)
		{	
			ImageCodecInfo jpegCodecInfo =	GetEncoderInfo("image/jpeg");	
			Encoder qualityEncoder = Encoder.Quality;			 
			EncoderParameters encoderParams = new EncoderParameters(1);
			EncoderParameter qualityEncoderParam = new EncoderParameter(qualityEncoder, jQuality);
			encoderParams.Param[0] = qualityEncoderParam;
			scaledBitmap.Save(stream, jpegCodecInfo, encoderParams);
		}

		ImageCodecInfo GetEncoderInfo(String mimeType)
		{
			int j;
			ImageCodecInfo[] encoders;
			encoders = ImageCodecInfo.GetImageEncoders();
			for(j = 0; j < encoders.Length; ++j)
			{
				if(encoders[j].MimeType.ToUpper() == mimeType.ToUpper())
					return encoders[j];
			}
			return null;
		}

		public void ResizeToJpeg(int xSize, int ySize, string fileName)
		{
			this.RGBRezise(xSize, ySize);
			this.Save(fileName, ImageFormat.Jpeg);
		}
		
		public void ResizeToJpeg(int xSize, int ySize, Stream stream)
		{
			this.RGBRezise(xSize, ySize);
			this.Save(stream, ImageFormat.Jpeg);
		}

		public void ResizeToJpeg(int xSize, int ySize, long jQuality, string fileName)
		{
			this.RGBRezise(xSize, ySize);
			this.Save(fileName, jQuality, ImageFormat.Jpeg);
		}
		
		public void ResizeToJpeg(int xSize, int ySize, long jQuality, Stream stream)
		{
			this.RGBRezise(xSize, ySize);
			this.Save(stream, jQuality, ImageFormat.Jpeg);
		}

		
		public void ResizeToGif(int xSize, int ySize, string fileName)
		{
			this.IndexedRezise(xSize, ySize);
			this.Save(fileName, ImageFormat.Gif);
		}
		
		public void ResizeToGif(int xSize, int ySize, Stream stream)
		{
			this.IndexedRezise(xSize, ySize);
			this.Save(stream, ImageFormat.Gif);
		}

		public void ResizeToPng(int xSize, int ySize, string fileName)
		{
			this.IndexedRezise(xSize, ySize);
			this.Save(fileName, ImageFormat.Png);
		}
		
		public void ResizeToPng(int xSize, int ySize, Stream stream)
		{
			this.IndexedRezise(xSize, ySize);
			this.Save(stream, ImageFormat.Png);
		}

	}
}
