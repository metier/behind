﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace jlib.DataFramework
{
	public class ColumnDescriptor : PropertyDescriptor
	{
		#region ***** CONSTRUCTORS *****
		public ColumnDescriptor(IColumn Column) : base(Column.PropertyName, null)
		{
			this.Column = Column;
		}
		#endregion

		#region ***** PROPERTIES *****
		public IColumn Column { get; set;}
		
		public override AttributeCollection Attributes
		{
			get
			{
				return base.Attributes;
			}
		}

		public override Type ComponentType
		{
			get
			{
				return typeof(IRecord);
			}
		}

		public override bool IsBrowsable
		{
			get
			{
				return false;
			}
		}

		public override bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		public override Type PropertyType
		{
			get
			{
				switch(this.Column.DBType)
				{
					case System.Data.DbType.AnsiString: return typeof(string);
					case System.Data.DbType.AnsiStringFixedLength: return typeof(string);
					case System.Data.DbType.Binary: return typeof(byte[]);
					case System.Data.DbType.Boolean: return typeof(bool);
					case System.Data.DbType.Byte: return typeof(byte);
					case System.Data.DbType.Currency: return typeof(decimal);
					case System.Data.DbType.Date: return typeof(DateTime);
					case System.Data.DbType.DateTime: return typeof(DateTime);
					case System.Data.DbType.Decimal: return typeof(decimal);
					case System.Data.DbType.Double: return typeof(double);
					case System.Data.DbType.Guid: return typeof(Guid);
					case System.Data.DbType.Int16: return typeof(short);
					case System.Data.DbType.Int32: return typeof(int);
					case System.Data.DbType.Int64: return typeof(long);
					case System.Data.DbType.Object: return typeof(object);
					case System.Data.DbType.SByte: return typeof(byte);
					case System.Data.DbType.Single: return typeof(float);
					case System.Data.DbType.String: return typeof(string);
					case System.Data.DbType.StringFixedLength: return typeof(string);
					case System.Data.DbType.Time: return typeof(TimeSpan);
					case System.Data.DbType.UInt16: return typeof(int);
					case System.Data.DbType.UInt32: return typeof(int);
					case System.Data.DbType.UInt64: return typeof(int);
					case System.Data.DbType.VarNumeric: return typeof(decimal);
					case System.Data.DbType.Xml: return typeof(string);

				}

				return typeof(string);
				
			}
		}
		#endregion

		#region ***** METHODS *****
		public override bool CanResetValue(object component)
		{
			return true;
		}
		public override bool Equals(object other)
		{
			if (other is ColumnDescriptor)
			{
				ColumnDescriptor descriptor = (ColumnDescriptor) other;
				return (descriptor.Column == this.Column);
			}
			return false;
		}
		public override int GetHashCode()
		{
			return this.Column.GetHashCode();
		}
		public override object GetValue(object component)
		{
			IRecord record = (IRecord)component;
			IField field = record.Fields.GetByPropertyName(this.Column.PropertyName);
			return field.RawValue;
		}

		public override void ResetValue(object component)
		{
			IRecord record = (IRecord)component;
			IField field = record.Fields.GetByPropertyName(this.Column.PropertyName);
			field.ResetValue();
		}

		public override void SetValue(object component, object value)
		{
			IRecord record = (IRecord)component;
			IField field = record.Fields.GetByPropertyName(this.Column.PropertyName);
			field.SetChangedValue(value);
		}

		public override bool ShouldSerializeValue(object component)
		{
			return false;
		}
		#endregion
		
		

		

		


		

	}
}
