﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data;
using jlib.db;
using System.ComponentModel;
using jlib.DataFramework.Extensions;
using jlib.functions;

namespace jlib.DataFramework
{
	public abstract class BaseRecord<ContextType> : IRecord, ICacheable, ISerializable, IDataRow, ICustomTypeDescriptor
		where ContextType : BaseContext, new()
	{
		#region ***** ENUMS *****
		
		#endregion

		#region ***** CONSTRUCTORS *****
		public BaseRecord(ContextType Context)
		{
			this.Context = Context;            
            this.Fields = new FieldCollection(this.Context, this);
			this.RecordState = DataRowAction.Add;
            ManualRevisionEntries = new List<RevisionEntry>();
		}
		#endregion

		#region ***** PROPERTIES *****
		public string DbEntityName { get; set; }
		public DataRowAction RecordState { get; set; }
        
        [jlib.functions.json.DynamicJson.NoJSONEnumeration]
		public FieldCollection Fields { get; private set; }
        
        [jlib.functions.json.DynamicJson.NoJSONEnumeration]
		public ContextType Context { get; set; }
		
		public object this[string PropertyName]
		{
            get { return (this.Fields.ContainsKey(PropertyName) ? this.Fields.GetFieldValueByPropertyName(PropertyName) : this.Fields.GetFieldValueByFieldName(PropertyName)); }
			set 
			{
				this.Fields.SetFieldChangedValueByPropertyName(PropertyName, value);
				//this.SetDataRecordStateChanged();
				//this.Fields.SetFieldLoadedValueByPropertyName(PropertyName, value); 
			}
		}

		public object this[int Index]
		{
			get { return this.Fields[Index].RawValue; }
			set 
			{ 
				this.Fields[Index].SetChangedValue(value);
				//this.SetDataRecordStateChanged();
			}
		}
        [jlib.functions.json.DynamicJson.NoJSONEnumeration]
		public ICollection Collection { get; set; }
		
        [jlib.functions.json.DynamicJson.NoJSONEnumeration]
        public List<RevisionEntry> ManualRevisionEntries {get;set;}

        private PropertyDescriptorCollection _PropertyDescriptorCollectionCache { get; set; }

		#endregion

		#region ***** IDataRow Methods *****
		object IDataRow.GetColumnByIndex(int Index)
		{
			return this.Fields[Index];
		}
		object IDataRow.GetColumnByName(string Name)
		{
			//If Column does not exist
			if (!this.Fields.ContainsKey(Name)) return null;

			//else return column
			return this.Fields[Name];
		}
        object IDataRow.GetValueByFieldName(string Name) 
		{
            //If Column does not exist
            Name=Name.ToLower();
            IField Field = this.Fields.Values.FirstOrDefault(x => x.FieldName.ToLower() == Name);
            return Field == null ? null : Field.RawValue; 
        }
        string IDataRow.GetFieldNameByIndex(int Index) 
		{
            return this.Fields[Index].FieldName;
        }
		int IDataRow.GetColumnCount()
		{
			return this.Fields.Count;
		}

		#endregion

		#region ***** METHODS *****
        public void SetDataRecordStateChanged()
		{
			if (this.RecordState != DataRowAction.Add)
				this.RecordState = DataRowAction.Change;
		}
		public BaseRecord<ContextType> Clone() 
		{
            return Clone(DataRowVersion.Current);
        }
        public BaseRecord<ContextType> Clone(DataRowVersion RowVersion) 
		{
            return Clone(RowVersion, false);
        }
        public BaseRecord<ContextType> Clone(DataRowVersion RowVersion, bool SetFieldsAsModified) 
		{
            if (RowVersion != DataRowVersion.Current && RowVersion != DataRowVersion.Original && RowVersion != DataRowVersion.Default) throw new Exception("Invalid Row State passed into Clone()");
            //BaseRecord<ContextType> NewObject = this.MemberwiseClone() as BaseRecord<ContextType>;
            BaseRecord<ContextType> NewObject = this.GetType().GetConstructor(new Type[] { }).Invoke(null) as BaseRecord<ContextType>;            
            foreach (KeyValuePair<string, IField> Field in this.Fields) {
                if (NewObject.Fields.ContainsKey(Field.Key)) {
                    NewObject.Fields[Field.Key].SetLoadedValue(Field.Value.OriginalRawValue);
                    if (RowVersion != DataRowVersion.Original) NewObject.Fields[Field.Key].SetChangedValue(Field.Value.RawValue);
                    if (SetFieldsAsModified) NewObject.Fields[Field.Key].FieldStatus = FieldStatuses.Modified;
                }
            }                                        
            return NewObject;
        }
		public virtual void GetObjectData(System.Runtime.Serialization.SerializationInfo SerializationInfo, System.Runtime.Serialization.StreamingContext StreamingContext)
		{
			if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Loop through fields setting values
			this.Fields
				.ToList()
				.ForEach(field =>
				{
					//Set Value
					SerializationInfo.AddValue(field.Value.PropertyName, field.Value.RawValue, field.Value.ObjectType);
				});

		}

		protected virtual void ImportInternalObject(string InternalObjectIdentifier, DataRow Row)
		{
			this.RecordState = DataRowAction.Change;
		}

		public virtual void Import(DataRow Row)
		{
			this.Import(Row, InternalObjectIdentifier: null);
		}

		public virtual void Import(DataRow Row, string InternalObjectIdentifier = null)
		{
			List<string> internalObjects = new List<string>();

			//Loop through each row
			foreach (DataColumn column in Row.Table.Columns)
			{
				//If InternalObjectIdentifier is specified, then must match
				if (!String.IsNullOrWhiteSpace(InternalObjectIdentifier) && !column.ColumnName.StartsWith(InternalObjectIdentifier + ".", StringComparison.CurrentCultureIgnoreCase))
				{
					//Skip Column
					continue;
				}

				//else If InternalObjectIdentifier is NOT specified but column has a objectIdentifier, then add to list and skip column
				else if (String.IsNullOrWhiteSpace(InternalObjectIdentifier) && column.ColumnName.Contains('.') && !column.ColumnName.StartsWith("Paging."))
				{
					//Get Identifier
					string identifier = column.ColumnName.Substring(0, column.ColumnName.IndexOf('.'));

					//Add to collection
					if (!internalObjects.Any(x => x.Equals(identifier, StringComparison.CurrentCultureIgnoreCase))) internalObjects.Add(identifier);

					//Skip Column
					continue;
				}

				//else Import Data Value
				else
				{
					//Get RootColumnName
					string columnName = column.ColumnName;
					if (!String.IsNullOrWhiteSpace(InternalObjectIdentifier)) columnName = columnName.Substring(InternalObjectIdentifier.Length + 1);

					//Check if field doesn't exist, then add it
					if (!this.Fields.ContainsFieldName(columnName))
					{
						this.Fields.Add(columnName, new BaseField<string>(this, FieldName: columnName, PropertyName: columnName, ParameterName: null, InitialValue: null, DBType: DbType.String, IsVirtualColumn: true));
					}

					//Get Value
					object value = Row[column];

					//Check for DbNull
					if (value == System.DBNull.Value) value = null;

					//Set Field Value
					this.Fields.SetFieldLoadedValueByFieldName(columnName, value);
				}
			}

			//Process Importing Internal Objects
			internalObjects.ForEach(x => this.ImportInternalObject(x, Row));

			//Set Record State
			this.RecordState = DataRowAction.Nothing;
		}

		public virtual void Import(DataRow Row, DataMapTable MapTable)
		{
			List<string> internalObjects = new List<string>();

			//Use map table for performance of loading
			foreach (DataMap map in MapTable)
			{
				//if Internal Object Field, then add to list and continue
				if (map.IsInternalObjectField)
				{
					//Get Identifier
					string identifier = map.NewFieldName.Substring(0, map.NewFieldName.IndexOf('.'));

					//Add to collection
					if (!internalObjects.Any(x => x.Equals(identifier, StringComparison.CurrentCultureIgnoreCase))) internalObjects.Add(identifier);

					//Skip Column
					continue;
				}

				//if New Field, then add
				else if (map.IsNewField)
				{
					this.Fields.Add(map.NewFieldName, new BaseField<string>(this, FieldName: map.NewFieldName, PropertyName: map.NewFieldName, ParameterName: null, InitialValue: null, DBType: DbType.String, IsVirtualColumn: true));
				}

				//Get Value
				object value = Row[map.SourceIndex];

				//Check for DbNull
				if (value == System.DBNull.Value) value = null;

				this.Fields[map.DestinationIndex].SetLoadedValue(value);
			}

			//Process Importing Internal Objects
			internalObjects.ForEach(x => this.ImportInternalObject(x, Row));

			//Set Record State
			this.RecordState = DataRowAction.Nothing;
		}

		public virtual void Import(IRecord Record)
		{
			//Loop through each row
			foreach (IField field in Record.Fields.Values)
			{
				//Set Field Value
				this.Fields.SetFieldLoadedValueByPropertyName(field.PropertyName, field.RawValue);
			}

			//Set Record State
			this.RecordState = DataRowAction.Nothing;
		}

		public virtual DataMapTable GetDataMapTable(DataRow Row)
		{
			//Create New DataMapTable
			DataMapTable mapTable = new DataMapTable();

			int newFieldIndex = this.Fields.Count - 1;

			//Loop through each column
			for (int sourceIndex = 0; sourceIndex < Row.Table.Columns.Count; sourceIndex++)
			{
				//Get Column Name
				string columnName = Row.Table.Columns[sourceIndex].ColumnName;

				//Loop through collection to find Match
				bool columnFound = false;
				for (int destinationIndex = 0; destinationIndex < this.Fields.Count; destinationIndex++)
				{
					//If Fields Match
					if (columnName.Equals(this.Fields[destinationIndex].FieldName, StringComparison.CurrentCultureIgnoreCase))
					{
						//Add to Map Table
						mapTable.Add(new DataMap(sourceIndex, destinationIndex));

						//Set Column Found
						columnFound = true;

						//Break current for iteration
						break;
					}
				}

				//If Column not found, add new field
				if (!columnFound)
				{
					//Determine if internal object field
					bool isInternalObjectField = columnName.IndexOf(".") > 0 && !columnName.StartsWith("Paging.");

					//Increment New Field Counter
					if(!isInternalObjectField) newFieldIndex++;

					mapTable.Add(new DataMap(sourceIndex, newFieldIndex)
						{
							IsNewField = true && !isInternalObjectField,
							IsInternalObjectField = isInternalObjectField,
							NewFieldName = columnName
						});
				}

			}

			//Return MapTable
			return mapTable;
		}

        public void ParseJson(string json) {
            if (json.IsNullOrEmpty()) return;
            dynamic obj = jlib.functions.json.DynamicJson.Parse(json);
            foreach (var s in obj.GetDynamicMemberNames()){
                if(jlib.functions.reflection.containsProperty(this,s))
                    jlib.functions.reflection.setValueOfProperty(this,s,obj[s]);
                else if(jlib.functions.reflection.containsField(this,s))
                    jlib.functions.reflection.setValueOfField(this, s, obj[s]);
            }                
        }
        public string ToJson() {
            return ToJson(null);
        }
        public string ToJson(functions.json.JsonWriter Writer) {
            return ToJson(Writer, this.GetJSONColumns(new List<string>(), new List<string>()));
        }
        public string ToJson(functions.json.JsonWriter Writer = null, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null, bool IncludeObjectType = true)
		{			
            return ToJson(Writer, this.GetJSONColumns(IncludeColumns, ExcludeColumns),null, IncludeObjectType);           
		}

        public string ToJson(functions.json.JsonWriter Writer = null, List<string> IncludeColumns = null, List<string> ExcludeColumns = null, bool IncludeObjectType = true) {
            //Create JsonWriter
            if (Writer == null) Writer = new functions.json.JsonWriter();

            //Start Object
            Writer.WriteObjectStart();

            //Object Type
            if(IncludeObjectType) Writer.WritePropertyNameValue("_Type", this.GetType().Name);

            //For each field
            List<string> includeColumns = this.GetJSONColumns(IncludeColumns, ExcludeColumns);
            foreach (string field in includeColumns) {
                if (this.Fields.ContainsPropertyName(field)) Writer.WritePropertyNameValue(field, this.Fields.GetFieldValueByPropertyName(field));
                else Writer.WritePropertyNameValue(field, jlib.functions.reflection.getPropertyValue(this, field));
            }

            //End Object
            Writer.WriteObjectEnd();

            //Return string
            return Writer.ToString();
        }
        public string ToJsonByAttribute() {
            return ToJsonByAttribute(0);
        }
        public string ToJsonByAttribute(int objectDepth) {
            return ToJsonByAttribute(objectDepth, typeof(jlib.DataFramework.Field));
        }
        public string ToJsonByAttribute(int objectDepth, Type attributeType, functions.json.JsonWriter Writer = null) {
            //Create JsonWriter
            if (Writer == null) Writer = new functions.json.JsonWriter();


            //Start Object
            Writer.WriteObjectStart();

            //Object Type
            Writer.WritePropertyNameValue("_Type", this.GetType().Name);
            
            //For each field
            var fields = jlib.DataFramework.Extensions.JSON.GetJSONValuesByAttribute(this, attributeType);

            fields.ToList().ForEach(x=>{
                if (x.Value != null && (x.Value as IRecord) != null) {
                    if (objectDepth > 0) {
                        Writer.WritePropertyName(x.Key);
                        (x.Value as IRecord).ToJsonByAttribute(objectDepth - 1, attributeType, Writer);
                    }
                } else {
                    Writer.WritePropertyNameValue(x.Key, x.Value);
                }
            });
            
            //End Object
            Writer.WriteObjectEnd();

            //Return string
            return Writer.ToString();
        }
	    public string ToJsonArray(functions.json.JsonWriter Writer = null, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null)
	    {
	        return ToJsonArray(Writer, IncludeColumns, ExcludeColumns, true);
	    }
        public string ToJsonArray(functions.json.JsonWriter Writer = null, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null, bool IncludeObjectType = true) {
            List<string> incluedColumns = this.GetJSONColumns(IncludeColumns, ExcludeColumns);
            return ToJsonArray(Writer, incluedColumns, IncludeObjectType);
        }
	    public string ToJsonArray(functions.json.JsonWriter Writer, List<string> IncludeColumns, bool IncludeObjectType)
		{
			//Create JsonWriter
			if(Writer == null) Writer = new functions.json.JsonWriter();

			//Start Object
			Writer.WriteArrayStart();

			//Object Type
			if(IncludeObjectType) Writer.Write(this.GetType().Name);
				
			//For each field

            foreach (string field in IncludeColumns)
			{
				if (this.Fields.ContainsPropertyName(field)) Writer.Write(this.Fields.GetFieldValueByPropertyName(field));
				else Writer.Write(jlib.functions.reflection.getPropertyValue(this, field));
			}

			//End Object
			Writer.WriteArrayEnd();

			//Return string
			return Writer.ToString();
		}

        public void AddManualRevisionEntry(string fieldName, string oldVal, string newVal) {            
            ManualRevisionEntries.Add(new RevisionEntry(fieldName,oldVal,newVal));
        }

		#endregion

		#region ***** ICUSTOMTYPEDESCRIPTOR METHODS *****
		AttributeCollection ICustomTypeDescriptor.GetAttributes()
		{
			return new AttributeCollection(null);
		}

		string ICustomTypeDescriptor.GetClassName()
		{
			return null;
		}

		string ICustomTypeDescriptor.GetComponentName()
		{
			return null;
		}

		TypeConverter ICustomTypeDescriptor.GetConverter()
		{
			return null;
		}

		EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
		{
			return null;
		}

		PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
		{
			return null;
		}

		object ICustomTypeDescriptor.GetEditor(Type editorBaseType)
		{
			return null;
		}

		EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
		{
			return new EventDescriptorCollection(null);
		}

		EventDescriptorCollection ICustomTypeDescriptor.GetEvents(Attribute[] attributes)
		{
			return new EventDescriptorCollection(null);
		}

		PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
		{
			return ((ICustomTypeDescriptor) this).GetProperties(null);
		}

		PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties(Attribute[] attributes)
		{
			//If there is a collection, refer to it
			if (this.Collection != null)
			{
				this._PropertyDescriptorCollectionCache = this.Collection.GetPropertyDescriptors();
			}

			//Else use local values
			else if (this._PropertyDescriptorCollectionCache == null)
			{
				PropertyDescriptor[] properties = this.Fields.Select(field => new ColumnDescriptor(field.Value as IColumn)).ToArray<ColumnDescriptor>();
				this._PropertyDescriptorCollectionCache = new PropertyDescriptorCollection(properties);
			}
			
			//return Cache
			return this._PropertyDescriptorCollectionCache;

		}

		object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor pd)
		{
			return this;
		}

		#endregion
	}
    public class RevisionEntry {
        public string FieldName{get;set;}
        public string OldVal{get;set;}
        public string NewVal{get;set;}
        public RevisionEntry(string fieldName, string oldVal, string newVal){
            FieldName=fieldName;
            OldVal=oldVal;
            NewVal=newVal;
        }
    }
}
