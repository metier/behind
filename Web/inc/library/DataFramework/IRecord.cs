﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace jlib.DataFramework
{
    public interface IRecord
	{
		//Properties
        FieldCollection Fields { get; }
		ICollection Collection { get; set; }
		DataRowAction RecordState { get; set; }
		string DbEntityName { get; }
        List<RevisionEntry> ManualRevisionEntries { get; set; }

		//Methods
		void Import(DataRow Row);
        void Import(IRecord Record);
		DataMapTable GetDataMapTable(DataRow Row);
        string ToJsonByAttribute();
        string ToJsonByAttribute(int objectDepth);
        string ToJsonByAttribute(int objectDepth, Type attributeType, functions.json.JsonWriter Writer = null);
        string ToJson(functions.json.JsonWriter Writer = null, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null, bool IncludeObjectType = true);
		string ToJsonArray(functions.json.JsonWriter Writer = null, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null);
        string ToJsonArray(functions.json.JsonWriter Writer = null, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null, bool IncludeObjectType = true);
        string ToJsonArray(functions.json.JsonWriter Writer, List<string> IncludeColumns, bool IncludeObjectType);
        void ParseJson(string json);
	}
}
