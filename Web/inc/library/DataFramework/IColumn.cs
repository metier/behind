﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace jlib.DataFramework
{
	public interface IColumn
	{
		//Properties
		string PropertyName { get; }
		string FieldName { get; }
		string PkParameterName { get; }
		string ParameterName { get; }
		bool IsPrimaryKey { get; }
		bool IsVirtualColumn { get; }
		bool IsNullable { get; }
		bool HasDbDefault { get; }
		bool IsIdentity { get; }
		DbType DBType { get; }
		bool IsStandardDefaultColumn { get; }
		Type Type { get; }
	}
}
