﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.DataFramework.Extensions
{
	public static class JSON
	{
		public static string ToJson<RecordType>(this List<RecordType> Collection, functions.json.JsonWriter Writer = null, bool IncludeRecordContainer = true, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null, Paging.PagingDetails PagingDetails = null )
			where RecordType : IRecord
		{
			//Create JsonWriter
			if(Writer == null) Writer = new functions.json.JsonWriter();

			//Start Object
			if(IncludeRecordContainer) Writer.WriteObjectStart();

			//Object Type
			if(IncludeRecordContainer) Writer.WritePropertyNameValue("_Type", Collection.GetType().Name);
				
			//For each field
			if(IncludeRecordContainer) Writer.WritePropertyName("Records");
			Writer.WriteArrayStart();
            Collection.ForEach(x => x.ToJson(Writer, IncludeColumns, ExcludeColumns, IncludeRecordContainer));
			Writer.WriteArrayEnd();

			//Paging
			if (PagingDetails != null && IncludeRecordContainer)
			{
				Writer.WritePropertyName("Paging");
				Writer.WriteObjectStart();
				Writer.WritePropertyNameValue("PageCount", PagingDetails.PageCount);
				Writer.WritePropertyNameValue("TotalRows", PagingDetails.TotalRows);
				Writer.WritePropertyNameValue("PageIndex", PagingDetails.PageIndex);
				Writer.WritePropertyNameValue("PageSize", PagingDetails.PageSize);
				Writer.WriteObjectEnd();

			}

			//End Object
			if(IncludeRecordContainer) Writer.WriteObjectEnd();

			//Return string
			return Writer.ToString();
		}

        public static string ToJsonArray<RecordType>(this List<RecordType> Collection, functions.json.JsonWriter Writer = null, params IColumn[] oColsToInclude)
            where RecordType : IRecord 
		{
            //Create JsonWriter
            if (Writer == null) Writer = new functions.json.JsonWriter();
            Writer.WriteArrayStart();
            
            for (int x = 0; x < Collection.Count; x++) 
			{
                Writer.WriteArrayStart();
                if (oColsToInclude == null)
					for (int y = 0; y < Collection[x].Fields.Count; y++)
					{
						Writer.Write(Collection[x].Fields[y].RawValue);
					}
                else
                    for (int y = 0; y < oColsToInclude.Length; y++) 
					{
                        if(Collection[x].Fields.ContainsKey(oColsToInclude[y].PropertyName))
                            Writer.Write(Collection[x].Fields[oColsToInclude[y].PropertyName].RawValue);
                        else
                            Writer.Write(jlib.functions.reflection.getPropertyValue(Collection[x], oColsToInclude[y].PropertyName));
                    }

                Writer.WriteArrayEnd();                
            }
            
            //End Array
            Writer.WriteArrayEnd();

            //Return string
            return Writer.ToString();
        }

        public static string ToJsonArray<RecordType>(this List<RecordType> Collection, functions.json.JsonWriter Writer = null, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null)
            where RecordType : IRecord
        {
            return ToJsonArray<RecordType>(Collection, Writer, IncludeColumns, ExcludeColumns, true);
        }

        public static string ToJsonArray<RecordType>(this List<RecordType> Collection, functions.json.JsonWriter Writer = null, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null, bool IncludeObjectType=true)
            where RecordType : IRecord 
		{
            //Create JsonWriter
            if (Writer == null) Writer = new functions.json.JsonWriter();
            Writer.WriteArrayStart();
            
            Writer.WriteArrayStart();
            if (Collection.Count > 0) {
                List<string> incluedColumns = Collection[0].GetJSONColumns(IncludeColumns, ExcludeColumns);
                Collection.ForEach(x => x.ToJsonArray(Writer, incluedColumns, IncludeObjectType));
            }
            Writer.WriteArrayEnd();                
            
            //End Array
            Writer.WriteArrayEnd();

            //Return string
            return Writer.ToString();
        }
        public static Dictionary<string,object> GetJSONValuesByAttribute<RecordType>(this RecordType Record, Type Attribute)
            where RecordType : IRecord {
            Dictionary<string,object>values=new Dictionary<string,object>();
                var props = from p in Record.GetType().GetProperties()
                            let attr = p.GetCustomAttributes(Attribute, true)
                        where attr.Length == 1
                            select new { Property = p, Attribute = attr.First() as Attribute };
                props.ToList().ForEach(x => {
                    if(x.Property.CanRead) values.Add(x.Property.Name, x.Property.GetValue(Record));                    
                });
            return values;
        }
        public static List<string> GetJSONColumns<RecordType>(this RecordType Record)
            where RecordType : IRecord {
                return GetJSONColumns(Record, new List<string>(), new List<string>());
        }
        public static List<string> GetJSONColumns<RecordType>(this RecordType Record, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null)
            where RecordType : IRecord {            
                return GetJSONColumns(Record, (IncludeColumns==null? new List<string>() : IncludeColumns.Select(x => x.PropertyName).ToList()), (ExcludeColumns==null? null : ExcludeColumns.Select(x => x.PropertyName).ToList()));
        }
        
        public static List<string> GetJSONColumns<RecordType>(this RecordType Record, List<string> IncludeColumns = null, List<string> ExcludeColumns = null)
			where RecordType : IRecord 
        {
            List<string> includedColumns = new List<string>();
            if (Record != null)
            {
				if (IncludeColumns == null || IncludeColumns.Count == 0)
				{
					Record.Fields.Values.ToList().ForEach(field =>
						{
							if (
							 !String.IsNullOrWhiteSpace(field.PropertyName)
							 && (ExcludeColumns == null || !ExcludeColumns.Any(x => x == field.PropertyName))
							)
							{
								includedColumns.Add(field.PropertyName);
							}
						});
				}
				else
				{
					IncludeColumns.ForEach(column =>
						{
							if (Record.Fields.ContainsPropertyName(column)) includedColumns.Add(column);
							else if (Record.GetType().GetProperty(column) != null) includedColumns.Add(column);
						});

				}

				/*
                foreach (IField field in Record.Fields.Values)
                {
					if (
							 !String.IsNullOrWhiteSpace(field.PropertyName)
							 && (IncludeColumns == null || IncludeColumns.Count == 0 || IncludeColumns.Any(x => x.PropertyName == field.PropertyName))
							 && (ExcludeColumns == null || !ExcludeColumns.Any(x => x.PropertyName == field.PropertyName))
						)
					{
						includedColumns.Add(field.PropertyName);
					}
                }
				*/
            }
            return includedColumns;
         }

	}
}
