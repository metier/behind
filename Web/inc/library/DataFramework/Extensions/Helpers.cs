﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jlib.DataFramework.Cache;
using jlib.DataFramework.Paging;
using jlib.DataFramework.QueryBuilder;
using jlib.functions;

namespace jlib.DataFramework.Extensions
{
	public static class Helpers
	{
		public static ReturnType GetSafeValue<ObjectType, ReturnType>(this ObjectType o, Func<ObjectType, ReturnType> fnGetValue, ReturnType DefaultValue)
			where ObjectType : IRecord, jlib.db.IDataRow
		{
			//Get Value
			object value = null;
			if (o == null) value = null;
			else value = fnGetValue(o);

			//If null , set default
			if (value == null) value = DefaultValue;

			//If return type is string
			if (typeof(ReturnType).Equals(typeof(String))) return (ReturnType)(object)(value.Str());
			else if (typeof(ReturnType).Equals(typeof(Boolean))) return (ReturnType)(object)(value.Bln());
			else if (typeof(ReturnType).Equals(typeof(DateTime))) return (ReturnType)(object)(value.Date());
			else if (typeof(ReturnType).Equals(typeof(int)) || typeof(ReturnType).Equals(typeof(Int32))) return (ReturnType)(object)(value.Short());
			else if (typeof(ReturnType).Equals(typeof(short)) || typeof(ReturnType).Equals(typeof(Int16))) return (ReturnType)(object)(value.Int());
			else if (typeof(ReturnType).Equals(typeof(long)) || typeof(ReturnType).Equals(typeof(Int64))) return (ReturnType)(object)(value.Lng());
			else if (typeof(ReturnType).Equals(typeof(Guid))) return (ReturnType)(object)(value.Guid());
			
			//Return
			return DefaultValue;
			
		}


		public static QueryPackageType ApplyPaging<QueryPackageType>(this QueryPackageType Query, PagingOptions Paging)
			where QueryPackageType: IQueryPackage, new()
		{
			if (Paging != null)
				Query.PagingOptions = Paging;

			return Query;

		}

		public static QueryPackageType ApplyFilter<QueryPackageType>(this QueryPackageType Query, BaseCondition Filter)
			where QueryPackageType: IQueryPackage, new()
		{
			if (Filter != null)
				Query = (QueryPackageType)Query.Where(Filter);

			return Query;

		}

		public static QueryPackageType ApplyCache<QueryPackageType>(this QueryPackageType Query, CacheOptions Cache)
			where QueryPackageType: IQueryPackage, new()
		{
			if (Cache != null)
				Query.CacheOptions = Cache;

			return Query;

		}
	}
}
