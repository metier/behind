﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace jlib.DataFramework
{
    public class BaseCollectionDataReaderBulk<CollectionType, RecordType, ContextType> : BaseCollectionDataReader<CollectionType, RecordType, ContextType>
        where CollectionType : BaseCollection<RecordType, ContextType>, ICollection, new()
        where RecordType : BaseRecord<ContextType>, IRecord, new()
        where ContextType : BaseContext, new() {
        #region ***** CONSTRUCTOR *****
        public BaseCollectionDataReaderBulk(IEnumerable<RecordType> items) 
            : base(items) {
                CurrentIndex = -1;
        }        

        #endregion

        public jlib.helpers.structures.OrderedDictionary<string,int> ColumnMapping{get;private set;}
        public int CurrentIndex { get; private set; }
        private object[] m_oCurrentRowValues;
        private object[] CurrentRowValues {
            get {
                if (m_oCurrentRowValues == null) {
                    m_oCurrentRowValues = new object[this.FieldCount];
                    m_oCurrentRowValues[0] = CurrentIndex;
                    m_oCurrentRowValues[1] = (int)this.Enumerator.Current.RecordState;
                    for (int x = 2; x < this.FieldCount; x++) {
                        m_oCurrentRowValues[x] = this.Enumerator.Current[ColumnMapping[x]];
                    }
                }
                return m_oCurrentRowValues;
            }
        }

        public void SetColumnsToLoad(Dictionary<string, IField> cols) {
            ColumnMapping = new helpers.structures.OrderedDictionary<string,int>();
            ColumnMapping.Add("__Index", -2);
            ColumnMapping.Add("__ChangeType", -1);
            cols.ToList().ForEach(x=>{
                ColumnMapping.Add(x.Key, this.CollectionSchema.Columns.IndexOfKey(x.Value.PropertyName));
            });
            
        }
        
        public override int FieldCount {
            get { return ColumnMapping.Count; }
        }

        public override int GetOrdinal(string fieldName) {
            return ColumnMapping.IndexOfKey(fieldName);            
        }

        public override string GetName(int ordinal) {
            return ColumnMapping.ElementAt(ordinal).Key;
        }

        /// <summary>
        /// Gets the column with the specified name. (Inherited from IDataRecord.)
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public override object this[string fieldName] {
            get { return CurrentRowValues[ColumnMapping[fieldName]]; }
        }

        /// <summary>
        /// Gets the column located at the specified index. (Inherited from IDataRecord.)
        /// </summary>
        /// <param name="ordinal"></param>
        /// <returns></returns>
        public override object this[int ordinal] {
            get { return CurrentRowValues[ordinal]; }
        }

        /// <summary>
        /// Gets the Type information corresponding to the type of Object that would be returned from GetValue. (Inherited from IDataRecord.)
        /// </summary>
        /// <param name="ordinal"></param>
        /// <returns></returns>
        public override Type GetFieldType(int ordinal) {
            if (ordinal == 0 || ordinal == 1) return typeof(int);
            return this.CollectionSchema.Columns[ColumnMapping.ElementAt(ordinal).Value].Type;
        }

        /// <summary>
		/// Advances the IDataReader to the next record.
		/// </summary>
		/// <returns></returns>
		public override bool Read()
		{
            m_oCurrentRowValues = null;
            bool success= this.Enumerator.MoveNext();
            if (success) CurrentIndex++;
            return success;
		}
        /// <summary>
        /// Advances the data reader to the next result, when reading the results of batch SQL statements.
        /// </summary>
        /// <returns></returns>
        public override bool NextResult() {
            return true;
        }
    }
	public class BaseCollectionDataReader<CollectionType, RecordType, ContextType> : IDataReader
		where CollectionType: BaseCollection<RecordType, ContextType>, ICollection, new()
		where RecordType: BaseRecord<ContextType>, IRecord, new()
		where ContextType: BaseContext, new()
	{
		#region ***** CONSTRUCTOR *****
		protected BaseCollectionDataReader(IEnumerable<RecordType> items)
		{
			this.Items = items;
			this.Enumerator = Items.GetEnumerator();
			this.CollectionSchema = new CollectionType();
		}
		#endregion

		#region ***** PROPERTIES *****
		protected internal CollectionType CollectionSchema { get; private set; }
		protected IEnumerable<RecordType> Items { get; private set; }
		protected IEnumerator<RecordType> Enumerator { get; private set; }
		
		/// <summary>
		/// Gets a value indicating whether the data reader is closed.
		/// </summary>
		public bool IsClosed
		{
			get { return this.Enumerator == null; }
		}

		/// <summary>
		/// Gets the number of rows changed, inserted, or deleted by execution of the SQL statement.
		/// </summary>
		public int RecordsAffected
		{
			get { return Items.Count(); }
		}

		/// <summary>
		/// Determines if there is items in the enumerator collection
		/// </summary>
		public bool HasRows
		{
			get { return this.Items != null && this.Items.Count() > 0; }
		}

		/// <summary>
		/// Gets the column with the specified name. (Inherited from IDataRecord.)
		/// </summary>
		/// <param name="fieldName"></param>
		/// <returns></returns>
        public virtual object this[string fieldName]
	    {
			get { return this.Enumerator.Current.Fields.GetFieldValueByFieldName(fieldName); }
		}

		/// <summary>
		/// Gets the column located at the specified index. (Inherited from IDataRecord.)
		/// </summary>
		/// <param name="ordinal"></param>
		/// <returns></returns>
		public virtual object this[int ordinal]
		{
			get { return this[this.GetName(ordinal)]; }
		}

		/// <summary>
		/// Gets the number of columns in the current row. (Inherited from IDataRecord.)
		/// </summary>
		public virtual int FieldCount
		{
			get { return this.CollectionSchema.Columns.Count; }
		}

		/// <summary>
		/// Gets a value indicating the depth of nesting for the current row.
		/// </summary>
		public int Depth
		{
			get { return 0; }
		}

		#endregion


		#region ***** METHODS *****
		/// <summary>
		/// Closes the IDataReader Object.
		/// </summary>
		public void Close()
		{
			this.Enumerator = null;
			this.Items = null;
		}

		/// <summary>
		/// Advances the IDataReader to the next record.
		/// </summary>
		/// <returns></returns>
		public virtual bool Read()
		{
			return this.Enumerator.MoveNext();
		}

		/// <summary>
		/// Advances the data reader to the next result, when reading the results of batch SQL statements.
		/// </summary>
		/// <returns></returns>
		public virtual bool NextResult()
		{
			return Read();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="fieldName"></param>
		/// <returns></returns>
		public virtual int GetOrdinal(string fieldName)
		{
			string key = this.CollectionSchema.Columns.FirstOrDefault(x=>x.Value.FieldName == fieldName).Key;
			return this.CollectionSchema.Columns.IndexOfKey(key);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ordinal"></param>
		/// <returns></returns>
        public virtual string GetName(int ordinal)
		{
			return this.CollectionSchema.Columns[ordinal].FieldName;
		}

		/// <summary>
		/// Populates an array of objects with the column values of the current record. (Inherited from IDataRecord.)
		/// </summary>
		/// <param name="values"></param>
		/// <returns></returns>
		public virtual int GetValues(object[] values)
		{
			int count = 0;
			for (int i = 0; i < Math.Min(values.Count(), this.FieldCount); i++)
			{
				count++;
				values[i] = this[i];
			}

			return count;
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources. (Inherited from IDisposable.)
		/// </summary>
		public void Dispose() { }

		/// <summary>
		/// Gets the Type information corresponding to the type of Object that would be returned from GetValue. (Inherited from IDataRecord.)
		/// </summary>
		/// <param name="ordinal"></param>
		/// <returns></returns>
        public virtual Type GetFieldType(int ordinal) 
		{
			return this.CollectionSchema.Columns[ordinal].Type; 
		}

		/// <summary>
		/// Gets the data type information for the specified field. (Inherited from IDataRecord.)
		/// </summary>
		/// <param name="ordinal"></param>
		/// <returns></returns>
		public string GetDataTypeName(int ordinal) 
		{ 
			return this.GetFieldType(ordinal).ToString(); 
		}

		public bool CanBulkInsert(string fieldName)
		{
			var field = this.CollectionSchema.Columns.FirstOrDefault(x => x.Value.FieldName == fieldName);

			if (field.Value.IsIdentity) return false;

			//default
			return true;
		}


		public object GetValue(int ordinal) { return this[ordinal]; }
		public string GetString(int ordinal) { return (string)this[ordinal]; }
		public bool GetBoolean(int ordinal) { return (bool)this[ordinal]; }
		public char GetChar(int ordinal) { return (char)this[ordinal]; }
		public DateTime GetDateTime(int ordinal) { return (DateTime)this[ordinal]; }
		public decimal GetDecimal(int ordinal) { return (decimal)this[ordinal]; }
		public double GetDouble(int ordinal) { return (double)this[ordinal]; }
		public float GetFloat(int ordinal) { return (float)this[ordinal]; }
		public Guid GetGuid(int ordinal) { return (Guid)this[ordinal]; }
		public short GetInt16(int ordinal) { return (short)this[ordinal]; }
		public int GetInt32(int ordinal) { return (int)this[ordinal]; }
		public long GetInt64(int ordinal) { return (long)this[ordinal]; }
		public bool IsDBNull(int ordinal) { return this[ordinal] == null; }


		//Not Implemented Methods
		public byte GetByte(int i) { throw new NotImplementedException(); }
		public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length) { throw new NotImplementedException(); }
		public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length) { throw new NotImplementedException(); }
		public IDataReader GetData(int i) { throw new NotImplementedException(); }
		public DataTable GetSchemaTable() { throw new NotImplementedException(); }
		#endregion


		
		
		

		
		

	}
}
