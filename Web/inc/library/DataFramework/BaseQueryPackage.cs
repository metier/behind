﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jlib.helpers.Structures;
using jlib.DataFramework.Cache;
using jlib.DataFramework.Paging;

namespace jlib.DataFramework
{
	public abstract class BaseQueryPackage<CollectionType>
		where CollectionType : ICollection, new()
	{
		#region ***** CONSTRUCTORS *****
		public BaseQueryPackage()
		{
			this.Parameters = new DynamicDictionary();
			this.SearchForCustomSortColumn = true;
			this.CustomSortColumnName = "jLibSortOrder";
			this.CustomSortDirection = QueryBuilder.OrderByDirections.Ascending;
		}
		#endregion

		#region ***** PROPERTIES *****
		private string _Statement = null;
		public string Statement 
		{ 
			get { return this._Statement; }			
			
			set
			{
				this._Statement = value;

				//If containts customer sort column
				if (this.SearchForCustomSortColumn && (this._Statement ?? "").IndexOf(this.CustomSortColumnName, StringComparison.CurrentCultureIgnoreCase) > 0)
				{
					//Create Custom Column
					IColumn sortColumn = new BaseColumn<string>(this.CustomSortColumnName, this.CustomSortColumnName, "@" + this.CustomSortColumnName, System.Data.DbType.String, false, true);

					//Create a builder
					this.InternalQueryBuilder = this.ToQueryBuilder();

					//Add Condition
					this.InternalQueryBuilder.AddOrderBy( new QueryBuilder.OrderBy(sortColumn, this.CustomSortDirection) );

					//convert back QueryPackage
					this.InternalQueryBuilder.ToQueryPackage();

				}

			}
		}
		public bool IsStoredProcedure { get; set; }
		public dynamic Parameters { get; set; }
		public CacheOptions CacheOptions { get; set; }
		public PagingOptions PagingOptions { get; set; }
		public QueryBuilder.QueryBuilder InternalQueryBuilder { get; set; }
		public bool SearchForCustomSortColumn { get; set; }
		public string CustomSortColumnName { get; set; }
		public QueryBuilder.OrderByDirections CustomSortDirection { get; set; }
		#endregion

		#region ***** ABSTRACT METHODS *****
		public abstract CollectionType Execute();
		public abstract CollectionType Select();
		public abstract void Delete();
		public abstract ColumnCollection Columns { get; }
		public abstract QueryBuilder.QueryBuilder ToQueryBuilder();
		#endregion

		#region ***** METHODS *****
		public void SetInternalStatement(string Statement)
		{
			this._Statement = Statement;
		}
		public virtual QueryPackageType OrderBy<QueryPackageType>(IColumn Column, QueryBuilder.OrderByDirections Direction)
			where QueryPackageType : IQueryPackage, new()
		{
			//Create a builder
			QueryBuilder.QueryBuilder builder = this.ToQueryBuilder();

			//Add Condition
			builder.AddOrderBy( new QueryBuilder.OrderBy(Column, Direction) );

			//Return QueryPackage
            return (QueryPackageType)builder.ToQueryPackage();
		}

		public virtual QueryPackageType Where<QueryPackageType>(QueryBuilder.BaseCondition Condition)
			where QueryPackageType: IQueryPackage, new()
		{
			//Create a builder
			QueryBuilder.QueryBuilder builder = this.ToQueryBuilder();

			//Add Condition
			builder.Conditions.Add(Condition);

			//Return QueryPackage
            return (QueryPackageType)builder.ToQueryPackage();
		}

		public virtual QueryPackageType And<QueryPackageType>(QueryBuilder.BaseCondition Condition)
			where QueryPackageType : IQueryPackage, new()
		{
			//Create a builder
			QueryBuilder.QueryBuilder builder = this.ToQueryBuilder();

			//Add Condition
			builder.Conditions.Add(Condition);

			var and = new QueryBuilder.AndCondition(builder.Conditions);
			builder.Conditions = new List<QueryBuilder.BaseCondition>() { and };

			//Return QueryPackage
			return (QueryPackageType)builder.ToQueryPackage();
		}

		public virtual QueryPackageType Or<QueryPackageType>(QueryBuilder.BaseCondition Condition)
			where QueryPackageType : IQueryPackage, new()
		{
			//Create a builder
			QueryBuilder.QueryBuilder builder = this.ToQueryBuilder();

			//Add Condition
			builder.Conditions.Add(Condition);

			var or = new QueryBuilder.OrCondition(builder.Conditions);
			builder.Conditions = new List<QueryBuilder.BaseCondition>() { or };

			//Return QueryPackage
			return (QueryPackageType)builder.ToQueryPackage();
		}

		public virtual QueryBuilder.QueryBuilder ToQueryBuilder<QueryPackageType,ContextType>()
			where QueryPackageType: IQueryPackage, new()
            where ContextType : BaseContext, new()
		{
			//If Internal QueryBuilder exists, then use
            if (this.InternalQueryBuilder != null)
			{
				var builder = this.InternalQueryBuilder;
				if (this.PagingOptions != null) builder.PagingOptions = this.PagingOptions;
				if (this.CacheOptions != null) builder.CacheOptions = this.CacheOptions;

				return builder;
			}

            //Else, Create Builder
            else
			{
                QueryPackageType newQuery = new QueryPackageType();
				var builder = new QueryBuilder.QueryBuilder( (IQueryPackage)this, newQuery, new ContextType());

                return builder;
            }
		}
		
		#endregion
	}
}
