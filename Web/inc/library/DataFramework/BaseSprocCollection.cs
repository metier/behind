﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.DataFramework
{
	public abstract class BaseSprocCollection<RecordType, ContextType> : BaseCollection<RecordType, ContextType>
		where RecordType : BaseRecord<ContextType>, IRecord, new()
		where ContextType : BaseContext, new()
	{
		public BaseSprocCollection(ContextType Context) : base(Context) { }
	}
}
