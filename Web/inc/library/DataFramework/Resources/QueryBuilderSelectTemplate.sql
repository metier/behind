﻿{InnerQueryWithBlock}
{WithTag} {InnerQueryResultsName} AS
(
	{InnerQueryStatement}
),
{OuterQueryResultsName} AS
(
	SELECT
		{InnerQueryResultsName}.*,
		ROW_NUMBER() OVER( {OrderByClause} ) AS {RowNumberColumnName}
	FROM
		{InnerQueryResultsName}
	{PreConditionsClause}
)

SELECT
	{OuterQueryResultsName}.*,
	(SELECT COUNT(*) FROM {OuterQueryResultsName}) AS {RecordCountColumnName}
FROM
	{OuterQueryResultsName}
{PostConditionsClause}
{OrderByClause}


