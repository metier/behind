﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace jlib.DataFramework.Resources
{
	public class ResourceHelper
	{
		private string _QueryBuilderTemplate = null;
		public string QueryBuilderSelectTemplate
		{
			get
			{
				if (this._QueryBuilderTemplate == null) this._QueryBuilderTemplate = this.GetResourceTextfile("QueryBuilderSelectTemplate.sql");

				return this._QueryBuilderTemplate;
			}
		}
		public string QueryBuilderDeleteTemplate
		{
			get
			{
				if (this._QueryBuilderTemplate == null) this._QueryBuilderTemplate = this.GetResourceTextfile("QueryBuilderDeleteTemplate.sql");

				return this._QueryBuilderTemplate;
			}
		}

		private string _OracleQueryBuilderTemplate = null;
        public string OracleQueryBuilderTemplate {
            get {
                if (this._OracleQueryBuilderTemplate == null) this._OracleQueryBuilderTemplate = this.GetResourceTextfile("OracleQueryBuilderTemplate.sql");

                return this._OracleQueryBuilderTemplate;
            }
        }



		private Stream GetResourceStream(string FileName)
		{
			return Assembly.GetExecutingAssembly().GetManifestResourceStream(String.Format("jlib.DataFramework.Resources.{0}", FileName));
		}
		private string GetResourceTextfile(string FileName)
		{
			string contents = "";
			using (StreamReader reader = new StreamReader(this.GetResourceStream(FileName)))
			{
				contents = reader.ReadToEnd();
			}

			return contents;
		}
	}
}
