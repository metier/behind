﻿{InnerQueryWithBlock}
{WithTag} {InnerQueryResultsName} AS
(
	{InnerQueryStatement}
),
{OuterQueryResultsName} AS
(
	SELECT
		{InnerQueryResultsName}.*,
		ROW_NUMBER() OVER( {OrderByClause} ) AS {RowNumberColumnName}
	FROM
		{InnerQueryResultsName}
	{PreConditionsClause}
)

DELETE
FROM
	{OuterQueryResultsName}
{PostConditionsClause}



