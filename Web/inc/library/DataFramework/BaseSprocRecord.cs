﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.DataFramework
{
	public abstract class BaseSprocRecord<ContextType> : BaseRecord<ContextType>
		where ContextType : BaseContext, new()
	{
		public BaseSprocRecord(ContextType Context) : base(Context) { }
	}
}
