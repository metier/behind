﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;

namespace jlib.DataFramework.Cache
{
    public class CacheEngine
	{
		#region ***** CONSTRUCTORS *****
		public CacheEngine()
		{
			this.Cache = MemoryCache.Default;
		}
		#endregion

		#region **** PROPERTIES *****
		protected MemoryCache Cache { get; set; }
		#endregion

		#region ***** METHODS *****
		public bool HasItem(string CacheKey)
		{
			//Make cachekey lower
			CacheKey = CacheKey.ToLower();

			return this.Cache[CacheKey] != null;
		}

		public void Add(ICacheItem CacheItem)
		{
			//Cache Key
			string cacheKey = CacheItem.CacheKey.ToLower();

			//Update CacheItem
			CacheItem.CacheKey = cacheKey;
			
			var item = new System.Runtime.Caching.CacheItem(cacheKey, CacheItem);

			var policy = new System.Runtime.Caching.CacheItemPolicy();
			switch(CacheItem.CachePeriod)
			{
				case CachePeriods.AbsoluteExpire:
					policy.AbsoluteExpiration = CacheItem.Expires;
					break;

				case CachePeriods.ExpireImmediate:
					policy.AbsoluteExpiration = DateTimeOffset.MinValue;
					break;

				case CachePeriods.NeverExpire:
					policy.AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration;
					break;

				case CachePeriods.RelativeExpire:
					policy.SlidingExpiration = CacheItem.Interval;
					break;
			}

			//Check if key already exists, if so then update
			if (this.HasItem(cacheKey))
			{
				this.Cache.Set(item, policy);
			}

			//else Add
			else
			{
				this.Cache.Add(item, policy);
			}
		}

		public ICacheItem this[string CacheKey]
		{
			get
			{
				//Standardize CacheKey
				string cacheKey = CacheKey.ToLower();

				return (ICacheItem)this.Cache[cacheKey];
			}
		}
        #endregion


    }
}
