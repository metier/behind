﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.DataFramework.Cache
{
    public abstract class CacheItem : ICacheItem
	{
		#region ***** CONSTRUCTORS *****
		public CacheItem(string CacheKey, CachePeriods CachePeriod, TimeSpan Interval, object Value)
		{
			this.CacheKey = CacheKey;
			this.CachePeriod = CachePeriod;
			this.Interval = Interval;
			this.Value = Value;
		}
		#endregion

		#region ***** PROPERTIES *****
		public virtual string CacheKey { get; set; }
		public virtual CachePeriods CachePeriod { get; set; }
		public virtual DateTime Inserted { get; set; }
		public virtual DateTime LastAccessed { get; set; }
		public virtual TimeSpan Interval { get; set; }

		/// <summary>
		/// The Value within the Cache
		/// </summary>
		public virtual object Value 
		{ 
			get
			{
				//Reset Last Accessed Date
				this.LastAccessed = DateTime.Now;
				
				//Return Value
				return this._Value;
			}
			set
			{
				//Reset Inserted Date
				this.Inserted = DateTime.Now;

				//Reset Last Accessed Date
				this.LastAccessed = DateTime.Now;

				//Set Value
				this._Value = value;
			}
		}
		private object _Value = null;
		
		/// <summary>
		/// Determines the Expiration Date
		/// </summary>
		public virtual DateTime Expires 
		{ 
			get
			{
				switch(this.CachePeriod)
				{
					case CachePeriods.NeverExpire:
						return DateTime.MaxValue;
						break;

					case CachePeriods.AbsoluteExpire:
						return this.Inserted.Add(this.Interval);
						break;

					case CachePeriods.RelativeExpire:
						return this.LastAccessed.Add(this.Interval);
						break;

					case CachePeriods.ExpireImmediate:
						return DateTime.MinValue;
						break;

					default:
						return DateTime.MinValue;
				}
			}
		}

		/// <summary>
		/// Determines if it is expired based on the expiration date
		/// </summary>
		public virtual bool IsExpired 
		{
		    get
		    {
				return (this.Expires < DateTime.Now);
		    }
		}
		
		#endregion

	   
	   

    }
}
