﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.DataFramework.Cache
{
	public class CacheOptions
	{
		public CacheOptions()
		{
			this.CachePeriod = CachePeriods.ExpireImmediate;
			this.Interval = new TimeSpan();
			this.LoadFromCache = true;
			this.RefreshCache = false;
		}

		public CachePeriods CachePeriod { get; set; }
		public TimeSpan Interval { get; set; }
		public bool LoadFromCache { get; set; }
		public bool RefreshCache { get; set; }
	}
}
