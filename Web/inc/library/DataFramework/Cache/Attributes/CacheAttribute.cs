﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace jlib.DataFramework.Cache.Attributes
{
	[System.AttributeUsage(System.AttributeTargets.Method | AttributeTargets.Class | System.AttributeTargets.Property)]
	public class CacheAttribute : System.Attribute
	{
		public CacheOptions Options = new CacheOptions();

		public CacheAttribute(CachePeriods CachePeriod, TimeSpan Interval)
		{
			this.Options.CachePeriod = CachePeriod;
			this.Options.Interval = Interval;
		}

		public CacheAttribute(CachePeriods CachePeriod, int Hours=0, int Minutes=0, int Seconds=0)
		{
			this.Options.CachePeriod = CachePeriod;
			this.Options.Interval = new TimeSpan(Hours, Minutes, Seconds);
		}

		public static CacheOptions GetCacheOptionsFromMethodStack(int StackFrame)
		{
			try
			{
				//Get the specified method handler and its custom attributes
				object[] attributes = new StackFrame(StackFrame).GetMethod().GetCustomAttributes(true);


				//Loop throguh all custom attributes
				foreach (object attribute in attributes)
				{
					//If a CacheAttribute is found
					if (attribute as Cache.Attributes.CacheAttribute != null)
					 {
						//Get Found Attribute
						Cache.Attributes.CacheAttribute foundAttribute = (attribute as Cache.Attributes.CacheAttribute);

						//Return Cache Options
						return foundAttribute.Options;

						break;
					}
				}
			}
			catch (Exception e) { }

			//No attributes found, return null
			return null;
		}

		public static CacheOptions GetCacheOptionsFromPreviousMethod()
		{
			return CacheAttribute.GetCacheOptionsFromMethodStack(3);
		}

		public static CacheOptions GetCacheOptionsFromCurrentMethod()
		{
			return CacheAttribute.GetCacheOptionsFromMethodStack(2);
		}
	}
}
