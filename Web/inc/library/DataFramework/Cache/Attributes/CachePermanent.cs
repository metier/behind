﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.DataFramework.Cache.Attributes
{
	public class CachePermanent : CacheAttribute
	{
		/// <summary>
		/// Add this item to cache and keep forever
		/// </summary>
		public CachePermanent() : base(CachePeriods.NeverExpire, TimeSpan.MaxValue)
		{

		}


	}
}
