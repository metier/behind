﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.DataFramework.Cache.Attributes
{
	public class CacheRelative : CacheAttribute
	{
		/// <summary>
		/// Add this item to cache  and keep for defined period since last used
		/// </summary>
		/// <param name="Days">Days to keep item in cache</param>
		/// <param name="Hours">Hours to keep item in cache</param>
		/// <param name="Minutes">Minutes to keep item in cache</param>
		/// <param name="Seconds">Seconds to keep item in cache</param>
		/// <param name="Milliseconds">Milliseconds to keep item in cache</param>
		public CacheRelative(int Days = 0, int Hours = 0, int Minutes = 0, int Seconds = 0, int Milliseconds = 0) : base(CachePeriods.RelativeExpire, new TimeSpan(Days, Hours, Minutes, Seconds, Milliseconds))
		{

		}


	}
}
