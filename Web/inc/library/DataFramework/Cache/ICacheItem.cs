﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Cache;

namespace jlib.DataFramework.Cache
{
    public enum CachePeriods
    {
		AbsoluteExpire,
		RelativeExpire,
		NeverExpire,
		ExpireImmediate
    }

    public interface ICacheItem
    {
		//Properties
		string CacheKey { get; set; }
		object Value { get; set; }
		CachePeriods CachePeriod { get; set; }
		DateTime Inserted { get; set; }
		DateTime LastAccessed { get; set; }
		TimeSpan Interval { get; set; }
		
		
		bool IsExpired { get; }
		DateTime Expires { get; }
    }

}
