﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;


namespace jlib.DataFramework
{
	public static class AutoCache
	{
		public static Cache.CacheEngine Engine = new Cache.CacheEngine();
	}

}

namespace jlib.DataFramework.Cache
{
	public static class CacheHelper
	{
		public static T GetCacheItem<T>(Cache.CacheOptions CacheOptions, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters) where T : class
		{
			//If No CacheOptions, or CacheOption require a new query to db, then return null
			if (CacheOptions == null || !CacheOptions.LoadFromCache || CacheOptions.RefreshCache)
				return null;

			//Get Key
			string cacheKey = Cache.DataCacheItem.BuildCacheKey(typeof(T), Command, Parameters);

			//Return cache
			return GetCacheItem<T>(CacheOptions, cacheKey);

		}

		public static T GetCacheItem<T>(Cache.CacheOptions CacheOptions, string Command, List<object> Parameters) where T : class
		{
			//If No CacheOptions, or CacheOption require a new query to db, then return null
			if (CacheOptions == null || !CacheOptions.LoadFromCache || CacheOptions.RefreshCache)
				return null;

			//Get Key
			string cacheKey = Cache.DataCacheItem.BuildCacheKey(typeof(T), Command, Parameters);

			//Return cache
			return GetCacheItem<T>(CacheOptions, cacheKey);

		}

		public static T GetCacheItem<T>(Cache.CacheOptions CacheOptions, string CacheKey) where T : class
		{
			//If No CacheOptions, or CacheOption require a new query to db, then return null
			if (CacheOptions == null || !CacheOptions.LoadFromCache || CacheOptions.RefreshCache)
				return null;

			
			//else, check cache
			if (AutoCache.Engine.HasItem(CacheKey))
			{
				//Get Item
				Cache.DataCacheItem cacheItem = AutoCache.Engine[CacheKey] as Cache.DataCacheItem;

				//Return CacheItem
				return (cacheItem.Value as T);
			}

			//Return null
			return null;

		}


		public static void SetCacheItem<T>(Cache.CacheOptions CacheOptions, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters, T Value) where T : class
		{
			//If No CacheOptions, or CacheOption says dont cache
			if (CacheOptions == null || CacheOptions.CachePeriod == Cache.CachePeriods.ExpireImmediate) return;
			
			//Get Key
			string cacheKey = Cache.DataCacheItem.BuildCacheKey(typeof(T), Command, Parameters);

			//Set Cache
			SetCacheItem(CacheOptions, cacheKey, Value);
		}

		public static void SetCacheItem<T>(Cache.CacheOptions CacheOptions, string Command, List<object> Parameters, T Value) where T : class
		{
			//If No CacheOptions, or CacheOption says dont cache
			if (CacheOptions == null || CacheOptions.CachePeriod == Cache.CachePeriods.ExpireImmediate) return;
			
			//Get Key
			string cacheKey = Cache.DataCacheItem.BuildCacheKey(typeof(T), Command, Parameters);

			//Set Cache
			SetCacheItem(CacheOptions, cacheKey, Value);
		}

		public static void SetCacheItem<T>(Cache.CacheOptions CacheOptions, string CacheKey, T Value) where T : class
		{
			//If No CacheOptions, or CacheOption says dont cache
			if (CacheOptions == null || CacheOptions.CachePeriod == Cache.CachePeriods.ExpireImmediate) return;
			
			//See if exists in cache
			bool existsInCache = AutoCache.Engine.HasItem(CacheKey);

			//If refresh, then
			if (existsInCache && CacheOptions.RefreshCache)
			{
				//Update Cache
				Cache.DataCacheItem cacheItem = AutoCache.Engine[CacheKey] as Cache.DataCacheItem;
				cacheItem.CachePeriod = CacheOptions.CachePeriod;
				cacheItem.Interval = CacheOptions.Interval;
				cacheItem.Value = Value;
			}

			//else, add to cache
			else if(!existsInCache)
			{
				Cache.DataCacheItem cacheItem = new Cache.DataCacheItem(CacheKey, CacheOptions.CachePeriod, CacheOptions.Interval, Value);
				AutoCache.Engine.Add(cacheItem);
			}
		}

	}
}