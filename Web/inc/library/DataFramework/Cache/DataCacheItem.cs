﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.Common;


namespace jlib.DataFramework.Cache
{
	public class DataCacheItem : CacheItem, ICacheItem
	{
		#region ***** CONSTRUCTORS *****
		public DataCacheItem(string CacheKey, CachePeriods CachePeriod, TimeSpan Interval, object Value) : base(CacheKey, CachePeriod, Interval, Value)
		{

		}
		#endregion

		#region ***** PROPERTIES *****

		#endregion

		#region ***** METHODS *****
		#endregion

		#region ***** STATIC METHODS *****
		public static string BuildCacheKey(Type DataType, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{
			//Format
			//		data-<type>-<command>-<@p1>,<p1>-<@p2>,<p2>
			string cacheKey = "data";

			//Append DataType
			cacheKey += "-" + DataType.FullName;

			//Append Command
			cacheKey += "-" + Command;

			//Append Parameters
			for(int x=0;x<Parameters.Count;x++)
				cacheKey += "-" + Parameters[x].ParameterName + "," + Parameters[x].Value.ToString();

			//Return Cache Key
			return cacheKey.ToLower();
		}
		public static string BuildCacheKey(Type DataType, string Command, List<object> Parameters)
		{
			//Format
			//		data-<type>-<command>-<@p1>,<p1>-<@p2>,<p2>
			string cacheKey = "data";

			//Append DataType
			cacheKey += "-" + DataType.FullName;

			//Append Command
			cacheKey += "-" + Command;

			//Append Parameters
			for(int x=0;x<Parameters.Count;x++)
				cacheKey += String.Format("-{0}", Parameters[x]);

			//Return Cache Key
			return cacheKey.ToLower();
		}
		#endregion

	}
}
