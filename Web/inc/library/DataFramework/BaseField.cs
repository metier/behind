﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using jlib.functions;

namespace jlib.DataFramework
{
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class Field : System.Attribute
    {
        public string FieldName;
        public string Table;
		public bool IsPrimaryKey = false;
		public bool IsForeignKey = false;
		public bool IsNullable = false;
		public bool IsIdentity = false;

        public Field(string Table, string FieldName, bool IsPrimaryKey = false, bool IsForeignKey = false, bool IsNullable = false, bool IsIdentity = false)
        {
            this.Table = Table;
            this.FieldName = FieldName;
			this.IsPrimaryKey = IsPrimaryKey;
			this.IsForeignKey = IsForeignKey;
			this.IsNullable = IsNullable;
			this.IsIdentity = IsIdentity;
        }
    }

	public enum FieldStatuses
	{
		NotSet,
		Loaded,
		Modified
	}

	public class BaseField<FieldType> : BaseColumn<FieldType>, IField
	{

		#region ***** CONSTRUCTORS *****
		public BaseField(IRecord Record, string FieldName, string PropertyName, string ParameterName, string PkParameterName, object InitialValue, DbType DBType, bool IsPrimaryKey = false, FieldStatuses FieldStatus = FieldStatuses.Loaded, bool IsIdentity = false, bool IsNullable = false, bool IsVirtualColumn = false) : base(FieldName, PropertyName, ParameterName, PkParameterName, DBType, IsPrimaryKey: IsPrimaryKey, IsIdentity: IsIdentity, IsNullable: IsNullable, IsVirtualColumn: IsVirtualColumn)
		{
			this.Record = Record;
			this.FieldStatus = FieldStatus;
			this.RawValue = InitialValue;
			this.OriginalRawValue = InitialValue;
		}
		public BaseField(IRecord Record, string FieldName, string PropertyName, string ParameterName, object InitialValue, DbType DBType, bool IsPrimaryKey = false, FieldStatuses FieldStatus = FieldStatuses.Loaded, bool IsIdentity = false, bool IsNullable = false, bool IsVirtualColumn = false) : base(FieldName, PropertyName, ParameterName, DBType, IsPrimaryKey: IsPrimaryKey, IsIdentity: IsIdentity, IsNullable: IsNullable, IsVirtualColumn: IsVirtualColumn)
		{
			this.Record = Record;
			this.FieldStatus = FieldStatus;
			this.RawValue = InitialValue;
			this.OriginalRawValue = InitialValue;
		}
		public BaseField(IRecord Record, IColumn Column, object InitialValue, FieldStatuses FieldStatus = FieldStatuses.Loaded) : base(Column)
		{
			this.Record = Record;
			this.FieldStatus = FieldStatus;
			this.RawValue = InitialValue;
			this.OriginalRawValue = InitialValue;
		}
		#endregion

		#region ***** PROPERTIES
		public FieldStatuses FieldStatus { get; set; }
		public object RawValue { get; private set; }
		public object OriginalRawValue { get; private set; }
		public IRecord Record { get; private set; }

		//The Type of the Field
        public Type ObjectType 
		{
            get 
			{
                return typeof(FieldType);
            }
        }

		/// <summary>
		///     The Field Value
		/// </summary>
		public FieldType Value 
		{
			get
			{
				if (this.RawValue == null) return default(FieldType);
				else if (typeof(FieldType) == typeof(short)) return (FieldType)(object)Convert.ToInt16(this.RawValue);
				else
					return (FieldType)this.RawValue; 
			}
			set
			{
				if(
					(this.FieldStatus == FieldStatuses.NotSet)
					|| (this.RawValue == null && value != null)
					|| (this.RawValue != null && value == null)
					|| (!this.RawValue.Equals(value))
					)
				{
					//Set Value
					this.RawValue = value;

					//Change Status
					this.FieldStatus = FieldStatuses.Modified;
					if (this.Record.RecordState != DataRowAction.Add) this.Record.RecordState = DataRowAction.Change;
				}
			}
		}


		/// <summary>
		///     The Original Field Value from the last initialization
		/// </summary>
		public FieldType OriginalValue
		{
			get
			{
				return (FieldType)this.OriginalRawValue;
			}
		}

		
		#endregion

		#region ***** METHODS *****
        //public IField Clone() {
        //    BaseField<FieldType> NewObject = new BaseField<FieldType>(this.FieldName, this.PropertyName, this.ParameterName, this.Value, this.DBType, IsPrimaryKey, this.FieldStatus, IsVirtualColumn);
        //    NewObject.OriginalRawValue = this.OriginalRawValue;
        //    NewObject.RawValue = this.RawValue;
        //    return NewObject;
        //}
		public object GetSqlValue(db.SqlHelper.ConnectionTypes ConnectionType, bool UseOriginalValue)
		{
			
            object Value = (UseOriginalValue ? this.OriginalRawValue : this.RawValue);
			
			//If type date, make sure value is correct
			if ((this.DBType == DbType.Date || this.DBType == DbType.DateTime || this.DBType == DbType.DateTime2))
			{
                if (Value == null || Value == DBNull.Value || (jlib.functions.convert.isDate(Value) && jlib.functions.convert.cDate(Value) == DateTime.MinValue))
				{
                    //With Oracle, we're supplying all values to the SPROCs, so NULL is a valid value for dates
					if (ConnectionType == db.SqlHelper.ConnectionTypes.MSSQL)
					{
						return (this.IsNullable) ? null : (object)DateTime.Parse("1/1/1754 00:00:00");
					}
					else
						return null;
				}
				else
                    return Value;
			}

			else if (this.DBType == DbType.Guid)
			{
                if ((Value == null || (Guid)Value == Guid.Empty) && this.IsPrimaryKey)
					return Guid.NewGuid();
				else
                    return Value ?? DBNull.Value;
			}

			else
                return Value ?? DBNull.Value;

		}
		public void SetLoadedValue(object Value)
		{
			this.OriginalRawValue = Value;
			this.RawValue = Value;
			this.FieldStatus = FieldStatuses.Loaded;
		}

		public void SetChangedValue(object Value)
		{
			if ((this.RawValue != null && !this.RawValue.Equals(Value)) || (this.RawValue == null && Value != null) || this.FieldStatus == FieldStatuses.NotSet)
			{
				this.RawValue = Value;
				this.FieldStatus = FieldStatuses.Modified;
				if (this.Record.RecordState != DataRowAction.Add) this.Record.RecordState = DataRowAction.Change;
			}
		}

		public void SetChangedValueIfDifferent(object Value)
		{
			if ((this.RawValue != null && !this.RawValue.Equals(Value)) || (this.RawValue == null && Value != null))
			{
				this.RawValue = Value;
				this.FieldStatus = FieldStatuses.Modified;
				if (this.Record.RecordState != DataRowAction.Add) this.Record.RecordState = DataRowAction.Change;
			}
		}

		public void ResetValue()
		{
			this.SetLoadedValue(this.OriginalRawValue);
            this.FieldStatus = FieldStatuses.Loaded;
		}
		public DbParameter BuildSqlParameter(db.SqlHelper.ConnectionTypes ConnectionType, bool UseOriginalValue)
		{
			//If Oracle
			if (ConnectionType == db.SqlHelper.ConnectionTypes.Oracle)
			{
				return new System.Data.OleDb.OleDbParameter()
						{
							ParameterName = this.ParameterName + (UseOriginalValue ? "_Original" : ""),
							Value = this.GetSqlValue(ConnectionType, UseOriginalValue),
							Direction = ParameterDirection.Input,
							DbType = this.DBType
						};
						
			}

			//Else, SQL
			else
			{
				//If string and larger than 2000 characters => use text field
				if((this.DBType == DbType.AnsiString || this.DBType == DbType.String) && this.Value.Str().Length > 2000)
				{ 
					return new SqlParameter()
						{
							ParameterName = this.ParameterName + (UseOriginalValue ? "_Original" : ""),
							Value = this.GetSqlValue(ConnectionType, UseOriginalValue),
							Direction = ParameterDirection.Input,

							SqlDbType = System.Data.SqlDbType.Text
						};
				}

				else
				{ 
					return new SqlParameter()
						{
							ParameterName = this.ParameterName + (UseOriginalValue ? "_Original" : ""),
							Value = this.GetSqlValue(ConnectionType, UseOriginalValue),
							Direction = ParameterDirection.Input,
							//JVT: if this is string, return nvarchar type
							DbType = (this.DBType == DbType.AnsiString ? DbType.String : this.DBType)
						};
				}
			}

		}
		public override string ToString()
		{
			return this.Value.Str();
		}
		#endregion

	}
}
