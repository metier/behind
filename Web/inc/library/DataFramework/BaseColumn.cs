﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using jlib.DataFramework.QueryBuilder;
using System.Collections;

namespace jlib.DataFramework
{
	public class BaseColumn<FieldType> : IColumn
	{

		#region ***** CONSTRUCTORS *****
		public BaseColumn(string FieldName, string PropertyName, string ParameterName, DbType DBType, bool IsPrimaryKey = false, bool IsIdentity = false, bool IsNullable = false, bool HasDbDefault = false, bool IsVirtualColumn = false)
		{
			this.FieldName = FieldName;
			this.PropertyName = PropertyName;
			this.ParameterName = ParameterName;
			this.PkParameterName = ParameterName;
			this.DBType = DBType;
			this.IsPrimaryKey = IsPrimaryKey;
			this.IsIdentity = IsIdentity;
			this.IsNullable = IsNullable;
			this.HasDbDefault = HasDbDefault;
			this.IsVirtualColumn = IsVirtualColumn;
		}
		public BaseColumn(string FieldName, string PropertyName, string ParameterName, string PkParameterName, DbType DBType, bool IsPrimaryKey = false, bool IsIdentity = false, bool IsNullable = false, bool HasDbDefault = false, bool IsVirtualColumn = false)
		{
			this.FieldName = FieldName;
			this.PropertyName = PropertyName;
			this.ParameterName = ParameterName;
			this.PkParameterName = PkParameterName;
			this.DBType = DBType;
			this.IsPrimaryKey = IsPrimaryKey;
			this.IsIdentity = IsIdentity;
			this.IsNullable = IsNullable;
			this.HasDbDefault = HasDbDefault;
			this.IsVirtualColumn = IsVirtualColumn;
			
		}
		public BaseColumn(IColumn Column)
		{
			this.FieldName = Column.FieldName;
			this.PropertyName = Column.PropertyName;
			this.ParameterName = Column.ParameterName;
			this.PkParameterName = Column.PkParameterName;
			this.DBType = Column.DBType;
			this.IsIdentity = Column.IsIdentity;
			this.IsPrimaryKey = Column.IsPrimaryKey;
			this.IsVirtualColumn = Column.IsVirtualColumn;
			this.IsNullable = Column.IsNullable;
			this.HasDbDefault = Column.HasDbDefault;
		}

		#endregion

		#region ***** PROPERTIES *****
		public string FieldName { get; private set; }
		public string PropertyName { get; private set; }
		public string ParameterName { get; private set; }
		public string PkParameterName { get; private set; }
		public bool IsPrimaryKey { get; private set; }
		public bool IsVirtualColumn { get; private set; }
		public bool IsIdentity { get; private set; }
		public bool IsNullable { get; private set; }
		public bool HasDbDefault { get; private set; }
		public DbType DBType { get; private set; }
		public bool IsStandardDefaultColumn 
		{ 
			get
			{
				List<string> fieldList = new List<string>() { "ID", "created_date", "modified_date", "posted_date", "CreatedDate", "ModifiedDate", "deleted", "is_deleted" };
				return fieldList.Contains(this.FieldName, StringComparer.CurrentCultureIgnoreCase);
			}
		}
		public Type Type
		{
			get
			{
				switch(this.DBType)
				{
					case DbType.AnsiString: return typeof(string);
					case DbType.AnsiStringFixedLength: return typeof(string);
					case DbType.Binary: return typeof(byte[]);
					case DbType.Boolean: return typeof(bool);
					case DbType.Byte: return typeof(byte);
					case DbType.Currency: return typeof(decimal);
					case DbType.Date: return typeof(DateTime);
					case DbType.DateTime: return typeof(DateTime);
					case DbType.DateTime2: return typeof(DateTime);
					case DbType.DateTimeOffset: return typeof(DateTimeOffset);
					case DbType.Decimal: return typeof(decimal);
					case DbType.Double: return typeof(double);
					case DbType.Guid: return typeof(Guid);
					case DbType.Int16: return typeof(short);
					case DbType.Int32: return typeof(int);
					case DbType.Int64: return typeof(long);
					case DbType.Object: return typeof(object);
					case DbType.SByte: return typeof(byte);
					case DbType.Single: return typeof(Single);
					case DbType.String: return typeof(string);
					case DbType.StringFixedLength: return typeof(string);
					case DbType.Time: return typeof(TimeSpan);
					case DbType.UInt16: return typeof(short);
					case DbType.UInt32: return typeof(int);
					case DbType.UInt64: return typeof(long);
					case DbType.VarNumeric: return typeof(decimal);
					case DbType.Xml: return typeof(string);
					default: return typeof(string);
				}

			}
		}
		#endregion


		#region ***** CONDITION METHODS *****

		public BaseCondition IsNull()
		{
			return new EqualCondition(this, null);
		}

		public BaseCondition IsNotNull()
		{
			return new NotEqualCondition(this, null);
		}

		public BaseCondition StartsWith(Object Value)
		{
			if (Value is IList)
			{
				List<BaseCondition> values = new List<BaseCondition>();
				foreach (object v in (IList)Value)
					values.Add(new LikeCondition(this, LikeConditionTypes.StartsWith, v));

				return new OrCondition(values);
			}
			else
				return new LikeCondition(this, LikeConditionTypes.StartsWith, Value);
		}

		public BaseCondition EndWith(Object Value)
		{
			if (Value is IList)
			{
				List<BaseCondition> values = new List<BaseCondition>();
				foreach (object v in (IList)Value)
					values.Add(new LikeCondition(this, LikeConditionTypes.EndsWith, v));

				return new OrCondition(values);
			}
			else
				return new LikeCondition(this, LikeConditionTypes.EndsWith, Value);
		}

		public BaseCondition Contains(Object Value)
		{
			if (Value is IList)
			{
				List<BaseCondition> values = new List<BaseCondition>();
				foreach (object v in (IList)Value)
					values.Add(new LikeCondition(this, LikeConditionTypes.Contains, v));

				return new OrCondition(values);
			}
			else
				return new LikeCondition(this, LikeConditionTypes.Contains, Value);
		}

		public BaseCondition Matches(Object Value)
		{
			if (Value is IList)
			{
				List<BaseCondition> values = new List<BaseCondition>();
				foreach (object v in (IList)Value)
					values.Add(new LikeCondition(this, LikeConditionTypes.Custom, v));

				return new OrCondition(values);
			}
			else
				return new LikeCondition(this, LikeConditionTypes.Custom, Value);
		}
		public BaseCondition IsAnyOf(IList Value)
		{
			List<object> values = new List<object>();
			foreach (object v in (IList)Value)
				values.Add(v);

			return new InCondition(this, values);
		}

		public BaseCondition IsNotAnyOf(IList Value)
		{
			List<object> values = new List<object>();
			foreach (object v in (IList)Value)
				values.Add(v);

			return new NotInCondition(this, values);
		}

		#endregion



		#region ***** OPERATOR OVERLOADS *****
		//Greater Than or Equal
		public static BaseCondition operator <=(object Value, BaseColumn<FieldType> Column) 
		{
			return new GreaterOrEqualThanCondition(Column, Value);
		}
		public static BaseCondition operator >=(BaseColumn<FieldType> Column, object Value) 
		{
			return new GreaterOrEqualThanCondition(Column, Value);
		}
		

		//Greater Than
		public static BaseCondition operator <(object Value, BaseColumn<FieldType> Column) 
		{
			return new GreaterThanCondition(Column, Value);
		}
		public static BaseCondition operator >(BaseColumn<FieldType> Column, object Value) 
		{
			return new GreaterThanCondition(Column, Value);
		}
		

		//Less Than or Equal
		public static BaseCondition operator <=(BaseColumn<FieldType> Column, object Value) 
		{
			return new LessOrEqualThanCondition(Column, Value);
		}
		public static BaseCondition operator >=(object Value, BaseColumn<FieldType> Column) 
		{
			return new LessOrEqualThanCondition(Column, Value);
		}
		

		//Less Than
		public static BaseCondition operator <(BaseColumn<FieldType> Column, object Value) 
		{
			return new LessThanCondition(Column, Value);
		}
		public static BaseCondition operator >(object Value, BaseColumn<FieldType> Column) 
		{
			return new LessThanCondition(Column, Value);
		}
		

		//Equal
		public static BaseCondition operator ==(BaseColumn<FieldType> Column, object Value) 
		{
			if (Value is IList)
			{
				List<object> values = new List<object>();
				foreach (object v in (IList)Value)
					values.Add(v);

				return new InCondition(Column, values);
			}
			else
				return new EqualCondition(Column, Value);
		}
		public static BaseCondition operator ==(object Value, BaseColumn<FieldType> Column) 
		{
			if (Value is IList)
			{
				List<object> values = new List<object>();
				foreach (object v in (IList)Value)
					values.Add(v);

				return new InCondition(Column, values);
			}
			else
				return new EqualCondition(Column, Value);
		}

        //Like and Like IN
        public static BaseCondition operator %(BaseColumn<FieldType> Column, object Value) 
		{
            if (Value is IList)
			{
                List<BaseCondition> values = new List<BaseCondition>();
                foreach (object v in (IList)Value)
                    values.Add(new LikeCondition(Column, LikeConditionTypes.Custom, v));
                                
                return new OrCondition(values);
            } else
                return new LikeCondition(Column, LikeConditionTypes.Custom, Value);
        }

        public static BaseCondition operator %(object Value, BaseColumn<FieldType> Column) 
		{
            if (Value is IList) 
			{
                List<BaseCondition> values = new List<BaseCondition>();
                foreach (object v in (IList)Value)
                    values.Add(new LikeCondition(Column, LikeConditionTypes.Custom, v));
                                
                return new OrCondition(values);
            } else
                return new LikeCondition(Column, LikeConditionTypes.Custom, Value);
        }

		//Not Equal
		public static BaseCondition operator !=(BaseColumn<FieldType> Column, object Value) 
		{
			return new NotEqualCondition(Column, Value);
		}
		public static BaseCondition operator !=(object Value, BaseColumn<FieldType> Column) 
		{
			return new NotEqualCondition(Column, Value);
		}

		//False
		public static BaseCondition operator !(BaseColumn<FieldType> Column) 
		{
			return new FalseCondition(Column);
		}
		
		#endregion


	}
}
