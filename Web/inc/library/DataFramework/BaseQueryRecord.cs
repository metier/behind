﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.DataFramework
{
	public class BaseQueryRecord<ContextType> : BaseRecord<ContextType>
		where ContextType : BaseContext, new()
	{
		

		#region ***** CONSTRUCTORS *****
		public BaseQueryRecord(ContextType Context) : base(Context) { }
		#endregion


		#region ***** PROPERTIES *****

		#endregion
		

		#region ***** STATIC LOAD METHODS *****
		
		#endregion
	}
}
