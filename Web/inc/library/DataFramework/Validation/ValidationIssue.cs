﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jlib.DataFramework.Validation
{
	public class ValidationIssue
	{
		//Properties
		public string FieldName { get; set; }
		public string ValidationKey { get; set; }
		public string ValidationMessage { get; set; }

        public ValidationIssue(string fieldName, string validationKey, string validationMessage)
        {
            this.FieldName = fieldName;
            this.ValidationKey = validationKey;
            this.ValidationMessage = validationMessage;
        }
        public ValidationIssue(){}
	}
}
