﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using jlib.functions;

namespace jlib.DataFramework.Validation
{
	public class ValidationRule
	{
		//Properties
		public IField ValidationField { get; set; }
		public Func<IField, ValidationIssue> ValidateMethod { get; set; }


		//Shared Rules
		/// <summary>
		/// Validate the field is not null
		/// </summary>
		/// <param name="Field"></param>
		/// <returns></returns>
		public static ValidationIssue ValidateIsNotNull(IField Field)
		{
			//Is Raw Value Null
			if (Field.RawValue == null)
			{
				return new ValidationIssue()
					{
						FieldName = Field.PropertyName,
						ValidationKey = "FieldValidation.IsNull",
						ValidationMessage = String.Format("{0} does not allow Null values", Field.PropertyName)
					};
			}
			
			//else
			return null;
		}

		/// <summary>
		/// Validate Field does not have an empty string value
		/// </summary>
		/// <param name="Field"></param>
		/// <returns></returns>
		public static ValidationIssue ValidateIsNotBlank(IField Field)
		{
			if (Field.RawValue == null || String.IsNullOrWhiteSpace(Field.RawValue.Str()))
			{
				return new ValidationIssue()
				{
					FieldName = Field.PropertyName,
					ValidationKey = "FieldValidation.IsEmpty",
					ValidationMessage = String.Format("{0} does not allow empty values", Field.PropertyName)
				};
			}

			if (Field.DBType == System.Data.DbType.Guid && Field.RawValue.Guid().Equals(Guid.Empty))
			{
				return new ValidationIssue()
				{
					FieldName = Field.PropertyName,
					ValidationKey = "FieldValidation.IsEmpty",
					ValidationMessage = String.Format("{0} does not allow empty values", Field.PropertyName)
				};
			}

			//else
			return null;
		}

		/// <summary>
		/// Validate ID has a value (> 0)
		/// </summary>
		/// <param name="Field"></param>
		/// <returns></returns>
		public static ValidationIssue ValidateIdHasValue(IField Field)
		{
			if (Field.RawValue == null || Field.RawValue.Int() <= 0)
			{
				return new ValidationIssue()
				{
					FieldName = Field.PropertyName,
					ValidationKey = "FieldValidation.IdNotSet",
					ValidationMessage = String.Format("{0} requires an ID value to be set", Field.PropertyName)
				};
			}

			//else
			return null;
		}

		/// <summary>
		/// Validate string value is of type email
		/// </summary>
		/// <param name="Field"></param>
		/// <returns></returns>
		public static ValidationIssue ValidateIsEmail(IField Field)
		{
			if (!jlib.functions.convert.isEmail(Field.RawValue, false))
			{
				return new ValidationIssue()
				{
					FieldName = Field.PropertyName,
					ValidationKey = "FieldValidation.NotEmail",
					ValidationMessage = String.Format("{0} must be of type email", Field.PropertyName)
				};
			}

			//else
			return null;
		}

		/// <summary>
		/// Validate string value is of type email
		/// </summary>
		/// <param name="Field"></param>
		/// <returns></returns>
		public static ValidationIssue ValidateIsEmailOrBlank(IField Field)
		{
			if (!String.IsNullOrWhiteSpace(Field.RawValue.ToString()) && !jlib.functions.convert.isEmail(Field.RawValue, false))
			{
				return new ValidationIssue()
				{
					FieldName = Field.PropertyName,
					ValidationKey = "FieldValidation.NotEmailOrBlank",
					ValidationMessage = String.Format("{0} must be of type email or blank", Field.PropertyName)
				};
			}

			//else
			return null;
		}

		/// <summary>
		/// Validate string value is of type date
		/// </summary>
		/// <param name="Field"></param>
		/// <returns></returns>
		public static ValidationIssue ValidateIsDateOrBlank(IField Field)
		{
			if (!String.IsNullOrWhiteSpace(Field.RawValue.Str()) && !jlib.functions.convert.isDate(Field.RawValue))
			{
				return new ValidationIssue()
				{
					FieldName = Field.PropertyName,
					ValidationKey = "FieldValidation.NotDateOrBlank",
					ValidationMessage = String.Format("{0} must be of type date", Field.PropertyName)
				};
			}
			 
			//else
			return null;
		}

		/// <summary>
		/// Validate is a number
		/// </summary>
		/// <param name="Field"></param>
		/// <returns></returns>
		public static ValidationIssue ValidateIsNumber(IField Field)
		{
			if (Field.RawValue != null && !jlib.functions.convert.isNumeric(Field.RawValue))
			{
				return new ValidationIssue()
				{
					FieldName = Field.PropertyName,
					ValidationKey = "FieldValidation.NotNumber",
					ValidationMessage = String.Format("{0} must be a number", Field.PropertyName)
				};
			}

			//else
			return null;
		}
	}
}
