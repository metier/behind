﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jlib.DataFramework.Validation
{
	public class ValidationSummary
	{
		//Constructors
		public ValidationSummary()
		{
			this.IsSuccessful = true;
			this.Errors = new List<ValidationIssue>();
		}

		//Properties
		public bool IsSuccessful { get; protected set; }
		public string Message { get; set; }
		public List<ValidationIssue> Errors { get; protected set; }

		//Methods
		public void AddIfError(ValidationIssue Issue)
		{
			if (Issue != null)
			{
				this.Errors.Add(Issue);
				this.IsSuccessful = false;
			}
		}
		
	}
}
