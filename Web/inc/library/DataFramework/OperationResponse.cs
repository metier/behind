﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.DataFramework
{
	public class OperationResponse
	{
		#region ***** DECLARATIONS *****
		private DateTime _StartTimer;
		#endregion

		#region ***** CONSTRUCTORS *****
		public OperationResponse() : this(true, 0, TimeSpan.MinValue, new List<string>())
		{
		}

		public OperationResponse(bool IsSuccessful, int ItemsAffected, TimeSpan ExecutionTime, List<string> Errors)
		{
			this.IsSuccessful = IsSuccessful;
			this.ItemsAffected = ItemsAffected;
			this.ExecutionTime = ExecutionTime;
			this.Errors = Errors;
		}
		#endregion

		#region ***** PROPERTIES *****
		public bool IsSuccessful { get; set; }
		public int ItemsAffected { get; set; }
		public TimeSpan ExecutionTime { get; set; }
		public List<string> Errors { get; set; }
		#endregion

		#region ***** METHODS *****
		public void StartTimer()
		{
			this._StartTimer = DateTime.Now;
		}
		public void StopTimer()
		{
			this.ExecutionTime = this._StartTimer.Subtract(DateTime.Now);
		}
		public void LogException(Exception exception)
		{
			this.IsSuccessful = false;
			this.Errors.Add(exception.ToString());
		}
		#endregion

	}
}
