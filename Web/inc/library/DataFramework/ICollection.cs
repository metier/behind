﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace jlib.DataFramework
{
	public interface ICollection : IListSource
	{
		//Properties
		Paging.PagingDetails PagingDetails { get; set; }
		int Count { get; }
		ColumnCollection Columns { get; }
        IRecord this[int index] { get; set; }
		string DbEntityName { get; set; }

		//Methods
        //void Add(IRecord record);
		void ImportDataTable(System.Data.DataTable dtRows);
		string ToJson(functions.json.JsonWriter Writer = null, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null);
        string ToJsonArray(functions.json.JsonWriter Writer = null, params IColumn[] oColsToInclude);
		string ToJsonArray(functions.json.JsonWriter Writer = null, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null);
        int DtsSave(jlib.db.SqlHelper SqlHelper, Action<object, System.Data.SqlClient.SqlRowsCopiedEventArgs> fUpdateProgress, System.Data.DataRowAction statesToProcess);
		PropertyDescriptorCollection GetPropertyDescriptors();
	}
}
