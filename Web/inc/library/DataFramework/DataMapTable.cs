﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.DataFramework
{
	public class DataMapTable : List<DataMap>
	{
	}

	public class DataMap
	{
		public DataMap(int SourceIndex, int DestinationIndex)
		{
			this.SourceIndex = SourceIndex;
			this.DestinationIndex = DestinationIndex;
		}

		public int SourceIndex { get; set; }
		public int DestinationIndex { get; set; }

		public bool IsInternalObjectField { get; set; }
		public bool IsNewField { get; set; }
		public string NewFieldName { get; set; }
	}
}
