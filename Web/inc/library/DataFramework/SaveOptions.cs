﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jlib.DataFramework
{
	public class SaveOptions
	{
		//Constructor
		public SaveOptions()
		{
			this.ReloadValuesFromDb = true;
			this.DisableTriggers = false;
		}

		//Properties
		public bool ReloadValuesFromDb { get; set; }
		public bool DisableTriggers { get; set; }

		public string Notes { get; set; }

	}
}
