﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jlib.db;
using jlib.functions;

namespace jlib.DataFramework
{
	public abstract class BaseContext
	{
        private jlib.db.SqlHelper.ConnectionTypes m_oConnectionType = SqlHelper.ConnectionTypes.Unknown;
        public BaseContext(){}
        public BaseContext(jlib.db.SqlHelper.ConnectionTypes oConnectionType) 
		{
            m_oConnectionType = oConnectionType;
        }
      
		//Properties
		public virtual jlib.db.SqlHelper.ConnectionTypes ConnectionType 
		{
            get 
			{
                return m_oConnectionType;
            }
        }
        public virtual DateTime VersionDate 
		{
            get 
			{
                //This should read from a user-level setting (not application-wide)
                return System.Web.HttpContext.Current==null ? DateTime.Now : System.Web.HttpContext.Current.Items["version.date"].Dte();
            }
        }
        public virtual bool TupleVersioning 
		{
            get 
			{
                return System.Web.HttpContext.Current==null || System.Web.HttpContext.Current.Items["versioning"].Bln();
            }
        }

		//Static properties


		//Methods
		public virtual SqlHelper GetSqlHelper()
		{
			//Return Sql Helper2
            SqlHelper oSqlHelper = SqlHelper.SqlHelperCreateConnectionStrings("sql.dsn");
            oSqlHelper.ConnectionType = this.ConnectionType;
            return oSqlHelper;
		}

		public virtual SqlHelper GetCurrentTransactionOrCreate()
		{
			return this.GetCurrentTransaction() ?? this.GetSqlHelper();
		}
		public abstract void SetCurrentTransaction(SqlHelper Sql);
		public abstract SqlHelper GetCurrentTransaction();

		public virtual SqlHelper GetSqlTransaction()
		{
			var sql = this.GetSqlHelper();
			sql.Connect(CreateTransaction: true);

			//Set Thread Static Variable
			this.SetCurrentTransaction(sql);

			return sql;
		}
	}
}
