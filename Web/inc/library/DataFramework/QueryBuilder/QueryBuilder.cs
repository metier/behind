﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jlib.functions;

namespace jlib.DataFramework.QueryBuilder
{
	public class QueryBuilder
	{
		#region ***** ENUMS *****
		public enum QueryBuilderOperationTypes
		{
			Select,
			Delete
		}
		#endregion

		#region ***** CONSTRUCTORS *****
		public QueryBuilder(IQueryPackage InnerQuery, IQueryPackage OutputQueryPackage, BaseContext oContext)
		{
			this.InnerQuery = InnerQuery;
			this.PagingOptions = InnerQuery.PagingOptions;
			this.CacheOptions = InnerQuery.CacheOptions;
			this.OutputQueryPackage = OutputQueryPackage;
			this.Conditions = new List<BaseCondition>();
			this.OrderBy = new List<OrderBy>();
            this.Context = oContext;
		}
		#endregion

		#region ***** PROPERTIES *****
		public IQueryPackage OutputQueryPackage { get; set; }
		public IQueryPackage InnerQuery { get; set; }
		public List<BaseCondition> Conditions { get; set; }
		private List<OrderBy> OrderBy { get; set; }
		public Paging.PagingOptions PagingOptions { get; set; }
		public Cache.CacheOptions CacheOptions { get; set; }
        public BaseContext Context { get; private set; }
		#endregion

		#region ***** METHODS *****
		public void ClearOrderBy()
		{
			this.OrderBy.Clear();
		}
		public void AddOrderBy(OrderBy OrderBy)
		{
			//If currently exists, then remove
            OrderBy currentOrderBy = this.OrderBy.FirstOrDefault(x => x.FieldName.Equals(OrderBy.FieldName, StringComparison.CurrentCultureIgnoreCase));
            if (currentOrderBy != null) this.OrderBy.Remove(currentOrderBy);

            //Add New one
			this.OrderBy.Add(OrderBy);

		}
		public IQueryPackage ToQueryPackage(QueryBuilderOperationTypes OperationType = QueryBuilderOperationTypes.Select)
		{            
			//Create QueryWriter
			QueryWriter writer;
            if (this.Context.ConnectionType == jlib.db.SqlHelper.ConnectionTypes.Oracle) writer = new OracleQueryWriter(this.InnerQuery);
            else writer = new QueryWriter(this.InnerQuery, OperationType);


			//Late Bind Conditions
			this.Conditions.Where(x => x is ILateBindingCondition).Cast<ILateBindingCondition>().ToList().ForEach(x => x.Bind(this.InnerQuery.Columns));

			//Write Conditions
            this.Conditions.ForEach(x => writer.WriteCondition(x.GenerateSql(writer, this.Context.ConnectionType)));

			//Write Paging
			if (this.PagingOptions != null)
			{
				//Get Indexes
				int startIndex = this.PagingOptions.PageIndex * this.PagingOptions.PageSize + 1;
				int endIndex = this.PagingOptions.PageIndex * this.PagingOptions.PageSize + this.PagingOptions.PageSize;
				writer.WritePaging(startIndex, endIndex);

				//Add Paging Order By
				if (!String.IsNullOrWhiteSpace(this.PagingOptions.SortField))
				{                    
					IColumn column = this.InnerQuery.Columns.SafeGetValue(this.PagingOptions.SortField);

					//Add Order BY
                    OrderBy orderBy = null;
					if (column == null) orderBy = new OrderBy(this.PagingOptions.SortField, this.PagingOptions.SortDirection);
					else orderBy = new OrderBy(column, this.PagingOptions.SortDirection);

					this.AddOrderBy(orderBy);
				}
			}

			//Write OrderBy
			this.OrderBy.ForEach(x => writer.WriteOrderBy(x.GenerateSql(writer)));

			//Create Query Package
			this.OutputQueryPackage.InternalQueryBuilder = this;
			this.OutputQueryPackage.SetInternalStatement(writer.ToString());
			this.OutputQueryPackage.IsStoredProcedure = false;

			//Set Parameters
			this.OutputQueryPackage.Parameters = writer.Parameters;

			//Clear Paging
			this.OutputQueryPackage.PagingOptions = this.PagingOptions;

			//Cache Options
			this.OutputQueryPackage.CacheOptions = this.CacheOptions;

			//return Query Package
			return this.OutputQueryPackage;
		}
		#endregion
	}
}
