﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Common;
using System.Reflection;
using System.Dynamic;
using System.Text.RegularExpressions;

namespace jlib.DataFramework.QueryBuilder
{
	public class QueryWriter
	{
		#region ***** CONSTANTS *****
		public const string INNER_QUERY_WITH_BLOCK = "{InnerQueryWithBlock}";
		public const string WITH_TAG = "{WithTag}";
		public const string INNER_QUERY_RESULTS_KEY = "{InnerQueryResultsName}";
		public const string INNER_QUERY_RESULTS_NAME = "jLibInnerResults";
		public const string OUTER_QUERY_RESULTS_KEY = "{OuterQueryResultsName}";
		public const string OUTER_QUERY_RESULTS_NAME = "jLibOuterResults";
		public const string INNER_QUERY_STATEMENT_KEY = "{InnerQueryStatement}";
		public const string ORDERBY_CLAUSE_KEY = "{OrderByClause}";
		public const string ROW_NUMBER_COLUMN_NAME_KEY = "{RowNumberColumnName}";
		public const string ROW_NUMBER_COLUMN_NAME = "[Paging.Index]";
		public const string RECORD_COUNT_COLUMN_NAME_KEY = "{RecordCountColumnName}";
		public const string RECORD_COUNT_COLUMN_NAME = "[Paging.TotalRows]";
		public const string PRE_PAGING_CONDITIONS_CLAUSE_KEY = "{PreConditionsClause}";
		public const string POST_PAGING_CONDITIONS_CLAUSE_KEY = "{PostConditionsClause}";
		public const string PAGING_START_INDEX_PARAMETER_NAME = "jLibPagingStartIndex";
		public const string PAGING_END_INDEX_PARAMETER_NAME = "jLibPagingEndIndex";
		#endregion

		#region ***** CONSTRUCTORS *****
        public QueryWriter(IQueryPackage InnerQuery, QueryBuilder.QueryBuilderOperationTypes OperationType = QueryBuilder.QueryBuilderOperationTypes.Select)
		{
			//Resource Helper
			Resources.ResourceHelper resources = new Resources.ResourceHelper();

			this.TemplateSql = (OperationType == QueryBuilder.QueryBuilderOperationTypes.Delete) ? resources.QueryBuilderDeleteTemplate : resources.QueryBuilderSelectTemplate;
			this.PreConditionsClause = new List<string>();
			this.PostConditionsClause = new List<string>();
			this.OrderBy = new List<string>();
            this.Parameters = new jlib.helpers.structures.OrderedDictionary<string, DbParameter>();

			//Set Inner Query
			this.WriteInnerQuery(InnerQuery);
		}
		#endregion

		#region ***** PROPERTIES *****
		public string InnerStatement { get; protected set; }
        public string TemplateSql { get; protected set; }
        public List<string> PreConditionsClause { get; protected set; }
        public List<string> PostConditionsClause { get; protected set; }
        public List<string> OrderBy { get; protected set; }
        public jlib.helpers.structures.OrderedDictionary<string, DbParameter> Parameters { get; protected set; }        

		public bool IgnoreEmptyOrderBy { get; set; }
		#endregion


		#region ***** HELPER METHODS *****
		public Match DoesInnerStatementContainWith()
		{
			return Regex.Match(this.InnerStatement ?? "", @"(?<WithExpression>WITH\s*\w*(\(.*?\))*\s*AS\s*\(.*?\))\s*(SELECT|UPDATE|INSERT|DELETE)+", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline | RegexOptions.Singleline);
		}
		public string[] SplitInnerStatementWithExpressions(Match match)
		{
			int pos = match.Groups["WithExpression"].Index + match.Groups["WithExpression"].Length;

			//Split based on pos
			return new string[] { this.InnerStatement.Substring(0, pos), this.InnerStatement.Substring(pos+1) };
		}
		#endregion

		#region ***** PARAMETER METHODS *****
		public virtual DbParameter CreateSqlParameter(string FieldName, object Value)
		{
			//Get Parameter Name
			string parameterName = "@" + FieldName;

			//Check to make sure FieldName is unique
			if (this.ParameterExists(parameterName))
			{
				//Find Unique Parameter Name
				int parameterIndex = 2;
				string newParameterName = "";
				while (this.ParameterExists(newParameterName = String.Format("{0}{1}", parameterName, parameterIndex))) 
				{
					parameterIndex++;
				}

				//Set Parameter Name
				parameterName = newParameterName;
			}

			//Create new SqlParameters
			SqlParameter sqlParameter = new SqlParameter(parameterName, Value);

			//Add to collection
			this.AddParameter(parameterName, sqlParameter);

			//Return Parameter
			return sqlParameter;
		}

		public void AddParameter(string ParameterName, SqlParameter Parameter)
		{
			this.Parameters.Add(ParameterName, Parameter);
		}

		public bool ParameterExists(string ParameterName)
		{
			return this.Parameters.ContainsKey(ParameterName);
		}

		#endregion

		#region ***** WRITE METHODS *****
		public virtual void WriteInnerQuery(IQueryPackage QueryPackage)
		{
			//If Sproc, throw error
			if(QueryPackage.IsStoredProcedure)
				throw new Exception("QueryBuilder does not work with stored procedures");

			//Set Internal Sql
			this.InnerStatement = QueryPackage.Statement;

			//Set Parameters
			foreach (string fieldName in QueryPackage.Parameters.Dictionary.Keys)
			{
				//Get Parameter Name
				string parameterName = "@" + fieldName;

				//Get Value
				object value = QueryPackage.Parameters.Dictionary[fieldName];

				//Create SqlParameter
				SqlParameter sqlParameter = new SqlParameter(parameterName, value);
				this.AddParameter(parameterName, sqlParameter);
			}
		}

		public void WriteCondition(string Sql)
		{
			this.PreConditionsClause.Add(Sql);
		}

		public void WriteOrderBy(string Sql)
		{
			this.OrderBy.Add(Sql);
		}

        public virtual void WritePaging(int StartIndex, int EndIndex)
		{
			//Get Parameters
			DbParameter startParameter = this.CreateSqlParameter(QueryWriter.PAGING_START_INDEX_PARAMETER_NAME, StartIndex);
			DbParameter endParameter = this.CreateSqlParameter(QueryWriter.PAGING_END_INDEX_PARAMETER_NAME, EndIndex);

			//Get Names
			string startParameterName = startParameter.ParameterName;
			string endParameterName = endParameter.ParameterName;

			//Add Where Clause
			this.PostConditionsClause.Add(String.Format("{0} BETWEEN {1} AND {2}", QueryWriter.ROW_NUMBER_COLUMN_NAME, startParameterName, endParameterName));
		}

		#endregion

		#region ***** OUTPUT METHODS *****
		public override string ToString()
		{
			//Get Template
			string sql = this.TemplateSql;

			//Write results sets
			sql = sql.Replace(QueryWriter.INNER_QUERY_RESULTS_KEY, QueryWriter.INNER_QUERY_RESULTS_NAME);
			sql = sql.Replace(QueryWriter.OUTER_QUERY_RESULTS_KEY, QueryWriter.OUTER_QUERY_RESULTS_NAME);
			sql = sql.Replace(QueryWriter.ROW_NUMBER_COLUMN_NAME_KEY, QueryWriter.ROW_NUMBER_COLUMN_NAME);
			sql = sql.Replace(QueryWriter.RECORD_COUNT_COLUMN_NAME_KEY, QueryWriter.RECORD_COUNT_COLUMN_NAME);

			//Write Inner query
			Match match = null;
			if ((match = this.DoesInnerStatementContainWith()).Success)
			{
				//Get With Expressions
				string[] expressions = this.SplitInnerStatementWithExpressions(match);

				//Append to sql output
				sql = sql.Replace(QueryWriter.WITH_TAG, "");
				sql = sql.Replace(QueryWriter.INNER_QUERY_WITH_BLOCK, expressions[0] + ",");
				sql = sql.Replace(QueryWriter.INNER_QUERY_STATEMENT_KEY, expressions[1]);
			}
			else
			{
				sql = sql.Replace(QueryWriter.WITH_TAG, "WITH");
				sql = sql.Replace(QueryWriter.INNER_QUERY_WITH_BLOCK, "");
				sql = sql.Replace(QueryWriter.INNER_QUERY_STATEMENT_KEY, this.InnerStatement);
			}

			//Write Pre Condition (ie. Filters)
			string preConditionsClause = "";
			if (this.PreConditionsClause.Count > 0)
				preConditionsClause = String.Concat("WHERE \r\n\t", String.Join("\n AND ", this.PreConditionsClause));
			sql = sql.Replace(QueryWriter.PRE_PAGING_CONDITIONS_CLAUSE_KEY, preConditionsClause);

			//Write Post Condition (ie. Paging)
			string postConditionsClause = "";
			if (this.PostConditionsClause.Count > 0)
				postConditionsClause = String.Concat("WHERE \r\n\t", String.Join("\n AND ", this.PostConditionsClause));
			sql = sql.Replace(QueryWriter.POST_PAGING_CONDITIONS_CLAUSE_KEY, postConditionsClause);

			//If Order By Not specified, use first column
			if (this.OrderBy.Count == 0) this.OrderBy.Add("(SELECT 1)");

			//Write Order By
			string orderByClause = String.Concat("ORDER BY ", String.Join(", ", this.OrderBy));
			sql = sql.Replace(QueryWriter.ORDERBY_CLAUSE_KEY, orderByClause);

			//Return string
			return sql;
		}
		#endregion
	}
}
