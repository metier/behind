﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace jlib.DataFramework.QueryBuilder
{
	public class ScalarValue : IColumn
	{
		#region ***** CONSTRUCTORS *****
		public ScalarValue(object Value)
		{
			this.Value = Value;
		}
		#endregion

		#region ***** PROPERTIES *****
		public object Value { get; set; }
		
		public DbType DBType { get { return this.GetDBType(this.Value.GetType()); } }
		public string FieldName { get { return String.Format("'{0}'", this.Value); } }
		public bool IsPrimaryKey { get { return false; } }
		public bool IsVirtualColumn { get { return true; } }
		public bool IsNullable { get { return true; } }
		public bool HasDbDefault { get { return false; } }
		public bool IsIdentity { get { return false; } }
		public string ParameterName { get { throw new NotImplementedException("Scalar Values don't have Parameter Values"); } }
		public string PkParameterName { get { throw new NotImplementedException("Scalar Values don't have Pk Parameter Values"); } }
		public string PropertyName { get { throw new NotImplementedException("Scalar Values don't have Property Names"); } }
		public bool IsStandardDefaultColumn 
		{ 
			get
			{
				List<string> fieldList = new List<string>() { "ID", "created_date", "modified_date", "posted_date", "CreatedDate", "ModifiedDate" };
				return fieldList.Contains(this.FieldName, StringComparer.CurrentCultureIgnoreCase);
			}
		}
		public Type Type
		{
			get
			{
				switch(this.DBType)
				{
					case DbType.AnsiString: return typeof(string);
					case DbType.AnsiStringFixedLength: return typeof(string);
					case DbType.Binary: return typeof(byte[]);
					case DbType.Boolean: return typeof(bool);
					case DbType.Byte: return typeof(byte);
					case DbType.Currency: return typeof(decimal);
					case DbType.Date: return typeof(DateTime);
					case DbType.DateTime: return typeof(DateTime);
					case DbType.DateTime2: return typeof(DateTime);
					case DbType.DateTimeOffset: return typeof(DateTimeOffset);
					case DbType.Decimal: return typeof(decimal);
					case DbType.Double: return typeof(double);
					case DbType.Guid: return typeof(Guid);
					case DbType.Int16: return typeof(short);
					case DbType.Int32: return typeof(int);
					case DbType.Int64: return typeof(long);
					case DbType.Object: return typeof(object);
					case DbType.SByte: return typeof(byte);
					case DbType.Single: return typeof(Single);
					case DbType.String: return typeof(string);
					case DbType.StringFixedLength: return typeof(string);
					case DbType.Time: return typeof(TimeSpan);
					case DbType.UInt16: return typeof(short);
					case DbType.UInt32: return typeof(int);
					case DbType.UInt64: return typeof(long);
					case DbType.VarNumeric: return typeof(decimal);
					case DbType.Xml: return typeof(string);
					default: return typeof(string);
				}

			}
		}
		#endregion

		#region ***** METHODS *****
		private DbType GetDBType(System.Type theType)
		{
			SqlParameter param;
			System.ComponentModel.TypeConverter tc;
			param = new SqlParameter();
			tc = System.ComponentModel.TypeDescriptor.GetConverter(param.DbType);
			if (tc.CanConvertFrom(theType))
			{
				param.DbType = (DbType)tc.ConvertFrom(theType.Name);
			}
			else
			{
				// try to forcefully convert
				try
				{
					param.DbType = (DbType)tc.ConvertFrom(theType.Name);
				}
				catch(Exception e)
				{
					// ignore the exception
				}
			}
			return param.DbType;
		}
		#endregion


		#region ***** OPERATOR OVERLOADS *****
		//Greater Than or Equal
		public static BaseCondition operator <=(object Value, ScalarValue Column) 
		{
			return new GreaterOrEqualThanCondition(Column, Value);
		}
		public static BaseCondition operator >=(ScalarValue Column, object Value) 
		{
			return new GreaterOrEqualThanCondition(Column, Value);
		}
		

		//Greater Than
		public static BaseCondition operator <(object Value, ScalarValue Column) 
		{
			return new GreaterThanCondition(Column, Value);
		}
		public static BaseCondition operator >(ScalarValue Column, object Value) 
		{
			return new GreaterThanCondition(Column, Value);
		}
		

		//Less Than or Equal
		public static BaseCondition operator <=(ScalarValue Column, object Value) 
		{
			return new LessOrEqualThanCondition(Column, Value);
		}
		public static BaseCondition operator >=(object Value, ScalarValue Column) 
		{
			return new LessOrEqualThanCondition(Column, Value);
		}
		

		//Less Than
		public static BaseCondition operator <(ScalarValue Column, object Value) 
		{
			return new LessThanCondition(Column, Value);
		}
		public static BaseCondition operator >(object Value, ScalarValue Column) 
		{
			return new LessThanCondition(Column, Value);
		}
		

		//Equal
		public static BaseCondition operator ==(ScalarValue Column, object Value) 
		{
			if (Value is IList)
			{
				List<object> values = new List<object>();
				foreach (object v in (IList)Value)
					values.Add(v);

				return new InCondition(Column, values);
			}
			else
				return new EqualCondition(Column, Value);
		}
		public static BaseCondition operator ==(object Value, ScalarValue Column) 
		{
			if (Value is IList)
			{
				List<object> values = new List<object>();
				foreach (object v in (IList)Value)
					values.Add(v);

				return new InCondition(Column, values);
			}
			else
				return new EqualCondition(Column, Value);
		}

		//Not Equal
		public static BaseCondition operator !=(ScalarValue Column, object Value) 
		{
			return new NotEqualCondition(Column, Value);
		}
		public static BaseCondition operator !=(object Value, ScalarValue Column) 
		{
			return new NotEqualCondition(Column, Value);
		}

		//False
		public static BaseCondition operator !(ScalarValue Column) 
		{
			return new FalseCondition(Column);
		}
		
		#endregion
		
	}
}
