﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.DataFramework.QueryBuilder
{
	public abstract class BaseCondition
	{
		#region ***** CONSTRUCTORS *****
		public BaseCondition(IColumn Column)
		{
			this.Column = Column;
		}
		#endregion

		#region ***** PROPERTIES *****
		public IColumn Column { get; set; }
		#endregion

		#region ***** METHODS *****
		public abstract string GenerateSql(QueryWriter Writer, jlib.db.SqlHelper.ConnectionTypes ConnectionType);

		public virtual string GetSqlFieldName()
		{
			return this.Column.FieldName;
		}

		public virtual string GetDefaultNullValue()
		{
			switch(Column.DBType)
			{
				case System.Data.DbType.Date:
				case System.Data.DbType.DateTime:
				case System.Data.DbType.DateTime2:
					return "";
					break;

				case System.Data.DbType.Boolean:
					return "0";
					break;

				case System.Data.DbType.Currency:
				case System.Data.DbType.Decimal:
				case System.Data.DbType.Double:
				case System.Data.DbType.Int16:
				case System.Data.DbType.Int32:
				case System.Data.DbType.Int64:
				case System.Data.DbType.Single:
					return "-3";
					break;

				case System.Data.DbType.Guid:
					return "00000000-0000-0000-0000-000000000000";
					break;

				default:
					return "";
			}
		}
		#endregion

		#region ***** OPERATOR OVERLOADS *****
		public static AndCondition operator &(BaseCondition Condition1, BaseCondition Condition2) 
		{
			return new AndCondition(new List<BaseCondition>() {Condition1, Condition2});
		}

		public static OrCondition operator |(BaseCondition Condition1, BaseCondition Condition2) 
		{
			return new OrCondition(new List<BaseCondition>() {Condition1, Condition2});
		}
		public static NotCondition operator !(BaseCondition Condition1)
		{
			return new NotCondition(Condition1);
		}
		
		#endregion
	}
}
