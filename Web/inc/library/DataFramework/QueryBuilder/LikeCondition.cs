﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace jlib.DataFramework.QueryBuilder
{
	public enum LikeConditionTypes
	{
		StartsWith,
		EndsWith,
		Contains,
		Custom
	}

	public class LikeCondition : BaseCondition
	{
		//Constructors
		public LikeCondition(IColumn Column, LikeConditionTypes ConditionType, object Value) : base(Column)
		{
			this.ConditionType = ConditionType;
			this.Value = Value;
		}
		public LikeCondition(IColumn Column) : base(Column)
		{
		
		}

		//Properties
		public object Value { get; set; }
		public LikeConditionTypes ConditionType { get; set; }

		//Methods
        public override string GenerateSql(QueryWriter Writer, jlib.db.SqlHelper.ConnectionTypes ConnectionType)
		{
			//Get Parameters
			DbParameter sqlParameter;
            if (ConnectionType == jlib.db.SqlHelper.ConnectionTypes.Oracle && (this.Value as string)!=null)
                sqlParameter = Writer.CreateSqlParameter(this.GetSqlFieldName(), this.Value.ToString().ToLower());
            else
                sqlParameter = Writer.CreateSqlParameter(this.GetSqlFieldName(), this.Value);

			string parameterName = sqlParameter.ParameterName;

			//Get Wildcards
			string leadingWildcard = (this.ConditionType == LikeConditionTypes.EndsWith || this.ConditionType == LikeConditionTypes.Contains) ? "%" : "";
			string trialingWildcard = (this.ConditionType == LikeConditionTypes.StartsWith || this.ConditionType == LikeConditionTypes.Contains) ? "%" : "";

			//Create Sql
			string sql;
            if( ConnectionType == jlib.db.SqlHelper.ConnectionTypes.Oracle)
                sql = String.Format("nvl(lower({0}),'{1}') LIKE '{2}' || {3} || '{4}'", this.GetSqlFieldName(), this.GetDefaultNullValue(), leadingWildcard, parameterName, trialingWildcard);
            else
                sql = String.Format("ISNULL([{0}],'{1}') LIKE '{2}'+{3}+'{4}'", this.GetSqlFieldName(), this.GetDefaultNullValue(), leadingWildcard, parameterName, trialingWildcard);

			//Return Sql
			return sql;

		}
	}
}
