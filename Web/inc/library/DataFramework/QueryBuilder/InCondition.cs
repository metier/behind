﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using jlib.functions;
using jlib.helpers;

namespace jlib.DataFramework.QueryBuilder
{

	public class InCondition : BaseCondition
	{
		//Constructors
		public InCondition(IColumn Column, List<object> Values) : base(Column)
		{
			this.Values = Values ?? new List<object>();
		}
		public InCondition(IColumn Column) : base(Column)
		{
			this.Values = new List<object>();
		}

		//Properties
		public List<object> Values { get; set; }

		//Methods
        public override string GenerateSql(QueryWriter Writer, jlib.db.SqlHelper.ConnectionTypes ConnectionType)
		{
			if (this.Values.Count == 0) this.Values.Add("0");

			//Create List of Parameter Names
			List<string> parameterNames = new List<string>();

			//Loop through each value
			foreach (object value in this.Values)
			{
				//Get Parameters
				DbParameter sqlParameter = Writer.CreateSqlParameter(this.GetSqlFieldName(), value);
				string parameterName = sqlParameter.ParameterName;

				//Add Parameter Name to collection
				parameterNames.Add(parameterName);
			}
			
			//Create Sql
			string parameterList = parameterNames.Join(",");
            string sql;
            if (ConnectionType == jlib.db.SqlHelper.ConnectionTypes.Oracle)
                sql = String.Format("nvl({0},'{1}') IN ({2})", this.GetSqlFieldName(), this.GetDefaultNullValue(), parameterList);
            else
			    sql = String.Format("ISNULL([{0}],'{1}') IN ({2})", this.GetSqlFieldName(), this.GetDefaultNullValue(), parameterList);

			//Return Sql
			return sql;

		}
	}
}
