﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.DataFramework.QueryBuilder
{
	public class TrueCondition : EqualCondition
	{
		//Constructors
		public TrueCondition(IColumn Column) : base(Column, true)
		{
		
		}

	}
}
