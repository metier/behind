﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.DataFramework.QueryBuilder
{
	public class FalseCondition : EqualCondition
	{
		//Constructors
		public FalseCondition(IColumn Column) : base(Column, false)
		{
		
		}


	}
}
