﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using jlib.functions;

namespace jlib.DataFramework.QueryBuilder
{
	public class NotEqualCondition : BaseCondition
	{
		//Constructors
		public NotEqualCondition(IColumn Column) : base(Column)
		{
		
		}
		public NotEqualCondition(IColumn Column, object Value) : base(Column)
		{
			this.Value = Value;
		}


		//Properties
		public object Value { get; set; }

		//Methods
		public override string GenerateSql(QueryWriter Writer, jlib.db.SqlHelper.ConnectionTypes ConnectionType)
		{
            if (Value.IsNullOrEmpty())
                return String.Format("[{0}] IS NOT NULL", this.GetSqlFieldName());

			//Get Parameters
			DbParameter sqlParameter = Writer.CreateSqlParameter(this.GetSqlFieldName(), this.Value);
			string parameterName = sqlParameter.ParameterName;

			//Create Sql
			string sql;
			if (ConnectionType == jlib.db.SqlHelper.ConnectionTypes.Oracle)
			{
				sql = String.Format("nvl({0},'{1}')<>{2}", this.GetSqlFieldName(), this.GetDefaultNullValue(), parameterName);
			}
			else
			{
				if (this.Column.DBType == System.Data.DbType.DateTime)
				{
					sql = String.Format("ISNULL(CONVERT(VARCHAR(255),{0}),'{1}')<>{2}", this.GetSqlFieldName(), this.GetDefaultNullValue(), parameterName);
				}
				else
				{
					sql = String.Format("ISNULL([{0}],'{1}')<>{2}", this.GetSqlFieldName(), this.GetDefaultNullValue(), parameterName);
				}
			}

			//Return Sql
			return sql;

		}

	}
}
