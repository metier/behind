﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using jlib.functions;
using jlib.helpers;

namespace jlib.DataFramework.QueryBuilder
{
	public class NotCondition : BaseCondition
	{
		//Constructors
		public NotCondition(BaseCondition Conditions) : base(null)
		{
			this.Condition = Condition;
		}


		//Properties
		public BaseCondition Condition { get; set; }

		//Methods
        public override string GenerateSql(QueryWriter Writer, jlib.db.SqlHelper.ConnectionTypes ConnectionType)
		{
			//Create Sql
			string sql = String.Format("\nNOT ({0})\n", this.Condition.GenerateSql(Writer, ConnectionType));

			//Return Sql
			return sql;

		}

	}
}
