﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace jlib.DataFramework.QueryBuilder
{
	public class LateBindingCondition : BaseCondition, ILateBindingCondition
	{
		//Constructors
		public LateBindingCondition(string Field, string Operator, object Value) : base(null)
		{
			this.Field = Field;
			this.Operator = Operator;
			this.Value = Value;
		}


		//Properties
		public string Field { get; set; }
		public string Operator { get; set; }
		public object Value { get; set; }

		private BaseCondition InternalCondition { get; set; }

		//Methods
		public void Bind(ColumnCollection Columns)
		{
			//Bind Column
			base.Column = Columns.GetByFieldName(this.Field) ?? Columns.GetByColumnName(this.Field);
			
			//Bind Condition
			switch(this.Operator.ToLower())
			{
				case "eq":	//Equal
					this.InternalCondition = new EqualCondition(base.Column, this.Value);
					break;
				case "ne":	//Not Equal
					this.InternalCondition = new NotEqualCondition(base.Column, this.Value);
					break;
				case "gt":	//Greater Than
					this.InternalCondition = new GreaterThanCondition(base.Column, this.Value);
					break;
				case "lt":	//Less Than
					this.InternalCondition = new LessThanCondition(base.Column, this.Value);
					break;
				case "ge":	//Greater Than or Equal
					this.InternalCondition = new GreaterOrEqualThanCondition(base.Column, this.Value);
					break;
				case "le":	//Less Than or Equal
					this.InternalCondition = new LessOrEqualThanCondition(base.Column, this.Value);
					break;
				case "startswith":	//Starts With
					this.InternalCondition = new LikeCondition(base.Column, LikeConditionTypes.StartsWith, this.Value);
					break;
				case "endswith":	//Ends With
					this.InternalCondition = new LikeCondition(base.Column, LikeConditionTypes.EndsWith, this.Value);
					break;
				case "contains":	//Contains
					this.InternalCondition = new LikeCondition(base.Column, LikeConditionTypes.Contains, this.Value);
					break;
						 

			}
		}

        public override string GenerateSql(QueryWriter Writer, jlib.db.SqlHelper.ConnectionTypes ConnectionType)
		{
			return this.InternalCondition.GenerateSql(Writer, ConnectionType);

		}

	}
}
