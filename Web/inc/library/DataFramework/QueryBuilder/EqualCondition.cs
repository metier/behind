﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using jlib.functions;

namespace jlib.DataFramework.QueryBuilder
{
	public class EqualCondition : BaseCondition
	{
		//Constructors
		public EqualCondition(IColumn Column) : base(Column)
		{
		
		}
		public EqualCondition(IColumn Column, object Value) : base(Column)
		{
			this.Value = Value;
		}


		//Properties
		public object Value { get; set; }

		//Methods
        public override string GenerateSql(QueryWriter Writer, jlib.db.SqlHelper.ConnectionTypes ConnectionType)
        {
			//If is null
			if (Value.IsNullOrEmpty())
                return String.Format("{0} IS NULL", this.GetSqlFieldName());
            
            //Get Parameters
			DbParameter sqlParameter;
            if (ConnectionType == jlib.db.SqlHelper.ConnectionTypes.Oracle && (this.Value as string)!=null)
                sqlParameter = Writer.CreateSqlParameter(this.GetSqlFieldName(), this.Value.ToString().ToLower());
            else
                sqlParameter = Writer.CreateSqlParameter(this.GetSqlFieldName(), this.Value);

			string parameterName = sqlParameter.ParameterName;

			//Create Sql
            string sql;
            if( ConnectionType == jlib.db.SqlHelper.ConnectionTypes.Oracle)
			    sql = String.Format("nvl(lower({0}),'{1}')={2}", this.GetSqlFieldName(), this.GetDefaultNullValue(), parameterName);
            else
                sql = String.Format("ISNULL([{0}],'{1}')={2}", this.GetSqlFieldName(), this.GetDefaultNullValue(), parameterName);


			//Return Sql
			return sql;

		}

	}
}
