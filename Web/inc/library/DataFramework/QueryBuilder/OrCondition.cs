﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using jlib.functions;
using jlib.helpers;

namespace jlib.DataFramework.QueryBuilder
{
	public class OrCondition : BaseCondition, ILateBindingCondition
	{
		//Constructors
		public OrCondition(List<BaseCondition> Conditions) : base(null)
		{
			this.Conditions = Conditions;
		}


		//Properties
		public List<BaseCondition> Conditions { get; set; }

		//Methods
		public void Bind(ColumnCollection Columns)
		{
			this.Conditions.Where(x => x is ILateBindingCondition).Cast<ILateBindingCondition>().ToList().ForEach(x => x.Bind(Columns));
		}
		public override string GenerateSql(QueryWriter Writer, jlib.db.SqlHelper.ConnectionTypes ConnectionType)
		{
			//Create Sql
            string combinedConditions = Conditions.Select(c => String.Format("({0})", c.GenerateSql(Writer, ConnectionType))).ToList().Join("\n OR ");
			string sql = String.Format("(\n{0}\n)", combinedConditions);

			//Return Sql
			return sql;

		}

	}
}
