﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace jlib.DataFramework.QueryBuilder
{
	public class BetweenCondition : BaseCondition
	{
		//Constructors
		public BetweenCondition(IColumn Column, object Value1, object Value2) : base(Column)
		{
			this.Value1 = Value1;
			this.Value2 = Value2;
		}
		public BetweenCondition(IColumn Column) : base(Column)
		{
		
		}

		//Properties
		public object Value1 { get; set; }
		public object Value2 { get; set; }

		//Methods
        public override string GenerateSql(QueryWriter Writer, jlib.db.SqlHelper.ConnectionTypes ConnectionType)
		{
			//Get Parameters
			DbParameter sqlParameter1 = Writer.CreateSqlParameter(this.GetSqlFieldName()+"1", this.Value1);
			string parameter1Name = sqlParameter1.ParameterName;

			DbParameter sqlParameter2 = Writer.CreateSqlParameter(this.GetSqlFieldName()+"2", this.Value2);
			string parameter2Name = sqlParameter2.ParameterName;

			//Create Sql
            string sql;
            if( ConnectionType == jlib.db.SqlHelper.ConnectionTypes.Oracle)
                sql = String.Format("nvl({0},'{1}') BETWEEN {2} AND {3}", this.GetSqlFieldName(), this.GetDefaultNullValue(), parameter1Name, parameter2Name);
            else
                sql = String.Format("ISNULL({0},'{1}') BETWEEN {2} AND {3}", this.GetSqlFieldName(), this.GetDefaultNullValue(), parameter1Name, parameter2Name);

			//Return Sql
			return sql;
		}
	}
}
