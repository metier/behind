﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jlib.DataFramework.QueryBuilder
{
	public interface ILateBindingCondition
	{
		void Bind(ColumnCollection Columns);
	}
}
