﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.DataFramework.QueryBuilder
{
	public enum OrderByDirections
	{
		Ascending,
		Descending
	}

	public class OrderBy
	{
        private string m_sFieldName = "";
        //Constructor
		public OrderBy(IColumn Column, OrderByDirections Direction)
		{
			this.Column = Column;
			this.Direction = Direction;
		}
        public OrderBy(string sFieldName, OrderByDirections Direction) {
            m_sFieldName = sFieldName;
            this.Direction = Direction;
        }

		//Properties
        public string FieldName {
            get {
                if (Column == null) return m_sFieldName;
                return Column.FieldName;
            }
        }
		public IColumn Column { get; set; }
		public OrderByDirections Direction { get; set; }

		//Methods
		public string GenerateSql(QueryWriter Writer)
		{
            if (Writer as OracleQueryWriter != null) {
                return string.Format("{0} {1}", (Column == null || (Column.DBType != System.Data.DbType.Int16 && Column.DBType != System.Data.DbType.Int32 && Column.DBType != System.Data.DbType.Int64 && Column.DBType != System.Data.DbType.VarNumeric) ? this.FieldName : "nvl(" + this.FieldName + ",0)"), this.Direction == OrderByDirections.Ascending ? "ASC" : "DESC");
            } else {
                return string.Format("[{0}] {1}", this.FieldName, this.Direction == OrderByDirections.Ascending ? "ASC" : "DESC");
            }
		}
	}
}
