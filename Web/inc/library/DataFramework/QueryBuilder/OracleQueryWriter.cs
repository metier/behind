﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data.Common;
using System.Reflection;
using System.Dynamic;
using System.Text.RegularExpressions;

namespace jlib.DataFramework.QueryBuilder
{
	public class OracleQueryWriter: QueryWriter
	{
		#region ***** CONSTANTS *****
		/*
        public const string INNER_QUERY_WITH_BLOCK = "{InnerQueryWithBlock}";
		public const string WITH_TAG = "{WithTag}";
		public const string INNER_QUERY_RESULTS_KEY = "{InnerQueryResultsName}";
		public const string INNER_QUERY_RESULTS_NAME = "jLibInnerResults";
		public const string OUTER_QUERY_RESULTS_KEY = "{OuterQueryResultsName}";
		public const string OUTER_QUERY_RESULTS_NAME = "jLibOuterResults";
		public const string INNER_QUERY_STATEMENT_KEY = "{InnerQueryStatement}";
		public const string ORDERBY_CLAUSE_KEY = "{OrderByClause}";
		public const string ROW_NUMBER_COLUMN_NAME_KEY = "{RowNumberColumnName}";*/
        public new const string ROW_NUMBER_COLUMN_NAME = "\"Paging.Index\"";
		//public const string RECORD_COUNT_COLUMN_NAME_KEY = "{RecordCountColumnName}";
        public new const string RECORD_COUNT_COLUMN_NAME = "\"Paging.TotalRows\"";
		/*public const string PRE_PAGING_CONDITIONS_CLAUSE_KEY = "{PreConditionsClause}";
		public const string POST_PAGING_CONDITIONS_CLAUSE_KEY = "{PostConditionsClause}";
		public const string PAGING_START_INDEX_PARAMETER_NAME = "jLibPagingStartIndex";
		public const string PAGING_END_INDEX_PARAMETER_NAME = "jLibPagingEndIndex";
         */
		#endregion

		#region ***** CONSTRUCTORS *****
        public OracleQueryWriter(IQueryPackage InnerQuery)
            : base(InnerQuery)
		{
			//Resource Helper
			Resources.ResourceHelper resources = new Resources.ResourceHelper();

			base.TemplateSql = resources.OracleQueryBuilderTemplate;            
		}
		#endregion

        #region ***** WRITE METHODS *****
        public override void WriteInnerQuery(IQueryPackage QueryPackage) {
            //If Sproc, throw error
            if (QueryPackage.IsStoredProcedure)
                throw new Exception("QueryBuilder does not work with stored procedures");

            //Set Internal Sql
            this.InnerStatement = QueryPackage.Statement;

            //Set Parameters
            foreach (string fieldName in QueryPackage.Parameters.Dictionary.Keys) {
                //Get Parameter Name
                string parameterName = "@" + fieldName;

                //Get Value
                object value = QueryPackage.Parameters.Dictionary[fieldName];

                //Create SqlParameter
                OleDbParameter sqlParameter = new OleDbParameter(parameterName, value);
                this.AddParameter(parameterName, sqlParameter);
            }
        }

        public override void WritePaging(int StartIndex, int EndIndex) {
            //Get Parameters
            DbParameter startParameter = this.CreateSqlParameter(OracleQueryWriter.PAGING_START_INDEX_PARAMETER_NAME, StartIndex);
            DbParameter endParameter = this.CreateSqlParameter(OracleQueryWriter.PAGING_END_INDEX_PARAMETER_NAME, EndIndex);

            //Get Names
            string startParameterName = startParameter.ParameterName;
            string endParameterName = endParameter.ParameterName;

            //Add Where Clause
            this.PostConditionsClause.Add(String.Format("{0} BETWEEN {1} AND {2}", OracleQueryWriter.ROW_NUMBER_COLUMN_NAME, startParameterName, endParameterName));
        }
        #endregion

		
		#region ***** PARAMETER METHODS *****
        public override DbParameter CreateSqlParameter(string FieldName, object Value)
		{
			//Get Parameter Name
			string parameterName = ":" + FieldName;

			//Check to make sure FieldName is unique
			if (this.ParameterExists(parameterName))
			{
				//Find Unique Parameter Name
				int parameterIndex = 2;
				string newParameterName = "";
				while (this.ParameterExists(newParameterName = String.Format("{0}{1}", parameterName, parameterIndex))) 
				{
					parameterIndex++;
				}

				//Set Parameter Name
				parameterName = newParameterName;
			}

			//Create new SqlParameters
            OleDbParameter sqlParameter = new OleDbParameter(parameterName, Value);

			//Add to collection
			this.AddParameter(parameterName, sqlParameter);

			//Return Parameter
			return sqlParameter;
		}

        public void AddParameter(string ParameterName, OleDbParameter Parameter)
		{
			this.Parameters.Add(ParameterName, Parameter);
		}

		#endregion

		
		#region ***** OUTPUT METHODS *****
		public override string ToString()
		{
			//Get Template
			string sql = this.TemplateSql;

			//Write results sets
            sql = sql.Replace(OracleQueryWriter.INNER_QUERY_RESULTS_KEY, OracleQueryWriter.INNER_QUERY_RESULTS_NAME);
            sql = sql.Replace(OracleQueryWriter.OUTER_QUERY_RESULTS_KEY, OracleQueryWriter.OUTER_QUERY_RESULTS_NAME);
            sql = sql.Replace(OracleQueryWriter.ROW_NUMBER_COLUMN_NAME_KEY, OracleQueryWriter.ROW_NUMBER_COLUMN_NAME);
			sql = sql.Replace(OracleQueryWriter.RECORD_COUNT_COLUMN_NAME_KEY,OracleQueryWriter.RECORD_COUNT_COLUMN_NAME);

			//Write Inner query
			Match match = null;
			if ((match = this.DoesInnerStatementContainWith()).Success)
			{
				//Get With Expressions
				string[] expressions = this.SplitInnerStatementWithExpressions(match);

				//Append to sql output
				sql = sql.Replace(OracleQueryWriter.WITH_TAG, "");
				sql = sql.Replace(OracleQueryWriter.INNER_QUERY_WITH_BLOCK, expressions[0] + ",");
				sql = sql.Replace(OracleQueryWriter.INNER_QUERY_STATEMENT_KEY, expressions[1]);
			}
			else
			{
				sql = sql.Replace(OracleQueryWriter.WITH_TAG, "WITH");
				sql = sql.Replace(OracleQueryWriter.INNER_QUERY_WITH_BLOCK, "");
				sql = sql.Replace(OracleQueryWriter.INNER_QUERY_STATEMENT_KEY, this.InnerStatement);
			}

			//Write Pre Condition (ie. Filters)
			string preConditionsClause = "";
			if (this.PreConditionsClause.Count > 0)
				preConditionsClause = String.Concat("WHERE \r\n\t", String.Join("\n AND ", this.PreConditionsClause));
			sql = sql.Replace(OracleQueryWriter.PRE_PAGING_CONDITIONS_CLAUSE_KEY, preConditionsClause);

			//Write Post Condition (ie. Paging)
			string postConditionsClause = "";
			if (this.PostConditionsClause.Count > 0)
				postConditionsClause = String.Concat("WHERE \r\n\t", String.Join("\n AND ", this.PostConditionsClause));
			sql = sql.Replace(OracleQueryWriter.POST_PAGING_CONDITIONS_CLAUSE_KEY, postConditionsClause);

			//If Order By Not specified, use first column
            if (this.OrderBy.Count == 0) {
                this.OrderBy.Add("(SELECT 1 FROM DUAL)");
            } else {
                //this.OrderBy.ForEach(x => x = (x.EndsWith(" ASC", StringComparison.CurrentCultureIgnoreCase) || x.EndsWith(" DESC", StringComparison.CurrentCultureIgnoreCase) ? "\"" + "\" " + x.Substring(x.Length-3) : x ));
            }

			//Write Order By
			string orderByClause = String.Concat("ORDER BY ", String.Join(", ", this.OrderBy));
			sql = sql.Replace(OracleQueryWriter.ORDERBY_CLAUSE_KEY, orderByClause);

			//Return string
			return sql;
		}
		#endregion
	}
}
