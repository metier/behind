﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace jlib.DataFramework.QueryBuilder
{
	public class LessOrEqualThanCondition : BaseCondition
	{
		//Constructors
		public LessOrEqualThanCondition(IColumn Column) : base(Column)
		{
		
		}
		public LessOrEqualThanCondition(IColumn Column, object Value) : base(Column)
		{
			this.Value = Value;
		}


		//Properties
		public object Value { get; set; }

		//Methods
        public override string GenerateSql(QueryWriter Writer, jlib.db.SqlHelper.ConnectionTypes ConnectionType)
		{
			//Get Parameters
			DbParameter sqlParameter = Writer.CreateSqlParameter(this.GetSqlFieldName(), this.Value);
			string parameterName = sqlParameter.ParameterName;

			//Create Sql
			string sql;
            if( ConnectionType == jlib.db.SqlHelper.ConnectionTypes.Oracle)
                sql = String.Format("nvl({0},'{1}')<={2}", this.GetSqlFieldName(), this.GetDefaultNullValue(), parameterName);
            else
                sql = String.Format("ISNULL([{0}],'{1}')<={2}", this.GetSqlFieldName(), this.GetDefaultNullValue(), parameterName);

			//Return Sql
			return sql;

		}

	}
}
