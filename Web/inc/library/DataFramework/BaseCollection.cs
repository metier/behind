﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using jlib.db;
using jlib.functions;
using System.ComponentModel;
using System.Collections;
using jlib.DataFramework.Extensions;
using jlib.DataFramework.Paging;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

namespace jlib.DataFramework
{
	public abstract class BaseCollection<RecordType, ContextType> : List<RecordType>, ICollection, IQueryable, ICacheable, ISerializable, ITypedList
		where RecordType: BaseRecord<ContextType>, IRecord, new()
		where ContextType: BaseContext, new()
	{
		#region ***** CONSTRUCTORS *****
		public BaseCollection(ContextType Context)
		{
			this.Context = Context;
			this.Columns = new ColumnCollection();
		}
		#endregion

		#region ***** PROPERTIES *****
		public string DbEntityName { get; set; }
		public Paging.PagingDetails PagingDetails { get; set; }
		public ContextType Context { get; set; }
		public ColumnCollection Columns { get; private set; }

		IRecord ICollection.this[int Index]
		{
			get { return base[Index]; }
			set { base[Index] = (RecordType)value;  }
		}

		private PropertyDescriptorCollection _PropertyDescriptorCollectionCache { get; set; }
		#endregion

		#region ***** METHODS *****
		public void ImportDataTable(DataTable dtRows)
		{
			//Declare DataMapTable
			DataMapTable mapTable = null;

			//Populate Internal List
			foreach (DataRow dataRow in dtRows.Rows)
			{
				//Create New Record
				RecordType record = new RecordType();
				record.Collection = this;
				record.Context = Context;

				//If not defined, create MapTable
				if (mapTable == null) mapTable = record.GetDataMapTable(dataRow);

				//Call Import Using Map Table
				record.Import(dataRow, mapTable);

				//Add to Internal List
				this.Add(record);
			}

			//if any columns are missing from parent then add
			foreach (DataColumn column in dtRows.Columns)
			{
				if (!this.Columns.ContainsDataField(column.ColumnName))
				{
					this.Columns.Add(column.ColumnName, new BaseColumn<string>(FieldName: column.ColumnName, PropertyName: column.ColumnName, ParameterName: null, DBType: DbType.String, IsVirtualColumn: true));
				}
			}
		}

        public virtual void GetObjectData(System.Runtime.Serialization.SerializationInfo SerializationInfo, System.Runtime.Serialization.StreamingContext StreamingContext) 
		{
            if (SerializationInfo == null)
                throw new ArgumentNullException("SerializationInfo");

			//Set Records
			SerializationInfo.AddValue("Items", this.ToList());
        }
        public virtual int DtsSave(SqlHelper SqlHelper, Action<object, SqlRowsCopiedEventArgs> fUpdateProgress, DataRowAction statesToProcess){
			///TODO: Implement Serialization Logic
			throw new NotImplementedException();
		}

		protected internal virtual SqlHelper GetSqlHelper()
		{
			return this.Context.GetSqlHelper();
		}

		public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
        {
			return this.GetPropertyDescriptors();
        }

        // This method is only used in the design-time framework  
        // and by the obsolete DataGrid control. 
        public string GetListName(PropertyDescriptor[] listAccessors)
        {   
            return typeof(RecordType).Name;
        }

		public PropertyDescriptorCollection GetPropertyDescriptors()
		{
			//if no cache, then build
			if (this._PropertyDescriptorCollectionCache == null)
			{
				PropertyDescriptor[] properties = this.Columns.Select(column => new ColumnDescriptor(column.Value as IColumn)).ToArray<ColumnDescriptor>();
				this._PropertyDescriptorCollectionCache = new PropertyDescriptorCollection(properties);
			}
			
			//return Cache
			return this._PropertyDescriptorCollectionCache;

		}
		IList IListSource.GetList()
		{
			return this;
		}

		bool IListSource.ContainsListCollection
		{
			get { return false; }
		}
		
		public string ToJson(functions.json.JsonWriter Writer = null, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null)
		{
            return Extensions.JSON.ToJson<RecordType>(this, Writer, true, IncludeColumns, ExcludeColumns, this.PagingDetails);
		}
        public string ToJsonArray(functions.json.JsonWriter Writer = null, params IColumn []oColsToInclude) 
		{
            return Extensions.JSON.ToJsonArray<RecordType>(this, Writer, oColsToInclude);
        }
		public string ToJsonArray(functions.json.JsonWriter Writer = null, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null) 
		{
            return Extensions.JSON.ToJsonArray<RecordType>(this, Writer, IncludeColumns, ExcludeColumns);
        }
        public string ToJsonMinified(functions.json.JsonWriter Writer = null, List<IColumn> IncludeColumns = null, List<IColumn> ExcludeColumns = null) 
		{
            string JSONArray = Extensions.JSON.ToJsonArray<RecordType>(this, Writer, IncludeColumns, ExcludeColumns, false);
            if (JSONArray.StartsWith("[[[")) JSONArray = JSONArray.Substr(1, -1);
            return "ExpandJSONArray('" + this.GetType().Name + "','" + (this.Count > 0 ? this[0].GetType().Name : "") + "'," + (this.Count== 0 ? "[]" : Extensions.JSON.GetJSONColumns<RecordType>(this[0], IncludeColumns, ExcludeColumns).ToJSON()) + "," + JSONArray + ")";
        }

		public virtual IDataReader GetDataReader() { throw new NotImplementedException(); }


		public virtual Dictionary<T, RecordType> ToDictionary<T>(Func<RecordType, T> fnGetKey)
		{
			//Create Dictionary
			var dictionary = new Dictionary<T, RecordType>();

			//Map To Dictionary
			this.ForEach(x => dictionary[fnGetKey(x)] = x);

			//Return
			return dictionary;

		}
		#endregion

		#region ***** STATIC CRUD METHODS *****
		public static CollectionType LoadByCommand<CollectionType, QueryPackageType>(ContextType Context, IQueryPackage Command)
			where CollectionType :  BaseCollection<RecordType, ContextType>, new()
			where QueryPackageType : IQueryPackage, new()
		{

			//SqlHelper
			SqlHelper sqlHelper = Context.GetSqlHelper();

			//If Cache is not supplied, load from previous method attribute
			var cacheOptions = Command.CacheOptions ?? Cache.Attributes.CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Process QueryBuilder
			//if(!Regex.IsMatch(Command.Statement ?? "", @"ORDER\s*BY", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline))

            Command = Command.ToQueryBuilder().ToQueryPackage(QueryBuilder.QueryBuilder.QueryBuilderOperationTypes.Select);
			if (Command.CacheOptions == null) Command.CacheOptions = cacheOptions;

			//Build Parameters Collection
            jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> sqlParameters = sqlHelper.BuildParameterCollection(Command.Parameters);

			//Create New Collection
			CollectionType results = new CollectionType();

			//Check Cache
			CollectionType cachedCollection = Cache.CacheHelper.GetCacheItem<CollectionType>(Command.CacheOptions, Command.Statement, sqlParameters);

			//Use cache
			if (cachedCollection != null)
			{
				//Loop through
				foreach(RecordType record in cachedCollection)
				{
					record.Collection = results;
					results.Add(record);
				}

				//if any columns are missing from parent, then add
				if (cachedCollection.Count > 0)
				{
					foreach (IField field in cachedCollection[0].Fields.Where(x=>x.Value.IsVirtualColumn).Select(x=>x.Value))
					{
						if (!results.Columns.ContainsKey(field.FieldName))
						{
							results.Columns.Add(field.FieldName, new BaseColumn<string>(FieldName: field.FieldName, PropertyName: field.FieldName, ParameterName: null, DBType: DbType.String, IsVirtualColumn: true));
						}
					}
				}

				//Copy Paging Details
				results.PagingDetails = cachedCollection.PagingDetails;
			}

			//Not in cache, so requery
			else
			{
				//Get Results from SQL Server
				DataTable dataTable = sqlHelper.Execute(Command.Statement, sqlParameters);

				//Import From Datatable
				results.ImportDataTable(dataTable);

				
				//Build PagingDetails
				results.PagingDetails = Paging.PagingDetails.UpdatePagingDetails(dataTable, Command.PagingOptions);
			}

			//Save to Cache
			Cache.CacheHelper.SetCacheItem(Command.CacheOptions, Command.Statement, sqlParameters, results);

			//Return Results
			return results;
			
		}
		
		public static CollectionType ImportDataTable<CollectionType>(DataTable dtRows) 
			where CollectionType :  BaseCollection<RecordType, ContextType>, new()
		{
			//Create New Collection
			CollectionType results = new CollectionType();

			//Import From Datatable
			results.ImportDataTable(dtRows);

			//Return
			return results;

		}

		public static void DeleteByCommand<CollectionType, QueryPackageType>(ContextType Context, IQueryPackage Command)
			where CollectionType : BaseCollection<RecordType, ContextType>, new()
			where QueryPackageType : IQueryPackage, new()
		{

			//SqlHelper
			SqlHelper sqlHelper = Context.GetSqlHelper();

			var builder = Command.ToQueryBuilder();
			builder.PagingOptions = null;
			builder.CacheOptions = null;
			builder.ClearOrderBy();
			Command = builder.ToQueryPackage(QueryBuilder.QueryBuilder.QueryBuilderOperationTypes.Delete);
			
			//Build Parameters Collection
			jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> sqlParameters = sqlHelper.BuildParameterCollection(Command.Parameters);

			//Create New Collection
			CollectionType results = new CollectionType();
			
			//Delete from SQL Server
			sqlHelper.Execute(Command.Statement, sqlParameters);
			

		}
		#endregion


	}
}
