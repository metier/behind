﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jlib.db;
using jlib.helpers.Structures;
using jlib.DataFramework.Cache;
using jlib.DataFramework.Paging;

namespace jlib.DataFramework
{
	public interface IQueryPackage
	{
		//Properties
		string Statement { get; set; }
		bool IsStoredProcedure { get; set; }
		dynamic Parameters { get; set; }
		CacheOptions CacheOptions { get; set; }
		PagingOptions PagingOptions { get; set; }
		ColumnCollection Columns { get; }
		QueryBuilder.QueryBuilder InternalQueryBuilder { get; set; }
		

		//Methods
		void SetInternalStatement(string Statement);
		ICollection Execute();
		ICollection Select();
		void Delete();

		QueryBuilder.QueryBuilder ToQueryBuilder();
		IQueryPackage Where(QueryBuilder.BaseCondition Condition);
		IQueryPackage Where(Func<ICollection, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition);
		IQueryPackage And(QueryBuilder.BaseCondition Condition);
		IQueryPackage And(Func<ICollection, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition);
		IQueryPackage Or(QueryBuilder.BaseCondition Condition);
		IQueryPackage Or(Func<ICollection, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition);
		IQueryPackage OrderBy(IColumn Column);
		IQueryPackage OrderBy(IColumn Column, QueryBuilder.OrderByDirections Direction);
		IQueryPackage OrderByAscending(IColumn Column);
		IQueryPackage OrderByDescending(IColumn Column);
	}
}
