﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using jlib.db;

namespace jlib.DataFramework
{
	public interface IUpdatable
	{
		void Save();
		void Save(int iAuthorID, SaveOptions Options);
		void Save(SaveOptions Options);
		void Save(SqlHelper SqlHelper, int iAuthorID);
		void Save(SqlHelper SqlHelper, int iAuthorID, SaveOptions Options);
		void Save(SqlHelper SqlHelper, SaveOptions Options);
		void Delete();
		void Delete(int iAuthorID);
		void Delete(SqlHelper SqlHelper, int iAuthorID);

	}
}
