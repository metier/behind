﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using jlib.DataFramework.Validation;
using jlib.db;
using System.Data.Common;
using jlib.functions;

namespace jlib.DataFramework
{
    [AttributeUsage(AttributeTargets.Class)]
    public class HasInsertTrigger : Attribute {
    }

	public abstract class BaseTableRecord<ContextType> : BaseRecord<ContextType>, IUpdatable
		where ContextType : BaseContext, new()
	{
        

		#region ***** CONSTRUCTORS *****
		public BaseTableRecord(ContextType Context) : base(Context)
		{

		}
		#endregion

		#region ***** PROPERTIES *****
		public string TABLE_NAME 
		{
			get { return base.DbEntityName; }
			protected set { base.DbEntityName = value; }
		}
		#endregion

		#region ***** METHODS *****
		public virtual ValidationSummary Validate()
		{
			//Declare Validation Summary
			ValidationSummary summary = new ValidationSummary();

			//Check for Nulls
			// - Real fields
			// - Not Nullable
			// - (RecordAction == Change || Not HasDbDefault)
			this.Fields
				.Where(field => !field.Value.IsIdentity && !field.Value.IsVirtualColumn && !field.Value.IsNullable && (this.RecordState == DataRowAction.Change || !field.Value.HasDbDefault))
			    .ToList()
			    .ForEach(field =>
				    {
						summary.AddIfError(ValidationRule.ValidateIsNotNull(field.Value));
				    });

			//Check Date Values
			this.Fields
				.Where(field => !field.Value.IsVirtualColumn && field.Value.ObjectType == typeof(DateTime))
				.ToList()
				.ForEach(field =>
				{
					//if blank ignore
					if(String.IsNullOrWhiteSpace(field.Value.RawValue.Str())) return;

					//Validate
					summary.AddIfError(ValidationRule.ValidateIsDateOrBlank(field.Value));
				});

			//Check Number Values
			this.Fields
				.Where(field => !field.Value.IsIdentity && !field.Value.IsVirtualColumn && (field.Value.ObjectType == typeof(int) || field.Value.ObjectType == typeof(decimal) || field.Value.ObjectType == typeof(long)))
				.ToList()
				.ForEach(field =>
				{
					//if blank ignore
					if (String.IsNullOrWhiteSpace(field.Value.RawValue.Str())) return;

					//Validate
					summary.AddIfError(ValidationRule.ValidateIsNumber(field.Value));
				});

			//Return summary
			return summary;
		}

		public virtual ValidationSummary ValidateAndSave() { return this.ValidateAndSave(0); }
		public virtual ValidationSummary ValidateAndSave(int AuthorId) { return this.ValidateAndSave<int>(AuthorId); }
		public virtual ValidationSummary ValidateAndSave(Guid AuthorId) { return this.ValidateAndSave<Guid>(AuthorId); }
		public virtual ValidationSummary ValidateAndSave<T>(T AuthorId)
		{
			//Validate
			var summary = this.Validate();

			//If successful, save
			if (summary.IsSuccessful) this.Save(AuthorId);

			//return summary
			return summary;
		}

		public virtual void Save() { SqlHelper sqlHelper = this.Context.GetCurrentTransactionOrCreate(); this.Save(sqlHelper, 0, null); }
		public virtual void Save(int AuthorId) { this.Save<int>(AuthorId); }
		public virtual void Save(Guid AuthorId) { this.Save<Guid>(AuthorId); }
		public virtual void Save<T>(T AuthorId) 
		{
            SqlHelper sqlHelper = this.Context.GetCurrentTransactionOrCreate();
            this.Save(sqlHelper, AuthorId, null);
        }


		public virtual void Save(bool ReloadDBValues) 
		{
			SqlHelper sqlHelper = this.Context.GetCurrentTransactionOrCreate();
            this.Save(sqlHelper, 0, new SaveOptions() { ReloadValuesFromDb=ReloadDBValues} );
        }
		public virtual void Save(SaveOptions Options) 
		{
			SqlHelper sqlHelper = this.Context.GetCurrentTransactionOrCreate();
            this.Save(sqlHelper, 0, Options);
		}

		public virtual void SaveWithNotes(int AuthorId, string Notes) { this.SaveWithNotes<int>(AuthorId, Notes); }
		public virtual void SaveWithNotes(Guid AuthorId, string Notes) { this.SaveWithNotes<Guid>(AuthorId, Notes); }
		public virtual void SaveWithNotes<T>(T AuthorId, string Notes) { SqlHelper sqlHelper = this.Context.GetCurrentTransactionOrCreate(); this.Save(sqlHelper, AuthorId, null); }

		public virtual void Save(int AuthorId, SaveOptions Options) { this.Save<int>(AuthorId, Options); }
		public virtual void Save(Guid AuthorId, SaveOptions Options) { this.Save<Guid>(AuthorId, Options); }
		public virtual void Save<T>(T AuthorId, SaveOptions Options)  { SqlHelper sqlHelper = this.Context.GetCurrentTransactionOrCreate(); this.Save(sqlHelper, AuthorId, Options); }
        public virtual void Save(SqlHelper SqlHelper, int AuthorId) { this.Save<int>(SqlHelper, AuthorId, null); }
		public virtual void Save(SqlHelper SqlHelper, Guid AuthorId) { this.Save<Guid>(SqlHelper, AuthorId, null); }
		public virtual void Save(SqlHelper SqlHelper, SaveOptions Options) { this.Save(SqlHelper, 0, Options); }
		public virtual void Save(SqlHelper SqlHelper, int AuthorId, SaveOptions Options) { this.Save<int>(SqlHelper, AuthorId, Options); }
		public virtual void Save(SqlHelper SqlHelper, Guid AuthorId, SaveOptions Options) { this.Save<Guid>(SqlHelper, AuthorId, Options); }
        public virtual void Save<T>(SqlHelper SqlHelper, T AuthorId, SaveOptions Options)
		{
			//Options
			Options = Options ?? new SaveOptions();

			//Insert / Update
			if (this.RecordState == DataRowAction.Add || this.RecordState == DataRowAction.Change)
			{
				//Get Changed Values
				List<IField> changedFields = this.Fields.GetChangedFields();

				//Get Primary Key Fields
				List<IField> pkFields = this.Fields.GetPkFields();

				//if changedFields is empty, reset
				if (changedFields.Count == 0)
				{
					return;
				}

				//Declare Sql
				string sql = "";
				List<DbParameter> parameters = new List<DbParameter>();
                

				//if new record, then do insert
                if (this.RecordState == DataRowAction.Add) 
				{
                    bool hasInsertTrigger = this.GetType().IsDefined(typeof(HasInsertTrigger), false);
                    if (IsOracle()) 
					{
                        //if (!reloadDBValues || hasInsertTrigger) sql += "DECLARE @InsertTable TABLE ( {0} ) ";
                        sql += @"INSERT INTO ""{1}""  ({2}) "                          
                                + @" VALUES ({4}) "
                                + (this.Fields.ContainsFieldName("ID") && this.Fields.GetFieldValueByFieldName("ID").Dbl() == 0 ? " RETURNING \"ID\" INTO :ID" : "")                            
                            ;                        
                    }
					
					else 
					{
                        if(!Options.ReloadValuesFromDb || hasInsertTrigger) sql+= "DECLARE @InsertTable TABLE ( {0} ); ";
                        sql += @"INSERT INTO [{1}]  ({2}) " 
                                + (!Options.ReloadValuesFromDb || hasInsertTrigger ? @"OUTPUT {3} INTO @InsertTable" : "OUTPUT INSERTED.*")
                                + @" VALUES ({4}); " 
                                + (Options.ReloadValuesFromDb && hasInsertTrigger ? @" SELECT * FROM [{1}] WITH(NOLOCK) INNER JOIN @InsertTable AS InsertTable ON {5}; " : "");
                    }
                    sql = String.Format(sql,
                            //Primary Keys Types
                            String.Join(",\n\t\t\t\t\t\t\t\t\t", pkFields.Select(x => String.Format(GetEscapeStart() +"{0}"+GetEscapeEnd()+" SQL_VARIANT", x.FieldName))),

                            //Table Name
                            this.TABLE_NAME,

                            //Changed Fields
                            String.Join(",\n\t\t\t\t\t\t\t\t\t", changedFields.Select(x => String.Format(GetEscapeStart()+"{0}"+GetEscapeEnd(), x.FieldName))),

                            //Pk Fields to Output
                            String.Join(",\n\t\t\t\t\t\t\t\t\t", pkFields.Select(x => String.Format("INSERTED." + GetEscapeStart()+ "{0}"+ GetEscapeEnd(), x.FieldName))),

                            //Changed Values
                            String.Join(",\n\t\t\t\t\t\t\t\t\t", changedFields.Select(x => {
                                //Create Sql Parameter
                                var parameter = x.BuildSqlParameter(this.Context.ConnectionType, false);

                                //Add to collection
                                parameters.Add(parameter);

                                //Return parameter name
                                return parameter.ParameterName;
                            })),

                            //Pk Select Expression
                            String.Join("\n\t\t\t\t\t\t\t\t\tAND ", pkFields.Select(x => String.Format(IsOracle() ? @"{0}.""{1}"" = InsertTable.""{1}""" : @"{0}.[{1}] = InsertTable.[{1}]", this.TABLE_NAME, x.FieldName)))
                        );

                    System.Data.OleDb.OleDbParameter oracleIDParam=null;
                    if (IsOracle() && sql.Contains(":ID")) 
					{
                        oracleIDParam = new System.Data.OleDb.OleDbParameter(":ID", System.Data.OleDb.OleDbType.Decimal);
                        oracleIDParam.Direction = ParameterDirection.ReturnValue;
                        oracleIDParam.Value = 0;
                        parameters.Add(oracleIDParam);
                    }
                    //Execute Command
                    DataTable dataTable = SqlHelper.ExecuteSql(sql, parameters.ToArray());

                    //Import Updates (if !reloadDBValues, only PKs are imported)
                    if (dataTable != null)
                        this.Import(dataTable.Rows[0]);
                    else if (oracleIDParam != null)
                        this.Fields.GetByFieldName("ID").SetLoadedValue(oracleIDParam.Value);
                }

                //Update
                else if (this.RecordState == DataRowAction.Change) 
				{
                    if(IsOracle())
					{
                        sql=@"
								UPDATE
									""{0}""
								SET
									{1}
								WHERE
									{2}";
                    }
					
					else
					{
                        sql=@"
								UPDATE
									[{0}]
								SET
									{1}
								WHERE
									{2};";
                    }
                    sql = String.Format(sql,

                                //Table Name
                                this.TABLE_NAME,

                                //Updated Values
                                String.Join(",\n\t\t\t\t\t\t\t\t\t", changedFields.Select(x => 
								{
                                    //Create Sql Parameter
                                    var parameter = x.BuildSqlParameter(this.Context.ConnectionType, false);

                                    //Add to collection
                                    parameters.Add(parameter);

                                    //Return parameter name
                                    return String.Format(IsOracle() ? @"""{0}"" = {1}" : @"[{0}] = {1}", x.FieldName, parameter.ParameterName);
                                })),

                                //Primary Key Clause
                                String.Join("\n\t\t\t\t\t\t\t\t\tAND ", pkFields.Select(x => 
								{
                                    //Create Sql Parameter
                                    var parameter = x.BuildSqlParameter(this.Context.ConnectionType, true);

                                    //Add to collection
                                    parameters.Add(parameter);

                                    //Return parameter name
                                    return String.Format(IsOracle() ? @"""{0}"" = {1}" : @"[{0}] = {1}", x.FieldName, parameter.ParameterName);
                                }))
                            );

					//If Disable Trigger Flag
					if(Options.DisableTriggers)
					{
						sql = "SET Context_info 0x4975;\n\n" + sql + "\n\nSET Context_info 0;";
					}

                    //Execute Command
                    DataTable dataTable = SqlHelper.ExecuteSql(sql, parameters.ToArray());

                    
                }

                //Update Fields
                this.Fields.ToList().ForEach(x => x.Value.SetLoadedValue(x.Value.RawValue));
                this.RecordState = DataRowAction.Nothing;				
			}			
		}
		public virtual void Delete() { SqlHelper sqlHelper = this.Context.GetCurrentTransactionOrCreate(); this.Delete(sqlHelper, 0); }
        public virtual void Delete(int AuthorId) { this.Delete<int>(AuthorId); }
		public virtual void Delete(Guid AuthorId) { this.Delete<Guid>(AuthorId); }
		public virtual void Delete<T>(T AuthorId)  { SqlHelper sqlHelper = this.Context.GetCurrentTransactionOrCreate(); this.Delete(sqlHelper, AuthorId); }
		public virtual void Delete(SqlHelper SqlHelper, Guid AuthorId) { this.Delete<Guid>(SqlHelper, AuthorId);  }
		public virtual void Delete(SqlHelper SqlHelper, int AuthorId) { this.Delete<int>(SqlHelper, AuthorId);  }
        public virtual void Delete<T>(SqlHelper SqlHelper, T AuthorId)
		{

			string deletedColName = null;
			if (this.Fields.GetByFieldName("Deleted") != null) deletedColName = "Deleted";
			else if (this.Fields.GetByFieldName("IsDeleted") != null) deletedColName = "IsDeleted";
			else if (this.Fields.GetByFieldName("Is_Deleted") != null) deletedColName = "Is_Deleted";

            string whereClause="";
            List<object> parameters=new List<object>();
            List<IField> pkFields = this.Fields.GetPkFields();
            pkFields.ForEach(x => {
                parameters.Add(x.PkParameterName);
                parameters.Add(x.RawValue);
                whereClause += (whereClause.IsNullOrEmpty() ? "" : " AND " ) + x.FieldName + "=" + x.PkParameterName;
            });          
            
            if(deletedColName.IsNullOrEmpty())
			{
				this.DeletePermanent(SqlHelper, AuthorId);
            }

			else
			{
				this.DeleteSoft(SqlHelper, AuthorId, deletedColName);
            }
            //Set Record State
            this.RecordState = DataRowAction.Delete;
        }
		public virtual void DeletePermanent<T>(SqlHelper SqlHelper, T AuthorId)
		{
			string whereClause = "";
			List<object> parameters = new List<object>();
			List<IField> pkFields = this.Fields.GetPkFields();
			pkFields.ForEach(x => {
				parameters.Add(x.PkParameterName);
				parameters.Add(x.RawValue);
				whereClause += (whereClause.IsNullOrEmpty() ? "" : " AND ") + x.FieldName + "=" + x.PkParameterName;
			});

			SqlHelper.ExecuteSql(@"DELETE FROM
									" + this.TABLE_NAME + @"								
								WHERE
                                    " + whereClause + @"
								", 
								parameters.ToArray()
							   );
		}
		public virtual void DeleteSoft<T>(SqlHelper SqlHelper, T AuthorId, string DeletedColumnName)
		{
			string whereClause = "";
			List<object> parameters = new List<object>();
			List<IField> pkFields = this.Fields.GetPkFields();
			pkFields.ForEach(x => {
				parameters.Add(x.PkParameterName);
				parameters.Add(x.RawValue);
				whereClause += (whereClause.IsNullOrEmpty() ? "" : " AND ") + x.FieldName + "=" + x.PkParameterName;
			});

			SqlHelper.ExecuteSql(@"UPDATE
									" + this.TABLE_NAME + @"
								SET
									" + (this.Fields.GetByFieldName("modified_date") == null ? "" : "modified_date=GETDATE(),") + @"	
									" + (this.Fields.GetByFieldName("ModifiedDate") == null ? "" : "ModifiedDate=GETDATE(),") + @"									
									" + DeletedColumnName + @"=1
								WHERE
                                    " + whereClause + @"
								", 
								parameters.ToArray()
								);
		}
		#endregion

		#region ***** INTERNAL METHODS *****
		protected bool IsOracle() 
		{
            return (this.Context.ConnectionType == jlib.db.SqlHelper.ConnectionTypes.Oracle);
	}
        private string GetEscapeStart() 
		{
            if (IsOracle()) return "\"";
            else return "[";
}
        private string GetEscapeEnd() 
		{
            if (IsOracle()) return "\"";
            else return "]";
        }
		#endregion
	}
}
