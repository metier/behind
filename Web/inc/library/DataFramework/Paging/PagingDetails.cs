﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace jlib.DataFramework.Paging
{
	[Serializable]
	public class PagingDetails
	{
		//Constructor
		public PagingDetails()
		{
			this.TotalRows = 0;
			this.PageCount = 0;
			this.PageSize = -1;
			this.PageIndex = 0;
		}

		//Properties
		public int TotalRows { get; set; }
		public int PageCount { get; set; }
		public int PageSize { get; set; }
		public int PageIndex { get; set; }
		
		//Static Methods
		public static PagingDetails UpdatePagingDetails(DataTable ResultsTable, PagingOptions PagingOptions)
		{
			//If no paging, then return null
			if (PagingOptions == null) return null;

			//Create New Object
			PagingDetails pagingDetails = new PagingDetails();
			pagingDetails.PageSize = PagingOptions.PageSize;
			pagingDetails.PageIndex = PagingOptions.PageIndex;
			pagingDetails.TotalRows = ResultsTable.Rows.Count > 0 ? jlib.functions.convert.cInt(ResultsTable.Rows[0]["Paging.TotalRows"]) : 0;
			pagingDetails.PageCount = pagingDetails.PageSize > 0 ? (int)Math.Ceiling((decimal)((decimal)pagingDetails.TotalRows / (decimal)pagingDetails.PageSize)) : 1;

			//Return
			return pagingDetails;

		}
	}
}
