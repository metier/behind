﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.OleDb;
using jlib.DataFramework.QueryBuilder;

namespace jlib.DataFramework.Paging
{
	public class PagingOptions
	{
		public int PageSize = -1;
		public int PageIndex = 0;
		public string SortField = null;
		public OrderByDirections SortDirection = OrderByDirections.Ascending;
        public PagingOptions() { }
        public PagingOptions(int PageSize, int PageIndex, string SortOrder) 
		{
            this.PageSize = PageSize;
            this.PageIndex = PageIndex;
            this.SortField = SortOrder;
        }
		public PagingOptions(int PageSize, int PageIndex, string SortOrder, OrderByDirections SortDirection) 
		{
            this.PageSize = PageSize;
            this.PageIndex = PageIndex;
            this.SortField = SortOrder;
			this.SortDirection = SortDirection;
        }
        public static void AddSqlParameters(jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> SqlParameters, PagingOptions Options, jlib.db.SqlHelper.ConnectionTypes ConnectionType) 
		{
            //If not specified, ignore
            if (Options == null) return;
            if (ConnectionType == db.SqlHelper.ConnectionTypes.Oracle) 
			{
                //Page Size
                if (Options.PageSize > 0) {
                    SqlParameters.Add("@XOpPageSize", new OleDbParameter("@XOpPageSize", Options.PageSize));
                    SqlParameters.Add("@XOpPageIndex", new OleDbParameter("@XOpPageIndex", Options.PageIndex));
                }

                //Sort Order
                if (!String.IsNullOrWhiteSpace(Options.SortField)) {
                    SqlParameters.Add("@XOpSortOrder", new OleDbParameter("@XOpSortOrder", Options.SortField));
                }
            } 
			
			else 
			{
                //Page Size
                if (Options.PageSize > 0) {
                    SqlParameters.Add("@XOpPageSize", new SqlParameter("@XOpPageSize", Options.PageSize));
                    SqlParameters.Add("@XOpPageIndex", new SqlParameter("@XOpPageIndex", Options.PageIndex));
                }

                //Sort Order
                if (!String.IsNullOrWhiteSpace(Options.SortField)) {
                    SqlParameters.Add("@XOpSortOrder", new SqlParameter("@XOpSortOrder", Options.SortField));
                }
            }
        }
	}
}
