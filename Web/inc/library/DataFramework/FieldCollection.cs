﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using jlib.helpers.structures;

namespace jlib.DataFramework
{
	public class FieldCollection : OrderedDictionary<string, IField> {
        
        #region ***** CONSTRUCTORS *****
        public FieldCollection(BaseContext oContext, IRecord Record) {
            this.Context = oContext;
            this.Record = Record;
        }
        #endregion
		
		#region ***** PROPERTIES *****
		public BaseContext Context { get; private set; }
        public IRecord Record { get; private set; }
		#endregion
        

        #region ***** METHODS *****

		[Obsolete]
        public DbParameter[] GetPrimaryKeySqlParameters()
		{
			return this.GetSqlParameters(true);
		}

		[Obsolete]
        public DbParameter[] GetSaveSqlParameters()
		{
			return this.GetSqlParameters(false);
		}

		[Obsolete]
        public DbParameter[] GetSqlParameters(bool PKsOnly)
		{
			//Create Parameter List
            List<DbParameter> sqlParameters = new List<DbParameter>();
			
			//If Oracle
            if (this.Context.ConnectionType == db.SqlHelper.ConnectionTypes.Oracle) 
			{
                //Oracle requires that we supply all parameters -- whether or not they have been set
                for (int x = 0; x < this.Values.Count; x++) 
				{
                    if (!this[x].FieldName.StartsWith("Paging.") && (!PKsOnly || this[x].IsPrimaryKey)) 
					{
                        System.Data.OleDb.OleDbParameter sqlParameter = new System.Data.OleDb.OleDbParameter() 
						{
                            ParameterName = (PKsOnly ? this[x].ParameterName.Substring(0, 1) + "PK_" + this[x].ParameterName.Substring(1) : this[x].ParameterName),
                            Value = ((PKsOnly && this.Record.RecordState== DataRowAction.Add) || this[x].FieldStatus == FieldStatuses.NotSet) ? null : this[x].GetSqlValue(this.Context.ConnectionType, PKsOnly),
                            Direction = !PKsOnly && this[x].IsPrimaryKey ? ParameterDirection.InputOutput : ParameterDirection.Input,
                            DbType = this[x].DBType
                        };
                        //Add Parameter
                        sqlParameters.Add(sqlParameter);
                    }
                }

            }
			
			//Else, SQL
			else
			{

                IEnumerable<IField> oFields = (PKsOnly ? this.Values.Where(field => field.IsPrimaryKey) : this.Values.Where(field => field.FieldStatus == FieldStatuses.Modified && !field.FieldName.StartsWith("Paging.")));

                foreach (IField field in oFields) {
                    DbParameter sqlParameter = new SqlParameter() {
                        ParameterName = (PKsOnly ? field.ParameterName.Substring(0, 1) + "PK_" + field.ParameterName.Substring(1) : field.ParameterName),
                        Value = field.FieldStatus == FieldStatuses.NotSet ? null : field.GetSqlValue(this.Context.ConnectionType, PKsOnly),
                        Direction = !PKsOnly && field.IsPrimaryKey ? ParameterDirection.InputOutput : ParameterDirection.Input,
                        DbType = field.DBType

                    };
                    //Add Parameter
                    sqlParameters.Add(sqlParameter);
                }                
            }

			//Convert TO Array
			return sqlParameters.ToArray();
		}

		public List<IField> GetChangedFields()
		{
			return this.Values.Where(field => field.FieldStatus == FieldStatuses.Modified && !field.FieldName.StartsWith("Paging.")).ToList();
		}

		public List<IField> GetPkFields()
		{
			return this.Values.Where(field => field.IsPrimaryKey).ToList();
		}

		public bool SetFieldLoadedValueByPropertyName(string PropertyName, object Value)
		{
			
			//If no matching fields
			if (!this.ContainsKey(PropertyName)) return false;

			//Locate Field
			IField matchingField = this[PropertyName];

			//Update Field Value
			matchingField.SetLoadedValue(Value);
			
			//Return Successful
			return true;
		}

		public bool SetFieldChangedValueByPropertyName(string PropertyName, object Value)
		{

			//If no matching fields
			if (!this.ContainsKey(PropertyName)) return false;

			//Locate Field
			IField matchingField = this[PropertyName];

			//Update Field Value
			matchingField.SetChangedValue(Value);

			//Return Successful
			return true;
		}

		public bool SetFieldLoadedValueByFieldName(string FieldName, object Value)
		{
			
			//Locate Field
			IField matchingField = this.GetByFieldName(FieldName);

			//If no matching fields
			if (matchingField == null) return false;

			//If DateTime and Date is 1/1/1900 or less, then set DateTime.Min
			if (matchingField.DBType == DbType.Date || matchingField.DBType == DbType.DateTime || matchingField.DBType == DbType.DateTime2)
			{
				if (Value != null && (DateTime)Value <= DateTime.Parse("1/1/1900 00:00:00"))
				{
					Value = DateTime.MinValue;
				}
			}

			//Update Field Value
			matchingField.SetLoadedValue(Value);

			//Return Successful
			return true;
		}


		public object GetFieldValueByPropertyName(string PropertyName)
		{
			//If no matching fields
			if (!this.ContainsKey(PropertyName)) return null;


			//Locate Field
			IField matchingField = this[PropertyName];

			
			//Return Field Value
			return matchingField.RawValue;

		}

		public object GetFieldValueByFieldName(string FieldName)
		{

			//Locate Field
			IField matchingField = this.GetByFieldName(FieldName);

			//If no matching fields
			if (matchingField == null) return null;

			//Return Field Value
			return matchingField.RawValue;

		}

		public bool ContainsFieldName(string FieldName)
		{
			return this.Any(x => x.Value.FieldName.Equals(FieldName, StringComparison.CurrentCultureIgnoreCase));
		}

		public IField GetByFieldName(string FieldName)
		{
			IField field = this.Where(x => x.Value.FieldName.Equals(FieldName, StringComparison.CurrentCultureIgnoreCase)).Select(x => x.Value).FirstOrDefault();

			//else, return value
			return field;
		}

        public bool ContainsPropertyName(string PropertyName) {
            return GetByPropertyName(PropertyName) != null;
        }

        public IField GetByPropertyName(string PropertyName)
		{
            if (this.ContainsKey(PropertyName)) return this[PropertyName];
            IField field = this.Where(x => x.Value.PropertyName.Equals(PropertyName, StringComparison.CurrentCultureIgnoreCase)).Select(x => x.Value).FirstOrDefault();

            return field;
		}
		
		public void CopyValuesToNewCollection(FieldCollection NewFields, bool IncludePrimaryKeys)
		{
			NewFields.Values.Where(x=>IncludePrimaryKeys || !x.IsPrimaryKey).ToList().ForEach(newField =>
				{
					IField origField = this.GetByFieldName(newField.FieldName);
					if(origField != null) newField.SetChangedValue(origField.RawValue);
				});
		}

		#endregion
	}
}
