﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace jlib.DataFramework
{
	public abstract class BaseViewRecord<ContextType> : BaseRecord<ContextType>
		where ContextType : BaseContext, new()
	{
		public BaseViewRecord(ContextType Context) : base(Context) { }

		#region ***** PROPERTIES *****
		public string VIEW_NAME 
		{
			get { return base.DbEntityName; }
			protected set { base.DbEntityName = value; }
		}
		#endregion

		#region ***** METHODS *****
		public static T LoadByPk<T>(jlib.db.SqlHelper SqlHelper, string ViewName, string PkField, object PkId, string SiteIdField = null, object SiteId = null)
			where T : BaseViewRecord<ContextType>, new()
		{
			//Execute Command
			DataTable dataTable = SqlHelper.ExecuteSql(@"
										SELECT
											TOP 1 *
										FROM
											[" + ViewName + @"] WITH (NOLOCK)
										WHERE
											
											[" + PkField + @"] = @PkId
											",
											"@PkId", PkId
											);

			//If Row returned
			if (dataTable.Rows.Count > 0)
			{
				//Create New Object
				var item = new T();
				item.Context = new ContextType();

				//Import
				item.Import(dataTable.Rows[0]);

				//Return Row
				return item;
			}

			//Else, Return null
			else
				return null;

		}
		#endregion
	}
}
