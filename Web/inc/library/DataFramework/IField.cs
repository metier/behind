﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;

namespace jlib.DataFramework
{
	public interface IField : IColumn
	{
		//Properties
		object RawValue { get; }
		object OriginalRawValue { get; }
        FieldStatuses FieldStatus { get; set; }
        Type ObjectType { get; }

		//Methods
		object GetSqlValue(db.SqlHelper.ConnectionTypes ConnectionType, bool UseOriginalValue);
		void SetLoadedValue(object Value);
		void SetChangedValue(object Value);
		void ResetValue();
		DbParameter BuildSqlParameter(db.SqlHelper.ConnectionTypes ConnectionType, bool UseOriginalValue);
	}
}
