﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using jlib.db;
using System.Dynamic;
using jlib.DataFramework.Cache;
using jlib.DataFramework.Paging;
using jlib.DataFramework.Cache.Attributes;
using jlib.DataFramework.QueryBuilder;
using jlib.functions;

namespace jlib.DataFramework
{
	public abstract class BaseTableCollection <CollectionType, RecordType, ContextType> : BaseCollection<RecordType, ContextType>, IUpdatable 
		where CollectionType : BaseTableCollection<CollectionType, RecordType, ContextType>, ICollection, new() 
		where RecordType : BaseTableRecord<ContextType>, IRecord, new() 
		where ContextType : BaseContext, new()
	{

		#region ***** CONSTRUCTORS *****
		public BaseTableCollection(ContextType Context) : base(Context)
		{
			
		}
		#endregion

		#region ***** PROPERTIES *****
		public string TABLE_NAME 
		{
			get { return base.DbEntityName; }
			protected set { base.DbEntityName = value; }
		}
		#endregion

		#region ***** STATIC LOAD BY COMMAND METHODS *****
		protected static QueryPackage GetAll(string Statement, CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//Build Package
			QueryPackage query = new QueryPackage();            
			query.Statement = Statement;
			query.IsStoredProcedure = false;
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}
		public static CollectionType LoadByCommand(ContextType Context, IQueryPackage Command)
		{
			return BaseCollection<RecordType, ContextType>.LoadByCommand<CollectionType, QueryPackage>(Context, Command);
		}

		public static void DeleteByCommand(ContextType Context, IQueryPackage Command)
		{
			BaseCollection<RecordType, ContextType>.DeleteByCommand<CollectionType, QueryPackage>(Context, Command);
		}

		#endregion

		#region ***** STATIC DELETE METHODS *****
		protected static void DeleteAll(string TableName)
		{
			SqlHelper sqlHelper = (new ContextType()).GetCurrentTransactionOrCreate();

			//Get Results from SQL Server
			sqlHelper.Execute(String.Format("DELETE FROM [{0}]", TableName));
		}

        public static void DeleteByPk(string TableName, int id)
        {
            SqlHelper sqlHelper = (new ContextType()).GetCurrentTransactionOrCreate();

            //Get Results from SQL Server
            sqlHelper.Execute(String.Format("DELETE FROM [{0}] WHERE ID = {1}", TableName, id));

        }

		public static void DeleteByPks<T>(string TableName, string PkName, List<T> Pks)
        {
			if (Pks.Count == 0) return;

            SqlHelper sqlHelper = (new ContextType()).GetCurrentTransactionOrCreate();

            //Get Results from SQL Server
            sqlHelper.Execute(String.Format("DELETE FROM [{0}] WHERE [{1}] IN ({2})", TableName, PkName, String.Join(",", Pks.Select(x=>String.Format("'{0}'", x)))));

        }
		#endregion

		#region ***** METHODS *****
		public virtual void Save() { this.Save(null, 0, null); }
        public virtual void Save(bool ReloadDBValues) { this.Save(null, 0, new SaveOptions() {ReloadValuesFromDb = true}); }
		public virtual void Save(SaveOptions Options) { this.Save(null, 0, Options); }
		public virtual void Save(int AuthorId) { this.Save(null, AuthorId, null); }
		public virtual void Save(Guid AuthorId) { this.Save(null, AuthorId, null); }
        public virtual void Save(SqlHelper SqlHelper)  { this.Save(SqlHelper, 0, null); }
        public virtual void Save(SqlHelper SqlHelper, int AuthorId) { this.Save(SqlHelper, AuthorId, null); }
		public virtual void Save(SqlHelper SqlHelper, Guid AuthorId) { this.Save(SqlHelper, AuthorId, null); }
		public virtual void Save(int AuthorId, SaveOptions Options) { this.Save(null, AuthorId, Options); }
		public virtual void Save(Guid AuthorId, SaveOptions Options) { this.Save(null, AuthorId, Options); }
		public virtual void Save(SqlHelper SqlHelper, SaveOptions Options) { this.Save(SqlHelper, 0, Options); }
		public virtual void Save(SqlHelper SqlHelper, int AuthorId, SaveOptions Options) { this.Save<int>(SqlHelper, AuthorId, Options); }
		public virtual void Save(SqlHelper SqlHelper, Guid AuthorId, SaveOptions Options) { this.Save<Guid>(SqlHelper, AuthorId, Options); }
		public virtual void Save<T>(SqlHelper SqlHelper, T AuthorId, SaveOptions Options)
		{
			//If no records to batch commit, then exit
			if (this.Count == 0)
				return;

			//Map Sql
			SqlHelper sqlHelper = SqlHelper;

			//Check if local transaction
			if (sqlHelper == null) sqlHelper = this.Context.GetCurrentTransaction();

			//If Transaction not declared, then declare
			bool useLocalConnection = (sqlHelper == null);

			//Connect
			if (useLocalConnection)
			{
				sqlHelper = this.Context.GetSqlHelper();
				sqlHelper.Connect();
			}

			//Loop through collection calling save
			foreach (RecordType record in this)
			{
                record.Save(sqlHelper, AuthorId, Options);
			}

			//Close Connection
			if (useLocalConnection) sqlHelper.Disconnect();
		}




		public virtual int DtsImportNewRecords()
		{
			return this.DtsImportNewRecords((new ContextType()).GetCurrentTransactionOrCreate(), null);
		}

		public virtual int DtsImportNewRecords(Action<object, SqlRowsCopiedEventArgs> fUpdateProgress)
		{
			return this.DtsImportNewRecords((new ContextType()).GetCurrentTransactionOrCreate(), fUpdateProgress);
		}

        public virtual int DtsImportNewRecords(SqlHelper SqlHelper, Action<object, SqlRowsCopiedEventArgs> fUpdateProgress) {
            //Create Connection
            SqlConnection sqlConnection = new SqlConnection(SqlHelper.SqlConnectionBuilder.BuildConnectionString());
            sqlConnection.Open();

            //Cleanup Memory
            //GC.Collect();

            //Get Data Reader
            IDataReader dataReader = this.GetDataReader();

            //Set File Total
            int rowsInserted = dataReader.RecordsAffected;

            // new method: SQLBulkCopy:
            using (SqlBulkCopy s = new SqlBulkCopy(sqlConnection)) {
                //Notification
                if (fUpdateProgress != null) {
                    s.SqlRowsCopied += new SqlRowsCopiedEventHandler(fUpdateProgress);
                    s.NotifyAfter = 10000;
                }

                //Destination
                s.DestinationTableName = this.TABLE_NAME;

                //Timeout
                s.BulkCopyTimeout = 60 * 30; // 30 minutes

                //Batch Size
                s.BatchSize = 10000;

                //Column Mappings
                s.ColumnMappings.Clear();
                for (int i = 0; i < dataReader.FieldCount; i++) {
                    //Get Field Name
                    string fieldName = dataReader.GetName(i);

                    //Check if mapping is allowed on column
                    bool allowMapping = true;
                    if (dataReader is BaseCollectionDataReader<CollectionType, RecordType, ContextType>) {
                        allowMapping = (dataReader as BaseCollectionDataReader<CollectionType, RecordType, ContextType>).CanBulkInsert(fieldName);
                    }

                    //Add to mapping collection
                    if (allowMapping) s.ColumnMappings.Add(fieldName, fieldName);
                }

                //Write Data
                s.WriteToServer(dataReader);
                s.Close();
            }

            //Close Connection
            sqlConnection.Close();

            //Return TotalRows
            return rowsInserted;

        }

        private string GetCreateTempTableSQL(string tableName, jlib.DataFramework.BaseCollectionDataReaderBulk<CollectionType, RecordType, ContextType> reader) {
            string sql=@"CREATE TABLE [" + tableName + @"] (";
            List<string> colDefs = new List<string>();
            for(int x=0;x< reader.ColumnMapping.Count;x++){
                var type = SqlHelper.DataMapping.GetSQLColumnType(reader.GetFieldType(x));
                colDefs.Add("[" + reader.GetName(x) + "] " + type + (type=="varchar" || type=="nvarchar" ? "(max)":""));
            }
//                [EntitySimpleID] INT NULL,
//[ColumnInt] INT NULL,
//[Child_ColumnInt] INT NULL,
//[cc0b8a88_db91_404b_b7a8_0d980af7448b] INT NOT NULL,
//CONSTRAINT [PK_cc0b8a88_db91_404b_b7a8_0d980af7448b] PRIMARY KEY CLUSTERED ( [cc0b8a88_db91_404b_b7a8_0d980af7448b] ))"

            return sql + jlib.functions.extensions.Join(colDefs,",") + ")";


        }
        public virtual int DtsDelete(SqlHelper SqlHelper, Action<object, SqlRowsCopiedEventArgs> fUpdateProgress) {
            return DtsSave(SqlHelper, fUpdateProgress, DataRowAction.Delete);
        }
        public virtual int DtsAdd(SqlHelper SqlHelper, Action<object, SqlRowsCopiedEventArgs> fUpdateProgress) {
            return DtsSave(SqlHelper, fUpdateProgress, DataRowAction.Add);
        }
        public virtual int DtsUpdate(SqlHelper SqlHelper, Action<object, SqlRowsCopiedEventArgs> fUpdateProgress) {
            return DtsSave(SqlHelper, fUpdateProgress, DataRowAction.Change);
        }
        public virtual int DtsSave(SqlHelper SqlHelper, Action<object, SqlRowsCopiedEventArgs> fUpdateProgress) {
            return DtsSave(SqlHelper, fUpdateProgress, DataRowAction.Add | DataRowAction.Delete | DataRowAction.Change);
        }
        
        //WARNING: A known limitation: Updating a table where the PK changes will not work
        public virtual int DtsSave() 
		{
            return this.DtsSave((new ContextType()).GetCurrentTransactionOrCreate(), null, DataRowAction.Add | DataRowAction.Delete | DataRowAction.Change);
        }

        //TODO: Add optional revision-handling
        public override int DtsSave(SqlHelper SqlHelper, Action<object, SqlRowsCopiedEventArgs> fUpdateProgress, DataRowAction statesToProcess)
		{
            CollectionType collection=new CollectionType();
            collection.Context=this.Context;         
            List<DataRowAction> currentStates = new List<DataRowAction>();
            //int numModifiedRecords=0;

            foreach(var record in this){
                //if(record.RecordState!= DataRowAction.Nothing) numModifiedRecords++;
                if (record.RecordState!= DataRowAction.Nothing && statesToProcess.HasFlag(record.RecordState)) {
                    collection.Add(record);
                    currentStates.AddIfNotExist(record.RecordState);
                }
            }
            if(collection.Count==0)return 0;
            
            //Should we fall back to regular save? There is a cost involved with BulkInsert
            if (collection.Count < 2) {
                this.Save(SqlHelper);
                return this.Count;
            }

            //If insert-only, skip temp table? Nope, because we want the ID back
            //if(statesToProcess == DataRowAction.Add || (statesToProcess.HasFlag(DataRowAction.Add) && numModifiedRecords == collection.Count)) return collection.DtsImportNewRecords(SqlHelper,fUpdateProgress);            

            //Get list of changed fields
            Dictionary<string, IField> changedFields=new Dictionary<string, IField>();
            List<string> pkfields=new List<string>();
            
            //Make sure we send the PKs -- these might not have changed and therefore need to be added
            collection[0].Fields.GetPkFields().ForEach(x=>{ 
                changedFields.AddIfNotExist(x.FieldName, x); 
                pkfields.AddIfNotExist(x.FieldName); 
            });         

            foreach(var record in collection)
                if(record.RecordState == DataRowAction.Add || record.RecordState == DataRowAction.Change){
                    record.Fields.GetChangedFields().ForEach(x=>{ 
                        changedFields.AddIfNotExist(x.FieldName, x);
                        if(record.RecordState == DataRowAction.Change && pkfields.Contains(x.FieldName)) throw new Exception("Can't bulk process an update where the PK is changing.");
                    });                                                                                                                          
                }
                        
            bool useLocalConnection=false;
			//Create Connection
            if (SqlHelper.DbConnection == null)
			{
				useLocalConnection = true;
				SqlHelper.Connect();
			}
            
            SqlConnection sqlConnection = SqlHelper.DbConnection as SqlConnection;
			            
			//Cleanup Memory
			//GC.Collect();

			//Get Data Reader
            var dataReader = new jlib.DataFramework.BaseCollectionDataReaderBulk<CollectionType, RecordType, ContextType>(collection);
            dataReader.SetColumnsToLoad(changedFields);                			

            string tempTableName = "#" + Guid.NewGuid().ToString().Replace('-','_');

            //create temp table
            SqlHelper.Execute(GetCreateTempTableSQL(tempTableName, dataReader));

			// new method: SQLBulkCopy:
            using (SqlBulkCopy s = new SqlBulkCopy(sqlConnection, SqlBulkCopyOptions.Default, SqlHelper.DbTransaction as SqlTransaction))
			{
				//Notification
				if (fUpdateProgress != null)
				{
					s.SqlRowsCopied += new SqlRowsCopiedEventHandler(fUpdateProgress);
					s.NotifyAfter = 10000;
				}

				//Destination
                s.DestinationTableName = "tempdb.."+tempTableName;

				//Timeout
				s.BulkCopyTimeout = 60*30; // 30 minutes

				//Batch Size
				s.BatchSize = 10000; 
				
				//Column Mappings
				s.ColumnMappings.Clear();
				for (int i = 0; i < dataReader.FieldCount; i++)				
                    s.ColumnMappings.Add(dataReader.GetName(i), dataReader.GetName(i));				
				
				//Write Data
				s.WriteToServer(dataReader);
				s.Close();
			}

            //Don't use a single large merge, since it has some bugs -- http://www.mssqltips.com/sqlservertip/3074/use-caution-with-sql-servers-merge-statement/                        
            string mergeSql = "";
            string pkWhere = "";
            pkfields.ForEach(pk=>{
                pkWhere += (pkWhere == "" ? "" : " AND ") + "TARGET.[" + pk + "] = SOURCE.[" + pk + "]";
            });
            string insertSource = "", insertTarget = "", updateSet = "";
            dataReader.ColumnMapping.ToList().ForEach(x => {
                if (x.Value > -1) {
                    //Exclude column from  insert if PK and AutoNumber                        
                    if (!pkfields.Contains(x.Key) || !collection[0].Fields.GetByFieldName(x.Key).IsIdentity) {
                        insertSource += (insertSource == "" ? "" : ",") + "SOURCE.[" + x.Key + "]";
                        insertTarget += (insertTarget == "" ? "" : ",") + "[" + x.Key + "]";
                        updateSet += (updateSet == "" ? "" : ",") + "TARGET.[" + x.Key + "] = SOURCE.[" + x.Key + "]";
                    }
                }
            });
            if (currentStates.Contains(DataRowAction.Add)) {                                
                //get output columns
                mergeSql += @"MERGE INTO " + this.TABLE_NAME + @" as TARGET
                    USING " + tempTableName + @" as SOURCE ON
                        1=0 --Never match
                    WHEN NOT MATCHED AND SOURCE.__ChangeType=" + (int)DataRowAction.Add + @" THEN
                        INSERT
                        (" + insertTarget + @")
                        VALUES
                        (" + insertSource + @")
                    OUTPUT
                        SOURCE.__Index,
                        INSERTED." + pkfields.Join(", INSERTED.") + ";";
            }

            if (currentStates.Contains(DataRowAction.Change)) {
                mergeSql += "\nUPDATE TARGET SET " + updateSet + " FROM " + this.TABLE_NAME + " TARGET JOIN " + tempTableName + " SOURCE ON __ChangeType=" + (int)DataRowAction.Change + " AND " + pkWhere + ";";
            }

            if (currentStates.Contains(DataRowAction.Delete)) {
                mergeSql += "\nDELETE TARGET FROM " + this.TABLE_NAME + " TARGET INNER JOIN " + tempTableName + " SOURCE ON __ChangeType=" + (int)DataRowAction.Delete + " AND " + pkWhere + ";";
            }
            mergeSql += "\nDROP TABLE " + tempTableName + ";";
            
            DataTable updatedRows = SqlHelper.Execute(mergeSql);
            
            //Close Connection			
            if (useLocalConnection) SqlHelper.Disconnect();

            if (currentStates.Contains(DataRowAction.Add)) {
                foreach (DataRow row in updatedRows.Rows) {
                    foreach (string pk in pkfields)
                        collection[row["__Index"].Int()].Fields.SetFieldLoadedValueByFieldName(pk, row[pk]);
                }
            }
            //Set as Updated
            foreach (var record in collection) {
                if (record.RecordState == DataRowAction.Delete) {                    
                    this.Remove(record);
                } else {
                    record.Fields.ToList().ForEach(x => x.Value.SetLoadedValue(x.Value.RawValue));
                    record.RecordState = DataRowAction.Nothing;
                }
            }
			
			//Return TotalRows
            return dataReader.RecordsAffected;
		}

		public virtual void Delete() { this.Delete(null); }
        public virtual void Delete(SqlHelper SqlHelper)  { this.Delete(SqlHelper, 0); }
		public virtual void Delete(int AuthorId) { this.Delete<int>(AuthorId); }
		public virtual void Delete(Guid AuthorId) { this.Delete<Guid>(AuthorId); }
		protected virtual void Delete<T>(T AuthorId) { this.Delete(null, AuthorId); }
		public virtual void Delete(SqlHelper SqlHelper, Guid AuthorId) { this.Delete<Guid>(SqlHelper, AuthorId); }
		public virtual void Delete(SqlHelper SqlHelper, int AuthorId) { this.Delete<int>(SqlHelper, AuthorId); }
        public virtual void Delete<T>(SqlHelper SqlHelper, T AuthorId)
        {
            _Delete<T>(SqlHelper, AuthorId, false);
        }
        public virtual void DeletePermanent<T>(T AuthorId)
        {
            DeletePermanent<T>(null, AuthorId);
        }
        public virtual void DeletePermanent<T>(SqlHelper SqlHelper, T AuthorId)
        {
            _Delete<T>(SqlHelper, AuthorId, true);
        }
        private void _Delete<T>(SqlHelper SqlHelper, T AuthorId, bool Permanent)
		{
			//If no records to batch commit, then exit
			if (this.Count == 0)
				return;

			//Map Sql
			SqlHelper sqlHelper = SqlHelper;

			//If Transaction not declared, then declare
			bool useLocalConnection = (sqlHelper == null);

			//Connect
			if (useLocalConnection)
			{
				sqlHelper = this.Context.GetSqlHelper();
				sqlHelper.Connect();
			}

			//Loop through collection calling delete
			foreach (RecordType record in this)
			{
                if(Permanent)
                    record.DeletePermanent(sqlHelper, AuthorId);
                else
                    record.Delete(sqlHelper, AuthorId);
			}

			//Close Connection
			if (useLocalConnection) sqlHelper.Disconnect();
		}

		#endregion
		
		#region ***** CLASS: QUERYPACKAGE *****
		public class QueryPackage : jlib.DataFramework.BaseQueryPackage<CollectionType>, IQueryPackage
		{
			
			public CollectionType Select(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
			{
				if (CacheOptions != null) this.CacheOptions = CacheOptions;
				if (PagingOptions != null) this.PagingOptions = PagingOptions;

			    return LoadByCommand(new ContextType(), this);
			}
			public CollectionType Execute(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null) { return this.Select(CacheOptions, PagingOptions); }
			public override CollectionType Execute() { return this.Select(); }
			public override CollectionType Select() { return this.Select(); }
			public override void Delete() { DeleteByCommand(new ContextType(), this); }

			ICollection IQueryPackage.Execute() { return this.Execute(); }
			ICollection IQueryPackage.Select() { return this.Execute(); }
			void IQueryPackage.Delete() { this.Delete(); }


			public override QueryBuilder.QueryBuilder ToQueryBuilder() { return base.ToQueryBuilder<QueryPackage, ContextType>(); }
			public override ColumnCollection Columns {  get { return new CollectionType().Columns; } }


			//WHERE
			public QueryPackage Where(QueryBuilder.BaseCondition Condition)
			{
				return base.Where<QueryPackage>(Condition);
			}

			IQueryPackage IQueryPackage.Where(QueryBuilder.BaseCondition Condition)
			{
				return this.Where(Condition);
			}

			public QueryPackage Where(Func<CollectionType, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
			{
				return this.Where(fnCondition(new CollectionType()));
			}

			IQueryPackage IQueryPackage.Where(Func<ICollection, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
			{
				return this.Where(fnCondition(new CollectionType()));
			}

			//AND
			public QueryPackage And(QueryBuilder.BaseCondition Condition)
			{
				return base.And<QueryPackage>(Condition);
			}
			IQueryPackage IQueryPackage.And(QueryBuilder.BaseCondition Condition)
			{
				return this.And(Condition);
			}

			public QueryPackage And(Func<CollectionType, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
			{
				return this.And(fnCondition(new CollectionType()));
			}

			IQueryPackage IQueryPackage.And(Func<ICollection, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
			{
				return this.And(fnCondition(new CollectionType()));
			}

			//OR
			public QueryPackage Or(QueryBuilder.BaseCondition Condition)
			{
				return base.Or<QueryPackage>(Condition);
			}
			IQueryPackage IQueryPackage.Or(QueryBuilder.BaseCondition Condition)
			{
				return this.Or(Condition);
			}

			public QueryPackage Or(Func<CollectionType, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
			{
				return this.Or(fnCondition(new CollectionType()));
			}

			IQueryPackage IQueryPackage.Or(Func<ICollection, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
			{
				return this.Or(fnCondition(new CollectionType()));
			}

			//ORDER BY
			public QueryPackage OrderBy(IColumn Column, QueryBuilder.OrderByDirections Direction)
			{
				return base.OrderBy<QueryPackage>(Column, Direction);
			}
			IQueryPackage IQueryPackage.OrderBy(IColumn Column, QueryBuilder.OrderByDirections Direction)
			{
				return this.OrderBy(Column, Direction);
			}
			public QueryPackage OrderBy(IColumn Column)
			{
				return this.OrderBy(Column, OrderByDirections.Ascending);
			}
			IQueryPackage IQueryPackage.OrderBy(IColumn Column)
			{
				return this.OrderBy(Column);
			}
			
			public QueryPackage OrderByAscending(IColumn Column)
			{
				return this.OrderBy(Column, QueryBuilder.OrderByDirections.Ascending);
			}
			IQueryPackage IQueryPackage.OrderByAscending(IColumn Column)
			{
				return this.OrderByAscending(Column);
			}
			public QueryPackage OrderByDescending(IColumn Column)
			{
				return this.OrderBy(Column, QueryBuilder.OrderByDirections.Descending);
			}
			IQueryPackage IQueryPackage.OrderByDescending(IColumn Column)
			{
				return this.OrderByDescending(Column);
			}

		}
		#endregion

	}
}
