﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using jlib.helpers.structures;

namespace jlib.DataFramework
{
	public class ColumnCollection : OrderedDictionary<string, IColumn>
	{

		#region ***** METHODS *****
        public bool ContainsDataField(string sFieldName) 
		{
			return this.Values.Any(x => x.FieldName.Equals(sFieldName, StringComparison.CurrentCultureIgnoreCase));
        }

		public IColumn GetByFieldName(string FieldName)
		{
			return this.Values.FirstOrDefault(x => x.FieldName.Equals(FieldName, StringComparison.CurrentCultureIgnoreCase));
		}
		public IColumn GetByColumnName(string ColumnName)
		{
			IColumn column = null;
			this.TryGetValue(ColumnName, out column);

			return column;
		}
		#endregion
	}
}
