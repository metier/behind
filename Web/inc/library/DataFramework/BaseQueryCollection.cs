﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jlib.DataFramework.Paging;
using jlib.db;
using System.Data.SqlClient;
using System.Data;
using jlib.DataFramework.Cache;
using jlib.DataFramework.Cache.Attributes;
using jlib.helpers.Structures;
using System.ComponentModel;

namespace jlib.DataFramework
{
	public abstract class BaseQueryCollection<CollectionType, RecordType, ContextType> : BaseCollection<RecordType, ContextType>
		where CollectionType : BaseQueryCollection<CollectionType, RecordType, ContextType>, new() 
		where RecordType : BaseQueryRecord<ContextType>, IRecord, new() 
		where ContextType : BaseContext, new()
	{
		
		#region ***** CONSTRUCTORS *****
		public BaseQueryCollection(ContextType Context, string Sql) : this(Context, Sql, new List<SqlParameter>()) {}
		public BaseQueryCollection(ContextType Context, string Sql, List<SqlParameter> SqlParameters) : base(Context)
		{
			this.Sql = Sql;
			this.SqlParameters = SqlParameters ?? new List<SqlParameter>();
		}
		#endregion


		#region ***** PROPERTIES *****
		public string Sql { get; set; }
		public List<SqlParameter> SqlParameters { get; set; }
		#endregion

		#region ***** METHODS *****
		public QueryPackage GetAll(CacheOptions CacheOptions = null, PagingOptions PagingOptions = null)
		{
			//If Cache is not supplied, load from previous method attribute
			CacheOptions = CacheOptions ?? CacheAttribute.GetCacheOptionsFromPreviousMethod();

			//Build Package
			QueryPackage query = this.GetBaseQueryPackage();
			query.CacheOptions = CacheOptions;
			query.PagingOptions = PagingOptions;

			//Return Query Package
			return query;

		}

		protected QueryPackage GetBaseQueryPackage()
		{
			QueryPackage query = new QueryPackage();
			
			//Set Sql
			query.IsStoredProcedure = false;
			query.Statement = this.Sql;
			
			//Set Parameters
			DynamicDictionary parameters = query.Parameters as DynamicDictionary;
			foreach (SqlParameter sqlParameter in this.SqlParameters)
			{
				parameters.Dictionary.Add(sqlParameter.ParameterName, sqlParameter.Value);
			}
			
			//Return Query Package
			return query;
		}
		#endregion

		#region ***** STATIC LOAD METHODS *****
		public static CollectionType LoadByCommand(ContextType Context, IQueryPackage Command)
		{
			return BaseCollection<RecordType, ContextType>.LoadByCommand<CollectionType, QueryPackage>(Context, Command);
		}
		#endregion

		#region ***** CLASS: QUERYPACKAGE *****
		public class QueryPackage : jlib.DataFramework.BaseQueryPackage<CollectionType>, IQueryPackage
		{
			public override CollectionType Select() { return LoadByCommand(new ContextType(), this); }
			public override CollectionType Execute() { return this.Select(); }
			public override void Delete() { throw new NotImplementedException(); }

			ICollection IQueryPackage.Execute() { return this.Select(); }
			ICollection IQueryPackage.Select() { return this.Select(); }
			void IQueryPackage.Delete() { throw new NotImplementedException(); }


			public override QueryBuilder.QueryBuilder ToQueryBuilder() { return base.ToQueryBuilder<QueryPackage, ContextType>(); }

			private ColumnCollection _Columns = null;
			public override ColumnCollection Columns 
			{ 
				get
				{
					if (this._Columns == null)
					{
						//Declare Columns Collection
						this._Columns = new ColumnCollection();

						//SqlHelper
						SqlHelper sqlHelper = new ContextType().GetSqlHelper();

						//Build Parameters Collection
                        jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> sqlParameters = sqlHelper.BuildParameterCollection(this.Parameters);

						//Execute
                        string sql = "";
                        if (sqlHelper.ConnectionType== SqlHelper.ConnectionTypes.Oracle)
                            sql = String.Format("SELECT * FROM ({0}) t WHERE ROWNUM=0", this.Statement);
                        else
                            sql =  String.Format("SELECT TOP 0 * FROM ({0}) AS t", this.Statement);

						DataTable dataTable = sqlHelper.Execute(sql, sqlParameters);

						//Get Columns
						foreach (DataColumn column in dataTable.Columns)
						{
							if (!this._Columns.ContainsKey(column.ColumnName))
							{
								this._Columns.Add(column.ColumnName, new BaseColumn<string>(FieldName: column.ColumnName, PropertyName: column.ColumnName, ParameterName: null, DBType: DbType.String, IsVirtualColumn: true));
							}
						}
					}

					return this._Columns;
				}
			}

			//WHERE
			public QueryPackage Where(QueryBuilder.BaseCondition Condition)
			{
				return base.Where<QueryPackage>(Condition);
			}

			IQueryPackage IQueryPackage.Where(QueryBuilder.BaseCondition Condition)
			{
				return this.Where(Condition);
			}
			IQueryPackage IQueryPackage.Where(Func<ICollection, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
			{
				return this.Where(fnCondition(new CollectionType()));
			}

			public QueryPackage Where(Func<CollectionType, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
			{
				return this.Where(fnCondition(new CollectionType()));
			}


			//AND
			public QueryPackage And(QueryBuilder.BaseCondition Condition)
			{
				return base.And<QueryPackage>(Condition);
			}
			IQueryPackage IQueryPackage.And(QueryBuilder.BaseCondition Condition)
			{
				return this.And(Condition);
			}

			public QueryPackage And(Func<CollectionType, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
			{
				return this.And(fnCondition(new CollectionType()));
			}

			IQueryPackage IQueryPackage.And(Func<ICollection, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
			{
				return this.And(fnCondition(new CollectionType()));
			}

			//OR
			public QueryPackage Or(QueryBuilder.BaseCondition Condition)
			{
				return base.Or<QueryPackage>(Condition);
			}
			IQueryPackage IQueryPackage.Or(QueryBuilder.BaseCondition Condition)
			{
				return this.Or(Condition);
			}

			public QueryPackage Or(Func<CollectionType, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
			{
				return this.Or(fnCondition(new CollectionType()));
			}

			IQueryPackage IQueryPackage.Or(Func<ICollection, jlib.DataFramework.QueryBuilder.BaseCondition> fnCondition)
			{
				return this.Or(fnCondition(new CollectionType()));
			}

			//ORDER BY
			public QueryPackage OrderBy(IColumn Column, QueryBuilder.OrderByDirections Direction)
			{
				return base.OrderBy<QueryPackage>(Column, Direction);
			}
			IQueryPackage IQueryPackage.OrderBy(IColumn Column, QueryBuilder.OrderByDirections Direction)
			{
				return this.OrderBy(Column, Direction);
			}
			public QueryPackage OrderBy(IColumn Column)
			{
				return this.OrderBy(Column, QueryBuilder.OrderByDirections.Ascending);
			}
			IQueryPackage IQueryPackage.OrderBy(IColumn Column)
			{
				return this.OrderBy(Column);
			}

			public QueryPackage OrderByAscending(IColumn Column)
			{
				return this.OrderBy(Column, QueryBuilder.OrderByDirections.Ascending);
			}
			IQueryPackage IQueryPackage.OrderByAscending(IColumn Column)
			{
				return this.OrderByAscending(Column);
			}
			public QueryPackage OrderByDescending(IColumn Column)
			{
				return this.OrderBy(Column, QueryBuilder.OrderByDirections.Descending);
			}
			IQueryPackage IQueryPackage.OrderByDescending(IColumn Column)
			{
				return this.OrderByDescending(Column);
			}


		}
		#endregion
	}
}
