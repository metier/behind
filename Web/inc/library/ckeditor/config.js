CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.fillEmptyBlocks = false;
    config.allowedContent = true;    
    config.extraPlugins += (config.extraPlugins == "" ? "" : ",") + 'simpleuploads,imagesfromword';    //imagepaste
    config.toolbar = 'Small';
    config.toolbar_Small =
    [
	['Source', 'Maximize'],
    //'Cut', 'Copy', 
	['PasteFromWord', 'Undo', 'Redo'],
    ['Bold', 'Italic', 'Underline', 'ShowBlocks'], ['JustifyLeft', 'JustifyCenter'], ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'], ['Table'],
	['Link', 'Unlink'], ['FontSize', 'TextColor']
];

    config.toolbar = 'Lexicon';
    config.toolbar_Lexicon =
    [
	['Source', 'Maximize'],
    //'Cut', 'Copy', 
	['PasteFromWord', 'Undo', 'Redo'],
    ['Bold', 'Italic', 'Underline', 'ShowBlocks', 'FontSize', 'TextColor'], ['JustifyLeft', 'JustifyCenter'], ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'], ['Table', 'Rule'],
	['Link', 'Unlink']
];

    config.toolbar = 'MetierContent';
    config.toolbar_MetierContent =
    [
	['Source', 'Maximize'],
    //'Cut', 'Copy', 
	['PasteText', 'PasteFromWord', 'Undo', 'Redo'],
    ['Bold', 'Italic', 'Underline', 'ShowBlocks', 'Format', 'RemoveFormat'],
    ['Find', 'Replace'],
    ['JustifyLeft', 'JustifyCenter'], ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'], ['Image'],
	['Link', 'Unlink', 'Anchor', 'Templates']
];


    config.toolbar_Full =
[
	{ name: 'document', items: ['Source', '-', 'Save', 'NewPage', 'DocProps', 'Preview', 'Print', '-', 'Templates'] },
	{ name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
	{ name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt'] },
	{ name: 'forms', items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton',
        'HiddenField']
	},
	'/',
	{ name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
	{ name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
	'-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']
	},
	{ name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
	{ name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'] },
	'/',
	{ name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
	{ name: 'colors', items: ['TextColor', 'BGColor'] },
	{ name: 'tools', items: ['Maximize', 'ShowBlocks', '-', 'About'] }
];

};


// Temporary workaround for providing editor 'read-only' toggling functionality.  

var cancelEvent = function (evt) {
    evt.cancel();
};

CKEDITOR.editor.prototype.readOnly = function (isReadOnly) {
    // Turn off contentEditable.
    this.document.$.body.disabled = isReadOnly;
    CKEDITOR.env.ie ? this.document.$.body.contentEditable = !isReadOnly
      : this.document.$.designMode = isReadOnly ? "off" : "on";

    // Prevent key handling.
    this[isReadOnly ? 'on' : 'removeListener']('key', cancelEvent, null, null, 0);
    this[isReadOnly ? 'on' : 'removeListener']('selectionChange', cancelEvent, null, null, 0);

    // Disable all commands in wysiwyg mode.
    var command,
         commands = this._.commands,
         mode = this.mode;

    for (var name in commands) {
        command = commands[name];
        isReadOnly ? command.disable() : command[command.modes[mode] ? 'enable' : 'disable']();
        this[isReadOnly ? 'on' : 'removeListener']('state', cancelEvent, null, null, 0);
    }
}