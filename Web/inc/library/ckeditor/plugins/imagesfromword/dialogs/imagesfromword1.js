﻿ /* globals CKEDITOR */

(function() {
	'use strict';

	CKEDITOR.dialog.add("imagesfromword", function (n) {
	    function o(n, e) {
	        if (i.length) {
	            f = i.shift().path;
	            var o = unescape(f).replace(/\//g, "\\"),
				s = e.getContentElement("info", "ruta");
	            s.setValue(o),
				s.select(),
				s.focus(),
				o = t.file + " ",
				1 < r && (u++, o += u + " " + t.of + " " + r),
				e.getContentElement("info", "ruta").setLabel(o)
	        } else
	            e.hide()
	    }
	    var t = n.lang.imagesfromword,
		e = n.lang.common.generalTab,
		i,
		u,
		r,
		f;
	    return {
	        title: t.title,
	        minWidth: 300,
	        minHeight: 80,
	        filebrowser: "uploadButton",
	        contents: [{
	            id: "info",
	            label: e,
	            title: e,
	            elements: [{
	                type: "html",
	                html: t.instructions
	            }, {
	                id: "ruta",
	                type: "text",
	                label: t.file,
	                onLoad: function () {
	                    var n = this.getInputElement(),
                        t = this.getDialog();
	                    n.on("click", function () {
	                        this.$.select()
	                    });
	                    n.on("copy", function () {
	                        t.getContentElement("info", "uploadButton").getInputElement().$.click()
	                    });
	                    n.$.readOnly = !0
	                }
	            }, {
	                id: "txtUrl",
	                type: "text",
	                hidden: "true",
	                onChange: function () {
	                    var r = this.getValue(),
                        t;
	                    if (r) {
	                        for (var e = this.getDialog(), u = n.document.$.getElementsByTagName("img"), i = 0; i < u.length; i++)
	                            t = u[i], t.getAttribute("data-cke-saved-src") == "file:///" + f && (t.setAttribute("data-cke-saved-src", r), t.setAttribute("src", r));
	                        this.setValue(""),
                            o(n, e)
	                    }
	                }
	            }, {
	                type: "file",
	                id: "upload",
	                label: n.lang.image.btnUpload,
	                style: "height:40px",
	                size: 38
	            }, {
	                type: "fileButton",
	                id: "uploadButton",
	                filebrowser: "info:txtUrl",
	                label: n.lang.image.btnUpload,
	                "for": ["info", "upload"],
	                requiresImage: !0
	            }
	            ]
	        }
	        ],
	        onShow: function () {
	            i = n.plugins.imagesfromword.queue,
				u = 0,
				r = i.length,
				o(n, this)
	        },
	        buttons: [CKEDITOR.dialog.cancelButton]
	    }
	});
})();