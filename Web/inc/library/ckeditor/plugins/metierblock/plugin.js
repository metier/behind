CKEDITOR.plugins.add( 'metierblock', {
    requires: 'widget',

    init: function (editor) {
        editor.ui.addButton('metierblock', { label: 'Metier Block', command: 'metierblock', icon: this.path + 'metierblock.png' });
        CKEDITOR.dialog.add('metierblock', this.path + 'dialogs/metierblock.js');
        editor.widgets.add( 'metierblock', {

            button: 'Create a metier block',
            dialog: 'metierblock',
            template:
                '<div class="metierblock">' +                    
					'<span class="metierblock-tags"></span>' +
                    '<div class="metierblock-content">Content...</div>' +
                '</div>',

            editables: {                
                content: {
                    selector: '.metierblock-content',
                    //allowedContent: 'h1 h2 h3 h4 h5 h6 h7 h8 h9 img table tr td th tbody thead p br ul ol li strong em u a[*](*)'
                }
            },

            allowedContent:
                'div(!metierblock); div(!metierblock-content); div(!metierblock-title)',

            requiredContent: 'div(metierblock)',

            upcast: function( element ) {
                return element.name == 'div' && element.hasClass( 'metierblock' );
            }
        } );
    }
} );