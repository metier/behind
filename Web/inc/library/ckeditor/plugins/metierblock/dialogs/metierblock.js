CKEDITOR.dialog.add( 'metierblock', function( editor ) {
    return {
        title: 'Edit Metier Block',
        minWidth: 400,
        minHeight: 200,
        contents: [
            {
                id: 'info',
                elements: [
                    {
                        id: 'background-color',
                        type: 'select',
                        label: 'Background color',
                        items: [
                            ['White', '#ffffff'],
                            ['Light Gray', '#dee2e5'],
                            ['Pistachio', '#dbeed9'],                            
                            ['Orange', '#f9bd8f'],
                            ['Green', '#e9feba'],
                            ['Blue', '#e0faff'],
                            ['Pink', '#ffd1ec'],
                            ['Yellow', '#fdfeba']
                        ],
                        setup: function (widget) {
                            function componentToHex(c) {
                                var hex = cInt(c).toString(16);
                                return hex.length == 1 ? "0" + hex : hex;
                            }

                            var s = $(widget.element.$).find(".metierblock-content").css("background-color");
                            if (s.indexOf("rgb") > -1) 
                                s = "#" + componentToHex(s.safeSplit("(", 1).safeSplit(",", 0)) + componentToHex(s.safeSplit(",", 1)) + componentToHex(s.safeSplit(",", 2).safeSplit(")", 0));
                            
                            this.setValue(s);
                        },
                        commit: function( widget ) {                            
                            $(widget.element.$).find(".metierblock-content").css("background-color", this.getValue());
                        }
                    },
                    {
                        id: 'border-width',
                        type: 'select',
                        label: 'Border width',
                        items: [
                            ['0', '0px'],
                            ['1', '1px'],
                            ['2', '2px'],
                            ['3', '3px'],
                            ['4', '4px']                            
                        ],
                        setup: function (widget) {                            
                            var s = $(widget.element.$).find(".metierblock-content").css("border-width");                            
                            this.setValue(cStr(s,"1px"));
                        },
                        commit: function (widget) {
                            $(widget.element.$).find(".metierblock-content").css("border-width", this.getValue());
                        }
                    },
                    {
                        id: 'tags',
                        type: 'text',
                        label: 'Content Tags',                        
                        setup: function (widget) {                            
                            var s = $(widget.element.$).find(".metierblock-tags").html();
                            this.setValue(s);
                        },
                        commit: function (widget) {
                            $(widget.element.$).find(".metierblock-tags").html(this.getValue());
                        }
                    }
					
                ]
            }
        ]
    };
} );