CKEDITOR.dialog.add('metierquestion', function (editor) {
    function componentToHex(c) {
        var hex = cInt(c).toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }
    return {
        title: 'Edit Metier Question',
        minWidth: 200,
        minHeight: 100,
        contents: [
            {
                id: 'info',
                elements: [
                    {
                        id: 'q-color',
                        type: 'select',
                        label: 'Question background color',
                        items: [
                             ['White', '#ffffff'],
                            ['Light Gray', '#dee2e5'],
                            ['Pistachio', '#dbeed9'],
                            ['Orange', '#f9bd8f'],
                            ['Green', '#e9feba'],
                            ['Blue', '#e0faff'],
                            ['Pink', '#ffd1ec'],
                            ['Yellow', '#fdfeba']
                        ],
                        setup: function (widget) {                            
                            var s = $(widget.element.$).find(".metierbox-content").css("background-color");
                            if (s.indexOf("rgb") > -1) 
                                s = "#" + componentToHex(s.safeSplit("(", 1).safeSplit(",", 0)) + componentToHex(s.safeSplit(",", 1)) + componentToHex(s.safeSplit(",", 2).safeSplit(")", 0));
                            
                            this.setValue(s);
                        },
                        commit: function( widget ) {                            
                            $(widget.element.$).find(".metierbox-content").css("background-color", this.getValue());
                        }
                    },
                    {
                        id: 'a-color',
                        type: 'select',
                        label: 'Answer background color',
                        items: [
                             ['White', '#ffffff'],
                            ['Light Gray', '#dee2e5'],
                            ['Pistachio', '#dbeed9'],
                            ['Orange', '#f9bd8f'],
                            ['Green', '#e9feba'],
                            ['Blue', '#e0faff'],
                            ['Pink', '#ffd1ec'],
                            ['Yellow', '#fdfeba']
                        ],
                        setup: function (widget) {
                            var s = $(widget.element.$).find(".metierbox-answer").css("background-color");
                            if (s.indexOf("rgb") > -1)
                                s = "#" + componentToHex(s.safeSplit("(", 1).safeSplit(",", 0)) + componentToHex(s.safeSplit(",", 1)) + componentToHex(s.safeSplit(",", 2).safeSplit(")", 0));

                            this.setValue(s);
                        },
                        commit: function (widget) {
                            $(widget.element.$).find(".metierbox-answer").css("background-color", this.getValue());
                        }
                    },
                     {
                         id: 'border-width',
                         type: 'select',
                         label: 'Border width',
                         items: [
                             ['0', '0px'],
                             ['1', '1px'],
                             ['2', '2px'],
                             ['3', '3px'],
                             ['4', '4px']
                         ],
                         setup: function (widget) {
                             var s = $(widget.element.$).find(".metierbox-content").css("border-width");
                             this.setValue(cStr(s, "1px"));
                         },
                         commit: function (widget) {
                             $(widget.element.$).find(".metierbox-content, .metierbox-answer").css("border-width", this.getValue());
                         }
                     }
                ]
            }
        ]
    };
} );