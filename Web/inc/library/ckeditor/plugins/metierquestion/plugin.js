CKEDITOR.plugins.add( 'metierquestion', {
    requires: 'widget',

    init: function (editor) {
        editor.ui.addButton('metierquestion', { label: 'Metier Question', command: 'metierquestion', icon: this.path + 'metierquestion.png' });
        CKEDITOR.dialog.add('metierquestion', this.path + 'dialogs/metierquestion.js');
        editor.widgets.add( 'metierquestion', {

            button: 'Create a metier question',
            dialog: 'metierquestion',
            template:
                '<div class="metierquestion">' +
                    '<div class="metierbox-title">Title</div>' +
                    '<div class="metierbox-content">Question...</div>' +
                    '<div class="metierbox-answer">Answer...</div>' +
                '</div>',

            editables: {
                title: {
                    selector: '.metierbox-title',
                    allowedContent: 'br strong em u a[*]'
                },
                content: {
                    selector: '.metierbox-content',
                    allowedContent: 'p br ul ol li strong em u a[*]'
                }
            },

            allowedContent:
                'div(!metierquestion); div(!metierbox-content); div(!metierbox-title); div(!metierbox-answer)',

            requiredContent: 'div(metierquestion)',

            upcast: function( element ) {
                return element.name == 'div' && element.hasClass( 'metierquestion' );
            }
        } );
    }
} );