CKEDITOR.dialog.add( 'metierheading', function( editor ) {
    return {
        title: 'Edit Metier Heading',
        minWidth: 200,
        minHeight: 100,
        contents: [
            {
                id: 'info',
                elements: [
                    {
                        id: 'style',
                        type: 'select',
                        label: 'Style',
                        items: [
                            ['Standard', 'standard'],
                            ['PMP', 'pmp']                            
                        ],
                        setup: function (widget) {
                            if ($(widget.element.$).hasClass("standard")) this.setValue("standard");
                            if ($(widget.element.$).hasClass("pmp")) this.setValue("pmp");                                                        
                        },
                        commit: function( widget ) {                            
                            $(widget.element.$).removeClass("standard pmp").addClass(this.getValue());
                        }
                    },
                     {
                         id: 'background-color',
                         type: 'select',
                         label: 'Background color',
                         items: [
                             ['White', '#ffffff'],
                             ['Light Gray', '#dee2e5'],
                             ['Pistachio', '#dbeed9'],
                             ['Orange', '#f9bd8f'],
                             ['Green', '#e9feba'],
                             ['Blue', '#e0faff'],
                             ['Pink', '#ffd1ec'],
                             ['Yellow', '#fdfeba'],
                             ['Light Blue', '#5b9bd5'],
                             ['Light Green', '#70ad47'],
                             ['Light Orange', '#ed7d31'],
                             ['Mustard', '#ffc000'],                         
                             ['PMP Green', '#64bc68'],
                             ['PMP Orange', '#ff874b'],
                             ['PMP Purple', '#9a68a6'],
                             ['PMP Grey', '#768994'],
                             ['PMP Light Brown', '#c0b38f'],
                         ],
                         setup: function (widget) {
                             function componentToHex(c) {
                                 var hex = cInt(c).toString(16);
                                 return hex.length == 1 ? "0" + hex : hex;
                             }

                             var s = $(widget.element.$).css("background-color");
                             if (s.indexOf("rgb") > -1)
                                 s = "#" + componentToHex(s.safeSplit("(", 1).safeSplit(",", 0)) + componentToHex(s.safeSplit(",", 1)) + componentToHex(s.safeSplit(",", 2).safeSplit(")", 0));

                             this.setValue(s);
                         },
                         commit: function (widget) {
                             $(widget.element.$).css("background-color", this.getValue());
                         }
                     },
                    {
                        id: 'icon',
                        type: 'text',
                        label: 'Icon',                        
                        setup: function (widget) {                            
                            var s = $(widget.element.$).find("img").prop("src");                            
                            this.setValue(s);
                        },
                        commit: function (widget) {
                            var imgContainer = $(widget.element.$).find(".metierheading-icon");
                            imgContainer.toggle(this.getValue() != "");
                            imgContainer.find("img").addClass("no-process icon").attr("src", this.getValue());
                            if (this.getValue() != "") {
                                var img = imgContainer.find("img");
                                if (img.length == 0) img = $('<img />').appendTo(imgContainer);                                
                            }
                        }
                    }
                ]
            }
        ]
    };
} );