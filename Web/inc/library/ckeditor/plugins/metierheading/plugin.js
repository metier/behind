CKEDITOR.plugins.add( 'metierheading', {
    requires: 'widget',

    init: function (editor) {
        editor.ui.addButton('metierheading', { label: 'Metier Heading', command: 'metierheading', icon: this.path + 'metierheading.png' });
        CKEDITOR.dialog.add('metierheading', this.path + 'dialogs/metierheading.js');
        editor.widgets.add( 'metierheading', {

            button: 'Create a Metier Heading',
            dialog: 'metierheading',
            template:
                '<div class="metierheading standard">' +
                    '<div class="metierheading-title"><h2>Title</h2></div>' +
                    '<div class="metierheading-icon"></div>' +
                '<div class="clearfix"></div></div>',

            editables: {
                title: {
                    selector: '.metierheading-title',
                    allowedContent: 'br strong em u h2 img a[*] (*) {*}'
                },
                image: {
                    selector: '.metierheading-icon',
                    allowedContent: 'br strong em u h2 img a[*] (*) {*}'
                }
            },

            allowedContent:
                'div(!metierheading); div(!metierheading-title)',

            requiredContent: 'div(metierheading)',

            upcast: function( element ) {
                return element.name == 'div' && element.hasClass( 'metierheading' );
            }
        } );
    }
} );