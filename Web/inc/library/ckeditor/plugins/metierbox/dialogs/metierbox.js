CKEDITOR.dialog.add( 'metierbox', function( editor ) {
    return {
        title: 'Choose Metierbox',
        minWidth: 200,
        minHeight: 100,
        contents: [
            {
               id: 'info',
               elements: [
                   {
                       id: 'style',
                       type: 'select',
                       label: 'Style',
                       items: [
                           ['Defenition', 'definition'],
                           ['Example', 'example'],
                           ['Other', 'other']
                       ],
                       setup: function (widget) {
                           if ($(widget.element.$).hasClass("definition")) this.setValue("definition");
                           if ($(widget.element.$).hasClass("example")) this.setValue("example");
                           if ($(widget.element.$).hasClass("other")) this.setValue("other");
                       },
                       commit: function( widget ) {                            
                           $(widget.element.$).removeClass("definition example other").addClass(this.getValue());
                       }
                   },
               ]
            }
        ]
    };
} );
