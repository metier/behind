CKEDITOR.plugins.add( 'metierbox', {
    requires: 'widget',

    init: function (editor) {
        editor.ui.addButton('metierbox', { label: 'Metier Box', command: 'metierbox', icon: this.path + 'metierbox.png' });
        CKEDITOR.dialog.add('metierbox', this.path + 'dialogs/metierbox.js');
        editor.widgets.add( 'metierbox', {

            button: 'Choose a metier box',
            dialog: 'metierbox',
            template:
                '<div class="metierbox definition">' +
                    '<div class="metierbox-title">Title</div>' +
                    '<div class="metierbox-content">Content...</div>' +
                '</div>',

            editables: {
                title: {
                    selector: '.metierbox-title',
                    allowedContent: 'br strong em u a[*]'
                },
                content: {
                    selector: '.metierbox-content',
                    allowedContent: 'img table tr td th tbody thead p br ul ol li strong em u a[*](*)'
                }
            },

            allowedContent:
                'div(!metierbox); div(!metierbox-content); div(!metierbox-title)',

            requiredContent: 'div(metierbox)',

            upcast: function( element ) {
                return element.name == 'div' && element.hasClass( 'metierbox' );
            }
        } );
    }
} );