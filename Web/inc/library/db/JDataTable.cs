﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace jlib.db
{
	public class JDataTable : DataTable
	{
		#region ***** CONSTRUCTORS ****
		public JDataTable() : base() {}
		public JDataTable(string tableName) : base(tableName) {}
		public JDataTable(string tableName, string tableNamespace) : base(tableName, tableNamespace) { }
		#endregion

		//private bool m_bInitialized = false, m_bSchemaInitialized = false;
		private string m_sDSN = "";
		private DataTable m_oSchemaTable = null;
		private System.Collections.Generic.List<DataColumn> m_oJPrimaryKey = new System.Collections.Generic.List<DataColumn>(), m_oJAutoNumberColumn = new System.Collections.Generic.List<DataColumn>(), m_oJIdentityColumn = new System.Collections.Generic.List<DataColumn>(), m_oJRequiredColumn = new System.Collections.Generic.List<DataColumn>();
		private System.Collections.Generic.Dictionary<DataColumn, string> m_oBaseTableName = new System.Collections.Generic.Dictionary<DataColumn, string>(), m_oBaseColumnName;
		public System.Collections.Generic.List<DataColumn> JPrimaryKey
		{
			get
			{
				return m_oJPrimaryKey;
			}
			set
			{
				m_oJPrimaryKey = value;
			}
		}
		public System.Collections.Generic.List<DataColumn> JAutoNumberColumn
		{
			get
			{
				return m_oJAutoNumberColumn;
			}
			set
			{
				m_oJAutoNumberColumn = value;
			}
		}
		public System.Collections.Generic.List<DataColumn> JIdentityColumn
		{
			get
			{
				return m_oJIdentityColumn;
			}
			set
			{
				m_oJIdentityColumn = value;
			}
		}
		public System.Collections.Generic.List<DataColumn> JRequiredColumn
		{
			get
			{
				return m_oJRequiredColumn;
			}
			set
			{
				m_oJRequiredColumn = value;
			}
		}
		public string DSN
		{
			get
			{
				return m_sDSN;
			}
			set
			{
				m_sDSN = value;
			}
		}
		public DataTable SchemaTable
		{
			get
			{
				return m_oSchemaTable;
			}
			set
			{
				m_oSchemaTable = value;
			}
		}
		public System.Collections.Generic.Dictionary<DataColumn, string> BaseTableName
		{
			get
			{
				return m_oBaseTableName;
			}
			set
			{
				m_oBaseTableName = value;
			}
		}
		public string getBaseColumnName(DataColumn oCol)
		{
			if (m_oBaseColumnName == null)
			{
				m_oBaseColumnName = new System.Collections.Generic.Dictionary<DataColumn, string>();
				DataColumn BaseColumnNameCol = SchemaTable.Columns["BaseColumnName"];
				for (int x = 0; x < Columns.Count; x++)
					m_oBaseColumnName.Add(Columns[x], (BaseColumnNameCol == null ? Columns[x].ColumnName : (string)SchemaTable.Rows[(int)Columns[x].ExtendedProperties["schema-position"]][BaseColumnNameCol]));
			}
			return m_oBaseColumnName[oCol];
		}

		public override DataTable Clone()
		{
			JDataTable oDT = base.Clone() as JDataTable;
			oDT.DSN = DSN;
			oDT.SchemaTable = SchemaTable;
			foreach (DataColumn oCol in JPrimaryKey) oDT.JPrimaryKey.Add(oDT.Columns[oCol.ColumnName]);
			foreach (DataColumn oCol in JAutoNumberColumn) oDT.JAutoNumberColumn.Add(oDT.Columns[oCol.ColumnName]);
			foreach (DataColumn oCol in JIdentityColumn) oDT.JIdentityColumn.Add(oDT.Columns[oCol.ColumnName]);
			foreach (DataColumn oCol in JRequiredColumn) oDT.JRequiredColumn.Add(oDT.Columns[oCol.ColumnName]);
			foreach (System.Collections.Generic.KeyValuePair<DataColumn, string> oPair in BaseTableName) oDT.BaseTableName.Add(oDT.Columns[oPair.Key.ColumnName], oPair.Value);
			return oDT;
		}
		//public void initializeSchema() {
		//    if (m_bSchemaInitialized) return;
		//    m_bSchemaInitialized = true;
		//    for (int x = 0; x < this.SchemaTable.Rows.Count; x++)
		//        if (convert.cBool(this.SchemaTable.Rows[x]["IsIdentity"])) {
		//            m_oJPrimaryKey.Add(this.Columns[x]);
		//            //this.Columns[x].AllowDBNull = true;
		//            //this.Columns[x].ReadOnly = false;
		//            //this.Columns[x].Unique = false;
		//        }
		//}

		protected override DataRow NewRowFromBuilder(DataRowBuilder builder) 
		{
			return new JDataRow(builder);
		}

		protected override Type GetRowType()
		{
			return typeof(JDataRow);
		}
	}
}
