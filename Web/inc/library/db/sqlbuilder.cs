using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;
using System.Xml;
using jlib.functions;

namespace jlib.db
{
	/// <summary>
	/// Summary description for sqlbuilder.
	/// </summary>
	public class sqlbuilder
	{
		#region ENUMERATION
		public enum QueryTypes
		{
			Select,
			Insert,
			Update,
			Delete,
			Truncate
		}
        public enum DataServerTypes {
            SQLServer,
            Oracle
        }
		#endregion

		#region DECLARATIONS
		private QueryTypes m_oQueryType		= QueryTypes.Select;
		private ArrayList m_oSelectColumns	= new ArrayList();
		private ArrayList m_oTables			= new ArrayList();
		private ArrayList m_oSelectOrder	= new ArrayList();
		private ArrayList m_oConditions		= new ArrayList();
		private ArrayList m_oUpdateExpressions	= new ArrayList();
		private ArrayList m_oInsertExpressions	= new ArrayList();
		private int m_iParameterIndex		= 1, m_iTop					= -1;
        //private static bool m_bDisableTriggers = false, m_bDisableTriggerContext = false;

        //private static ado_helper m_oData = null;
		#endregion

		#region CONSTRUCTORS
		public sqlbuilder()
		{

		}
		public sqlbuilder(QueryTypes oQueryType)
		{
			m_oQueryType = oQueryType;
		} 
		public sqlbuilder(params object[] oParameters)
		{
			for(int i=0; i<oParameters.Length;i=i+2)
			{
				this.addCondition((string)oParameters[i], oParameters[i+1]);
			}
		}
		#endregion

		#region PROPERTIES
        //public static bool DisableTriggers {
        //    get {
        //        return m_bDisableTriggers;
        //    }
        //    set {
        //        m_bDisableTriggers = value;
        //    }
        //}
        //public static bool DisableTriggerContext {
        //    get {
        //        return m_bDisableTriggerContext;
        //    }
        //    set {
        //        m_bDisableTriggerContext = value;
        //    }
        //}

		public QueryTypes QueryType
		{
			get
			{
				return m_oQueryType;
			}
			set
			{
				m_oQueryType = value;
			}
		}
		public ArrayList SelectColumns
		{
			get
			{
				return m_oSelectColumns;
			}
			set
			{
				m_oSelectColumns = value;
			}
		}
		public ArrayList UpdateExpressions
		{
			get
			{
				return m_oUpdateExpressions;
			}
			set
			{
				m_oUpdateExpressions = value;
			}
		}
		public ArrayList InsertExpressions {
			get {
				return m_oInsertExpressions;
			}
			set {
				m_oInsertExpressions = value;
			}
		}
		
		public ArrayList Tables
		{
			get
			{
				return m_oTables;
			}
			set
			{
				m_oTables = value;
			}
		}
		public ArrayList SelectOrder
		{
			get
			{
				return m_oSelectOrder;
			}
			set
			{
				m_oSelectOrder = value;
			}
		}
		public ArrayList Conditions
		{
			get
			{
				return m_oConditions;
			}
			set
			{
				m_oConditions = value;
			}
		}		

		public int Top
		{
			get
			{
				return m_iTop;
			}
			set
			{
				m_iTop = value;
			}
		}

		//private static ado_helper AdoHelper {
		//    get {
		//        if ( m_oData == null ) m_oData = new ado_helper();
		//        return m_oData;
		//    }
		//    set {
		//        m_oData = value;
		//    }
		//}

		#endregion

		#region ADD METHODS
		public void addSelectColumn(string sColumn)
		{
			this.SelectColumns.Add( sColumn );
		}
		public void addTable(string sTable)
		{
			this.Tables.Add( sTable );
		}
		public void addSelectOrder(string sOrder)
		{
			for ( int x = 0; x < this.SelectOrder.Count; x++ )
				if ( convert.cStr( this.SelectOrder[ x ] ).ToLower().Trim().StartsWith( sOrder.ToLower() ) )
					return;

			this.SelectOrder.Add( sOrder );
		}
		
		
		public updatevalue addUpdateExpression(string sField, object oValue)
		{
			updatevalue oUpdateValue	= new updatevalue(sField, oValue);
			oUpdateValue.ParameterStartIndex	= m_iParameterIndex;
			m_iParameterIndex++;
			this.UpdateExpressions.Add( oUpdateValue );

			return oUpdateValue;
		}

        public void addUpdateExpressions( params object[] oParameters ) {

            for ( int x = 0; x < oParameters.Length; x = x + 2 )
                addUpdateExpression( convert.cStr( oParameters[x] ), oParameters[x + 1] );

        }

		public insertvalue addInsertExpression(string sField, object oValue) {
			
			insertvalue oInsertValue	= new insertvalue( sField, oValue );
			oInsertValue.ParameterStartIndex	= m_iParameterIndex;
			m_iParameterIndex++;
			this.InsertExpressions.Add( oInsertValue );
			
			return oInsertValue;
		}

        public void addInsertExpressions( params object[] oParameters ) {

            for ( int x = 0; x < oParameters.Length; x = x + 2 ) 
                addInsertExpression( convert.cStr( oParameters[x] ), oParameters[x + 1] );            
            
        }
		
		public condition addCondition(string sCondition)
		{
			condition oCondition	= new condition(sCondition);
			this.Conditions.Add( oCondition );

			return oCondition;
		}

		public void addConditions( params object[] oParameters ) {

			for ( int x = 0; x < oParameters.Length; x = x + 1 ){
				if ( oParameters[x] as condition != null ){
					addCondition( oParameters[x] as condition );
				}else{
                    if (oParameters[x + 1] == null) 
                        addCondition(convert.cStr(oParameters[x]) + " is null");
                    else if (convert.cStr(oParameters[x + 1]).IndexOf("%") > -1)
                        addCondition(convert.cStr(oParameters[x]), condition.ConditionTypes.Like, oParameters[x + 1]);
                    else
                        addCondition(convert.cStr(oParameters[x]), oParameters[x + 1]);            
					x++;
				}
			}
		}

		public bool containsCondition( string sCondition ) {
			foreach ( condition oCondition in this.Conditions ){
				if ( oCondition.Field.ToLower() == sCondition.ToLower() || oCondition.CustomCondition.IndexOf( sCondition ) == 0 )
					return true;
			}
			return false;
		}
		public condition addCondition(string sField, condition.ConditionTypes oConditionType, object oValue)
		{
			condition oCondition	= new condition(sField, oConditionType, oValue);
			oCondition.ParameterStartIndex	= m_iParameterIndex;
			m_iParameterIndex		+= oCondition.Parameters;
			this.Conditions.Add( oCondition );

			return oCondition;
		}
		public condition addCondition(string sField, object oValue)
		{
			condition oCondition	= new condition(sField, oValue);
			addCondition( oCondition );			
			return oCondition;
		}

		public condition addCondition(condition oCondition) {
			
			oCondition.ParameterStartIndex	= m_iParameterIndex;
			m_iParameterIndex		+= oCondition.Parameters;
			this.Conditions.Add( oCondition );

			return oCondition;
		}

		public condition addCondition(string sField, condition.ConditionTypes oConditionType, object oValue1, object oValue2)
		{
			condition oCondition	= new condition(sField, oConditionType, oValue1, oValue2);
			oCondition.ParameterStartIndex	= m_iParameterIndex;
			m_iParameterIndex		+= oCondition.Parameters;
			this.Conditions.Add( oCondition );

			return oCondition;
		}
		public condition addCondition(string sField, object oValue1, object oValue2)
		{
			condition oCondition	= new condition(sField, oValue1, oValue2);
			oCondition.ParameterStartIndex	= m_iParameterIndex;
			m_iParameterIndex		+= oCondition.Parameters;
			this.Conditions.Add( oCondition );

			return oCondition;
		}
		#endregion

		#region METHODS
		public string generateQuery()
		{
			//Variables
			//int iParameterIndex			= 1;
			string sSql					= "";
			string sSelectColumns		= "";
			string sTables				= "";
			string sConditions			= "";
			string sSortOrder			= "";
			string sUpdateExpressions	= "";
			string sTop					= "";

			string sInsertColumns		= "";
			string sInsertValues		= "";

			//Create SQL Template
			switch(this.QueryType)
			{
				case QueryTypes.Update:
					sSql			= "UPDATE {tables} SET {updateexpressions} {conditions}";
					break;
				
				case QueryTypes.Select:
					sSql			= "SELECT {top} {selectcolumns} FROM {tables} {conditions} {sortorder}";
					break;

				case QueryTypes.Delete:
					sSql			= "DELETE FROM {tables} {conditions}";
					break;

				case QueryTypes.Insert:
					sSql			= "INSERT INTO {tables} ({insertcolumns}) values ({insertvalues}) ";
					break;
			}

			//Select Column Generation
			sSelectColumns	= convert.cJoin(",", this.SelectColumns);
			if(this.SelectColumns.Count == 0)
				sSelectColumns		= "*";
			
			//Table Generation
            sTables = convert.cJoin(",", this.Tables);

			//Condition Generation
			if(this.Conditions.Count > 0)
				sConditions = " WHERE ";
            sConditions += convert.cJoin(" AND ", this.Conditions);
			
			//Update Expressions
            sUpdateExpressions = convert.cJoin(",", this.UpdateExpressions);


			bool bIDInsert				= false;
			foreach ( insertvalue oInsertValue in this.InsertExpressions ){
				
				if ( oInsertValue.Field.ToLower() == "id" ) bIDInsert		= true;
				sInsertColumns			+= "[" + oInsertValue.Field + "],";
				sInsertValues			+= "@p" + oInsertValue.ParameterStartIndex + ",";				
			}

			if ( sInsertColumns != "" ){
				sInsertColumns			= sInsertColumns.Substring( 0 , sInsertColumns.Length - 1 );
				sInsertValues			= sInsertValues.Substring( 0 , sInsertValues.Length - 1 );
			}

			//Top Specification
			if(this.Top != -1)
				sTop			= String.Format(" TOP {0} ", this.Top);

			//Sort Order
			if(this.SelectOrder.Count > 0)
				sSortOrder = " ORDER BY ";
            sSortOrder += convert.cJoin(",", this.SelectOrder);

			//Dynamic Variable Placement
			sSql	= sSql.Replace("{selectcolumns}", sSelectColumns);
			sSql	= sSql.Replace("{tables}", sTables);
			if ( sTables == "" ) sSql	= sSql.Replace("FROM", "");
			
			sSql	= sSql.Replace("{conditions}", sConditions);
			sSql	= sSql.Replace("{sortorder}", sSortOrder);
			sSql	= sSql.Replace("{updateexpressions}", sUpdateExpressions);
			sSql	= sSql.Replace("{top}", sTop);

			sSql	= sSql.Replace("{insertcolumns}", sInsertColumns);
			sSql	= sSql.Replace("{insertvalues}", sInsertValues);

			
			//Return Value
			return ( bIDInsert ? " SET IDENTITY_INSERT " + functions.convert.cStr( this.Tables[ 0 ] ) + " ON;" : "" ) + sSql + ( bIDInsert ? " SET IDENTITY_INSERT " + functions.convert.cStr( this.Tables[ 0 ] ) + " OFF;" : "" );

		}
		public JDataTable execute(){			
            return execute( new ado_helper());
		}
		public JDataTable execute(jlib.db.ado_helper oHelper)
		{            
			SqlCommand sqlCommand = new SqlCommand();
			SqlParameterCollection oParameters	= sqlCommand.Parameters;
			foreach(condition oCondition in this.Conditions)
			{
                if (oCondition.Parameters > 0) {
                    if (oCondition.ConditionType == condition.ConditionTypes.Equal && oCondition.Value == null) {
                        oCondition.CustomCondition = oCondition.Field + " IS NULL";
                    } else {
                        oParameters.Add(oCondition.generateParameter1());
                    }
                }

				if(oCondition.Parameters > 1)
					oParameters.Add( oCondition.generateParameter2() );
			}
			foreach(updatevalue oUpdateValue in this.UpdateExpressions)
			{
				oParameters.Add( oUpdateValue.generateParameter() );
			}

			foreach(insertvalue oInsertValue in this.InsertExpressions) {
				oParameters.Add( oInsertValue.generateParameter() );
			}
            
			return oHelper.Execute_SQL(this.generateQuery(), oParameters );
		}

        public static JDataTable executeInsert( string sTableName, params object[] oParameters ) {            			
            return executeInsert( new ado_helper(), sTableName, oParameters );
        }

        public static JDataTable executeInsert( jlib.db.ado_helper oHelper, string sTableName, params object[] oParameters ) {                        
            JDataTable oDT = getNewRecord(oHelper, sTableName, oParameters);            
            oHelper._update(oDT, oHelper.DisableTriggerContext,"","",null);
            return oDT;
        }

        public static JDataTable executeUpdate( string sTableName, object[] oUpdateArr, params object[] oWhereParams ) {
            return executeUpdate(new ado_helper(), sTableName, oUpdateArr, oWhereParams);
        }

        public static JDataTable executeUpdate( jlib.db.ado_helper oHelper, string sTableName, object[] oUpdateArr, params object[] oWhereParams ) {            
            sqlbuilder sqlBuilder = new sqlbuilder( QueryTypes.Update );
            sqlBuilder.addTable( sTableName );
            
            for ( int x = 0; x < oUpdateArr.Length; x = x + 2 )
                sqlBuilder.addUpdateExpression( convert.cStr( oUpdateArr[x] ), oUpdateArr[ x + 1 ]);

            sqlBuilder.addConditions( oWhereParams );
            return sqlBuilder.execute( oHelper );
        }

		public static JDataTable executeUpdate(string sTableName, string sWhereFieldName, object oWhereValue, params object[] oUpdateParams)
		{
            return executeUpdate(new ado_helper(), sTableName, sWhereFieldName, oWhereValue, oUpdateParams);
		}

		public static JDataTable executeUpdate(jlib.db.ado_helper oHelper, string sTableName, string sWhereFieldName, object oWhereValue, params object[] oUpdateParams)
		{			
			sqlbuilder sqlBuilder = new sqlbuilder(QueryTypes.Update);
			sqlBuilder.addTable(sTableName);

			for (int x = 0; x < oUpdateParams.Length; x = x + 2)
				sqlBuilder.addUpdateExpression(convert.cStr(oUpdateParams[x]), oUpdateParams[x + 1]);

			sqlBuilder.addCondition(sWhereFieldName, oWhereValue);
			return sqlBuilder.execute(oHelper);
		}

        public static JDataTable executeSelect(string sTableName, params object[] oParameters) {
            return executeSelectFields( "", sTableName, oParameters );
        }

        public static JDataTable executeSelect( jlib.db.ado_helper oHelper, string sTableName, params object[] oParameters ) {
            return executeSelectFields( oHelper, "", sTableName, oParameters );
        }

        public static JDataTable executeSelectFields( string sFields, string sTableName, params object[] oParameters ) {
            return executeSelectFields(new ado_helper(), sFields, sTableName, oParameters);
        }

        public static JDataTable executeSelectFields(jlib.db.ado_helper oHelper, string sFields, string sTableName, params object[] oParameters) {
        
			sqlbuilder sqlBuilder = new sqlbuilder( QueryTypes.Select );
            
            if( sFields != "" && sFields != "*" )
                sqlBuilder.addSelectColumn( sFields );

			sqlBuilder.addTable( sTableName );
			sqlBuilder.addConditions( oParameters );            
			return sqlBuilder.execute( oHelper );
		}

		public static JDataTable executeSelectPaged(string sPreFields, string sPreTables, string sPreFilter, string sPostFields, string sSort, int iCurrentPage, int iPageSize) {
			return executeSelectPaged(sPreFields, sPreTables, sPreFilter, sPostFields, "", "", sSort, "", iCurrentPage, iPageSize);
		}

        public static JDataTable executeSelectPaged(string sPreFields, string sPreTables, string sPreFilter, string sPostFields, string sPostTables, string sPostFilter, string sSort, int iCurrentPage, int iPageSize) {            
            return executeSelectPaged(sPreFields, sPreTables, sPreFilter, sPostFields, sPostTables, sPostFilter, sSort, "", iCurrentPage, iPageSize);
        }

        public static JDataTable executeSelectPaged(string sPreFields, string sPreTables, string sPreFilter, string sPostFields, string sPostTables, string sPostFilter, string sInitialSort, string sPostSort, int iCurrentPage, int iPageSize) {
            return executeSelectPaged(new ado_helper(), sPreFields, sPreTables, sPreFilter, sPostFields, sPostTables, sPostFilter, sInitialSort, sPostSort, iCurrentPage, iPageSize);
        }

        public static JDataTable executeSelectPaged(jlib.db.ado_helper oHelper, string sPreFields, string sPreTables, string sPreFilter, string sPostFields, string sPostTables, string sPostFilter, string sSort, int iCurrentPage, int iPageSize) {
            return executeSelectPaged(oHelper, sPreFields, sPreTables, sPreFilter, sPostFields, sPostTables, sPostFilter, sSort, sSort, iCurrentPage, iPageSize);
        }
        public static JDataTable executeSelectPaged(jlib.db.ado_helper oHelper, string sPreFields, string sPreTables, string sPreFilter, string sPostFields, string sPostTables, string sPostFilter, string sInitialSort, string sPostSort, int iCurrentPage, int iPageSize) {
            return executeSelectPaged(oHelper, sPreFields, sPreTables, sPreFilter, sPostFields, sPostTables, sPostFilter, sInitialSort, sPostSort, iCurrentPage, iPageSize, "","");
        }
        public static JDataTable executeSelectPaged(jlib.db.ado_helper oHelper, string sPreFields, string sPreTables, string sPreFilter, string sPostFields, string sPostTables, string sPostFilter, string sInitialSort, string sPostSort, int iCurrentPage, int iPageSize, string sPKName, string sCustomScript) {
            return executeSelectPaged(oHelper, sPreFields, sPreTables, sPreFilter, sPostFields, sPostTables, sPostFilter, sInitialSort, sPostSort, "", "", iCurrentPage, iPageSize, sPKName, sCustomScript);
        }
        public static JDataTable executeSelectPaged(jlib.db.ado_helper oHelper, string sPreFields, string sPreTables, string sPreFilter, string sPostFields, string sPostTables, string sPostFilter, string sInitialSort, string sPostSort, string sInitialGroup, string sPostGroup, int iCurrentPage, int iPageSize, string sPKName, string sCustomScript) 
		{

			sPreFields = sPreFields ?? "*";
			sPreTables = sPreTables ?? "";
			sPreFilter = sPreFilter ?? "";
			sPostFields = sPostFields ?? "";
			sPostTables = sPostTables ?? "";
			sPostFilter = sPostFilter ?? "";
			sInitialSort = sInitialSort ?? "";
			sPostSort = sPostSort ?? "";
			sPKName = sPKName ?? "";
			sCustomScript = sCustomScript ?? "";


            if (sPostFields == "") sPostFields = sPreFields;
			if (sPreFilter.Trim().ToLower().StartsWith("and")) sPreFilter = parse.inner_substring_i(sPreFilter, "and", null, null, null);
			if (sPostFilter.Trim().ToLower().StartsWith("and")) sPostFilter = parse.inner_substring_i(sPostFilter, "and", null, null, null);
            sCustomScript = convert.cStr(sCustomScript);
            sPKName = convert.cStr(sPKName);
			//string sSQL = "WITH PageQuery AS (SELECT " + sPreFields + ", ROW_NUMBER() OVER(ORDER BY " + sInitialSort + ")  AS rownum FROM " + sPreTables + ( sPreFilter == "" ? "" : " WHERE " + sPreFilter ) + ")";
            ////sSQL += "SELECT " + sPreFields + " " + (sPostFields == "" ? "" : ", " + sPostFields) + " FROM PageQuery " + (sPostTables == "" ? "" : (sPostTables.ToLower().IndexOf("join ") == -1 ? ", " : "") + sPostTables) + " WHERE rownum between " + ((iCurrentPage - 1) * iPageSize) + " AND " + ((iCurrentPage) * iPageSize + 1) + " ORDER BY " + sPostSort + ";";
            //sSQL += "SELECT " + sPostFields + " FROM PageQuery " + (sPostTables == "" ? "" : (sPostTables.ToLower().IndexOf("join ") == -1 ? ", " : "") + sPostTables) + " WHERE rownum between " + ((iCurrentPage - 1) * iPageSize) + " AND " + ((iCurrentPage) * iPageSize + 1) + " ORDER BY " + sPostSort + ";";
            //sSQL += "SELECT COUNT(*) from " + sPreTables + ( sPreFilter == "" ? "" : " WHERE " + sPreFilter ) + ";";
            //return oHelper.Execute_SQL( sSQL );            
            string sSQL = "";
            if (sCustomScript!="" && sPKName!="") {
                sSQL = "CREATE TABLE #ids (id INT);\n insert into #ids(id) SELECT * FROM ";
                sSQL += " (SELECT " + sPKName + " FROM " + sPreTables + (sPreFilter == "" ? "" : " WHERE " + sPreFilter) + convert.cIfValue(sInitialGroup, " GROUP BY ", "", "") + ") inner_table ";
                sSQL += (sPostFilter == "" ? "" : " WHERE " + sPostFilter) + ";\n";                
                sSQL += " SELECT " + sPostFields + (sPostFields == "*" ? "" : ", num_rows") + " FROM " + (sPostTables != "" ? sPostTables + ", " : "");
                sSQL += " (SELECT " + sPreFields + (sPreFields.IndexOf("as num_rows", StringComparison.CurrentCultureIgnoreCase) > -1 || sPreTables.IndexOf("as num_rows", StringComparison.CurrentCultureIgnoreCase) > -1 ? "" : ", (select COUNT(*) from #ids) num_rows") + (sPreFields.IndexOf("as rownum", StringComparison.CurrentCultureIgnoreCase) > -1 || sPreTables.IndexOf("as rownum", StringComparison.CurrentCultureIgnoreCase) > -1 ? "" : ", ROW_NUMBER() OVER( ORDER BY " + sInitialSort + ") AS rownum ");
                sSQL += " FROM " + sPreTables + ", #ids temp_ids WHERE " + sPKName + "=temp_ids.id ) inner_table ";
                sSQL += " WHERE rownum between " + (1 + (iCurrentPage) * iPageSize) + " AND " + ((iCurrentPage + 1) * iPageSize) + convert.cIfValue(sPostGroup, " GROUP BY ", "", "") + " ORDER BY " + (sPostSort == "" ? " rownum" : sPostSort) + ";\n" + sCustomScript + ";\ndrop table #ids;";
            } else {
                sSQL = "SELECT " + sPostFields + (sPostFields == "*" ? "" : ", num_rows") + " FROM " + (sPostTables != "" ? sPostTables + ", " : "");
                sSQL += " (SELECT " + sPreFields + (sPreFields.IndexOf("as num_rows", StringComparison.CurrentCultureIgnoreCase) > -1 || sPreTables.IndexOf("as num_rows", StringComparison.CurrentCultureIgnoreCase) > -1 ? "" : ", count(*) over() as num_rows") + (sPreFields.IndexOf("as rownum", StringComparison.CurrentCultureIgnoreCase) > -1 || sPreTables.IndexOf("as rownum", StringComparison.CurrentCultureIgnoreCase) > -1 ? "" : ", ROW_NUMBER() OVER( ORDER BY " + sInitialSort + ") AS rownum ");
                sSQL += " FROM " + sPreTables + (sPreFilter == "" ? "" : " WHERE " + sPreFilter) + convert.cIfValue(sInitialGroup, " GROUP BY ", "", "") + ") inner_table ";
                sSQL += " WHERE " + (sPostFilter != "" ? sPostFilter + " AND " : "") + " rownum between " + (1 + (iCurrentPage) * iPageSize) + " AND " + ((iCurrentPage + 1) * iPageSize) + convert.cIfValue(sPostGroup, " GROUP BY ", "", "") + " ORDER BY " + (sPostSort == "" ? " rownum" : sPostSort) + ";";
            }
            return oHelper.Execute_SQL(sSQL);
        }
        public static JDataTable executeSelectPagedOracle(jlib.db.ado_helper oHelper, string sPreFields, string sPreTables, string sPreFilter, string sPostFields, string sPostTables, string sPostFilter, string sInitialSort, string sPostSort, int iCurrentPage, int iPageSize, string sPKName, string sCustomScript) {
            return executeSelectPagedOracle(oHelper, sPreFields, sPreTables, sPreFilter, sPostFields, sPostTables, sPostFilter, sInitialSort, sPostSort, "", "", iCurrentPage, iPageSize, sPKName, sCustomScript);
        }
        public static JDataTable executeSelectPagedOracle(jlib.db.ado_helper oHelper, string sPreFields, string sPreTables, string sPreFilter, string sPostFields, string sPostTables, string sPostFilter, string sInitialSort, string sPostSort, string sInitialGroup, string sPostGroup, int iCurrentPage, int iPageSize, string sPKName, string sCustomScript) {
            if (sPostFields == "") sPostFields = sPreFields;
            if (sPreFilter.Trim().ToLower().StartsWith("and")) sPreFilter = parse.inner_substring_i(sPreFilter, "and", null, null, null);
            if (sPostFilter.Trim().ToLower().StartsWith("and")) sPostFilter = parse.inner_substring_i(sPostFilter, "and", null, null, null);
            sCustomScript = convert.cStr(sCustomScript);
            sPKName = convert.cStr(sPKName);
            

            string sSQL = "";
            //if (sCustomScript != "" && sPKName != "") {
            //    sSQL = "CREATE TABLE #ids (id INT);\n insert into #ids(id) SELECT * FROM ";
            //    sSQL += " (SELECT " + sPKName + " FROM " + sPreTables + (sPreFilter == "" ? "" : " WHERE " + sPreFilter) + ") inner_table ";
            //    sSQL += (sPostFilter == "" ? "" : " WHERE " + sPostFilter) + ";\n";
            //    sSQL += " SELECT * FROM " + (sPostTables != "" ? sPostTables + ", " : "");
            //    sSQL += " (SELECT " + sPreFields + (sPreFields.IndexOf("as num_rows", StringComparison.CurrentCultureIgnoreCase) > -1 || sPreTables.IndexOf("as num_rows", StringComparison.CurrentCultureIgnoreCase) > -1 ? "" : ", count(*) over() as num_rows") + (sPreFields.IndexOf("as rownum", StringComparison.CurrentCultureIgnoreCase) > -1 || sPreTables.IndexOf("as rownum", StringComparison.CurrentCultureIgnoreCase) > -1 ? "" : ", ROW_NUMBER() OVER( ORDER BY " + sInitialSort + ") AS rownum ");
            //    sSQL += " FROM " + sPreTables + ", #ids temp_ids WHERE " + sPKName + "=temp_ids.id ) inner_table ";
            //    sSQL += " WHERE rownum between " + (1 + (iCurrentPage) * iPageSize) + " AND " + ((iCurrentPage + 1) * iPageSize) + " ORDER BY " + (sPostSort == "" ? " rownum" : sPostSort) + ";\n" + sCustomScript + ";\ndrop table #ids;";
            //} else {
                sSQL = "SELECT " + sPostFields + (sPostFields == "*" ? "" : ", num_rows") + " FROM " + (sPostTables != "" ? sPostTables + ", " : "");
                sSQL += "(SELECT inner_table.*, rownum as row_num from (SELECT " + sPreFields + (sPreFields.IndexOf("as num_rows", StringComparison.CurrentCultureIgnoreCase) > -1 || sPreTables.IndexOf("as num_rows", StringComparison.CurrentCultureIgnoreCase) > -1 ? "" : ", count(*) over() as num_rows");
                sSQL += " FROM " + sPreTables + (sPreFilter == "" ? "" : " WHERE " + sPreFilter) + convert.cIfValue(sInitialGroup, " GROUP BY ", "", "") + (sInitialSort == "" ? "" : " order by " + sInitialSort) + ") inner_table where rownum<=" + ((iCurrentPage + 1) * iPageSize) + ") inner_table ";
                sSQL += " WHERE " + (sPostFilter != "" ? sPostFilter + " AND " : "") + " row_num >= " + (1 + (iCurrentPage) * iPageSize) + convert.cIfValue(sPostGroup, " GROUP BY ", "", "") + " ORDER BY " + (sPostSort == "" ? " row_num" : sPostSort);
            //}
            return oHelper.Execute_SQL(sSQL);
        }

        public static JDataTable executeSelectSort( string sTableName, string sSort, params object[] oParameters ) {
            return executeSelectSortFields( "", sTableName, sSort, oParameters );
        }

        public static JDataTable executeSelectSort( jlib.db.ado_helper oHelper, string sTableName, string sSortOrder, params object[] oParameters ) {
            return executeSelectSortFields( oHelper, "", sTableName, sSortOrder, oParameters );
        }

        public static JDataTable executeSelectSortFields( string sFields, string sTableName, string sSort, params object[] oParameters ) {
            return executeSelectSortFields(new ado_helper(), sFields, sTableName, sSort, oParameters);
        }

        public static JDataTable executeSelectSortFields( jlib.db.ado_helper oHelper, string sFields, string sTableName, string sSortOrder, params object[] oParameters ) {
            
            sqlbuilder sqlBuilder = new sqlbuilder( QueryTypes.Select );
            
            if ( sFields != "" && sFields != "*" )
                sqlBuilder.addSelectColumn( sFields );

            sqlBuilder.addTable( sTableName );
            sqlBuilder.addConditions( oParameters );
            if ( convert.cStr( sSortOrder ) != "" ) {
                string[] sArr = parse.split( sSortOrder, "," );
                for ( int x = 0; x < sArr.Length; x++ ) {
                    sqlBuilder.addSelectOrder( sArr[x] );
                }
            }
            return sqlBuilder.execute( oHelper );
        }

        public static JDataTable executeDelete( string sTableName, params object[] oParameters ) {
            return executeDelete(new ado_helper(), sTableName, oParameters);
        }

        public static JDataTable executeDelete( jlib.db.ado_helper oHelper, string sTableName, params object[] oParameters ) {            
            sqlbuilder sqlBuilder = new sqlbuilder( QueryTypes.Delete );
            sqlBuilder.addTable( sTableName );
            sqlBuilder.addConditions( oParameters );
            return sqlBuilder.execute( oHelper );
        }


        public static DataRow getDataRow( string sSQL ) {
            return getDataRow( sSQL, 0 );
        }
        public static JDataTable getNewRecord(string sTableName, bool bAddRecord) {
            return new ado_helper().new_Record(sTableName, bAddRecord);
        }
        public static JDataTable getNewRecord(jlib.db.ado_helper oHelper, string sTableName, bool bAddRecord) {
            return oHelper.new_Record(sTableName, bAddRecord);
        }
        public static JDataTable getNewRecord( string sTableName, params object[] oInitialValues ) {
            return getNewRecord(new ado_helper(), sTableName, oInitialValues);
        }
        public static JDataTable getNewRecord( jlib.db.ado_helper oHelper, string sTableName, params object[] oInitialValues ) {

            JDataTable oDT = oHelper.new_Record( sTableName, true );
            if ( oInitialValues != null ) {
                for( int x = 0; x < oInitialValues.Length; x=x+2)
                    oDT.Rows[0][ convert.cStr(oInitialValues[x])]   = oInitialValues[x+1];
            }
            return oDT;
        }
        public static JDataTable getNewRecord(jlib.db.ado_helper oHelper, string sTableName, helpers.structures.collection oInitialValues) {
            return setValues(getNewRecord(oHelper, sTableName).Rows[0], oInitialValues).Table as JDataTable;
        }
        public static DataRow setValues(DataRow oRow, helpers.structures.collection oInitialValues) {
            for( int x=0;x<oInitialValues.Count;x++)
                if(oRow.Table.Columns.Contains(oInitialValues.Keys[x])) ado_helper.setFieldValue(oRow, oInitialValues.Keys[x], oInitialValues[x]);

            return oRow;
        }
        public static DataRow setValues(DataRow oRow, XmlNode xmlNode, params string[] sFieldNames) {
            return setValues(oRow, xmlNode, true, sFieldNames);
        }
        public static DataRow setValues(DataRow oRow, XmlNode xmlNode, bool bFieldNamesAsPairs, params string[] sFieldNames){
            if (sFieldNames != null && sFieldNames.Length == 1) sFieldNames = parse.split(sFieldNames[0], ",");
            if (sFieldNames == null || sFieldNames.Length == 0) {
                foreach (XmlNode child in xmlNode.ChildNodes) {
                    if(oRow.Table.Columns.Contains(child.Name))
                        ado_helper.setFieldValue(oRow, child.Name, xml.getXmlNodeTextValue(child));
                }
                //if (xmlNode.SelectSingleNode(oRow.Table.Columns[x].ColumnName) != null)
                      //  ado_helper.setFieldValue(oRow, oRow.Table.Columns[x].ColumnName, xml.getXmlNodeTextValue(xmlNode, oRow.Table.Columns[x].ColumnName));
            } else {
                for (int x = 0; x < sFieldNames.Length; x = x + (bFieldNamesAsPairs? 2:1)) ado_helper.setFieldValue(oRow, sFieldNames[x], xml.getXmlNodeTextValue(xmlNode, sFieldNames[x + (bFieldNamesAsPairs? 1:0)]));
            }
            return oRow;
        }

		public static DataRow setValues(DataRow oRow,  params object[] oInitialValues )  {
			if (oInitialValues != null) {
				for (int x = 0; x < oInitialValues.Length; x = x + 2)
					ado_helper.setFieldValue(oRow, convert.cStr(oInitialValues[x]),  oInitialValues[x + 1]);
			}
			return oRow;
		}

        public static JDataTable setRowValues(JDataTable oTable, params object[] oInitialValues) {
            if (oInitialValues != null) {
                for (int y = 0; y < oTable.Rows.Count; y++) {
                    for (int x = 0; x < oInitialValues.Length; x = x + 2)
                        ado_helper.setFieldValue(oTable.Rows[y], convert.cStr(oInitialValues[x]),  oInitialValues[x + 1]);
                }
            }
            return oTable;
        }

        public static JDataTable setRowValues(JDataTable oTable, string sPKName, DataRow[] oFKRows, string sFKName, params object[] oInitialValues) {
            if (oInitialValues != null && oFKRows != null && oFKRows.Length > 0 && oTable.Rows.Count > 0) {
                for (int y = 0; y < oFKRows.Length; y++) {
                    setRowValues(oTable.Select(sPKName + "='" + ado_helper.PrepareDB(oFKRows[y][sFKName]) + "'"), oInitialValues);                    
                }
            }
            return oTable;
        }

        public static DataRow setRowValues(DataRow oRow, params object[] oInitialValues) {
            if (oInitialValues != null) {                
                for (int x = 0; x < oInitialValues.Length; x = x + 2)
                    ado_helper.setFieldValue(oRow, convert.cStr(oInitialValues[x]), oInitialValues[x + 1]);
            }
            return oRow;
        }

        public static DataRow[] setRowValues(DataRow[] oRows, params object[] oInitialValues) {            
            for (int y = 0; y < oRows.Length; y++) 
                setRowValues(oRows[y],oInitialValues);                     
            return oRows;
        }

        public static JDataTable removeRows(JDataTable oTable, string sExpression) {
            DataRow[] oRows = oTable.Select(sExpression);
            for (int x = 0; x < oRows.Length; x++)
                oTable.Rows.Remove(oRows[x]);

            return oTable;
        }

        public static DataRow getDataRow( string sSQL, int iRowIndex ) {
            return getDataRow(new ado_helper(), sSQL, iRowIndex);
        }

        public static DataRow getDataRow( jlib.db.ado_helper oHelper, string sSQL, int iRowIndex ) {

            JDataTable oDT = getDataTable( oHelper, sSQL );
            if ( oDT.Rows.Count >= iRowIndex + 1 )
                return oDT.Rows[iRowIndex];

            return null;
        }

        public static DataRow getDataRow( string sTableName, params object[] oParameters ) {

            JDataTable oDT = sqlbuilder.executeSelect( sTableName, oParameters );
            return ( oDT.Rows.Count > 0 ? oDT.Rows[0] : null );
        }

        public static DataRow getDataRow( jlib.db.ado_helper oHelper, string sTableName, params object[] oParameters ) {

            JDataTable oDT = sqlbuilder.executeSelect( oHelper, sTableName, oParameters );
            return ( oDT.Rows.Count > 0 ? oDT.Rows[0] : null );
        }

        public static JDataTable getDataTable(string sSQL, params object[] oSQLParams) {
            return getDataTable(new ado_helper(), sSQL, oSQLParams);
        }

        //public static JDataTable getDataTableF(string sSQL, params object[] oSQLParams) {
        //    return getDataTableF(new ado_helper(), sSQL, oSQLParams); 
        //}

        //public static JDataTable getDataTableF(jlib.db.ado_helper oHelper, string sSQL, params object[] sParameters) {
        //    if (sSQL.Trim() == "") return null;
        //    return oHelper.Execute_SQLF(sSQL, sParameters);
        //}
        public static JDataTable getDataTable(jlib.db.ado_helper oHelper, string sSQL, params object[] oSQLParams) {
            return oHelper.Execute_SQL(sSQL, oSQLParams);
        }
        public static JDataTable getDataTableRetry( jlib.db.ado_helper oHelper, string sSQL, int iNumRetries, int iDelay, params object[] oSQLParams ) {
            JDataTable oDT=null;
            for (int x = 0; ; x++) {
                try {
                    oDT = oHelper.Execute_SQL(sSQL, oSQLParams);
                    break;
                } catch (Exception e) {
                    if (x == iNumRetries-1) throw e;
                    System.Threading.Thread.Sleep(iDelay);
                }
            }
            return oDT;
        }
        
        public static object executeSelectValue( string sTableName, string sFieldName, params object[] oParameters ) {
            return executeSelectSortValue(new ado_helper(), sTableName, sFieldName, "", oParameters);
        }
        public static object executeSelectSortValue(string sTableName, string sFieldName, string sSort, params object[] oParameters) {
            return executeSelectSortValue(new ado_helper(), sTableName, sFieldName, sSort, oParameters);
        }
        public static object executeSelectValue(jlib.db.ado_helper oHelper, string sTableName, string sFieldName, params object[] oParameters) {
            return executeSelectSortValue( oHelper, sTableName, sFieldName, "", oParameters);
        }
        public static object executeSelectSortValue( jlib.db.ado_helper oHelper, string sTableName, string sFieldName, string sSort, params object[] oParameters ) {
            
            sqlbuilder sqlBuilder = new sqlbuilder( QueryTypes.Select );
            sqlBuilder.addTable( sTableName );
            sqlBuilder.addSelectColumn( sFieldName );
            sqlBuilder.addConditions( oParameters );
            if (sSort!="")sqlBuilder.addSelectOrder(sSort);
            DataTable oDT = sqlBuilder.execute( oHelper );
            if ( oDT.Rows.Count > 0 ) return oDT.Rows[0][0];
            return null;
        }		 

		#endregion
	}
}
