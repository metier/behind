﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using Microsoft.Win32;
using System.Text.RegularExpressions;

namespace jlib.db
{
	/// <summary>
	/// ConnectionBuilder Class for connecting 
	/// </summary>
	public static class ConnectionBuilder
	{
		#region ENUMS
		public enum ConnectionStringSources
		{
			AutoDetect,
			Application,
			AppSettings,
			ConnectionStrings,
			Custom,
			CustomBuilder,
			Registry,
			Session
		}
		#endregion

		#region INTERFACES
		public interface IConnectionBuilder
		{
			string BuildConnectionString();
			bool PersistToApplication { get; set; }
		}
		#endregion

		#region CLASS: SQL
		public class Sql : IConnectionBuilder
		{
			#region CONSTRUCTORS
			/// <summary>
			/// Creates a new Sql and uses ConnectionStringSources=AutoDetect
			/// AutoDetect determines the connection string by:
			/// 1.  Checks the Application State for a Key Match
			/// 2.  Checks the Session State for a Key Match
			/// 3.  Checks the Connection Strings for a Key Match
			/// 4.  Checks the AppSettings for a Key Match
			/// 5.	Checks the registry for a Key Match
			/// 6.  Uses CustomBuilder.
			/// </summary>
			public Sql()
			{
			}
			public Sql(ConnectionStringSources ConnectionStringSource)
			{
				this.ConnectionStringSource = ConnectionStringSource;
			}
			public Sql(RegistryKey RegistryKey, string Key)
				: this(ConnectionStringSources.Registry)
			{
				this.RegistryKey = RegistryKey;
				this.Key = Key;
			}
			public Sql(string Server, string Database)
				: this(ConnectionStringSources.CustomBuilder)
			{
				this.Server = Server;
				this.Database = Database;
			}
			public Sql(string Server, string Database, bool AsynchronousProcessing)
				: this(ConnectionStringSources.CustomBuilder)
			{
				this.Server = Server;
				this.Database = Database;
				this.AsynchronousProcessing = AsynchronousProcessing;
			}
			public Sql(string Server, string Database, string FailoverServer)
				: this(ConnectionStringSources.CustomBuilder)
			{
				this.Server = Server;
				this.Database = Database;
				this.FailoverServer = FailoverServer;
			}
			public Sql(string Server, string Database, string FailoverServer, bool AsynchronousProcessing)
				: this(ConnectionStringSources.CustomBuilder)
			{
				this.Server = Server;
				this.Database = Database;
				this.FailoverServer = FailoverServer;
				this.AsynchronousProcessing = AsynchronousProcessing;
			}
			public Sql(string Server, string Database, string UserID, string Password)
				: this(ConnectionStringSources.CustomBuilder)
			{
				this.Server = Server;
				this.Database = Database;
				this.UserID = UserID;
				this.Password = Password;
			}
			public Sql(string Server, string Database, string UserID, string Password, bool AsynchronousProcessing)
				: this(ConnectionStringSources.CustomBuilder)
			{
				this.Server = Server;
				this.Database = Database;
				this.UserID = UserID;
				this.Password = Password;
				this.AsynchronousProcessing = AsynchronousProcessing;
			}
			public Sql(string Server, string Database, string UserID, string Password, string FailoverServer)
				: this(ConnectionStringSources.CustomBuilder)
			{
				this.Server = Server;
				this.Database = Database;
				this.UserID = UserID;
				this.Password = Password;
				this.FailoverServer = FailoverServer;
			}
			public Sql(string Server, string Database, string UserID, string Password, string FailoverServer, bool AsynchronousProcessing)
				: this(ConnectionStringSources.CustomBuilder)
			{
				this.Server = Server;
				this.Database = Database;
				this.UserID = UserID;
				this.Password = Password;
				this.FailoverServer = FailoverServer;
				this.AsynchronousProcessing = AsynchronousProcessing;

			}
			public Sql(string Server, string Database, string UserID, string Password, bool TrustedConnection, string FailoverServer, bool AsynchronousProcessing)
				: this(ConnectionStringSources.CustomBuilder)
			{
				this.Server = Server;
				this.Database = Database;
				this.UserID = UserID;
				this.Password = Password;
				this.TrustedConnection = TrustedConnection;
				this.FailoverServer = FailoverServer;
				this.AsynchronousProcessing = AsynchronousProcessing;

			}
			#endregion

			#region PROPERTIES
			public static string DefaultKey = "sql.dsn";
			public static SortedList<string, string> ApplicationConnectionStrings = new SortedList<string, string>();

			/// <summary>
			/// The Source of the Connection String
			/// </summary>
			public ConnectionStringSources ConnectionStringSource
			{
				get { return this._ConnectionStringSource; }
				set { this._ConnectionStringSource = value; this._BuildConnectionStringValue = ""; }
			}
			private ConnectionStringSources _ConnectionStringSource = ConnectionStringSources.AutoDetect;

			/// <summary>
			/// A Custom Connection String
			/// </summary>
			public string ConnectionString
			{
				get { return this._ConnectionString; }
				set
				{
					this._ConnectionString = value;
					this.ConnectionStringSource = ConnectionStringSources.Custom;
					this._BuildConnectionStringValue = "";
				}
			}
			private string _ConnectionString = "";

			/// <summary>
			/// The Key for storing the Connection String
			/// </summary>
			public string Key
			{
				get { return this._Key; }
				set { this._Key = value; this._BuildConnectionStringValue = ""; }
			}
			private string _Key = Sql.DefaultKey;

			/// <summary>
			/// Registry Location for storing the connection string
			/// </summary>
			public RegistryKey RegistryKey
			{
				get
				{
					if (this._RegistryKey == null)
						this._RegistryKey = functions.registry.GetExecutingAssemblyRegistryKey();

					return this._RegistryKey;
				}
				set { this._RegistryKey = value; this._BuildConnectionStringValue = ""; }
			}
			private RegistryKey _RegistryKey = null;

			/// <summary>
			/// SQL Server
			/// </summary>
			public string Server
			{
				get { return this._Server; }
				set { this._Server = value; this._BuildConnectionStringValue = ""; }
			}
			private string _Server = "";

			/// <summary>
			/// SQL Database Name
			/// </summary>
			public string Database
			{
				get { return this._Database; }
				set { this._Database = value; this._BuildConnectionStringValue = ""; }
			}
			private string _Database = "";

			/// <summary>
			/// SQL UserID
			/// </summary>
			public string UserID
			{
				get { return this._UserID; }
				set { this._UserID = value; this._BuildConnectionStringValue = ""; }
			}
			private string _UserID = "";

			/// <summary>
			/// SQL Password
			/// </summary>
			public string Password
			{
				get { return this._Password; }
				set { this._Password = value; this._BuildConnectionStringValue = ""; }
			}
			private string _Password = "";

			/// <summary>
			/// Set as trusted connection
			/// </summary>
			public bool TrustedConnection
			{
				get { return this._TrustedConnection; }
				set { this._TrustedConnection = value; this._BuildConnectionStringValue = ""; }
			}
			private bool _TrustedConnection = false;

			/// <summary>
			/// Specify Failover/Mirror Server
			/// </summary>
			public string FailoverServer
			{
				get { return this._FailoverServer; }
				set { this._FailoverServer = value; this._BuildConnectionStringValue = ""; }
			}
			private string _FailoverServer = "";

			/// <summary>
			/// Set Asynchronous Mode to true
			/// </summary>
			public bool AsynchronousProcessing
			{
				get { return this._AsynchronousProcessing; }
				set { this._AsynchronousProcessing = value; this._BuildConnectionStringValue = ""; }
			}
			private bool _AsynchronousProcessing = false;

			/// <summary>
			/// Set Setting to persist settings to application state
			/// </summary>
			public bool PersistToApplication
			{
				get { return this._PersistToApplication; }
				set { this._PersistToApplication = value; }
			}
			private bool _PersistToApplication = false;


			#endregion

			#region METHODS: Build Connection String

			public string BuildConnectionString()
			{
				//If Cached, then return cache
				if (this._BuildConnectionStringValue != "")
					return this.SetAsynchronousConnectionStringFlag(this._BuildConnectionStringValue, this.AsynchronousProcessing);

				string sConnectionString = "";

				//If Persist to Connection is set, then read from application
				if (PersistToApplication)
				{
					sConnectionString = this.BuildConnectionStringFromApplication();

					//Connection string is in application, so return
					if (sConnectionString != "")
					{
						//Save to Cache
						this._BuildConnectionStringValue = sConnectionString;

						//Return Connection String
						return this.SetAsynchronousConnectionStringFlag(sConnectionString, this.AsynchronousProcessing);
					}
				}

				//Get Native Connection String
				sConnectionString = this._BuildConnectionString();

				//if connection string is blank, then throw error
				if (sConnectionString == "")
					throw (new Exception("Connection String is blank"));

				//if Asynchronous, then 
				sConnectionString = this.SetAsynchronousConnectionStringFlag(sConnectionString, this.AsynchronousProcessing);

				//Save to Cache
				this._BuildConnectionStringValue = sConnectionString;

				//Persist to Connection
				if (this.PersistToApplication)
				{
					this.SaveToApplication();
				}

				//return connection string
				return sConnectionString;
			}

			private string SetAsynchronousConnectionStringFlag(string ConnectionString, bool Asynchronous)
			{
				//does it contain the parameter?
				ConnectionString = Regex.Replace(ConnectionString, "Asynchronous Processing=True;", "", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace);
				ConnectionString = Regex.Replace(ConnectionString, "Asynchronous Processing=False;", "", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace);

				//Add Statement
				if (Asynchronous)
					ConnectionString += (ConnectionString.EndsWith(";") ? "" : ";") + "Asynchronous Processing=True;";

				//return Connection String
				return ConnectionString;
			}

			private string _BuildConnectionStringValue = "";
			private string _BuildConnectionString()
			{

				//Build New Value
				switch (ConnectionStringSource)
				{
					case ConnectionStringSources.AutoDetect:
						// AutoDetect determines the connection string by:
						string sAutoDetectConnectionString = "";

						// 1.  Checks the Application State for a Key Match
						if ((sAutoDetectConnectionString = this.BuildConnectionStringFromApplication()) != "")
							return sAutoDetectConnectionString;

						// 2.  Checks the Session State for a Key Match
						if ((sAutoDetectConnectionString = this.BuildConnectionStringFromSession()) != "")
							return sAutoDetectConnectionString;

						// 3.  Checks the Connection Strings for a Key Match
						if ((sAutoDetectConnectionString = this.BuildConnectionStringFromConnectionStrings()) != "")
							return sAutoDetectConnectionString;

						// 4.  Checks the AppSettigns for a Key Match
						if ((sAutoDetectConnectionString = this.BuildConnectionStringFromAppSettings()) != "")
							return sAutoDetectConnectionString;

						// 5.	Checks the registry for a Key Match
						if ((sAutoDetectConnectionString = this.BuildConnectionStringFromRegistry()) != "")
							return sAutoDetectConnectionString;

						// 6.  Uses CustomBuilder.
						return this.BuildConnectionStringFromCustomBuilder();

						break;

					case ConnectionStringSources.Application:
						return BuildConnectionStringFromApplication();
						break;

					case ConnectionStringSources.AppSettings:
						return BuildConnectionStringFromAppSettings();
						break;

					case ConnectionStringSources.Custom:
						return this._ConnectionString;
						break;

					case ConnectionStringSources.CustomBuilder:
						return BuildConnectionStringFromCustomBuilder();
						break;

					case ConnectionStringSources.ConnectionStrings:
						return BuildConnectionStringFromConnectionStrings();
						break;

					case ConnectionStringSources.Registry:
						return BuildConnectionStringFromRegistry();
						break;

					case ConnectionStringSources.Session:
						return BuildConnectionStringFromSession();
						break;
				}

				return "";
			}
			private string BuildConnectionStringFromCustomBuilder()
			{
				//Declare String Builder
				StringBuilder sbConnectionString = new StringBuilder();

				//Server
				sbConnectionString.AppendFormat("Server={0};", this.Server);
				sbConnectionString.AppendFormat("Database={0};", this.Database);

				//Use Sql Authentication
				if (!TrustedConnection)
				{
					sbConnectionString.AppendFormat("User ID={0};", this.UserID);
					sbConnectionString.AppendFormat("Password={0};", this.Password);
				}
				else
					sbConnectionString.Append("Trusted_Connection=True;");

				//Failover / Mirroring
				if (FailoverServer != null && FailoverServer != "")
				{
					sbConnectionString.AppendFormat("Failover Partner={0};", this.FailoverServer);
				}

				//Asynchronous Processing
				if (AsynchronousProcessing)
				{
					sbConnectionString.Append("Asynchronous Processing=True;");
				}

				return sbConnectionString.ToString();
			}
			private string BuildConnectionStringFromAppSettings()
			{
				if (ConfigurationManager.AppSettings[this.Key] != null)
					return ConfigurationManager.AppSettings[this.Key];
				else
					return "";
			}
			private string BuildConnectionStringFromApplication()
			{
				if (this.Key == null || this.Key == "")
					return "";

				try
				{
					if (System.Web.HttpContext.Current != null)
						return functions.convert.cStr(System.Web.HttpContext.Current.Application[this.Key]);
					else
					{
						if (ConnectionBuilder.Sql.ApplicationConnectionStrings.ContainsKey(this.Key))
							return ConnectionBuilder.Sql.ApplicationConnectionStrings[this.Key];
						else
							return "";
					}

				}
				catch (Exception e)
				{
					return "";
				}
			}
			private string BuildConnectionStringFromConnectionStrings()
			{
				if (ConfigurationManager.ConnectionStrings[this.Key] != null)
					return ConfigurationManager.ConnectionStrings[this.Key].ConnectionString;

				return "";
			}
			private string BuildConnectionStringFromRegistry()
			{
				object oValue;
				if ((oValue = this.RegistryKey.GetValue(this.Key)) != null)
					return oValue.ToString();

				return "";
			}
			private string BuildConnectionStringFromSession()
			{
				if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session[this.Key] != null)
					return System.Web.HttpContext.Current.Session[this.Key].ToString();
				else
					return "";
			}
			#endregion

			#region METHODS
			public void SaveToRegistry()
			{
				this.RegistryKey.SetValue(this.Key, this.BuildConnectionString());
			}
			public void SaveToSession()
			{
				System.Web.HttpContext.Current.Session[this.Key] = this.BuildConnectionString();
			}
			public void SaveToApplication()
			{
				if (System.Web.HttpContext.Current != null)
					System.Web.HttpContext.Current.Application[this.Key] = this.BuildConnectionString();
				else
					ConnectionBuilder.Sql.ApplicationConnectionStrings[this.Key] = this.BuildConnectionString();
			}
			#endregion

		}
		#endregion

		#region CLASS: CSV
		#endregion

	}
}
