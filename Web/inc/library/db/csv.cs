﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using System.Linq;
using System.IO;
using System.Data;
using System.Data.Common;

namespace jlib.db {
   public class csv {

        /// <summary>
        /// Provides a mechanism via which CSV data can be easily written.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The <c>CsvWriter</c> class allows CSV data to be written to any stream-based destination. By default, CSV values are separated by commas
        /// (<c>,</c>) and delimited by double quotes (<c>"</c>). If necessary, custom characters can be specified when creating the <c>CsvWriter</c>.
        /// </para>
        /// <para>
        /// The number of records that have been written so far is exposed via the <see cref="RecordNumber"/> property. Writing a header record does not
        /// increment this property.
        /// </para>
        /// <para>
        /// A CSV header record can be optionally written by the <c>CsvWriter</c>. If a header record is to be written, it must be done first thing with
        /// the <see cref="WriteHeaderRecord"/> method. If a header record is written, it is exposed via the <see cref="HeaderRecord"/> property.
        /// </para>
        /// <para>
        /// Data records can be written with the <see cref="WriteDataRecord"/> or <see cref="WriteDataRecords"/> methods. These methods are overloaded to
        /// accept either instances of <see cref="DataRecord"/> or an array of <c>string</c>s.
        /// </para>
        /// </remarks>
        /// <threadsafety>
        /// The <c>CsvWriter</c> class does not lock internally. Therefore, it is unsafe to share an instance across threads without implementing your own
        /// synchronisation solution.
        /// </threadsafety>
        /// <example>
        /// <para>
        /// The following example writes some simple CSV data to a file:
        /// </para>
        /// <para>
        /// <code lang="C#">
        /// <![CDATA[
        /// using (CsvWriter writer = new CsvWriter(@"C:\Temp\data.csv")) {
        ///		writer.WriteHeaderRecord("Name", "Age", "Gender");
        ///		writer.WriteDataRecord("Kent", 25, Gender.Male);
        ///		writer.WriteDataRecord("Belinda", 26, Gender.Female);
        ///		writer.WriteDataRecord("Tempany", 0, Gender.Female);
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// <code lang="vb">
        /// <![CDATA[
        /// Dim writer As CsvWriter = Nothing
        /// 
        /// Try
        ///		writer = New CsvWriter("C:\Temp\data.csv")
        ///		writer.WriteHeaderRecord("Name", "Age", "Gender")
        ///		writer.WriteDataRecord("Kent", 25, Gender.Male)
        ///		writer.WriteDataRecord("Belinda", 26, Gender.Female)
        ///		writer.WriteDataRecord("Tempany", 0, Gender.Female)
        /// Finally
        ///		If (Not writer Is Nothing) Then
        ///			writer.Close()
        ///		End If
        /// End Try
        /// ]]>
        /// </code>
        /// </para>
        /// </example>
        /// <example>
        /// <para>
        /// The following example writes the contents of a <c>DataTable</c> to a <see cref="MemoryStream"/>. CSV values are separated by tabs and
        /// delimited by the pipe characters (<c>|</c>). Linux-style line breaks are written by the <c>CsvWriter</c>, regardless of the hosting platform:
        /// </para>
        /// <para>
        /// <code lang="C#">
        /// <![CDATA[
        /// DataTable table = GetDataTable();
        /// MemoryStream memStream = new MemoryStream();
        /// 
        /// using (CsvWriter writer = new CsvWriter(memStream)) {
        ///		writer.NewLine = "\r";
        ///		writer.ValueSeparator = '\t';
        ///		writer.ValueDelimiter = '|';
        ///		writer.WriteAll(table, true);
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// <code lang="vb">
        /// <![CDATA[
        /// Dim table As DataTable = GetDataTable
        /// Dim memStream As MemoryStream = New MemoryStream
        /// Dim writer As CsvWriter = Nothing
        /// 
        /// Try
        ///		writer = New CsvWriter(memStream)
        ///		writer.NewLine = vbLf
        ///		writer.ValueSeparator = vbTab
        ///		writer.ValueDelimiter = "|"c
        ///		writer.WriteAll(table, True)
        /// Finally
        ///		If (Not writer Is Nothing) Then
        ///			writer.Close()
        ///		End If
        /// End Try
        /// ]]>
        /// </code>
        /// </para>
        /// </example>

        /// <summary>
        /// A base class for CSV record types.
        /// </summary>
        /// <remarks>
        /// The CSV record types <see cref="HeaderRecord"/> and <see cref="DataRecord"/> obtain common functionality by inheriting from this class.
        /// </remarks>
        [Serializable]
        public abstract class RecordBase {
            /// <summary>
            /// See <see cref="Values"/>.
            /// </summary>
            private IList<string> _values;

            /// <summary>
            /// The character used to separator values in the <see cref="ToString"/> implementation
            /// </summary>
            public const char ValueSeparator = (char)0x2022;

            /// <summary>
            /// Gets the value at the specified index for this CSV record.
            /// </summary>
            public string this[int index] {
                get {
                    return _values[index];
                }
                set {
                    _values[index] = value;
                }
            }

            /// <summary>
            /// Gets a collection of values in this CSV record.
            /// </summary>
            public IList<string> Values {
                get {
                    return _values;
                }
            }

            /// <summary>
            /// Initialises an instance of <c>RecordBase</c> with no values.
            /// </summary>
            protected RecordBase() {
                _values = new List<string>();
            }

            /// <summary>
            /// Initialises an instance of the <c>RecordBase</c> class with the values specified.
            /// </summary>
            /// <param name="values">
            /// The values for the CSV record.
            /// </param>
            protected RecordBase(string[] values)
                : this(values, false) {
            }

            /// <summary>
            /// Initialises an instance of the <c>RecordBase</c> class with the values specified, optionally making the value collection read-only.
            /// </summary>
            /// <param name="values">
            /// The values for the CSV record.
            /// </param>
            /// <param name="readOnly">
            /// If <see langword="true"/>, the value collection will be read-only.
            /// </param>
            protected RecordBase(string[] values, bool readOnly) {                
                _values = new List<string>(values);

                if (readOnly) {
                    //just use the wrapper readonly collection
                    _values = new ReadOnlyCollection<string>(_values);
                }
            }

            /// <summary>
            /// Gets the value at the specified index, or <see langword="null"/> if the index is invalid.
            /// </summary>
            /// <param name="index">
            /// The index.
            /// </param>
            /// <returns>
            /// The value, or <see langword="null"/>.
            /// </returns>
            public string GetValueOrNull(int index) {
                return _values.ElementAtOrDefault(index);
            }

            /// <summary>
            /// Determines whether this <c>RecordBase</c> is equal to <paramref name="obj"/>.
            /// </summary>
            /// <remarks>
            /// Two <c>RecordBase</c> instances are considered equal if they contain the same number of values, and each of their corresponding values are also
            /// equal.
            /// </remarks>
            /// <param name="obj">
            /// The object to compare to this <c>RecordBase</c>.
            /// </param>
            /// <returns>
            /// <see langword="true"/> if <paramref name="obj"/> is equal to this <c>RecordBase</c>, otherwise <see langword="false"/>.
            /// </returns>
            public override bool Equals(object obj) {
                if (object.ReferenceEquals(obj, this)) {
                    return true;
                }

                var record = obj as RecordBase;

                if (record == null) {
                    return false;
                }

                if (_values.Count != record._values.Count) {
                    return false;
                }

                for (var i = 0; i < _values.Count; ++i) {
                    if (_values[i] != record._values[i]) {
                        return false;
                    }
                }

                return true;
            }

            /// <summary>
            /// Gets a hash code for this <c>RecordBase</c>.
            /// </summary>
            /// <returns>
            /// A hash code for this <c>RecordBase</c>.
            /// </returns>
            public override int GetHashCode() {
                var retVal = 17;

                for (var i = 0; i < _values.Count; ++i) {
                    retVal += _values[i].GetHashCode();
                }

                return retVal;
            }

            /// <summary>
            /// Returns a <c>string</c> representation of this CSV record.
            /// </summary>
            /// <remarks>
            /// This method is provided for debugging and diagnostics only. Each value in the record is present in the returned string, with a bullet
            /// character (<c>U+2022</c>) separating them.
            /// </remarks>
            /// <returns>
            /// A <c>string</c> representation of this record.
            /// </returns>
            public sealed override string ToString() {
                var retVal = new StringBuilder();

                foreach (var val in _values) {
                    retVal.Append(val).Append(ValueSeparator);
                }

                return retVal.ToString();
            }
        }


        /// <summary>
        /// Represents a header record from a CSV source.
        /// </summary>
        /// <remarks>
        /// An instance of this class represents the header record in a CSV data source. Such a record defines only the column names for the CSV data.
        /// </remarks>

        [Serializable]
        public sealed class HeaderRecord : RecordBase {
            /// <summary>
            /// Maps column names to indexes.
            /// </summary>
            private readonly IDictionary<string, int> _columnToIndexMap;

            /// <summary>
            /// Gets the index of the specified column in this header record.
            /// </summary>
            /// <remarks>
            /// This indexer can be used to determine the corresponding index of a named column in this header record. If the specified column is not found
            /// in this header record, an exception is thrown.
            /// </remarks>
            public int this[string column] {
                get {
                    if (!_columnToIndexMap.ContainsKey(column)) {
                        throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "No column named '{0}' exists in this header record.", column));
                    }

                    return _columnToIndexMap[column];
                }
            }

            /// <summary>
            /// Creates and returns an instance of <c>HeaderRecord</c> by parsing via the provided CSV parser.
            /// </summary>
            /// <param name="parser">
            /// The CSV parser to use.
            /// </param>
            /// <returns>
            /// The CSV header record, or <see langword="null"/> if no record was found in the parser provided.
            /// </returns>
            internal static HeaderRecord FromParser(CsvParser parser) {
                HeaderRecord retVal = null;
                string[] values = parser.ParseRecord();

                if (values != null) {
                    retVal = new HeaderRecord(values, true);
                }

                return retVal;
            }

            /// <summary>
            /// Constructs an empty instance of <c>HeaderRecord</c>.
            /// </summary>
            public HeaderRecord() {
            }

            /// <summary>
            /// Constructs an instance of <c>HeaderRecord</c> with the columns specified.
            /// </summary>
            /// <param name="columns">
            /// The columns in the CSV header.
            /// </param>
            public HeaderRecord(string[] columns)
                : this(columns, false) {
            }

            /// <summary>
            /// Constructs an instance of <c>HeaderRecord</c> with the columns specified, optionally making the values in the header record read-only.
            /// </summary>
            /// <param name="columns">
            /// The columns in the CSV header.
            /// </param>
            /// <param name="readOnly">
            /// If <see langword="true"/>, the values in this header record are read-only.
            /// </param>
            public HeaderRecord(string[] columns, bool readOnly)
                : base(columns, readOnly) {
                _columnToIndexMap = new Dictionary<string, int>();

                //populate the dictionary with column name -> index mappings
                for (int i = 0; i < Values.Count; ++i) {
                    string columnName = Values[i];

                    if (_columnToIndexMap.ContainsKey(columnName)) {
                        throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "A column named '{0}' appears more than once in the header record.", columnName));
                    }

                    _columnToIndexMap[columnName] = i;
                }
            }

            /// <summary>
            /// Gets the index of the column with the specified name.
            /// </summary>
            /// <param name="column">
            /// The name of the column.
            /// </param>
            /// <returns>
            /// The index of the column, or <c>-1</c> if the column does not exist in this header record.
            /// </returns>
            public int IndexOf(string column) {                
                int index = int.MinValue;

                if (!_columnToIndexMap.TryGetValue(column, out index)) {
                    return -1;
                }

                return index;
            }
        }


        /// <summary>
        /// Represents a single CSV data record.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Instances of this class are used to represent a record of CSV data. Each record has any number of values in it, accessible via the indexers in
        /// this class.
        /// </para>
        /// <para>
        /// If the CSV data source that this record originates from had a header record initialised, it is exposed via the <see cref="HeaderRecord"/>
        /// property.
        /// </para>
        /// </remarks>
        [Serializable]
        public sealed class DataRecord : RecordBase {
            
            /// <summary>
            /// See <see cref="HeaderRecord"/>.
            /// </summary>
            private HeaderRecord _headerRecord;

            /// <summary>
            /// Gets the header record for this CSV record, or <see langword="null"/> if no header record applies.
            /// </summary>
            /// <remarks>
            /// If no header record was initially read from the CSV data source, then this property yields <see langword="null"/>. Otherwise, it yields the
            /// <see cref="HeaderRecord"/> instance that contains the details of the header record.
            /// </remarks>
            public HeaderRecord HeaderRecord {
                get {
                    return _headerRecord;
                }
            }

            /// <summary>
            /// Gets a value in this CSV record by column name.
            /// </summary>
            /// <remarks>
            /// This indexer can be used to retrieve a record value by column name. It is only possible to do so if the header record was initialised in the
            /// CSV data source. If not, <see cref="HeaderRecord"/> will be <see langword="null"/> and this indexer will throw an exception if used.
            /// </remarks>
            public string this[string column] {
                get {
                    if (_headerRecord == null) throw new Exception("No header record is associated with this DataRecord.");                    
                    return Values[_headerRecord[column]];
                }
            }

            /// <summary>
            /// Creates and returns an instance of <c>DataRecord</c> by parsing with the provided CSV parser.
            /// </summary>
            /// <param name="headerRecord">
            /// The header record for the parsed data record, or <see langword="null"/> if irrelevant.
            /// </param>
            /// <param name="parser">
            /// The CSV parser to use.
            /// </param>
            /// <returns>
            /// The CSV record, or <see langword="null"/> if no record was found in the reader provided.
            /// </returns>
            internal static DataRecord FromParser(HeaderRecord headerRecord, CsvParser parser) {
                DataRecord retVal = null;
                var values = parser.ParseRecord();

                if (values != null) {
                    retVal = new DataRecord(headerRecord, values, true);
                }

                return retVal;
            }

            /// <summary>
            /// Constructs an instance of <c>DataRecord</c> with the header specified.
            /// </summary>
            /// <param name="headerRecord">
            /// The header record for this CSV record, or <see langword="null"/> if no header record applies.
            /// </param>
            public DataRecord(HeaderRecord headerRecord) {
                _headerRecord = headerRecord;
            }

            /// <summary>
            /// Constructs an instance of <c>DataRecord</c> with the header and values specified.
            /// </summary>
            /// <param name="headerRecord">
            /// The header record for this CSV record, or <see langword="null"/> if no header record applies.
            /// </param>
            /// <param name="values">
            /// The values for this CSV record.
            /// </param>
            public DataRecord(HeaderRecord headerRecord, string[] values)
                : this(headerRecord, values, false) {
            }

            /// <summary>
            /// Constructs an instance of <c>DataRecord</c> with the header and values specified, optionally making the values in this data record
            /// read-only.
            /// </summary>
            /// <param name="headerRecord">
            /// The header record for this CSV record, or <see langword="null"/> if no header record applies.
            /// </param>
            /// <param name="values">
            /// The values for this CSV record.
            /// </param>
            /// <param name="readOnly">
            /// If <see langword="true"/>, the values in this data record are read-only.
            /// </param>
            public DataRecord(HeaderRecord headerRecord, string[] values, bool readOnly)
                : base(values, readOnly) {
                _headerRecord = headerRecord;
            }

            /// <summary>
            /// Gets the value in the specified column, or <see langword="null"/> if the value does not exist.
            /// </summary>
            /// <param name="column">
            /// The column name.
            /// </param>
            /// <returns>
            /// The value, or <see langword="null"/> if the value does not exist for this record.
            /// </returns>
            public string GetValueOrNull(string column) {                
                if(_headerRecord == null) throw new Exception("No header record is associated with this DataRecord.");
                return GetValueOrNull(_headerRecord.IndexOf(column));
            }

            /// <summary>
            /// Determines whether this <c>DataRecord</c> is equal to <paramref name="obj"/>.
            /// </summary>
            /// <remarks>
            /// Two <c>DataRecord</c> instances are considered equal if all their values are equal and their header records are equal.
            /// </remarks>
            /// <param name="obj">
            /// The object to compare to this <c>DataRecord</c>.
            /// </param>
            /// <returns>
            /// <see langword="true"/> if this <c>DataRecord</c> equals <paramref name="obj"/>, otherwise <see langword="false"/>.
            /// </returns>
            public override bool Equals(object obj) {
                if (object.ReferenceEquals(obj, this)) {
                    return true;
                }

                var record = obj as DataRecord;

                if (record == null) {
                    return false;
                }

                //this checks that all values are equal
                if (!base.Equals(obj)) {
                    return false;
                }

                if ((_headerRecord == null) && (record._headerRecord == null)) {
                    //both have no header and equal values, therefore they are equal
                    return true;
                } else if (((_headerRecord != null) && (record._headerRecord != null)) && (_headerRecord.Equals(record._headerRecord))) {
                    //both have equal headers and equal values, therefore they are equal
                    return true;
                }

                return false;
            }

            /// <summary>
            /// Gets a hash code for this <c>DataRecord</c>.
            /// </summary>
            /// <returns>
            /// A hash code for this <c>DataRecord</c>.
            /// </returns>
            public override int GetHashCode() {
                var retVal = base.GetHashCode();

                if (_headerRecord != null) {
                    retVal = (int)Math.Pow(retVal, _headerRecord.GetHashCode());
                }

                return retVal;
            }
        }


        public class CsvWriter : IDisposable {
            
            private static readonly Encoding defaultEncoding = Encoding.Default;
            /// <summary>
            /// The <see cref="TextWriter"/> used to output CSV data.
            /// </summary>
            private TextWriter _writer;

            /// <summary>
            /// See <see cref="HeaderRecord"/>.
            /// </summary>
            private HeaderRecord _headerRecord;

            /// <summary>
            /// See <see cref="AlwaysDelimit"/>.
            /// </summary>
            private bool _alwaysDelimit;

            /// <summary>
            /// The buffer of characters containing the current value.
            /// </summary>
            private char[] _valueBuffer;

            /// <summary>
            /// The last valid index into <see cref="_valueBuffer"/>.
            /// </summary>
            private int _valueBufferEndIndex;

            /// <summary>
            /// See <see cref="ValueSeparator"/>.
            /// </summary>
            private char _valueSeparator;

            /// <summary>
            /// See <see cref="ValueDelimiter"/>.
            /// </summary>
            private char _valueDelimiter;

            /// <summary>
            /// See <see cref="RecordNumber"/>.
            /// </summary>
            private long _recordNumber;

            /// <summary>
            /// Set to <see langword="true"/> once the first record is written.
            /// </summary>
            private bool _passedFirstRecord;

            /// <summary>
            /// The space character.
            /// </summary>
            private const char SPACE = ' ';

            /// <summary>
            /// The carriage return character. Escape code is <c>\r</c>.
            /// </summary>
            private const char CR = (char)0x0d;

            /// <summary>
            /// The line-feed character. Escape code is <c>\n</c>.
            /// </summary>
            private const char LF = (char)0x0a;

            /// <summary>
            /// Gets the encoding of the underlying writer for this <c>CsvWriter</c>.
            /// </summary>
            public Encoding Encoding {
                get {
                    
                    return _writer.Encoding;
                }
            }

            /// <summary>
            /// Gets or sets a value indicating whether values should always be delimited.
            /// </summary>
            /// <remarks>
            /// By default the <c>CsvWriter</c> will only delimit values that require delimiting. Setting this property to <c>true</c> will ensure that all written values are
            /// delimited.
            /// </remarks>
            public bool AlwaysDelimit {
                get {                    
                    return _alwaysDelimit;
                }
                set {                    
                    _alwaysDelimit = value;
                }
            }

            /// <summary>
            /// Gets or sets the character placed between values in the CSV data.
            /// </summary>
            /// <remarks>
            /// This property retrieves the character that this <c>CsvWriter</c> will use to separate distinct values in the CSV data. The default value
            /// of this property is a comma (<c>,</c>).
            /// </remarks>
            public char ValueSeparator {
                get {                    
                    return _valueSeparator;
                }
                set {
                    
                    if (value == _valueDelimiter) throw new Exception("The value separator and value delimiter must be different.");
                    if (value == SPACE) throw new Exception("Space is not a valid value separator or delimiter.");

                    _valueSeparator = value;
                }
            }

            /// <summary>
            /// Gets the character possibly placed around values in the CSV data.
            /// </summary>
            /// <remarks>
            /// <para>
            /// This property retrieves the character that this <c>CsvWriter</c> will use to demarcate values in the CSV data. The default value of this
            /// property is a double quote (<c>"</c>).
            /// </para>
            /// <para>
            /// If <see cref="AlwaysDelimit"/> is <c>true</c>, then values written by this <c>CsvWriter</c> will always be delimited with this character. Otherwise, the
            /// <c>CsvWriter</c> will decide whether values must be delimited based on their content.
            /// </para>
            /// </remarks>
            public char ValueDelimiter {
                get {
                    
                    return _valueDelimiter;
                }
                set {
                                        
                    if (value == _valueDelimiter) throw new Exception("The value separator and value delimiter must be different.");
                    if (value == SPACE) throw new Exception("Space is not a valid value separator or delimiter.");

                    _valueDelimiter = value;
                }
            }

            /// <summary>
            /// Gets or sets the line terminator for this <c>CsvWriter</c>.
            /// </summary>
            /// <remarks>
            /// This property gets or sets the line terminator for the underlying <c>TextWriter</c> used by this <c>CsvWriter</c>. If this is set to <see langword="null"/> the
            /// default newline string is used instead.
            /// </remarks>
            public string NewLine {
                get {
                    
                    return _writer.NewLine;
                }
                set {
                    
                    _writer.NewLine = value;
                }
            }

            /// <summary>
            /// Gets the CSV header for this writer.
            /// </summary>
            /// <value>
            /// The CSV header record for this writer, or <see langword="null"/> if no header record applies.
            /// </value>
            /// <remarks>
            /// This property can be used to retrieve the <see cref="HeaderRecord"/> that represents the header record for this <c>CsvWriter</c>. If a
            /// header record has been written (using, for example, <see cref="WriteHeaderRecord"/>) then this property will retrieve the details of the
            /// header record. If a header record has not been written, this property will return <see langword="null"/>.
            /// </remarks>
            public HeaderRecord HeaderRecord {
                get {
                    
                    return _headerRecord;
                }
            }

            /// <summary>
            /// Gets the current record number.
            /// </summary>
            /// <remarks>
            /// This property gives the number of records that the <c>CsvWriter</c> has written. The CSV header does not count. That is, calling
            /// <see cref="WriteHeaderRecord"/> will not increment this property. Only successful calls to <see cref="WriteDataRecord"/> (and related methods)
            /// will increment this property.
            /// </remarks>
            public long RecordNumber {
                get {
                    
                    return _recordNumber;
                }
            }

            /// <summary>
            /// Constructs and initialises an instance of <c>CsvWriter</c> based on the information provided.
            /// </summary>
            /// <remarks>
            /// If the specified file already exists, it will be overwritten.
            /// </remarks>
            /// <param name="stream">
            /// The stream to which CSV data will be written.
            /// </param>
            public CsvWriter(Stream stream)
                : this(stream, defaultEncoding) {
            }

            /// <summary>
            /// Constructs and initialises an instance of <c>CsvWriter</c> based on the information provided.
            /// </summary>
            /// <remarks>
            /// If the specified file already exists, it will be overwritten.
            /// </remarks>
            /// <param name="stream">
            /// The stream to which CSV data will be written.
            /// </param>
            /// <param name="encoding">
            /// The encoding for the data in <paramref name="stream"/>.
            /// </param>
            public CsvWriter(Stream stream, Encoding encoding)
                : this(new StreamWriter(stream, encoding)) {
            }

            /// <summary>
            /// Constructs and initialises an instance of <c>CsvWriter</c> based on the information provided.
            /// </summary>
            /// <remarks>
            /// If the specified file already exists, it will be overwritten.
            /// </remarks>
            /// <param name="path">
            /// The full path to the file to which CSV data will be written.
            /// </param>
            public CsvWriter(string path)
                : this(path, false, defaultEncoding) {
            }

            /// <summary>
            /// Constructs and initialises an instance of <c>CsvWriter</c> based on the information provided.
            /// </summary>
            /// <remarks>
            /// If the specified file already exists, it will be overwritten.
            /// </remarks>
            /// <param name="path">
            /// The full path to the file to which CSV data will be written.
            /// </param>
            /// <param name="encoding">
            /// The encoding for the data in <paramref name="path"/>.
            /// </param>
            public CsvWriter(string path, Encoding encoding)
                : this(path, false, encoding) {
            }

            /// <summary>
            /// Constructs and initialises an instance of <c>CsvWriter</c> based on the information provided.
            /// </summary>
            /// <remarks>
            /// If the specified file already exists, it will be overwritten.
            /// </remarks>
            /// <param name="path">
            /// The full path to the file to which CSV data will be written.
            /// </param>
            /// <param name="append">
            /// If <c>true</c>, data will be appended to the specified file.
            /// </param>
            public CsvWriter(string path, bool append)
                : this(path, append, defaultEncoding) {
            }

            /// <summary>
            /// Constructs and initialises an instance of <c>CsvWriter</c> based on the information provided.
            /// </summary>
            /// <remarks>
            /// If the specified file already exists, it will be overwritten.
            /// </remarks>
            /// <param name="path">
            /// The full path to the file to which CSV data will be written.
            /// </param>
            /// <param name="append">
            /// If <c>true</c>, data will be appended to the specified file.
            /// </param>
            /// <param name="encoding">
            /// The encoding for the data in <paramref name="path"/>.
            /// </param>
            public CsvWriter(string path, bool append, Encoding encoding)
                : this(new StreamWriter(path, append, encoding)) {
            }

            /// <summary>
            /// Constructs and initialises an instance of <c>CsvWriter</c> based on the information provided.
            /// </summary>
            /// <param name="writer">
            /// The <see cref="TextWriter"/> to which CSV data will be written.
            /// </param>
            public CsvWriter(TextWriter writer) {
                
                _writer = writer;
                _valueSeparator = CsvParser.DefaultValueSeparator;
                _valueDelimiter = CsvParser.DefaultValueDelimiter;
                _valueBuffer = new char[128];
            }

            /// <summary>
            /// Disposes of this <c>CsvWriter</c> instance.
            /// </summary>
            void IDisposable.Dispose() {
                Close();
                Dispose(true);
            }

            /// <summary>
            /// Allows sub classes to implement disposing logic.
            /// </summary>
            /// <param name="disposing">
            /// <see langword="true"/> if this method is being called in response to a <see cref="Dispose"/> call, or <see langword="false"/> if
            /// it is being called during finalization.
            /// </param>
            protected virtual void Dispose(bool disposing) {
            }

            /// <summary>
            /// Closes this <c>CsvWriter</c> instance and releases all resources acquired by it.
            /// </summary>
            /// <remarks>
            /// Once an instance of <c>CsvWriter</c> is no longer needed, call this method to immediately release any resources. Closing a <c>CsvWriter</c> is equivalent to
            /// disposing of it in a C# <c>using</c> block.
            /// </remarks>
            public void Close() {
                if (_writer != null) _writer.Close();                
            }

            /// <summary>
            /// Flushes the underlying buffer of this <c>CsvWriter</c>.
            /// </summary>
            /// <remarks>
            /// This method can be used to flush the underlying <c>Stream</c> that this <c>CsvWriter</c> writes to.
            /// </remarks>
            public void Flush() {                
                _writer.Flush();
            }

            /// <summary>
            /// Writes the specified header record.
            /// </summary>
            /// <remarks>
            /// This method writes the specified header record to the underlying <c>Stream</c>. Once successfully written, the header record is exposed via
            /// the <see cref="HeaderRecord"/> property.
            /// </remarks>
            /// <param name="headerRecord">
            /// The CSV header record to be written.
            /// </param>
            public void WriteHeaderRecord(HeaderRecord headerRecord) {
                if (_passedFirstRecord) throw new Exception("The first record has already been passed - cannot write header record.");
                
                _headerRecord = headerRecord;
                WriteRecord(headerRecord.Values, false);
            }

            /// <summary>
            /// Writes a header record with the specified columns.
            /// </summary>
            /// <remarks>
            /// Each item in <paramref name="headerRecord"/> is converted to a <c>string</c> via its <c>ToString</c> implementation. If any item is <see langword="null"/>,
            /// it is substituted for an empty <c>string</c> (<see cref="string.Empty"/>).
            /// </remarks>
            /// <param name="headerRecord">
            /// An array of header column names.
            /// </param>
            public void WriteHeaderRecord(params object[] headerRecord) {
                
                string[] headerRecordAsStrings = new string[headerRecord.Length];

                for (int i = 0; i < headerRecordAsStrings.Length; ++i) {
                    object o = headerRecord[i];

                    if (o != null) {
                        headerRecordAsStrings[i] = o.ToString();
                    } else {
                        headerRecordAsStrings[i] = string.Empty;
                    }
                }

                WriteHeaderRecord(headerRecordAsStrings);
            }

            /// <summary>
            /// Writes the specified header record.
            /// </summary>
            /// <remarks>
            /// This method writes the specified header record to the underlying <c>Stream</c>. Once successfully written, the header record is exposed via
            /// the <see cref="HeaderRecord"/> property.
            /// </remarks>
            /// <param name="headerRecord">
            /// The CSV header record to be written.
            /// </param>
            public void WriteHeaderRecord(string[] headerRecord) {
                                
                if (_passedFirstRecord) throw new Exception("The first record has already been passed - cannot write header record.");
                
                _headerRecord = new HeaderRecord(headerRecord, true);
                WriteRecord(headerRecord, false);
            }

            /// <summary>
            /// Writes the specified record to this <c>CsvWriter</c>.
            /// </summary>
            /// <remarks>
            /// This method writes a single data record to this <c>CsvWriter</c>. The <see cref="RecordNumber"/> property is incremented upon successfully writing
            /// the record.
            /// </remarks>
            /// <param name="dataRecord">
            /// The record to be written.
            /// </param>
            public void WriteDataRecord(DataRecord dataRecord) {
                
                WriteRecord(dataRecord.Values, true);
            }


            /// <summary>
            /// Writes a data record with the specified values.
            /// </summary>
            /// <remarks>
            /// Each item in <paramref name="dataRecord"/> is converted to a <c>string</c> via its <c>ToString</c> implementation. If any item is <see langword="null"/>, it is substituted
            /// for an empty <c>string</c> (<see cref="string.Empty"/>).
            /// </remarks>
            /// <param name="dataRecord">
            /// An array of data values.
            /// </param>
            public void WriteDataRecord(params object[] dataRecord) {
                
                string[] dataRecordAsStrings = new string[dataRecord.Length];

                for (int i = 0; i < dataRecordAsStrings.Length; ++i) {
                    object o = dataRecord[i];

                    if (o != null) {
                        dataRecordAsStrings[i] = o.ToString();
                    } else {
                        dataRecordAsStrings[i] = string.Empty;
                    }
                }

                WriteDataRecord(dataRecordAsStrings);
            }

            /// <summary>
            /// Writes the specified record to this <c>CsvWriter</c>.
            /// </summary>
            /// <remarks>
            /// This method writes a single data record to this <c>CsvWriter</c>. The <see cref="RecordNumber"/> property is incremented upon successfully writing
            /// the record.
            /// </remarks>
            /// <param name="dataRecord">
            /// The record to be written.
            /// </param>
            public void WriteDataRecord(string[] dataRecord) {
                
                WriteRecord(dataRecord, true);
            }

            /// <summary>
            /// Writes all records specified by <paramref name="dataRecords"/> to this <c>CsvWriter</c>.
            /// </summary>
            /// <remarks>
            /// This method writes all data records in <paramref name="dataRecords"/> to this <c>CsvWriter</c> and increments the <see cref="RecordNumber"/> property
            /// as records are written.
            /// </remarks>
            /// <param name="dataRecords">
            /// The records to be written.
            /// </param>
            public void WriteDataRecords(ICollection<DataRecord> dataRecords) {
                
                foreach (DataRecord dataRecord in dataRecords) {
                    WriteRecord(dataRecord.Values, true);
                }
            }

            /// <summary>
            /// Writes all records specified by <paramref name="dataRecords"/> to this <c>CsvWriter</c>.
            /// </summary>
            /// <remarks>
            /// This method writes all data records in <paramref name="dataRecords"/> to this <c>CsvWriter</c> and increments the <see cref="RecordNumber"/> property
            /// as records are written.
            /// </remarks>
            /// <param name="dataRecords">
            /// The records to be written.
            /// </param>
            public void WriteDataRecords(ICollection<string[]> dataRecords) {
                                
                foreach (string[] dataRecord in dataRecords) {
                    WriteRecord(dataRecord, true);
                }
            }


            /// <summary>
            /// Writes the data in <paramref name="table"/> as CSV data.
            /// </summary>
            /// <remarks>
            /// This method writes all the data in <paramref name="table"/> to this <c>CsvWriter</c>, including a header record. If a header record has already
            /// been written to this <c>CsvWriter</c> this method will throw an exception. That being the case, you should use <see cref="WriteAll(DataTable, bool)"/>
            /// instead, specifying <see langword="false"/> for the second parameter.
            /// </remarks>
            /// <param name="table">
            /// The <c>DataTable</c> whose data is to be written as CSV data.
            /// </param>
            public void WriteAll(DataTable table) {
                WriteAll(table, true);
            }

            /// <summary>
            /// Writes the data in <paramref name="table"/> as CSV data.
            /// </summary>
            /// <remarks>
            /// This method writes all the data in <paramref name="table"/> to this <c>CsvWriter</c>, optionally writing a header record based on the columns in the
            /// table.
            /// </remarks>
            /// <param name="table">
            /// The <c>DataTable</c> whose data is to be written as CSV data.
            /// </param>
            /// <param name="writeHeaderRecord">
            /// If <see langword="true"/>, a CSV header will be written based on the column names for the table.
            /// </param>
            public void WriteAll(DataTable table, bool writeHeaderRecord) {
                                
                if (writeHeaderRecord) {
                    HeaderRecord headerRecord = new HeaderRecord();

                    foreach (DataColumn column in table.Columns) {
                        headerRecord.Values.Add(column.ColumnName);
                    }

                    WriteHeaderRecord(headerRecord);
                }

                foreach (DataRow row in table.Rows) {
                    DataRecord dataRecord = new DataRecord(_headerRecord);

                    foreach (object item in row.ItemArray) {
                        dataRecord.Values.Add((item == null) ? string.Empty : item.ToString());
                    }

                    WriteDataRecord(dataRecord);
                }
            }

            /// <summary>
            /// Writes the first <see cref="DataTable"/> in <paramref name="dataSet"/> as CSV data.
            /// </summary>
            /// <remarks>
            /// This method writes all the data in the first table of <paramref name="dataSet"/> to this <c>CsvWriter</c>, including a header record.
            /// If a header record has already been written to this <c>CsvWriter</c> this method will throw an exception. That being the case, you
            /// should use <see cref="WriteAll(DataSet, bool)"/> instead, specifying <see langword="false"/> for the second parameter.
            /// </remarks>
            /// <param name="dataSet">
            /// The <c>DataSet</c> whose first table is to be written as CSV data.
            /// </param>
            public void WriteAll(DataSet dataSet) {
                WriteAll(dataSet, true);
            }

            /// <summary>
            /// Writes the first <see cref="DataTable"/> in <paramref name="dataSet"/> as CSV data.
            /// </summary>
            /// <remarks>
            /// This method writes all the data in the first table of <paramref name="dataSet"/> to this <c>CsvWriter</c>, optionally writing a header
            /// record based on the columns in the table.
            /// </remarks>
            /// <param name="dataSet">
            /// The <c>DataSet</c> whose first table is to be written as CSV data.
            /// </param>
            /// <param name="writeHeaderRecord">
            /// If <see langword="true"/>, a CSV header will be written based on the column names for the table.
            /// </param>
            public void WriteAll(DataSet dataSet, bool writeHeaderRecord) {
                                
                if (dataSet.Tables.Count == 0) throw new Exception("The specified DataSet does not contain a table to write.");
                
                WriteAll(dataSet.Tables[0], writeHeaderRecord);
            }


            /// <summary>
            /// Writes the specified record to the target <see cref="TextWriter"/>, ensuring all values are correctly separated and escaped.
            /// </summary>
            /// <remarks>
            /// This method is used internally by the <c>CsvWriter</c> to write CSV records.
            /// </remarks>
            /// <param name="record">
            /// The record to be written.
            /// </param>
            /// <param name="incrementRecordNumber">
            /// <see langword="true"/> if the record number should be incremented, otherwise <see langword="false"/>.
            /// </param>
            private void WriteRecord(IEnumerable<string> record, bool incrementRecordNumber) {
                bool firstValue = true;

                foreach (string value in record) {
                    if (!firstValue) {
                        _writer.Write(_valueSeparator);
                    } else {
                        firstValue = false;
                    }

                    WriteValue(value);
                }

                //uses the underlying TextWriter.NewLine property
                _writer.WriteLine();
                _passedFirstRecord = true;

                if (incrementRecordNumber) {
                    ++_recordNumber;
                }
            }

            /// <summary>
            /// Writes the specified value to the target <see cref="TextWriter"/>, ensuring it is correctly escaped.
            /// </summary>
            /// <remarks>
            /// This method is used internally by the <c>CsvWriter</c> to write individual CSV values.
            /// </remarks>
            /// <param name="val">
            /// The value to be written.
            /// </param>
            private void WriteValue(string val) {
                _valueBufferEndIndex = 0;
                bool delimit = _alwaysDelimit;

                if (!string.IsNullOrEmpty(val)) {
                    //delimit to preserve white-space at the beginning or end of the value
                    if ((val[0] == SPACE) || (val[val.Length - 1] == SPACE)) {
                        delimit = true;
                    }

                    for (int i = 0; i < val.Length; ++i) {
                        char c = val[i];

                        if ((c == _valueSeparator) || (c == CR) || (c == LF)) {
                            //all these characters require the value to be delimited
                            AppendToValue(c);
                            delimit = true;
                        } else if (c == _valueDelimiter) {
                            //escape the delimiter by writing it twice
                            AppendToValue(_valueDelimiter);
                            AppendToValue(_valueDelimiter);
                            delimit = true;
                        } else {
                            AppendToValue(c);
                        }
                    }
                }

                if (delimit) {
                    _writer.Write(_valueDelimiter);
                }

                //write the value
                _writer.Write(_valueBuffer, 0, _valueBufferEndIndex);

                if (delimit) {
                    _writer.Write(_valueDelimiter);
                }

                _valueBufferEndIndex = 0;
            }

            /// <summary>
            /// Appends the specified character onto the end of the current value.
            /// </summary>
            /// <param name="c">
            /// The character to append.
            /// </param>
            private void AppendToValue(char c) {
                EnsureValueBufferCapacity(1);
                _valueBuffer[_valueBufferEndIndex++] = c;
            }

            /// <summary>
            /// Ensures the value buffer contains enough space for <paramref name="count"/> more characters.
            /// </summary>
            private void EnsureValueBufferCapacity(int count) {
                if ((_valueBufferEndIndex + count) > _valueBuffer.Length) {
                    char[] newBuffer = new char[Math.Max(_valueBuffer.Length * 2, (count >> 1) << 2)];

                    //profiling revealed a loop to be faster than Array.Copy, despite Array.Copy having an internal implementation
                    for (int i = 0; i < _valueBufferEndIndex; ++i) {
                        newBuffer[i] = _valueBuffer[i];
                    }

                    _valueBuffer = newBuffer;
                }
            }

        }
        

        /// <summary>
        /// Implements the CSV parser.
        /// </summary>
        /// <remarks>
        /// This class implements the CSV parsing capabilities of KBCsv.
        /// </remarks>
        internal sealed class CsvParser : IDisposable {
            
            /// <summary>
            /// The source of the CSV data.
            /// </summary>
            private readonly TextReader _reader;

            /// <summary>
            /// See <see cref="PreserveLeadingWhiteSpace"/>.
            /// </summary>
            private bool _preserveLeadingWhiteSpace;

            /// <summary>
            /// See <see cref="PreserveTrailingWhiteSpace"/>.
            /// </summary>
            private bool _preserveTrailingWhiteSpace;

            /// <summary>
            /// See <see cref="ValueSeparator"/>.
            /// </summary>
            private char _valueSeparator;

            /// <summary>
            /// See <see cref="ValueDelimiter"/>.
            /// </summary>
            private char _valueDelimiter;

            /// <summary>
            /// Buffers CSV data.
            /// </summary>
            private readonly char[] _buffer;

            /// <summary>
            /// The current index into <see cref="_buffer"/>.
            /// </summary>
            private int _bufferIndex;

            /// <summary>
            /// The last valid index into <see cref="_buffer"/>.
            /// </summary>
            private int _bufferEndIndex;

            /// <summary>
            /// The list of values currently parsed by the parser.
            /// </summary>
            private string[] _valueList;

            /// <summary>
            /// The last valid index into <see cref="_valueList"/>.
            /// </summary>
            public int _valuesListEndIndex;

            /// <summary>
            /// The buffer of characters containing the current value.
            /// </summary>
            private char[] _valueBuffer;

            /// <summary>
            /// The last valid index into <see cref="_valueBuffer"/>.
            /// </summary>
            private int _valueBufferEndIndex;

            /// <summary>
            /// An index into <see cref="_valueBuffer"/> indicating the first character that might be removed if it is leading white-space.
            /// </summary>
            private int _valueBufferFirstEligibleLeadingWhiteSpace;

            /// <summary>
            /// An index into <see cref="_valueBuffer"/> indicating the first character that might be removed if it is trailing white-space.
            /// </summary>
            private int _valueBufferFirstEligibleTrailingWhiteSpace;

            /// <summary>
            /// <see langword="true"/> if the current value is delimited and the parser is in the delimited area.
            /// </summary>
            private bool _inDelimitedArea;

            /// <summary>
            /// The starting index of the current value part.
            /// </summary>
            private int _valuePartStartIndex;

            /// <summary>
            /// Set to <see langword="true"/> once the first record is passed (or the <see cref="CsvReader"/> decides that the first record has been passed.
            /// </summary>
            private bool _passedFirstRecord;

            /// <summary>
            /// Used to quickly recognise whether a character is potentially special or not.
            /// </summary>
            private int _specialCharacterMask;

            /// <summary>
            /// The space character.
            /// </summary>
            private const char SPACE = ' ';

            /// <summary>
            /// The tab character.
            /// </summary>
            private const char TAB = '\t';

            /// <summary>
            /// The carriage return character. Escape code is <c>\r</c>.
            /// </summary>
            private const char CR = (char)0x0d;

            /// <summary>
            /// The line-feed character. Escape code is <c>\n</c>.
            /// </summary>
            private const char LF = (char)0x0a;

            /// <summary>
            /// One char less than the size of the internal buffer. The extra char is used to support a faster peek operation.
            /// </summary>
            private const int BUFFER_SIZE = 2047;

            /// <summary>
            /// The default value separator.
            /// </summary>
            public const char DefaultValueSeparator = ',';

            /// <summary>
            /// The default value delimiter.
            /// </summary>
            public const char DefaultValueDelimiter = '"';

            /// <summary>
            /// Gets or sets a value indicating whether leading whitespace is to be preserved.
            /// </summary>
            public bool PreserveLeadingWhiteSpace {
                get {
                    return _preserveLeadingWhiteSpace;
                }
                set {
                    _preserveLeadingWhiteSpace = value;
                }
            }

            /// <summary>
            /// Gets or sets a value indicating whether trailing whitespace is to be preserved.
            /// </summary>
            public bool PreserveTrailingWhiteSpace {
                get {
                    return _preserveTrailingWhiteSpace;
                }
                set {
                    _preserveTrailingWhiteSpace = value;
                }
            }

            /// <summary>
            /// Gets or sets the character that separates values in the CSV data.
            /// </summary>
            public char ValueSeparator {
                get {
                    return _valueSeparator;
                }
                set {
                    if (value == _valueDelimiter) throw new Exception("The value separator and value delimiter must be different.");
                    if(value == SPACE)throw new Exception("Space is not a valid value separator or delimiter.");

                    _valueSeparator = value;
                    UpdateSpecialCharacterMask();
                }
            }

            /// <summary>
            /// Gets or sets the character that optionally delimits values in the CSV data.
            /// </summary>
            public char ValueDelimiter {
                get {
                    return _valueDelimiter;
                }
                set {
                    if (value == _valueDelimiter) throw new Exception("The value separator and value delimiter must be different.");
                    if (value == SPACE) throw new Exception("Space is not a valid value separator or delimiter.");

                    _valueDelimiter = value;
                    UpdateSpecialCharacterMask();
                }
            }

            /// <summary>
            /// Gets a value indicating whether the parser's buffer contains more records in addition to those already parsed.
            /// </summary>
            public bool HasMoreRecords {
                get {
                    if (_bufferIndex < _bufferEndIndex) {
                        //the buffer isn't empty so there must be more records
                        return true;
                    } else {
                        //the buffer is empty so peek into the reader to see whether there is more data
                        return (_reader.Peek() != -1);
                    }
                }
            }

            /// <summary>
            /// Gets a value indicating whether the parser has passed the first record in the input source.
            /// </summary>
            public bool PassedFirstRecord {
                get {
                    return _passedFirstRecord;
                }
                set {
                    _passedFirstRecord = value;
                }
            }

            /// <summary>
            /// Constructs and inititialises an instance of <c>CsvParser</c> with the details provided.
            /// </summary>
            /// <param name="reader">
            /// The instance of <see cref="TextReader"/> from which CSV data will be read.
            /// </param>
            public CsvParser(TextReader reader) {
                                _reader = reader;
                //the extra char is used to facilitate a faster peek operation
                _buffer = new char[BUFFER_SIZE + 1];
                _valueList = new string[16];
                _valueBuffer = new char[128];
                _valuePartStartIndex = -1;
                //set defaults
                _valueSeparator = DefaultValueSeparator;
                _valueDelimiter = DefaultValueDelimiter;
                //get the default special character mask
                UpdateSpecialCharacterMask();
            }

            /// <summary>
            /// Efficiently skips the next CSV record.
            /// </summary>
            /// <returns>
            /// <see langword="true"/> if a record was successfully skipped, otherwise <see langword="false"/>.
            /// </returns>
            public bool SkipRecord() {
                if (HasMoreRecords) {
                    //taking a local copy allows optimisations that otherwise could not be performed because the CLR knows that no other thread
                    //can touch our local reference
                    char[] buffer = _buffer;

                    while (true) {
                        if (_bufferIndex != _bufferEndIndex) {
                            char c = buffer[_bufferIndex++];

                            if ((c & _specialCharacterMask) == c) {
                                if (!_inDelimitedArea) {
                                    if (c == _valueDelimiter) {
                                        //found a delimiter so enter delimited area and set the start index for the value
                                        _inDelimitedArea = true;
                                    } else if (c == CR) {
                                        if (buffer[_bufferIndex] == LF) {
                                            SwallowChar();
                                        }

                                        return true;
                                    } else if (c == LF) {
                                        return true;
                                    }
                                } else if (c == _valueDelimiter) {
                                    if (buffer[_bufferIndex] != _valueDelimiter) {
                                        //delimiter isn't escaped so we are no longer in a delimited area
                                        _inDelimitedArea = false;
                                    }
                                }
                            }
                        } else if (!FillBufferIgnoreValues()) {
                            //data exhausted - get out of here
                            return true;
                        }
                    }
                } else {
                    //no more records - can't skip
                    return false;
                }
            }

            /// <summary>
            /// Reads and parses the CSV into a <c>string</c> array containing the values contained in a single CSV record.
            /// </summary>
            /// <returns>
            /// An array of field values for the record, or <see langword="null"/> if no record was found.
            /// </returns>
            public string[] ParseRecord() {
                _valuesListEndIndex = 0;
                char c = char.MinValue;
                //taking a local copy allows optimisations that otherwise could not be performed because the CLR knows that no other thread
                //can touch our local reference
                char[] buffer = _buffer;

                while (true) {
                    if (_bufferIndex != _bufferEndIndex) {
                        if (_valuePartStartIndex == -1) {
                            _valuePartStartIndex = _bufferIndex;
                        }

                        c = buffer[_bufferIndex++];

                        if ((c & _specialCharacterMask) == c) {
                            if (!_inDelimitedArea) {
                                if (c == _valueDelimiter) {
                                    //found a delimiter so enter delimited area and set the start index for the value
                                    _inDelimitedArea = true;
                                    CloseValuePartExcludeCurrent();
                                    _valuePartStartIndex = _bufferIndex;

                                    //we have to make sure that delimited text isn't stripped if it is white-space
                                    if (_valueBufferFirstEligibleLeadingWhiteSpace == 0) {
                                        _valueBufferFirstEligibleLeadingWhiteSpace = _valueBufferEndIndex;
                                    }
                                } else if (c == _valueSeparator) {
                                    CloseValue(false);
                                } else if (c == CR) {
                                    CloseValue(false);

                                    if (buffer[_bufferIndex] == LF) {
                                        SwallowChar();
                                    }

                                    break;
                                } else if (c == LF) {
                                    CloseValue(false);
                                    break;
                                }
                            } else if (c == _valueDelimiter) {
                                if (buffer[_bufferIndex] == _valueDelimiter) {
                                    CloseValuePart();
                                    SwallowChar();
                                    _valuePartStartIndex = _bufferIndex;
                                } else {
                                    //delimiter isn't escaped so we are no longer in a delimited area
                                    _inDelimitedArea = false;
                                    CloseValuePartExcludeCurrent();
                                    _valuePartStartIndex = _bufferIndex;
                                    //we have to make sure that delimited text isn't stripped if it is white-space
                                    _valueBufferFirstEligibleTrailingWhiteSpace = _valueBufferEndIndex;
                                }
                            }
                        }
                    } else if (!FillBuffer()) {
                        //special case: if the last character was a separator we need to add a blank value. eg. CSV "Value," will result in "Value", ""
                        if (c == _valueSeparator) {
                            AddValue(string.Empty);
                        }

                        //data exhausted - get out of loop
                        break;
                    }
                }

                //this will return null if there are no values
                return GetValues();
            }

            /// <summary>
            /// Closes the current value part.
            /// </summary>
            private void CloseValuePart() {
                AppendToValue(_valuePartStartIndex, _bufferIndex);
                _valuePartStartIndex = -1;
            }

            /// <summary>
            /// Closes the current value part, but excludes the current character from the value part.
            /// </summary>
            private void CloseValuePartExcludeCurrent() {
                AppendToValue(_valuePartStartIndex, _bufferIndex - 1);
            }

            /// <summary>
            /// Closes the current value by adding it to the list of values in the current record. Assumes that there is actually a value to add, either in <c>_value</c> or in
            /// <see cref="_buffer"/> starting at <see cref="_valuePartStartIndex"/> and ending at <see cref="_bufferIndex"/>.
            /// </summary>
            /// <param name="includeCurrentChar">
            /// If <see langword="true"/>, the current character is included in the value. Otherwise, it is excluded.
            /// </param>
            private void CloseValue(bool includeCurrentChar) {
                int endIndex = _bufferIndex;

                if ((!includeCurrentChar) && (endIndex > _valuePartStartIndex)) {
                    endIndex -= 1;
                }
                
                if (_valueBufferEndIndex == 0) {
                    if (endIndex == 0) {
                        AddValue(string.Empty);
                    } else {
                        //the value did not require the use of the ValueBuilder
                        int startIndex = _valuePartStartIndex;
                        //taking a local copy allows optimisations that otherwise could not be performed because the CLR knows that no other thread
                        //can touch our local reference
                        char[] buffer = _buffer;

                        if (!_preserveLeadingWhiteSpace) {
                            //strip all leading white-space
                            while ((startIndex < endIndex) && (IsWhiteSpace(buffer[startIndex]))) {
                                ++startIndex;
                            }
                        }

                        if (!_preserveTrailingWhiteSpace) {
                            //strip all trailing white-space
                            while ((endIndex > startIndex) && (IsWhiteSpace(buffer[endIndex - 1]))) {
                                --endIndex;
                            }
                        }

                        AddValue(new string(buffer, startIndex, endIndex - startIndex));
                    }
                } else {
                    //we needed the ValueBuilder to compose the value
                    AppendToValue(_valuePartStartIndex, endIndex);

                    if (!_preserveLeadingWhiteSpace || !_preserveTrailingWhiteSpace) {
                        //strip all white-space prior to _valueBufferFirstEligibleLeadingWhiteSpace and after _valueBufferFirstEligibleTrailingWhiteSpace
                        AddValue(GetValue(_valueBufferFirstEligibleLeadingWhiteSpace, _valueBufferFirstEligibleTrailingWhiteSpace));
                    } else {
                        AddValue(GetValue());
                    }

                    _valueBufferEndIndex = 0;
                    _valueBufferFirstEligibleLeadingWhiteSpace = 0;
                }

                _valuePartStartIndex = -1;
            }

            /// <summary>
            /// Determines whether <paramref name="c"/> is white-space.
            /// </summary>
            /// <param name="c">
            /// The character to check.
            /// </param>
            /// <returns>
            /// <see langword="true"/> if <paramref name="c"/> is white-space, otherwise <see langword="false"/>.
            /// </returns>
            internal static bool IsWhiteSpace(char c) {
                return ((c == SPACE) || (c == TAB));
            }

            /// <summary>
            /// Fills that data buffer. Assumes that the buffer is already exhausted.
            /// </summary>
            /// <returns>
            /// <see langword="true"/> if data was read into the buffer, otherwise <see langword="false"/>.
            /// </returns>
            private bool FillBuffer() {
                
                //may need to close a value or value part depending on the state of the stream
                if (_valuePartStartIndex != -1) {
                    if (_reader.Peek() != -1) {
                        CloseValuePart();
                        _valuePartStartIndex = 0;
                    } else {
                        CloseValue(true);
                    }
                }

                _bufferEndIndex = _reader.Read(_buffer, 0, BUFFER_SIZE);
                //this is possible because the buffer is one char bigger than BUFFER_SIZE. This fact is used to implement a faster peek operation
                _buffer[_bufferEndIndex] = (char)_reader.Peek();
                _bufferIndex = 0;
                _passedFirstRecord = true;
                return (_bufferEndIndex > 0);
            }

            /// <summary>
            /// Fills the buffer with data, but does not bother with closing values. This is used from the <see cref="SkipRecord"/> method,
            /// since that does not concern itself with values.
            /// </summary>
            /// <returns>
            /// <see langword="true"/> if data was read into the buffer, otherwise <see langword="false"/>.
            /// </returns>
            private bool FillBufferIgnoreValues() {                
                _bufferEndIndex = _reader.Read(_buffer, 0, BUFFER_SIZE);
                //this is possible because the buffer is one char bigger than BUFFER_SIZE. This fact is used to implement a faster peek operation
                _buffer[_bufferEndIndex] = (char)_reader.Peek();
                _bufferIndex = 0;
                _passedFirstRecord = true;
                return (_bufferEndIndex > 0);
            }


            /// <summary>
            /// Swallows the current character in the data buffer. Assumes that there is a character to swallow, but refills the buffer if necessary.
            /// </summary>
            private void SwallowChar() {
                if (_bufferIndex < BUFFER_SIZE) {
                    //in this case there are still unread chars in the buffer so just skip one
                    ++_bufferIndex;
                } else if (_bufferIndex < _bufferEndIndex) {
                    //in this case we are pointing to the second-to-last char in the buffer, so we need to refill the buffer since the last char is a peeked char
                    FillBuffer();
                } else {
                    //in this case we are pointing to the last char in the buffer, which is a peeked char. therefore, we need to refill and skip past that char
                    FillBuffer();
                    ++_bufferIndex;
                }
            }

            /// <summary>
            /// Disposes of this <c>CsvParser</c> instance.
            /// </summary>
            void IDisposable.Dispose() {
                Close();
            }

            /// <summary>
            /// Closes this <c>CsvParser</c> instance and releases all resources acquired by it.
            /// </summary>
            public void Close() {
                if (_reader != null) {
                    _reader.Close();
                }
            }

            /// <summary>
            /// Adds a value to the value list.
            /// </summary>
            /// <param name="val">
            /// The value to add.
            /// </param>
            private void AddValue(string val) {
                EnsureValueListCapacity();
                _valueList[_valuesListEndIndex++] = val;
            }

            /// <summary>
            /// Gets an array of values that have been added to <see cref="_valueList"/>.
            /// </summary>
            /// <returns>
            /// An array of type <c>string</c> containing all the values in the value list, or <see langword="null"/> if there are no values in the list.
            /// </returns>
            private string[] GetValues() {
                if (_valuesListEndIndex > 0) {
                    string[] retVal = new string[_valuesListEndIndex];

                    for (int i = 0; i < _valuesListEndIndex; ++i) {
                        retVal[i] = _valueList[i];
                    }

                    return retVal;
                } else {
                    return null;
                }
            }

            /// <summary>
            /// Ensures the value list contains enough space for another value, and increases its size if not.
            /// </summary>
            private void EnsureValueListCapacity() {
                if (_valuesListEndIndex == _valueList.Length) {
                    string[] newBuffer = new string[_valueList.Length * 2];

                    for (int i = 0; i < _valuesListEndIndex; ++i) {
                        newBuffer[i] = _valueList[i];
                    }

                    _valueList = newBuffer;
                }
            }

            /// <summary>
            /// Appends the specified characters from <see cref="_buffer"/> onto the end of the current value.
            /// </summary>
            /// <param name="startIndex">
            /// The index at which to begin copying.
            /// </param>
            /// <param name="endIndex">
            /// The index at which to cease copying. The character at this index is not copied.
            /// </param>
            private void AppendToValue(int startIndex, int endIndex) {
                EnsureValueBufferCapacity(endIndex - startIndex);
                char[] valueBuffer = _valueBuffer;
                char[] buffer = _buffer;

                //profiling revealed a loop to be faster than Array.Copy
                //in addition, profiling revealed that taking a local copy of the _buffer reference impedes performance here
                for (int i = startIndex; i < endIndex; ++i) {
                    valueBuffer[_valueBufferEndIndex++] = buffer[i];
                }
            }

            /// <summary>
            /// Gets the current value.
            /// </summary>
            /// <returns></returns>
            private string GetValue() {
                return new string(_valueBuffer, 0, _valueBufferEndIndex);
            }

            /// <summary>
            /// Gets the current value, optionally removing trailing white-space.
            /// </summary>
            /// <param name="valueBufferFirstEligibleLeadingWhiteSpace">
            /// The index of the first character that cannot possibly be leading white-space.
            /// </param>
            /// <param name="valueBufferFirstEligibleTrailingWhiteSpace">
            /// The index of the first character that may be trailing white-space.
            /// </param>
            /// <returns>
            /// An instance of <c>string</c> containing the resultant value.
            /// </returns>
            private string GetValue(int valueBufferFirstEligibleLeadingWhiteSpace, int valueBufferFirstEligibleTrailingWhiteSpace) {
                int startIndex = 0;
                int endIndex = _valueBufferEndIndex - 1;

                if (!_preserveLeadingWhiteSpace) {
                    while ((startIndex < valueBufferFirstEligibleLeadingWhiteSpace) && (IsWhiteSpace(_valueBuffer[startIndex]))) {
                        ++startIndex;
                    }
                }

                if (!_preserveTrailingWhiteSpace) {
                    while ((endIndex >= valueBufferFirstEligibleTrailingWhiteSpace) && (IsWhiteSpace(_valueBuffer[endIndex]))) {
                        --endIndex;
                    }
                }

                return new string(_valueBuffer, startIndex, endIndex - startIndex + 1);
            }

            /// <summary>
            /// Ensures the value buffer contains enough space for <paramref name="count"/> more characters.
            /// </summary>
            private void EnsureValueBufferCapacity(int count) {
                if ((_valueBufferEndIndex + count) > _valueBuffer.Length) {
                    char[] newBuffer = new char[Math.Max(_valueBuffer.Length * 2, (count >> 1) << 2)];

                    //profiling revealed a loop to be faster than Array.Copy, despite Array.Copy having an internal implementation
                    for (int i = 0; i < _valueBufferEndIndex; ++i) {
                        newBuffer[i] = _valueBuffer[i];
                    }

                    _valueBuffer = newBuffer;
                }
            }

            /// <summary>
            /// Updates the mask used to quickly filter out non-special characters.
            /// </summary>
            private void UpdateSpecialCharacterMask() {
                _specialCharacterMask = _valueSeparator | _valueDelimiter | CR | LF;
            }
        }

    }
}
