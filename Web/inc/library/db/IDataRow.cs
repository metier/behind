﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.db
{
	public interface IDataRow
	{
		//Properties
		object this[int Index] { get; set; }
		object this[string Key] { get; set; }

		//Methods
		object GetColumnByIndex(int Index);
		object GetColumnByName(string Name);
        object GetValueByFieldName(string Name);
        string GetFieldNameByIndex(int Index);
		int GetColumnCount();
		System.Data.DataRowAction RecordState { get; set; }
	}
}
