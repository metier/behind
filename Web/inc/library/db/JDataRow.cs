﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace jlib.db
{
	public class JDataRow : DataRow, IDataRow 
	{
		public JDataRow() : base(null)
		{
		}

		public JDataRow(DataRowBuilder builder) : base(builder)
		{
		}

		#region ***** IDataRow Methods *****
		object IDataRow.GetColumnByIndex(int Index)
		{
			return this.Table.Columns[Index];
		}
		object IDataRow.GetColumnByName(string Name)
		{
            if (!this.Table.Columns.Contains(Name)) return null;
			return this.Table.Columns[Name];
		}
        object IDataRow.GetValueByFieldName(string Name) {
            if (!this.Table.Columns.Contains(Name)) return null;
            return this[Name];
        }
        string IDataRow.GetFieldNameByIndex(int Index) {
            return this.Table.Columns[Index].ColumnName;
        }
		int IDataRow.GetColumnCount()
		{
			return this.Table.Columns.Count;
		}
		#endregion

		#region ***** IDataRow Properties *****
		public DataRowAction RecordState
		{
			get 
			{

				if (this.RowState == DataRowState.Added) return DataRowAction.Add;
				else if (this.RowState == DataRowState.Deleted) return DataRowAction.Delete;
				else if (this.RowState == DataRowState.Modified) return DataRowAction.Change;
				else if (this.RowState == DataRowState.Unchanged) return DataRowAction.Nothing;
				else return DataRowAction.Add;
			}
			set 
			{ 
				//Do nothing
			}
		}
		#endregion
	}
}
