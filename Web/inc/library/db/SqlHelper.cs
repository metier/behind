﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;
using System.Configuration;
using System.Net;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.OleDb;
using System.Collections;
using System.Threading;
using jlib.helpers.Structures;

namespace jlib.db
{
	public class SqlHelper : IDisposable
	{
		#region DELEGATES / EVENTS
		/// <summary>
		/// The delegate to be called when a Async Request Timeout
		/// </summary>
		/// <param name="RequestState">The State Details of the Request</param>
		public delegate void TimeoutCallbackDelegate(AsyncExecuteState RequestState);
		public delegate void AsyncCallbackDelegate(AsyncExecuteState RequestState, DbDataReader DBReader, DataSet DataSet);

		public delegate void QueryCompleteDelegate(QueryCompleteEventArgs QueryCompleteEventArgs);
		public event QueryCompleteDelegate QueryComplete;
		#endregion



		#region CONSTRUCTORS
		/// <summary>
		/// Creates a SqlHelper Object using the Default ConnectionBuilder.
		///		ConnectionBuilder.Source = AutoDetect
		/// </summary>
		public SqlHelper() : this(new ConnectionBuilder.Sql()) { }
		/// <summary>
		/// Creates a SqlHelper Object using the provided ConnectionString
		/// </summary>
		public SqlHelper(string ConnectionString) : this(new ConnectionBuilder.Sql(ConnectionBuilder.ConnectionStringSources.Custom) { ConnectionString = ConnectionString }) { }
		/// <summary>
		/// Creates a SqlHelper Object using the provided ConnectionBuilder.
		/// </summary>
		public SqlHelper(ConnectionBuilder.Sql SqlConnectionBuilder)
		{
            this.SqlConnectionBuilder = SqlConnectionBuilder;
            this.ConnectionType = ConnectionTypes.Unknown;
		}

		#endregion

		#region CONSTRUCTORS: Static
		/// <summary>
		/// SqlHelper Create Factory to create an instance using AutoDetect Connection Mode provided by a Key.
		/// </summary>
		/// <param name="Key">The Key used to identify this connection</param>
		/// <returns>An instance of a SqlHelper</returns>
		public static SqlHelper SqlHelperCreateAutoDetect(string Key)
		{
			return new SqlHelper(new ConnectionBuilder.Sql(ConnectionBuilder.ConnectionStringSources.AutoDetect) { Key = Key });
		}
		/// <summary>
		/// SqlHelper Create Factory to create an instance using AppSettings to read the connection string.
		/// </summary>
		/// <param name="Key">The Key used to identify this connection</param>
		/// <returns>An instance of a SqlHelper</returns>
		public static SqlHelper SqlHelperCreateAppSettings(string Key)
		{
			return new SqlHelper(new ConnectionBuilder.Sql(ConnectionBuilder.ConnectionStringSources.AppSettings) { Key = Key });
		}
		/// <summary>
		/// SqlHelper Create Factory to create an instance using Connection Strings to read the connection string.
		/// </summary>
		/// <param name="Key">The Key used to identify this connection</param>
		/// <returns>An instance of a SqlHelper</returns>
		public static SqlHelper SqlHelperCreateConnectionStrings(string Key)
		{
			return new SqlHelper(new ConnectionBuilder.Sql(ConnectionBuilder.ConnectionStringSources.ConnectionStrings) { Key = Key });
		}
		#endregion


		#region PROPERTIES: Objects
		/// <summary>
		/// The ConnectionBuilder Object used to create a Sql Connection
		/// </summary>
		public ConnectionBuilder.Sql SqlConnectionBuilder { get; set; }

		/// <summary>
		/// Sql Connection Scope
		/// </summary>
        public System.Data.Common.DbConnection DbConnection { get; set; }

		/// <summary>
		/// Sql Transaction Scope
		/// </summary>
        public System.Data.Common.DbTransaction DbTransaction { get; set; }

		/// <summary>
		/// Global Timeout Callback Method called when an Async request is made and it times-out.
		/// </summary>
		public TimeoutCallbackDelegate TimeoutCallback { get; set; }
		private List<TimeoutCallbackDelegate> _InternalTimeoutCallbacks = new List<TimeoutCallbackDelegate>();

		/// <summary>
		/// Global Async Callback Method called when an Async request is complete.
		/// </summary>
		public AsyncCallbackDelegate AsyncExecuteCallback { get; set; }
		private List<AsyncCallbackDelegate> _InternalAsyncExecuteCallbacks = new List<AsyncCallbackDelegate>();

		/// <summary>
		/// Asynchronous Flag to set complete
		/// </summary>
		public ManualResetEvent DoneEvent
		{
			get { return this._DoneEvent; }
			set { this._DoneEvent = value; }
		}
		private ManualResetEvent _DoneEvent = new ManualResetEvent(false);

		public bool IsConnected { get; set; }

		#endregion

		#region PROPERTIES: Return Values

		/// <summary>
		/// The Execution Time that it took to perform the query
		/// </summary>
		public TimeSpan ExecutionTime
		{
			get { return this._ExecutionTime; }
		}
		private TimeSpan _ExecutionTime = new TimeSpan(0);
		private DateTime _StartTime = DateTime.Now;

		public DbDataAdapter SqlAdapter
		{
			get { return this._SqlAdapter; }
            protected set { this._SqlAdapter = value; }
		}
        private DbDataAdapter _SqlAdapter = null;
		#endregion

		#region PROPERTIES: Settings
		/// <summary>
		/// The default timeout used for the command object
		/// </summary>
		public int CommandTimeout
		{
			get { return this._CommandTimeout; }
			set { this._CommandTimeout = value; }
		}
		private int _CommandTimeout = 1800;

        /// <summary>
        /// The type of the active connection
        /// </summary>
        public enum ConnectionTypes {
            Unknown,
            MSSQL,
            Oracle
        }
        public ConnectionTypes ConnectionType { get; set; }

        /// <summary>
        /// Flag to indicate that triggers should not fire
        /// </summary>
        public bool DisableTriggers { get; set; }

		/// <summary>
		/// The collection of DataTable Names used for multi-dimensional queries
		/// </summary>
		public string[] DataTableNames
		{
			get { return this._DataTableNames; }
			set { this._DataTableNames = value; }
		}
		private string[] _DataTableNames = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20" };

		/// <summary>
		/// This setting determines if the results are read from the SqlDataReader into the DataSet
		/// </summary>
		public bool FillDataSetOnExecute
		{
			get { return this._PopulateDataSetFromReader; }
			set { this._PopulateDataSetFromReader = value; }
		}
		/// <summary>
		/// This setting determines if the results are read from the SqlDataReader into the DataSet
		/// </summary>
		public bool PopulateDataSetFromReader
		{
			get { return this._PopulateDataSetFromReader; }
			set { this._PopulateDataSetFromReader = value; }
		}
		private bool _PopulateDataSetFromReader = true;

		/// <summary>
		/// Fills the dataset object after the Execute/Update method is called
		/// </summary>
		/*
		public bool FillDatasetOnExecute
		{
			get { return this._FillDatasetOnExecute; }
			set { this._FillDatasetOnExecute = value; }
		}
		private bool _FillDatasetOnExecute = true;
		*/
		#endregion



		#region METHODS: Helper Functions
		/// <summary>
		/// Clone the current SQL Helper
		///		- Creates a new SqlHelper with the same Connection Builder Object
		/// </summary>
		/// <returns></returns>
		public SqlHelper Clone()
		{
            return new SqlHelper(this.SqlConnectionBuilder);
		}

		/// <summary>
		/// Establishes Connection To Data Source
		/// </summary>
		public DbConnection Connect(bool CreateTransaction = true)
		{
			///Raise Pre-Connect Event
			///.

			//Establish New Connection
            string sConnectionString = this.SqlConnectionBuilder.BuildConnectionString();
            if (this.ConnectionType == ConnectionTypes.Oracle)
                this.DbConnection = new OleDbConnection(sConnectionString);
            else
                this.DbConnection = new SqlConnection(sConnectionString);

			//Open Connection
			this.DbConnection.Open();

			//Create Transaction
			if(CreateTransaction) this.DbTransaction = this.DbConnection.BeginTransaction();

			///Raise Post-Connect Event
			///.
			///

			//Change Connected State Flag
			this.IsConnected = true;

			//Return Connection
			return this.DbConnection;

		}


		/// <summary>
		/// Disconnects from Data Source
		/// </summary>
		public void Disconnect()
		{
			this.Disconnect(this.DbConnection);

			
			this.DbConnection = null;
		}

		/// <summary>
		/// Disconnects from Data Source
		/// </summary>
		public void Disconnect(DbConnection SqlConnection)
		{
			///Raise Pre-Disconnect Event
			///.

			//Close Transaction
			this.CommitTransaction();

			//Open Connection
			if(this.DbConnection != null)
				this.DbConnection.Close();

			///Raise Post-Disconnect Event
			///.

			//Change Connected Flag State
			this.IsConnected = false;
		}

		public bool CommitTransaction()
		{
			if(this.DbTransaction != null)
			{
				this.DbTransaction.Commit();
				this.DbTransaction = null;

				return true;
			}

			return false;
		}

		public bool RollbackTransaction()
		{
			if(this.DbTransaction != null)
			{
				this.DbTransaction.Rollback();
				this.DbTransaction = null;

				return true;
			}

			return false;
		}

		/// <summary>
		/// Builds a New Command Object
		/// </summary>
		/// <param name="Command">The Command Text</param>
		/// <param name="SqlConnection">The Open Connection Object</param>
		/// <param name="CommandType">The Command Type</param>
		/// <param name="CommandTimeout">The Command Timeout</param>
		/// <returns>A Newly Constructed Command Object</returns>
		private DbCommand BuildCommandObject(DbConnection SqlConnection, System.Data.CommandType CommandType, string Command, int CommandTimeout)
		{
			//Create Command Object
            DbCommand dbCommand = null;
            if (SqlConnection as SqlConnection != null) dbCommand = new SqlCommand(Command, SqlConnection as SqlConnection);
            else dbCommand = new OleDbCommand(Command, SqlConnection as OleDbConnection);

            dbCommand.CommandType = CommandType;
            dbCommand.CommandTimeout = CommandTimeout;

			//Set Transaction
			if (this.DbTransaction != null)
                dbCommand.Transaction = this.DbTransaction;

			//Return Command Obect
            return dbCommand;
		}


		/// <summary>
		/// Builds a New Parameter Collection within an existing Command Object
		/// </summary>
		/// <param name="Command">The Existing Command Object</param>
		/// <param name="Parameters">The Parameter Collection</param>
		/// <returns>The New SqlParameterCollection</returns>
        private DbParameterCollection AddParameters(DbCommand Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{
            for(int x=0;x<Parameters.Count;x++){
                if (Parameters[x] as OleDbParameter != null) Parameters[x].ParameterName = "o_" + Parameters[x].ParameterName.Substring(1);
				//Add Parameter To Collection
                Command.Parameters.Add(Parameters[x]);
                //This is a hack for the OleDB provider. Otherwise, it might pass inn 0, which will cause the Oracle Provider to throw an error
                if (Parameters[x].DbType == DbType.AnsiStringFixedLength) Parameters[x].Size = Parameters[x].Size;
                if (Parameters[x].Value == null) Parameters[x].Value = DBNull.Value;
			}

			return Command.Parameters;
		}

		/// <summary>
		/// Builds a New Parameter Collection within an existing Command Object
		/// </summary>
		/// <param name="Command">The Existing Command Object</param>
		/// <param name="Parameters">The Parameter Collection:  AutoDetects if the array is a @n1,v1,@n2,v2 arrangement or just a collection of SqlParameters</param>
		/// <returns>The New SqlParameterCollection</returns>
        public jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> BuildParameterCollection(params object[] Parameters)
		{
			//Create SqlParameter List 
            jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> ParameterCollection = new jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter>();

			//if no parameters, then return
			if (Parameters.Length == 0)
				return ParameterCollection;

			//if already a sortedlist, then skip
            if (Parameters[0] as jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> != null)
                return (Parameters[0] as jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter>);


            for (int iParameter = 0; iParameter < Parameters.Length; iParameter++) {
                object CurrentParam = Parameters[iParameter];
                if (CurrentParam as DbParameter[] != null) {
                    foreach (DbParameter SqlParameter in CurrentParam as DbParameter[]) {
                        //Add Parameter To Collection
                        ParameterCollection.Add(SqlParameter.ParameterName, SqlParameter);
                    }
                }else if (CurrentParam as DbParameter != null) {
                    //Add Parameter To Collection
                    ParameterCollection.Add((CurrentParam as DbParameter).ParameterName, CurrentParam as DbParameter);
                }


                //If DynamicDictionary, then map
                else if (CurrentParam as DynamicDictionary != null) {
                    foreach (KeyValuePair<string, object> parameter in (CurrentParam as DynamicDictionary).Dictionary) {
                        //Get Parameter Name
                        string parameterName = "@" + parameter.Key;

                        //Build New Parameter
                        DbParameter SqlParameter = null;
                        if (this.ConnectionType == ConnectionTypes.Oracle)
                            SqlParameter = new OleDbParameter(parameterName, parameter.Value);
                        else
                            SqlParameter = new SqlParameter(parameterName, parameter.Value);

                        //Add Parameter To Collection
                        ParameterCollection.Add(parameterName, SqlParameter);
                    }
                }


                //if @n1,v1,@n2,v2, then build
                else {

                    //Build New Parameter
                    DbParameter SqlParameter = null;
                    if (this.ConnectionType == ConnectionTypes.Oracle)
                        SqlParameter = new OleDbParameter(Parameters[iParameter].ToString(), Parameters[iParameter + 1]);
                    else
                        SqlParameter = new SqlParameter(Parameters[iParameter].ToString(), Parameters[iParameter + 1]);

                    //Add Parameter To Collection
                    ParameterCollection.Add(Parameters[iParameter].ToString(), SqlParameter);
                    iParameter++;

                }
            }
			return ParameterCollection;
		}

		#endregion

		#region METHODS: Execute
		public DataTable ExecuteSql(string Command, params object[] Parameters)
		{
			//Execute Query
			return Execute(CommandType.Text, Command, this.BuildParameterCollection(Parameters));
		}
		public DataTable ExecuteSql(DbConnection SqlConnection, string Command, params object[] Parameters)
		{
			//Execute Query
			return Execute(SqlConnection, CommandType.Text, Command, this.BuildParameterCollection(Parameters));
		}
		public DataTable ExecuteSproc(string Command, params object[] Parameters)
		{
			//Execute Query
			return Execute(CommandType.StoredProcedure, Command, this.BuildParameterCollection(Parameters));
		}
		public DataTable ExecuteSproc(SqlConnection SqlConnection, string Command, params object[] Parameters)
		{
			//Execute Query
			return Execute(SqlConnection, CommandType.StoredProcedure, Command, this.BuildParameterCollection(Parameters));
		}
		public DataTable Execute(string Command, params object[] Parameters)
		{
			CommandType commandType = (Command.Trim().IndexOf(' ') > 0) ? CommandType.Text : CommandType.StoredProcedure;

			//Execute Query
			return Execute(commandType, Command, this.BuildParameterCollection(Parameters));
		}
        public DataTable Execute(string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{
			CommandType commandType = (Command.Trim().IndexOf(' ') > 0) ? CommandType.Text : CommandType.StoredProcedure;

			//Execute Query
			return Execute(commandType, Command, Parameters);
		}
		public DataTable Execute(System.Data.CommandType CommandType, string Command, params object[] Parameters)
		{
			//Execute Query
			return Execute(CommandType, Command, this.BuildParameterCollection(Parameters));
		}
        public DataTable Execute(System.Data.CommandType CommandType, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{

			//Get DataSet
			DataSet DataSet = this.ExecuteX(CommandType, Command, Parameters);

			//If No Datatables in DataSet, return Null
			if (DataSet.Tables.Count == 0)
				return null;

			//Get First DataTable
			return DataSet.Tables[0];
		}

        public DataTable Execute(DbConnection SqlConnection, System.Data.CommandType CommandType, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{
			//Get Results From Multi-Dimensional Query
			DataSet DataSet = this.ExecuteX(SqlConnection, CommandType, Command, Parameters);

			//If No Datatables in DataSet, return Null
			if (DataSet.Tables.Count == 0)
				return null;

			//Get First DataTable
			return DataSet.Tables[0];

		}

		#endregion

		#region METHODS: ExecuteX
		public DataSet ExecuteXSql(string Command, params object[] Parameters)
		{
			//Execute Query
			return ExecuteX(CommandType.Text, Command, this.BuildParameterCollection(Parameters));
		}
		public DataSet ExecuteXSproc(string Command, params object[] Parameters)
		{
			//Execute Query
			return ExecuteX(CommandType.StoredProcedure, Command, this.BuildParameterCollection(Parameters));
		}
		public DataSet ExecuteX(System.Data.CommandType CommandType, string Command, params object[] Parameters)
		{
			//Execute Query
			return ExecuteX(CommandType, Command, this.BuildParameterCollection(Parameters));
		}
        public DataSet ExecuteX(System.Data.CommandType CommandType, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{
			//Create Sql Connection (if not connected)
			bool useLocalConnection = false;
			if (this.DbConnection == null)
			{
				useLocalConnection = true;
				this.Connect();
			}

			//Execute Query
			DataSet dataSet = ExecuteX(this.DbConnection, CommandType, Command, Parameters);

			//Close Connection
			if(useLocalConnection) this.Disconnect();

			//return Results
			return dataSet;
		}

        public DataSet ExecuteX(DbConnection SqlConnection, System.Data.CommandType CommandType, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{

			try
			{
				//Start Execution Time Stamp
				this._StartTime = DateTime.Now;


				//Declare return DataSet
				DataSet dsResults = new DataSet();

				//Create Command Object
				if (CommandType == System.Data.CommandType.StoredProcedure && this.ConnectionType == ConnectionTypes.Oracle)
				{
					Command = "{call " + Command + "(";
					for (int x = 0; x < Parameters.Count; x++) Command += (x == 0 ? "" : ",") + "?";
					Command += ")}";
					CommandType = System.Data.CommandType.Text;
				}
				DbCommand SqlCommand = this.BuildCommandObject(SqlConnection, CommandType, Command, this.CommandTimeout);

                if ((this.ConnectionType == ConnectionTypes.MSSQL || this.ConnectionType == ConnectionTypes.Unknown) && DisableTriggers)
				{
					if (CommandType == System.Data.CommandType.Text)
                        SqlCommand.CommandText = "SET Context_Info 0x55555;" + SqlCommand.CommandText + "SET Context_Info 0;";
					else
					{
						SqlCommand TriggerCommand = new SqlCommand("SET Context_Info 0x55555;", SqlConnection as SqlConnection, SqlCommand.Transaction as SqlTransaction);
						TriggerCommand.ExecuteNonQuery();
					}
				}

				//Build Parameter Collection
				DbParameterCollection SqlParameters = this.AddParameters(SqlCommand, Parameters);

				//Execute and fill DataSet
				if (this.FillDataSetOnExecute)
				{
					//Build Adapter
					if (this.ConnectionType == ConnectionTypes.Oracle)
						this._SqlAdapter = new OleDbDataAdapter(SqlCommand as OleDbCommand);
					else
						this._SqlAdapter = new SqlDataAdapter(SqlCommand as SqlCommand);

					//Create DataSet
					this._SqlAdapter.Fill(dsResults);
				}

				//Just Execute
				else
					SqlCommand.ExecuteNonQuery();

				//Stop Time Counter
				this._ExecutionTime = DateTime.Now.Subtract(this._StartTime);

				//Throw Events
				if (this.QueryComplete != null)
					this.QueryComplete(new QueryCompleteEventArgs(this, SqlCommand, Parameters, dsResults, null, this._SqlAdapter));

				//Set Done
				this.DoneEvent.Set();

				//Return DataSet
				return dsResults;
			}
			catch (Exception e)
			{
                
				//rollback transaction
				this.RollbackTransaction();

				//throw error
				string ParameterString = "\n\nParams:\n";
                foreach (var param in Parameters)
                    ParameterString+=" * " + param.Key + " = " + param.Value.Value + "\n";
                    

				throw new Exception(String.Format("Error executing Sql because '{0}': \n{1}\n\n", e.Message, Command + ParameterString), e);
			}
		}

		#endregion
        
		#region METHODS: Execute-Reader
        public DbDataReader ExecuteReaderSql(string Command, params object[] Parameters)
		{
			//Execute Query
			return ExecuteReader(CommandType.Text, Command, this.BuildParameterCollection(Parameters));
		}
        public DbDataReader ExecuteReaderSproc(string Command, params object[] Parameters)
		{
			//Execute Query
			return ExecuteReader(CommandType.StoredProcedure, Command, this.BuildParameterCollection(Parameters));
		}
        public DbDataReader ExecuteReader(System.Data.CommandType CommandType, string Command, params object[] Parameters)
		{
			//Execute Query
			return ExecuteReader(CommandType, Command, this.BuildParameterCollection(Parameters));
		}
        public DbDataReader ExecuteReader(System.Data.CommandType CommandType, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{
			//Create Sql Connection (if not connected)
			bool useLocalConnection = false;
			if (this.DbConnection == null)
			{
				useLocalConnection = true;
				this.Connect();
			}

			//Execute Query
			DbDataReader sqlDataReader = ExecuteReader(this.DbConnection, CommandType, Command, Parameters);

			//Discconnect Connection
			if(useLocalConnection) this.Disconnect();

			//Return Results
			return sqlDataReader;
		}
        public DbDataReader ExecuteReader(DbConnection SqlConnection, System.Data.CommandType CommandType, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{
			//Start Execution Time Stamp
			this._StartTime = DateTime.Now;

			//Create Command Object
            DbCommand SqlCommand = this.BuildCommandObject(SqlConnection, CommandType, Command, this.CommandTimeout);

			//Build Parameter Collection
			DbParameterCollection SqlParameters = this.AddParameters(SqlCommand, Parameters);

			//Get Results
            DbDataReader SqlReader = SqlCommand.ExecuteReader();

			//Stop Time Counter
			this._ExecutionTime = DateTime.Now.Subtract(this._StartTime);

			//Throw Events
			if (this.QueryComplete != null)
				this.QueryComplete(new QueryCompleteEventArgs(this, SqlCommand, Parameters, null, SqlReader, null));

			//Set Done
			this.DoneEvent.Set();

			//Return DataSet
			return SqlReader;

		}
		#endregion

		#region METHODS: AsyncExecute
		public AsyncExecuteState AsyncExecuteSql(string Command, params object[] Parameters)
		{
			//Execute Query
			return AsyncExecute(CommandType.Text, Command, this.BuildParameterCollection(Parameters));
		}
		public AsyncExecuteState AsyncExecuteSql(string Command, AsyncCallbackDelegate AsyncCallback, TimeoutCallbackDelegate TimeoutCallback, params object[] Parameters)
		{
			//Execute Query
			return AsyncExecute(CommandType.Text, Command, AsyncCallback, TimeoutCallback, this.BuildParameterCollection(Parameters));
		}
		public AsyncExecuteState AsyncExecuteSproc(string Command, params object[] Parameters)
		{
			//Execute Query
			return AsyncExecute(CommandType.StoredProcedure, Command, this.BuildParameterCollection(Parameters));
		}
		public AsyncExecuteState AsyncExecuteSproc(string Command, AsyncCallbackDelegate AsyncCallback, TimeoutCallbackDelegate TimeoutCallback, params object[] Parameters)
		{
			//Execute Query
			return AsyncExecute(CommandType.StoredProcedure, Command, AsyncCallback, TimeoutCallback, this.BuildParameterCollection(Parameters));
		}
		public AsyncExecuteState AsyncExecute(System.Data.CommandType CommandType, string Command, params object[] Parameters)
		{
			//Execute Query
			return AsyncExecute(CommandType, Command, this.BuildParameterCollection(Parameters));
		}
		public AsyncExecuteState AsyncExecute(System.Data.CommandType CommandType, string Command, AsyncCallbackDelegate AsyncCallback, TimeoutCallbackDelegate TimeoutCallback, params object[] Parameters)
		{
			//Set Timeout Method
			this._InternalTimeoutCallbacks.Add(TimeoutCallback);
			this._InternalAsyncExecuteCallbacks.Add(AsyncCallback);

			//Execute Query
			return AsyncExecute(CommandType, Command, this.BuildParameterCollection(Parameters), AsyncCallback, TimeoutCallback);
		}
        public AsyncExecuteState AsyncExecute(System.Data.CommandType CommandType, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{
			//Execute Query
			return AsyncExecute(CommandType: CommandType, Command: Command, Parameters: Parameters, AsyncCallback: null, TimeoutCallback: null);
		}
        public AsyncExecuteState AsyncExecute(System.Data.CommandType CommandType, string Command, AsyncCallbackDelegate AsyncCallback, TimeoutCallbackDelegate TimeoutCallback, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{
			//Set Connection to Asyncrhonous
			this.SqlConnectionBuilder.AsynchronousProcessing = true;

			//Create Sql Connection (if not connected)
			bool useLocalConnection = false;
			if (this.DbConnection == null)
			{
				useLocalConnection = true;
				this.Connect();
			}

			//Execute Query
			AsyncExecuteState asyncExecuteState = AsyncExecute(this.DbConnection, CommandType, Command, Parameters, AsyncCallback, TimeoutCallback);

			//Close Connection
			if(useLocalConnection) this.Disconnect();

			//Return Results
			return asyncExecuteState;
		}
        public AsyncExecuteState AsyncExecute(DbConnection SqlConnection, System.Data.CommandType CommandType, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters, AsyncCallbackDelegate AsyncCallback, TimeoutCallbackDelegate TimeoutCallback)
		{
			//Start Execution Time Stamp
			this._StartTime = DateTime.Now;

			//Create Command Object
            SqlCommand SqlCommand = this.BuildCommandObject(SqlConnection, CommandType, Command, this.CommandTimeout) as SqlCommand;

			//Build Parameter Collection
            DbParameterCollection SqlParameters = this.AddParameters(SqlCommand, Parameters);

			//Create New ExecuteState
			AsyncExecuteState ExecuteState = new AsyncExecuteState(this, SqlCommand, Parameters, false, AsyncCallback, TimeoutCallback);
			IAsyncResult AsyncResult = SqlCommand.BeginExecuteReader(this.AsyncCallbackMethod, ExecuteState);


			//Register the callback
			ThreadPool.RegisterWaitForSingleObject(
				AsyncResult.AsyncWaitHandle,						//Handle to Asynchronous Query
				new WaitOrTimerCallback(this.AsyncTimeoutCallbackMethod),//Timeout Callback Function
				ExecuteState,										//Execute State
				this.CommandTimeout,								//Timeout in ms
				true												//True=Executes only once
				);

			//Return ExecuteState
			return ExecuteState;

		}
		private void AsyncCallbackMethod(IAsyncResult Result)
		{

			//Get the Execute State
			AsyncExecuteState ExecuteState = (AsyncExecuteState)Result.AsyncState;

			//Get Results
			SqlDataReader SqlReader = (ExecuteState.SqlCommand as SqlCommand).EndExecuteReader(Result);

			//Create DataSet
			DataSet DataSet = new DataSet();

			if (this.PopulateDataSetFromReader)
				DataSet.Load(SqlReader, LoadOption.OverwriteChanges, this.DataTableNames);

			//Close Connections
			this.Disconnect(ExecuteState.SqlCommand.Connection);
			this.DbConnection = null;

			//Set Complete
			ExecuteState.IsComplete = true;

			//Stop Time Counter
			this._ExecutionTime = DateTime.Now.Subtract(this._StartTime);

			//Throw Events
			if (this.QueryComplete != null)
				this.QueryComplete(new QueryCompleteEventArgs(this, ExecuteState.SqlCommand, ExecuteState.SqlParameters, DataSet, SqlReader, null));

			//If there is global Callback Method, then Invoke
			if (this.AsyncExecuteCallback != null)
				this.AsyncExecuteCallback(ExecuteState, SqlReader, DataSet);

			//If there is a request specific callback method, then invoke
			if (ExecuteState.AsyncCallback != null)
				ExecuteState.AsyncCallback(ExecuteState, SqlReader, DataSet);

			//Set Done
			this.DoneEvent.Set();

		}
		private void AsyncTimeoutCallbackMethod(object State, bool IsTimedOut)
		{
			//If called because a timeout
			if (IsTimedOut)
			{
				//Convert State to RequestState
				AsyncExecuteState ExecuteState = (AsyncExecuteState)State;

				//Set Complete
				ExecuteState.IsComplete = true;

				//Stop Time Counter
				this._ExecutionTime = DateTime.Now.Subtract(this._StartTime);

				//If able to convert, then cancel Request.
				if (ExecuteState != null)
					ExecuteState.SqlCommand.Cancel();

				//if global TimeoutCallback set, then call
				if (this.TimeoutCallback != null)
					this.TimeoutCallback(ExecuteState);

				//If there is a request specific callback method, then invoke
				if (ExecuteState.TimeoutCallback != null)
					ExecuteState.TimeoutCallback(ExecuteState);

				//Set Done
				this.DoneEvent.Set();
			}

		}
		#endregion

		#region METHODS: Update
		public void Update(DataTable DataTable)
		{
			//Update
            Update(DataTable, false);
		}

		public void Update(DataTable DataTable, bool RefillDataSet)
		{
            if (DataTable.DataSet == null) {
                var ds = new DataSet();
                ds.Tables.Add(DataTable);
            }
			Update(this._SqlAdapter, DataTable.DataSet, RefillDataSet);
		}

		public void Update(SqlDataAdapter SqlAdapter, DataTable DataTable, bool RefillDataSet)
		{
			Update(SqlAdapter, DataTable.DataSet, RefillDataSet);
		}


		public void Update(DataSet DataSet)
		{
			//Update
			Update(this._SqlAdapter, DataSet, false);
		}

		public void Update(DataSet DataSet, bool RefillDataSet)
		{
			//Update
			Update(this._SqlAdapter, DataSet, RefillDataSet);
		}

		public void Update(DbDataAdapter SqlAdapter, DataSet DataSet, bool RefillDataSet)
		{
			//Create Sql Connection (if not connected)
			bool useLocalConnection = false;
			if (this.DbConnection == null)
			{
				useLocalConnection = true;
				this.Connect();
			}

			//Update
			this.Update(this.DbConnection, SqlAdapter, DataSet, RefillDataSet);

			//Close Connection
			if(useLocalConnection) this.Disconnect();

		}
		/// <summary>
		/// Update the DataSet to the Sql Connection
		/// </summary>
		public void Update(DbConnection SqlConnection, DbDataAdapter SqlAdapter, DataSet DataSet, bool RefillDataSet)
		{
			try
			{
				//Create SqlCommandBuilder For Adapter
                DbCommandBuilder sqlBuilder = null;
                if (SqlConnection as SqlConnection != null) sqlBuilder = new SqlCommandBuilder(SqlAdapter as SqlDataAdapter);
                else sqlBuilder = new OleDbCommandBuilder(SqlAdapter as OleDbDataAdapter);

				sqlBuilder.SetAllValues = false;

				//If PrimaryKey Update, Attach to Row Update
				//this._SqlAdapter.RowUpdated += new SqlRowUpdatedEventHandler(this.SqlAdapter_OnRowUpdate);

				//Update Table
				SqlAdapter.Update(DataSet);

				//If Primary Key is Updated, the repopulate DataSet
				if (RefillDataSet)
					SqlAdapter.Fill(DataSet);

			}
			//Catch Errors and cleanup connection
			catch (Exception Ex)
			{
				//Try to Disconnect
				try { this.Disconnect(SqlConnection); }
				catch { }

				///Throw Standard Error
				///.
				throw;

			}

		}
		#endregion



		#region METHODS: Static Execute (Auto)
		public static class Auto
		{
			public static DataTable ExecuteSql(string Command, params object[] Parameters)
			{
				return ExecuteSql(ConnectionBuilder.Sql.DefaultKey, Command, Parameters);
			}
			public static DataTable ExecuteSql(string Key, string Command, params object[] Parameters)
			{
				//Create Sql Helper Class
				SqlHelper sqlHelper = SqlHelper.SqlHelperCreateAutoDetect(Key);

				//Execute
				return sqlHelper.ExecuteSql(Command, Parameters);
			}
			public static DataTable ExecuteSproc(string Command, params object[] Parameters)
			{
				//Execute
				return ExecuteSproc(ConnectionBuilder.Sql.DefaultKey, Command, Parameters);
			}
			public static DataTable ExecuteSproc(string Key, string Command, params object[] Parameters)
			{
				//Create Sql Helper Class
				SqlHelper sqlHelper = SqlHelper.SqlHelperCreateAutoDetect(Key);

				//Execute
				return sqlHelper.ExecuteSproc(Command, Parameters);
			}
			public static DataSet ExecuteXSql(string Command, params object[] Parameters)
			{
				//Execute
				return ExecuteXSql(ConnectionBuilder.Sql.DefaultKey, Command, Parameters);
			}
			public static DataSet ExecuteXSql(string Key, string Command, params object[] Parameters)
			{
				//Create Sql Helper Class
				SqlHelper sqlHelper = SqlHelper.SqlHelperCreateAutoDetect(Key);

				//Execute
				return sqlHelper.ExecuteXSql(Command, Parameters);
			}
			public static DataSet ExecuteXSproc(string Command, params object[] Parameters)
			{
				//Execute
				return ExecuteXSproc(ConnectionBuilder.Sql.DefaultKey, Command, Parameters);
			}
			public static DataSet ExecuteXSproc(string Key, string Command, params object[] Parameters)
			{
				//Create Sql Helper Class
				SqlHelper sqlHelper = SqlHelper.SqlHelperCreateAutoDetect(Key);

				//Execute
				return sqlHelper.ExecuteXSproc(Command, Parameters);
			}
            public static DbDataReader ExecuteReaderSql(string Command, params object[] Parameters)
			{
				//Execute
				return ExecuteReaderSql(ConnectionBuilder.Sql.DefaultKey, Command, Parameters);
			}
            public static DbDataReader ExecuteReaderSql(string Key, string Command, params object[] Parameters)
			{
				//Create Sql Helper Class
				SqlHelper sqlHelper = SqlHelper.SqlHelperCreateAutoDetect(Key);

				//Execute
				return sqlHelper.ExecuteReaderSql(Command, Parameters);
			}
            public static DbDataReader ExecuteReaderSproc(string Command, params object[] Parameters)
			{
				//Execute
				return ExecuteReaderSproc(ConnectionBuilder.Sql.DefaultKey, Command, Parameters);
			}
            public static DbDataReader ExecuteReaderSproc(string Key, string Command, params object[] Parameters)
			{
				//Create Sql Helper Class
				SqlHelper sqlHelper = SqlHelper.SqlHelperCreateAutoDetect(Key);

				//Execute
				return sqlHelper.ExecuteReaderSproc(Command, Parameters);
			}
			public static AsyncExecuteState AsyncExecuteSql(string Command, params object[] Parameters)
			{
				//Execute Query
				return AsyncExecuteSql(ConnectionBuilder.Sql.DefaultKey, Command, Parameters);
			}
			public static AsyncExecuteState AsyncExecuteSql(string Key, string Command, params object[] Parameters)
			{
				//Create Sql Helper Class
				SqlHelper sqlHelper = SqlHelper.SqlHelperCreateAutoDetect(Key);

				//Execute Query
				return sqlHelper.AsyncExecuteSql(Command, Parameters);
			}
			public static AsyncExecuteState AsyncExecuteSql(string Command, AsyncCallbackDelegate AsyncCallback, TimeoutCallbackDelegate TimeoutCallback, params object[] Parameters)
			{
				//Execute Query
				return AsyncExecuteSql(ConnectionBuilder.Sql.DefaultKey, Command, AsyncCallback, TimeoutCallback, Parameters);
			}
			public static AsyncExecuteState AsyncExecuteSql(string Key, string Command, AsyncCallbackDelegate AsyncCallback, TimeoutCallbackDelegate TimeoutCallback, params object[] Parameters)
			{
				//Create Sql Helper Class
				SqlHelper sqlHelper = SqlHelper.SqlHelperCreateAutoDetect(Key);

				//Execute Query
				return sqlHelper.AsyncExecuteSql(Command, AsyncCallback, TimeoutCallback, Parameters);
			}
			public static AsyncExecuteState AsyncExecuteSproc(string Command, params object[] Parameters)
			{
				//Execute Query
				return AsyncExecuteSproc(ConnectionBuilder.Sql.DefaultKey, Command, Parameters);
			}
			public static AsyncExecuteState AsyncExecuteSproc(string Key, string Command, params object[] Parameters)
			{
				//Create Sql Helper Class
				SqlHelper sqlHelper = SqlHelper.SqlHelperCreateAutoDetect(Key);

				//Execute Query
				return sqlHelper.AsyncExecuteSproc(Command, Parameters);
			}
			public static AsyncExecuteState AsyncExecuteSproc(string Command, AsyncCallbackDelegate AsyncCallback, TimeoutCallbackDelegate TimeoutCallback, params object[] Parameters)
			{
				//Execute Query
				return AsyncExecuteSproc(ConnectionBuilder.Sql.DefaultKey, Command, AsyncCallback, TimeoutCallback, Parameters);
			}
			public static AsyncExecuteState AsyncExecuteSproc(string Key, string Command, AsyncCallbackDelegate AsyncCallback, TimeoutCallbackDelegate TimeoutCallback, params object[] Parameters)
			{
				//Create Sql Helper Class
				SqlHelper sqlHelper = SqlHelper.SqlHelperCreateAutoDetect(Key);

				//Execute Query
				return sqlHelper.AsyncExecuteSproc(Command, AsyncCallback, TimeoutCallback, Parameters);
			}
		}
		#endregion

		#region METHODS: Static Execute (SqlConnection)
        public static DataTable ExecuteSql(DbConnection SqlConnection, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{
			//Create Sql Helper Class
			SqlHelper sqlHelper = new SqlHelper();

			//Execute
			return sqlHelper.Execute(SqlConnection, CommandType.Text, Command, Parameters);
		}
        public static DataTable ExecuteSproc(DbConnection SqlConnection, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{
			//Create Sql Helper Class
			SqlHelper sqlHelper = new SqlHelper();

			//Execute
			return sqlHelper.Execute(SqlConnection, CommandType.StoredProcedure, Command, Parameters);
		}
        public static DbDataReader ExecuteReaderSql(DbConnection SqlConnection, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{
			//Create Sql Helper Class
			SqlHelper sqlHelper = new SqlHelper();

			//Execute
			return sqlHelper.ExecuteReader(SqlConnection, CommandType.Text, Command, Parameters);
		}
        public static DbDataReader ExecuteReaderSproc(DbConnection SqlConnection, string Command, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> Parameters)
		{
			//Create Sql Helper Class
			SqlHelper sqlHelper = new SqlHelper();

			//Execute
			return sqlHelper.ExecuteReader(SqlConnection, CommandType.StoredProcedure, Command, Parameters);
		}
		#endregion


		#region CLASS: AsyncExecuteState
		/// <summary>
		/// The Asynchronous Request State Object
		/// </summary>
		public class AsyncExecuteState
		{
			//Declarations
			public SqlHelper SqlHelper = null;							// holds the SqlHelper
            public jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> SqlParameters = null;		// store any data in this
			public bool IsComplete = false;
            public DbCommand SqlCommand = null;
			public TimeoutCallbackDelegate TimeoutCallback = null;
			public AsyncCallbackDelegate AsyncCallback = null;

			//Constructor
            public AsyncExecuteState(SqlHelper SqlHelper, DbCommand SqlCommand, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> SqlParameters, bool IsComplete, AsyncCallbackDelegate AsyncCallback, TimeoutCallbackDelegate TimeoutCallback)
			{
				this.SqlHelper = SqlHelper;
				this.SqlCommand = SqlCommand;
				this.SqlParameters = SqlParameters;
				this.IsComplete = IsComplete;
				this.AsyncCallback = AsyncCallback;
				this.TimeoutCallback = TimeoutCallback;
			}

		}
		#endregion

		#region CLASS: QueryCompleteEventArgs
		public class QueryCompleteEventArgs : EventArgs
		{
			#region PROPERTIES
			private SqlHelper _SqlHelper;
			public SqlHelper SqlHelper
			{
				get { return this._SqlHelper; }
			}
            private DbCommand _SqlCommand;
            public DbCommand SqlCommand
			{
				get { return this._SqlCommand; }
			}
            private jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> _SqlParameters;
            public jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> SqlParameters
			{
				get { return this._SqlParameters; }
			}
			private DataSet _DataSet;
			public DataSet DataSet
			{
				get { return this._DataSet; }
			}
            private DbDataReader _SqlDataReader;
            public DbDataReader SqlDataReader
			{
				get { return this._SqlDataReader; }
			}
            private DbDataAdapter _SqlAdapter;
			public DbDataAdapter SqlAdapter
			{
				get { return this._SqlAdapter; }
			}
			#endregion

			#region CONSTRUCTORS
            public QueryCompleteEventArgs(SqlHelper SqlHelper, DbCommand SqlCommand, jlib.helpers.structures.OrderedDictionary<string, System.Data.Common.DbParameter> SqlParameters, DataSet DataSet, DbDataReader SqlDataReader, DbDataAdapter SqlAdapter)
			{
				this._SqlHelper = SqlHelper;
				this._SqlCommand = SqlCommand;
				this._SqlParameters = SqlParameters;
				this._DataSet = DataSet;
				this._SqlDataReader = SqlDataReader;
				this._SqlAdapter = SqlAdapter;
			}
			#endregion
		}
		#endregion

        public class DataMapping {
            public class DataMap {                
                public string SQLServerType { get; set; }
                public Type FrameworkType { get; set; }
                public System.Data.SqlDbType SQLDbType { get; set; }
                public string SQLDataReaderSQLTDbypedAccessor { get; set; }
                public System.Data.DbType DbType { get; set; }
                public string SQLDataReaderDbTypedAccessor { get; set; }
                public DataMap(string sQLServerType, Type frameworkType, System.Data.SqlDbType sQLDbType, string sQLDataReaderSQLTDbypedAccessor, System.Data.DbType dbType, string sQLDataReaderDbTypedAccessor) {
                    SQLServerType = sQLServerType;
                    FrameworkType = frameworkType;
                    SQLDbType = sQLDbType;
                    SQLDataReaderSQLTDbypedAccessor = sQLDataReaderSQLTDbypedAccessor;
                    DbType = dbType;
                    SQLDataReaderDbTypedAccessor = sQLDataReaderDbTypedAccessor;
                }
            }

            //Created from https://msdn.microsoft.com/en-us/library/cc716729%28v=vs.110%29.aspx
            private static List<DataMap> DataTypeMap = new List<DataMap>() { 
                new DataMap("bigint",typeof(Int64),System.Data.SqlDbType.BigInt,"GetSqlInt64",System.Data.DbType.Int64,"GetInt64"),
new DataMap("binary",typeof(Byte[]),System.Data.SqlDbType.VarBinary,"GetSqlBinary",System.Data.DbType.Binary,"GetBytes"),
new DataMap("bit",typeof(Boolean),System.Data.SqlDbType.Bit,"GetSqlBoolean",System.Data.DbType.Boolean,"GetBoolean"),
new DataMap("varchar",typeof(String),System.Data.SqlDbType.VarChar,"GetSqlString",System.Data.DbType.String,"GetString"),
new DataMap("varchar",typeof(Char[]),System.Data.SqlDbType.VarChar,"GetSqlString",System.Data.DbType.String,"GetChars"),
new DataMap("varchar",typeof(String),System.Data.SqlDbType.VarChar,"GetSqlString",System.Data.DbType.AnsiString,"GetString"),
new DataMap("varchar",typeof(Char[]),System.Data.SqlDbType.VarChar,"GetSqlString",System.Data.DbType.AnsiString,"GetChars"),
new DataMap("nvarchar",typeof(String),System.Data.SqlDbType.NVarChar,"GetSqlString",System.Data.DbType.String,"GetString"),
new DataMap("nvarchar",typeof(Char[]),System.Data.SqlDbType.NVarChar,"GetSqlString",System.Data.DbType.String,"GetChars"),
new DataMap("text",typeof(String),System.Data.SqlDbType.Text,"GetSqlString",System.Data.DbType.String,"GetString"),
new DataMap("text",typeof(Char[]),System.Data.SqlDbType.Text,"GetSqlString",System.Data.DbType.String,"GetChars"),
new DataMap("ntext",typeof(String),System.Data.SqlDbType.NText,"GetSqlString",System.Data.DbType.String,"GetString"),
new DataMap("ntext",typeof(Char[]),System.Data.SqlDbType.NText,"GetSqlString",System.Data.DbType.String,"GetChars"),
new DataMap("char",typeof(String),System.Data.SqlDbType.Char,"GetSqlString",System.Data.DbType.String,"GetString"),
new DataMap("char",typeof(Char[]),System.Data.SqlDbType.Char,"GetSqlString",System.Data.DbType.String,"GetChars"),
new DataMap("char",typeof(String),System.Data.SqlDbType.Char,"GetSqlString",System.Data.DbType.AnsiStringFixedLength,"GetString"),
new DataMap("char",typeof(Char[]),System.Data.SqlDbType.Char,"GetSqlString",System.Data.DbType.AnsiStringFixedLength,"GetChars"),
new DataMap("datetime",typeof(DateTime),System.Data.SqlDbType.DateTime,"GetSqlDateTime",System.Data.DbType.DateTime,"GetDateTime"),
new DataMap("datetime2",typeof(DateTime),System.Data.SqlDbType.DateTime2,null,System.Data.DbType.DateTime2,"GetDateTime"),
new DataMap("datetimeoffset",typeof(DateTimeOffset),System.Data.SqlDbType.DateTimeOffset,null,System.Data.DbType.DateTimeOffset,"GetDateTimeOffset"),
new DataMap("date",typeof(DateTime),System.Data.SqlDbType.Date,"GetSqlDateTime",System.Data.DbType.Date,"GetDateTime"),
new DataMap("decimal",typeof(Decimal),System.Data.SqlDbType.Decimal,"GetSqlDecimal",System.Data.DbType.Decimal,"GetDecimal"),
new DataMap("FILESTREAM attribute (varbinary(max))",typeof(Byte[]),System.Data.SqlDbType.VarBinary,"GetSqlBytes",System.Data.DbType.Binary,"GetBytes"),
new DataMap("float",typeof(Double),System.Data.SqlDbType.Float,"GetSqlDouble",System.Data.DbType.Double,"GetDouble"),
new DataMap("image",typeof(Byte[]),System.Data.SqlDbType.Binary,"GetSqlBinary",System.Data.DbType.Binary,"GetBytes"),
new DataMap("int",typeof(Int32),System.Data.SqlDbType.Int,"GetSqlInt32",System.Data.DbType.Int32,"GetInt32"),
new DataMap("money",typeof(Decimal),System.Data.SqlDbType.Money,"GetSqlMoney",System.Data.DbType.Decimal,"GetDecimal"),
new DataMap("nchar",typeof(String),System.Data.SqlDbType.NChar,"GetSqlString",System.Data.DbType.StringFixedLength,"GetString"),
new DataMap("nchar",typeof(Char[]),System.Data.SqlDbType.NChar,"GetSqlString",System.Data.DbType.StringFixedLength,"GetChars"),
new DataMap("numeric",typeof(Decimal),System.Data.SqlDbType.Decimal,"GetSqlDecimal",System.Data.DbType.Decimal,"GetDecimal"),
new DataMap("real",typeof(Single),System.Data.SqlDbType.Real,"GetSqlSingle",System.Data.DbType.Single,"GetFloat"),
new DataMap("rowversion",typeof(Byte[]),System.Data.SqlDbType.Timestamp,"GetSqlBinary",System.Data.DbType.Binary,"GetBytes"),
new DataMap("smalldatetime",typeof(DateTime),System.Data.SqlDbType.DateTime,"GetSqlDateTime",System.Data.DbType.DateTime,"GetDateTime"),
new DataMap("smallint",typeof(Int16),System.Data.SqlDbType.SmallInt,"GetSqlInt16",System.Data.DbType.Int16,"GetInt16"),
new DataMap("smallmoney",typeof(Decimal),System.Data.SqlDbType.SmallMoney,"GetSqlMoney",System.Data.DbType.Decimal,"GetDecimal"),
new DataMap("sql_variant",typeof(Object),System.Data.SqlDbType.Variant,"GetSqlValue",System.Data.DbType.Object,"GetValue"),
new DataMap("time",typeof(TimeSpan),System.Data.SqlDbType.Time,null,System.Data.DbType.Time,"GetDateTime"),
new DataMap("timestamp",typeof(Byte[]),System.Data.SqlDbType.Timestamp,"GetSqlBinary",System.Data.DbType.Binary,"GetBytes"),
new DataMap("tinyint",typeof(Byte),System.Data.SqlDbType.TinyInt,"GetSqlByte",System.Data.DbType.Byte,"GetByte"),
new DataMap("uniqueidentifier",typeof(Guid),System.Data.SqlDbType.UniqueIdentifier,"GetSqlGuid",System.Data.DbType.Guid,"GetGuid"),
new DataMap("varbinary",typeof(Byte[]),System.Data.SqlDbType.VarBinary,"GetSqlBinary",System.Data.DbType.Binary,"GetBytes"),
new DataMap("xml",typeof(System.Xml.XmlDocument),System.Data.SqlDbType.Xml,"GetSqlXml",System.Data.DbType.Xml,null)

            };
            public static string GetSQLColumnType(System.Data.DbType dbType){
                lock (DataTypeMap)
                    for (int x = 0; x < DataTypeMap.Count; x++)
                        if (DataTypeMap[x].DbType == dbType) return DataTypeMap[x].SQLServerType;

                return null;
            }
            public static string GetSQLColumnType(System.Data.SqlDbType sqlDbType) {
                lock (DataTypeMap)
                    for (int x = 0; x < DataTypeMap.Count; x++)
                        if (DataTypeMap[x].SQLDbType == sqlDbType) return DataTypeMap[x].SQLServerType;

                return null;
            }

            public static string GetSQLColumnType(Type frameworkType) {
                lock (DataTypeMap)
                    for (int x = 0; x < DataTypeMap.Count; x++)
                        if (DataTypeMap[x].FrameworkType == frameworkType) return DataTypeMap[x].SQLServerType;

                return null;
            }
        }

		void IDisposable.Dispose()
		{
			if(this.IsConnected) this.Disconnect();
		}
	}
}
