using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using jlib.functions;

namespace jlib.db
{
	/// <summary>
	/// Summary description for condition.
	/// </summary>
	public class condition
	{
		#region ENUMERATION
		public enum ConditionTypes
		{
			Equal,
			NotEqual,
            NotLike,
			GreaterThan,
			LessThan,
			GreaterEqualThan,
			LessEqualThan,
			Between,
			Like
		}
		#endregion

		#region DECLARATIONS
		private string m_sCustomCondition		= "";
		private string m_sField					= "";
		private object m_oValue1				= null;
		private object m_oValue2				= null;
		private ConditionTypes m_oConditionType	= ConditionTypes.Equal;
		private int m_iParameterStartIndex		= 1;
		#endregion

		#region CONSTRUCTORS
		public condition()
		{

		}
		public condition(string sCondition)
		{
			this.CustomCondition	= sCondition;
		}
		public condition(string sField, ConditionTypes oConditionType, object oValue)
		{
			this.Field				= sField;
			this.ConditionType		= oConditionType;
			this.Value1				= oValue;
		}
		public condition(string sField, object oValue)
		{
			this.Field				= sField;
			this.ConditionType		= ConditionTypes.Equal;
			this.Value1				= oValue;
		}
		public condition(string sField, ConditionTypes oConditionType, object oValue1, object oValue2)
		{
			this.Field				= sField;
			this.ConditionType		= oConditionType;
			this.Value1				= oValue1;
			this.Value2				= oValue2;
		}
		public condition(string sField, object oValue1, object oValue2)
		{
			this.Field				= sField;
			this.ConditionType		= ConditionTypes.Between;
			this.Value1				= oValue1;
			this.Value2				= oValue2;
		}
		#endregion

		#region PROPERTIES
		public string CustomCondition
		{
			get
			{
				return m_sCustomCondition;
			}
			set
			{
				m_sCustomCondition = value;
			}
		}
		public string Field
		{
			get
			{
				return m_sField;
			}
			set
			{
				m_sField = value;
			}
		}
		public object Value
		{
			get
			{
				return m_oValue1;
			}
			set
			{
				m_oValue1 = value;
			}
		}
		public object Value1
		{
			get
			{
				return m_oValue1;
			}
			set
			{
				m_oValue1 = value;
			}
		}
		public object Value2
		{
			get
			{
				return m_oValue2;
			}
			set
			{
				m_oValue2 = value;
			}
		}
		public ConditionTypes ConditionType
		{
			get
			{
				return m_oConditionType;
			}
			set
			{
				m_oConditionType = value;
			}
		}
		public int ParameterStartIndex
		{
			get
			{
				return m_iParameterStartIndex;
			}
			set
			{
				m_iParameterStartIndex = value;
			}
		}
		public int Parameters
		{
			get
			{
				if(this.CustomCondition != "")
					return 0;

				switch(this.ConditionType)
				{
					case ConditionTypes.Between:
						return 2;
				}

				return 1;
			}
		}
		#endregion

		#region METHODS
		public string generateCondition(int iStartIndex)
		{
			this.ParameterStartIndex	= iStartIndex;
			return generateCondition();
		}
		public string generateCondition()
		{
			if(this.CustomCondition != "")
				return this.CustomCondition;

			string sClause				= "";

			switch(this.ConditionType)
			{
				case ConditionTypes.Equal:
					sClause		= " {0} = @p{1} ";
					break;

				case ConditionTypes.Between:
					sClause		= " {0} BETWEEN @p{1} AND @p{2} ";
					break;

				case ConditionTypes.GreaterEqualThan:
					sClause		= " {0} >= @p{1} ";
					break;

				case ConditionTypes.GreaterThan:
					sClause		= " {0} > @p{1} ";
					break;

				case ConditionTypes.LessEqualThan:
					sClause		= " {0} <= @p{1} ";
					break;

				case ConditionTypes.LessThan:
					sClause		= " {0} < @p{1} ";
					break;

				case ConditionTypes.Like:
					sClause		= " {0} LIKE @p{1} ";
					break;

				case ConditionTypes.NotEqual:
					sClause		= " {0} <> @p{1} ";
					break;

                case ConditionTypes.NotLike:
                    sClause = " {0} NOT LIKE @p{1} ";
                    break;
			}

			if(this.Parameters == 2)
				return String.Format(sClause, this.Field, this.ParameterStartIndex, this.ParameterStartIndex+1);

			return String.Format(sClause, this.Field, this.ParameterStartIndex);
		}
        public string toSQLString() {
            return toSQLString(sqlbuilder.DataServerTypes.SQLServer, typeof(string), null);
        }
		public string toSQLString(sqlbuilder.DataServerTypes oServerType, Type oType, string sFieldCastFunction ) {
			if (this.CustomCondition != "") return this.CustomCondition;
            sFieldCastFunction = jlib.functions.convert.cStr(sFieldCastFunction);

			string sClause = "";

			switch (this.ConditionType) {
				case ConditionTypes.Equal:
					sClause = " {0} = {1} ";
					break;

				case ConditionTypes.Between:
					sClause = " {0} BETWEEN {1} AND {2} ";
					break;

				case ConditionTypes.GreaterEqualThan:
					sClause = " {0} >= {1} ";
					break;

				case ConditionTypes.GreaterThan:
					sClause = " {0} > {1} ";
					break;

				case ConditionTypes.LessEqualThan:
					sClause = " {0} <= {1} ";
					break;

				case ConditionTypes.LessThan:
					sClause = " {0} < {1} ";
					break;

				case ConditionTypes.Like:
					sClause = " {0} LIKE {1} ";
					break;

				case ConditionTypes.NotEqual:
					sClause = " {0} <> {1} ";
					break;

                case ConditionTypes.NotLike:
                    sClause = " {0} NOT LIKE {1} ";
                    break;
			}

            string Condition1 = this.Value1.Str();
            if (Condition1.StartsWith("%'") || Condition1.EndsWith("'%")) Condition1 = jlib.functions.parse.stripCharacter(Condition1, "%'", "'%");

            if (oType == typeof(DateTime)) {
                if (this.Value1 as DateTime? == null) this.Value1 = jlib.functions.convert.cDate(jlib.functions.parse.replaceAll(this.Value1, "%", ""), true);
                if (this.Parameters == 2 && this.Value2 as DateTime? == null) this.Value2 = jlib.functions.convert.cDate(jlib.functions.parse.replaceAll(this.Value2, "%", ""), true);
                if (oServerType == sqlbuilder.DataServerTypes.SQLServer) {
                    if (sFieldCastFunction == "") sFieldCastFunction = "CAST({0} AS DATE)";
                    if (this.Parameters == 2)
                        return String.Format(sClause, String.Format(sFieldCastFunction,this.Field), String.Format("'{0:yyyy-MM-dd}'", this.Value1), String.Format("'{0:yyyy-MM-dd}'", this.Value2));

                    return String.Format(sClause, String.Format(sFieldCastFunction, this.Field), String.Format("'{0:yyyy-MM-dd}'", this.Value1));
                }
            }
            if (oServerType == sqlbuilder.DataServerTypes.Oracle) {
                if (oType == typeof(DateTime)) {
                    if (sFieldCastFunction == "") sFieldCastFunction = "TRUNC({0})";
                    if (this.Parameters == 2)
                        return String.Format(sClause, String.Format(sFieldCastFunction, this.Field), "to_date(" + String.Format("'{0:yyyy-MM-dd}'", this.Value1) + ",'yyyy-mm-dd')", "to_date(" + String.Format("'{0:yyyy-MM-dd}'", this.Value2) + ",'yyyy-mm-dd')");

                    return String.Format(sClause, String.Format(sFieldCastFunction, this.Field), "to_date(" + String.Format("'{0:yyyy-MM-dd}'", this.Value1) + ",'yyyy-mm-dd')");
                } else {
                    if (this.Parameters == 2)
                        return String.Format(sClause, "lower(" + this.Field + ")", "'" + ado_helper.PrepareDB(Condition1).ToLower() + "'", "'" + ado_helper.PrepareDB(this.Value2).ToLower() + "'");

                    return String.Format(sClause, "lower(" + this.Field + ")", "'" + ado_helper.PrepareDB(Condition1).ToLower() + "'");
                }
            }

            if (oType == typeof(double))
                return String.Format(sClause, this.Field, Condition1.Dbl());

			if (this.Parameters == 2)
				return String.Format(sClause, this.Field, "'" + ado_helper.PrepareDB(Condition1) + "'", "'"+ ado_helper.PrepareDB(this.Value2) + "'");

			return String.Format(sClause, this.Field, "'" + ado_helper.PrepareDB(Condition1) + "'");
		}
		public SqlParameter generateParameter()
		{
			return generateParameter1();
		}
		public SqlParameter generateParameter1()
		{	
			if ( this.Field.IndexOf( "aux_field_" ) == 0 )
				return new SqlParameter( String.Format("@p{0}", this.ParameterStartIndex), functions.convert.cStr( this.Value1 ));
			else
				return new SqlParameter( String.Format("@p{0}", this.ParameterStartIndex), this.Value1 );

			
			
		}
		public SqlParameter generateParameter2()
		{
			if ( this.Field.IndexOf( "aux_field_" ) == 0 )
				return new SqlParameter( String.Format("@p{0}", this.ParameterStartIndex + 1 ), functions.convert.cStr( this.Value2  ));
			else
				return new SqlParameter( String.Format("@p{0}", this.ParameterStartIndex + 1 ), this.Value2 );				
		}
		public override string ToString()
		{
			return generateCondition();
		}

		#endregion
	}
}
