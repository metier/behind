using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace jlib.db
{
	/// <summary>
	/// Summary description for condition.
	/// </summary>
	public class updatevalue
	{
		#region ENUMERATION
		#endregion

		#region DECLARATIONS
		private string m_sCustomExpression		= "";
		private string m_sField					= "";
		private object m_oValue					= null;
		private int m_iParameterStartIndex		= 1;
		#endregion

		#region CONSTRUCTORS
		public updatevalue()
		{

		}
		public updatevalue(string sExpression)
		{
			this.CustomExpression	= sExpression;
		}
		public updatevalue(string sField, object oValue)
		{
			this.Field				= sField;
			this.Value				= oValue;
		}
		#endregion

		#region PROPERTIES
		public string CustomExpression
		{
			get
			{
				return m_sCustomExpression;
			}
			set
			{
				m_sCustomExpression = value;
			}
		}
		public string Field
		{
			get
			{
				return m_sField;
			}
			set
			{
				m_sField = value;
			}
		}
		public object Value
		{
			get
			{
				return m_oValue;
			}
			set
			{
				m_oValue = value;
			}
		}
		public int ParameterStartIndex
		{
			get
			{
				return m_iParameterStartIndex;
			}
			set
			{
				m_iParameterStartIndex = value;
			}
		}
		#endregion

		#region METHODS
		public string generateExpression(int iStartIndex)
		{
			this.ParameterStartIndex	= iStartIndex;
			return generateExpression();
		}
		public string generateExpression()
		{
			if(this.CustomExpression != "")
				return this.CustomExpression;

			return String.Format(" [{0}] = @p{1} ", this.Field, this.ParameterStartIndex);
		}
		public SqlParameter generateParameter()
		{
			return new SqlParameter( String.Format("@p{0}", this.ParameterStartIndex), this.Value);
		}
		public override string ToString()
		{
			return generateExpression();
		}

		#endregion
	}
}
