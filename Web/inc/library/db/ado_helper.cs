using System;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using jlib.functions;
using System.Linq;

namespace jlib.db {

    public class RowCollectionDataReaderBulk : IDataReader{
        
        #region ***** CONSTRUCTOR *****
        public RowCollectionDataReaderBulk(List<DataRow> rowsToWrite) {
            Items = rowsToWrite;
            Enumerator = Items.GetEnumerator();
            CurrentIndex = -1;
        }            

        #endregion

        #region ***** PROPERTIES *****
        protected List<DataRow> Items { get; private set; }
        protected IEnumerator<DataRow> Enumerator { get; private set; }
        public jlib.helpers.structures.OrderedDictionary<string, int> ColumnMapping { get; private set; }
        public int CurrentIndex { get; private set; }
        private object[] m_oCurrentRowValues;
        private object[] CurrentRowValues {
            get {
                if (m_oCurrentRowValues == null) {
                    m_oCurrentRowValues = new object[this.FieldCount];
                    m_oCurrentRowValues[0] = CurrentIndex;
                    m_oCurrentRowValues[1] = (int)this.Enumerator.Current.RowState;
                    for (int x = 2; x < this.FieldCount; x++) {
                        if(this.Enumerator.Current.RowState == DataRowState.Deleted) m_oCurrentRowValues[x] = this.Enumerator.Current[ColumnMapping[x], DataRowVersion.Original];
                        else m_oCurrentRowValues[x] = this.Enumerator.Current[ColumnMapping[x]];
                    }
                }
                return m_oCurrentRowValues;
            }            
        }

        /// <summary>
        /// Gets a value indicating whether the data reader is closed.
        /// </summary>
        public bool IsClosed {
            get { return this.Enumerator == null; }
        }

        /// <summary>
        /// Gets the number of rows changed, inserted, or deleted by execution of the SQL statement.
        /// </summary>
        public int RecordsAffected {
            get { return Items.Count(); }
        }

        /// <summary>
        /// Determines if there is items in the enumerator collection
        /// </summary>
        public bool HasRows {
            get { return this.Items != null && this.Items.Count() > 0; }
        }

        /// <summary>
        /// Gets the column with the specified name. (Inherited from IDataRecord.)
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public object this[string fieldName] {
            get { return CurrentRowValues[ColumnMapping[fieldName]]; }
        }

        /// <summary>
        /// Gets the column located at the specified index. (Inherited from IDataRecord.)
        /// </summary>
        /// <param name="ordinal"></param>
        /// <returns></returns>
        public object this[int ordinal] {
            get { return CurrentRowValues[ordinal]; }
        }

        /// <summary>
        /// Gets the number of columns in the current row. (Inherited from IDataRecord.)
        /// </summary>
        public int FieldCount {
            get { return ColumnMapping.Count; }
        }

        /// <summary>
        /// Gets a value indicating the depth of nesting for the current row.
        /// </summary>
        public int Depth {
            get { return 0; }
        }

        #endregion

        #region ***** METHODS *****
        /// <summary>
        /// Closes the IDataReader Object.
        /// </summary>
        public void Close() {
            this.Enumerator = null;
            this.Items = null;
        }

        /// <summary>
        /// Advances the IDataReader to the next record.
        /// </summary>
        /// <returns></returns>
        public bool Read() {
            m_oCurrentRowValues = null;
            bool success = this.Enumerator.MoveNext();
            if (success) CurrentIndex++;
            return success;
        }        

        /// <summary>
        /// Advances the data reader to the next result, when reading the results of batch SQL statements.
        /// </summary>
        /// <returns></returns>
        public bool NextResult() {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public int GetOrdinal(string fieldName) {
            return ColumnMapping.IndexOfKey(fieldName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ordinal"></param>
        /// <returns></returns>
        public string GetName(int ordinal) {
            return ColumnMapping.ElementAt(ordinal).Key;
        }

        /// <summary>
        /// Populates an array of objects with the column values of the current record. (Inherited from IDataRecord.)
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public int GetValues(object[] values) {
            int count = 0;
            for (int i = 0; i <= Math.Min(values.Count(), this.FieldCount); i++) {
                count++;
                values[i] = this[i];
            }            
            return count;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources. (Inherited from IDisposable.)
        /// </summary>
        public void Dispose() { }

        /// <summary>
        /// Gets the Type information corresponding to the type of Object that would be returned from GetValue. (Inherited from IDataRecord.)
        /// </summary>
        /// <param name="ordinal"></param>
        /// <returns></returns>
        public Type GetFieldType(int ordinal) {
            if (ordinal == 0 || ordinal == 1) return typeof(int);
            return this.Items[0].Table.Columns[ColumnMapping.ElementAt(ordinal).Value].DataType;
        }

        /// <summary>
        /// Gets the data type information for the specified field. (Inherited from IDataRecord.)
        /// </summary>
        /// <param name="ordinal"></param>
        /// <returns></returns>
        public string GetDataTypeName(int ordinal) {
            return this.GetFieldType(ordinal).ToString();
        }


        public object GetValue(int ordinal) { return this[ordinal]; }
        public string GetString(int ordinal) { return (string)this[ordinal]; }
        public bool GetBoolean(int ordinal) { return (bool)this[ordinal]; }
        public char GetChar(int ordinal) { return (char)this[ordinal]; }
        public DateTime GetDateTime(int ordinal) { return (DateTime)this[ordinal]; }
        public decimal GetDecimal(int ordinal) { return (decimal)this[ordinal]; }
        public double GetDouble(int ordinal) { return (double)this[ordinal]; }
        public float GetFloat(int ordinal) { return (float)this[ordinal]; }
        public Guid GetGuid(int ordinal) { return (Guid)this[ordinal]; }
        public short GetInt16(int ordinal) { return (short)this[ordinal]; }
        public int GetInt32(int ordinal) { return (int)this[ordinal]; }
        public long GetInt64(int ordinal) { return (long)this[ordinal]; }
        public bool IsDBNull(int ordinal) { return this[ordinal] == System.DBNull.Value || this[ordinal] == null; }


        //Not Implemented Methods
        public byte GetByte(int i) { throw new NotImplementedException(); }
        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length) { throw new NotImplementedException(); }
        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length) { throw new NotImplementedException(); }
        public IDataReader GetData(int i) { throw new NotImplementedException(); }
        public DataTable GetSchemaTable() { throw new NotImplementedException(); }

        public void SetColumnsToLoad(Dictionary<string, DataColumn> cols) {
            ColumnMapping = new helpers.structures.OrderedDictionary<string, int>();
            ColumnMapping.Add("__Index", -2);
            ColumnMapping.Add("__ChangeType", -1);
            cols.ToList().ForEach(x => {
                ColumnMapping.Add(x.Key, Items[0].Table.Columns.IndexOf(x.Key));
            });

        }

        #endregion
    }

    public class JDataReaderAdapter : System.Data.Common.DbDataAdapter {
        public int FillFromReader(JDataTable oTable, IDataReader oReader) {
            if (oReader == null || oReader.FieldCount == 0) return 0;
            int iCounter=0;
            oTable.SchemaTable = oReader.GetSchemaTable();
            DataColumn ColumnNameCol = oTable.SchemaTable.Columns["ColumnName"];
            DataColumn DataTypeCol = oTable.SchemaTable.Columns["DataType"];
            DataColumn IsAutoIncrementCol = oTable.SchemaTable.Columns["IsAutoIncrement"];
            DataColumn AllowDBNullCol = oTable.SchemaTable.Columns["AllowDBNull"];
            //DataColumn IsReadOnlyCol = oTable.SchemaTable.Columns["IsReadOnly"];
            DataColumn IsKeyCol = oTable.SchemaTable.Columns["IsKey"];
            //DataColumn IsUniqueCol = oTable.SchemaTable.Columns["IsUnique"];
            DataColumn ColumnSizeCol = oTable.SchemaTable.Columns["ColumnSize"];
            DataColumn ColumnIsHiddenCol = oTable.SchemaTable.Columns["IsHidden"];
            DataColumn IdentityCol = oTable.SchemaTable.Columns["IsIdentity"];
            //DataColumn ProviderSpecificDataTypeCol = oTable.SchemaTable.Columns["ProviderSpecificDataType"];
            //DataColumn DataTypeNameCol = oTable.SchemaTable.Columns["DataTypeName"];
            DataColumn BaseTableNameCol = oTable.SchemaTable.Columns["BaseTableName"];

            //BaseColumnName sometimes get set to the wrong column by the data provider for OleDB.
            if (oReader as System.Data.OleDb.OleDbDataReader != null && oTable.SchemaTable.Columns.Contains("BaseColumnName")) oTable.SchemaTable.Columns.Remove("BaseColumnName");

            for (int x = 0; x < oReader.FieldCount; x++) {                
                DataColumn oCol = new DataColumn(oTable.SchemaTable.Rows[x][ColumnNameCol].ToString(), (Type)oTable.SchemaTable.Rows[x][DataTypeCol]);
                if (ColumnIsHiddenCol!=null && oTable.SchemaTable.Rows[x][ColumnIsHiddenCol] != DBNull.Value && (bool)oTable.SchemaTable.Rows[x][ColumnIsHiddenCol]) {
                    oCol.ColumnMapping = MappingType.Hidden;
                } else {
                    if (AllowDBNullCol != null && oTable.SchemaTable.Rows[x][AllowDBNullCol] != DBNull.Value && !(bool)oTable.SchemaTable.Rows[x][AllowDBNullCol]) oTable.JRequiredColumn.Add(oCol);
                    if ((ColumnIsHiddenCol==null||oTable.SchemaTable.Rows[x][ColumnIsHiddenCol] != DBNull.Value) && (bool)oTable.SchemaTable.Rows[x][IsKeyCol]) oTable.JPrimaryKey.Add(oCol);
                    if ((ColumnIsHiddenCol==null||oTable.SchemaTable.Rows[x][ColumnIsHiddenCol] != DBNull.Value) && (bool)oTable.SchemaTable.Rows[x][IsAutoIncrementCol]) oTable.JAutoNumberColumn.Add(oCol);
                    if (IdentityCol!=null && (ColumnIsHiddenCol == null || oTable.SchemaTable.Rows[x][ColumnIsHiddenCol] != DBNull.Value) && (bool)oTable.SchemaTable.Rows[x][IdentityCol]) oTable.JIdentityColumn.Add(oCol);
                    oTable.BaseTableName.Add(oCol, (oTable.SchemaTable.Rows[x][BaseTableNameCol]==System.DBNull.Value ?"" : (string)oTable.SchemaTable.Rows[x][BaseTableNameCol]));
                    if (oTable.TableName == "" && oTable.SchemaTable.Rows[x][BaseTableNameCol] != System.DBNull.Value) oTable.TableName = (string)oTable.SchemaTable.Rows[x][BaseTableNameCol];
                }                
                //oCol.ExtendedProperties["ProviderSpecificDataType"] = oTable.SchemaTable.Rows[x][ProviderSpecificDataTypeCol];
                //oCol.ExtendedProperties["DataTypeName"] = oTable.SchemaTable.Rows[x][DataTypeNameCol];
                
                if (oTable.Columns.Contains(oCol.ColumnName)) oCol.ColumnName += x;
                if (oCol.DataType == typeof(System.String)) oCol.MaxLength = (int)oTable.SchemaTable.Rows[x][ColumnSizeCol];                
                oTable.Columns.Add(oCol);
                oCol.ExtendedProperties.Add("schema-position", x);
            }
            //oTable.MinimumCapacity = 100;               
            object []oValues=new object[oReader.FieldCount];
            oTable.BeginLoadData();
            //object o = null;
            //System.Reflection.MethodInfo oMethod = oReader.GetType().GetMethod("GetValueInternal", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            //System.Reflection.MethodInfo oMethod = oReader.GetType().GetMethod("ReadColumn", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic, null, new Type[]{typeof(int)},null);
            
            while (oReader.Read()) {
                //DataRow oRow=oTable.NewRow();
                //oRow.ItemArray = oValues;
                //for (int x = 0; x < oReader.FieldCount; x++) oRow.ItemArray[x] = oReader.GetValue(x);
                //for (int x = 0; x < oReader.FieldCount; x++) oValues[x] = oReader.GetValue(x);
                //for (int x = 0; x < oReader.FieldCount; x++) oMethod.Invoke(oReader, i);
                                
                //oTable.Rows.Add(oRow);
                oReader.GetValues(oValues);
                oTable.LoadDataRow(oValues, LoadOption.OverwriteChanges);
                iCounter++;                
            }
            for (int x = 0; x < oTable.Columns.Count; x++) {
                if (oTable.Columns[x].ColumnMapping == MappingType.Hidden) {
                    oTable.Columns.RemoveAt(x);
                    x--;
                }
            }
            oTable.EndLoadData();
            oTable.AcceptChanges();
            return iCounter;
        }
    }

    //public delegate void OnDataBeforeUpdateHandler(object sender, DataTable oData, DataTable oSchema);
    /// <summary>
    /// Summary description for ado_helper.
    /// </summary>
    public class ado_helper {
        #region DECLARATIONS

        //public event OnDataBeforeUpdateHandler DataBeforeUpdate;
        /// <summary>
        /// The connection type for the ado_helper
        /// </summary>
        public enum connection_type {
            sql,
            access,
            text, /* Text (tab) Saparated Data Source */
            csv,
            excel,
            other
        }

        private OleDbConnection m_oOleConn;
        private SqlConnection m_oConn;
        private DataSet m_oDataSet;

        private string m_sInsertCommand = "", m_sUpdateCommand = "", m_sOleInsertCommand = "", m_sOleUpdateCommand = "", m_sConnectionString = "", m_sDirectory = "", m_sFileName = "", m_sSheet = "";
        private connection_type m_iConnectionType = connection_type.sql;

        private bool m_bDisableTriggerContext = false;

        private static System.Collections.Generic.Dictionary<string, JDataTable> m_oTableDefinitions = new System.Collections.Generic.Dictionary<string, JDataTable>();
        
        private static int m_iTransactionLockingRetry = -1, m_iTransactionLockingDelay = -1;
        private static IsolationLevel? m_oGlobalTransactionIsolationLevel = null;
        private IsolationLevel? m_oTransactionIsolationLevel = null;
        #endregion

        #region CONSTRUCTORS
        /// <summary>
        /// Constructor for the ado_helper library
        /// </summary>
        public ado_helper() : this(null) { }
        /// <summary>
        /// Constructor for the ado_helper library
        /// </summary>
        /// <param name="sConnectionStringSettingsKey">Connection String Key</param>
        public ado_helper(string sConnection, connection_type oType): this(sConnection) {
            this.ConnectionType = oType;
        }
        /// <summary>
        /// Constructor for the ado_helper library
        /// </summary>
        /// <param name="sConnection">Connection String</param>
        public ado_helper(string sConnection) {
            if (convert.cStr(sConnection) == "") {
                m_sConnectionString = System.Configuration.ConfigurationManager.AppSettings["sql.dsn"];

                if (m_sConnectionString == null) {
                    m_sConnectionString = System.Configuration.ConfigurationManager.AppSettings["sql_dsn"];
                }
            } else {
                m_sConnectionString = sConnection;
                try {
                    if (System.Configuration.ConfigurationManager.AppSettings[sConnection] != null) m_sConnectionString = System.Configuration.ConfigurationManager.AppSettings[sConnection];
                } catch (Exception) { }
            }
        }
        /// <summary>
        /// Constructor for the ado_helper library
        /// </summary>
        /// <param name="sServer">The SQL Server Connecting To</param>
        /// <param name="sUID">The user id</param>
        /// <param name="sPWD">The password</param>
        /// <param name="sDatabase">The catalog name</param>
        public ado_helper(string sServer, string sUID, string sPWD, string sDatabase) {
            m_sConnectionString = "server=" + sServer + ";uid=" + sUID + ";pwd=" + sPWD + ";database=" + sDatabase + ";";
        }
        /// <summary>
        /// Constructor for the ado_helper library
        /// </summary>
        /// <param name="sDirectory">The directory source</param>
        /// <param name="sFileName">The filename for the connection</param>
        public ado_helper(string sDirectory, string sFileName) {
            m_sDirectory = sDirectory;
            m_sFileName = sFileName;

            string sExtension = ".csv";

            if (sFileName.Length > 4) sExtension = sFileName.Substring(sFileName.Length - 4, 4).ToLower();

            switch (sExtension) {
                case ".csv":
                    m_iConnectionType = connection_type.csv;
                    m_sConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + m_sDirectory + "; Extended Properties=Text;";
                    break;


                case ".txt":
                    m_iConnectionType = connection_type.text;
                    m_sConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + m_sDirectory + "; Extended Properties=Text;";
                    break;

                case ".xls":
                    m_iConnectionType = connection_type.excel;
                    m_sConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + m_sDirectory + m_sFileName + "; Extended Properties=Excel 8.0;";
                    break;

                case ".mdb":
                    m_iConnectionType = connection_type.access;

                    break;

            }
        }

        /// <summary>
        /// Constructor for the ado_helper library
        /// </summary>
        /// <param name="sDirectory">The directory source</param>
        /// <param name="sFileName">The filename for the connection</param>
        /// <param name="iType">The type of connection</param>
        public ado_helper(string sDirectory, string sFileName, connection_type iType) {
            m_iConnectionType = iType;
            m_sDirectory = sDirectory;
            m_sFileName = sFileName;

            switch (iType) {
                case connection_type.csv:
                    m_sConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + m_sDirectory + "; Extended Properties=Text;";
                    break;

                //				case connection_type.tab:
                //					m_sConnectionString				= @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + m_sDirectory + "; Extended Properties='text;FMT=TabDelimited;HDR=NO'";
                //					break;

                case connection_type.text:
                    m_sConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + m_sDirectory + "; Extended Properties=Text;";
                    break;

                case connection_type.excel:
                    m_sConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + m_sDirectory + "; Extended Properties=Excel 8.0;";
                    break;

                case connection_type.access:

                    break;

                case connection_type.other:
                    m_sConnectionString = sDirectory;
                    break;

            }

        }
        #endregion CONSTRUCTORS

        #region PROPERTIES

        //public DataTable CurrentSchema {
        //    get {
        //        return (RetrieveSchema && m_oDataSet != null && m_oDataSet.Tables[0] != null && m_oDataSet.Tables[0].Columns.Count == m_oDataTable.Columns.Count ? m_oDataSet.Tables[0] : null);
        //    }
        //}

        public bool DisableTriggerContext {
            get {
                return m_bDisableTriggerContext;
            }
            set {
                m_bDisableTriggerContext = value;
            }
        }

        /// <summary>
        /// The SQL Insert Statement, "" means automatic
        /// </summary>
        public string InsertCommand {
            get {
                return m_sInsertCommand;
            }
            set {
                m_sInsertCommand = value;
            }
        }

        /// <summary>
        /// The SQL Update Statement, "" means automatic
        /// </summary>
        public string UpdateCommand {
            get {
                return m_sUpdateCommand;
            }
            set {
                m_sUpdateCommand = value;
            }
        }


        /// <summary>
        /// The SQL Insert Statement, "" means automatic
        /// </summary>
        public string OleInsertCommand {
            get {
                return m_sOleInsertCommand;
            }
            set {
                m_sOleInsertCommand = value;
            }
        }

        /// <summary>
        /// The SQL Update Statement, "" means automatic
        /// </summary>
        public string OleUpdateCommand {
            get {
                return m_sOleUpdateCommand;
            }
            set {
                m_sOleUpdateCommand = value;
            }
        }


        /// <summary>
        /// The name of the excel sheet to load
        /// </summary>
        public string Sheet {
            get {
                return m_sSheet;
            }
            set {
                m_sSheet = value;
            }

        }

        /// <summary>
        /// The directory for the ole datasource
        /// </summary>
        public string Directory {
            get {
                return m_sDirectory;
            }
            set {
                m_sDirectory = value;
            }

        }

        /// <summary>
        /// The filename of the datasource that is being loaded (ole)
        /// </summary>
        public string FileName {
            get {
                return m_sFileName;
            }
            set {
                m_sFileName = value;
            }

        }

        /// <summary>
        /// The connection string for the desired data source
        /// </summary>
        public string ConnectionString {
            get {
                return m_sConnectionString;
            }
            set {
                m_sConnectionString = value;
            }
        }

        /// <summary>
        /// The connection type, sql, access, txt, csv, xls
        /// </summary>
        public connection_type ConnectionType {
            get {
                return m_iConnectionType;
            }
            set {
                m_iConnectionType = value;
            }

        }

        /// <summary>
        /// The SQL Connection object
        /// </summary>
        public SqlConnection Conn {
            get {
                return m_oConn;
            }
            set {
                m_oConn = value;
            }

        }

        /// <summary>
        /// The OLE Connection object
        /// </summary>
        public OleDbConnection OleConn {
            get {
                return m_oOleConn;
            }
            set {
                m_oOleConn = value;
            }
        }


        /// <summary>
        /// The helpers returned DataSet Object
        /// </summary>
        public DataSet dataSet {
            get {
                return m_oDataSet;
            }
            set {
                m_oDataSet = value;
            }
        }

        private static IsolationLevel GlobalTransactionIsolationLevel {
            get {
                if (m_oGlobalTransactionIsolationLevel == null) {
                    if (ConfigurationManager.AppSettings["transaction.isolation.level"] == "Snapshot") m_oGlobalTransactionIsolationLevel = IsolationLevel.Snapshot;
                    else if (ConfigurationManager.AppSettings["transaction.isolation.level"] == "ReadCommitted") m_oGlobalTransactionIsolationLevel = IsolationLevel.ReadCommitted;
                    else if (ConfigurationManager.AppSettings["transaction.isolation.level"] == "ReadUncommitted") m_oGlobalTransactionIsolationLevel = IsolationLevel.ReadUncommitted;
                    else if (ConfigurationManager.AppSettings["transaction.isolation.level"] == "Chaos") m_oGlobalTransactionIsolationLevel = IsolationLevel.Chaos;                    
                    else if (ConfigurationManager.AppSettings["transaction.isolation.level"] == "RepeatableRead") m_oGlobalTransactionIsolationLevel = IsolationLevel.RepeatableRead;
                    else if (ConfigurationManager.AppSettings["transaction.isolation.level"] == "Serializable") m_oGlobalTransactionIsolationLevel = IsolationLevel.Serializable;                    
                    else m_oGlobalTransactionIsolationLevel = IsolationLevel.Unspecified;
                }
                return m_oGlobalTransactionIsolationLevel.Value;
            }
        }
        public static string getTransactionIsolationLevel(IsolationLevel? oTransactionIsolationLevel, IsolationLevel oGlobalTransactionIsolationLevel) {
            IsolationLevel oLevel = (oTransactionIsolationLevel != null ? oTransactionIsolationLevel.Value : oGlobalTransactionIsolationLevel);
            if (oLevel == IsolationLevel.Unspecified) return "";
            return "set transaction isolation level " + (oLevel == IsolationLevel.ReadUncommitted ? "read uncommitted" : (oLevel == IsolationLevel.ReadCommitted ? "read committed" : (oLevel == IsolationLevel.Snapshot ? "snapshot" : (oLevel == IsolationLevel.RepeatableRead ? "snapshot" : (oLevel == IsolationLevel.Serializable ? "serializable" : "repeatable read"))))) + ";";                            
        }
        public IsolationLevel? TransactionIsolationLevel {
            get {
                return m_oTransactionIsolationLevel;
            }
            set {
                m_oTransactionIsolationLevel = value;
            }
        }
        private static int TransactionLockingRetry{
            get{
                if (m_iTransactionLockingRetry < 0) m_iTransactionLockingRetry = convert.cInt(ConfigurationManager.AppSettings["transaction.locking.retry"],1);
                return m_iTransactionLockingRetry;
            }
        }
        private static int TransactionLockingDelay {
            get {
                if (m_iTransactionLockingDelay < 0) m_iTransactionLockingDelay = convert.cInt(ConfigurationManager.AppSettings["transaction.locking.delay"],500);
                return m_iTransactionLockingDelay;
            }
        }        
            
        #endregion PROPERTIES

        #region EVENTS
        //protected virtual void OnDataBeforeUpdate(DataTable oData, DataTable oSchema) {
        //    if (DataBeforeUpdate != null) DataBeforeUpdate(this, oData, oSchema);
        //}

        #endregion EVENTS
        #region METHODS
        public static JDataTable getTableDefintion(ado_helper oData, string sTable) {
            lock (m_oTableDefinitions) {
                if (sTable == null) {
                    m_oTableDefinitions.Clear();
                    return null;
                }
                if (!m_oTableDefinitions.ContainsKey(oData.ConnectionString + ":" + sTable)) {
                    JDataTable o = oData.Execute_SQL(oData.ConnectionType == connection_type.sql ? "SELECT TOP 0 * FROM [" + sTable + "]" : "SELECT * FROM " + sTable + " where rownum=0");
                    m_oTableDefinitions[oData.ConnectionString + ":" + sTable] = o;
                }
                return m_oTableDefinitions[oData.ConnectionString + ":" + sTable];
            }
        }
        public static JDataTable pivotTable(JDataTable oDT, string sKeyColumn, string sPivotNameColumn, string sPivotValueColumn, bool bOrderColsAlphabetically) {
            return pivotTable(oDT, sKeyColumn, sPivotNameColumn, null, sPivotValueColumn, bOrderColsAlphabetically);
        }
        public static JDataTable pivotTable(JDataTable oDT, string sKeyColumn, string sPivotNameColumn, string[] sPivotCols, string sPivotValueColumn, bool bOrderColsAlphabetically) {
            JDataTable oNewTable = new JDataTable();
            string sLastKey = "//dummy//";
            bool bFirstRow = true;

            // Add non-pivot columns to the data table:

            for (int i = 0; i < oDT.Columns.Count; i++)
                if (i != oDT.Columns.IndexOf(sPivotNameColumn) && i != oDT.Columns.IndexOf(sPivotValueColumn))
                    oNewTable.Columns.Add(oDT.Columns[i].ColumnName, oDT.Columns[i].DataType);
            if (sPivotCols != null) {
                for (int x = 0; x < sPivotCols.Length; x++) oNewTable.Columns.Add(sPivotCols[x], oDT.Columns[sPivotNameColumn].DataType);
            }
            DataRow oRow = oNewTable.NewRow();

            // now, fill up the table with the data:
            for (int x = 0; x < oDT.Rows.Count; x++) {
                // see if we need to start a new row
                if (ado_helper.getPKValue(oDT.Rows[x], sKeyColumn) != sLastKey) {

                    if (!bFirstRow) oNewTable.Rows.Add(oRow);
                    oRow = oNewTable.NewRow();
                    bFirstRow = false;
                    // Add all non-pivot column values to the new row:
                    for (int i = 0; i <= oDT.Columns.Count - 3; i++)
                        oRow[i] = oDT.Rows[x][oNewTable.Columns[i].ColumnName];
                    sLastKey = ado_helper.getPKValue(oDT.Rows[x], sKeyColumn);
                }
                // assign the pivot values to the proper column; add new columns if needed:
                string s = convert.cStr(oDT.Rows[x][sPivotNameColumn],"[blank]");
                if (sPivotCols == null || oNewTable.Columns.Contains(s)) {
                    if (!oNewTable.Columns.Contains(s)) {
                        DataColumn c = oNewTable.Columns.Add(s, oDT.Columns[sPivotValueColumn].DataType);
                        if (bOrderColsAlphabetically) {
                            int newOrdinal = c.Ordinal;
                            for (int i = newOrdinal - 1; i >= oDT.Columns.Count - 2; i--)
                                if (c.ColumnName.CompareTo(oNewTable.Columns[i].ColumnName) < 0)
                                    newOrdinal = i;
                            c.SetOrdinal(newOrdinal);
                        }
                    }
                    oRow[s] = oDT.Rows[x][sPivotValueColumn];
                }
            }

            oNewTable.Rows.Add(oRow);
            return oNewTable;
        }

        public static string getPKValue(DataRow oRow, string sPK) {
            string[] sPKs = parse.split(sPK, ",");
            string sValue = "";
            for (int x = 0; x < sPKs.Length; x++)
                sValue += (sValue == "" ? "" : ",") + oRow[sPKs[x]];
            return sValue;
        }
        public static object[] getPKParameters(object oPKValue, string sPKField) {
            string[] sPKArr = parse.split(sPKField, ",");
            string[] sValues = (sPKArr.Length == 1 ? new string[] { convert.cStr(oPKValue) } : parse.split(oPKValue, ","));
            object[] oPKs = new object[sPKArr.Length * 2];
            for (int x = 0; x < sPKArr.Length; x++) {
                if (x < sPKArr.Length) oPKs[x * 2] = sPKArr[x];
                if (x < sValues.Length) oPKs[(x * 2) + 1] = sValues[x];
            }
            return oPKs;
        }

        public static string getPKString(object[] oPKParameters) {
            string sValue = "";
            for (int x = 0; x < oPKParameters.Length; x = x + 2)
                sValue += oPKParameters[x] + "='" + ado_helper.PrepareDB(oPKParameters[x + 1]) + "' " + (x < oPKParameters.Length - 2 ? "and " : "");

            return sValue;
        }
        public static string getSQLValue(DataRow oRow, string sColName, OLEDBEngine oEngine) {
            return getSQLValue(oRow.Table.Columns[sColName].DataType.FullName, oRow[sColName], oEngine);
        }
        public static string getSQLValue(string sDataTypeName, object oObj, OLEDBEngine oEngine) {
            switch (sDataTypeName) {

                case "System.DateTime":
                    if (convert.cStr(oObj).Trim() == "") {
                        switch (oEngine) {

                            case OLEDBEngine.FoxPro:
                                return "{}";

                            case OLEDBEngine.SQL: //sql
                            default:
                                return "null";
                        }
                    } else {

                        switch (oEngine) {

                            case OLEDBEngine.FoxPro://foxpro
                                return "{" + oObj + "}";


                            case OLEDBEngine.Oracle:
                                return "TO_DATE('" + String.Format("{0:yyyy-MM-dd HH:mm:ss}", convert.cDate(oObj)) + "','yyyy-mm-dd hh24:mi:ss')";


                            case OLEDBEngine.SQL:
                            default:
                                return "'" + oObj + "'";
                        }

                    }


                /*case System.Data.DbType.Currency:
                case System.Data.DbType.Decimal:
                case System.Data.DbType.Double:
                case System.Data.DbType.Int16:
                case System.Data.DbType.Int32:
                case System.Data.DbType.Int64:
                case System.Data.DbType.Single:
                case System.Data.DbType.UInt16:
                case System.Data.DbType.UInt64:
                case System.Data.DbType.VarNumeric:
*/

                case "System.Decimal":
                case "System.Double":
                    if (oEngine == OLEDBEngine.Oracle)
                        return (oObj == DBNull.Value ? "null" : convert.cStr(oObj));
                    else
                        return convert.cDbl(oObj).ToString();


                case "System.Boolean":
                    if (oEngine == OLEDBEngine.Oracle)
                        return (oObj == DBNull.Value ? "null" : convert.cStr(oObj));
                    else
                        return (convert.cBool(oObj) ? "1" : "0");

                case "System.String":
                default:
                    return "'" + parse.replaceAll(oObj, "'", "''") + "'";
            }
        }

        public string getInsertStatement(DataRow oRow, DataTable oTable, string sTableName, OLEDBEngine oEngine) {

            string sFields = "insert into " + sTableName + " (";
            string sValues = " values (";
            for (int x = 0; x < oTable.Columns.Count; x++) {
                sFields = sFields + oTable.Columns[x].ColumnName + ", ";
                sValues += getSQLValue(oTable.Columns[x].DataType.FullName, oRow[x], oEngine) + " ,";
            }
            return sFields.Substring(0, sFields.Length - 2) + ") " + sValues.Substring(0, sValues.Length - 2) + ")" + (oEngine == OLEDBEngine.Oracle ? ";" : "");
        }

        public enum OLEDBEngine {
            SQL = 1,
            FoxPro = 2,
            Oracle = 3
        }

        public bool OleInsertDataRow(DataRow oRow, DataTable oTable, string sTableName, OLEDBEngine oEngine) {

            System.Data.OleDb.OleDbCommand oComm = new OleDbCommand();
            oComm.CommandType = System.Data.CommandType.Text;
            oComm.CommandTimeout = 1800;

            string sFields = "insert into " + sTableName + " (";
            string sValues = " values (";
            string sValues1 = " values (";
            for (int x = 0; x < oTable.Columns.Count; x++) {
                sFields = sFields + oTable.Columns[x].ColumnName + ", ";


                switch (oTable.Columns[x].DataType.FullName) {

                    case "System.DateTime"://System.TypeCode.DateTime:
                        if (oRow[x].ToString().Trim() == "") {
                            switch (oEngine) {

                                case OLEDBEngine.FoxPro://foxpro
                                    sValues += "{}, ";
                                    sValues1 += "{}, ";
                                    break;

                                case OLEDBEngine.SQL: //sql								
                                default:
                                    sValues += "null, ";
                                    sValues1 += "null, ";
                                    break;
                            }
                        } else {

                            switch (oEngine) {

                                case OLEDBEngine.FoxPro://foxpro
                                    sValues += "{" + oRow[x].ToString().Trim() + "}, ";
                                    sValues1 += "{" + oRow[x].ToString().Trim() + "}, ";
                                    break;

                                case OLEDBEngine.SQL: //sql																
                                default:
                                    sValues += "'" + oRow[x].ToString().Trim() + "', ";
                                    sValues1 += "'" + oRow[x].ToString().Trim() + "', ";
                                    break;
                            }

                        }
                        break;

                    /*case System.Data.DbType.Currency:
                    case System.Data.DbType.Decimal:
                    case System.Data.DbType.Double:
                    case System.Data.DbType.Int16:
                    case System.Data.DbType.Int32:
                    case System.Data.DbType.Int64:
                    case System.Data.DbType.Single:
                    case System.Data.DbType.UInt16:
                    case System.Data.DbType.UInt64:
                    case System.Data.DbType.VarNumeric:
*/

                    case "System.Decimal":
                    case "System.Double":
                        sValues += convert.cDbl(oRow[x]) + ", ";
                        sValues1 += convert.cDbl(oRow[x]) + ", ";
                        break;

                    case "System.Boolean":
                        sValues += (convert.cBool(oRow[x].ToString().Trim()) ? "1" : "0") + ", ";
                        sValues1 += (convert.cBool(oRow[x].ToString().Trim()) ? "1" : "0") + ", ";
                        break;

                    case "System.String":
                    default:

                        sValues += "?, ";
                        System.Data.OleDb.OleDbParameter oParam = new System.Data.OleDb.OleDbParameter();
                        oComm.Parameters.Add(oParam);
                        oParam.Value = oRow[x].ToString();
                        sValues1 += "'" + ado_helper.PrepareDB(oRow[x], false) + "', ";

                        break;
                }
            }

            try {
                connect();
                oComm.CommandText = sFields.Substring(0, sFields.Length - 2) + ") " + sValues.Substring(0, sValues.Length - 2) + ")";
                oComm.Connection = this.OleConn;
                oComm.ExecuteNonQuery();
                disconnect();
                return true;
            } catch (Exception ex) {
                throw new Exception("Can't execute OleInsertDataRow: " + oComm.CommandText + "\n\n Actual SQL: " + sFields.Substring(0, sFields.Length - 2) + ") " + sValues1.Substring(0, sValues1.Length - 2) + ")\n\n" + ex.Message, ex);
            }
        }

        public void connect() {
            connect(false);
        }
        public void connect(bool beginTransaction) {

            if (m_iConnectionType == connection_type.sql) {
                if(m_oConn==null)m_oConn = new SqlConnection(m_sConnectionString);
                if (m_oConn.State != ConnectionState.Open) {
                    m_oConn.Open();
                    if (beginTransaction && DbTransaction == null) DbTransaction = m_oConn.BeginTransaction();
                }
            } else {
                if (m_oOleConn == null) m_oOleConn = new OleDbConnection(m_sConnectionString);
                if (m_oOleConn.State != ConnectionState.Open) {
                    m_oOleConn.Open();
                    if (beginTransaction && DbTransaction == null) DbTransaction = m_oOleConn.BeginTransaction();
                }
            }
        }
        public DbTransaction DbTransaction { get; set; }
        public void RollbackAndDisconnect() {
            if (DbTransaction != null) {
                DbTransaction.Rollback();
                DbTransaction = null;
            }
            disconnect();
        }
        public void CommitAndDisconnect() {
            if (DbTransaction != null) {
                DbTransaction.Commit();
                DbTransaction = null;
            }
            disconnect();
        }
        /// <summary>
        /// Disconnect from the specified data source
        /// </summary>
        public void disconnect() {            
            if (m_iConnectionType == connection_type.sql) {
                if (m_oConn != null) m_oConn.Close();
            } else {
                if(m_oOleConn!=null)m_oOleConn.Close();
            }            
        }

        /// <summary>
        /// Executes a commond on the specified OLE data source
        /// </summary>
        /// <returns>DataTable</returns>
        public JDataTable OleExecuteProc(string sCommand) {
            //connect
            connect();
            //execute
            OleDbCommand oOleCommand = new OleDbCommand(sCommand, m_oOleConn);
            oOleCommand.CommandType = System.Data.CommandType.StoredProcedure;
            oOleCommand.CommandTimeout = 1800;
            
            OleDbDataReader oReader = oOleCommand.ExecuteReader();

            JDataReaderAdapter oDataAdapter = new JDataReaderAdapter();
            m_oDataSet = new DataSet();

            JDataTable oDataTable = null;
            int iCounter = 0;
            while (true) {
                oDataTable = new JDataTable();
                oDataAdapter.FillFromReader(oDataTable, oReader);
                oDataTable.DSN = m_sConnectionString;
                //                if (iCounter == 0 && oDataTable.TableName == "") oDataTable.TableName = sTable;
                if (m_oDataSet.Tables.Contains(oDataTable.TableName)) oDataTable.TableName += ":" + iCounter;
                m_oDataSet.Tables.Add(oDataTable);
                if (oReader.IsClosed || !oReader.NextResult()) break;
                iCounter++;
            }
            oReader.Close();

            //OleDbDataAdapter oOleDataAdapter = new OleDbDataAdapter(oOleCommand);
            //try {
            //    m_oDataSet = new DataSet("sp");
            //    oOleDataAdapter.Fill(m_oDataSet, "sp");

            //    oDataTable = m_oDataSet.Tables["sp"];

            //} catch (Exception e) {
            //    Console.WriteLine(sCommand);
            //    throw new Exception("Can't execute: " + sCommand + "\n\n" + e.Message, e);
            //}
            disconnect();

            return oDataTable;

        }

        /// <summary>
        /// Executes a commond on the specified OLE data source
        /// </summary>
        /// <returns>DataTable</returns>
        private JDataTable _OleExecute(string sCommand, params object[] _oParameters) {
            List<object> oParams = new List<object>(_oParameters);
            for(int x=0;x<oParams.Count;x++){
                if(oParams[x] as SqlParameterCollection != null){
                    SqlParameterCollection oCollection = oParams[x] as SqlParameterCollection;
                    oParams.RemoveAt(x--);
                    for (int y = 0; y < oCollection.Count; y++) oParams.Add(oCollection[y]);
                }
            }
            //connect
            connect();
            //execute
            OleDbCommand oOleCommand = new OleDbCommand(sCommand, m_oOleConn);
            oOleCommand.CommandType = System.Data.CommandType.Text;
            oOleCommand.CommandTimeout = 1800;

            Dictionary<string, OleDbParameter> oParamList = new Dictionary<string, OleDbParameter>();
            for (int i = 0; i < oParams.Count; i++) {
                OleDbParameter oParam = oParams[i] as OleDbParameter;
                if (oParam == null && oParams[i] as SqlParameter != null) oParam = new OleDbParameter(":" + (oParams[i] as SqlParameter).ParameterName.Substring(1), (oParams[i] as SqlParameter).Value);
                if (oParam == null) oParam = new OleDbParameter(":" + oParams[i].ToString().Substring(1), oParams[++i]);
                if (!oParamList.ContainsKey(oParam.ParameterName)){
                    oParamList.Add(oParam.ParameterName, oParam);
                    oOleCommand.CommandText = parse.replaceAll(oOleCommand.CommandText, "@" + oParam.ParameterName.Substring(1), ":" + oParam.ParameterName.Substring(1));
                }
            }
            string[] sArr = parse.split(oOleCommand.CommandText, ":");
            for (int i = 0; i < sArr.Length; i++) {
                string sKey = parse.findSeparator(sArr[i], " ", ",", "=", "\n", "\r",")","(",";");
                if (sKey == "") sKey = sArr[i];
                else sKey = sArr[i].Substring(0, sArr[i].IndexOf(sKey));
                if (sKey != "" && oParamList.ContainsKey(":" + sKey)) {
                    oOleCommand.Parameters.Add(oOleCommand.Parameters.Contains(oParamList[":" + sKey])? new OleDbParameter(oParamList[":" + sKey].ParameterName+"_"+i,oParamList[":" + sKey].Value) : oParamList[":" + sKey]);
                }
            }
            JDataTable oDataTable=null;
            try {
                OleDbDataReader oReader = ((" " + sCommand).IndexOf(" insert ", StringComparison.CurrentCultureIgnoreCase) > -1 || (" " + sCommand).IndexOf(" update ", StringComparison.CurrentCultureIgnoreCase) > -1 || (" " + sCommand).IndexOf(" delete ", StringComparison.CurrentCultureIgnoreCase) > -1 ? oOleCommand.ExecuteReader() : oOleCommand.ExecuteReader(CommandBehavior.KeyInfo));

                //            JDataReaderAdapter oDataAdapter = new JDataReaderAdapter();
                //            m_oDataSet = new DataSet();

                //            JDataTable oDataTable = null;
                //            int iCounter = 0;
                //            while (true) {
                //                oDataTable = new JDataTable();
                //                oDataAdapter.FillFromReader(oDataTable, oReader);
                //                oDataTable.DSN = m_sConnectionString;
                ////                if (iCounter == 0 && oDataTable.TableName == "") oDataTable.TableName = sTable;
                //                if (m_oDataSet.Tables.Contains(oDataTable.TableName)) oDataTable.TableName += ":" + iCounter;
                //                m_oDataSet.Tables.Add(oDataTable);
                //                if (oReader.IsClosed || !oReader.NextResult()) break;
                //                iCounter++;
                //            }
                oDataTable = getTableFromReader(oReader, "", true);            
            //OleDbDataAdapter oOleDataAdapter = new OleDbDataAdapter(oOleCommand);
            //try {
            //    m_oDataSet = new DataSet("sp");
            //    oOleDataAdapter.Fill(m_oDataSet, "sp");

            //    oDataTable = m_oDataSet.Tables["sp"];

            } catch (Exception e) {
                Console.WriteLine(sCommand);
                throw new Exception("Can't execute: " + sCommand + "\n\n" + e.Message, e);
            }
            disconnect();

            return oDataTable;

        }

        /// <summary>
        /// Executes a command on the SQL Data source
        /// </summary>
        /// <param name="oCommandType">Specifies whether the command is table-direct, stored-procedure, text</param>
        /// <param name="oParameters">The SqlParamterCollection</param>
        /// <returns>DataTable of the results</returns>
        private JDataTable _Execute(string sCommand, params object[] oParameters) {
            if (this.ConnectionType != connection_type.sql) return _OleExecute(sCommand, oParameters);
            SqlCommand oSqlCommand = null;
            try {
                bool isPrevConnected = (this.Conn != null && this.DbTransaction != null);
                if(!isPrevConnected) connect();

                string TableNames = "";
                string sTable = "sp";

                //bool bUpdatesTable = true;
                string sSQL = parse.replaceAll(" " + sCommand + " ", "\n", " ", "\t", " ", "\r", "", ";", "; ");
                if (sSQL.IndexOf(" update ", StringComparison.CurrentCultureIgnoreCase) > -1) {
                    TableNames = parse.inner_substring_i(sSQL, " update ", null, " set ", null).Trim();
                } else if (sSQL.IndexOf(" insert ", StringComparison.CurrentCultureIgnoreCase) > -1) {
                    TableNames = parse.inner_substring_i(sSQL, " insert ", "into", " values", null).Trim();
                    if (TableNames == "") TableNames = parse.inner_substring_i(sSQL, " insert ", "into", "(", null).Trim();
                } else if (sSQL.IndexOf(" delete ", StringComparison.CurrentCultureIgnoreCase) > -1) {
                    TableNames = parse.inner_substring_i(sSQL, " delete ", "from", " where ", null).Trim();
                } else if (sSQL.IndexOf(" select ", StringComparison.CurrentCultureIgnoreCase) > -1) {
                    TableNames = parse.inner_substring_i(sSQL, " select ", "from ", " ", null).Trim();
                    //bUpdatesTable = false;
                }
                if (TableNames.IndexOf("(") > -1) TableNames = parse.split(TableNames, "(")[0];
                if (TableNames.IndexOf(";") > -1) TableNames = parse.split(TableNames, ";")[0];
                sTable = (TableNames == "" ? "sp" : convert.cStr(parse.split(TableNames, ",")[0], "sp"));
                if (DisableTriggerContext) sCommand = "SET Context_Info 0x55555;" + sCommand + ";SET Context_Info 0;";
                sCommand = getTransactionIsolationLevel(TransactionIsolationLevel, GlobalTransactionIsolationLevel) + sCommand;

                //SqlTransaction oTrans = m_oConn.BeginTransaction(); //(this.TransactionIsolationLevel!=null ? m_oConn.BeginTransaction(TransactionIsolationLevel.Value) : (ado_helper.GlobalTransactionIsolationLevel == IsolationLevel.Unspecified ? m_oConn.BeginTransaction() : m_oConn.BeginTransaction(ado_helper.GlobalTransactionIsolationLevel)));
                oSqlCommand = new SqlCommand(sCommand, m_oConn);
                oSqlCommand.CommandType = CommandType.Text;
                oSqlCommand.CommandTimeout = 1800;
                if (isPrevConnected) oSqlCommand.Transaction = this.DbTransaction as SqlTransaction;

                for (int i = 0; i < oParameters.Length; i++) {
                    if (oParameters[i] as SqlParameterCollection != null) {
                        SqlParameterCollection oCollection = oParameters[i] as SqlParameterCollection;
                        foreach (SqlParameter oParam1 in oCollection) {
                            SqlParameter oParam = new SqlParameter(oParam1.ParameterName, oParam1.Value);
                            if (oParam.SqlDbType == System.Data.SqlDbType.NVarChar) oParam.SqlDbType = System.Data.SqlDbType.VarChar;
                            oSqlCommand.Parameters.Add(oParam);
                        }
                    } else {
                        SqlParameter oParam = new SqlParameter(oParameters[i].ToString(), oParameters[i + 1]);
                        if (oParam.SqlDbType == System.Data.SqlDbType.NVarChar) oParam.SqlDbType = System.Data.SqlDbType.VarChar;
                        oSqlCommand.Parameters.Add(oParam);
                        i++;
                    }
                }

                //oDataAdapter.MissingSchemaAction = System.Data.MissingSchemaAction.Ignore;
                SqlDataReader oReader = null;
                for (int x = 0; ; x++) {
                    try {
                        oReader = oSqlCommand.ExecuteReader(CommandBehavior.KeyInfo);
                        break;
                    } catch (Exception e) {
                        if (TransactionLockingRetry - x > 0 && e.Message.Contains("was deadlocked on lock resources")) System.Threading.Thread.Sleep((jlib.helpers.general.Random(TransactionLockingDelay, TransactionLockingDelay*4) / 2));
                        else throw e;
                    }
                }
                
                JDataTable oDataTable = getTableFromReader(oReader, sTable, true);

                if (!isPrevConnected) disconnect();

                return (m_oDataSet.Tables.Count == 0 ? null : m_oDataSet.Tables[0] as JDataTable);
            } catch (Exception ex) {
                string s="SQL: " + sCommand;
                for (int x = 0; x < oParameters.Length; x++) {
                    if (oParameters[x] as SqlParameterCollection != null) {
                        foreach (SqlParameter oParam in oParameters[x] as SqlParameterCollection) s += ", " + oParam.ParameterName + "=" + oParam.Value;
                    } else s += ", " + oParameters[x] + "=" + oParameters[++x];                                            
                }
                throw new Exception(s, ex);
            }
        }

        public JDataTable getTableFromReader(DbDataReader oReader, string sTable, bool bCloseReader) {
            JDataReaderAdapter oDataAdapter = new JDataReaderAdapter();
            if (sTable.Str() == "") sTable = "sp";
            m_oDataSet = new DataSet(sTable);
            JDataTable oDataTable = null;
            int iCounter = 0;
            while (true) {
                oDataTable = new JDataTable();
                oDataAdapter.FillFromReader(oDataTable, oReader);
                oDataTable.DSN = m_sConnectionString;
                if (iCounter == 0 && oDataTable.TableName == "") oDataTable.TableName = sTable;
                if (m_oDataSet.Tables.Contains(oDataTable.TableName)) oDataTable.TableName += ":" + iCounter;
                m_oDataSet.Tables.Add(oDataTable);
                if (oReader.IsClosed || !oReader.NextResult()) break;
                iCounter++;
            }
            if (bCloseReader) oReader.Close();
            return oDataTable;
        }

        void oDataAdapter_FillError(object sender, FillErrorEventArgs e) {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Executes a SQL Statement on the SQL Server
        /// </summary>
        /// <param name="sCommand">The SQL Statement to run</param>
        /// <param name="sParameters">The Array of parameters in the format (a1,a2,b1,b2,...,n1,n2) where a1 would be the parameter id(i.e. "@id") and a2 would be the value(i.e 1 or "vsi")</param>
        /// <returns>DataTable of the Results</returns>
        public JDataTable Execute_SQL(string sCommand, params object[] sParameters) {
            return _Execute(sCommand, sParameters);
        }

        /// <summary>
        /// Executes a Formattable SQL Statement on the SQL Server, 
        /// </summary>
        /// <param name="sCommand">The SQL Statement to run (as a formattable string.. ie select * from d_table where field1 = '{0}')</param>
        /// <param name="sParameters">The Array of parameters in the format (n1,n2,n3)</param>
        /// <returns>DataTable of the Results</returns>
        public JDataTable Execute_SQLF(string sCommand, params object[] sParameters) {
            for (int x = 0; x < sParameters.Length; x++) {
                if (sParameters[x].GetType() == typeof(double) || sParameters[x].GetType() == typeof(float) || sParameters[x].GetType() == typeof(decimal))
                    sParameters[x] = parse.replaceAll(sParameters[x].ToString(), ",", ".");
            }

            return Execute_SQL(String.Format(sCommand, sParameters));
        }

        /// <summary>
        /// Executes a SQL Statement on the OLE DataSource
        /// </summary>
        /// <param name="sCommand">The SQL Statement to Execute</param>
        /// <returns>The DataTable of Results</returns>
        private JDataTable OleExecute_SQL(string sCommand) {
            return _OleExecute(sCommand);
        }


        private object OleExecute_SQL_Return_value(string sCommand) {
            return OleExecute_SQL_Return_value(sCommand, null);
        }
        /// <summary>
        /// Executes a SQL Query on an OLE DataSource and returns a single field
        /// </summary>
        /// <param name="sCommand">Command to Execute</param>
        /// <param name="sField">Field to return</param>
        /// <returns>The value of the selected field</returns>
        private object OleExecute_SQL_Return_value(string sCommand, string sField) {
            JDataTable oDataTable = _OleExecute(sCommand);

            if (oDataTable.Rows.Count > 0) return (sField == null || sField == "" ? oDataTable.Rows[0][0] : oDataTable.Rows[0][sField]);

            return "";
        }


        /// <summary>
        /// Saves the datatable as a character separated file
        /// </summary>
        /// <param name="oDataTable">The DataTable</param>
        /// <param name="sFileName">The Filename to save the dataset as</param>
        /// <param name="sColSeparator">The Separator to use between columns</param>
        /// <param name="sRowSeparator">The Separator to use between rows</param>
        /// <returns>True if successful</returns>
        public static bool Save_Character_Separated_File(DataTable oDataTable, string sFileName, string sColSeparator, string sRowSeparator, string sFieldEncapsulation, string[] sEscapeCharacters, string[] sEscapeCharactersRepl) {
            return Save_Character_Separated_File(oDataTable, sFileName, sColSeparator, sRowSeparator, sFieldEncapsulation, sEscapeCharacters, sEscapeCharactersRepl, null, (convert.cStr(sFileName) == "" ? false : true));
        }

        /// <summary>
        /// Saves the datatable as a character separated file
        /// </summary>
        /// <param name="oDataTable">The DataTable</param>
        /// <param name="sFileName">The Filename to save the dataset as</param>
        /// <param name="sColSeparator">The Separator to use between columns</param>
        /// <param name="sRowSeparator">The Separator to use between rows</param>
        /// <param name="sFields">A tab separated string of fields to be exported (can be numeric [field index] or string [field name]</param>		
        /// <returns>True if successful</returns>
        public static bool Save_Character_Separated_File(DataTable oDataTable, string sFileName, string sColSeparator, string sRowSeparator, string sFieldEncapsulation, string[] sEscapeCharacters, string[] sEscapeCharactersRepl, string sColumns) {
            return Save_Character_Separated_File(oDataTable, ref sFileName, sColSeparator, sRowSeparator, sFieldEncapsulation, sEscapeCharacters, sEscapeCharactersRepl, sColumns, null, (convert.cStr(sFileName) == "" ? false : true));
        }

        /// <summary>
        /// Saves the datatable as a character separated file
        /// </summary>
        /// <param name="oDataTable">The DataTable</param>
        /// <param name="sFileName">The Filename to save the dataset as</param>
        /// <param name="sColSeparator">The Separator to use between columns</param>
        /// <param name="sRowSeparator">The Separator to use between rows</param>
        /// <param name="sFields">A tab separated string of fields to be exported (can be numeric [field index] or string [field name]</param>		
        /// <returns>True if successful</returns>
        public static bool Save_Character_Separated_File(DataTable oDataTable, string sFileName, string sColSeparator, string sRowSeparator, string sFieldEncapsulation, string[] sEscapeCharacters, string[] sEscapeCharactersRepl, string sColumns, bool bSaveToDisk) {
            return Save_Character_Separated_File(oDataTable, ref sFileName, sColSeparator, sRowSeparator, sFieldEncapsulation, sEscapeCharacters, sEscapeCharactersRepl, sColumns, null, bSaveToDisk);
        }



        /// <summary>
        /// Saves the datatable as a character separated file
        /// </summary>
        /// <param name="oDataTable">The DataTable</param>
        /// <param name="sFileName">The Filename to save the dataset as</param>
        /// <param name="sColSeparator">The Separator to use between columns</param>
        /// <param name="sRowSeparator">The Separator to use between rows</param>
        /// <param name="sFields">A tab separated string of fields to be exported (can be numeric [field index] or string [field name]</param>
        /// <param name="sColumnHeaders">A tab separated string of column headers</param>		
        /// <returns>True if successful</returns>
        public static bool Save_Character_Separated_File(DataTable oDataTable, string sFileName, string sColSeparator, string sRowSeparator, string sFieldEncapsulation, string[] sEscapeCharacters, string[] sEscapeCharactersRepl, string sColumnList, string sColumnHeaders) {
            return Save_Character_Separated_File(oDataTable, ref sFileName, sColSeparator, sRowSeparator, sFieldEncapsulation, sEscapeCharacters, sEscapeCharactersRepl, sColumnList, sColumnHeaders, (convert.cStr(sFileName) == "" ? false : true));
        }
        /// <summary>
        /// Saves the datatable as a character separated file
        /// </summary>
        /// <param name="oDataTable">The DataTable</param>
        /// <param name="sFileName">The Filename to save the dataset as</param>
        /// <param name="sColSeparator">The Separator to use between columns</param>
        /// <param name="sRowSeparator">The Separator to use between rows</param>
        /// <param name="sFields">A tab separated string of fields to be exported (can be numeric [field index] or string [field name]</param>
        /// <param name="sColumnHeaders">A tab separated string of column headers</param>		
        /// <returns>True if successful</returns>
        public static bool Save_Character_Separated_File(DataTable oDataTable, ref string sFileName, string sColSeparator, string sRowSeparator, string sFieldEncapsulation, string[] sEscapeCharacters, string[] sEscapeCharactersRepl, string sColumnList, string sColumnHeaders, bool bSaveToDisk) {

            string sColumns = "";

            if (sColumnList == null) {
                sColumns = "";
                for (int x = 0; x < oDataTable.Columns.Count; x++) {
                    sColumns += x + "\t";
                }
            } else {
                string[] sArr = parse.split(sColumnList, "\t");
                for (int x = 0; x < sArr.Length; x++) {
                    if (convert.isNumeric(sArr[x])) {
                        sColumns += sArr[x].Trim() + "\t";
                    } else {
                        for (int y = 0; y < oDataTable.Columns.Count; y++) {
                            if (oDataTable.Columns[y].ColumnName.ToLower() == sArr[x].ToLower().Trim()) {
                                sColumns += y + "\t";
                                break;
                            }
                        }
                    }
                }
            }

            if (sColumns.Substring(sColumns.Length - 1) == "\t")
                sColumns = sColumns.Substring(0, sColumns.Length - 1);

            string[] sColMapping = parse.split(sColumns, "\t");

            if (bSaveToDisk && System.IO.File.Exists(sFileName)) {
                try {
                    System.IO.File.Delete(sFileName);
                } catch (Exception) { }
            }

            System.IO.TextWriter oWriter = null;
            if (bSaveToDisk)
                oWriter = System.IO.File.CreateText(sFileName);
            else
                oWriter = new System.IO.StringWriter();

            string sData;


            if (sColumnHeaders != null) {

                string[] sArr = parse.split(sColumnHeaders, "\t");
                for (int x = 0; x < sArr.Length; x++) {

                    sData = sArr[x].Trim();

                    if (sEscapeCharacters != null) {
                        for (int z = 0; z < sEscapeCharacters.Length; z++) {
                            sData = sData.Replace(sEscapeCharacters[z], sEscapeCharactersRepl[z]);
                        }
                    }

                    oWriter.Write(sFieldEncapsulation + sData + sFieldEncapsulation);

                    if (sArr.Length > x + 1)
                        oWriter.Write(sColSeparator);

                }

            } else {

                for (int x = 0; x < sColMapping.Length; x++) {

                    if (sColMapping[x] != "") {
                        sData = oDataTable.Columns[convert.cInt(sColMapping[x])].Caption;

                        if (sEscapeCharacters != null) {
                            for (int z = 0; z < sEscapeCharacters.Length; z++) {
                                sData = sData.Replace(sEscapeCharacters[z], sEscapeCharactersRepl[z]);
                            }
                        }

                        oWriter.Write(sFieldEncapsulation + sData + sFieldEncapsulation);
                    }

                    if (sColMapping[x] != "" && sColMapping.Length > x + 1)
                        oWriter.Write(sColSeparator);
                }
            }

            oWriter.Write(sRowSeparator);

            for (int x = 0; x < oDataTable.Rows.Count; x++) {

                for (int y = 0; y < sColMapping.Length; y++) {

                    if (sColMapping[y] != "") {


                        sData = oDataTable.Rows[x][convert.cInt(sColMapping[y])].ToString();
                        if (sEscapeCharacters != null) {
                            for (int z = 0; z < sEscapeCharacters.Length; z++) {
                                sData = sData.Replace(sEscapeCharacters[z], sEscapeCharactersRepl[z]);
                            }
                        }


                        oWriter.Write(sFieldEncapsulation + sData + sFieldEncapsulation);

                    }

                    if (sColMapping[y] != "" && sColMapping.Length > y + 1)
                        oWriter.Write(sColSeparator);


                }

                oWriter.Write(sRowSeparator);
            }

            oWriter.Flush();

            if (!bSaveToDisk)
                sFileName = oWriter.ToString();


            oWriter.Close();
            return true;

        }


        /// <summary>
        /// Selects all the rows from a specified table
        /// </summary>
        /// <param name="sTable">The table name</param>
        /// <returns>The DataTable of Rows fromt he specified Table</returns>
        public DataTable Select_Table(string sTable) {
            return _Execute("SELECT * FROM [" + sTable + "]");
        }

        /// <summary>
        /// Creates a new DataTable from a specified file
        /// </summary>
        /// <returns>The DataTable from the specified file</returns>
        public DataTable Select_File() {
            string sCommand = @"SELECT * FROM [" + m_sFileName + "]";
            if (m_iConnectionType == connection_type.excel) sCommand = @"SELECT * FROM [" + m_sSheet + "]";

            return _OleExecute(sCommand);
        }

        public ArrayList OleGetTableNames() {
            ArrayList oList = new ArrayList();

            connect();
            DataTable schemaTable = m_oOleConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

            for (int x = 0; x < schemaTable.Rows.Count; x++) {
                oList.Add(schemaTable.Rows[x]["TABLE_NAME"].ToString());
            }

            disconnect();
            return oList;
        }

        /// <summary>
        /// Select the values of a Excel Sheet into a DataTable
        /// </summary>
        /// <returns>The DataTable of the excel Sheet</returns>
        public DataTable Select_Sheet() {

            if (m_sSheet == "") {

                ArrayList oTables = OleGetTableNames();

                for (int x = 0; x < oTables.Count; x++) {
                    if (oTables[x].ToString().IndexOf("Sheet1") > -1) {
                        m_sSheet = oTables[x].ToString();
                        break;
                    }
                }


                try {

                    DataTable oDT = _OleExecute("SELECT * FROM [" + m_sSheet + "]");
                    if (oDT.Rows.Count < 2 && oDT.Columns.Count < 2)
                        m_sSheet = "";
                    else
                        return oDT;

                } catch (Exception) { }

                if (m_sSheet == "" && oTables.Count > 0)
                    m_sSheet = oTables[0].ToString();

            }
            return _OleExecute("SELECT * FROM [" + m_sSheet + "]");
        }

        /// <summary>
        /// Create a new record from a specified table (and add it to the table's row collection)
        /// </summary>
        /// <param name="sTable">Table name</param>
        /// <returns>New DataTable</returns>

        public JDataTable new_Record(string sTable) {
            return new_Record(sTable, true);
        }

        /// <summary>
        /// Create a new record from a specified table
        /// </summary>
        /// <param name="sTable">Table name</param>
        /// <param name="bAddNew">Adds the new record to the empty datatable</param>
        /// <returns>New DataTable</returns>
        public JDataTable new_Record(string sTable, bool bAddNew) {
            JDataTable oDT = ado_helper.getTableDefintion(this, sTable).Clone() as JDataTable;
            DataRow oDataRow = oDT.NewRow();

            if (oDT.Columns.Contains("posted_date")) oDataRow["posted_date"] = DateTime.Now;
            if (oDT.Columns.Contains("deleted")) oDataRow["deleted"] = false;
            if (bAddNew) oDT.Rows.Add(oDataRow);
            return oDT;
        }

        public class query {
            public static string getQuery(connection_type oType, JDataTable oTable, DataRow oRow, System.Data.Common.DbParameterCollection oParams, string sLockHint) {
                return getQuery(oType, oTable, oRow, oParams, sLockHint, oRow.RowState);
            }
            public static string getQuery(connection_type oType, JDataTable oTable, DataRow oRow, System.Data.Common.DbParameterCollection oParams, string sLockHint, DataRowState oRowState) {
                int iPCounter = 1;
                string sParamPrefix = (oType == connection_type.other ? ":" : "@");
                string[] sColNameEnclosure = (oType == connection_type.other ? new string[] { "", "" } : new string[] { "[", "]" });
                string sCommand = (oRowState == DataRowState.Added ? "INSERT INTO " + oTable.TableName + (sLockHint == "" ? "" : " WITH (" + sLockHint + ")") + " (" : (oRowState == DataRowState.Deleted ? "DELETE FROM " + oTable.TableName + " WHERE " : "UPDATE " + oTable.TableName + (sLockHint == "" ? "" : " WITH (" + sLockHint + ")") + " SET "));
                if (oRowState == DataRowState.Added) {
                    string sValues = ") VALUES (";
                    for (int x = 0; x < oTable.Columns.Count; x++) {
                        if (oRow[x] != null && oRow[x] != DBNull.Value && !oTable.Columns[x].AutoIncrement && oTable.TableName == oTable.BaseTableName[oTable.Columns[x]]) {
                            sCommand += sColNameEnclosure[0] + oTable.getBaseColumnName(oTable.Columns[x]) + sColNameEnclosure[1] + ",";
                            sValues += sParamPrefix+"p" + iPCounter + ",";
                            oParams.Add(getSQLParameter(oType, sParamPrefix + "p" + iPCounter, oTable, oTable.Columns[x], oRow[x]));
                            iPCounter++;
                        }
                    }
                    sCommand = sCommand.Substring(0, sCommand.Length - 1) + sValues.Substring(0, sValues.Length - 1) + ")";
                } else {//Deleted or updated
                    if (oRowState == DataRowState.Modified) {
                        for (int x = 0; x < oTable.Columns.Count; x++) {
                            if (oTable.TableName == oTable.BaseTableName[oTable.Columns[x]] && oRow[x, DataRowVersion.Original] != null || oRow[x, DataRowVersion.Current] != null) {
                                if ((oRow[x, DataRowVersion.Original] == null || oRow[x, DataRowVersion.Current] == null) || !oRow[x, DataRowVersion.Original].Equals(oRow[x, DataRowVersion.Current])) {
                                    sCommand += sColNameEnclosure[0]+ oTable.getBaseColumnName(oTable.Columns[x]) + sColNameEnclosure[1]+"=" + sParamPrefix + "p" + iPCounter + ",";
                                    oParams.Add(getSQLParameter(oType, sParamPrefix + "p" + iPCounter, oTable, oTable.Columns[x], oRow[x]));
                                    iPCounter++;
                                }
                            }
                        }
                        if (iPCounter == 1) return "";
                        sCommand = sCommand.Substring(0, sCommand.Length - 1) + " WHERE ";
                    }

                    IEnumerator oCols = null;

                    if (oTable.JPrimaryKey.Count > 0)
                        oCols = oTable.JPrimaryKey.GetEnumerator();
                    else if (oTable.JIdentityColumn.Count > 0)
                        oCols = oTable.JIdentityColumn.GetEnumerator();
                    else
                        oCols = oTable.Columns.GetEnumerator();

                    while (oCols.MoveNext()) {                        
                        DataColumn oCol = oCols.Current as DataColumn;
                        if (oTable.TableName == oTable.BaseTableName[oCol]) {
                            if (oRow[oCol, DataRowVersion.Original] == null) {
                                sCommand += sColNameEnclosure[0] + oTable.getBaseColumnName(oCol) + sColNameEnclosure[1] + " IS NULL AND ";
                            } else {
                                if (oCol.DataType != typeof(System.Byte[]) && (oCol.DataType != typeof(System.String) || oCol.MaxLength < 8000)) {
                                    sCommand += sColNameEnclosure[0] + oTable.getBaseColumnName(oCol) + sColNameEnclosure[1] + "=" + sParamPrefix + "p" + iPCounter + " AND ";
                                    oParams.Add(getSQLParameter(oType, sParamPrefix + "p" + iPCounter, oTable, oCol, oRow[oCol, DataRowVersion.Original])); //(oRow.RowState == DataRowState.Deleted ? DataRowVersion.Original : DataRowVersion.Current)]));
                                    iPCounter++;
                                }
                            }
                        }
                    }
                    sCommand = sCommand.Substring(0, sCommand.Length - 5);
                }
                return sCommand;
            }

            public static DbParameter getSQLParameter(connection_type oType, string sName, JDataTable oTable, DataColumn oCol, object oData) {
                DbParameter oParam;
                if (oType == connection_type.other) oParam = new OleDbParameter(sName, oData);
                else oParam = new SqlParameter(sName, oData);
                if (oCol.DataType == typeof(System.String) && oType == connection_type.sql) {
                    (oParam as SqlParameter).SqlDbType = SqlDbType.VarChar;
                    //if (oData != null) oParam.Size = convert.cStr(oData);
                }
                //} else if (oCol.DataType == typeof(System.DateTime)) oParam.SqlDbType = SqlDbType.DateTime;
                //else if (oCol.DataType == typeof(System.Boolean)) oParam.SqlDbType = SqlDbType.Bit;
                //else if (oCol.DataType.Name.StartsWith("Int")) oParam.SqlDbType = SqlDbType.Int;
                //else if (oCol.DataType == typeof(System.Decimal)) oParam.SqlDbType = SqlDbType.Money;
                //else if (oCol.DataType == typeof(System.Byte)) oParam.SqlDbType = SqlDbType.TinyInt;
                //else if (oCol.DataType == typeof(System.Byte[])) oParam.SqlDbType = SqlDbType.Image;
                //else throw new Exception("Unhandled data type: " + oCol.DataType.FullName);
                return oParam;
            }
        }
        public static DataTable update(DataTable oTable) {
            return update(oTable, false);
        }
        public static DataRow[] update(DataRow[] oRowsToProcess) {
            return update(oRowsToProcess, false);            
        }
        public static DataRow[] update(DataRow[] oRowsToProcess, bool bDisableTriggerContext) {
            return update(oRowsToProcess, bDisableTriggerContext, "");
        }
        public static DataRow[] update(DataRow[] oRowsToProcess, bool bDisableTriggerContext, string sPreSQL) {
            return update(oRowsToProcess, bDisableTriggerContext, sPreSQL, "");            
        }

        public static DataRow[] update(DataRow[] oRowsToProcess, bool bDisableTriggerContext, string sPreSQL, string sLockHint) {
            if (oRowsToProcess==null || oRowsToProcess.Length == 0) return oRowsToProcess;
            (new ado_helper())._update(oRowsToProcess[0].Table, bDisableTriggerContext, sPreSQL, sLockHint, oRowsToProcess);
            return oRowsToProcess;
        }

        public static DataTable update(DataTable oTable, bool bDisableTriggerContext) {
            return update(oTable, bDisableTriggerContext, "");
        }
        public static DataTable update(DataTable oTable, bool bDisableTriggerContext, string sPreSQL) {
            return update(oTable as DataTable, bDisableTriggerContext, sPreSQL, "");
        }

        public static DataTable update(DataTable oDTTable, bool bDisableTriggerContext, string sPreSQL, string sLockHint) {
            return (new ado_helper())._update(oDTTable, bDisableTriggerContext, sPreSQL, sLockHint, null);
        }
        
        public DataTable _update(DataTable oDTTable, bool bDisableTriggerContext, string sPreSQL, string sLockHint, DataRow[] oRowsToProcess) {
            DbConnection connection=null;
            DbTransaction transaction = null;
            if (this.DbTransaction != null) {
                transaction = this.DbTransaction;
                connection = this.Conn;
            }
 
			//Variables for current Sql Values
			string currentSql = "";
			DbParameterCollection currentParameters = null;

            JDataTable oTable = oDTTable as JDataTable;
            if (oTable == null) throw new Exception("Can't execute update() when oDTTable is " + (oDTTable==null?"empty": "not a JDataTable"));
            if (oTable.DSN == "") throw new Exception("Can't execute update() when DSN is empty!");
            if (oTable.Columns.Count == 0) return oDTTable;
            connection_type oType = (oTable.DSN.Contains("OraOLEDB") ? connection_type.other : connection_type.sql);
            DbConnection oConn = connection;
            if (connection == null) {
                if (oType == connection_type.other) oConn = new OleDbConnection(oTable.DSN);
                else oConn = new SqlConnection(oTable.DSN);
                oConn.Open();
            }
            
            for (int iRetry = 0; iRetry < TransactionLockingRetry; iRetry++) {
                DbCommand oCommand = oConn.CreateCommand();
                if (transaction != null) oCommand.Transaction = transaction;
                else oCommand.Transaction = oConn.BeginTransaction();// (ado_helper.GlobalTransactionIsolationLevel == IsolationLevel.Unspecified ? oConn.BeginTransaction() : oConn.BeginTransaction(ado_helper.GlobalTransactionIsolationLevel));

				oCommand.CommandTimeout = 1800;
                try {
                    sPreSQL = getTransactionIsolationLevel(null, GlobalTransactionIsolationLevel) + sPreSQL;
                    if (sPreSQL != "" || bDisableTriggerContext) {
                        oCommand.CommandText = sPreSQL + ";" + (bDisableTriggerContext ? "SET Context_Info 0x55555;" : "");
                        oCommand.ExecuteNonQuery();
                    }
                    IEnumerator oEnumerator = (oRowsToProcess != null ? oRowsToProcess.GetEnumerator() : oTable.Rows.GetEnumerator());

                    while(oEnumerator.MoveNext())
					{
						currentSql = "";
						currentParameters = null;

                        DataRow oRow = oEnumerator.Current as DataRow;
                        if (oRow.RowState == DataRowState.Detached) throw new Exception("Attempting to update a row that is in a detached state.");
                        if (oRow.RowState == DataRowState.Modified || oRow.RowState == DataRowState.Deleted || oRow.RowState == DataRowState.Added) {
                            oCommand.Parameters.Clear();
                            string sQuery = query.getQuery(oType, oTable, oRow, oCommand.Parameters, sLockHint);
                            if (sQuery != "") {
                                if (sPreSQL != "") sQuery = sPreSQL + sQuery;
                                
								//Save Sql Values for error
								currentSql = sQuery;
								currentParameters = oCommand.Parameters;

								//Execute
								oCommand.CommandText = sQuery;

                                if (oRow.RowState == DataRowState.Added && oTable.JAutoNumberColumn.Count == 1 && (oConn as SqlConnection) != null) {
                                    oCommand.CommandText += ";SELECT SCOPE_IDENTITY()";
                                    object oAutoNumber = oCommand.ExecuteScalar();
                                    oRow[oTable.JAutoNumberColumn[0]] = oAutoNumber;
                                } else {
                                    int iAffected = oCommand.ExecuteNonQuery();
                                    if (iAffected != 1) throw new Exception("Update affected " + iAffected + " row(s). Expected it to affect 1 row.");
                                }
                            }
                        }                        
                    }
                    if (bDisableTriggerContext) {
                        oCommand.CommandText = "SET Context_Info 0;";
                        oCommand.ExecuteNonQuery();
                    }
                    if( transaction == null ) oCommand.Transaction.Commit();
                    oTable.AcceptChanges();
                    break;
                } catch (Exception e) {
                    if (oCommand != null && oCommand.Transaction != null && transaction==null) oCommand.Transaction.Rollback();
					if (TransactionLockingRetry > 1 && e.Message.Contains("was deadlocked on lock resources")) System.Threading.Thread.Sleep(TransactionLockingDelay);
					else
					{
						//Build Parameters Output
						string parameters = "";
						if (currentParameters != null)
						{
							foreach (DbParameter parameter in currentParameters)
							{
								parameters += String.Format("{0}='{1}',", parameter.ParameterName, parameter.Value);
							}
						}

						throw new Exception(String.Format("Update failed because '{0}'\n\nSql:{1}\n\nParameters:{2}", e.Message, currentSql, parameters), e);
					}
                }
            }
            if (connection == null) oConn.Close();
            return oTable;
        }

        public static string GetCreateTempTableSQL(string tableName, RowCollectionDataReaderBulk reader) {
            string sql = @"CREATE TABLE [" + tableName + @"] (";
            List<string> colDefs = new List<string>();
            for (int x = 0; x < reader.ColumnMapping.Count; x++) {
                var type = SqlHelper.DataMapping.GetSQLColumnType(reader.GetFieldType(x));
                colDefs.Add("[" + reader.GetName(x) + "] " + type + (type == "varchar" || type == "nvarchar" ? "(max)" : ""));
            }
            return sql + jlib.functions.extensions.Join(colDefs, ",") + ")";
        }
        public static string GetCreateTempTableSQL(string tableName, DataTable table) {
            string sql = @"CREATE TABLE [" + tableName + @"] (";
            List<string> colDefs = new List<string>();
            for (int x = 0; x < table.Columns.Count; x++) {
                var type = SqlHelper.DataMapping.GetSQLColumnType(table.Columns[x].DataType);
                colDefs.Add("[" + table.Columns[x].ColumnName + "] " + type + (type == "varchar" || type == "nvarchar" ? "(max)" : ""));
            }
            return sql + jlib.functions.extensions.Join(colDefs, ",") + ")";
        }
        public DataTable _DtsUpdate(DataTable dTable, bool bDisableTriggerContext, string sPreSQL, Action<object, SqlRowsCopiedEventArgs> fUpdateProgress, DataRowState statesToProcess) {
            
            JDataTable oTable = dTable as JDataTable;
            if (oTable == null) throw new Exception("Can't execute update() when oDTTable is " + (dTable == null ? "empty" : "not a JDataTable"));
            if (oTable.DSN == "") throw new Exception("Can't execute update() when DSN is empty!");
            if (oTable.Columns.Count == 0) return dTable;
            connection_type oType = (oTable.DSN.Contains("OraOLEDB") ? connection_type.other : connection_type.sql);
                        
            List<DataRowState> currentStates = new List<DataRowState>();
            List<DataRow> rowsToWrite = new List<DataRow>();
            foreach (DataRow row in oTable.Rows) {
                //if(record.RecordState!= DataRowAction.Nothing) numModifiedRecords++;
                if (statesToProcess.HasFlag(row.RowState)) {
                    rowsToWrite.Add(row);
                    currentStates.AddIfNotExist(row.RowState);
                }
            }
            if (rowsToWrite.Count == 0) return dTable;

            //Should we fall back to regular save? There is a cost involved with BulkInsert
            if (rowsToWrite.Count < 2) 
                return _update(dTable, bDisableTriggerContext, sPreSQL, "", rowsToWrite.ToArray());                        

            Dictionary<string, DataColumn> changedFields = new Dictionary<string, DataColumn>();
            List<string> pkfields = new List<string>();

            //Make sure we send the PKs -- these might not have changed and therefore need to be added            
            oTable.JPrimaryKey.ForEach(x => {
                changedFields.AddIfNotExist(x.ColumnName, x);
                pkfields.AddIfNotExist(x.ColumnName);
            });

            foreach (var record in rowsToWrite)
                if (record.RowState == DataRowState.Added || record.RowState == DataRowState.Modified) {
                    for (int x = 0; x < dTable.Columns.Count; x++) {
                        if ((record.RowState == DataRowState.Added && record[x] != System.DBNull.Value && record[x] != null) 
                            || (record.RowState == DataRowState.Modified && record[x, DataRowVersion.Original] != record[x, DataRowVersion.Current])) {

                            changedFields.AddIfNotExist(dTable.Columns[x].ColumnName, dTable.Columns[x]);
                            if (record.RowState == DataRowState.Modified && pkfields.Contains(dTable.Columns[x].ColumnName) && record[x, DataRowVersion.Original].Str() != record[x, DataRowVersion.Current].Str()) throw new Exception("Can't bulk process an update where the PK is changing.");
                        }
                    }
                }


            //////////////////


            SqlHelper SqlHelper=new db.SqlHelper(oTable.DSN);            
            if (this.DbTransaction != null) {
                SqlHelper.DbTransaction = this.DbTransaction;
                SqlHelper.DbConnection = this.Conn;
            }

            bool useLocalConnection = false;
            //Create Connection
            if (SqlHelper.DbConnection == null) {
                useLocalConnection = true;
                SqlHelper.Connect();
            }
            
            var dataReader = new RowCollectionDataReaderBulk(rowsToWrite);
            dataReader.SetColumnsToLoad(changedFields);

            string tempTableName = "#"+Guid.NewGuid().ToString().Replace('-', '_');
            
            //create temp table
            SqlHelper.Execute((bDisableTriggerContext ? "SET Context_Info 0x55555; " : "") + ado_helper.GetCreateTempTableSQL(tempTableName, dataReader));
            
            // new method: SQLBulkCopy:
            using (SqlBulkCopy s = new SqlBulkCopy(SqlHelper.DbConnection as SqlConnection, SqlBulkCopyOptions.Default, SqlHelper.DbTransaction as SqlTransaction)) {
                //Notification
                if (fUpdateProgress != null) {
                    s.SqlRowsCopied += new SqlRowsCopiedEventHandler(fUpdateProgress);
                    s.NotifyAfter = 10000;
                }

                //Destination
                s.DestinationTableName = "tempdb.." + tempTableName;

                //Timeout
                s.BulkCopyTimeout = 60 * 30; // 30 minutes

                //Batch Size
                s.BatchSize = 10000;

                //Column Mappings
                s.ColumnMappings.Clear();
                for (int i = 0; i < dataReader.FieldCount; i++)
                    s.ColumnMappings.Add(dataReader.GetName(i), dataReader.GetName(i));

                //Write Data
                s.WriteToServer(dataReader);
                s.Close();
            }

            //Don't use a single large merge, since it has some bugs -- http://www.mssqltips.com/sqlservertip/3074/use-caution-with-sql-servers-merge-statement/            
            string mergeSql = "";
            string pkWhere = "";
            pkfields.ForEach(pk => {
                pkWhere += (pkWhere == "" ? "" : " AND ") + "TARGET.[" + pk + "] = SOURCE.[" + pk + "]";
            });

            string insertSource = "", insertTarget = "", updateSet="";
            dataReader.ColumnMapping.ToList().ForEach(x => {
                if (x.Value > -1) {
                    //Exclude column from  insert if PK and AutoNumber
                    if (!pkfields.Contains(x.Key) || oTable.JAutoNumberColumn.Count == 0) {
                        insertSource += (insertSource == "" ? "" : ",") + "SOURCE.[" + x.Key + "]";
                        insertTarget += (insertTarget == "" ? "" : ",") + "[" + x.Key + "]";
                        updateSet += (updateSet == "" ? "" : ",") + "TARGET.[" + x.Key + "] = SOURCE.[" + x.Key + "]";
                    }
                }
            });

            if (currentStates.Contains(DataRowState.Added)) {
                //get output columns
                mergeSql += @"MERGE INTO " + dTable.TableName + @" as TARGET
                    USING " + tempTableName + @" as SOURCE ON
                        1=0 --Never match
                    WHEN NOT MATCHED AND SOURCE.__ChangeType=" + (int)DataRowState.Added + @" THEN
                        INSERT
                        (" + insertTarget + @")
                        VALUES
                        (" + insertSource + @")
                    OUTPUT
                        SOURCE.__Index,
                        INSERTED." + pkfields.Join(", INSERTED.") + ";";
            }

            if (currentStates.Contains( DataRowState.Modified)) {                
                mergeSql += "\nUPDATE TARGET SET " + updateSet + " FROM " + dTable.TableName + " TARGET JOIN " + tempTableName + " SOURCE ON __ChangeType=" + (int)DataRowState.Modified + " AND " + pkWhere + ";";
            }

            if (currentStates.Contains(DataRowState.Deleted)) {
                mergeSql += "\nDELETE TARGET FROM " + dTable.TableName + " TARGET INNER JOIN " + tempTableName + " SOURCE ON __ChangeType=" + (int)DataRowState.Deleted + " AND " + pkWhere + ";";
            }
            mergeSql += "\nDROP TABLE " + tempTableName + ";";
            if (bDisableTriggerContext) mergeSql += "SET Context_Info 0;";
                
            DataTable updatedRows = SqlHelper.Execute(mergeSql);

            //Close Connection			
            if (useLocalConnection) SqlHelper.Disconnect();

            if (currentStates.Contains(DataRowState.Added)) {
                foreach (DataRow row in updatedRows.Rows) {
                    foreach (string pk in pkfields)
                        rowsToWrite[row["__Index"].Int()][pk] = row[pk];
                }
            }
            //Set as Updated
            foreach (var record in rowsToWrite) 
                record.AcceptChanges();                
            
            return oTable;
        }
        public static bool updateLoggingChanges(DataRow[] oRowsToProcess, string sTableName, string sIDField, int iAuthorID) {
            if (oRowsToProcess == null || oRowsToProcess.Length == 0) return false;
            return _updateLoggingChanges(oRowsToProcess[0].Table, false, sTableName, sIDField, iAuthorID, oRowsToProcess);
        }
        public static bool updateLoggingChanges(DataTable oDT, string sTableName, string sIDField, int iAuthorID) {
            return updateLoggingChanges(oDT, false, sTableName, sIDField, iAuthorID);
        }
        public static bool updateLoggingChanges(DataTable oDT, bool bDisableTriggerContext, string sTableName, string sIDField, int iAuthorID) {
            return _updateLoggingChanges(oDT, bDisableTriggerContext, sTableName, sIDField, iAuthorID, null);
        }
        private static bool _updateLoggingChanges(DataTable oDT, bool bDisableTriggerContext, string sTableName, string sIDField, int iAuthorID, DataRow[] oRowsToProcess) {            
            Dictionary<DataRow, XmlDocument> oData = new Dictionary<DataRow, XmlDocument>();
            string sDSN = convert.cStr(ConfigurationManager.AppSettings["sql.dsn.versioning"], ConfigurationManager.AppSettings["sql.dsn.data"]);
            ado_helper oConnection=null;
            if (sDSN != "") {
                oConnection = new ado_helper(sDSN);
                IEnumerator oEnumerator = (oRowsToProcess != null ? oRowsToProcess.GetEnumerator() : oDT.Rows.GetEnumerator());

                while (oEnumerator.MoveNext()) {
                    DataRow oRow = oEnumerator.Current as DataRow;

                    if (oRow.RowState != DataRowState.Unchanged) {
                        XmlDocument xmlData = new XmlDocument();
                        xmlData.LoadXml("<r s=\"" + (int)oRow.RowState + "\" />");
                        bool bHasData = true;
                        if (oRow.RowState == DataRowState.Modified) {
                            bHasData = false;
                            for (int y = 0; y < oDT.Columns.Count; y++) {
                                if (convert.cStr(oRow[y, DataRowVersion.Original], oRow[y, DataRowVersion.Current]) != ""
                                    && (oRow[y, DataRowVersion.Original] == null || !oRow[y, DataRowVersion.Original].Equals(oRow[y, DataRowVersion.Current]))) {
                                    XmlNode xmlField = xml.setXmlAttributeValue(xml.addXmlElement(xmlData.DocumentElement, "f"), "n", oDT.Columns[y].ColumnName);
                                    xml.addXmlElement(xmlField, "o", oRow[y, DataRowVersion.Original]);
                                    xml.addXmlElement(xmlField, "c", oRow[y, DataRowVersion.Current]);
                                    bHasData = true;
                                }
                            }
                        }
                        if (bHasData) oData.Add(oRow, xmlData);
                    }
                }
            }        
            ado_helper.update(oDT, bDisableTriggerContext);

            foreach (DataRow oRow in oData.Keys) sqlbuilder.executeInsert(oConnection, "d_revisions", "user_id", iAuthorID, "table_name", sTableName, "pk_id", (oRow.RowState == DataRowState.Deleted ? oRow[sIDField, DataRowVersion.Original] : oRow[sIDField]), "data", oData[oRow].OuterXml);

            return true;
        }

        private void m_oOleDataAdapter_RowUpdating(object sender, OleDbRowUpdatingEventArgs e) {
            if (e.StatementType == System.Data.StatementType.Insert && m_sOleInsertCommand != "") {
                e.Command.CommandText = m_sOleInsertCommand;
            }

            if (e.StatementType == System.Data.StatementType.Update && m_sOleUpdateCommand != "") {
                e.Command.CommandText = m_sOleUpdateCommand;
            }
        }

        public static string PrepareDB(object oData) {
            return PrepareDB(oData, true);
        }
        public static string PrepareDB(object oData, bool bRemoveReservedWords) {
            if (bRemoveReservedWords)
                return parse.replaceAll(oData, false, "'", "''", "update ", "", "delete ", "", "alter ", "", "insert ", "", "create ", "", "[", "[[]");
            else
                return parse.replaceAll(oData, "'", "''", "[","[[]");
        }

        public static bool setIfChanged(DataRow oRow, string sFieldName, object oValue) {
            try {
                if (!oRow.Table.Columns.Contains(sFieldName)) return false;

                if (oRow.Table.Columns[sFieldName].DataType.FullName == "System.String") {
                    return ado_helper.setIfChanged(oRow, sFieldName, (oValue == null ? null : oValue.Str()));
                } else if (oRow.Table.Columns[sFieldName].DataType.FullName == "System.Boolean") {
                    bool? bValue = null;
                    if (oValue.Str() != "") bValue = oValue.Bln();
                    return ado_helper.setIfChanged(oRow, sFieldName, bValue);
                } else if (oRow.Table.Columns[sFieldName].DataType.Name == "DateTime" && jlib.functions.convert.cStr(oValue) == "") {
                    if (oRow[sFieldName] != null) {
                        oRow[sFieldName] = System.DBNull.Value;
                        return true;
                    }
                } else if (oRow.Table.Columns[sFieldName].DataType.Name == "DateTime") {
                    return ado_helper.setIfChanged(oRow, sFieldName, oValue.Dte());
                } else if (oRow.Table.Columns[sFieldName].DataType.FullName == "System.Byte" || oRow.Table.Columns[sFieldName].DataType.FullName.StartsWith("System.Int") || oRow.Table.Columns[sFieldName].DataType.FullName == "System.Decimal") {
                    double? dValue = null;
                    if (oValue.Str() != "") dValue = oValue.Dbl();
                    return ado_helper.setIfChanged(oRow, sFieldName, dValue);                
                } else {
                    oRow[sFieldName] = oValue;
                }

                return false;
            } catch (Exception) {
                return false;
            }
        }

        public static bool setIfChanged(DataRow oRow, string sField, double? dValue) {
            if (!dValue.HasValue && !oRow.Table.Columns[sField].AllowDBNull) dValue = 0;
            if ((!dValue.HasValue && oRow[sField]==System.DBNull.Value) || (oRow[sField].Str()!="" && convert.isNumeric(oRow[sField]) && oRow[sField].Dbl() == dValue)) return false;
            if (dValue.HasValue) oRow[sField] = dValue;
            else oRow[sField] = System.DBNull.Value;
            return true;
        }

        public static bool setIfChanged(DataRow oRow, string sField, int iValue) {
            return setIfChanged(oRow, sField, iValue.Dbl());
        }

        public static bool setIfChanged(DataRow oRow, string sField, string sValue) {
            if ((oRow[sField] as string)==sValue || (oRow[sField]!=DBNull.Value && oRow[sField].Str() == sValue)) return false;
            oRow[sField] = sValue;
            return true;
        }

        public static bool setIfChanged(DataRow oRow, string sField, bool? bValue) {
            if (!bValue.HasValue && !oRow.Table.Columns[sField].AllowDBNull) bValue = false;
            if ((!bValue.HasValue && oRow[sField] == System.DBNull.Value) || (oRow[sField].Str() != "" && oRow[sField].Bln() == bValue)) return false;
            if (bValue.HasValue) oRow[sField] = bValue;
            else oRow[sField] = System.DBNull.Value;
            return true;
        }
        public static bool setIfChanged(DataRow oRow, string sField, DateTime oValue) {
            if (oRow[sField].Str()!="" && oRow[sField].Dte() == oValue) return false;
            oRow[sField] = oValue;
            return true;
        }
        public static DataRow cloneDataRow(DataRow oSourceRow) {
            return cloneDataRow(oSourceRow, "");
        }
        public static DataRow cloneDataRow(DataRow oSourceRow, string sExcludedFields) {
            DataTable oDT=oSourceRow.Table.Clone();
            oDT.Rows.Add(oDT.NewRow());
            DataRow oDestRow = oDT.Rows[0];
            sExcludedFields = "," + sExcludedFields.Str().Trim() + ",";
            for (int x = 0; x < oSourceRow.Table.Columns.Count; x++) {
                if (sExcludedFields.IndexOf("," + oSourceRow.Table.Columns[x].ColumnName.ToLower() + ",", StringComparison.CurrentCultureIgnoreCase) == -1 && oDestRow.Table.Columns.Contains(oSourceRow.Table.Columns[x].ColumnName))
                    oDestRow[oSourceRow.Table.Columns[x].ColumnName] = oSourceRow[x];
            }
            return oDestRow;
        }
        public static DataRow cloneDataRow(DataRow oSourceRow, DataRow oDestRow, string sExcludedFields) {

            sExcludedFields = "," + sExcludedFields.Str().Trim() + ",";
            for (int x = 0; x < oSourceRow.Table.Columns.Count; x++) {
                if (sExcludedFields.IndexOf("," + oSourceRow.Table.Columns[x].ColumnName.ToLower() + ",", StringComparison.CurrentCultureIgnoreCase) == -1 && oDestRow.Table.Columns.Contains(oSourceRow.Table.Columns[x].ColumnName))
                    oDestRow[oSourceRow.Table.Columns[x].ColumnName] = oSourceRow[x];
            }
            return oDestRow;
        }

        public static bool setFieldValue(DataRow oRow, string sFieldName, object oValue) {
            try {
                if (!oRow.Table.Columns.Contains(sFieldName)) return false;

                ado_helper.setIfChanged(oRow, sFieldName, convert.cCastObjectToType(oValue,oRow.Table.Columns[sFieldName].DataType));                     
                
                return true;
            } catch (Exception) {
                return false;
            }
        }

        public static bool setFieldIfBlank(DataRow oRow, string sFieldName, object oValue) {
            try {
                if (!oRow.Table.Columns.Contains(sFieldName)) return false;
                if (oRow[sFieldName].Str() != "") return true;

                if (oRow.Table.Columns[sFieldName].DataType.FullName == "System.String") {
                    oRow[sFieldName] = convert.cStr(oValue);
                } else if (oRow.Table.Columns[sFieldName].DataType.FullName == "System.Boolean") {
                    oRow[sFieldName] = convert.cBool(oValue);
                } else if (oRow.Table.Columns[sFieldName].DataType.Name == "DateTime" && jlib.functions.convert.cStr(oValue) == "") {
                    oRow[sFieldName] = System.DBNull.Value;
                } else if (oRow.Table.Columns[sFieldName].DataType.Name == "DateTime") {
                    oRow[sFieldName] = jlib.functions.convert.cDate(oValue);
                } else if (oRow.Table.Columns[sFieldName].DataType.FullName.StartsWith("System.Int") || oRow.Table.Columns[sFieldName].DataType.FullName == "System.Decimal") {
                    oRow[sFieldName] = jlib.functions.convert.cDbl(oValue);
                } else {
                    oRow[sFieldName] = oValue;
                }

                return true;
            } catch (Exception) {
                return false;
            }
        }

        //#if NET_2_0        
        public static DataTable mergeTables(DataTableCollection oTables, string sPKField, string sSort) {
            for (int x = 1; x < oTables.Count; x++)
                oTables[0].Merge(oTables[x], false, MissingSchemaAction.Ignore);

            if (sPKField != "") {
                for (int x = 0; x < oTables[0].Rows.Count; x++) {
                    if (oTables[0].Select(sPKField + "='" + PrepareDB(oTables[0].Rows[x][sPKField]) + "'").Length > 1) {
                        oTables[0].Rows.Remove(oTables[0].Rows[x]);
                        x--;
                    }
                }
            }

            if (sSort != "") {
                DataView oView = new DataView(oTables[0]);
                oView.Sort = sSort;
                return oView.Table;
            }
            return oTables[0];
        }
        //#endif

        //public static string calculateChecksum(DataRow oRow, string sCols) {
        //    double dHash = 0;
        //    sCols = convert.cStr(sCols).Trim().ToLower();
        //    if (sCols != "") sCols = "," + sCols + ",";
        //    for (int x = 0; x < oRow.Table.Columns.Count; x++)
        //        if (sCols == "" || sCols.IndexOf("," + oRow.Table.Columns[x].Caption.ToLower() + ",") > -1)
        //            dHash += convert.cStr(oRow[x]).GetHashCode();

        //    return convert.cStr(dHash);
        //}
        public static string calculateChecksum(DataRow oRow, string sCols) {
            System.Text.StringBuilder oSB = new System.Text.StringBuilder();
            sCols = convert.cStr(sCols).Trim().ToLower();
            if (sCols != "") sCols = "," + sCols + ",";
            for (int x = 0; x < oRow.Table.Columns.Count; x++)
                if (sCols == "" || sCols.IndexOf("," + oRow.Table.Columns[x].Caption.ToLower() + ",") > -1)
                    oSB.Append(convert.cStr(oRow[x]));

            return convert.cStr(oSB.ToString().GetHashCode());
        }
        public static string calculateChecksum(DataRow row, List<string> cols) {
            System.Text.StringBuilder oSB = new System.Text.StringBuilder();
            for (int x = 0; x < row.Table.Columns.Count; x++)
                if (cols == null || cols.Contains(row.Table.Columns[x].Caption, StringComparison.CurrentCultureIgnoreCase))
                    oSB.Append(convert.cStr(row[x]));

            return convert.cStr(oSB.ToString().GetHashCode());
        }
        public static string calculateChecksum(jlib.db.IDataRow row, List<string> cols) {            
            System.Text.StringBuilder oSB = new System.Text.StringBuilder();            
            for (int x = 0; x < row.GetColumnCount(); x++)
                if(cols == null || cols.Contains(row.GetFieldNameByIndex(x), StringComparison.CurrentCultureIgnoreCase))                
                    oSB.Append(convert.cStr(row[x]));

            return convert.cStr(oSB.ToString().GetHashCode());
        }

        #endregion METHODS

		#region ***** STATIC METHODS *****
		public static void TrimTrailingSpaces(DataRow row)
		{
			DataColumnCollection columns = row.Table.Columns;
			foreach (DataColumn column in columns)
			{
				if ((row[column] != null) && (row[column].GetType() == typeof(string)))
				{
					row[column] = row[column].Str().TrimEnd(new char[] { ' ' });
				}
			}
		}
		public static void TrimTrailingSpaces(DataTable table)
		{
			for (int i = 0; i < table.Rows.Count; i++)
			{
				TrimTrailingSpaces(table.Rows[i]);
			}
		}


 

		#endregion
	}
}
