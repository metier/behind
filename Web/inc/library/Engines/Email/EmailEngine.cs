﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using jlib.functions;

namespace jlib.Engines.Email
{
    public class EmailEngine
    {
        public static class AppSettings
        {
            public const string SMTP_DISABLE = "smtp.disable";
            public const string SMTP_OVERRIDE_TO = "smtp.override.to";
            public const string SMTP_OVERRIDE_FROM = "smtp.override.from";
            public const string SMTP_ADD_CC = "smtp.add.cc";
            public const string SMTP_ADD_BCC = "smtp.add.bcc";
            public const string SMTP_ADD_SUBJECT = "smtp.add.subject";
            public const string SMTP_SERVER = "smtp.server";
            public const string SMTP_PORT = "smtp.port";
            public const string SMTP_USERNAME = "smtp.username";
            public const string SMTP_PASSWORD = "smtp.password";
         
        }
        #region ***** CONSTRUCTORS *****
        public EmailEngine()
        {
            this.To = new List<System.Net.Mail.MailAddress>();
            this.Cc = new List<System.Net.Mail.MailAddress>();
            this.Bcc = new List<System.Net.Mail.MailAddress>();
            this.Attachments = new List<System.Net.Mail.Attachment>();            
            this.FromIsOverriden = false;
            this.ToIsOverriden = false;
            this.IsBodyHtml = true;

            //Load Configuration Details
            this.SmtpHost = ConfigurationManager.AppSettings[AppSettings.SMTP_SERVER];
            this.SmtpPort = ConfigurationManager.AppSettings[AppSettings.SMTP_PORT].Int();
            this.SmtpUsername = ConfigurationManager.AppSettings[AppSettings.SMTP_USERNAME];
            this.SmtpPassword = ConfigurationManager.AppSettings[AppSettings.SMTP_PASSWORD];
            this.SendEmailEnabled = !ConfigurationManager.AppSettings[AppSettings.SMTP_DISABLE].Bln();

        }
        #endregion



        #region ***** PROPERTIES *****
        public System.Net.Mail.MailAddress From { get; set; }
        public List<System.Net.Mail.MailAddress> To { get; set; }
        public List<System.Net.Mail.MailAddress> Cc { get; set; }
        public List<System.Net.Mail.MailAddress> Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int ToUserId { get; set; }
        public bool IsBodyHtml { get; set; }
        public List<System.Net.Mail.Attachment> Attachments { get; set; }
        public bool FromIsOverriden { get; set; }
        public bool ToIsOverriden { get; set; }
        public bool SendEmailEnabled { get; set; }
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
        #endregion



        #region ***** METHODS *****
        public EmailLogData Send()
        {
            //Create Client
            var smtp = new System.Net.Mail.SmtpClient(this.SmtpHost, this.SmtpPort);
            if (this.SmtpUsername.IsNotNullOrEmpty() && this.SmtpPassword.IsNotNullOrEmpty()) smtp.Credentials = new System.Net.NetworkCredential(this.SmtpUsername, this.SmtpPassword);

            //Create Message
            var message = new System.Net.Mail.MailMessage();

            //From
            message.From = this.GetFrom();

            //To
            this.ImportMailAddressList(message.To, this.GetTo());

            //CC, BCC
            this.ImportMailAddressList(message.CC, this.GetCc());
            this.ImportMailAddressList(message.Bcc, this.GetBcc());

            //Subject
            message.Subject = this.GetSubject();

            //Attachments
            if (this.Attachments != null)
                this.Attachments.ForEach(attachment => message.Attachments.Add(attachment));

            //Body
            message.IsBodyHtml = this.IsBodyHtml;
            message.Body = this.GetBody();

            //Send
            bool sendSuccessful = false;
            Exception sendException = null;
            if (this.SendEmailEnabled)
            {
                try
                {
                    smtp.Send(message);
                    sendSuccessful = true;
                }
                catch (Exception e)
                {
                    sendException = e;
                    sendSuccessful = false;
                }

            }

            var log = GetLogData(message, sendSuccessful, sendException);

            //Dispose of Message
            this.DisploseOfMessage(message);

            return log;
        }
        public class EmailLogData
        {
            public string Subject { get; set; }
            public string Body { get; set; }
            public string EmailFrom { get; set; }
            public string EmailTo { get; set; }
            public string EmailCc { get; set; }
            public string EmailBcc { get; set; }
            public bool IsHtml { get; set; }
            public bool Sent { get; set; }
            public string ErrorMessage { get; set; }
            public int UserID { get; set; }
        }

        private EmailLogData GetLogData(System.Net.Mail.MailMessage Message, bool SendSuccessful, Exception SendException)
        {
            var log = new EmailLogData();
            log.Subject = convert.cEllipsis(Message.Subject, 255, true);
            log.Body = Message.Body;
            log.EmailFrom = this.From.Address;
            log.EmailTo = String.Join(",", this.To.Select(x => x.Address));
            if (this.Cc != null) log.EmailCc = String.Join(",", this.Cc.Select(x => x.Address));
            if (this.Bcc != null) log.EmailBcc = String.Join(",", this.Bcc.Select(x => x.Address));
            log.IsHtml = Message.IsBodyHtml;
            log.Sent = SendSuccessful;
            if (SendException != null) log.ErrorMessage = SendException.ToString();
            log.UserID = this.ToUserId;
            return log;


            ///TODO: Attachments
            //if (oFiles != null && oFiles.Count > 0 && convert.cStr(ConfigurationManager.AppSettings["email.attachment.path"]) != "") {
            //	string sAttachments = "";
            //	for (int x = 0; x < oFiles.Count; x++) {
            //		string sFileName = io.getUniqueFileName(ConfigurationManager.AppSettings["email.attachment.path"] + "\\" + oDT.Rows[0]["id"] + "-" + oFiles[x].Key);
            //		if (io.write_file(sFileName, oFiles[x].Value)) sAttachments += (sAttachments == "" ? "" : "\n") + sFileName;
            //	}
            //	if (sAttachments != "") {
            //		oDT.Rows[0]["attachments"] = sAttachments;
            //		ado_helper.update(oDT);
            //	}
            //}

        }

        public void DisploseOfMessage(System.Net.Mail.MailMessage Message)
        {
            //Attachments
            if (Message.Attachments == null || Message.Attachments.Count() == 0) return;
            for (int x = 0; x < Message.Attachments.Count; x++)
            {
                try { Message.Attachments[x].Dispose(); } catch (Exception) { }
            }

        }


        public System.Net.Mail.MailAddress GetFrom()
        {
            //check to see if override
            var overrideFrom = ConfigurationManager.AppSettings[AppSettings.SMTP_OVERRIDE_FROM];
            if (overrideFrom.IsNotNullOrEmpty())
            {
                this.FromIsOverriden = true;
                return new System.Net.Mail.MailAddress(overrideFrom);
            }

            //otherwise, return passed in value
            return this.From;
        }

        public List<System.Net.Mail.MailAddress> GetTo()
        {
            //check to see if override
            var overrideTo = ConfigurationManager.AppSettings[AppSettings.SMTP_OVERRIDE_TO];
            if (overrideTo.IsNotNullOrEmpty())
            {
                this.ToIsOverriden = true;
                return EmailEngine.ParseIntoMailAddresses(overrideTo);
            }

            //otherwise, return passed in value
            return this.To;
        }

        public List<System.Net.Mail.MailAddress> GetCc()
        {
            //if overridden, return empty
            if (this.ToIsOverriden) return new List<System.Net.Mail.MailAddress>();

            //check to see if override
            var addCc = ConfigurationManager.AppSettings[AppSettings.SMTP_ADD_CC];
            if (addCc.IsNotNullOrEmpty()) this.Cc.AddRange(EmailEngine.ParseIntoMailAddresses(addCc));

            //otherwise, return passed in value
            return this.Cc;
        }

        public List<System.Net.Mail.MailAddress> GetBcc()
        {
            //if overridden, return empty
            if (this.ToIsOverriden) return new List<System.Net.Mail.MailAddress>();

            //check to see if override
            var addBcc = ConfigurationManager.AppSettings[AppSettings.SMTP_ADD_BCC];
            if (addBcc.IsNotNullOrEmpty()) this.Bcc.AddRange(EmailEngine.ParseIntoMailAddresses(addBcc));

            //otherwise, return passed in value
            return this.Bcc;
        }

        public string GetSubject()
        {
            var subject = this.Subject;

            //check to see if override
            var addSubject = ConfigurationManager.AppSettings[AppSettings.SMTP_ADD_SUBJECT];
            if (addSubject.IsNotNullOrEmpty()) subject += addSubject;

            //return
            return subject;
        }
        public string GetBody()
        {
            var body = this.Body;

            //if override, prefix body
            if (this.ToIsOverriden || this.FromIsOverriden)
            {
                //Create Prefix
                var prefix = new List<string>();
                prefix.Add(String.Format("FROM: {0}", this.From.Address));
                prefix.Add(String.Format("TO: {0}", String.Join(",", this.To.Select(x => x.Address))));
                if (this.Cc != null && this.Cc.Count() > 0) prefix.Add(String.Format("CC: {0}", String.Join(",", this.Cc.Select(x => x.Address))));
                if (this.Bcc != null && this.Bcc.Count() > 0) prefix.Add(String.Format("BCC: {0}", String.Join(",", this.Bcc.Select(x => x.Address))));

                //combine
                body = String.Join(this.IsBodyHtml ? "<br>" : "\n", prefix)
                        + (this.IsBodyHtml ? "<hr><br>" : "-----------\n")
                        + body;

            }

            //return
            return body;
        }
        public void ImportMailAddressList(System.Net.Mail.MailAddressCollection Collection, List<System.Net.Mail.MailAddress> Addresses)
        {
            if (Collection == null || Addresses == null) return;

            //Add
            Addresses.ForEach(x => Collection.Add(x));
        }
        #endregion

        #region ***** STATIC METHODS *****
        public static List<System.Net.Mail.MailAddress> ParseIntoMailAddresses(string Addresses)
        {
            //Declare output
            var output = new List<System.Net.Mail.MailAddress>();

            //if no addresses, return empty
            if (Addresses.IsNullOrEmpty()) return output;

            //Replace all separators with ,
            var addressList = Addresses.Split(new char[] { ' ', ';', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            addressList.ForEach(x =>
            {
                var address = x;

                //if in format of "name <email@domain.com>"
                address = address.Str().Replace("&lt;", "<").Replace("&gt;", ">");
                if (address.IndexOf('<') != -1) address = jlib.functions.parse.inner_substring(address, "<", null, ">", null);

                //remove invalid characters
                address = parse.replaceAll(address, " ", "", " ", "", ";", ",", ",,", ",", ",,", ",");

                //trim ending characters
                address = address.TrimEnd(new char[] { ',', ' ' });

                //if not a valid email, dont add to list
                if (!jlib.functions.convert.isEmail(address)) return;

                //add address
                output.Add(new System.Net.Mail.MailAddress(address));
            });


            //Return
            return output;
        }
        #endregion

    }
}
