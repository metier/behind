using System;
using System.Web.UI;
using jlib.functions;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Text;
using System.Web.UI.WebControls;

namespace jlib.components
{
	/// <summary>
	/// Summary description for textbox.
	/// </summary>
    public class user_control : System.Web.UI.UserControl, iControl
	{

		private string m_sDataMember	= "", m_sFieldDesc		= "", m_sDefaultValue = "";
        private bool m_bRequired = false, m_bDisableSave = false, m_bRendered = false;
		private int m_iMinLength = -1, m_iMaxLength = -1;
		public bool ReadOnly { get; set; }
        public bool Rendered {
            get {
                return m_bRendered;
            }
        }

		public bool Enabled {
			get {
				return true;
			}
			set { }
		}

		public string DataMember{
			get{
				return m_sDataMember;
			}
			set{
				m_sDataMember	= value;
			}
		}

		public string FieldDesc{
			get{

				if ( m_sFieldDesc != "" )
					return m_sFieldDesc;

				if ( m_sDataMember != "" ){
					if ( m_sDataMember.IndexOf( "." ) != -1 )
						return parse.split( m_sDataMember, "." )[ 1 ];
					else
						return m_sDataMember;
				}
				return this.ID;			
				
			}

			set{
				m_sFieldDesc	= value;
			}
		}

        public string DefaultValue {
			get{
                return m_sDefaultValue;
			}
			set{
                m_sDefaultValue = value;
			}
		}

		public bool Required{
			get{
				return m_bRequired;
			}
			set{
				m_bRequired	= value;
			}
		}

		public bool DisableSave {
			get {
				return m_bDisableSave;
			}
			set {
				m_bDisableSave = value;
			}
		}

		public int MinLength {
			get {
				return m_iMinLength;
			}
			set {
				m_iMinLength = value;
			}
		}

		public int MaxLength {
			get {
				return m_iMaxLength;
			}
			set {
				m_iMaxLength = value;				
			}
		}

		public virtual string validate() {			
			return "";
		}

		public void setVaildateFailure( bool bFailure ){
			
		}

        protected override void Render(HtmlTextWriter writer) {
            m_bRendered = true;
            base.Render(writer);
        }

        public virtual object getValue() {            
            return null;
        }

        public virtual iControl setValue(object oValue) {
            return this;
        }
        
        public int populateValues( string sTableName, DataRow oRow ) {
            return helpers.control.populateValues( this, sTableName, oRow as jlib.db.IDataRow );
        }

        public int populateValues(string sTableName, jlib.db.IDataRow oRow)
        {
            return helpers.control.populateValues(Page, sTableName, oRow);
        }

        public int populateValues(string sPrefix, helpers.structures.collection oData)
        {
            return helpers.control.populateValues(Page, sPrefix, oData);
        }

        public DataRow collectValues( string sTableName, DataRow oRow ) {
            return helpers.control.collectValues(this, sTableName, oRow as jlib.db.IDataRow) as DataRow;
        }

        public string validateForm( Control oParentControl ) {
            return helpers.control.validateForm( oParentControl );            
        }
	}
}
