using System;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Data;
using jlib.db;
using jlib.functions;


namespace jlib.components {
	/// <summary>
	/// Summary description for webpage.
	/// </summary>
	public class masterpage : System.Web.UI.MasterPage {
		


        protected jlib.controls.cooltabs.container ctrPriNav;
        protected jlib.controls.cooltabs.container ctrSubNav;

		/* Properties */

        public jlib.controls.cooltabs.container PriNav {
            get {
                return ctrPriNav;
            }
            set {
                ctrPriNav = value;
            }
        }

        public jlib.controls.cooltabs.container SubNav {
            get {
                return ctrSubNav;
            }
            set {
                ctrSubNav = value;
            }
        }

        

        #region methods
        public virtual void Page_PreInit(object sender, EventArgs e) {

        }
        #endregion


    }
}
