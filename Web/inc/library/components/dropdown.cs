using System;
using System.Web.UI;
using jlib.functions;
using System.Web.UI.WebControls;
using System.Collections.Generic;

namespace jlib.components
{
	/// <summary>
	/// Summary description for textbox.
	/// </summary>
    public class dropdown : System.Web.UI.WebControls.DropDownList, iControl//, IPostBackEventHandler
	{

		private string m_sDataMember	= "";
		private bool m_bRequired		= false;
		private bool m_bDisableSave = false;
        private bool m_bDirty           = false;
        
		private string m_sFieldDesc		= "";
        private bool m_bAjaxValidate = false, m_bAjaxValidateText = true, m_bDisplayValidateStatus = false, m_bRendered = false;

        private bool m_bPaging      = false;
        private int m_iPageSize     = 10;
		public bool ReadOnly { get; set; }
        //private BoundModeEnum BoundMode = BoundModeEnum.Normal;

        //public enum BoundModeEnum {
        //    Normal,
        //    Loose,
        //    Fixed
        //}
        public bool Rendered {
            get {
                return m_bRendered;
            }
        }
        public bool Paging {
            get {
                return m_bPaging;
            }
            set {
                m_bPaging = value;
            }
        }

        public int PageSize {
            get {
                return m_iPageSize;
            }
            set {
                m_iPageSize = value;
            }
        }


        public bool DisplayValidateStatus {
            get {
                return m_bDisplayValidateStatus;
            }
            set {
                m_bDisplayValidateStatus = value;
            }
        }

        public bool AjaxValidate {
            get {
                return m_bAjaxValidate;
            }
            set {
                m_bAjaxValidate = value;
            }
        }

        // Should we display Text as a part of the Ajax Validate
        public bool AjaxValidateText {
            get {
                return m_bAjaxValidateText;
            }
            set {
                m_bAjaxValidateText = value;
            }
        }

		public override string DataMember{
			get{
				return m_sDataMember;
			}
			set{
				m_sDataMember	= value;
			}
		}


		public string FieldDesc{
			get{

				if ( m_sFieldDesc != "" )
					return m_sFieldDesc;

				if ( m_sDataMember != "" ){
					if ( m_sDataMember.IndexOf( "." ) != -1 )
						return parse.split( m_sDataMember, "." )[ 1 ];
					else
						return m_sDataMember;
				}
				return this.ID;			
				
			}

			set{
				m_sFieldDesc	= value;
			}
		}

		public bool Required{
			get{
				return m_bRequired;
			}
			set{
				m_bRequired	= value;
			}
		}

		public bool DisableSave {
			get {
				return m_bDisableSave;
			}
			set {
				m_bDisableSave = value;
			}
		}

        public override string SelectedValue {
            get {
                if (!m_bDirty && Page.IsPostBack) {
                    jlib.components.form oForm = Page.Form as jlib.components.form;
                    if (oForm != null && oForm.ControlState[this.UniqueID] != null) return convert.cStr(oForm.ControlState[this.UniqueID]);
                    return convert.cStr(Page.Request.Form[this.UniqueID]);
                }

                return base.SelectedValue;
            }
            set {
                m_bDirty = true;
                base.SelectedValue = value;
            }
        }

		public string SelectedText {
			get {
				return (this.SelectedItem == null ? "" : this.SelectedItem.Text);
			}
			set {
				try {
					this.SelectedIndex = this.Items.IndexOf(this.Items.FindByText(value));
				} catch (Exception) { }
			}
		}

        public override int SelectedIndex {
            get {
                if (!m_bDirty && Page.IsPostBack && this.Items.Count > 0) {
                    try {
                        this.SelectedIndex = this.Items.IndexOf(this.Items.FindByValue(Page.Request.Form[this.UniqueID]));
                    } catch (Exception) { }
                }
                return base.SelectedIndex;
            }
            set {
                m_bDirty = true;
                base.SelectedIndex = value;
                dropdown_SelectedIndexChanged( null, null );
            }
        }

        public string OnClientChange {
            get {
                return this.Attributes["onchange"];
            }
            set {
                this.Attributes["onchange"] = value;
            }
        }

        public override void DataBind() {
            if (this.DataSource as System.Xml.XmlNodeList != null) {
                System.Xml.XmlNodeList oList = this.DataSource as System.Xml.XmlNodeList;
                for (int x = 0; x < oList.Count; x++) this.Items.Add(new ListItem((this.DataTextField.StartsWith("@")? xml.getXmlAttributeValue(oList[x],DataTextField.Substring(1)) : xml.getXmlNodeTextValue(oList[x], DataTextField)), (this.DataValueField.StartsWith("@")? xml.getXmlAttributeValue(oList[x],DataValueField.Substring(1)) : xml.getXmlNodeTextValue(oList[x], DataValueField))));
            } else {
                try {
                    base.DataBind();
                } catch (Exception) {
                    System.Data.DataTable o = (this.DataSource as System.Data.DataTable);
                    if (o.Rows.Count > 0) {
                        this.SelectedValue = convert.cStr(o.Rows[0][this.DataValueField]);
                        base.DataBind();
                    }
                }
            }
            if (!m_bDirty && Page.IsPostBack && convert.cStr(Page.Request.Form[this.UniqueID]) != ""){
                try {
                    this.SelectedIndex = this.Items.IndexOf(this.Items.FindByValue(Page.Request.Form[this.UniqueID]));
                } catch (Exception) { }
            }
        }

		public string validate() {

			if ( Required && this.SelectedValue == "" )
				return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} must be selected." : "{0} m� velges."), this.FieldDesc);                    			

			return "";
		}

		public void setVaildateFailure( bool bFailure ){
            if ( bFailure ) {
                this.CssClass = parse.replaceAll( this.CssClass, " validate_failure", "" ) + " validate_failure";
            } else {
                this.CssClass = parse.replaceAll( this.CssClass, " validate_failure", "" );
            }
		}


        public object getValue() {
            return this.SelectedValue;
        }

        public iControl setValue(object oValue) {
            return setValue(oValue, false);
        }
        public iControl setValue(object oValue, bool bIgnoreError) {
            if (oValue == null) this.SelectedValue = null;
            else this.SelectedValue = convert.cStr(oValue);
            if (this.SelectedValue != convert.cStr(oValue) && !bIgnoreError) throw new Exception("Trying to set dropdown to a value that is not in the list! Invalid value was: " + oValue);
            return this;
        }

        public void TranslateItems(string language, string SiteId, string CompanyId, string url) {            
            foreach (ListItem item in this.Items) {
                if (!item.Attributes["TranslationKey"].IsNullOrEmpty()) item.Text = helpers.translation.translate(language , this.Text, url, Page, item.Attributes["TranslationKey"], SiteId, CompanyId);
            }            
        }
        protected override void OnInit(EventArgs e) {
            this.Page.PreRender += new EventHandler(Page_PreRender);
            this.Load += new EventHandler(dropdown_Load);
            this.SelectedIndexChanged += new EventHandler(dropdown_SelectedIndexChanged);
            base.OnInit(e);
            webpage oPage = (Page as webpage);
            if (oPage != null) {
                TranslateItems(oPage.Language, oPage.TranslationSiteId, oPage.TranslationCompanyId, (System.Web.HttpContext.Current.Request.ApplicationPath.Length < 2 ? oPage.Request.Url.AbsolutePath : parse.stripLeadingCharacter(oPage.Request.Url.AbsolutePath, System.Web.HttpContext.Current.Request.ApplicationPath)));
            }
        }

        void Page_PreRender( object sender, EventArgs e ) {
            ViewState[this.ClientID + "_Required"] = this.Required;
            //Page.ClientScript.RegisterStartupScript( this.GetType(), "combo.css", "<link href=\"/inc/library/components/media/combo.css\" rel=\"stylesheet\" type=\"text/css\"></link>", false );
        }

        void dropdown_Load( object sender, EventArgs e ) {
            if ( ViewState[this.ClientID + "_Required"] != null )
                this.Required = convert.cBool( ViewState[this.ClientID + "_Required"] );
        }

        public void dropdown_SelectedIndexChanged( object sender, EventArgs e ) {
            ViewState[this.ClientID + "_SelectedIndexChanged"] = "true";
            if ( sender != null ) Page.ClientScript.RegisterStartupScript( this.GetType(), this.ClientID + "_focus", String.Format( "try{{document.getElementById( '{0}' ).focus();}}catch(e){{}}", this.ClientID ), true );
            //Page.Form.DefaultFocus = this.ClientID;            
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer){

			//Output as a Label
			if (this.ReadOnly)
			{
				Literal literal = new Literal();
				literal.Text = this.SelectedText;
				literal.ID = this.ID + "_readonly";
				literal.RenderControl(writer);
				return;
			}

            m_bRendered = true;
            if ( this.AjaxValidate ) {
                //this.Attributes["onblur"] = Page.ClientScript.GetPostBackClientHyperlink( this, "validate" );
                this.AutoPostBack = true;
            }

            //writer.Write("<span class=\"control_container\">");
            
            //if( this.Paging ){
            //    writer.Write( "<input type=\"text\" id=\"" + this.ClientID + "\" name=\"" + this.UniqueID + "\" class=\"ComboBoxText\" />" );
            //    writer.Write( "<br /><div style='z-index:5000;position:absolute;width:" + this.Width + ";height:200px;background-color:white;border:1px solid black'>askksakssksak</div>" );

            
            //}else{
                base.Render(writer);
            //}
            if ( Page.IsPostBack && DisplayValidateStatus && ( convert.cStr( ViewState[this.ClientID + "_SelectedIndexChanged"] ) == "true" )){// || !MagicAjax.MagicAjaxContext.Current.IsAjaxCall ) ) {
                writer.Write( "&nbsp;" );
                if ( this.validate() == "" ) {
                    writer.Write( "<img src=\"/inc/m/accept.gif\" alt=\"Verdi godtatt\">" );
                } else {
                    writer.Write( "<img src=\"/inc/m/exclamation.gif\" alt=\"Verdi ikke godtatt\">" );
                    if ( AjaxValidateText )
                        writer.Write( "<br /><span class=\"validation_error\">" + validate() + "</span>" );
                }
            }            
            //writer.Write("</span>");
            
        }

		protected override void RenderContents(HtmlTextWriter writer) {
						
			string currentOptionGroup;
			List<string> renderedOptionGroups = new List<string>();
			foreach (ListItem item in this.Items) {
				if (item.Attributes["OptionGroup"] == null) {
					RenderListItem(item, writer);
				} else {
					currentOptionGroup = item.Attributes["OptionGroup"];
					if (renderedOptionGroups.Contains(currentOptionGroup)) {
						RenderListItem(item, writer);
					} else {
						if (renderedOptionGroups.Count > 0) {
							RenderOptionGroupEndTag(writer);
						}
						RenderOptionGroupBeginTag(currentOptionGroup,
												  writer);
						renderedOptionGroups.Add(currentOptionGroup);
						RenderListItem(item, writer);
					}
				}
			}
			if (renderedOptionGroups.Count > 0) {
				RenderOptionGroupEndTag(writer);
			}
		}


		private void RenderOptionGroupBeginTag(string name, HtmlTextWriter writer) {
			writer.WriteBeginTag("optgroup");
			writer.WriteAttribute("label", name);
			writer.Write(HtmlTextWriter.TagRightChar);
			writer.WriteLine();
		}
		private void RenderOptionGroupEndTag(HtmlTextWriter writer) {
			writer.WriteEndTag("optgroup");
			writer.WriteLine();
		}
		private void RenderListItem(ListItem item, HtmlTextWriter writer) {
			writer.WriteBeginTag("option");
			writer.WriteAttribute("value", item.Value, true);
			if (item.Selected) {
				writer.WriteAttribute("selected", "selected", false);
			}
			foreach (string key in item.Attributes.Keys) {
				writer.WriteAttribute(key, item.Attributes[key]);
			}
			writer.Write(HtmlTextWriter.TagRightChar);
			System.Web.HttpUtility.HtmlEncode(item.Text, writer);
			writer.WriteEndTag("option");
			writer.WriteLine();
		}

	}
}
