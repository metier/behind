<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="combo.ascx.cs" Inherits="jlib.components.webcombo.combo" %>
<common:hidden id="ctrValue" runat="server" /><common:hidden id="ctrMultiValue" runat="server" /><common:hidden id="ctrData" runat="server" />
<div class="cCombo" runat="server" id="ctrCombo" >
<table>
<tr>
<td><div runat="server" id="ctrDataContainer" class="data_container">
<common:textbox AUTOCOMPLETE="off" onkeydown="if( event.keyCode == 13 ){ event.cancelBubble = true; return false;}" onkeyup="if( event.keyCode == 13 ){ event.cancelBubble = true; return false;}" AjaxCall="async" id="ctrText" runat="server" /></div></td>
<td runat="server" id="ctrButtonContainer" ><img class="button" id="ctrButton" runat="server" src="/inc/library/components/webcombo/media/button.gif" normal="/inc/library/components/webcombo/media/button.gif" hover="/inc/library/components/webcombo/media/button_hl.gif" pressed="/inc/library/components/webcombo/media/button_pressed.gif" onmouseover="this.src=this.getAttribute('hover');" onmouseout="this.src=this.getAttribute('normal');" onmousedown="this.src=this.getAttribute('pressed');" onmouseup="this.src=this.getAttribute('hover');" /></td>
<td visible="false" runat="server" id="ctrClearButtonContainer" class="delete"><div onmouseover="this.className = 'hover';" onmouseout="this.className = '';" ><img runat="server" id="ctrClearButton" src="/inc/library/components/webcombo/media/delete.gif" style="margin:2px" /></div></td>
</tr>
</table>
<div id="ctrList" class="list" runat="Server" style="display:none;">
<div runat="server" id="ctrTableContainer" style="overflow-y:scroll;width:100%;height:150px;"></div>
<table style="height:16px;background-color:Lime;width:100%;padding:4px"><tr><td runat="server" id="ctrRecordStatus">Record 1 of 10</td><td align="right" style="width:15px"><img runat="server" id="ctrFetchMoreRecords" style="position:relative;top:-2px" src="/inc/library/components/webcombo/media/paging.gif" loading="/inc/library/components/webcombo/media/loading.gif" normal="/inc/library/components/webcombo/media/paging.gif" hover="/inc/library/components/webcombo/media/paging_hl.gif" onmouseover="if( this.getAttribute('disable_hover') != '1' ) { this.src=this.getAttribute('hover'); }" onmouseout="if( this.getAttribute('disable_hover') != '1' ) { this.src=this.getAttribute('normal'); }" /></td></tr></table>
</div>
</div>