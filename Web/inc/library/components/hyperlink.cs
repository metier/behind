using System;
using System.Data;
using jlib.functions;
using jlib.db;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace jlib.components
{
	/// <summary>
	/// Summary description for textbox.
	/// </summary>
    public class hyperlink : System.Web.UI.WebControls.HyperLink, iControl, IPostBackEventHandler 
	{
		public bool ReadOnly { get; set; }
        private bool m_bPostBackRaised = false, m_bRendered = false, m_bRenderClientID = false, m_bTranslationHide=false;
        private string m_sDataMember = "", m_sTranslationKey = "", m_sFormatString = "", m_sQueryString = "", m_OnClientClick = "", m_sAjaxTargetControls = "", m_sAjaxDimControls = "", m_sAjaxPostControls = "";
        private textbox.FieldType m_eValidateAs = textbox.FieldType.Text;
        private object m_oValue = null;

        private Page m_oWebPage = null;		
		
        public event EventHandler Click;

        protected virtual void OnClick( EventArgs e ) {
            if ( Click != null ) Click( this, e );
        }
        
        
        public override string ClientID {
			get {
				m_bRenderClientID = true;
				return base.ClientID;
			}
		}
        public bool Rendered {
            get {
                return m_bRendered;
            }
        }
		public bool DisableSave {
			get {
				return true;
			}
			set {
			}
		}
        public bool Required {
            get {
                return true;
            }
            set {
            }
        }
		public string AjaxTargetControls {
			get {
				return m_sAjaxTargetControls;
			}
			set {
				m_sAjaxTargetControls = value;
			}
		}
		public string AjaxDimControls {
			get {
				return m_sAjaxDimControls;
			}
			set {
				m_sAjaxDimControls = value;
			}
		}
		public string AjaxPostControls {
			get {
				return m_sAjaxPostControls;
			}
			set {
				m_sAjaxPostControls = value;
			}
		}

        public bool IsClicked {
            get {
                if (Page.IsPostBack){// && Click != null) {
                    if ((Page as jlib.components.webpage).EventTarget == this.UniqueID) {
                        RaisePostBackEvent((Page as jlib.components.webpage).EventArgument);
                    }
                }
                return m_bPostBackRaised;
            }
        }

		public string DataMember{
			get{
				return m_sDataMember;
			}
			set{
				m_sDataMember	= value;
			}
		}

        public string FormatString {
            get {
                return m_sFormatString;
            }
            set {
                m_sFormatString = value;
            }
        }
        public string TranslationKey {
            get {
                return m_sTranslationKey;
            }
            set {
                m_sTranslationKey = value;
            }
        }
        public string QueryString {
            get {
                return m_sQueryString;
            }
            set {
                m_sQueryString = value;
            }
        }
        
        

        public string ValidateAs {
            get {
                return m_eValidateAs.ToString();
            }
            set {
                try {
                    m_eValidateAs = (textbox.FieldType)Enum.Parse( typeof( textbox.FieldType ), value, true );
                } catch ( Exception ) {
                    m_eValidateAs = textbox.FieldType.Text;
                }

            }
        }
		
		public string validate() { return ""; }


        public object getValue() { return m_oValue; }

        public iControl setValue(object oValue) {
            if ( m_eValidateAs == textbox.FieldType.Number ) oValue = convert.cInt( oValue );
            m_oValue = oValue;
            return this;
        }

        public void setVaildateFailure( bool bFailure ) {}


        public override string Text {
            get {
                return convert.cStr( getValue() );
            }
            set {
                setValue( value );
            }
        }

        public string OnClientClick {
            get {
                return m_OnClientClick;
            }
            set {
                m_OnClientClick = value;
            }
        }

        

        public Page WebPage {
            get {
                return ( m_oWebPage == null ? Page : m_oWebPage );
            }
            set {
                m_oWebPage = value;
            }
        }

        public string getUrl() {
			
            if ( Click != null ) {
                return WebPage.ClientScript.GetPostBackClientHyperlink( this, "Click");
                
            } else {

                string sLink = convert.cStr(this.NavigateUrl, this.Attributes["href"]);
                if (sLink.IsNullOrEmpty()) sLink = this.getValue().Str();
                if (sLink.Str().StartsWith("~")) sLink = System.Web.HttpContext.Current.Request.ApplicationPath + sLink.Substring(1);
                if ( QueryString.StartsWith( "?" ) ) QueryString = QueryString.Substring( 1 );
                if ( QueryString != "" ) sLink += ( sLink.IndexOf( "?" ) == -1 ? "?" : ( sLink.EndsWith( "%" ) || QueryString.StartsWith( "&" ) ? "" : "&" ) ) + QueryString;

                return sLink;
            }
        }



        protected override void OnLoad( EventArgs e ) {
            webpage oPage = (Page as webpage);
            if (oPage != null) {
                if (TranslationKey != "") {
                    string s = helpers.translation.translate(oPage.Language, this.Text, Page, TranslationKey);
                    if (s == null) {
                        m_bTranslationHide = true;
                        Text = "";
                        Visible = false;
                    } else this.Text = s;
                }
                if (Page.IsPostBack) {// && Click != null ) {
                    if (oPage.EventTarget == this.UniqueID) {
                        RaisePostBackEvent(oPage.EventArgument);
                    }
                }
            }
            base.OnLoad( e );
        }

		public string Render() {
			htmltextwriter oWriter = new htmltextwriter(new System.IO.StringWriter());
			this.Render(oWriter);
			return oWriter.InnerWriter.ToString();
		}
		
		protected override void Render( System.Web.UI.HtmlTextWriter writer ) {
            if (m_bTranslationHide) return;
            m_bRendered = true;
            if(convert.cStr(this.Attributes["onclick"])!=convert.cStr(OnClientClick)) this.Attributes["onclick"] = OnClientClick;
			
                string sLink = helpers.net.nav.getNavTree().getRedirect(getUrl());
                        
                if ( this.FormatString != "" && m_oValue != null )
                    this.Text = String.Format( this.FormatString, m_oValue );
                else
                    this.Text = convert.cStr( m_oValue );

                writer.Write( "<a" );
                if ( sLink != "" ) {
                    writer.Write( " href=\"" );
                    writer.WriteEncodedUrl( sLink );
                    writer.Write( "\"" );
                }

                if ( this.Target != "" ) writer.Write( " target=\"{0}\"", this.Target );
                if ( this.ToolTip != "" ) writer.Write( " Title=\"{0}\"", this.ToolTip );
				if (this.CssClass != "") writer.Write(" class=\"{0}\"", this.CssClass);
                if ( this.Attributes.Count > 0 ) this.Attributes.Render( writer );
				if (m_bRenderClientID) writer.WriteAttribute("id", this.ClientID);
                //if ( this.Style.Count > 0 ) writer.Write( " style=\"" + this.Style.Value + "\"" );

                writer.Write( ">" );
                //base.RenderChildren( writer );
                base.RenderContents( writer );
                writer.Write( "</a>" );

			//}
            
            //base.Render( writer );
        }

        public void RaisePostBackEvent( string sEventArgument ) {

            //if ( sEventArgument.StartsWith( "Click_" ) && !m_bPostBackRaised ) {
			if (!m_bPostBackRaised) {
                OnClick( new EventArgs() );
                m_bPostBackRaised = true;
            }
        }

	}
}
