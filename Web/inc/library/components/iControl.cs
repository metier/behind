using System;

namespace jlib.components
{
	/// <summary>
	/// Summary description for iControl.
	/// </summary>
	public interface iControl
	{
        string DataMember{ get; set; }
		bool Enabled { get; set; }
        bool Rendered { get; }
		bool DisableSave { get;set;}
        bool Required { get; set; }
        string ID { get; set; }
        bool Visible { get; set; }
        string ClientID { get; }
        string UniqueID { get; }
		//int HashCode { get; }
		//string Markup { get; set; }
		string validate();
		void setVaildateFailure( bool bFailure );
        object getValue();
        iControl setValue( object oValue );
		bool ReadOnly { get; set; }
	}
}
