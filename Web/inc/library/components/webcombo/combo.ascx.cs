using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Xml;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using jlib.components.webcombo;
using jlib.db;
using jlib.functions;


namespace jlib.components.webcombo
{

    public delegate void OnGetPage( object sender, comboEventArgs oArgs );
    public delegate void OnValueChanged( object sender, EventArgs oArgs );

    public class combo : System.Web.UI.UserControl, iControl
    {
        public event OnGetPage GetPage;
        public event OnValueChanged ValueChanged;

        private string m_sTables = "", m_sPK = "ID", m_sSort = "", m_sFields = "", m_sWhereClause = "", m_sGroup = "";
        private string m_sCustomHTMLDefinition = "{1}";
        private int m_iPKIndex = 0, m_iSearchIndex = 0, m_iDisplayIndex = 0;
        private bool m_bAutoPostBack = false, m_bAutoComplete = false, m_bPartialMatch = false;
        private bool m_bForceReloadOnListVisible = false, m_bWildCardSearch = false, m_bEnabled = true, m_bLocked = false, m_bDisableSave = false, m_bRendered = false;
                              

        private string m_sDataMember = "";
        private bool m_bRequired = false, m_bMultiSelect = false, m_bForceSelection = true;

        private string m_sFieldDesc = "", m_sSelectedEvent = "", m_sDblClickEvent = "";

        public hidden ctrValue, ctrMultiValue, ctrData;

        protected HtmlContainerControl ctrCombo;        
        
        public textbox ctrText;
		protected HtmlContainerControl ctrDataContainer;
        protected HtmlImage ctrButton;
		protected HtmlTableCell ctrButtonContainer;
        protected HtmlContainerControl ctrClearButtonContainer;
        protected HtmlImage ctrClearButton;

        protected HtmlContainerControl ctrList, ctrTableContainer;                                

        protected HtmlTableCell ctrRecordStatus;
        protected HtmlImage ctrFetchMoreRecords;

        public HtmlContainerControl Combo {
            get {
                return ctrCombo;
            }
        }
		public bool ReadOnly { get; set; }
		public bool Enabled {
			get {
				return m_bEnabled;
			}
			set {
				m_bEnabled = value;
			}
		}
        public bool Rendered {
            get {
                return m_bRendered;
            }
        }
		public bool Locked {
			get {
				return m_bLocked;
			}
			set {
				m_bLocked = value;
			}
		}
		public bool DisableSave {
			get {
				return m_bDisableSave;
			}
			set {
				m_bDisableSave = value;
			}
		}

        public Unit Width {
            get {
                return convert.cUnit(ctrCombo.Style["width"]) ;
            }
            set {
                ctrCombo.Style["width"] = value.ToString();
				this.ctrText.Width = convert.cUnit(value.Value - (MultiSelect ? 10 : 27));
				this.ctrDataContainer.Style["width"] = convert.cUnit(value.Value - ( MultiSelect ? 0 : 17 )).ToString();
				ctrList.Style["width"] = value.ToString();
            }
        }

		public Unit Height {
			get {
				return convert.cUnit(ctrDataContainer.Style["height"]);
			}
			set {
				ctrDataContainer.Style["height"] = value.ToString();				
			}
		}

        public int PageSize {
            get {
                if ( convert.cInt( this.ViewState["PageSize"] ) < 1 ) return 10;
                return convert.cInt( this.ViewState["PageSize"] );                
            }
            set {
                this.ViewState["PageSize"] = value;                
            }
        }

        public string Text {
            get {
                return ctrText.Text;
            }
            set {
                ctrText.Text = value;
            }
        }

        public string Value {
            get {
				if (MultiSelect) {
					ctrMultiValue.Value = parse.replaceAll(ctrMultiValue.Value, ",,", ",");
					if (ctrMultiValue.Value.StartsWith(",")) ctrMultiValue.Value = ctrMultiValue.Value.Substring(1);
					if (ctrMultiValue.Value.EndsWith(",")) ctrMultiValue.Value = ctrMultiValue.Value.Substring(0, ctrMultiValue.Value.Length -1);					
					return ctrMultiValue.Value;
				}
                return ctrValue.Value;
            }
            set {
				if (MultiSelect) {
					ctrMultiValue.Value = value;
					return;
				}

				if ( ctrValue.Value == value && this.Text.Trim() != "" ) return;
                ctrValue.Value = value;

                if ( this.Value == "" ) return;
                //ado_helper oData = new ado_helper();
                //DataTable oDT = oData.Execute_SP( "Paging_Cursor", "@Tables", this.Tables, "@PK", this.PK, "@Sort", this.Sort, "@PageNumber", 0, "@PageSize", PageSize, "@Fields", this.Fields, "@Filter", this.PK + "='" + ado_helper.PrepareDB( this.Value ) + "'" , "@PostJoin", "", "@Group", this.Group );
				DataTable oDT = sqlbuilder.getDataTable(String.Format("SELECT TOP {0} {1} from {2} WHERE {3} ORDER BY {4}", this.PageSize, this.Fields, this.Tables, this.PK + "='" + ado_helper.PrepareDB(this.Value) + "'", "ID"));
                if ( oDT.Rows.Count > 0 ) this.Text = convert.cStr( oDT.Rows[0][this.DisplayIndex] );
            }
        }

        public string InitialData {
            get {
                return ctrData.Value;
            }
            set {
                ctrData.Value = value;
            }
        }


        public string Tables {
            get {
                return m_sTables;
            }
            set {
                m_sTables = value;
            }
        }

        public string PK {
            get {
                return m_sPK;
            }
            set {
                m_sPK = value;                
            }
        }

        public string Sort {
            get {
                return m_sSort;
            }
            set {
                m_sSort = value;                
            }
        }

        public string Fields {
            get {
                return m_sFields;
            }
            set {
                m_sFields = value;
            }
        }

        public string WhereClause {
            get {
                return m_sWhereClause;
            }
            set {
                m_sWhereClause = value;
            }
        }

        public string Group {
            get {
                return m_sGroup;
            }
            set {
                m_sGroup = value;
            }
        }

        public string DataMember {
            get {
                return m_sDataMember;
            }
            set {
                m_sDataMember = value;
            }
        }

        public bool Required {
            get {
                return m_bRequired;
            }
            set {
                m_bRequired = value;
            }
		}

		public bool MultiSelect {
			get {
				return m_bMultiSelect;
			}
			set {
				m_bMultiSelect = value;
			}
		}

		public bool WildCardSearch {
			get {
				return m_bWildCardSearch;
			}
			set {
				m_bWildCardSearch = value;
			}
		}

		public bool ForceReloadOnListVisible {
			get {
				return m_bForceReloadOnListVisible;
			}
			set {
				m_bForceReloadOnListVisible = value;
			}
		}		

        public string FieldDesc {
            get {

                if ( m_sFieldDesc != "" )
                    return m_sFieldDesc;

                if ( m_sDataMember != "" ) {
                    if ( m_sDataMember.IndexOf( "." ) != -1 )
                        return parse.split( m_sDataMember, "." )[1];
                    else
                        return m_sDataMember;
                }
                return this.ID;

            }

            set {
                m_sFieldDesc = value;
            }
        }

        public string CustomHTMLDefinition {
            get {
                return m_sCustomHTMLDefinition;
            }
            set {
                m_sCustomHTMLDefinition = value;
            }
        }


        public int PKIndex {
            get {
                return m_iPKIndex;
            }
            set {
                m_iPKIndex = value;
            }
        }

        public int SearchIndex {
            get {
                return m_iSearchIndex;
            }
            set {
                m_iSearchIndex = value;
            }
        }

        public int DisplayIndex {
            get {
                return m_iDisplayIndex;
            }
            set {
                m_iDisplayIndex = value;
            }
        }

        public bool AutoPostBack {
            get {
                return m_bAutoPostBack;
            }
            set {
                m_bAutoPostBack = value;
            }
        }

		//Indicate whether we should auto-complete as the user types
        public bool AutoComplete {
            get {
                return m_bAutoComplete;
            }
            set {
                m_bAutoComplete = value;
            }
        }

		public bool ForceSelection {
			get {
				return m_bForceSelection;
			}
			set {
				m_bForceSelection = value;
			}
		}

		//Indicates whether we should allow for partial matching (this is only valuable when Value is text)
        public bool PartialMatch {
            get {
                return m_bPartialMatch;
            }
            set {
                m_bPartialMatch = value;
            }
        }

        public bool ClearButton {
            get {
                return ctrClearButtonContainer.Visible;
            }
            set {
                ctrClearButtonContainer.Visible = value;
            }
        }

		public string SelectedEvent {
            get {
				return m_sSelectedEvent;
            }
            set {
				m_sSelectedEvent = value;
            }
        }

		public string DblClickEvent {
            get {
				return m_sDblClickEvent;
            }
            set {
				m_sDblClickEvent = value;
            }
        }

		

        protected virtual void OnGetPage( comboEventArgs oArgs ){
            if ( GetPage != null ) GetPage( this, oArgs );
        }

        protected virtual void OnValueChanged( EventArgs oArgs ) {
            if ( ValueChanged != null ) ValueChanged( this, oArgs );
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            
            if (ctrValue.Value == "" && convert.cStr(Request[ctrValue.UniqueID]) != "") ctrValue.Value = convert.cStr(Request[ctrValue.UniqueID]);
			if (ctrMultiValue.Value == "" && convert.cStr(Request[ctrMultiValue.UniqueID]) != "") ctrMultiValue.Value = convert.cStr(Request[ctrMultiValue.UniqueID]);
			
        }

        public void Page_Init( object o, EventArgs e ) {
            Page.InitComplete += new EventHandler( Page_InitComplete );
            Page.LoadComplete += new EventHandler( Page_LoadComplete );
        }

        void Page_LoadComplete( object sender, EventArgs e ) {
            if ( Request["__EVENTARGUMENT"] == "GETDATA" ) Response.End();
        }

        void Page_InitComplete( object sender, EventArgs e ) {
            //if ( Request["__EVENTTARGET"] == this.ctrText.UniqueID && Request["__EVENTARGUMENT"] == "GETDATA" ) {
                //Page.Controls.Clear();


            //this was moved from PAGE LOAD (and before that from Render, since GETDATA was causing issues)
			if ((Request.QueryString["__EVENTTARGET"] == this.ctrText.UniqueID && Request.QueryString["__EVENTARGUMENT"] == "GETDATA") || (Request.Form["__EVENTTARGET"] == this.ctrText.UniqueID && Request.Form["__EVENTARGUMENT"] == "GETDATA")) {

                if ( convert.cInt( Request.QueryString["page_size"] ) > 0 ) this.PageSize = convert.cInt( Request.QueryString["page_size"] );

                comboEventArgs oArgs = new comboEventArgs( this, this.PageSize, this.Text, this.Value );
                OnGetPage( oArgs );

                if ( oArgs.DataSource == null ) {
                    ado_helper oData1 = new ado_helper();
                    //oData.Execute_SP( "Paging_Cursor", "@Tables", sTables, "@PK", sPK, "@Sort", sInitialSort, "@PageNumber", ( this.CurrentPage + 1 ), "@PageSize", this.PageSize, "@Fields", sFields, "@Filter", sFilter, "@Group", sGroup );
					//oArgs.DataSource = oData1.Execute_SP("Paging_Cursor", "@Tables", this.Tables, "@PK", this.PK, "@Sort", (ctrValue.Value != "" ? "CASE WHEN " + this.PK + "='" + ado_helper.PrepareDB(ctrValue.Value) + "' THEN 0 ELSE 1 END, " : "" ) + this.Sort, "@PageNumber", 0, "@PageSize", PageSize, "@Fields", this.Fields, "@Filter", "(" + (ctrValue.Value != "" ? this.PK + "='" + ado_helper.PrepareDB(ctrValue.Value) + "' OR " : "") + " ( " + this.Sort + " >= '" + (WildCardSearch ? "%" : "") + parse.replaceAll(this.Text, "'", "''") + "' AND " + this.Sort + " LIKE '" + (WildCardSearch ? "%" : "") + parse.replaceAll(this.Text, "'", "''") + "%' " + (this.WhereClause == "" ? "" : " and " + this.WhereClause) + "))", "@PostJoin", "", "@Group", this.Group);
					string sText = this.Text;
					if (sText.Trim() == "") sText = "a";
					oArgs.DataSource = sqlbuilder.getDataTable(oData1, String.Format("SELECT TOP {0} {1} from {2} WHERE {3} ORDER BY {4}", this.PageSize, this.Fields, this.Tables, "(" + (ctrValue.Value != "" ? this.PK + "='" + (this.PK.ToLower() == "id" ? convert.cInt(parse.split(ctrValue.Value, ",")[0]).ToString() : ado_helper.PrepareDB(ctrValue.Value)) + "' OR " : "") + " ( " + this.Sort + " >= '" + (WildCardSearch ? "%" : "") + parse.replaceAll(sText, "'", "''") + "' AND " + this.Sort + " LIKE '" + (WildCardSearch ? "%" : "") + parse.replaceAll(sText, "'", "''") + "%' " + (this.WhereClause == "" ? "" : " and " + this.WhereClause) + "))", (ctrValue.Value != "" ? "CASE WHEN " + this.PK + "='" + ado_helper.PrepareDB(ctrValue.Value) + "' THEN 0 ELSE 1 END, " : "") + this.Sort));
					if (oArgs.DataSource.Rows.Count == 0 && sText != this.Text) {
						sText = this.Text;
						sqlbuilder.getDataTable(oData1, String.Format("SELECT TOP {0} {1} from {2} WHERE {3} ORDER BY {4}", this.PageSize, this.Fields, this.Tables, "(" + (ctrValue.Value != "" ? this.PK + "='" + (this.PK.ToLower() == "id" ? convert.cInt(parse.split(ctrValue.Value, ",")[0]).ToString() : ado_helper.PrepareDB(ctrValue.Value)) + "' OR " : "") + " ( " + this.Sort + " >= '" + (WildCardSearch ? "%" : "") + parse.replaceAll(sText, "'", "''") + "' AND " + this.Sort + " LIKE '" + (WildCardSearch ? "%" : "") + parse.replaceAll(sText, "'", "''") + "%' " + (this.WhereClause == "" ? "" : " and " + this.WhereClause) + "))", (ctrValue.Value != "" ? "CASE WHEN " + this.PK + "='" + ado_helper.PrepareDB(ctrValue.Value) + "' THEN 0 ELSE 1 END, " : "") + this.Sort));
					}
                }        

                Response.ContentType = "text/javascript";

				Response.Write("cComboDataCallbackJSON( $('" + ctrCombo.ClientID + "'), " + jlib.functions.convert.cJSON(oArgs.DataSource, false) + ", " + convert.cInt(Request["search_counter"]) + " );");
                Response.End();
            }
        
        }

        public void Page_Load( object o, EventArgs e ){

            if ( Request["__EVENTTARGET"] == this.ctrText.UniqueID && Request["__EVENTARGUMENT"] == "GETDATA" ) Page_InitComplete( null, null );            

            if ( Request["__EVENTTARGET"] == this.ctrText.UniqueID && Request["__EVENTARGUMENT"] == "CHANGED" ) {
                OnValueChanged( new EventArgs() );
            }
            

        }

        public void Page_PreRender( object sender, EventArgs e ) {

            if ( Request["__EVENTTARGET"] == this.ctrText.UniqueID && Request["__EVENTARGUMENT"] == "GETDATA" ) Page_InitComplete( null, null );

			if (!convert.cBool(ConfigurationManager.AppSettings["common.disable.includes"])) {
				Page.RegisterClientScriptBlock("jlib.components.webcombo.combo.css", "<LINK href=\"/inc/library/components/webcombo/media/combo.css\" type=\"text/css\" rel=\"stylesheet\" />	");
				Page.RegisterClientScriptBlock("jlib.components.webcombo.combo.js", "<script type=\"text/javascript\" src=\"/inc/library/components/webcombo/media/combo.js\"></script>");
			}


			string sMultiSelectData = "";
			if (MultiSelect && this.Value != "") {
				string []sSort = parse.split( parse.stripLeadingCharacter( parse.stripEndingCharacter( this.Value,","),","), ",");
				for (int x = 0; x < sSort.Length; x++)
					if (sSort[x].Trim() != "") sSort[x] = (x == 0 ? "CASE " : "") + " WHEN " + this.PK + "=" + sSort[x] + " THEN " + x + " ";

				DataTable oDT = sqlbuilder.getDataTable( String.Format("select {0} from {1} WHERE {2} IN ({3}) {4} {5}", this.Fields, this.Tables, this.PK, this.Value, (this.WhereClause == "" ? "" : " AND " + this.WhereClause ), ( parse.join(sSort,"").Trim() == "" ? "" : " ORDER BY " +parse.join(sSort,"").Trim() + " END " )));// ( this.Sort == "" ? "" : " ORDER BY " + this.Sort ));
				sMultiSelectData = jlib.functions.convert.cJSON(oDT, false);				
			}

			Page.RegisterStartupScript("draw_combo_" + this.UniqueID, String.Format("<script>initcCombo('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}',{12},{13});</script>", ctrCombo.ClientID, ctrDataContainer.ClientID, ctrText.ClientID, ctrButton.ClientID, ctrList.ClientID, ctrData.ClientID, ctrTableContainer.ClientID, ctrValue.ClientID, ctrMultiValue.ClientID, ctrRecordStatus.ClientID, ctrFetchMoreRecords.ClientID, ctrClearButton.ClientID, (sMultiSelectData == "" ? "null" : sMultiSelectData), (convert.cStr(SelectedEvent).Trim() == "" ? "null" : SelectedEvent)));
            //Response.Write( MagicAjax.AjaxCallHelper.GetAjaxCallEventReference( this, 
            ctrCombo.Attributes["PKIndex"] = this.PKIndex.ToString();
            ctrCombo.Attributes["SearchIndex"] = this.SearchIndex.ToString();
            ctrCombo.Attributes["DisplayIndex"] = this.DisplayIndex.ToString();
            ctrCombo.Attributes["CustomHTMLDefinition"] = this.CustomHTMLDefinition.ToString();
            ctrCombo.Attributes["PageSize"] = this.PageSize.ToString();
            ctrCombo.Attributes["AutoPostBack"] = AutoPostBack.ToString();
            ctrCombo.Attributes["AutoComplete"] = AutoComplete.ToString();
            ctrCombo.Attributes["PartialMatch"] = PartialMatch.ToString();
			ctrCombo.Attributes["MultiSelect"] = MultiSelect.ToString().ToLower();
			ctrCombo.Attributes["DblClickEvent"] = DblClickEvent;
			ctrCombo.Attributes["ForceSelection"] = ForceSelection.ToString();
			ctrCombo.Attributes["ForceReloadOnListVisible"] = ForceReloadOnListVisible.ToString().ToLower();
			
			this.Width = this.Width;
			if (MultiSelect) {
				ctrButtonContainer.Visible = false;
				ctrCombo.Attributes["PartialMatch"] = "false";
				ctrCombo.Attributes["AutoPostBack"] = "false";
				ctrDataContainer.Style["border-right"] = "1px solid #8396c3";
			} else {
				//ctrText.Width = convert.cUnit("100%");
			}

			if (this.Locked || !this.Enabled) {
				ctrButtonContainer.Visible = false;
				ctrDataContainer.Style["border-right"] = "1px solid #8396c3";
				ctrCombo.Attributes["PartialMatch"] = "false";
				ctrCombo.Attributes["AutoPostBack"] = "false";
				ctrCombo.Attributes["Enabled"] = "false";
			}
        }
       

        public string validate() {
            if ( this.Required && this.Value == "" ) {                
                    return this.FieldDesc + " m� v�re valgt.";
            }
            return "";
        }
        protected override void Render(HtmlTextWriter writer) {
            m_bRendered = true;
            base.Render(writer);
        }

        public void setVaildateFailure( bool bFailure ) {
            if ( bFailure ) {
                ctrCombo.Attributes["class"] = parse.replaceAll( ctrCombo.Attributes["class"], " validate_failure", "" ) + " validate_failure";
            } else {
                ctrCombo.Attributes["class"] = parse.replaceAll( ctrCombo.Attributes["class"], " validate_failure", "" );
            }
        }

        public object getValue() {
            return this.Value;
        }

        public iControl setValue( object oObject ) {
            this.Value = convert.cStr( oObject );
            return this;            
        }

		public void setMultiValue(DataTable oDT) {
			this.Value = "";
			for (int x = 0; x < oDT.Rows.Count; x++)
				this.Value += ","+oDT.Rows[x][0];
		}
	}

	public class comboEventArgs : EventArgs {

        private DataTable m_oDataSource = null;
        private int m_iPageSize = 10;
        private string m_sValue = "";
        private string m_sText  = "";
        private combo m_oOwner = null;

        public comboEventArgs( combo oOwner, int iPageSize, string sText, string sValue ) {
            m_oOwner = oOwner;
            m_iPageSize = iPageSize;
            m_sText = sText;
            m_sValue = sValue;
        }

        public combo Owner {
            get {
                return m_oOwner;
            }
            set {
                m_oOwner = value;
            }
        }

        public int PageSize {
            get {
                return m_iPageSize;
            }
            set {
                m_iPageSize = value;
            }
        }

        public string Text{
            get {
                return m_sText;
            }
            set {
                m_sText = value;
            }
        }

        public string Value {
            get {
                return m_sValue;
            }
            set {
                m_sValue = value;
            }
        }
        
        public DataTable DataSource {
            get {
                return m_oDataSource;
            }
            set {
                m_oDataSource = value;
            }
        }
    }
}
