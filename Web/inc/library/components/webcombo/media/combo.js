function drawCombo(sContainer, sID, sSettings, iWidth, sText, sValue, sMultiValue, sMultiValueData, sSelectedEvent){
        
    $(sContainer).update('<input type="hidden" id="' + sID + '_ctrValue" value="' + cStr(sValue) + '"><input value="' + cStr(sMultiValue) + '" type="hidden" id="' + sID + '_ctrMultiValue"><input type="hidden" id="' + sID + '_ctrData"><div id="' + sID + '_ctrCombo" ' + sSettings + ' type="common_combo">'
        
        + '<div><div id="' + sID + '_ctrDataContainer" class="data_container" style="width:' + (iWidth-17) + 'px;float:left">'
        + '<input value="' + cStr(sText)+ '" name="' + sID + '" type="text" id="' + sID + '_ctrText" AUTOCOMPLETE="off" onkeydown="if( event.keyCode == 13 ){ event.cancelBubble = true; return false;}" onkeyup="if( event.keyCode == 13 ){ event.cancelBubble = true; return false;}" AjaxCall="async" style="width:' + (iWidth-27) + 'px;" /></div>'
        + '<div style="float:left" id="' + sID + '_ctrButtonContainer"><img src="/inc/library/components/webcombo/media/button.gif" id="' + sID + '_ctrButton" onmouseout="this.src=this.getAttribute(\'normal\');" hover="/inc/library/components/webcombo/media/button_hl.gif" normal="/inc/library/components/webcombo/media/button.gif" onmouseup="this.src=this.getAttribute(\'hover\');" class="button" onmouseover="this.src=this.getAttribute(\'hover\');" pressed="/inc/library/components/webcombo/media/button_pressed.gif" onmousedown="this.src=this.getAttribute(\'pressed\');" /></div>'
        + '<div style="float:left" id="' + sID + '_ctrClearButtonContainer" class="delete"><div onmouseover="this.className = \'hover\';" onmouseout="this.className = \'\';" ><img src="/inc/library/components/webcombo/media/delete.gif" id="' + sID + '_ctrClearButton" style="margin:2px" /></div></div></div><br style="clear:both" />'
        
        + '<div id="' + sID + '_ctrList" class="list" style="display:none;width:' + (iWidth) + 'px;"><div id="' + sID + '_ctrTableContainer" style="overflow-y:scroll;width:100%;height:150px;"></div>'
        + '<div style="height:16px;background-color:Lime;width:100%;padding:1px"><div id="' + sID + '_ctrRecordStatus" style="float:left">Record 1 of 10</div><div style="float:right;width:15px"><img src="/inc/library/components/webcombo/media/paging.gif" id="' + sID + '_ctrFetchMoreRecords" style="position:relative;top:-2px" loading="/inc/library/components/webcombo/media/loading.gif" normal="/inc/library/components/webcombo/media/paging.gif" hover="/inc/library/components/webcombo/media/paging_hl.gif" onmouseover="if( this.getAttribute(\'disable_hover\') != \'1\' ) { this.src=this.getAttribute(\'hover\'); }" onmouseout="if( this.getAttribute(\'disable_hover\') != \'1\' ) { this.src=this.getAttribute(\'normal\'); }" /></div></div>'
        //'<table style="height:16px;background-color:Lime;width:100%;padding:4px"><tr><td id="' + sID + '_ctrRecordStatus">Record 1 of 10</td><td align="right" style="width:15px"><img src="/inc/library/components/webcombo/media/paging.gif" id="' + sID + '_ctrFetchMoreRecords" style="position:relative;top:-2px" loading="/inc/library/components/webcombo/media/loading.gif" normal="/inc/library/components/webcombo/media/paging.gif" hover="/inc/library/components/webcombo/media/paging_hl.gif" onmouseover="if( this.getAttribute(\'disable_hover\') != \'1\' ) { this.src=this.getAttribute(\'hover\'); }" onmouseout="if( this.getAttribute(\'disable_hover\') != \'1\' ) { this.src=this.getAttribute(\'normal\'); }" /></td></tr></table>'
        + '</div></div>');
        
        /*
        '<table><tr><td><div id="' + sID + '_ctrDataContainer" class="data_container" style="width:' + (iWidth-17) + 'px;">'
        + '<input value="' + cStr(sText)+ '" name="' + sID + '" type="text" id="' + sID + '_ctrText" AUTOCOMPLETE="off" onkeydown="if( event.keyCode == 13 ){ event.cancelBubble = true; return false;}" onkeyup="if( event.keyCode == 13 ){ event.cancelBubble = true; return false;}" AjaxCall="async" style="width:' + (iWidth-27) + 'px;" /></div></td>'
        + '<td id="' + sID + '_ctrButtonContainer"><img src="/inc/library/components/webcombo/media/button.gif" id="' + sID + '_ctrButton" onmouseout="this.src=this.getAttribute(\'normal\');" hover="/inc/library/components/webcombo/media/button_hl.gif" normal="/inc/library/components/webcombo/media/button.gif" onmouseup="this.src=this.getAttribute(\'hover\');" class="button" onmouseover="this.src=this.getAttribute(\'hover\');" pressed="/inc/library/components/webcombo/media/button_pressed.gif" onmousedown="this.src=this.getAttribute(\'pressed\');" /></td>'
        + '<td id="' + sID + '_ctrClearButtonContainer" class="delete"><div onmouseover="this.className = \'hover\';" onmouseout="this.className = \'\';" ><img src="/inc/library/components/webcombo/media/delete.gif" id="' + sID + '_ctrClearButton" style="margin:2px" /></div></td></tr></table>'
        */
        
                
    initcCombo(sID + '_ctrCombo',sID + '_ctrDataContainer',sID + '_ctrText',sID + '_ctrButton',sID + '_ctrList',sID + '_ctrData',sID + '_ctrTableContainer',sID + '_ctrValue',sID + '_ctrMultiValue',sID + '_ctrRecordStatus',sID + '_ctrFetchMoreRecords',sID + '_ctrClearButton', sMultiValueData, sSelectedEvent);
}
function initcCombo( sComboID, sDataContainerID, sTextID, sButtonID, sListID, sDataID, sTableContainerID, sValueID, sMultiValueID, sStatusID, sMoreID, sClearID, sMultiValueData, sSelectedEvent ){

    var oCombo  = $( sComboID );
    
    if( oCombo.Initiated ) return;
    oCombo.Initiated = true;
    oCombo.SearchCounter = 0;
    oCombo.DataPending = false;
           
    oCombo.DataContainer = $( sDataContainerID );
    oCombo.DataContainer.Combo = oCombo;    
    Event.observe(oCombo.DataContainer, "click", function(event){oCombo.Text.focus(); } );    
    
    oCombo.Text = $( sTextID );
    oCombo.Text.Combo   = oCombo;
    oCombo.Text.IsDirty = true;
    oCombo.Text.HasFocus = false;
    oCombo.Enabled  = oCombo.getAttribute( "Enabled" ) != "false";
    
    if( oCombo.Enabled ){
        Event.observe(oCombo.Text, "keypress", function(event){cComboTextChanged(oCombo,event)} );
        Event.observe(oCombo.Text, "keyup", function(event){cComboTextChanged(oCombo,event)} );
        Event.observe(oCombo.Text, "focus", function(event){ if( oCombo.Text.HasFocus ) return; setSelectionRange(oCombo.Text,0,oCombo.Text.value.length ); oCombo.Text.HasFocus = true; } );
        Event.observe(oCombo.Text, "blur", function(event){ oCombo.Text.HasFocus = false; } );
    }
                
    oCombo.Value = $( sValueID );
    oCombo.setAttribute( "current_value", oCombo.Value.value );
    
    oCombo.MultiValue = $( sMultiValueID );      
    oCombo.MultiSelect  = oCombo.getAttribute( "MultiSelect" ) == "true";
    oCombo.ForceReloadOnListVisible = oCombo.getAttribute( "ForceReloadOnListVisible" ) == "true";
    oCombo.AddMultiItem = cComboAddMultiItem;
    oCombo.RemoveMultiItem = cComboRemoveMultiItem;
    
    oCombo.getValue = function(iIndex){ if(iIndex<0||cStr(iIndex)=="") return (oCombo.MultiSelect?oCombo.MultiValue.value:oCombo.Value.value); else if (oCombo.getAttribute( "selected_index" ) && cInt(oCombo.getAttribute( "selected_index" ))>=0) return oCombo.Data[cInt(oCombo.getAttribute( "selected_index" ))][ iIndex ]; };
    //oCombo.getValue = function(iIndex){ return (oCombo.MultiSelect?oCombo.MultiValue.value:oCombo.Value.value);  };
    oCombo.getText = function(){return oCombo.Text.value;};
    
    if( oCombo.MultiSelect && sMultiValueData != null ){
        oCombo.MultiValue.value = "";
        
        for( var x = 0; x < sMultiValueData.length; x++ )            
            oCombo.AddMultiItem( sMultiValueData[x][ cInt(oCombo.getAttribute( "PKIndex" )) ], sMultiValueData[x][ cInt(oCombo.getAttribute( "DisplayIndex" )) ] );
                
    }
    
    oCombo.Button           = $( sButtonID );
    if(oCombo.Button){
        oCombo.Button.Combo     = oCombo;
        Event.observe( oCombo.Button, "click", function(event){ if( oCombo.ForceReloadOnListVisible  && oCombo.List.style.display == 'none') oCombo.Text.IsDirty = true; cComboShowHideList(oCombo, event); }  );
    }
    
    oCombo.Status           = $( sStatusID );
    oCombo.MoreButton       = $( sMoreID );    
    Event.observe( oCombo.MoreButton, "click", function(){ if( oCombo.MoreButton.getAttribute( "disable_hover" ) == "1" ) return;  oCombo.setAttribute( "PageSize", "" + (Number(oCombo.getAttribute( "PageSize" ) ) + 10 )); oCombo.Text.IsDirty = true; cComboLoadData(oCombo); }  );
    
    oCombo.ClearButton       = $( sClearID );
    if( oCombo.ClearButton != null )     
        Event.observe( oCombo.ClearButton, "click", function(){ oCombo.Value.value = ""; oCombo.Text.value = ""; } );
        
        
    oCombo.List = $( sListID );    
    oCombo.List.Combo   = oCombo;
    oCombo.List.show    = cComboListShow;
    oCombo.List.hide    = cComboListHide;
    oCombo.List.TableContainer   = $( sTableContainerID );
        
    Event.observe( oCombo.List, "click" , function(){ oCombo.Text.focus();cComboListHide( oCombo, event );  }  );    
                
    oCombo.DataStore    = $( sDataID );
    oCombo.SetSelected  = cComboSetSelected;
            
    oCombo.Data         = Array();
    
    if( oCombo.Enabled ){    
        Event.observe( document.body, "click" , function(event){ if( oCombo.List.style.display == '' ) cComboListHide( oCombo, event )}  , sComboID);
        Event.observe( document.body, "keypress" , function(event){ if( oCombo.List.style.display == '' ) cComboListHide( oCombo, event )}  , sComboID);
    }
    
    oCombo.selectedEvent = ( sSelectedEvent != null || oCombo.MultiSelect ? function(event){ if (oCombo.MultiSelect)  oCombo.AddMultiItem(); if(sSelectedEvent != null) sSelectedEvent(oCombo,oCombo.Value.value,oCombo.Text.value); } : null );
    
//    cComboLoadData( oCombo, true );
}

function cComboUpdateHighlight( oCombo ){
    if( oCombo.Text.getAttribute( "typed_text" ) == null ) return;
    if( oCombo.Text.value.toUpperCase().indexOf( oCombo.Text.getAttribute( "typed_text" ).toUpperCase() ) == 0 )
        var iOldLength      = oCombo.Text.getAttribute( "typed_text" ).length + 1;
    else
        var iOldLength      = 0;
                    
    setSelectionRange( oCombo.Text, iOldLength, oCombo.Text.value.length );    
}

function cComboTextChanged( oCombo, event ){
    
    if (event && ( event.ctrlKey || event.altKey )) return;
    
    var oCombo  = ( oCombo == null ? this.Combo : oCombo );        
    
    //esc pressed, restore old value/text
    if( event.keyCode == 27 ){        
        oCombo.SetSelected( ); Event.stop(event); oCombo.List.hide(null,event); return false;
    }
    
    if( event.type == "keydown" || event.type == "keypress" ){
        //9 = tab
        
        //if( event.keyCode == 13 || event.keyCode == 9 ){ if( oCombo.MultiSelect ){ oCombo.Value.value = ""; oCombo.Text.value = "";}else{ oCombo.SetSelected( );} oCombo.List.hide(null,event); return false; }
        if( event.keyCode == 13 || event.keyCode == 9 ){ 
            if( !oCombo.Text.IsDirty && !oCombo.DataPending ){             
                if( oCombo.MultiSelect ){ oCombo.Value.value = ""; oCombo.Text.value = "";}else if(oCombo.getAttribute( "ForceSelection") == "True"){ oCombo.SetSelected( );}
            }else if( oCombo.Text.IsDirty ){
                cComboLoadData( oCombo, true );
                clearTimeout( oCombo.DataTimer );
            }
            oCombo.List.hide(null,event); 
            return false; 
        }
        
        var iSelectedRow    = oCombo.getAttribute( "selected_index" );
        if( event.keyCode == 38 || ( event.keyCode == 40 && oCombo.List.style.display != "none" ) || event.keyCode == 33 || event.keyCode == 34 ){
                               
            if( oCombo.Data.length > 0 ){
            
                if( iSelectedRow == null ) iSelectedRow = -1;
                iSelectedRow = cInt( iSelectedRow );
                if( oCombo.List.style.display == '' ) iSelectedRow    = ( event.keyCode == 38 ? iSelectedRow-1 : ( event.keyCode == 40  ? iSelectedRow+1 : ( event.keyCode == 34 ? oCombo.Data.length - 1 : 0 ))) ;
                if( iSelectedRow < 0 ) iSelectedRow = oCombo.Data.length - 1;
                if( iSelectedRow > oCombo.Data.length - 1 ) iSelectedRow = 0;                        
                oCombo.SetSelected( iSelectedRow, oCombo.Data[iSelectedRow][ cInt( oCombo.getAttribute( "PKIndex" ) )], null );                    
                oCombo.SetSelected( );
                
                Event.stop(event);                        
                return false;            
            }            
        }
    }else{
    //this is a keyup
        if( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 32 || event.keyCode >= 65 || ( event.keyCode == 40 && oCombo.List.style.display == "none" )  ){
            //String.fromCharCode(event.keyCode)
            if( oCombo.Text.value != oCombo.Text.getAttribute( "typed_text" ) ){
                clearTimeout( oCombo.DataTimer );        
                oCombo.Text.IsDirty = true;
                oCombo.DataTimer    = setTimeout( function(){ cComboLoadData( oCombo ); }, 400 );
                oCombo.Text.setAttribute( "typed_text" , oCombo.Text.value  );            
            }else{
                oCombo.List.show();
            }   
        }
    }
}


function cComboSetSelected( iIndex, iValue, sDisplay, sSearch ){
    var oCombo  = this;
    
    if( iIndex == null )
        iIndex  = oCombo.getAttribute( "selected_index" );        
    
    if( iIndex == null ){
        if( oCombo.Value.value == null || oCombo.Value.value == "" )
            oCombo.Text.value = '';
        return;
    }
    iIndex = cInt(iIndex);
    if( iValue == null || iIndex == -1 ) iValue = "";

    
    if( iValue == "" && iIndex > -1 ){
        
        if( oCombo.Data.length > 0 ){
            if( iIndex < 0 ) iIndex = oCombo.Data.length - 1;
            if( iIndex > oCombo.Data.length - 1 ) iIndex = 0;
            iValue  = oCombo.Data[iIndex][ cInt( oCombo.getAttribute( "PKIndex" ) ) ];
            if( sDisplay == null ) sDisplay = oCombo.Data[iIndex][ cInt( oCombo.getAttribute( "DisplayIndex" ) ) ];
        }
    }
    
    if( sDisplay == null && oCombo.getAttribute( "AutoComplete" ) == "True"  ){
            
        if( oCombo.Data.length > 0 )
            sDisplay = oCombo.Data[iIndex][ cInt( oCombo.getAttribute( "SearchIndex" ) ) ];        
    }    
    
    if( sDisplay != null )
        oCombo.Text.value   = sDisplay;    
        
    oCombo.Value.value  = iValue;
    
    if ( $( oCombo.getAttribute( "id" ) +  "_row_" + oCombo.getAttribute( "selected_index" ) ) != null ) $( oCombo.getAttribute( "id" ) +  "_row_" + oCombo.getAttribute( "selected_index" ) ).className = $( oCombo.getAttribute( "id" ) +  "_row_" + oCombo.getAttribute( "selected_index" ) ).className.split(" selected").join("");
    
    if( iIndex > -1 ){
        oCombo.setAttribute( "selected_index", iIndex );
        var oSelectedRow    = $( oCombo.getAttribute( "id" ) +  "_row_" + iIndex );        
        if( oSelectedRow != null ){
            oSelectedRow.className = oSelectedRow.className.split(" selected").join("") + " selected";            
            clearTimeout( oCombo.List.TableContainer.ScrollTimeout );        
                        
            if ( Position.cumulativeOffset( oSelectedRow )[1] - Position.cumulativeOffset( oCombo.List.TableContainer )[1] > oCombo.List.TableContainer.scrollTop + 120 )
                oCombo.List.TableContainer.ScrollTimeout = setTimeout( function(){ oCombo.List.TableContainer.scrollTop = Position.cumulativeOffset( oSelectedRow )[1] - Position.cumulativeOffset( oCombo.List.TableContainer )[1] - 120 ; }, 100 );
            else if ( Position.cumulativeOffset( oSelectedRow )[1] - Position.cumulativeOffset( oCombo.List.TableContainer )[1] < oCombo.List.TableContainer.scrollTop + 10 )                
                oCombo.List.TableContainer.ScrollTimeout = setTimeout( function(){ oCombo.List.TableContainer.scrollTop = Position.cumulativeOffset( oSelectedRow )[1] - Position.cumulativeOffset( oCombo.List.TableContainer )[1] - 10 ; }, 100 );
              
        }
    }        
}

function cComboLoadData( oCombo , bKeepHidden ){
    var oCombo  = ( oCombo == null ? this.Combo : oCombo );
    oCombo.SearchCounter++;
    
    if( oCombo.Text.IsDirty ){        
        doCallBack( oCombo.Text.getAttribute( "name" ), "GETDATA", getCallBackUrl() + ( getCallBackUrl().indexOf( "?" ) == -1 ? "?" : "&" ) + "page_size=" + oCombo.getAttribute( "PageSize" ) + "&search_counter=" + oCombo.SearchCounter, new Array( "input", "select" ), "", null, null , true, true );            
        oCombo.Text.IsDirty = false;
    }
    if( bKeepHidden == null || bKeepHidden == false )
        oCombo.List.show();
        
    oCombo.DataPending = true;
}

function cComboDataCallbackJSON( oCombo, oJSON, iCounter ){
    
    if( oCombo.SearchCounter > iCounter ) return;
    oCombo.DataPending = false;
    
    try{
        oCombo.List.TableContainer.innerHTML = "";
    }catch(e){}
    
    oCombo.Data = oJSON;
    
    var sMarkup = "";
    var iSelectedIndex   = -1;
    var iAutoCompleteIndex   = -1;
    var bUniqueAutoComplete = true;
    
    for( var x = 0; x < oJSON.length; x++ ){
        
        var sArr  = oCombo.getAttribute( "CustomHTMLDefinition" ).split("${");
        for( var y = 1; y < sArr.length; y++ ){                    

            try{
                sArr[y-1]   += oJSON[x][ cInt(sArr[y].split("}")[0]) ];
            }catch(e){alert('Column # ' + sArr[y].split("}")[0] + ' not found in datasource. Please check your CustomHTMLDefinition');return;}
            
            sArr[y]     = sArr[y].split("}");
            sArr[y][0]  = "";
            sArr[y]     = sArr[y].join("");
        }
                                                
        //sMarkup += "<table><tr id='" + oCombo.getAttribute( "id" ) +  "_row_" + x + "'><td onclick=\"$( '" + oCombo.getAttribute( "id" ) + "' ).SetSelected( " + x + " );\" onmouseover=\"$( '" + oCombo.getAttribute( "id" ) + "' ).SetSelected( " + x + ", '" + replaceAll(oCombo.Data[x][ cInt( oCombo.getAttribute( "PKIndex" ) ) ], "'", "\\'" ) + "' );\" onmouseout=\"this.className='';\">" + sArr.join("") + "</td></tr></table>";
        sMarkup += "<div class='row' id='" + oCombo.getAttribute( "id" ) +  "_row_" + x + "' onclick=\"$( '" + oCombo.getAttribute( "id" ) + "' ).SetSelected( " + x + " );\" onmouseover=\"$( '" + oCombo.getAttribute( "id" ) + "' ).SetSelected( " + x + ", '" + replaceAll(oCombo.Data[x][ cInt( oCombo.getAttribute( "PKIndex" ) ) ], "'", "\\'" ) + "' );\" onmouseout=\"this.className='row';\">" + sArr.join("") + "</div>";
        
        if( oCombo.Value.value == oJSON[x][ cInt(oCombo.getAttribute( "PKIndex" )) ])  iSelectedIndex = x;
        
        if( oCombo.Text.value != "" && oJSON[x][ cInt(oCombo.getAttribute( "SearchIndex" )) ].toUpperCase().indexOf( oCombo.Text.value.toUpperCase() ) == 0 ){
            if( iAutoCompleteIndex == -1 ) 
                iAutoCompleteIndex = x;
            else
                bUniqueAutoComplete = false;
        }
    }

    oCombo.List.TableContainer.innerHTML = sMarkup;
    oCombo.Status.innerHTML = "Record 1 of " + oJSON.length;   
    
    oCombo.MoreButton.setAttribute( "disable_hover", "0" );
    oCombo.MoreButton.src   = oCombo.MoreButton.getAttribute( "normal" );
    
    if( oCombo.getAttribute( "AutoComplete" ) == "True" && iAutoCompleteIndex > -1 ){
        getSelectionRange( oCombo.Text ).text    = "";
        var iOldLength      = oCombo.Text.value.length;            
        oCombo.Text.value   = oJSON[iAutoCompleteIndex][ cInt(oCombo.getAttribute( "SearchIndex" )) ];            
        setSelectionRange( oCombo.Text, iOldLength, oCombo.Text.value.length );                                                    
    }
    
    if( oCombo.List.style.display == 'none' && iAutoCompleteIndex > -1 && ( bUniqueAutoComplete || oCombo.getAttribute( "ForceSelection") == "True" )){
        oCombo.SetSelected( iAutoCompleteIndex );                
        if( oCombo.List.style.display == 'none' && oCombo.getAttribute( "AutoPostBack" ) == "True" && oCombo.Value.value != oCombo.getAttribute( "current_value" ) && oCombo.Text.getAttribute( "typed_text" ) != null ) setTimeout("__doPostBack('" + oCombo.Text.getAttribute( "name" ) + "','CHANGED')", 0);                            
    }else if( iSelectedIndex > -1 ){
        oCombo.SetSelected( iSelectedIndex, oCombo.Value.value, oCombo.Text.value, '' );
    }
    
    
    if( oCombo.List.style.display == 'none' && iSelectedIndex < 0 && !( iAutoCompleteIndex > -1 && (bUniqueAutoComplete || oCombo.getAttribute( "ForceSelection") == "True" ))){
        oCombo.Text.value = "";
        oCombo.Value.value = "";     
    }
        
    if( oCombo.List.style.display == 'none' && oCombo.selectedEvent && oCombo.Value.value != oCombo.getAttribute( "current_value" ) && oCombo.getAttribute( "current_value" ) != null) oCombo.selectedEvent(oCombo,oCombo.Value.value,oCombo.Text.value);    
}

function cComboShowHideList(oCombo, event){
    if( oCombo == null ) oCombo = this.Combo;

    if( oCombo.List.style.display == 'none' )
        oCombo.List.show();
    else
        oCombo.List.hide(null,event);          
        
    if( event ) Event.stop(event);
    return false;  
}

function cComboListShow(){

    var oCombo  = this.Combo;
    if( oCombo.Text.IsDirty  ){
        clearTimeout( oCombo.DataTimer );        
        oCombo.Text.IsDirty = true;
        oCombo.DataTimer    = setTimeout( function(){ cComboLoadData( oCombo ); }, 400 );
    }else{    
        if( this.style.display != '' )
            oCombo.setAttribute( "current_value" , oCombo.Value.value );        	    
	           
        this.style.display  = '';		

	    if( oCombo.OverlappingDropdowns == null || oCombo.OverlappingDropdowns == undefined )
		    oCombo.OverlappingDropdowns	= hideElementsUnder("select", this, true );
    }    
}

function cComboListHide( oCombo, event ){            
    var oList   = ( oCombo != null ? oCombo.List : this );        
    var oCombo  = oList.Combo;
    
    try{        
        if( !event.keyCode ) event.keyCode = 0;
    }catch(e){}

    //if( event.keyCode < 8 && ( Event.element(event) == oList || Event.element(event) == oCombo.Button || Event.element(event) == oCombo.Text || Event.element(event) == oCombo.MoreButton || Event.element(event) == oCombo.MoreButton.parentElement || Event.element(event) == oCombo.Status )) return;        
            
    //|| Event.element(event) == oCombo.Button
    if( event.keyCode != 27 && event.keyCode != 13 && event.keyCode != 9 && ( Event.element(event) == oList || Event.element(event) == oCombo.Text || Event.element(event) == oCombo.MoreButton || Event.element(event) == oCombo.MoreButton.parentElement || Event.element(event) == oCombo.Status )) return;        

    
    if( oList.style.display == '' && oCombo.Value.value != oCombo.getAttribute( "current_value" )){
        if( oCombo.getAttribute( "AutoPostBack" ) == "True" )
            setTimeout("__doPostBack('" + oCombo.Text.getAttribute( "name" ) + "','CHANGED')", 0);                    
    }
    
    if( oCombo.getAttribute( "PartialMatch" ) == "True" ){
         oCombo.Value.value  = oCombo.Text.value;
    }else if( !oCombo.DataPending ){    
        oCombo.SetSelected( null );
        if( oCombo.selectedEvent && oCombo.Value.value != oCombo.getAttribute( "current_value" ) && oCombo.getAttribute( "current_value" ) != null) oCombo.selectedEvent(oCombo,oCombo.Value.value,oCombo.Text.value);
    }
    
    oList.style.display  = 'none';    
    oCombo.MoreButton.setAttribute( "disable_hover", "1" );
    oCombo.MoreButton.src   = oCombo.MoreButton.getAttribute( "loading" );    
        
    if( oCombo.OverlappingDropdowns != null ){
	    for( x = 0; x < oCombo.OverlappingDropdowns.length; x++ )
		    oCombo.OverlappingDropdowns[x].style.display = '';
		
	    oCombo.OverlappingDropdowns = null;

    }
    if( event && event.keyCode != 9) Event.stop(event);
    return false;
}

function cComboAddMultiItem( sID, sText ){
    if( sID == null ) sID = this.Value.value;
    if( sID == null || sID == "" ) return;    
    if( sText == null ){
    
        if( this.getAttribute( "selected_index" ) )
            sText = this.Data[ cInt(this.getAttribute( "selected_index" )) ][ cInt(this.getAttribute( "DisplayIndex" )) ];
        else
            sText = this.Text.value;
            
        this.Value.value = "";
        this.Text.value = "";
    }
    
    if( this.MultiValue.value.split( "," + sID + "," ).length > 1 ) return;
    if( !this.MultiValue.value.startsWith( "," ) ) this.MultiValue.value = "," + this.MultiValue.value;
    if( !this.MultiValue.value.endsWith( "," ) ) this.MultiValue.value = this.MultiValue.value + ",";
        
    this.MultiValue.value += sID + ","; 
    new Insertion.Before( this.Text, "<a tabindex=\"-1\" href=\"javascript:void(0);\" " + ( this.getAttribute( "DblClickEvent") != "" ? " ondblclick=\"" + replaceAll( this.getAttribute( "DblClickEvent"), "{id}", sID ) + "\"" : "" ) + " class=\"item\"><span><span><span><span>" + sText + "<span onclick=\"document.getElementById( '" + this.getAttribute("id") + "' ).RemoveMultiItem(this, " + sID + "); event.cancelBubble=true; return false;\" onmouseover=\"this.className='x_hover'\" onmouseout=\"this.className='x'\" class=\"x\">&nbsp;</span></span></span></span></span></a>" ); 
}

function cComboRemoveMultiItem( oElement, sID ){
    this.MultiValue.value = this.MultiValue.value.split( "," + sID + "," ).join(",");
    Element.Methods.remove(oElement.parentNode.parentNode.parentNode.parentNode.parentNode);    
}