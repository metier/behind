using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using jlib.functions;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Xml;
using System.Web.UI;
using System.Drawing.Imaging;

namespace jlib.components.button
{

	/// <summary>
	///		Summary description for webcontrol.
	/// </summary>
	public class button : System.Web.UI.WebControls.ImageButton
	{
		public enum ButtonModes {
			ImageButton,
			SubmitButton,
			ButtonButton,
			ResetButton,
			SubmitInput,
            ButtonInput,
            ResetInput
		}
		#region DECLARATIONS
        public bool m_bCloseWindowOnClick = false, m_bDisableDynamicEffects = false, m_bRendered = false;

        public string m_sText = ""; 
        public string m_sButtonType = "";
		public string m_sCustomIcon			= "";

		private ButtonModes m_oButtonMode = ButtonModes.ImageButton;
		private Page m_oWebPage = null;		
		private string m_sAjaxTargetControls = "", m_sAjaxDimControls = "", m_sAjaxPostControls = "";
		
		#endregion

		#region CONSTRUCTORS
		#endregion

		protected override void Render(System.Web.UI.HtmlTextWriter writer) {
            m_bRendered = true;
			if (ButtonMode == ButtonModes.ImageButton) {
				//Output Dynamic CSS
				
				if (this.CssClass == "") this.CssClass = "image_button";

				if (this.Disabled) {
					this.ImageUrl = jlib.helpers.general.getDynamicButton(this.ButtonName, "disabled");
					this.Style.Add("cursor", "normal");
				} else {
					this.ImageUrl = jlib.helpers.general.getDynamicButton(this.ButtonName, "normal");
				}
				this.AlternateText = Text;

				if (DisableDynamicEffects == false) {
					this.Attributes["onmouseover"] = String.Format("if(this.getAttribute('state')!='disabled'){{this.style.cursor='hand';this.src='{0}'; }}", jlib.helpers.general.getDynamicButton(this.ButtonName, "hover"));
					this.Attributes["onmouseout"] = String.Format("if(this.getAttribute('state')!='disabled')this.src='{0}';", jlib.helpers.general.getDynamicButton(this.ButtonName, "normal"));
					this.Attributes["onmousedown"] = String.Format("if(this.getAttribute('state')!='disabled')this.src='{0}';", jlib.helpers.general.getDynamicButton(this.ButtonName, "active"));
					this.Attributes["onmouseup"] = String.Format("if(this.getAttribute('state')!='disabled')this.src='{0}';", jlib.helpers.general.getDynamicButton(this.ButtonName, "normal"));
					this.Attributes["ondisabled"] = String.Format("{0}", jlib.helpers.general.getDynamicButton(this.ButtonName, "disabled"));

					if (convert.cStr(this.Attributes["onclick"]).IndexOf("if(this.getAttribute('state')=='disabled')") == -1)
						this.Attributes["onclick"] = "if(this.getAttribute('state')=='disabled'){return false;}" + (this.Attributes["onclick"] == null ? "" : "else{" + this.Attributes["onclick"] + "}");

					if (this.Visible)
						Page.RegisterStartupScript("required_scripts_" + this.ClientID, "<script>if(document.getElementById('" + this.ClientID + "')!=null)document.getElementById('" + this.ClientID + "').SetEnabled=function(){this.setAttribute('state','enabled');this.style.cursor='hand';this.onmouseout();};\nif(document.getElementById('" + this.ClientID + "')!= null ) document.getElementById('" + this.ClientID + "').SetDisabled	= function() { this.setAttribute( 'state', 'disabled'); this.style.cursor = 'normal'; this.src	= this.getAttribute( 'ondisabled' ); };</script>");
					//this.Attributes["SetEnabled"]	= "this.setAttribute( 'state' ) = 'enabled';";
					//this.Attributes["SetDisabled"]	= "this.setAttribute( 'state' ) = 'disabled';";
				}


				if (!System.IO.File.Exists(Page.Server.MapPath(this.ImageUrl))) {

					string[] sModeArray = new string[] { "normal", "active", "focused", "hover" };
					for (int x = 0; x < sModeArray.Length; x++) {

						System.Drawing.Image oLeft = System.Drawing.Image.FromFile(Page.Server.MapPath(jlib.helpers.general.getLibraryButton("template/" + sModeArray[x], "left")));
						System.Drawing.Image oMiddle = System.Drawing.Image.FromFile(Page.Server.MapPath(jlib.helpers.general.getLibraryButton("template/" + sModeArray[x], "middle")));
						System.Drawing.Image oRight = System.Drawing.Image.FromFile(Page.Server.MapPath(jlib.helpers.general.getLibraryButton("template/" + sModeArray[x], "right")));
						System.Drawing.Image oIcon = null;

						if (this.ButtonType != "")
							oIcon = System.Drawing.Image.FromFile(Page.Server.MapPath(jlib.helpers.general.getLibraryIcon("16x16", this.ButtonType)));
						else if (this.CustomIcon != "")
							oIcon = jlib.helpers.graphics.getImageObject(this.CustomIcon);


						System.Drawing.Image oOutput = System.Drawing.Image.FromFile(Page.Server.MapPath(helpers.general.getLibraryButton("template/" + sModeArray[x], "middle")));

						Graphics oGraphics = null;

						try {
							oGraphics = Graphics.FromImage(oOutput);
						} catch (Exception) {
							Bitmap oBitmapTemp = new Bitmap(oOutput.Width, oOutput.Height);
							oGraphics = Graphics.FromImage(oBitmapTemp);
							oOutput = oBitmapTemp;
						}

						Font oFont = new Font("Tahoma", 8);

						Point oDimensions = button.MeasureDisplayStringWidth(oGraphics, Text, oFont);
						oDimensions.X = oDimensions.X + 7 + 8 + ((this.ButtonType != "") ? (16 + 3) : 0);
						Bitmap oBitmap = new Bitmap(oOutput, oDimensions.X, oMiddle.Height);
						oGraphics.Dispose();
						oGraphics = Graphics.FromImage(oBitmap);

						for (int y = (oLeft.Width / oMiddle.Width); (y * oMiddle.Width) < oDimensions.X - (oRight.Width / oMiddle.Width); y++)
							oGraphics.DrawImage(oMiddle, y * oMiddle.Width, 0);

						oGraphics.DrawImage(oLeft, 0, 0);
						oGraphics.DrawImage(oRight, oDimensions.X - oRight.Width, 0);

						if (oIcon != null)
							oGraphics.DrawImage(oIcon, 7, 4);

						oGraphics.DrawString(Text, oFont, System.Drawing.Brushes.Black, 7 + ((oIcon != null) ? (16 + 3) : 0), 5);

                        string FileName = Page.Server.MapPath(jlib.helpers.general.getDynamicButton(this.ButtonName, sModeArray[x]));
                        io.createDirectory(FileName, true);
                        oBitmap.Save(FileName);

						//draw the disabled state
						if (sModeArray[x] == "normal") {
							oGraphics.FillRectangle(new SolidBrush(Color.FromArgb(127, 255, 255, 255)), 1, 1, oBitmap.Width - 2, oBitmap.Height - 1);
							oBitmap.Save(Page.Server.MapPath(jlib.helpers.general.getDynamicButton(this.ButtonName, "disabled")));
						}

						oGraphics.Dispose();
						oBitmap.Dispose();
						oLeft.Dispose();
						oRight.Dispose();
						oMiddle.Dispose();

						if (oIcon != null)
							oIcon.Dispose();

						oOutput.Dispose();

					}
				}				
				base.Render(writer);
			} else if (ButtonMode == ButtonModes.ButtonButton || ButtonMode == ButtonModes.SubmitButton || ButtonMode == ButtonModes.ResetButton) {
                writer.Write("<button value=\"1\" type=\"" + (ButtonMode == ButtonModes.ButtonButton ? "button" : ButtonMode == ButtonModes.SubmitButton ? "submit" : "reset") + "\" name=\"" + this.UniqueID + "\" id=\"" + this.ClientID + "\"" + (this.Style == null || this.Style.Count == 0 ? "" : " style=\"" + this.Style.Value + "\"") + (this.CssClass == "" ? "" : " class=\"" + this.CssClass + "\"") + (this.OnClick == "" ? "" : " onclick=\"" + this.OnClick + "\"") + ">" + Text + "</button>");
            } else if (ButtonMode == ButtonModes.SubmitInput || ButtonMode == ButtonModes.ResetInput || ButtonMode == ButtonModes.ButtonInput) {
                writer.Write("<input type=\"" + (ButtonMode == ButtonModes.ButtonInput ? "button" : ButtonMode == ButtonModes.SubmitInput ? "submit" : "reset") + "\" name=\"" + this.UniqueID + "\" id=\"" + this.ClientID + "\"" + (this.Style == null || this.Style.Count == 0 ? "" : " style=\"" + this.Style.Value + "\"") + (this.CssClass == "" ? "" : " class=\"" + this.CssClass + "\"") + (this.OnClick == "" ? "" : " onclick=\"" + this.OnClick + "\"") + " value='" + Text + "' />");
            }
		}

		static public Point MeasureDisplayStringWidth( Graphics oGraphics, string sText, Font oFont ) {

			System.Drawing.StringFormat oFormat  = new System.Drawing.StringFormat();
			System.Drawing.RectangleF   oRect    = new System.Drawing.RectangleF(0, 0, 1000, 1000);
			
			System.Drawing.CharacterRange[] oRanges  = { new System.Drawing.CharacterRange(0, sText.Length) };
			System.Drawing.Region[]         oRegions = new System.Drawing.Region[1];

			oFormat.SetMeasurableCharacterRanges( oRanges );

			oRegions = oGraphics.MeasureCharacterRanges( sText, oFont, oRect, oFormat );
			oRect    = oRegions[0].GetBounds( oGraphics );
			
			return new Point( (int)(oRect.Right + 1.0f)  , (int)(oRect.Bottom + 1.0f)  );
			
		}



		#region PROPERTIES
        public string TranslationKey { get; set; }

        public bool Rendered {
            get {
                return m_bRendered;
            }
        }
		public new string OnClick {
			get {
				return this.OnClientClick;
			}
			set {
				this.OnClientClick = value;
			}
		}

		public ButtonModes ButtonMode {
			get {
				return m_oButtonMode;
			}
			set {
				m_oButtonMode = value;
			}
		}

		public bool Disabled{
			get{
				return convert.cBool( this.Attributes["state"] == "disabled" );
			}
			set{
				if ( value )
					this.Attributes["state"]		= "disabled";
				else
					this.Attributes["state"]		= "";
			}
		}


		public string ButtonType {
			get {
				return m_sButtonType;
			}
			set {
				m_sButtonType = value;
			}
		}

		
		public string CustomIcon {
			get {
				return m_sCustomIcon;
			}
			set {
				m_sCustomIcon = value;
			}
		}

		public bool CloseWindowOnClick
		{
			get
			{
				return m_bCloseWindowOnClick;
			}
			set
			{
				m_bCloseWindowOnClick = value;
			}
		}

		public bool DisableDynamicEffects{
			get {
				return m_bDisableDynamicEffects;
			}
			set {
				m_bDisableDynamicEffects = value;
			}
		}
        public new string Text {
            get {
                return m_sText;
            }
            set {
                m_sText = value;
            }
        }
		public string ButtonName
		{
			get
			{
                string sName = "button_"+this.ID.GetHashCode();
				
				if ( this.CustomIcon != "" )
					sName		+= this.CustomIcon.GetHashCode();

				return sName;
			}
		}

		public bool IsClicked {
			get {
                if (Page!=null && Page.IsPostBack) {
					if (convert.cStr(Page.Request.Form[this.UniqueID + ".x"]) != "" ) return true;
                    if ((Page as jlib.components.webpage) != null && (Page as jlib.components.webpage).EventTarget == this.UniqueID) return true;
					if (convert.cStr(Page.Request.Form[this.UniqueID]) != "") return true;
				}
				return false;
			}
		}
		
		public string AjaxTargetControls {
			get {
				return m_sAjaxTargetControls;
			}
			set {
				m_sAjaxTargetControls = value;
			}
		}
		public string AjaxDimControls {
			get {
				return m_sAjaxDimControls;
			}
			set {
				m_sAjaxDimControls = value;
			}
		}
		public string AjaxPostControls {
			get {
				return m_sAjaxPostControls;
			}
			set {
				m_sAjaxPostControls = value;
			}
		}


		public Page WebPage {
			get {
				return (m_oWebPage == null ? Page : m_oWebPage);
			}
			set {
				m_oWebPage = value;
			}
		}

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
			this.Page.LoadComplete += new EventHandler(Page_LoadComplete);

            if (TranslationKey != "") {
                webpage oPage = (Page as webpage);
                if (oPage != null) this.Text = helpers.translation.translate(oPage.Language, this.Text, Page, TranslationKey).Str();                
            }            
		}

		void Page_LoadComplete(object sender, EventArgs e) {
            if ((this.ButtonMode == ButtonModes.ButtonButton || this.ButtonMode == ButtonModes.SubmitButton || this.ButtonMode == ButtonModes.ResetButton || this.ButtonMode == ButtonModes.SubmitInput) && this.IsClicked) base.OnClick(new ImageClickEventArgs(0, 0));
		}

		protected override void OnLoad(EventArgs e) {			
			base.OnLoad(e);			
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			//this.PreRender += new System.EventHandler(this.Button_PreRender);			
			this.Click += new System.Web.UI.ImageClickEventHandler(button_Click);
			
		}
		#endregion

		#region METHODS
		private void button_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(this.CloseWindowOnClick)
			{
				Page.Response.Write("<script>self.close();</script>");
			}
		}

		public static bool isClicked(button oButton) {
			
			if(oButton.ButtonMode == ButtonModes.ImageButton )
				return HttpContext.Current.Request.Form[oButton.UniqueID + ".y"] != null;
			
			return HttpContext.Current.Request.Form[oButton.UniqueID] != null;
		}

		protected override void RaisePostBackEvent(string eventArgument) 
		{
			try 
			{
				base.RaisePostBackEvent(eventArgument);
            } 
			catch (Exception e) 
			{
				throw;
				//string s = e.Message; 
			}
		}
		#endregion

		
	}
}

