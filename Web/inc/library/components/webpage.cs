using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Runtime.Serialization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using jlib.db;
using jlib.functions;
using System.Linq;

namespace jlib.components {
	/// <summary>
	/// Summary description for webpage.
	/// </summary>
    public class webpage : System.Web.UI.Page{//, IWebPage {

        public delegate void OnLanguageNeeded(object sender, out string sLanguage);
        public event OnLanguageNeeded LanguageNeeded;
        public delegate void OnLanguageSiteAndCompanyNeeded(object sender, out string sLanguage, out string sSiteId, out string sCompanyId);
        public event OnLanguageSiteAndCompanyNeeded LanguageCompanyAndSiteNeeded;

        protected System.Data.DataRow m_oCurrentUser = null;
		private int m_iSessionID = -1;

		protected System.Web.UI.WebControls.Label m_oErrorLabel = null;
        private Hashtable m_oScripts = new Hashtable();
        private Dictionary<string, string> m_oQueryString = null;
		
        private string m_sTranslationLanguage = null, m_sTranslationSiteId, m_sTranslationCompanyId, m_sEventTarget, m_sEventArgument, m_sIP, m_sHost, m_sUserAgent, m_sReferrerSearchEngine, m_sReferrerSearchEngineWords = null, m_sMetaKeywords, m_sMetaDescription;
        private Control m_oEventTargetControl = null;

        /// <summary>
        /// Constructor
        /// </summary>

        public webpage() {
            EnableEventValidation = false;
            this.AjaxEnabled = false;
            this.PostbackEnabled = true;
        }
		

		/* Properties */

        public string Language {
            get {
                if (m_sTranslationLanguage == null) {
                    string LanguageId = "", SiteId="", CompanyId="";
                    if (LanguageNeeded != null) LanguageNeeded(this, out LanguageId);
                    if (LanguageCompanyAndSiteNeeded != null) LanguageCompanyAndSiteNeeded(this, out LanguageId, out SiteId, out CompanyId);
                    m_sTranslationLanguage = LanguageId;
                    m_sTranslationSiteId = SiteId;
                    m_sTranslationCompanyId = CompanyId;
                }
                return m_sTranslationLanguage;
            }
            set {
                m_sTranslationLanguage = value;
            }
        }
        public string TranslationSiteId {
            get
            {
                string Temp = Language;
                return m_sTranslationSiteId;
            }
        }
        public string TranslationCompanyId
        {
            get
            {
                string Temp = Language;
                return m_sTranslationCompanyId;
            }
        }

        /// <summary>
        /// Master Page
        /// </summary>
        public jlib.components.masterpage MasterPage {
            get {
                if (this._MasterPage == null)
                    this._MasterPage = Page.Master as jlib.components.masterpage;

                return this._MasterPage;
            }
        }
        private jlib.components.masterpage _MasterPage = null;

        /// <summary>
        /// Page has Ajax Enabled
        /// </summary>
        public bool AjaxEnabled { get; private set; }

        /// <summary>
        /// Page allows Postback
        /// </summary>
        public bool PostbackEnabled { get; private set; }


		public string IP {
			get {

				if (m_sIP == null) {
					if (convert.cStr(Request.ServerVariables["HTTP_X_FORWARDED_FOR"]) != "") m_sIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
					else m_sIP = convert.cStr(Request.ServerVariables["REMOTE_ADDR"]);
				}
				return m_sIP;
			}
		}


		public string Host {
			get {
				if (m_sHost == null) {
					if (convert.cStr(Request.ServerVariables["HTTP_X_FORWARDED_HOST"]) != "") m_sHost = Request.ServerVariables["HTTP_X_FORWARDED_HOST"];
					else m_sHost = Request.Url.Host;
				}
				return m_sHost;
			}
		}
		

		public string UserAgent {
			get {

				if (m_sUserAgent == null) {
					if (Request.Cookies["proxy_user_agent"] != null && Request.Cookies["proxy_user_agent"].Value != "") m_sUserAgent = Request.Cookies["proxy_user_agent"].Value;
					else m_sUserAgent = convert.cStr(Request.ServerVariables["HTTP_USER_AGENT"]);
				}
				return m_sUserAgent;
			}
		}

		public string Referrer {
			get {

				if (convert.cStr(Request.QueryString["referrer"]) != "")
					return convert.cStr(Request.QueryString["referrer"]);

				return convert.cStr(Request.ServerVariables["HTTP_REFERER"]);
			}
		}


		public string ReferrerSearchEngine {
			get {
				if (m_sReferrerSearchEngine == null)
					m_sReferrerSearchEngine = jlib.net.HTTP.getReferrerSearchEngine(Referrer);

				return m_sReferrerSearchEngine;
			}
		}

		public string ReferrerSearchEngineWords {
			get {
				if (m_sReferrerSearchEngineWords == null)
					m_sReferrerSearchEngineWords = jlib.net.HTTP.getReferrerSearchEngineWords(Referrer);

				return m_sReferrerSearchEngineWords;
			}
		}

		public string EventTarget {
			get {
				if( m_sEventTarget == null ){
					if (Request.QueryString["__EVENTTARGET"] != null)
						m_sEventTarget = convert.cStr(Request.QueryString["__EVENTTARGET"]);
					else
						m_sEventTarget = convert.cStr(Request.Form["__EVENTTARGET"]);			
				}
				return m_sEventTarget;
			}
			set{
				m_sEventTarget = value;
                m_oEventTargetControl = null;
			}
		}
        public Control EventTargetControl {
            get {
                if (m_oEventTargetControl == null){
                if (EventTarget == "") return null;
                m_oEventTargetControl = jlib.helpers.control.findControlByUniqueId(Page, Page.Controls, parse.replaceAll(EventTarget, "_", "$"));
                }
                return m_oEventTargetControl;
            }
        }

		public string EventArgument {
			get {
				if (m_sEventArgument == null) {
					if (Request.QueryString["__EVENTARGUMENT"] != null)
						m_sEventArgument = Request.QueryString["__EVENTARGUMENT"];
					else
						m_sEventArgument = Request.Form["__EVENTARGUMENT"];
				}

				return m_sEventArgument;
			}
			set{
				m_sEventArgument = value;
			}
		}		

		
        public bool IsAjaxPostback {
            get {
                return convert.cBool(this.Request["__AJAXCALL"]) || this.EventArgument == "AJAXLOADCONTROL";
            }
        }
        
		public int SessionID {
			get {
				return m_iSessionID;
			}
		}

        /// <summary>
        /// Determines if the request is a Search Engine Crawler
        /// </summary>
        public bool IsCrawler { get { return Request.Browser.Crawler; } }


		public DataRow CurrentUser {
			get {

				if (m_oCurrentUser == null) {
                    ado_helper oData = new ado_helper(ConfigurationManager.AppSettings["sql.dsn.login"]);
					if (convert.cInt(Session["user.id"]) != 0) {
						m_oCurrentUser = sqlbuilder.getDataRow(oData, "d_users", "id", convert.cInt(Session["user.id"]));
						return m_oCurrentUser;
					}
					string sSessionID = "";
					string sUserGUID = "";
					
					DataTable oDT = null;

					if (jlib.net.HTTP.getCookieValue(Request.Cookies, "common.session_id1", "" ) != "" && jlib.net.HTTP.getCookieValue( Request.Cookies,"common.session_guid1", "" ) != "") {
						sSessionID = jlib.net.HTTP.getCookieValue(Request.Cookies,"common.session_id1","");
						sUserGUID = jlib.net.HTTP.getCookieValue(Request.Cookies,"common.session_guid1", "");
                        oDT = sqlbuilder.executeSelect(oData, "d_sessions", "id" , convert.cInt(sSessionID), new condition("last_date > DateAdd( minute, -" + convert.cInt(ConfigurationManager.AppSettings["session.timeout"], 20) + ", GetDate() )"), new condition( "logout_date is null"));
					}
                    //if ((oDT == null || oDT.Rows.Count != 1) && jlib.net.HTTP.getCookieValue(Response.Cookies["common.session_id1"], "") != "" && jlib.net.HTTP.getCookieValue(Response.Cookies["common.session_guid1"], "") != "") {
                    //    sSessionID = jlib.net.HTTP.getCookieValue(Response.Cookies["common.session_id1"],"");
                    //    sUserGUID = jlib.net.HTTP.getCookieValue( Response.Cookies["common.session_guid1"],"");
                    //    oDT = oData.Execute_SQL("select * from d_sessions where id = " + convert.cInt(sSessionID) + " and last_date > DateAdd( minute, -" + convert.cInt(ConfigurationManager.AppSettings["session.timeout"],20) + ", GetDate() ) and logout_date is null;");
                    //}


					if ( oDT != null ) {

                        if (oDT.Rows.Count == 1 && parse.replaceAll(oDT.Rows[0]["GUID"], "{", "", "}", "").ToLower() == parse.replaceAll(sUserGUID, "{", "", "}", "").ToLower()) {
                            Session["db.session.id"] = oDT.Rows[0]["id"];
							m_iSessionID = convert.cInt(sSessionID);
							oData.Execute_SQL("update d_sessions set last_date = GetDate() where id = " + convert.cInt(sSessionID));

							oDT = sqlbuilder.executeSelect(oData, "d_users", "id", oDT.Rows[0]["user_id"]);
							if (oDT.Rows.Count > 0) {
								m_oCurrentUser = oDT.Rows[0];
                                Session["user.id"] = oDT.Rows[0]["id"];
							}
						}

						if (m_oCurrentUser == null) {
							if (jlib.net.HTTP.getCookieValue(Response.Cookies,"common.session_id1", "") != "") Response.Cookies["common.session_id1"].Value = "";							
						}
						
					}				
				}
				return m_oCurrentUser;
			}
			set {
				m_oCurrentUser = value;
			}
		}
		

        public int CurrentUserID {
            get {
                if ( this.CurrentUser == null )
                    return 0;

                return convert.cInt(CurrentUser["id"]);
            }            
        }

        /// <summary>
        /// Access to the Generic Error Label Control
        /// </summary>
        public System.Web.UI.WebControls.Label ErrorLabel { get; set; }

        /// <summary>
        /// Error Messaging Text
        /// </summary>
        public string ErrorText { get; set; }

        

        public new string MetaKeywords {
            get {
                return m_sMetaKeywords;
            }
            set {
                m_sMetaKeywords = Regex.Replace(value, "\\s+", " ");
            }
        }
        public new string MetaDescription {
            get {
                return m_sMetaDescription;
            }
            set {
                m_sMetaDescription = Regex.Replace(value, "\\s+", " ");
            }
        }    

        public Dictionary<string,string> QueryStringTable {
            get {
                if (m_oQueryString == null) {
                    m_oQueryString = new Dictionary<string, string>(Request.QueryString.Count);
                    //Loop through QueryString Adding Safe Values
                    for (int x = 0; x < Request.QueryString.Count; x++) {
                        try {
                            m_oQueryString.Add(Request.QueryString.GetKey(x), parse.replaceAll(convert.cStr(Request.QueryString[x]), "insert ", "", "select ", "", "delete ", "", "update ", ""));
                        } catch (Exception) { }
                    }
                }
                return m_oQueryString;
            }
        }

        /// <summary>
        /// Gets a Querystring Value from the QueryString Table
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public string QueryString(string sKey) {
            return QueryString(sKey, true);
        }

        /// <summary>
        /// Strip certain items from the querystring and return the resulting QueryString
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public string QueryStringStripKeys(params string[] Keys) {
            //Declare Return Value
            string queryString = "";

            //Build QueryString
            foreach (string key in this.QueryStringTable.Keys) {
                if (!Keys.Contains(key)) {
                    queryString += String.Format("{0}={1}&", key, this.QueryStringTable[key]);
                }
            }

            //Return Value
            return parse.stripEndingCharacter(queryString, "&");
        }

        /// <summary>
        /// Gets a Querystring Value from the QueryString Table
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public int QueryStringInt(string Key) {
            return convert.cInt(QueryString(Key, false));
        }

        /// <summary>
        /// Gets a Querystring Value from the QueryString Table
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public double QueryStringDbl(string Key) {
            return convert.cDbl(QueryString(Key, false));
        }

        /// <summary>
        /// Gets a Querystring Value from the QueryString Table
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public long QueryStringLong(string Key) {
            return convert.cLong(QueryString(Key, false));
        }

        /// <summary>
        /// Gets a Querystring Value from the QueryString Table
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="AllowMultipleValues"></param>
        /// <returns></returns>
        public string QueryString(string Key, bool AllowMultipleValues) {
            //If key is not provided, return full querystring value
            if (Key == null || Key == "") {
                string str = "";
                foreach (string itemKey in this.QueryStringTable.Keys)
                    str += (str == "" ? "" : "&") + itemKey + "=" + this.QueryStringTable[itemKey];

                return str;
            }

            //Get Value by key
            string value = "";
            if (this.QueryStringTable.ContainsKey(Key)) value = convert.cStr(this.QueryStringTable[Key]);

            //If multiple values, return first
            if (value.IndexOf(",") > -1 && !AllowMultipleValues)
                value = parse.split(value, ",")[0];

            //Return Value
            return value;
        }


		
		public bool IsPostBackA {
			get {
				return Request.Form.Count > 0;
			}
		}

        #region methods

		public override void VerifyRenderingInServerForm(Control control) {
			//base.VerifyRenderingInServerForm(control);
		}

		protected override void Render(HtmlTextWriter writer) {
            if (this.Header != null) {
                if (convert.cStr(MetaDescription) != "") this.Header.Controls.Add(convert.cLiteral("<meta name=\"description\" content=\"" + parse.replaceAll(MetaDescription, "\"", "'") + "\" />"));
                if (convert.cStr(MetaKeywords) != "") this.Header.Controls.Add(convert.cLiteral("<meta name=\"keywords\" content=\"" + parse.replaceAll(MetaKeywords, "\"", "'") + "\" />"));
            }

            //Check to see if ajax request
            if (this.EventArgument == "AJAXLOADCONTROL") {
                bool bEscapeTextarea=convert.cStr(Request.ContentType).IndexOf("multipart/form-data")>-1;
                if (bEscapeTextarea) Response.Write("<textarea>");
                List<Control> oControls = jlib.helpers.control.findControlById(this, this.Controls, this.EventTarget);
                if (oControls!=null && oControls.Count > 0) {
                    List<iControl> oIControls = jlib.helpers.control.getDynamicControls(oControls[0] as System.Web.UI.Control, null);
                    for (int x = 0; x < oIControls.Count; x++) {
                        Response.Write("$(\"#" + oIControls[x].ClientID + "\")." + (oIControls[x] as jlib.components.label != null ? "html" : "val") + "(\"" + parse.replaceAll(oIControls[x].getValue(), "&quot;", "\"", "textarea", (bEscapeTextarea ? "JVTtextarea" : "textarea"), "\"", "\\\"", "\r", "", "\n", "\\n") + "\");\n");
                    }
                }
                oControls = jlib.helpers.control.findControlType(this, this.Controls, typeof(jlib.controls.ajaxpane.index), true);
                for (int x = 0; x < oControls.Count; x++) {
                    string sHTML=(oControls[x] as jlib.controls.ajaxpane.index).RenderContent();

                    Response.Write("$(\"#" + (oControls[x] as jlib.controls.ajaxpane.index).ContentContainer.ClientID + "\").html(\"" + parse.replaceAll(parse.stripSCRIPT(sHTML), "&quot;", "\"", "textarea", (bEscapeTextarea ? "JVTtextarea" : "textarea"), "\"", "\\\"", "\r", "", "\n", "\\n") + "\");\n");
                    sHTML=parse.extractSCRIPT(sHTML, true);
                    if (sHTML != "") Response.Write(sHTML);
                }
                if (bEscapeTextarea) Response.Write("</textarea>");
            } else {

                
                writer = new htmltextwriter(writer.InnerWriter);
                if (this.QueryString("AJAXFORM") == "true") {
                    htmltextwriter writer1 = new htmltextwriter(new StringWriter());
                    this.Form.RenderControl(writer1);
                    writer.Write(parse.inner_substring(writer1.InnerWriter.ToString(), "<form ", ">", "</form>", null));
                    //for (int x = 0; x < this.Form.Controls.Count; x++)
                    //    this.Form.Controls[x].RenderControl(writer);
                    //string s = parse.inner_substring(writer.ToString(), "<form ", ">", "</form>", null);
                    
                } else {
                    base.Render(writer);
                }
            }
		}
        public int populateValues( string sTableName, DataRow oRow ) {
            return helpers.control.populateValues(Page, sTableName, oRow as jlib.db.IDataRow);
		}
        public int populateValues(string sTableName, jlib.db.IDataRow oRow) {
            return helpers.control.populateValues(Page, sTableName, oRow);
        }
        public int populateValues(string sPrefix, helpers.structures.collection oData) {
            return helpers.control.populateValues(Page, sPrefix, oData);
        }
		
		public DataRow collectValues( string sTableName, DataRow oRow ){
            return helpers.control.collectValues( Page, sTableName, oRow as jlib.db.IDataRow ) as DataRow;
		}
        public IDataRow collectValues(string sTableName, IDataRow oRow) {
            return helpers.control.collectValues(Page, sTableName, oRow);
        }


        public bool validateForm( ) {
            return validateForm( Page );
        }
        public bool validateForm( Control oParentControl ) {

            ErrorText = helpers.control.validateForm( oParentControl );
            
            if ( ErrorLabel != null ) {
                if ( ErrorText == "" ){
                    ErrorLabel.Visible = false;
                }else {                    
                    ErrorLabel.Visible = true;
                    ErrorLabel.Text =  ErrorText+"<br style='clear:both' />";
                }
            }
			return ( ErrorText == "" );
        }

        public void logout() {
            logout(true);
        }
        public void logout(bool bAbandonSession) {
            logout(bAbandonSession, ConfigurationManager.AppSettings["sql.dsn.login"]);
        }
        public void logout(bool bAbandonSession, string sDSN) {
            if( this.CurrentUserID != 0 )
                sqlbuilder.getDataTable(new ado_helper(sDSN), "update d_login_attempts set last_date=GetDate(), logout_date=GetDate() where id in (select top 1 ID from d_login_attempts WHERE logout_date IS NULL AND session_id = '" + Session.SessionID + "' AND user_id = " + this.CurrentUserID + " ORDER BY ID DESC); update d_sessions set logout_date=getdate() where id=" + convert.cInt(Session["db.session.id"], jlib.net.HTTP.getCookieValue( Request.Cookies,"common.session_id1","")));

			//if ( this.CurrentUser != null ) {
			//    ado_helper oData = new ado_helper();
			//    oData.Execute_SQL( "delete from d_sessions where id = " + this.SessionID );
			//}			
			
            for (int x = 0; x < Response.Cookies.Count; x++)
                if (Response.Cookies[x].Name.IndexOf("common") > -1) {
                    Response.Cookies.Remove(Response.Cookies[x].Name);
                    x--;
                }

            for( int x=0;x<Request.Cookies.Count;x++)
                if (Request.Cookies[x].Name.IndexOf("common") > -1) {
                    //Response.Cookies[Request.Cookies[x].Name].Expires = DateTime.Now.AddYears(-1);
                    Request.Cookies[x].Expires = DateTime.Now.AddYears(-1);
                    Request.Cookies[x].Value = "";
                    Request.Cookies[x].Domain = (parse.split(Host, ".").Length > 2 ? parse.replaceAll(Host, parse.split(Host, ".")[0] + ".", "") : Host);
                    if(Request.Cookies[x].Values!=null)Request.Cookies[x].Values.Clear();
                    Response.Cookies.Add(Request.Cookies[x]);
                    Request.Cookies.Remove(Request.Cookies[x].Name);
                    x--;
                }
            
            if(bAbandonSession)Session.Abandon();
            Session["user.id"] = 0;
            Session["db.session.id"] = 0;
            this.CurrentUser = null;			
        }

		public bool login(string sUserName, string sPassword) {
			return login(sUserName, sPassword, true);
		}

        public bool login( string sUserName, string sPassword, bool bEnableCookies ) {
            return login(sUserName, sPassword, bEnableCookies, ConfigurationManager.AppSettings["sql.dsn.login"]);
        }
        public bool login(string sUserName, string sPassword, bool bEnableCookies, string sDSN) {

            logout(false);

            ado_helper oData = new ado_helper(sDSN);
			sUserName = ado_helper.PrepareDB(sUserName.Trim());
			sPassword = ado_helper.PrepareDB(sPassword.Trim());

            //if ( sUserName.Length > 50 ) sUserName = sUserName.Substring( 0, 50 );
            //if ( sPassword.Length > 20 ) sPassword = sPassword.Substring( 0, 20 );
			if (sUserName == "" || sPassword == "") return false;

            DataTable oDT = oData.Execute_SQL("select * from d_users where ( username = '" + sUserName + "' or email = '" + sUserName + "' ) and deleted = 0 and password = '" + sPassword + "' order by case when username = '" + sUserName + "' THEN 0 ELSE 1 END;");
            if ( oDT.Rows.Count == 0 )
                oDT = oData.Execute_SQL("select * from d_users where ( username = '" + sUserName + "' or email = '" + sUserName + "' ) and deleted = 0  and ID in ( select user_id from d_users_temp_passwords where DateAdd( day, 1, posted_date ) > GetDate() AND password = '" + sPassword + "') order by case when username = '" + sUserName + "' THEN 0 ELSE 1 END;");
            
            if ( oDT.Rows.Count == 0 ) {
                sqlbuilder.executeInsert(oData, "d_login_attempts", "ip", this.IP, "username", sUserName, "password", sPassword, "session_id", Session.SessionID, "url", Request.Url.AbsoluteUri);
                return false;
            } else {

                Session["logging_user_id"] = oDT.Rows[0]["id"];
                DataTable oSession = sqlbuilder.executeInsert( oData, "d_sessions", "user_id", oDT.Rows[0]["id"]);				                

                //We have to re-query so the GUID will refresh
                oSession = sqlbuilder.executeSelect(oData, "d_sessions", "id" , oSession.Rows[0]["id"] );
                //Request.Cookies.Clear();
                //Response.Cookies.Clear();
				if (bEnableCookies) {
                    string sHost = (parse.split(Host, ".").Length < 3 || (parse.split(Host, ".").Length == 4 && convert.isNumeric(parse.split(Host, ".")[0]) && convert.isNumeric(parse.split(Host, ".")[1]) && convert.isNumeric(parse.split(Host, ".")[2]) && convert.isNumeric(parse.split(Host, ".")[3])) ? Host : parse.replaceAll(Host, parse.split(Host, ".")[0] + ".", ""));
                    Response.Cookies.Clear();
					Response.Cookies["common.session_id1"].Value = convert.cStr(oSession.Rows[0]["id"]);
					//Response.Cookies["common.session_id1"].Expires = System.DateTime.Now.AddYears(10);
                    if(!sHost.Equals("localhost", StringComparison.CurrentCultureIgnoreCase))
						Response.Cookies["common.session_id1"].Domain = sHost;
				
					Response.Cookies["common.session_guid1"].Value = convert.cStr(oSession.Rows[0]["GUID"]);
					//Response.Cookies["common.session_guid1"].Expires = System.DateTime.Now.AddYears(10);
                    Response.Cookies["common.session_guid1"].Domain = Response.Cookies["common.session_id1"].Domain;
				}
				Session["user.id"] = oDT.Rows[0]["id"];
                Session["db.session.id"] = oSession.Rows[0]["id"];
                sqlbuilder.executeInsert(oData, "d_login_attempts", "ip", this.IP, "username", sUserName, "password", sPassword, "session_id", Session.SessionID, "user_id", oDT.Rows[0]["id"], "url", convert.cEllipsis(Request.Url.AbsoluteUri,512,false));
                return true;
            }
        }

        public void registerScript(System.Type oType, string sKey, string sScript) {
            if (sScript.IndexOf("<script", StringComparison.CurrentCultureIgnoreCase) > -1)
                sScript = parse.inner_substring_i(sScript, "<script", ">", "</script>", null);
            Page.ClientScript.RegisterStartupScript(oType, sKey, sScript, true);
            m_oScripts.Add(oType.FullName + "_" + sKey, sScript);
        }

        public void Page_PreInit(object sender, EventArgs e) {
            if (this.Master as masterpage != null) (this.Master as masterpage).Page_PreInit(sender, e);
        }
        public void Page_Init( object sender, EventArgs e ) {
            //this line is necessary to force the MagicAjax library to use our custom version of the js file
			//MagicAjax.MagicAjaxContext.Current.Configuration.DisableMagic = true;
			//MagicAjax.MagicAjaxContext.Current.Configuration.ScriptPath = "/inc/js";
            //MagicAjax.MagicAjaxContext.Current.Configuration.Tracing = true;
			
			//if( jlib.net.HTTP.isSearchEngineSpider( Request.UserAgent ) 
			//|| !Request.Browser.SupportsXmlHttp
            
            this.AjaxEnabled = convert.cBool(ConfigurationManager.AppSettings["common.ajax"]);
            if (this.IsCrawler || Request.Browser.EcmaScriptVersion.Major < 1) this.AjaxEnabled = false;
            if (this.IsCrawler || Request.Browser.EcmaScriptVersion.Major < 1) this.PostbackEnabled = false;
            if (System.Configuration.ConfigurationManager.AppSettings["maintenance.mode"].Str().Equals("true", StringComparison.CurrentCultureIgnoreCase)) Server.Transfer("/inc/pages/maintenance.aspx");
            

        }



        public virtual void RaiseAjaxCallStartEvent() { }
        public virtual void RaiseAjaxCallEndEvent() { }

        #endregion

          #region PersistentStatePage http://www.codeproject.com/aspnet/persistentstatepage.asp

        /// <summary>
		/// The persisted PageState.
		/// This is non-null if a postback is being emulated from the persisted PageState.
		/// </summary>
		private PageState pageState=null;

		/// <summary>
		/// Contains the URL to redirect to
		/// </summary>
		private string redirectSavingPageStateURL=null;


		/// <summary>
		/// Indicates whether the current page is being or has been restored from a persisted state
		/// </summary>
		public bool IsRestoredPageState {
			get {
				return pageState!=null;
			}
		}

		/// <summary>
		/// Returns the post data from the persisted PageState if it exists, otherwise the actual post data from Request.Form
		/// </summary>
		public NameValueCollection PostData {
			get {
				return IsRestoredPageState ? pageState.PostData : Request.Form;
			}
		}

		/// <summary>
		/// Returns the data passed back from the sub-page which can be used to make changes to the saved page state.
		/// This data is passed back via the query string.
		/// </summary>
		public NameValueCollection PassBackData {
			get {
				return IsRestoredPageState ? pageState.PassBackData : null;
			}
		}

		/// <summary>
		/// Call this method instead of Response.Redirect(url) to cause this page to restore its current state when it is next displayed.
		/// </summary>
		public void RedirectSavingPageState(string url) {
			redirectSavingPageStateURL=url;
		}

		/// <summary>
		/// Call this method to redirect to the specified relative URL,
		/// specifying whether to restore the page's saved state
		/// (assuming its state was saved when it was last shown).
		/// The specified URL is relative to that of the current request.
		/// This method is usually called from a "sub-page", which doesn't extend this class. 
		/// </summary>
		public static void RedirectToSavedPage(string url, bool restorePageState) {
			if (!restorePageState) RemoveSavedPageState(url);
			HttpContext.Current.Response.Redirect(url);
		}

		/// <summary>
		/// Call this method to clear the saved PageState of the page with the specified relative URL.
		/// This ensures that the next redirect to the specified page will not revert to the saved state.
		/// The specified URL is relative to that of the current request.
		/// </summary>
		public static void RemoveSavedPageState(string url) {
			RemoveSavedPageState(new Uri(HttpContext.Current.Request.Url,url));
		}

		/// <summary>
		/// This method is called by the framework after the Init event to determine whether a postback is being performed.
		/// The Page.IsPostBack property returns true iff this method returns a non-null.
		/// </summary>
		protected override NameValueCollection DeterminePostBackMode() {
			if (HttpContext.Current.Session == null) return base.DeterminePostBackMode();
			pageState=LoadPageState(Request.Url);
			NameValueCollection normalReturnObject=base.DeterminePostBackMode();
            if ( !IsRestoredPageState ) return normalReturnObject; // default to normal behaviour if there is no persisted pagestate.
			if (normalReturnObject!=null) {
                if ( normalReturnObject["__EVENTARGUMENT"] == "GETDATA" ) return normalReturnObject;
				// this is a normal postback, so don't use persisted page state
				pageState=null;
				RemoveSavedPageState(Request.Url); // clear the page state from the persistence medium so it is not used again
				return normalReturnObject;
			}
			// If we get to this point, we want to restore the persisted page state.
			// Save PassBackData if we have not already done so:
			if (pageState.PassBackData==null) {
				pageState.PassBackData=Request.QueryString;
				// call SavePageState again in case the change we just made is not persisted purely in memory:
				SavePageState(pageState.Url,pageState);
			}
			// Check whether the current request URL matches the persisted URL:
			if (pageState.Url.AbsoluteUri!=Request.Url.AbsoluteUri) {
				// The url, and hence the query string, doesn't match the one in the page state, so reload this page immediately with the persisted URL.
				Response.Redirect(pageState.Url.AbsoluteUri,true);
			}
			RemoveSavedPageState(Request.Url); // clear the page state from the persistence medium so it is not used again
			// This method must return something other than null, otherwise the framework does not call
			// the LoadPageStateFromPersistenceMedium() method and IsPostBack will return false.
			// Request.Form is an empty object of the correct type to achieve this.
			return Request.Form;
		}

		/// <summary>
		/// This method is called by the framework after DeterminePostBackMode(), but before custom event handling.
		/// It returns the view state that the framework uses to restore the state of the controls.
		/// </summary>
        protected override object LoadPageStateFromPersistenceMedium() {
            if ( !IsRestoredPageState ) return base.LoadPageStateFromPersistenceMedium(); // default to normal behaviour if we don't want to restore the persisted page state
            return pageState.ViewStateObject; // otherwise return the ViewStateObject contained in the persisted pageState
        }

		/// <summary>
		/// This method is called by the framework after LoadPageStateFromPersistenceMedium() to raise the Load event.
		/// Controls are populated with data from PostData here because it has to happen after
		/// the framework has loaded the view state, which happens after the execution of the
		/// LoadPageStateFromPersistenceMedium() method.
		/// </summary>
		override protected void OnLoad(EventArgs e) {            
			// The following code is meant to emulate what ASP.NET does "automagically" for us when it
			// populates the controls with post data before processing the events.
			// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpguide/html/cpconreceivingpostbackdatachangednotifications.asp
			// The difference is that this one populates them with our persisted post data instead of the
			// actual post data from Request.Form.
			if (IsRestoredPageState) {
				// Populate controls with PostData, saving a list of those that were modified:
				ArrayList modifiedControls=new ArrayList();
				LoadPostData(this,modifiedControls);
				// Raise PostDataChanged event on all modified controls:
				foreach (IPostBackDataHandler control in modifiedControls)
					control.RaisePostDataChangedEvent();
			}
			base.OnLoad(e);
		}

		/// <summary>
		/// This method performs depth-first recursion on all controls contained in the specified control,
		/// calling the framework's LoadPostData on each and adding those modified to the modifiedControls list.
		/// </summary>
		private void LoadPostData(Control control, ArrayList modifiedControls) {
			// Perform recursion of child controls:
			foreach (Control childControl in control.Controls)
				LoadPostData(childControl,modifiedControls);
			// Load the post data for this control:
			if (control is IPostBackDataHandler) {
				// Get the value of the control's name attribute, which is the GroupName of radio buttons,
				// or the same as the UniqueID attribute for all other controls:
				string nameAttribute=(control is RadioButton) ? ((RadioButton)control).GroupName : control.UniqueID;
				if (control is CheckBoxList) {
					// CheckBoxLists also require special handling:
					int i=0;
					foreach (ListItem listItem in ((ListControl)control).Items)
						if (PostData[nameAttribute+':'+(i++)]!=null) {
							listItem.Selected=true;
							modifiedControls.Add(control);
						}
				} else {
					// Don't process this control if its key isn't in the PostData, as the
					// LoadPostData implementation of some controls throws an exception in this case.
					if (PostData[nameAttribute]==null) return;
					// Call the framework's LoadPostData on this control using the name attribute as the post data key:
					if (((IPostBackDataHandler)control).LoadPostData(nameAttribute,PostData))
						modifiedControls.Add(control);
				}
			}
		}

		/// <summary>
		/// This method is called by the framework between the PreRender and Render events.
		/// It is only called if this page is to be redisplayed, not if Response.Redirect has been called.
		/// To ensure it is called before we redirect, we must postpone the Response.Redirect call until now.
		/// </summary>
		protected override void SavePageStateToPersistenceMedium(object viewState) {
			if (redirectSavingPageStateURL==null) {
				// default to normal behaviour
				base.SavePageStateToPersistenceMedium(viewState);
			} else {
				// persist the current state and redirect to the new page
				SavePageState(Request.Url,new PageState(viewState,Request.Form,Request.Url));
				Response.Redirect(redirectSavingPageStateURL);
			}
		}

		/// <summary>
		/// Override this method to load the state from a persistence medium other than the Session object.
		/// </summary>
		protected static PageState LoadPageState(Uri pageURL) {
			if (HttpContext.Current.Session == null) return null;
			return (PageState)HttpContext.Current.Session[GetPageStateKey(pageURL)];
		}

		/// <summary>
		/// Override this method to save the state to a persistence medium other than the Session object.
		/// </summary>
		protected static void SavePageState(Uri pageURL, PageState pageState) {
			HttpContext.Current.Session[GetPageStateKey(pageURL)]=pageState;
		}

		/// <summary>
		/// Override this method to remove the state from a persistence medium other than the Session object.
		/// </summary>
		protected static void RemoveSavedPageState(Uri pageURL) {
			SavePageState(pageURL,null);
		}

		/// <summary>
		/// Returns a key which will uniquely identify a page in a global namespace based on its URL.
		/// </summary>
		private static string GetPageStateKey(Uri pageURL) {
			return "_PAGE_STATE_"+pageURL.AbsolutePath;
        }

        #endregion
    }



    /// <summary>
    /// This is the object stored in the persistence medium, containing the view state, post data, and URL.
    /// </summary>
    [Serializable]
    public class PageState {
        private SerializableViewState serializableViewState;
        public NameValueCollection PostData;
        public Uri Url;

        // PassBackData is used to store data that is passed back to the parent page from the sub-page:
        public NameValueCollection PassBackData;

        public PageState( object viewStateObject, NameValueCollection postData, Uri url ) {
            serializableViewState = new SerializableViewState( viewStateObject );
            PostData = postData;
            Url = url;
        }

        public object ViewStateObject {
            get {
                return serializableViewState.ViewStateObject;
            }
        }        
    }
    
    /// <summary>
    /// This is a simple wrapper around the view state object to make it serializable.
    /// </summary>
    [Serializable]
    public class SerializableViewState : ISerializable {
        public object ViewStateObject;

        private const string ViewStateStringKey = "ViewStateString";

        public SerializableViewState( object viewStateObject ) {
            ViewStateObject = viewStateObject;
        }

        public SerializableViewState( SerializationInfo info, StreamingContext context ) {
            ViewStateObject = new LosFormatter().Deserialize( info.GetString( ViewStateStringKey ) );
        }

        public void GetObjectData( SerializationInfo info, StreamingContext context ) {
            StringWriter stringWriter = new StringWriter();
            new LosFormatter().Serialize( stringWriter, ViewStateObject );
            info.AddValue( ViewStateStringKey, stringWriter.ToString() );
        }
    }

	public class htmltextwriter : System.Web.UI.HtmlTextWriter {
        public htmltextwriter() : base(new System.IO.StringWriter(), "") { }
		public htmltextwriter(System.IO.TextWriter oWriter) : base(oWriter, "") {
            m_oWriter = oWriter;
        }
        private System.IO.TextWriter m_oWriter;
		public override string NewLine {
			get {
				return "";
			}
			set {
				//base.NewLine = value;
			}
		}
		protected override void OutputTabs() {
			//base.OutputTabs();
		}
		//public override void AddAttribute(string name, string value) {
		//    base.AddAttribute(name, value);
		//}
		//protected override void AddAttribute(string name, string value, System.Web.UI.HtmlTextWriterAttribute key) {
		//    base.AddAttribute(name, value, key);
		//}
		//public override void AddAttribute(System.Web.UI.HtmlTextWriterAttribute key, string value, bool fEncode) {
		//    base.AddAttribute(key, value, fEncode);
		//}
		//protected override bool OnTagRender(string name, System.Web.UI.HtmlTextWriterTag key) {
		//    return base.OnTagRender(name, key);
		//}
		protected override bool OnAttributeRender(string name, string value, System.Web.UI.HtmlTextWriterAttribute key) {
			if (key != HtmlTextWriterAttribute.Id || value != "")
				return base.OnAttributeRender(name, value, key);
			else return false;
		}
        public new System.IO.TextWriter InnerWriter {
            get {
                return m_oWriter;
            }
            set {
                m_oWriter = value;
            }
        }
		public override string ToString() {

            return m_oWriter.ToString();
			//return this.InnerWriter.ToString();
		}
	}
}
