using System;
using System.Web.UI;
using jlib.functions;
using System.Web.UI.WebControls;

namespace jlib.components
{
	/// <summary>
	/// Summary description for textbox.
	/// </summary>
    [ParseChildren(false)]
	public class checkbox : System.Web.UI.WebControls.CheckBox, iControl 
	{

		private string m_sDataMember	= "";
        private bool m_bRequired = false, m_bDisableSave = false, m_bDirty = false, m_bRendered = false, m_bTranslationHide = false;
        private string m_sFieldDesc = "", m_sLabelClass = "";

        public string TranslationKey { get; set; }
        public bool Rendered {
            get {
                return m_bRendered;
            }
        }
		public string DataMember{
			get{
				return m_sDataMember;
			}
			set{
				m_sDataMember	= value;
			}
		}

        public string LabelClass {
            get {
                return m_sLabelClass;
            }
            set {
                m_sLabelClass = value;
            }
        }

		public string FieldDesc{
			get{

				if ( m_sFieldDesc != "" )
					return m_sFieldDesc;

				if ( m_sDataMember != "" ){
					if ( m_sDataMember.IndexOf( "." ) != -1 )
						return parse.split( m_sDataMember, "." )[ 1 ];
					else
						return m_sDataMember;
				}
				return this.ID;			
				
			}

			set{
				m_sFieldDesc	= value;
			}
		}

		public bool Required{
			get{
				return m_bRequired;
			}
			set{
				m_bRequired	= value;
			}
		}
        public bool IsClicked {
            get {
                if (Page.IsPostBack) {
                    if ((Page as jlib.components.webpage).EventTarget == this.UniqueID) return true;                    
                }
                return false;
            }
        }

		public bool DisableSave {
			get {
				return m_bDisableSave;
			}
			set {
				m_bDisableSave = value;
			}
		}

        public override bool Checked {
            get {
                if ( !m_bDirty && !base.Checked && Page.IsPostBack )
                    return convert.cStr( Page.Request.Form[this.UniqueID] ) != "";

                return base.Checked;
            }
            set {
                m_bDirty = true;
                base.Checked = value;
            }
        }

		public bool ReadOnly { get; set; }

        protected override void OnInit(EventArgs e) {
            //this.Load += new EventHandler(label_Load);            
            base.OnInit(e);
            if (TranslationKey != "") {
                webpage oPage = (Page as webpage);
                if (oPage != null) {
                    string s = helpers.translation.translate(oPage.Language, this.Text, Page, TranslationKey);
                    if (s == null) {
                        m_bTranslationHide = true;
                        Text = "";
                        Visible = false;
                    } else this.Text = s;
                }
            }            
        }
		public string validate() {		
			
			if ( Required && !this.Checked )
				return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} must be checked in order to continue." : "{0} m� v�re avkrysset."), this.FieldDesc);                    				
				
			return "";
		}

		public void setVaildateFailure( bool bFailure ){
            if ( bFailure ) {
                this.CssClass = parse.replaceAll( this.CssClass, " validate_failure", "" ) + " validate_failure";
            } else {
                this.CssClass = parse.replaceAll( this.CssClass, " validate_failure", "" );
            }
		}

        public object getValue() {
            return ( this.Checked ? true : false );
        }

        public iControl setValue(object oValue) {
            this.Checked = convert.cBool( oValue );
            return this;
        }


        protected override void Render(System.Web.UI.HtmlTextWriter writer){
            if (m_bTranslationHide) return;
            
			//Output as a Label
			if (this.ReadOnly)
			{
				Literal literal = new Literal();
				literal.Text = (this.Checked ? "Yes" : "No") + " " + this.Text;
				literal.ID = this.ID + "_readonly";
				literal.RenderControl(writer);
				return;
			}

            if (this.Controls != null && this.Controls.Count>0) {
                jlib.components.htmltextwriter o = new htmltextwriter(new System.IO.StringWriter());
                for (int x = 0; x < this.Controls.Count; x++) this.Controls[x].RenderControl(o);                
                this.Text = o.ToString();
            }
            m_bRendered = true;
			string sOnChange = convert.cStr(this.Attributes["onchange"]);
			this.Attributes.Remove("onchange");

            string sOnClick = convert.cStr(this.Attributes["onclientclick"]);
            this.Attributes.Remove("onclientclick");
            
			htmltextwriter oWriter = new htmltextwriter(new System.IO.StringWriter());
			base.Render(oWriter);
            string sHTML = oWriter.InnerWriter.ToString();

			if( sOnChange.Trim() != "" ) sHTML= parse.replaceAll(sHTML, "<input ", "<input onchange=\"" + sOnChange + "\" " );
            if (sOnClick.Trim() != "") sHTML = parse.replaceAll(sHTML, "<input ", "<input onclick=\"" + sOnClick + "\" ");
            if (m_sLabelClass != "") sHTML = parse.replaceAll(sHTML, "<label", "<label class=\"" + m_sLabelClass + "\"");
			
            writer.Write(sHTML);
			
        }

	}
}
