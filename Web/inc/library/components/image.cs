using System;
using jlib.functions;

namespace jlib.components
{
	/// <summary>
	/// Summary description for textbox.
	/// </summary>
	public class image : System.Web.UI.WebControls.Image, iControl 
	{
		public bool ReadOnly { get; set; }
		private string m_sDataMember	= "";        
        private string m_sCrop = "";
        private bool m_bResize = true, m_bStretch = false, m_bImageNotAvailable = true, m_bRenderClientID = false, m_bEnableThumbnailCaching = true, m_bRendered = false, m_bBlankPixelIfNotFound = true;

        public bool Rendered {
            get {
                return m_bRendered;
            }
        }
		public override string ClientID {
			get {
                if (m_bRendered && !m_bRenderClientID) return "";
				m_bRenderClientID = true;
				return base.ClientID;
			}
		}

		public string DataMember{
			get{                
				return m_sDataMember;
			}
			set{
				m_sDataMember	= value;
			}
		}

        public string Crop {
            get {
                return m_sCrop;
            }
            set {
                m_sCrop = value;
            }
        }

		public bool DisableSave {
			get {
				return true;
			}
			set {
			}
		}
        public bool Required {
            get {
                return true;
            }
            set {
            }
        }

        public bool ImageNotAvailable {
            get {
                return m_bImageNotAvailable;
            }
            set {
                m_bImageNotAvailable = value;
            }
        }

        public bool Resize {
            get {
                return m_bResize;
            }
            set {
                m_bResize = value;
            }
        }
        public bool BlankPixelIfNotFound {
            get {
                return m_bBlankPixelIfNotFound;
            }
            set {
                m_bBlankPixelIfNotFound = value;
            }
        }
        
		public bool EnableThumbnailCaching {
			get {
				return m_bEnableThumbnailCaching;
			}
			set {
				m_bEnableThumbnailCaching = value;
			}
		}

        public bool Stretch {
            get {
                return m_bStretch;
            }
            set {
                m_bStretch = value;
            }
        }


		public string validate() { return ""; }


        public object getValue() { return this.ImageUrl; }

        public iControl setValue(object oValue) {                        
            this.ImageUrl = convert.cStr( oValue );
            return this;
        }

        public void setVaildateFailure( bool bFailure ) {}
        		

        protected override void Render( System.Web.UI.HtmlTextWriter writer ) {			
            m_bRendered = true;
            if( !m_bImageNotAvailable && this.ImageUrl == "" )
                return;

            if ( m_bImageNotAvailable && this.ImageUrl == "" )
                this.ImageUrl = "/inc/m/img_not_avail.jpg";

            if ( this.Resize && !this.Height.IsEmpty && !this.Width.IsEmpty )
                this.ImageUrl = helpers.general.getThumbnailUrl(this.ImageUrl, convert.cInt(this.Width.Value), convert.cInt(this.Height.Value), jlib.helpers.general.FileStatus.None, this.Crop, this.EnableThumbnailCaching, this.BlankPixelIfNotFound);

            System.Web.UI.WebControls.Unit oWidth = this.Width;
            System.Web.UI.WebControls.Unit oHeight = this.Height;
            if ( !this.Stretch ) {
                this.Width = System.Web.UI.WebControls.Unit.Empty;
                this.Height = System.Web.UI.WebControls.Unit.Empty;
            }

            System.Web.UI.HtmlTextWriter oHWriter = new System.Web.UI.HtmlTextWriter(new System.IO.StringWriter());
            base.Render(oHWriter);            
			writer.Write(parse.replaceAll(oHWriter.InnerWriter.ToString()," id=\"\"","", ((Style.Count== 0 || Style.Value.IndexOf("border-width:", StringComparison.CurrentCultureIgnoreCase)==-1) && this.BorderWidth.IsEmpty ?"border-width:0px":"") , ""));
			
			
            if ( !this.Stretch ) {
                this.Width = oWidth;
                this.Height = oHeight;
            }
        }
		protected override void RenderChildren(System.Web.UI.HtmlTextWriter writer) {
			base.RenderChildren(writer);
		}
		protected override void RenderContents(System.Web.UI.HtmlTextWriter writer) {
			base.RenderContents(writer);
		}
		public override void RenderControl(System.Web.UI.HtmlTextWriter writer) {
			base.RenderControl(writer);
		}
		
	}	
}
