using System;
using jlib.functions;
using System.Reflection;

namespace jlib.components
{
	/// <summary>
	/// Summary description for textbox.
	/// </summary>
    public class radiobutton : System.Web.UI.WebControls.RadioButton, iControl
	{
		#region DECLARATIONS
				
        private string m_sDataMember = "";
        private bool m_bRequired = false, m_bDisableSave = false, m_bDirty = false, m_bRendered = false;
        private string m_sFieldDesc = "";
		private string m_sLabelClass = "";

		#endregion

		
		#region PROPERTIES
		public bool ReadOnly { get; set; }
		protected override void OnInit(EventArgs e) {
			base.OnInit(e);
			if (Page.IsPostBack && convert.cStr(this.ID) != "" && convert.cStr(Page.Request.Form[parse.split(this.UniqueID, this.ID)[0] + this.GroupName]) == this.ID) this.Checked = true;
		}

        public bool Rendered {
            get {
                return m_bRendered;
            }
        }

        public string DataMember {
            get {
                return m_sDataMember;
            }
            set {
                m_sDataMember = value;
            }
        }

        public string FieldDesc {
            get {

                if ( m_sFieldDesc != "" )
                    return m_sFieldDesc;

                if ( m_sDataMember != "" ) {
                    if ( m_sDataMember.IndexOf( "." ) != -1 )
                        return parse.split( m_sDataMember, "." )[1];
                    else
                        return m_sDataMember;
                }
                return this.ID;

            }

            set {
                m_sFieldDesc = value;
            }
        }

        public bool Required {
            get {
                return m_bRequired;
            }
            set {
                m_bRequired = value;
            }
        }

		public bool DisableSave {
			get {
				return m_bDisableSave;
			}
			set {
				m_bDisableSave = value;
			}
		}

		public string LabelClass {
			get {
				return m_sLabelClass;
			}
			set {
				m_sLabelClass = value;
			}
		}

        public string validate() {

            if (Required && (!this.Checked && convert.cStr(Page.Request.Form[parse.split(this.UniqueID, this.ID)[0] + this.GroupName])=="") )
				return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} must be selected." : "{0} m� v�re avkrysset."), this.FieldDesc);                                    

            return "";
        }

		//public override bool Checked {
		//    get {
		//        if ( !m_bDirty && !base.Checked && Page.IsPostBack )
		//            return convert.cStr( Page.Request.Form[this.UniqueID] ) != "" || convert.cStr( Page.Request.Form[ this.UniqueID.Remove(this.UniqueID.Length - this.ID.Length ) + this.GroupName]) == this.ID ;

		//        return base.Checked;
		//    }
		//    set {
		//        m_bDirty = true;
		//        base.Checked = value;
		//    }
        //}

        public void setVaildateFailure( bool bFailure ) {
            if ( bFailure ) {
                this.CssClass = parse.replaceAll( this.CssClass, " validate_failure", "" ) + " validate_failure";
            } else {
                this.CssClass = parse.replaceAll( this.CssClass, " validate_failure", "" );
            }
        }

        public object getValue() {
            return ( this.Checked ? "1" : "0" );
        }

        public iControl setValue(object oValue) {
            this.Checked = convert.cBool( oValue );
            return this;
        }

		#endregion

		#region METHODS

		protected override void Render(System.Web.UI.HtmlTextWriter writer) {
            m_bRendered = true;
            string sOnChange = convert.cStr(this.Attributes["onchange"]);
            this.Attributes.Remove("onchange");

            string sOnClick = convert.cStr(this.Attributes["onclientclick"]);
            this.Attributes.Remove("onclientclick");

            htmltextwriter oWriter = new htmltextwriter(new System.IO.StringWriter());
            base.Render(oWriter);

            string sHTML = oWriter.InnerWriter.ToString();
            if (sOnChange.Trim() != "") sHTML = parse.replaceAll(sHTML, "<input ", "<input onchange=\"" + sOnChange + "\" ");
            if (sOnClick.Trim() != "") sHTML = parse.replaceAll(sHTML, "<input ", "<input onclick=\"" + sOnClick + "\" ");
            if (m_sLabelClass != "") sHTML = parse.replaceAll(sHTML, "<label", "<label class=\"" + m_sLabelClass + "\"");

            writer.Write(sHTML);			
		}

        public override bool Checked {
            get {
                try {
                    if (!m_bDirty && !base.Checked && Page.IsPostBack)
                        return convert.cStr(Page.Request.Form[this.UniqueID.Substring(0, this.UniqueID.Length - this.ID.Length) + this.GroupName]) == this.ID;
                } catch (Exception) { }                

                return base.Checked;
            }
            set {
                m_bDirty = true;
                base.Checked = value;
            }
        }

        //protected override void Render(System.Web.UI.HtmlTextWriter writer) 
        //{
        //    try
        //    {
        //        //Check GroupName For ID
        //        if(Page.Request[this.GroupName] == this.ClientID)
        //            this.Checked = true;

        //        System.IO.StringWriter swWriter				= new System.IO.StringWriter();
        //        System.Web.UI.HtmlTextWriter htmlWriter		= new System.Web.UI.HtmlTextWriter( swWriter );
			
        //        base.Render( htmlWriter );

        //        string sSource	= swWriter.GetStringBuilder().ToString();
				
        //        //sSource = sSource.Replace("value=\"", "value2=\"");
        //        //sSource = sSource.Replace("name=\"", String.Format("name=\"{0}\" value=\"{1}\" tmp=\"", this.GroupName, this.ClientID));
				
				
        //        writer.Write(sSource);
			
        //    }
        //    catch(Exception e)
        //    {
        //        //throw e;
        //        base.Render(writer);
	
        //    }
        //}
		
		#endregion

		
	}
}
