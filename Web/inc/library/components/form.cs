using System;
using System.Collections.Generic;
using jlib.functions;
using System.Security.Cryptography;

namespace jlib.components
{
	/// <summary>
	/// Summary description for textbox.
	/// </summary>
	public class form : System.Web.UI.HtmlControls.HtmlForm
	{
		private string m_sAction = "";
        private bool m_bDisableViewState = false, m_bRendered = false, m_bEnableIControlViewState = false, m_bIControlsParsed = false;

        private System.Collections.Hashtable m_oIControlValues=new System.Collections.Hashtable(), m_oViewState=new System.Collections.Hashtable();
        private System.Collections.Generic.List<iControl> oIControls;

		public bool UseFullActionPath { get; set; }

        public System.Collections.Hashtable Viewstate{
            get {
                return m_oViewState;
            }
            set {
                m_oViewState = value;
            }
        }

        public System.Collections.Hashtable ControlState {
            get {
                return m_oIControlValues;
            }            
        }

        public bool EnableIControlViewState {
            get {
                return m_bEnableIControlViewState;
            }
            set {
                m_bEnableIControlViewState = value;
                if (value) parseViewState();
            }
        }
		public bool DisableViewState {
			get {
				return m_bDisableViewState;
			}
			set {
				m_bDisableViewState = value;
			}
		}
        public bool Rendered {
            get {
                return m_bRendered;
            }
        }
        public new string Action {
			get {
				return m_sAction;
			}
			set {
				m_sAction = value;
			}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter output) {
            m_bRendered = true;
			htmltextwriter writer = new htmltextwriter(new System.IO.StringWriter());
			base.Render(writer);
			string sValue = writer.InnerWriter.ToString();
			if (Action != "" )
			{
                //sValue = parse.replaceAll(sValue, parse.inner_substring(sValue, "<form", "action=\"", "\"", null), Action);
                sValue = parse.replaceAll(sValue, "action=\"" + parse.inner_substring(sValue, "<form", "action=\"", "\"", null), "action=\"" + Action);
			} 
			
			else if ( Page.Request.Path != Page.Request.CurrentExecutionFilePath  || this.UseFullActionPath)
			{
				string sCurrentAction = parse.inner_substring(sValue, "<form", "action=\"", "\"", null);
				sValue = parse.replaceAll(sValue, sCurrentAction, Page.Request.CurrentExecutionFilePath + (sCurrentAction.IndexOf("?")>-1? sCurrentAction.Substring(sCurrentAction.IndexOf("?")) : "" ) );
			} 
			
			else if (convert.cStr(Context.Items["UrlRewriter.NET.RawUrl"]) != "" && convert.cStr(Context.Items["UrlRewriter.NET.RawUrl"]).IndexOf(Page.Request.Path) == -1) 
			{
				string sCurrentAction = parse.inner_substring(sValue, "<form", "action=\"", "\"", null);
				sValue = parse.replaceAll(sValue, "\"" + sCurrentAction + "\"", "\"" + convert.cStr(Context.Items["UrlRewriter.NET.RawUrl"]) + "\"");
			}



			if (DisableViewState && sValue.IndexOf("__VIEWSTATE") > -1) {
				if( sValue.IndexOf("__VIEWSTATE=") > -1 ){
					string sTerminator = (sValue.IndexOf( "&" , sValue.IndexOf("__VIEWSTATE=")) < sValue.IndexOf( "\"" , sValue.IndexOf("__VIEWSTATE=")) ? "&" : "\"" );
					sValue = parse.inner_substring(sValue, null, null, "__VIEWSTATE=", null) + (sTerminator == "&" ? "" : sTerminator) +parse.inner_substring(sValue, "__VIEWSTATE=", sTerminator, null, null);
				}
				if( sValue.IndexOf("\"__VIEWSTATE\"") > -1) {
					int iViewstateOffset = sValue.LastIndexOf("<", sValue.IndexOf("\"__VIEWSTATE"));
					sValue = sValue.Substring(0, iViewstateOffset) + sValue.Substring(sValue.IndexOf(">", iViewstateOffset)+1);
				}
			}
            if (EnableIControlViewState) {                
                string sViewState = "";
                for (int x = 0; x < oIControls.Count; x++)
                    if (!oIControls[x].Rendered) {
                        if (oIControls[x] as label == null || (oIControls[x] as label).EnableViewState)
                            sViewState += (sViewState == "" ? "" : "&") + oIControls[x].UniqueID + "=" + System.Web.HttpUtility.UrlEncode(convert.cStr(oIControls[x].getValue()));
                    }
                foreach( string sKey in m_oViewState.Keys)
                    sViewState += (sViewState == "" ? "" : "&") + "::" + sKey + "=" + System.Web.HttpUtility.UrlEncode(convert.cStr(m_oViewState[sKey]));

                if (sViewState != "") {
                    MD5 oCrypto = new MD5CryptoServiceProvider();
                    string sHash = Convert.ToBase64String(oCrypto.ComputeHash(System.Text.ASCIIEncoding.Default.GetBytes(sViewState + System.Configuration.ConfigurationManager.AppSettings["sql.dsn"])));
                    sValue = parse.replaceAll(sValue, "</form>", "<input type=\"hidden\" value=\"" + sHash + sViewState + "\" name=\"__IVIEWSTATE\"></form>");
                }
            }
			output.Write(sValue);            
		}
        protected override void OnInit(EventArgs e) {
            parseViewState();
            base.OnInit(e);            
        }
        private void parseViewState() {
            if (EnableIControlViewState) {
                if (!m_bIControlsParsed) {
                    m_bIControlsParsed = true;                    
                    if (convert.cStr(System.Web.HttpContext.Current.Request.Form["__IVIEWSTATE"]) != "") {
                        string sHash = parse.inner_substring(System.Web.HttpContext.Current.Request.Form["__IVIEWSTATE"], null, null, "==", null) + "==";
                        string sViewState = convert.cStr(System.Web.HttpContext.Current.Request.Form["__IVIEWSTATE"]).Substring(sHash.Length);
                        MD5 oCrypto = new MD5CryptoServiceProvider();
                        if (sHash == Convert.ToBase64String(oCrypto.ComputeHash(System.Text.ASCIIEncoding.Default.GetBytes(sViewState + System.Configuration.ConfigurationManager.AppSettings["sql.dsn"])))) {
                            string[] sArr = parse.split(sViewState, "&");
                            for (int x = 0; x < sArr.Length; x++) {
                                if (parse.splitValue(sArr[x], "=", 0).StartsWith("::"))
                                    m_oViewState.Add(parse.splitValue(sArr[x], "=", 0).Substring(2), System.Web.HttpUtility.UrlDecode(parse.splitValue(sArr[x], "=", 1)));
                                else
                                    m_oIControlValues.Add(parse.splitValue(sArr[x], "=", 0), System.Web.HttpUtility.UrlDecode(parse.splitValue(sArr[x], "=", 1)));
                            }
                        }
                    }
                    oIControls = jlib.helpers.control.getDynamicControls(this, null);
                    for( int x=0;x<oIControls.Count;x++)
                        try {
                            if (m_oIControlValues[oIControls[x].UniqueID] != null) oIControls[x].setValue( convert.cStr(m_oIControlValues[oIControls[x].UniqueID]) == "" ? null : m_oIControlValues[oIControls[x].UniqueID]);
                        } catch (Exception) { }
                }
            }
        }

        protected override void AddedControl(System.Web.UI.Control control, int index) {            
            base.AddedControl(control, index);
            if (EnableIControlViewState && m_bIControlsParsed) {                    
                iControl oControl = control as iControl;                
                if (oControl != null){
                    if(m_oIControlValues[oControl.UniqueID] != null) oControl.setValue(convert.cStr(m_oIControlValues[oControl.UniqueID]) == "" ? null : m_oIControlValues[oControl.UniqueID]);
                    oIControls.Add(oControl);
                }
            }
        }
	}	
}
