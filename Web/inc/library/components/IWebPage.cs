﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web;

namespace jlib.components
{
	public interface IWebPage
	{
		bool IsPostBackA { get; }

		/// <summary>
		/// Get Server Control Referenced By EventTarget PostBack
		/// </summary>
		Control EventTargetControl { get; }

		/// <summary>
		/// Gets the EventTarget Postback Value
		/// </summary>
		string EventTarget { get; set; }

		/// <summary>
		/// Gets the EventArgument Postback Value
		/// </summary>
		string EventArgument { get; set; }

		/// <summary>
		/// Determines if Postback was result of Ajax Operation
		/// </summary>
		bool IsAjaxPostback { get; }

        /// <summary>
        /// Gets the Language Value
        /// </summary>
        string Language { get; set; }

        /// <summary>
        /// Register Startup Script
        /// </summary>
        void registerScript(System.Type oType, string sKey, string sScript);

		HttpRequest Request { get; }
		HttpResponse Response { get; }
	}
}
