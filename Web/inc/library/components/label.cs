using System;
using jlib.functions;

namespace jlib.components
{
	/// <summary>
	/// Summary description for textbox.
	/// </summary>
	public class label : System.Web.UI.WebControls.Label, iControl 
	{
		public bool ReadOnly { get; set; }
        private bool m_bConvertNewlineToBr = false, m_bRendered = false, m_bTranslationHide=false;
        private string m_sDataMember = "", m_sFormatString = "", m_sTag = "span",m_sTranslationKey="";
        private bool? m_bDisableDrawSpan = null, m_bEnableViewStateDirty = null, m_bRenderClientID=null;
        private textbox.FieldType m_eValidateAs = textbox.FieldType.Text;
        private textbox.Capitalize m_eCapitalize = textbox.Capitalize.None;
        private object m_oValue = null;
			

        public bool Rendered {
            get {
                return m_bRendered;
            }
        }
		public bool ConvertNewlineToBr {
			get {
				return m_bConvertNewlineToBr;
			}
			set {
				m_bConvertNewlineToBr = value;
			}
		}

		public override string ClientID {
			get {
                if (m_bRenderClientID == null) m_bRenderClientID = true;
				return base.ClientID;
			}
		}

		public bool DisableSave {
			get {
				return true;
			}
			set {
			}
		}
        public bool Required {
            get {
                return true;
            }
            set {
            }
        }

        public bool RenderClientID {
            get {
                return (m_bRenderClientID==null?false:m_bRenderClientID.Value);
            }
            set {
                m_bRenderClientID = value;
            }
        }
        
		public string DataMember{
			get{                
				return m_sDataMember;
			}
			set{
				m_sDataMember	= value;
			}
		}

        public string FormatString {
            get {
                return m_sFormatString;
            }
            set {
                m_sFormatString = value;
            }
        }
        public string TranslationKey {
            get {
                return m_sTranslationKey;
            }
            set {
                m_sTranslationKey = value;
            }
        }
        
        public bool ?DisableDrawSpan {
            get {
                return m_bDisableDrawSpan;
            }
            set {
                m_bDisableDrawSpan = value;
            }
        }
	
		public string Tag {
			get {
				return m_sTag;
			}
			set {
				m_sTag = value;
                if (value == "")
                    DisableDrawSpan = true;
                else
                    DisableDrawSpan = false;
			}
		}


        public string Capitalize {
            get {
                return m_eCapitalize.ToString();
            }
            set {
                try {
                    m_eCapitalize = (textbox.Capitalize)Enum.Parse(typeof(textbox.Capitalize), value, true);
                } catch ( Exception ) {
                    m_eCapitalize = textbox.Capitalize.None;
                }                
            }
        }
        public string ValidateAs {
            get {
                return m_eValidateAs.ToString();
            }
            set {
                try {
                    m_eValidateAs = (textbox.FieldType)Enum.Parse( typeof( textbox.FieldType ), value, true );
                } catch ( Exception ) {
                    m_eValidateAs = textbox.FieldType.Text;
                }                
            }
        }
        
		public string validate() { return ""; }


        public object getValue() { return m_oValue; }

        public iControl setValue( object oValue ){            
            if ( m_eValidateAs == textbox.FieldType.Number ) oValue = convert.cInt( oValue );
            m_oValue = oValue;
            return this;
        }

        public void setVaildateFailure( bool bFailure ) {}


        public override string Text {
            get {
                return convert.cStr( getValue() );
            }
            set {
                setValue( value );
            }
        }

		//public void label_Load( object sender, EventArgs e ) {
		//    if ( this.Text == "" && Page.IsPostBack && this.EnableViewState )
		//        this.Text = convert.cStr(this.ViewState[this.UniqueID]);
		//}

        
		public override bool EnableViewState {
			get {
                if(m_bEnableViewStateDirty==null) return base.EnableViewState;
                return m_bEnableViewStateDirty.Value;
			}
			set {
                m_bEnableViewStateDirty = value;
			}
		}
        public void UpdateTranslationKey(string newKey) {
            TranslationKey = newKey;
            Translate();
        }
        private void Translate() {
            if (TranslationKey != "") {
                webpage oPage = (Page as webpage);
                if (oPage != null) {
                    string s = helpers.translation.translate(oPage.Language, this.Text, Page, TranslationKey);
                    if (s == null) {
                        m_bTranslationHide = true;
                        Text = "";
                        Visible = false;
                    } else this.Text = s;
                }
            }
        }
        protected override void OnInit( EventArgs e ) {
            //this.Load += new EventHandler(label_Load);            
            base.OnInit( e );
            Translate();
            if (m_bEnableViewStateDirty == null || !m_bEnableViewStateDirty.Value) base.EnableViewState = false;
			if (Page.IsPostBack && this.EnableViewState && convert.cStr(Page.Request[this.UniqueID + "_value"]) != "") this.Text = Page.Request[this.UniqueID + "_value"];
        }
		
        protected override void Render( System.Web.UI.HtmlTextWriter writer ) {
            if (m_bTranslationHide) return;
            
            m_bRendered = true;
			if (this.FormatString != "" && m_oValue != null)
				this.Text = String.Format(this.FormatString, (m_oValue.GetType().Name == "Boolean" ? (convert.cBool(m_oValue) ? 1 : 0) : m_oValue));
            else if (m_eValidateAs == textbox.FieldType.Currency)
                this.Text = convert.cCurrency(m_oValue);
            else if (convert.cStr(m_oValue) != "" && (m_eValidateAs == textbox.FieldType.Date || m_eValidateAs == textbox.FieldType.Date1))
                this.Text = (this.FormatString=="" ? convert.cDate(m_oValue).ToShortDateString() :String.Format(this.FormatString, convert.cDate(m_oValue)) );
            else
				this.Text = convert.cStr(m_oValue);

			if (this.EnableViewState && this.Text != "") Page.RegisterHiddenField(this.UniqueID + "_value", this.Text);

            if ((this.Style.Count > 0 || this.Controls.Count > 0 || this.CssClass!="") && DisableDrawSpan==null) DisableDrawSpan = false;
            if (m_bRenderClientID != null && m_bRenderClientID.Value) DisableDrawSpan = false;

			if (ConvertNewlineToBr) this.Text = parse.replaceAll(this.Text, "\n", "<br>");
            if (m_eCapitalize == textbox.Capitalize.Normal && this.Text.Length > 0) this.Text = this.Text.Substring(0, 1).ToUpper() + this.Text.Substring(1);
            System.IO.StringWriter oWriter = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter oHWriter = new System.Web.UI.HtmlTextWriter(oWriter);
            if (DisableDrawSpan==null || DisableDrawSpan == true) {
                this.RenderChildren(oHWriter);
                writer.Write(this.Text + oWriter.ToString());
			} else {
				bool bRenderClientID = (m_bRenderClientID!=null && m_bRenderClientID.Value);
				base.Render( oHWriter );

				if (Tag != "")
				    writer.Write( parse.replaceAll( "<" + Tag + oWriter.GetStringBuilder().ToString().Substring(5, oWriter.GetStringBuilder().ToString().Length - 5 - 7) + "</" + Tag + ">", ( bRenderClientID ? "1" : "" ) +"id=\"" + this.ClientID + "\"", "" ));
				else
					writer.Write(parse.replaceAll(oWriter.GetStringBuilder().ToString(), (bRenderClientID ? "1" : "") + "id=\"" + this.ClientID + "\"", ""));
			}
        }

	}
}
