using System;
using System.Web.UI;
using jlib.functions;

using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Text;
using System.Web.UI.WebControls;

namespace jlib.components
{
	/// <summary>
	/// Summary description for textbox.
	/// </summary>
    public class textbox : System.Web.UI.WebControls.TextBox, iControl, IPostBackEventHandler
	{

        private string m_sDataMember = "", m_sTranslationKey = "", m_sFieldDesc = "", m_sDefaultValue = "", m_sFormatString = "";
        private bool m_bRequired = false, m_bDisableSave = false, m_bDirty = false, m_bRendered = false, m_bAjaxValidate = false, m_bAjaxValidateText = true, m_bDisplayValidateStatus = false, m_bLocked = false, m_bTranslationHide=false;
        
		private FieldType m_eValidateAs	= FieldType.Text;		
		private int m_iMinLength = -1, m_iMaxLength = -1, m_iMinValue = int.MinValue, m_iMaxValue = int.MaxValue;
        

        private ImageButton m_oDefaultButton = null;

        public enum Capitalize {
            None,
            Normal,
            Camel
        }
		public enum FieldType{
			Text,
			Email,
			Number,
			DecimalNumber,
			Date,
            Date1,
			Time,
            Phone,
            Currency,
            Percent,
            Editor,
			NoValidation
		}

        public textbox() {
            this.MaskCharAlpha = "a";
            this.MaskCharNumeric = "n";
            this.MaskCharAlphaNumeric = "x";
            this.MaskCharDisplay = "_";
        }

		public string DataMember{
			get{
				return m_sDataMember;
			}
			set{
				m_sDataMember	= value;
			}
		}

        public string FormatString {
            get {
                return m_sFormatString;
            }
            set {
                m_sFormatString = value;
            }
        }
        public string TranslationKey {
            get {
                return m_sTranslationKey;
            }
            set {
                m_sTranslationKey = value;
            }
        }
        public bool DisplayValidateStatus {
            get {
                return m_bDisplayValidateStatus;
            }
            set {
                m_bDisplayValidateStatus = value;
            }
        }

        public bool Rendered {
            get {
                return m_bRendered;
            }
        }
        public bool AjaxValidate {
            get {
                return m_bAjaxValidate;
            }
            set {
                m_bAjaxValidate = value;
            }
        }
        // Should we display Text as a part of the Ajax Validate
        public bool AjaxValidateText {
            get {
                return m_bAjaxValidateText;
            }
            set {
                m_bAjaxValidateText = value;
            }
        }

        public bool Locked {
            get {                
                return m_bLocked;
            }
            set {
                m_bLocked = value;
            }
        }

	    public string ValidateAs{
			get{
				return m_eValidateAs.ToString();
			}
			set{
				try{
					m_eValidateAs	= (FieldType)Enum.Parse( typeof( FieldType ), value, true );
                    if ( m_eValidateAs == FieldType.Phone ) {
                        this.Mask = "(nnn) nnn-nnnn";
                        m_eValidateAs = FieldType.Text;
                    }

				}catch(Exception){
					m_eValidateAs	= FieldType.Text;
				}
				
			}
		}
		

		public string FieldDesc{
			get{

				if ( m_sFieldDesc != "" )
					return m_sFieldDesc;

				if ( m_sDataMember != "" ){
					if ( m_sDataMember.IndexOf( "." ) != -1 )
						return parse.split( m_sDataMember, "." )[ 1 ];
					else
						return m_sDataMember;
				}
				return this.ID;			
				
			}

			set{
				m_sFieldDesc	= value;
			}
		}

        public string DefaultValue {
			get{
                return m_sDefaultValue;
			}
			set{
                m_sDefaultValue = value;
			}
		}

		public bool Required{
			get{
				return m_bRequired;
			}
			set{
				m_bRequired	= value;
			}
		}

		public bool DisableSave {
			get {
				return m_bDisableSave;
			}
			set {
				m_bDisableSave = value;
			}
		}

		public int MinLength{
			get{
				return m_iMinLength;
			}
			set{
				m_iMinLength	= value;
			}
		}

        public override int MaxLength {
            get {
                return m_iMaxLength;
            }
            set {
                m_iMaxLength = value;
                if ( m_iMaxLength > 0 )
                    base.MaxLength = m_iMaxLength;
            }
        }

        public int MinValue {
            get {
                return m_iMinValue;
            }
            set {
                m_iMinValue = value;                
            }
        }

        public int MaxValue {
            get {
                return m_iMaxValue;
            }
            set {
                m_iMaxValue = value;
            }
        }

        public override string Text {
            get {
				string sValue = base.Text;
				if ( !m_bDirty && base.Text == "" && Page.IsPostBack )
                    sValue = convert.cStr( Page.Request.Form[this.UniqueID] );
				
				return sValue;
            }
            set {
                m_bDirty = true;

                value = convert.cStr( value );
                if ( this.MaxLength > -1 && value.Length > this.MaxLength )
                    value = value.Substring( 0, this.MaxLength );                

                base.Text = value;

                textbox_TextChanged( null, null );
            }
        }

        public ImageButton DefaultButton {
            get {
                return m_oDefaultButton;
            }
            set {
                m_oDefaultButton = value;
            }
        }


        public string OnClientChange {
            get {
                return this.Attributes["onchange"];
            }
            set {
                this.Attributes["onchange"] = value;
            }
        }
        public string OnClientClick {
            get {
                return this.Attributes["onclick"];
            }
            set {
                this.Attributes["onclick"] = value;
            }
        }

		//public int HashCode{
		//    get{
		//        return m_iHashCode;
		//    }
		//}

		public string validate() {
			if (this.Locked || !this.Enabled) return "";

			string sValue = convert.cStr(this.getValue()).Trim();

            if ( sValue != "" ) {
                if ( MinLength > 0 && sValue.Length < MinLength && !( !Required && sValue == "" ) )
					return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} must be minimum {1} character(s)." : "{0} m� v�re minimum {1} tegn."), this.FieldDesc, MinLength);

                if ( MaxLength > 0 && sValue.Length > MaxLength )
					return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} can be max {1} character(s)." : "{0} m� v�re maksimum {1} tegn."), this.FieldDesc, MaxLength);                    
                
                if ( (m_eValidateAs == FieldType.Date ||m_eValidateAs == FieldType.Date1) && !convert.isDate( sValue ) )
					return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} is not a vailid date. Please use the format mm/dd/yyyy." : "{0} er ikke et gyldig dato. Vennligst bruk formatet dd.mm.�� (dd=dag, mm=m�ned, ��=�r)."), this.FieldDesc);

				if (m_eValidateAs == FieldType.Email && !convert.isEmail(sValue))
					return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} is not a valid email address. Please use user@domain.com." : "{0} er ikke en gyldig email. Vennligst benytt formatet bruker@domene.no"), this.FieldDesc);

                if ( m_eValidateAs == FieldType.DecimalNumber && !convert.isNumeric( sValue ) )
					return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} is not a valid number." : "{0} er ikke et gyldig nummer"), this.FieldDesc);
                    
                if ( m_eValidateAs == FieldType.Number && ( !convert.isNumeric( sValue ) || convert.cInt( sValue ) != convert.cDec( sValue ) ) )
					return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} is not a valid number. This field does not allow decimals." : "{0} er ikke et gyldig nummer. Dette feltet godtar ikke desimaler."), this.FieldDesc);
                    
                if ( m_eValidateAs == FieldType.Number || m_eValidateAs == FieldType.DecimalNumber ) {
                    
                    if ( this.MinValue > int.MinValue && convert.cInt( sValue ) < this.MinValue )
						return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} must be greater than or equal to {1}." : "{0} m� v�re {1} eller st�rre."), this.FieldDesc, this.MinValue);					                       

                    if ( this.MaxValue < int.MaxValue && convert.cInt( sValue ) > this.MaxValue)
						return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} must be less than or equal to {1}." : "{0} m� v�re {1} eller mindre."), this.FieldDesc, this.MaxValue);					                       
                                        
				}

            } else if ( this.Required && sValue == "" ) {
				return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} is a required field." : "{0} m� v�re fylt ut."), this.FieldDesc);                    
            }

			return "";
		}

		public void setVaildateFailure( bool bFailure ){
            if ( bFailure ) {
                this.CssClass = parse.replaceAll( this.CssClass, " validate_failure", "" ) + " validate_failure";
            } else {
                this.CssClass = parse.replaceAll( this.CssClass, " validate_failure", "" );
            }
		}

        public string getSQLValue()
        {
            return jlib.db.ado_helper.PrepareDB(getValue());
        }

        public object getValue() {
			if (this.m_eValidateAs == FieldType.Currency)
				return (System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"] == "nb-NO" ? parse.replaceAll(parse.replaceAll(this.Text, "nok", "", "kr", ""), ",", ".") : parse.replaceAll(this.Text, "$", ""));

            if ( this.m_eValidateAs == FieldType.Percent )
                return parse.replaceAll( this.Text, "%", "" );

            if ( this.m_eValidateAs == FieldType.Phone )
                return parse.replaceAll( this.Text, "(", "", ")", "", "-", "", " ", "" );

			if ( (m_eValidateAs == FieldType.Date || m_eValidateAs == FieldType.Date1) && System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"] == "nb-NO" && this.Text.IndexOf(".") > -1 && parse.split(this.Text, ".").Length > 2){
				try{
					return new DateTime(convert.cInt(parse.split(this.Text, ".")[2]) + (convert.cInt(parse.split(this.Text, ".")[2]) > 100 ? 0 : 2000), convert.cInt(parse.split(this.Text, ".")[1]), convert.cInt(parse.split(this.Text, ".")[0]));
				}catch(Exception){
					return "";
				}
			}									
			
            return this.Text;
        }

        public iControl setValue(object oValue) {
            if ( m_eValidateAs == FieldType.Number )
                oValue = convert.cInt( oValue );

            if ( this.FormatString != "" )
                this.Text = String.Format( this.FormatString, oValue );
			else if ((m_eValidateAs == FieldType.Date||m_eValidateAs == FieldType.Date1) && convert.cStr(oValue) != "") {
				if (System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"] == "nb-NO")
					this.Text = jlib.functions.format.no.cDefaultDate(oValue);
				else
					this.Text = String.Format("{0:d}", convert.cDate(oValue));
			} else if (m_eValidateAs == FieldType.Time && convert.cStr(oValue) != "") {
				if (System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"] == "nb-NO")
                    this.Text = jlib.functions.format.no.cDefaultTime(oValue);
				else
					this.Text = String.Format("{0:t}", convert.cDate(oValue));
			} else if (m_eValidateAs == FieldType.Currency && System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"] == "nb-NO")
				this.Text = parse.replaceAll(convert.cDecimalNumber(oValue, 2), ".", ",");
			else
				this.Text = convert.cStr(oValue);

            return this;
        }

        protected override void OnInit(EventArgs e){

            if (TranslationKey != "") {
                webpage oPage = (Page as webpage);
                if (oPage != null) {
                    string s = helpers.translation.translate(oPage.Language, this.Text, Page, TranslationKey);
                    if (s == null) {
                        m_bTranslationHide = true;
                        Text = "";
                        Visible = false;                        
                    } else this.Text = s;
                }
            }
            this.TextChanged += new EventHandler( textbox_TextChanged );            
            this.Load += new EventHandler( textbox_Load );
            this.Page.PreRender += new EventHandler( Page_PreRender );
            base.OnInit(e);
        }

        void Page_PreRender( object sender, EventArgs e ) {
            ViewState[this.ClientID + "_Required"] = this.Required;

            if (this.m_eValidateAs == FieldType.Date ||
                this.m_eValidateAs == FieldType.Date1) {
                //Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "calendar_include", "/inc/js/_cal.js");
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "calendar_constructor", "<DIV ID=\"divCal\" class=\"Calendar_new\"></DIV><script>var oCal = new CalendarPopup(\"divCal\");</script>", false);

                Page.ClientScript.RegisterStartupScript(this.GetType(), "calendar_constructor-" + this.ClientID, "$(function() {$('#" + this.ClientID + "').datepicker({" + (System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"] == "nb-NO" ? "dateFormat: 'dd.mm.yy'" : "") + "});});", true);
            } else if (this.m_eValidateAs == FieldType.Editor) {
                Page.ClientScript.RegisterClientScriptInclude("CKEditor", "/inc/library/ckeditor/ckeditor.js");
                //Page.ClientScript.RegisterClientScriptInclude("CKEditor-jquery", "/inc/library/ckeditor/adapters/jquery.js");
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "CKEditor-constructor" + this.ClientID, "CKEDITOR.basePath='/inc/library/ckeditor/';CKEDITOR.replace( '" + this.ClientID + "',{skin : 'office2003',toolbar : 'Lexicon'" + (!this.Height.IsEmpty ? ",height:'" + this.Height.Value + (this.Height.Type == UnitType.Percentage ? "%" : "px") + "'" : "") + (!this.Width.IsEmpty ? ",width:'" + this.Width.Value + (this.Width.Type == UnitType.Percentage ? "%" : "px") + "'" : "") + "});", true);                
            }
            if ( ( this.Mask != null ) && ( this.Mask.Length > 0 ) )
                this.writeScriptBlock();

        }        

        void textbox_Load( object sender, EventArgs e ) {
            if ( ViewState[this.ClientID + "_Required"] != null )
                this.Required = convert.cBool( ViewState[this.ClientID + "_Required"] );
        }

        void textbox_TextChanged( object sender, EventArgs e ) {            
            ViewState[this.ClientID + "_TextChanged"] = "true";
            //Page.Form.DefaultFocus = this.ClientID;
        }



        protected override void Render(System.Web.UI.HtmlTextWriter writer){

			//Output as a Label
			if (this.ReadOnly)
			{
				Literal literal = new Literal();
				literal.Text = this.Text;
				literal.ID = this.ID + "_readonly";
				literal.RenderControl(writer);
				return;
			}

            if (m_bTranslationHide) return;
            m_bRendered = true;
            //if (this.m_eValidateAs == FieldType.Editor) {
            //    writer.Write("<div id=\"" + this.ClientID + "\">" + this.Text + "</div><input type=\"hidden\" id=\"" + this.ClientID + "_value\" name=\"" + this.UniqueID + "\" />");
            //    writer.Write("<script language=\"javascript\">CKEDITOR.basePath='/inc/library/ckeditor/';if(CKEDITOR.instances." + this.ClientID + ")CKEDITOR.remove(CKEDITOR.instances." + this.ClientID + "); CKEDITOR.replace( '" + this.ClientID + "',{skin : 'office2003',toolbar : 'Lexicon'" + (!this.Height.IsEmpty ? ",height:'" + this.Height.Value + (this.Height.Type == UnitType.Percentage ? "%" : "px") + "'" : "") + (!this.Width.IsEmpty ? ",width:'" + this.Width.Value + (this.Width.Type == UnitType.Percentage ? "%" : "px") + "'" : "") + "});$(\"form\").submit(function () { $(\"#" + this.ClientID + "_value\").val(CKEDITOR.instances[\"" + this.ClientID + "\"].getData()); });</script>");                        
            //} else {

                System.IO.StringWriter oWriter = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter oHWriter = new System.Web.UI.HtmlTextWriter(oWriter);

                if (this.m_eValidateAs == FieldType.Editor) {
                    this.TextMode = TextBoxMode.MultiLine;
                }

                if (DefaultButton != null)
                    this.Attributes["onkeydown"] = String.Format(" if( event != null && event.keyCode == 13 ) {{ document.getElementById( '{0}' ).click(); return false;}}", DefaultButton.ClientID);


                if (Locked) {
                    this.Attributes["onfocus"] = "try{this.blur();}catch(e){}";
                    this.Attributes["onkeypress"] = "return false;";
                    this.Attributes["onkeydown"] = "return false;";
                    this.Attributes["onmousedown"] = "return false;";
                    this.Attributes["onclick"] = "return false;";
                    this.Attributes["ondblclick"] = "return false;";
                    this.Attributes["ondragstart"] = "return false;";
                    this.Attributes["onselectstart"] = "return false;";
                }

                if ((this.Mask != null) && (this.Mask.Length > 0)) {
                    this.Text = this.xc7e65e5b1c57211f();
                    this.Attributes["mask"] = this.Mask;
                    this.Attributes["maskAlpha"] = this.MaskCharAlpha;
                    this.Attributes["maskNumeric"] = this.MaskCharNumeric;
                    this.Attributes["maskAlphaNumeric"] = this.MaskCharAlphaNumeric;
                    this.Attributes["maskDisplay"] = this.MaskCharDisplay;
                    this.Attributes["OnWrongKeyPressed"] = this.OnWrongKeyPressed;
                    this.Attributes["RegexPattern"] = this.RegexPattern;
                    this.Attributes["OnRegexMatch"] = this.OnRegexMatch;
                    this.Attributes["OnRegexNoMatch"] = this.OnRegexNoMatch;
                    this.Attributes["OnKeyDown"] = "javascript:CONTROL_InputMask_KeyDown(event, this);";
                    this.Attributes["OnKeyPress"] = "javascript:CONTROL_InputMask_KeyPress(event, this);";
                    this.Attributes["OnClick"] = "javascript:CONTROL_InputMask_OnClick(event, this);";
                    this.Attributes["OnFocus"] = "javascript:CONTROL_InputMask_GotFocus(this);";
                    this.Attributes["OnBlur"] = "javascript:CONTROL_InputMask_LostFocus(this);";
                    this.Attributes["OnCut"] = "javascript:CONTROL_InputMask_OnCut(this);";
                    this.Attributes["OnPaste"] = "javascript:CONTROL_InputMask_OnPaste(this);";
                    this.Attributes["OnInput"] = "javascript:CONTROL_InputMask_OnInput(event, this);";
                }

                if (m_eValidateAs == FieldType.Currency && System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"] != "nb-NO") {
                    this.Attributes["onblur"] = "this.value = formatCurrency( this.value );";
                    this.Attributes["onfocus"] = "this.value = replaceAll( this.value , '$' , '' );setSelectionRange( this, 0, this.value.length );";
                    this.Text = convert.cDollar(this.Text, 2);
                } else if (m_eValidateAs == FieldType.Percent) {
                    this.Attributes["onblur"] = "this.value = formatPercent( this.value );";
                    this.Attributes["onfocus"] = "this.value = replaceAll( this.value , '%' , '' );setSelectionRange( this, 0, this.value.length );";
                    this.Text = convert.cPercent(convert.cFlt(parse.replaceAll(this.Text, "%", "")) / 100, (MinLength > 0 ? MinLength : 0));

                    //} else if ( m_eValidateAs == FieldType.Phone ) {

                } else if (m_eValidateAs == FieldType.Date) {
                    //this.Attributes["onblur"] = "this.value = formatDate( this.value );";
                    //if ( this.Text != "" ) this.Text = (System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"] == "nb-NO" ? jlib.helpers.no.format.cDefaultDate(convert.cDate( this.Text )) : String.Format( "{0:d}", convert.cDate( this.Text ) ));
                } else {
                    if (this.AjaxValidate && this.Text == "")
                        this.Attributes["onblur"] = Page.ClientScript.GetPostBackClientHyperlink(this, "validate");
                    else if (this.AjaxValidate)
                        this.AutoPostBack = true;
                }

                //oHWriter.Write( "<span class=\"control_container\">" );
                base.Render(oHWriter);

                if (m_eValidateAs == FieldType.Date && this.Enabled)
                    //oHWriter.Write("&nbsp;<a href=\"#\" id=\"" + this.ClientID + "_link\" name=\"" + this.ClientID + "_link\" onclick=\"oCal.select(document.getElementById( '" + this.ClientID + "' ) ,'" + this.ClientID + "_link'," + (System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"] == "nb-NO" ? "'dd.MM.yyyy'" : "'MM/dd/yyyy'") + "); return false;\"><img src=\"/inc/m/calendar.gif\"  border=0 /></a>");
                    oHWriter.Write("&nbsp;<a href=\"#\" id=\"" + this.ClientID + "_link\" name=\"" + this.ClientID + "_link\" onclick=\"oCal.select(document.getElementById( '" + this.ClientID + "' ) ,'" + this.ClientID + "_link'," + (System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"] == "nb-NO" ? "'dd.MM.yyyy'" : "'MM/dd/yyyy'") + "); return false;\"><img src=\"/inc/m/calendar.gif\"  border=0 /></a>");
                //cal11.select(SEventForm.ea_fromDate,'anchor12','yyyy-MM-dd'); return false;

                if (Page.IsPostBack && DisplayValidateStatus && (convert.cStr(ViewState[this.ClientID + "_TextChanged"]) == "true")) {// || !MagicAjax.MagicAjaxContext.Current.IsAjaxCall ) ) {
                    oHWriter.Write("&nbsp;");
                    if (this.validate() == "") {
                        oHWriter.Write("<img src=\"/inc/m/accept.gif\" alt=\"Verdi godtatt\">");
                    } else {
                        oHWriter.Write("<img src=\"/inc/m/exclamation.gif\" alt=\"Verdi ikke godtatt\">");
                        if (AjaxValidateText)
                            oHWriter.Write("<br /><span class=\"validation_error\">" + validate() + "</span>");
                    }
                }
                //oHWriter.Write( "</span>" );

                //if( System.Configuration.ConfigurationManager.AppSettings["common.ajax"] == "true" ){
                //    //m_iHashCode = oWriter.GetStringBuilder().ToString().GetHashCode();
                //    writer.Write( "<input hashcode=\"" + m_iHashCode + "\"" + oWriter.GetStringBuilder().ToString().Substring( 6 ));
                //}else{

                writer.Write(oWriter.GetStringBuilder().ToString());
                //}
            //}            
            //if (this.m_eValidateAs == FieldType.Editor) writer.Write("<script language=\"javascript\">CKEDITOR.basePath='/inc/library/ckeditor/';if(CKEDITOR.instances." + this.ClientID + ")CKEDITOR.remove(CKEDITOR.instances." + this.ClientID + "); $( '#" + this.ClientID + "').ckeditor({skin : 'office2003',toolbar : 'Lexicon'" + (!this.Height.IsEmpty ? ",height:'" + this.Height.Value + (this.Height.Type == UnitType.Percentage ? "%" : "px") + "'" : "") + (!this.Width.IsEmpty ? ",width:'" + this.Width.Value + (this.Width.Type == UnitType.Percentage ? "%" : "px") + "'" : "") + "});</script>");                        
                if (this.m_eValidateAs == FieldType.Editor) writer.Write("<script language=\"javascript\">CKEDITOR.basePath='/inc/library/ckeditor/';if(CKEDITOR.instances." + this.ClientID + ")CKEDITOR.remove(CKEDITOR.instances." + this.ClientID + "); CKEDITOR.replace( '" + this.ClientID + "',{skin : 'kama',toolbar : 'Lexicon'" + (!this.Height.IsEmpty ? ",height:'" + this.Height.Value + (this.Height.Type == UnitType.Percentage ? "%" : "px") + "'" : "") + (!this.Width.IsEmpty ? ",width:'" + this.Width.Value + (this.Width.Type == UnitType.Percentage ? "%" : "px") + "'" : "") + "});$(\"form\").submit(function () { $(\"#" + this.ClientID + "_value\").val(CKEDITOR.instances[\"" + this.ClientID + "\"].getData()); });</script>");                        
        }

        public void RaisePostBackEvent( string sEventArgument ) {
            textbox_TextChanged( null, null );
        }

        /* MASKED BOX LOGIC */

        [Description( "The mask pattern that determines how this control allows or limits inputted characters." ), Category( "Input" )]
        public string Mask {
            get {
                if ( this.ViewState["Mask"] != null ) {
                    return convert.cStr( RuntimeHelpers.GetObjectValue( this.ViewState["Mask"] ) );
                }
                return string.Empty;
            }
            set {
                this.ViewState["Mask"] = value;
            }
        }

        [DefaultValue( "a" ), Description( "The single character that represents all allowed alphabetic instances within the mask." ), Category( "Input" )]
        public string MaskCharAlpha {
            get {
                if ( this.ViewState["MaskCharAlpha"] != null ) {
                    return convert.cStr( RuntimeHelpers.GetObjectValue( this.ViewState["MaskCharAlpha"] ) );
                }
                return string.Empty;
            }
            set {
                if ( value.Length > 0 ) {
                    this.ViewState["MaskCharAlpha"] = value.Substring( 0, 1 );
                } else {
                    this.ViewState["MaskCharNumeric"] = string.Empty;
                }
            }
        }

        [DefaultValue( "x" ), Description( "The single character that represents all allowed alphabetic or numeric instances within the mask." ), Category( "Input" )]
        public string MaskCharAlphaNumeric {
            get {
                if ( this.ViewState["MaskCharAlphaNumeric"] != null ) {
                    return convert.cStr( RuntimeHelpers.GetObjectValue( this.ViewState["MaskCharAlphaNumeric"] ) );
                }
                return string.Empty;
            }
            set {
                if ( value.Length > 0 ) {
                    this.ViewState["MaskCharAlphaNumeric"] = value.Substring( 0, 1 );
                } else {
                    this.ViewState["MaskCharNumeric"] = string.Empty;
                }
            }
        }

        [Category( "Input" ), Description( "The single character that is displayed as the placeholder for potential, allowed  input characters." ), DefaultValue( "_" )]
        public string MaskCharDisplay {
            get {
                if ( this.ViewState["MaskCharDisplay"] != null ) {
                    return convert.cStr( RuntimeHelpers.GetObjectValue( this.ViewState["MaskCharDisplay"] ) );
                }
                return string.Empty;
            }
            set {
                if ( value.Length > 0 ) {
                    this.ViewState["MaskCharDisplay"] = value.Substring( 0, 1 );
                } else {
                    this.ViewState["MaskCharNumeric"] = string.Empty;
                }
            }
        }

        [Description( "The single character that represents all allowed numeric instances within the mask." ), DefaultValue( "n" ), Category( "Input" )]
        public string MaskCharNumeric {
            get {
                if ( this.ViewState["MaskCharNumeric"] != null ) {
                    return convert.cStr( RuntimeHelpers.GetObjectValue( this.ViewState["MaskCharNumeric"] ) );
                }
                return string.Empty;
            }
            set {
                if ( value.Length > 0 ) {
                    this.ViewState["MaskCharNumeric"] = value.Substring( 0, 1 );
                } else {
                    this.ViewState["MaskCharNumeric"] = string.Empty;
                }
            }
        }

        [Category( "Validation" ), Description( "The javascript function or expression which will be called if the value of the control validates as provided by the RegexPattern() property.  This is similar to an event handler" )]
        public string OnRegexMatch {
            get {
                if ( this.ViewState["OnRegexMatch"] != null ) {
                    return convert.cStr( RuntimeHelpers.GetObjectValue( this.ViewState["OnRegexMatch"] ) );
                }
                return string.Empty;
            }
            set {
                this.ViewState["OnRegexMatch"] = value;
            }
        }

        [Category( "Validation" ), Description( "The javascript function or expression which will be called if the value of the control does not validate as provided by the RegexPattern() property.  This is similar to an event handler" )]
        public string OnRegexNoMatch {
            get {
                if ( this.ViewState["OnRegexNoMatch"] != null ) {
                    return convert.cStr( RuntimeHelpers.GetObjectValue( this.ViewState["OnRegexNoMatch"] ) );
                }
                return string.Empty;
            }
            set {
                this.ViewState["OnRegexNoMatch"] = value;
            }
        }

        [Category( "Validation" ), Description( "The javascript function or expression which will be called when a key is pressed inside a mask character instance that is invalid to that instance. This is similar to an event handler" )]
        public string OnWrongKeyPressed {
            get {
                if ( this.ViewState["OnWrongKeyPressed"] != null ) {
                    return convert.cStr( RuntimeHelpers.GetObjectValue( this.ViewState["OnWrongKeyPressed"] ) );
                }
                return string.Empty;
            }
            set {
                this.ViewState["OnWrongKeyPressed"] = value;
            }
        }

        [Description( "The regular expression pattern which will be used to validate the value of the control once the control has lost focus." ), Category( "Validation" )]
        public string RegexPattern {
            get {
                if ( this.ViewState["RegexPattern"] != null ) {
                    return convert.cStr( RuntimeHelpers.GetObjectValue( this.ViewState["RegexPattern"] ) );
                }
                return string.Empty;
            }
            set {
                this.ViewState["RegexPattern"] = value;
            }
        }

        [Browsable( false )]
        public string TextUnMasked {
            get {
                return textbox.x2a31fbf23e400128( this.Text, InputType.AphaNumericOnly );
            }
        }

        private enum InputType {
            None,
            NumericOnly,
            AphaNumericOnly
        }


        private static string x2a31fbf23e400128( string xd2f68ee6f47e9dfb, InputType oInputType ) {
            string text1 = string.Empty;
            int num2 = xd2f68ee6f47e9dfb.Length - 1;
            for ( int num1 = 0; num1 <= num2; num1++ ) {
                switch ( oInputType ) {
                    case InputType.None:
                        text1 = text1 + xd2f68ee6f47e9dfb.Substring( num1, 1 );
                        break;

                    case InputType.NumericOnly:
                        if ( char.IsDigit( xd2f68ee6f47e9dfb, num1 ) ) {
                            text1 = text1 + xd2f68ee6f47e9dfb.Substring( num1, 1 );
                        }
                        break;

                    case InputType.AphaNumericOnly:
                        if ( char.IsLetterOrDigit( xd2f68ee6f47e9dfb, num1 ) ) {
                            text1 = text1 + xd2f68ee6f47e9dfb.Substring( num1, 1 );
                        }
                        break;

                    default:
                        break;
                }

            }
            return text1;
        }

        private void writeScriptBlock() {
            StringBuilder builder1 = new StringBuilder();
            builder1.Append( "\r\n\r\n<script type=text/javascript>\r\n\r\n" );
            builder1.Append( "var $a;function CONTROL_InputMask_OnInput(event,$b){CONTROL_InputMask_ValidateContent($b);CONTROL_InputMask_PutCaretPos($b,CONTROL_InputMask_GetSelectionStart($b));};function CONTROL_InputMask_ValidateContent($b){var $c=\"\";var $d=$b.value;var $e=$b.getAttribute(\"mask\");var $f=$b.getAttribute(\"maskAlpha\");var $g=$b.getAttribute(\"maskNumeric\");var $h=$b.getAttribute(\"maskAlphaNumeric\");var $i=$b.getAttribute(\"maskDisplay\");for(i=0;i<$e.length;i++){if($e.substring(i,(i+1))==$f){while($d.length>0&&(!(($d.substring(0,1).charCodeAt(0)>=65&&$d.substring(0,1).charCodeAt(0)<=90)||($d.substring(0,1).charCodeAt(0)>=97&&$d.substring(0,1).charCodeAt(0)<=122)))){$d=$d.substring(1);};if($d.length>0){$c+=$d.substring(0,1);$d=$d.substring(1);}else{$c+=$i;}}else if($e.substring(i,(i+1))==$g){while($d.length>0&&(!($d.substring(0,1).charCodeAt(0)>=48&&$d.substring(0,1).charCodeAt(0)<=57))){$d=$d.substring(1);};if($d.length>0){$c+=$d.substring(0,1);$d=$d.substring(1);}else{$c+=$i;}}else if($e.substring(i,(i+1))==$h){while($d.length>0&&(!(($d.substring(0,1).charCodeAt(0)>=65&&$d.substring(0,1).charCodeAt(0)<=90)||($d.substring(0,1).charCodeAt(0)>=97&&$d.substring(0,1).charCodeAt(0)<=122)||($d.substring(0,1).charCodeAt(0)>=48&&$d.substring(0,1).charCodeAt(0)<=57)))){$d=$d.substring(1);};if($d.length>0){$c+=$d.substring(0,1);$d=$d.substring(1);}else{$c+=$i;}}else{$c+=$e.substring(i,(i+1));}};$b.value=$c;};function CONTROL_InputMask_ValidatePos($j,$b){if($b==null){$b=$a;};CONTROL_InputMask_PutCaretPos($b,$j);};function CONTROL_InputMask_OnPaste($b){var $d=window.clipboardData.getData(\"Text\");var $j=CONTROL_InputMask_PlaceInMask($b,$d);$a=$b;window.setTimeout(\"CONTROL_InputMask_ValidatePos(\"+$j+\")\",10);CONTROL_InputMask_StopEvent(event);};function CONTROL_InputMask_OnCut($b){var $k=CONTROL_InputMask_GetSelectionStart($b);var $l=CONTROL_InputMask_GetSelectionEnd($b);if($k+$l==$b.value.length){window.clipboardData.setData(\"Text\",$b.value);$b.value=\"\";CONTROL_InputMask_GotFocus($b);}else{var $i=$b.getAttribute(\"maskDisplay\");window.clipboardData.setData(\"Text\",$b.value.substring($k,($k+1)));CONTROL_InputMask_UpdateChar($b,$k,$i);CONTROL_InputMask_PutCaretPos($b,$k);};CONTROL_InputMask_StopEvent(event);};function CONTROL_InputMask_PlaceInMask($b,$d){var $c=\"\";if($d.length>0){var $e=$b.getAttribute(\"mask\");var $f=$b.getAttribute(\"maskAlpha\");var $g=$b.getAttribute(\"maskNumeric\");var $h=$b.getAttribute(\"maskAlphaNumeric\");var $i=$b.getAttribute(\"maskDisplay\");var $m=CONTROL_InputMask_GetSelectionStart($b);$c+=$b.value.substring(0,$m);for(i=$m;i<$e.length;i++){if($e.substring(i,(i+1))==$f){while($d.length>0&&(!(($d.substring(0,1).charCodeAt(0)>=65&&$d.substring(0,1).charCodeAt(0)<=90)||($d.substring(0,1).charCodeAt(0)>=97&&$d.substring(0,1).charCodeAt(0)<=122)))){$d=$d.substring(1);};if($d.length>0){$c+=$d.substring(0,1);$d=$d.substring(1);}else{break;}}else if($e.substring(i,(i+1))==$g){while($d.length>0&&(!($d.substring(0,1).charCodeAt(0)>=48&&$d.substring(0,1).charCodeAt(0)<=57))){$d=$d.substring(1);};if($d.length>0){$c+=$d.substring(0,1);$d=$d.substring(1);}else{break;}}else if($e.substring(i,(i+1))==$h){while($d.length>0&&(!(($d.substring(0,1).charCodeAt(0)>=65&&$d.substring(0,1).charCodeAt(0)<=90)||($d.substring(0,1).charCodeAt(0)>=97&&$d.substring(0,1).charCodeAt(0)<=122)||($d.substring(0,1).charCodeAt(0)>=48&&$d.substring(0,1).charCodeAt(0)<=57)))){$d=$d.substring(1);};if($d.length>0){$c+=$d.substring(0,1);$d=$d.substring(1);}else{break;}}else{$c+=$e.substring(i,(i+1));}};$c+=$b.value.substring(i,$e.length);};$b.value=$c;return i;};function CONTROL_InputMask_LostFocus($b){var $e=$b.getAttribute(\"mask\");if($e!=null&&$b.value==CONTROL_InputMask_GetDisplayMask($b,true)){$b.value=\"\";}else{if($b.value!=null&&$b.value.length>0){var $n=$b.getAttribute(\"RegexPattern\");var $o=$b.getAttribute(\"OnRegexMatch\");var $p=$b.getAttribute(\"OnRegexNoMatch\");if($n!=null&&$n.length>0){var re=new RegExp($n);if($b.value.match(re)){if($o!=null&&$o.length>0){eval($o);}}else{if($p!=null&&$p.length>0){eval($p);}}}}}};function CONTROL_InputMask_KeyDown(event,$b){var $e=$b.getAttribute(\"mask\");if($e!=null&&$e.length>0){var k=CONTROL_InputMask_GetKeyCode(event);if(k==8||(k>=33&&k<=40)||k==46){switch(k){case 8:CONTROL_InputMask_KeyBackspace($b);break;case 33:CONTROL_InputMask_PushPosBegin($b);break;case 34:CONTROL_InputMask_PushPosEnd($b);break;case 35:CONTROL_InputMask_PushPosEnd($b);break;case 36:CONTROL_InputMask_PushPosBegin($b);break;case 37:CONTROL_InputMask_PushPosLeft($b);break;case 38:CONTROL_InputMask_PushPosLeft($b);break;case 39:CONTROL_InputMask_PushPosRight($b);break;case 40:CONTROL_InputMask_PushPosRight($b);break;case 46:CONTROL_InputMask_KeyDelete($b);break;};CONTROL_InputMask_StopEvent(event);}}};function CONTROL_InputMask_KeyPress(event,$b){var $e=$b.getAttribute(\"mask\");if($e!=null&&$e.length>0){var kc=CONTROL_InputMask_GetKeyCode(event);var ss=CONTROL_InputMask_GetSelectionStart($b);if(kc!=9){if(CONTROL_InputMask_KeyIsValid($b,ss,kc)){var ks=String.fromCharCode(kc);CONTROL_InputMask_UpdateChar($b,ss,ks);CONTROL_InputMask_PutCaretPos($b,ss+1);}else{var $q=$b.getAttribute(\"OnWrongKeyPressed\");if($q!=null&&$q.length>0){eval($q);}};CONTROL_InputMask_StopEvent(event);}}};function CONTROL_InputMask_StopEvent(event){if(document.all){event.returnValue=false;}else if(event.preventDefault){event.preventDefault();}};function CONTROL_InputMask_StopEventPropagation(event){event.cancelBubble=true;if(event.stopPropagation){event.stopPropagation();}};function CONTROL_InputMask_GotFocus($b){var $e=$b.getAttribute(\"mask\");if($e!=null&&$e.length>0){var $r=CONTROL_InputMask_GetSelectionStart($b);var $j=CONTROL_InputMask_GetValidPos($b,$r);if($j!=-1){if($b.value==null||$b.value.length==0){$b.value=CONTROL_InputMask_GetDisplayMask($b,true);};CONTROL_InputMask_PutCaretPos($b,$j);}else{$b.blur();}}};function CONTROL_InputMask_OnClick(event,$b){var $e=$b.getAttribute(\"mask\");if($e!=null&&$e.length>0){var $s=CONTROL_InputMask_GetSelectionStart($b);CONTROL_InputMask_PutCaretPos($b,$s);};CONTROL_InputMask_StopEventPropagation(event);};function CONTROL_InputMask_GetKeyCode(event){return(event.keyCode?event.keyCode:event.which?event.which:event.charCode);};function CONTROL_InputMask_KeyDelete($b){var $k=CONTROL_InputMask_GetSelectionStart($b);var $l=CONTROL_InputMask_GetSelectionEnd($b);if($k==0&&$l==$b.value.length){$b.value=\"\";CONTROL_InputMask_GotFocus($b);}else{var $i=$b.getAttribute(\"maskDisplay\");CONTROL_InputMask_UpdateChar($b,$k,$i);CONTROL_InputMask_PutCaretPos($b,$k+1);}};function CONTROL_InputMask_KeyBackspace($b){var $k=CONTROL_InputMask_GetSelectionStart($b);var $l=CONTROL_InputMask_GetSelectionEnd($b);if($k==0&&$l==$b.value.length){$b.value=\"\";CONTROL_InputMask_GotFocus($b);}else{var $i=$b.getAttribute(\"maskDisplay\");CONTROL_InputMask_UpdateChar($b,$k,$i);$j=CONTROL_InputMask_GetValidPos($b,$k-1,true);CONTROL_InputMask_PutCaretPos($b,$j);}};function CONTROL_InputMask_PushPosLeft($b){var k=CONTROL_InputMask_GetSelectionStart($b);if((k-1)>=0){CONTROL_InputMask_PutCaretPos($b,(k-1),true);}};function CONTROL_InputMask_PushPosRight($b){var k=CONTROL_InputMask_GetSelectionStart($b);if((k+1)<$b.value.length){CONTROL_InputMask_PutCaretPos($b,(k+1));}};function CONTROL_InputMask_PushPosBegin($b){CONTROL_InputMask_PutCaretPos($b,0);};function CONTROL_InputMask_PushPosEnd($b){CONTROL_InputMask_PutCaretPos($b,$b.getAttribute(\"mask\").length);};function CONTROL_InputMask_UpdateChar($b,$j,ks){var x=$b.value;var $t=x.substring(0,$j);var $u=x.substring($j+1,x.length);$b.value=$t+ks+$u;};function CONTROL_InputMask_PutCaretPos($b,$j,$v){if($j<=0){$j=0;};if($j>=$b.value.length-1){$j=$b.value.length-1};$j=CONTROL_InputMask_GetValidPos($b,$j,$v);if($j!=-1){if($b.createTextRange){var $w=$b.createTextRange();$w.moveStart(\"character\",$j);$w.moveEnd('character',$j+1-$b.value.length);$w.select();}else if($b.setSelectionRange){$b.focus();$b.setSelectionRange($j,$j+1);}}else{$b.blur();}};function CONTROL_InputMask_GetValidPos($b,$j,$v){if($v==null){$v=false;};if(CONTROL_InputMask_PosIsValid($b,$j)){return $j;}else{var $e=$b.getAttribute(\"mask\");if($v){while($j>=0){if(CONTROL_InputMask_PosIsValid($b,$j)){return $j;};$j--;};while($j<$e.length-1){if(CONTROL_InputMask_PosIsValid($b,$j)){return $j;};$j++;}}else{while($j<$e.length-1){if(CONTROL_InputMask_PosIsValid($b,$j)){return $j;};$j++;};while($j>=0){if(CONTROL_InputMask_PosIsValid($b,$j)){return $j;};$j--;}};return -1;}};function CONTROL_InputMask_PosIsValid($b,$j){var $e=$b.getAttribute(\"mask\");var m=$e.split(\"\");if(m.length>$j){if(m[$j]!=null){var $f=$b.getAttribute(\"maskAlpha\");var $g=$b.getAttribute(\"maskNumeric\");var $h=$b.getAttribute(\"maskAlphaNumeric\");if(m[$j]==$f||m[$j]==$g||m[$j]==$h){return true;}else{return false;}}else{return false;}}else{return false;}};function CONTROL_InputMask_KeyIsValid($b,$j,$x){var m=$b.getAttribute(\"mask\").split(\"\");if(m.length>$j){var $y=m[$j];var $z=$b.getAttribute(\"maskAlpha\");var $A=$b.getAttribute(\"maskNumeric\");var $B=$b.getAttribute(\"maskAlphaNumeric\");if($y==$z){if(($x>=65&&$x<=90)||($x>=97&&$x<=122)){return true;}else{return false;}}else if($y==$A){if($x>=48&&$x<=57){return true;}else{return false;}}else if($y==$B){if(($x>=48&&$x<=57)||($x>=65&&$x<=90)||($x>=97&&$x<=122)){return true;}else{return false;}}else{return false;}}else{return false;}};function CONTROL_InputMask_GetSelectionStart($b){if($b.createTextRange){$C=document.selection.createRange().duplicate();$C.moveEnd(\"character\",$b.value.length);$j=$b.value.lastIndexOf($C.text);if($C.text==\"\")$j=$b.value.length;return $j;}else{return $b.selectionStart;}};function CONTROL_InputMask_GetSelectionEnd($b){if($b.createTextRange){$C=document.selection.createRange().duplicate();$C.moveStart(\"character\",-$b.value.length);$j=$C.text.length;return $j;}else{return $b.selectionEnd;}};function CONTROL_InputMask_GetDisplayMask($b,$D){var $e=$b.getAttribute(\"mask\");if($D==true){var f=\"\";var $f=$b.getAttribute(\"maskAlpha\");var $g=$b.getAttribute(\"maskNumeric\");var $h=$b.getAttribute(\"maskAlphaNumeric\");var $i=$b.getAttribute(\"maskDisplay\");var m=$e.split(\"\");for(mi=0;mi<m.length;mi++){if(m[mi]==$f||m[mi]==$g||m[mi]==$h){f+=$i;}else{f+=m[mi];}};return f;}else{return $e;}}" );
            builder1.Append( "\r\n\r\n</script>\r\n\r\n" );
            this.Page.RegisterClientScriptBlock( "Common.Controls.InputMask", builder1.ToString() );

        }

        private string xc7e65e5b1c57211f() {
            bool flag1 = false;
            string text2 = string.Empty;
            string text3 = textbox.x2a31fbf23e400128( this.Text, InputType.AphaNumericOnly );
            if ( text3.Length > 0 ) {
                int num2 = this.Mask.Length - 1;
                for ( int num1 = 0; num1 <= num2; num1++ ) {
                    if ( this.Mask.Substring( num1, 1 ).IndexOf( this.MaskCharAlpha ) == 0 ) {
                        while ( ( text3.Length > 0 ) && !char.IsLetter( text3, 0 ) ) {
                            text3 = text3.Remove( 0, 1 );
                        }
                        if ( text3.Length > 0 ) {
                            text2 = text2 + text3.Substring( 0, 1 );
                            text3 = text3.Remove( 0, 1 );
                            flag1 = true;
                        } else {
                            text2 = text2 + this.MaskCharDisplay;
                        }
                    }
                        //else if (StringType.StrCmp(this.Mask.Substring(num1, 1), this.MaskCharNumeric, false) == 0)
                    else if ( this.Mask.Substring( num1, 1 ).IndexOf( this.MaskCharNumeric ) == 0 ) {
                        while ( ( text3.Length > 0 ) && !char.IsNumber( text3, 0 ) ) {
                            text3 = text3.Remove( 0, 1 );
                        }
                        if ( text3.Length > 0 ) {
                            text2 = text2 + text3.Substring( 0, 1 );
                            text3 = text3.Remove( 0, 1 );
                            flag1 = true;
                        } else {
                            text2 = text2 + this.MaskCharDisplay;
                        }
                    } else if ( this.Mask.Substring( num1, 1 ).IndexOf( this.MaskCharAlphaNumeric ) == 0 ) {
                        if ( text3.Length > 0 ) {
                            text2 = text2 + text3.Substring( 0, 1 );
                            text3 = text3.Remove( 0, 1 );
                            flag1 = true;
                        } else {
                            text2 = text2 + this.MaskCharDisplay;
                        }
                    } else if ( ( text3.Length > 0 ) && ( this.Mask.Substring( num1, 1 ).IndexOf( text3.Substring( 0, 1 ) ) == 0 ) ) {
                        text2 = text2 + text3.Substring( 0, 1 );
                        text3 = text3.Remove( 0, 1 );
                        flag1 = true;
                    } else {
                        text2 = text2 + this.Mask.Substring( num1, 1 );
                    }
                }
            }
            if ( !flag1 ) {
                text2 = string.Empty;
            }
            return text2;
        }

	}
}
