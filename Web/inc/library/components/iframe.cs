using System;
using System.Collections.Generic;
using jlib.functions;
using System.Security.Cryptography;

namespace jlib.components
{
	/// <summary>
	/// Summary description for iframe.
	/// </summary>
	public class iframe : System.Web.UI.HtmlControls.HtmlGenericControl
	{
		public string FrameSrc { get; set; }
		public bool ResizeToContent { get; set; }
		public int FrameBorder { get; set; }

		public iframe(object o) : this()
		{

		}

		public iframe() : base("iframe")
		{
			this.FrameBorder = 0;
		}

		protected override void Render(System.Web.UI.HtmlTextWriter output) 
		{
			if (this.ResizeToContent)
			{
				output.Write(@"
				<script>
				$(function()
				{
					var $frame = $('#" + this.ClientID + @"');

					function resize" + this.ClientID + @"()
					{
						var $frame = $('#" + this.ClientID + @"');
                        if($frame[0].contentWindow.document.body==null)return;
                        
						$frame.height($frame[0].contentWindow.document.body.offsetHeight + 'px');
					}

					if (fnGetSafariVersion()>0 || fnGetOperaVersion()>0) 
					{ 

						$frame.load(function() { setTimeout(resize" + this.ClientID + @", 0); });

						$frame.each(function()
						{
							var $this = $(this);
							var source = $this.attr('src');
							$this.attr('src', '').attr('src', source);
						});

        			} 
			
					else 
					{
						$frame.load(resize" + this.ClientID + @");

					}

					setInterval(resize" + this.ClientID + @", 500);
				});
				</script>");

				this.Attributes["Width"] = "100%";
				this.Attributes["scrolling"] = "no";
			}
	
			base.Attributes["src"] = this.FrameSrc;
			base.Attributes["frameborder"] = this.FrameBorder.ToString();
			base.Render(output);
		}

	}	
}
