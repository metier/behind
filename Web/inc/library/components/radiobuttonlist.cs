using System;
using jlib.functions;
using System.Reflection;

namespace jlib.components
{
	/// <summary>
	/// Summary description for textbox.
	/// </summary>
    public class radiobuttonlist : System.Web.UI.WebControls.RadioButtonList, iControl
	{
		#region DECLARATIONS
				
        private string m_sDataMember = "";
        private bool m_bRequired = false, m_bDisableSave = false, m_bDirty = false, m_bRendered = false;
        private string m_sFieldDesc = "", m_sLabelClass = "", m_sOnClientClick = "", m_sOnClientChange="";
		
		#endregion

		
		#region PROPERTIES
		public bool ReadOnly { get; set; }
        public bool Rendered {
            get {
                return m_bRendered;
            }
        }

        public override string DataMember {
            get {
                return m_sDataMember;
            }
            set {
                m_sDataMember = value;
            }
        }

        public string FieldDesc {
            get {

                if ( m_sFieldDesc != "" )
                    return m_sFieldDesc;

                if ( m_sDataMember != "" ) {
                    if ( m_sDataMember.IndexOf( "." ) != -1 )
                        return parse.split( m_sDataMember, "." )[1];
                    else
                        return m_sDataMember;
                }
                return this.ID;

            }

            set {
                m_sFieldDesc = value;
            }
        }

        public bool Required {
            get {
                return m_bRequired;
            }
            set {
                m_bRequired = value;
            }
        }

		public bool DisableSave {
			get {
				return m_bDisableSave;
			}
			set {
				m_bDisableSave = value;
			}
		}

        public string validate() {

            if ( Required && this.SelectedValue == "" )
				return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} requires a selection." : "{0} m� v�re avkrysset."), this.FieldDesc);                    
				
            return "";
        }

        public override string SelectedValue {
            get {
                if ( !m_bDirty && base.SelectedValue == "" && Page.IsPostBack ) 
                    return convert.cStr( Page.Request.Form[this.UniqueID] );

                return base.SelectedValue;
            }
            set {
                m_bDirty = true;
                base.SelectedValue = value;
            }
        }

        public string OnClientClick {
            get {
                return m_sOnClientClick;
            }
            set {
                m_sOnClientClick = value;                
            }
        }

        public string OnClientChange {
            get {
                return m_sOnClientChange;
            }
            set {
                m_sOnClientChange = value;                
            }
        }				                

        public override int SelectedIndex {
            get {
                if ( !m_bDirty && base.SelectedIndex < 0 && Page.IsPostBack ) {
                    try {
                        return this.Items.IndexOf( this.Items.FindByValue( convert.cStr( Page.Request.Form[this.UniqueID] ) ) );
                    } catch ( Exception ) { }
                }

                return base.SelectedIndex;
            }
            set {
                m_bDirty = true;
                base.SelectedIndex = value;
            }
        }

		public string LabelClass {
			get {
				return m_sLabelClass;
			}
			set {
				m_sLabelClass = value;
			}
		}

        public override System.Web.UI.WebControls.ListItem SelectedItem {
            get {
                if ( !m_bDirty && base.SelectedItem == null && Page.IsPostBack ) {
                    try {
                        return this.Items.FindByValue( convert.cStr( Page.Request.Form[this.UniqueID] ) );
                    } catch ( Exception ) { }
                }
                return base.SelectedItem;
            }
        }

        public void setVaildateFailure( bool bFailure ) {

        }

        public object getValue() {
            return this.SelectedValue;
        }

        public iControl setValue(object oValue) {
            return setValue(oValue, false);
        }
        public iControl setValue(object oValue, bool bIgnoreError) {
            if (oValue == null) this.SelectedValue = null;
            else this.SelectedValue = convert.cStr(oValue);
            if (this.SelectedValue != convert.cStr(oValue) && !bIgnoreError) throw new Exception("Trying to set dropdown to a value that is not in the list!");
            return this;
        }

        protected override void Render( System.Web.UI.HtmlTextWriter writer ) {
            m_bRendered = true;
            if( this.SelectedIndex > -1 )
                this.Items[this.SelectedIndex].Selected = true;

            System.IO.StringWriter o = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter a = new System.Web.UI.HtmlTextWriter( o );

            base.Render( a );
            string sHTML = o.ToString();
            if( LabelClass != "" ) sHTML=parse.replaceAll(o.ToString(),"<label ", "<label class=\"" + LabelClass + "\" ");
            if (OnClientClick != "") sHTML = parse.replaceAll(o.ToString(), "<input ", "<input onclick=\"" + OnClientClick + "\" ");
            if (OnClientChange != "") sHTML = parse.replaceAll(o.ToString(), "<input ", "<input onchange=\"" + OnClientChange + "\" ");

            writer.Write(sHTML);            
        }

		#endregion

		#region METHODS
		
		#endregion

		
	}
}
