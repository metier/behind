using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.functions;

namespace jlib.components
{
	/// <summary>
	/// Summary description for textbox.
	/// </summary>
    public class listbox : System.Web.UI.WebControls.ListBox, iControl//, IPostBackEventHandler
	{
		public bool ReadOnly { get; set; }
		private string m_sDataMember	= "";
		private bool m_bRequired		= false;
		private bool m_bDisableSave = false;
		private string m_sFieldDesc		= "";
        private bool m_bAjaxValidate = false, m_bAjaxValidateText = true, m_bRendered = false;
        private bool m_bDirty = false;
        
        private bool m_bDisplayValidateStatus = false;
        public bool Rendered {
            get {
                return m_bRendered;
            }
        }
        public bool DisplayValidateStatus {
            get {
                return m_bDisplayValidateStatus;
            }
            set {
                m_bDisplayValidateStatus = value;
            }
        }

		public bool DisableSave {
			get {
				return m_bDisableSave;
			}
			set {
				m_bDisableSave = value;
			}
		}

        public override string SelectedValue {
            get {
                if (!m_bDirty && Page.IsPostBack)
                    return convert.cStr(Page.Request.Form[this.UniqueID]);

                return base.SelectedValue;
            }
            set {
                m_bDirty = true;
                base.SelectedValue = value;
            }
        }

        public string SelectedText {
            get {
                return (this.SelectedItem == null ? "" : this.SelectedItem.Text);
            }
            set {
                try {
                    this.SelectedIndex = this.Items.IndexOf(this.Items.FindByText(value));
                } catch (Exception) { }
            }
        }

        public override int SelectedIndex {
            get {
                if (!m_bDirty && Page.IsPostBack && this.Items.Count > 0) {
                    try {
                        this.SelectedIndex = this.Items.IndexOf(this.Items.FindByValue(Page.Request.Form[this.UniqueID]));
                    } catch (Exception) { }
                }
                return base.SelectedIndex;
            }
            set {
                m_bDirty = true;
                base.SelectedIndex = value;
                listbox_SelectedIndexChanged(null, null);
            }
        }

        public bool AjaxValidate {
            get {
                return m_bAjaxValidate;
            }
            set {
                m_bAjaxValidate = value;
            }
        }

        // Should we display Text as a part of the Ajax Validate
        public bool AjaxValidateText {
            get {
                return m_bAjaxValidateText;
            }
            set {
                m_bAjaxValidateText = value;
            }
        }

        
		public override string DataMember{
			get{
				return m_sDataMember;
			}
			set{
				m_sDataMember	= value;
			}
		}


		public string FieldDesc{
			get{

				if ( m_sFieldDesc != "" )
					return m_sFieldDesc;

				if ( m_sDataMember != "" ){
					if ( m_sDataMember.IndexOf( "." ) != -1 )
						return parse.split( m_sDataMember, "." )[ 1 ];
					else
						return m_sDataMember;
				}
				return this.ID;			
				
			}

			set{
				m_sFieldDesc	= value;
			}
		}

		public bool Required{
			get{
				return m_bRequired;
			}
			set{
				m_bRequired	= value;
			}
		}


        public override void DataBind() {
            if (this.DataSource as System.Xml.XmlNodeList != null) {
                System.Xml.XmlNodeList oList = this.DataSource as System.Xml.XmlNodeList;
                for (int x = 0; x < oList.Count; x++) {
                    string sValue=(this.DataValueField.StartsWith("@") ? xml.getXmlAttributeValue(oList[x], DataValueField.Substring(1)) : xml.getXmlNodeTextValue(oList[x], DataValueField));
                    if( this.Items.FindByValue(sValue)==null) this.Items.Add(new ListItem((this.DataTextField.StartsWith("@") ? xml.getXmlAttributeValue(oList[x], DataTextField.Substring(1)) : xml.getXmlNodeTextValue(oList[x], DataTextField)), sValue));
                }
            } else {
                try {
                    base.DataBind();
                } catch (Exception) {
                    this.SelectedValue = convert.cStr((this.DataSource as System.Data.DataTable).Rows[0][this.DataValueField]);
                    base.DataBind();
                }
            }
            if (!m_bDirty && Page.IsPostBack && convert.cStr(Page.Request.Form[this.UniqueID]) != ""){
                try {
                    this.SelectedIndex = this.Items.IndexOf(this.Items.FindByValue(Page.Request.Form[this.UniqueID]));
                } catch (Exception) { }
            }
        }
		public string validate() {

            if ( Required && this.SelectedValue == "" ) {
                listbox_SelectedIndexChanged( null, null );
				return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} requires a selection." : "{0} m� velges."), this.FieldDesc);                    				
            }

			return "";
		}

		public void setVaildateFailure( bool bFailure ){
			
		}


        public object getValue() {
            return this.SelectedValue;
        }

        public iControl setValue(object oValue) {
            this.SelectedValue  = convert.cStr( oValue );
            return this;
        }

        protected override void OnInit( EventArgs e ) {
            this.Page.PreRender += new EventHandler( Page_PreRender );
            this.Load += new EventHandler( listbox_Load );            
            this.SelectedIndexChanged += new EventHandler( listbox_SelectedIndexChanged );
            base.OnInit( e );
        }

        void Page_PreRender( object sender, EventArgs e ) {
            ViewState[this.ClientID + "_Required"] = this.Required;
        }

        void listbox_Load( object sender, EventArgs e ) {
            if ( ViewState[this.ClientID + "_Required"] != null )
                this.Required = convert.cBool( ViewState[this.ClientID + "_Required"] );
        }


        public void listbox_SelectedIndexChanged( object sender, EventArgs e ) {
            ViewState[this.ClientID + "_SelectedIndexChanged"] = "true";
            if ( sender != null ) Page.ClientScript.RegisterStartupScript( this.GetType(), this.ClientID + "_focus", String.Format( "document.getElementById( '{0}' ).focus();", this.ClientID ), true );
            //Page.Form.DefaultFocus = this.ClientID;            
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer){
            m_bRendered = true;
            if ( this.AjaxValidate ) {
                //this.Attributes["onblur"] = Page.ClientScript.GetPostBackClientHyperlink( this, "validate" );
                this.AutoPostBack = true;
            }

            //writer.Write("<span class=\"control_container\">");
            base.Render(writer);
            if ( Page.IsPostBack && DisplayValidateStatus && ( convert.cStr( ViewState[this.ClientID + "_SelectedIndexChanged"] ) == "true" )){// || !MagicAjax.MagicAjaxContext.Current.IsAjaxCall ) ) {
                writer.Write( "&nbsp;" );
                if ( this.validate() == "" ) {
                    writer.Write( "<img src=\"/inc/m/accept.gif\" alt=\"Verdi godtatt\">" );
                } else {
                    writer.Write( "<img src=\"/inc/m/exclamation.gif\" alt=\"Verdi ikke godtatt\">" );
                    if ( AjaxValidateText )
                        writer.Write( "<br /><span class=\"validation_error\">" + validate() + "</span>" );
                }
            }
            //writer.Write("</span>");
            
        }

        //public new void RaisePostBackEvent( string sEventArgument ) {
        //    listbox_SelectedIndexChanged( null, null );
        //}

	}
}
