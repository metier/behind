using System;
using System.Web.UI;
using jlib.functions;

using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Text;
using System.Web.UI.WebControls;

namespace jlib.components
{
	/// <summary>
	/// Summary description for hidden.
	/// </summary>
    public class hidden : System.Web.UI.HtmlControls.HtmlInputHidden, iControl
	{
		public bool ReadOnly { get; set; }
        public hidden() {
            DataMember = "";
        }
        private bool m_bDirty = false, m_bRendered=false;

        public bool Rendered {
            get {
                return m_bRendered;
            }
        }
		public override string ClientID {
			get {
                RenderClientID = true;
				return base.ClientID;
			}
		}
        
        public string TranslationKey { get; set; }
        public string DataMember { get; set; }
        public string CssClass { get; set; }
        public bool DisableSave { get; set; }
        public bool RenderClientID { get; set; }
        
        public bool Required {
            get {
                return true;
            }
            set {
            }
        }

		public bool Enabled {
			get {
				return true;
			}
			set { }
		}
		

		public string validate() {return "";}


        public object getValue() {
            return this.Value;
        }

        public iControl setValue(object oValue) {
            this.Value = convert.cStr(oValue);
            return this;
        }

        public void setVaildateFailure(bool bFailure) { }

		public override string Value {
			get {
				if (!m_bDirty && base.Value == "" && Page.IsPostBack)
					return convert.cStr(Page.Request.Form[this.UniqueID]);

				return base.Value;
			}
			set {
				base.Value = parse.replaceAll(value,"\"", "&quot;");
				m_bDirty = true;
			}
		}

        protected override void OnInit(EventArgs e) {
            //this.Load += new EventHandler(label_Load);            
            base.OnInit(e);
            if (!Page.IsPostBack && TranslationKey != "") {
                webpage oPage = (Page as webpage);
                if (oPage != null) {
                    this.Value = helpers.translation.translate(oPage.Language, this.Value, Page, TranslationKey).Str();                    
                }
            }            
        }

		protected override void Render(HtmlTextWriter writer) {
            m_bRendered = true;
			writer.Write( "<input" + (CssClass ==""?"": " class='" + CssClass +"'") + " type=\"hidden\" name=\"" + this.UniqueID + "\"" + ( RenderClientID ? " id=\"" + this.ClientID + "\"" : "" ) + ( this.Value == "" ? "" : " value=\"" + this.Value + "\"" ) + ">");
			//System.IO.StringWriter oWriter = new System.IO.StringWriter();
			//HtmlTextWriter oHWriter = new HtmlTextWriter(oWriter);
			//base.Render(oHWriter);
			//if( this.Value != "" &&  oWriter.GetStringBuilder().ToString().IndexOf( "value=" ) == -1 )
			//    writer.Write( parse.replaceAll( oWriter.GetStringBuilder().ToString(), "<input ", "<input value=\"" + System.Web.HttpUtility.HtmlEncode( this.Value ) + "\" " ));
			//else
			//    writer.Write(oWriter.GetStringBuilder().ToString());
		}
     
	}
}
