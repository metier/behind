using System;
using System.Web.UI;
using jlib.functions;

using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Text;
using System.Web.UI.WebControls;

namespace jlib.components
{
	/// <summary>
	/// Summary description for htmlbutton
	/// </summary>
    public class htmlbutton : System.Web.UI.HtmlControls.HtmlButton
	{

        private bool m_bRendered = false;

		protected override void OnInit(EventArgs e) {
			base.OnInit(e);
			this.Attributes["name"] = parse.replaceAll(this.ID, Char.ToString(this.IdSeparator), "_");
			this.Attributes["value"] = "1";
            //this.Attributes["type"] = "submit";
		}
		public override string UniqueID {
			get {
				return this.Attributes["name"];
				//return base.UniqueID;
			}
		}
        public bool Rendered {
            get {
                return m_bRendered;
            }
        }
        //button, reset, submit
        public string Type {
            get {
                return this.Attributes["type"];
            }
            set {                
                this.Attributes["type"] = value;
            }
        }
        protected override void Render(HtmlTextWriter writer) {
            m_bRendered = true;
            if (this.Type == "") this.Type = "submit";
            base.Render(writer);
        }
	}
}
