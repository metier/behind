﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.components {
    public class exception: System.Exception {
        private string m_sUserMessage = "";
        public exception(string sUserMessage): base() {
            m_sUserMessage = sUserMessage;
        }
        public exception(string sUserMessage, string sMessage): base(sMessage) {
            m_sUserMessage = sUserMessage;
        }
        public exception(string sUserMessage, string sMessage, Exception oInnerException): base(sMessage, oInnerException) {
            m_sUserMessage = sUserMessage;
        }
        public string UserMessage {
            get {
                return m_sUserMessage;
            }
            set {
                m_sUserMessage = value;
            }
        }        
    }
}
