﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Xml;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using jlib.components.button;
using jlib.db;
using jlib.functions;
using jlib.components;
using System.Web.UI.DataVisualization.Charting;

namespace jlib.components {
    public class chart : System.Web.UI.DataVisualization.Charting.Chart {
        private string m_sCacheKey = "";
        private DateTime m_oMaxAge = DateTime.Now;

        public string CacheKey {
            get {
                return convert.cStr(m_sCacheKey);
            }
            set {
                m_sCacheKey = value;
            }
        }
        public DateTime MaxAge {
            get {
                return m_oMaxAge;
            }
            set {
                m_oMaxAge = value;
            }
        }
        public bool IsCached {
            get {
                return Url != "";
            }
        }
        public string Url {
            get {
                if (CacheKey != "") {
                    string sFileName = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["temp.dir"] + (ConfigurationManager.AppSettings["temp.dir"].EndsWith("/") ? "" : "/") + "charts/" + CacheKey + ".png");
                    if (System.IO.File.Exists(sFileName) && System.IO.File.GetLastWriteTime(sFileName) > MaxAge) return ConfigurationManager.AppSettings["temp.dir"] + (ConfigurationManager.AppSettings["temp.dir"].EndsWith("/") ? "" : "/") + "charts/" + CacheKey + ".png";
                }
                return "";
            }
        }
        protected override void Render(HtmlTextWriter writer) {
            jlib.components.htmltextwriter oWriter = new htmltextwriter(new System.IO.StringWriter());
            base.Render(oWriter);
            string sHTML = oWriter.ToString();
            if (CacheKey != "") {
                string sSrc = parse.inner_substring(sHTML, "src=\"", null, "\"", null);
                string sFileName = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["temp.dir"] + (ConfigurationManager.AppSettings["temp.dir"].EndsWith("/") ? "" : "/") + "charts/" + CacheKey + ".png");
                if (Url == "") {
                    if (System.IO.File.Exists(sFileName)) io.delete_file(sFileName);
                    if (!System.IO.File.Exists(sFileName)) {
                        jlib.net.HTTP oHTTP = new jlib.net.HTTP();
                        io.write_file(sFileName, oHTTP.GetBinary("http://" + System.Web.HttpContext.Current.Request.Url.Host + ":" + System.Web.HttpContext.Current.Request.Url.Port.ToString() + sSrc, null, null));
                        if (sHTML.IndexOf("<map") > -1) io.write_file(sFileName.Substring(0, sFileName.Length - 3) + "map", "<map" + parse.inner_substring(sHTML, "<map", null, "</map>", null) + "</map>");
                    }
                }
                if (Url != "") {
                    sHTML = parse.replaceAll(sHTML, sSrc, Url);
                    if (sHTML.IndexOf("<map") == -1 && System.IO.File.Exists(sFileName.Substring(0, sFileName.Length - 3) + "map")) sHTML = parse.replaceAll(sHTML, "<img ", "<img usemap=\"#" + this.ClientID + "ImageMap\" ") + io.read_file(sFileName.Substring(0, sFileName.Length - 3) + "map");
                }
            }
            writer.Write(sHTML);
        }
    }
}
