using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using jlib.components.button;
using jlib.components;
using jlib.db;
using jlib.functions;


namespace jlib.controls.ajaxpane
{
    [ParseChildren( false )]
    public class index : System.Web.UI.UserControl {
        public label ContentContainer;

        protected override void OnInit(EventArgs e) {
            while (this.Controls.Count > 1) ContentContainer.Controls.Add(this.Controls[1]);
            base.OnInit(e);
        }

        public string RenderContent() {
            htmltextwriter oHTMLWriter = new htmltextwriter(new System.IO.StringWriter());
            //for(int x=0;x<this.Controls.Count;x++)this.Controls[x].RenderControl(oHTMLWriter);
            this.RenderChildren(oHTMLWriter);
            return oHTMLWriter.ToString();
        }
        protected override void Render(HtmlTextWriter writer) {
            htmltextwriter oHTMLWriter = new htmltextwriter(new System.IO.StringWriter());
            base.Render(oHTMLWriter);            
            writer.Write("<div" + oHTMLWriter.ToString().Substr(5,-5) + "div>");
            //writer.Write( parse.replaceAll( "<span" + Tag + oWriter.GetStringBuilder().ToString().Substring(5, oWriter.GetStringBuilder().ToString().Length - 5 - 7) + "</" + Tag + ">", ( bRenderClientID ? "1" : "" ) +"id=\"" + this.ClientID + "\"", "" ));
        }
    }
}
