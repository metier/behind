using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using jlib.components.button;
using jlib.components;
using jlib.db;
using jlib.functions;


namespace jlib.controls.ajaxpanel
{

    [ParseChildren( true )]
	public class index : System.Web.UI.UserControl
    {

		protected hidden ctrUrl;
		protected HtmlContainerControl ctrContainer;

		public string CurrentUrl {
			get {
				return ctrUrl.Value;
			}
			set {
				ctrUrl.Value = value;
			}
		}

		public String Style {
			get {				
				return ctrContainer.Style.Value;
			}
			set {
				ctrContainer.Style.Value = value;
				//for( int x = 0; x < value.Count;x++ )
				//    ctrContainer.Style.Add( value.Value
			}
		}

		public PlaceHolder Content {
			set {
				ctrContainer.Controls.Clear();
				while (value.Controls.Count > 0)
					ctrContainer.Controls.Add(value.Controls[0]);
			}
			//get {
			//    return ctrContainer;
			//}
		}


		void Page_PreRender(object sender, EventArgs e) {
			Page.RegisterClientScriptBlock("jlib.controls.ajaxpanel.ajaxpanel.js", "<script type=\"text/javascript\" src=\"/inc/library/controls/ajaxpanel/media/ajaxpanel.js\"></script>");
			//if (this.CurrentUrl != "") ctrContainer.Controls.Add(Page.LoadControl(CurrentUrl));
		}

        protected override void Render( HtmlTextWriter writer ) {

			Page.RegisterStartupScript("draw_ajaxpanel_" + this.UniqueID, String.Format("<script>initCommonAjaxPanel( '{0}', '{1}' );" + (this.CurrentUrl == "" ? "" : "$('{0}').showUrl('{2}');") + "</script>", ctrContainer.ClientID, ctrUrl.ClientID, this.CurrentUrl));			
            base.Render( writer );
        }
                
    }
}
