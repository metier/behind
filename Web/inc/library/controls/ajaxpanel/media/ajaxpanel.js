function initCommonAjaxPanel( sPanelID, sUrlID ){

    var oPanel      = $( sPanelID );    
    oPanel.Content  = $( sPanelID );
    oPanel.Url      = $( sUrlID );
    
    oPanel.SearchCounter = 0;
    oPanel.showUrl = commonAjaxPanelShowUrl;
        
    if( oPanel.Initiated ) return;
    oPanel.Initiated = true;        
}

function commonAjaxPanelShowUrl( sUrl ){    
    var oPanel = this;    
    if( oPanel.Url.value == sUrl && oPanel.Content.innerHTML != "") return;
    this.SearchCounter++;
    var iSearchCounter = this.SearchCounter;
    
    oPanel.Content.innerHTML = "<img src=\"/inc/library/controls/ajaxpanel/media/loading_indicator.gif\">&nbsp;Vennligst vent mens innholdet laster...";    
    doCallBack( null, null, sUrl + "&search_counter=" + this.SearchCounter + "&panel_id=" + this.getAttribute("id"), null, "", null, function(oResponse){commonAjaxPanelDataCallback( oPanel,oResponse.responseText, iSearchCounter )  ;} , true, true );    
    oPanel.Url.value = sUrl;
}

function commonAjaxPanelDataCallback( oPanel, sData, iCounter ){
    if( oPanel == null ) oPanel = this;
    if( oPanel.SearchCounter > iCounter ) return;
    
    try{
        oPanel.Content.innerHTML = sData;
    }catch(e){}
}