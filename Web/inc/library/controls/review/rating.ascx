<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="rating.ascx.cs" Inherits="jlib.controls.review.rating" %>
<%@ Register TagPrefix="bids" Namespace="jlib.components" Assembly="jlib" %>
<div runat="server" id="ctrContainer">
    <bids:radiobutton runat="server" id="ctrRating1" selected="true" groupname="rating_overall" text="1" />&nbsp;
    <bids:radiobutton runat="server" id="ctrRating2" groupname="rating_overall" text="2" />&nbsp;
    <bids:radiobutton runat="server" id="ctrRating3" groupname="rating_overall" text="3" />&nbsp;
    <bids:radiobutton runat="server" id="ctrRating4" groupname="rating_overall" text="4" />&nbsp;
    <bids:radiobutton runat="server" id="ctrRating5" groupname="rating_overall" text="5" />
</div>