using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using jlib.components.button;
using jlib.db;
using jlib.functions;


namespace jlib.controls.review
{
    public class rating : System.Web.UI.UserControl
    {
        protected bool m_bRequired = false;

        protected System.Web.UI.HtmlControls.HtmlGenericControl ctrContainer;
        protected jlib.components.radiobutton ctrRating1;
        protected jlib.components.radiobutton ctrRating2;
        protected jlib.components.radiobutton ctrRating3;
        protected jlib.components.radiobutton ctrRating4;
        protected jlib.components.radiobutton ctrRating5;
        

        public bool Required {
            get {
                return m_bRequired;
            }
            set {
                m_bRequired = value;
            }
        }


        public string GroupName {
            get {
                return ctrRating1.GroupName;
            }            
            set {
                ctrRating1.GroupName = value;
                ctrRating2.GroupName = value;
                ctrRating3.GroupName = value;
                ctrRating4.GroupName = value;
                ctrRating5.GroupName = value;
            }
        }

        public int SelectedIndex {
            get {
                if ( GroupName == "" ) GroupName = ctrRating1.UniqueID;
                if ( ctrRating1.Checked ) return 1;
                if ( ctrRating2.Checked ) return 2;
                if ( ctrRating3.Checked ) return 3;
                if ( ctrRating4.Checked ) return 4;
                if ( ctrRating5.Checked ) return 5;
                return 0;
            }
            set {
                ctrRating1.Checked = ( value == 1 );
                ctrRating2.Checked = ( value == 2 );
                ctrRating3.Checked = ( value == 3 );
                ctrRating4.Checked = ( value == 4 );
                ctrRating5.Checked = ( value == 5 );
            }
        }
        
        
        protected override void Render( HtmlTextWriter writer ) {

            if ( GroupName == "" ) GroupName = ctrRating1.UniqueID;
            
            base.Render( writer );
        }
                
    }
}
