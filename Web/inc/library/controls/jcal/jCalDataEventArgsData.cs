﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jlib.DataFramework.QueryBuilder;

namespace jlib.controls.jcal
{
    public class jCalDataEventArgsData : jCalDataEventArgs
	{
		#region ***** CONSTRUCTORS *****
        public jCalDataEventArgsData(jCal oCal, QueryBuilder QueryBuilder, DateTime oStartTime, DateTime oEndTime) : base(oCal, oStartTime, oEndTime){
            this.QueryBuilder = QueryBuilder;
        }
		#endregion

		#region ***** PROPERTIES *****
		public QueryBuilder QueryBuilder { get; set; }
		#endregion

		#region ***** METHODS *****
		#endregion

    }
}
