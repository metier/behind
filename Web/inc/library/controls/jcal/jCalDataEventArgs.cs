﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.controls.jcal
{
    public class jCalDataEventArgs : EventArgs
	{
		#region ***** CONSTRUCTORS *****
        public jCalDataEventArgs(jCal oCal, DateTime oStartTime, DateTime oEndTime) 
		{
            this.Cal = oCal;
            this.StartTime = oStartTime;
            this.EndTime = oEndTime;
            
        }
		#endregion

		#region ***** PROPERTIES *****
        public jCal Cal { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }        
        public jCalEvent Event { get; set; }
        public bool Return { get; set; }
        public string ClientJSBefore { get; set; }
        public string ClientJSAfter { get; set; }

		#endregion

		#region ***** METHODS *****
		#endregion

        public class jCalEvent {
            public jCalEvent(int iID, string sTitle, string sDescription, DateTime oStartTime, DateTime oEndTime, bool bAllDay, string sCustomData) {
                ID = iID;
                Title = sTitle;
                Description = sDescription;
                StartTime = oStartTime;
                EndTime = oEndTime;
                AllDay = bAllDay;
                CustomData = sCustomData;
            }
            public int ID { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public bool AllDay { get; set; }
            public string CustomData { get; set; }
        }

    }
}
