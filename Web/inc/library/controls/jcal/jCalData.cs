using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using jlib.components.button;
using jlib.db;
using jlib.functions;
using System.ComponentModel;
using jlib.DataFramework.QueryBuilder;
using jlib.DataFramework;
using System.Collections.Generic;
using System.Linq;


namespace jlib.controls.jcal 
{
 
	public class jCalData : jCal{
        #region ***** CONSTRUCTORS *****
        public jCalData(string s): base(s) {

        }
        #endregion

        #region ***** PROPERTIES *****
        //private jlib.DataFramework.ICollection _DataSource = null;
        //public jlib.DataFramework.ICollection DataSource {
        //    get {
        //        if (this._DataSource == null) this._DataSource = this.GetDataSource() as DataFramework.ICollection;

        //        return this._DataSource;
        //    }
        //    private set { this.SetDataSource(value); }
        //}
        public jlib.DataFramework.IQueryPackage QuerySource { get; set; }
        public IColumn []ColumnsToInclude { get; set; }
        #endregion

        #region ***** METHODS *****
        protected internal override bool IsDataSourceProvided {
            get {
                return this.QuerySource != null;// || this._DataSource != null;
            }
        }
        protected internal override string RenderData() {
            DataFramework.ICollection oData = QuerySource.Execute();
            return oData.ToJsonArray(null, ColumnsToInclude);
        }
        //protected internal override IListSource GetDataSource()
        //{
			
        //    //Determine valueQuery, Query
        //    string valueQuery = String.IsNullOrWhiteSpace(Page.Request["__ID"]) ? "" : Page.Request["__ID"];
        //    string textQuery = String.IsNullOrWhiteSpace(Page.Request["__QUERY"]) ? "" : Page.Request["__QUERY"];

        //    //Use Query Source to retrieve data
        //    if (this._DataSource == null && this.QuerySource != null)
        //    {
        //        //Get Query Builder
        //        QueryBuilder queryBuilder = this.QuerySource.ToQueryBuilder();

        //        //Throw Bind event
        //        jComboDataEventArgsData e = new jComboDataEventArgsData(this, valueQuery, queryBuilder, textQuery, this.MaxRecords);
        //        this.OnComboDatabind(e);
				
        //        //Paging
        //        queryBuilder.PagingOptions = new DataFramework.Paging.PagingOptions()
        //            {
        //                PageIndex = 0,
        //                PageSize = this.MaxRecords,
        //                SortOrder = this.DataTextField
        //            };

        //        //Apply Query Filter
        //        if (!String.IsNullOrWhiteSpace(textQuery))
        //        {
        //            //Get Condition
        //            BaseCondition condition = this.GetLikeFilterCondition(this.DataTextField, textQuery);

        //            //Add Condition
        //            queryBuilder.Conditions.Add(condition);

        //        }

        //        //Apply value Filter
        //        if (!String.IsNullOrWhiteSpace(valueQuery))
        //        {
        //            //Get Value List
        //            List<string> values = parse.split(valueQuery.Replace("|", ","), ",").ToList();

        //            //Get Condition
        //            BaseCondition condition = this.GetInListFilterConditions(this.DataValueField, values);

        //            //Add Condition
        //            queryBuilder.Conditions.Add(condition);

        //        }

        //        //Execute
        //        this.DataSource = queryBuilder.ToQueryPackage().Execute();
        //    }

        //    return this._DataSource;
        //}
        //protected internal override void SetDataSource(IListSource value) {
        //    //Set Base
        //    this._DataSource = value as jlib.DataFramework.ICollection;
        //}
        //protected internal override IListSource GetInitialValues()
        //{
        //    //Throw Exception
        //    if (this.QuerySource == null)
        //        throw new ArgumentNullException("QuerySource");
			
        //    //Get Query Builder
        //    QueryBuilder queryBuilder = this.QuerySource.ToQueryBuilder();

        //    //Filter By Current Values
        //    queryBuilder.Conditions.Add(this.GetInListFilterConditions(this.DataValueField, this.GetValuesList()));

        //    return queryBuilder.ToQueryPackage().Execute();
			
        //}
        //protected internal BaseCondition GetLikeFilterCondition(string columnName, string FilterValue)
        //{

        //    //Get IColumn
        //    IColumn dataColumn = null;
        //    if (this.QuerySource.Columns.ContainsKey(columnName))
        //        dataColumn = this.QuerySource.Columns[columnName];

        //    //If no data column, then must be virtual
        //    if (dataColumn == null)
        //    {
        //        dataColumn = new BaseColumn<string>(columnName, columnName, "@" + columnName, DbType.String, false);
        //    }

        //    //Create Condition
        //    BaseCondition condition = new LikeCondition(dataColumn, LikeConditionTypes.StartsWith, FilterValue);
			
        //    //Return Condition
        //    return condition;

        //}
        //protected internal BaseCondition GetInListFilterConditions(string columnName, List<string> FilterValues)
        //{

        //    //Get IColumn
        //    IColumn dataColumn = null;
        //    if (this.QuerySource.Columns.ContainsKey(columnName))
        //        dataColumn = this.QuerySource.Columns[columnName];

        //    //If no data column, then must be virtual
        //    if (dataColumn == null)
        //    {
        //        dataColumn = new BaseColumn<string>(columnName, columnName, "@" + columnName, DbType.String, false);
        //    }

        //    //Create Condition
        //    InCondition condition = new InCondition(dataColumn, FilterValues.ToList<object>());
			
        //    //Return Condition
        //    return condition;

        //}
        #endregion

        //#region ***** CONTROL LIFECYCLE *****
        //protected override void Render(HtmlTextWriter writer)
        //{
        //    //Set JS Overload Methods
        //    this.Attributes["getText"] = String.Format("return oRow['{0}'];", this.DataTextField);
        //    this.Attributes["getItem"] = String.Format("return oRow['{0}'];", this.DataTextField);
        //    this.Attributes["getValue"] = String.Format("return oRow['{0}'];", this.DataValueField);

        //    //Call Base Render
        //    base.Render(writer);
        //}
        //#endregion
	}
}

