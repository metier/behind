﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using jlib.components.button;
using jlib.db;
using jlib.functions;
using System.ComponentModel;

namespace jlib.controls.jcal
{
    //Delegates
    public delegate void OnCalDatabindHandler(object sender, jCalDataEventArgs e);
    public delegate void OnCalEventUpdateHandler(object sender, jCalDataEventArgs e);
    public delegate void OnCalEventDeleteHandler(object sender, jCalDataEventArgs e);
    	
	public abstract class jCal : System.Web.UI.HtmlControls.HtmlGenericControl, System.Web.UI.INamingContainer{

        #region ***** EVENTS *****
        public event OnCalDatabindHandler CalDatabind;
        public event OnCalEventUpdateHandler CalEventUpdate;
        public event OnCalEventDeleteHandler CalEventDelete;
        
        protected virtual void OnCalDatabind(jCalDataEventArgs e) {
            if (CalDatabind != null) CalDatabind(this, e);
        }
        protected virtual void OnCalEventUpdate(jCalDataEventArgs e) {
            if (CalEventUpdate != null) CalEventUpdate(this, e);
        }
        protected virtual void OnCalEventDelete(jCalDataEventArgs e) {
            if (CalEventDelete != null) CalEventDelete(this, e);
        }        
        #endregion

        #region ***** CONSTRUCTORS *****
        public jCal(string s){            
            //Editable = true;
            //Weekends = false;
            //Theme = false;
            //FirstHour = 8;
            //SlotMinutes = 30;
            //DefaultView = "month";
            StartDate = DateTime.Now;
        }
        #endregion

        #region ***** PROPERTIES *****

        public string ConfigObject { get; set; }
        public string JSName { get; set; }
        //public string DefaultView { get; set; }
        
        public DateTime StartDate { get; set; }

        #endregion


        #region ***** ABSTRACT PROPERTIES / METHODS *****
        protected internal abstract bool IsDataSourceProvided { get; }
        //protected internal abstract void SetDataSource(IListSource value);
        //protected internal abstract IListSource GetDataSource();
        protected internal abstract string RenderData();
        //protected internal abstract IListSource GetInitialValues();
        #endregion

        //#region ***** METHODS *****
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        //public string validate()
        //{
        //    if (parse.replaceAll(this.GetValue(), "|", "", "\n", "").Trim() == "" && Required)
        //        return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} is a required field." : "{0} må være fylt ut."), this.FieldDescription);

        //    return "";
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="oValue"></param>
        ///// <returns></returns>
        //public components.iControl SetValue(object Value)
        //{
        //    this.Attributes["initial-value"] = convert.cStr(parse.replaceAll(Value, "|", "") == "" ? "" : Value);
        //    this.IsValueInitialized = true;
        //    return this;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        //public object GetValue()
        //{
        //    if (!this.IsValueInitialized) Page_Init(null, null);
        //    return this.Attributes["initial-value"];
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        //public List<string> GetValuesList()
        //{
        //    //Get Value Formatted String
        //    string value = parse.stripLeadingCharacter(parse.stripEndingCharacter(parse.replaceAll(this.GetValue(), false, true, "||", "|"), "|"), "|");

        //    //Convert to List
        //    List<string> values = parse.split(value, "|").ToList<string>();

        //    //Return Values
        //    return values;
        //}
		
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="bFailure"></param>
        //public void setVaildateFailure(bool bFailure)
        //{
        //    if (bFailure)
        //    {
        //        this.Attributes["class"] = parse.replaceAll(this.Attributes["class"], " validate_failure", "") + " validate_failure";
        //    }
        //    else
        //    {
        //        this.Attributes["class"] = parse.replaceAll(this.Attributes["class"], " validate_failure", "");
        //    }
        //}

        //#endregion

        #region ***** CONTROL LIFE CYCLE *****

        protected override void OnInit(EventArgs e)
		{
			Page.InitComplete += new EventHandler(Control_InitComplete);
			base.OnInit(e);
		}

        void Control_InitComplete(object sender, EventArgs e) {
            jlib.components.webpage oPage = (Page as jlib.components.webpage);
            if (oPage.EventTarget == this.ClientID) {
                System.Web.HttpContext.Current.Response.Cache.SetNoStore();                                
                //oPage.Response.ContentType = "application/json";                
                oPage.Response.Clear();
                oPage.Response.ContentType = "text/javascript";

                DateTime oStartDate = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(oPage.Request["Start"].Lng()).ToLocalTime();
                DateTime oEndDate = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(oPage.Request["End"].Lng()).ToLocalTime();
                jCalDataEventArgsData oArgs = new jCalDataEventArgsData(this, null, oStartDate, oEndDate);

                jlib.functions.json.JsonData oData = jlib.functions.json.JsonMapper.ToObject(oPage.Request.Form["_request"]);
                string sAction=oData["Action"].ToString();
                switch (sAction) {
                    case "move":
                    case "delete":
                    case "add":
                        oArgs.Event = new jCalDataEventArgs.jCalEvent(oData["Argument"]["id"].Int(), oData["Argument"]["title"].Str(), oData["Argument"]["description"].Str(),
                            convert.isNumeric(oData["Argument"]["start"]) ? new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(oData["Argument"]["start"].Lng()).ToLocalTime() : oData["Argument"]["start"].Dte(),
                            convert.isNumeric(oData["Argument"]["end"]) ? new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(oData["Argument"]["end"].Lng()).ToLocalTime() : oData["Argument"]["end"].Dte(), oData["Argument"]["allDay"].Bln(), oData["Argument"]["custom"].Str());
                        if (sAction == "move" || sAction == "add") OnCalEventUpdate(oArgs);
                        else if (sAction == "delete") OnCalEventDelete(oArgs);
                        
                        //jlib.functions.json.JsonData oArgument = oData["Argument"];
                        //oData
                                                
                    
                        break;
                    case "refresh":
                        break;
                }
                                
                OnCalDatabind(oArgs);
                oPage.Response.Write(oArgs.ClientJSBefore + ";var _Data={\"Data\":" + RenderData() + ",\"Start\":new Date(" + oArgs.StartTime.Year + "," + (oArgs.StartTime.Month - 1) + "," + oArgs.StartTime.Day + "),\"End\":new Date(" + oArgs.EndTime.Year + "," + (oArgs.EndTime.Month - 1) + "," + oArgs.EndTime.Day + ")};" + oArgs.ClientJSAfter);
                oPage.Response.End();
            }
        }        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer) {
            TagName = "div";
            this.Attributes["class"] = this.Attributes["class"] + " jcal";

            //Render Control into StringWriter
            base.Render(writer);

            jCalDataEventArgsData oArgs = new jCalDataEventArgsData(this, null, StartDate.AddMonths(-6), StartDate.AddMonths(6));
            OnCalDatabind(oArgs);
            //string sData = "{\"Data\":" + RenderData() + ",\"Start\":\"" + (oArgs.StartTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds + "\",\"End\":\"" + (oArgs.EndTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds + "\"}";
            string sData = "{\"Data\":" + RenderData() + ",\"Start\":new Date(" + oArgs.StartTime.Year + "," + (oArgs.StartTime.Month - 1) + "," + oArgs.StartTime.Day + "),\"End\":new Date(" + oArgs.EndTime.Year + "," + (oArgs.EndTime.Month - 1) + "," + oArgs.EndTime.Day + ")}";
            

            //Output JS Header
            writer.Write("<script language=\"javascript\">var _JCalData" + this.ClientID + "=" + sData + ";" + (JSName.Str() == "" ? "" : "var " + JSName + "=null;") + "$(document).ready(function(){ " + JSName + "=$('#" + this.ClientID + "').JCal(" + ConfigObject + ").JCal(\"gotoDate\", " + StartDate.Year + ", " + (StartDate.Month - 1) + ", " + StartDate.Day + ").JCal(\"Instance\");});</script>");
        }

        #endregion        
		
	}
}

