using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using jlib.components.button;
using jlib.db;
using jlib.functions;
using System.ComponentModel;


namespace jlib.controls.jcal 
{
 
	public class jCalAdo : jCal{
        #region ***** CONSTRUCTORS *****
        public jCalAdo(string s) : base(s)
        {

            //Property Default Values
            //this.Sort = "";
        }
        #endregion
        
        //#region ***** PROPERTIES *****
        //public string SQL { get; set; }
        //public string Fields { get; set; }
        //public string Tables { get; set; }
        //public string Filter { get; set; }
        //public string Grouping { get; set; }
        //public string Sort { get; set; }

        ///// <summary>
        ///// 
        ///// </summary>
        //public ado_helper DataHelper
        //{
        //    get
        //    {
        //        if (_DataHelper == null && this.DataSourceType == sqlbuilder.DataServerTypes.SQLServer) 
        //            _DataHelper = new ado_helper();

        //        return _DataHelper;
        //    }
        //    set
        //    {
        //        _DataHelper = value;
        //    }
        //}
        //private ado_helper _DataHelper = null;

        ///// <summary>
        ///// 
        ///// </summary>
        //public string FieldDesc
        //{
        //    get { return base.FieldDescription; }
        //    set { base.FieldDescription = value; }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        //protected internal override bool IsDataSourceProvided
        //{
        //    get
        //    {
        //        return !String.IsNullOrWhiteSpace(this.Tables) || !String.IsNullOrWhiteSpace(this.SQL);
        //    }
        //}
        //#endregion

        //#region ***** METHODS *****
        //public components.iControl SetValue(DataTable DT, string FieldName)
        //{
        //    return this.SetValue(convert.cJoin(DT, FieldName, "|", true));
        //}
        //protected internal override void SetDataSource(IListSource value)
        //{
        //    throw new NotImplementedException();
        //}
        //protected internal override IListSource GetDataSource()
        //{
        //    //Determine Initial ID, Query
        //    string initialId = String.IsNullOrWhiteSpace(Page.Request["__ID"]) ? "" : Page.Request["__ID"];
        //    string query = String.IsNullOrWhiteSpace(Page.Request["__QUERY"]) ? "" : Page.Request["__QUERY"];

        //    //Declare Sql Statement Variable
        //    string sql = "";

        //    //If SQL Provided then use
        //    if(!String.IsNullOrWhiteSpace(this.SQL))
        //    {
        //        //Set Sql Value
        //        sql = this.SQL;
				
        //        //Throw Bind event
        //        jComboDataEventArgsAdo e = new jComboDataEventArgsAdo(this, initialId, sql, query, this.MaxRecords);
        //        this.OnComboDatabind(e);
        //    }

        //    //Else, use constructive sql
        //    else
        //    {
        //        //Throw Bind event
        //        jComboDataEventArgsAdo e = new jComboDataEventArgsAdo(this,initialId, this.Fields, this.Tables, this.Filter + (this.Filter == "" ? "" : " and ") + "LOWER(" + this.DataTextField + ") like '%" + ado_helper.PrepareDB(query).ToLower() + "%'", this.Sort, query, this.MaxRecords);
        //        this.OnComboDatabind(e);

        //        //Build Sql
        //        if (DataSourceType == sqlbuilder.DataServerTypes.SQLServer)
        //        {
        //            sql = String.Format("SELECT TOP {0} {1} FROM {2} WHERE {3} {4}", e.Max, e.Fields, e.Tables, e.Filter, (e.Sort == "") ? "" : " ORDER BY " + e.Sort);
        //        }
        //        else if (DataSourceType == sqlbuilder.DataServerTypes.Oracle)
        //        {
        //            sql = String.Format("SELECT * FROM (SELECT {0} FROM {1} WHERE {2} {3} INNER_TABLE WHERE ROWNUM <= {4}", e.Fields, e.Tables, e.Filter, (e.Sort == "") ? "" : " ORDER BY " + e.Sort, e.Max);
        //        }
        //    }
	
        //    //Get Data
        //    DataTable data =  sqlbuilder.getDataTable(DataHelper, sql);

        //    //Return Data Source
        //    return data;

        //}
        //protected internal override IListSource GetInitialValues()
        //{
        //    //Declare Sql value
        //    string sSQL = "(";
        //    string[] sValues = parse.split(this.GetValue(), ",");
			
        //    //For Each Value
        //    for (int x = 0; x < sValues.Length; x++)
        //    {
        //        if (sValues[x].Trim() != "")
        //        {
        //            sSQL += this.DataValueField + "='" + ado_helper.PrepareDB(sValues[x]) + "' OE ";
        //        }
        //    }
				

        //    DataTable oDT = null;

        //    if (DataSourceType == sqlbuilder.DataServerTypes.SQLServer)
        //        oDT = sqlbuilder.getDataTable(DataHelper, "SELECT " + this.Fields + " FROM " + this.Tables + " WHERE " + (this.Filter == "" ? "" : this.Filter + " AND ") + sSQL.Substring(0, sSQL.Length - 4) + ")");
        //    else if (DataSourceType == sqlbuilder.DataServerTypes.Oracle)
        //        oDT = sqlbuilder.getDataTable(DataHelper, "SELECT " + this.Fields + " FROM " + this.Tables + " WHERE " + (this.Filter == "" ? "" : this.Filter + " AND ") + sSQL.Substring(0, sSQL.Length - 4) + ")");

        //    //Return DataSource
        //    return oDT;
        //}
        //#endregion


        //#region ***** OBSOLETE METHODS / PROPERTIES *****
        //[Obsolete]
        //public string[] getValueMultiple()
        //{
        //    return base.GetValuesList().ToArray();
        //}
        //[Obsolete]
        //public components.iControl setValue(DataTable DT, string FieldName)
        //{
        //    return this.SetValue(DT, FieldName);
        //}
        //#endregion
	}
}

