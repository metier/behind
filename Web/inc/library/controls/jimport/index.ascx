﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="index.ascx.cs" Inherits="jlib.controls.jimport.index" %>

<common:ajaxpane runat="server" id="aContainer" cssclass="jimport">
	<asp:Label runat="server" ID="lFormError" Visible="false" CssClass="form_error" />

	<asp:PlaceHolder runat="server" ID="pStep0" Visible="false">

	<div class="raGroup" class="jimport"> 
		<h2><common:label runat="server" id="lTemplateHeader">Choose a template</common:label></h2> 
	
		<asp:placeholder runat="server" ID="pTemplateSelector">
			<fieldset> 
				<p>Select the template to apply to this data</p>
				<common:dropdown runat="server" id="dTemplate" required="false" fielddesc="Choose template" autopostback="true" datamember="xxx.template">
					<asp:listitem text="(Please choose)" value="" />
				</common:dropdown>
				<br />
			</fieldset>
		</asp:placeholder>

		<fieldset> 
			<common:checkbox runat="server" id="cHeaderRow" text="<b>Data contains header row</b>" LabelClass="inline" datamember="xxx.header_row" />
			<asp:PlaceHolder runat="server" ID="pReplaceDataContainer">
				<br /><common:checkbox runat="server" id="cReplaceData" text="<b>Replace data</b>" LabelClass="inline" />
				&nbsp;&nbsp;<common:radiobutton LabelClass="inline" runat="server" text="Add Unmatched" id="cReplaceDataHandling1" checked="true" groupname="cReplaceDataHandling" />
				<common:radiobutton LabelClass="inline" runat="server" text="Skip Unmatched" id="cReplaceDataHandling2" groupname="cReplaceDataHandling" />
			</asp:PlaceHolder>
		</fieldset>
	</div>

	<div class="raGroup" class="jimport"> 
		<asp:PlaceHolder runat="server" ID="pAllowUpload">
			<h2>Select a file</h2> 
			<fieldset> 
				<p>Supported formats are Excel Spreadsheets (.xls), Comma separated (.csv) and Tab separated (.txt)</p>
				<asp:FileUpload runat="server" ID="fFile" />
			</fieldset>
			<br /><br />
		</asp:PlaceHolder>
	
		<h2><common:label runat="server" id="lPasteHeader">- OR - paste your data from Excel below</common:label></h2> 
		<table id="jimport-data">
			<tr>
				<td><common:textbox runat="server" id="tData" textmode="multiline" width="350px" height="150px" /></td>
				<common:label runat="server" id="lInstructions" tag="td" cssclass="jimport-instructions" />
			</tr>
		</table>
		<common:label runat="server" id="lControls" />
	</div>

	</asp:PlaceHolder>
	
	<asp:PlaceHolder runat="server" ID="pStep1" Visible="false">
		<asp:PlaceHolder runat="server" ID="pLoadColumnPreset">
			<div class="raGroup"> 
				<h2>Load a column preset</h2> 
				<fieldset> 
					<p>Select the saved column preset below, and click Load</p>
					<common:dropdown runat="server" id="dLoadPreset"><asp:listitem>(Please select)</asp:listitem></common:dropdown><common:button.button buttonmode="SubmitButton" runat="server" id="bLoadPreset" text="Load" />
				</fieldset>
			</div>
			<br />
		</asp:PlaceHolder>
		
		<div class="raGroup"> 
			<h2>Review Data</h2>
			<jgrid:jGridAdo runat="server" id="gPreview" DataViewFiltering="false" BindDropdownByID="false" style="width:956px;" JSName="oPreview" Pagination="true" SortOrder="desc" PageSize="20" SortCol="0" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets ra' style='border-top:0px;margin-bottom:0'" DisplayFilter="false" DisplayAdd="false" AutoFilter="true" DeleteConfirmation="Are you sure you wish to delete this row?">
				<Cols>                    
					<jgrid:col id="cID1" runat="server" HText="ID" ColType="Hide" PrimaryKey="true" FormatString="_ID" SortField="_ID" />        
                
					<jgrid:col id="cError" runat="server" HText="" ColType="Text" FormatString="#_error#" />              
                    <jgrid:col id="cWarn" runat="server" HText="" ColType="Text" FormatString="#_warn#" />   
				</Cols>
			</jgrid:jGridAdo>
		</div>
		
		<asp:PlaceHolder runat="server" ID="pSaveColumnPreset">
			<br />
			<div class="raGroup"> 
				<h2>Save column preset</h2> 
				<fieldset>
					<p>Select a column preset to overwrite, or select (New Preset) and type name. Then click Save</p>
					<common:dropdown runat="server" id="dSavePreset" onclientchange="$(this).siblings('span').toggle(this.value=='');"><asp:listitem value="">(New Preset)</asp:listitem></common:dropdown> <span>Please enter name: <common:textbox runat="server" id="tPresetName" /></span><common:button.button runat="server" buttonmode="SubmitButton" id="bSavePreset" text="Save" />
				</fieldset>
			</div>
		</asp:PlaceHolder>

		<br />

	</asp:PlaceHolder>
	
	<asp:PlaceHolder runat="server" ID="pStep2" Visible="false">
		<style>
		DIV.MergeRow
		{
			margin-bottom: 10px;
		}
		TABLE.MergeResults, TABLE.MergeResults TD, TABLE.MergeResults TH
		{
			border: solid 1px silver;
			border-collapse: collapse;
			text-align: left;
		}
		TABLE.MergeResults TH
		{
			background-color: #DADADA;
		}
		TABLE.MergeResults TD, TABLE.MergeResults TH
		{
			border: solid 1px silver;
			border-collapse: collapse;
			padding: 2px;
		}
		DIV.MergeRow SPAN
		{
			display: inline-block;
			width: 50px;
			float: left;
		}
		DIV.MergeRow DIV.Options
		{
			margin-left: 50px;
		}
		</style>

		<div class="raGroup"> 
			<common:label runat="server" id="lSuccessHeading" tag="h2">Import completed</common:label>
			<fieldset>
				<common:label runat="server" id="lSuccessSubline" tag="p">{0} new row(s) added.</common:label>
				<br /><common:hyperlink runat="server" id="hRestart" cssclass="import-reset-link">Click here to import more data.</common:hyperlink>
			</fieldset>
		</div>

		<asp:PlaceHolder runat="server" ID="plhMergeOptions" Visible="false">
		<div class="raGroup"> 
			<input type="hidden" name="ApplyMerge" value="true" />
			<h2>Merge Options</h2>
			<fieldset>
				<p><asp:Literal runat="server" ID="ltrRowsToMerge" /></p>
				<asp:ListView runat="server" ID="lvMergeRows">
					<LayoutTemplate>
						<asp:PlaceHolder runat="server" ID="itemPlaceholder" />
					</LayoutTemplate>
					<ItemTemplate>
						<div class="MergeRow">
							<span><%# Container.DataItemIndex+1 %>.</span>
							<div class="Values"><%# this.MergeResultValues( Container.DataItem as jlib.controls.jimport.ImportMergeResult ) %></div>
							<div class="Options">
								<b>Use:</b>
								<input type="radio" id="Use_<%# ( Container.DataItem as jlib.controls.jimport.ImportMergeResult ).ImportRowIndex %>_Current" name="Use_<%# ( Container.DataItem as jlib.controls.jimport.ImportMergeResult ).ImportRowIndex %>" value="Current" />Current 
								<input type="radio" id="Use_<%# ( Container.DataItem as jlib.controls.jimport.ImportMergeResult ).ImportRowIndex %>_Imported" name="Use_<%# ( Container.DataItem as jlib.controls.jimport.ImportMergeResult ).ImportRowIndex %>" value="Imported" />Imported  
								<input type="radio" id="Use_<%# ( Container.DataItem as jlib.controls.jimport.ImportMergeResult ).ImportRowIndex %>_Both" name="Use_<%# ( Container.DataItem as jlib.controls.jimport.ImportMergeResult ).ImportRowIndex %>" value="Both" checked="checked" />Both
							</div>
						</div>
					</ItemTemplate>
				</asp:ListView>
				<common:button.button onclick="{fnImportBeforeSubmit(this); $('#hJImportNextTab').val(2);document.forms[0].submit();}" ButtonMode="ButtonButton" id="bMergeApply" runat="server" text="Apply" />
			</fieldset>
		</div>
		</asp:PlaceHolder>

	</asp:PlaceHolder>

    <common:checkbox runat="server" ID="cIgnoreWarnings" Text="Ignore warnings" visible="false" /><br />
	<common:button.button onclick="fnImportBeforeSubmit(this); {$('#hJImportNextTab').val(Number($('#hJImportCurrentTab').val())-1);document.forms[0].submit();}" ButtonMode="ButtonButton" id="bImportPrevious" runat="server" text="&lt;&lt; Previous" />
	<common:button.button onclick="fnImportBeforeSubmit(this); $('#hJImportNextTab').val(Number($('#hJImportCurrentTab').val())+1);document.forms[0].submit();" ButtonMode="ButtonButton" id="bImportNext" runat="server" text="Next &gt;&gt;" /> 
	

	<script language="javascript">
	function fnImportBeforeSubmit(ctrl){
	    var o=$('#jimport_name');
	    if(o.length==0)o=$('<input type="hidden" name="jimport_name" id="jimport_name" />').appendTo($(document.forms[0])); 	    
	    o.val($(ctrl).find("div[id*='aContainer_ContentContainer']").attr("id"));
	}

	var sRenderedPreview="";
	<common:label runat="server" id="lScript" />
	var oPreview;
	if(oPreview!=null){
		oPreview.registerEventListener("GridRenderCell", function(oEvent) {
			if (oEvent.Data.Index == c_Header){
			    if( oEvent.Data.ColObject.ID != "cID1" && oEvent.Data.ColObject.ID != "cError" && oEvent.Data.ColObject.ID != "cWarn") {
					if(sRenderedPreview==""){
						sRenderedPreview="<select><option value=''>(Skip)</option>";
						$.each(oPreviewColumns, function(iIndex,oValue){sRenderedPreview+="<option value=\"" + oValue[2] + "\">"+oValue[0]+(oValue[1]!=""?" [" + oValue[1] + "]":"")+"</option>"});
						sRenderedPreview+="</select>";
					}                
					oEvent.Data.HTML+="<br/><br/>"+sRenderedPreview.split("<select").join("<select name='cColOrder_" + (oEvent.Data.Col-1) + "'").split("value=\"" + oPreviewColumnsOrder.split("|")[oEvent.Data.Col-1] + "\"").join("value=\"" + oPreviewColumnsOrder.split("|")[oEvent.Data.Col-1] + "\" selected=\"selected\"");
				}else{
					oEvent.Data.HTML="";
				}
			}
			if(oEvent.Data.Index>-1 &&oEvent.Data.ColObject.ID == "cError" && oEvent.Data.HTML!="") oEvent.Data.HTML="<ul style='color:red;font-weight:bold'><li>* " + stripStr(oEvent.Data.HTML.substring(0,oEvent.Data.HTML.length),"\n").split("\n").join("</li><li>* ") + "</li></ul>";
			if(oEvent.Data.Index>-1 &&oEvent.Data.ColObject.ID == "cWarn" && oEvent.Data.HTML!="") oEvent.Data.HTML="<ul style='color:orange;font-weight:bold'><li>* " + stripStr(oEvent.Data.HTML.substring(0,oEvent.Data.HTML.length),"\n").split("\n").join("</li><li>* ") + "</li></ul>";
		});
		oPreview.render();
	}
	 
	$("div[id*='aContainer_ContentContainer']").addClass("jimport");
                
	</script>
</common:ajaxpane>