using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Xml;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using jlib.components.button;
using jlib.components;
using jlib.db;
using jlib.functions;
using jlib.controls.jgrid;
using System.Linq;
using System.Collections.Generic;

namespace jlib.controls.jimport{

    public delegate void OnColLoad(object sender, string sTemplateID, ref string sColMapping);
    public delegate void OnRowValidate(object sender, string sTemplateID, XmlNode[] xmlColumns, DataRow drImportValues);
    public delegate void OnDataValidate(object sender, string sTemplateID, XmlNode[] xmlColumns, DataTable dtImportValues, out bool bError);
    public delegate void OnBeforeSettingValues(object sender, string sTemplateID, XmlNode[] xmlColumns, DataRow drImportValues, helpers.structures.collection oDataSets);
    public delegate void OnBeforeCommit(object sender, string sTemplateID, XmlNode[] xmlColumns, DataRow drRowValues, helpers.structures.collection oDataSets, int iDataSetIndex);
    public delegate void OnAfterSave(object sender, string sTemplateID, XmlNode[] xmlColumns, DataRow drImportValues, helpers.structures.collection oDataSets, int iRow);
    public delegate void OnRowLookup(object sender, string sTemplateID, XmlNode[] xmlColumns, DataRow drImportValues, string sTableName, helpers.structures.collection oDataSets, ref string sWhere);
    public delegate void OnImportComplete(object sender, string sTemplateID, XmlNode[] xmlColumns, DataTable dtImportValues);
    
    public delegate void OnLoadImportData(object sender, ref DataSet oDS);
    public delegate void OnSaveImportData(object sender, DataSet oDS);

	public delegate jlib.controls.jimport.index.MatchedRowTypes GetMatchedRowTypeDelegate(object sender, string TemplateID, XmlNode[] XmlColumns, jlib.helpers.structures.collection ImportTable, DataRow MatchedRow);

	public class ImportMergeResult
	{
		public jlib.helpers.structures.collection Imported;
		public DataRow Current;
		public int ImportRowIndex;
	}

    [ParseChildren( true )]
    public class index : System.Web.UI.UserControl
    {
		public index()
		{
			//Set Default Get Matched Row Type
			this.GetMatchedRowType = this._GetMatchedRowType;
		}

        protected jlib.controls.ajaxpane.index aContainer;
        protected dropdown dTemplate, dLoadPreset, dSavePreset;
        protected checkbox cHeaderRow, cReplaceData, cIgnoreWarnings;
        protected radiobutton cReplaceDataHandling1, cReplaceDataHandling2;
        protected textbox tData, tPresetName;
        protected FileUpload fFile;
        protected PlaceHolder pLoadColumnPreset, pSaveColumnPreset, pTemplateSelector, pReplaceDataContainer, pAllowUpload, pStep0, pStep1, pStep2;
        protected button bLoadPreset, bSavePreset, bImportPrevious, bImportNext;
        protected Label lFormError;
        protected label lSuccessHeading, lTemplateHeader, lControls, lPasteHeader, lSuccessSubline, lScript, lInstructions;
        protected jGridAdo gPreview;
        protected hyperlink hRestart;        

		protected PlaceHolder plhMergeOptions;
		protected Literal ltrRowsToMerge;
		protected ListView lvMergeRows;
		protected button bMergeApply;

        private string m_sKeyTable = "", m_sKeyName = "", m_sXmlFile="", m_sTemplate="", m_sOnRenderScript="";
        private int iCurrentTab = 0, iNextTab=0, m_iKeyID = 0;
        private bool m_bAjaxMode = false;
        private DataSet m_oDS = new DataSet();
        private ado_helper oTempHelper = new ado_helper();
        private DataTable oTempDT;
        private XmlDocument xmlSettings=new XmlDocument();
        private XmlNode[] xmlColOrder;
        private XmlNode xmlTemplate;
        public static int NumAuxColumns = 5;
        private DataHandling m_oReplaceData = DataHandling.Add_All;
        private System.Reflection.Assembly m_oPluginAssembly;
        private System.Text.StringBuilder m_oErrorMessage = new System.Text.StringBuilder();

        public System.Reflection.Assembly PluginAssembly {
            set {
                m_oPluginAssembly = value;
            }
            get {
                return m_oPluginAssembly;
            }
        }
        public string KeyTable {
            get {                
                return m_sKeyTable;
            }
            set {
                m_sKeyTable = value;
            }
        }
        public string KeyName {
            get {
                return m_sKeyName;
            }
            set {
                m_sKeyName = value;
            }
        }

        public string XmlFile {
            get {
                return m_sXmlFile;
            }
            set {
                m_sXmlFile = value;
            }
        }

        public string Template {
            get {
                return m_sTemplate;
            }
            set {
                m_sTemplate = value;
            }
        }
        public string OnRenderScript {
            get {
                return m_sOnRenderScript;
            }
            set {
                m_sOnRenderScript = value;
            }
        }
                        
        public int KeyID {
            get {
                return m_iKeyID;
            }
            set {
                m_iKeyID = value;
            }
        }

        public string TemplateID {
            get {
                return convert.cStr(m_oDS.Tables[0].Rows[0]["template"]);
            }
        }
        public DataTable ImportDataTable {
            get {
                return (m_oDS.Tables.Count > 2 ? m_oDS.Tables[1] : null);
            }
        }
        public bool HeaderRow{
            get{
                return convert.cBool(m_oDS.Tables[0].Rows[0]["header_row"]);
            }
            set {
                if (!this.IsPostBack) cHeaderRow.Checked = value;
            }
        }
        public DataHandling ReplaceData {
            get{
                return m_oReplaceData;
            }
            set {
                m_oReplaceData = value;
                cReplaceData.Checked = (ReplaceData != DataHandling.Add_All);
                cReplaceDataHandling1.Checked = (ReplaceData == DataHandling.Add_Unmatched);
            }
        }
        public bool AllowUpload {
            get {
                return pAllowUpload.Visible;
            }
            set {
                pAllowUpload.Visible = value;
            }
        }          
        public bool DisableColumnPreset {
            get {
                return !pLoadColumnPreset.Visible;
            }
            set {
                pLoadColumnPreset.Visible = !value;
                pSaveColumnPreset.Visible = !value;
            }
        }
        
        public bool AllowDataReplace {
            get {
                return pReplaceDataContainer.Visible;
            }
            set {
                pReplaceDataContainer.Visible = value;
            }
        }
        public bool AjaxMode {
            get {
                return m_bAjaxMode;
            }
            set {
                m_bAjaxMode = value;
            }
        }

        public enum DataHandling {
            Add_All=0,
            Add_Unmatched=1,
            Skip_Unmatched=2,
			Add_Unmatched_Warn_Matched=3
        }

		public enum MatchedRowTypes 
		{
			NewRecord=0,
            NotMatched=1,
            FullMatch=2,
            PartialMatch=3
        }

		public GetMatchedRowTypeDelegate GetMatchedRowType = null; 

        public event OnColLoad ColLoad;
        public event OnRowValidate RowValidate;
        public event OnDataValidate DataValidate;
        public event OnBeforeSettingValues BeforeSettingValues;
        public event OnBeforeCommit BeforeCommit;
        public event OnAfterSave AfterSave;
        public event OnImportComplete ImportComplete;        
        public event OnRowLookup RowLookup;
        public event OnLoadImportData LoadImportData;
        public event OnSaveImportData SaveImportData;
        
        protected void Page_Init(object sender, EventArgs e) {
            Page.InitComplete+=new EventHandler(Page_InitComplete);
        }

        public string DSN {
            set {
                oTempHelper = new ado_helper(value);
            }
        }

		protected jlib.controls.jimport.index.MatchedRowTypes _GetMatchedRowType(object sender, string TemplateID, XmlNode[] XmlColumns, jlib.helpers.structures.collection ImportTable, DataRow MatchedRow)
		{
			return MatchedRowTypes.FullMatch;
		}

        protected void Page_InitComplete(object sender, EventArgs e) {            
            iCurrentTab = Math.Max(0, convert.cInt(Request["hJImportCurrentTab"]));
            iNextTab = Math.Max(0, convert.cInt(Request["hJImportNextTab"]));
            //if (!this.IsPostBack) cHeaderRow.Checked = true;

            if (LoadImportData == null) {
                oTempDT = sqlbuilder.executeSelect(oTempHelper, "d_temp_data", "key_name", KeyName, "table_name", KeyTable, "pk_id", KeyID);
                if (oTempDT.Rows.Count == 0) oTempDT.Rows.Add(sqlbuilder.setRowValues(oTempDT.NewRow(), "key_name", KeyName, "table_name", KeyTable, "pk_id", KeyID));
                byte[] bData = oTempDT.Rows[0]["binary_value"] as byte[];
                if (bData != null && bData.Length > 0 && (this.IsPostBack || Page.Request["reset"] != "true")) m_oDS = jlib.functions.serialize.AdoNetHelper.DeserializeDataSet(bData);
            } else {
                LoadImportData(this, ref m_oDS);
            }
            if (m_oDS.Tables.Count == 0) {
                m_oDS.Tables.Add("settings");
                m_oDS.Tables[0].Columns.AddRange(new DataColumn[] { new DataColumn("template"), new DataColumn("header_row"), new DataColumn("replace_data"), new DataColumn("xml"), new DataColumn("col_order"), new DataColumn("import_error") });
                m_oDS.Tables[0].Rows.Add(m_oDS.Tables[0].NewRow());
            }
            if (!m_oDS.Tables[0].Columns.Contains("import_error")) m_oDS.Tables[0].Columns.Add(new DataColumn("import_error"));
                                 
            if (XmlFile != "") {
                XmlDocument xmlTemplates = new XmlDocument();
                xmlTemplates.Load(Server.MapPath(XmlFile));
                XmlNodeList oNodes = xmlTemplates.SelectNodes("//template[@import='true']");
                for (int x = 0; x < oNodes.Count; x++) {
                    if (!convert.cBool(xml.getXmlAttributeValue(oNodes[x], "internal"))) dTemplate.Items.Add(new ListItem(convert.cStr(xml.getXmlAttributeValue(oNodes[x], "dropdown-name"), "Import " + xml.getXmlAttributeValue(oNodes[x], "name")), xml.getXmlAttributeValue(oNodes[x], "id")));
                    if (Template == xml.getXmlAttributeValue(oNodes[x], "id") || (!this.IsPostBack && Page.Request["template"] == xml.getXmlAttributeValue(oNodes[x], "id"))) {
                        dTemplate.SelectedIndex = x - 1;
                        xmlTemplate = oNodes[x];
                    } else if ((xml.getXmlAttributeValue(oNodes[x], "id") == convert.cStr(dTemplate.getValue()) && iCurrentTab == 0) || (iCurrentTab > 0 && convert.cStr(m_oDS.Tables[0].Rows[0]["template"]) == xml.getXmlAttributeValue(oNodes[x], "id"))) {
                        xmlTemplate = oNodes[x];
                    }                    
                }
            }

            if (iCurrentTab == 0) {
                if (xmlTemplate != null) {
                    if (xml.getXmlAttributeValue(xmlTemplate, "header-row") != "") cHeaderRow.Checked = convert.cBool(xml.getXmlAttributeValue(xmlTemplate, "header-row"));
                    if (xml.getXmlAttributeValue(xmlTemplate, "replace-data") != "") {
                        cReplaceData.Checked = convert.cInt(xml.getXmlAttributeValue(xmlTemplate, "replace-data")) > 0;
                        cReplaceDataHandling1.Checked = convert.cInt(xml.getXmlAttributeValue(xmlTemplate, "replace-data")) == 1;
                    }
                }
                m_oDS.Tables[0].Rows[0]["header_row"] = (cHeaderRow.Checked ? 1 : 0);
                m_oDS.Tables[0].Rows[0]["replace_data"] = (cReplaceData.Checked ? (cReplaceDataHandling1.Checked ? 1 : 2) : 0);
            }
            m_oReplaceData = (DataHandling)convert.cInt(m_oDS.Tables[0].Rows[0]["replace_data"]);
            gPreview.GridBeforeDatabind += new OnGridBeforeDatabindHandler(gPreview_GridBeforeDatabind);
            
            xmlSettings.LoadXml((convert.cStr(m_oDS.Tables[0].Rows[0]["xml"]) == "" ? "<settings />" : convert.cStr(m_oDS.Tables[0].Rows[0]["xml"])));
            if (xmlSettings.SelectSingleNode("settings/presets") == null) xml.addXmlElement(xmlSettings.DocumentElement, "presets");
            if(!this.IsPostBack) xml.removeXmlNode(xmlSettings.SelectSingleNode("settings/session_data"));
            xml.addXmlElement(xmlSettings.DocumentElement, "session_data");

            if (xmlTemplate != null) {
                XmlNode xmlSessionData = xmlSettings.SelectSingleNode("settings/session_data");
                m_oDS.Tables[0].Rows[0]["template"] = xml.getXmlAttributeValue(xmlTemplate, "id");
                lInstructions.Text = xml.getXmlNodeTextValue(xmlTemplate, "instructions[@import='true']");
                if (xml.getXmlAttributeValue(xmlTemplate, "settings[@import='true']", "replace-data") != "") {
                    if (xml.getXmlAttributeValue(xmlTemplate, "settings[@import='true']", "replace-data") == "hide") AllowDataReplace = false;
                }
                lSuccessHeading.Text = convert.cStr(xml.getXmlAttributeValue(xmlTemplate, "settings[@import='true']", "success-heading"), lSuccessHeading.Text);
                

                XmlNodeList xmlNodes=xmlTemplate.SelectNodes("fields/field[@import='true']");
                for( int x=0;x<xmlNodes.Count;x++){
                    string sHTML = "<table id=\"jimport-controls\"><tr>";
                    string sValue = (Request["import_option_" + x] == null ? null : (Request["import_option_" + x] == xml.getXmlAttributeValue(xmlNodes[x], "prompt") ? "" : Request["import_option_" + x]));

                    if (iNextTab == 1 && convert.cBool(xml.getXmlAttributeValue(xmlNodes[x], "required")) && convert.cStr(sValue) == "") {
                        //sHTML += "<td style=\"color:red;font-weight:bold\">Required: &nbsp;</td>";
                        lFormError.Text += "<li>You have to " + (xml.getXmlAttributeValue(xmlNodes[x], "type") == "text" ? " type a " :  " select ") + xml.getXmlAttributeValue(xmlNodes[x], "fielddesc") + "</li>";
                        iNextTab = 0;
                    }
                    sHTML += (xml.getXmlAttributeValue(xmlNodes[x], "label") == "" ? "" : "<td>" + xml.getXmlAttributeValue(xmlNodes[x], "label") + "</td>");

                    if (sValue != null) {
                        xml.removeXmlNode(xmlSessionData.SelectSingleNode("input[@name='import_option_" + x + "']"));
                        xml.setXmlAttributeValue(xml.addXmlElement(xmlSessionData, "input"), "name", "import_option_" + x, "value", sValue, "mapping", xml.getXmlAttributeValue(xmlNodes[x], "mapping"));
                    }

                    if (xml.getXmlAttributeValue(xmlNodes[x], "type") == "dropdown") {
                        sHTML += "<td><select name=\"import_option_" + x + "\">" + (xml.getXmlAttributeValue(xmlNodes[x], "prompt") == "" ? "" : "<option value=\"\">" + xml.getXmlAttributeValue(xmlNodes[x], "prompt") + "</option>");
                        if (xml.getXmlAttributeValue(xmlNodes[x], "sql") != "") {
                            sHTML += parse.replaceAll(convert.cJoinModelString("<option value=\"{0}\">{1}</option>", sqlbuilder.getDataTable(xml.getXmlAttributeValue(xmlNodes[x], "sql")), new string[] { "id", "name" }), "value=\"" + xml.getXmlAttributeValue(xmlSessionData.SelectSingleNode("input[@name='import_option_" + x + "']"), "value") + "\"", "selected=\"selected\" value=\"" + xml.getXmlAttributeValue(xmlSessionData.SelectSingleNode("input[@name='import_option_" + x + "']"), "value") + "\"");
                        } else if (xml.getXmlAttributeValue(xmlNodes[x], "options") != "") {
                            string[] sArr = parse.split(xml.getXmlAttributeValue(xmlNodes[x], "options"), "|");
                            for (int y = 0; y < sArr.Length; y++) sHTML += String.Format("<option value=\"{0}\">{1}</option>", parse.split(sArr[y], "=")[0], sArr[y].Substring(sArr[y].Contains("=") ? sArr[y].IndexOf("=") + 1 : 0));
                        }
                        sHTML += "</select></td>";
                    } else if (xml.getXmlAttributeValue(xmlNodes[x], "type") == "jcombo")
                        sHTML += "<td><div value='" + sValue + "' name=\"import_option_" + x + "\" id=\"import_option_" + x + "\" jsname=\"import_option_" + x + "\" " + xml.getXmlAttributeValue(xmlNodes[x], "tag") + " " + (xml.getXmlAttributeValue(xmlNodes[x], "sql") == "" ? "" : " data='JComboData" + x + "'") + "></div><script language=\"javascript\">" + (xml.getXmlAttributeValue(xmlNodes[x], "sql") == "" ? "" : "var JComboData" + x + "=" + convert.cJSON(sqlbuilder.getDataTable(xml.getXmlAttributeValue(xmlNodes[x], "sql")), false)) + ";$(document).ready(function(){$('#import_option_" + x + "').JCombo({options:'tag'});});</script></td>";
                    else if (xml.getXmlAttributeValue(xmlNodes[x], "type") == "text")
                        sHTML += "<td><input type=\"text\" name=\"import_option_" + x + "\" onfocus=\"if(this.value=='" + parse.replaceAll(xml.getXmlAttributeValue(xmlNodes[x], "prompt"), "'", "\\'") + "')this.value='';\" value='" + parse.replaceAll((xmlSessionData.SelectSingleNode("input[@name='import_option_" + x + "']") != null ? xml.getXmlAttributeValue(xmlSessionData.SelectSingleNode("input[@name='import_option_" + x + "']"), "value") : xml.getXmlAttributeValue(xmlNodes[x], "prompt")), "'", "\\'") + "'></td>";
                    
                    sHTML+=(xml.getXmlAttributeValue(xmlNodes[x],"disclaimer")=="" ? "" : "<td>"+xml.getXmlAttributeValue(xmlNodes[x],"disclaimer")+"</td>");
                    lControls.Text += sHTML + "</tr></table><br />";                    
                }
                

                if (iNextTab == 1 || iCurrentTab==1) {
                    //+ (convert.cInt(m_oDS.Tables[0].Rows[0]["replace_data"])>0?"":"[not(@pk='true')]") 
                    DataTable oCols = convert.cXmlToDataTable(xmlTemplate.SelectNodes("cols/col"), "name", "@name", "import-description", "@import-description", "mapping", "@mapping");
                    for (int x = 0; x < oCols.Rows.Count; x++) {
                        string sColMapping = convert.cStr(oCols.Rows[x]["mapping"]);
                        if (ColLoad != null) ColLoad(this, convert.cStr(m_oDS.Tables[0].Rows[0]["template"]), ref sColMapping);
                        if (sColMapping == "") {
                            oCols.Rows.RemoveAt(x);
                            x--;
                        }
                    }
                    lScript.Text += "var oPreviewColumns=" + convert.cJSON(oCols, false) + ";var oPreviewColumnsOrder=\"\";";

                    string sColOrder = "";
                    for (int x = 0; x < 100; x++) sColOrder += Request["cColOrder_" + x] + "|";                    
                    m_oDS.Tables[0].Rows[0]["col_order"] = sColOrder;

                    lScript.Text += "oPreviewColumnsOrder='" + (bLoadPreset.IsClicked && convert.cStr(dLoadPreset.getValue()) != "" ? xml.getXmlNodeValue(xmlSettings.SelectSingleNode("//preset[@template='" + xml.getXmlAttributeValue(xmlTemplate, "id") + "'][@name='" + dLoadPreset.getValue() + "']"), "col_order") : sColOrder) + "';";

                    if (bSavePreset.IsClicked) {

                        if (convert.cStr(dSavePreset.getValue()) != "" || tPresetName.Text != "") {
                            xml.removeXmlNode(xmlSettings.SelectSingleNode("//preset[@template='" + xml.getXmlAttributeValue(xmlTemplate, "id") + "'][@name='" + convert.cStr(dSavePreset.getValue(), tPresetName.getValue()) + "']"));
                            XmlNode xmlPreset = xml.addXmlElement(xmlSettings.SelectSingleNode("settings/presets"), "preset", false);
                            xml.setXmlAttributeValue(xmlPreset, "template", xml.getXmlAttributeValue(xmlTemplate, "id"), "name", convert.cStr(dSavePreset.getValue(), tPresetName.getValue()));
                            //xml.addXmlElement(xmlPreset, "col_order", Request["col_order"]);                        
                            xml.addXmlElement(xmlPreset, "col_order", sColOrder);                            
                        }
                    }
                    xmlNodes = xmlSettings.SelectNodes("settings/presets/preset");
                    for (int x = 0; x < xmlNodes.Count; x++) {
                        dSavePreset.Items.Add(xml.getXmlAttributeValue(xmlNodes[x], "name"));
                        dLoadPreset.Items.Add(xml.getXmlAttributeValue(xmlNodes[x], "name"));
                    }
                }
                if (convert.cStr(m_oDS.Tables[0].Rows[0]["col_order"]) != ""){
                    string []sArr=parse.split(m_oDS.Tables[0].Rows[0]["col_order"],"|");
                    xmlColOrder=new XmlNode[sArr.Length];
                    for(int x=0;x<sArr.Length;x++)
                        if(sArr[x].Trim()!="")xmlColOrder[x]=xmlTemplate.SelectSingleNode("cols/col[@mapping='"+sArr[x]+"']");
                }
                for (int x = 0; x < xmlTemplate.SelectNodes("plugins/plugin[@import='true']").Count; x++) {
                    plugin oPlugin = (PluginAssembly==null? jlib.functions.reflection.instantiateObject(jlib.functions.reflection.buildAssemblyPath(xml.getXmlAttributeValue(xmlTemplate.SelectNodes("plugins/plugin")[x], "assembly")), xml.getXmlAttributeValue(xmlTemplate.SelectNodes("plugins/plugin")[x], "class")) : jlib.functions.reflection.instantiateObject(PluginAssembly , xml.getXmlAttributeValue(xmlTemplate.SelectNodes("plugins/plugin")[x], "class")) ) as plugin;
                    oPlugin.attach(this);
                }
            }
        }
        
        void gPreview_GridBeforeDatabind(object sender, GridCellEventArgs e) {
            if (m_oDS.Tables.Count > 1) gPreview.DataSource = new DataView(m_oDS.Tables[1], (gPreview.Pagination ? "[_error]<>'' or [_warn]<>'' or  [_id]<'" + convert.cPadString(gPreview.PageSize + 1, 5, "0", true) + "'" : ""), "[_error] desc, [_warn] desc, [_id]", DataViewRowState.CurrentRows).ToJDataTable();
        }

        protected void Page_Load(object sender, EventArgs e) {
            if (iCurrentTab > 0 && iNextTab == 0) {
                cHeaderRow.Checked = convert.cBool(m_oDS.Tables[0].Rows[0]["header_row"]);
                cReplaceData.Checked = (ReplaceData == DataHandling.Skip_Unmatched);
                cReplaceDataHandling1.Checked = (ReplaceData == DataHandling.Add_Unmatched);
            }
            if (!this.IsPostBack && Request["success"] == "true") {
                iNextTab = 2;
                if (convert.cStr(m_oDS.Tables[0].Rows[0]["import_error"]) != "") {
                    lFormError.Text = convert.cStr(m_oDS.Tables[0].Rows[0]["import_error"]);
                    lFormError.Visible = true;
                }
            } else {

                if (iCurrentTab == 0) {
                    //jlib.helpers.control.collectValues(this, "xxx", m_oDS.Tables[0].Rows[0]);           
                    if (tData.Text != "" || fFile.HasFile) {
                        if (m_oDS.Tables.Count > 1) m_oDS.Tables.RemoveAt(1);
                        m_oDS.Tables.Add("data");
                        m_oDS.Tables[1].Columns.Add("_id");
                        m_oDS.Tables[1].Columns.Add("_imported", typeof(int));
						m_oDS.Tables[1].Columns.Add("_merge_id", typeof(int));
                        m_oDS.Tables[1].Columns.Add("_error");
                        m_oDS.Tables[1].Columns.Add("_warn");
                    }
                    if (xmlTemplate != null) {
                        if (fFile.HasFile) {
                            string sFileName = fFile.FileName;
                            if (!sFileName.ToLower().EndsWith(".xlsx") && !sFileName.ToLower().EndsWith(".xls") && !sFileName.ToLower().EndsWith(".csv") && !sFileName.ToLower().EndsWith(".txt")) {
                                lFormError.Text += "Filetype not recognized. Please make sure the filename ends in .txt, .csv, .xls or .xlsx";
                            } else {
                                SpreadsheetGear.IWorkbook oExcel = SpreadsheetGear.Factory.GetWorkbookSet().Workbooks.OpenFromStream(fFile.FileContent);
                                //attempt to match columns
                                if (HeaderRow && oExcel.Worksheets.Count > 0 && oExcel.Worksheets[0].UsedRange.RowCount > 0) {
                                    m_oDS.Tables[0].Rows[0]["col_order"] = "";
                                    for (int y = 0; y < oExcel.Worksheets[0].UsedRange.ColumnCount; y++) {
                                        if (convert.cStr(oExcel.Worksheets[0].Cells[0, y].Text) != "" && xmlTemplate.SelectSingleNode("cols/col[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='" + parse.replaceAll(oExcel.Worksheets[0].Cells[0, y].Text, "'", "&apos;").ToLower() + "']") != null)
                                            m_oDS.Tables[0].Rows[0]["col_order"] = convert.cStr(m_oDS.Tables[0].Rows[0]["col_order"]) + xml.getXmlAttributeValue(xmlTemplate.SelectSingleNode("cols/col[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='" + parse.replaceAll(oExcel.Worksheets[0].Cells[0, y].Text, "'", "&apos;").ToLower() + "']"), "mapping");
                                        m_oDS.Tables[0].Rows[0]["col_order"] += "|";
                                    }
                                    lScript.Text += "var oPreviewColumnsOrder='" + m_oDS.Tables[0].Rows[0]["col_order"] + "';";
                                }
                                for (int x = (HeaderRow ? 1 : 0); x < oExcel.Worksheets[0].UsedRange.RowCount; x++) {
                                    DataRow oRow = m_oDS.Tables[1].NewRow();
                                    oRow[0] = convert.cPadString(x, 5, "0", true);
                                    bool bHasData = false;

                                    for (int y = 0; y < oExcel.Worksheets[0].UsedRange.ColumnCount; y++) {
                                        if (y + NumAuxColumns >= m_oDS.Tables[1].Columns.Count) m_oDS.Tables[1].Columns.Add("col_" + y);
                                        oRow[y + NumAuxColumns] = convert.cStr(oExcel.Worksheets[0].UsedRange.Cells[x, y].Text).Trim();
                                        if (convert.cStr(oExcel.Worksheets[0].UsedRange.Cells[x, y].Text).Trim() != "") bHasData = true;
                                    }
                                    if (bHasData) m_oDS.Tables[1].Rows.Add(oRow);
                                }
                            }
                        } else if (tData.Text != "") {
                            string[] sData = parse.split(parse.replaceAll(tData.Text, "\r", ""), "\n");

                            //attempt to match columns
                            if (HeaderRow && sData.Length > 0) {
                                m_oDS.Tables[0].Rows[0]["col_order"] = "";
                                string[] sData1 = parse.split(sData[0], "\t");
                                for (int y = 0; y < sData1.Length; y++) {
                                    if (sData1[y] != "") {
                                        XmlNode                 xmlMapping= xmlTemplate.SelectSingleNode("cols/col[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='" + parse.replaceAll(sData1[y], "'", "&apos;", char.ToString((char)160), " ").ToLower() + "']");
                                        if (xmlMapping == null) xmlMapping = xmlTemplate.SelectSingleNode("cols/col[translate(translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),' ','')='" + parse.replaceAll(sData1[y], "'", "&apos;", " ", "", char.ToString((char)160), " ").ToLower() + "']");
                                        if (xmlMapping != null) m_oDS.Tables[0].Rows[0]["col_order"] = convert.cStr(m_oDS.Tables[0].Rows[0]["col_order"]) + xml.getXmlAttributeValue(xmlMapping, "mapping");                                                                                    
                                    }
                                    m_oDS.Tables[0].Rows[0]["col_order"] += "|";
                                }
                                lScript.Text += "var oPreviewColumnsOrder='" + m_oDS.Tables[0].Rows[0]["col_order"] + "';";
                            }
                            for (int x = (HeaderRow ? 1 : 0); x < sData.Length; x++) {
                                string[] sData1 = parse.split(sData[x], "\t");
                                if (sData1.Length > 0) {
                                    DataRow oRow = m_oDS.Tables[1].NewRow();
                                    bool bHasData = false;
                                    oRow[0] = convert.cPadString(x, 5, "0", true);
                                    int iColCounter = 0;
                                    while (true) {
                                        for (int y = 0; y < sData1.Length; y++) {
                                            if (iColCounter + NumAuxColumns >= m_oDS.Tables[1].Columns.Count) m_oDS.Tables[1].Columns.Add("col_" + iColCounter);
                                            oRow[iColCounter + NumAuxColumns] = convert.cIfValue(oRow[iColCounter + NumAuxColumns], "", "\n", "") + sData1[y].Trim();
                                            if (sData1[y].Trim() != "") bHasData = true;
                                            iColCounter++;
                                        }
                                        iColCounter--;
                                        if (!convert.cStr(oRow[iColCounter + NumAuxColumns]).StartsWith("\"") || x >= sData.Length - 1 || iColCounter + parse.split(sData[x + 1], "\t").Length > Math.Max(m_oDS.Tables[1].Columns.Count - NumAuxColumns, (HeaderRow ? parse.split(m_oDS.Tables[0].Rows[0]["col_order"], "|").Length - 1 : 0)) || Math.Max(m_oDS.Tables[1].Columns.Count - NumAuxColumns, (HeaderRow ? parse.split(m_oDS.Tables[0].Rows[0]["col_order"], "|").Length - 1 : 0)) == 1)
                                            break;
                                        else {
                                            sData1 = parse.split(sData[++x], "\t");
                                            if (sData1.Length > 0 && sData1[0].EndsWith("\"")) {
                                                sData1[0] = sData1[0].Substring(0, sData1[0].Length - 1);
                                                if (convert.cStr(oRow[iColCounter + NumAuxColumns]).StartsWith("\"")) oRow[iColCounter + NumAuxColumns] = convert.cStr(oRow[iColCounter + NumAuxColumns]).Substring(1);
                                            }
                                        }
                                    }
                                    if (bHasData) m_oDS.Tables[1].Rows.Add(oRow);
                                }
                            }
                        }
                    }
                }
                //validation rules
                if (iNextTab > 0) {
                    if (m_oDS.Tables.Count < 2 || m_oDS.Tables[1].Rows.Count == 0) {
                        iNextTab = 0;
                        lFormError.Text += "<li>You have to select a file or paste some data from Excel in order to continue.</li>";
                    } else if (convert.cStr(m_oDS.Tables[0].Rows[0]["template"]) == "") {
                        iNextTab = 0;
                        lFormError.Text += "<li>You have to select a template in order to continue.</li>";
                    } else if (xmlTemplate == null) {
                        iNextTab = 0;
                        lFormError.Text += "<li>You need to select a template to base the import on.</li>";
                    }
                }

                if (iNextTab == 2) {
                    for (int x = 0; x < xmlTemplate.SelectNodes("cols/col").Count; x++) {
                        if ((convert.cBool(xml.getXmlAttributeValue(xmlTemplate.SelectNodes("cols/col")[x], "required")) && ("|" + m_oDS.Tables[0].Rows[0]["col_order"] + "|").IndexOf("|" + xml.getXmlAttributeValue(xmlTemplate.SelectNodes("cols/col")[x], "mapping") + "|") == -1)
                            || (ReplaceData == DataHandling.Add_All && convert.cBool(xml.getXmlAttributeValue(xmlTemplate.SelectNodes("cols/col")[x], "add-required")) && ("|" + m_oDS.Tables[0].Rows[0]["col_order"] + "|").IndexOf("|" + xml.getXmlAttributeValue(xmlTemplate.SelectNodes("cols/col")[x], "mapping") + "|") == -1)) {
                            iNextTab = 1;
                            lFormError.Text += "<li>'" + xml.getXmlAttributeValue(xmlTemplate.SelectNodes("cols/col")[x], "name") + "' is required.</li>";
                        }
                    }
                }
                if (iNextTab == 2) {
                    bool bError=false, bWarn=false;
                    for (int x = 0; x < m_oDS.Tables[1].Rows.Count; x++) {
                        if (!convert.cBool(m_oDS.Tables[1].Rows[x]["_imported"])) {
                            m_oDS.Tables[1].Rows[x]["_error"] = "";
                            for (int y = 0; y < xmlColOrder.Length; y++) {
                                if (xmlColOrder[y] != null) {
                                    string sValue = convert.cStr(m_oDS.Tables[1].Rows[x][y + NumAuxColumns]);
                                    if (convert.cBool(xml.getXmlAttributeValue(xmlColOrder[y], "required")) && sValue == "") m_oDS.Tables[1].Rows[x]["_error"] += xml.getXmlAttributeValue(xmlColOrder[y], "name") + " is required.\n";
                                    else if (xml.getXmlAttributeValue(xmlColOrder[y], "validateas").ToLower() == "number" && !convert.isNumeric(sValue) && sValue != "") m_oDS.Tables[1].Rows[x]["_error"] += xml.getXmlAttributeValue(xmlColOrder[y], "name") + " must be numeric.\n";
                                    else if (xml.getXmlAttributeValue(xmlColOrder[y], "validateas").ToLower() == "date" && !convert.isDate(sValue) && sValue != "") m_oDS.Tables[1].Rows[x]["_error"] += xml.getXmlAttributeValue(xmlColOrder[y], "name") + " must be a date.\n";
                                    else if (xml.getXmlAttributeValue(xmlColOrder[y], "validateas").ToLower() == "email" && !convert.isEmail(sValue) && sValue != "") m_oDS.Tables[1].Rows[x]["_error"] += xml.getXmlAttributeValue(xmlColOrder[y], "name") + " must be a valid email address.\n";
                                    else if (sValue != "" && xml.getXmlAttributeValue(xmlColOrder[y], "lookup") != "") {
                                        if (!m_oDS.Tables[1].Columns.Contains("_" + m_oDS.Tables[1].Columns[y + NumAuxColumns])) m_oDS.Tables[1].Columns.Add("_" + m_oDS.Tables[1].Columns[y + NumAuxColumns].ColumnName);
                                        if (convert.cStr(m_oDS.Tables[1].Rows[x]["_" + m_oDS.Tables[1].Columns[y + NumAuxColumns]]) == "") {
                                            DataTable oLookup = sqlbuilder.getDataTable(String.Format(xml.getXmlAttributeValue(xmlColOrder[y], "lookup"), ado_helper.PrepareDB(sValue)));
                                            if (oLookup.Rows.Count == 0) m_oDS.Tables[1].Rows[x]["_error"] = xml.getXmlAttributeValue(xmlColOrder[y], "name") + " '" + sValue + "' was not found.\n";
                                            else m_oDS.Tables[1].Rows[x]["_" + m_oDS.Tables[1].Columns[y + NumAuxColumns]] = oLookup.Rows[0][0];
                                        }
                                    }

                                }
                            }
                            if (RowValidate != null) RowValidate(this, convert.cStr(m_oDS.Tables[0].Rows[0]["template"]), xmlColOrder, m_oDS.Tables[1].Rows[x]);
                            if (convert.cStr(m_oDS.Tables[1].Rows[x]["_error"]) != "") bError = true;
                            if (convert.cStr(m_oDS.Tables[1].Rows[x]["_warn"]) != "") bWarn = true;
                        }
                    }                    
                    if (!bError) {
                        if(DataValidate!=null) DataValidate(this, convert.cStr(m_oDS.Tables[0].Rows[0]["template"]), xmlColOrder, m_oDS.Tables[1], out bError);                        
                    }
                    if (bError || (bWarn && !cIgnoreWarnings.Checked)) {
                        iNextTab = 1;
                        if (bError) lFormError.Text += "<li>The validation of your data failed. Please correct the errors in the <b>Review Data</b>-grid and continue.</li>";
                        else
                        {
                            lFormError.Text += "<li>The validation of your data produced warnings. Check Ignore Warnings and click Next to proceed.</li>";
                            cIgnoreWarnings.Visible = true;
                        }
                    }
                }

                //Perform import
				List<ImportMergeResult> mergeRows = new List<ImportMergeResult>();
				bool applyMerge = convert.cBool(Request["ApplyMerge"]);
                int iCounter = 0;
                if (iNextTab == 2) 
				{
                    for (int x = 0; x < m_oDS.Tables[1].Rows.Count; x++) 
					{
						//If in Merge Apply Mode and doesnt apply, then skip
						string mergeSource = convert.cStr(Request[String.Format("Use_{0}", x)]).ToLower();
						if (applyMerge && mergeSource == "") continue;

						//Set Default Merge Id
						m_oDS.Tables[1].Rows[x]["_merge_id"] = 0;

						//If not already imported
                        if (!m_oDS.Tables[1].Rows[x]["_imported"].Bln() && m_oDS.Tables[1].Rows[x]["_error"].IsNullOrEmpty()) {
                            //Get Mapped Data to columns
                            helpers.structures.collection oSets = getDataMapping(m_oDS.Tables[1], x);

                            //Call events to get values
                            if (BeforeSettingValues != null) BeforeSettingValues(this, convert.cStr(m_oDS.Tables[0].Rows[0]["template"]), xmlColOrder, m_oDS.Tables[1].Rows[x], oSets);
                            if (!m_oDS.Tables[1].Rows[x]["_imported"].Bln() && m_oDS.Tables[1].Rows[x]["_error"].IsNullOrEmpty()) {
                                //Process Each Import Set
                                for (int y = 0; y < oSets.Count; y++) {
                                    //If Import Set Column Name doesnt start with ~
                                    if (!oSets.Keys[y].StartsWith("~")) {
                                        ado_helper oAdo = new ado_helper(xml.getXmlAttributeValue(xmlTemplate, "dsn-key"));
                                        JDataTable oDT = null;
                                        MatchedRowTypes matchType = MatchedRowTypes.NewRecord;

                                        //If not automatic insert
                                        if (ReplaceData != DataHandling.Add_All) {
                                            //Find Matched Record
                                            string sWhere = parse.splitValue(xml.getXmlAttributeValue(xmlTemplate.SelectSingleNode("cols/col[@pk='true']"), "mapping"), ".", 1) + "='" + ado_helper.PrepareDB((oSets[y] as helpers.structures.collection)[parse.splitValue(xml.getXmlAttributeValue(xmlTemplate.SelectSingleNode("cols/col[@pk='true']"), "mapping"), ".", 1)]) + "'";
                                            if (RowLookup != null) RowLookup(this, convert.cStr(m_oDS.Tables[0].Rows[0]["template"]), xmlColOrder, m_oDS.Tables[1].Rows[x], convert.cStr(oSets.Keys[y]), oSets, ref sWhere);
                                            oDT = sqlbuilder.getDataTable(oAdo, "select * from " + oSets.Keys[y] + " where " + sWhere);

                                            //If Multiple Matches
                                            if (oDT.Rows.Count > 1) throw new Exception("Halting import! The row " + sWhere + " has more than one match (count: " + oDT.Rows.Count + ").");

                                            //Determine Row Match Type
                                            if (oDT != null && oDT.Rows.Count > 0) {
                                                //Get Import Table
                                                jlib.helpers.structures.collection importTable = oSets[convert.cStr(oSets.Keys[y])] as jlib.helpers.structures.collection;

                                                //Get Match Type
                                                matchType = this.GetMatchedRowType(this, convert.cStr(m_oDS.Tables[0].Rows[0]["template"]), xmlColOrder, importTable, oDT.Rows[0]);
                                            }
                                        }

                                        //Create New Row
                                        if (oDT == null || (oDT.Rows.Count == 0 && ReplaceData == DataHandling.Add_Unmatched) || matchType == MatchedRowTypes.NotMatched) {
                                            matchType = MatchedRowTypes.NewRecord;

                                            oDT = sqlbuilder.getNewRecord(oAdo, oSets.Keys[y]);
                                        }

                                        //If there is a matched row
                                        if (oDT.Rows.Count > 0) {

                                            //Merge Process
                                            if (applyMerge) {
                                                //Use Current
                                                if (mergeSource == "current") {
                                                    //do nothing
                                                }

                                                //User Imported
                                                if (mergeSource == "imported") {
                                                    //update matched record
                                                    //us already found oDT
                                                }

                                                //Use Current and Imported
                                                if (mergeSource == "both") {
                                                    //Create a new record so imported get created separately
                                                    oDT = sqlbuilder.getNewRecord(oAdo, oSets.Keys[y]);
                                                }

                                                //Update DataRow
                                                if (mergeSource == "imported" || mergeSource == "both") {
                                                    //Update Values
                                                    sqlbuilder.setValues(oDT.Rows[0], (oSets[y] as helpers.structures.collection));

                                                    //Call Pre Commit Event
                                                    if (BeforeCommit != null)
                                                        BeforeCommit(this, convert.cStr(m_oDS.Tables[0].Rows[0]["template"]), xmlColOrder, oDT.Rows[0], oSets, y);

                                                    //Commit
                                                    ado_helper.update(oDT);

                                                    //Update ID if applicable
                                                    if (oDT.Columns.Contains("ID"))
                                                        (oSets[y] as helpers.structures.collection)["id"] = oDT.Rows[0]["id"];
                                                }

                                                //Set Imported Flag
                                                m_oDS.Tables[1].Rows[x]["_imported"] = "1";
                                            }

                                            //Not a merge operation, handle normally
                                            else {

                                                switch (matchType) {
                                                    case MatchedRowTypes.NewRecord:
                                                    case MatchedRowTypes.FullMatch:
                                                        //Update Values
                                                        sqlbuilder.setValues(oDT.Rows[0], (oSets[y] as helpers.structures.collection));

                                                        //Call Pre Commit Event
                                                        if (BeforeCommit != null)
                                                            BeforeCommit(this, convert.cStr(m_oDS.Tables[0].Rows[0]["template"]), xmlColOrder, oDT.Rows[0], oSets, y);

                                                        //Commit
                                                        ado_helper.update(oDT);

                                                        //Update ID if applicable
                                                        if (oDT.Columns.Contains("ID"))
                                                            (oSets[y] as helpers.structures.collection)["id"] = oDT.Rows[0]["id"];

                                                        //Set Imported Flag
                                                        m_oDS.Tables[1].Rows[x]["_imported"] = "1";

                                                        break;

                                                    case MatchedRowTypes.PartialMatch:
                                                        mergeRows.Add(new ImportMergeResult() {
                                                            Current = oDT.Rows[0],
                                                            Imported = oSets[y] as helpers.structures.collection,
                                                            ImportRowIndex = x
                                                        });

                                                        m_oDS.Tables[1].Rows[x]["_merge_id"] = oDT.Rows[0]["id"];
                                                        m_oDS.Tables[1].Rows[x]["_imported"] = "0";
                                                        break;

                                                    case MatchedRowTypes.NotMatched:
                                                    default:
                                                        throw new Exception("Halting import! Unable to determine next course of action for imported row.");
                                                        break;
                                                }

                                            }

                                        }
                                    }
                                }

                            }
                            //Call Post Save Event
                            if (convert.cBool(m_oDS.Tables[1].Rows[x]["_imported"]) && AfterSave != null)
                                AfterSave(this, convert.cStr(m_oDS.Tables[0].Rows[0]["template"]), xmlColOrder, m_oDS.Tables[1].Rows[x], oSets, x);
                        }
                        
                        if (!m_oDS.Tables[1].Rows[x]["_error"].IsNullOrEmpty())  
                            addError(m_oDS.Tables[1].Rows[x]["_error"].Str());
						
                        //Count Imported Rows
                        if (convert.cBool(m_oDS.Tables[1].Rows[x]["_imported"])) iCounter++;

                    }

					//Call Import Complete Event
                    if (ImportComplete != null)
						ImportComplete(this, convert.cStr(m_oDS.Tables[0].Rows[0]["template"]), xmlColOrder, m_oDS.Tables[1]);
                }

				//Show Errors
                if (m_oErrorMessage.Length > 0) lFormError.Text += m_oErrorMessage.ToString();
                if (lFormError.Text != "") 
				{
                    lFormError.Visible = true;
                    lFormError.Text = "<ul>" + lFormError.Text + "</ul>";
                }


                m_oDS.Tables[0].Rows[0]["xml"] = xmlSettings.OuterXml;
                m_oDS.Tables[0].Rows[0]["import_error"] = lFormError.Text;
                
                if (SaveImportData == null) {
                    oTempDT.Rows[0]["posted_date"] = DateTime.Now;
                    oTempDT.Rows[0]["binary_value"] = jlib.functions.serialize.AdoNetHelper.SerializeDataSet(m_oDS);
                    ado_helper.update(oTempDT);
                } else {
                    SaveImportData(this, m_oDS);
                }
                if (this.IsPostBack && iNextTab == 2) 
				{
                    iCurrentTab = iNextTab;
                    lSuccessSubline.Text = String.Format(convert.cStr(xml.getXmlAttributeValue(xmlTemplate, "settings[@import='true']", "success-subline"), lSuccessSubline.Text), iCounter);                    

					//Output Merge Records
					if (mergeRows.Count > 0)
					{
						plhMergeOptions.Visible = true;
						ltrRowsToMerge.Text = String.Format("{0} Rows need to be merged.", mergeRows.Count);
						lvMergeRows.DataSource = mergeRows;
						lvMergeRows.DataBind();

					}

                    if (!AjaxMode && !Request.QueryString["success"].Bln()) Response.Redirect(Request.Url.AbsolutePath + jlib.net.HTTP.mergeQueryStrings("success=true&template=" + System.Web.HttpUtility.UrlEncode(xml.getXmlAttributeValue(xmlTemplate, "id")) + "&rowcount=" + iCounter, Page.Request.QueryString.ToString()));
                }
            }
        }
		public string MergeResultValues( ImportMergeResult mergeResult )
		{
			

			
			//Declare 3 Partition Values Arrays
			string[][] Values = new string[3][];
			Values[0] = new string[mergeResult.Imported.Count];
			Values[1] = new string[mergeResult.Imported.Count];
			Values[2] = new string[mergeResult.Imported.Count];
			
			int i = 0;
			foreach (string key in mergeResult.Imported.Keys)
			{
				Values[0][i] = key;
				Values[1][i] = mergeResult.Imported[key].ToString();

				if (mergeResult.Current.Table.Columns.Contains(key))
					Values[2][i] = mergeResult.Current[key].ToString();
				else
					Values[2][i] = "";

				i++;
			}

			//Generate Table
			string output = "<table class=\"MergeResults\">"
								+ "<TR><TH>Source</TH>" + Values[0].Select(v => "<TH>" + v + "</TH>").ToList().Join("") + "</TR>"
								+ "<TR><TD>Imported</TD>" + Values[1].Select(v => "<TD>" + v + "</TD>").ToList().Join("") + "</TR>"
								+ "<TR><TD>Current</TD>" + Values[2].Select(v => "<TD>" + v + "</TD>").ToList().Join("") + "</TR>"
								+ "</table>";

			//Return output
			return output;
			
		}
        public void addError(string sError) {
            if(!m_oErrorMessage.ToString().Contains(sError))
                m_oErrorMessage.Append("<li>" + sError+"</li>");
        }
        private List<string> m_oColMapping;
        public int getColIndex(string sColID) {
            if (m_oColMapping == null) {
                m_oColMapping = new List<string>();
                for (int y = 0; y < xmlColOrder.Length; y++) {
                    m_oColMapping.Add("");
                    if (xmlColOrder[y] != null) m_oColMapping[m_oColMapping.Count - 1] = xml.getXmlAttributeValue(xmlColOrder[y], "mapping");                    
                }
            }
            if (m_oColMapping.Contains(sColID)) return m_oColMapping.IndexOf(sColID) + NumAuxColumns;
            return -1;
        }
        public helpers.structures.collection getDataMapping(DataTable oDT, int iRow) {
            return getDataMapping(oDT.Rows[iRow]);
        }
        public helpers.structures.collection getDataMapping(DataRow oRow) {
            helpers.structures.collection oSets = new helpers.structures.collection();
            for (int y = 0; y < xmlColOrder.Length; y++) {
                if (xmlColOrder[y] != null) {
                    string sTable = parse.splitValue(xml.getXmlAttributeValue(xmlColOrder[y], "mapping"), ".", 0), sField = parse.splitValue(xml.getXmlAttributeValue(xmlColOrder[y], "mapping"), ".", 1);
                    if (oSets[sTable] == null) oSets.Add(sTable, new helpers.structures.collection());
                    (oSets[sTable] as helpers.structures.collection).Add(sField, (oRow.Table.Columns.Contains("_" + oRow.Table.Columns[y + NumAuxColumns].ColumnName) ? oRow["_" + oRow.Table.Columns[y + NumAuxColumns].ColumnName] : oRow[y + NumAuxColumns]));
                }
            }

            XmlNodeList xmlSessionData = xmlSettings.SelectNodes("settings/session_data/input");
            for (int z = 0; z < xmlSessionData.Count; z++) {
                string sTable = parse.splitValue(xml.getXmlAttributeValue(xmlSessionData[z], "mapping"), ".", 0), sField = parse.splitValue(xml.getXmlAttributeValue(xmlSessionData[z], "mapping"), ".", 1);
                if (oSets[sTable] == null) oSets.Add(sTable, new helpers.structures.collection());
                (oSets[sTable] as helpers.structures.collection).Add(sField, xml.getXmlAttributeValue(xmlSessionData[z], "value"));
            }
            return oSets;
        }
        //oValues is a collection of collections
        public void setDataMappingValues(DataRow oRow, jlib.helpers.structures.collection oValues) {
            for (int y = 0; y < xmlColOrder.Length; y++) {
                if (xmlColOrder[y] != null) {
                    string sTable = parse.splitValue(xml.getXmlAttributeValue(xmlColOrder[y], "mapping"), ".", 0), sField = parse.splitValue(xml.getXmlAttributeValue(xmlColOrder[y], "mapping"), ".", 1);
                    if(oValues[sTable]!=null){
                        if (oRow.Table.Columns.Contains("_" + oRow.Table.Columns[y + NumAuxColumns].ColumnName)) oRow["_" + oRow.Table.Columns[y + NumAuxColumns].ColumnName] = (oValues[sTable] as jlib.helpers.structures.collection)[sField];
                        else oRow[y + NumAuxColumns] = (oValues[sTable] as jlib.helpers.structures.collection)[sField];
                    }
                }
            }
        }
        protected void Page_PreRender(object sender, EventArgs e) {
            if (lInstructions.Text == "") lInstructions.Visible = false;
            iCurrentTab = iNextTab;
            if (!AllowUpload) lPasteHeader.Text = "Paste your data from Excel below";
            if (Template != "") {
                pTemplateSelector.Visible = false;
                lTemplateHeader.Text = "Choose options";
            }
            if (AjaxMode) {
                bImportNext.OnClientClick = "fnImportBeforeSubmit(this); $('#hJImportNextTab')[0].value=(Number($('#hJImportCurrentTab').val())+1);AJAXControlAction($(this).attr('name'),null,null);$('#" + bImportNext.ClientID + ",#" + bImportPrevious.ClientID + "').hide().eq(0).after('<img src=/inc/library/controls/jimport/media/loading_indicator.gif>Processing - Please wait.');";
                bImportPrevious.OnClientClick = "fnImportBeforeSubmit(this); $('#hJImportNextTab')[0].value=(Number($('#hJImportCurrentTab').val())-1);AJAXControlAction($(this).attr('name'),null,null);$('#" + bImportNext.ClientID + ",#" + bImportPrevious.ClientID + "').hide().eq(0).after('<img src=/inc/library/controls/jimport/media/loading_indicator.gif>Processing - Please wait.');";
                bMergeApply.OnClientClick = "fnImportBeforeSubmit(this); $('#hJImportNextTab')[0].value=(2);AJAXControlAction($(this).attr('name'),null,null);$('#" + bMergeApply.ClientID + "').hide().eq(0).after('<img src=/inc/library/controls/jimport/media/loading_indicator.gif>Processing - Please wait.');";
                bLoadPreset.ButtonMode = button.ButtonModes.ButtonButton;
                bLoadPreset.OnClientClick = "fnImportBeforeSubmit(this); AJAXControlAction($(this).attr('name'),null,null);";
                bSavePreset.ButtonMode = button.ButtonModes.ButtonButton;
                bSavePreset.OnClientClick = "fnImportBeforeSubmit(this); AJAXControlAction($(this).attr('name'),null,null);";                
            }

            if (iCurrentTab == 0) {
                pStep0.Visible = true;
                bImportPrevious.Visible = false;
            } else if (iCurrentTab == 1) {
                pStep1.Visible = true;
                for (int x = 0; x < m_oDS.Tables[1].Columns.Count; x++) {
                    if (m_oDS.Tables[1].Columns[x].ColumnName.StartsWith("col_")) {
                        GridColumn oCol = new GridColumn();
                        oCol.HAtt = "class='status'";
                        oCol.FInputAtt = "class='text'";
                        oCol.ColType = "NoEdit";
                        oCol.FormatString = m_oDS.Tables[1].Columns[x].ColumnName;
                        oCol.HText = "Col #" + (x-NumAuxColumns);
                        oCol.ID = "__import_" + x;
                        oCol.ConvertNewlineToBr = true;
                        gPreview.addColumn(oCol, gPreview.Columns.Count - 2);
                    }

                }
            } else if (iCurrentTab == 2) {
                pStep2.Visible = true;
                bImportPrevious.Visible = false;
                bImportNext.Visible = false;
                if (!this.IsPostBack) lSuccessSubline.Text = String.Format(convert.cStr(xml.getXmlAttributeValue(xmlTemplate, "settings[@import='true']", "success-subline"), lSuccessSubline.Text), Request["rowcount"]);                                        
                if (AjaxMode) {
                    hRestart.NavigateUrl = "javascript:void(0);";
                    hRestart.OnClientClick = "$('#hJImportNextTab')[0].value=0;AJAXControlAction('" + bImportNext.UniqueID + "',null,null);";
                } else
                    hRestart.NavigateUrl = jlib.net.HTTP.stripQueryStringParameters(Request.Url.PathAndQuery, "success", "template", "rowcount");
            }

            aContainer.ContentContainer.Controls.Add(convert.cLiteral("<input type='hidden' id='hJImportCurrentTab' name='hJImportCurrentTab' value='" + iCurrentTab + "' /><input type='hidden' id='hJImportNextTab' name='hJImportNextTab' value='" + iCurrentTab + "' />"));
            //Page.ClientScript.RegisterHiddenField("hJImportCurrentTab", iCurrentTab.ToString());
            //Page.ClientScript.RegisterHiddenField("hJImportNextTab", iCurrentTab.ToString());
            dSavePreset.SelectedIndex = 0;
            tPresetName.Text = "";
            lScript.Text += "\n"+ OnRenderScript;
        }

    }
    public abstract class plugin {
        public abstract void attach(jimport.index oImporter);
    }
}
