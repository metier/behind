using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using jlib.components.button;
using jlib.components;
using jlib.db;
using jlib.functions;


namespace jlib.controls.popup
{
    public delegate void OnControlShowHandler( object sender, EventArgs e );
    public delegate void OnControlHideHandler( object sender, EventArgs e );

    [ParseChildren( true )]
    public class index : System.Web.UI.UserControl
    {

        public event OnControlShowHandler ControlShow;
        public event OnControlHideHandler ControlHide;

        private Unit m_oHeight = new Unit(), m_oWidth   = new Unit(), m_oTop = new Unit(), m_oLeft = new Unit();

        private string m_sPosition = "absolute", m_sAnchorTClientID = "", m_sAnchorLClientID = "";

        private bool m_bModal = false, m_bZoomEffect = false, m_bServerSideMode = true, m_bSmartClose = false, m_bForceTopVisible = false;
		
		private string m_sAutoReposition = "";
		private int m_iDisplayTimeout = 100;
		
        protected hidden ctrVisible;
		
        protected ImageButton ctrAutoFillWindowClose;
		protected HtmlGenericControl ctrContainer, ctrPopupWindow, ctrMask;
		protected label lScripts;                        

        public PlaceHolder Content {
            set {
                ctrContainer.Controls.Clear();
                while ( value.Controls.Count > 0 )
                    ctrContainer.Controls.Add( value.Controls[0] );
            }
            //get {
            //    return ctrContainer;
            //}
        }


        public string ControlURL {            
            set {
                Control oControl    = Page.LoadControl( value );
                ctrContainer.Controls.Add( oControl );
            }
        }
		public ImageButton CloseButton {
			get {
				return ctrAutoFillWindowClose;
			}
		}

        public void OnControlShow( EventArgs e ) {
            if ( ControlShow != null ) ControlShow( this, e );
        }

        public void OnControlHide( EventArgs e ) {
            if ( ControlHide != null ) ControlHide( this, e );
        }

        public void Page_Init( object sender, EventArgs e ){
            //ctrAutoFillWindowClose.Click    += new ImageClickEventHandler(ctrAutoFillWindowClose_Click);
            
            this.Visible =  (convert.cStr(Request[ctrVisible.UniqueID]) != "" ? convert.cBool(Request[ctrVisible.UniqueID]) : false);			

			if (convert.cStr(Page.Request.Form[ctrAutoFillWindowClose.UniqueID + ".x"]) != "") ctrAutoFillWindowClose_Click(null, null);
        }

		void Page_PreRender(object sender, EventArgs e) {
			if (!convert.cBool(ConfigurationManager.AppSettings["common.disable.includes"])) {
				Page.RegisterClientScriptBlock("jlib.controls.popup.popup.css", "<LINK href=\"/inc/library/controls/popup/media/popup.css\" type=\"text/css\" rel=\"stylesheet\" />");
				Page.RegisterClientScriptBlock("jlib.controls.popup.popup.js", "<script type=\"text/javascript\" src=\"/inc/library/controls/popup/media/popup.js\"></script>");
			}
		}

       
        protected void ctrAutoFillWindowClose_Click(object sender, ImageClickEventArgs e){
			//ctrAutoFillWindowClose.Attributes["onclick"] = "$('" + ctrPopupWindow.ClientID + "').hide();return false;";
			this.Visible = false;
            if ( Modal ) {
                (Page as jlib.components.webpage).registerScript(typeof(index), "popup_mask_show", "showElementsByTag( \"select\" );");
            }
            OnControlHide( new EventArgs() );
        }

        public Unit Width {
            get {
                return m_oWidth;
            }
            set {
                m_oWidth = value;
            }
        }

        public Unit Height {
            get {
                return m_oHeight;
            }
            set {
                m_oHeight = value;
            }
        }

        public Unit Top {
            get {
                return m_oTop;
            }
            set {
                m_oTop = value;
            }
        }

        public Unit Left {
            get {
                return m_oLeft;
            }
            set {
                m_oLeft = value;
            }
        }

		public int DisplayTimeout {
			get {
				return m_iDisplayTimeout;
			}
			set {
				m_iDisplayTimeout = value;
			}
		}

        public string Position{
            get {
                return m_sPosition;
            }
            set {
                m_sPosition = value;
            }
        }

        public string AnchorClientID {            
            set {
                AnchorLClientID = value;
                AnchorTClientID = value;                
            }
        }

        public string AnchorLClientID {
            get {
                return m_sAnchorLClientID;
            }
            set {
                m_sAnchorLClientID = value;
            }
        }

        public string AnchorTClientID {
            get {
                return m_sAnchorTClientID;
            }
            set {
                m_sAnchorTClientID = value;
            }
        }

        public bool Modal {
            get {
                return m_bModal;
            }
            set {
                m_bModal = value;
            }
        }

        public bool ZoomEffect {
            get {
                return m_bZoomEffect;
            }
            set {
                m_bZoomEffect = value;
            }
        }

		public bool SmartClose {
            get {
				return m_bSmartClose;
            }
            set {
				m_bSmartClose = value;
            }
        }
		
		public bool ServerSideMode {
            get {
				return m_bServerSideMode;
            }
            set {
				m_bServerSideMode = value;
				this.Visible = this.Visible;
            }
        }

		public bool ForceTopVisible {
            get {
				return m_bForceTopVisible;
            }
            set {
				m_bForceTopVisible = value;				
            }
        }

		public hidden VisibleField{
			get{
				return ctrVisible;
			}
			set{
				ctrVisible = value;
			}
		}
				

        public new bool Visible {
            get {
				return convert.cBool(ctrVisible.Value);                
            }
            set {
				if (ServerSideMode)
					base.Visible = value;
				else
					base.Visible = true;

                ctrVisible.Value = value.ToString();
            }
        }

		public bool AutoReposition {
            get {
				return convert.cBool(m_sAutoReposition);
            }
            set {
				m_sAutoReposition = value.ToString();				
            }
        }

		
		//public override string ClientID {
		//    get {
		//        return ctrPopupWindow.ClientID;
		//    }
		//}
		//public override string UniqueID {
		//    get {
		//        return ctrPopupWindow.UniqueID;
		//    }
		//}

		public HtmlGenericControl Window {
			get {
				return ctrPopupWindow;
			}
		}

        public void Close() {
            ctrAutoFillWindowClose_Click( null, null );
        }

        public void Show() {
            this.Visible = true;
            OnControlShow( new EventArgs() );
        }

        protected override void Render( HtmlTextWriter writer ) {
			lScripts.Text = String.Format("initCommonPopup( '{0}', '{1}', '{2}', '{3}', '{4}' );" + ( this.Visible ? "$('{0}').show();" : "" ) + "", ctrPopupWindow.ClientID,ctrContainer.ClientID, ctrAutoFillWindowClose.ClientID, ctrMask.ClientID, ctrVisible.ClientID);
			ctrPopupWindow.Attributes["Modal"] = this.Modal.ToString();
			ctrPopupWindow.Attributes["ServerSideMode"] = ServerSideMode.ToString();						
			ctrPopupWindow.Attributes["ZoomEffect"] = this.ZoomEffect.ToString();
			ctrPopupWindow.Attributes["AnchorLClientID"] = m_sAnchorLClientID;
			ctrPopupWindow.Attributes["AnchorTClientID"] = m_sAnchorTClientID;
			ctrPopupWindow.Attributes["Left"] = Left.ToString();
			ctrPopupWindow.Attributes["Top"] = Top.ToString();
			ctrPopupWindow.Attributes["Width"] = Width.ToString();
			ctrPopupWindow.Attributes["Height"] = Height.ToString();
			ctrPopupWindow.Attributes["Position"] = Position.ToLower();
			ctrPopupWindow.Attributes["SmartClose"] = SmartClose.ToString();
			ctrPopupWindow.Attributes["ForceTopVisible"] = ForceTopVisible.ToString();
			ctrPopupWindow.Attributes["AutoReposition"] = m_sAutoReposition.ToLower();
			ctrPopupWindow.Attributes["DisplayTimeout"] = m_iDisplayTimeout.ToString();
			

            base.Render( writer );
        }                
    }
}
