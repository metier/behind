<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="index.ascx.cs" Inherits="jlib.controls.popup.index" %>
<common:hidden runat="server" id="ctrVisible" />
<div class="popup" id="ctrMask" runat="server" style="z-index:999;position:absolute"></div>
<div style="border:1px solid black;z-index:1000" runat="server" id="ctrPopupWindow">
<div style="background-color:White;border:3px solid orange;height:100%;width:100%">
<asp:ImageButton runat="server" ID="ctrAutoFillWindowClose" ImageUrl="/inc/m/close_inactive.gif" onmouseover="this.src='/inc/m/close_active.gif';" onmouseout="this.src='/inc/m/close_inactive.gif';" style="float:right;position:relative;top:2px;left:-1px;height:10px" />

<div style="margin:5px 5px 5px 5px;" runat="server" id="ctrContainer"></div>
</div>
</div>
<script language="javascript"><common:label runat="server" ID="lScripts" DisableDrawSpan="true" /></script>