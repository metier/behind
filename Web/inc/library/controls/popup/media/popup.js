function initCommonPopup( sPopupID, sContentID, sCloseButtonID, sMaskID, sVisibleID ){

    var oPopup      = $( sPopupID );    
    oPopup.Content  = $( sContentID );
    oPopup.CloseButton  = $( sCloseButtonID );
    oPopup.MaskElement  = $( sMaskID );
    oPopup.VisibleElement = $( sVisibleID );
    oPopup.SearchCounter = 0;
    oPopup.AutoReposition = oPopup.getAttribute( "AutoReposition" );
    
    oPopup.Modal    = oPopup.getAttribute( "Modal" ) == "True";
    oPopup.ZoomEffect = oPopup.getAttribute( "ZoomEffect" ) == "True";
    oPopup.ServerSideMode = oPopup.getAttribute( "ServerSideMode" ) == "True";
    oPopup.SmartClose = oPopup.getAttribute( "SmartClose" ) == "True";
    oPopup.ForceTopVisible = oPopup.getAttribute( "ForceTopVisible" ) == "True";
    
    oPopup.AnchorLClientID = oPopup.getAttribute( "AnchorLClientID" );
    oPopup.AnchorTClientID = oPopup.getAttribute( "AnchorTClientID" );
    oPopup.Left     = oPopup.getAttribute( "Left" );
    oPopup.Top      = oPopup.getAttribute( "Top" );
    oPopup.Width    = oPopup.getAttribute( "Width" );
    oPopup.Height   = oPopup.getAttribute( "Height" );
    oPopup.Position = oPopup.getAttribute( "Position" );    
    
    oPopup.DisplayTimeout = parseFloat(oPopup.getAttribute( "DisplayTimeout" ));    
    
    
    oPopup.hide = commonPopupHide;               
    oPopup.show = commonPopupShow;
    oPopup.showUrl = commonPopupShowUrl;
    
    oPopup.hide();
    if( oPopup.Position != "" ) oPopup.style.position = oPopup.Position;
    if( oPopup.Width != "" ) oPopup.style.width = oPopup.Width;
    if( oPopup.Height != "" ) oPopup.style.height = oPopup.Height;
        
    if( oPopup.Initiated ) return;
    oPopup.Initiated = true;        
    
    Event.observe(oPopup.CloseButton, "click", function(event){ if( !oPopup.ServerSideMode ) oPopup.hide(null,event); } );
                    
    //setTimeout( function(){ Event.observe( document.body, "click" , function(event){ oPopup.hide() } ); }, 1000 );
    
    Event.observe( document.body, "keypress" , function(event){ if( oPopup.SmartClose && event.keyCode == Event.KEY_ESC ){ oPopup.hide(); Event.stop(event); }}  );
    if( oPopup.MaskElement != null ) Event.observe( oPopup.MaskElement, "click" , function(event){ if( oPopup.SmartClose ){ oPopup.hide(); Event.stop(event); }}  );

}

function commonPopupShowUrl( sUrl ){
    //alert(sUrl );
    //return;
    var oPopup = this;
    this.SearchCounter++;
    var iSearchCounter = this.SearchCounter;
    if( sUrl != "" && sUrl != null )oPopup.Content.innerHTML = "<img src=\"/inc/library/controls/popup/media/loading_indicator.gif\">&nbsp;Vennligst vent mens innholdet laster...";
    this.show();
    if( sUrl != "" && sUrl != null )doCallBack( null, null, sUrl + "&search_counter=" + this.SearchCounter + "&popup_id=" + this.getAttribute("id"), null, "", null, function(oResponse){commonPopupDataCallback( oPopup,oResponse.responseText, iSearchCounter )  ;} , true, true );
    
}

function commonPopupDataCallback( oPopup, sData, iCounter ){
    if( oPopup == null ) oPopup = this;
    if( oPopup.SearchCounter > iCounter ) return;
    
    try{
        oPopup.Content.innerHTML = sData;
    }catch(e){}
}


function commonPopupShow( oPopup, event ){                
    if( oPopup  == null ) oPopup = this;    
    setTimeout( function(){ commonPopupShowA( oPopup, event ); }, oPopup.DisplayTimeout );    
}
function commonPopupShowA( oPopup, event ){                
    oPopup.style.display = "";
    if( oPopup.Modal && oPopup.MaskElement != null ) oPopup.MaskElement.style.display = "";
    if( oPopup.Modal ){
        hideElementsByTag( "select", oPopup.getAttribute("id") );
        positionElement( true, false, false, oPopup.MaskElement.getAttribute("id"), "", "", "0px", "0px", "fullscreen", "fullscreen", true, false );
    }
    
    positionElement( true, oPopup.Modal, oPopup.ZoomEffect, oPopup.getAttribute("id"), oPopup.AnchorLClientID, oPopup.AnchorTClientID, oPopup.Left, oPopup.Top, oPopup.Width, oPopup.Height, oPopup.AutoReposition, oPopup.ForceTopVisible );
    //position the popup
    
}

function commonPopupHide( oPopup, event ){                
    if( oPopup  == null ) oPopup = this;
    if( event ) Event.stop(event);
    if( oPopup.style.display == "none" ) return;
    
    oPopup.style.display = "none";
    if( oPopup.MaskElement != null ) oPopup.MaskElement.style.display = "none";
    
    if( oPopup.Modal ) showElementsByTag( "select" );
    
    return false;
}
