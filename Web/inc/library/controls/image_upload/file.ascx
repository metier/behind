<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="file.ascx.cs" Inherits="jlib.controls.image_upload.file" %>
<tr>
    <td align="right"><b><asp:Label runat="server" ID="ctrIndex" /></b></td>
    <td runat="server" id="ctrImageUploadContainer"><input type="file" id="ctrImageUpload" name="ctrImageUpload" runat="Server" /></td>
    <td runat="server" id="ctrImageFilenameContainer"></td>
    <td runat="server" id="ctrImageOptionsContainer"><common:hyperlink navigateurl="javascript:void(0);" id="ctrDisplayCrop" runat="server">Vis</common:hyperlink>&nbsp;|&nbsp;<common:hyperlink navigateurl="javascript:void(0);" id="ctrRemoveFile" runat="server">Fjern</common:hyperlink></td>
</tr>
<common:hidden runat="server" id="ctrImageDimensions" />
<common:hidden runat="server" id="ctrImageDeleted" />
<common:hidden runat="server" id="ctrInitialUrl" />