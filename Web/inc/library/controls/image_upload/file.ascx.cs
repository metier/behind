using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using jlib.components.button;
using jlib.components;
using jlib.db;
using jlib.functions;


namespace jlib.controls.image_upload
{
    
    public class file : System.Web.UI.UserControl
    {
        protected System.Web.UI.WebControls.Label ctrIndex;

        protected System.Web.UI.HtmlControls.HtmlInputFile ctrImageUpload;
        
        protected hyperlink ctrDisplayCrop;
        protected hyperlink ctrRemoveFile;

        protected hidden ctrImageDimensions, ctrImageDeleted, ctrInitialUrl;
               

        protected System.Web.UI.HtmlControls.HtmlTableCell ctrImageUploadContainer;
        protected System.Web.UI.HtmlControls.HtmlTableCell ctrImageFilenameContainer;        
        protected System.Web.UI.HtmlControls.HtmlTableCell ctrImageOptionsContainer;
        
                
        private int m_iIndex = 0;

        public HttpPostedFile PostedFile {
            get {
                return ( ctrImageUpload.PostedFile == null || ctrImageUpload.PostedFile.ContentLength == 0 ? null : ctrImageUpload.PostedFile);
            }
        }

        public int Index {
            get {
                
                return m_iIndex;
            }
            set {
                m_iIndex = value;
            }
        }

        public bool Deleted {
            get {
                return ctrImageDeleted.Value == "1";
            }
            set {
                ctrImageDeleted.Value = ( value ? "1" : "" );
            }
        }


        public string InitialUrl {
            get {
                return ctrInitialUrl.Value;
            }
            set {
                ctrInitialUrl.Value = value;
            }
        }

        public string CropDimensions {
            get {
                return ctrImageDimensions.Value;
            }
            set {
                ctrImageDimensions.Value = value;
            }
        }

        
        protected override void Render( HtmlTextWriter writer ) {

            ctrIndex.Text = Index + ".";
            if ( InitialUrl == "" ) {
                ctrImageOptionsContainer.Style["display"] = "none";
                ctrImageFilenameContainer.Style["display"] = "none";
            } else {
                ctrImageUploadContainer.Style["display"] = "none";
                ctrImageFilenameContainer.InnerHtml = System.IO.Path.GetFileName( InitialUrl );
            }            
            
            ctrImageUpload.Attributes["onchange"] = String.Format( "document.getElementById( '{3}' ).value = ''; displayCrop( '{0}', '{1}', '{2}', '{3}' );", ctrImageUpload.ClientID, ctrImageUploadContainer.ClientID, ctrImageOptionsContainer.ClientID, ctrImageDimensions.ClientID );
            ctrRemoveFile.Attributes["onclick"] = String.Format( "removeFile( '{0}', '{1}', '{2}', '{3}', '{4}' );", ctrImageUploadContainer.ClientID, ctrImageFilenameContainer.ClientID, ctrImageOptionsContainer.ClientID, ctrImageDeleted.ClientID, ctrImageDimensions.ClientID );
            ctrDisplayCrop.Attributes["onclick"] = String.Format("displayCrop( '{0}', '{1}', '{2}', '{3}', '{4}'  );", ctrImageUpload.ClientID, ctrImageUploadContainer.ClientID, ctrImageOptionsContainer.ClientID, ctrImageDimensions.ClientID, parse.replaceAll(InitialUrl, "\\", "\\\\", "'", "\\'"));            
            
            //ctrTitle.Text = Title;
            
            //if ( RTitle != "" ) {
            //    ctrRTitle.Text = RTitle;
            //    ctrRightSection.Visible = true;
            //} else {
            //    ctrRightSection.Visible = false;
            //}

            base.Render( writer );
        }

        
                
    }
}
