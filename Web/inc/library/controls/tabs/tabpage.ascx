<%@ Control Language="c#" AutoEventWireup="false" Codebehind="tabpage.ascx.cs" Inherits="jlib.controls.tabs.tabpage" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<table runat="server" id="tableMain" style="display:none;" class="tab_page">
    <tr>
        <td runat="server" id="tdContent" valign="top">
	        <asp:placeholder runat="server" id="plhContent" />
        </td>
    </tr>
</table>    