<%@ Control Language="c#" AutoEventWireup="false" Codebehind="container.ascx.cs" Inherits="jlib.controls.tabs.container" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="icms" Namespace="jlib.components" Assembly="jlib" %>

<link rel="stylesheet" href="~/inc/library/controls/tabs/container.css" />
<div runat="server" id="ctrContainer">
<table class="tab_container" style="width:100%">
	<tr id="trTabs">
		<asp:placeholder runat="server" id="plhTabs" />					
		<td class="tab_empty" style="border-left:0px">&nbsp;</td>
	</tr>				
	<tr>
	    <td runat="server" id="ctrTabPages" >
	        <asp:placeholder runat="server" id="plhTabPages" />				
	    </td>
	</tr>
</table>
</div>