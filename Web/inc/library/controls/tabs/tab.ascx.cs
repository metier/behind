using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using jlib.functions;
using System.Web.UI;

namespace jlib.controls.tabs
{

	/// <summary>
	///		Summary description for webcontrol.
	/// </summary>
	[ParseChildren(false)]
	public class tab : System.Web.UI.UserControl
	{
		public System.Web.UI.HtmlControls.HtmlTableCell tdTab;
		protected System.Web.UI.WebControls.Literal ltrCaption;

		private int m_iTabID					= 1;
		private string m_sWidth					= "100";
		private tabpage m_oTabpage				= null;

        private void Control_Load( object sender, System.EventArgs e )
		{
            this.tdTab.Attributes["class"] = "tab_notactive";
            this.Tabpage.tableMain.Style["display"] = "none";
            tdTab.Attributes["tab_id"] = convert.cStr( m_iTabID );
            tdTab.Width = m_sWidth;
            Tabpage.Visible = this.Visible;            
		}
		protected void Control_PreRender(object sender, EventArgs e)
		{
            
		}

		#region tabs
		public string Caption
		{
			get
			{
				return ltrCaption.Text;
			}
			set
			{
				ltrCaption.Text = value;
			}
		}
		public int TabID
		{
			get
			{
				return m_iTabID;
			}
			set
			{
				m_iTabID = value;
			}
		}
		public string TagHandle
		{
			get
			{
				return tdTab.ClientID;
			}
		}
		public tabpage Tabpage
		{
			get
			{
				return m_oTabpage;
			}
			set
			{
				m_oTabpage = value;

				this.TabID		= value.TabID;
				this.Caption	= value.TabCaption;
			}
		}
		#endregion


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler( this.Control_Load );
            this.PreRender += new EventHandler( this.Control_PreRender );
		}
		#endregion

		
	}
}
