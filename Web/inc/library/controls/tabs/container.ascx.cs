using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using jlib.functions;
using System.Web.UI;

namespace jlib.controls.tabs
{

	/// <summary>
	///		Summary description for webcontrol.
	/// </summary>
	[ParseChildren(true, "TabPages")]
    public class container : System.Web.UI.UserControl
	{
	
		protected System.Web.UI.WebControls.PlaceHolder plhTabs;
		protected System.Web.UI.WebControls.PlaceHolder plhTabPages;
        protected System.Web.UI.HtmlControls.HtmlContainerControl ctrContainer;
        protected System.Web.UI.HtmlControls.HtmlTableCell ctrTabPages;
        
	
		private int m_iStartTab						= 1;

		private void Page_Load(object sender, System.EventArgs e)
		{
			
		}
        
        public string Width {
            get {
                return ctrContainer.Style["width"];
            }
            set {
                ctrContainer.Style["width"] = value;
            }
        }

		private void Page_PreRender(object sender, EventArgs e)
		{

			//Page.RegisterStartupScript("jlib.controls.tabs.starttab", String.Format("<script>Container.StartTab = {0};</script>", this.StartTab));

            
			foreach(tab oTab in plhTabs.Controls){
				oTab.Visible = oTab.Tabpage.Visible;
			}

            if ( StartTab > plhTabPages.Controls.Count )
                StartTab = 1;

            if ( plhTabPages.Controls.Count > 0 ) {
                ( plhTabPages.Controls[StartTab - 1] as tabpage ).tableMain.Style["display"] = "";
                ( plhTabs.Controls[StartTab - 1] as tab ).tdTab.Attributes["class"] = "tab_active";
            }

            string sAllTabsInactive = "";
            for ( int x = 0; x < plhTabs.Controls.Count; x++ )
                sAllTabsInactive    += String.Format( "document.getElementById( '{0}' ).className   = 'tab_notactive';" , ( plhTabs.Controls[ x ] as tab).tdTab.ClientID );

            string sAllPagesHidden = "";
            for ( int x = 0; x < plhTabPages.Controls.Count; x++ )
                sAllPagesHidden    += String.Format( "document.getElementById( '{0}' ).style.display   = 'none';" , ( plhTabPages.Controls[ x ] as tabpage).tableMain.ClientID );

            for ( int x = 0; x < plhTabs.Controls.Count; x++ ) 
                ( plhTabs.Controls[x] as tab ).tdTab.Attributes["onclick"] = sAllPagesHidden + sAllTabsInactive + "this.className  = 'tab_active'; document.getElementById( '" + ( plhTabPages.Controls[x] as tabpage).tableMain.ClientID + "' ).style.display = '';";

            ctrTabPages.ColSpan = plhTabs.Controls.Count * 2 + 1;
		}

        #region PROPERTIES


        public int StartTab
		{
			get
			{
				return m_iStartTab;
			}
			set
			{
				m_iStartTab = value;
			}
		}
		
		public PlaceHolder Tabs
		{
			get
			{
				return plhTabs;
			}
			set
			{
				plhTabs = value;
			}
		}
		public PlaceHolder TabPages
		{
			get
			{
				return plhTabPages;
			}
			set
			{
				PlaceHolder plhTemp	= (PlaceHolder)value;
                plhTabs.Controls.Clear();
                plhTabPages.Controls.Clear();
                
                while( plhTemp.Controls.Count > 0 ){
                    if ( ( plhTemp.Controls[ 0 ] as tabpage ) == null ) {
                        plhTemp.Controls.Remove( plhTemp.Controls[0] );
                    }else{
						//Add TabPage
                        tabpage oTabPage = (tabpage)plhTemp.Controls[0];
                        plhTabPages.Controls.Add( oTabPage );
						//plhTabPages.Controls.Add( oTabPage );

						//Add Corresponding Tab
						tab oTab			= (tab)Page.LoadControl("/inc/library/controls/tabs/tab.ascx");
						oTab.Tabpage		= oTabPage;
						plhTabs.Controls.Add( oTab );

					}
				}								
			}
		}
		#endregion

		#region METHODS
		
		#endregion


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			this.PreRender+=new EventHandler(Page_PreRender);			
		}
		#endregion

		
	}
}
