﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.controls.jcombo
{
    public class jComboDataEventArgs : EventArgs
	{
		#region ***** CONSTRUCTORS *****
        public jComboDataEventArgs(jCombo Combo, string InitialID, string Query, int Max, string SessionData) 
		{
            this.Combo = Combo;
            this.InitialID = InitialID;
			this.Query = Query;
            this.Max = Max;
            this.SessionData = SessionData;
        }
		#endregion

		#region ***** PROPERTIES *****
		public jCombo Combo { get; set; }
		public string InitialID { get; set; }
		public string Query { get; set; }
        public int Max { get; set; }
        public string SessionData { get; set; }
		#endregion

		#region ***** METHODS *****
		#endregion

    }
}
