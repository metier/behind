﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.controls.jcombo
{
	public class ComboDataEventArgs : jComboDataEventArgsAdo
	{
		public ComboDataEventArgs(jCombo jCombo, string InitialID, string Fields, string Tables, string Filter, string Sort, string Query, int Max, string SessionData)
            : base(jCombo, InitialID, Fields, Tables, Filter, Sort, Query, Max, SessionData) 
		{
        }
	}
}
