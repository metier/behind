﻿; (function($) {
    $.fn.extend({
        JCombo: function(options) {
            if(!$(this).is("input")) return this.each(function() { new $.JCombo(this, options); });
        }
    });
    $.extend({ valHooks: {JCombo:{get:function(elem){return elem.JCombo.getValue();},set:function(elem,value){elem.JCombo.setValue(value);}}}});
    $.JCombo = function(div, options) 
	{    
        if (div.JCombo != null) return div.JCombo;  
    
	    $(div).data("original-html", div.outerHTML);        
    
	    if (options.options == "tag") 
		{        
            options = $.extend({}, $.JCombo.defaults, 
			{                
                Max: $(div).attr("max") || 20,
                Arrow: $(div).attr("arrow") == "true",
                Url: $(div).attr("url"),
                Multi: $(div).attr("multi") == "true",
                ID: $(div).attr("id"),
                Data: ($(div).attr("data") == null ? null : eval(($(div).attr("datadeep") != "true" ? $(div).attr("data") : "$.extend(true,{},{x:" + $(div).attr("data") + "}).x"))),
                Value: $(div).attr("initial-value"),
                Text: $(div).attr("text"),
                Watermark: $(div).attr("watermark"),
                Width: cStr($(div).attr("width")).replace("px",""),
				Height: cStr(cStr($(div).attr("height")).replace("px",""),"300"),
                LWidth: cStr($(div).attr("listwidth")).replace("px",""),
                JSName: $(div).attr("jsname"),
                onPostback: ($(div).attr("onpostback") == null ? null : eval("new Function('oCombo','oValues',\"" + $(div).attr("onpostback").split('"').join("'") + "\");")),
                onChange: ($(div).attr("onclientchange") == null ? null : eval("new Function('oCombo','oValues',\"" + $(div).attr("onclientchange").split('"').join("'") + "\");")),
                onDblClick: ($(div).attr("onclientdblclick") == null ? null : eval("new Function('oCombo','sID','oBit',\"" + $(div).attr("onclientdblclick").split('"').join("'") + "\");")),
                getValue: ($(div).attr("getvalue") == null ? $.JCombo.defaults.getValue : eval("new Function('oRow','iIndex','oCombo',\"" + $(div).attr("getvalue").split('"').join("'") + "\");")),
                getText: ($(div).attr("gettext") == null ? $.JCombo.defaults.getText : eval("new Function('oRow','iIndex','oCombo',\"" + $(div).attr("gettext").split('"').join("'") + "\");")),
                getItem: ($(div).attr("getitem") == null ? $.JCombo.defaults.getItem : eval("new Function('oRow','iIndex','oCombo',\"" + $(div).attr("getitem").split('"').join("'") + "\");")),
                DataMode: $(div).attr("datamode") || "client",                
                Required: $(div).prop("required") != "false",
                SelectFirst: $(div).attr("selectfirst") == "true",
                Disable: $(div).attr("disable") == "true",
                LFilter: $(div).attr("listfilter") != "false",
                Backspace: $(div).attr("backspace") == "true",
                Bound: $(div).attr("bound") != "false",
                NoRecords: ($(div).attr("norecords") == null ? "No records found" : $(div).attr("norecords")),                
                onInvalidIDError: ($(div).attr("oninvalididerror") == "false" ? null:( $(div).attr("oninvalididerror") == null || $(div).attr("oninvalididerror")=="true" ? $.JCombo.defaults.onInvalidIDError : eval("new Function('oOption','oCombo',\"" + $(div).attr("oninvalididerror").split('"').join("'") + "\");"))),
                AlwaysQuery: $(div).attr("alwaysquery") == "true",
                FixedNoneItem: $(div).attr("FixedNoneItem")
            }, options);
        }
        var oCombo = this;        
        if (options.JSName != null) $.data(document.body, options.JSName, this);
        div.JCombo = this;
        oCombo.Options = options;        
        oCombo.Data = (options.Data == undefined || options.Data.Records == undefined) ? options.Data : options.Data.Records;
        function PrepareValues() {
            for (var x = 0; oCombo.Data != null && x < oCombo.Data.length; x++) {
                if (oCombo.Data[x]._text == null) {
                    oCombo.Data[x]._value = oCombo.Options.getValue(oCombo.Data[x], x, oCombo);
                    if (oCombo.Data[x]._value !== false) {
                        oCombo.Data[x]._formatted = "" + oCombo.Options.getItem(oCombo.Data[x], x, oCombo);
                        oCombo.Data[x]._text = "" + oCombo.Options.getText(oCombo.Data[x], x, oCombo);
                    }
                }
            }
        }
        oCombo.render=function(div,oCombo)
		{
            oCombo.Init = false;
            oCombo.div = $(div).addClass("JCombo").prop("type","JCombo").css('z-index', 0).html("<table cellpadding=0 cellspacing=0 style='width:100%' id='jc_table'><tr><td valign='top' id='jc_content'></td><td valign='top' id='jc_button'></td></tr></table>");
            if (oCombo.Options.Width != "") oCombo.div.css("width", oCombo.Options.Width);

            if (oCombo.Options.Arrow) oCombo.btn = oCombo.div.find("td:eq(1)").append("<div class='jc_btn' style='cursor:pointer' />").find(".jc_btn").mouseenter(function() { if (oCombo.Options.Disable) return; $(this).addClass("hover"); }).mouseleave(function() { $(this).removeClass("hover") }).mouseup(function() { $(this).removeClass("pressed"); if (oCombo.Options.Disable) return;  oCombo.txt.focus(); fnJComboShow(oCombo.txt.val()); }).mousedown(function() { if (oCombo.Options.Disable) return; $(this).addClass("pressed"); });
            oCombo.content = oCombo.div.find("td:eq(0)");
            oCombo.hid = $(document.createElement('input')).attr('type', 'hidden').attr('id', oCombo.div.attr('id')||"").attr('name', oCombo.div.attr('id')||"").attr('jcombo', 'jcombo').appendTo(oCombo.div);
            
            oCombo.txt = $(document.createElement('input')).attr("autocomplete", "off").attr("id", oCombo.div.attr('id')+'_input').attr("class","jc_input").val(oCombo.Options.Text).appendTo(oCombo.content)
                .focus(function(e) {
                    if (oCombo.Options.Disable) {
                        this.blur();
                        return;
                    }
                    if (oCombo.HideTimer) clearTimeout(oCombo.HideTimer);
                    oCombo.content.addClass("active");
                    $(this).removeClass('watermark');
                    if (oCombo.Options.Watermark !== '' && this.value === oCombo.Options.Watermark)
                        this.value='';
                    else
                        this.select();
                    this.resize();
                })
            .blur(function(e) {
                if (oCombo.content.hasClass("hover")) return;
                oCombo.content.removeClass("active");
                if (oCombo.HideTimer) clearTimeout(oCombo.HideTimer);
                oCombo.HideTimer = setTimeout(function() { if (!oCombo.Options.MouseDownOnSelect) { if (!oCombo.content.hasClass("active")) { if (oCombo.items && oCombo.items.filter(".jc_over")[0] && oCombo.Options.Required) oCombo.select(); else if (oCombo.items) { if (oCombo.Options.Multi) oCombo.txt.val(""); else if (cStr(oCombo.txt.attr("clean")) != oCombo.txt.val()) { if (oCombo.Options.Bound) { oCombo.txt.val(""); var bChange = oCombo.hid.val() != ""; oCombo.hid.val(""); } if ((bChange || !oCombo.Options.Bound) && oCombo.Options.onChange) oCombo.Options.onChange(oCombo, null); } } } oCombo.hide(); } if (oCombo.txt.val() == "" && !oCombo.Options.Disable) { oCombo.txt.val(oCombo.Options.Watermark).addClass("watermark")[0].resize(); } }, 200);
            }).keydown(fnJComboKey).addClass('watermark').val(oCombo.Options.Watermark);
            oCombo.txt[0].resize=function(){var i=20+(this.value.length * 5);$(this).css("width", oCombo.Options.Width>0?Math.min(oCombo.Options.Width,i):i + "px");};
            oCombo.content.click(function() { oCombo.txt.focus(); });
            oCombo.load = oCombo.content.append("<div class='ajax' />").find(".ajax").hide();

			oCombo.txtValues = {};

            //setTimeout(function() { }, 1);
             $(function () {
                if (oCombo.Options.Value != "") oCombo.setValue(oCombo.Options.Value, oCombo.Options.Text);
                oCombo.txt[0].resize();
                if (oCombo.Options.Disable) oCombo.disable(true);
                oCombo.Init = true;            
            });     
        };               
        oCombo.visible = function() { return oCombo.list != null && oCombo.list.is(":visible"); };
        oCombo.current = function() { return oCombo.visible() && oCombo.items && (oCombo.items.filter(".jc_over")[0] || oCombo.Options.SelectFirst && oCombo.items[0]); };
        oCombo.remove = function(oBit) {
            if (oCombo.Options.Disable) return;
            if (oBit == null) oBit = this;
            var oArr = oCombo.hid.val().split("|");
            oArr[$.data(oBit, "index")] = "";
            oCombo.hid.val(oArr.join("|"));
            $(oBit).parents("a:eq(0)").remove();
            if (oCombo.Options.onChange) oCombo.Options.onChange(oCombo, null);            
        };
        oCombo.reset = function (bDataOnly, bDataReBind) { if (oCombo.Data && oCombo.Options.DataMode == "server") oCombo.Data = null; if (oCombo.Data) { for (var x = 0; x < oCombo.Data.length; x++) { if (bDataReBind && oCombo.Data[x]._undeletable) { oCombo.Data.remove(x, x); x--; } else { oCombo.Data[x]._text = null; oCombo.Data[x]._value = null; } } }; if (oCombo.list && !bDataOnly) $.data(oCombo.list, "value", null); if (oCombo.Data) oCombo.Data.Query = null; return oCombo; };
        oCombo.getNumMatches = function () { if (!oCombo.Data) return -1; PrepareValues(); var i = 0; $.each(oCombo.Data, function () { if (this._value !== false) i++; }); return i; };
        oCombo.scroll = function() { return (oCombo.list && oCombo.list.height() < oCombo.list.find("ul").height()) };
        oCombo.hide = function() { oCombo.content.removeClass("active"); if (oCombo.list) oCombo.list.hide(); }
        oCombo.getPostbackVariables = function (sVal, sID) { return { "__AJAXCALL": 1, "__QUERY": sVal, "__ID": sID, "__EVENTTARGET": oCombo.Options.ID, "__EVENTARGUMENT": 'GetJComboData', "max": oCombo.Options.Max, "seed": (new Date()).getTime() }; };
        oCombo.getServerData = function (oValues)
        {
            oValues.SessionData = oCombo.SessionData;
            oCombo.Data=null;            
            oCombo.AjaxRequest = $.ajax(
				{
					dataType: "json",
					url: oCombo.Options.Url || $(oCombo.div).parents("form:eq(0)").attr("action") || window.location.href,
					data: oValues,
					type: "POST",
					error: function (oData, textStatus){
					    alert("Error: " + textStatus);
					},
					success: function (oData, textStatus)
					{
					    oCombo.SessionData = "";
						oCombo.Data = (oData.Records == undefined) ? oData : oData.Records;
						oCombo.load.hide();
						oCombo.AjaxRequest = null;
						if (textStatus == "success")
						{
							if (cStr(oValues.__ID) != "")
							{
								if (oData != null) oCombo.setValue(oValues.__ID);
							} else
							{
								oCombo.Data.Query = oValues.__QUERY;
								if (oCombo.content.hasClass("active")) fnJComboShow(oValues.__QUERY, true);
							}
						} else
						{
							alert("An error occurred: " + textStatus + ". Please try your request again.");
							oCombo.txt.val("");
							oCombo.list.hide();
						}
					}
				});

        };

        oCombo.addBit = function(sID, sTxt) {
            if (("|" + oCombo.hid.val() + "|").indexOf("|" + sID + "|") == -1) {
                oCombo.hid.val(oCombo.hid.val() + "|" + sID);
				oCombo.txtValues[sID] = sTxt;
                $.data($("<a href='javascript:void(0);' id='jc_bit'><div class='bit'><div class='bit'><div class='bit'><table class='bit'><tr><td>" + sTxt + "</td><td id='jc_bit_del'></td></tr></table></div></div></div></a>").insertBefore(oCombo.txt).bind("mouseup",function(e){if(fnEventButton(e)=="right"){oCombo.remove($(this).find("#jc_bit_del")[0]);return false;}}).dblclick(function() { if (oCombo.Options.onDblClick&&!oCombo.Options.Disable) { oCombo.Options.onDblClick(oCombo, sID, this); } }).find("#jc_bit_del").toggle(!oCombo.Options.Disable).click(function() { oCombo.remove(this) })[0], "index", oCombo.hid.val().split("|").length - 1);
                return true;
            }
            return false;
        };

        oCombo.setValue = function(sVal, sTxt,bIgnoreIfInvalid) {
            if(!oCombo.Init && oCombo.Options.Value!=sVal) oCombo.Options.Value=sVal;
                
            sTxt = cStr(sTxt);
            var sOld = oCombo.hid.val();
            var i=-1;
            if (cStr(sVal).indexOf("\n") > -1) {
                sTxt = sVal.split("\n")[1];
                sVal = sVal.split("\n")[0];
            }
            oCombo.txt.val("").removeClass('watermark');
            oCombo.hid.val("");
            oCombo.content.find("a[id='jc_bit']").remove();
            
            if(sTxt==""&&oCombo.Data==null&&oCombo.Options.DataMode=="server"){
                oCombo.hid.val((sVal+"").replace(/(<([^>]+)>)/ig,""));
				var oValues = oCombo.getPostbackVariables("",sVal);
				if (oCombo.Options.onPostback != null && !oCombo.Options.onPostback(oCombo, oValues))return;
                oCombo.getServerData(oValues);
                return;
            }
            PrepareValues();
                        
            if (oCombo.Options.Multi) {
                if (sVal !== '') {
                    if (oCombo.Data == null) {
                        if (sTxt.split("|").length == sVal.split("|").length) {
                            for (var x = 0; x < sVal.split("|").length; x++) oCombo.addBit(sVal.split("|")[x], sTxt.split("|")[x]);

                        } else {
                            //Not handled yet!
                        }
                    } else {
                        for (var x = 0; x < oCombo.Data.length; x++) if (("|" + sVal + "|").indexOf("|" + oCombo.Data[x]._value + "|") > -1) oCombo.addBit(oCombo.Data[x]._value, oCombo.Data[x]._text);
                    }
                }
            } else {
                oCombo.hid.val((sVal+"").replace(/(<([^>]+)>)/ig,""));
                oCombo.txt.val((sTxt+"").replace(/(<([^>]+)>)/ig,""));
                
                if (sVal != "" && sTxt == "" && oCombo.Data != null) {
                    for (var x = 0; x < oCombo.Data.length; x++)
                        if (oCombo.Data[x]._value == sVal && (oCombo.Data[x]._value != false || typeof oCombo.Data[x]._value != "boolean")) {
                        sTxt = (oCombo.Data[x]._text+"").replace(/(<([^>]+)>)/ig,"");
                            i = x;
                            break;
                        }
                    oCombo.txt.val(sTxt);
                    if (sTxt == "") {                        
                        if (i == -1 && sVal != "" && sVal != "0" && oCombo.Options.DataMode != "server" && oCombo.Options.Bound) {
                            if (oCombo.Options.onInvalidIDError && !bIgnoreIfInvalid) {
                                var o = new Object();
                                o._formatted = "*** ID " + sVal + " not found ***";
                                o._text = "*** ID " + sVal + " not found ***";
                                o._value = sVal;
                                o._undeletable = true;
                                if (oCombo.Options.onInvalidIDError == true || oCombo.Options.onInvalidIDError(o, oCombo)) {
                                    i = oCombo.Data.length;
                                    for (var x = 0; x < oCombo.Data.length; x++) {
                                        if (o._formatted < oCombo.Data[x]._formatted) {
                                            i = x;
                                            break;
                                        }
                                    }
                                    oCombo.Data.splice(i, 0, o);
                                oCombo.txt.val((o._text+"").replace(/(<([^>]+)>)/ig,""));
                                }
                            } else {
                                oCombo.hid.val("");
                            }
                        }
                    }
                }
            }
            if (oCombo.txt.val() == "" && oCombo.Data && !oCombo.Options.Bound) {
                oCombo.txt.val(oCombo.hid.val());
                oCombo.hid.val("");
            }
            if (oCombo.txt.val() == "") oCombo.txt.val(oCombo.Options.Watermark).addClass('watermark');            
            oCombo.txt.attr("clean", oCombo.txt.val())[0].resize();            
            if (oCombo.Init && sOld != oCombo.hid.val() && oCombo.Options.onChange) oCombo.Options.onChange(oCombo, (i>-1?oCombo.Data[i]:null));
            return oCombo;
        };
        oCombo.getValue = function(bValueAndText) {
            if(!oCombo.Init)return oCombo.Options.Value;
            var s = oCombo.txt.val(); var v = oCombo.hid.val();
            if (s == oCombo.Options.Watermark) s = "";
            if (oCombo.Options.Multi) {
                s = "";
                if (bValueAndText) {
                    var o = oCombo.div.find("#jc_bit span:eq(0)");
                    for (var x = 0; x < o.length; x++) s += (s == "" ? "" : "|") + $(o[x]).html();
                }
                var o = v.split("|"); v = "";
                for (var x = 0; x < o.length; x++) if (o[x] != "") v += (v == "" ? "" : "|") + o[x];
            } else if (!oCombo.Options.Bound)
                return s + (bValueAndText && s != "" ? "\n" + s : "");
            return v + (bValueAndText && s != "" ? "\n" + s : "");
        };
        oCombo.getIndex = function() {
            var s = oCombo.getValue(false).split("|")[0];
            if (s != "") {
                for (var x = 0; x < oCombo.Data.length; x++)
                    if (s == oCombo.Data[x]._value) return x;
            }
            return -1;
        };
        oCombo.setIndex=function(i){
            oCombo.getValue(false);
            var j=0;
            for (var x = 0; x < oCombo.Data.length; x++)
                    if (oCombo.Data[x]._value != false || typeof oCombo.Data[x]._value != "boolean"){
                        if(i==j){                            
                            oCombo.setValue(oCombo.Data[x]._value,oCombo.Data[x]._text);
                            return true;
                        }
                        j++;                        
                    }
            return false;
        };
        oCombo.length=function(){
            var i=0;
            oCombo.getValue(false);
            for (var x = 0; x < oCombo.Data.length; x++)
                    if (oCombo.Data[x]._value != false || typeof oCombo.Data[x]._value != "boolean") i++;
            return i;
        }
        oCombo.getText = function() { return oCombo.txt.val(); };
        oCombo.disable = function(b) { if (b == undefined) return oCombo.Options.Disable; if(oCombo.Init && oCombo.Options.Disable == b)return; oCombo.Options.Disable = b; if (oCombo.Options.Arrow) oCombo.btn.toggleClass("disabled",b); oCombo.content.toggleClass("jc_disable", b); if (b) oCombo.txt.val("");else oCombo.setValue(oCombo.getValue()); };
        oCombo.render(div,oCombo);
        $(document.body).click(function(e){if(oCombo.visible()&&e.target&&$(oCombo.div).has(e.target).length==0){oCombo.txt.blur();}});
        
        function fnJComboShow(sVal, IsRecursiveCall) 
		{
			if(IsRecursiveCall == undefined) IsRecursiveCall = false;

			if (oCombo.Options.Watermark !== '' && sVal === oCombo.Options.Watermark)
				sVal = '';

            sVal = sVal.toLowerCase();
			
            if (!oCombo.list)
                oCombo.list = $(document.createElement('div')).attr('id', 'jc_results').css('height', oCombo.Options.Height).css('width', ((oCombo.Options.LWidth == "" ? (oCombo.content.outerWidth() - 2 - 16) + 'px' : oCombo.Options.LWidth+"px"))).css('left', 0).appendTo($("<div style='position:relative;'></div>").appendTo(oCombo.content)).hide().mousedown(function() { oCombo.content.addClass("hover"); }).mouseup(function() { if (oCombo.content.hasClass("hover")) { oCombo.content.removeClass("hover"); if (oCombo.content.hasClass("active")) oCombo.txt.focus(); else oCombo.txt.blur(); } }).mouseleave(function() { oCombo.list.mouseup(); }).mousemove(function(event) { if (event.which == 0 || event.button == 0) oCombo.list.mouseup(); });

            if ($.data(oCombo.list.show(), "value") == sVal) return;
            var oValues = oCombo.getPostbackVariables(sVal);
            if (oCombo.Options.DataMode=="server") {
                if (oCombo.Options.onPostback != null && !oCombo.Options.onPostback(oCombo, oValues))return;
                //do we need to issue new query
                if (oCombo.Data == null || oCombo.Data.Query == null || (sVal.toLowerCase().indexOf(oCombo.Data.Query.toLowerCase()) != 0 || (oCombo.Data.length >= oCombo.Options.Max && sVal != oCombo.Data.Query) || (oCombo.Options.AlwaysQuery && !IsRecursiveCall))) {
                    oCombo.load.show();
                    oCombo.list.show().html("<div style='padding:4px;'><b>Please wait...</b></div>").css('top', oCombo.content.outerHeight() - 1);
                    if (oCombo.AjaxTimer) clearTimeout(oCombo.AjaxTimer);
                    oCombo.AjaxTimer = setTimeout(function() { oCombo.getServerData(oValues); }, 400);
                    return;
                }
            }
            $.data(oCombo.list, "value", sVal);
            var iActive = (oCombo.items && oCombo.items.filter(".jc_over").length > 0 && $.data(oCombo.items.filter(".jc_over")[0], "jc") != null ? $.data(oCombo.items.filter(".jc_over")[0], "jc")._value : null);
            oCombo.list.show().empty().css('top', oCombo.content.outerHeight() - 1).scrollTop(0);

            var ul = $("<ul/>").appendTo(oCombo.list).mouseover(function(event) {
                if ($(event.target).closest("li").length > 0) {
                    active = $("li", ul).removeClass("jc_over").index($(event.target).closest("li")[0]);
                    $(event.target).closest("li").addClass("jc_over");
                }
            }).click(function(event) {
                $(event.target).closest("li").addClass("jc_over");
                oCombo.select();
                oCombo.txt.focus();
                return false;
            });

            var i = 0, oMatch, oRegEx, oRegM;
            if (cStr(sVal) != "") oRegEx = new RegExp("(?!<[^<>]*)(" + sVal.replace(/>/gi, "&gt;").replace(/</gi, "&lt;").replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1").replace(/\%/g, '(.)*') + ")(?![^<>]*>)", "gi");

        	//Add None Value
            if (oCombo.Options.FixedNoneItem != null)
            {
            	var noneData = jQuery.parseJSON(oCombo.Options.FixedNoneItem); 
            	noneData._formatted = "" + oCombo.Options.getItem(noneData, x, oCombo);
            	noneData._text = "" + oCombo.Options.getText(noneData, x, oCombo);
            	noneData._value = oCombo.Options.getValue(noneData, x, oCombo);
            	noneData._isNoneItem = true;
            	oCombo.Data.splice(0, 0, noneData);
            }

			//Process Data
            for (var x = 0; x < oCombo.Data.length && i < oCombo.Options.Max; x++) {
                if (oCombo.Data[x]._text == null || oCombo.Data[x]._text == "undefined" || oCombo.Data[x]._value == false) 
				{
                    oCombo.Data[x]._value = oCombo.Options.getValue(oCombo.Data[x], x, oCombo);
                    if(oCombo.Data[x]._value!==false){
                        oCombo.Data[x]._formatted = "" + oCombo.Options.getItem(oCombo.Data[x], x, oCombo);
                        oCombo.Data[x]._text = "" + oCombo.Options.getText(oCombo.Data[x], x, oCombo);                    
                    }
                }
                if (oCombo.Data[x]._value === false) continue;                                
				//var oRegEx = new RegExp("(?!<[^<>]*)(" + sVal.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1").replace(/\%/g, '(.)*') + ")(?![^<>]*>)", "gi");
                if(oRegEx) oRegM = oCombo.Data[x]._formatted.match(oRegEx);
                if (!oCombo.Options.LFilter || oRegM != null || oRegEx == null || (noneData && noneData._isNoneItem == true))
                {
                    var s=oCombo.Data[x]._formatted;
                    if(oRegEx)s=s.replace(oRegEx, "<b>$1</b>");
                    var li = $("<li/>").html(s).addClass((oRegM != null && iActive == oCombo.Data[x]._value ? "jc_over " : "") + (i % 2 == 0 ? "jc_even" : "jc_odd")).appendTo(ul)[0];
                    $.data(li, "jc", oCombo.Data[x]);
                    if (!oMatch && (oCombo.Options.LFilter || oRegM != null)) oMatch = li;
                    i++;
                }
            }
            

            oCombo.items = ul.find("li");
            if (oCombo.Options.SelectFirst && oCombo.items.filter(".jc_over").length == 0 && oMatch) $(oMatch).addClass("jc_over");
            if (oCombo.scroll() && oCombo.current()) oCombo.list.scrollTop(0).scrollTop($(oCombo.current()).position().top - (oCombo.list.height() / 2));

            if (i == 0) oCombo.list.html("<div style='padding:4px;'><b>" + oCombo.Options.NoRecords + "</b></div>");
        }
        oCombo.select = function() {
            var c = oCombo.current();
            if (c) {
                if (oCombo.Options.Multi == true) {
                    if (oCombo.addBit($.data(c, "jc")._value, $.data(c, "jc")._text) && oCombo.Options.onChange) oCombo.Options.onChange(oCombo, $.data(c, "jc"));
                    oCombo.txt.val("");
                    oCombo.list.hide();
                } else {
                    c = $.data(c, "jc") || (oCombo.Data ? oCombo.Data[0] : null);
                    if (c) {
                        var b = c._value != oCombo.hid.val();
                        oCombo.hid.val((c._value+"").replace(/(<([^>]+)>)/ig,""));
                        oCombo.txt.val((c._text+"").replace(/(<([^>]+)>)/ig,""));
                        oCombo.txt.attr("clean", c._text);
                        if (b && oCombo.Options.onChange) oCombo.Options.onChange(oCombo, c);
                    }
                    oCombo.list.hide();
                }
            }
            oCombo.txt[0].resize();
        };
        function fnJComboKey(e) {

            var iKey = e.keyCode || e.which;
            if ("|16|17|18|37|39|".indexOf("|" + iKey + "|") > -1) return; // 16 = Shift, 17 = Ctrl,18=Alt, 37=left arrow, 39=right arrow
            if ("|38|".indexOf("|" + iKey + "|") > -1 && !oCombo.visible()) return; // 38 = Up arrow            
            if ("|13|9|".indexOf("|" + iKey + "|") > -1) { //Enter, TAB
                if (iKey == 13 || oCombo.Options.Required) oCombo.select();
                if (iKey == 13) {
                    if (e.preventDefault) e.preventDefault();
                    if (e.stopPropagation) e.stopPropagation();
                }
                return;
            }
            if (iKey == 27) {//Esc
                oCombo.txt.val("");
                if (oCombo.list) oCombo.list.hide();
                if (e.preventDefault) e.preventDefault();
                if (e.stopPropagation) e.stopPropagation();
                return true;
            }
            if (iKey == 8 && oCombo.txt.val().length == 0) {//Backspace
                var o = oCombo.txt.parent().find("a#jc_bit span#jc_bit_del");
                if (o.length > 0) {
                    if (oCombo.Options.Backspace) oCombo.remove(o[o.length - 1]);
                } else if (!oCombo.Options.SelectFirst) {
                    var sOld=oCombo.hid.val();
                    oCombo.hid.val("");                    
                    if(oCombo.list){
                    $.data(oCombo.list, "value", null);
                    oCombo.list.hide();
                    }
                    if (oCombo.Options.onChange && sOld!="") oCombo.Options.onChange(oCombo, null);
                    return;
                }
            }
            setTimeout(function() {
                fnJComboShow(oCombo.txt.val());
                oCombo.txt[0].resize();
            }, 100);

            if ("|38|40|36|35|33|34|".indexOf("|" + iKey + "|") > -1 && oCombo.items && oCombo.Data) {//38 = Up ,40=down, 36=home, 35=end,33=page up, 34=page down
                var curr = oCombo.items.filter(".jc_over");
                var active = (iKey == 38 ? curr.prev("li") : (iKey == 40 ? curr.next("li") : (iKey == 33 ? curr.prevAll("li:eq(5)") : (iKey == 34 ? curr.nextAll("li:eq(5)") : null))));
                if (active == null || active.length == 0) {
                    if (iKey == 38 || iKey == 35 || iKey == 34) active = oCombo.items.filter(":last");
                    if ((iKey == 36 || iKey == 33) || (iKey == 40 && curr.length == 0)) active = oCombo.items.filter(":first");
                }

                //get more data
                if ((iKey == 40 || iKey == 34) && (active.length == 0 || active.is(":last-child"))) {
                    if (oCombo.Data.length >= oCombo.Options.Max) {
                        oCombo.Options.Max += 10;
                        $.data(oCombo.list, "value", null);
                    }
                    if (oCombo.Options.DataMode != "server" && active.length == 0) active = oCombo.items.filter(":first");
                }

                if (active.length > 0) {
                    if (!active.hasClass("jc_over")) {
                        oCombo.items.removeClass("jc_over");
                        active.addClass("jc_over");
                        if (oCombo.scroll()) oCombo.list.scrollTop(0).scrollTop(active.position().top - (oCombo.list.height() / 2));
                    }
                }
            }
        }    
		
		return oCombo;    
    }
    $.JCombo.defaults = {        
        Value: "",
        Text: "",
        Data: null,
        Arrow: true,
        Url: null,
        Multi: true,
        Watermark: "",
        Max: 100,
        SelectFirst: true,
        getText: function(oRow, iIndex, oCombo) { return "" + oRow[1]; },
        getItem: function(oRow, iIndex, oCombo) { return this.getText(oRow, iIndex, oCombo); },
        getValue: function(oRow, iIndex, oCombo) { return oRow[0]; },
        onPostback: null,
        JSName: null,
        onChange: null,
        onDblClick: null,		
        Width: "",
        LWidth: "",
        Scroll: true,
        ID: "",        
        DataMode: "client",
        Required: true,
        Disable: false,
        LFilter: true,
        Backspace: false,
        Bound: true,
        NoRecords: "",
        onInvalidIDError: true,
        AlwaysQuery: false,
    	FixedNoneItem: null
    };
    $.JCombo.unescape= function(s) { return replaceAll(s,"&lt;","<","&gt;",">"); };
})(jQuery);