﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using jlib.components.button;
using jlib.db;
using jlib.functions;
using System.ComponentModel;

namespace jlib.controls.jcombo
{
	//Delegates
	public delegate void OnComboDatabindHandler(object sender, jComboDataEventArgs e);
    public delegate void OnComboBeforeDatabindHandler(object sender, jComboDataEventArgs e);
	
	//Enums
	public enum DataModes
	{
		Client,
		Server
	}

	public abstract class jCombo : System.Web.UI.HtmlControls.HtmlGenericControl, System.Web.UI.INamingContainer, jlib.components.iControl
	{
		#region ***** EVENTS *****
		public event OnComboDatabindHandler ComboDatabind;
		protected virtual void OnComboDatabind(jComboDataEventArgs e)
		{
			if (ComboDatabind != null) 
				ComboDatabind(this, e);
		}
        public event OnComboBeforeDatabindHandler ComboBeforeDatabind;
        protected virtual void OnComboBeforeDatabind(jComboDataEventArgs e) {
            if (ComboBeforeDatabind != null) ComboBeforeDatabind(this, e);
        }
		#endregion

		#region ***** CONSTRUCTORS *****
		public jCombo(string s)
		{
			this.TagName = "div";

			//Property Default Values
			this.DataMember = "";
			this.OnPostback = "";
			this.OnChange = "";
			this.Disable = false;
			this.Required = false;
			this.Enabled = true;
			this.DisableSave = false;
			this.DisplayArrow = true;
			this.DataSourceType = sqlbuilder.DataServerTypes.SQLServer;
			this.DataMode = DataModes.Client;
			this.IsValueInitialized = false;
			this.IsRendered = false;
			this.IsDataBindCalled = false;
		}
		#endregion

		#region ***** PROPERTIES *****
		public string DataMember { get; set; }
		public string OnPostback { get; set; }
		public string OnChange { get; set; }
        public string Height { get; set; }
		public bool Disable { get; set; }
        public bool WildcardMatch { get; set; }
		public bool Required { get; set; }
		public bool Enabled { get; set; }
		public bool DisableSave { get; set; }
        public bool DisplayArrow { get; set; }
        public sqlbuilder.DataServerTypes DataSourceType { get; set; }
		public DataModes DataMode { get; set; }
		public bool IsValueInitialized { get; protected set; }
		public bool IsRendered { get; protected set; }
		public bool IsDataBindCalled { get; protected set; }
		public bool ReadOnly { get; set; }
        public bool Multi { get; set; }
		public string FixedNoneItem { get; set; }        

		/// <summary>
		/// 
		/// </summary>
		public bool IsComboDatabindNull
		{
			get { return this.ComboDatabind == null && this.ComboBeforeDatabind==null; }
		}

		/// <summary>
		/// 
		/// </summary>
		public bool DataModeClient 
		{
			get { return this.DataMode == DataModes.Client; }
			set { this.DataMode = (value) ? DataModes.Client : DataModes.Server; }

		}

		/// <summary>
		/// The Value of the Combo Box
		/// </summary>
		public string Value
		{
			get
			{
				return convert.cStr(this.GetValue());
			}
			set
			{
				this.SetValue(value);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string DataTextField
		{
			get
			{
				return this._DataTextField;
			}
			set
			{
				this.DataModeClient = String.IsNullOrWhiteSpace(value);
				this._DataTextField = value;
			}
		}
		private string _DataTextField = "";
		
		/// <summary>
		/// 
		/// </summary>
		public string DataValueField
		{
			get
			{
				return this._DataValueField;
			}
			set
			{
				this.DataModeClient = String.IsNullOrWhiteSpace(value);
				this._DataValueField = value;
			}
		}
		private string _DataValueField = "";


		/// <summary>
		/// 
		/// </summary>
		public string FieldDescription
		{
			get
			{

				if (!String.IsNullOrWhiteSpace(_FieldDescription))
					return _FieldDescription;

				if (!String.IsNullOrWhiteSpace(this.DataMember))
				{
					if (this.DataMember.IndexOf(".") != -1)
						return parse.split(this.DataMember, ".")[1];
					else
						return this.DataMember;
				}
				return this.ID;
			}

			set
			{
				_FieldDescription = value;
			}
		}
		private string _FieldDescription = "";
		
		public int MaxRecords
		{
			get
			{
				return convert.cInt(Page.Request.QueryString["max"], this.Attributes["max"], 100);
			}
		}
        public string SessionData
		{
			get
			{
				return convert.cStr(Page.Request.Form["SessionData"], this.Attributes["SessionData"]);
			}
		}        

		#endregion


		#region ***** ABSTRACT PROPERTIES / METHODS *****
		protected internal abstract bool IsDataSourceProvided { get; }
		protected internal abstract void SetDataSource(IListSource value);
		protected internal abstract IListSource GetDataSource();
		protected internal abstract IListSource GetInitialValues();
		protected internal abstract List<string> GetInitialValuesText();
		#endregion

		#region ***** METHODS *****
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string validate()
		{
			if (Required && (parse.replaceAll(this.GetValue(), "|", "", "\n", "").Trim() == "" || this.GetValue().Str() == "0" || this.GetValue().Str() == "-1"))
				return String.Format((System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "{0} is a required field." : "{0} må være fylt ut."), this.FieldDescription);

			return "";
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="oValue"></param>
		/// <returns></returns>
		public components.iControl SetValue(object Value)
		{
			this.Attributes["initial-value"] = convert.cStr(parse.replaceAll(Value, "|", "") == "" ? "" : Value);
			this.IsValueInitialized = true;
			return this;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public object GetValue()
		{
			if (!this.IsValueInitialized) Page_Init(null, null);
			return this.Attributes["initial-value"];
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public List<string> GetValuesList()
		{
			//Get Value Formatted String
			string value = parse.stripLeadingCharacter(parse.stripEndingCharacter(parse.replaceAll(this.GetValue(), false, true, "||", "|"), "|"), "|");

			//Convert to List
			List<string> values = parse.split(value, "|").ToList<string>();

			//Return Values
			return values;
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="bFailure"></param>
		public void setVaildateFailure(bool bFailure)
		{
			if (bFailure)
			{
				this.Attributes["class"] = parse.replaceAll(this.Attributes["class"], " validate_failure", "") + " validate_failure";
			}
			else
			{
				this.Attributes["class"] = parse.replaceAll(this.Attributes["class"], " validate_failure", "");
			}
		}

		#endregion

		#region ***** CONTROL LIFE CYCLE *****

		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
		protected override void OnInit(EventArgs e)
		{
			//trigger here and in Page_Init
			Page_InitComplete(null, null);
			base.OnInit(e);
			//this.Page.InitComplete += new EventHandler(Page_InitComplete);
			//this.Page.PreRenderComplete += new EventHandler(Page_PreRenderComplete);
			this.Page.Init += new EventHandler(Page_Init);
			this.PreRender += new EventHandler(combo_PreRender);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public void Page_Init(object sender, EventArgs e)
		{
			Page_InitComplete(null, null);

            if (Page.IsPostBack) 
                this.SetValue(Page.Request[this.ClientID]);             
		}

        /// <summary>
        /// Will do an immediate DataBind and will cause Page Cycle execution to end if this is a data-callback directed at this instance of the control
        /// </summary>
        public void DataBind() {
            Page_Init(null, null);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public virtual void combo_PreRender(object sender, EventArgs e)
		{
			//if (Enabled) Page.ClientScript.RegisterStartupScript(this.GetType(), "JCombo_INIT-" + this.ClientID, "$('#" + this.ClientID + "').JCombo({ options: 'tag' });", true);
			if (this.DataModeClient && this.IsDataSourceProvided)
			{
				//Get Entire Working Set
				IListSource data = this.GetDataSource();
				
				//Output Values as JSON
				Page.ClientScript.RegisterClientScriptBlock(this.GetType(), data.GetHashCode().ToString().ToLower(), "var JCombo" + Math.Abs(data.GetHashCode()).ToString() + "=" + jlib.functions.convert.cJSON(data) + ";", true);

				//Set Data Attribute
				this.Attributes["data"] = "JCombo" + Math.Abs(data.GetHashCode());
			}

			//Get Initial Values
			if (!this.DataModeClient && !String.IsNullOrWhiteSpace(convert.cStr(this.GetValue())) && this.IsDataSourceProvided)
			{
				//Get Initial Values
				IListSource currentValues = this.GetInitialValues();
				
				//Output Values as JSON
				Page.ClientScript.RegisterClientScriptBlock(this.GetType(), this.ClientID, "var JCombo" + this.ClientID + "=" + jlib.functions.convert.cJSON(currentValues) + ";", true);

				//Set Data Attribute
				this.Attributes["data"] = "JCombo" + this.ClientID;

			}
            this.Attributes["arrow"] = DisplayArrow ? "true" : "false";
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="ex"></param>
		public virtual void Page_InitComplete(object sender, EventArgs ex)
		{
			if ((Page as jlib.components.webpage).EventTarget == this.ClientID && (this.IsDataSourceProvided || !this.IsComboDatabindNull))
			{
				//If this method already called
				if (this.IsDataBindCalled)
					return;

				//Set Called Flag
				this.IsDataBindCalled = true;

				//If Postback, Set current value from request
				if (Page.IsPostBack)
					this.SetValue(Page.Request[this.ClientID]);

				//Clear Page Output
				Page.Response.Clear();

				//Get JSON Output
				IListSource data = this.GetDataSource();

				//Output Data
				Page.Response.Write(convert.cJSON(data));
				Page.Response.End();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="writer"></param>
		protected override void Render(HtmlTextWriter writer)
		{
			//Output as a Label
			if (this.ReadOnly)
			{
				//Create Values List
				Literal literal = new Literal();
				literal.Text = this.GetInitialValuesText().Join(",");
				literal.ID = this.ID + "_readonly";
				literal.RenderControl(writer);
				return;
			}

			//Set Rendered
			this.IsRendered = true;

			this.Attributes["datamode"] = this.DataMode.ToString().ToLower();
			this.Attributes["required"] = this.Required.ToString().ToLower();
            if (this.Attributes["class"].Str().IndexOf("JCombo") == -1) this.Attributes["class"] += " JCombo";

			if (OnChange != "") this.Attributes["onchange"] = OnChange;
	
			if (OnPostback != "") this.Attributes["onpostback"] = OnPostback;
            
            if (Height.Str() != "") this.Attributes["height"] = Height.ToLower().Replace("px", "");

			if (Disable) this.Attributes["disable"] = "true";

            if (Multi) this.Attributes["multi"] = "true";

			if (!String.IsNullOrWhiteSpace(this.FixedNoneItem)) this.Attributes["FixedNoneItem"] = this.FixedNoneItem;


			//Render Control into StringWriter
			base.Render(writer);

			//Output JS Header
			writer.Write("<script language=\"javascript\">$(document).ready(function(){$('#" + this.ClientID + "').JCombo({options:'tag'});});</script>");
		}

		#endregion

		#region ***** OBSOLETE METHODS / PROPERTIES *****
		[Obsolete]
		public components.iControl setValue(object Value)
		{
			return this.SetValue(Value);
		}
		[Obsolete]
		public object getValue()
		{
			return this.GetValue();
		}
		[Obsolete]
		public bool Rendered
		{
			get { return this.IsRendered; }
		}
		[Obsolete]
		public bool ValueInitialized
		{
			get { return this.IsValueInitialized; }
		}
		[Obsolete]
		public bool DataBindCalled
		{
			get { return this.IsDataBindCalled; }
		}
		#endregion

		
	}
}

