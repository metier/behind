﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.controls.jcombo
{
    public class jComboDataEventArgsAdo : jComboDataEventArgs
	{
		#region ***** CONSTRUCTORS *****
        public jComboDataEventArgsAdo(jCombo jCombo, string InitialID, string Fields, string Tables, string Filter, string Sort, string Query, int Max, string SessionData)
            : base(jCombo, InitialID, Query, Max, SessionData)
		{
            this.Fields = Fields;
            this.Tables = Tables;
            this.Filter = Filter;
            this.Sort = Sort;
        }
        public jComboDataEventArgsAdo(jCombo jCombo, string InitialID, string SQL, string Query, int Max, string SessionData)
            : base(jCombo, InitialID, Query, Max, SessionData)
		{
			this.SQL = SQL;
        }
		#endregion

		#region ***** PROPERTIES *****
        public string Fields { get; set; } 
		public string Tables { get; set; } 
		public string Filter { get; set; } 
		public string Sort { get; set; }
		public string SQL { get; set; }
		#endregion

		#region ***** METHODS *****
		#endregion

    }
}
