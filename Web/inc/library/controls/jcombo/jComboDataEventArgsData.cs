﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jlib.DataFramework.QueryBuilder;

namespace jlib.controls.jcombo
{
    public class jComboDataEventArgsData : jComboDataEventArgs
	{
		#region ***** CONSTRUCTORS *****
        public jComboDataEventArgsData(jCombo jCombo, string InitialID, QueryBuilder QueryBuilder, string Query, int Max, string SessionData)
            : base(jCombo, InitialID, Query, Max, SessionData)
		{
        }
		#endregion

		#region ***** PROPERTIES *****
		public QueryBuilder QueryBuilder { get; set; }
		#endregion

		#region ***** METHODS *****
		#endregion

    }
}
