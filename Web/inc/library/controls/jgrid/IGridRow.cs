﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.controls.jgrid
{
	public interface IGridRow
	{
		jGrid OwnerGrid { get; set; }
		void OnGridDatabind(object sender, jlib.controls.jgrid.GridCellEventArgs e);
		void OnGridInit(object sender, jlib.controls.jgrid.GridCellEventArgs e);

	}
}
