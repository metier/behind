﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Xml;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using jlib.components.button;
using jlib.db;
using jlib.functions;
using System.Linq;
using SpreadsheetGear;
using System.ComponentModel;

namespace jlib.controls.jgrid
{
	public delegate void OnGridDropdownDataLoaded(object sender, GridCellEventArgs e);
	public delegate void OnGridBeforeFilterHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridAfterFilterHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridRowPrerenderHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridRowPostrenderHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridPrintPrerenderHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridRowPrintPrerenderHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridCellPrintPrerenderHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridLoadHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridBeforeDatabindHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridBeforeDataQueryHandler(object sender, GridDataEventArgs e);
	public delegate void OnGridDatabindHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridRowClientDelHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridRowClientUpdateHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridRowClientAddHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridCallbackHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridCustomDataHandler(object sender, GridCellEventArgs e);
	public delegate void OnGridViewsLoadHandler(jGrid Grid);
	public delegate void OnGridViewsSaveHandler(jGrid Grid);
	public delegate void OnGridRowAddHandler(object sender, EventArgs e);

	[ParseChildren(true)]
	public abstract class jGrid : System.Web.UI.WebControls.Table, System.Web.UI.INamingContainer, System.ICloneable
	{

		#region ***** CONSTRUCTORS *****
		public jGrid()
		{
			this.Pagination = true;
			this.DataViewFiltering = true;
			this.DisableInitialFiltering = false;
			this.DisableInitialSorting = false;
			this.DisableInitialView = false;
			this.BindDropdownByID = false;
			this.DisableDataViewFiltering = false;

			this.Columns = new List<GridColumn>();
			this.MetaData = new GridMetaData();
			this.Views = new GridViews(this);
			this.PK = "ID";
		}
		#endregion

		#region ***** EVENTS *****
		public event OnGridDropdownDataLoaded GridDropdownDataLoaded;
		public event OnGridBeforeFilterHandler GridBeforeFilter;
		public event OnGridAfterFilterHandler GridAfterFilter;
		public event OnGridRowPrerenderHandler GridRowPrerender;
		public event OnGridRowPostrenderHandler GridRowPostrender;
		public event OnGridPrintPrerenderHandler GridPrintPrerender;
		public event OnGridRowPrintPrerenderHandler GridRowPrintPrerender;
		public event OnGridCellPrintPrerenderHandler GridCellPrintPrerender;

		public event OnGridLoadHandler GridLoad;
		public event OnGridDatabindHandler GridDatabind;
		public event OnGridBeforeDataQueryHandler GridBeforeDataQuery;
		public event OnGridBeforeDatabindHandler GridBeforeDatabind;

		public event OnGridRowClientDelHandler GridRowClientDel;
		public event OnGridRowAddHandler GridRowAdd;
		public event OnGridRowClientUpdateHandler GridRowClientUpdate;
		public event OnGridRowClientAddHandler GridRowClientAdd;
		public event OnGridCallbackHandler GridCallback;
		public event OnGridCustomDataHandler GridCustomData;

		public event OnGridViewsLoadHandler GridViewsLoad;
		public event OnGridViewsSaveHandler GridViewsSave;

		public virtual void OnGridDropdownDataLoaded(GridCellEventArgs e)
		{
			if (GridDropdownDataLoaded != null) GridDropdownDataLoaded(this, e);
		}
		protected virtual void OnGridBeforeFilter(GridCellEventArgs e)
		{
			if (GridBeforeFilter != null) GridBeforeFilter(this, e);
		}
		protected virtual void OnGridAfterFilter(GridCellEventArgs e)
		{
			if (GridAfterFilter != null) GridAfterFilter(this, e);
		}
		protected virtual void OnGridPrintPrerender(GridCellEventArgs e)
		{
			if (GridPrintPrerender != null) GridPrintPrerender(this, e);
		}
		protected virtual void OnGridRowPrintPrerender(GridCellEventArgs e)
		{
			if (GridRowPrintPrerender != null) GridRowPrintPrerender(this, e);
		}
		protected virtual void OnGridCellPrintPrerender(GridCellEventArgs e)
		{
			if (GridCellPrintPrerender != null) GridCellPrintPrerender(this, e);
		}
		protected virtual void OnGridLoad(GridCellEventArgs e)
		{
			if (GridLoad != null) GridLoad(this, e);
		}
		protected virtual void OnGridDatabind(GridCellEventArgs e)
		{
			if (GridDatabind != null) GridDatabind(this, e);
		}
		protected virtual void OnGridBeforeDatabind(GridCellEventArgs e)
		{
			if (GridBeforeDatabind != null) GridBeforeDatabind(this, e);
		}
		protected virtual void OnGridBeforeDataQuery(GridDataEventArgs e)
		{
			if (GridBeforeDataQuery != null) GridBeforeDataQuery(this, e);
		}
		protected virtual void OnGridRowClientDel(GridCellEventArgs e)
		{
			if (GridRowClientDel != null) GridRowClientDel(this, e);
		}
		protected virtual void OnGridRowClientUpdate(GridCellEventArgs e)
		{
			if (GridRowClientUpdate != null) GridRowClientUpdate(this, e);
		}
		protected virtual void OnGridRowClientAdd(GridCellEventArgs e)
		{
			if (GridRowClientAdd != null) GridRowClientAdd(this, e);
		}
		protected virtual void OnGridCallback(GridCellEventArgs e)
		{
			if (GridCallback != null) GridCallback(this, e);
		}
		protected virtual void OnGridCustomData(GridCellEventArgs e)
		{
			if (GridCustomData != null) GridCustomData(this, e);
		}
		protected virtual void OnGridRowAdd(EventArgs e)
		{
			if (GridRowAdd != null) GridRowAdd(this, e);
		}

		#endregion

		#region ***** PROPERTIES *****
		public GridMetaData MetaData { get; private set; }

		public bool Pagination { get; set; }
		public bool DisableInitialView { get; set; }
		public bool DisableInitialFiltering { get; set; }
		public bool DisableInitialSorting { get; set; }
		public bool DataViewFiltering { get; set; }
		public bool DisableDataViewFiltering { get; set; }
		public bool BindDropdownByID { get; set; }
		public string CustomJS { get; set; }
		public string DateCastFunction { get; set; }
		public int NumCols { get; private set; }
		public GridColumn OptionsCol { get; private set; }
		public List<GridColumn> Columns { get; set; }
		public string JSName { get; set; }

		protected internal bool? _DataViewSorting = null;
		public bool DataViewSorting
		{
			get
			{
				if (_DataViewSorting == null) return DataViewFiltering;
				return _DataViewSorting.Value;
			}
			set
			{
				_DataViewSorting = value;
			}
		}

		public string PK { get; set; }
		public GridViews Views { get; set; }
		
		protected internal System.Web.UI.WebControls.PlaceHolder _Cols = null;
        public PlaceHolder Cols
		{
			get { return this._Cols; }
			set
			{
				this._Cols = value;

				for (int x = 0; x < this._Cols.Controls.Count; x++)
				{
					if (this._Cols.Controls[x] as GridColumn != null)
					{
						addColumn(this._Cols.Controls[x] as GridColumn);
					}
				}
				if (String.IsNullOrWhiteSpace(this.Columns[this.MetaData.PKCol].SortField)) throw new Exception("The SortField property must be set on the primary key column!");
			}
			
		}

		public IListSource DataSource	
		{ 
			get
			{
				return this.GetDataSource();
			}
			set
			{
				this.SetDataSource(value);
			}
		}

		#endregion

		#region ***** METADATA PROPERTIES *****
		public bool IsEmbedded
		{
			get
			{
				return this.MetaData.IsEmbedded == 1;
			}
		}

		public bool DisplayFilter
		{
			get
			{
				return this.MetaData.DisplayFilter == 1;
			}
			set
			{
				this.MetaData.DisplayFilter = (value ? 1 : 0);
			}
		}

		public bool DisplayAdd
		{
			get
			{
				return this.MetaData.DisplayAdd == 1;
			}
			set
			{
				this.MetaData.DisplayAdd = (value ? 1 : 0);
			}
		}

		public string OnRowDoubleClick
		{
			get
			{
				return this.MetaData.OnRowDoubleClick;
			}
			set
			{
				this.MetaData.OnRowDoubleClick = parse.replaceAll(value, "'", "\"");
			}
		}
		#endregion

		#region ***** ABSTRACT METHODS *****
		protected internal abstract IListSource GetDataSource();
		protected internal abstract void SetDataSource(IListSource value);
		public abstract List<string> GetDataSourceColumns();
		public abstract IDataRow GetDataSourceRow(int RowIndex);
		public abstract bool DoesDataSourceColumnExist(string ColumnName);
		public abstract int GetDataSourceRowCount();
		#endregion

		#region ***** METHODS *****

		#endregion







		private string m_sParentGridID = "", m_sLoadViewContainer = "";

		private string m_sCellControlPath = "", m_sTAtt = "";
		private bool bOnGridBeforeDatabindCalled = false, m_bInitialized = false, m_bGridPostback = false;
		

		
		
		
		//private System.Collections.Generic.List<string> m_oColumnIDs = new List<string>();

		private GridMetaData m_oRequestMetaData;

		
		private GridView m_oDefaultView;



		

		//Fields for SQL
		
		

		

		
		
		
		public void addLinkedGrid(jGrid Grid)
		{
			this.MetaData.LinkedGridIDs = parse.stripLeadingCharacter(this.MetaData.LinkedGridIDs + "," + Grid.ClientID, ",");
		}

		public string ParentGridID
		{
			set
			{
				m_sParentGridID = value;
			}
		}

		public string CRowClass
		{
			get
			{
				return this.MetaData.CRowClass;
			}
			set
			{
				this.MetaData.CRowClass = value;
			}
		}
		public string SessionData
		{
			get
			{
				return this.MetaData.SessionData;
			}
			set
			{
				this.MetaData.SessionData = value;
			}
		}

		public string DeleteConfirmation
		{
			get
			{
				return this.MetaData.DeleteConfirmation;
			}
			set
			{
				this.MetaData.DeleteConfirmation = value;
			}
		}


		public string CellControlPath
		{
			get
			{
				return m_sCellControlPath;
			}
			set
			{
				m_sCellControlPath = value;
			}
		}
		public string LoadViewContainer
		{
			get
			{
				return m_sLoadViewContainer;
			}
			set
			{
				m_sLoadViewContainer = value;
			}
		}

		public string TAtt
		{
			get
			{
				return m_sTAtt;
			}
			set
			{
				m_sTAtt = " " + value;
			}
		}


		#warning TODO: Abstract to Provider Layer
		public int PageSize
		{
			get
			{
				if (Pagination) return this.Views.SelectedView.PageSize;
				return 999999999;
			}
			set
			{
				this.Views.SelectedView.PageSize = value;
			}
		}

		public int CurrentPage
		{
			get
			{
				if (Pagination) return this.MetaData.Page;
				return 0;
			}
			set
			{
				this.MetaData.Page = value;
			}
		}

		public int NumPages
		{
			get
			{
				if (Pagination) return this.MetaData.Pages;
				return 1;
			}
		}

		public int SortCol
		{
			get
			{
				return this.Views.SelectedView.SortCol;
			}
			set
			{
				this.Views.SelectedView.SortCol = value;
			}
		}

		public string SortOrder
		{
			get
			{
				return (this.Views.SelectedView.SortOrder == 0 ? "asc" : (this.Views.SelectedView.SortOrder == 1 ? "desc" : ""));
			}
			set
			{
				this.Views.SelectedView.SortOrder = (value.ToLower().IndexOf("asc") > -1 ? 0 : (value.ToLower().IndexOf("desc") > -1 ? 1 : -1));
			}
		}

		public string NoRecordsMessage
		{
			get
			{
				return this.MetaData.NoRecordsMessage;
			}
			set
			{
				this.MetaData.NoRecordsMessage = value;
			}
		}

		

		public bool AutoFilter
		{
			get
			{
				return this.MetaData.AutoFilter == 1;
			}
			set
			{
				this.MetaData.AutoFilter = (value ? 1 : 0);
			}
		}


		
		public void addColumn(GridColumn Column)
		{
			//Add Column to end of collection
			this.addColumn(Column, (this.Columns == null ? 0 : this.Columns.Count));
		}
		public void addColumn(GridColumn Column, int Index)
		{
            if (Columns == null) Columns = new List<GridColumn>();
			//If Index outside Bounds
			if (Index > this.Columns.Count) 
				Index = this.Columns.Count;

			//If Column is type Options, then set local pointer
			if (Column.ColType == "Options") 
				this.OptionsCol = Column;

			//If Column does not have an ID, then throw exception
			if (String.IsNullOrWhiteSpace(convert.cStr(Column.ID))) 
				throw new Exception("ID Not set for column #" + Index);

			//Set the Column Grid Reference
			Column.Grid = this;

			//If Column is for Primary Key, set Meta Data Reference
			if (Column.PrimaryKey) 
				this.MetaData.PKCol = Index;

			//Insert Columnet 
			this.Columns.Insert(Index, Column);

			//Determine Number Of Columns
			this.NumCols = this.Columns.Sum(col => Math.Max(col.ColumnSpan, 1));
		}
		public bool removeColumn(GridColumn Column)
		{
			//If Column is Null or Collection does not contain column
			if (Column == null || !this.Columns.Contains(Column)) return false;
			
			//Remove Column
			this.Columns.Remove(Column);

			//Determine Number Of Columns
			this.NumCols = this.Columns.Sum(col => Math.Max(col.ColumnSpan, 1));

			return true;
		}
		public bool removeColumn(string sColID)
		{
			return removeColumn(getCol(sColID));
		}




		public string getFilter(string sColID)
		{
			return this.Views[0].getFilter(sColID);
		}
		public void setFilter(string sColID, string sValue)
		{
			if ((DisableInitialFiltering || DisableInitialSorting || DisableInitialView) && !m_bInitialized) throw new Exception("Can't set Filter/sort until after Control_Init when DisableInitialFiltering=true or DisableInitialSorting=true or DisableInitialView=true.");
			this.Views[0].setFilter(sColID, sValue);
		}

		



		void Control_Load(object sender, EventArgs e)
		{
			if (!m_bInitialized) Control_InitComplete(sender, e);
			OnGridLoad(new GridCellEventArgs(this, null));
			if (!String.IsNullOrWhiteSpace(m_sParentGridID))
			{
				List<Control> oGrid = jlib.helpers.control.findControlById(Page, Page.Controls, m_sParentGridID);
				if (oGrid.Count > 0 && oGrid[0] as jGrid != null)
					(oGrid[0] as jGrid).addLinkedGrid(this);
			}
		}


		/* METHOD SECTION */


		protected override void OnInit(EventArgs e)
		{
			this.MetaData.Grid = this;
			this.Load += new EventHandler(Control_Load);
			Page.InitComplete += new EventHandler(Control_InitComplete);
			base.OnInit(e);
		}

		void Control_InitComplete(object sender, EventArgs e)
		{
			m_bInitialized = true;
			jlib.components.webpage oPage = (Page as jlib.components.webpage);
			if (!oPage.IsPostBackA && GridViewsLoad != null)
			{
				m_oDefaultView = this.Views.SelectedView;
				GridViewsLoad(this);
				if (DisableInitialView)
				{
                    this.Views[0] = m_oDefaultView;
					this.Views.SelectedView = m_oDefaultView;
				}
				else
				{
					if (DisableInitialFiltering) this.Views[0].resetFilter();
					if (DisableInitialSorting)
					{
						this.SortOrder = (m_oDefaultView.SortOrder == 0 ? "asc" : (m_oDefaultView.SortOrder == 1 ? "desc" : ""));
						this.SortCol = m_oDefaultView.SortCol;
					}
				}
			}
			bool bEmbeddedGridMatch = convert.cJoin("_", parse.split(oPage.EventTarget, "_"), 3, -1) == this.UniqueID || convert.cJoin("_", parse.split(oPage.EventTarget, "_"), 3, -1) == this.ClientID;
			if (oPage.EventTarget == this.UniqueID || oPage.EventTarget == this.ClientID || bEmbeddedGridMatch)
			{
				System.Web.HttpContext.Current.Response.Cache.SetNoStore();
				m_bGridPostback = true;
				oPage.Response.Clear();
				oPage.Response.ContentType = "text/javascript";
				//jlib.functions.json.JsonData oData = jlib.functions.json.JsonMapper.ToObject(convert.cStreamToString(oPage.Request.InputStream, Encoding.UTF8));                
				jlib.functions.json.JsonData oData = jlib.functions.json.JsonMapper.ToObject(oPage.Request.Form["_request"]);

				m_oRequestMetaData = jlib.functions.json.JsonMapper.ToObject<GridMetaData>(oData["MetaData"].ToJson());
				if (!String.IsNullOrWhiteSpace(m_oRequestMetaData.ClientID))
				{
					m_oRequestMetaData.Grid = this;
					m_oRequestMetaData.StatusMessage = "";
					this.MetaData = m_oRequestMetaData;
					this.MetaData.DisplayAdd = -1;
					this.MetaData.DisplayFilter = -1;
					if (oData["Views"].Count > 0)
					{
						this.Views.parse(oData["Views"], true);
					}
					else
					{
						//make sure we render Views to the client
						m_bGridPostback = false;
					}
				}
				if (bEmbeddedGridMatch)
				{
					SessionData = parse.splitValue(oPage.EventTarget, "_", 2);
					this.MetaData.IsEmbedded = 1;
				}

				ArrayList oActions = new ArrayList();
				switch (convert.cStr(oData["Action"]))
				{
					case "multi":
						for (int x = 0; x < oData["Requests"].Count; x++)
							oActions.Add(oData["Requests"][x]);

						break;
					default:
						oActions.Add(oData);
						break;
				}

				bool? bRefreshData = null;
				bool bRefreshIDs = false, bRefreshColData = false;
				string sStatusMessage = "", sClientJSBefore = "", sClientJSAfter = "";
				ArrayList oStatus = new ArrayList();
				for (int x = 0; x < oActions.Count; x++)
				{
					jlib.functions.json.JsonData oAction = oActions[x] as jlib.functions.json.JsonData;
					GridCellEventArgs oEvent = new GridCellEventArgs(this, null, convert.cArrayToString((IList)oAction["values"]), convert.cInt(oAction["pk"]));

					oEvent.JSONObject = oAction;
					this.OnGridCallback(oEvent);

					switch (convert.cStr(oAction["Action"]))
					{
						case "refresh":
							bRefreshData = true;
							bRefreshColData = convert.cBool(oAction["RefreshColData"]) || oEvent.RefreshColData;
							break;
						case "delete":
							this.OnGridRowClientDel(oEvent);
							bRefreshData = true;
							break;
						case "add":
							this.OnGridRowClientAdd(oEvent);
							bRefreshData = true;
							break;
						case "update":
							this.OnGridRowClientUpdate(oEvent);
							bRefreshData = true;
							break;
						case "getselection":
							bRefreshIDs = true;
							break;
						case "custom":
							oEvent.Type = convert.cStr(oAction["Type"]);
							oEvent.Argument = convert.cStr(oAction["Argument"]);
							oEvent.JSONData = oAction["Data"];
							string sKey = oEvent.Type + "|" + oEvent.Argument + "|" + (oEvent.JSONData == null ? "" : oEvent.JSONData.ToString());
							//Detect if duplicate request is submitted
							if (convert.cStr(Page.Session["jgrid.request"]) == sKey && ((DateTime)Page.Session["jgrid.request.date"]) > DateTime.Now.AddSeconds(-3)) Page.Response.End();
							this.OnGridCustomData(oEvent);
							if (oEvent.RefreshData) bRefreshData = true;
							Page.Session["jgrid.request"] = sKey;
							Page.Session["jgrid.request.date"] = DateTime.Now;
							break;
					}

					oStatus.Add(convert.cArrayList((String.IsNullOrWhiteSpace(convert.cStr(oAction["Index"])) ? "-1" : convert.cStr(oAction["Index"])), oEvent.Return ? "0" : "1", oEvent.ReturnValue, oEvent.Argument));
					if (!String.IsNullOrWhiteSpace(oEvent.ReturnValue)) sStatusMessage += oEvent.ReturnValue + "\n";
					if (oEvent.RefreshColData) bRefreshColData = true;
					if (!String.IsNullOrWhiteSpace(oEvent.ClientJSBefore)) sClientJSBefore += oEvent.ClientJSBefore;
					if (!String.IsNullOrWhiteSpace(oEvent.ClientJSAfter)) sClientJSAfter += oEvent.ClientJSAfter;
				}

				if (convert.cBool(bRefreshData) || bRefreshIDs || !String.IsNullOrWhiteSpace(sClientJSBefore) || !String.IsNullOrWhiteSpace(sClientJSAfter) || !String.IsNullOrWhiteSpace(convert.cStr(sStatusMessage)))
				{
					this.MetaData.StatusMessage = sStatusMessage;
					try
					{
						oPage.Response.ContentType = "text/javascript";
                        GridCellEventArgs oEvent = new GridCellEventArgs(this, null);
                        string sData = renderData(bRefreshColData, convert.cBool(bRefreshData), true, bRefreshIDs, oStatus, oEvent);
						oPage.Response.Write(sClientJSBefore + oEvent.ClientJSBefore + "$('#" + (bEmbeddedGridMatch ? oPage.EventTarget : this.ClientID) + "')[0].JGrid.callback(''," + sData + ");" + sClientJSAfter + oEvent.ClientJSAfter);
					}
					catch (Exception ex)
					{
						if (convert.cBool(ConfigurationManager.AppSettings["site.dev"]))
						{
							oPage.Response.Write("alert('" + parse.replaceAll(ex.Message + "\n\n" + ex.StackTrace, "\\", "\\\\", "'", "\\'", "\n", "\\n", "\r", "") + "');");
						}
					}
				}
				oPage.Response.End();
			}
			if (Page.IsPostBack)
			{
				if (!String.IsNullOrWhiteSpace(convert.cStr(Page.Request[this.ClientID + "_MetaData"])))
				{
					this.MetaData = jlib.functions.json.JsonMapper.ToObject<GridMetaData>(Page.Request[this.ClientID + "_MetaData"]); ;
					this.MetaData.Grid = this;
				}
				if (!String.IsNullOrWhiteSpace(convert.cStr(Page.Request[this.ClientID + "_MetaData"]))) this.Views.parse(jlib.functions.json.JsonMapper.ToObject(Page.Request[this.ClientID + "_Views"]), true);
			}

			this.MetaData.StatusMessage = "";
		}
		

		
		public GridColumn getCol(int Index)
		{
			if (Index < 0 || Index >= this.Columns.Count) return null;
			return this.Columns[Index];
		}
		public GridColumn getCol(string ColumnID)
		{
			return this.Columns.Where(col => col.ID.Equals(ColumnID, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
		}
        public string renderData(bool bRenderCols, bool bRenderData, bool bRenderMetaData, bool bRenderIDList, ArrayList oRequestStatus) {
            GridCellEventArgs oEvent = new GridCellEventArgs(this, null);
            return renderData(bRenderCols, bRenderData, bRenderMetaData, bRenderIDList, oRequestStatus, oEvent);
        }
		public string renderData(bool bRenderCols, bool bRenderData, bool bRenderMetaData, bool bRenderIDList, ArrayList oRequestStatus, GridCellEventArgs oEvent)
		{

			if (bRenderIDList || bRenderData)
			{
				if (!bOnGridBeforeDatabindCalled)
				{
                    this.OnGridBeforeDatabind(oEvent);
                    if (!bRenderCols && oEvent.RefreshColData) bRenderCols = true;
					bOnGridBeforeDatabindCalled = true;
                    if (oRequestStatus != null && oRequestStatus.Count > 0) {
                        //Preserve values from oRequestStatus, if these have already been set
                        if ((oRequestStatus[0] as ArrayList)[1] != null) oEvent.Return = convert.cStr((oRequestStatus[0] as ArrayList)[1]) == "0";
                        if ((oRequestStatus[0] as ArrayList)[2] != null) oEvent.ReturnValue = convert.cStr((oRequestStatus[0] as ArrayList)[2], oEvent.ReturnValue);
                        if ((oRequestStatus[0] as ArrayList)[3] != null) oEvent.Argument = convert.cStr((oRequestStatus[0] as ArrayList)[3], oEvent.Argument);
                    }
                    if (!oEvent.Return || !String.IsNullOrWhiteSpace(oEvent.ReturnValue))
					{
						if (oRequestStatus == null) oRequestStatus = new ArrayList();
                        if (oRequestStatus.Count == 0) oRequestStatus.Add(new ArrayList() { null, null, null, null });
                        
						(oRequestStatus[0] as ArrayList)[1] = (oEvent.Return ? 0 : 1);
                        (oRequestStatus[0] as ArrayList)[2] = oEvent.ReturnValue;
                        (oRequestStatus[0] as ArrayList)[3] = oEvent.Argument;
                        if (!String.IsNullOrWhiteSpace(oEvent.ReturnValue) && !this.MetaData.StatusMessage.Str().Contains(oEvent.ReturnValue)) this.MetaData.StatusMessage += (String.IsNullOrWhiteSpace(this.MetaData.StatusMessage) ? "" : "\n") + oEvent.ReturnValue;
					}
				}
			}

			jlib.functions.json.JsonMapper.RegisterExporter<helpers.structures.collection_aux>(new jlib.functions.json.ExporterFunc<helpers.structures.collection_aux>(helpers.structures.collection.JSonSerialize));

			StringBuilder oSB = new StringBuilder("{");

			if (bRenderData)
			{//render data?								

				if (this.DataSource != null)
				{
					oSB.Append("Data:[");
					int iStart = 0;
					int iEnd = this.GetDataSourceRowCount();
					if (iEnd == this.MetaData.NumRows && Pagination && DataViewFiltering && this.MetaData.Pages > 1)
					{
						iStart = this.MetaData.Page * this.PageSize;
						iEnd = (this.MetaData.Page + 1) * this.PageSize;
					}
					for (; iStart < this.GetDataSourceRowCount() && iStart < iEnd; iStart++)
					{
						string[] sValues = new string[this.Columns.Count];
						for (int y = 0; y < this.Columns.Count; y++)
						{
							if (this.Columns[y].Visible)
							{
								sValues[y] = "";
								if (!String.IsNullOrWhiteSpace(this.Columns[y].FormatString))
								{
									if (this.Columns[y].FormatString.IndexOf("#") == -1 && this.Columns[y].FormatString.IndexOf("{") == -1)
									{
										if (!this.DoesDataSourceColumnExist(this.Columns[y].FormatString))
											sValues[y] = this.Columns[y].FormatString;
										else
											sValues[y] = convert.cStr(this.GetDataSourceRow(iStart)[this.Columns[y].FormatString]);
									}
									else
									{

										sValues[y] = helpers.general.formatString(this.Columns[y].FormatString, this.Columns[y].FormatStringArguments, this.GetDataSourceRow(iStart));
									}
								}

								if (sValues[y].IndexOf("#click#") > -1)
								{
									if (sValues[y].IndexOf("'#click#'") > -1) sValues[y] = parse.replaceAll(sValues[y], "'#click#'", "\"#click#\"");
									sValues[y] = parse.replaceAll(sValues[y], "#click#",
										Page.ClientScript.GetPostBackClientHyperlink(this, "GridRowClick_" + this.GetDataSourceRow(iStart)[this.PK]));
								}

								if (sValues[y].IndexOf("#edit#") > -1)
								{
									if (sValues[y].IndexOf("'#edit#'") > -1) sValues[y] = parse.replaceAll(sValues[y], "'#edit#'", "\"#edit#\"");
									sValues[y] = parse.replaceAll(sValues[y], "#edit#",
										Page.ClientScript.GetPostBackClientHyperlink(this, "GridRowEdit_" + this.GetDataSourceRow(iStart)[this.PK]));
								}

								if (sValues[y].IndexOf("#del#") > -1)
								{
									if (sValues[y].IndexOf("'#del#'") > -1) sValues[y] = parse.replaceAll(sValues[y], "'#del#'", "\"#del#\"");
									sValues[y] = parse.replaceAll(sValues[y], "#del#",
										Page.ClientScript.GetPostBackClientHyperlink(this, "GridRowDel_" + this.GetDataSourceRow(iStart)[this.PK]));
								}

								if (sValues[y].IndexOf("#del_js#") > -1)
								{
									if (sValues[y].IndexOf("'#del_js#'") > -1) sValues[y] = parse.replaceAll(sValues[y], "'#del_js#'", "\"#del_js#\"");
									sValues[y] = parse.replaceAll(sValues[y], "#del_js#",
										parse.replaceAll(Page.ClientScript.GetPostBackClientHyperlink(this, "GridRowDel_" + this.GetDataSourceRow(iStart)[this.PK]), "javascript:", "javascript: if( window.confirm( '" + (System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"] == "nb-NO" ? "Er du sikker på at du vil slette?" : "Are you sure you want to delete this row?") + "' )){ ") + "}");
								}
								if (this.Columns[y].ConvertNewlineToBr) sValues[y] = parse.replaceAll(sValues[y], "\n", "<br>");
							}
						}

						GridCellEventArgs oArgs = null;
						if (GridRowPrerender != null)
						{
							oArgs = new GridCellEventArgs(this, null, sValues, (this.DoesDataSourceColumnExist(this.Columns[this.MetaData.PKCol].SortField) ? convert.cInt(this.GetDataSourceRow(iStart)[this.Columns[this.MetaData.PKCol].SortField]) : 0), this.GetDataSourceRow(iStart), iStart);
							GridRowPrerender(this, oArgs);
							sValues = oArgs.getTextValues();
                            if (!oArgs.ClientJSBefore.IsNullOrEmpty()) oEvent.ClientJSBefore = parse.replaceAll(oEvent.ClientJSBefore, oArgs.ClientJSBefore, "") + oArgs.ClientJSBefore;
                            if (!oArgs.ClientJSAfter.IsNullOrEmpty()) oEvent.ClientJSAfter = parse.replaceAll(oEvent.ClientJSAfter, oArgs.ClientJSAfter, "") + oArgs.ClientJSAfter;
						}
						if (oArgs == null || oArgs.Return)
						{
							StringBuilder oSB1 = new StringBuilder("[", 1000);
							for (int y = 0; y < sValues.Length; y++)
							{
								oSB1.Append(jlib.functions.JSONConverter.cJSONSafeValue(sValues[y], typeof(string), true));
								if (y < sValues.Length - 1) oSB1.Append(",");
							}
							oSB1.Append("],");
							string sRow = oSB1.ToString();
							if (GridRowPostrender != null)
							{
								oArgs = new GridCellEventArgs(this, new string[] { oSB1.ToString() }, null, (this.DoesDataSourceColumnExist(this.Columns[this.MetaData.PKCol].SortField) ? convert.cInt(this.GetDataSourceRow(iStart)[this.Columns[this.MetaData.PKCol].SortField]) : 0), this.GetDataSourceRow(iStart), iStart);
								GridRowPostrender(this, oArgs);
								sRow = oArgs.getColDataValue(this.Columns[0]);
                                if (!oArgs.ClientJSBefore.IsNullOrEmpty()) oEvent.ClientJSBefore = parse.replaceAll(oEvent.ClientJSBefore, oArgs.ClientJSBefore, "") + oArgs.ClientJSBefore;
                                if (!oArgs.ClientJSAfter.IsNullOrEmpty()) oEvent.ClientJSAfter = parse.replaceAll(oEvent.ClientJSAfter, oArgs.ClientJSAfter, "") + oArgs.ClientJSAfter;
							}
							oSB.Append(sRow);
						}
					}
					if (oSB.ToString().EndsWith(",")) oSB.Length--;
					oSB.Append("],");
				}
			}

			
			if (bRenderIDList)
			{
				#warning ? Not sure if this is used anymore
				//if (m_sFields != "")
				//    oSB.Append("SelectionList:[" + convert.cJoin(sqlbuilder.getDataTable(this.DataHelper, "select " + this.Columns[this.MetaData.PKCol].SortFieldPrefixed + " from " + m_sTables + " where " + m_sFilter), this.Columns[this.MetaData.PKCol].SortField, ",") + "],");
				throw new NotImplementedException("bRenderIDList is currently not implemented");

			}

			if (bRenderMetaData)
			{//render aux?
				this.MetaData.ColIDs = new string[this.Columns.Count];
				for (int x = 0; x < this.MetaData.ColIDs.Length; x++) this.MetaData.ColIDs[x] = this.Columns[x].ID;
				oSB.Append("MetaData:" + jlib.functions.json.JsonMapper.ToJson(this.MetaData) ?? "");
				oSB.Append(",");
			}
			if (oRequestStatus != null)
			{
				oSB.Append("RequestStatus:" + jlib.functions.json.JsonMapper.ToJson(oRequestStatus) ?? "");
				oSB.Append(",");
			}

			if (bRenderCols)
			{//render cols?
				oSB.Append("Cols:[");
				for (int x = 0; x < this.Columns.Count; x++)
				{
					oSB.Append(parse.replaceAll(jlib.functions.json.JsonMapper.ToJson(this.Columns[x].ColData) ?? "", "\\\\'", "\\\"") + ",");
				}
				if (oSB.ToString().EndsWith(",")) oSB.Length--;
				oSB.Append("],");
			}
			if (bRenderCols || !m_bGridPostback || (m_oRequestMetaData != null && String.IsNullOrWhiteSpace(m_oRequestMetaData.ClientID)))
			{
				if (m_oDefaultView != null)
				{
                    m_oDefaultView.Name = GridViews.DEFAULT_VIEW_NAME;
					m_oDefaultView.ReadOnly = true;

					bool bFound = false;
					for (int x = 1; x < Views.Count; x++)
					{
                        if (Views[x].Name == GridViews.DEFAULT_VIEW_NAME)
						{
							Views[x] = m_oDefaultView;
							bFound = true;
							break;
						}
					}
					if (!bFound) this.Views.Insert((Views.Count == 0 ? 0 : 1), m_oDefaultView);
				}
				oSB.Append("Views:[" + Views.toJSON() + "],");
			}

			if (oSB.ToString().EndsWith(",")) oSB.Length--;
			oSB.Append("}");
			if (GridViewsSave != null) this.GridViewsSave(this);
			return oSB.ToString();
		}

		public Object Clone()
		{

			jGrid oNewGrid = jlib.helpers.control.cloneObject(this, new string[] { "Columns" }, true) as jGrid;

            oNewGrid.Columns = new List<GridColumn>();
			oNewGrid.Cols = jlib.helpers.control.cloneObject(this.Cols) as PlaceHolder;
			for (int y = 0; y < oNewGrid.Columns.Count; y++) oNewGrid.Columns[y].SortField = this.Columns[y].SortFieldPrefixed; //new col.ColDataSettings(oNewGrid.Columns[y]);
            oNewGrid.Views = new GridViews(oNewGrid);
			oNewGrid.Views.parse(this.Views.serialize(), true);

			return oNewGrid;
		}

		

		



		#region ***** RENDER METHODS *****
		/// <summary>
		/// Overloaded Render Method
		/// </summary>
		/// <param name="writer"></param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (String.IsNullOrWhiteSpace(JSName)) JSName = this.ClientID;
			if (TAtt.ToLower().IndexOf(" class=") == -1) TAtt += " class='jgrid'";
			else if (TAtt.ToLower().IndexOf(" class='") > -1) TAtt = parse.replaceAll(TAtt, " class='", " class='jgrid ");
			else TAtt = parse.replaceAll(TAtt, " class=\"", " class=\"jgrid ");

			string sHTML = "<table id='" + this.ClientID + "' " + TAtt + "></table>";
			if (!String.IsNullOrWhiteSpace(m_sParentGridID))
			{
				writer.Write("<input type='hidden' id='" + this.ClientID + "' value=\"" + parse.replaceAll(sHTML, "\"", "\\'") + "\">");
			}
			else
			{
				writer.Write(sHTML + "<script>" + "var " + JSName + "=new clsJGrid('" + this.ClientID + "');" + this.CustomJS + "$(document).ready(function(){" + JSName + ".callback(''," + renderData(true, true, true, false, null) + ");});" + (String.IsNullOrWhiteSpace(LoadViewContainer) ? "" : "$('#" + LoadViewContainer + "').html('Load View: <select id=\"" + this.ClientID + "_views\"><option value=\"\"></option><option value=\"1\">(Default)</option></select> <button type=\"button\" onclick=\"" + JSName + ".loadView($(this).siblings(\\'select\\').val());\">Go</button> <button type=\"button\" onclick=\"" + JSName + ".configureView($(this).siblings(\\'select\\').val());\">Configure</button>');") + "</script>");
			}
		}

		/// <summary>
		/// Outputs the grid data in HTML Form
		/// </summary>
		/// <param name="RowCounter"></param>
		/// <returns></returns>
		public string SaveAsHTML(ref int RowCounter)
		{
			StringBuilder oSB = new StringBuilder();

			int iT1 = this.CurrentPage; int iT2 = this.PageSize;
			this.CurrentPage = 0; this.PageSize = 999999999;

			GridCellEventArgs e = new GridCellEventArgs(this, null);
			this.OnGridBeforeDatabind(e);
			bOnGridBeforeDatabindCalled = false;

			string sData = "";
			for (int y = 0; y < this.Columns.Count; y++)
			{
				if (this.Columns[y].ColType != "Hide" && this.Columns[y].ColType != "Options" && this.Columns[y].Visible)
				{
					e = new GridCellEventArgs(this, "<th " + this.Columns[y].ColData.HAtt + ">", "</th>", "<a href='javascript:void(0);'>" + this.Columns[y].HText + "</a>", -3000, null, -3000, this.Columns[y]);
					this.OnGridCellPrintPrerender(e);
					sData += e.HTMLTagStart + e.HTMLValue + e.HTMLTagEnd;
				}
			}

			e = new GridCellEventArgs(this, "<tr>", "</tr>", sData, -3000, null, -3000, null);
			this.OnGridRowPrintPrerender(e);
			if (e.Return)
				oSB.Append("<thead>" + e.HTMLTagStart + e.HTMLValue + e.HTMLTagEnd + "</thead>");

			for (int x = 0; x < this.GetDataSourceRowCount(); x++)
			{
				sData = "";
				for (int y = 0; y < this.Columns.Count; y++)
				{
					string sCellValue = "";
					if (this.Columns[y].ColType != "Hide" && this.Columns[y].ColType != "Options" && this.Columns[y].Visible)
					{
						if (!String.IsNullOrWhiteSpace(this.Columns[y].FormatString))
						{
							if (this.Columns[y].FormatString.IndexOf("#") == -1 && this.Columns[y].FormatString.IndexOf("{") == -1)
							{
								if (this.DoesDataSourceColumnExist(this.Columns[y].FormatString))
									sCellValue = this.Columns[y].FormatString;
								else
									sCellValue = convert.cStr(this.GetDataSourceRow(x)[this.Columns[y].FormatString]);
							}
							else
							{
								sCellValue = helpers.general.formatString(this.Columns[y].FormatString, this.Columns[y].FormatStringArguments, this.GetDataSourceRow(x));
							}
						}
						e = new GridCellEventArgs(this, "<td " + this.Columns[y].ColData.CAtt + ">", "</td>", sCellValue, convert.cInt(this.GetDataSourceRow(x)[this.Columns[this.MetaData.PKCol].SortField]), this.GetDataSourceRow(x), x, this.Columns[y]);
						this.OnGridCellPrintPrerender(e);
						sData += e.HTMLTagStart + e.HTMLValue + e.HTMLTagEnd;
					}
				}
				e = new GridCellEventArgs(this, "<tr class='" + (x % 2 == 0 ? "even" : "odd") + " " + this.MetaData.CRowClass + "'>", "</tr>", sData, convert.cInt(this.GetDataSourceRow(x)[this.Columns[this.MetaData.PKCol].SortField]), this.GetDataSourceRow(x), x, null);
				this.OnGridRowPrintPrerender(e);
				if (e.Return)
					oSB.Append("<thead>" + e.HTMLTagStart + e.HTMLValue + e.HTMLTagEnd + "</thead>");
			}

			e = new GridCellEventArgs(this, "<table id='" + this.ClientID + "' " + TAtt + ">", "</table>", oSB.ToString(), 0, null, 0, null);
			this.OnGridPrintPrerender(e);

			this.DataSource = null;
			this.CurrentPage = iT1; this.PageSize = iT2;

			if (e.Return)
				return e.HTMLTagStart + e.HTMLValue + e.HTMLTagEnd;

			return "";
		}

		/// <summary>
		/// Outputs the grid data in Excel
		/// </summary>
		/// <param name="TemplateName"></param>
		/// <param name="WorkbookFile"></param>
		/// <param name="FileName"></param>
		/// <param name="SheetName"></param>
		/// <param name="SheetNumber"></param>
		/// <param name="RowCounter"></param>
		/// <param name="Title"></param>
		/// <returns></returns>
		public IWorkbook SaveAsExcel8(string TemplateName, IWorkbook WorkbookFile, string FileName, string SheetName, int SheetNumber, ref int RowCounter, string Title)
		{
			return this.SaveAsExcel(FileFormat.Excel8, TemplateName, WorkbookFile, FileName, SheetName, SheetNumber, ref RowCounter, Title);
		}

		/// <summary>
		/// Outputs the grid data in Excel
		/// </summary>
		/// <param name="TemplateName"></param>
		/// <param name="WorkbookFile"></param>
		/// <param name="FileName"></param>
		/// <param name="SheetName"></param>
		/// <param name="SheetNumber"></param>
		/// <param name="RowCounter"></param>
		/// <param name="Title"></param>
		/// <returns></returns>
		public IWorkbook SaveAsExcel(string TemplateName, IWorkbook WorkbookFile, string FileName, string SheetName, int SheetNumber, ref int RowCounter, string Title)
		{
			return this.SaveAsExcel(FileFormat.OpenXMLWorkbook, TemplateName, WorkbookFile, FileName, SheetName, SheetNumber, ref RowCounter, Title);
		}

		/// <summary>
		/// Outputs the grid data in Excel
		/// </summary>
		/// <param name="TemplateName"></param>
		/// <param name="WorkbookFile"></param>
		/// <param name="FileName"></param>
		/// <param name="SheetName"></param>
		/// <param name="SheetNumber"></param>
		/// <param name="RowCounter"></param>
		/// <param name="Title"></param>
		/// <returns></returns>
		public IWorkbook SaveAsExcel(SpreadsheetGear.FileFormat ExcelFileFormat, string TemplateName, IWorkbook WorkbookFile, string FileName, string SheetName, int SheetNumber, ref int RowCounter, string Title)
		{

			if (WorkbookFile == null)
			{
				if (!String.IsNullOrWhiteSpace(convert.cStr(TemplateName))) WorkbookFile = SpreadsheetGear.Factory.GetWorkbookSet().Workbooks.Open(System.Web.HttpContext.Current.Server.MapPath(TemplateName));
				else WorkbookFile = SpreadsheetGear.Factory.GetWorkbook();
			}
			if (WorkbookFile.Worksheets.Count <= SheetNumber)
			{
				WorkbookFile.Worksheets.Add();
				WorkbookFile.Worksheets[WorkbookFile.Worksheets.Count - 1].Name = convert.cEllipsis(parse.removeChars(convert.cStr(SheetName, "Sheet " + WorkbookFile.Worksheets.Count), "[]*/:?\\"), 31, false, false);
				SheetNumber = WorkbookFile.Worksheets.Count - 1;
			}
			else
			{
				if (!String.IsNullOrWhiteSpace(convert.cStr(SheetName))) WorkbookFile.Worksheets[SheetNumber].Name = convert.cEllipsis(parse.removeChars(SheetName, "[]*/:?\\"), 31, false, false);
			}
			var oValues = (SpreadsheetGear.Advanced.Cells.IValues)WorkbookFile.Worksheets[SheetNumber];

			if (!String.IsNullOrWhiteSpace(Title))
			{
				WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, 0].Font.Size = 18;
				WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, 0].Font.Bold = true;
				WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, 0].Value = Title;
				RowCounter = RowCounter + 2;
			}

			int iT1 = this.CurrentPage; int iT2 = this.PageSize;
			this.CurrentPage = 0; this.PageSize = 65535 - RowCounter - 1;

			GridCellEventArgs e = new GridCellEventArgs(this, null);
			this.OnGridBeforeDatabind(e);
			bOnGridBeforeDatabindCalled = false;

			int[] iColOrder = this.Views[0].ColOrder;
			bool[] bVisibleCols = new bool[this.Columns.Count];
			int iCounter = 0;
			for (int y = 0; y < iColOrder.Length; y++)
			{
				bVisibleCols[iColOrder[y]] = this.Columns[iColOrder[y]].Grouping.ToLower() != "hide" && this.Columns[iColOrder[y]].Grouping != "" && !",hide,select,options,".Contains("," + this.Columns[iColOrder[y]].ColType.ToLower() + ",") && this.Columns[iColOrder[y]].Visible;
				if (bVisibleCols[iColOrder[y]])
				{
					WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, iCounter].Font.Size = 12;
					WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, iCounter].Interior.Color = System.Drawing.Color.FromArgb(91, 0, 0);
					WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, iCounter].Font.Color = System.Drawing.Color.White;
					WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, iCounter].Font.Bold = true;
					oValues.SetText(RowCounter, iCounter, (",,hide,select,text,noedit,options,".Contains(this.Columns[iColOrder[y]].Grouping.ToLower()) ? "" : this.Columns[iColOrder[y]].Grouping + "(") + this.Columns[iColOrder[y]].HText + (",,hide,select,text,noedit,options,".Contains(this.Columns[iColOrder[y]].Grouping.ToLower()) ? "" : ")"));
					iCounter++;
				}
			}
			RowCounter++;
			for (int x = 0; x < this.GetDataSourceRowCount(); x++)
			{
				string[] sValues = new string[this.Columns.Count];
				for (int y = 0; y < iColOrder.Length; y++)
				{
					if (bVisibleCols[iColOrder[y]])
					{
						sValues[iColOrder[y]] = "";
						if (!String.IsNullOrWhiteSpace(this.Columns[iColOrder[y]].FormatString))
						{
							if (this.Columns[iColOrder[y]].FormatString.IndexOf("#") == -1 && this.Columns[iColOrder[y]].FormatString.IndexOf("{") == -1)
							{
								if (this.DoesDataSourceColumnExist(this.Columns[iColOrder[y]].FormatString))
                                    sValues[iColOrder[y]] = convert.cStr(this.GetDataSourceRow(x)[this.Columns[iColOrder[y]].FormatString]);
								else
									sValues[iColOrder[y]] = this.Columns[iColOrder[y]].FormatString;
							}
							else
							{

								sValues[iColOrder[y]] = helpers.general.formatString(this.Columns[iColOrder[y]].FormatString, this.Columns[iColOrder[y]].FormatStringArguments, this.GetDataSourceRow(x));
							}
						}
					}
				}
				GridCellEventArgs oArgs = null;
				if (GridRowPrerender != null)
				{
					oArgs = new GridCellEventArgs(this, null, sValues, convert.cInt(this.GetDataSourceRow(x)[this.Columns[this.MetaData.PKCol].SortField]), this.GetDataSourceRow(x), x, WorkbookFile, SheetNumber, RowCounter);
					GridRowPrerender(this, oArgs);
					sValues = oArgs.getTextValues();
					RowCounter = oArgs.ExcelRowCounter;
				}
				if (oArgs == null || oArgs.Return)
				{
					iCounter = 0;
					for (int y = 0; y < iColOrder.Length; y++)
					{
						if (bVisibleCols[iColOrder[y]])
						{
							if (this.Columns[iColOrder[y]].ColType == "JCombo" && sValues[iColOrder[y]].Contains("\n")) sValues[iColOrder[y]] = sValues[iColOrder[y]].Substring(sValues[iColOrder[y]].IndexOf("\n") + 1);
							string sValue = parse.stripHTML(sValues[iColOrder[y]], true);
							if (convert.isNumeric(sValue) && (this.Columns[iColOrder[y]].ExcelColType == "auto" || this.Columns[iColOrder[y]].ExcelColType == "number"))
								oValues.SetNumber(RowCounter, iCounter, convert.cDbl(sValue));
							else
								oValues.SetText(RowCounter, iCounter, sValue);
							iCounter++;
						}
					}
					if (GridRowPostrender != null)
					{
						oArgs = new GridCellEventArgs(this, null, sValues, convert.cInt(this.GetDataSourceRow(x)[this.Columns[this.MetaData.PKCol].SortField]), this.GetDataSourceRow(x), x, WorkbookFile, SheetNumber, RowCounter);
						GridRowPostrender(this, oArgs);
					}
					RowCounter++;
				}
			}
			this.DataSource = null;
			this.CurrentPage = iT1; this.PageSize = iT2;
			WorkbookFile.Worksheets[SheetNumber].UsedRange.Columns.AutoFit();

			if (!String.IsNullOrWhiteSpace(convert.cStr(FileName))) WorkbookFile.SaveAs(FileName, ExcelFileFormat);
			return WorkbookFile;
		}

		#endregion

	}

}