﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace jlib.controls.jgrid
{
	public class GridMetaData
	{
		#region ***** CONSTRUCTORS *****
		
		public GridMetaData()
		{
            this.SessionData = "";
            this.CRowClass = "";
            this.OnRowDoubleClick = "";
            this.StatusMessage = "";
            this.LinkedGridIDs = "";
            this.ID = "";
            this.ClientID = "";                
        }
        #endregion

		#region ***** PROPERTIES *****
		
		[XmlIgnore]
		public jGrid Grid
		{
			get
			{
				return this._Grid;
			}
			set 
			{
				this._Grid = value;
				this.ID = value.UniqueID;
				this.ClientID = value.ClientID;
			}
		}
		private jGrid _Grid = null;

		public string ID { get; set; }
        public string ClientID { get; set; }
		public int Page {get; set;}
        public int NumRows {get; set;}
        public int IsEmbedded { get; set; }
        public int PKCol { get; set; }
        public int AutoFilter { get; set; }
        public int DisplayFilter { get; set; }
        public int DisplayAdd { get;  set; }
        public string CRowClass {get; set;}
        public string SessionData { get; set; }
        public string OnRowDoubleClick { get; set; }            
        public string StatusMessage {get; set;}
        public string LinkedGridIDs {get; set;}


		public int Pages 
		{
			get 
			{
				if (this.Grid.PageSize > 0) return (NumRows / this.Grid.PageSize) + (NumRows % this.Grid.PageSize > 0 ? 1 : 0);
				return 0;
			}
		}

		#endregion


        


        
        public string[] ColIDs = new string[0];
        public string NoRecordsMessage = "No records found that match your request";
        public string DeleteConfirmation = "Are you sure you wish to delete this row?";
      
		
    }
}
