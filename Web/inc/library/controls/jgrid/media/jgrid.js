var c_Add=-1000;
var c_Filter=-2000;
var c_Header=-3000;

function clsJGrid(sID){    
	this.ID 	= sID;
    if($("#"+sID).length>0){
        this.Table	= $("#"+sID)[0];
        this.Table.JGrid = this;
    }
	this.Data={};
	this.Cols={};
	this.Views={};
	this.MetaData={};
	this.PersistedData={Requests:[],Selections:[],EditingArr:{},LinkedGrid:[],SelectionMode:0,SelectionCount:0};		
	this.PersistedData.EditingArr[c_Add]=[];	
	this.DeletedArr=[];
	this.Request=null;	
	this.MaxRequests=5;
	this.EventHandler=[];
	this.EventType=[];
	this.IsAjaxActive=false;
		
	this.callback	= fnJGridCallback;
	this.render = fnJGridRender;
	this.renderCell = fnJGridRenderCell;
	this.refresh = function () { this.addRequest(this.buildRequest("refresh")); };
	this.collectRow = fnJGridCollectEditingRow;	
	this.registerEventListener  = fnRegisterEventListener;
	this.raiseEvent  = fnRaiseEvent;
	this.refreshSelect = fnJGridRefreshSelect;
	this.processQueue   = fnJGridProcessQueue;
	this.addRequest = fnJGridAddRequest;
	this.buildRequest = fnJGridBuildRequest;
	this.savePersistData = fnJGridSavePersistData;
	this.saveEditingRows=fnJGridRowSave;
	this.configureView = fnJGridConfigureView;
	this.loadView = fnJGridLoadView;

	this.setFilter  = fnJGridSetFilter;
	this.setSelection   = fnJGridSetSelection;
	this.getSelection = fnJGridGetSelection;
	this.getCol = function (sID) { return this._Cols[sID]; };
	this.getData = function (iIndex, sCol) { if (isNaN(sCol)) { sCol=this.getCol(sCol);if(sCol==null)return null; sCol=sCol.Index;} if (this.Data[iIndex] == null) return null; return this.Data[iIndex][sCol]; };
	this.getPKIndex = function(sID){sID=""+sID;for(var x=0;x<this.Data.length;x++)if((""+this.Data[x][this.MetaData.PKCol])==sID)return x; return -1;};
	this.getEditingData = function (iID, sCol) { if (isNaN(sCol)) sCol = this.getCol(sCol).Index; if (this.PersistedData.EditingArr[iID] == null) { var i = this.getPKIndex(iID); if (i == -1) return null; return this.getData(i, sCol); } return cStr(this.PersistedData.EditingArr[iID][sCol]); };
	this.setEditingData = function (iID, sCol, sVal) { if (isNaN(sCol)) sCol = this.getCol(sCol).Index; if (this.PersistedData.EditingArr[iID] == null) { this.PersistedData.EditingArr[iID] = []; } this.PersistedData.EditingArr[iID][sCol]=sVal; };
	this.setData = function (iIndex, sCol, sVal) { if (isNaN(sCol)) sCol = this.getCol(sCol).Index; this.Data[iIndex][sCol] = sVal; this.Data[iIndex].HTML = ""; if (this.PersistedData.EditingArr[this.Data[iIndex][this.MetaData.PKCol]] != null) this.PersistedData.EditingArr[this.Data[iIndex][this.MetaData.PKCol]][sCol] = sVal; };
	this.clearCache = function (iIndex) { if (iIndex < 0 || iIndex >= this.Data.length) return false; this.Data[iIndex].HTML = ""; }
    this.setCellValue = function(iIndex, iCol, sValue) {
	    if (isNaN(iCol)) iCol = this.getCol(iCol).Index;
	    if (typeof iIndex == "object") {
	        var oRow = iIndex;
	        iIndex = oRow.getAttribute("index");
	    } else {
	        var oRow = $(this.Table).find(">*>tr[index='" + iIndex + "']")[0];
	    }	    	    
	    var o=$(oRow).find("[jid='" + this.Cols[iCol].ID+"']");
	    if (o.length == 0) return null;
	    if ((iIndex == c_Filter && this.Cols[iCol].FType != "" ? this.Cols[iCol].FType : this.Cols[iCol].Type) == "UCombo") {
	        if (sValue != null) fnJGridUComboSetByID(null, o[0], sValue, false);	    
        }else{
	        if (sValue != null) o.val(sValue);            
	    }
	    return o[0];
	};
	this.getControl = function(iIndex, iCol) { return this.setCellValue(iIndex, iCol); };
	this.getCellValue = function(iIndex, iCol) {	    
	    if (isNaN(iCol)) iCol = this.getCol(iCol).Index;
	    if (typeof iIndex == "object") {
	        var oRow = iIndex;
	        iIndex = oRow.getAttribute("index");	        
	    } else {
	        var oRow = $(this.Table).find(">*>tr[index='" + iIndex + "']")[0];	        
	    }
	    var sColType = this.Cols[iCol].Type;
	    if (iIndex == c_Filter && this.Cols[iCol].FType != "") sColType = this.Cols[iCol].FType;
        var o=$(oRow).find("[jid='" + this.Cols[iCol].ID+"']");
	    if (o.length == 0){
	        if (",Select,Options,".indexOf(sColType) == -1 && (iIndex >= 0 || iIndex == c_Filter)) return cStr(this.PersistedData.EditingArr[iIndex < 0 ? iIndex : this.getData(iIndex, this.MetaData.PKCol)] == null ? this.getData(iIndex, iCol) : this.PersistedData.EditingArr[iIndex < 0 ? iIndex : this.getData(iIndex, this.MetaData.PKCol)][iCol]); //Hide,NoEdit
            return "";
        }
	    
        if ("textarea,input".indexOf(o[0].nodeName.toLowerCase()) > -1) {
            if (cStr(o[0].type).toLowerCase() == "checkbox") return (o[0].checked?"1":"0");            
            return o.val();
        }
        if ("select".indexOf(o[0].nodeName.toLowerCase()) > -1) return (o[0].selectedIndex == -1 ? "" : (this.Cols[iCol].BindDropdownByID == 1 ? o[0].options[o[0].selectedIndex].value : o[0].options[o[0].selectedIndex].text));
        if (sColType == "UCombo") return (fnJGridUComboGet(this.Cols[iCol].BindDropdownByID, o[0])).split("&amp;").join("&");
        else if (o.hasClass("JCombo")) return (o[0].JCombo != null ? o[0].JCombo.getValue(this.Cols[iCol].BindDropdownByID == 1) : o.prop("initial-value"));
	    return "";
	};
	this.refreshColVisibility = function () {
	    for (var x = 0; x < this.Cols.length; x++) {
	        if (this.Cols[x].GroupTypes == "") {
	            this.Cols[x].Type = "Hide";
            }else{
	            if (this.Views[0].Grouping[x] == "Hide" && this.Cols[x].Type != "Hide") {	                
	                this.Cols[x].Type = "Hide";
	            } else if (this.Views[0].Grouping[x] != "Hide" && this.Cols[x].Type == "Hide") this.Cols[x].Type = (this.Cols[x]._Type == "Hide" ? "NoEdit" : this.Cols[x]._Type);
	        }
	    }
	};
	this.reloadViewsList = function () {
	    var s = cStr($(this.ID + "_views").val());
	    $("#" + this.ID + "_views option:gt(1)").remove();	    
	    for (var x = 2; x < this.Views.length; x++) if (this.Views[x] != null) $("#" + this.ID + "_views").append("<option value=" + x + (x == s || (s == "" && this.Views[x].Selected == 1) ? " selected='selected'" : "") + ">" + this.Views[x].Name + "</option>");
	};
	var oGrid = this;
	$("form").submit(function () { if ($("#" + oGrid.ID + "_MetaData").length == 0) $("form").append("<input type='hidden' id='" + oGrid.ID + "_MetaData' name='" + oGrid.ID + "_MetaData'/><input type='hidden' id='" + oGrid.ID + "_Views' name='" + oGrid.ID + "_Views'/>"); $("#" + oGrid.ID + "_MetaData").val(JSON.stringify(oGrid.MetaData)); $("#" + oGrid.ID + "_Views").val(JSON.stringify(oGrid.Views)); });
}

//the callback function that is called when an ASYNC request finishes
function fnJGridCallback(sID,oJSON){
    if( this==null ){
        if(cStr(sID)!="")return $("#"+sID)[0].JGrid.callback(null,oJSON);
        return $("#"+oJSON.MetaData.ClientID)[0].JGrid.callback(null,oJSON);
    }
    if(this.raiseEvent("GridCallbackStart").Cancel) return;
    var oTimer = new Date();
    if (oJSON.Views) {
        if(this.Views[0]!=null)oJSON.Views[0]=this.Views[0];
        this.Views = oJSON.Views;
        this.reloadViewsList();
    }
    if(oJSON.MetaData){
        var oOldMeta=this.MetaData;
        this.MetaData=oJSON.MetaData;
        if(this.MetaData.DisplayAdd==-1)this.MetaData.DisplayAdd=oOldMeta.DisplayAdd;
        if(this.MetaData.DisplayFilter == -1) this.MetaData.DisplayFilter = oOldMeta.DisplayFilter;
        if (this.PersistedData.EditingArr[c_Filter] == null && this.Views[0]) this.PersistedData.EditingArr[c_Filter] = this.Views[0].Filter;
        if(this.raiseEvent("GridCallbackMetaData").Cancel)return;        
    }

    if (oJSON.Cols) {
        this.Cols = oJSON.Cols;
        this._Cols=[];
        for (var x = 0; x < this.Cols.length; x++) {
            if (/MSIE (5|6)/.test(navigator.userAgent)) {
                this.Cols[x].Type = this.Cols[x].Type.split("UCombo").join("Combo");
                this.Cols[x].FType = this.Cols[x].FType.split("UCombo").join("Combo");
            }
            this._Cols[this.Cols[x].ID]=this.Cols[x];
            this.Cols[x].Index = x;
            this.Cols[x]._Type = this.Cols[x].Type;            
        }
        for (var x = 0; x < this.Cols.length; x++) {
            if (this.Cols[x].EditCols != '') {
                var sArr = this.Cols[x].EditCols.split(",");
                this.Cols[x].EditCols = [];
                for (var y = 0; y < sArr.length; y++) {                    
                    var oCol=this.getCol(sArr[y]);
                    if (oCol!=null) {
                        this.Cols[x].EditCols.push(oCol);
                        this.getCol(sArr[y]).renderCell=oCol;
                    }
                }                
            }
        }
        for (var y = 0; y < this.Views.length;y++){
            if (this.Views[y].Cols.length < this.Cols.length) {
                for (var x = 0; x < this.Cols.length; x++) {
                    if ($.inArray(x, this.Views[y].Cols) == -1) {
                        this.Views[y].Filter.push("");
                        this.Views[y].Grouping.push("Select");
                        this.Views[y].Cols.push(x);
                    }
                }
            }
        };
        this.refreshColVisibility();
    }
    this.OldData = this.Data;
    if(oJSON.Data){
        this.Data = oJSON.Data;
        this.PersistedData.Requests=[];
        for (var x = 0; x < this.Data.length; x++)
            if (this.OldData != null && this.OldData[x] != null && this.Data[x]!=null && this.OldData[x][this.MetaData.PKCol] == this.Data[x][this.MetaData.PKCol] && this.OldData[x].join("") == this.Data[x].join("")) this.Data[x].HTML = this.OldData[x].HTML;
    }
    
    this.RequestStatus = oJSON.RequestStatus;
        //oJSON.RequestStatus[x][0]=>Request ID
        //oJSON.RequestStatus[x][1]=>(0=Success,1=Unsuccessful)
        //oJSON.RequestStatus[x][2]=>Optional Message    
    
    if( oJSON.SelectionList ){
        this.PersistedData.SelectionCallback.Data=oJSON.SelectionList;
        this.getSelection(this.PersistedData.SelectionCallback.Function,this.PersistedData.SelectionCallback.AuxData,true);        
    }    
    
    this.render();
    //if(this.ID=="ctl01_c0_gGrid")$('#cLoadTime').html($('#cLoadTime').html()+" :: Render time: " + ((new Date()).getTime() - oTimer.getTime()) + " ms");
    this.raiseEvent("GridCallbackEnd");
}

function fnJGridSavePersistData(bResetFilter,bResetAdd){
    if (this.PersistedData.EditingArr[c_Filter]) {
        if (this.MetaData.DisplayFilter == 1 && !this.PersistedData.EditingArr[c_Filter].DisableCollect) {
            var oRow = $(this.Table).find("tr.filter");
            if (bResetFilter || oRow.length == 0) {
                if (bResetFilter || this.PersistedData.EditingArr[c_Filter].length == 0) {
                    this.PersistedData.EditingArr[c_Filter] = [];
                    for (var y = 0; y < this.Cols.length; y++) this.PersistedData.EditingArr[c_Filter][y] = "";
                }
            } else
                this.PersistedData.EditingArr[c_Filter] = this.collectRow(oRow[0]);
        }
        this.PersistedData.EditingArr[c_Filter].DisableCollect = null;
    }
    if(this.PersistedData.EditingArr[c_Add]=="deleted"){
        bResetAdd=true;
        this.PersistedData.OldColHTML="";
        this.raiseEvent("GridAddDone");
    }

    if (this.PersistedData.EditingArr[c_Add]&&!this.PersistedData.EditingArr[c_Add].DisableCollect) {
        var oRow = $(this.Table).find(">*>tr.add");
        if(bResetAdd||oRow.length==0){
            this.PersistedData.EditingArr[c_Add] = [];
            for(var y=0;y<this.Cols.length;y++)this.PersistedData.EditingArr[c_Add][y]=cStr(this.Cols[y].CInputDefault);
        }else
            this.PersistedData.EditingArr[c_Add] = this.collectRow(oRow[0], c_Add);
        this.PersistedData.EditingArr[c_Add].DisableCollect = null;
    }    

    if(this.OldData==null)this.OldData=this.Data;
    var oEdits = $(this.Table).find(">*>tr.editing");
    for(var x=0;x<oEdits.length;x++){
        if(this.OldData[cInt(oEdits[x].getAttribute("index"))] && this.PersistedData.EditingArr[this.OldData[cInt(oEdits[x].getAttribute("index"))][this.MetaData.PKCol]]!=null)
            this.PersistedData.EditingArr[this.OldData[cInt(oEdits[x].getAttribute("index"))][this.MetaData.PKCol]]=this.collectRow(oEdits[x]);
    }
    if(this.PersistedData.LinkedGrid.length>0){
        var iNumGrids=this.MetaData.LinkedGridIDs.split(",").length;
        for(var x=0;x<this.Data.length;x++){
            var iPK=this.Data[x][this.MetaData.PKCol];
            if(this.PersistedData.LinkedGrid[iPK]!=null){
                for(var y=0;y<iNumGrids;y++)
                    if (this.PersistedData.LinkedGrid[iPK]["show_" + y] == 1 && this.PersistedData.LinkedGrid[iPK]["grid_" + y].Table!=null) this.PersistedData.LinkedGrid[iPK]["grid_" + y].savePersistData();                
            }
        }
    }    
}

//function to render the grid content
function fnJGridRender(sID,bPartialRendering){
    if (this == null) return $("#" + sID)[0].JGrid.render(null, bPartialRendering);
    this.Table=$("#"+this.ID)[0];
    if (!this.Table) return;
    //if (this.Rendering) alert('collision');
    this.Rendering = true;
    this.Table.JGrid=this;
    var oData = {Grid: this};
    if (bPartialRendering != false) bPartialRendering = (this.OldData == null && bPartialRendering) || (this.OldData != null && this.OldData.length == this.Data.length && $.grep(this.OldData, function (o,i) { return o[oData.Grid.MetaData.PKCol] != oData.Grid.Data[i][oData.Grid.MetaData.PKCol]; }).length == 0);
    if (this.MetaData.IsEmbedded == 1 || this.PersistedData.LinkedGrid.length > 0) bPartialRendering = false;    
    //bPartialRendering = false;
    //if (this.ID == "ctl01_c0_gGrid") $('#cLoadTime').html("Partial: " + bPartialRendering);
    
	//Render Cols
	if( this.raiseEvent("GridRenderStart").Cancel)return;	
    var sFilter="";
    var sAdd="";
    this.Cols.HTML="";
        
    this.savePersistData();

    for (var y=0;y<this.Cols.length; y++){
        var oCol = this.Cols[this.Views[0].Cols[y]];
        if (oCol == null) {
            this.Views[0].Cols.push(y);
            oCol = this.Cols[y];
        }
        if (oCol.Type != "Hide") {
            if (oCol.renderCell == null) {
                //Render Col Headings
                oData = this.renderCell(oData, c_Header, oCol.Index, c_Header);
                this.Cols.HTML += oData.TagStart + oData.HTML + oData.TagEnd;

                if (this.MetaData.DisplayFilter == 1) {
                    oData = this.renderCell(oData, c_Filter, oCol.Index, c_Filter);
                    sFilter += oData.TagStart + oData.HTML + oData.TagEnd;
                }

                if (this.MetaData.DisplayAdd == 1) {
                    var sHTML = "";
                    if (oCol.EditCols != null) {
                        for (var z = 0; z < oCol.EditCols.length; z++) {
                            oData = this.renderCell(oData, c_Add, oCol.EditCols[z].Index, c_Add);
                            sHTML += oData.TagStart + oData.HTML + oData.TagEnd;
                        }
                    }
                    oData = this.renderCell(oData, c_Add, oCol.Index, c_Add);
                    sAdd += oData.TagStart + (sHTML == "" || oCol.Type != "NoEdit" ? oData.HTML : "") + (sHTML == "" ? oData.TagEnd : oData.TagEnd.split("</td>").join("") + sHTML + "</td>");
                }
            }
        }
    }
    
    //Render Heading
    oData.TagStart = "<tr index='" + c_Header + "' pk_id='" + c_Header + "'>";
    oData.HTML=this.Cols.HTML;
    oData.TagEnd="</tr>";
    oData.Index=c_Header;
    this.raiseEvent("GridRenderRow", oData);    
    this.Cols.HTML="<thead class='jgrid-head'>"+oData.TagStart+oData.HTML+oData.TagEnd;
        
    //Render filter
    if (this.MetaData.DisplayFilter==1){
        oData.TagStart = "<tr class='filter' index='" + c_Filter + "' pk_id='" + c_Filter + "'>";
        oData.HTML=sFilter;        
        oData.Index=c_Filter;
        this.raiseEvent("GridRenderRow",oData);
        this.Cols.HTML+=oData.TagStart+oData.HTML+oData.TagEnd;
    }
    
    //Render Add-row
    if (this.MetaData.DisplayAdd==1){
        oData.TagStart = "<tr class='add' index='" + c_Add + "' pk_id='" + c_Add + "'>";
        oData.HTML=sAdd;
        oData.Index=c_Add;
        this.raiseEvent("GridRenderRow",oData);
        this.Cols.HTML+=oData.TagStart+oData.HTML+oData.TagEnd;
    }    
    
   this.Cols.HTML+="</thead>";
          
   var sJS = "";
   this.Data.HTML = "";
   if (this.Data.length == 0 && this.MetaData.NoRecordsMessage!="")this.Data.HTML = "<tr class='even " + this.MetaData.CRowClass + "'><td colspan='"+this.Cols.length+"'>" + this.MetaData.NoRecordsMessage + "</td></tr>";

   for (var x = 0; x < this.Data.length; x++) {
       this.Data[x].Render=false;
       if (!bPartialRendering || !this.Data[x].HTML) {
           this.Data[x].Render=true;
           var iPK = this.Data[x][this.MetaData.PKCol];
           if (this.PersistedData.EditingArr[iPK] != null && this.PersistedData.EditingArr[iPK].length == 0) this.PersistedData.EditingArr[iPK] = this.Data[x].slice();

           if (!this.Data[x].HTML || this.PersistedData.EditingArr[iPK] != null) {
               this.Data[x].HTML = " ";
               if ($.inArray(iPK, this.DeletedArr) == -1 && cStr(iPK) != "") {

                   for (var y = 0; y < this.Cols.length; y++) {
                       var oCol = this.Cols[this.Views[0].Cols[y]];
                       if (oCol.Type != "Hide" && oCol.renderCell == null) {

                           var sHTML = "";
                           if (this.PersistedData.EditingArr[iPK] != null && oCol.EditCols != null) {
                               for (var z = 0; z < oCol.EditCols.length; z++) {
                                   oData = this.renderCell(oData, x, oCol.EditCols[z].Index, iPK);
                                   sHTML += oData.TagStart + oData.HTML + oData.TagEnd;
                               }
                           }
                           oData = this.renderCell(oData, x, oCol.Index, iPK);
                           this.Data[x].HTML += oData.TagStart+(sHTML == "" || oCol.Type != "NoEdit" ? oData.HTML : "") + (sHTML == "" ? oData.TagEnd : oData.TagEnd.split("</td>").join("") + sHTML + "</td>");
                       }
                   }

                   //Render row
                   oData.TagStart = "<tr index='" + x + "' pk_id='" + iPK + "' class='"+ (x % 2 == 0 ? "even" : "odd")+" jgrid-row" + " " + this.MetaData.CRowClass + (this.PersistedData.SelectionMode + cInt(($.inArray(iPK, this.PersistedData.Selections) > -1 ? 1 : 0)) == 1 ? " selected" : "") + (this.PersistedData.EditingArr[iPK] != null ? " editing" : "") + "'" + (this.MetaData.OnRowDoubleClick != "" && this.PersistedData.EditingArr[iPK] == null ? " ondblclick='" + this.MetaData.OnRowDoubleClick + "'" : "") + ">";
                   oData.HTML = cStr(this.Data[x].HTML);
                   oData.TagEnd = "</tr>";
                   oData.Editing = this.PersistedData.EditingArr[iPK] != null;

                   if (this.NumVisibleCols == null) this.NumVisibleCols = oData.HTML.split("<td").length;
                   if (!this.raiseEvent("GridRenderRow", oData).Cancel) {
                       this.Data[x].HTML = oData.TagStart + oData.HTML + oData.TagEnd;

                       if (this.PersistedData.LinkedGrid[iPK] != null) {
                           for (var y = 0; y < this.MetaData.LinkedGridIDs.split(",").length; y++) {
                               if (this.PersistedData.LinkedGrid[iPK]["show_" + y] == 1) {
                                   oData.TagStart = "<tr index='" + x + "' pk_id='" + iPK + "' class='linked " + (x % 2 == 0 ? "even" : "odd") + " " + this.MetaData.CRowClass + (this.PersistedData.SelectionMode + cInt(($.inArray(iPK, this.PersistedData.Selections) > -1 ? 1 : 0)) == 1 ? " selected" : "") + (this.PersistedData.EditingArr[iPK] != null ? " editing" : "") + "'><td colspan='" + this.NumVisibleCols + "'>";
                                   oData.HTML = this.PersistedData.LinkedGrid[iPK]["html_" + y];
                                   oData.TagEnd = "</td></tr>";
                                   oData.GridIndex = y;
                                   this.raiseEvent("GridRenderEmbeddedGrid", oData);
                                   this.Data[x].HTML += oData.TagStart + oData.HTML + oData.TagEnd;
                                   if (this.PersistedData.LinkedGrid[iPK]["grid_" + y].Table == null)
                                       sJS += "var o=$('#" + this.ID + "')[0].JGrid.PersistedData.LinkedGrid[" + iPK + "]; $('#'+o[\"grid_" + y + "\"].ID)[0].JGrid=o[\"grid_" + y + "\"];o[\"grid_" + y + "\"].IsEmbedded=1;o[\"grid_" + y + "\"].addRequest(o[\"grid_" + y + "\"].buildRequest(\"refresh\",\"RefreshColData\",1));";
                                   else
                                       sJS += "$('#" + this.ID + "')[0].JGrid.PersistedData.LinkedGrid[" + iPK + "][\"grid_" + y + "\"].render();";
                               }
                           }
                       }
                   }
               }
           } else {
               if (this.PersistedData.LinkedGrid[iPK] != null)
                   for (var y = 0; y < this.MetaData.LinkedGridIDs.split(",").length; y++)
                       if (this.PersistedData.LinkedGrid[iPK]["show_" + y] == 1) sJS += "$('#" + this.ID + "')[0].JGrid.PersistedData.LinkedGrid[" + iPK + "][\"grid_" + y + "\"].render();";
           }
           this.Data.HTML += cStr(this.Data[x].HTML);           
       }
   }
   
    if (!bPartialRendering) {
        this.Data.HTML = (this.Data.HTML.toLowerCase().indexOf("<tbody") == -1 ? "<tbody class='jgrid-tbody'>" : "") + this.Data.HTML + (this.Data.HTML.toLowerCase().indexOf("<tbody") == -1 ? "</tbody>" : "");
        if (this.Cols.HTML == this.PersistedData.OldColHTML && this.MetaData.IsEmbedded==0) {
            $(this.Table).find("tbody").detach().end().append(this.Data.HTML);
            $(this.Table).find(".jgrid-head div.JCombo").each(function (i, o) { if(o.JCombo)o.JCombo.render(o, o.JCombo); });            
        } else {            
            $(this.Table).html(this.Cols.HTML + this.Data.HTML);
        }
    } else {
        if (this.Cols.HTML!=this.PersistedData.OldColHTML) $(this.Table).find(".jgrid-head").detach().end().append(this.Cols.HTML);
        for (var x = 0; x < this.Data.length; x++) {
            if (this.Data[x].Render) {
                var s = "";
                for (var y = x; y < this.Data.length; y++) {
                    s += this.Data[y].HTML;
                    if (this.Data[x].HTML.indexOf("<tbody") != 0 || this.Data[y].HTML.indexOf("</tbody>") > -1) break;
                }
                if (s.indexOf("<tbody") != 0 && s.lastIndexOf("</tbody>") == s.length - 8) s = s.substring(0, s.length - 8);
                $(this.Table).find(".jgrid-row[index='" + x + "']").eq(0).before(s).end().detach();                
            }
        }
    }    
    this.PersistedData.OldColHTML = this.Cols.HTML;
    sID = this.ID;
    var __grid = this;
    this.Rendering = false;
    $(function () {
        var o = $(__grid.Table).find(">*>tr[index='-2000']");
        o.on("focus", "[jid]", function () { __grid.PersistedData.__Focus = $(this).prop("jid"); });
        o = o.find("input.daterangepicker");
        if (o.length > 0 && o.DateRangePicker) o.DateRangePicker({ onOpen: function () { $.data(this.input, "val", this.input.value); }, onClose: function () { if ($.data(this.input, "val") != this.input.value) { try { var oData = fnJGridGetData(this.input); if (oData.Index == c_Filter && oData.Grid.MetaData.AutoFilter == 1) fnJGridSetFilter(null, oData.Element); } catch (e) { } } } });
        o = $(__grid.Table).find(">*>tr.jgrid-row input.daterangepicker, >*>tr.add input.daterangepicker");
        if (o.length > 0 && o.datepicker) o.datepicker();
        o = $(__grid.Table).find("div.JCombo");
        if (o.length > 0 && o.JCombo) o.JCombo({ options: 'tag' });
        __grid.raiseEvent("GridRenderEnd");
        __grid.OldData = null;
        if (sJS != "") eval(sJS);
        if (__grid.PersistedData.__Focus != null) setTimeout(function () { $(__grid.Table).find(">*>tr[index='-2000'] [jid='" + __grid.PersistedData.__Focus + "']").focus(); }, 100);        
    });
}

function fnJGridRenderCell(oData, x, y, iPK) {
    if (isNaN(y)) y = this.getCol(y).Index;
    if (isNaN(iPK)) iPK =(x<0?x: this.Data[x][this.MetaData.PKCol]);
    oData.Index = x;
    oData.ID = iPK;
    oData.Col = y;
    oData.ColObject = this.Cols[y];
    oData.HTML = "";
    
    if (x >= 0) {

        oData.TagStart = (this.Cols[y].renderCell == null && !oData.NoTable ? "<td index='" + y + "' " + this.Cols[y].CAtt + ">" : "");
        oData.TagEnd = (this.Cols[y].renderCell == null && !oData.NoTable ? "</td>" : "");
        //are we editing                    
        if (this.PersistedData.EditingArr[iPK] != null) {
            oData.Editing = true;
            if (this.Cols[y].Type == "Text") {
                oData.HTML = "<input jid='" + this.Cols[y].ID + "' onkeydown='return fnJGridHandleKeyDown(event," + x + ");' type='text' " + cStr(this.Cols[y].CInputAtt) + " value='" + replaceAll(this.PersistedData.EditingArr[iPK][y], "'", "&#39;") + "'>";
            } else if (this.Cols[y].Type == "Textarea") {
                oData.HTML = "<textarea jid='" + this.Cols[y].ID + "' onkeydown='return fnJGridHandleKeyDown(event," + x + ");' " + cStr(this.Cols[y].CInputAtt) + ">" + this.PersistedData.EditingArr[iPK][y]+  "</textarea>";
            } else if (this.Cols[y].Type == "Combo") {
                var oVals = this.Cols[y].CInputValues;
                oData.HTML = "<select jid='" + this.Cols[y].ID + "' " + cStr(this.Cols[y].CInputAtt) + ">";
                if (oVals)
                    for (var z = 0; z < oVals.length; z++)
                        oData.HTML += "<option" + (this.PersistedData.EditingArr[iPK][y] == (this.Cols[y].BindDropdownByID == 1 ? oVals[z][0] : oVals[z][1]) ? " selected" : "") + " value='" + replaceAll((this.Cols[y].BindDropdownByID == 1 ? oVals[z][0] : oVals[z][1]), "'", "&#39;") + "'>" + oVals[z][1] + "</option>";
                oData.HTML += "</select>";
            } else if (this.Cols[y].Type == "UCombo")
                oData.HTML = fnJGridUComboRender(this.Cols[y].ID, iPK, this.Cols[y].CInputValues, (this.Cols[y].BindDropdownByID == 1 ? 0 : 1), (this.PersistedData.EditingArr[iPK].length > y ? this.PersistedData.EditingArr[iPK][y] : ""), this.Cols[y].CInputAtt, this.Cols[y].UListAtt);
            else if(this.Cols[y].Type == "JCombo"){
                var s=(this.PersistedData.EditingArr[iPK].length > y ? this.PersistedData.EditingArr[iPK][y] : "");
                oData.HTML = "<div jid='" + this.Cols[y].ID + "' " + this.Cols[y].CInputAtt + " initial-value='" + replaceAll(s, "'", "&#39;") + "'" + (s.indexOf("\n") == -1 && this.Cols[y].BindDropdownByID == 0 ? " text='" + s + "'" : "") + "></div>";
            }else if (this.Cols[y].Type == "Options")
                oData.HTML = "<button type='button' onclick='fnJGridRowSave(event,this);'>Save</button> <a href='javascript:void(0);' onclick='fnJGridRowEdit(this);'>Cancel</a>";
            
            if (oData.HTML == "")
                oData.HTML = "<span jid='" + this.Cols[y].ID + "'>"+(cStr(this.Data[x][y]) == "" ? "&nbsp;" : cStr(this.Data[x][y]))+"</span>";
            //else {
                //oData.TagStart += "<div index='" + y + "'>";
                //oData.TagEnd = "</div>" + oData.TagEnd;
            //}

        } else {
            oData.Editing = false;
            if (this.Cols[y].Type == "Select") {
                oData.HTML = "<input jid='" + this.Cols[y].ID + "' type='checkbox' " + this.Cols[y].CInputAtt + " onclick='fnJGridSelectRow(event,this);'" + (this.PersistedData.SelectionMode + cInt(($.inArray(iPK, this.PersistedData.Selections) > -1 ? 1 : 0)) == 1 ? " checked" : "") + ">";
            } else if (this.Cols[y].BindDropdownByID == 1 && (this.Cols[y].Type == "Combo" || this.Cols[y].Type == "UCombo")) {
                var sVal = cStr(fnJGridUComboLookup(this.Cols[y].CInputValues, false, cStr(this.Data[x][y]))[1]);
                oData.HTML = (sVal == "" ? "&nbsp;" : sVal);
            } else if (this.Cols[y].BindDropdownByID == 1 && this.Cols[y].Type == "JCombo") {
                if((""+this.Data[x][y])!=""){
                    if (this.Cols[y].DataSource == null) this.Cols[y].DataSource = eval($("<div " + this.Cols[y].CInputAtt + "></div>").attr("data"));
                    if (this.Cols[y].DataSource != null && this.Data[x][y].indexOf("\n") == -1) {
                        for (var z = 0; z < this.Cols[y].DataSource.length; z++)
                            if (("|" + this.Data[x][y] + "|").indexOf("|" + this.Cols[y].DataSource[z][0] + "|") > -1) {
                                oData.HTML += (oData.HTML == "" ? "" : "<br>") + this.Cols[y].DataSource[z][1];
                                //break;
                            }
                    } else {
                        oData.HTML = (this.Data[x][y].indexOf("\n") > -1 ? this.Data[x][y].split("\n")[1] : this.Data[x][y]);
                    }                    
                }
            } else {
                oData.HTML = (cStr(this.Data[x][y]) == "" ? "&nbsp;" : cStr(this.Data[x][y]));
            }
        }
        //if (oData.HTML.split("&nbsp;").join("") == "" || (this.Cols[y].Type == "Options" && this.PersistedData.EditingArr[iPK] == null)) oData.HTML = this.Cols[y].CText;
        if (oData.HTML.split("&nbsp;").join("") == "") oData.HTML = this.Cols[y].CText;
    } else if (x == c_Filter) {

        oData.TagStart = "<td index='" + y + "' " + this.Cols[y].HAtt + ">";
        oData.TagEnd = "</td>";
        if (this.Cols[y].FType=="") this.Cols[y].FType = this.Cols[y]._Type;
        if (this.Cols[y].FType == "Text"){
            oData.HTML += "<input jid='" + this.Cols[y].ID + "' type='text' onkeydown='return fnJGridHandleKeyDown(event," + x + ");'" + (this.MetaData.AutoFilter == 1 && !oData.ViewMode ? " onchange='if($.data(this,\"val\")==this.value)return;$.data(this,\"val\",this.value);fnJGridSetFilter(event,this);' " : "") + (cStr(this.Cols[y].FInputAtt) != "" ? this.Cols[y].FInputAtt : this.Cols[y].CInputAtt) + " value='" + replaceAll(this.PersistedData.EditingArr[x][y], "'", "&#39;") + "'>";
        } else if (this.Cols[y].FType == "Textarea") {
            oData.HTML += "<textarea jid='" + this.Cols[y].ID + "' onkeydown='return fnJGridHandleKeyDown(event," + x + ");'" + (this.MetaData.AutoFilter == 1 && !oData.ViewMode ? " onchange='if($.data(this,\"val\")==this.value)return;$.data(this,\"val\",this.value);fnJGridSetFilter(event,this);' " : "") + (cStr(this.Cols[y].FInputAtt) != "" ? this.Cols[y].FInputAtt : this.Cols[y].CInputAtt) + ">" + this.PersistedData.EditingArr[x][y] + "</textarea>";
        } else if (this.Cols[y].FType == "Combo" || this.Cols[y].FType == "UCombo") {
            if (this.Cols[y].FInputValues != null) var oVals = this.Cols[y].FInputValues; else var oVals = this.Cols[y].CInputValues;
            if (this.Cols[y].FType == "UCombo") {
                oData.HTML += fnJGridUComboRender(this.Cols[y].ID, x, oVals, (this.Cols[y].BindDropdownByID == 1 ? 0 : 1), cStr(this.PersistedData.EditingArr[x][y]), (this.MetaData.AutoFilter==1&&!oData.ViewMode ? "autofilter=1 " : "") + (cStr(this.Cols[y].FInputAtt) != "" ? this.Cols[y].FInputAtt : this.Cols[y].CInputAtt), this.Cols[y].UListAtt);
            } else {
                oData.HTML += "<select jid='" + this.Cols[y].ID + "'" + (this.MetaData.AutoFilter == 1 && !oData.ViewMode ? " onchange='fnJGridSetFilter(event,this);'" : "") + " " + (cStr(this.Cols[y].FInputAtt) != "" ? this.Cols[y].FInputAtt : this.Cols[y].CInputAtt) + ">";
                oData.HTML += "<option></option>";

                if (oVals != null)
                    for (var z = 0; z < oVals.length; z++)
                        oData.HTML += "<option" + (cStr(this.PersistedData.EditingArr[x][y]) == (this.Cols[y].BindDropdownByID == 1 ? oVals[z][0] : oVals[z][1]) ? " selected" : "") + " value='" + replaceAll((this.Cols[y].BindDropdownByID == 1 ? oVals[z][0] : oVals[z][1]), "'", "&#39;") + "'>" + oVals[z][1] + "</option>";

                oData.HTML += "</select>";
            }
        }else if(this.Cols[y].FType == "JCombo"){
            //alert("write: " + this.PersistedData.EditingArr[x].length > y ? this.PersistedData.EditingArr[x][y] : "");
            oData.HTML = "<div jid='" + this.Cols[y].ID + "'" + (this.MetaData.AutoFilter == 1 && !oData.ViewMode ? " onclientchange='fnJGridSetFilter(null,oCombo.div);'" : "") + " " + (cStr(this.Cols[y].FInputAtt) != "" ? this.Cols[y].FInputAtt : this.Cols[y].CInputAtt) + " initial-value='" + replaceAll(this.PersistedData.EditingArr[x][y], "'", "&#39;") + "'></div>";
        }else if (this.Cols[y].FType == "Options") {
            oData.HTML += "<a href='javascript:void(0);' onclick='fnJGridSetFilter(event,this,true);'>Reset Filter</a>" + (!this.MetaData.AutoFilter == 1 ? "&nbsp;<button type='button' onclick='fnJGridSetFilter(event,this);'>Filter</button>" : "");
        } else {
            oData.HTML += "&nbsp;";
        }

    } else if (x == c_Header) {
        var s = (",,hide,select,".indexOf(this.Views[0].Grouping[y].toLowerCase()) > -1 ? "" : this.Views[0].Grouping[y]);
        oData.HTML = (this.Cols[y].Sortable == 0 ? "" : "<a href='javascript:void(0);'" + " class='sort" + (this.Views[0].SortCol != y ? "" : " active" + (this.Views[0].SortOrder == 0 ? " down" : " up")) + "'>") + (s == "" || "MaxMinCountAvgGroup".indexOf(s.split("-")[0]) == -1 ? "" : s + "(") + (this.Cols[y].HText == "" ? "&nbsp;" : this.Cols[y].HText) + (s == "" || "MaxMinCountAvgGroup".indexOf(s.split("-")[0]) == -1 ? "" : ")") + (this.Cols[y].Sortable == 0 ? "" : "</a>");
        oData.TagStart = "<th index='" + y + "' " + this.Cols[y].HAtt + (this.Cols[y].Sortable == 1 ? " onclick='fnJGridSetSort(this," + y + ");'" : "") + ">";
        oData.TagEnd = "</th>";
    } else if (x == c_Add) {
        if (this.Cols[y].FType == "") this.Cols[y].FType = this.Cols[y].Type;
        oData.TagStart = (this.Cols[y].renderCell == null && !oData.NoTable ? "<td index='" + y + "' " + this.Cols[y].CAtt + ">" : "");
        oData.TagEnd = (this.Cols[y].renderCell == null && !oData.NoTable ? "</td>" : "");
                
        if (this.Cols[y].Type == "Text") {
            oData.HTML += "<input jid='" + this.Cols[y].ID + "' onkeydown='return fnJGridHandleKeyDown(event," + x + ");' type='text' " + cStr(this.Cols[y].CInputAtt) + " value='" + replaceAll((this.PersistedData.EditingArr[x].length > y ? this.PersistedData.EditingArr[x][y] : cStr(this.Cols[y].AddPrompt)), "'", "&#39;") + "'>";
        } else if (this.Cols[y].Type == "Textarea") {
            oData.HTML += "<textarea jid='" + this.Cols[y].ID + "' onkeydown='return fnJGridHandleKeyDown(event," + x + ");' " + cStr(this.Cols[y].CInputAtt) + ">" + (this.PersistedData.EditingArr[x].length > y ? this.PersistedData.EditingArr[x][y] : cStr(this.Cols[y].AddPrompt)) + "</textarea>";
        } else if (this.Cols[y].Type == "Combo") {
            var oVals = this.Cols[y].CInputValues;
            oData.HTML += "<select jid='" + this.Cols[y].ID + "' " + cStr(this.Cols[y].CInputAtt) + ">" + (cStr(this.Cols[y].AddPrompt) == "" ? "" : "<option value='" + replaceAll(this.Cols[y].AddPrompt, "'", "&#39;") + "'>" + this.Cols[y].AddPrompt + "</option>");
            if (oVals)
                for (var z = 0; z < oVals.length; z++)
                    oData.HTML += "<option value='" + replaceAll((this.Cols[y].BindDropdownByID == 1 ? oVals[z][0] : oVals[z][1]), "'", "&#39;") + "'" + ((cStr(this.PersistedData.EditingArr[x][y]) == (this.Cols[y].BindDropdownByID == 1 ? oVals[z][0] : oVals[z][1])) || (this.PersistedData.EditingArr[x].length == 0 && this.Cols[y].CInputDefault == oVals[z][1]) && oData.HTML.indexOf(" selected") == -1 ? " selected" : "") + ">" + oVals[z][1] + "</option>";

            oData.HTML += "</select>";
        } else if (this.Cols[y].Type == "UCombo")
            oData.HTML += fnJGridUComboRender(this.Cols[y].ID, x, this.Cols[y].CInputValues, (this.Cols[y].BindDropdownByID == 1 ? 0 : 1), (this.PersistedData.EditingArr[x].length > y ? this.PersistedData.EditingArr[x][y] : this.Cols[y].CInputDefault), this.Cols[y].CInputAtt, this.Cols[y].UListAtt);
         else if (this.Cols[y].Type == "JCombo")
             oData.HTML = "<div jid='" + this.Cols[y].ID + "' " + this.Cols[y].CInputAtt + " initial-value='" + replaceAll((this.PersistedData.EditingArr[x].length > y ? this.PersistedData.EditingArr[x][y] : this.Cols[y].CInputDefault), "'", "&#39;") + "'></div>";
            
         else if (this.Cols[y].FType == "Options") 
            oData.HTML += "<button type='button' onclick='fnJGridRowSave(event,this);'>Add</button>&nbsp;<a href='javascript:void(0);' onclick='var oData=fnJGridGetData(this);oData.Grid.PersistedData.EditingArr[c_Add]=\"deleted\";oData.Grid.render(null,true);'>Reset</a>";         
         else
             oData.HTML += "<span jid='" + this.Cols[y].ID + "'>"+cStr((this.PersistedData.EditingArr[x]&&this.PersistedData.EditingArr[x].length > y ? this.PersistedData.EditingArr[x][y]:""),"&nbsp;")+"</span>";
           
    }
    
    this.raiseEvent("GridRenderCell", oData);
    return oData;
}

function fnJGridSetSort(oElement,iIndex){
    var oData=fnJGridGetData(oElement);    
    oData.ColIndex=iIndex;
    oData.OldColIndex = oData.Grid.Views[0].SortCol;
    if (oData.Grid.Views[0].SortCol == iIndex) {
        oData.Grid.Views[0].SortOrder = (oData.Grid.Views[0].SortOrder == 0 ? 1 : 0);
    }else{
        oData.Grid.Views[0].SortOrder = 0;
        oData.Grid.Views[0].SortCol = iIndex;
    }    
    oData.Grid.raiseEvent("GridSortEnd",oData);

    oData.Grid.refresh();
    oData.Grid.Cols.HTML="";
}

function fnJGridSetFilter(event,oElement,bReset,bDisableRefresh){
    if (event != null) event.cancelBubble = true; //Event.stop(event);
    
    if(this==null || this.Table==null){
        var oData=fnJGridGetData(oElement);
        return oData.Grid.setFilter(event,oElement,bReset);
    }    
    var oRow = $(this.Table).find("tr.filter");
    if(bReset){
        oRow.find(".JCombo").each(function (i, o) { if(o.JCombo)o.JCombo.reset(); });        
        oRow.find("input, textarea").val("");
        oRow.find("select").each(function (i, o) { o.selectedIndex = 0; });
        oRow.find("ul > li > a.active").each(function (i, o) {o.setAttribute("value","");$(o).replaceWith("");});        
        this.raiseEvent("GridFilterReset", null);
    }    
    this.MetaData.Page = 0;
    this.Views[0].Filter = this.collectRow(oRow[0]);            
    this.PersistedData.Selections=[];
    this.PersistedData.SelectionMode=0;
    this.PersistedData.SelectionCount = 0;    
    this.raiseEvent("RowSelectionChange", null);

    if(!bDisableRefresh)this.refresh();
}

function fnJGridAddRequest(oRequest) {    
    this.PersistedData.Requests.push(oRequest);
    this.processQueue();
}

function fnJGridProcessQueue(sID){
    if (this == null) return $("#"+sID)[0].JGrid.processQueue();

    this.PersistedData.Requests.removeByValue(null, null);
    var iActive=0;
    $.grep(this.PersistedData.Requests, function (o, i) { if (o.Status == "running") iActive++; });
    this.IsAjaxActive = (iActive > 0);
    
    var oLastRequest=null;
    var oTempRequests=[];
    //if(iActive<this.MaxRequests){
    if (iActive < this.PersistedData.Requests.length) {
        for (var x = 0; x < this.PersistedData.Requests.length; x++) {
            if (this.PersistedData.Requests[x].Status == null) {
                if (this.PersistedData.Requests[x].Action == "multi" && oTempRequests.length > 0) break;
                oLastRequest = this.PersistedData.Requests[x];
                this.PersistedData.Requests.removeByValue(null, oLastRequest);
                if (oLastRequest.Action != "refresh") oTempRequests.push(oLastRequest);
                if (oLastRequest.Action == "multi") break;
            }
        }
    }
    //}
    if(oLastRequest!=null){
        oLastRequest = $.extend(true, {}, oLastRequest);
        oLastRequest.Status = "running";
        this.PersistedData.Requests.push(oLastRequest);
        if (cInt(oLastRequest.Attempts) == 0) oLastRequest.Attempts = 1;
        oLastRequest.Failure=false;
        if(oTempRequests.length>1){
            oLastRequest.Action="multi";
            oLastRequest.Requests=oTempRequests;
        }
        oLastRequest.MetaData = this.MetaData;
        oLastRequest.ID=this.ID;
        oLastRequest.Views = this.Views;
        var oGrid = this;
        var s = $(this.Table).parents("form:eq(0)").attr("action") || window.location.href;
        var oSend = { "_request": oLastRequest, "Url": s.split("#")[0] + (s.indexOf("?") == -1 ? "?" : "&") + "__EVENTTARGET=" + escape(oGrid.ID) + "&__AJAXCALL=1" };
        if (!this.raiseEvent("GridAjaxBeforeStart", oSend).Cancel) {
            oSend._request = cStr(JSON.stringify(oLastRequest));
            var oFRequest = function () { var oAjax = $.ajax({ "type": "POST", "url": oSend.Url + "&attempt=" + oLastRequest.Attempts + "&_time=" + escape(oLastRequest._time), "data": oSend, "success": oFSuccess, "error": oFError }); }
            var oFSuccess = function (oData, textStatus) {
                oLastRequest.Status = textStatus;
                oLastRequest.Data = oData;
                oGrid.raiseEvent("GridAjaxComplete", oLastRequest);                
                oGrid.PersistedData.Requests.removeByValue(null, oLastRequest);                
                oGrid.processQueue();
            };
            var oFError = function (oData, textStatus) {
                oLastRequest.Attempts++;
                oLastRequest.Status = textStatus;
                if (oLastRequest.Attempts < 5) {
                    oLastRequest.Status = "running";
                    oFRequest();
                } else {
                    if (!oGrid.raiseEvent("GridAjaxError", { "Data": oData, "Text": textStatus, "Request": oLastRequest }).Cancel) alert("An unknown error occured while attempting to send your request to the server.\nI've attempted the operation five times unsuccessfully (" + textStatus + "). Our technicians have been notified.");
                    oLastRequest.Data = oData;
                    oGrid.raiseEvent("GridAjaxComplete", oLastRequest);
                    oGrid.PersistedData.Requests.removeByValue(null, oLastRequest);
                }
            };
            oLastRequest.Status = "running";
            oFRequest();
            this.raiseEvent("GridAjaxStart", oLastRequest);
            this.IsAjaxActive = true;
            $("div[class='ui-daterangepickercontain']").remove();
        }
    }
}

function fnJGridCollectEditingRow(iIndex, iID) {
    var oValues=[];
    if( isNaN(iIndex)){
        oRow=iIndex;
        if(cStr(oRow.getAttribute("index"))!="")iIndex=cInt(oRow.getAttribute("index"));
    } else
        var oRow = $(this.Table).find('>*>tr[index="' + iIndex +'"]')[0];

    if (iID == null && !isNaN(iIndex)) iID = iIndex;
    for (var y = 0; y < this.Cols.length; y++)
        oValues[y] = this.getCellValue(oRow,y);
        
    if( iID==c_Add)
        for(var y=0;y<this.Cols.length;y++) if(cStr(this.Cols[y].AddPrompt)==oValues[y])oValues[y]="";
    
    var oEvent={"Grid":this,"Values":oValues, "Row":oRow, "Index":(isNaN(iIndex)?-1:iIndex)};
    this.raiseEvent("GridRowCollected",oEvent);
    return oEvent.Values;
}

function fnJGridRowSave(event,oCell){
    var bSuccess=true;
    if (event != null) event.cancelBubble = true;
    if (oCell == null){
        oCell=[];
        for(var x=0;x<this.Data.length;x++)
            if (this.PersistedData.EditingArr[this.Data[x][this.MetaData.PKCol]] != null) oCell.push($(this.Table).find(">*>tr[index='" + x + "'] td:eq(0)")[0]);
    }
    if (!$.isArray(oCell)) oCell = new Array(oCell);
    var oRequests = [];
    var oData;
    for (var x = 0; x < oCell.length; x++) {
        if (!isNaN(Number(oCell[x]))) oCell[x] = $(this.Table).find(">*>tr[index='" + oCell[x] + "'] td:eq(0)")[0];        
        oData = fnJGridGetData(oCell[x]);
        oData.Values = oData.Grid.collectRow(oData.Index);

        if (oData.Index >= 0)
            for (var y = 0; y < oData.Grid.Cols.length; y++)
                if ((oData.Grid.Cols[y].Type == "NoEdit" && cStr(oData.Values[y]) == "") || oData.Grid.Cols[y].Type == "Options")
                    oData.Values[y] = oData.Grid.Data[oData.Index][y];

        if (!oData.Grid.raiseEvent("GridRowBeforeSave", oData).Cancel) {
            if (oData.Index > -1) oData.Grid.Data[oData.Index].HTML = "";
            oData.Grid.PersistedData.EditingArr[oData.ID] = null;
            if (oData.Index < 0) {
                oData.Grid.PersistedData.EditingArr[oData.ID] = "deleted";
                oData.Grid.PersistedData.OldColHTML = "";
            }
            oRequests.push(oData.Grid.buildRequest("update", "pk", oData.ID, "values", oData.Values));
            if(oData.Index>-1)oData.Grid.Data[oData.Index] = oData.Values;

            if (oData.Grid.raiseEvent("GridRowAfterSave", oData).Cancel) return true;
        } else
            bSuccess = false;        
    }
    
    if (oRequests.length > 0){
        oData.Grid.addRequest(oData.Grid.buildRequest("multi", "Requests", oRequests));
        //oData.Grid.render(null,true);
    }
    return bSuccess;
}

function fnJGridRowEdit(oCell){
    if (!$.isArray(oCell)) oCell = new Array(oCell);
    for (var x = 0; x < oCell.length; x++) {
        oData = fnJGridGetData(oCell[x]);
        oData.Grid.Data[oData.Index].HTML = "";

        if (oData.Grid.PersistedData.EditingArr[oData.ID] == null) {
            oData.Grid.PersistedData.EditingArr[oData.ID] = oData.Grid.Data[oData.Index].slice();
        } else {
            oData.Grid.PersistedData.EditingArr[oData.ID] = null;
        }
    }
    oData.Grid.render(null,true);
}

function fnJGridShowLinkedGrid(oCell,iGridIndex){
    var oData=fnJGridGetData(oCell);
    var iPK = oData.ID;var oLinkedGrid = oData.Grid.PersistedData.LinkedGrid;
    oData.Grid.Data[oData.Index].HTML = "";
    
    if (iGridIndex >= oData.Grid.MetaData.LinkedGridIDs.split(",").length) return;
    var sGridID = oData.Grid.MetaData.LinkedGridIDs.split(",")[iGridIndex];
    if (oLinkedGrid[iPK] == null) oLinkedGrid[iPK] = {};
    if (oLinkedGrid[iPK]["show_" + iGridIndex] == 1) {
        oLinkedGrid[iPK]["show_" + iGridIndex] = 0;
    } else {
        oLinkedGrid[iPK]["show_" + iGridIndex] = 1;
        if (oLinkedGrid[iPK]["html_" + iGridIndex] == null) {
            oLinkedGrid[iPK]["html_" + iGridIndex] = $("#"+sGridID)[0].value.split("id='").join("id='_" + iGridIndex + "_" + iPK + "_");
            oLinkedGrid[iPK]["grid_" + iGridIndex] = new clsJGrid("_" + iGridIndex + "_" + iPK + "_" + sGridID);
        }
    }
    oData.Grid.render(null,true);
}

function fnJGridRowDel(oCell){
    var oData=fnJGridGetData(oCell);
    if(window.confirm(oData.Grid.MetaData.DeleteConfirmation)){
        oData.Grid.Data[oData.Index].HTML="";
        oData.Grid.DeletedArr.push(oData.ID);
        oData.Grid.addRequest(oData.Grid.buildRequest("delete", "pk", oData.ID));
        oData.Grid.PersistedData.Selections.pop(oData.ID);
        oData.Grid.render();
        oData.Grid.raiseEvent("RowDeleted", oData);
    }
}
function fnJGridBuildRequest(sAction){
    var oRequest={"Action":sAction};
    if (fnJGridBuildRequest.arguments)
        for(var x=1;x<fnJGridBuildRequest.arguments.length;x=x+2) oRequest[fnJGridBuildRequest.arguments[x]] = fnJGridBuildRequest.arguments[x + 1];

    if (this.raiseEvent("GridRequestCreated",oRequest).Cancel) return;
    var o=new Date();
    oRequest._time=cStr(o.getHours()).lpad("0",2)+":"+cStr(o.getMinutes()).lpad("0",2)+":"+cStr(o.getSeconds()).lpad("0",2)+"."+cStr(o.getMilliseconds()).lpad("0",3);         
    return oRequest;
}

function fnRegisterEventListener(sEvent, oListener){
    this.EventHandler.push(oListener);
    this.EventType.push(sEvent);
    return this;
}

function fnRaiseEvent(sEvent,oData){
    var oObj={"Event":sEvent, "Grid":this, "Counter":0, "Data":oData, "Cancel": false};                
    for(var x=0;x<this.EventHandler.length;x++){
        if(this.EventType[x]==sEvent){        
            this.EventHandler[x](oObj);                
            oObj.Counter++;
        }
    }
    return oObj;
}

function fnJGridGetData(oElement){
    var oObj={Element:$(oElement)};    
    if (oElement.id == "JGridColView") {
        oObj.Row=oElement;
        oObj.Grid = $.data(document.body, "grid");
    } else {
        oObj.Cell = oObj.Element.closest("td,th")[0];
        oObj.Row = oObj.Element.closest("tr")[0];
        oObj.Grid = oObj.Element.closest("table.jgrid")[0].JGrid;
    }
    oObj.Index = cInt(oObj.Row.getAttribute("index"));
    oObj.ID = oObj.Row.getAttribute("pk_id");
    oObj.dataIndex = function() { return cInt(oObj.Cell.getAttribute("index")); }
    oObj.Element=oObj.Element[0];
    return oObj;
}

function fnJGridSelectRow(event,oElement){
    var oData=fnJGridGetData(oElement);
    var i=(event && event.shiftKey&&oData.Grid.PersistedData.LastSelectionID?oData.Grid.getPKIndex(oData.Grid.PersistedData.LastSelectionID):-1);    
    for (var x = Math.min(i == -1 ? 9999 : i, oData.Index); x <= Math.max(i, oData.Index); x++) {
        var sPK = oData.Grid.getData(x, oData.Grid.MetaData.PKCol);
        var j = $.inArray(sPK, oData.Grid.PersistedData.Selections);
        if (oElement.checked && oData.Grid.PersistedData.SelectionMode == 0 || !oElement.checked && oData.Grid.PersistedData.SelectionMode == 1) {
            if (j == -1) oData.Grid.PersistedData.Selections.push(sPK);
        } else if (j > -1) oData.Grid.PersistedData.Selections.remove(j, j);

        var oRow = $(oData.Grid.Table).find(">*>tr[index='" + x + "']");
        oRow.toggleClass("selected", oElement.checked);
        if (oData.Index != x) oRow.find("td[index='" + oData.dataIndex() + "'] input[type='checkbox']").prop("checked", oElement.checked);
        oData.Grid.Data[x].HTML = "";
    }
    oData.Grid.PersistedData.SelectionCount = (oData.Grid.PersistedData.SelectionMode == 0 ? oData.Grid.PersistedData.Selections.length : oData.Grid.MetaData.NumRows - oData.Grid.PersistedData.Selections.length);
    oData.Grid.PersistedData.LastSelectionID = oData.ID;
    oData.Grid.raiseEvent("RowSelectionChange", oData);
    if(event)event.cancelBubble=true;
    return true;
}

function fnJGridRefreshSelect(sID){
    if (this == null || this.Table == null) return $("#"+sID)[0].JGrid.refreshSelect();
    try{          
        var oArr=$(this.Table).find("input[type='checkbox']");  
          
        for(var x=0;x<oArr.length;x++){
            var iID=this.Data[oArr[x].up("tr").getAttribute("index")][this.MetaData.PKCol];
            oArr[x].checked = (oData.Grid.PersistedData.SelectionMode == 0 && $.inArray(iID,this.PersistedData.Selections)>-1) || (oData.Grid.PersistedData.SelectionMode == 1 && $.inArray(iID,this.PersistedData.Selections)==-1);
        }
    }catch(e){}
}

function fnJGridSetSelection(iMode){
    //iMode: 0=None, 1=Visible, 2=All
    this.PersistedData.Selections=[];
    this.PersistedData.SelectionMode=0;
    if(iMode==1){
        for(var x=0;x<this.Data.length;x++)
            if ($.inArray(this.Data[x][this.MetaData.PKCol],this.PersistedData.Selections) == -1)
                this.PersistedData.Selections.push(this.Data[x][this.MetaData.PKCol]);

    }else if(iMode==2){
        this.PersistedData.SelectionMode=1;
    }
    
    for(var x=0;x<this.Data.length;x++)this.Data[x].HTML="";
    var oData={};
    this.PersistedData.SelectionCount=(this.PersistedData.SelectionMode==0? this.PersistedData.Selections.length : this.MetaData.NumRows-this.PersistedData.Selections.length);
    this.raiseEvent("RowSelectionChange", oData);
    this.render(null,true);
}
function fnJGridGetSelection(fnCallback, oData, bCallback){
    var sIDs="";
    if(this.PersistedData.Selections==null)this.PersistedData.Selections=[];
    if(this.PersistedData.SelectionMode==1){
        if(bCallback){
            for(var x=0;x<this.PersistedData.Selections.length;x++)
                this.PersistedData.SelectionCallback.Data=this.PersistedData.SelectionCallback.Data.without(this.PersistedData.Selections[x]);
            
            sIDs=this.PersistedData.SelectionCallback.Data.join(",");
            this.PersistedData.SelectionCallback=null;
        }else{
            this.PersistedData.SelectionCallback={"Function":fnCallback, "AuxData":oData};                        
            this.addRequest(this.buildRequest("getselection"));                
            return;
        }
    }else{
        sIDs=this.PersistedData.Selections.join(",");    
    }    
    if(fnCallback!=null)fnCallback(this,sIDs,oData);
    return sIDs;
}

function fnJGridHandleKeyDown(event,iRowIndex){
    if (event.keyCode == 13 || event.which == 13){
        event.cancelBubble = true; //Event.stop(event);
        //if (iRowIndex == c_Filter)
        $(event.srcElement || event.target).trigger("change");        
        return false;
    }
}

function fnJGridUComboSetByID(event, oUL, sID, bSupressEvents) {
    var oItems = $(oUL).find("li > ul > li > a");
    for (var x = 0; x < oItems.length; x++) {
        if (cStr(oItems[x].getAttribute("value"), $(oItems[x]).attr("id"), $(oItems[x]).text()) == sID)
            return fnJGridUComboSet(event, oItems[x], bSupressEvents);
        
    }
    return false;
}

function fnJGridUComboSet(event, oEl, bSupressEvents) {
    var oUL=$(oEl).parents("ul:eq(1)");
    if(oUL.length==0)oUL=$(oEl).closest("ul");
    var oActive = $(oUL).find("a.active")[0];
    if (oActive == undefined) {
        $(oUL).prepend("<li><a href='javascript:void(0);' class='active'></a></li>");
        oActive = $(oUL).find("a.active")[0];
    }
    oActive.setAttribute("value",oEl.getAttribute("value"));
    oActive.className="active " + oEl.className;    
    $(oActive).html(oEl.innerHTML);
    oUL = oUL[0];
    if (oUL.getAttribute("autofilter") == 1) fnJGridSetFilter(event, oEl);
    if (oUL.getAttribute("onchange_set") == null) {
        $(oUL).bind("onchange", function(){eval(oUL.getAttribute("onchange"))});
        oUL.setAttribute("onchange_set", "1");
    }
    if(!bSupressEvents)$(oUL).trigger("onchange");
    if (event) event.cancelBubble = true; //Event.stop(event);
}
function fnJGridUComboGet(iBindDropdownByID, oCell) {
    return (oCell==null?"": ($(oCell).find("li a.active").length==0?"": (iBindDropdownByID == 1 ? $(oCell).find("li a.active")[0].getAttribute("value") : $(oCell).find("li a.active")[0].innerHTML)));
}
function fnJGridUComboLookup(oList, bMultiMatch, sID, iIDDimension, iDimension) {
    var oReturn = [];
    if (iIDDimension == null) iIDDimension = 0;
    if (oList == null) return (iDimension==null?(bMultiMatch?new Array([]):[]):"");
    for (var x = 0; x < oList.length; x++) {
        if (cStr(sID) == cStr(oList[x][iIDDimension])) {
            oReturn.push(iDimension == null ? oList[x] : oList[x][iDimension]);
            if (!bMultiMatch) return oReturn[0];
        }
    }
    if (oReturn.length == 0) return (iDimension == null ? (bMultiMatch ? new Array([]) : []) : "");
    return oReturn;
}
function fnJGridUComboRender(sID, iPK, oVals, iIDIndex, sValue, sListAtt, sUListAtt, iValIndex) {
    var iSelected = -1, sVals = "";
    if (iValIndex == null) iValIndex = 1;
    if (oVals != null)
        for (var z = 0; z < oVals.length; z++) {
            sVals += "<li><a href='javascript:void(0);' onclick='fnJGridUComboSet(event,this);' value='" + replaceAll(oVals[z][iIDIndex], "'", "&#39;") + "' " + oVals[z][2] + ">" + oVals[z][iValIndex] + "</a></li>";
            if (iSelected == -1 && sValue == oVals[z][iIDIndex]) iSelected = z;
        }
    return "<ul jid='"+sID+"' " + sListAtt + "><li><a href='javascript:void(0);' value='" + replaceAll((iSelected == -1 ? "" : oVals[iSelected][iIDIndex]), "'", "&#39;") + "' class='active'>" + (iSelected == -1 ? "" : oVals[iSelected][iValIndex]) + "</a><ul " + sUListAtt + ">" + sVals + "</ul></li></ul>";
}
function fnJGridConfigureView(iView) {
    if (iView=="")iView=0;
    var v=(iView==null?null:this.Views[iView]);
    if(iView==0)v.Filter=this.collectRow(c_Filter);
    this.PersistedData.EditingArr[c_Filter] = (v==null?[]: v.Filter);
    
    var oFrame = $.data(document.body, "grid-window");
    if(!oFrame) {
        oFrame = $('<div/>').appendTo($(document.body)).dialog({ modal: true });
        $.data(document.body, "grid-window",oFrame)
    }
    var Order = (iView == null ? [] : v.Cols);
    if (iView == null) 
    for (var x = 0; x < this.Cols.length; x++) {
            Order[x] = x;
            this.Cols[x].Type = this.Cols[x]._Type;
    }
    var t="";
    var o={"ViewMode":true, "Grid":this};        
    for (var x = 0; x < this.Cols.length; x++) {
        if (this.Cols[Order[x]].GroupTypes != "" && this.Cols[Order[x]].renderCell == null) {
            o = this.renderCell(o, c_Filter, Order[x], c_Filter);
            var u = "";
            $.each(this.Cols[Order[x]].GroupTypes.split(","), function (i, s) { u += "<option" + (v && v.Grouping[Order[x]].toLowerCase() == s.toLowerCase() ? " selected=selected" : "") + " value='"+s+"'>" + s + "</option>" });
            t += "<tr id='" + Order[x] + "' class='draggable " + (x % 2 == 0 ? "even" : "odd") + "'><td style='cursor:move'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span></td><td>" + (this.Cols[Order[x]].HText != "" ? this.Cols[Order[x]].HText : (this.Cols[Order[x]]._Type == "Select" || this.Cols[Order[x]]._Type == "Options" ? this.Cols[Order[x]]._Type : "")) + "</td><td style='text-align:center'>" + (this.Cols[Order[x]].GroupTypes.indexOf(",") == -1 ? "<input type='checkbox' class='show-options' name='options_" + Order[x] + "'" + (((!v || v.Grouping[Order[x]] == "Hide") && this.Cols[Order[x]].Type == "Hide") || (v && v.Grouping[Order[x]] == "Hide") ? "" : " checked='checked'") + ">" : "<select class='show-options' name='options_" + Order[x] + "'><option value='Hide'>Hide</option>" + u + "</select>") + "</td><td style='text-align:center'>" + (this.Cols[Order[x]].Sortable == 1 ? "<select onchange='var v=this;if($(v).val()!=\"\"){$(this).parents(\"table:eq(0)\").find(\"select.sort\").each(function(i,o){if(o!=v)o.selectedIndex=0;});}' class='sort' col-index='" + Order[x] + "'><option value=''></option><option value='0'" + ((iView == null ? this.Views[0].SortCol : this.Views[iView].SortCol) == Order[x] && (iView == null ? this.Views[0].SortOrder : this.Views[iView].SortOrder) == 0 ? " selected" : "") + ">Asc</option><option value='1'" + ((iView == null ? this.Views[0].SortCol : this.Views[iView].SortCol) == Order[x] && (iView == null ? this.Views[0].SortOrder : this.Views[iView].SortOrder) == 1 ? " selected" : "") + ">Desc</option></select>" : "") + "</td><td>" + (this.Cols[Order[x]]._Type == "Select" || this.Cols[Order[x]]._Type == "Options" ? "" : o.HTML) + "</div></td></tr>";
        }
    }    
    var l,s;
    $.each(this.Views,function(i,v){if(v&&i>1){l+="<option value="+i+(iView==i?" selected=selected":"")+" ro="+(v.ReadOnly?1:0)+">"+v.Name+"</option>";if(!v.ReadOnly)s+="<option value="+i+(iView==i?" selected=selected":"")+">"+v.Name+"</option>";}});
    oFrame.dialog({ position: [20, 20], height: $(window).height() - 100, width: "800px", buttons: { "Cancel": function () { $(this).dialog("close"); }, "Apply": function () { fnJGridSaveView(0, $(this).find("button")[0]); $(this).dialog("close"); } },
        open: function (ev, ui) { }, close: function (ev, ui) { }
    }).dialog("option", "title", "Configure View").dialog("open").html(
        "<span><h2>Load a view</h2><select onchange='$(this).siblings(\"button:not(:eq(3))\").attr(\"disabled\",this.value==\"\"?\"disabled\":\"\").eq(1).attr(\"disabled\",this.value==\"\"||$(this).find(\"option:selected\").attr(\"ro\")==\"1\")'>" + l + "</select> <button onclick=\"$.data(document.body,'grid').configureView($(this).siblings('select').val());\">Load</button> | <button type='button' onclick=\"var s=$(this).siblings('select').val();var g=$.data(document.body,'grid');if(s!=''){ g.Views[Number(s)]=null;g.reloadViewsList();$(this).parents('div:eq(0)').find('select option[value='+s+']').remove(); }$(this).parents('div:eq(0)').find('select').change(); \">Delete view</button> | <button type='button' class='r' onclick=\"$.data(document.body,'grid').configureView(null);\">Restore Default(s)</button></span><table index='" + c_Filter + "' style='border-bottom:#ccc 1px solid' class='" + $(this.Table).attr('class') + "' id='JGridColView'><thead><tr><th colspan='2'><a href='#'>Name</a></th><th><a href='#'>Show</a></th><th><a href='#'>Sort</a></th><th><a href='#'>Filter</a></th></tr></thead><tbody>" + t + "</tbody></table><h2>Save the view</h2><fieldset><p>Select a view to overwrite, or select (New View) and type name. Then click Save</p><span><select onchange='$(this).siblings(\"span\").toggle(this.value==\"\");'><option value=\"\" onchange='$(this).siblings(\"span\").toggle(this.value==\"\")'>(New View)</option>" + s + "</select> <span>Please enter name: <input type='text' /></span>"
        + "<button type='button' onclick='if(fnJGridSaveView(null,this))$.data(document.body,\"grid-window\").dialog(\"close\");'>Save and Apply</button></span></fieldset>"
        );

        o = oFrame.find("input.daterangepicker");
        if (o.length > 0 && o.DateRangePicker) o.DateRangePicker({ onOpen: function () { $.data(this.input, "date-val", this.input.value); }});
        o = oFrame.find("div.JCombo");
        if (o.length > 0 && o.JCombo) o.JCombo({ options: 'tag' });
        oFrame.find("select").change();
        oFrame.find("#JGridColView tbody").sortable({ items: 'tr.draggable' });
        oFrame.find("#JGridColView tbody").disableSelection();
        oFrame.find("#JGridColView input").click(function () { this.focus(); });    ;
    $.data(document.body, "grid",this);
}
function fnJGridLoadView(i,bReload){
    if (i == null||(""+i)=="") return;
    if (bReload == null) bReload = i > 0 && (this.Views[0].SortCol != this.Views[i].SortCol || this.Views[0].Filter.join("").split("\"\"").join("") != this.Views[i].Filter.join("").split("\"\"").join("") || this.Views[0].Grouping.join(",")!= this.Views[i].Grouping.join(","));    
    this.Views[0] = $.extend(true, {}, this.Views[i]);
    this.NumVisibleCols=null;    
    for (var x = 0; x < this.Data.length; x++) this.Data[x].HTML=null;    
    this.PersistedData.EditingArr[c_Filter] = this.Views[0].Filter;
    this.PersistedData.EditingArr[c_Filter].DisableCollect = true;
    this.refreshColVisibility();
    if (bReload) this.refresh();
    else this.render();
}
function fnJGridSaveView(iIndex,oEl) {
var g = $.data(document.body, "grid");
var o=$(oEl).parents("div:eq(0)");
var v = (iIndex == null ? null : g.Views[iIndex]);
var t;
if (v==null) {
    var s = $(oEl).parents("span:eq(0)").find("select").val();
    if (s == "") {
        s = $(oEl).parents("span:eq(0)").find("input").val();
        if (s == "") {
            alert("You need to provide a name for the view. No changes saved."); return false;
        };
        v = {"Name": s,"ReadOnly":0,"PageSize":g.Views.length==0?20:g.Views[0].PageSize};  g.Views.push(v); iIndex = g.Views.length-1;
    } else { iIndex = Number(s); v = g.Views[iIndex]; }
} else t = v.SortCol + v.SortOrder + v.Filter.join(",") + v.Cols.join(",") + v.Grouping.join(",");
var u=o.find("select.sort option:selected[value!='']");
if(u.length==0)u=o.find("select.sort option[value!='']");
v.SortCol=Number(u.parents("select").attr("col-index"));
v.SortOrder=u.val();
v.Grouping = [];
o.find("#JGridColView .show-options").each(function () { v.Grouping[Number(this.getAttribute("name").substring(8))] = (this.tagName.toLowerCase() == "select" ? $(this).val() : this.checked ? "Select" : "Hide"); });
v.Cols=o.find("#JGridColView tbody").sortable("toArray");
for (var x = 0; x < g.Cols.length; x++) if ($.inArray("" + x, v.Cols) == -1) v.Cols.push(x);
$.each(v.Cols, function (index, value) { v.Cols[index] = Number(value); v.Grouping[index] = v.Grouping[index]; });
v.Filter=g.collectRow(o.find("#JGridColView")[0]);
g.reloadViewsList();
g.loadView(iIndex, t != v.SortCol + v.SortOrder + v.Filter.join(",") + v.Cols.join(",") + v.Grouping.join(","));
return true;
}