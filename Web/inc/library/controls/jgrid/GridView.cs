﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using jlib.functions;
using System.Collections;

namespace jlib.controls.jgrid
{
	public class GridViews : List<GridView>
	{
		#region ***** ENUMS & CONSTANTS *****
		public const string CURRENT_VIEW_NAME = "__current";
        public const string DEFAULT_VIEW_NAME = "__default";

		#endregion

		#region ***** CONSTRUCTORS *****
		public GridViews(jGrid jGrid)
		{
			this.Grid = jGrid;
		}
		#endregion


		#region ***** PROPERTIES *****
		private jGrid Grid { get; set; }

		public new GridView this[int Index]
		{
			get
			{
				//If no items, then add a default item
				if(Index == 0 && base.Count == 0)
				{
					this.Add(new GridView(this.Grid, CURRENT_VIEW_NAME, true, -1, 0, 50));
				}
				return base[Index];
			}
			set
			{
				base[Index] = value;
			}
		}

		private GridView _SelectedView = null;
		public GridView SelectedView
		{
			get
			{
				//If Selected View already linked, return
				if (this._SelectedView != null && this._SelectedView.Selected)
					return this._SelectedView;

				//Set selected View
				this._SelectedView = this.Where(x => x.Selected).FirstOrDefault();

				//If still no view, use first one
				if(this._SelectedView == null)
					this._SelectedView = this[0];

				//Return view
				return this._SelectedView;
			}

			set
			{
				//Clear Current Selection
				this._SelectedView = null;
				this.ForEach(v => v.Selected = false);

				//If Selected Value == null
				if (value == null) return;

				//See if it exists in current collection
				GridView view = this.Where(v => v.Name.Equals(value.Name, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                if (view == null) {
                    this.Add(value);
                } else {
                    this.Insert(this.IndexOf(view), value);
                    this.Remove(view);
                }
				
				//Set Selected By Name
				this.setSelected(value.Name);
			}

		}
		#endregion


		#region ***** METHODS *****
		/// <summary>
		/// Parse Views from JSON Data
		/// </summary>
		/// <param name="JsonData"></param>
		/// <param name="ReplaceViews"></param>
		/// <returns></returns>
		public bool parse(jlib.functions.json.JsonData JsonData, bool ReplaceViews)
		{
			//If Replace, then clear current collection
			if (ReplaceViews)
				this.Clear();

			//Prase Json Data
			foreach (jlib.functions.json.JsonData data in JsonData)
			{
				if (data != null)
				{
					//Parse JSON Data
					GridView view = new GridView(this.Grid, convert.cStr(data["Name"]), convert.cBool(data["ReadOnly"]), convert.cInt(data["SortCol"]), convert.cInt(data["SortOrder"]), convert.cInt(data["PageSize"]));
					view.parse(data);

					//Add to collection
					base.Add(view);
				}
			}
			
			return true;
		}

		/// <summary>
		/// Parse Views From XML Data
		/// </summary>
		/// <param name="XmlData"></param>
		/// <param name="ReplaceViews"></param>
		/// <returns></returns>
		public bool parse(string XmlData, bool ReplaceViews)
		{
			if (XmlData != "")
			{
				//Get Current SortCol
				int sortCol = this.Grid.SortCol;

				//If Replace, then clear current collection
				if (ReplaceViews)
					this.Clear();

				try
				{
					//Load Xml
					XmlDocument oDoc = new XmlDocument();
					oDoc.LoadXml(XmlData);

					//Select Notes
					XmlNodeList xmlViews = oDoc.SelectNodes("//view");

					foreach (XmlNode xmlView in xmlViews)
					{
						//Create Grid
						GridView view = new GridView(this.Grid, xmlView);

						//If first view, then set SortCol
                        if (this.Count == 0 && sortCol>-1) view.SortCol = sortCol;

						//Add View
						this.Add(view);

					}

					return true;
				}
				catch (Exception) { return false; }

			}
			return true;
		}

		/// <summary>
		/// Set the Selected View By Name
		/// </summary>
		/// <param name="Name"></param>
		public void setSelected(string Name)
		{
			//Set all views as not selected
			this.ForEach(x => x.Selected = false);

			//Set Selected View
			GridView selectedView = this.Where(x => x.Name.Equals(Name, StringComparison.CurrentCultureIgnoreCase)).ToList().FirstOrDefault();
			if (selectedView != null)
			{
				this.Grid.DisableInitialFiltering = false;
				this.Grid.DisableInitialSorting = false;
				this.Grid.DisableInitialView = false;

				//Set View As Selected
				selectedView.Selected = true;

				//Set Local Pointer to this view
				this._SelectedView = selectedView;
			}

		}

		/// <summary>
		/// Serializes the Views to XML including Non-Persistent Data
		/// </summary>
		/// <returns>Xml Serialization</returns>
		public string serialize()
		{
			return serialize(true);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="IncludeNonPersistentData"></param>
		/// <returns></returns>
		public string serialize(bool IncludeNonPersistentData)
		{
			//Define Xml
			XmlDocument oDoc = new XmlDocument();
			oDoc.LoadXml("<views />");

			//Serialize Child Objects
			this.ForEach(view =>
				{
					oDoc.DocumentElement.AppendChild(oDoc.ImportNode(view.serialize().DocumentElement, true));
				});
				

			if (!IncludeNonPersistentData && this.Grid.DisableInitialFiltering) 
				xml.removeXmlNode(oDoc.SelectNodes("//view[1]/Col/@Filter | //view[@Name='" + GridViews.DEFAULT_VIEW_NAME + "']/Col/@Filter"));

			if (!IncludeNonPersistentData && this.Grid.DisableInitialSorting)
			{
                xml.removeXmlNode(oDoc.SelectNodes("//view[1]/@SortOrder | //view[@Name='" + GridViews.DEFAULT_VIEW_NAME + "']/@SortOrder"));
                xml.removeXmlNode(oDoc.SelectNodes("//view[1]/@SortCol  | //view[@Name='" + GridViews.DEFAULT_VIEW_NAME + "']/@SortCol"));
			}

			//SortOrder="1" SortCol="cID" 
			return oDoc.OuterXml;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string toJSON()
		{
			string json = "";

			this.ForEach(view => 
				{
					json += (json == "" ? "" : ",") + view.toJSON();
				});

			//Return JSON
			return json;
		}
		#endregion



		

		
		

		
		
	}

	public class GridView
	{
		#region ***** CONSTRUCTORS *****
		public GridView(jGrid jGrid, string sName, bool bReadOnly, int iSortCol, int iSortOrder, int iPageSize) : this()
		{
			this.Grid = jGrid;
			this.Name = sName;
			this.ReadOnly = bReadOnly;
			this.SortCol = iSortCol;
			this.SortOrder = iSortOrder;
			this.PageSize = iPageSize;
		}
		public GridView(jGrid jGrid, XmlNode oNode) : this()
		{
			this.Grid = jGrid;
			this.Name = xml.getXmlAttributeValue(oNode, "Name");
			this.PageSize = convert.cInt(xml.getXmlAttributeValue(oNode, "PageSize"), 50);
			this.ReadOnly = convert.cBool(xml.getXmlAttributeValue(oNode, "ReadOnly"));
			if (this.Grid.Views.Count > 0 && this.Grid.Views.SelectedView != null) this.SortCol = this.Grid.SortCol;
			this.SortOrder = convert.cInt(xml.getXmlAttributeValue(oNode, "SortOrder"));
			if (this.Grid.getCol(xml.getXmlAttributeValue(oNode, "SortCol")) != null && this.Grid.getCol(xml.getXmlAttributeValue(oNode, "SortCol")).SortFieldPrefixed != "") this.SortCol = jGrid.getCol(xml.getXmlAttributeValue(oNode, "SortCol")).Index;
			for (int x = 0; x < this.Grid.Columns.Count; x++)
			{
				XmlNode oNode1 = oNode.SelectSingleNode("Col[@ID='" + this.Grid.Columns[x].ID + "']");
				m_oGrouping[this.Grid.Columns[x].ID] = convert.cStr(xml.getXmlAttributeValue(oNode1, "Grouping"), this.Grid.Columns[x].ColType == "Hide" ? "Hide" : "Select");
				if (oNode1 != null) m_oFilter[this.Grid.Columns[x].ID] = xml.getXmlAttributeValue(oNode1, "Filter");
			}

			XmlNodeList xmlCols = oNode.SelectNodes("Col");
			for (int x = 0; x < xmlCols.Count; x++)
			{
				m_oCols.Add(xml.getXmlAttributeValue(xmlCols[x], "ID"));
			}
		}
		private GridView()
		{
			//Set Initial Values
			this.PageSize = 50;
			this._SortCol = -1;
			this.SortOrder = 0;
		}
		#endregion


		#region ***** PROPERTIES *****
		private jGrid Grid { get; set; }
		public string Name { get; set; }
		public int PageSize { get; set; }
		private int _SortCol = -1;
		public int SortCol { get { return this._SortCol; } set { this._SortCol = value; } }
		public int SortOrder { get; set; }


		
		public bool ReadOnly = false, Selected = false;
		private System.Collections.Generic.Dictionary<string, string> m_oFilter = new Dictionary<string, string>(), m_oGrouping = new Dictionary<string, string>();
		private System.Collections.Generic.List<string> m_oCols = new List<string>();
		#endregion



		#region ***** METHODS *****
        public int ColIndex(jlib.controls.jgrid.GridColumn Col) {
            if (Cols.Contains(Col.ID)) return Cols.IndexOf(Col.ID);
            return -1;
        }
		#endregion

        public int[] ColOrder {
            get {
                int[] iColOrder = new int[Cols.Count];
                for (int x = 0; x < iColOrder.Length; x++) {
                    if (this.Grid.getCol(Cols[x]) != null)
                        iColOrder[x] = this.Grid.getCol(Cols[x]).Index;
                }
                return iColOrder;
            }
        }

		public List<string> Cols
		{
			get
			{
				if (m_oCols.Count == 0)
					for (int x = 0; x < this.Grid.Columns.Count; x++) m_oCols.Add(this.Grid.Columns[x].ID);

				return m_oCols;
			}
            set {
                m_oCols = value;
            }
		}
		public Dictionary<string, string> Filter
		{
			get
			{
				return m_oFilter;
			}
			set
			{
				m_oFilter = value;
			}
		}
		public Dictionary<string, string> Grouping
		{
			get
			{
				if (m_oGrouping.Count == 0)
					for (int x = 0; x < this.Grid.Columns.Count; x++) m_oGrouping[this.Grid.Columns[x].ID] = (this.Grid.Columns[x].ColType == "Hide" ? "Hide" : "Select");
				return m_oGrouping;
			}
			set
			{
				m_oGrouping = value;
			}
		}



		
		public string getFilter(string sColID)
		{
			return m_oFilter.ContainsKey(sColID) ? m_oFilter[sColID] : "";
		}
		public void setFilter(string sColID, string sValue)
		{
			m_oFilter[sColID] = sValue;
		}
		public void resetFilter()
		{
			m_oFilter = new Dictionary<string, string>();
		}
		public void parse(jlib.functions.json.JsonData oData)
		{
			IList oCols = (oData["Cols"] as IList);
			IList oFilter = (oData["Filter"] as IList);
			IList oGrouping = (oData["Grouping"] as IList);
			m_oCols.Clear();
			for (int x = 0; x < oCols.Count; x++)
			{
				string sOrderID = (convert.cInt(oCols[x]) < this.Grid.MetaData.ColIDs.Length ? this.Grid.MetaData.ColIDs[convert.cInt(oCols[x])] : (this.Grid.getCol(convert.cInt(oCols[x])) == null ? "" : this.Grid.getCol(convert.cInt(oCols[x])).ID));
				string sID = (x < this.Grid.MetaData.ColIDs.Length ? this.Grid.MetaData.ColIDs[x] : (this.Grid.getCol(x) == null ? "" : this.Grid.getCol(x).ID));
				if (sID != "")
				{
					GridColumn oCol = this.Grid.getCol(sID);
					m_oCols.Add(sOrderID);
					setFilter(sID, (oFilter == null || oFilter.Count <= x ? "" : convert.cStr(oFilter[x])));
					m_oGrouping[sID] = (oGrouping == null || oGrouping.Count <= x ? (oCol != null && oCol.ColType == "Hide" ? "hide" : "select") : convert.cStr(oGrouping[x]));
				}
			}
		}

		private void recalculate()
		{
			for (int x = 0; x < this.Grid.Columns.Count; x++)
			{
				if (!m_oCols.Contains(this.Grid.Columns[x].ID)) m_oCols.Insert(this.Grid.OptionsCol == null || !m_oCols.Contains(this.Grid.OptionsCol.ID) ? m_oCols.Count : m_oCols.IndexOf(this.Grid.OptionsCol.ID), this.Grid.Columns[x].ID);
			}

			for (int x = 0; x < m_oCols.Count; x++)
			{
				if (this.Grid.getCol(m_oCols[x]) == null || x != m_oCols.LastIndexOf(m_oCols[x]))
				{
					m_oCols.RemoveAt(x);
					x--;
				}
			}

			if (this.Grid.getCol(SortCol) == null)
			{
				SortCol = this.Grid.MetaData.PKCol;
			}
			//m_oGrouping.Clear();
		}
		public string toJSON()
		{
			recalculate();
			string sRet = "{Name:" + jlib.functions.JSONConverter.cJSONSafeValue(Name, typeof(string)) + ",PageSize:" + PageSize + ",SortOrder:" + SortOrder + ",SortCol:" + SortCol + ",ReadOnly:" + (ReadOnly ? 1 : 0) + ",Selected:" + (Selected ? 1 : 0);
			string[] sFilter = new string[this.Grid.Columns.Count];
			string[] sGrouping = new string[this.Grid.Columns.Count];
			string[] sCols = new string[this.Grid.Columns.Count];
			for (int x = 0; x < this.Grid.Columns.Count; x++)
			{
				sCols[m_oCols.IndexOf(this.Grid.Columns[x].ID)] = x.ToString();
				sFilter[x] = m_oFilter.ContainsKey(this.Grid.Columns[x].ID) ? m_oFilter[this.Grid.Columns[x].ID] : "";
				sGrouping[x] = (m_oGrouping.ContainsKey(this.Grid.Columns[x].ID) ? m_oGrouping[this.Grid.Columns[x].ID] : (this.Grid.Columns[x].ColType == "Hide" ? "Hide" : "Select"));
			}
			sRet += ",Filter:" + JSONConverter.cJSONArray(sFilter, typeof(string)) + ",Grouping:['" + convert.cJoin("','", sGrouping) + "'],Cols:[" + convert.cJoin(",", sCols) + "]}";
			return sRet;
		}
		public XmlDocument serialize()
		{
			recalculate();
			XmlDocument oDoc = new XmlDocument();

			XmlNode oNode = xml.setXmlAttributeValue(xml.addXmlElement(oDoc, "view"), "Name", Name, "SortOrder", SortOrder, "SortCol", this.Grid.Columns[SortCol].ID, "ReadOnly", ReadOnly, "PageSize", PageSize);
			for (int x = 0; x < m_oCols.Count; x++)
				xml.setXmlAttributeValue(xml.addXmlElement(oNode, "Col"), "ID", m_oCols[x], "Filter", m_oFilter.ContainsKey(m_oCols[x]) ? m_oFilter[m_oCols[x]] : "", "Grouping", m_oGrouping.ContainsKey(m_oCols[x]) ? m_oGrouping[m_oCols[x]] : "");

			return oDoc;
		}
	}
}
