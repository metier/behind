﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.controls.jgrid
{
	public abstract class BaseGridRow : System.Web.UI.UserControl, IGridRow
	{

		#region ***** PROPERTIES *****
		public jGrid OwnerGrid { get; set; }
		#endregion

		#region ***** METHODS *****
		public virtual void OnGridDatabind(object sender, jlib.controls.jgrid.GridCellEventArgs e)
		{
		
		}
		public virtual void OnGridInit(object sender, jlib.controls.jgrid.GridCellEventArgs e)
		{
			this.OwnerGrid = sender as jGrid;
		}
		#endregion

		
	}
}
