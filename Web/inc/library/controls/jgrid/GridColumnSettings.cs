﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jlib.functions;
using jlib.db;

namespace jlib.controls.jgrid
{
	public class GridColumnSettings
	{


		#region ***** CONSTRUCTORS *****
		public GridColumnSettings(GridColumn Column) 
		{
			this.Column = Column;

			//Set Initial Values
			this.GroupTypes = "Select";
			this.Type = "Text";
			this.MultiValue = 0;
			this.Sortable = 0;

			this.HText= "";
			this.HAtt= ""; 
			this.CAtt= ""; 
			this.CInputAtt= ""; 
			this.FInputAtt= ""; 
			this.CText= ""; 
			this.CInputDefault= ""; 
			this.AddPrompt= ""; 
			this.ID= ""; 
			this.UListAtt= ""; 
			this.EditCols= ""; 
			this.FType= "";   
		}
		#endregion


		#region ***** PROPERTIES *****
		private GridColumn Column { get; set; }
		public string HText { get; set; }
		public string HAtt { get; set; } 
		public string CAtt { get; set; } 
		public string CInputAtt { get; set; } 
		public string FInputAtt { get; set; } 
        public string CText { get; set; } 
		public string CInputDefault { get; set; } 
		public string AddPrompt { get; set; } 
        public string GroupTypes { get; set; } 
        public string ID { get; set; } 
        public string UListAtt { get; set; } 
        public string EditCols { get; set; } 
		public int MultiValue { get; set; } 
		public string Type { get; set; } 
		public string FType { get; set; }            
        public int Sortable { get; set; } 
		#endregion

		///TODO: Finish Arranging
		private helpers.structures.collection_aux m_oCInputList; 
		private helpers.structures.collection_aux m_oFInputList;
		private bool m_bHasCDatabound = false;
		private bool m_bHasFDatabound = false;
        private int? m_iBindDropdownByID = null;
        
		#warning should be enumerated
		public int BindDropdownByID 
		{
            get 
			{
                if (m_iBindDropdownByID == null) return (this.Column.Grid.BindDropdownByID?1:0);
                return convert.cInt(m_iBindDropdownByID);
            }
            set 
			{
                m_iBindDropdownByID = value;
            }
        }
        public helpers.structures.collection_aux CInputValues
        {
            get 
			{

                if ((Type != "Combo" && FType != "Combo") && (Type != "UCombo" && FType != "UCombo")) return null;

                if (!m_bHasCDatabound) {
                    if (convert.cStr(this.Column.SQLDataSource) != "")
                        this.Column.setCInputValues(sqlbuilder.getDataTable(this.Column.SQLDataSource), this.Column.DataValueField, this.Column.DataTextField, this.Column.DataAuxField);

                    GridCellEventArgs e = new GridCellEventArgs(this.Column.Grid, this.Column, m_oCInputList, GridCellEventArgs.RowTypes.Data);
                    this.Column.Grid.OnGridDropdownDataLoaded(e);
                    m_oCInputList = e.Data;
                    m_bHasCDatabound = true;
                }
                if (m_oCInputList == null) return null;

                //string[] sTemp = new string[m_oCInputList.Count];
                //for (int x = 0; x < m_oCInputList.Count; x++)
                //    sTemp[x] = (m_oCol.Grid.UseComboID ? m_oCInputList.GetKey(x) + "|" : "") + convert.cStr(m_oCInputList[x]);

                return m_oCInputList;
            }
        }

        public helpers.structures.collection_aux FInputValues
		{            
            get 
			{

                if ((Type != "Combo" && FType != "Combo") && (Type != "UCombo" && FType != "UCombo")) return null;

                if (!m_bHasFDatabound) {
                    if (convert.cStr(this.Column.FSQLDataSource) != "")
                        this.Column.setFInputValues(sqlbuilder.getDataTable(this.Column.FSQLDataSource), this.Column.DataValueField, this.Column.DataTextField, this.Column.DataAuxField );


                    if (m_oFInputList == null) m_oFInputList = new helpers.structures.collection_aux();
                    GridCellEventArgs e = new GridCellEventArgs(this.Column.Grid, this.Column, m_oFInputList, GridCellEventArgs.RowTypes.Filter);

                    this.Column.Grid.OnGridDropdownDataLoaded(e);
                    m_oFInputList = (e.Data.Count == 0 ? null : e.Data);

                    m_bHasFDatabound = true;
                }
                if (m_oFInputList == null) return null;

                //string[] sTemp = new string[m_oFInputList.Count];
                //for (int x = 0; x < m_oFInputList.Count; x++)
                //    sTemp[x] = (m_oCol.Grid.UseComboID ? m_oCInputList.GetKey(x) + "|" : "") + convert.cStr(m_oFInputList[x]);

                return m_oFInputList;
            }
        }


		#region ***** METHODS *****
		public void setCInputValues(helpers.structures.collection_aux oList) 
		{
			m_oCInputList = oList;
		}
        public void setFInputValues(helpers.structures.collection_aux oList) 
		{
			m_oFInputList = oList;
		}
        public helpers.structures.collection_aux getCInputValues() 
		{
			if (m_oCInputList == null) 
			{
                helpers.structures.collection_aux a = CInputValues;
			}
			return m_oCInputList;
		}
		public string getCInputKey(object InputValue) 
		{
            if (this.Column.ColType == "JCombo" && (this.BindDropdownByID==1) && convert.cStr(InputValue).IndexOf("\n")>-1) 
				return parse.split(InputValue, "\n")[0];
			
			if (getCInputValues() == null) 
					return convert.cStr(InputValue);

			//else
			return convert.cStr(m_oCInputList.GetKey(InputValue) ?? InputValue);
		}
		#endregion
		
	}
}
