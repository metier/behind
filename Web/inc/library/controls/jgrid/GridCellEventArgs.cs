﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using jlib.functions;
using jlib.db;

namespace jlib.controls.jgrid
{
	public class GridCellEventArgs : EventArgs
	{
		#region ***** ENUMS *****
		
		public enum RowTypes
		{
			Add,
			Filter,
			Data
		}
		#endregion


		#region ***** CONSTRUCTORS ******
		public GridCellEventArgs(jGrid oGrid, GridColumn oColumn, helpers.structures.collection_aux oData, RowTypes oRowType)
			: this(oGrid, null, null, -1, null, -1)
		{
			m_oData = oData;
			this.Column = oColumn;
			m_oRowType = oRowType;
		}

		public GridCellEventArgs(jGrid oGrid, helpers.structures.collection_aux Filter)
			: this(oGrid, null, null, -1, null, -1)
		{
			this.Filter = Filter;
		}

		public GridCellEventArgs(jGrid oGrid, string sTagStart, string sTagEnd, string sHTML, int iPK, IDataRow oDBRow, int iRowIndex, GridColumn Column)
			: this(oGrid, null, null, iPK, oDBRow, iRowIndex)
		{
			this.HTMLTagStart = sTagStart;
			this.HTMLValue = sHTML;
			this.HTMLTagEnd = sTagEnd;
			this.Column = Column;
		}

		public GridCellEventArgs(jGrid oGrid, string[] sDataValues, string[] sTextValues, int iPK) : this(oGrid, sDataValues, sTextValues, iPK, null, -1) 
		{
		}

		public GridCellEventArgs(jGrid oGrid, string[] sDataValues, string[] sTextValues, int iPK, IDataRow oDBRow, int iRowIndex, SpreadsheetGear.IWorkbook oExcelFile, int iExcelSheetIndex, int iExcelRowCounter)
			: this(oGrid, sDataValues, sTextValues, iPK, oDBRow, iRowIndex)
		{
			m_oExcelFile = oExcelFile;
			m_iExcelRowCounter = iExcelRowCounter;
			m_iExcelSheetIndex = iExcelSheetIndex;
		}
		public GridCellEventArgs(jGrid Grid, string[] DataValues, string[] TextValues, int PK, IDataRow DBRow, int RowIndex)
		{
			this.Grid = Grid;
			m_sDataValues = DataValues;
			m_sTextValues = TextValues;
			m_iPK = PK;
			this.DBRow = DBRow;
			m_iRowIndex = RowIndex;
			if (DBRow == null && RowIndex >= 0 && RowIndex < Grid.GetDataSourceRowCount()) DBRow = Grid.GetDataSourceRow(RowIndex);
            this.Return = true;
		}

		#endregion


		#region ***** PROPERTIES *****
		public helpers.structures.collection_aux Filter { get; set; }
		public string HTMLValue { get; set; }
		public string HTMLTagStart { get; set; }
		public string HTMLTagEnd { get; set; }
		public GridColumn Column { get; set; }
		//public bool Success { get; set; }
		public jGrid Grid { get; private set; }
		public IDataRow DBRow { get; set; }

		#endregion






		private string[] m_sTextValues, m_sDataValues = null;
		//private DataRow m_oDBDataRow = null;

		private int m_iPK = -1, m_iRowIndex = -1, m_iExcelRowCounter = -1, m_iExcelSheetIndex = -1, m_iIndex = -1;
		private string m_sReturnValue = "", m_sType = "", m_sArgument = "", m_sClientJSBefore = "", m_sClientJSAfter = "";
		private jlib.functions.json.JsonData m_oJSONData, m_oJSONObject;
		
		
		private helpers.structures.collection_aux m_oData;
		
		private bool m_bReturn = true, m_bRefreshMetaData = false, m_bRefreshColData = false, m_bRefreshData = false;
		private RowTypes m_oRowType = RowTypes.Data;
		
		



		private SpreadsheetGear.IWorkbook m_oExcelFile;

		

		

		
		
		private string[] TextValues
		{
			get
			{
				return m_sTextValues;
			}
			set
			{
				m_sTextValues = value;
			}
		}
		public string[] getTextValues()
		{
			return TextValues;
		}

		private string[] DataValues
		{
			get
			{
				if (m_sDataValues == null)
				{
					if (TextValues == null) return null;
					m_sDataValues = new string[TextValues.Length];
					for (int x = 0; x < Math.Min(m_sDataValues.Length, Grid.Columns.Count); x++)
						m_sDataValues[x] = Grid.Columns[x].ColData.getCInputKey(TextValues[x]);
				}
				return m_sDataValues;
			}
		}
		public int setRowValues(DataRow oRow)
		{
			int iCounter = 0;
			for (int x = 0; x < DataValues.Length; x++)
			{
				if (oRow.Table.Columns.Contains(convert.cStr(Grid.Columns[x].DataMember)))
				{
					ado_helper.setFieldValue(oRow, Grid.Columns[x].DataMember, DataValues[x]);
					iCounter++;
				}
			}
			return iCounter;
		}
		public string getColDataValue(GridColumn oCol)
		{
			if (!this.Grid.Columns.Contains(oCol)) throw new Exception("JGrid: Invalid Col specified. Index " + oCol.Index + ", ID " + oCol.ID + " was not found.");
			return DataValues.SafeGetValue(oCol.Index);
		}
		public string getColValue(GridColumn oCol)
		{
			if (!this.Grid.Columns.Contains(oCol)) throw new Exception("JGrid: Invalid Col specified. Index " + oCol.Index + ", ID " + oCol.ID + " was not found.");
			return TextValues.SafeGetValue(oCol.Index);
		}

		public string getColDataValue(string sColID)
		{
			GridColumn oCol = Grid.getCol(sColID);
			if (oCol != null) return DataValues.SafeGetValue(oCol.Index);
			throw new Exception("JGrid: Invalid ColID specified. ID " + sColID + " was not found, so value could not be read.");
		}
		public string getColValue(string sColID)
		{
			GridColumn oCol = Grid.getCol(sColID);
			if (oCol != null) return TextValues.SafeGetValue(oCol.Index);
			throw new Exception("JGrid: Invalid ColID specified. ID " + sColID + " was not found, so value could not be read.");
		}
		public bool setColValue(GridColumn oCol, string sValue)
		{
			if (!Grid.Columns.Contains(oCol)) throw new Exception("JGrid: Invalid ColIndex specified. Index " + oCol.Index + ", ID " + oCol.ID + " was not found, so value '" + sValue + "' could not be set.");            
			TextValues[oCol.Index] = sValue;
			DataValues[oCol.Index] = sValue;
			return true;
		}
		//public bool setColDataValue(col oCol, string sValue) {
		//    if (!Grid.Columns.Contains(oCol)) throw new Exception("JGrid: Invalid ColIndex specified. Index " + oCol.Index + ", ID " + oCol.ID + " was not found, so value '" + sValue + "' could not be set.");
		//    DataValues[oCol.Index] = sValue;
		//    return true;
		//}
		public bool setColValue(string sColID, string sValue)
		{
			GridColumn oCol = Grid.getCol(sColID);
			if (oCol != null)
			{
				TextValues[oCol.Index] = sValue;
				return true;
			}
			//throw new Exception("JGrid: Invalid ColID specified. ID " + sColID + " was not found, so value '" + sValue + "' could not be set.");         
			return false;
		}
		

		public int RowIndex
		{
			get
			{
				return m_iRowIndex;
			}
			set
			{
				m_iRowIndex = value;
			}
		}

		public int ExcelRowCounter
		{
			get
			{
				return m_iExcelRowCounter;
			}
			set
			{
				m_iExcelRowCounter = value;
			}
		}
		public int ExcelSheetIndex
		{
			get
			{
				return m_iExcelSheetIndex;
			}
		}
		public SpreadsheetGear.IWorkbook ExcelFile
		{
			get
			{
				return m_oExcelFile;
			}
		}

		public RowTypes RowType
		{
			get
			{
				return m_oRowType;
			}
			set
			{
				m_oRowType = value;
			}
		}

		public int Index
		{
			get
			{
				if (m_iIndex < 0 && Column != null) return Column.Index;
				return m_iIndex;
			}
			set
			{
				m_iIndex = value;
			}
		}

		

		public bool RefreshMetaData
		{
			get
			{
				return m_bRefreshMetaData;
			}
			set
			{
				m_bRefreshMetaData = value;
			}
		}
		public bool RefreshData
		{
			get
			{
				return m_bRefreshData;
			}
			set
			{
				m_bRefreshData = value;
			}
		}
		public bool RefreshColData
		{
			get
			{
				return m_bRefreshColData;
			}
			set
			{
				m_bRefreshColData = value;
			}
		}

		public bool Return
		{
			get
			{
				return m_bReturn;
			}
			set
			{
				m_bReturn = value;
			}
		}


		public helpers.structures.collection_aux Data
		{
			get
			{
				if (m_oData == null) m_oData = new helpers.structures.collection_aux();
				return m_oData;
			}
			set
			{
				m_oData = value;
			}
		}

		

		public int PK
		{
			get
			{
				return m_iPK;
			}
			set
			{
				m_iPK = value;
			}
		}

		public string ReturnValue
		{
			set
			{
				m_sReturnValue = value;
			}
			get
			{
				return m_sReturnValue;
			}
		}

		public string ClientJSAfter
		{
			set
			{
				m_sClientJSAfter = value;
			}
			get
			{
				return m_sClientJSAfter;
			}
		}
		public string ClientJSBefore
		{
			set
			{
				m_sClientJSBefore = value;
			}
			get
			{
				return m_sClientJSBefore;
			}
		}

		public string Type
		{
			set
			{
				m_sType = value;
			}
			get
			{
				return m_sType;
			}
		}

		public string Argument
		{
			set
			{
				m_sArgument = value;
			}
			get
			{
				return m_sArgument;
			}
		}

		public jlib.functions.json.JsonData JSONData
		{
			set
			{
				m_oJSONData = value;
			}
			get
			{
				return m_oJSONData;
			}
		}

		public jlib.functions.json.JsonData JSONObject
		{
			set
			{
				m_oJSONObject = value;
			}
			get
			{
				return m_oJSONObject;
			}
		}
	}
}
