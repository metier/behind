﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpreadsheetGear;
using jlib.db;
using System.Data;
using jlib.functions;
using System.ComponentModel;
using jlib.DataFramework;
using jlib.DataFramework.QueryBuilder;

namespace jlib.controls.jgrid
{
	public class jGridData : jGrid
	{
		#region ***** CONSTRUCTORS *****
		public jGridData() : base()
		{
			
		}
		#endregion

		#region ***** PROPERTIES *****
		private jlib.DataFramework.ICollection _DataSource = null;
		public new jlib.DataFramework.ICollection DataSource
		{
			get
			{
				if (this._DataSource == null) this._DataSource = this.GetDataSource() as ICollection;

				return this._DataSource;
			}
			set { this.SetDataSource(value); }
		}
		public jlib.DataFramework.IQueryPackage QuerySource { get; set; }
		#endregion

		#region ***** METHODS *****
		public override List<string> GetDataSourceColumns()
		{
			List<string> columns = new List<string>();
			foreach(IColumn column in this.DataSource.Columns.Values)
				columns.Add(column.FieldName);

			//Return List of Columns
			return columns;
		}
		public override IDataRow GetDataSourceRow(int RowIndex)
		{
			return (IDataRow)this.DataSource[RowIndex];
		}

		public override bool DoesDataSourceColumnExist(string ColumnName)
		{
            //Need to also check the .ToUpper() version, since Oracle upper-cases column names
            return this.DataSource.Columns.SafeGetValue(ColumnName, ColumnName.ToUpper()) != null;
		}
		public override int GetDataSourceRowCount()
		{
			return this.DataSource.Count;
		}

		protected internal BaseCondition GetFilterCondition(GridColumn Column, string FilterValue)
		{
			//Get criteria
			string criteria = Column.ColData.getCInputKey(FilterValue);

			//Create Column
			BaseCondition condition = null;

			//Get Column Name
			string columnName = Column.DataMember;
			if (!String.IsNullOrWhiteSpace(Column.FField)) columnName = Column.FField;

			//Get IColumn
			IColumn dataColumn = null;
			if (this.QuerySource.Columns.ContainsKey(columnName))
				dataColumn = this.QuerySource.Columns[columnName];

			//If no data column, then must be virtual
			if (dataColumn == null)
			{
				if(columnName.ToLower().IndexOf("date") != -1)
					dataColumn = new BaseColumn<string>(columnName, columnName, "@" + columnName, DbType.DateTime, false);
				else
					dataColumn = new BaseColumn<string>(columnName, columnName, "@" + columnName, DbType.String, false);
			}

			//If DateRangePicker
			if((Column.ColData.CInputAtt.Contains("daterangepicker") || Column.ColData.FInputAtt.Contains("daterangepicker"))  && criteria.Contains("-"))
				condition = new BetweenCondition(dataColumn,  convert.cDate(parse.splitValue(criteria, "-", 0), true).Date,  convert.cDate(parse.splitValue(criteria, "-", 1), true).Date);

			//Else, If JCombo-Multi
			else if (Column.ColType == "JCombo" && Column.ColData.CInputAtt.IndexOf("multi='true'") > -1 && criteria.IndexOf("|") > -1)
			{
				throw new NotImplementedException();
			}

			//Else, If Empty String ('')
			else if (criteria == "''")
				condition = new EqualCondition(dataColumn, ""); 			
			
			//Else, not like
			else if (criteria.StartsWith("<>%") || criteria.StartsWith("><%"))
			{
				string value = FilterValue.Substring(3);
				if (value == "''") value = "";

				condition = new NotLikeCondition(dataColumn, LikeConditionTypes.Contains, value);
			}

			//Else, if !=
			else if (criteria.StartsWith("<>") || criteria.StartsWith("><"))
			{
				string value = FilterValue.Substring(2);
				if (value == "''") value = "";

				condition = new NotEqualCondition(dataColumn, value);
			}

			//Else, if <=
			else if (criteria.StartsWith("<="))
				condition = new LessOrEqualThanCondition(dataColumn, FilterValue.Substring(2));

			//Else, if <
			else if (criteria.StartsWith("<"))
				condition = new LessThanCondition(dataColumn, FilterValue.Substring(1));

			//Else, if >=
			else if (criteria.StartsWith(">="))
				condition = new GreaterOrEqualThanCondition(dataColumn, FilterValue.Substring(2));

			//Else, if >
			else if (criteria.StartsWith(">"))
				condition = new GreaterThanCondition(dataColumn, FilterValue.Substring(1));

			//Else, if like
			else if (criteria.IndexOf("%") != -1)
				condition = new LikeCondition(dataColumn, LikeConditionTypes.Custom, FilterValue);

			//Else, if like
			else if (Column.FPartialMatch)
				condition = new LikeCondition(dataColumn, (FilterValue.StartsWith("'") ? LikeConditionTypes.StartsWith : LikeConditionTypes.Contains), FilterValue.Substring(FilterValue.StartsWith("'") ?1:0));

			//Else, equal (=)
			else
				condition = new EqualCondition(dataColumn, FilterValue);


			//Return Condition
			return condition;

		}

		protected internal override IListSource GetDataSource()
		{
            OnGridBeforeDatabind(new GridCellEventArgs(this, null));
			//Use Query Source to retrieve data
			if (this._DataSource == null && this.QuerySource != null)
			{
				//Get Filters
				helpers.structures.collection_aux filters = new helpers.structures.collection_aux();
				this.Views.SelectedView.Filter.ToList().ForEach(x => filters.Add(x.Key, x.Value));
				
				//Throw Event
				GridCellEventArgs oEvent = new GridCellEventArgs(this, filters);
				this.OnGridBeforeFilter(oEvent);


				//Get Query Builder
				QueryBuilder queryBuilder = this.QuerySource.ToQueryBuilder();

                //Get Sort Order
                string sortPropertyName = this.Columns[this.SortCol == -1 ? 0 : this.SortCol].SortField.Str();                

                if (sortPropertyName.Str() != "" && sortPropertyName != "#") 
				{
					queryBuilder.ClearOrderBy();
                    var sortProperties = sortPropertyName.Split(',').ToList();
                    sortProperties.ForEach(SortProperty => {
                        SortProperty = SortProperty.Trim();
                        if (SortProperty.IsNotNullOrEmpty())
                        {
                            IColumn orderByColumn;

                            //Get column from table collection
                            if (this.QuerySource.Columns.ContainsKey(SortProperty))
                                orderByColumn = this.QuerySource.Columns[SortProperty];

                            //virtual column
                            else orderByColumn = new BaseColumn<string>(SortProperty, SortProperty, "@" + SortProperty, DbType.String, false, true);

                            //Add to sort order
                            queryBuilder.AddOrderBy(new OrderBy(orderByColumn, this.SortOrder == "desc" ? OrderByDirections.Descending : OrderByDirections.Ascending));
                        }
                    });                    
                }
			
				//Paging
				if(this.CurrentPage >= 0 && this.PageSize >= 0)
				{
					queryBuilder.PagingOptions = new DataFramework.Paging.PagingOptions()
						{
							PageIndex = this.CurrentPage,
							PageSize = this.PageSize
						};
				}


				//Process Each Filter
				foreach (string key in filters.Keys)
				{
					//filter
					string filter = (string)filters[key];

					if (!String.IsNullOrWhiteSpace(filter))
					{
						//Get Column
						GridColumn gridColumn = this.Columns.FirstOrDefault(x => x.ID.Equals(key, StringComparison.CurrentCultureIgnoreCase));

						//If no grid column
						if(gridColumn == null) continue;

						//Get Condition
						BaseCondition condition = this.GetFilterCondition(gridColumn, filter);

						//Add Condition
						queryBuilder.Conditions.Add(condition);

					}
				}

				//Execute
				this.DataSource = queryBuilder.ToQueryPackage().Execute();
			}

			return this._DataSource;
		}

		protected internal override void SetDataSource(IListSource value)
		{
			//Set Base
			this._DataSource = value as jlib.DataFramework.ICollection;

			//If Set
            if (value != null) 
			{

				this.MetaData.NumRows = (this.DataSource.PagingDetails == null) ? this.DataSource.Count : this.DataSource.PagingDetails.TotalRows;
				OnGridDatabind(new GridCellEventArgs(this, null));
            }
		}
		#endregion




	}
}
