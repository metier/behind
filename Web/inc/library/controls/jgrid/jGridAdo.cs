﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpreadsheetGear;
using jlib.db;
using System.Data;
using jlib.functions;
using System.ComponentModel;

namespace jlib.controls.jgrid
{
	public class jGridAdo : jGrid
	{
		#region ***** CONSTRUCTORS *****
		public jGridAdo() : base()
		{
			this.DataSourceType = sqlbuilder.DataServerTypes.SQLServer;
		}
		#endregion

		#region ***** PROPERTIES *****
		public string Fields { get; private set; }
		public string Filter { get; private set; }
		public string Tables { get; private set; }
		public string Grouping { get; private set; }
		public string SQLWhere { get; private set; }
		public JDataTable InternalDataSource { get; set; }
		public sqlbuilder.DataServerTypes DataSourceType { get; set; }

		private ado_helper _DataHelper = null;
		public ado_helper DataHelper 
		{
			get 
			{
				if (_DataHelper == null && this.DataSourceType == sqlbuilder.DataServerTypes.SQLServer) 
					this._DataHelper = new ado_helper();

				return _DataHelper;
			}
			set 
			{
				this._DataHelper = value;

				if(value!=null)
					DataSourceType = (value.ConnectionType== ado_helper.connection_type.sql ? sqlbuilder.DataServerTypes.SQLServer : sqlbuilder.DataServerTypes.Oracle);
			}
		}

		public new JDataTable DataSource	
		{ 
			get
			{
				return this.GetDataSource() as JDataTable;
			}
			set
			{
				this.SetDataSource(value);
			}
		}

		

		public string SQLFilter
		{
			get
			{
				if (this.SQLWhere == null)
				{
					helpers.structures.collection_aux oFilter = new helpers.structures.collection_aux();

					//JVT change 2009-03-05: Moved OnGridBeforeFilter before the this.MetaData.Filter processing!
					GridCellEventArgs oEvent = new GridCellEventArgs(this, oFilter);
					this.OnGridBeforeFilter(oEvent);

					foreach (KeyValuePair<string, string> oEntry in this.Views[0].Filter)
					{
						if (!String.IsNullOrWhiteSpace(oEntry.Value.Trim()))
						{
							GridColumn oCol = getCol(oEntry.Key);
							if (oCol != null && !String.IsNullOrWhiteSpace(oCol.FField))
							{
								Type oDataType = null;
                                if (oCol.FColType == "Textarea" || (oCol.FColType.IsNullOrEmpty() && oCol.ColType == "Textarea")) 
                                    oCol.MultiValueSeparator = "\n";
                                
								string[] sFilters = (String.IsNullOrEmpty(oCol.MultiValueSeparator) || !oEntry.Value.Contains(oCol.MultiValueSeparator) ? new string[] { oEntry.Value } : parse.split(oEntry.Value, oCol.MultiValueSeparator));
								string sFilter = "";
								for (int x = 0; x < sFilters.Length; x++)
								{
									if (!String.IsNullOrWhiteSpace(parse.stripWhiteChars(sFilters[x]))) sFilter += getCondition(oCol, parse.stripWhiteChars(sFilters[x]), out oDataType).toSQLString(this.DataSourceType, oDataType, (typeof(DateTime) == oDataType ? this.DateCastFunction : "")) + " OR ";
								}
								if (!String.IsNullOrWhiteSpace(sFilter)) oFilter.Add(oCol.ID, new condition(parse.stripEndingCharacter(sFilter, " OR ")));
							}
						}
					}

					this.OnGridAfterFilter(oEvent);
					for (int x = 0; x < oEvent.Filter.Count; x++)
					{
						//sSQL += " and " + this.Columns[x].SortField + " like '" + ado_helper.PrepareDB(this.MetaData.Filter[x]) + (this.Columns[x].FColType == "Combo" || this.Columns[x].ColType == "Combo" && this.Columns[x].FColType == "" ? "" : "%") + "'";
						if (oEvent.Filter[x] != null) this.SQLWhere += " and (" + (oEvent.Filter[x] as condition).toSQLString(this.DataSourceType, oEvent.Filter.getAuxData(x) as Type, (typeof(DateTime) == oEvent.Filter.getAuxData(x) as Type ? this.DateCastFunction : "")) + ")";
					}
					//return sSQLWhere;
				}
				return convert.cStr(this.SQLWhere);
			}
			set
			{
				this.SQLWhere = value;
			}
		}
		#endregion

		#region ***** METHODS *****
		public void setDataSourcePaged(string Fields, string Tables, string Filter)
		{
			this.setDataSourcePaged(Fields, Tables, Filter, "");
		}
		public void setDataSourcePaged(string Fields, string Tables, string Filter, string Grouping)
		{
			this.Fields = Fields;
			this.Filter = (String.IsNullOrWhiteSpace(Filter) ? "1=1" : Filter) + this.SQLFilter;
			this.Tables = Tables;
			this.Grouping = Grouping;
		}

		public override List<string> GetDataSourceColumns()
		{
			List<string> columns = new List<string>();
			foreach(DataColumn column in this.InternalDataSource.Columns)
				columns.Add(column.ColumnName);

			//Return List of Columns
			return columns;
		}
		public override IDataRow GetDataSourceRow(int RowIndex)
		{
			return (IDataRow)this.InternalDataSource.Rows[RowIndex];
		}

		public override bool DoesDataSourceColumnExist(string ColumnName)
		{
			return this.InternalDataSource.Columns.Contains(ColumnName);
		}
		public override int GetDataSourceRowCount()
		{
            if (this.InternalDataSource == null && !String.IsNullOrWhiteSpace(this.Fields)) GetDataSource();
			return this.InternalDataSource.Rows.Count;
		}
		protected internal override IListSource GetDataSource()
		{
			//If nto set, then load
            if (this.InternalDataSource == null && !String.IsNullOrWhiteSpace(this.Fields)) 
			{
                string sSort = "";
                if (this.SortCol > -1 && !String.IsNullOrWhiteSpace(this.Columns[this.SortCol].SortFieldPrefixed)) sSort = this.Columns[this.SortCol].SortFieldPrefixed + (this.Columns[this.SortCol].SortFieldPrefixed.EndsWith(" asc", StringComparison.CurrentCultureIgnoreCase) || this.Columns[this.SortCol].SortFieldPrefixed.EndsWith(" desc", StringComparison.CurrentCultureIgnoreCase) ? "" : " " + this.SortOrder);

                if (String.IsNullOrWhiteSpace(sSort)) throw new Exception("JGrid Error: Sort has not been set for setDataSourcePaged");

                GridDataEventArgs e = new GridDataEventArgs(this, this.Fields, this.Tables, this.Filter, this.Grouping, "*", "", "", sSort, "", this.CurrentPage, this.PageSize);
                this.OnGridBeforeDataQuery(e);
                if (this.InternalDataSource == null) 
				{
                    //+ " " + e.PreGrouping
                    if (this.DataSourceType == sqlbuilder.DataServerTypes.SQLServer)
                        this.InternalDataSource = (JDataTable)sqlbuilder.executeSelectPaged(DataHelper, e.PreFields, e.PreTables, e.PreFilter, e.PostFields, e.PostTables, e.PostFilter, e.InitialSort, e.PostSort, e.CurrentPage, e.PageSize, e.PKField, e.CustomScript);
                    else if (this.DataSourceType == sqlbuilder.DataServerTypes.Oracle)
                        this.InternalDataSource = (JDataTable)sqlbuilder.executeSelectPagedOracle(DataHelper, e.PreFields, e.PreTables, e.PreFilter, e.PostFields, e.PostTables, e.PostFilter, e.InitialSort, e.PostSort, e.PreGrouping, "", e.CurrentPage, e.PageSize, e.PKField, e.CustomScript);

                    this.MetaData.NumRows = (this.InternalDataSource.Rows.Count == 0 ? 0 : convert.cInt(this.InternalDataSource.Rows[0]["num_rows"]));

                    OnGridDatabind(new GridCellEventArgs(this, null));
                }

            }

            return this.InternalDataSource;
		}

		protected internal override void SetDataSource(IListSource value)
		{
			//Set Base
			this.InternalDataSource = value as JDataTable;

			//If Set
            if (value != null) 
			{
                if (DataViewFiltering || convert.cBool(this._DataViewSorting)) 
				{
                    string sSort = "";
                    if (this.SortCol > -1 && !String.IsNullOrWhiteSpace(this.Columns[this.SortCol].SortFieldPrefixed)) 
					{
                        if(this.Columns[this.SortCol].SortFieldPrefixed.IndexOf(" asc",  StringComparison.CurrentCultureIgnoreCase)==-1 && this.Columns[this.SortCol].SortFieldPrefixed.IndexOf(" desc",  StringComparison.CurrentCultureIgnoreCase)==-1)
                            sSort = parse.replaceAll(this.Columns[this.SortCol].SortFieldPrefixed, ",", " "+ this.SortOrder + ",") + " " + this.SortOrder;
                        else
                            sSort = this.Columns[this.SortCol].SortFieldPrefixed + (this.Columns[this.SortCol].SortFieldPrefixed.EndsWith(" asc", StringComparison.CurrentCultureIgnoreCase) || this.Columns[this.SortCol].SortFieldPrefixed.EndsWith(" desc", StringComparison.CurrentCultureIgnoreCase) ? "" : " " + this.SortOrder);
                    }
                    if (!this.DisableDataViewFiltering && (!String.IsNullOrWhiteSpace(sSort) || !String.IsNullOrWhiteSpace(SQLFilter)))
                        this.InternalDataSource = new DataView(value as JDataTable, (DataViewFiltering? parse.stripLeadingCharacter(SQLFilter, "and").Trim() : "" ), (convert.cBool(this.DataViewSorting) ? sSort : "" ), DataViewRowState.CurrentRows).ToJDataTable();
                }

                this.MetaData.NumRows = (this.InternalDataSource.Columns.Contains("num_rows") && this.InternalDataSource.Rows.Count > 0 ? convert.cInt(this.InternalDataSource.Rows[0]["num_rows"]) : this.InternalDataSource.Rows.Count);
                OnGridDatabind(new GridCellEventArgs(this, null));
            }
		}
		public condition getCondition(GridColumn Column, string FilterText)
		{
			System.Type oDataType = null;
			return getCondition(Column, FilterText, out oDataType);
		}
		public condition getCondition(GridColumn Column, string sFilterText, out Type DataType)
		{
			string sCriterea = Column.ColData.getCInputKey(sFilterText).Trim();
			DataType = null;
			if (Column.FField == "#") return new condition(sCriterea);
			else
			{
				if ((Column.ColData.CInputAtt.IndexOf("daterangepicker") > -1 || Column.ColData.FInputAtt.IndexOf("daterangepicker") > -1) && sCriterea.IndexOf("-") > -1)
				{
					DataType = typeof(DateTime);
					return new condition(Column.FField, condition.ConditionTypes.Between, convert.cDate(parse.splitValue(sCriterea, "-", 0), true).Date, convert.cDate(parse.splitValue(sCriterea, "-", 1), true).Date);
				}
				else
				{
					if (Column.ColType == "JCombo" && Column.ColData.CInputAtt.IndexOf("multi='true'") > -1 && sCriterea.IndexOf("|") > -1)
					{
                        return new condition("(" + Column.FField + " like '" + (!sCriterea.StartsWith("'") && Column.FPartialMatch ? "%" : "") + convert.cJoin((Column.FPartialMatch ? "%" : "") + "' OR " + Column.FField + " LIKE '" + (!sCriterea.StartsWith("'") && Column.FPartialMatch ? "%" : ""), parse.split(ado_helper.PrepareDB(sCriterea.Substring(sCriterea.StartsWith("'") && Column.FPartialMatch ? 1 : 0)), "|")) + (Column.FPartialMatch ? "%" : "") + "')");
					}
					else
					{
                        if (Column.ColData.CInputAtt.IndexOf("daterangepicker") > -1 || Column.ColData.FInputAtt.IndexOf("daterangepicker") > -1) DataType = typeof(DateTime);
                        else if (Column.ColData.CInputAtt.IndexOf("numeric") > -1 || Column.ColData.FInputAtt.IndexOf("numeric") > -1) DataType = typeof(double);
                        else DataType = typeof(string);
						bool bAComparison = sCriterea.StartsWith("<") || sCriterea.StartsWith(">"), bNotEqual = sCriterea.StartsWith("<>") || sCriterea.StartsWith("><");
                        if (sCriterea == "''") {
                            if (DataType == typeof(double)) return new condition("(" + Column.FField + " is null)");
                            return new condition("(" + Column.FField + " is null OR " + Column.FField + "='')");
                        } else {
                            if (DataType == typeof(double)) return new condition(Column.FField, (bNotEqual ? condition.ConditionTypes.NotEqual : (sCriterea.StartsWith("<") ? condition.ConditionTypes.LessThan : (sCriterea.StartsWith(">") ? condition.ConditionTypes.GreaterThan : condition.ConditionTypes.Equal))), parse.removeStrings(sCriterea, "<", ">"));
                            return new condition(Column.FField, (bNotEqual ? condition.ConditionTypes.NotLike : (sCriterea.StartsWith("<") ? condition.ConditionTypes.LessThan : (sCriterea.StartsWith(">") ? condition.ConditionTypes.GreaterThan : (Column.FLikeComparison ? condition.ConditionTypes.Like : condition.ConditionTypes.Equal)))), (!sCriterea.StartsWith("'") && Column.FPartialMatch && (!bAComparison || bNotEqual) && Column.FLikeComparison ? "%" : "") + parse.stripLeadingCharacter(sCriterea.Substring(sCriterea.StartsWith("'") && Column.FPartialMatch ? 1 : 0), "<", ">") + ((Column.FColType == "Combo" || Column.FColType == "UCombo") || ((Column.ColType == "Combo" || Column.ColType == "UCombo") && Column.FColType == "") || (bAComparison && !bNotEqual) || !Column.FLikeComparison ? "" : "%"));
                        }
					}
				}
			}
		}
		#endregion

		#region ***** OBSOLETE METHODS *****

		[Obsolete("Use the SaveAsHtml method")]
		public string saveAsHTML(ref int RowCounter) { return base.SaveAsHTML(ref RowCounter); }
		
		[Obsolete("Use the SaveAsExcel method")]
		public IWorkbook saveAsExcel(string TemplateName, IWorkbook WorkbookFile, string FileName, string SheetName, int SheetNumber, ref int RowCounter, string Title) { return base.SaveAsExcel(TemplateName, WorkbookFile, FileName, SheetName, SheetNumber, ref RowCounter, Title); }

        public condition buildCondition(string sColID, string sFilterText) {
            if (sFilterText.Trim() != "") {
                GridColumn oCol = getCol(sColID);
                if (oCol != null && oCol.FField != "") {
                    Type oDataType = null;
                    string[] sFilters = (oCol.MultiValueSeparator == "" || !sFilterText.Contains(oCol.MultiValueSeparator) ? new string[] { sFilterText } : parse.split(sFilterText, oCol.MultiValueSeparator));
                    string sFilter = "";
                    for (int x = 0; x < sFilters.Length; x++) {
                        if (parse.stripWhiteChars(sFilters[x]) != "") sFilter += getCondition(oCol, parse.stripWhiteChars(sFilters[x]), out oDataType).toSQLString(this.DataSourceType, oDataType, (typeof(DateTime) == oDataType ? this.DateCastFunction : "")) + " OR ";
                    }
                    if (sFilter != "") return new condition(parse.stripEndingCharacter(sFilter, " OR "));
                }
            }
            return null;
        }
		#endregion



	}
}
