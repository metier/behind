﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using jlib.components.button;
using jlib.db;
using jlib.functions;

namespace jlib.controls.jgrid
{
	[ParseChildren( true )]
	public class GridColumn : System.Web.UI.Control, System.ICloneable
	{
		#region ***** ENUMS *****
		public enum ColTypes 
		{
			Hide,
			Select,
			Text,
            Textarea,
			Combo,
            UCombo,
            JCombo,
			NoEdit,
			Options
		}
		#endregion


		#region ***** CONSTRUCTORS *****
		public GridColumn()
		{

			//Set Initial Values
			this.ColumnSpan = 1;
			this.ColData = new GridColumnSettings(this);
			this.ExcelColType = "string";

			this.FormatString = "";
			this.FormatStringArguments = "";
			this.MultiValueSeparator = "";
			this.SQLDataSource = "";
			this.FSQLDataSource = "";
			this.DataValueField = "";
			this.DataTextField = "";
			this.DataAuxField = "";
			this.SortFieldPrefixed = "";

			this.ConvertNewlineToBr = false;
			this.FPartialMatch = false;
			this.FLikeComparison = true;
			this.PrimaryKey = false;
		}
		#endregion


		#region ***** PROPERTIES *****
		public jGrid Grid { get; set; }
		public int ColumnSpan { get; private set; }
		public bool PrimaryKey { get; set; }
		public bool ConvertNewlineToBr { get; set; }
		public bool FPartialMatch { get; set; }
        public bool FLikeComparison { get; set; }
        public string FormatString  { get; set; }
        public string FormatStringArguments  { get; set; }
        public string MultiValueSeparator { get; set; }
		public string SQLDataSource { get; set; }
		public string FSQLDataSource { get; set; }
		public string DataValueField { get; set; }
		public string DataTextField { get; set; }
		public string DataAuxField { get; set; }
		public GridColumnSettings ColData { get; set; }
		public string ExcelColType { get; set; }
		public string SortFieldPrefixed { get; private set; }

		public override string ID
        {
            get { return base.ID; }
            set
            {
                ColData.ID = value;
                base.ID = value;
            }
        }

		private string _DataMember = null;
		public string DataMember 
		{
            get 
			{
                if (_DataMember != null && this._DataMember.IndexOf(".") > -1) return parse.split(this._DataMember, ".")[1];
                if (_DataMember == null && parse.removeXChars(this.SortField.ToLower(), "abcdefghijklmnopqrstuvwxyz") != "") return this.SortField;
                if (_DataMember == null && parse.replaceAll(this.FormatString, "#", "").ToLower() == parse.removeXChars(this.FormatString.ToLower(), "abcdefghijklmnopqrstuvwxyz_")) return parse.replaceAll(this.FormatString, "#", "");
                return this._DataMember;
            }
            set 
			{
                _DataMember = value;
            }
        }

		#endregion



		#region ***** METHODS *****
		#endregion





		
		private string m_sFField = null;
		
		

		

		public int Index 
		{
			get 
			{
                if (this.Grid.Columns!=null && this.Grid.Columns.Contains(this)) return this.Grid.Columns.IndexOf(this);
                return -1;
			}			
		}

		
        public string Grouping 
		{
            get 
			{
                string s = "";
                if (this.Grid.Views.SelectedView.Grouping.TryGetValue(this.ID, out s)) return s;
                return "Hide";
            }
        }
        public string groupedDataName(string sPrefix) 
		{            
            string sFieldName = sPrefix+ convert.cStr(SortField, FormatStringArguments, FormatString);
            if (",select,group,".Contains("," + Grouping.ToLower() + ",")) return sFieldName;
            if (Grouping.ToLower() == "group-day") return "CAST(" + sFieldName + " AS DATE)";
            if (Grouping.ToLower() == "group-month") return "CAST(DATEPART(YEAR," + sFieldName + ") AS VARCHAR(100)) + '-' + RIGHT('0'+CAST(DATEPART(month," + sFieldName + ") AS VARCHAR(100)),2)"; //"SUBSTRING(DATENAME(month," + sFieldName + "),1,3) + ' ' + CAST(DATEPART(YEAR," + sFieldName + ") AS VARCHAR(100))";
            if(Grouping.ToLower() == "group-year")return "DATEPART(YEAR," + sFieldName + ")";
            return Grouping + "(" + sFieldName + ")";            
        }

		public string groupedDataNamesWithAs(string Prefix) 
		{


			string output = "";

			string[] fieldNames = (convert.cStr(SortField, FormatStringArguments, FormatString)).Split(',');

			foreach (string fieldName in fieldNames)
			{
				output += this.groupedDataNameWithAs(Prefix, fieldName) + ",";
			}
			
			if (output.EndsWith(",")) output = output.Substring(0, output.Length - 1);

			//return
			return output;
        }

		public string groupedDataNameWithAs(string prefix, string fieldName) 
		{


			string output = "";

			string fqFieldName = prefix + fieldName.Trim();

			if (",select,group,".Contains("," + Grouping.ToLower() + ",")) output += fqFieldName + " AS " + fieldName + ",";
			else if (Grouping.ToLower() == "group-day") output += "CAST(" + fqFieldName + " AS DATE)" + " AS " + fieldName + ",";
			else if (Grouping.ToLower() == "group-month") output += "CAST(DATEPART(YEAR," + fqFieldName + ") AS VARCHAR(100)) + '-' + RIGHT('0'+CAST(DATEPART(month," + fqFieldName + ") AS VARCHAR(100)),2)" + " AS " + fieldName + ","; //"SUBSTRING(DATENAME(month," + sFieldName + "),1,3) + ' ' + CAST(DATEPART(YEAR," + sFieldName + ") AS VARCHAR(100))";
			else if (Grouping.ToLower() == "group-year") output += "DATEPART(YEAR," + fqFieldName + ")" + " AS " + fieldName + ",";
			else output += this.Grouping + "(" + fqFieldName + ")" + " AS " + fieldName + ",";
			
			if (output.EndsWith(",")) output = output.Substring(0, output.Length - 1);

			//return
			return output;
        }

		public string groupedDataName(string prefix, string fieldName) 
		{


			string output = "";

			string fqFieldName = prefix + fieldName.Trim();

			if (",select,group,".Contains("," + Grouping.ToLower() + ",")) output += fqFieldName + ",";
			else if (Grouping.ToLower() == "group-day") output += "CAST(" + fqFieldName + " AS DATE)" + ",";
			else if (Grouping.ToLower() == "group-month") output += "CAST(DATEPART(YEAR," + fqFieldName + ") AS VARCHAR(100)) + '-' + RIGHT('0'+CAST(DATEPART(month," + fqFieldName + ") AS VARCHAR(100)),2)" + ","; //"SUBSTRING(DATENAME(month," + sFieldName + "),1,3) + ' ' + CAST(DATEPART(YEAR," + sFieldName + ") AS VARCHAR(100))";
			else if (Grouping.ToLower() == "group-year") output += "DATEPART(YEAR," + fqFieldName + ")" + ",";
			else output += this.Grouping + "(" + fqFieldName + ")" + ",";
			
			if (output.EndsWith(",")) output = output.Substring(0, output.Length - 1);

			//return
			return output;
        }

        

		
		
		#warning Should be enumerated
		public string ColType 
		{
			get 
			{
				return this.ColData.Type;
			}
			set 
			{
				this.ColData.Type = (value==""?"":((ColTypes)Enum.Parse(typeof(ColTypes), value, true)).ToString());
                if (value != "" && this.ID != "" && this.Grid != null && this.Grid.Views.Count > 0 && this.Grid.Views.SelectedView !=null) this.Grid.Views[0].Grouping[this.ID] = ColData.Type;
			}
		}

		public string FColType {
			get {				
				return this.ColData.FType;
			}
			set {
				this.ColData.FType = (value==""?"":((ColTypes)Enum.Parse(typeof(ColTypes), value, true)).ToString());
			}
		}

		public string FField {
			get {
				if (m_sFField == null) return SortField;
				return m_sFField;
			}
			set {
				m_sFField = value;
			}
		}

		public string AddPrompt 
		{
			get 
			{
				return this.ColData.AddPrompt;
			}
			set 
			{
				this.ColData.AddPrompt = value;
			}
		}
        public string GroupTypes 
		{
			get 
			{
                return ColData.GroupTypes;
			}
			set 
			{
                ColData.GroupTypes = value;
			}
		}
       
		
		public string CInputAtt 
		{			
			get 
			{
                return this.ColData.CInputAtt;
			}
            set 
			{
				this.ColData.CInputAtt = value;
			}
		}
		public string FInputAtt 
		{			
			set 
			{
				this.ColData.FInputAtt = value;
			}
		}
        public string UListAtt 
		{
            set 
			{
                this.ColData.UListAtt = value;
            }
        }
        public string EditCols 
		{
            set 
			{
                this.ColData.EditCols = value;
            }
        }
        public bool MultiValue 
		{
            set 
			{
                this.ColData.MultiValue = (value ? 1 : 0);
            }
        }

		public string CInputDefault 
		{			
			set 
			{
				this.ColData.CInputDefault = value;
			}
		}
		
		public string CInputValues 
		{			
			set 
			{
                helpers.structures.collection_aux oList = new helpers.structures.collection_aux();
				string []sValues=parse.split(value,"|");
				for (int x = 0; x < sValues.Length; x = x + 2)
					oList.Add(sValues[x], sValues[x + 1]);
				
				this.ColData.setCInputValues(oList);
			}
		}		
        
        public bool BindDropdownByID 
		{
            set 
			{
                this.ColData.BindDropdownByID = (value ? 1 : 0);
            }
        }

		public string HAtt
		{			
			set 
			{
				this.ColData.HAtt = value;
			}
		}

		public string CAtt
		{			
			set 
			{
				this.ColData.CAtt = value;
			}
		}

		

		

        public string SortField 
		{
            get 
			{
                if (this.SortFieldPrefixed.IndexOf(".") > -1 && this.SortFieldPrefixed==parse.removeXChars(this.SortFieldPrefixed,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._ ")) return parse.split(this.SortFieldPrefixed, ".")[1];
                return this.SortFieldPrefixed;
            }
            set 
			{
                this.SortFieldPrefixed = value;
                this.ColData.Sortable = (value == "" ? 0 : (value=="#" ? -1:1));
            }
        }
        
        

        
        
        
        public string HText 
		{            
            set 
			{
				this.ColData.HText = value;
            }
			get 
			{
				return this.ColData.HText;
			}
        }

		public string CText 
		{
			set 
			{
				this.ColData.CText = value;
			}
            get 
			{
                return this.ColData.CText;
            }
		}
        
        
		

        public override bool Visible 
		{
            get 
			{
                return base.Visible;
            }
            set 
			{
                base.Visible = value;
                if (!value) 
				{
                    this.ColType = "Hide";
                    this.FColType = "Hide";
                }
            }
        }

		public void setCInputValues(DataTable oDT, string sDataValueField, string sDataTextField, string sAuxDataField) 
		{
            if (convert.cStr(sDataValueField) == "") 
			{
                sDataValueField = oDT.Columns[0].ColumnName;
                sDataTextField = oDT.Columns[1].ColumnName;
                if(oDT.Columns.Count>2)sAuxDataField = oDT.Columns[2].ColumnName;
            }
            helpers.structures.collection_aux oList = new helpers.structures.collection_aux();
            for (int x = 0; x < oDT.Rows.Count; x++) 
			{
                if (oList.Contains(convert.cStr(oDT.Rows[x][sDataValueField])))
                    oList.Remove(convert.cStr(oDT.Rows[x][sDataValueField]));
                oList.Add(convert.cStr(oDT.Rows[x][sDataValueField]), oDT.Rows[x][sDataTextField], (convert.cStr(sAuxDataField) == "" ? "" : oDT.Rows[x][sAuxDataField]));
            }
			ColData.setCInputValues(oList);
		}

        public void setFInputValues(DataTable oDT, string sDataValueField, string sDataTextField, string sAuxDataField) 
		{
            if (convert.cStr(sDataValueField) == "") 
			{
                sDataValueField = oDT.Columns[0].ColumnName;
                sDataTextField = oDT.Columns[1].ColumnName;
                if (oDT.Columns.Count > 2) sAuxDataField = oDT.Columns[2].ColumnName;
            } 
            helpers.structures.collection_aux oList = new helpers.structures.collection_aux();
            for (int x = 0; x < oDT.Rows.Count; x++) 
			{
                if (oList.Contains(convert.cStr(oDT.Rows[x][sDataValueField])))
                    oList.Remove(convert.cStr(oDT.Rows[x][sDataValueField]));
                oList.Add(convert.cStr(oDT.Rows[x][sDataValueField]), oDT.Rows[x][sDataTextField], (convert.cStr(sAuxDataField) == "" ? "" : oDT.Rows[x][sAuxDataField]));
            }
			ColData.setFInputValues(oList);
		}
        
		public object Clone() 
		{
            return jlib.helpers.control.cloneObject(this, new string[] { "ColData" }, true);
        }
	}
}
