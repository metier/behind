﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jlib.controls.jgrid
{
	public class GridDataEventArgs : EventArgs 
	{
        public jGrid Grid = null;
        public string PreFields, PreTables, PreFilter, PreGrouping, PostFields, PostTables, PostFilter, InitialSort, PostSort, CustomScript, PKField;
        public int CurrentPage, PageSize;
        public GridDataEventArgs(jGrid Grid, string PreFields, string PreTables, string PreFilter, string PreGrouping, string PostFields, string PostTables, string PostFilter, string InitialSort, string PostSort, int CurrentPage, int PageSize) 
		{
            this.Grid = Grid;
            this.PreFields = PreFields;
            this.PreTables = PreTables;
            this.PreFilter = PreFilter;
            this.PreGrouping = PreGrouping;
            this.PostFields = PostFields;
            this.PostTables = PostTables;
            this.PostFilter = PostFilter;
            this.InitialSort = InitialSort;
            this.PostSort = PostSort;
            this.CurrentPage = CurrentPage;
            this.PageSize = PageSize;
        }
    }
}
