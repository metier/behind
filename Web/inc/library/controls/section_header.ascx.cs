using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using jlib.components.button;
using jlib.db;
using jlib.functions;


namespace jlib.controls
{

    [ParseChildren( true )]
    public class section_header : System.Web.UI.UserControl
    {

        private string m_sTitle = "";
        //private string m_sRTitle = "";
        
        protected System.Web.UI.WebControls.Label ctrTitle;
        protected System.Web.UI.WebControls.PlaceHolder ctrRTitle;

        protected System.Web.UI.HtmlControls.HtmlTable ctrHeaderTable, ctrContentTable;
        protected System.Web.UI.HtmlControls.HtmlTableCell ctrContentCell, ctrRightSection;
        
        public PlaceHolder Content {
            set {
                ctrContentCell.Controls.Clear();
                while ( value.Controls.Count > 0 )
                    ctrContentCell.Controls.Add( value.Controls[0] );
            }
        }


        public HtmlTable HeaderTable {
            get {
                return ctrHeaderTable;
            }
            set {
                ctrHeaderTable = value;
            }
        }

        public HtmlTable ContentTable {
            get {
                return ctrContentTable;
            }
            set {
                ctrContentTable = value;
            }
        }

        public string Title {
            get {
                return m_sTitle;
            }
            set {
                m_sTitle = value;
            }
        }

        public string RTitle {            
            set {
                ctrRightSection.Controls.Add( convert.cLiteral( value ));                                
            }
        }

        public System.Web.UI.Control RTitleControl {
            set {
                ctrRightSection.Controls.Add( value );
            }
        }
        
        protected override void Render( HtmlTextWriter writer ) {
            ctrTitle.Text = Title;

            ctrRightSection.Visible = ctrRightSection.Controls.Count > 0;
            
            if ( ctrContentCell.Controls.Count == 0 )
                ctrContentTable.Visible     = false;

            base.Render( writer );
        }
                
    }
}
