<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="section_header.ascx.cs" Inherits="jlib.controls.section_header" %>
<table runat="server" id="ctrHeaderTable" class="section_header" style="width:100%;">
    <tr>
        <td class="left"></td>
        <td class="fill"><asp:Label runat="server" ID="ctrTitle" /></td>
        <td class="fill" runat="server" align="right" id="ctrRightSection"><asp:placeholder runat="server" ID="ctrRTitle" /></td>
        <td class="right"></td>
    </tr>
</table>
<table runat="server" id="ctrContentTable" class="section_box">
    <tr>
        <td runat="server" id="ctrContentCell" valign="top" style="padding:8px"></td>
    </tr>
</table>