using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using jlib.functions;
using System.Web.UI;

namespace jlib.controls.cooltabs
{

	/// <summary>
	///		Summary description for webcontrol.
	/// </summary>
	[ParseChildren(true, "Content")]
	public class tabpage : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.PlaceHolder plhContent;
		public System.Web.UI.HtmlControls.HtmlTable tableMain;
		protected System.Web.UI.HtmlControls.HtmlTableCell tdContent;

		private string m_sTabCaption			= "";
        private string m_sTabWidth = "";
		private int m_iTabID					= 1;
		private int m_iPagePadding				= 4;
        private string m_sTag = "";
        private string m_sNavigateUrl = "";

		private void Page_Load(object sender, System.EventArgs e)
		{
            tableMain.Style["display"] = "none";            
			
		}
		private void Page_PreRender(object sender, System.EventArgs e)
		{
			tableMain.Attributes["tab_id"]		= convert.cStr( this.TabID );
			tdContent.Style["padding"]			= String.Format("{0}px", this.PagePadding);
		}

		#region PROPERTIES

		public int TabID
		{
			get
			{
				return m_iTabID;
			}
			set
			{
				m_iTabID = value;
			}
		}
		public int PagePadding
		{
			get
			{
				return m_iPagePadding;
			}
			set
			{
				m_iPagePadding = value;
			}
		}
       

		public PlaceHolder Content
		{
			set{
                plhContent.Controls.Clear();
				plhContent.Controls.Add(value);
			}
            get {
                return plhContent;
            }
		}
		public string TagHandle
		{
			get
			{
				return tableMain.ClientID;
			}
		}
		public string TabCaption
		{
			get
			{
				return m_sTabCaption;
			}
			set
			{
				m_sTabCaption = value;
			}
		}

        public string TabWidth
		{
			get
			{
                return m_sTabWidth;
			}
			set
			{
                m_sTabWidth = value;
			}
		}

        public string Tag {
            get {
                return m_sTag;
            }
            set {
                m_sTag = value;
            }
        }

        public string NavigateUrl {
            get {
                return m_sNavigateUrl;
            }
            set {
                m_sNavigateUrl = value;
            }
        }


		#endregion

		#region METHODS

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			this.PreRender += new System.EventHandler(this.Page_PreRender);

		}
		#endregion

		
	}
}
