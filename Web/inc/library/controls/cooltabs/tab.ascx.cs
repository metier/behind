using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using jlib.functions;
using System.Web.UI;

namespace jlib.controls.cooltabs {

    /// <summary>
    ///		Summary description for webcontrol.
    /// </summary>
    [ParseChildren( false )]
    public class tab : System.Web.UI.UserControl {
        public System.Web.UI.HtmlControls.HtmlTableCell tdTab;
        protected System.Web.UI.WebControls.Literal ltrCaption;
        protected HtmlTableCell ctrPadding;

        private int m_iTabID = 1;
        private string m_sTabWidth = "";        
        private tabpage m_oTabpage = null;

        private void Page_Load( object sender, System.EventArgs e ) {
            tdTab.Attributes["class"] = "tab_notactive";
        }
        private void Page_PreRender( object sender, EventArgs e ) {
            tdTab.Attributes["tab_id"] = convert.cStr( m_iTabID );
            if ( m_sTabWidth != "" )
                tdTab.Width = m_sTabWidth;
        }

        #region tabs
        public string Caption {
            get {
                return ltrCaption.Text;
            }
            set {
                ltrCaption.Text = value;
            }
        }
        public int TabID {
            get {
                return m_iTabID;
            }
            set {
                m_iTabID = value;
            }
        }

        public string TagHandle {
            get {
                return tdTab.ClientID;
            }
        }

        public string Tag {
            get {
                return m_oTabpage.Tag;
            }
            set {
                m_oTabpage.Tag = value;
            }
        }

        public string NavigateUrl {
            get {
                return m_oTabpage.NavigateUrl;
            }
            set {
                m_oTabpage.NavigateUrl = value;
            }
        }

        
        public int TabSpacing {
            set {
                ctrPadding.Visible = ( value > 0 );
                ctrPadding.Style["width"] = value + "px";
            }
        }

        public tabpage Tabpage {
            get {
                return m_oTabpage;
            }
            set {
                m_oTabpage = value;

                this.TabID = value.TabID;
                this.Caption = value.TabCaption;
                this.m_sTabWidth = value.TabWidth;
            }
        }
        #endregion


        #region Web Form Designer generated code
        override protected void OnInit( EventArgs e ) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit( e );
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Load += new System.EventHandler( this.Page_Load );
            this.PreRender += new EventHandler( this.Page_PreRender );
        }
        #endregion


    }
}
