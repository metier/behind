var TabContainer				= document;
	
	//Variables
	TabContainer.Pages		= new Array();
	TabContainer.Tabs			= new Array();
	TabContainer.CurrentTab	= null;
	TabContainer.StartTab		= 1;
	
	//Methods
	TabContainer.Init				= fnPropertiesInit;
	TabContainer.findPageByTabId	= fnPropertiesFindPageByTabId;

function fnPropertiesInit()
{
	//Load Pages
	for(var i=0;i<document.getElementById("tdPages").children.length;i++)
	{
		var oElm	= document.getElementById("tdPages").children[i];
		if(oElm.Context	== "intellicms.library.dialogs.properties.tabpage")
		{
			//Set Size
			try
			{
				oElm.style.height	= (parseInt(document.parentWindow.frameElement.offsetHeight)- 55) + "px";
			}catch(e){}
		
			//Add Page Properties & Methods
			oElm.TabID		= oElm.getAttribute("tab_id");
			oElm.show		= fnPageShow;
			oElm.hide		= fnPageHide;
			
			this.Pages[this.Pages.length] = oElm;
		}
	}
	
	//Load Tabs
	for(var i=0;i<document.getElementById("trTabs").children.length;i++)
	{
		var oElm	= document.getElementById("trTabs").children[i];
		if(oElm.Context	== "intellicms.library.dialogs.properties.tab")
		{
			//Add Tab Properties & Methods
			oElm.TabID			= oElm.getAttribute("tab_id");
			oElm.TabPage		= null; //this.findPageByTabId(oElm.TabID);
			oElm.setActive		= fnTabSetActive;
			oElm.setNotActive	= fnTabSetNotActive;
			oElm.onclick		= fnTabOnClick;
			oElm.getTabPage		= fnTabGetTabPage;
			
			//Add Tab to Collection
			this.Tabs[this.Tabs.length] = oElm;
		}
	}
	
	//Show First Tab
	this.Tabs[this.StartTab - 1].setActive();
	this.CurrentTab				= this.Tabs[this.StartTab - 1];
	
}
function fnPropertiesFindPageByTabId(sTabID)
{
	for(var i=0;i<this.Pages.length;i++)
	{
		if(this.Pages[i].TabID == sTabID)
		{
			return this.Pages[i];
		}
	}
	
	return null;
}
function fnTabGetTabPage()
{
	this.TabPage	= TabContainer.findPageByTabId(this.TabID)
	
	return this.TabPage;
}
function fnTabOnClick()
{
	if(Properties.CurrentTab != null)
		Properties.CurrentTab.setNotActive();
	
	Properties.CurrentTab	= this;
	Properties.CurrentTab.setActive();
}

function fnTabSetActive()
{
	this.className			= "tab_active";
	this.getTabPage().show();
}
function fnTabSetNotActive()
{
	this.className			= "tab_notactive";
	this.getTabPage().hide();
}

function fnPageShow()
{
	this.style.display		= "";
}
function fnPageHide()
{
	this.style.display		= "none";
}

