<%@ Control Language="c#" AutoEventWireup="false" Codebehind="tab.ascx.cs" Inherits="jlib.controls.cooltabs.tab" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<td class="tab_notactive" runat="server" id="tdTab">
    <table width="100%">
        <tr>
            <td class="tab_left">&nbsp;</td>
            <td class="tab_fill" nowrap="nowrap"><asp:literal runat="server" id="ltrCaption" /></td>            
            <td class="tab_right">&nbsp;</td>
            <td style="width:5px" runat="server" id="ctrPadding"></td>
        </tr>
    </table>        
</td>

