using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using jlib.functions;
using System.Web.UI;

namespace jlib.controls.cooltabs
{

    public delegate void OnTabChangeHandler( object sender, TabEventArgs e );

	/// <summary>
	///		Summary description for webcontrol.
	/// </summary>
	[ParseChildren(true, "TabPages")]
    public class container : System.Web.UI.UserControl, IPostBackEventHandler
	{

        protected System.Web.UI.HtmlControls.HtmlInputHidden ctrTabIndex;
        protected System.Web.UI.HtmlControls.HtmlContainerControl ctrCss;
		protected System.Web.UI.WebControls.PlaceHolder plhTabs;
		protected System.Web.UI.WebControls.PlaceHolder plhTabPages;
        protected System.Web.UI.HtmlControls.HtmlTable ctrContainer;

        protected System.Web.UI.HtmlControls.HtmlTableRow ctrTabPagesRow;
        protected System.Web.UI.HtmlControls.HtmlTableCell ctrTabPages;
        
	
		private int m_iStartTabID						= 0;
        private string m_sBackgroundColor = "";
        private int m_iTabSpacing                       = 5;
        private tab []m_oTabs               = null;
        public event OnTabChangeHandler TabChange;

        protected virtual void OnTabChange( TabEventArgs e ) {
            if ( TabChange != null ) TabChange( this, e );
        }

        public string CssPath {
            get {
                return ctrCss.Attributes["href"];
            }
            set {
                ctrCss.Attributes["href"] = value;
            }
        }

        public string CssClass {
            get {
                return ctrContainer.Attributes["class"];
            }
            set {
                ctrContainer.Attributes["class"] = value;
            }
        }

       

        public string Width {
            get {
                return ctrContainer.Style["width"];
            }
            set {
                ctrContainer.Style["width"] = value;
            }
        }

        public string BackgroundColor {
            get {
                return m_sBackgroundColor;
            }
            set {
                m_sBackgroundColor = value;
            }
        }

        public int TabSpacing {
            set {
                m_iTabSpacing = value;
            }
        }

        protected override void OnInit( EventArgs e ) {
            this.PreRender += new EventHandler( Control_PreRender );            
            this.Init += new EventHandler( Control_Init );

            base.OnInit( e );
        }

        protected void Control_Init( object sender, EventArgs e ) {
            this.StartTabID = convert.cInt( ctrTabIndex.Value );
        }

        protected void Control_PreRender( object sender, EventArgs e )
		{

			//Page.RegisterStartupScript("jlib.controls.tabs.starttab", String.Format("<script>Container.StartTab = {0};</script>", this.StartTab));
            bool bHasPostback = false;
            //bool bHasHref = false;
            for ( int x = 0; getByIndex( x ) != null; x++ ) {                                
                tab oTab = getByIndex( x );
                if ( ( oTab.Tabpage.Content.Controls.Count == 0 || oTab.Tabpage.Content.Controls[0].Controls.Count == 0 ) ) {
                    oTab.Tabpage.Visible = false;
                    bHasPostback = true;                        
                }
                //if ( oTab.NavigateUrl != "" ) bHasHref = true;
                oTab.TabSpacing = m_iTabSpacing;            
            }

            if ( StartTabID > plhTabPages.Controls.Count )
                StartTabID = 0;

            if ( plhTabPages.Controls.Count > 0 ) {
                ( plhTabPages.Controls[StartTabID] as tabpage ).tableMain.Style["display"] = "";
                ( plhTabs.Controls[StartTabID] as tab ).tdTab.Attributes["class"] = "tab_active";
                ctrTabPagesRow.Visible = ( plhTabPages.Controls[StartTabID] as tabpage ).Visible;
            }

            string sAllTabsInactive = "";
            for ( int x = 0; getByIndex( x ) != null ; x++ )
                if ( getByIndex( x ).Visible )
                    sAllTabsInactive    += String.Format( "document.getElementById( '{0}' ).className   = 'tab_notactive';" , ( plhTabs.Controls[ x ] as tab).tdTab.ClientID );

            string sAllPagesHidden = "";
            for ( int x = 0; getByIndex( x ) != null ; x++ ) {
                tab oTab = getByIndex( x );
                if ( oTab.Tabpage.Visible )
                    sAllPagesHidden += String.Format( "document.getElementById( '{0}' ).style.display   = 'none';", oTab.Tabpage.tableMain.ClientID );
            }

            for ( int x = 0; getByIndex( x ) != null; x++ ) {
                tab oTab = getByIndex( x );

                jlib.components.hyperlink oLink = new jlib.components.hyperlink();
                if ( oTab.NavigateUrl != "" ) {

                    oTab.tdTab.Attributes["onclick"] = "window.location = '" + oTab.NavigateUrl + "';";
                    while( oTab.tdTab.Controls.Count > 0 )
                        oLink.Controls.Add( oTab.tdTab.Controls[0]);

                    oTab.tdTab.Controls.Add( oLink );
                    oLink.NavigateUrl = oTab.NavigateUrl;
                } else if ( bHasPostback ) {
                    oTab.tdTab.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink( this, "TabClick_" + x );

                    while ( oTab.tdTab.Controls.Count > 0 )
                        oLink.Controls.Add( oTab.tdTab.Controls[0] );

                    oTab.tdTab.Controls.Add( oLink );
                    oLink.NavigateUrl = "#";
                    oLink.Attributes["onclick"] = oTab.tdTab.Attributes["onclick"];

                } else {
                    oTab.tdTab.Attributes["onclick"] = sAllPagesHidden + sAllTabsInactive + "this.className  = 'tab_active'; document.getElementById( '" + oTab.Tabpage.tableMain.ClientID + "' ).style.display = '';";
                }
            }
            ctrTabPages.ColSpan = plhTabs.Controls.Count + 1;

            if ( BackgroundColor != "" ) ctrTabPages.Style["background-color"] = BackgroundColor;
            ctrTabIndex.Value = this.StartTabID.ToString();

		}

        #region PROPERTIES


        public int StartTabID
		{
			get
			{
				return m_iStartTabID;
			}
			set
			{
				m_iStartTabID = value;
			}
		}
		
		public PlaceHolder Tabs
		{
			get
			{
				return plhTabs;
			}
			set
			{
				plhTabs = value;
			}
		}
		public PlaceHolder TabPages
		{
			get
			{
				return plhTabPages;
			}
			set
			{
                m_oTabs = new tab[value.Controls.Count];
                //plhTabPages = value;
                while ( value.Controls.Count > 0 ) {
                    if ( value.Controls[0] as tabpage == null )
                        value.Controls.RemoveAt( 0 );
                    else
                        plhTabPages.Controls.Add( value.Controls[0] );
                }

                for ( int x = 0; x < plhTabPages.Controls.Count; x++ ) {
                    
                    tabpage oTabPage = (tabpage)plhTabPages.Controls[x];
                    
                    //Add Corresponding Tab
                    tab oTab = (tab)Page.LoadControl( "/inc/library/controls/cooltabs/tab.ascx" );
                    m_oTabs[x] = oTab;
                    oTab.Tabpage = oTabPage;
                    oTab.Visible = oTabPage.Visible;
                    plhTabs.Controls.Add( oTab );
                }
               
			}
		}
        #endregion

        #region METHODS
        public bool selectByTag( string sTag ) {
            for ( int x = 0; x < plhTabs.Controls.Count; x++ ) {
                tab oTab = plhTabs.Controls[x] as tab;
                if ( oTab != null && oTab.Tag == sTag ) {
                    StartTabID = x;
                    return true;
                }
            }
            return false;
        }

        public tab getByTag( string sTag ) {
            
            for ( int x = 0;; x++ ) {
                if ( getByIndex( x ) == null ) return null;

                if ( getByIndex( x ).Tag == sTag )
                    return getByIndex( x );                
            }            
        }

        public tab getByIndex( int iIndex ) {
            if ( m_oTabs == null || iIndex < 0 || iIndex > m_oTabs.Length - 1 )
                return null;

            return m_oTabs[iIndex];
        }

        #endregion

        #region IPostBackEventHandler Members

            public void RaisePostBackEvent( string sEventArgument ) {

            if ( sEventArgument.StartsWith( "TabClick_" ) ) {
                this.StartTabID = convert.cInt( parse.replaceAll( sEventArgument, "TabClick_", "" ) );
                
                this.OnTabChange( new TabEventArgs( this.StartTabID, this.Tabs.Controls[ this.StartTabID ] as tab, this.TabPages.Controls[this.StartTabID] as tabpage ));
                
            //} else {
                //base.RaisePostBackEvent( sEventArgument );
            }
        }
        #endregion

	}

    public class TabEventArgs : EventArgs {

        private int m_iIndex = -1;
        private tab m_oTab = null;
        private tabpage m_oTabPage = null;

        public TabEventArgs( int iIndex, tab oTab, tabpage oTabPage ) {
            m_iIndex = iIndex;
            m_oTab = oTab;
            m_oTabPage = oTabPage;
        }

        public int Index {
            get {
                return m_iIndex;
            }
            set {
                m_iIndex = value;
            }
        }
        public tab Tab {
            get {
                return m_oTab;
            }
            set {
                m_oTab = value;
            }
        }
        public tabpage TabPage {
            get {
                return m_oTabPage;
            }
            set {
                m_oTabPage = value;
            }
        }
    }
}
