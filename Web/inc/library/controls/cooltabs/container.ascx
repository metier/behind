<%@ Control Language="c#" AutoEventWireup="false" Codebehind="container.ascx.cs" Inherits="jlib.controls.cooltabs.container" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="icms" Namespace="jlib.components" Assembly="jlib" %>
<input type="hidden" runat="server" id="ctrTabIndex" value="0" />
<link rel="stylesheet" href="~/inc/library/controls/cooltabs/container.css" runat="server" id="ctrCss" />

<table class="tab_container" runat="server" id="ctrContainer">
	<tr id="trTabs">
	    <td>
	        <table>
	            <tr><asp:placeholder runat="server" id="plhTabs" /></tr>
	        </table>
	    </td>
		
		<td class="tab_empty">&nbsp;</td>
	</tr>				
	<tr runat="server" id="ctrTabPagesRow">
	    <td runat="server" id="ctrTabPages" >
	        <asp:placeholder runat="server" id="plhTabPages" />				
	    </td>
	</tr>
</table>
