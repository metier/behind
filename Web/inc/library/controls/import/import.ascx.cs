﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
using System.Data;
using jlib.components.button;
using jlib.db;
using jlib.functions;


namespace jlib.controls.import{
    public delegate void OnRowValidateHandler(object sender, DataRow oRow);
    public delegate void OnDataValidateHandler(object sender, DataTable oData);
    public delegate void OnColumnMatchHandler(object sender);
    public delegate void OnDataFixHandler(object sender, DataTable oData);
    public delegate void OnRowCommitHandler(object sender, DataRow oImportRow, DataRow oTableRow, ref bool bReturnValue);
    public delegate void OnDataCommitHandler(object sender, DataTable oData);
    public delegate void OnRenderErrorsHandler(object sender, DataTable oValidationErrors);
    public delegate void OnResolveValuesHandler(object sender, DataRow oError, DataRow oRow, string sFormValue, ref object[] oReplacementPairs);
            
    

        public class import : System.Web.UI.WebControls.Table, System.Web.UI.INamingContainer {

            public event OnRowValidateHandler RowValidate;
            public event OnDataValidateHandler DataValidate;
            public event OnColumnMatchHandler ColumnMatch;
            public event OnDataFixHandler DataFix;
            public event OnRowCommitHandler RowCommit;
            public event OnDataCommitHandler DataCommit;
            public event OnRenderErrorsHandler RenderErrors;
            public event OnResolveValuesHandler ResolveValues;
            
            public import() {
                m_oDataSet.Tables.Add(new DataTable());
                m_oDataSet.Tables.Add(new DataTable());
            }

            public class importCol {
                public string ID = "";
                public string DataMember = "";
                public string Type = "";
                public string Title = "";
                public string Value = "";
                public bool Required = false;
                public int Index = -1;
                public int DataCol = -1;
                public importCol(string sID, string sDataMember, string sType, string sTitle, string sValue, bool bRequired, int iIndex) {
                    ID = sID;
                    DataMember = sDataMember;
                    Type = sType;
                    Title = sTitle;
                    Value = sValue;
                    Required = bRequired;
                    Index = iIndex;
                }
            }

            public enum ValidationErrorTypes {
                RequiredFieldMissing = 1,
                FieldNotRecognized = 10,
                InvalidFieldData = 11,
                DuplicateDataValues = 20,
                KeyIntegrityError = 30
            }

			private int m_iBatchID = -1, m_iStep = 0;
            //private DataTable m_oData = new DataTable(), m_oValidationErrors = new DataTable();
            private DataSet m_oDataSet = new DataSet();
            private XmlDocument m_oSettings = new XmlDocument(), m_oTemplate = new XmlDocument();
            
            private importCol[] m_oImportColumns;

            //private ImportFormat m_oImportFormat = ImportFormat.Unknown;

            public enum ImportFormat {
                Excel,
                CSV,
                XML,
                Unknown
            }

            //public import(string sTemplateXML) {
            //    m_oTemplate.LoadXml(sTemplateXML);
                
            //    xml.addXmlElement(m_oSettings,"data_settings").AppendChild( m_oSettings.ImportNode(m_oTemplate.SelectSingleNode("//data_settings"),true));
            //}


            public Literal Settings {
				//get {
				//    return convert.cLiteral(m_oTemplate.OuterXml);
				//}
                set {
					XmlDocument oDoc = new XmlDocument();
					oDoc.LoadXml(value.Text);					
					
					m_oTemplate.AppendChild(m_oTemplate.ImportNode(oDoc.SelectSingleNode("//template"),true));
					m_oSettings.AppendChild(m_oSettings.ImportNode(oDoc.SelectSingleNode("//data_settings"), true));                    
                }
            }


            public string SettingsXml {
                get {
                    return m_oSettings.OuterXml;
                }
                set {
                    m_oSettings.LoadXml(value);
                }
            }

			public int BatchID{
				get {
					return m_iBatchID;
				}
				set {
					m_iBatchID = value;
				}
			}

			public int Step {
				get {
					return m_iStep;
				}
				set {
					m_iStep = value;
				}
			}

            public bool HeaderRow {
                get {
                    return convert.cBool(getDataSetting("col_headers"));
                }
                set {
                    XmlNode oNode = m_oSettings;                    
                    setDataSetting("col_headers", value.ToString());
                }
            }

            public int NumCols {
                get {
                    return convert.cInt(this.getDataSetting("num_cols"));
                        //this.Data.Columns.Count-4;
                }
            }

            protected virtual void OnRowValidate(DataRow oRow) {
                if (RowValidate != null) RowValidate(this, oRow);                
            }

            protected virtual void OnDataValidate(DataTable oData) {
                if (DataValidate != null) DataValidate(this, oData);

            }

            protected virtual void OnColumnMatch() {
                if (ColumnMatch != null) ColumnMatch(this);
            }

            protected virtual void OnDataFix(DataTable oData) {

                //import_error_use_X == when importing duplicates, use this row and discard the others
                HttpRequest oRequest = System.Web.HttpContext.Current.Request;
                for (int x = 0; x < oRequest.Form.Count; x++) {
                    if (oRequest.Form.Keys[x].StartsWith("import_error_use_")) {
                        DataRow[] oErrors = ValidationErrors.Select("id=" + parse.replaceAll(oRequest.Form.Keys[x], "import_error_use_", ""));
                        sqlbuilder.setRowValues(Data, "counter", ValidationErrors.Select("id<>" + oErrors[0]["row"] + " and value='" + ado_helper.PrepareDB(oErrors[0]["value"]) + "'"), "ignore", true);
                        sqlbuilder.removeRows(ValidationErrors, "value='" + ado_helper.PrepareDB(oErrors[0]["value"]) + "'");
                    }


                    if (oRequest.Form.Keys[x].IndexOf("import_error_fix_") == 0 && convert.cStr(oRequest.Form[x]) != "") {
                        int iID = convert.cInt(parse.replaceAll(oRequest.Form.Keys[x], "import_error_fix_", ""));
                        DataRow[] oAffectedErrors = null;
                        try {
                            oAffectedErrors = ValidationErrors.Select("id=" + iID);
                        } catch (Exception) { }

                        if (oAffectedErrors!= null && oAffectedErrors.Length > 0 && convert.cStr(oRequest.Form["import_error_all_" + iID]) != "") oAffectedErrors = ValidationErrors.Select("value='" + ado_helper.PrepareDB(oAffectedErrors[0]["value"]) + "'");

                        if (oAffectedErrors != null && oAffectedErrors.Length > 0) {

                            if (oRequest.Form[x].Equals("ignore")) {
                                for (int y = 0; y < oAffectedErrors.Length; y++) {
                                    Data.Rows[convert.cInt(oAffectedErrors[y]["row"])]["ignore"] = true;
                                    sqlbuilder.removeRows(ValidationErrors, "row=" + oAffectedErrors[y]["row"]);
                                }
                            } else if (convert.cStr(oRequest.Form[x]) != "") {

                                object[] oReplacementPairs = null;
                                this.OnResolveValues(oAffectedErrors[0], Data.Rows[convert.cInt(oAffectedErrors[0]["row"])], oRequest.Form[x], ref oReplacementPairs);

                                if (oReplacementPairs != null) {
                                    for (int y = 0; y < oAffectedErrors.Length; y++) {
                                        for (int z = 0; z < oReplacementPairs.Length; z = z + 2)
                                            Data.Rows[convert.cInt(oAffectedErrors[y]["row"])][convert.cStr(oReplacementPairs[z])] = oReplacementPairs[z + 1];

                                        sqlbuilder.removeRows(ValidationErrors, "id=" + oAffectedErrors[y]["id"]);
                                        //iImport.Data.Rows[convert.cInt(oAffectedErrors[y]["row"])][iImport.Data.Columns[convert.cInt(oAffectedErrors[y]["col"])].Caption] = oNewValue;
                                        //sqlbuilder.removeRows(iImport.ValidationErrors, "id=" + oAffectedErrors[y]["id"]);
                                    }
                                }

                            }
                        }

                    }                   

                }

                //fix corrections from user
                string[] sArr = parse.split(oRequest.Form["import_includes"], ",");
                for (int x = 0; x < sArr.Length; x++) {
                    if (convert.cStr(oRequest.Form["import_include_" + sArr[x]]) == "") {
                        Data.Select("counter=" + sArr[x])[0]["ignore"] = true;
                        sqlbuilder.removeRows(ValidationErrors, "row=" + sArr[x]);
                    }
                }

                if (DataFix != null) DataFix(this, oData);
            }

            protected virtual void OnRowCommit(DataRow oImportRow) {
                if (!Data.Columns.Contains("fk_id")) Data.Columns.Add("fk_id", typeof(int));
                if (RowCommit != null) {
                    bool bReturnValue = true; DataRow oTableRow=null; ado_helper oData =null;
                    
                    if (this.getTemplateSetting("table_name") != "") {                        
                        oData = new ado_helper();
                        if( convert.cInt(oImportRow["fk_id"]) == 0 )
                            oTableRow = sqlbuilder.getNewRecord(oData, this.getTemplateSetting("table_name")).Rows[0];
                        else
                            oTableRow = sqlbuilder.executeSelect(oData, this.getTemplateSetting("table_name"), "id", oImportRow["fk_id"]).Rows[0];

                        for (int x = 0; x < ImportColumns.Length; x++) {
                            if (ImportColumns[x].DataMember != "" && ImportColumns[x].DataCol > -1) oTableRow[ImportColumns[x].DataMember] = oImportRow[ImportColumns[x].DataCol];
                        }
                    }                    
                    RowCommit(this, oImportRow, oTableRow, ref bReturnValue);
                    if (bReturnValue && oData != null) oData.Update();
                }
            }

            protected virtual void OnDataCommit(DataTable oData) {
                if (DataCommit != null) DataCommit(this, oData);

            }

            protected virtual void OnRenderErrors(DataTable oValidationErrors) {
                if (RenderErrors != null) RenderErrors(this, oValidationErrors);

            }

            protected virtual void OnResolveValues(DataRow oError, DataRow oRow, string sFormValue, ref object[] oReplacementPairs) {
                if (ResolveValues != null) ResolveValues(this, oError, oRow, sFormValue, ref oReplacementPairs);
            }                        

            public bool loadBatch(int iID) {
                BatchID = iID;
                DataTable oDT = sqlbuilder.executeSelect("d_version_batches", "id", iID);
                Settings = convert.cLiteral(io.read_file(convert.cStr(oDT.Rows[0]["template_url"])));
                if (convert.cStr(oDT.Rows[0]["settings"]) != "") m_oSettings.LoadXml(convert.cStr(oDT.Rows[0]["settings"]));
                if (convert.cStr(oDT.Rows[0]["data"]) != "") {
                    m_oDataSet = new DataSet("importer");
                    m_oDataSet.ReadXml(new StringReader(convert.cStr(oDT.Rows[0]["data"])));
                    if (m_oDataSet.Tables.Count == 0) m_oDataSet.Tables.Add(new DataTable("import"));
                    if (m_oDataSet.Tables.Count == 1) m_oDataSet.Tables.Add(new DataTable("errors"));
                }
                Step = convert.cInt(oDT.Rows[0]["step"]);
                return true;
            }

            public bool saveBatch() {
                ado_helper oData = new ado_helper();
                DataTable oDT = sqlbuilder.executeSelect(oData, "d_version_batches", "id", BatchID);
                oDT.Rows[0]["settings"] = m_oSettings.OuterXml;
                StringWriter oSWriter = new StringWriter();
                //m_oData.DataSet.WriteXml( oSWriter);

                m_oDataSet.WriteXml(oSWriter);
                oDT.Rows[0]["data"] = oSWriter.ToString();//m_oData.OuterXml;
                oDT.Rows[0]["step"] = Step;
                oData.Update();
                return true;
            }

            public string nextStep() {
                //validate info
                string sError = "";
                if (Step > 3) return "Invalid step";
                if (m_oTemplate.OuterXml == "") return "Template values haven't been loaded";
                if (m_oSettings.OuterXml == "") return "Data values haven't been loaded";
                if (Data.Rows.Count == 0) return "No data has been loaded or data not recognized";

                if (Step == 0) {
                    this.OnColumnMatch();
                } 
                
                if (Step == 1) {

                    string[] sDataCols = new string[this.NumCols];
                    for (int x = 0; x < sDataCols.Length; x++) sDataCols[x] = getSelectedColumnValue(x);

                    //Need to check that all required columns are included
                    //Need to check that no columns appear twice

                    for (int x = 0; x < ImportColumns.Length; x++) {

                        if (convert.isNumeric(ImportColumns[x].Value)) {
                            if (ImportColumns[x].Required) {
                                if (ImportColumns[x].DataCol < 0)
                                    //if (!parse.search(sDataCols, ImportColumns[x].Index.ToString()))
                                    sError += "Column " + ImportColumns[x].Title + " is mandatory for this import. Please correct and try again.<br>";
                            }
                            if (parse.search_index(sDataCols, parse.search_index(sDataCols, 0, ImportColumns[x].Index.ToString()) + 1, ImportColumns[x].Index.ToString()) > -1)
                                sError += "Column " + ImportColumns[x].Title + " can only appear once for this import. Please correct and try again.<br>";
                        }
                    }

                    if (sError != "")
                    {
                        this.OnColumnMatch();
                        return sError;
                    }
                    Step++;
                }
                if (Step == 2) {
                    if (ValidationErrors.Rows.Count == 0) {
                        ValidationErrors.Rows.Clear();
                        ValidationErrors.Columns.Clear();

                        ValidationErrors.Columns.Add("id", typeof(int));
                        ValidationErrors.Columns.Add("row", typeof(int));
                        ValidationErrors.Columns.Add("col", typeof(int));
                        ValidationErrors.Columns.Add("type", typeof(int));
                        ValidationErrors.Columns.Add("text", typeof(string));
                        ValidationErrors.Columns.Add("value", typeof(string)); //text used for grouping similar errors

                        for (int x = (HeaderRow ? 1 : 0); x < NumRows; x++) {
                            for (int y = 0; y < ImportColumns.Length; y++) {
                                if (ImportColumns[y].Required && convert.cStr(Data.Rows[x][ImportColumns[y].DataCol]) == "")
                                    addValidateError(x, ImportColumns[y].DataCol, ValidationErrorTypes.RequiredFieldMissing, "Field " + ImportColumns[y].Title + " is required.", "Field " + ImportColumns[y].Title + " is required.");
                            }

                            this.OnRowValidate(Data.Rows[x]);
                        }
                        this.OnDataValidate(Data);
                    }

                  
                    this.OnDataFix(ValidationErrors);

                    if (ValidationErrors.Rows.Count == 0) {
                        Step++;
                    } else {
                        this.OnRenderErrors(ValidationErrors);
                        return "We had problems importing some of your data. Please correct the items below before you continue.";
                    }
                }

                //perform actual import
                if (Step == 3) {
                    for (int x = 0; x < this.Data.Rows.Count; x++)
                        if (!convert.cBool(Data.Rows[x]["ignore"])) {
                            this.OnRowCommit(Data.Rows[x]);
                        }
                    this.OnDataCommit(this.Data);
                    //return "import step not completed yet";
                }

                Step++;
                return "";
            }


            public void addValidateError(int iRow, int iCol, ValidationErrorTypes oType, string sText, string sGroupingValue) {
                DataRow oRow = ValidationErrors.NewRow();
                ValidationErrors.Rows.Add(oRow);
                sqlbuilder.setValues(oRow, "id", ValidationErrors.Rows.Count - 1, "row", iRow, "col", iCol, "type", (int)oType, "text", sText, "value", sGroupingValue);
                Data.Rows[iRow]["errors"] = convert.cStr(Data.Rows[iRow]["errors"]) + oRow["id"] + ",";
            }

            public bool prevStep() {
                if (Step == 0) return false;
                Step--;
                return true;
            }

            public bool loadFile(string sUrl) {
                return loadFile(sUrl, ImportFormat.Unknown);
            }

            public bool loadFile(string sUrl, ImportFormat oFormat) {
                this.Step = 0;
                jlib.excel.ExcelFile oExcel = new jlib.excel.ExcelFile();
                if (sUrl.ToLower().IndexOf(".xls") > -1 || oFormat == ImportFormat.Excel) {
                    oExcel.LoadXls(sUrl);
                    oFormat = ImportFormat.Excel;
                } else {
                    oExcel.LoadCsv(sUrl, jlib.excel.CsvType.Unknown);
                    oFormat = ImportFormat.CSV;
                }

                if (oExcel.Worksheets[0].Rows.Count > 0) {
                    Data.Rows.Clear();
                    Data.Columns.Clear();

                    int iNumCols = 0;
                    for (int x = 0; x < oExcel.Worksheets[0].Rows.Count; x++)
                        if (oExcel.Worksheets[0].Rows[x].AllocatedCells.Count > iNumCols) iNumCols = oExcel.Worksheets[0].Rows[x].AllocatedCells.Count;


                    this.setDataSetting("num_cols", iNumCols.ToString());

                    for (int y = 0; y < iNumCols; y++) {
                        string sColname = "col_" + y;
                        if (ImportColumns[convert.cInt(getSelectedColumnValue(y))].ID != "" && !Data.Columns.Contains(ImportColumns[convert.cInt(getSelectedColumnValue(y))].ID)) sColname = ImportColumns[convert.cInt(getSelectedColumnValue(y))].ID;
                        Data.Columns.Add(sColname, typeof(string));
                    }

                    
                    Data.Columns.Add("counter", typeof(int));
                    Data.Columns.Add("errors", typeof(string));
                    Data.Columns.Add("fk_id", typeof(int));
                    Data.Columns.Add("ignore", typeof(bool), "false");

                    for (int x = 0; x < oExcel.Worksheets[0].Rows.Count; x++) {
                        bool bHasData = false;
                        DataRow oRow = Data.NewRow();
                        oRow["counter"] = x;
                        for (int y = 0; y < oExcel.Worksheets[0].Rows[x].AllocatedCells.Count; y++) {
                            oRow[y] = convert.cStr(oExcel.Worksheets[0].Cells[x, y].Value).Trim();
                            if (convert.cStr(oRow[y]).Trim() != "") bHasData = true;
                        }
                        oRow["errors"] = ",";
                        if (bHasData) Data.Rows.Add(oRow);
                    }
                }

                return true;
            }


            public int NumRows {
                get {
                    return Data.Rows.Count - (HeaderRow ? 1 : 0);
                }
            }

            public DataTable Data {
                get {
                    return m_oDataSet.Tables[0];
                }
            }

            public DataTable ValidationErrors {
                get {
                    return m_oDataSet.Tables[1];
                }
            }

            public string getTemplateSetting(string sSetting) {
                return xml.getXmlNodeValue(m_oTemplate, "//import_settings/setting[@id='" + sSetting + "']");
            }

            public string getDataSetting(string sSetting) {
                return xml.getXmlNodeValue(m_oSettings.SelectSingleNode("//data_settings"), "setting[@id='" + sSetting + "']");
            }

            public void setDataSetting(string sSetting, string sValue) {
                XmlNode oNode = m_oSettings.SelectSingleNode("//data_settings");
                if (oNode.SelectSingleNode("setting[@id='" + sSetting + "']") == null) {
                    XmlElement oNode1 = oNode.OwnerDocument.CreateElement("setting");
                    oNode1.SetAttribute("id", sSetting);
                    oNode.AppendChild(oNode1);
                }
                xml.setXmlNodeValue(oNode, "setting[@id='" + sSetting + "']", sValue);
            }

            public string getSelectedColumnValue(int iDataColIndex) {
                return getDataSetting("col_value_" + iDataColIndex);
            }
            public void setSelectedColumnValue(int iDataColIndex, string sValue) {
                setDataSetting("col_value_" + iDataColIndex, sValue);
                if( ImportColumns[convert.cInt(sValue)].ID != "" ){
                    if (Data.Columns.Contains(ImportColumns[convert.cInt(sValue)].ID)) {
                        Data.Columns[ImportColumns[convert.cInt(sValue)].ID].Caption = "col_" + Data.Columns.IndexOf(Data.Columns[ImportColumns[convert.cInt(sValue)].ID]);
                        Data.Columns[ImportColumns[convert.cInt(sValue)].ID].ColumnName = Data.Columns[ImportColumns[convert.cInt(sValue)].ID].Caption;
                    }
                    Data.Columns[iDataColIndex].Caption = ImportColumns[convert.cInt(sValue)].ID;
                    Data.Columns[iDataColIndex].ColumnName = Data.Columns[iDataColIndex].Caption;
                    ImportColumns[convert.cInt(sValue)].DataCol = iDataColIndex;
                }                
                if (Step == 0) Step++;
            }

            public string getColumnTitle(int iColIndex) {
                return parse.replaceAll(getTemplateSetting("column_header"), "{column_number}", (iColIndex + 1).ToString());
            }


            public importCol[] ImportColumns {
                get {
                    if (m_oImportColumns == null) {
                        XmlNodeList oCols = m_oTemplate.SelectNodes("//cols/col");
                        m_oImportColumns = new importCol[oCols.Count + 2];
                        m_oImportColumns[0] = new importCol("", "", "", getTemplateSetting("prompt_text"), "", false, 0);

                        for (int x = 0; x < oCols.Count; x++) {
                            m_oImportColumns[x + 1] = new importCol(xml.getXmlAttributeValue(oCols[x], "id"), xml.getXmlAttributeValue(oCols[x], "datamember"), xml.getXmlAttributeValue(oCols[x], "type"), xml.getXmlAttributeValue(oCols[x], "title"), x.ToString(), convert.cBool(xml.getXmlAttributeValue(oCols[x], "required")), x + 1);

                            for (int y = 0; y < NumCols; y++) {
                                if (getSelectedColumnValue(y) == convert.cStr(m_oImportColumns[x + 1].Index)) m_oImportColumns[x + 1].DataCol = y;
                            }
                        }

                        m_oImportColumns[oCols.Count + 1] = new importCol("", "", "", getTemplateSetting("ignore_text"), "-", false, oCols.Count + 1);
                    }
                    return m_oImportColumns;
                }
            }

            public importCol getColumnById(string sID) {
                if (convert.cStr(sID) == "") return null;
                for (int x = 0; x < ImportColumns.Length; x++)
                    if (ImportColumns[x].ID == sID) return ImportColumns[x];
                return null;
            }

            public importCol getColumnFromDataColumn(int iDataColumn) {
                return this.ImportColumns[convert.cInt(this.getSelectedColumnValue(iDataColumn))];
            }


            protected override void Render(HtmlTextWriter writer) {

                base.Render(writer);
            }

        }
    }