using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using jlib.components.button;
using jlib.db;
using jlib.functions;


namespace jlib.controls.scroller
{

    [ParseChildren( true )]
    public class index : System.Web.UI.UserControl
    {
		

        protected Control m_oParentControl;
		protected HtmlGenericControl ctrScroller;
        

        public PlaceHolder Content {
            set {
				ctrScroller.Controls.Clear();
                while ( value.Controls.Count > 0 )
					ctrScroller.Controls.Add(value.Controls[0]);
            }            
        }

		public Control ParentControl {
			get {				
				return m_oParentControl;
			}
			set {
				m_oParentControl = value;
			}
		}

		//public string ParentControlId {
		//    set {
		//        ArrayList oList = jlib.functions.controls.findControlById(Page, Page.Controls, value);
		//        if (oList.Count > 0) m_oParentControl = oList[0] as Control;
		//    }
		//}

		void Page_PreRender(object sender, EventArgs e) {
			Page.ClientScript.RegisterStartupScript(this.GetType(), this.UniqueID,
				@"var oScroller = $('" + ctrScroller.ClientID + "'); oScroller.ScrollParent = $('" + ParentControl.ClientID + "'); oScroller.scrollControl = fnScrollControl;Event.observe( window, \"resize\" , function(){ clearTimeout(oScroller.ScrollTimer); oScroller.ScrollTimer = setTimeout( function(){ oScroller.scrollControl();}, 400 ); } ); Event.observe( window, \"scroll\" , function(){ clearTimeout(oScroller.ScrollTimer); oScroller.ScrollTimer = setTimeout( function(){ oScroller.scrollControl();}, 400 ); } );", true);            
		}                
    }
}
