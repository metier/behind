﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using jlib.DataFramework.Validation;

namespace jlib.Web.WebApiFramework
{
	public class WebApiResponse<T>
	{
		public ValidationSummary Status { get; set; }

		public T Results { get; set; }

	}
}
