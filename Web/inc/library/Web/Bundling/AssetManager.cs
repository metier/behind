﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Web;
using System.Web.Optimization.Resources;

namespace jlib.Web.Bundling
{

    internal sealed class AssetManager
    {
        private System.Web.Optimization.BundleCollection _bundles;
        private readonly HttpContextBase _httpContext;
        private bool? _optimizationEnabled;
        private System.Web.Optimization.IBundleResolver _resolver;
        private Func<string, string, string> _resolveUrlMethod;
        internal static readonly object AssetsManagerKey = typeof(AssetManager);

        public AssetManager(HttpContextBase context)
        {
            this._httpContext = context;
        }

        private IEnumerable<AssetTag> DeterminePathsToRender(IEnumerable<string> assets)
        {
            List<AssetTag> refs = new List<AssetTag>();
            foreach (string str in assets)
            {
                if (this.Resolver.IsBundleVirtualPath(str))
                {
                    if (!this.OptimizationEnabled)
                    {
                        foreach (string str2 in this.Resolver.GetBundleContents(str))
                        {
                            refs.Add(new AssetTag(str2));
                        }
                    }
                    else
                    {
                        refs.Add(new AssetTag(str));
                        if (this.Bundles.UseCdn)
                        {
                            System.Web.Optimization.Bundle bundleFor = this.Bundles.GetBundleFor(str);
                            if (((bundleFor != null) && !string.IsNullOrEmpty(bundleFor.CdnPath)) && !string.IsNullOrEmpty(bundleFor.CdnFallbackExpression))
                            {
                                AssetTag item = new AssetTag(string.Format(CultureInfo.InvariantCulture, "CdnFallBackScriptString=<script>({0})||document.write('<script src=\"{1}\"></script>');</script>", new object[] { bundleFor.CdnFallbackExpression, this.ResolveVirtualPath(str) })) {
                                    IsStaticAsset = true
                                };
                                refs.Add(item);
                            }
                        }
                    }
                }
                else
                {
                    refs.Add(new AssetTag(str));
                }
            }
            return this.EliminateDuplicatesAndResolveUrls(refs);
        }

        private IEnumerable<AssetTag> EliminateDuplicatesAndResolveUrls(IEnumerable<AssetTag> refs)
        {
            List<AssetTag> source = new List<AssetTag>();
            HashSet<string> set = new HashSet<string>();
            HashSet<string> bundledContents = new HashSet<string>();
            System.Web.Optimization.IBundleResolver resolver = this.Resolver;
            foreach (AssetTag tag in refs)
            {
                if (tag.IsStaticAsset)
                {
                    source.Add(tag);
                }
                else
                {
                    string item = tag.Value;
                    if (!set.Contains(item))
                    {
                        if (resolver.IsBundleVirtualPath(item))
                        {
                            foreach (string str2 in resolver.GetBundleContents(item))
                            {
                                bundledContents.Add(this.ResolveVirtualPath(str2));
                            }
                            tag.Value = resolver.GetBundleUrl(item);
                            source.Add(tag);
                        }
                        else
                        {
                            string str3 = this.ResolveVirtualPath(item);
                            if (!set.Contains(str3))
                            {
                                set.Add(str3);
                                tag.Value = str3;
                                source.Add(tag);
                            }
                        }
                        set.Add(item);
                    }
                }
            }
            return source.Where<AssetTag>(delegate (AssetTag asset) {
                if (!asset.IsStaticAsset)
                {
                    return !bundledContents.Contains(asset.Value);
                }
                return true;
            });
        }

        public static AssetManager GetInstance(HttpContextBase context)
        {
            if (context == null)
            {
                return null;
            }
            AssetManager manager = (AssetManager) context.Items[AssetsManagerKey];
            if (manager == null)
            {
                manager = new AssetManager(context);
                context.Items[AssetsManagerKey] = manager;
            }
            return manager;
        }

        public IHtmlString RenderExplicit(string tagFormat, params string[] paths)
        {
            IEnumerable<AssetTag> enumerable = this.DeterminePathsToRender(paths);
            StringBuilder builder = new StringBuilder();
            foreach (AssetTag tag in enumerable)
            {
                builder.Append(tag.Render(tagFormat));
                builder.Append(Environment.NewLine);
            }
            return new HtmlString(builder.ToString());
        }

        internal HtmlString ResolveUrl(string url)
        {
            if (this.Resolver.IsBundleVirtualPath(url))
            {
                return new HtmlString(this.Bundles.ResolveBundleUrl(url));
            }
            return new HtmlString(this.ResolveVirtualPath(url));
        }

        internal string ResolveVirtualPath(string virtualPath)
        {
            Uri uri;
            if (Uri.TryCreate(virtualPath, UriKind.Absolute, out uri))
            {
                return virtualPath;
            }
            string appRelativeCurrentExecutionFilePath = "";
            if (this._httpContext.Request != null)
            {
                appRelativeCurrentExecutionFilePath = this._httpContext.Request.AppRelativeCurrentExecutionFilePath;
            }
            return this.ResolveUrlMethod(appRelativeCurrentExecutionFilePath, virtualPath);
        }

        internal System.Web.Optimization.BundleCollection Bundles
        {
            get
            {
                return (this._bundles ?? System.Web.Optimization.BundleTable.Bundles);
            }
            set
            {
                this._bundles = value;
				//this._bundles.Context = this.Context;

				this._bundles
						.GetType()
						.GetProperty("Context", System.Reflection.BindingFlags.NonPublic)
						.SetValue(this._bundles, this.Context);

                
            }
        }

        internal HttpContextBase Context
        {
            get
            {
                return this._httpContext;
            }
        }

        internal bool OptimizationEnabled
        {
            get
            {
                if (this._optimizationEnabled.HasValue)
                {
                    return this._optimizationEnabled.Value;
                }
                return System.Web.Optimization.BundleTable.EnableOptimizations;
            }
            set
            {
                this._optimizationEnabled = new bool?(value);
            }
        }

        internal System.Web.Optimization.IBundleResolver Resolver
        {
            get
            {
                return (this._resolver ?? System.Web.Optimization.BundleResolver.Current);
            }
            set
            {
                this._resolver = value;
            }
        }

        internal Func<string, string, string> ResolveUrlMethod
        {
            get
            {
				if(this._resolveUrlMethod != null) 
					return this._resolveUrlMethod;

                return (basePath, relativePath) => UrlUtil.Url(basePath, relativePath);
            }
            set
            {
                this._resolveUrlMethod = value;
            }
        }

        internal class AssetTag
        {
            public AssetTag(string value)
            {
                this.Value = value;
            }

            public string Render(string tagFormat)
            {
                if (this.IsStaticAsset)
                {
                    return this.Value;
                }


				//Append version to Path
				var path = this.Value;

				

				//Locate File
				if(path.IndexOf("?") == -1 && path.IndexOf("=") == -1)
				{ 
					//Check for min version
					if(path.IndexOf(".min.", StringComparison.CurrentCultureIgnoreCase) == -1)
					{
						var minPath = path.Replace(".css", ".min.css").Replace(".js", ".min.js");
						if(System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(minPath)))
						{
							path = minPath;
						}
					}

					var file = System.Web.Hosting.HostingEnvironment.MapPath(path);
					if(System.IO.File.Exists(file))
					{
						var fileInfo = new System.IO.FileInfo(file);

						//Get Last Modified Date
						var lastModifiedDate = fileInfo.LastWriteTime;

						//Add path
						path = String.Format("{0}?t={1:HHmmfff}", path, lastModifiedDate);
					}
				}


				//return
                return string.Format(CultureInfo.InvariantCulture, tagFormat, new object[] { HttpUtility.UrlPathEncode(path) });
            }

            public bool IsStaticAsset { get; set; }

            public string Value { get; set; }
        }
    }
}
