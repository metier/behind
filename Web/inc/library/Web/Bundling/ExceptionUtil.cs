﻿using System;
using System.Globalization;
using System.Web.Optimization.Resources;

namespace jlib.Web.Bundling
{

    internal static class ExceptionUtil
    {
        internal static bool IsPureWildcardSearchPattern(string searchPattern)
        {
            if (!string.IsNullOrEmpty(searchPattern))
            {
                string a = searchPattern.Trim();
                if (string.Equals(a, "*", StringComparison.OrdinalIgnoreCase) || string.Equals(a, "*.*", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }

        internal static ArgumentException ParameterNullOrEmpty(string parameter)
        {
            return new ArgumentException(string.Format(CultureInfo.CurrentCulture, "Parameter_NullOrEmpty=The string parameter '{0}' cannot be null or empty.", new object[] { parameter }), parameter);
        }

        internal static ArgumentException PropertyNullOrEmpty(string property)
        {
            return new ArgumentException(string.Format(CultureInfo.CurrentCulture, "Property_NullOrEmpty=The value assigned to property '{0}' cannot be null or empty.", new object[] { property }), property);
        }

        internal static Exception ValidateVirtualPath(string virtualPath, string argumentName)
        {
            if (string.IsNullOrEmpty(virtualPath))
            {
                return ParameterNullOrEmpty(argumentName);
            }
            if (!virtualPath.StartsWith("~/", StringComparison.OrdinalIgnoreCase))
            {
                return new ArgumentException(string.Format(CultureInfo.CurrentCulture, "UrlMappings_only_app_relative_url_allowed=The URL '{0}' is not valid. Only application relative URLs (~/url) are allowed.", new object[] { virtualPath }), argumentName);
            }
            return null;
        }
    }


}
