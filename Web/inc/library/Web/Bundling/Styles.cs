﻿using System;
using System.Web;

namespace jlib.Web.Bundling
{

    public static class Styles
    {
        private static HttpContextBase _context;
        private static string _defaultTagFormat = "<link href=\"{0}\" rel=\"stylesheet\"/>";

        public static IHtmlString Render(params string[] paths)
        {
            return RenderFormat(DefaultTagFormat, paths);
        }

        public static IHtmlString RenderFormat(string tagFormat, params string[] paths)
        {
            if (string.IsNullOrEmpty(tagFormat))
            {
                throw ExceptionUtil.ParameterNullOrEmpty("tagFormat");
            }
            if (paths == null)
            {
                throw new ArgumentNullException("paths");
            }
            foreach (string str in paths)
            {
                if (string.IsNullOrEmpty(str))
                {
                    throw ExceptionUtil.ParameterNullOrEmpty("paths");
                }
            }
            return Manager.RenderExplicit(tagFormat, paths);
        }

        public static IHtmlString Url(string virtualPath)
        {
            return Manager.ResolveUrl(virtualPath);
        }

        internal static HttpContextBase Context
        {
            get
            {
                return (_context ?? new HttpContextWrapper(HttpContext.Current));
            }
            set
            {
                _context = value;
            }
        }

        public static string DefaultTagFormat
        {
            get
            {
                return _defaultTagFormat;
            }
            set
            {
                _defaultTagFormat = value;
            }
        }

        private static AssetManager Manager
        {
            get
            {
                return AssetManager.GetInstance(Context);
            }
        }
    }
}
