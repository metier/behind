﻿using System;
using System.Web;

namespace jlib.Web.Bundling
{

    internal static class UrlUtil
    {
        internal static string Url(string basePath, string path)
        {
            if (basePath != null)
            {
                path = VirtualPathUtility.Combine(basePath, path);
            }
            path = VirtualPathUtility.ToAbsolute(path);
            return HttpUtility.UrlPathEncode(path);
        }
    }
}

