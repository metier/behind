﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Optimization;

namespace jlib.Web.Bundling
{
	public class JsBundle : Bundle
	{
        public JsBundle(string virtualPath) : base(virtualPath)
        {
        }

        public JsBundle(string virtualPath, string cdnPath) : base(virtualPath, cdnPath)
        {
            base.ConcatenationToken = ";" + "\n\n\n" + "/*********************************************/" + "\n\n\n";
        }
    }
}

