﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using jlib.functions;

namespace jlib.helpers {
    public class net {
        public static string getICalEvent(DateTimeOffset oStartDate, DateTimeOffset oEndDate, string sDescription, string sLocation, string sSummary, string sOrganizerName, string sOrganizerEmail, string sUrl, string sCategory, string sPriority) {
            return "BEGIN:VCALENDAR\n" +
       "PRODID:-//von Tangen and Sons//NONSGML jLib//EN\n" +
       "VERSION:2.0\n" +
       "BEGIN:VEVENT\n" +
       "DTSTART:" + parse.replaceAll(String.Format("{0:u}", oStartDate.ToUniversalTime()), ":", "", "-", "", " ", "T") + "\n" +
       "DTEND:" + parse.replaceAll(String.Format("{0:u}", oEndDate.ToUniversalTime()), ":", "", "-", "", " ", "T") + "\n" +
       (sCategory == "" ? "" : "CATEGORIES:" + sCategory.ToUpper() + "\n") +
       "LOCATION;ENCODING=QUOTED-PRINTABLE:" + convert.cQuotedPrintable(sLocation) + "\n" +
       "DESCRIPTION;ENCODING=QUOTED-PRINTABLE:" + convert.cQuotedPrintable(sDescription) + "\n" +
       "SUMMARY;ENCODING=QUOTED-PRINTABLE:" + convert.cQuotedPrintable(sSummary) + "\n" +
       (sOrganizerName + sOrganizerEmail != "" ? "ORGANIZER;CN=\"" + convert.cStr(sOrganizerName, sOrganizerEmail) + "\":mailto:" + sOrganizerEmail + "\n" : "") +
       "TRIGGER;RELATED=START:-PT12H\n" +
       (sUrl == "" ? "" : "URL:" + sUrl + "\n") +
       (sPriority==""?"": "PRIORITY:"  + sPriority + "\n") +
       "END:VEVENT\n" +
       "END:VCALENDAR";
        }

        public class nav {
            public enum redirectOptions {
                None,
                R301,
                R302
            }
            public class navOptions {
                public XmlNode XmlNav = null;
                public redirectOptions Options = redirectOptions.None;
                public DateTime? SiteMapDate;
                public double SitemapPriority = 1;
                public navOptions(XmlNode oNode, redirectOptions oOptions, DateTime? oSitemapDate, double dSitemapPriority) {
                    XmlNav = oNode;
                    Options = oOptions;
                    SiteMapDate = oSitemapDate;
                    SitemapPriority = (dSitemapPriority == 1 ? convert.cDbl(xml.getXmlAttributeValue(oNode, "priority"), dSitemapPriority) : dSitemapPriority);
                    if (oNode != null && xml.getXmlAttributeValue(oNode, "redirect") == "R301") Options = redirectOptions.R301;
                    else if (oNode != null && xml.getXmlAttributeValue(oNode, "redirect") == "R302") Options = redirectOptions.R302;
                }
            }
            public class navTree {
                public XmlDocument Structure = new XmlDocument();                
                public List<string> Files = new List<string>();
                public List<string> Targets = new List<string>();
                public List<string> TargetsLowerCase = new List<string>();
                public List<navOptions> Data = new List<navOptions>();
                public DateTime SitemapDate = DateTime.MinValue;
                public DateTime NavObjectDate = DateTime.Now;
                private bool m_bHasManualRedirects = false;
                public bool HasManualRedirects {
                    get {
                        return m_bHasManualRedirects;
                    }
                }
                public bool addRedirect(string sSource, string sTarget) {
                    return addRedirect(sSource, sTarget, redirectOptions.None);
                }
                public bool addRedirect(string sSource, string sTarget, redirectOptions oOptions) {
                    return addRedirect(sSource, sTarget, new navOptions(null, oOptions, null, 1));
                }
                public bool addRedirect(string sSource, string sTarget, redirectOptions oOptions, DateTime oSitemapDate, double dSitemapPriority) {
                    return addRedirect(sSource, sTarget, new navOptions(null, oOptions, oSitemapDate, dSitemapPriority));
                }
                public bool addRedirect(string sSource, string sTarget, navOptions oOptions) {
                    //if (Files.Contains(sSource) || (sSource == sTarget && oOptions.XmlNav==null)) return false;
                    if ((sSource == sTarget && oOptions.XmlNav == null)) return false;
                    Files.Add(sSource);
                    Targets.Add(sTarget);
                    TargetsLowerCase.Add(sTarget.ToLower());
                    Data.Add(oOptions);
                    if (oOptions==null || oOptions.XmlNav==null) m_bHasManualRedirects = true;
                    return true;
                }
                public string getActualPath(string sFilePath) {
                    return getActualPath(sFilePath, false);
                }
                public string getActualPath(string sFilePath, bool bAllowResponseRedirect) {
                    return getRedirect(sFilePath, bAllowResponseRedirect, true);
                }
                
                public string getRedirect(string sFilePath) {
                    return getRedirect(sFilePath, false);
                }
                public string getRedirect(string sFilePath, bool bAllowResponseRedirect) {
                    return getRedirect(sFilePath, bAllowResponseRedirect, false);
                }
                public string getRedirect(string sFilePath, bool bAllowResponseRedirect, bool bReverseLookup) {
                    List<string> oSource = (bReverseLookup ? TargetsLowerCase : Files);
                    List<string> oDest = (!bReverseLookup ? TargetsLowerCase : Files);
                    if (oSource.Count == 0) return sFilePath;
                    List<int> iIndexList = new List<int>();
                    string sBaseUrl = (sFilePath.Contains("://") && sFilePath.IndexOf("/", sFilePath.IndexOf("://")+3)>-1 ? sFilePath.Substring(sFilePath.IndexOf("/", sFilePath.IndexOf("://") + 3)) : sFilePath);
                    string sQueryString = "";
                    if (oSource.Contains(sBaseUrl.ToLower())) {
                        iIndexList = oSource.IndexOfMulti(sBaseUrl.ToLower());
                    }
                    if (iIndexList.Count == 0) {
                        sQueryString = (sFilePath.Contains("?") ? sFilePath.Substring(sFilePath.IndexOf("?")) : "");
                        if (sBaseUrl.Contains("?")) {
                            sBaseUrl = sBaseUrl.Substring(0, sBaseUrl.Length-sQueryString.Length);
                            if (oSource.Contains(sBaseUrl.ToLower())) iIndexList = oSource.IndexOfMulti(sBaseUrl.ToLower());                                
                        }
                        if (iIndexList.Count == 0) return sFilePath;
                    }
                    int iIndex = (bReverseLookup ? iIndexList[0] : -1);
                    for(int x=0;x<iIndexList.Count;x++){
                        if (Data[iIndexList[x]].Options == redirectOptions.None) {
                            iIndex = iIndexList[x];
                            break;
                        }
                    }
                    if (iIndex == -1) return sFilePath;
                    string sDest = oDest[iIndex];
                    if (sDest.Contains("?") && sQueryString != "") {
                        sQueryString = jlib.net.HTTP.mergeQueryStrings(sDest.Substring(sDest.IndexOf("?")), sQueryString);
                        sDest = sDest.Substring(0, sDest.IndexOf("?"));
                    }
                    if (Data[iIndex].Options == redirectOptions.R301 && bAllowResponseRedirect) System.Web.HttpContext.Current.Response.RedirectPermanent(sDest + sQueryString, true);
                    else if (Data[iIndex].Options == redirectOptions.R302 && bAllowResponseRedirect) System.Web.HttpContext.Current.Response.Redirect(sDest + sQueryString, true);
                    return sDest + sQueryString;
                }

                public bool writeSitemap(string sFileName) {
                    XmlDocument xmlSitemap = new XmlDocument();
                    xmlSitemap.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        //"<?xml-stylesheet type=\"text/xsl\" href=\"/inc/library/media/xsl/gss.xsl\"?>\n" +
"<urlset xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 "+
                    "http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\" xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"></urlset>");
                    for (int x = 0; x < Targets.Count; x++)
                        if (Targets.IndexOf(Targets[x]) == x && Data[x].Options== redirectOptions.None)
                            xml.setXmlNodeValue(xml.addXmlElement(xmlSitemap.DocumentElement, "url"), 
                                "loc", System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Host + Targets[x],
                                "lastmod", String.Format("{0:yyyy-MM-dd}",(Data[x].SiteMapDate.HasValue? Data[x].SiteMapDate.Value : System.IO.File.GetLastWriteTime( System.Web.HttpContext.Current.Server.MapPath(Files[x].Contains("?")? Files[x].Substring(0,Files[x].IndexOf("?")):Files[x]) ))),
                                "priority",Data[x].SitemapPriority
                                );

                    io.write_file(System.Web.HttpContext.Current.Server.MapPath(sFileName), parse.replaceAll(xmlSitemap.OuterXml," xmlns=\"\"",""), false, UTF8Encoding.UTF8);
 //                    <url>
 // <loc>http://lexicontech.com/overstock/opticon-opv1001-p-455.html</loc>
 // <lastmod>2010-10-11</lastmod>
 // <priority>0.98</priority>
 //</url>

                    return true;
                }
            }
            private static navTree m_oNavTree = null;
            public static object NavTreeLock = new object();
            public static void resetNavTree() {
                lock (NavTreeLock) m_oNavTree = null;
            }
            public static navTree getNavTree() {
                lock (NavTreeLock) {
                    if (m_oNavTree != null && (m_oNavTree.NavObjectDate < DateTime.Now.AddHours(-1) || System.Configuration.ConfigurationManager.AppSettings["rewriter.site.structure"] != null && System.IO.File.GetLastWriteTime(System.Web.HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["rewriter.site.structure"])) != m_oNavTree.SitemapDate)) m_oNavTree = null;
                    if (m_oNavTree == null) {
                        m_oNavTree = new navTree();
                        if (System.Configuration.ConfigurationManager.AppSettings["rewriter.site.structure"] != null) {
                            string sPath = System.Web.HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["rewriter.site.structure"]);
                            m_oNavTree.SitemapDate = System.IO.File.GetLastWriteTime(sPath);
                            m_oNavTree.NavObjectDate = DateTime.Now;
                            m_oNavTree.Structure = new XmlDocument();
                            m_oNavTree.Structure.Load(sPath);
                            //jlib.structures.multi_dictionary<string, string, XmlNode> oStructure = new jlib.structures.multi_dictionary<string, string, XmlNode>();
                            XmlNodeList xmlNodes = m_oNavTree.Structure.SelectNodes("//map");
                            for (int x = 0; x < xmlNodes.Count; x++) m_oNavTree.addRedirect(xml.getXmlAttributeValue(xmlNodes[x], "url"), xml.getXmlAttributeValue(xmlNodes[x], "target"), new navOptions(xmlNodes[x], redirectOptions.None, null, 1));
                        }
                    }
                    return m_oNavTree;
                }
            }            
        }
    }
}
