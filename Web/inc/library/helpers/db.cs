using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using jlib.components;
using jlib.db;
using jlib.functions;

namespace jlib.helpers {
    public class db : jlib.db.ado_helper {

        //public static XmlDocument saveVersion( DataRow oRow, string sTableName ) {
            
        //    int iOriginalID = 0;
        //    if( oRow.Table.Columns.Contains( "ID" ) ){
        //        iOriginalID = convert.cInt(oRow["ID" ]);
        //    }else if( oRow.Table.PrimaryKey != null && oRow.Table.PrimaryKey.Length > 0 ){
        //        iOriginalID = convert.cInt( oRow[oRow.Table.PrimaryKey[0].ColumnName] );
        //    }

        //    return saveVersion( oRow, sTableName, iOriginalID );
        //}                

        public static XmlDocument saveVersion(DataRow oRow, string sTableName, int iOriginalID, bool bAwaitingApproval, int iAuthorID) {
            return saveVersion(oRow, sTableName, iOriginalID, bAwaitingApproval, iAuthorID, 0);
        }
        public static XmlDocument saveVersion( DataRow oRow, string sTableName, int iOriginalID, bool bAwaitingApproval, int iAuthorID, int iBatchID ) {
            
            XmlDocument oDoc = new XmlDocument();
            oDoc.LoadXml( "<version><fields></fields></version>" );
            for ( int x = 0; x < oRow.Table.Columns.Count; x++ ) {
                XmlNode xmlEl = oDoc.CreateElement( "field" );
                oDoc.SelectSingleNode( "//fields" ).AppendChild( xmlEl );
                xml.setXmlAttributeValue( xmlEl, "name", oRow.Table.Columns[x].ColumnName );
                xmlEl.InnerText = convert.cStr( oRow[x] );
            }

            sqlbuilder.executeInsert("d_versions", "data", oDoc.OuterXml, "table_name", sTableName, "original_id", iOriginalID, "awaiting_approval", 1, "author_id", iAuthorID, "batch_id", iBatchID);

            return oDoc;
        }

        public static DataRow loadVersion( int iID ) {            
            return loadVersion( iID, new ado_helper() );
        }

        public static DataRow loadVersion( int iID, ado_helper oData ) {

            DataTable oDT = sqlbuilder.executeSelect( "d_versions", "id", iID );
            DataRow oNewRow = null;
            if( convert.cInt( oDT.Rows[0]["original_id"] ) > 0)
                oNewRow = sqlbuilder.getDataRow(oData,  convert.cStr( oDT.Rows[0]["table_name"] ), "id", oDT.Rows[0]["original_id"] );
            else
                oNewRow = sqlbuilder.getNewRecord(oData, convert.cStr( oDT.Rows[0]["table_name"] ) ).Rows[0];
                
            XmlDocument oDoc = new XmlDocument();
            oDoc.LoadXml( convert.cStr( oDT.Rows[0]["data"] ) );

            XmlNodeList oNodes = oDoc.SelectNodes( "//field" );
            for( int x = 0; x < oNodes.Count; x++ ){
                if( oNewRow.Table.Columns.Contains( oNodes[x].Attributes[ "name" ].Value )){
                    
                    if ( oNewRow.Table.Columns[oNodes[x].Attributes["name"].Value].DataType.FullName == "System.String" ) {
                        oNewRow[oNodes[x].Attributes["name"].Value] = oNodes[x].InnerText;
                    } else if ( oNewRow.Table.Columns[oNodes[x].Attributes["name"].Value].DataType.FullName == "System.Boolean" ) {
                        oNewRow[oNodes[x].Attributes["name"].Value] = convert.cBool( oNodes[x].InnerText );
					} else if (oNewRow.Table.Columns[oNodes[x].Attributes["name"].Value].DataType.FullName == "System.DateTime" && oNodes[x].InnerText == "") {
						oNewRow[oNodes[x].Attributes["name"].Value] = DBNull.Value;							
                    } else {
                        oNewRow[oNodes[x].Attributes["name"].Value] = oNodes[x].InnerText;
                    }

                }
            }

            return oNewRow;
        }
    }
}
