﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jlib.controls.image_upload;
using jlib.functions;

namespace jlib.helpers
{
	public static class Razor
	{
        private static string GetRazorFileContent(string FilePath)
        {
            //Check if file exists            
            if (!System.IO.File.Exists(FilePath))
            {
                if (FilePath.StartsWith("/") && System.Web.HttpContext.Current != null)
                    FilePath = System.Web.HttpContext.Current.Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath + FilePath);

                if (FilePath.StartsWith("~"))
                {
                    if(System.Web.HttpContext.Current != null)
                        FilePath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + FilePath.Substring(1);
                    else
                        FilePath = System.AppDomain.CurrentDomain.BaseDirectory + FilePath.Substring(1);                    
                }                                        

                if (!System.IO.File.Exists(FilePath))
                    return "";
            }

            //Get Template Source
            return System.IO.File.ReadAllText(FilePath);
        }
        public static string RenderRazorTemplate<T>(string filePath, T Parameters)
		{            
            string template = GetRazorFileContent(filePath);
            if (template.IsNullOrEmpty())
                return "";
            
			//Call Method passing in source
			return RenderRazorTemplate<T>(null, template, Parameters);

		}

		public static string RenderRazorTemplate<T>(string PreSource, string Source, T Parameters)
		{
			//Get Template Source
			string template = PreSource.Str() + Source.Str();
			
			//This is required to force loading of the CSharp assembly (or RazorEngine might throw an error)
			bool loaded = typeof(Microsoft.CSharp.RuntimeBinder.Binder).Assembly != null;

			//Declare output
			string output = "";

			//Execute
			try
			{
				System.Reflection.Assembly CallingAsembly = System.Reflection.Assembly.GetEntryAssembly();
				if (CallingAsembly != null)
				{
					List<System.Reflection.AssemblyName> loadedAssemblies = new List<System.Reflection.AssemblyName>();
					RazorEngine.Compilation.CompilerServicesUtility.GetLoadedAssemblies().ToList().ForEach(x => { loadedAssemblies.Add(x.GetName()); });
					CallingAsembly.GetReferencedAssemblies().ToList().ForEach(x =>
					{
						if (!loadedAssemblies.Contains(x))
						{
							System.Reflection.Assembly.Load(x.FullName);
							loadedAssemblies.Add(x);
						}
					});
				}

                var config = new RazorEngine.Configuration.TemplateServiceConfiguration
                {                    
                    //EncodedStringFactory = new RazorEngine.Text.HtmlEncodedStringFactory(),// .RawStringFactory(),

                    Resolver = new RazorEngine.Templating.DelegateTemplateResolver(name =>
                    {                        
                        var content = GetRazorFileContent(name);
                        return content;
                    })
                };

                // Try to render output using Razor
                //using (var service = new RazorEngine.Templating.TemplateService(config))
                //{                    
                //    output = service.Parse(template, Parameters,null,null);                    
                //}
                RazorEngine.Razor.SetTemplateService(new RazorEngine.Templating.TemplateService(config));
                output = RazorEngine.Razor.Parse(template, Parameters, null);
			}
			catch (RazorEngine.Templating.TemplateCompilationException e)
			{
				string Message = "RenderRazorTemplate Error -- Could not execute source: " + template;

				if (e.Errors != null)
				{
					Message += "\n -------------- TemplateCompilationException Error Variables ------------ \n";
					foreach (System.CodeDom.Compiler.CompilerError compileError in e.Errors)
						Message += "* Line:" + (compileError.Line - 32) + ": (" + compileError.ErrorNumber + ") " + compileError.ErrorText + "\n";
				}
				throw new Exception(Message, e);
			}

			/*
			catch (RazorEngine.Templating.TemplateParsingException e)
			{
				//Get Source Lines
				var sourceLines = template.Split(new char[] { '\n' }, StringSplitOptions.None);

				int startLine = Math.Max(e.Line - 3, 0);
				int endLine = Math.Min(e.Line + 3, sourceLines.Count());

				//Create new Exception
				e.Source = String.Join("\n", sourceLines.Skip(startLine).Take(endLine - startLine));
				throw;

			}
			*/
			catch (Exception e)
			{
				throw;
			}
			

			//return output
			return parse.replaceAll(output,"\r\n","\n");

		}

	}
}
