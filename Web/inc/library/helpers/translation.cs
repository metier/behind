﻿using System;
using System.Web;
using System.Web.Configuration;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using jlib.functions;

namespace jlib.helpers {
    public class translation {        
        private static Dictionary<string, string> Translations = null;
        private static Dictionary<string, Dictionary<string,string>> PageSettings = null;
        private static object _lock = new object();
        private static DateTime  m_oNextFileModDateCheck;
        private static string BuildDictionaryKey(string Language, string Key, string SiteId, string CompanyId, string Url)
        {
            return Language + ":" + Key + ":" + SiteId + ":" + CompanyId + ":" + Url;
        }
        public static Dictionary<string, string> getEntries(string Language, System.Web.UI.Page CurrentPage, List<string> Pages, string SiteId=null, string CompanyId = null) {
            components.webpage webPage = CurrentPage as components.webpage;
            if (webPage != null)
            {
                if (SiteId.IsNullOrEmpty())
                    SiteId = webPage.TranslationSiteId;
                if (CompanyId.IsNullOrEmpty())
                    CompanyId = webPage.TranslationCompanyId;
            }
            Dictionary<string, string> Entries = new Dictionary<string, string>();
            if (Translations == null || m_oNextFileModDateCheck < DateTime.Now) reload();

            Language = Language.Str();
            if (Translations.Count == 0 || Language == "") return Entries;
            lock (_lock) {
                foreach (string Page in Pages)
                    if (PageSettings.ContainsKey(Language + ":" + Page))
                        Language = convert.cStr(PageSettings.SafeGetValue(Language + ":" + Page).SafeGetValue("redirect-language"), Language);
                    //default to english if invalid language specified
                    else if (PageSettings.ContainsKey("en:" + Page))
                        Language = convert.cStr(PageSettings.SafeGetValue("en" + ":" + Page).SafeGetValue("redirect-language"), "en");

                foreach (string Page in Pages) {
                    Dictionary<string, string> PageSetting = PageSettings.SafeGetValue(Language + ":" + Page);
                    if (PageSetting != null) {
                        
                        foreach (KeyValuePair<string, string> Entry in Translations) {
                            if (Entry.Key.StartsWith(Language + ":") && Entry.Key.EndsWith(Page)) {
                                string[] keyParts = parse.split(Entry.Key, ":");
                                string key = keyParts[1];
                                if (!Entries.ContainsKey(key)) {
                                    string value = "";
                                    if(Translations.TryGetValue(BuildDictionaryKey(Language, key, SiteId, CompanyId, Page), out value)) Entries.Add(key, value);
                                    else if (SiteId.IsNotNullOrEmpty() && CompanyId.IsNotNullOrEmpty() && Translations.TryGetValue(BuildDictionaryKey(Language, key, "", CompanyId, Page), out value)) Entries.Add(key, value);
                                    else if (SiteId.IsNotNullOrEmpty() && CompanyId.IsNotNullOrEmpty() && Translations.TryGetValue(BuildDictionaryKey(Language, key, SiteId, "", Page), out value)) Entries.Add(key, value);
                                    else if (Translations.TryGetValue(BuildDictionaryKey(Language, key, "", "", Page), out value)) Entries.Add(key, value);

                                }                                
                            }
                        }
                    }
                }
            }
            return Entries;
        }

        public static void reload() {
            lock (_lock) {
                Translations = new Dictionary<string, string>();
                PageSettings = new Dictionary<string,Dictionary<string,string>>();
                m_oNextFileModDateCheck = DateTime.Now.AddMinutes(5);
                string FileNameFilter = convert.cStr(System.Web.Configuration.WebConfigurationManager.AppSettings["translation.file"]);
                if (FileNameFilter == "") return;
                XmlDocument oDoc = new XmlDocument();

                List<string> Files = System.IO.Directory.GetFiles(System.Web.HttpContext.Current.Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath) 
                    + System.IO.Path.GetDirectoryName(FileNameFilter), System.IO.Path.GetFileNameWithoutExtension(FileNameFilter)+ "*" + System.IO.Path.GetExtension(FileNameFilter)).ToList();
                Files.ForEach(FileName =>
                {
                    oDoc.Load(FileName);
                    string SiteId = xml.getXmlAttributeValue(oDoc.DocumentElement, "site");
                    string CompanyId = xml.getXmlAttributeValue(oDoc.DocumentElement, "company");
                    XmlNodeList xmlPages = oDoc.SelectNodes("pages/page");
                    for (int x = 0; x < xmlPages.Count; x++)
                    {
                        string sKey = xml.getXmlAttributeValue(xmlPages[x], "lang") + ":" + xml.getXmlAttributeValue(xmlPages[x], "url");

                        //Fix so you can put ~ in the path
                        if (sKey.Contains("~"))
                            sKey = sKey.Replace("~", HttpContext.Current.Request.ApplicationPath);

                        Dictionary<string, string> Settings = PageSettings.SafeGetValue(sKey);
                        if (Settings == null)
                        {
                            Settings = new Dictionary<string, string>();
                            PageSettings.Add(sKey, Settings);
                            foreach (XmlAttribute xmlAttibute in xmlPages[x].Attributes)
                                Settings.Add(xmlAttibute.Name, xmlAttibute.Value);
                        }

                        XmlNodeList xmlItems;
                        string RedirectLanguage = Settings.SafeGetValue("redirect-language");
                        if (RedirectLanguage.IsNullOrEmpty())
                            xmlItems = xmlPages[x].SelectNodes("item");
                        else
                            xmlItems = oDoc.SelectNodes("pages/page[@lang='" + RedirectLanguage + "'][@url='" + xml.getXmlAttributeValue(xmlPages[x], "url") + "']/item");

                        if (xmlItems.Count > 0)
                        {
                            for (int y = 0; y < xmlItems.Count; y++)
                            {
                                sKey = BuildDictionaryKey(convert.cStr(xml.getXmlAttributeValue(xmlPages[x], "lang"), xml.getXmlAttributeValue(xmlItems[y], "lang")), xml.getXmlAttributeValue(xmlItems[y], "name"), convert.cStr(xml.getXmlAttributeValue(xmlItems[y], "site"), SiteId), convert.cStr(xml.getXmlAttributeValue(xmlItems[y], "company"), CompanyId), xml.getXmlAttributeValue(xmlPages[x], "url"));
                                 
                                //Fix so you can put ~ in the path
                                if (sKey.Contains("~"))
                                    sKey = sKey.Replace("~", HttpContext.Current.Request.ApplicationPath);

                                if (!Translations.ContainsKey(sKey)) Translations.Add(sKey, xml.getXmlNodeTextValue(xmlItems[y]));
                            }
                        }
                        else
                        {
                            sKey = xml.getXmlAttributeValue(xmlPages[x], "lang") + ":" + xml.getXmlAttributeValue(xmlPages[x], "name") + ":" + xml.getXmlAttributeValue(xmlPages[x], "url");
                            if (!Translations.ContainsKey(sKey)) Translations.Add(sKey, xml.getXmlNodeTextValue(xmlPages[x]));
                        }
                    }
                });
            }
        }
        public static string translate(string sLanguage, string sOriginalValue, System.Web.UI.Page oPage, string sKey, string SiteId = null, string CompanyId = null) {
            return translate(sLanguage, sOriginalValue, (System.Web.HttpContext.Current.Request.ApplicationPath.Length<2 ? oPage.Request.Url.AbsolutePath : parse.stripLeadingCharacter(oPage.Request.Url.AbsolutePath, System.Web.HttpContext.Current.Request.ApplicationPath)), oPage, sKey, SiteId, CompanyId);
        }

        public static void translate(string sLanguage,List<jlib.components.iControl> controls , string sURL, System.Web.UI.Page oPage=null, string SiteId = null, string CompanyId = null) {
            controls.ForEach(x => {
                string key = jlib.functions.reflection.getMemberValue(x, "TranslationKey").Str();
                if(!key.IsNullOrEmpty()){
                    string s = helpers.translation.translate(sLanguage, x.getValue().Str(), sURL, oPage, key, SiteId, CompanyId);
                    if (!s.IsNullOrEmpty()) {
                        if(!jlib.functions.reflection.setValueOfProperty(x,"Text", s)) x.setValue(s);
                    }
                }
                if (x as components.dropdown != null) (x as components.dropdown).TranslateItems(sLanguage, SiteId, CompanyId, sURL);                
            });
        }        
        public static string translate(string sLanguage, string sOriginalValue, string sURL, System.Web.UI.Page oPage, string sKey, string SiteId = null, string CompanyId = null)
        {
            components.webpage page = oPage as components.webpage;
            if (page != null)
            {
                if (SiteId.IsNullOrEmpty())
                    SiteId = page.TranslationSiteId;

                if (CompanyId.IsNullOrEmpty())
                    CompanyId = page.TranslationCompanyId;
            }
            if (Translations == null || m_oNextFileModDateCheck < DateTime.Now) reload();
            sLanguage = convert.cStr(sLanguage);
            if (Translations.Count == 0 || sLanguage == "") return sOriginalValue;
            string sReturnValue = null;
            lock (_lock) {
                
                if (CompanyId.IsNotNullOrEmpty()) {
                    
                    if (Translations.TryGetValue(BuildDictionaryKey(sLanguage, sKey, SiteId, CompanyId, sURL), out sReturnValue)) return (sReturnValue == "" ? null : sReturnValue);
                    if (SiteId.IsNotNullOrEmpty() && Translations.TryGetValue(BuildDictionaryKey(sLanguage, sKey, "", CompanyId, sURL), out sReturnValue)) return (sReturnValue == "" ? null : sReturnValue);

                    //Is there a Global value for this key?
                    
                    if (Translations.TryGetValue(BuildDictionaryKey(sLanguage, sKey, SiteId, CompanyId, "::generic"), out sReturnValue)) return (sReturnValue == "" ? null : sReturnValue);
                    if (SiteId.IsNotNullOrEmpty() && Translations.TryGetValue(BuildDictionaryKey(sLanguage, sKey, "", CompanyId, "::generic"), out sReturnValue)) return (sReturnValue == "" ? null : sReturnValue);

                    if (Translations.TryGetValue(BuildDictionaryKey(sLanguage, sKey, SiteId, CompanyId, ""), out sReturnValue)) return (sReturnValue == "" ? null : sReturnValue);
                    if (SiteId.IsNotNullOrEmpty() && Translations.TryGetValue(BuildDictionaryKey(sLanguage, sKey, "", CompanyId, ""), out sReturnValue)) return (sReturnValue == "" ? null : sReturnValue);
                }

                //Is there a language-specific value?               
                
                if (Translations.TryGetValue(BuildDictionaryKey(sLanguage, sKey, SiteId, "", sURL), out sReturnValue)) return (sReturnValue==""?null:sReturnValue);

                
                //Is there a Global value for this key?
                if (Translations.TryGetValue(BuildDictionaryKey(sLanguage, sKey, SiteId, "", "::generic"), out sReturnValue)) return (sReturnValue == "" ? null : sReturnValue);
                if (Translations.TryGetValue(BuildDictionaryKey(sLanguage, sKey, SiteId, "", ""), out sReturnValue)) return (sReturnValue == "" ? null : sReturnValue);

                //Try getting non-site specific version
                if (SiteId.IsNotNullOrEmpty())
                {
                    if (Translations.TryGetValue(BuildDictionaryKey(sLanguage, sKey, "", "", sURL), out sReturnValue)) return (sReturnValue == "" ? null : sReturnValue);

                    //Is there a Global value for this key?
                    if (Translations.TryGetValue(BuildDictionaryKey(sLanguage, sKey, "", "", "::generic"), out sReturnValue)) return (sReturnValue == "" ? null : sReturnValue);
                    if (Translations.TryGetValue(BuildDictionaryKey(sLanguage, sKey, "", "", ""), out sReturnValue)) return (sReturnValue == "" ? null : sReturnValue);
                }
                return sOriginalValue;
            }
        }
        public static string translateHTML(string HTML, string TranslateTag, string Language, string URL, System.Web.UI.Page oPage=null) {
            try {
                if (PageSettings == null) reload();
                lock (PageSettings) {
                    if (PageSettings.ContainsKey(Language + ":" + URL))
                        Language = convert.cStr(PageSettings.SafeGetValue(Language + ":" + URL).SafeGetValue("redirect-language"), Language);
                    //default to english if invalid language specified
                    else if (PageSettings.ContainsKey("en:" + URL))
                        Language = convert.cStr(PageSettings.SafeGetValue("en" + ":" + URL).SafeGetValue("redirect-language"), "en");
                }

                string[] Translate = parse.split(HTML, "<" + TranslateTag + " key=\"", false);
                for (int x = 1; x < Translate.Length; x++) {
                    string Key = Translate[x].Substring(0, Translate[x].IndexOf("\""));
                    string Old = (Translate[x].IndexOf("</" + TranslateTag + ">", Key.Length + 2)==-1 ? "": Translate[x].Substring(Key.Length + 2, Translate[x].IndexOf("</" + TranslateTag + ">", Key.Length + 2) - Key.Length - 2));
                    Translate[x] = jlib.helpers.translation.translate(Language, Old, URL, oPage, Key) + (Translate[x].IndexOf("</" + TranslateTag + ">", Key.Length + 2)==-1 ? Translate[x].Substring(Translate[x].IndexOf(">")+1) : Translate[x].Substring(Key.Length + Old.Length + TranslateTag.Length + 5));
                }
                return parse.join(Translate, "");
            } catch (Exception e) {
                return HTML;
            }
        }
    }
}
