using System;
using System.Collections;
using System.Collections.Specialized;
using jlib.functions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace jlib.helpers.structures
{

    /// <summary>
    /// Multi-Key Dictionary Class
    /// </summary>
    /// <typeparam name="V">Value Type</typeparam>
    /// <typeparam name="K">Primary Key Type</typeparam>
    /// <typeparam name="L">Sub Key Type</typeparam>
    public class multi_dictionary<K, L, V> : Dictionary<K, V> {
        internal readonly Dictionary<L, K> subDictionary = new Dictionary<L, K>();
        internal readonly Dictionary<K, L> primaryToSubkeyMapping = new Dictionary<K, L>();

        readonly object lockBase = new object();
        readonly object lockSub = new object();

        public V this[L subKey] {
            get {
                V item;
                if (TryGetValueSubkey(subKey, out item))
                    return item;

                throw new KeyNotFoundException("sub key not found: " + subKey.ToString());
            }
        }

        public new V this[K primaryKey] {
            get {
                V item;
                if (TryGetValue(primaryKey, out item))
                    return item;

                throw new KeyNotFoundException("primary key not found: " + primaryKey.ToString());
            }
        }

        public L getSubkey(K primaryKey) {
            L item;
            if (primaryToSubkeyMapping.TryGetValue(primaryKey, out item)) return item;

            throw new KeyNotFoundException("primary key not found: " + primaryKey.ToString());
        }
        public V GetValue(K primaryKey) {
            return base[primaryKey];
        }

        public void Associate(L subKey, K primaryKey) {
            lock (lockBase) {
                if (!base.ContainsKey(primaryKey))
                    throw new KeyNotFoundException(string.Format("The base dictionary does not contain the key '{0}'", primaryKey));

                lock (lockSub) {
                    if (subDictionary.ContainsKey(subKey)) {
                        subDictionary[subKey] = primaryKey;
                        primaryToSubkeyMapping[primaryKey] = subKey;
                    } else {
                        subDictionary.Add(subKey, primaryKey);
                        primaryToSubkeyMapping.Add(primaryKey, subKey);
                    }
                }
            }
        }

        public bool TryGetValueSubkey(L subKey, out V val) {
            val = default(V);

            lock (lockSub) {
                K ep;
                if (subDictionary.TryGetValue(subKey, out ep)) {
                    if (!TryGetValue(ep, out val)) {
                        return false;
                    }
                } else {
                    return false;
                }
            }

            return true;
        }        

        public new bool TryGetValue(K primaryKey, out V val) {
            lock (lockBase) {
                if (!base.TryGetValue(primaryKey, out val)) {
                    return false;
                }
            }

            return true;
        }

        public bool ContainsKeySubkey(L subKey) {
            V val;

            return TryGetValueSubkey(subKey, out val);
        }

        public new bool ContainsKey(K primaryKey) {
            V val;

            return TryGetValue(primaryKey, out val);
        }

        public new void Remove(K primaryKey) {
            lock (lockSub) {
                subDictionary.Remove(primaryToSubkeyMapping[primaryKey]);
                primaryToSubkeyMapping.Remove(primaryKey);
            }

            lock (lockBase)
                base.Remove(primaryKey);
        }

        public void RemoveSubkey(L subKey) {
            lock (lockSub) {
                lock (lockBase)
                    base.Remove(subDictionary[subKey]);

                primaryToSubkeyMapping.Remove(subDictionary[subKey]);
                subDictionary.Remove(subKey);
            }
        }

        public new void Add(K primaryKey, V val) {
            lock (lockBase)
                base.Add(primaryKey, val);
        }

        public void Add(K primaryKey, L subKey, V val) {
            lock (lockBase)
                base.Add(primaryKey, val);

            Associate(subKey, primaryKey);
        }

        public V[] CloneValues() {
            lock (lockBase) {
                V[] values = new V[Values.Count];

                Values.CopyTo(values, 0);

                return values;
            }
        }

        public K[] ClonePrimaryKeys() {
            lock (lockBase) {
                K[] values = new K[Keys.Count];

                Keys.CopyTo(values, 0);

                return values;
            }
        }

        public L[] CloneSubKeys() {
            lock (lockBase) {
                L[] values = new L[subDictionary.Keys.Count];

                subDictionary.Keys.CopyTo(values, 0);

                return values;
            }
        }
    }

    
    /// <summary>
	/// Summary description for module_collection.
	/// </summary>
	public class collection : NameObjectCollectionBase, IEnumerator
	{
        public static void JSonSerialize(object o, jlib.functions.json.JsonWriter writer) {
            if (o != null && (o as collection).Count > 0) {
                writer.WriteArrayStart();
                writer.TextWriter.Write( "[" + parse.stripEndingCharacter(parse.stripLeadingCharacter(convert.cJSON(o as collection, false), "[","Array("), "]",")") + "]" );
                writer.WriteArrayEnd();
            } else {
                writer.Write(null);
            }
        }

		#region DECLARATIONS
		private int				m_iIndex			= 0;
		#endregion


		#region CONSTRUCTORS
		public collection()
		{
			
		}
        public collection(params object[] oValuePairs){
            this.Set(oValuePairs);
        }
		#endregion


		#region PROPERTIES
		public int Index
		{
			get
			{
				return m_iIndex;
			}
		}
		public object Current
		{
			get
			{
				return BaseGet(m_iIndex);
			}
		}
		public object this[int iIndex]
		{
			get
			{
                if (iIndex < 0 || iIndex >= this.Count) return null;
				return BaseGet(iIndex);
			}
			set
			{
				BaseSet(iIndex, value);
			}
		}
		public object this[object oName]
		{
			get
			{
				return BaseGet(convert.cStr(oName));
			}
			set
			{
                BaseSet(convert.cStr(oName), value);
			}
		}
		public override int Count
		{
			get
			{
				return base.Count;
			}
		}
		public string[] AllKeys
		{
			get
			{
				return this.BaseGetAllKeys();
			}
		}
		public object[] AllValues
		{
			get
			{
				return this.BaseGetAllValues();
			}
		}
		public override NameObjectCollectionBase.KeysCollection Keys
		{
			get
			{
				return base.Keys;
			}
		}
		#endregion


		#region METHODS
        public Dictionary<string, object> ToDictionary() {
            Dictionary<string, object> o = new Dictionary<string, object>();
            for (int x = 0; x < this.Count; x++) o.Add(this.GetKey(x), this[x]);
            return o;
        }
        public DataRow ToDataRow() {
            DataRow o = (new DataTable()).NewRow();
            for (int x = 0; x < this.Count; x++) {
                o.Table.Columns.Add(this.GetKey(x));
                o[o.Table.Columns.Count - 1] = this[x];
            }
            return o;
        }
        public virtual string[] ItemArray(int iIndex) {
            return new string[] { this.Keys[iIndex] , convert.cStr(this[iIndex]) };
        }
		public collection Add(object oKey, object oValue)
		{
			BaseAdd(convert.cStr(oKey), oValue);
            return this;
		}
        public collection AddAt(int iIndex, string sName, object oObject) {
			if (iIndex >= this.Count) {
				Add(sName, oObject);
				return this;
			}

			string[] sKeys = this.AllKeys;
			object[] oValues = this.AllValues;
			this.Clear();
			for (int x = 0; x < sKeys.Length; x++) {
				if (iIndex != x){
					BaseAdd(sKeys[x], oValues[x]);
				}else {
					BaseAdd(sName, oObject);
					BaseAdd(sKeys[x], oValues[x]);
				}
			}
            return this;
		}
		public void Remove(string sName)
		{
			BaseRemove(sName);
		}
		public void Remove(int iIndex)
		{
			BaseRemoveAt(iIndex);
		}
		public bool Contains(string sName)
		{	
			string[] sKeys		= this.AllKeys;
			for(int i=0; i<sKeys.Length; i++)
			{
				if(sKeys[i] == sName)
				{
					return true;
				}
			}
			return false;
		}
		public bool Contains(object oObject)
		{	
			object[] oValues		= this.AllValues;
			for(int i=0; i<oValues.Length; i++)
			{
				if(oValues[i].Equals(oObject))
				{
					return true;
				}
			}
			return false;
		}
        //public int Length()
        //{
        //    return this.Count - 1;
        //}
		public void Clear()
		{
			BaseClear();
		}
		public string GetKey(int iIndex)
		{
			return BaseGetKey(iIndex);
		}
		public string GetKey(object oObject)
		{
			object[] oValues		= this.AllValues;
			for(int i=0; i<oValues.Length; i++)
			{
				if(oValues[i].Equals(oObject))
				{
					return BaseGetKey(i);
				}
			}
			return null;
		}
        public object SafeGetValue(string sName) {
            if(this.Contains(sName)) return this[sName];
            return null;
        }
		public object GetValue(string sName)
		{
			return this[sName];
		}
		public object GetValue(int iIndex)
		{
			return this[iIndex];
		}
		public bool MoveNext()
		{
			if(m_iIndex < this.Count - 1)
			{
				m_iIndex++;
				return true;
			}
			else
			{
				return false;
			}
		}
		public bool MovePrevious()
		{
			if(m_iIndex > 0)
			{
				m_iIndex--;
				return true;
			}
			else
			{
				return false;
			}
		}
		public bool MoveTo(int iIndex)
		{
			if(iIndex > 0 && iIndex < this.Count - 1)
			{
				m_iIndex = iIndex;
				return true;
			}
			else
			{
				return false;
			}
		}
		public void Reset()
		{
			m_iIndex	= -1;
		}
        public collection Set(params object[] oValuePairs) {
            for (int x = 0; x < oValuePairs.Length; x = x + 2)
                this[convert.cStr(oValuePairs[x])] = oValuePairs[x + 1];
            return this;
        }
        #endregion
    }
    public class collection_aux : collection {
        Hashtable oAux = new Hashtable();
        
        public object getAuxData(string sKey) {
            return oAux[sKey];
        }
        public object getAuxData(int iIndex) {
            return oAux[base.Keys[iIndex]];
        }
        public object[] AllAuxData {
            get {
                object[] oData = new object[this.Count];
                for (int x = 0; x < this.Count; x++)
                    oData[x] = oAux[this.GetKey(x)];

                return oData;
            }
        }
        public override string[] ItemArray(int iIndex) {
            return new string[] { this.Keys[iIndex], convert.cStr(this[iIndex]), convert.cStr(getAuxData(iIndex)) };
        }

        public void Add(string sKey, object oValue, object oAuxData) {
            BaseAdd(sKey, oValue);
            oAux.Add(sKey, oAuxData);
        }
        public void AddAt(int iIndex, string sKey, object oValue, object oAuxData) {
            if (iIndex >= this.Count) {
                Add(sKey, oValue, oAuxData);
                return;
            }
            oAux.Add(sKey, oAuxData);
            string[] sKeys = this.AllKeys;
            object[] oValues = this.AllValues;
            this.Clear();
            for (int x = 0; x < sKeys.Length; x++) {
                if (iIndex != x) {
                    BaseAdd(sKeys[x], oValues[x]);                    
                } else {
                    BaseAdd(sKey, oValue);
                    BaseAdd(sKeys[x], oValues[x]);
                }
            }            
        }
        public new void Remove(string sKey) {
            BaseRemove(sKey);
            oAux.Remove(sKey);
        }
        public new void Remove(int iIndex) {
            Remove(base.Keys[iIndex]);            
        }
        public collection_aux search(string sKey, object oData, object oAuxData, bool bPartialMatch) {
            collection_aux oCollection = new collection_aux();
            for (int x = 0; x < this.Count; x++) {
                bool bMatch = true;
                if( bMatch && sKey!=null && (bPartialMatch ? this.GetKey(x).IndexOf(sKey)==-1 : this.GetKey(x) != sKey)) bMatch=false;
                if( bMatch && oData!=null && !(oData == this[x] || (bPartialMatch && oData.GetType() == typeof(string) && convert.cStr(this[x]).IndexOf(convert.cStr(oData))>-1))) bMatch=false;
                if (bMatch && oData != null && !(oAuxData == this.getAuxData(x) || (bPartialMatch && oAuxData.GetType() == typeof(string) && convert.cStr(this.getAuxData(x)).IndexOf(convert.cStr(oAuxData)) > -1))) bMatch = false;
                if (bMatch) oCollection.Add(this.GetKey(x), this[x], this.getAuxData(x));
            }
            return oCollection;
        }
    }
}
