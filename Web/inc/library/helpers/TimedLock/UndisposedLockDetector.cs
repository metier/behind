﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jlib.helpers.TimedLock
{
	/// Debug object to detect and warn developers of any undisposed locks.
	public class UndisposedLockDetector
	{
		private object _lockObject = null;

		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <param name="lockObject"></param>
		public UndisposedLockDetector(object lockObject)
		{
			_lockObject = lockObject;
		}
 
		/// <summary>
		/// Finalizer. This should not get called if the object has been correctly disposed of.
		/// </summary>
		~UndisposedLockDetector()
		{
			string message;
			// If this finalizer runs, someone somewhere failed to
			// call Dispose, which means we've failed to leave
			// a monitor!
			if ( _lockObject != null )
			{
				message = "Error. Undisposed TimedLock on object: " + Environment.NewLine + _lockObject.ToString() ;
			}
			else
			{
				message = "Error. Undisposed TimedLock on a null object.";
			}
			Console.WriteLine( message );
			System.Diagnostics.Debug.Fail( message );
		}
 
	}
}
