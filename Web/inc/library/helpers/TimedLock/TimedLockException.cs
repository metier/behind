﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jlib.helpers.TimedLock
{
	/// <summary>
	/// Exception thrown when the <c>TimedLock</c> lock expires while waiting for a lock on a resource.
	/// </summary>
	[Serializable]
	public class TimedLockException : ApplicationException 
	{ 
		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <param name="message">The lock exception message.</param>
		public TimedLockException(string message) 
			:base( message ) 
		{
		}
	}
}
