﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace jlib.helpers.TimedLock
{
	/// <summary>
	/// A resource locking class which contains built in deadlock handling.
	/// Use in preference to using the lock statemet ("lock( myObject)") as the
	/// TimedLock object has built in deadlock handling.
	/// </summary>
	/// <example>Intented use:
	/// // Place lock on resource
	/// using( TimedLock.Lock( myObject ))
	/// {
	///		// .. do stuff 
	///	} 
	/// </example>
	public struct TimedLock: IDisposable
	{ 

		//Constructors
		private TimedLock (object lockObject)
		{
			_lockObject = lockObject;
			#if DEBUG
			_undisposedLockDetector = new UndisposedLockDetector( lockObject );
			#endif
		}
 
		//Properties
		private object _lockObject;
		public static TimeSpan DefaultTimeOut = TimeSpan.FromSeconds(20);
		public static Dictionary<object, string> LockTree = new Dictionary<object, string>();
		#if DEBUG
		private UndisposedLockDetector _undisposedLockDetector;
		#endif

 		//Methods
		/// <summary>
		/// Applies a lock to the specified resource (using the default timeout <c>DefaultTimeOut</c>)..
		/// </summary>
		/// <param name="lockObject">The resource to lock.</param>
		/// <returns>Returns the lock.</returns>
		/// <exception cref="TimedLockException">Thrown when the DefaultTimeOut is exceeded.</exception>
		/// <example>Intented use:
		/// // Place lock on resource
		/// using( TimedLock.Lock ( myObject ))
		/// {
		///		// .. do stuff 
		///	} 
		/// </example>
		public static IDisposable Lock (object lockObject) 
		{
			return Lock( lockObject, DefaultTimeOut, null );
		}
 
		/// <summary>
		/// Applies a lock to the specified resource (using the default timeout <c>DefaultTimeOut</c>)..
		/// </summary>
		/// <param name="lockObject">The resource to lock.</param>
		/// <param name="LockName">Information to add to the error message if the lock times out (nullable).</param>
		/// <returns>Returns the lock (must be Disposed of).</returns>
		/// <exception cref="TimedLockException">Thrown when the DefaultTimeOut is exceeded.</exception>
		/// <example>Intented use:
		/// // Place lock on resource
		/// using( TimedLock.Lock ( myObject ))
		/// {
		///		// .. do stuff 
		///	} 
		/// </example>
		public static IDisposable Lock (object lockObject, string LockName) 
		{
			return Lock( lockObject, DefaultTimeOut, LockName );
		}
 
		/// <summary>
		/// Applies a lock to the specified resource (using the specified timeout)..
		/// </summary>
		/// <param name="lockObject">The resource to lock.</param>
		/// <param name="timeout">The deadlock timeout</param>
		/// <returns>Returns the lock.</returns>
		/// <exception cref="TimedLockException">Thrown when the <c>DefaultTimeOut</c> is exceeded.</exception>
		/// <example>Intented use:
		/// using( TimedLock.Lock ( myObject ))
		/// {
		///		// .. do stuff 
		///	} 
		/// </example>
		public static IDisposable Lock(object lockObject, TimeSpan timeout)
		{
			return Lock( lockObject, timeout, null );
		}
 
		/// <summary>
		/// Applies a lock to the specified resource (using the specified timeout)..
		/// </summary>
		/// <param name="LockObject">The resource to lock.</param>
		/// <param name="Timeout">The deadlock timeout</param>
		/// <param name="LockName">Information to add to the error message if the lock times out (nullable).</param>
		/// <returns>Returns the lock.</returns>
		/// <exception cref="TimedLockException">Thrown when the <c>DefaultTimeOut</c> is exceeded.</exception>
		/// <example>Intented use:
		/// using( TimedLock.Lock ( myObject ))
		/// {
		///		// .. do stuff 
		///	} 
		/// </example>
		public static IDisposable Lock(object LockObject, TimeSpan Timeout, string LockName)
		{ 
			// Check parameters
			if ( LockObject == null )
			{
				// Throw an exception
				throw new ArgumentNullException("LockObject", "Please specify a lockObject.");
			}
 
			// Create new lock
			TimedLock timedLock = new TimedLock ( LockObject );
 
			// Try a lock on the resource
			if (!Monitor.TryEnter (LockObject, Timeout))
			{
				#if DEBUG
				// Failed to get the lock, so prevent the finalizer from being called
				System.GC.SuppressFinalize(timedLock._undisposedLockDetector);
				#endif

				//Get Lock Holder
				string lockHolder = "Unknown";
				TimedLock.LockTree.TryGetValue(LockObject, out lockHolder);

				// Throw new timeout exception
				throw new TimedLockException( String.Format("Unable to acquire lock, currently held by {0}", lockHolder) );
			}

			//Successfully got lock, so update Lock Tree
			TimedLock.LockTree[LockObject] = LockName;

			return timedLock;
		}
 
		/// <summary>
		/// Disposes of the timed lock resource (MUST BE CALLED - USE USING STATEMENTS).
		/// </summary>
		public void Dispose()
		{
			//Remove from tree
			TimedLock.LockTree.Remove(_lockObject);

			Monitor.Exit ( _lockObject );
			_lockObject = null;
			// It's a bad error if someone forgets to call Dispose,
			// so in Debug builds, we put a finalizer in to detect
			// the error. If Dispose is called, we suppress the
			// finalizer.
			#if DEBUG
			// Prevent finalizer from being called.
			GC.SuppressFinalize(_undisposedLockDetector);
			#endif
		}
 
		
		
	}
}
