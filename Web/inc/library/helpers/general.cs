using System;
using System.Text.RegularExpressions;
using System.Data;
using System.Text;
using System.Web;
using System.Net;
using System.Web.UI;
using System.Collections;
using System.Configuration;
using jlib.functions;
using jlib.db;
using System.IO;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml;
using System.Security.Cryptography;

namespace jlib.helpers
{
	/// <summary>
	/// Summary description for xml.
	/// </summary>
	public class general
	{
		public static string getSalt(string key)
		{
			return getRandomString(key, 22);
		}

        public static string getRandomString(string sValidChars, int iLength){            
            RNGCryptoServiceProvider oProvider = new RNGCryptoServiceProvider();
            byte[] b = new byte[256];
            oProvider.GetBytes(b);
            return convert.cEllipsis(parse.removeXChars(System.Convert.ToBase64String(b), sValidChars), iLength, false);                    
        }   
		public static string getDynamicButton(string sName, string sState)
		{
			return String.Format("/inc/temp/buttons/{0}-{1}.gif", sName, sState);
		}

		public static string getLibraryButton(string sName, string sState)
		{
			return String.Format("/inc/library/media/buttons/{0}-{1}.gif", sName, sState);
		}

		public static string getLibraryIcon(string sSize, string sName)
		{
			return String.Format("/inc/library/media/icons/{0}/{1}.gif", sSize, sName);
		}

		public enum FileStatus
		{
			None = 0,
			Normal = 1,
			Deleted = 2,
			UnApproved = 3,
			Modified = 4
		}

		public static string getThumbnailUrl(string sUrl, int iWidth, int iHeight, bool bNoResizeIfFit, bool bMaxMode, string sWatermark, general.FileStatus oStatus)
		{
			return getThumbnailUrl(sUrl, iWidth, iHeight, bNoResizeIfFit, bMaxMode, true, sWatermark, oStatus, true, 0, 0, 0, 0);
		}

		public static string getThumbnailUrl(string sUrl, int iWidth, int iHeight, bool bNoResizeIfFit, bool bMaxMode, string sWatermark, general.FileStatus oStatus, int iQuality)
		{
			return getThumbnailUrl(sUrl, iWidth, iHeight, bNoResizeIfFit, bMaxMode, true, sWatermark, oStatus, true, 0, 0, 0, 0, iQuality);
		}

		public static string getThumbnailUrl(string sUrl, int iWidth, int iHeight, bool bNoResizeIfFit, bool bMaxMode, bool bProportional, string sWatermark, general.FileStatus oStatus, bool bUseCache, int iCropLeft, int iCropTop, int iCropRight, int iCropBottom)
		{
			return getThumbnailUrl(sUrl, iWidth, iHeight, bNoResizeIfFit, bMaxMode, true, sWatermark, oStatus, bUseCache, iCropLeft, iCropTop, iCropRight, iCropBottom, 0);
		}
		public static string getThumbnailUrl(string sUrl, int iWidth, int iHeight, bool bNoResizeIfFit, bool bMaxMode, bool bProportional, string sWatermark, general.FileStatus oStatus, bool bUseCache, int iCropLeft, int iCropTop, int iCropRight, int iCropBottom, int iQuality)
		{
			return getThumbnailUrl(sUrl, iWidth, iHeight, bNoResizeIfFit, bMaxMode, true, sWatermark, oStatus, bUseCache, iCropLeft, iCropTop, iCropRight, iCropBottom, iQuality, false);
		}
		public static string getThumbnailUrl(string sUrl, int iWidth, int iHeight, bool bNoResizeIfFit, bool bMaxMode, bool bProportional, string sWatermark, general.FileStatus oStatus, bool bUseCache, int iCropLeft, int iCropTop, int iCropRight, int iCropBottom, int iQuality, bool bBlankPixelIfNotFound)
		{

			string sOverlay = "";
			if (sUrl == "") return "";
			//switch ( oStatus ) {
			//    case FileStatus.UnApproved:
			//        sOverlay = "/intellicms/library/media/icons/status/unapproved.ico";
			//        break;

			//    case FileStatus.Deleted:
			//        sOverlay = "/intellicms/library/media/icons/status/deleted.ico";
			//        break;
			//}

			if (convert.cStr(ConfigurationManager.AppSettings["default.image.url"]) != "" && sUrl.Length > ConfigurationManager.AppSettings["default.image.url"].Length && sUrl.ToLower().Substring(0, ConfigurationManager.AppSettings["default.image.url"].Length) == ConfigurationManager.AppSettings["default.image.url"].ToLower())
				sUrl = sUrl.Substring(ConfigurationManager.AppSettings["default.image.url"].Length);

			string baseUrl = "/inc/library/exec/thumbnail.aspx";
			if (convert.cStr(ConfigurationManager.AppSettings["thumbnail.baseurl"]) != "") baseUrl = convert.cStr(ConfigurationManager.AppSettings["thumbnail.baseurl"]);

			//crop = t parameter
			//watermark = e parameter (embosse)
			string url = baseUrl + String.Format("?u={0}&w={1}&h={2}&n={3}&m={4}&p={5}" + (sWatermark == "" ? "" : "&e={6}") + (sOverlay == "" ? "" : "&o={7}") + "&c={8}" + (iCropBottom == 0 && iCropLeft == 0 && iCropTop == 0 && iCropRight == 0 ? "" : "&t={9}") + "&q={10}&b={11}", System.Web.HttpUtility.UrlEncode(sUrl), iWidth, iHeight, (bNoResizeIfFit ? 1 : 0), (bMaxMode ? 1 : 0), (bProportional ? 1 : 0), System.Web.HttpUtility.UrlEncode(sWatermark), System.Web.HttpUtility.UrlEncode(sOverlay), (bUseCache ? 1 : 0), iCropLeft + "-" + iCropTop + "-" + iCropRight + "-" + iCropBottom, iQuality, (bBlankPixelIfNotFound ? 1 : 0));
			if (System.Web.HttpContext.Current.Request.ApplicationPath.Str() != "/") url = System.Web.HttpContext.Current.Request.ApplicationPath + url;
			return url;
		}

		public static string getThumbnailUrl(string sUrl, int iWidth, int iHeight, general.FileStatus oStatus)
		{
			return getThumbnailUrl(sUrl, iWidth, iHeight, true, true, "", oStatus);
		}

		public static string getThumbnailUrl(string sUrl, int iWidth, int iHeight, bool bProportional, general.FileStatus oStatus)
		{
			return getThumbnailUrl(sUrl, iWidth, iHeight, true, bProportional, bProportional, "", oStatus, true, 0, 0, 0, 0);
		}
		public static string getThumbnailUrl(string sUrl, int iWidth, int iHeight, general.FileStatus oStatus, string sCropDimensions)
		{
			return getThumbnailUrl(sUrl, iWidth, iHeight, oStatus, sCropDimensions, true);
		}
		public static string getThumbnailUrl(string sUrl, int iWidth, int iHeight, general.FileStatus oStatus, string sCropDimensions, bool bUseCache)
		{
			return getThumbnailUrl(sUrl, iWidth, iHeight, oStatus, sCropDimensions, bUseCache, false);
		}
		public static string getThumbnailUrl(string sUrl, int iWidth, int iHeight, general.FileStatus oStatus, string sCropDimensions, bool bUseCache, bool bBlankPixelIfNotFound)
		{

			int iCropLeft = convert.cInt(parse.split(sCropDimensions, "|", 0));
			int iCropTop = convert.cInt(parse.split(sCropDimensions, "|", 1));
			int iCropRight = convert.cInt(parse.split(sCropDimensions, "|", 2));
			int iCropBottom = convert.cInt(parse.split(sCropDimensions, "|", 3));

			return getThumbnailUrl(sUrl, iWidth, iHeight, true, true, true, "", oStatus, bUseCache, iCropLeft, iCropTop, iCropRight, iCropBottom, 0, bBlankPixelIfNotFound);
		}

		public static string getThumbnailUrl(string sUrl, int iWidth, int iHeight, general.FileStatus oStatus, int iCropLeft, int iCropTop, int iCropRight, int iCropBottom)
		{

			return getThumbnailUrl(sUrl, iWidth, iHeight, true, true, true, "", oStatus, true, iCropLeft, iCropTop, iCropRight, iCropBottom);
		}

		public static string getThumbnailUrl(string sUrl, System.Web.UI.WebControls.Unit uWidth, System.Web.UI.WebControls.Unit uHeight, general.FileStatus oStatus)
		{
			return getThumbnailUrl(sUrl, convert.cInt(uWidth), convert.cInt(uHeight), true, true, "", oStatus);
		}

		public static string getThumbnailUrl(string sUrl, int iWidth, general.FileStatus oStatus)
		{
			return getThumbnailUrl(sUrl, iWidth, 0, true, true, "", oStatus);
		}

		public static string getThumbnailUrl(string sUrl, System.Web.UI.WebControls.Unit uWidth, general.FileStatus oStatus)
		{
			return getThumbnailUrl(sUrl, convert.cInt(uWidth), 0, true, true, "", oStatus);
		}



		public static string getScreenshotUrl(string sUrl, int iWidth, int iHeight, bool bNoResizeIfFit, bool bMaxMode, string sWatermark)
		{
			return getScreenshotUrl(sUrl, iWidth, iHeight, bNoResizeIfFit, bMaxMode, true, sWatermark);
		}
		public static string getScreenshotUrl(string sUrl, int iWidth, int iHeight, bool bNoResizeIfFit, bool bMaxMode, bool bProportional, string sWatermark)
		{
            return String.Format(System.Web.HttpContext.Current.Request.ApplicationPath + "/inc/library/exec/screenshot.aspx?url={0}&w={1}&h={2}&n={3}&m={4}&p={5}&e={6}", System.Web.HttpUtility.UrlEncode(sUrl), iWidth, iHeight, (bNoResizeIfFit ? 1 : 0), (bMaxMode ? 1 : 0), (bProportional ? 1 : 0), System.Web.HttpUtility.UrlEncode(sWatermark));
		}

		public static string getScreenshotUrl(string sUrl, int iWidth, int iHeight)
		{
			return getScreenshotUrl(sUrl, iWidth, iHeight, true, true, "");
		}

		public static string getScreenshotUrl(string sUrl, int iWidth, int iHeight, bool bProportional)
		{
			return getScreenshotUrl(sUrl, iWidth, iHeight, true, bProportional, bProportional, "");
		}

		public static string getScreenshotUrl(string sUrl, System.Web.UI.WebControls.Unit uWidth, System.Web.UI.WebControls.Unit uHeight)
		{
			return getScreenshotUrl(sUrl, convert.cInt(uWidth), convert.cInt(uHeight), true, true, "");
		}

		public static string getScreenshotUrl(string sUrl, int iWidth)
		{
			return getScreenshotUrl(sUrl, iWidth, 0, true, true, "");
		}

		public static string getScreenshotUrl(string sUrl, System.Web.UI.WebControls.Unit uWidth)
		{
			return getScreenshotUrl(sUrl, convert.cInt(uWidth), 0, true, true, "");
		}

		public static string buildImageUrl(string sFileName, int iRowID, int iCounter, string sDirSeparator)
		{
			return sDirSeparator + parse.sRight("0000000000" + (convert.cInt(iRowID / 10000)), 8) + sDirSeparator + iRowID + "_" + iCounter + System.IO.Path.GetExtension(sFileName);
		}

		public static string formatStringA(string sFormatString, params object[] oParams)
		{
			if (ConfigurationManager.AppSettings["site.culture.id"] == null)
				return String.Format(sFormatString, oParams);
			else
				return String.Format(new System.Globalization.CultureInfo(ConfigurationManager.AppSettings["site.culture.id"], true), sFormatString, oParams);
		}

		public static string formatString(string sFormatString, JDataRow oRow, string sBlankValue)
		{
			return formatString(sFormatString, oRow as IDataRow, sBlankValue);
		}

		public static string formatString(string sFormatString, IDataRow oRow, string sBlankValue)
		{
			object[] oVals = new object[oRow.GetColumnCount()];
			for (int x = 0; x < oVals.Length; x++)
				oVals[x] = convert.cStr(oRow[x]) == "" ? sBlankValue : oRow[x];

			if (ConfigurationManager.AppSettings["site.culture.id"] == null)
				return String.Format(sFormatString, oVals);
			else
				return String.Format(new System.Globalization.CultureInfo(ConfigurationManager.AppSettings["site.culture.id"], true), sFormatString, oVals);
		}

		public static string formatString(string sFormatString, string sFormatStringArguments, JDataRow oRow)
		{
			return formatString(sFormatString, sFormatStringArguments, oRow as IDataRow);
		}

		public static string formatString(string sFormatString, string sFormatStringArguments, IDataRow oRow)
		{
			return formatString(sFormatString, sFormatStringArguments, oRow, "", null);
		}

		public static string formatString(string sFormatString, object oSourceObject)
		{
			return formatString(sFormatString, oSourceObject, "");
		}
		public static string formatString(string sFormatString, object oSourceObject, string sPrefix)
		{
			return formatString(sFormatString, oSourceObject, sPrefix, null);
		}
        public static string formatString(string sFormatString, DataRow oRow, string sBlankValue) {
            object[] oVals = new object[oRow.Table.Columns.Count];
            for (int x = 0; x < oVals.Length; x++)
                oVals[x] = convert.cStr(oRow[x]) == "" ? sBlankValue : oRow[x];

            if (ConfigurationManager.AppSettings["site.culture.id"] == null)
                return String.Format(sFormatString, oVals);
            else
                return String.Format(new System.Globalization.CultureInfo(ConfigurationManager.AppSettings["site.culture.id"], true), sFormatString, oVals);
        }
		public static string formatString(string sFormatString, object oSourceObject, string sPrefix, Hashtable oReplacementValues)
		{
			return formatString(sFormatString, oSourceObject, sPrefix, oReplacementValues, false, null);
		}

		public static string formatString(string sFormatString, object oSourceObject, string sPrefix, Hashtable oReplacementValues, bool bSQLEscapeReplacements, XmlNode oReplacementNode)
		{
			//When no replacement value is found, we need the # to be kept in the returned string, in case it is being used in the data.exchanger for multiple calls to formatString with different variables passed in each time
			string[] sArr = parse.split(sFormatString, "#", true);

			for (int z = 0; z < sArr.Length; z++)
			{
				if (sArr[z].StartsWith("#"))
				{
					string sKey = sArr[z].Substring(1);
					if (sPrefix != "" && sKey.StartsWith(sPrefix + ".")) sKey = sKey.Substring(sPrefix.Length + 1);
					try
					{
						if (oSourceObject != null && jlib.functions.reflection.containsProperty(oSourceObject, sKey))
						{
							sArr[z] = convert.cStr(bSQLEscapeReplacements ? ado_helper.PrepareDB(reflection.getPropertyValue(oSourceObject, sKey)) : reflection.getPropertyValue(oSourceObject, sKey));
							if (z < sArr.Length - 1) sArr[z + 1] = sArr[z + 1].Substring(1);
						}
						else if (oReplacementValues != null && oReplacementValues[sKey] != null)
						{
							sArr[z] = convert.cStr(bSQLEscapeReplacements ? ado_helper.PrepareDB(oReplacementValues[sKey]) : oReplacementValues[sKey]);
							if (z < sArr.Length - 1) sArr[z + 1] = sArr[z + 1].Substring(1);
						}
						else if (oReplacementNode != null && oReplacementNode.SelectSingleNode(sKey) != null)
						{
							sArr[z] = (bSQLEscapeReplacements ? ado_helper.PrepareDB(xml.getXmlNodeTextValue(oReplacementNode, sKey)) : xml.getXmlNodeTextValue(oReplacementNode, sKey));
							if (z < sArr.Length - 1) sArr[z + 1] = sArr[z + 1].Substring(1);
						}
						else if (oSourceObject != null && sKey.IndexOf("(") > -1)
						{
							string sMethodName = sKey.Substring(0, sKey.IndexOf("("));
							if (oSourceObject.GetType().GetMember(sMethodName).Length > 0)
							{
								string[] sParameters = parse.split(parse.inner_substring(sKey, "(", null, ")", null), ",");
								sArr[z] = convert.cStr(bSQLEscapeReplacements ? ado_helper.PrepareDB(reflection.executeMethodUntyped(oSourceObject, sMethodName, sParameters)) : reflection.executeMethodUntyped(oSourceObject, sMethodName, sParameters));
								if (z < sArr.Length - 1) sArr[z + 1] = sArr[z + 1].Substring(1);
							}
						}
					}
					catch (Exception) { }
				}
			}

			return parse.join(sArr, "");
		}
		public static string formatString(string sFormatString, string sFormatStringArguments, JDataRow oRow, Hashtable oReplacementValues)
		{
			return formatString(sFormatString, sFormatStringArguments, oRow as IDataRow, oReplacementValues);
		}
		public static string formatString(string sFormatString, string sFormatStringArguments, IDataRow oRow, Hashtable oReplacementValues)
		{
			return formatString(sFormatString, sFormatStringArguments, oRow, "", oReplacementValues);
		}
		public static string formatString(string sFormatString, string sFormatStringArguments, JDataRow oRow, string sPrefix)
		{
			return formatString(sFormatString, sFormatStringArguments, oRow as IDataRow, sPrefix);
		}
		public static string formatString(string sFormatString, string sFormatStringArguments, IDataRow oRow, string sPrefix)
		{
			return formatString(sFormatString, sFormatStringArguments, oRow, "", null);
		}
		public static string formatString(string sFormatString, string sFormatStringArguments, JDataRow oRow, string sPrefix, Hashtable oReplacementValues)
		{
			return formatString(sFormatString, sFormatStringArguments, oRow as IDataRow, sPrefix, oReplacementValues);
		}
		public static string formatString(string sFormatString, string sFormatStringArguments, IDataRow oRow, string sPrefix, Hashtable oReplacementValues)
		{
			return formatString(sFormatString, sFormatStringArguments, oRow, sPrefix, oReplacementValues, false);
		}
		public static string formatString(string sFormatString, string sFormatStringArguments, JDataRow oRow, string sPrefix, Hashtable oReplacementValues, bool bSQLEscapeReplacements)
		{
			return formatString(sFormatString, sFormatStringArguments, oRow as IDataRow, sPrefix, oReplacementValues, bSQLEscapeReplacements);
		}
		public static string formatString(string sFormatString, string sFormatStringArguments, IDataRow oRow, string sPrefix, Hashtable oReplacementValues, bool bSQLEscapeReplacements)
		{

			string[] sArr = parse.split(sFormatString, "#", true);

			for (int z = 0; z < sArr.Length; z++)
			{
				if (sArr[z].StartsWith("#"))
				{
					string sKey = sArr[z].Substring(1);
					if (sPrefix != "" && sKey.StartsWith(sPrefix + ".")) sKey = sKey.Substring(sPrefix.Length + 1);
					if (oRow == null || oRow.GetColumnByName(sKey) == null)
					{
						if (oReplacementValues != null && oReplacementValues[sKey] != null)
						{
							sArr[z] = convert.cStr(bSQLEscapeReplacements ? ado_helper.PrepareDB(oReplacementValues[sKey]) : oReplacementValues[sKey]);
							if (z < sArr.Length - 1) sArr[z + 1] = sArr[z + 1].Substring(1);

						}

						//If FormatString has a hierarchy
						else if (oRow != null && jlib.functions.reflection.doesPropertyExist(oRow, sKey))
						{
							sArr[z] =  (bSQLEscapeReplacements ? ado_helper.PrepareDB(jlib.functions.reflection.getPropertyValue(oRow, sKey)) : jlib.functions.reflection.getPropertyValue(oRow, sKey).Str());
                            if (z < sArr.Length - 1) sArr[z + 1] = sArr[z + 1].Substring(1);
						}

						//else 
						//	sArr[z] = sArr[z].Substring(1);

					}
					else
					{
						sArr[z] = convert.cStr(bSQLEscapeReplacements ? ado_helper.PrepareDB(oRow[sKey]) : oRow[sKey]);
						if (z < sArr.Length - 1) sArr[z + 1] = sArr[z + 1].Substring(1);
					}
				}				
			}

			string sTemp = parse.join(sArr, "");
			if (sTemp.IndexOf("{") > -1 && convert.cStr(sFormatStringArguments) != "")
			{
				sArr = parse.split(sFormatStringArguments, ",");
				object[] oValues = new object[sArr.Length];
				for (int z = 0; z < sArr.Length; z++)
				{
					if (oRow != null && oRow.GetColumnByName(sArr[z]) != null)
						oValues[z] = oRow[sArr[z]];
				}

				if (ConfigurationManager.AppSettings["site.culture.id"] == null)
					sTemp = String.Format(sTemp, oValues);
				else
					sTemp = String.Format(new System.Globalization.CultureInfo(ConfigurationManager.AppSettings["site.culture.id"], true), sTemp, oValues);
			}

			return sTemp;
		}

        public static int Random(int Min, int Max) {
            RNGCryptoServiceProvider Random = new RNGCryptoServiceProvider();
            byte[] Buffer = new byte[4];

            Random.GetBytes(Buffer);
            int Result = BitConverter.ToInt32(Buffer, 0);

            return new Random(Result).Next(Min, Max);            
        }

		public static string createPassword(int iMinLength, int iMaxLength)
		{
			Random oRnd = new Random(System.DateTime.Now.Millisecond);
			StringBuilder oBuilder = new StringBuilder();
			int iNumChars = iMinLength + oRnd.Next(iMaxLength - iMinLength + 1);
			for (int i = 0; i < iNumChars; i++)
			{
				switch (oRnd.Next(3)) // which kind of character to add ?
				{
					case 1: // adds an uppercase character
					//oBuilder.Append( (char)( 'A' + rnd.Next( 26 ) ) );
					//break;
					case 2: // adds an lowercase char
						oBuilder.Append((char)('a' + oRnd.Next(26)));
						break;
					case 0: // adds a numeric char
						oBuilder.Append((char)('0' + oRnd.Next(10)));
						break;
				}
			}
			return oBuilder.ToString();
		}


		public static string getImageFilePath(string sKey, int iID, string sUrl)
		{

			string sNewFilePath = System.Configuration.ConfigurationManager.AppSettings[sKey];
			if (convert.cStr(sNewFilePath) == "") sNewFilePath = sKey;

			string sNewFileName = parse.sRight("0000000000" + (convert.cInt(iID / 10000)), 8) + "\\" + iID + System.IO.Path.GetExtension(sUrl);
			if (sNewFilePath.IndexOf("://") > -1) sNewFileName = parse.replaceAll(sNewFileName, "\\", "/");
			return sNewFilePath + sNewFileName;
		}

		public static string getSafeUrl(object oData, int iMaxLen)
		{
			string sValidUrlCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-/";
			string sData = parse.replaceAll(oData, true, ".", "_", ",", "_", "&", "og", " ", "_", "ë", "e", "Ë", "E", "é", "e", "É", "E", "è", "e", "È", "E", "ø", "o", "Ø", "O", "å", "a", "Å", "A", "æ", "e", "Æ", "E", "ö", "o", "Ö", "O", "ä", "a", "Ä", "A", "á", "a", "Á", "A", "ü", "u", "Ü", "U", "ú", "u", "Ú", "U", "č", "c", "Č", "C", "đ", "d", "Đ", "D", "ŋ", "n", "Ŋ", "N", "š", "s", "Š", "S", "ŧ", "t", "Ŧ", "T", "ž", "z", "Ž", "Z");
			return (iMaxLen > 0 ? convert.cEllipsis(parse.removeXChars(sData, sValidUrlCharacters), iMaxLen, false) : parse.removeXChars(sData, sValidUrlCharacters));
		}

		public static Guid CreateSequentialGuid()
		{
			byte[] destinationArray = Guid.NewGuid().ToByteArray();
			DateTime time = new DateTime(0x76c, 1, 1);
			DateTime now = DateTime.Now;
 
			// Get the days and milliseconds which will be 
			// used to build the byte string
			TimeSpan span = new TimeSpan(now.Ticks - time.Ticks);
			TimeSpan timeOfDay = now.TimeOfDay;
 
			// Convert to a byte array 
			// Note that SQL Server is accurate to 1/300th of a  
			// millisecond so we divide by 3.333333
			byte[] bytes = BitConverter.GetBytes(span.Days);
			byte[] array = BitConverter.GetBytes(
						   (long)
						   (timeOfDay.TotalMilliseconds / 3.333333));
 
			// Reverse the bytes to match SQL Servers ordering 
			Array.Reverse(bytes);
			Array.Reverse(array);
 
			// Copy the bytes into the guid
			Array.Copy(bytes, bytes.Length - 2, 
							  destinationArray, 
							  destinationArray.Length - 6, 2);
			Array.Copy(array, array.Length - 4, 
							  destinationArray, 
							  destinationArray.Length - 4, 4);
			return new Guid(destinationArray);
	}
}
}
