using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using jlib.db;
using jlib.functions;

namespace jlib.helpers {
	public class email {

		private int m_iBodyID = 0;
		private string m_sTemplateName = "";
		private string m_sHtmlMessage = "";
		private string m_sTextMessage = "";
		private string m_sSubject = "";
		private int m_iSenderID = 0;
		private string m_sSenderEmail = "";
		private string m_sSenderName = "";
		private ArrayList m_oRecipients = new ArrayList();
		private Hashtable m_oCommonReplacementValues = new Hashtable();
		private ArrayList m_oUrlReplacementValues = new ArrayList();
		protected bool m_bMassEmailMode = false;

		public email(string sTemplateName, string sSubject, string sSenderName, string sSenderEmail, int iSenderID, bool bMassEmailMode) {

			if (iSenderID < 1) iSenderID = convert.cInt(sqlbuilder.executeSelectValue("d_users", "id", "email", sSenderEmail.Trim()));							

			m_sTemplateName = sTemplateName;
			m_sHtmlMessage = io.read_file(System.Web.HttpContext.Current.Server.MapPath("/inc/media/templates/" + sTemplateName + ".htm"));
			m_sTextMessage = io.read_file(System.Web.HttpContext.Current.Server.MapPath("/inc/media/templates/" + sTemplateName + ".txt"));
			m_sSubject = sSubject;
			m_sSenderName = sSenderName;
			m_sSenderEmail = sSenderEmail;
			m_iSenderID = iSenderID;

			//if mass email mode, then we store one body-text for all recipients (ie., all recipients get the SAME message, and it's bulked out as one BCC)
			m_bMassEmailMode = bMassEmailMode;
		}

		public Recipient getRecipient(int iIndex) {
			if (iIndex < 0 || iIndex >= RecipientCount) return null;
			return m_oRecipients[iIndex] as Recipient;
		}
		public int RecipientCount{
			get {
				return m_oRecipients.Count;
			}
		}

		public void addRecipient(string sToName, string sToEmail, int iToID, bool bHtml, params object []oReplacementValues) {
			if (iToID < 1) iToID = convert.cInt(sqlbuilder.executeSelectValue("d_users", "id", "email", sToEmail.Trim()));
			Recipient oRecipient = new Recipient(this, bHtml, sToName, sToEmail, iToID, oReplacementValues);
			m_oRecipients.Add(oRecipient);
			oRecipient.save();
		}

		public void addCommonReplacementPairs(string sOriginalValue, string sNewValue) {
			m_oCommonReplacementValues.Add(sOriginalValue, sNewValue);
		}

        public Recipient addDBReplacement(Recipient oRecipient, DataRow oRow, string sTableName) {
            if (sTableName == "") sTableName = oRow.Table.TableName;
            for (int x = 0; x < oRow.Table.Columns.Count; x++)
                oRecipient.Message = parse.replaceAll(oRecipient.Message, "{" + sTableName + "." + oRow.Table.Columns[x].ColumnName + "}", (oRecipient.Html ? parse.replaceAll(oRow[x], "\n", "<br>") : convert.cStr(oRow[x])), "{" + oRow.Table.Columns[x].ColumnName + "}", (oRecipient.Html ? parse.replaceAll(oRow[x], "\n", "<br>") : convert.cStr(oRow[x])));
            
            return oRecipient;
        }

		public void addUrlReplacementPairs(string sOriginalValue, string sNewValue, string sName) {
			addUrlReplacementPairs(sOriginalValue, sNewValue, sName, "");
		}
		public void addUrlReplacementPairs(string sOriginalValue, string sNewValue, string sName, string sAction) {
			m_oUrlReplacementValues.Add(new jlib.functions.util.FourValueClass(sOriginalValue, sNewValue, sName, sAction));				
		}

		public bool send() {
			jlib.db.ado_helper oData = new jlib.db.ado_helper(parse.isNull( ConfigurationManager.AppSettings["sql_dsn.emailer"], ConfigurationManager.AppSettings["sql.dsn"]));            
			
			DataTable oBody = null;
			if( m_bMassEmailMode ){

				oBody = sqlbuilder.getNewRecord(oData, "d_message_bodies");				
				IEnumerator oEnum = m_oCommonReplacementValues.GetEnumerator();
				while(oEnum.MoveNext()){
					m_sTextMessage = parse.replaceAll( m_sTextMessage, convert.cStr(oEnum.Current), convert.cStr(m_oCommonReplacementValues[oEnum.Current]));
					m_sHtmlMessage = parse.replaceAll( m_sHtmlMessage, convert.cStr(oEnum.Current), convert.cStr(m_oCommonReplacementValues[oEnum.Current]));
				}
				
				for( int x = 0;x<m_oUrlReplacementValues.Count;x++ ){
					jlib.functions.util.FourValueClass oValues = m_oUrlReplacementValues[x] as jlib.functions.util.FourValueClass;
					m_sTextMessage = parse.replaceAll( m_sTextMessage, convert.cStr(oValues.Value1), convert.cStr(oValues.Value2));
					m_sHtmlMessage = parse.replaceAll( m_sHtmlMessage, convert.cStr(oValues.Value1), convert.cStr(oValues.Value2));
				}
				ado_helper.update(oBody);
				m_iBodyID = convert.cInt(oBody.Rows[0]["id"]);
			}

			for (int x = 0; x < m_oRecipients.Count; x++) {
				Recipient oRecipient = m_oRecipients[x] as Recipient;
				if (!m_bMassEmailMode) {

					for( int y = 0;y<m_oUrlReplacementValues.Count;y++ ){
						jlib.functions.util.FourValueClass oValues = m_oUrlReplacementValues[y] as jlib.functions.util.FourValueClass;
						oRecipient.Message = parse.replaceAll(oRecipient.Message, convert.cStr(oValues.Value1), getTrackingLink(convert.cStr(oValues.Value2), oRecipient.ID, convert.cStr(oValues.Value3), convert.cStr(oValues.Value4)));						
					}
				}
				oRecipient.send();
			}
			processQueue();
			return true;
		}

		public static string getTrackingLink(string sUrl, int iMessageRecipientID, string sName) {
			return getTrackingLink(sUrl, iMessageRecipientID, sName, "");
		}

		public static string getTrackingLink(string sUrl, int iMessageRecipientID, string sName, string sAction) {
            jlib.db.ado_helper oData = new jlib.db.ado_helper(parse.isNull(ConfigurationManager.AppSettings["sql_dsn.emailer"], ConfigurationManager.AppSettings["sql.dsn"]));

			DataTable oTracking = sqlbuilder.getNewRecord(oData, "d_url_alias", "recipient_id", iMessageRecipientID, "redirect_url", sUrl, "link_name", sName, "url", "/_" + parse.replaceAll(Guid.NewGuid().ToString(), "-", ""), "action", sAction);
			ado_helper.update(oTracking);
			return "http://www.guruguiden.no" + oTracking.Rows[0]["url"];
		}

		public static int processQueue() {
            jlib.db.ado_helper oData = new jlib.db.ado_helper(parse.isNull(ConfigurationManager.AppSettings["sql_dsn.emailer"], ConfigurationManager.AppSettings["sql.dsn"]));
			DataTable oQueue = sqlbuilder.getDataTable(oData, "select d_message_recipients.*, d_message_bodies.html_body as massemail_html, d_message_bodies.text_body as massemail_text from d_message_recipients left join d_message_bodies on body_id = d_message_bodies.id where d_message_recipients.deleted = 0 AND sent_date is null");
				
			for (int x = 0; x < oQueue.Rows.Count; x++) {
				
				if( convert.cStr( oQueue.Rows[x]["to_email"]) == "" && convert.cInt( oQueue.Rows[x]["to_id"]) > 0) oQueue.Rows[x]["to_email"] = convert.cStr( sqlbuilder.executeSelectValue( "d_users", "email", "id", oQueue.Rows[x]["to_id"]));
				if( convert.cStr( oQueue.Rows[x]["from_email"]) == "" && convert.cInt( oQueue.Rows[x]["from_id"]) > 0) oQueue.Rows[x]["from_email"] = convert.cStr( sqlbuilder.executeSelectValue( "d_users", "email", "id", oQueue.Rows[x]["from_id"]));

				//is this part of mass email send-out?
				if (convert.cStr(oQueue.Rows[x]["from_email"]) != "" && convert.cStr(oQueue.Rows[x]["to_email"]) != "") {
					if (convert.cInt(oQueue.Rows[x]["body_id"]) > 0)
						jlib.net.SMTP.sendEmail(convert.cStr(oQueue.Rows[x]["from_email"]), convert.cStr(oQueue.Rows[x]["to_email"]), convert.cStr(oQueue.Rows[x]["subject"]), (convert.cBool(convert.cStr(oQueue.Rows[x]["is_html"])) ? convert.cStr(oQueue.Rows[x]["massemail_html"]) : convert.cStr(oQueue.Rows[x]["massemail_text"])), convert.cBool(convert.cStr(oQueue.Rows[x]["is_html"])), ConfigurationManager.AppSettings["server.smtp"]);
					else
						jlib.net.SMTP.sendEmail(convert.cStr(oQueue.Rows[x]["from_email"]), convert.cStr(oQueue.Rows[x]["to_email"]), convert.cStr(oQueue.Rows[x]["subject"]), convert.cStr(oQueue.Rows[x]["body"]), convert.cBool(convert.cStr(oQueue.Rows[x]["is_html"])), ConfigurationManager.AppSettings["server.smtp"]);
				}
				sqlbuilder.executeUpdate(oData, "d_message_recipients", "id", oQueue.Rows[x]["id"], "sent_date", System.DateTime.Now);
			}
			sqlbuilder.executeInsert("d_log", "event", "gugu.email.queue.sent", "param", oQueue.Rows.Count, "posted_date", System.DateTime.Now);
			return oQueue.Rows.Count;
		}


		public class Recipient {
			private email m_oEmail;
			private bool m_bHtml = true;
			private string m_sToEmail = "";
			private string m_sToName = "";
			private string m_sMessage = "";
			private int m_iToID = 0;
			private int m_iID = 0;
			
			private bool m_bMessageModified = false;


			public Recipient(email oEmail, bool bHtml, string sToName, string sToEmail, int iToID, params object []oReplacementValues) {

				m_oEmail = oEmail;
				m_bHtml = bHtml;
				m_sToEmail = sToEmail;
				m_sToName = sToName;
				m_iToID = iToID;

				if (!m_oEmail.m_bMassEmailMode) {
					m_sMessage = (bHtml ? m_oEmail.m_sHtmlMessage : m_oEmail.m_sTextMessage);
					for (int x = 0; x < oReplacementValues.Length; x=x+2)
						m_sMessage = parse.replaceAll(m_sMessage,convert.cStr(oReplacementValues[x]), convert.cStr(oReplacementValues[x + 1]));
				}
				
			}

			public string Message {
				get {
					return m_sMessage;
				}
				set {
					m_sMessage = value;
					m_bMessageModified = true;
				}
			}

			public bool MessageModified {
				get {
					return m_bMessageModified;
				}
			}

            public bool Html {
                get {
                    return m_bHtml;
                }
            }

			public int ID {
				get {
					return m_iID;
				}
				set {
					m_iID = value;
				}
			}

			public void send() { save(false); }
			public void save() { save(true); }
			public void save(bool bDeleted) {

                jlib.db.ado_helper oData = new jlib.db.ado_helper(parse.isNull(ConfigurationManager.AppSettings["sql_dsn.emailer"], ConfigurationManager.AppSettings["sql.dsn"]));
				JDataTable oDT;
				if (m_iID == 0) {
					oDT = sqlbuilder.getNewRecord(oData, "d_message_recipients");
				} else {
					oDT = sqlbuilder.executeSelect(oData, "d_message_recipients", "id", m_iID);
				}
				//oDT.Rows[0] = 
					sqlbuilder.setValues(oDT.Rows[0], "deleted", (bDeleted?1:0), "to_email", m_sToEmail, "to_name", m_sToName, "to_id", m_iToID, "is_html", m_bHtml,
					"body_id", m_oEmail.m_iBodyID, "body", m_sMessage, "subject", m_oEmail.m_sSubject, "from_email", m_oEmail.m_sSenderEmail,
					"from_name", m_oEmail.m_sSenderName, "from_id", m_oEmail.m_iSenderID, "template_name", m_oEmail.m_sTemplateName);

                ado_helper.update(oDT);
				m_iID = convert.cInt(oDT.Rows[0]["id"]);
			}
		}

		public class emailBody {

			string m_sHtmlMessage = "";
			string m_sTextMessage = "";
			public emailBody(string sTemplateName) {

				m_sHtmlMessage = io.read_file(System.Web.HttpContext.Current.Server.MapPath("/inc/media/templates/" + sTemplateName + ".htm"));
				m_sTextMessage = io.read_file(System.Web.HttpContext.Current.Server.MapPath("/inc/media/templates/" + sTemplateName + ".txt"));
			}

			public string Text {
				get {
					return m_sTextMessage;
				}
				set {
					m_sTextMessage = value;
				}
			}

			public string Html {
				get {
					return m_sHtmlMessage;
				}
				set {
					m_sHtmlMessage = value;
				}
			}

			public void replaceAll(params string []sReplacementPairs) {
				m_sHtmlMessage = parse.replaceAll(m_sHtmlMessage, sReplacementPairs);
				m_sTextMessage = parse.replaceAll(m_sTextMessage, sReplacementPairs);
			}

			public void formatString(string sFormatStringArguments, IDataRow oRow) {
				m_sHtmlMessage= jlib.helpers.general.formatString(m_sHtmlMessage, sFormatStringArguments, oRow);
				m_sTextMessage = jlib.helpers.general.formatString(m_sTextMessage, sFormatStringArguments, oRow);
			}
		}
	}
}
