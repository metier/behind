﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jlib.helpers.Structures
{
	[Serializable]
	public class SerializableException
	{
		//constructor
		public SerializableException() { }
		public SerializableException(Exception e)
		{
			if(e != null)
			{
				this.Message = e.Message;
				this.Source = e.Source;
				this.StackTrace = e.StackTrace;

				if (e.InnerException != null) 
					this.InnerException = new SerializableException(e.InnerException);
			}
		}

		//properties
		public string Message { get; set; }
		public string Source { get; set; }
		public string StackTrace { get; set; }
		public SerializableException InnerException { get; set; }

	}
}
