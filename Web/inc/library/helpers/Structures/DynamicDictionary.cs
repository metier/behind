﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Dynamic;

namespace jlib.helpers.Structures
{
	[Serializable]
	public class DynamicDictionary : DynamicObject
	{
		// The inner dictionary.
		private Dictionary<string, object> _Dictionary = new Dictionary<string, object>();
		public Dictionary<string, object> Dictionary { get { return this._Dictionary; } }

		// This property returns the number of elements
		// in the inner dictionary.
		public int Count
		{
			get
			{
				return _Dictionary.Count;
			}
		}

		// If you try to get a value of a property 
		// not defined in the class, this method is called.
		public override bool TryGetMember(GetMemberBinder binder, out object result)
		{
			// Converting the property name to lowercase
			// so that property names become case-insensitive.
			string name = binder.Name;

			// If the property name is found in a dictionary,
			// set the result parameter to the property value and return true.
			// Otherwise, return false.
			return _Dictionary.TryGetValue(name, out result);
		}

		// If you try to set a value of a property that is
		// not defined in the class, this method is called.
		public override bool TrySetMember(SetMemberBinder binder, object value)
		{
			// Converting the property name to lowercase
			// so that property names become case-insensitive.
			_Dictionary[binder.Name] = value;

			// You can always add a value to a dictionary,
			// so this method always returns true.
			return true;
		}

		public override bool TrySetIndex(SetIndexBinder binder, object[] indexes, object value)
		{
			if (_Dictionary.ContainsKey((string)indexes[0]))
				_Dictionary[(string)indexes[0]] = value;
			else
				_Dictionary.Add((string)indexes[0], value);
			return true;
		}
		public override bool TryGetIndex(GetIndexBinder binder, object[] indexes, out object result)
		{
			return _Dictionary.TryGetValue((string)indexes[0], out result);
		}

		public bool HasProperty(string Name)
		{
			return this.Dictionary.ContainsKey(Name);
		}
	}
}
