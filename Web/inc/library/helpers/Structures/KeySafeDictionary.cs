﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jlib.helpers.Structures
{
	public class KeySafeDictionary<K,V> : Dictionary<K,V>
	{
		//Constructors
		public KeySafeDictionary() : base() {}
		public KeySafeDictionary(int capacity) : base(capacity) { }
		public KeySafeDictionary(IEqualityComparer<K> comparer) : base(comparer) { }
		public KeySafeDictionary(IDictionary<K,V> items) : base(items) { }
		public KeySafeDictionary(int capacity, IEqualityComparer<K> comparer) : base(capacity, comparer) { }
		public KeySafeDictionary(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
		public KeySafeDictionary(IDictionary<K,V> items, IEqualityComparer<K> comparer) : base(items, comparer) { }

		//Methods
		public V this[K key, V defaultValue]
		{
			get
			{
				//Get Initial Value
				V output = (defaultValue == null) ? default(V) : defaultValue;

				this.TryGetValue(key, out output);

				return output;
			}

		}
	}
}
