﻿namespace jlib.helpers.FastMember
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Reflection;
    using System.Reflection.Emit;
    using System.Threading;

    public abstract class TypeAccessor
    {
        private static AssemblyBuilder assembly;
        private static int counter;
        private static ModuleBuilder module;
        private static readonly Hashtable nonPublicAccessors = new Hashtable();
        private static readonly Hashtable publicAccessorsOnly = new Hashtable();
        private static readonly MethodInfo strinqEquals = typeof(string).GetMethod("op_Equality", new Type[] { typeof(string), typeof(string) });
        private static readonly MethodInfo tryGetValue = typeof(Dictionary<string, int>).GetMethod("TryGetValue");

        protected TypeAccessor()
        {
        }

        private static void Cast(ILGenerator il, Type type, bool valueAsPointer)
        {
            if (type != typeof(object))
            {
                if (type.IsValueType)
                {
                    if (valueAsPointer)
                    {
                        il.Emit(OpCodes.Unbox, type);
                    }
                    else
                    {
                        il.Emit(OpCodes.Unbox_Any, type);
                    }
                }
                else
                {
                    il.Emit(OpCodes.Castclass, type);
                }
            }
        }

        public static TypeAccessor Create(Type type)
        {
            return Create(type, false);
        }

        public static TypeAccessor Create(Type type, bool allowNonPublicAccessors)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }
            Hashtable hashtable = allowNonPublicAccessors ? nonPublicAccessors : publicAccessorsOnly;
            TypeAccessor accessor = (TypeAccessor) hashtable[type];
            if (accessor != null)
            {
                return accessor;
            }
            lock (hashtable)
            {
                accessor = (TypeAccessor) hashtable[type];
                if (accessor == null)
                {
                    accessor = CreateNew(type, allowNonPublicAccessors);
                    hashtable[type] = accessor;
                }
                return accessor;
            }
        }

        public virtual object CreateNew()
        {
            throw new NotSupportedException();
        }

        private static TypeAccessor CreateNew(Type type, bool allowNonPublicAccessors)
        {
            ILGenerator iLGenerator;
            MethodInfo info7;
            if (typeof(IDynamicMetaObjectProvider).IsAssignableFrom(type))
            {
                return DynamicAccessor.Singleton;
            }
            PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
            Dictionary<string, int> map = new Dictionary<string, int>(StringComparer.Ordinal);
            List<MemberInfo> members = new List<MemberInfo>(properties.Length + fields.Length);
            int num = 0;
            foreach (PropertyInfo info in properties)
            {
                if (!map.ContainsKey(info.Name) && (info.GetIndexParameters().Length == 0))
                {
                    map.Add(info.Name, num++);
                    members.Add(info);
                }
            }
            foreach (FieldInfo info2 in fields)
            {
                if (!map.ContainsKey(info2.Name))
                {
                    map.Add(info2.Name, num++);
                    members.Add(info2);
                }
            }
            ConstructorInfo con = null;
            if (type.IsClass && !type.IsAbstract)
            {
                con = type.GetConstructor(Type.EmptyTypes);
            }
            if (!IsFullyPublic(type, properties, allowNonPublicAccessors))
            {
                DynamicMethod method = new DynamicMethod(type.FullName + "_get", typeof(object), new Type[] { typeof(int), typeof(object) }, type, true);
                DynamicMethod method2 = new DynamicMethod(type.FullName + "_set", null, new Type[] { typeof(int), typeof(object), typeof(object) }, type, true);
                WriteMapImpl(method.GetILGenerator(), type, members, null, allowNonPublicAccessors, true);
                WriteMapImpl(method2.GetILGenerator(), type, members, null, allowNonPublicAccessors, false);
                DynamicMethod method3 = null;
                if (con != null)
                {
                    method3 = new DynamicMethod(type.FullName + "_ctor", typeof(object), Type.EmptyTypes, type, true);
                    iLGenerator = method3.GetILGenerator();
                    iLGenerator.Emit(OpCodes.Newobj, con);
                    iLGenerator.Emit(OpCodes.Ret);
                }
                return new DelegateAccessor(map, (Func<int, object, object>) method.CreateDelegate(typeof(Func<int, object, object>)), (Action<int, object, object>) method2.CreateDelegate(typeof(Action<int, object, object>)), (method3 == null) ? null : ((Func<object>) method3.CreateDelegate(typeof(Func<object>))), type);
            }
            if (assembly == null)
            {
                AssemblyName name = new AssemblyName("FastMember_dynamic");
                assembly = AppDomain.CurrentDomain.DefineDynamicAssembly(name, AssemblyBuilderAccess.Run);
                module = assembly.DefineDynamicModule(name.Name);
            }
            TypeBuilder builder = module.DefineType(string.Concat(new object[] { "FastMember_dynamic.", type.Name, "_", Interlocked.Increment(ref counter) }), ((typeof(TypeAccessor).Attributes | TypeAttributes.Sealed) | TypeAttributes.Public) & ~TypeAttributes.Abstract, typeof(RuntimeTypeAccessor));
            iLGenerator = builder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, new Type[] { typeof(Dictionary<string, int>) }).GetILGenerator();
            iLGenerator.Emit(OpCodes.Ldarg_0);
            iLGenerator.Emit(OpCodes.Ldarg_1);
            FieldBuilder field = builder.DefineField("_map", typeof(Dictionary<string, int>), FieldAttributes.InitOnly | FieldAttributes.Private);
            iLGenerator.Emit(OpCodes.Stfld, field);
            iLGenerator.Emit(OpCodes.Ret);
            PropertyInfo property = typeof(TypeAccessor).GetProperty("Item");
            MethodInfo getMethod = property.GetGetMethod();
            MethodInfo setMethod = property.GetSetMethod();
            MethodBuilder methodInfoBody = builder.DefineMethod(getMethod.Name, getMethod.Attributes & ~MethodAttributes.Abstract, typeof(object), new Type[] { typeof(object), typeof(string) });
            WriteMapImpl(methodInfoBody.GetILGenerator(), type, members, field, allowNonPublicAccessors, true);
            builder.DefineMethodOverride(methodInfoBody, getMethod);
            methodInfoBody = builder.DefineMethod(setMethod.Name, setMethod.Attributes & ~MethodAttributes.Abstract, null, new Type[] { typeof(object), typeof(string), typeof(object) });
            WriteMapImpl(methodInfoBody.GetILGenerator(), type, members, field, allowNonPublicAccessors, false);
            builder.DefineMethodOverride(methodInfoBody, setMethod);
            if (con != null)
            {
                info7 = typeof(TypeAccessor).GetProperty("CreateNewSupported").GetGetMethod();
                methodInfoBody = builder.DefineMethod(info7.Name, info7.Attributes, info7.ReturnType, Type.EmptyTypes);
                iLGenerator = methodInfoBody.GetILGenerator();
                iLGenerator.Emit(OpCodes.Ldc_I4_1);
                iLGenerator.Emit(OpCodes.Ret);
                builder.DefineMethodOverride(methodInfoBody, info7);
                info7 = typeof(TypeAccessor).GetMethod("CreateNew");
                methodInfoBody = builder.DefineMethod(info7.Name, info7.Attributes, info7.ReturnType, Type.EmptyTypes);
                iLGenerator = methodInfoBody.GetILGenerator();
                iLGenerator.Emit(OpCodes.Newobj, con);
                iLGenerator.Emit(OpCodes.Ret);
                builder.DefineMethodOverride(methodInfoBody, info7);
            }
            info7 = typeof(RuntimeTypeAccessor).GetProperty("Type", BindingFlags.NonPublic | BindingFlags.Instance).GetGetMethod(true);
            methodInfoBody = builder.DefineMethod(info7.Name, info7.Attributes & ~MethodAttributes.Abstract, info7.ReturnType, Type.EmptyTypes);
            iLGenerator = methodInfoBody.GetILGenerator();
            iLGenerator.Emit(OpCodes.Ldtoken, type);
            iLGenerator.Emit(OpCodes.Call, typeof(Type).GetMethod("GetTypeFromHandle"));
            iLGenerator.Emit(OpCodes.Ret);
            builder.DefineMethodOverride(methodInfoBody, info7);
            return (TypeAccessor) Activator.CreateInstance(builder.CreateType(), new object[] { map });
        }

        public virtual MemberSet GetMembers()
        {
            throw new NotSupportedException();
        }

        private static bool IsFullyPublic(Type type, PropertyInfo[] props, bool allowNonPublicAccessors)
        {
            while (type.IsNestedPublic)
            {
                type = type.DeclaringType;
            }
            if (!type.IsPublic)
            {
                return false;
            }
            if (allowNonPublicAccessors)
            {
                for (int i = 0; i < props.Length; i++)
                {
                    if ((props[i].GetGetMethod(true) != null) && (props[i].GetGetMethod(false) == null))
                    {
                        return false;
                    }
                    if ((props[i].GetSetMethod(true) != null) && (props[i].GetSetMethod(false) == null))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private static void WriteMapImpl(ILGenerator il, Type type, List<MemberInfo> members, FieldBuilder mapField, bool allowNonPublicAccessors, bool isGet)
        {
            OpCode code;
            OpCode code2;
            OpCode code3;
            Label label = il.DefineLabel();
            if (mapField == null)
            {
                code2 = OpCodes.Ldarg_0;
                code = OpCodes.Ldarg_1;
                code3 = OpCodes.Ldarg_2;
            }
            else
            {
                il.DeclareLocal(typeof(int));
                code2 = OpCodes.Ldloc_0;
                code = OpCodes.Ldarg_1;
                code3 = OpCodes.Ldarg_3;
                il.Emit(OpCodes.Ldarg_0);
                il.Emit(OpCodes.Ldfld, mapField);
                il.Emit(OpCodes.Ldarg_2);
                il.Emit(OpCodes.Ldloca_S, (byte) 0);
                il.EmitCall(OpCodes.Callvirt, tryGetValue, null);
                il.Emit(OpCodes.Brfalse, label);
            }
            Label[] labels = new Label[members.Count];
            for (int i = 0; i < labels.Length; i++)
            {
                labels[i] = il.DefineLabel();
            }
            il.Emit(code2);
            il.Emit(OpCodes.Switch, labels);
            il.MarkLabel(label);
            il.Emit(OpCodes.Ldstr, "name");
            il.Emit(OpCodes.Newobj, typeof(ArgumentOutOfRangeException).GetConstructor(new Type[] { typeof(string) }));
            il.Emit(OpCodes.Throw);
            for (int j = 0; j < labels.Length; j++)
            {
                il.MarkLabel(labels[j]);
                MemberInfo info = members[j];
                bool flag = true;
                switch (info.MemberType)
                {
                    case MemberTypes.Field:
                    {
                        FieldInfo field = (FieldInfo) info;
                        il.Emit(code);
                        Cast(il, type, true);
                        if (isGet)
                        {
                            il.Emit(OpCodes.Ldfld, field);
                            if (field.FieldType.IsValueType)
                            {
                                il.Emit(OpCodes.Box, field.FieldType);
                            }
                        }
                        else
                        {
                            il.Emit(code3);
                            Cast(il, field.FieldType, false);
                            il.Emit(OpCodes.Stfld, field);
                        }
                        il.Emit(OpCodes.Ret);
                        flag = false;
                        break;
                    }
                    case MemberTypes.Property:
                    {
                        MethodInfo info4;
                        PropertyInfo info3 = (PropertyInfo) info;
                        if (info3.CanRead && ((info4 = isGet ? info3.GetGetMethod(allowNonPublicAccessors) : info3.GetSetMethod(allowNonPublicAccessors)) != null))
                        {
                            il.Emit(code);
                            Cast(il, type, true);
                            if (isGet)
                            {
                                il.EmitCall(type.IsValueType ? OpCodes.Call : OpCodes.Callvirt, info4, null);
                                if (info3.PropertyType.IsValueType)
                                {
                                    il.Emit(OpCodes.Box, info3.PropertyType);
                                }
                            }
                            else
                            {
                                il.Emit(code3);
                                Cast(il, info3.PropertyType, false);
                                il.EmitCall(type.IsValueType ? OpCodes.Call : OpCodes.Callvirt, info4, null);
                            }
                            il.Emit(OpCodes.Ret);
                            flag = false;
                        }
                        break;
                    }
                }
                if (flag)
                {
                    il.Emit(OpCodes.Br, label);
                }
            }
        }

        public virtual bool CreateNewSupported
        {
            get
            {
                return false;
            }
        }

        public virtual bool GetMembersSupported
        {
            get
            {
                return false;
            }
        }

        public abstract object this[object target, string name] { get; set; }

        private sealed class DelegateAccessor : TypeAccessor.RuntimeTypeAccessor
        {
            private readonly Func<object> ctor;
            private readonly Func<int, object, object> getter;
            private readonly Dictionary<string, int> map;
            private readonly Action<int, object, object> setter;
            private readonly System.Type type;

            public DelegateAccessor(Dictionary<string, int> map, Func<int, object, object> getter, Action<int, object, object> setter, Func<object> ctor, System.Type type)
            {
                this.map = map;
                this.getter = getter;
                this.setter = setter;
                this.ctor = ctor;
                this.type = type;
            }

            public override object CreateNew()
            {
                if (this.ctor == null)
                {
                    return base.CreateNew();
                }
                return this.ctor();
            }

            public override bool CreateNewSupported
            {
                get
                {
                    return (this.ctor != null);
                }
            }

            public override object this[object target, string name]
            {
                get
                {
                    int num;
                    if (!this.map.TryGetValue(name, out num))
                    {
                        throw new ArgumentOutOfRangeException("name");
                    }
                    return this.getter(num, target);
                }
                set
                {
                    int num;
                    if (!this.map.TryGetValue(name, out num))
                    {
                        throw new ArgumentOutOfRangeException("name");
                    }
                    this.setter(num, target, value);
                }
            }

            protected override System.Type Type
            {
                get
                {
                    return this.type;
                }
            }
        }

        private sealed class DynamicAccessor : TypeAccessor
        {
            public static readonly TypeAccessor.DynamicAccessor Singleton = new TypeAccessor.DynamicAccessor();

            private DynamicAccessor()
            {
            }

            public override object this[object target, string name]
            {
                get
                {
                    return CallSiteCache.GetValue(name, target);
                }
                set
                {
                    CallSiteCache.SetValue(name, target, value);
                }
            }
        }

        protected abstract class RuntimeTypeAccessor : TypeAccessor
        {
            private MemberSet members;

            protected RuntimeTypeAccessor()
            {
            }

            public override MemberSet GetMembers()
            {
                return (this.members ?? (this.members = new MemberSet(this.Type)));
            }

            public override bool GetMembersSupported
            {
                get
                {
                    return true;
                }
            }

            protected abstract System.Type Type { get; }
        }
    }
}

