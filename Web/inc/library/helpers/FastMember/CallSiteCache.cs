﻿//Based on FastMember http://www.nuget.org/packages/FastMember, v1.0.0.11
namespace jlib.helpers.FastMember
{
    using Microsoft.CSharp.RuntimeBinder;
    using System;
    using System.Collections;
    using System.Runtime.CompilerServices;

    internal static class CallSiteCache
    {
        private static readonly Hashtable getters = new Hashtable();
        private static readonly Hashtable setters = new Hashtable();

        internal static object GetValue(string name, object target)
        {
            CallSite<Func<CallSite, object, object>> site = (CallSite<Func<CallSite, object, object>>) getters[name];
            if (site == null)
            {
                CallSite<Func<CallSite, object, object>> site2 = CallSite<Func<CallSite, object, object>>.Create(Binder.GetMember(CSharpBinderFlags.None, name, typeof(CallSiteCache), new CSharpArgumentInfo[] { CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, null) }));
                lock (getters)
                {
                    site = (CallSite<Func<CallSite, object, object>>) getters[name];
                    if (site == null)
                    {
                        getters[name] = site = site2;
                    }
                }
            }
            return site.Target(site, target);
        }

        internal static void SetValue(string name, object target, object value)
        {
            CallSite<Func<CallSite, object, object, object>> site = (CallSite<Func<CallSite, object, object, object>>) setters[name];
            if (site == null)
            {
                CallSite<Func<CallSite, object, object, object>> site2 = CallSite<Func<CallSite, object, object, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, name, typeof(CallSiteCache), new CSharpArgumentInfo[] { CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, null), CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, null) }));
                lock (setters)
                {
                    site = (CallSite<Func<CallSite, object, object, object>>) setters[name];
                    if (site == null)
                    {
                        setters[name] = site = site2;
                    }
                }
            }
            site.Target(site, target, value);
        }
    }
}

