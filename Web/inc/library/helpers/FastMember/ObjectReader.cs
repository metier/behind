﻿namespace jlib.helpers.FastMember
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Reflection;

    public class ObjectReader : IDataReader, IDisposable, IDataRecord
    {
        private readonly TypeAccessor accessor;
        private readonly BitArray allowNull;
        private object current;
        private readonly Type[] effectiveTypes;
        private readonly string[] memberNames;
        private IEnumerator source;

        public ObjectReader(Type type, IEnumerable source, params string[] members)
        {
            if (source == null)
            {
                throw new ArgumentOutOfRangeException("source");
            }
            bool flag = (members == null) || (members.Length == 0);
            this.accessor = TypeAccessor.Create(type);
            if (this.accessor.GetMembersSupported)
            {
                MemberSet set = this.accessor.GetMembers();
                if (flag)
                {
                    members = new string[set.Count];
                    for (int j = 0; j < members.Length; j++)
                    {
                        members[j] = set[j].Name;
                    }
                }
                this.allowNull = new BitArray(members.Length);
                this.effectiveTypes = new Type[members.Length];
                for (int i = 0; i < members.Length; i++)
                {
                    Type type2 = null;
                    bool flag2 = true;
                    string str = members[i];
                    foreach (Member member in set)
                    {
                        if (member.Name == str)
                        {
                            if (type2 == null)
                            {
                                Type nullableType = member.Type;
                                type2 = Nullable.GetUnderlyingType(nullableType) ?? nullableType;
                                flag2 = !type2.IsValueType || !(type2 == nullableType);
                            }
                            else
                            {
                                type2 = null;
                                break;
                            }
                        }
                    }
                    this.allowNull[i] = flag2;
                    this.effectiveTypes[i] = type2 ?? typeof(object);
                }
            }
            else if (flag)
            {
                throw new InvalidOperationException("Member information is not available for this type; the required members must be specified explicitly");
            }
            this.current = null;
            this.memberNames = (string[]) members.Clone();
            this.source = source.GetEnumerator();
        }

        public static ObjectReader Create<T>(IEnumerable<T> source, params string[] members)
        {
            return new ObjectReader(typeof(T), source, members);
        }

        public void Dispose()
        {
            this.current = null;
            IDisposable source = this.source as IDisposable;
            this.source = null;
            if (source != null)
            {
                source.Dispose();
            }
        }

        void IDataReader.Close()
        {
            this.Dispose();
        }

        DataTable IDataReader.GetSchemaTable()
        {
            DataTable table2 = new DataTable();
            table2.Columns.Add("ColumnOrdinal", typeof(int));
            table2.Columns.Add("ColumnName", typeof(string));
            table2.Columns.Add("DataType", typeof(Type));
            table2.Columns.Add("ColumnSize", typeof(int));
            table2.Columns.Add("AllowDBNull", typeof(bool));
            DataTable table = table2;
            object[] values = new object[5];
            for (int i = 0; i < this.memberNames.Length; i++)
            {
                values[0] = i;
                values[1] = this.memberNames[i];
                values[2] = (this.effectiveTypes == null) ? typeof(object) : this.effectiveTypes[i];
                values[3] = -1;
                values[4] = (this.allowNull == null) ? ((object) 1) : ((object) this.allowNull[i]);
                table.Rows.Add(values);
            }
            return table;
        }

        bool IDataReader.NextResult()
        {
            return false;
        }

        bool IDataReader.Read()
        {
            IEnumerator source = this.source;
            if ((source != null) && source.MoveNext())
            {
                this.current = source.Current;
                return true;
            }
            this.current = null;
            return false;
        }

        bool IDataRecord.GetBoolean(int i)
        {
            return (bool) this[i];
        }

        byte IDataRecord.GetByte(int i)
        {
            return (byte) this[i];
        }

        long IDataRecord.GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            byte[] src = (byte[]) this[i];
            int num = src.Length - ((int) fieldOffset);
            if (num <= 0)
            {
                return 0L;
            }
            int count = Math.Min(length, num);
            Buffer.BlockCopy(src, (int) fieldOffset, buffer, bufferoffset, count);
            return (long) count;
        }

        char IDataRecord.GetChar(int i)
        {
            return (char) this[i];
        }

        long IDataRecord.GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            string str = (string) this[i];
            int num = str.Length - ((int) fieldoffset);
            if (num <= 0)
            {
                return 0L;
            }
            int count = Math.Min(length, num);
            str.CopyTo((int) fieldoffset, buffer, bufferoffset, count);
            return (long) count;
        }

        IDataReader IDataRecord.GetData(int i)
        {
            throw new NotSupportedException();
        }

        string IDataRecord.GetDataTypeName(int i)
        {
            return ((this.effectiveTypes == null) ? (typeof(object)) : (this.effectiveTypes[i])).Name;
        }

        DateTime IDataRecord.GetDateTime(int i)
        {
            return (DateTime) this[i];
        }

        decimal IDataRecord.GetDecimal(int i)
        {
            return (decimal) this[i];
        }

        double IDataRecord.GetDouble(int i)
        {
            return (double) this[i];
        }

        Type IDataRecord.GetFieldType(int i)
        {
            if (this.effectiveTypes != null)
            {
                return this.effectiveTypes[i];
            }
            return typeof(object);
        }

        float IDataRecord.GetFloat(int i)
        {
            return (float) this[i];
        }

        Guid IDataRecord.GetGuid(int i)
        {
            return (Guid) this[i];
        }

        short IDataRecord.GetInt16(int i)
        {
            return (short) this[i];
        }

        int IDataRecord.GetInt32(int i)
        {
            return (int) this[i];
        }

        long IDataRecord.GetInt64(int i)
        {
            return (long) this[i];
        }

        string IDataRecord.GetName(int i)
        {
            return this.memberNames[i];
        }

        int IDataRecord.GetOrdinal(string name)
        {
            return Array.IndexOf<string>(this.memberNames, name);
        }

        string IDataRecord.GetString(int i)
        {
            return (string) this[i];
        }

        object IDataRecord.GetValue(int i)
        {
            return this[i];
        }

        int IDataRecord.GetValues(object[] values)
        {
            string[] memberNames = this.memberNames;
            object current = this.current;
            TypeAccessor accessor = this.accessor;
            int num = Math.Min(values.Length, memberNames.Length);
            for (int i = 0; i < num; i++)
            {
                values[i] = accessor[current, memberNames[i]] ?? DBNull.Value;
            }
            return num;
        }

        bool IDataRecord.IsDBNull(int i)
        {
            return (this[i] is DBNull);
        }

        public object this[int i]
        {
            get
            {
                return (this.accessor[this.current, this.memberNames[i]] ?? DBNull.Value);
            }
        }

        int IDataReader.Depth
        {
            get
            {
                return 0;
            }
        }

        bool IDataReader.IsClosed
        {
            get
            {
                return (this.source == null);
            }
        }

        int IDataReader.RecordsAffected
        {
            get
            {
                return 0;
            }
        }

        int IDataRecord.FieldCount
        {
            get
            {
                return this.memberNames.Length;
            }
        }

        object IDataRecord.this[string name]
        {
            get
            {
                return (this.accessor[this.current, name] ?? DBNull.Value);
            }
        }
    }
}

