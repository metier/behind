﻿namespace jlib.helpers.FastMember
{
    using System;
    using System.Reflection;

    public sealed class Member
    {
        private readonly MemberInfo member;

        internal Member(MemberInfo member)
        {
            this.member = member;
        }

        public bool IsDefined(System.Type attributeType)
        {
            if (attributeType == null)
            {
                throw new ArgumentNullException("attributeType");
            }
            return Attribute.IsDefined(this.member, attributeType);
        }

        public string Name
        {
            get
            {
                return this.member.Name;
            }
        }

        public System.Type Type
        {
            get
            {
                MemberTypes memberType = this.member.MemberType;
                if (memberType != MemberTypes.Field)
                {
                    if (memberType != MemberTypes.Property)
                    {
                        throw new NotSupportedException(this.member.MemberType.ToString());
                    }
                    return ((PropertyInfo) this.member).PropertyType;
                }
                return ((FieldInfo) this.member).FieldType;
            }
        }
    }
}

