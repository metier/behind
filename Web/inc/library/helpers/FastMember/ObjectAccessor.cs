﻿namespace jlib.helpers.FastMember
{
    using System;
    using System.Dynamic;
    using System.Reflection;

    public abstract class ObjectAccessor
    {
        protected ObjectAccessor()
        {
        }

        public static ObjectAccessor Create(object target)
        {
            return Create(target, false);
        }

        public static ObjectAccessor Create(object target, bool allowNonPublicAccessors)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }
            IDynamicMetaObjectProvider provider = target as IDynamicMetaObjectProvider;
            if (provider != null)
            {
                return new DynamicWrapper(provider);
            }
            return new TypeAccessorWrapper(target, TypeAccessor.Create(target.GetType()));
        }

        public override bool Equals(object obj)
        {
            return this.Target.Equals(obj);
        }

        public override int GetHashCode()
        {
            return this.Target.GetHashCode();
        }

        public override string ToString()
        {
            return this.Target.ToString();
        }

        public abstract object this[string name] { get; set; }

        public abstract object Target { get; }

        private sealed class DynamicWrapper : ObjectAccessor
        {
            private readonly IDynamicMetaObjectProvider target;

            public DynamicWrapper(IDynamicMetaObjectProvider target)
            {
                this.target = target;
            }

            public override object this[string name]
            {
                get
                {
                    return CallSiteCache.GetValue(name, this.target);
                }
                set
                {
                    CallSiteCache.SetValue(name, this.target, value);
                }
            }

            public override object Target
            {
                get
                {
                    return this.target;
                }
            }
        }

        private sealed class TypeAccessorWrapper : ObjectAccessor
        {
            private readonly TypeAccessor accessor;
            private readonly object target;

            public TypeAccessorWrapper(object target, TypeAccessor accessor)
            {
                this.target = target;
                this.accessor = accessor;
            }

            public override object this[string name]
            {
                get
                {
                    return this.accessor[this.target, name];
                }
                set
                {
                    this.accessor[this.target, name] = value;
                }
            }

            public override object Target
            {
                get
                {
                    return this.target;
                }
            }
        }
    }
}

