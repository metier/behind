﻿namespace jlib.helpers.FastMember
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    public sealed class MemberSet : IList<Member>, ICollection<Member>, IEnumerable<Member>, IEnumerable
    {
        private Member[] members;

        internal MemberSet(Type type)
        {
            this.members = (from member in type.GetProperties().Cast<MemberInfo>().Concat<MemberInfo>(type.GetFields().Cast<MemberInfo>()).OrderBy<MemberInfo, string>(x => x.Name, StringComparer.InvariantCulture) select new Member(member)).ToArray<Member>();
        }

        public IEnumerator<Member> GetEnumerator()
        {
            foreach (Member iteratorVariable0 in this.members)
            {
                yield return iteratorVariable0;
            }
        }

        void ICollection<Member>.Add(Member item)
        {
            throw new NotSupportedException();
        }

        void ICollection<Member>.Clear()
        {
            throw new NotSupportedException();
        }

        bool ICollection<Member>.Contains(Member item)
        {
            return this.members.Contains<Member>(item);
        }

        void ICollection<Member>.CopyTo(Member[] array, int arrayIndex)
        {
            this.members.CopyTo(array, arrayIndex);
        }

        bool ICollection<Member>.Remove(Member item)
        {
            throw new NotSupportedException();
        }

        int IList<Member>.IndexOf(Member member)
        {
            return Array.IndexOf<Member>(this.members, member);
        }

        void IList<Member>.Insert(int index, Member item)
        {
            throw new NotSupportedException();
        }

        void IList<Member>.RemoveAt(int index)
        {
            throw new NotSupportedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public int Count
        {
            get
            {
                return this.members.Length;
            }
        }

        public Member this[int index]
        {
            get
            {
                return this.members[index];
            }
        }

        bool ICollection<Member>.IsReadOnly
        {
            get
            {
                return true;
            }
        }

        Member IList<Member>.this[int index]
        {
            get
            {
                return this.members[index];
            }
            set
            {
                throw new NotSupportedException();
            }
        }

    }
}

