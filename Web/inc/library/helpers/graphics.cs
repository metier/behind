using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text;
using System.Threading;
using System.IO;
using System.Runtime.InteropServices;
using System.Web;
using System.Net;
using jlib.functions;

namespace jlib.helpers {
    /// <summary>
    /// Summary description for ado_helper.
    /// </summary>
    public class graphics {
        #region METHODS
        
            //Remove white and transparent borders
            public static Bitmap Crop(Bitmap bmp) {
                int w = bmp.Width;
                int h = bmp.Height;

                Func<int, bool> allWhiteRow = row => {
                    for (int i = 0; i < w; ++i){
                        Color C = bmp.GetPixel(i, row);
                        if (C.R != 255 && C.A!=0) return false;
                    }
                    return true;
                };

                Func<int, bool> allWhiteColumn = col => {
                    for (int i = 0; i < h; ++i) {
                        Color C = bmp.GetPixel(col, i);
                        if (C.R != 255 && C.A!=0) return false;
                    } return true;
                };

                int topmost = 0;
                for (int row = 0; row < h; ++row) {
                    if (allWhiteRow(row))
                        topmost = row;
                    else break;
                }

                int bottommost = 0;
                for (int row = h - 1; row >= 0; --row) {
                    if (allWhiteRow(row))
                        bottommost = row;
                    else break;
                }

                int leftmost = 0, rightmost = 0;
                for (int col = 0; col < w; ++col) {
                    if (allWhiteColumn(col))
                        leftmost = col;
                    else
                        break;
                }

                for (int col = w - 1; col >= 0; --col) {
                    if (allWhiteColumn(col))
                        rightmost = col;
                    else
                        break;
                }

                if (rightmost == 0) rightmost = w; // As reached left
                if (bottommost == 0) bottommost = h; // As reached top.

                int croppedWidth = rightmost - leftmost;
                int croppedHeight = bottommost - topmost;

                if (croppedWidth == 0) { // No border on left or right        
                    leftmost = 0;
                    croppedWidth = w;
                }

                if (croppedHeight == 0) { // No border on top or bottom        
                    topmost = 0;
                    croppedHeight = h;
                }

                try {
                    var target = new Bitmap(croppedWidth, croppedHeight, bmp.PixelFormat);
                    using (Graphics g = Graphics.FromImage(target)) {
                        g.DrawImage(bmp,
                          new RectangleF(0, 0, croppedWidth, croppedHeight),
                          new RectangleF(leftmost, topmost, croppedWidth, croppedHeight),
                          GraphicsUnit.Pixel);
                    }
                    return target;
                } catch (Exception ex) {
                    throw new Exception(
                      string.Format("Values are topmost={0} btm={1} left={2} right={3} croppedWidth={4} croppedHeight={5}", topmost, bottommost, leftmost, rightmost, croppedWidth, croppedHeight),
                      ex);
                }
            }

            public static byte[] RenderFontImage(string Data, string FontName, int FontSize, System.Drawing.FontStyle FontStyle, int OutputHeight, bool Barcode) {

                Data = convert.cStr(Barcode ? calculateBarcodeString(Data) : Data);
                if (Data == "") return null;
                //sQuery = System.Text.Encoding.ASCII.GetString(System.Text.Encoding.Unicode.GetBytes(new string((char)154, 1)));
                Bitmap oBitmap = new Bitmap(1, 1);

                int iWidth = 0;
                int iHeight = 0;

                // Create the Font object for the image text drawing.
                Font oFont = new Font(convert.cStr(FontName, "Arial"), convert.cInt(FontSize, 20), FontStyle, System.Drawing.GraphicsUnit.Pixel);

                // Create a graphics object to measure the text's width and height.
                Graphics oGraphics = Graphics.FromImage(oBitmap);

                // This is where the bitmap size is determined.
                iWidth = (int)oGraphics.MeasureString(Data, oFont).Width;
                iHeight = convert.cInt(OutputHeight, (int)oGraphics.MeasureString(Data, oFont).Height);

                // Create the bmpImage again with the correct size for the text and font.
                oBitmap = new Bitmap(oBitmap, new Size(iWidth, iHeight));

                // Add the colors to the new bitmap.
                oGraphics = Graphics.FromImage(oBitmap);

                // Set Background color
                oGraphics.Clear(Color.White);
                //oGraphics.Clear(Color.Transparent);
                oGraphics.SmoothingMode = SmoothingMode.Default;
                oGraphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
                //oGraphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint;
                oGraphics.DrawString(Data, oFont, new SolidBrush(Color.FromArgb(0, 0, 0)), 0, 0);
                oGraphics.Flush();

                MemoryStream oStream = new MemoryStream();
                oBitmap.Save(oStream, ImageFormat.Png);
                oBitmap.Dispose();
                return convert.cStreamToByteArray(oStream);
            }

            public static string calculateBarcodeString(string sInput) {
                System.Collections.Generic.List<byte> oBytes = new System.Collections.Generic.List<byte>();
                oBytes.Add(154);
                //System.Text.StringBuilder oSB = new System.Text.StringBuilder();
                //oSB.Append("�");//154
                //oSB.Append(sInput);

                int iChecksum = 104;//This is for calculating 128b
                for (int x = 0; x < sInput.Length; x++) {
                    oBytes.Add((byte)((int)sInput.Substring(x, 1).ToCharArray()[0]));
                    iChecksum += ((int)sInput.Substring(x, 1).ToCharArray()[0] - 32) * (x + 1);
                }
                //iChecksum = (iChecksum % 103)+32+18;

                iChecksum = iChecksum % 103;
                if (iChecksum + 32 >= 127)
                    iChecksum = iChecksum + 32 + 18;
                else
                    iChecksum = iChecksum + 32;

                //oSB.Append((char)iChecksum);
                //oSB.Append("�");//156
                oBytes.Add((byte)iChecksum);
                oBytes.Add(156);
                //char[] sRet = oSB.ToString().ToCharArray();
                for (int x = 0; x < oBytes.Count; x++)
                    if ((int)oBytes[x] == 32) oBytes[x] = (byte)128;

                return convert.cByteToString(oBytes.ToArray(), Encoding.GetEncoding(1252));
                //return new string(sRet);// System.Text.Encoding.ASCII.GetString(System.Text.Encoding.Unicode.GetBytes(new string(sRet)));
                //return System.Text.Encoding.Unicode.GetString(System.Text.Encoding.Unicode.GetBytes(sRet));// new string(sRet);
            }

            private static class Code128Code {

                public enum CodeSet {
                    CodeA
                    , CodeB
                    // ,CodeC   // not supported
                }

                /// <summary>
                /// Represent the set of code values to be output into barcode form
                /// </summary>
                public class Code128Content {
                    private int[] mCodeList;

                    /// <summary>
                    /// Create content based on a string of ASCII data
                    /// </summary>
                    /// <param name="AsciiData">the string that should be represented</param>
                    public Code128Content(string AsciiData) {
                        mCodeList = StringToCode128(AsciiData);
                    }

                    /// <summary>
                    /// Provides the Code128 code values representing the object's string
                    /// </summary>
                    public int[] Codes {
                        get {
                            return mCodeList;
                        }
                    }

                    /// <summary>
                    /// Transform the string into integers representing the Code128 codes
                    /// necessary to represent it
                    /// </summary>
                    /// <param name="AsciiData">String to be encoded</param>
                    /// <returns>Code128 representation</returns>
                    private int[] StringToCode128(string AsciiData) {
                        // turn the string into ascii byte data
                        byte[] asciiBytes = Encoding.ASCII.GetBytes(AsciiData);

                        // decide which codeset to start with
                        Code128Code.CodeSetAllowed csa1 = asciiBytes.Length > 0 ? Code128Code.CodesetAllowedForChar(asciiBytes[0]) : Code128Code.CodeSetAllowed.CodeAorB;
                        Code128Code.CodeSetAllowed csa2 = asciiBytes.Length > 0 ? Code128Code.CodesetAllowedForChar(asciiBytes[1]) : Code128Code.CodeSetAllowed.CodeAorB;
                        CodeSet currcs = GetBestStartSet(csa1, csa2);

                        // set up the beginning of the barcode
                        System.Collections.ArrayList codes = new System.Collections.ArrayList(asciiBytes.Length + 3); // assume no codeset changes, account for start, checksum, and stop
                        codes.Add(Code128Code.StartCodeForCodeSet(currcs));

                        // add the codes for each character in the string
                        for (int i = 0; i < asciiBytes.Length; i++) {
                            int thischar = asciiBytes[i];
                            int nextchar = asciiBytes.Length > (i + 1) ? asciiBytes[i + 1] : -1;

                            codes.AddRange(Code128Code.CodesForChar(thischar, nextchar, ref currcs));
                        }

                        // calculate the check digit
                        int checksum = (int)(codes[0]);
                        for (int i = 1; i < codes.Count; i++) {
                            checksum += i * (int)(codes[i]);
                        }
                        codes.Add(checksum % 103);

                        codes.Add(Code128Code.StopCode());

                        int[] result = codes.ToArray(typeof(int)) as int[];
                        return result;
                    }

                    /// <summary>
                    /// Determines the best starting code set based on the the first two 
                    /// characters of the string to be encoded
                    /// </summary>
                    /// <param name="csa1">First character of input string</param>
                    /// <param name="csa2">Second character of input string</param>
                    /// <returns>The codeset determined to be best to start with</returns>
                    private CodeSet GetBestStartSet(Code128Code.CodeSetAllowed csa1, Code128Code.CodeSetAllowed csa2) {
                        int vote = 0;

                        vote += (csa1 == Code128Code.CodeSetAllowed.CodeA) ? 1 : 0;
                        vote += (csa1 == Code128Code.CodeSetAllowed.CodeB) ? -1 : 0;
                        vote += (csa2 == Code128Code.CodeSetAllowed.CodeA) ? 1 : 0;
                        vote += (csa2 == Code128Code.CodeSetAllowed.CodeB) ? -1 : 0;

                        return (vote > 0) ? CodeSet.CodeA : CodeSet.CodeB;   // ties go to codeB due to my own prejudices
                    }
                }

                #region Code patterns

                private const int cSHIFT = 98;
                private const int cCODEA = 101;
                private const int cCODEB = 100;

                private const int cSTARTA = 103;
                private const int cSTARTB = 104;
                private const int cSTOP = 106;

                /// <summary>
                /// Get the Code128 code value(s) to represent an ASCII character, with 
                /// optional look-ahead for length optimization
                /// </summary>
                /// <param name="CharAscii">The ASCII value of the character to translate</param>
                /// <param name="LookAheadAscii">The next character in sequence (or -1 if none)</param>
                /// <param name="CurrCodeSet">The current codeset, that the returned codes need to follow;
                /// if the returned codes change that, then this value will be changed to reflect it</param>
                /// <returns>An array of integers representing the codes that need to be output to produce the 
                /// given character</returns>
                public static int[] CodesForChar(int CharAscii, int LookAheadAscii, ref CodeSet CurrCodeSet) {
                    int[] result;
                    int shifter = -1;

                    if (!CharCompatibleWithCodeset(CharAscii, CurrCodeSet)) {
                        // if we have a lookahead character AND if the next character is ALSO not compatible
                        if ((LookAheadAscii != -1) && !CharCompatibleWithCodeset(LookAheadAscii, CurrCodeSet)) {
                            // we need to switch code sets
                            switch (CurrCodeSet) {
                                case CodeSet.CodeA:
                                    shifter = cCODEB;
                                    CurrCodeSet = CodeSet.CodeB;
                                    break;
                                case CodeSet.CodeB:
                                    shifter = cCODEA;
                                    CurrCodeSet = CodeSet.CodeA;
                                    break;
                            }
                        } else {
                            // no need to switch code sets, a temporary SHIFT will suffice
                            shifter = cSHIFT;
                        }
                    }

                    if (shifter != -1) {
                        result = new int[2];
                        result[0] = shifter;
                        result[1] = CodeValueForChar(CharAscii);
                    } else {
                        result = new int[1];
                        result[0] = CodeValueForChar(CharAscii);
                    }

                    return result;
                }

                /// <summary>
                /// Tells us which codesets a given character value is allowed in
                /// </summary>
                /// <param name="CharAscii">ASCII value of character to look at</param>
                /// <returns>Which codeset(s) can be used to represent this character</returns>
                public static CodeSetAllowed CodesetAllowedForChar(int CharAscii) {
                    if (CharAscii >= 32 && CharAscii <= 95) {
                        return CodeSetAllowed.CodeAorB;
                    } else {
                        return (CharAscii < 32) ? CodeSetAllowed.CodeA : CodeSetAllowed.CodeB;
                    }
                }

                /// <summary>
                /// Determine if a character can be represented in a given codeset
                /// </summary>
                /// <param name="CharAscii">character to check for</param>
                /// <param name="currcs">codeset context to test</param>
                /// <returns>true if the codeset contains a representation for the ASCII character</returns>
                public static bool CharCompatibleWithCodeset(int CharAscii, CodeSet currcs) {
                    CodeSetAllowed csa = CodesetAllowedForChar(CharAscii);
                    return csa == CodeSetAllowed.CodeAorB
                             || (csa == CodeSetAllowed.CodeA && currcs == CodeSet.CodeA)
                             || (csa == CodeSetAllowed.CodeB && currcs == CodeSet.CodeB);
                }

                /// <summary>
                /// Gets the integer code128 code value for a character (assuming the appropriate code set)
                /// </summary>
                /// <param name="CharAscii">character to convert</param>
                /// <returns>code128 symbol value for the character</returns>
                public static int CodeValueForChar(int CharAscii) {
                    return (CharAscii >= 32) ? CharAscii - 32 : CharAscii + 64;
                }

                /// <summary>
                /// Return the appropriate START code depending on the codeset we want to be in
                /// </summary>
                /// <param name="cs">The codeset you want to start in</param>
                /// <returns>The code128 code to start a barcode in that codeset</returns>
                public static int StartCodeForCodeSet(CodeSet cs) {
                    return cs == CodeSet.CodeA ? cSTARTA : cSTARTB;
                }

                /// <summary>
                /// Return the Code128 stop code
                /// </summary>
                /// <returns>the stop code</returns>
                public static int StopCode() {
                    return cSTOP;
                }

                /// <summary>
                /// Indicates which code sets can represent a character -- CodeA, CodeB, or either
                /// </summary>
                public enum CodeSetAllowed {
                    CodeA,
                    CodeB,
                    CodeAorB
                }

                // in principle these rows should each have 6 elements
                // however, the last one -- STOP -- has 7. The cost of the
                // extra integers is trivial, and this lets the code flow
                // much more elegantly
                private static readonly int[,] cPatterns = 
                     {
                        {2,1,2,2,2,2,0,0},  // 0
                        {2,2,2,1,2,2,0,0},  // 1
                        {2,2,2,2,2,1,0,0},  // 2
                        {1,2,1,2,2,3,0,0},  // 3
                        {1,2,1,3,2,2,0,0},  // 4
                        {1,3,1,2,2,2,0,0},  // 5
                        {1,2,2,2,1,3,0,0},  // 6
                        {1,2,2,3,1,2,0,0},  // 7
                        {1,3,2,2,1,2,0,0},  // 8
                        {2,2,1,2,1,3,0,0},  // 9
                        {2,2,1,3,1,2,0,0},  // 10
                        {2,3,1,2,1,2,0,0},  // 11
                        {1,1,2,2,3,2,0,0},  // 12
                        {1,2,2,1,3,2,0,0},  // 13
                        {1,2,2,2,3,1,0,0},  // 14
                        {1,1,3,2,2,2,0,0},  // 15
                        {1,2,3,1,2,2,0,0},  // 16
                        {1,2,3,2,2,1,0,0},  // 17
                        {2,2,3,2,1,1,0,0},  // 18
                        {2,2,1,1,3,2,0,0},  // 19
                        {2,2,1,2,3,1,0,0},  // 20
                        {2,1,3,2,1,2,0,0},  // 21
                        {2,2,3,1,1,2,0,0},  // 22
                        {3,1,2,1,3,1,0,0},  // 23
                        {3,1,1,2,2,2,0,0},  // 24
                        {3,2,1,1,2,2,0,0},  // 25
                        {3,2,1,2,2,1,0,0},  // 26
                        {3,1,2,2,1,2,0,0},  // 27
                        {3,2,2,1,1,2,0,0},  // 28
                        {3,2,2,2,1,1,0,0},  // 29
                        {2,1,2,1,2,3,0,0},  // 30
                        {2,1,2,3,2,1,0,0},  // 31
                        {2,3,2,1,2,1,0,0},  // 32
                        {1,1,1,3,2,3,0,0},  // 33
                        {1,3,1,1,2,3,0,0},  // 34
                        {1,3,1,3,2,1,0,0},  // 35
                        {1,1,2,3,1,3,0,0},  // 36
                        {1,3,2,1,1,3,0,0},  // 37
                        {1,3,2,3,1,1,0,0},  // 38
                        {2,1,1,3,1,3,0,0},  // 39
                        {2,3,1,1,1,3,0,0},  // 40
                        {2,3,1,3,1,1,0,0},  // 41
                        {1,1,2,1,3,3,0,0},  // 42
                        {1,1,2,3,3,1,0,0},  // 43
                        {1,3,2,1,3,1,0,0},  // 44
                        {1,1,3,1,2,3,0,0},  // 45
                        {1,1,3,3,2,1,0,0},  // 46
                        {1,3,3,1,2,1,0,0},  // 47
                        {3,1,3,1,2,1,0,0},  // 48
                        {2,1,1,3,3,1,0,0},  // 49
                        {2,3,1,1,3,1,0,0},  // 50
                        {2,1,3,1,1,3,0,0},  // 51
                        {2,1,3,3,1,1,0,0},  // 52
                        {2,1,3,1,3,1,0,0},  // 53
                        {3,1,1,1,2,3,0,0},  // 54
                        {3,1,1,3,2,1,0,0},  // 55
                        {3,3,1,1,2,1,0,0},  // 56
                        {3,1,2,1,1,3,0,0},  // 57
                        {3,1,2,3,1,1,0,0},  // 58
                        {3,3,2,1,1,1,0,0},  // 59
                        {3,1,4,1,1,1,0,0},  // 60
                        {2,2,1,4,1,1,0,0},  // 61
                        {4,3,1,1,1,1,0,0},  // 62
                        {1,1,1,2,2,4,0,0},  // 63
                        {1,1,1,4,2,2,0,0},  // 64
                        {1,2,1,1,2,4,0,0},  // 65
                        {1,2,1,4,2,1,0,0},  // 66
                        {1,4,1,1,2,2,0,0},  // 67
                        {1,4,1,2,2,1,0,0},  // 68
                        {1,1,2,2,1,4,0,0},  // 69
                        {1,1,2,4,1,2,0,0},  // 70
                        {1,2,2,1,1,4,0,0},  // 71
                        {1,2,2,4,1,1,0,0},  // 72
                        {1,4,2,1,1,2,0,0},  // 73
                        {1,4,2,2,1,1,0,0},  // 74
                        {2,4,1,2,1,1,0,0},  // 75
                        {2,2,1,1,1,4,0,0},  // 76
                        {4,1,3,1,1,1,0,0},  // 77
                        {2,4,1,1,1,2,0,0},  // 78
                        {1,3,4,1,1,1,0,0},  // 79
                        {1,1,1,2,4,2,0,0},  // 80
                        {1,2,1,1,4,2,0,0},  // 81
                        {1,2,1,2,4,1,0,0},  // 82
                        {1,1,4,2,1,2,0,0},  // 83
                        {1,2,4,1,1,2,0,0},  // 84
                        {1,2,4,2,1,1,0,0},  // 85
                        {4,1,1,2,1,2,0,0},  // 86
                        {4,2,1,1,1,2,0,0},  // 87
                        {4,2,1,2,1,1,0,0},  // 88
                        {2,1,2,1,4,1,0,0},  // 89
                        {2,1,4,1,2,1,0,0},  // 90
                        {4,1,2,1,2,1,0,0},  // 91
                        {1,1,1,1,4,3,0,0},  // 92
                        {1,1,1,3,4,1,0,0},  // 93
                        {1,3,1,1,4,1,0,0},  // 94
                        {1,1,4,1,1,3,0,0},  // 95
                        {1,1,4,3,1,1,0,0},  // 96
                        {4,1,1,1,1,3,0,0},  // 97
                        {4,1,1,3,1,1,0,0},  // 98
                        {1,1,3,1,4,1,0,0},  // 99
                        {1,1,4,1,3,1,0,0},  // 100
                        {3,1,1,1,4,1,0,0},  // 101
                        {4,1,1,1,3,1,0,0},  // 102
                        {2,1,1,4,1,2,0,0},  // 103
                        {2,1,1,2,1,4,0,0},  // 104
                        {2,1,1,2,3,2,0,0},  // 105
                        {2,3,3,1,1,1,2,0}   // 106
                     };

                #endregion Code patterns

                private const int cQuietWidth = 10;

                /// <summary>
                /// Make an image of a Code128 barcode for a given string
                /// </summary>
                /// <param name="InputData">Message to be encoded</param>
                /// <param name="BarWeight">Base thickness for bar width (1 or 2 works well)</param>
                /// <param name="AddQuietZone">Add required horiz margins (use if output is tight)</param>
                /// <returns>An Image of the Code128 barcode representing the message</returns>
                public static Image MakeBarcodeImage(string InputData, int BarWeight, bool AddQuietZone) {
                    // get the Code128 codes to represent the message
                    Code128Content content = new Code128Content(InputData);
                    int[] codes = content.Codes;

                    int width, height;
                    width = ((codes.Length - 3) * 11 + 35) * BarWeight;
                    height = Convert.ToInt32(System.Math.Ceiling(Convert.ToSingle(width) * .15F));

                    if (AddQuietZone) {
                        width += 2 * cQuietWidth * BarWeight;  // on both sides
                    }

                    // get surface to draw on
                    Image myimg = new System.Drawing.Bitmap(width, height);
                    using (Graphics gr = Graphics.FromImage(myimg)) {

                        // set to white so we don't have to fill the spaces with white
                        gr.FillRectangle(System.Drawing.Brushes.White, 0, 0, width, height);

                        // skip quiet zone
                        int cursor = AddQuietZone ? cQuietWidth * BarWeight : 0;

                        for (int codeidx = 0; codeidx < codes.Length; codeidx++) {
                            int code = codes[codeidx];

                            // take the bars two at a time: a black and a white
                            for (int bar = 0; bar < 8; bar += 2) {
                                int barwidth = cPatterns[code, bar] * BarWeight;
                                int spcwidth = cPatterns[code, bar + 1] * BarWeight;

                                // if width is zero, don't try to draw it
                                if (barwidth > 0) {
                                    gr.FillRectangle(System.Drawing.Brushes.Black, cursor, 0, barwidth, height);
                                }

                                // note that we never need to draw the space, since we 
                                // initialized the graphics to all white

                                // advance cursor beyond this pair
                                cursor += (barwidth + spcwidth);
                            }
                        }
                    }

                    return myimg;

                }

            }
            

        public static System.Drawing.Image getImageObject(string sUri) {
            try {
                if (sUri.ToLower().IndexOf("http:") > -1 || sUri.ToLower().IndexOf("https:") > -1 || sUri.ToLower().IndexOf("ftp:") > -1) {

                    WebRequest oRequest;
                    oRequest = HttpWebRequest.Create(sUri);
                    WebResponse oResponse = oRequest.GetResponse();

                    return System.Drawing.Image.FromStream(oResponse.GetResponseStream());

                } else {

                    if (sUri.IndexOf(":\\") == -1) {
                        try {
                            sUri = System.Web.HttpContext.Current.Server.MapPath(sUri);
                        } catch (Exception) { }
                    }

                    if (sUri.Substring(sUri.Length - 4).ToLower() == ".ico") {

                        MemoryStream oStream = new MemoryStream();
                        Icon oIcon = new Icon(sUri);
                        oIcon.Save(oStream);

                        return System.Drawing.Image.FromStream(oStream);

                    } else {

                        return System.Drawing.Image.FromFile(sUri);
                    }
                }
            } catch (Exception) {

                System.Drawing.Image oImg = new System.Drawing.Bitmap(1, 1);
                Graphics gImage = Graphics.FromImage(oImg);
                gImage.FillRectangle(Brushes.White, new Rectangle(0, 0, 1, 1));
                gImage.Dispose();

                return oImg;
            }
        }

        /// <summary>
        /// Resizes an image to fit within a boudning box
        /// </summary>
        /// <param name="sPath">Path of local image</param>
        /// <param name="iWidth">Width of boundign box</param>
        /// <param name="iHeight">Height of Bounding Box</param>
        /// <returns>Resized Image</returns>
        public static Bitmap resizeImage(string sPath, int iWidth, int iHeight) {
            try {
                //Open File Stream and Image
                System.IO.FileStream oStream = new System.IO.FileStream(sPath, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite);
                System.Drawing.Image oImage = System.Drawing.Image.FromStream(oStream);
                System.Drawing.Bitmap oBitmap;

                //Close File Stream
                oStream.Close();

                float fFactor1 = (float)((float)iWidth / (float)oImage.Width);
                float fFactor2 = (float)((float)iHeight / (float)oImage.Height);
                float fFactor = 1f;

                int iNewWidth = oImage.Width;
                int iNewHeight = oImage.Height;

                fFactor = fFactor1;
                if (fFactor1 < fFactor2)
                    fFactor = fFactor1;

                if (fFactor2 < fFactor1)
                    fFactor = fFactor2;

                iNewWidth = jlib.functions.convert.cInt((float)oImage.Width * fFactor);
                iNewHeight = jlib.functions.convert.cInt((float)oImage.Height * fFactor);

                oBitmap = new Bitmap(oImage, iNewWidth, iNewHeight);

                return oBitmap;
            } catch { }

            return null;
        }


        public static bool isWebSafeImage(string sPath) {
            switch (Path.GetExtension(sPath).ToLower()) {
                case ".gif":
                case ".jpg":
                case ".png":
                    return true;                    

                default:
                    return false;                    
            }

        }

        public static ImageCodecInfo getEncoderInfo(String sMimeType) {

            ImageCodecInfo[] oEncoders = ImageCodecInfo.GetImageEncoders();
            for (int x = 0; x < oEncoders.Length; x++) {
                if (oEncoders[x].MimeType == sMimeType)
                    return oEncoders[x];
            }
            return null;
        }

        #endregion METHODS

        #region iconhelper
        public class iconhelper {
            private bool debug = false;
            private MemoryStream icoStream;
            private iconHeader icoHeader;
            private ArrayList icoEntrys;
            public enum displayType { Largest = 0, Smallest = 1 }

            //--------------------------------------------------------------------------
            //   Class: iconHeader
            // Purpose: Stores the headers of the ICO
            //
            private class iconHeader {
                public Int16 Reserved;
                public Int16 Type;
                public Int16 Count;

                public iconHeader(MemoryStream icoStream) {
                    BinaryReader icoFile = new BinaryReader(icoStream);

                    Reserved = icoFile.ReadInt16();
                    Type = icoFile.ReadInt16();
                    Count = icoFile.ReadInt16();
                }
            }

            //--------------------------------------------------------------------------
            //   Class: iconEntry
            // Purpose: Each icon if the file has its own header.  This is where I read
            //          the info for each icon.
            //
            public class iconEntry {
                public byte Width;
                public byte Height;
                public byte ColorCount;
                public byte Reserved;
                public Int16 Planes;
                public Int16 BitCount;
                public Int32 BytesInRes;
                public Int32 ImageOffset;

                public iconEntry(MemoryStream icoStream) {
                    BinaryReader icoFile = new BinaryReader(icoStream);

                    Width = icoFile.ReadByte();
                    Height = icoFile.ReadByte();
                    ColorCount = icoFile.ReadByte();
                    Reserved = icoFile.ReadByte();
                    Planes = icoFile.ReadInt16();
                    BitCount = icoFile.ReadInt16();
                    BytesInRes = icoFile.ReadInt32();
                    ImageOffset = icoFile.ReadInt32();
                }
            }

            //--------------------------------------------------------------------------
            // Main class
            //--------------------------------------------------------------------------

            //--------------------------------------------------------------------------
            // Function: loadFile
            //  Purpose: Loads the icon file into the memory stream
            //
            private bool loadFile(string filename) {
                try {
                    FileStream icoFile = new FileStream(filename, FileMode.Open, FileAccess.Read);
                    byte[] icoArray = new byte[icoFile.Length];
                    icoFile.Read(icoArray, 0, (int)icoFile.Length);
                    icoStream = new MemoryStream(icoArray);
                } catch (Exception) { return false; } finally { }

                return true;
            }

            private bool loadFile(Stream oStream) {
                try {

                    //StreamReader o = new StreamReader( oStream );								
                    //MemoryStream oMemoryStream	= new MemoryStream( );				
                    byte[] icoArray = new byte[oStream.Length];
                    oStream.Read(icoArray, 0, (int)oStream.Length);
                    icoStream = new MemoryStream(icoArray);
                } catch { return false; } finally { }

                return true;
            }

            //--------------------------------------------------------------------------
            // Function: buildIcon
            //  Purpose: Loads the icon file into the memory stream
            //
            public Icon buildIcon(int index) {
                iconEntry thisIcon = (iconEntry)icoEntrys[index];
                MemoryStream newIcon = new MemoryStream();
                BinaryWriter writer = new BinaryWriter(newIcon);

                // New Values
                Int16 newNumber = 1;
                Int32 newOffset = 22;

                // Write it
                writer.Write(icoHeader.Reserved);
                writer.Write(icoHeader.Type);
                writer.Write(newNumber);
                writer.Write(thisIcon.Width);
                writer.Write(thisIcon.Height);
                writer.Write(thisIcon.ColorCount);
                writer.Write(thisIcon.Reserved);
                writer.Write(thisIcon.Planes);
                writer.Write(thisIcon.BitCount);
                writer.Write(thisIcon.BytesInRes);
                writer.Write(newOffset);

                // Grab the icon
                byte[] tmpBuffer = new byte[thisIcon.BytesInRes];
                icoStream.Position = thisIcon.ImageOffset;
                icoStream.Read(tmpBuffer, 0, thisIcon.BytesInRes);
                writer.Write(tmpBuffer);

                // Finish up
                writer.Flush();
                newIcon.Position = 0;
                return new Icon(newIcon, thisIcon.Width, thisIcon.Height);
            }

            private Icon searchIcons(displayType searchKey) {
                int foundIndex = 0;
                int counter = 0;

                foreach (iconEntry thisIcon in icoEntrys) {
                    iconEntry current = (iconEntry)icoEntrys[foundIndex];

                    if (searchKey == displayType.Largest) {
                        if (thisIcon.Width > current.Width && thisIcon.Height > current.Height) {
                            foundIndex = counter;
                        }
                        if (debug) Console.Write("Search for the largest");
                    } else {
                        if (thisIcon.Width < current.Width && thisIcon.Height < current.Height) {
                            foundIndex = counter;
                        }
                        if (debug) Console.Write("Search for the smallest");
                    }

                    counter++;
                }

                return buildIcon(foundIndex);
            }

            public ArrayList IconsInfo { get { return icoEntrys; } }
            public Icon FindIcon(displayType searchKey) { return searchIcons(searchKey); }
            public Icon FindIcon(int iWidth, int iHeight) {

                int iFoundIndex = -1;
                int iDelta = -1;


                for (int x = 0; x < icoEntrys.Count; x++) {

                    iconEntry oCurrent = (iconEntry)icoEntrys[x];

                    if (iFoundIndex == -1) {

                        iFoundIndex = x;
                        iDelta = Math.Abs(oCurrent.Width - iWidth) + Math.Abs(oCurrent.Height - iHeight);

                    } else if (iDelta > Math.Abs(oCurrent.Width - iWidth) + Math.Abs(oCurrent.Height - iHeight)) {

                        iFoundIndex = x;
                        iDelta = Math.Abs(oCurrent.Width - iWidth) + Math.Abs(oCurrent.Height - iHeight);
                    }
                }

                return buildIcon(iFoundIndex);

            }
            public int Count { get { return icoEntrys.Count; } }

            //--------------------------------------------------------------------------
            // Function: Constructor
            //  Purpose: Loads the icon file into the memory stream
            //
            public iconhelper(string sFileName) {
                icoEntrys = new ArrayList();

                if (loadFile(sFileName))
                    parseIcon();

            }

            public iconhelper(Stream oStream) {
                icoEntrys = new ArrayList();

                if (loadFile(oStream))
                    parseIcon();

            }

            private void parseIcon() {

                icoHeader = new iconHeader(icoStream);
                if (debug) { Console.WriteLine("There are {0} images in this icon file", icoHeader.Count); }

                // Read the icons
                for (int counter = 0; counter < icoHeader.Count; counter++) {
                    iconEntry entry = new iconEntry(icoStream);
                    icoEntrys.Add(entry);
                    if (debug) { Console.WriteLine("This entry has a width of {0} and a height of {1}", entry.Width, entry.Height); }
                }

            }
        }

        #endregion iconhelper

        #region OctreeQuantizer.cs
        public unsafe class OctreeQuantizer : Quantizer {
            /// <summary>
            /// Construct the octree quantizer
            /// </summary>
            /// <remarks>
            /// The Octree quantizer is a two pass algorithm. The initial pass sets up the octree,
            /// the second pass quantizes a color based on the nodes in the tree
            /// </remarks>
            /// <param name="maxColors">The maximum number of colors to return</param>
            /// <param name="maxColorBits">The number of significant bits</param>
            public OctreeQuantizer(int maxColors, int maxColorBits)
                : base(false) {
                if (maxColors > 255)
                    throw new ArgumentOutOfRangeException("maxColors", maxColors, "The number of colors should be less than 256");

                if ((maxColorBits < 1) | (maxColorBits > 8))
                    throw new ArgumentOutOfRangeException("maxColorBits", maxColorBits, "This should be between 1 and 8");

                // Construct the octree
                _octree = new Octree(maxColorBits);

                _maxColors = maxColors;
            }

            /// <summary>
            /// Process the pixel in the first pass of the algorithm
            /// </summary>
            /// <param name="pixel">The pixel to quantize</param>
            /// <remarks>
            /// This function need only be overridden if your quantize algorithm needs two passes,
            /// such as an Octree quantizer.
            /// </remarks>
            
            protected override void InitialQuantizePixel(Color32* pixel) {
                // Add the color to the octree
                _octree.AddColor(pixel);
            }

            /// <summary>
            /// Override this to process the pixel in the second pass of the algorithm
            /// </summary>
            /// <param name="pixel">The pixel to quantize</param>
            /// <returns>The quantized value</returns>
            
            protected override byte QuantizePixel(Color32* pixel) {
                byte paletteIndex = (byte)_maxColors;	// The color at [_maxColors] is set to transparent

                // Get the palette index if this non-transparent
                if (pixel->Alpha > 0)
                    paletteIndex = (byte)_octree.GetPaletteIndex(pixel);

                return paletteIndex;
            }

            /// <summary>
            /// Retrieve the palette for the quantized image
            /// </summary>
            /// <param name="original">Any old palette, this is overrwritten</param>
            /// <returns>The new color palette</returns>
            protected override ColorPalette GetPalette(ColorPalette original) {
                // First off convert the octree to _maxColors colors
                ArrayList palette = _octree.Palletize(_maxColors - 1);

                // Then convert the palette based on those colors
                for (int index = 0; index < palette.Count; index++)
                    original.Entries[index] = (Color)palette[index];

                // Add the transparent color
                original.Entries[_maxColors] = Color.FromArgb(0, 0, 0, 0);

                return original;
            }

            /// <summary>
            /// Stores the tree
            /// </summary>
            private Octree _octree;

            /// <summary>
            /// Maximum allowed color depth
            /// </summary>
            private int _maxColors;

            /// <summary>
            /// Class which does the actual quantization
            /// </summary>
            private class Octree {
                /// <summary>
                /// Construct the octree
                /// </summary>
                /// <param name="maxColorBits">The maximum number of significant bits in the image</param>
                public Octree(int maxColorBits) {
                    _maxColorBits = maxColorBits;
                    _leafCount = 0;
                    _reducibleNodes = new OctreeNode[9];
                    _root = new OctreeNode(0, _maxColorBits, this);
                    _previousColor = 0;
                    _previousNode = null;
                }

                /// <summary>
                /// Add a given color value to the octree
                /// </summary>
                /// <param name="pixel"></param>
                public void AddColor(Color32* pixel) {
                    // Check if this request is for the same color as the last
                    if (_previousColor == pixel->ARGB) {
                        // If so, check if I have a previous node setup. This will only ocurr if the first color in the image
                        // happens to be black, with an alpha component of zero.
                        if (null == _previousNode) {
                            _previousColor = pixel->ARGB;
                            _root.AddColor(pixel, _maxColorBits, 0, this);
                        } else
                            // Just update the previous node
                            _previousNode.Increment(pixel);
                    } else {
                        _previousColor = pixel->ARGB;
                        _root.AddColor(pixel, _maxColorBits, 0, this);
                    }
                }

                /// <summary>
                /// Reduce the depth of the tree
                /// </summary>
                public void Reduce() {
                    int index;

                    // Find the deepest level containing at least one reducible node
                    for (index = _maxColorBits - 1; (index > 0) && (null == _reducibleNodes[index]); index--) ;

                    // Reduce the node most recently added to the list at level 'index'
                    OctreeNode node = _reducibleNodes[index];
                    _reducibleNodes[index] = node.NextReducible;

                    // Decrement the leaf count after reducing the node
                    _leafCount -= node.Reduce();

                    // And just in case I've reduced the last color to be added, and the next color to
                    // be added is the same, invalidate the previousNode...
                    _previousNode = null;
                }

                /// <summary>
                /// Get/Set the number of leaves in the tree
                /// </summary>
                public int Leaves {
                    get { return _leafCount; }
                    set { _leafCount = value; }
                }

                /// <summary>
                /// Return the array of reducible nodes
                /// </summary>
                protected OctreeNode[] ReducibleNodes {
                    get { return _reducibleNodes; }
                }

                /// <summary>
                /// Keep track of the previous node that was quantized
                /// </summary>
                /// <param name="node">The node last quantized</param>
                protected void TrackPrevious(OctreeNode node) {
                    _previousNode = node;
                }

                /// <summary>
                /// Convert the nodes in the octree to a palette with a maximum of colorCount colors
                /// </summary>
                /// <param name="colorCount">The maximum number of colors</param>
                /// <returns>An arraylist with the palettized colors</returns>
                public ArrayList Palletize(int colorCount) {
                    while (Leaves > colorCount)
                        Reduce();

                    // Now palettize the nodes
                    ArrayList palette = new ArrayList(Leaves);
                    int paletteIndex = 0;
                    _root.ConstructPalette(palette, ref paletteIndex);

                    // And return the palette
                    return palette;
                }

                /// <summary>
                /// Get the palette index for the passed color
                /// </summary>
                /// <param name="pixel"></param>
                /// <returns></returns>
                public int GetPaletteIndex(Color32* pixel) {
                    return _root.GetPaletteIndex(pixel, 0);
                }

                /// <summary>
                /// Mask used when getting the appropriate pixels for a given node
                /// </summary>
                private static int[] mask = new int[8] { 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01 };

                /// <summary>
                /// The root of the octree
                /// </summary>
                private OctreeNode _root;

                /// <summary>
                /// Number of leaves in the tree
                /// </summary>
                private int _leafCount;

                /// <summary>
                /// Array of reducible nodes
                /// </summary>
                private OctreeNode[] _reducibleNodes;

                /// <summary>
                /// Maximum number of significant bits in the image
                /// </summary>
                private int _maxColorBits;

                /// <summary>
                /// Store the last node quantized
                /// </summary>
                private OctreeNode _previousNode;

                /// <summary>
                /// Cache the previous color quantized
                /// </summary>
                private int _previousColor;

                /// <summary>
                /// Class which encapsulates each node in the tree
                /// </summary>
                protected class OctreeNode {
                    /// <summary>
                    /// Construct the node
                    /// </summary>
                    /// <param name="level">The level in the tree = 0 - 7</param>
                    /// <param name="colorBits">The number of significant color bits in the image</param>
                    /// <param name="octree">The tree to which this node belongs</param>
                    public OctreeNode(int level, int colorBits, Octree octree) {
                        // Construct the new node
                        _leaf = (level == colorBits);

                        _red = _green = _blue = 0;
                        _pixelCount = 0;

                        // If a leaf, increment the leaf count
                        if (_leaf) {
                            octree.Leaves++;
                            _nextReducible = null;
                            _children = null;
                        } else {
                            // Otherwise add this to the reducible nodes
                            _nextReducible = octree.ReducibleNodes[level];
                            octree.ReducibleNodes[level] = this;
                            _children = new OctreeNode[8];
                        }
                    }

                    /// <summary>
                    /// Add a color into the tree
                    /// </summary>
                    /// <param name="pixel">The color</param>
                    /// <param name="colorBits">The number of significant color bits</param>
                    /// <param name="level">The level in the tree</param>
                    /// <param name="octree">The tree to which this node belongs</param>
                    public void AddColor(Color32* pixel, int colorBits, int level, Octree octree) {
                        // Update the color information if this is a leaf
                        if (_leaf) {
                            Increment(pixel);
                            // Setup the previous node
                            octree.TrackPrevious(this);
                        } else {
                            // Go to the next level down in the tree
                            int shift = 7 - level;
                            int index = ((pixel->Red & mask[level]) >> (shift - 2)) |
                                        ((pixel->Green & mask[level]) >> (shift - 1)) |
                                        ((pixel->Blue & mask[level]) >> (shift));

                            OctreeNode child = _children[index];

                            if (null == child) {
                                // Create a new child node & store in the array
                                child = new OctreeNode(level + 1, colorBits, octree);
                                _children[index] = child;
                            }

                            // Add the color to the child node
                            child.AddColor(pixel, colorBits, level + 1, octree);
                        }

                    }

                    /// <summary>
                    /// Get/Set the next reducible node
                    /// </summary>
                    public OctreeNode NextReducible {
                        get { return _nextReducible; }
                        set { _nextReducible = value; }
                    }

                    /// <summary>
                    /// Return the child nodes
                    /// </summary>
                    public OctreeNode[] Children {
                        get { return _children; }
                    }

                    /// <summary>
                    /// Reduce this node by removing all of its children
                    /// </summary>
                    /// <returns>The number of leaves removed</returns>
                    public int Reduce() {
                        _red = _green = _blue = 0;
                        int children = 0;

                        // Loop through all children and add their information to this node
                        for (int index = 0; index < 8; index++) {
                            if (null != _children[index]) {
                                _red += _children[index]._red;
                                _green += _children[index]._green;
                                _blue += _children[index]._blue;
                                _pixelCount += _children[index]._pixelCount;
                                ++children;
                                _children[index] = null;
                            }
                        }

                        // Now change this to a leaf node
                        _leaf = true;

                        // Return the number of nodes to decrement the leaf count by
                        return (children - 1);
                    }

                    /// <summary>
                    /// Traverse the tree, building up the color palette
                    /// </summary>
                    /// <param name="palette">The palette</param>
                    /// <param name="paletteIndex">The current palette index</param>
                    public void ConstructPalette(ArrayList palette, ref int paletteIndex) {
                        if (_leaf) {
                            // Consume the next palette index
                            _paletteIndex = paletteIndex++;

                            // And set the color of the palette entry
                            palette.Add(Color.FromArgb(_red / _pixelCount, _green / _pixelCount, _blue / _pixelCount));
                        } else {
                            // Loop through children looking for leaves
                            for (int index = 0; index < 8; index++) {
                                if (null != _children[index])
                                    _children[index].ConstructPalette(palette, ref paletteIndex);
                            }
                        }
                    }

                    /// <summary>
                    /// Return the palette index for the passed color
                    /// </summary>
                    public int GetPaletteIndex(Color32* pixel, int level) {
                        int paletteIndex = _paletteIndex;

                        if (!_leaf) {
                            int shift = 7 - level;
                            int index = ((pixel->Red & mask[level]) >> (shift - 2)) |
                                        ((pixel->Green & mask[level]) >> (shift - 1)) |
                                        ((pixel->Blue & mask[level]) >> (shift));

                            if (null != _children[index])
                                paletteIndex = _children[index].GetPaletteIndex(pixel, level + 1);
                            else
                                throw new Exception("Didn't expect this!");
                        }

                        return paletteIndex;
                    }

                    /// <summary>
                    /// Increment the pixel count and add to the color information
                    /// </summary>
                    public void Increment(Color32* pixel) {
                        _pixelCount++;
                        _red += pixel->Red;
                        _green += pixel->Green;
                        _blue += pixel->Blue;
                    }

                    /// <summary>
                    /// Flag indicating that this is a leaf node
                    /// </summary>
                    private bool _leaf;

                    /// <summary>
                    /// Number of pixels in this node
                    /// </summary>
                    private int _pixelCount;

                    /// <summary>
                    /// Red component
                    /// </summary>
                    private int _red;

                    /// <summary>
                    /// Green Component
                    /// </summary>
                    private int _green;

                    /// <summary>
                    /// Blue component
                    /// </summary>
                    private int _blue;

                    /// <summary>
                    /// Pointers to any child nodes
                    /// </summary>
                    private OctreeNode[] _children;

                    /// <summary>
                    /// Pointer to next reducible node
                    /// </summary>
                    private OctreeNode _nextReducible;

                    /// <summary>
                    /// The index of this node in the palette
                    /// </summary>
                    private int _paletteIndex;

                }
            }

        }
        #endregion OctreeQuantizer.cs

        #region PaletteQuantizer.cs
        public unsafe class PaletteQuantizer : Quantizer {
            /// <summary>
            /// Construct the palette quantizer
            /// </summary>
            /// <param name="palette">The color palette to quantize to</param>
            /// <remarks>
            /// Palette quantization only requires a single quantization step
            /// </remarks>
            public PaletteQuantizer(ArrayList palette)
                : base(true) {
                _colorMap = new Hashtable();

                _colors = new Color[palette.Count];
                palette.CopyTo(_colors);
            }

            /// <summary>
            /// Override this to process the pixel in the second pass of the algorithm
            /// </summary>
            /// <param name="pixel">The pixel to quantize</param>
            /// <returns>The quantized value</returns>
            
            protected override byte QuantizePixel(Color32* pixel) {
                byte colorIndex = 0;
                int colorHash = pixel->ARGB;

                // Check if the color is in the lookup table
                if (_colorMap.ContainsKey(colorHash))
                    colorIndex = (byte)_colorMap[colorHash];
                else {
                    // Not found - loop through the palette and find the nearest match.
                    // Firstly check the alpha value - if 0, lookup the transparent color
                    if (0 == pixel->Alpha) {
                        // Transparent. Lookup the first color with an alpha value of 0
                        for (int index = 0; index < _colors.Length; index++) {
                            if (0 == _colors[index].A) {
                                colorIndex = (byte)index;
                                break;
                            }
                        }
                    } else {
                        // Not transparent...
                        int leastDistance = int.MaxValue;
                        int red = pixel->Red;
                        int green = pixel->Green;
                        int blue = pixel->Blue;

                        // Loop through the entire palette, looking for the closest color match
                        for (int index = 0; index < _colors.Length; index++) {
                            Color paletteColor = _colors[index];

                            int redDistance = paletteColor.R - red;
                            int greenDistance = paletteColor.G - green;
                            int blueDistance = paletteColor.B - blue;

                            int distance = (redDistance * redDistance) +
                                               (greenDistance * greenDistance) +
                                               (blueDistance * blueDistance);

                            if (distance < leastDistance) {
                                colorIndex = (byte)index;
                                leastDistance = distance;

                                // And if it's an exact match, exit the loop
                                if (0 == distance)
                                    break;
                            }
                        }
                    }

                    // Now I have the color, pop it into the hashtable for next time
                    _colorMap.Add(colorHash, colorIndex);
                }

                return colorIndex;
            }

            /// <summary>
            /// Retrieve the palette for the quantized image
            /// </summary>
            /// <param name="palette">Any old palette, this is overrwritten</param>
            /// <returns>The new color palette</returns>
            protected override ColorPalette GetPalette(ColorPalette palette) {
                for (int index = 0; index < _colors.Length; index++)
                    palette.Entries[index] = _colors[index];

                return palette;
            }

            /// <summary>
            /// Lookup table for colors
            /// </summary>
            private Hashtable _colorMap;

            /// <summary>
            /// List of all colors in the palette
            /// </summary>
            private Color[] _colors;
        }
        #endregion PaletteQuantizer.cs

        #region Quantizer.cs
        public unsafe abstract class Quantizer {
            /// <summary>
            /// Construct the quantizer
            /// </summary>
            /// <param name="singlePass">If true, the quantization only needs to loop through the source pixels once</param>
            /// <remarks>
            /// If you construct this class with a true value for singlePass, then the code will, when quantizing your image,
            /// only call the 'QuantizeImage' function. If two passes are required, the code will call 'InitialQuantizeImage'
            /// and then 'QuantizeImage'.
            /// </remarks>
            public Quantizer(bool singlePass) {
                _singlePass = singlePass;
            }

            /// <summary>
            /// Quantize an image and return the resulting output bitmap
            /// </summary>
            /// <param name="source">The image to quantize</param>
            /// <returns>A quantized version of the image</returns>
            public Bitmap Quantize(Image source) {
                // Get the size of the source image
                int height = source.Height;
                int width = source.Width;

                // And construct a rectangle from these dimensions
                Rectangle bounds = new Rectangle(0, 0, width, height);

                // First off take a 32bpp copy of the image
                Bitmap copy = new Bitmap(width, height, PixelFormat.Format32bppArgb);

                // And construct an 8bpp version
                Bitmap output = new Bitmap(width, height, PixelFormat.Format8bppIndexed);

                // Now lock the bitmap into memory
                using (Graphics g = Graphics.FromImage(copy)) {
                    g.PageUnit = GraphicsUnit.Pixel;

                    // Draw the source image onto the copy bitmap,
                    // which will effect a widening as appropriate.
                    g.DrawImageUnscaled(source, bounds);
                }

                // Define a pointer to the bitmap data
                BitmapData sourceData = null;

                try {
                    // Get the source image bits and lock into memory
                    sourceData = copy.LockBits(bounds, ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

                    // Call the FirstPass function if not a single pass algorithm.
                    // For something like an octree quantizer, this will run through
                    // all image pixels, build a data structure, and create a palette.
                    if (!_singlePass)
                        FirstPass(sourceData, width, height);

                    // Then set the color palette on the output bitmap. I'm passing in the current palette 
                    // as there's no way to construct a new, empty palette.
                    output.Palette = this.GetPalette(output.Palette);

                    // Then call the second pass which actually does the conversion
                    SecondPass(sourceData, output, width, height, bounds);
                } finally {
                    // Ensure that the bits are unlocked
                    copy.UnlockBits(sourceData);
                }

                // Last but not least, return the output bitmap
                return output;
            }

            /// <summary>
            /// Execute the first pass through the pixels in the image
            /// </summary>
            /// <param name="sourceData">The source data</param>
            /// <param name="width">The width in pixels of the image</param>
            /// <param name="height">The height in pixels of the image</param>
            protected virtual void FirstPass(BitmapData sourceData, int width, int height) {
                // Define the source data pointers. The source row is a byte to
                // keep addition of the stride value easier (as this is in bytes)
                byte* pSourceRow = (byte*)sourceData.Scan0.ToPointer();
                Int32* pSourcePixel;

                // Loop through each row
                for (int row = 0; row < height; row++) {
                    // Set the source pixel to the first pixel in this row
                    pSourcePixel = (Int32*)pSourceRow;

                    // And loop through each column
                    for (int col = 0; col < width; col++, pSourcePixel++)
                        // Now I have the pixel, call the FirstPassQuantize function...
                        InitialQuantizePixel((Color32*)pSourcePixel);

                    // Add the stride to the source row
                    pSourceRow += sourceData.Stride;
                }
            }

            /// <summary>
            /// Execute a second pass through the bitmap
            /// </summary>
            /// <param name="sourceData">The source bitmap, locked into memory</param>
            /// <param name="output">The output bitmap</param>
            /// <param name="width">The width in pixels of the image</param>
            /// <param name="height">The height in pixels of the image</param>
            /// <param name="bounds">The bounding rectangle</param>
            protected virtual void SecondPass(BitmapData sourceData, Bitmap output, int width, int height, Rectangle bounds) {
                BitmapData outputData = null;

                try {
                    // Lock the output bitmap into memory
                    outputData = output.LockBits(bounds, ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

                    // Define the source data pointers. The source row is a byte to
                    // keep addition of the stride value easier (as this is in bytes)
                    byte* pSourceRow = (byte*)sourceData.Scan0.ToPointer();
                    Int32* pSourcePixel = (Int32*)pSourceRow;
                    Int32* pPreviousPixel = pSourcePixel;

                    // Now define the destination data pointers
                    byte* pDestinationRow = (byte*)outputData.Scan0.ToPointer();
                    byte* pDestinationPixel = pDestinationRow;

                    // And convert the first pixel, so that I have values going into the loop
                    byte pixelValue = QuantizePixel((Color32*)pSourcePixel);

                    // Assign the value of the first pixel
                    *pDestinationPixel = pixelValue;

                    // Loop through each row
                    for (int row = 0; row < height; row++) {
                        // Set the source pixel to the first pixel in this row
                        pSourcePixel = (Int32*)pSourceRow;

                        // And set the destination pixel pointer to the first pixel in the row
                        pDestinationPixel = pDestinationRow;

                        // Loop through each pixel on this scan line
                        for (int col = 0; col < width; col++, pSourcePixel++, pDestinationPixel++) {
                            // Check if this is the same as the last pixel. If so use that value
                            // rather than calculating it again. This is an inexpensive optimisation.
                            if (*pPreviousPixel != *pSourcePixel) {
                                // Quantize the pixel
                                pixelValue = QuantizePixel((Color32*)pSourcePixel);

                                // And setup the previous pointer
                                pPreviousPixel = pSourcePixel;
                            }

                            // And set the pixel in the output
                            *pDestinationPixel = pixelValue;
                        }

                        // Add the stride to the source row
                        pSourceRow += sourceData.Stride;

                        // And to the destination row
                        pDestinationRow += outputData.Stride;
                    }
                } finally {
                    // Ensure that I unlock the output bits
                    output.UnlockBits(outputData);
                }
            }

            /// <summary>
            /// Override this to process the pixel in the first pass of the algorithm
            /// </summary>
            /// <param name="pixel">The pixel to quantize</param>
            /// <remarks>
            /// This function need only be overridden if your quantize algorithm needs two passes,
            /// such as an Octree quantizer.
            /// </remarks>
            
            protected virtual void InitialQuantizePixel(Color32* pixel) {
            }

            /// <summary>
            /// Override this to process the pixel in the second pass of the algorithm
            /// </summary>
            /// <param name="pixel">The pixel to quantize</param>
            /// <returns>The quantized value</returns>
            
            protected virtual byte QuantizePixel(Color32* pixel) { return 0; }

            /// <summary>
            /// Retrieve the palette for the quantized image
            /// </summary>
            /// <param name="original">Any old palette, this is overrwritten</param>
            /// <returns>The new color palette</returns>
            protected abstract ColorPalette GetPalette(ColorPalette original);

            /// <summary>
            /// Flag used to indicate whether a single pass or two passes are needed for quantization.
            /// </summary>
            private bool _singlePass;

            /// <summary>
            /// Struct that defines a 32 bpp colour
            /// </summary>
            /// <remarks>
            /// This struct is used to read data from a 32 bits per pixel image
            /// in memory, and is ordered in this manner as this is the way that
            /// the data is layed out in memory
            /// </remarks>
            [StructLayout(LayoutKind.Explicit)]
            public struct Color32 {
                /// <summary>
                /// Holds the blue component of the colour
                /// </summary>
                [FieldOffset(0)]
                public byte Blue;
                /// <summary>
                /// Holds the green component of the colour
                /// </summary>
                [FieldOffset(1)]
                public byte Green;
                /// <summary>
                /// Holds the red component of the colour
                /// </summary>
                [FieldOffset(2)]
                public byte Red;
                /// <summary>
                /// Holds the alpha component of the colour
                /// </summary>
                [FieldOffset(3)]
                public byte Alpha;

                /// <summary>
                /// Permits the color32 to be treated as an int32
                /// </summary>
                [FieldOffset(0)]
                public int ARGB;

                /// <summary>
                /// Return the color for this Color32 object
                /// </summary>
                public Color Color {
                    get { return Color.FromArgb(Alpha, Red, Green, Blue); }
                }
            }
        }
        #endregion Quantizer.cs
    }

}