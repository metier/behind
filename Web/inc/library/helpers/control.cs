using System;
using System.Data;
using System.Web.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using jlib.components;
using jlib.functions;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Drawing;
using System.Threading;
using System.Text.RegularExpressions;
using System.Xml;
using System.Reflection;
using System.Web;
using System.IO;
using jlib.db;

namespace jlib.helpers {
    public class control {
        public static int setControlValues(string sFormData, Control oParentControl) {            
            string[] sData = parse.split(sFormData, "&");
            int iCounter = 0;
            for (int x = 0; x < sData.Length; x++) {                
                string sID = parse.splitValue(sData[x], "=", 0);
                string sValue = parse.splitValue(sData[x], "=", 1);
                if (sID != "") {
                    iControl oControl = jlib.helpers.control.findControlByUniqueId(oParentControl.Page, oParentControl.Controls, sID) as iControl;
                    if (oControl != null) {
                        oControl.setValue(sValue);
                        iCounter++;
                    }
                }
            }
            return iCounter;
        }

        public static int setControlValues(string sFormData, List<iControl> oControls) {
            int iCounter = 0;
            for (int x = 0; x < oControls.Count; x++) {
                if (oControls[x].UniqueID != "") {
                    try {
                        oControls[x].setValue(System.Web.HttpUtility.UrlDecode(parse.inner_substring(parse.splitValue("&&" + sFormData + "&", "&" + System.Web.HttpUtility.UrlEncode(oControls[x].UniqueID) + "=", 1), null, null, "&", null)));
                        iCounter++;
                    } catch (Exception) { }
                }
            }
            return iCounter;
        }

        public static List<Control> filterVisibleControls(List<Control> oArr)
        {
            List<Control> oArr1 = new List<Control>();
            for (int x = 0; x < oArr.Count; x++) {
                Control oCtrl = oArr[x];
                while (oCtrl != null) {
                    if (!oCtrl.Visible) break;
                    oCtrl = oCtrl.Parent;
                }
                if (oCtrl == null) oArr1.Add(oArr[x]);
            }
            return oArr1;
        }

        public static List<iControl> filterDynamicControls(System.Web.UI.Control oParentControl, string sTableName, string sFieldNames)
        {
            List<iControl> oList = getDynamicControls(oParentControl, null);
            List<iControl> oList1 = new List<iControl>();
            for (int x = 0; x < oList.Count; x++){
                if (convert.cStr(sTableName) == "" || oList[x].DataMember.StartsWith(sTableName + ".")){
                    if (convert.cStr(sFieldNames) == "" || (oList[x].DataMember.IndexOf(".") > -1 && ("," + sFieldNames + ",").IndexOf((sFieldNames.IndexOf(".") > -1 ? oList[x].DataMember : parse.split(oList[x].DataMember, ".")[1]) + ",") > -1))
                        oList1.Add(oList[x]);
                }
            }
            return oList1;
        }


        public static List<jlib.components.iControl> getDynamicControls(System.Web.UI.Control oControl, List<jlib.components.iControl> oList) {
            return getDynamicControls(oControl, oList, "");
		}

        public static List<jlib.components.iControl> getDynamicControls(System.Web.UI.Control oControl, List<jlib.components.iControl> oList, string sDataMemberMatch) {
			
            sDataMemberMatch = convert.cStr(sDataMemberMatch);

            if (oList == null) {
                oList = new List<iControl>();

                if ( oControl as jlib.components.iControl != null )
                    if (sDataMemberMatch == "" || (oControl as jlib.components.iControl).DataMember.IndexOf(sDataMemberMatch) > -1) oList.Add(oControl as iControl);
            }

            for ( int x = 0; x < oControl.Controls.Count; x++ ) {

                if ( oControl.Controls[x] as jlib.components.iControl != null )
                    if (sDataMemberMatch == "" || (oControl.Controls[x] as jlib.components.iControl).DataMember.IndexOf(sDataMemberMatch) > -1) oList.Add(oControl.Controls[x] as iControl);                   

                if ( oControl.Controls[x].Controls != null && oControl.Controls[x].Controls.Count > 0 )
                    getDynamicControls(oControl.Controls[x], oList, sDataMemberMatch);
            }

            return oList;
        }

        public static int populateValues(System.Web.UI.Control oParentControl, string sTableName, DataRow oRow) {
            return populateValues(oParentControl, sTableName, oRow as IDataRow);
        }

        public static int populateValues(System.Web.UI.Control oParentControl, string sTableName, IDataRow oRow) {
			return populateValues(oParentControl, sTableName, oRow, false);
		}
        public static int populateValues(System.Web.UI.Control oParentControl, string sPrefix, helpers.structures.collection oData) {
            return populateValues(oParentControl, sPrefix, oData, false);
        }
        public static int populateValues(System.Web.UI.Control oParentControl, string sPrefix, helpers.structures.collection oData, bool bDisabledOnly) {
            return populateValues(oParentControl, sPrefix, null, oData, bDisabledOnly);
        }
			
        public static int populateValues( System.Web.UI.Control oParentControl, string sTableName, IDataRow oRow, bool bDisabledOnly ) {
            return populateValues(oParentControl, sTableName, oRow, null, bDisabledOnly);
        }

        public static int populateValues(System.Web.UI.Control oParentControl, string sTableName, IDataRow oRow, helpers.structures.collection oData, bool bDisabledOnly) {            
            int iFieldCounter = 0;
            List<iControl> oControls = helpers.control.getDynamicControls( oParentControl, null );
            for ( int x = 0; x < oControls.Count; x++ ) {
                if ((!oControls[x].Enabled || !bDisabledOnly || (oControls[x] as jlib.components.label != null || oControls[x] as jlib.components.image != null)) && (sTableName == "" || oControls[x].DataMember.IndexOf(sTableName + ".") <= 1)) {
                    if (oControls[x].DataMember.StartsWith("!")) {
                        List<Control> oControlMatch = jlib.helpers.control.findControlById(oParentControl.Page, oParentControl.Page.Controls, parse.split(oControls[x].DataMember.Substring(1), ".")[0]);
						if (oControlMatch.Count > 0) {
                            oControls[x].setValue(jlib.functions.reflection.getPropertyValue(oControlMatch[0], parse.split(oControls[x].DataMember, ".")[1]));
							iFieldCounter++;
						}
                    } else if (oControls[x].DataMember.ToLower().StartsWith(sTableName.ToLower()+".")) {
						try {
                            string columnName = oControls[x].DataMember.Remove(0, sTableName.Length + 1);
                            if (oRow == null && oData==null)
                                oControls[x].setValue(null);
							else
                                if (oRow != null) {                                    
                                    if (oRow.GetColumnByName(columnName) == null && oRow as jlib.DataFramework.IRecord != null) {
                                        oControls[x].setValue(jlib.functions.reflection.getPropertyValue(oRow,columnName));
                                    } else {
                                        try {
                                            oControls[x].setValue(oRow[columnName]);
                                        } catch (Exception ex) {
                                            Type oType = null;
                                            if (oRow as DataRow != null) oType = (oRow as DataRow).Table.Columns[columnName].DataType;
                                            else oType = (oRow as jlib.DataFramework.IRecord).Fields[columnName].GetType();
                                            if (oType.FullName == "System.Boolean") {
                                                oControls[x].setValue((convert.cBool(oRow[columnName]) ? "1" : "0"));
                                            } else {
                                                throw ex;
                                            }
                                        }
                                    }
                                } else {
                                    try {
                                        oControls[x].setValue(oData[columnName]);
                                    } catch (Exception ex) {
                                        if (oData[columnName].GetType().FullName == "System.Boolean") {
                                            oControls[x].setValue((convert.cBool(oData[columnName]) ? "1" : "0"));
                                        } else {
                                            throw ex;
                                        }
                                    }
                                }
							iFieldCounter++;
						} catch (Exception) { }
					}
                }
            }

            return iFieldCounter;
        }

		public static bool populateValue(Control parentControl, string sDataMember, object oValue) {
            return populateValue(getDynamicControls(parentControl, null), sDataMember, oValue);
		}
        
        public static bool populateValue(List<iControl> oControls, string sDataMember, object oValue){
            bool bSuccess = false;
            for (int x = 0; x < oControls.Count; x++){
                if (oControls[x].DataMember == sDataMember){
                    oControls[x].setValue(oValue);
                    bSuccess = true;
                }
            }
            return bSuccess;
        }

        public static bool populateValue(List<iControl> oControls, object oValue) {
            bool bSuccess = false;
            for (int x = 0; x < oControls.Count; x++) {
                oControls[x].setValue(oValue);
                bSuccess = true;
            }
            return bSuccess;
        }

        public static bool populateProperty(Page oPage, string sDataMember, string sProperty, object oValue) {
            return populateProperty(getDynamicControls(oPage, null), sDataMember,sProperty, oValue);
        }
        public static bool populateProperty(List<iControl> oControls, string sDataMember, string sProperty, object oValue) {
            bool bSuccess = false;
            for (int x = 0; x < oControls.Count; x++) {
                if (oControls[x].DataMember == sDataMember) {
                    try {
                        bSuccess = jlib.functions.reflection.setValueOfProperty(oControls[x], sProperty, oValue);                        
                    } catch (Exception) { }
                }
            }
            return bSuccess;
        }
        public static DataRow collectValues(System.Web.UI.Control oParentControl, string sTableName, DataRow oRow) {
            return collectValues(oParentControl, sTableName, oRow as IDataRow) as DataRow;
        }
        public static string collectValue(System.Web.UI.Control oParentControl, string sDataMember) {
            return collectValue(helpers.control.getDynamicControls(oParentControl, null), sDataMember);
        }
        public static string collectValue(List<iControl> oControls, string sDataMember) {
            for (int x = 0; x < oControls.Count; x++) {
                if (oControls[x].DataMember == sDataMember) {
                    return oControls[x].getValue().Str();                    
                }
            }
            return null;
        }

        public static IDataRow collectValues( System.Web.UI.Control oParentControl, string sTableName, IDataRow oRow ) {
            if ( oRow == null ) return oRow;

            int iFieldCounter = 0;
            List<iControl> oControls = helpers.control.getDynamicControls(oParentControl, null);
            for ( int x = 0; x < oControls.Count; x++ ) {
                if (!oControls[x].DisableSave && oControls[x].Enabled && (oControls[x] as jlib.components.textbox == null || !(oControls[x] as jlib.components.textbox).Locked)) {
                    if (oControls[x].DataMember.IndexOf(sTableName + ".") == 0) {
						try {
                            string columName=oControls[x].DataMember.Remove(0, sTableName.Length + 1);
                            string sValue = convert.cStr(oControls[x].getValue()).Trim();
                            string fieldValue = "";
                            if (oRow.GetColumnByName(columName) == null && oRow as jlib.DataFramework.IRecord != null) {
                                fieldValue = jlib.functions.reflection.getPropertyValue(oRow, columName).Str();
                            } else {
                                fieldValue = oRow[oControls[x].DataMember.Remove(0, sTableName.Length + 1)].Str();
                            }
                            if (!fieldValue.Equals(sValue) && oControls[x] as jlib.components.image == null) {

                                Type type = null;
                                if (oRow as DataRow != null) type = (oRow as DataRow).Table.Columns[oControls[x].DataMember.Remove(0, sTableName.Length + 1)].DataType;
                                else type = jlib.functions.reflection.getPropertyInfo(oRow, columName).PropertyType;
                                    
                                object value = null;
                                
								//String
								if (type.FullName == "System.String") 
								{
                                    value = sValue;
                                }
								
								//Boolean
								else if (type == typeof(bool) || type == typeof(bool?)) 
								{
                                    value = convert.cBool(oControls[x].getValue());
                                } 
								
								//DateTime
								else if ((type == typeof(DateTime) || type == typeof(DateTime?)) && (String.IsNullOrWhiteSpace(jlib.functions.convert.cStr(oControls[x].getValue()))) ) 
								{
                                    value = System.DBNull.Value;
                                }
								else if ((type == typeof(DateTime) || type == typeof(DateTime?))) 
								{

                                    if (oControls[x] as textbox != null) {
										if ((oControls[x] as textbox).ValidateAs == textbox.FieldType.Time.ToString())
											value = jlib.functions.convert.buildDate(convert.cDate(oRow[oControls[x].DataMember.Remove(0, sTableName.Length + 1)]), jlib.functions.convert.cDate(oControls[x].getValue()));
										else if ((oControls[x] as textbox).ValidateAs == textbox.FieldType.Date.ToString())
											value = jlib.functions.convert.buildDate(jlib.functions.convert.cDate(oControls[x].getValue()), convert.cDate(oRow[oControls[x].DataMember.Remove(0, sTableName.Length + 1)]));
										else if ((oControls[x] as textbox).ValidateAs == textbox.FieldType.NoValidation.ToString())
											value = oControls[x].getValue();
										else
											value = jlib.functions.convert.cDate(oControls[x].getValue());
									}else
                                        value = jlib.functions.convert.cDate(oControls[x].getValue());

                                } 
								
								//short
								else if (type == typeof(short) || type == typeof(short?)) 
								{
                                    value = jlib.functions.convert.cInt(oControls[x].getValue());									
                                }

								//int
								else if (type == typeof(int) || type == typeof(int?)) 
								{
									value = jlib.functions.convert.cInt(oControls[x].getValue());
								}

								//long
								else if (type == typeof(long) || type == typeof(long?)) 
								{
									value = jlib.functions.convert.cLong(oControls[x].getValue());
								}

								//decimal
								else if (type == typeof(decimal) || type == typeof(decimal?))
								{
                                    value = jlib.functions.convert.cDec(oControls[x].getValue());									
								} 
								
								//other = string
								else 
								{
                                    value = oControls[x].getValue();
								}
                                
                                if(oRow.GetColumnByName(columName)==null && oRow as jlib.DataFramework.IRecord!=null)
								{
                                    jlib.functions.reflection.setValueOfProperty(oRow, columName, value);
                                }
								else
								{
                                    oRow[columName] = value;
									if(oRow.RecordState != DataRowAction.Add) oRow.RecordState = DataRowAction.Change;
                                }
								iFieldCounter++;
							}
						} catch (Exception) { }
					}
				}
            }

            //return iFieldCounter;
            return oRow;
        }
        
        public static string validateForm( System.Web.UI.Control oParentControl  ) {

            string sErrorText = "";
            List<iControl> oControls = helpers.control.getDynamicControls(oParentControl, null);
            for ( int x = 0; x < oControls.Count; x++ ) {
                iControl oControl = oControls[x] as iControl;
                string sValidate = oControl.validate();
                if ( !oControl.DisableSave && sValidate != "" ) {
                    if ( sErrorText == "" ) {
                        oParentControl.Page.ClientScript.RegisterStartupScript( oParentControl.Page.GetType(), "focus_control", String.Format( "<script>try{{ document.getElementById( '{0}' ).focus(); }}catch(e){{}}</script>", ( oControl as System.Web.UI.Control ).ID ));
                    }
                    sErrorText += "<li>" + sValidate + "</li>";
                }
            }

            if (sErrorText == "") return "";

            return "<b>"+(System.Configuration.ConfigurationManager.AppSettings["common.language"] == "en" ? "Form validation errors:" : "P�krevd informasjon mangler:" )+ "</b><br/><ul>" + sErrorText + "</ul>";                
        }

		public static bool isClicked( System.Web.UI.Control oControl) {

			if (oControl.Page.IsPostBack) {
				if (convert.cStr(oControl.Page.Request.Form[oControl.UniqueID + ".x"]) != "" || convert.cStr(oControl.Page.Request.Form[oControl.UniqueID]) != "")
					return true;
			}
			return false;
			
		}

        public static void setSaveable(Control oControl, bool bEnableSave)
        {
            List<iControl> oControls = jlib.helpers.control.getDynamicControls(oControl, null);
            for (int x = 0; x < oControls.Count; x++) oControls[x].DisableSave = !bEnableSave;            
        }
        public static void setRequired(Control oControl, bool bRequired) {
            List<iControl> oControls = jlib.helpers.control.getDynamicControls(oControl, null);
            for (int x = 0; x < oControls.Count; x++) oControls[x].Required = bRequired;
        }
        
        #region METHODS

        
        public static string renderControl(string sSource, bool bSourceIsEditableMode) {


            string sUrl = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/intellicms/frames/content/nugget_server.aspx";
            string sPostData = "source=" + System.Web.HttpUtility.UrlEncode(sSource);
            sUrl += "?__EVENTARGUMENT=get_content_preview&source_is_editable_mode=" + (bSourceIsEditableMode ? "true" : "false");

            jlib.net.HTTP oHTTP = new jlib.net.HTTP();


            string sData = oHTTP.Post(sUrl, sPostData);
            return sData;

        }
        public static string renderControl(System.Web.UI.Control oBodyControl) {

            StringWriter oWriter = new StringWriter();
            System.Web.UI.HtmlTextWriter oHTML = new HtmlTextWriter(oWriter);
            oBodyControl.RenderControl(oHTML);
            return oWriter.ToString();

        }


        public static string prepareHTML(string sHTML) {
            //sHTML					= parse.replaceAll( sHTML, "sIFR.replaceElement" , "//sIFR.replaceElement" );
            sHTML = parse.replaceAll(sHTML, "<%=Request.FilePath%>", "");
            //sHTML					= parse.replaceAll( sHTML, " editable=" , " contenteditable=" );

            return sHTML;
        }

        public static List<Control> findControlType(Page oPage, System.Web.UI.ControlCollection oControls, System.Type oControlType, bool bSubclassMode) {

            List<Control> oList = new List<Control>();

            for (int x = 0; x < oControls.Count; x++) {

                Control oCtrl = oControls[x];

                if (bSubclassMode && oControlType.IsInstanceOfType(oCtrl))
                    oList.Add(oCtrl);
                else if (bSubclassMode && oCtrl.GetType().IsSubclassOf(oControlType))
                    oList.Add(oCtrl);
                else if (oCtrl.GetType().Equals(oControlType))
                    oList.Add(oCtrl);

                if (oCtrl.GetType().FullName.IndexOf("ASP.") > -1 && oCtrl.GetType().FullName.IndexOf("_ascx") > -1) {
                    if (oCtrl.Controls.Count == 0) {
                        int iIndex = oCtrl.Parent.Controls.IndexOf(oCtrl);

                        string sControlSrc = jlib.functions.io.read_file(HttpContext.Current.Server.MapPath(oCtrl.TemplateSourceDirectory + "/" + parse.inner_substring(oCtrl.GetType().FullName, ".", null, null, null).Replace("_ascx", ".ascx")));
                        sControlSrc = prepareHTML(sControlSrc);

                        Control oTempControl = oPage.ParseControl(sControlSrc);
                        oCtrl.Parent.Controls.AddAt(iIndex, oTempControl);
                        oCtrl.Parent.Controls.Remove(oCtrl);
                        oCtrl = oTempControl;
                    }
                }

                if (oCtrl.Controls.Count > 0)
                    oList.AddRange(findControlType(oPage, oControls[x].Controls, oControlType, bSubclassMode));
            }
            return oList;
        }

        public static ArrayList findControls(Page oPage, System.Web.UI.ControlCollection oControls, System.Type oType) {
            return findControls(oPage, oControls, oType, false);
        }

        public static ArrayList findLiteralControl(Page oPage, System.Web.UI.ControlCollection oControls, string sText, bool bCaseSensitive) {
            ArrayList oList = findControls(oPage, oControls, typeof(System.Web.UI.LiteralControl));

            for (int x = 0; x < oList.Count; x++) {
                System.Web.UI.LiteralControl oLit = (System.Web.UI.LiteralControl)oList[x];

                if ((!bCaseSensitive && oLit.Text.ToLower().IndexOf(sText.ToLower()) == -1) || (bCaseSensitive && oLit.Text.IndexOf(sText) == -1)) {
                    oList.RemoveAt(x);
                    x--;
                }
            }

            return oList;
        }

        public static ArrayList findControls(Page oPage, System.Web.UI.ControlCollection oControls, System.Type oType, bool bInheritanceMode) {

            ArrayList oArr = new ArrayList();

            for (int x = 0; x < oControls.Count; x++) {

                Control oCtrl = oControls[x];

                if (oType.IsInstanceOfType(oCtrl) || (bInheritanceMode && oCtrl.GetType().IsSubclassOf(oType)))
                    oArr.Add(oCtrl);

                if (oCtrl.GetType().FullName.IndexOf("ASP.") > -1 && oCtrl.GetType().FullName.IndexOf("_ascx") > -1) {
                    if (oCtrl.Controls.Count == 0) {
                        int iIndex = oCtrl.Parent.Controls.IndexOf(oCtrl);

                        string sControlSrc = jlib.functions.io.read_file(HttpContext.Current.Server.MapPath(oCtrl.TemplateSourceDirectory + "/" + parse.inner_substring(oCtrl.GetType().FullName, ".", null, null, null).Replace("_ascx", ".ascx")));
                        sControlSrc = prepareHTML(sControlSrc);


                        Control oTempControl = oPage.ParseControl(sControlSrc);
                        oCtrl.Parent.Controls.AddAt(iIndex, oTempControl);
                        oCtrl.Parent.Controls.Remove(oCtrl);
                        oCtrl = oTempControl;
                    }
                }

                if (oCtrl.Controls.Count > 0)
                    oArr.AddRange(findControls(oPage, oControls[x].Controls, oType, bInheritanceMode));
            }
            return oArr;
        }
        public static List<iControl> findControlsByDataMember(List<iControl> oControls, string sDataMember) {
            List<iControl> o = new List<iControl>();
            for (int x = 0; x < oControls.Count; x++)
                if (oControls[x].DataMember == sDataMember) o.Add(oControls[x]);
            return o;
        }
        public static Control findControlByUniqueId(Page oPage, System.Web.UI.ControlCollection oControls, string sUniqueID) {

            ArrayList oArr = new ArrayList();

            for (int x = 0; x < oControls.Count; x++) {

                Control oCtrl = oControls[x];

                if (oCtrl.UniqueID == sUniqueID)
                    return oCtrl;

                if (oCtrl.GetType().FullName.IndexOf("ASP.") > -1 && oCtrl.GetType().FullName.IndexOf("_ascx") > -1) {
                    if (oCtrl.Controls.Count == 0) {
                        int iIndex = oCtrl.Parent.Controls.IndexOf(oCtrl);

                        string sControlSrc = jlib.functions.io.read_file(HttpContext.Current.Server.MapPath(oCtrl.TemplateSourceDirectory + "/" + parse.inner_substring(oCtrl.GetType().FullName, ".", null, null, null).Replace("_ascx", ".ascx")));
                        sControlSrc = prepareHTML(sControlSrc);

                        Control oTempControl = oPage.ParseControl(sControlSrc);
                        oCtrl.Parent.Controls.AddAt(iIndex, oTempControl);
                        oCtrl.Parent.Controls.Remove(oCtrl);
                        oCtrl = oTempControl;
                    }
                }

                if (oCtrl.Controls.Count > 0) {
                    Control oFoundControl = findControlByUniqueId(oPage, oControls[x].Controls, sUniqueID);
                    if (oFoundControl != null) return oFoundControl;
                }
            }
            return null;
        }

        public static List<iControl> findControlById(List<iControl> oControls, string sID) {
            List<iControl> o = new List<iControl>();
            for (int x = 0; x < oControls.Count; x++)
                if (oControls[x].ID == sID) o.Add(oControls[x]);
            return o;
        }

        public static List<Control> findControlById(Page oPage, System.Web.UI.ControlCollection oControls, string sID) {
            List<Control> oList = null;

            if (sID.IndexOf("$") > -1) {
                oList = new List<Control>();
                oList.Add(findControlByUniqueId(oPage, oControls, sID));
                return oList;
            }
            //This method can accept a UniqueID: parentControl$childControl OR a Common ID: parentControl.childControl
            //string[] sArr = parse.split(sID, (sID.IndexOf(".") > -1 ? "." : "$"));

            string[] sArr = parse.split(sID, ".");

            for (int x = 0; x < sArr.Length && (oList == null || oList.Count > 0); x++) {
                if (oList == null)
                    oList = _findControlById(oPage, oControls, sArr[x], true);
                else
                    oList = _findControlById(oPage, oList[0].Controls, sArr[x], false);

            }
            return oList;
        }

        private static List<Control> _findControlById(Page oPage, System.Web.UI.ControlCollection oControls, string sID, bool bDescendControlTree) {

            List<Control> oList = new List<Control>();

            for (int x = 0; x < oControls.Count; x++) {

                Control oCtrl = oControls[x];

                if (oCtrl.ID == sID)
                    oList.Add(oCtrl);

                if (oCtrl.GetType().FullName.IndexOf("ASP.") > -1 && oCtrl.GetType().FullName.IndexOf("_ascx") > -1) {
                    if (oCtrl.Controls.Count == 0) {
                        int iIndex = oCtrl.Parent.Controls.IndexOf(oCtrl);

                        string sControlSrc = jlib.functions.io.read_file(HttpContext.Current.Server.MapPath(oCtrl.TemplateSourceDirectory + "/" + parse.inner_substring(oCtrl.GetType().FullName, ".", null, null, null).Replace("_ascx", ".ascx")));
                        sControlSrc = prepareHTML(sControlSrc);


                        Control oTempControl = oPage.ParseControl(sControlSrc);
                        oCtrl.Parent.Controls.AddAt(iIndex, oTempControl);
                        oCtrl.Parent.Controls.Remove(oCtrl);
                        oCtrl = oTempControl;
                    }
                }

                if (oCtrl.Controls.Count > 0 && bDescendControlTree)
                    oList.AddRange(findControlById(oPage, oControls[x].Controls, sID));
            }
            return oList;
        }

        public static ArrayList findControls(Page oPage, System.Web.UI.ControlCollection oControls, string sAssemblyName) {
            return findControls(oPage, oControls, sAssemblyName, false);
        }

        public static ArrayList findControls(Page oPage, System.Web.UI.ControlCollection oControls, string sAssemblyName, bool bBaseClassMode) {
            return findControls(oPage, oControls, sAssemblyName, bBaseClassMode, false);
        }
        public static ArrayList findControls(Page oPage, System.Web.UI.ControlCollection oControls, string sAssemblyName, bool bBaseClassMode, bool bPartialMatchMode) {

            ArrayList oArr = new ArrayList();

            for (int x = 0; x < oControls.Count; x++) {

                Control oCtrl = oControls[x];

                if (!bBaseClassMode && oCtrl.GetType().FullName == sAssemblyName || bBaseClassMode && oCtrl.GetType().BaseType != null && oCtrl.GetType().BaseType.FullName == sAssemblyName)
                    oArr.Add(oCtrl);
                else if (bPartialMatchMode)
                    if (!bBaseClassMode && oCtrl.GetType().FullName.IndexOf(sAssemblyName) == 0 || bBaseClassMode && oCtrl.GetType().BaseType != null && oCtrl.GetType().BaseType.FullName.IndexOf(sAssemblyName) > -1)
                        oArr.Add(oCtrl);

                if (oCtrl.GetType().FullName.IndexOf("ASP.") > -1 && oCtrl.GetType().FullName.IndexOf("_ascx") > -1) {
                    if (oCtrl.Controls.Count == 0) {
                        int iIndex = oCtrl.Parent.Controls.IndexOf(oCtrl);

                        string sControlSrc = jlib.functions.io.read_file(HttpContext.Current.Server.MapPath(oCtrl.TemplateSourceDirectory + "/" + parse.inner_substring(oCtrl.GetType().FullName, ".", null, null, null).Replace("_ascx", ".ascx")));
                        sControlSrc = prepareHTML(sControlSrc);


                        Control oTempControl = oPage.ParseControl(sControlSrc);
                        oCtrl.Parent.Controls.AddAt(iIndex, oTempControl);
                        oCtrl.Parent.Controls.Remove(oCtrl);
                        oCtrl = oTempControl;
                    }
                }

                if (oCtrl.Controls.Count > 0)
                    oArr.AddRange(findControls(oPage, oControls[x].Controls, sAssemblyName, bBaseClassMode, bPartialMatchMode));
            }
            return oArr;
        }

        public static void moveControls(Control oDestination, ControlCollection oSource) {
            while (oSource.Count > 0)
                oDestination.Controls.Add(oSource[0]);
        }

        public static object cloneObject(object oObject) {
            return cloneObject(oObject, null);
        }
        public static object cloneObject(object oObject, string[] sExcludedProperties) {
            return cloneObject(oObject, sExcludedProperties, false);
        }
        public static object cloneObject(object oObject, string[] sExcludedProperties, bool bSkipICloneable) {
            Type tType = oObject.GetType();
            Object oNew;
            if (!bSkipICloneable && tType.GetInterface("System.ICloneable") != null)
                return (oObject as System.ICloneable).Clone();
            else if (tType.FullName == "System.Web.UI.ResourceBasedLiteralControl")
                oNew = new LiteralControl();// tType.InvokeMember("", System.Reflection.BindingFlags.CreateInstance, null, oObject, null);
            else
                oNew = tType.InvokeMember("", System.Reflection.BindingFlags.CreateInstance, null, oObject, null);
            PropertyInfo[] oProperties = oNew.GetType().GetProperties();
            foreach (PropertyInfo oProperty in oProperties) {
                try {
                    if (oProperty.CanRead && util.find_in_array(sExcludedProperties, oProperty.Name) == -1) {
                        object oValue = oProperty.GetValue(oObject, null);
                        //This line causes loops when the col object tries to clone its m_oGrid property
                        //if (oValue as System.ICloneable != null) oValue = (oValue as System.ICloneable).Clone();
                        if (oProperty.CanWrite)
                            oProperty.SetValue(oNew, oValue, null);
                        else {
                            PropertyInfo o = jlib.functions.reflection.getPropertyInfo(oValue, "Value");
                            if (o != null && o.CanRead && o.CanWrite) {
                                o.SetValue(oProperty.GetValue(oNew, null), o.GetValue(oValue, null), null);
                                continue;
                            }
                            //if(jlib.functions.reflection.containsProperty(oValue,"Value"))
                        }
                        //This was STUPID: It broke the grid when the grid is cloned and added to a page. I don't know why
                        //else if (oValue as IEnumerable != null) {
                        //    IEnumerable oEnumerable = oValue as IEnumerable;
                        //    IEnumerator oEnumerator = oEnumerable.GetEnumerator();
                        //    while(oEnumerator.MoveNext()){
                        //        jlib.functions.reflection.executeMethod(oProperty.GetValue(oNew, null), "Add", new object[] { cloneObject(oEnumerator.Current) });
                        //    }
                        //}                        
                    }
                } catch (Exception e) { Console.Write(e.Message); }
            }

            if (oObject as System.Web.UI.Control != null) {
                System.Web.UI.Control oControl = oObject as System.Web.UI.Control;
                System.Web.UI.Control oNewControl = oNew as System.Web.UI.Control;
                for (int x = 0; x < oControl.Controls.Count; x++)
                    oNewControl.Controls.Add(cloneObject(oControl.Controls[x], sExcludedProperties) as System.Web.UI.Control);

            }

            return oNew;
        }

        #endregion METHODS
    }
}
