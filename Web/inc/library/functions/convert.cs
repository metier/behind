using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Drawing;
using System.Threading;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using jlib.db;
using System.ComponentModel;
using System.Globalization;
using System.Net.Mail;

namespace jlib.functions
{
	/// <summary>
	/// Summary description for ado_helper.
	/// </summary>
	public static class convert
	{
        public enum RoundingOption {
            No_Rounding = 0,
            Round_Up_To_Nearest_Even = 1,
            Round_To_Nearest_Even = 2
        }

		#region METHODS
		public static string cHex(object oValue)
		{
            return String.Format("{0:x}", Convert.ToUInt32(cInt(oValue)));
		}

		public static int cHexToInt(string sValue)
		{
			return int.Parse(sValue, System.Globalization.NumberStyles.HexNumber, null);
		}

		/// <summary>
		/// Converts an object to an integer
		/// </summary>
		/// <param name="oValue">The object to convert to an integer</param>
		/// <returns>The integer value of the object</returns>
        public static int cInt(params object[] oValues){
            return (int)cDbl(oValues);			
		}
        public static int? cIntNullable(params object[] oValues) {
            return (int?)cDblNullable(true, oValues);
        }
        public static long? cLongNullable(params object[] oValues) {
            return (long?)cDblNullable(true, oValues);
        }
		public static long cLong(params object[] oValues)
		{
			return (long)cDbl(oValues);
		}
        public static short cShort(params object[] oValues) {
			return (short)cDbl(oValues);
        }
        public static decimal cDec(params object[] oValues) {
            return (decimal)cDbl(oValues);
        }
        public static Single cSng(params object[] oValues) {
            return (Single)cDbl(oValues);
        }
        public static float cFlt(params object[] oValues) {
            return (float)cDbl(oValues);
        }
        public static byte cByte(params object[] oValues) {
            return (byte)cDbl(oValues);
        }
		public static int cIntRounding(object oValue) {			
			double dVal = convert.cDbl(oValue);

			return convert.cInt(dVal) + (dVal - convert.cInt(dVal) >= 0.5 ? 1 : 0);
		}

			
		/// <summary>
		/// Converts an object to a byte[]
		/// </summary>
		/// <param name="oValue">The object to convert to a byte[]</param>
		/// <returns>The byte[] value of the object</returns>
		public static byte[] cByteArray(object oValue)
		{
			byte[] oBytes			= null;

			try
			{
				oBytes		= (byte[])oValue;
			}
			catch{}

			return oBytes;
		}
		
		/// <summary>
		/// Converts an object to a double
		/// </summary>
		/// <param name="oValue">The object to convert to a double</param>
		/// <returns>The double value of the object</returns>
        public static double cDbl(params object[] oValues) {
            return cDblNullable(false, oValues).Value;
        }
        /// <summary>
        /// Converts an object to a double
        /// </summary>
        /// <param name="nullable">Return null rather than 0 if no numbers are found in oValues</param>
        /// <param name="oValues">List of objects to convert to a double</param>
        /// <returns>The first non-zero double found in oValues</returns>
		public static double? cDblNullable(bool nullable, params object []oValues){
            double? dReturnValue = null;
            if (oValues != null && oValues.Length > 0) {

                for (int x = 0; x < oValues.Length && (dReturnValue == null || dReturnValue.Value == 0 ); x++) {
                    if (oValues[x] != null && oValues[x] != DBNull.Value) {
                        if (oValues[x].GetType() == typeof(System.Decimal))
                            dReturnValue = (double)(System.Decimal)oValues[x];
                        else if (oValues[x].GetType() == typeof(System.Double))
                            dReturnValue = (double)(System.Double)oValues[x];
                        else if (oValues[x].GetType() == typeof(System.Int16))
                            dReturnValue = (double)(System.Int16)oValues[x];
                        else if (oValues[x].GetType() == typeof(System.Int32))
                            dReturnValue = (double)(System.Int32)oValues[x];
                        else if (oValues[x].GetType() == typeof(System.Int64))
                            dReturnValue = (double)(System.Int64)oValues[x];
                        else if (oValues[x].GetType() == typeof(System.Boolean))
                            dReturnValue = (System.Boolean)oValues[x] ? 1 : 0;
                        else {
                            string sValue = System.Convert.ToString(oValues[x]);
                            if (oValues[x].GetType().IsArray) {
                                Array o = oValues[x] as Array;
                                for (int y = 0; y < o.Length; y++)
                                    if (convert.cDbl(o.GetValue(y)) != 0) return convert.cDbl(o.GetValue(y));
                            }
                            bool bPercent = sValue.IndexOf("%") > -1;
                            sValue = sValue.Replace("$", "").Replace("%", "").Replace(",", "").Replace(" ", "");
                            double d = 0;
                            if (double.TryParse(sValue, out d)) {
                                dReturnValue = d;
                                if (bPercent) dReturnValue = dReturnValue / 100;
                            }
                            if (dReturnValue == null && oValues[x].GetType().BaseType == typeof(System.Enum))
                                try {
                                    dReturnValue = Convert.ToInt32(oValues[x]);
                                } catch (Exception) { }
                        }
                    }
                }
            }
            if (dReturnValue == null && !nullable) return 0;
			return dReturnValue;
		}

		/// <summary>
		/// Checks if an object is numeric
		/// </summary>
		/// <param name="oValue">The object to check</param>
		/// <returns>Indicator whether this is a numeric value</returns>
		public static bool isNumeric ( object oValue ){
            return isNumeric(oValue, true);			
		}

        /// <summary>
        /// Checks if an object is numeric
        /// </summary>
        /// <param name="oValue">The object to check</param>
        /// <returns>Indicator whether this is a numeric value</returns>
        public static bool isNumeric(object oValue, bool bStripCurrencyAndPercent) {
            //try {
                string s = (bStripCurrencyAndPercent ? parse.replaceAll(oValue, "$", "", "%", "", " ", "", ",", "") : convert.cStr(oValue));
                return s!="" && parse.removeXChars(s, "-0123456789.") == s && s.LastIndexOf("-") < 1 && s.Split('.').Length < 3;
            //    double.Parse(s);
            //    return true;
            //} catch (Exception) {
            //    return false;
            //}
        }

        /// <summary>
        /// Checks if an object is HTML
        /// </summary>
        /// <param name="oValue">The object to check</param>
        /// <returns>Indicator whether this is HTML</returns>
        public static bool isHtml(object oValue) {
            return convert.cStr(oValue).IndexOf("<") > -1;
        }
        public static string cSetChar(string sStr, int iIndex, string sChar) {
            return cSetChar(sStr, iIndex, sChar, "");
        }
        public static string cSetChar(string sStr, int iIndex, string sChar, string sPadChar) {
            if (sPadChar == "" && iIndex > sStr.Length) return sStr;
            return convert.cPadString(sStr,iIndex,sPadChar,false) + sChar + (sStr.Length<iIndex + sChar.Length?"": sStr.Substring(iIndex + sChar.Length));
        }
		/// <summary>
		/// Converts an object to a string
		/// </summary>
		/// <param name="oValue">The object to convert to a string</param>
		/// <returns>Returns the string representation</returns>
		public static string cStr(params object []oValues)
		{
			if (oValues == null) return "";

			string sReturnValue		= "";
            try {
                if(oValues.Length>0&& oValues[0]!=null) sReturnValue = System.Convert.ToString(oValues[0]);
            } catch { }
            if (oValues.Length==1|| sReturnValue.Trim() != "") return sReturnValue;

            for (int x = 1; x < oValues.Length && sReturnValue.Trim() == ""; x++) {
                try {
                    if (oValues[x] != null) sReturnValue = System.Convert.ToString(oValues[x]);
                } catch { }
            }

			return sReturnValue;
		}

        public static T cValue<T>(params T[] oValues) {
            for (int x = 0; x < oValues.Length; x++) if (oValues[x] != null) return oValues[x];
            return default(T);
        }
        public static string cIfValue(object oValue, string sPrefix, string sSuffix, string sBlankValue) {
            return (String.IsNullOrWhiteSpace(convert.cStr(oValue)) ? sBlankValue : sPrefix + oValue + sSuffix);
        }

		public static Guid cGuid(object value)
		{
			Guid output = Guid.Empty;
			Guid.TryParse(value.Str(), out output);

			return output;
		}

		/// <summary>
		/// Converts an object to a capitalized string
		/// </summary>
		/// <param name="oValue">The object to convert to a capitalized string</param>
		/// <returns>Returns the string representation</returns>
		public static string cCapitalizedStr(object oValue) {
			
			string []sArr			= parse.split( cStr( oValue ) , " ");
			for ( int x = 0 ; x < sArr.Length; x++ ){
				if ( sArr[ x ].Length > 0 )
					sArr[ x ]		= sArr[ x ].Substring( 0 , 1 ).ToUpper() + sArr[ x ].Substring( 1 ).ToLower();
			}

			oValue = String.Join( " " , sArr );

            sArr = parse.split(cStr(oValue), "-");
            for (int x = 0; x < sArr.Length; x++) {
                if (sArr[x].Length > 0)
                    sArr[x] = sArr[x].Substring(0, 1).ToUpper() + sArr[x].Substring(1);
            }

            return String.Join("-", sArr);
		}

		/// <summary>
		/// Converts an object to an ellipsis string
		/// </summary>
		/// <param name="oValue">The object to convert to a string</param>
		/// <returns>Returns the string representation</returns>
		public static string cEllipsis(object oValue , int iMaxLen , bool bAddEllipsis ) {
			return cEllipsis(oValue, iMaxLen, bAddEllipsis, false);
		}

		public static string cEllipsis(object oValue, int iMaxLen, bool bAddEllipsis, bool bBreakOnSpace) {
			return cEllipsis(oValue, iMaxLen, bAddEllipsis , bBreakOnSpace, false);
		}
		public static string cEllipsis(object oValue, int iMaxLen, bool bAddEllipsis, bool bBreakOnSpace, bool bHtmlMode) {
			return cEllipsis(oValue, iMaxLen, (bAddEllipsis ? "..." : ""), bBreakOnSpace, bHtmlMode);
		}


		public static string cEllipsis(object oValue, int iMaxLen, string sEllipsis) {
			return cEllipsis(oValue, iMaxLen, sEllipsis, false, false);
		}

        /// <summary>
        /// Converts an object to an ellipsis string
        /// </summary>
        /// <param name="oValue">The object to convert to a string</param>
        /// <returns>Returns the string representation</returns>
		public static string cEllipsis(object oValue, int iMaxLen, string sEllipsis, bool bBreakOnSpace, bool bHtmlMode) {
			if (iMaxLen <= 0) return "";
			if( sEllipsis.Length >= iMaxLen ) return sEllipsis.Substring( 0, iMaxLen );
			string sReturnValue = "";

            sReturnValue = cStr(oValue);            

            if (sReturnValue.Length > iMaxLen) {
				if (bBreakOnSpace) {
					iMaxLen = sReturnValue.Substring(0, iMaxLen - sEllipsis.Length).LastIndexOf(" ") + sEllipsis.Length;
					if (iMaxLen < 0) return sEllipsis;
				}

				bool bIsCut = false;
				if (sEllipsis != "" && sReturnValue.Length > iMaxLen && sEllipsis.Length < iMaxLen) {
					sReturnValue = sReturnValue.Substring(0, iMaxLen - sEllipsis.Length);
					bIsCut = true;
				} else if (sEllipsis == "") {
					sReturnValue = sReturnValue.Substring(0, iMaxLen);
					bIsCut = true;
				}
				if (bIsCut && bHtmlMode) 
					if (sReturnValue.LastIndexOf(">") < sReturnValue.LastIndexOf("<")) sReturnValue = sReturnValue.Substring(0, sReturnValue.LastIndexOf("<"));
				
				if (bIsCut) sReturnValue += sEllipsis;

            }

            return sReturnValue;
        }

		/// <summary>
		/// Converts an object to an ellipsis string
		/// </summary>
		/// <param name="oValue">The object to convert to a string</param>
		/// <returns>Returns the string representation</returns>
		public static string cEllipsis(object oValue , int iMaxLen ) {
			
			return cEllipsis( oValue, iMaxLen, true, false );
		}


		/// <summary>
		/// Converts an object to a percent value
		/// </summary>
		/// <param name="oValue">The object to convert to a percent</param>
		/// <param name="iNumDecimals">The number of decimals to include</param>
		/// <returns>The string value with the percentage</returns>
		public static string cPercent( object oValue , int iNumDecimals ) {
            
            string sSeparator = ".";
            if (System.Configuration.ConfigurationManager.AppSettings["site.culture.id"] != null)
                sSeparator = (convert.cDbl(0.00000001).ToString(new System.Globalization.CultureInfo(System.Configuration.ConfigurationManager.AppSettings["site.culture.id"])).IndexOf(".") > -1 ? "." : ",");

            string sReturnValue = "0" + sSeparator + "000000000000000000000000000000";

			try {
				double dValue		= cDbl( oValue );
				dValue = dValue * 100;
				//if (convert.cStr(oValue).IndexOf("%") == -1) dValue = dValue * 100; else dValue = dValue / 100;

				sReturnValue = parse.replaceAll(dValue.ToString(), ".", sSeparator, ",", sSeparator);

                if ( sReturnValue.IndexOf( sSeparator ) == -1 )
                    sReturnValue += sSeparator;

				for ( int i = 0 ; i < iNumDecimals ; i++ )
					sReturnValue	+= "0";


                sReturnValue = sReturnValue.Substring( 0, sReturnValue.IndexOf( sSeparator ) + ( iNumDecimals == 0 ? 0 : iNumDecimals + 1 ) ) + "%";
			}
			catch{}

			return sReturnValue;
		}
        
        public static double cRound(object oValue, RoundingOption oOption) {
            if (oOption == RoundingOption.Round_Up_To_Nearest_Even) return Math.Round(convert.cDbl(oValue), 0) + (Math.Round(convert.cDbl(oValue), 0) < convert.cDbl(oValue) ? 1 : 0);
            else if (oOption == RoundingOption.Round_To_Nearest_Even) return Math.Round(convert.cDbl(oValue), 0);
            return convert.cDbl(oValue);
        }

		/// <summary>
		/// Converts an object to a number with n decimals
		/// </summary>
		/// <param name="oValue">The object to convert to a Decimal Number</param>
		/// <param name="iNumDecimals">The number of decimals to include</param>
		/// <returns>The string value with the Decimal Number</returns>
		public static string cDecimalNumber(object oValue, int iNumDecimals) {
			return cDecimalNumber(oValue, iNumDecimals,	"");
		}
		public static string cDecimalNumber(object oValue, int iNumDecimals, string sSeparator) {
			return cDecimalNumber(oValue, iNumDecimals, sSeparator,true);
		}
		public static string cDecimalNumber(object oValue, int iNumDecimals, string sSeparator, bool bKeepDecimalZeros) {
			return cDecimalNumber(oValue, iNumDecimals, sSeparator, bKeepDecimalZeros, false);
		}

		/// <summary>
		/// Converts an object to a number with n decimals
		/// </summary>
		/// <param name="oValue">The object to convert to a Decimal Number</param>
		/// <param name="iNumDecimals">The number of decimals to include</param>
		/// <param name="sSeparator">The decimal separator to use</param>
		/// <param name="bKeepDecimalZeros">Should we keep all decimals, even though they're zero?</param>
		/// <param name="bAllowStripSomeDecimals">If we allow zero decimals to be stripped, should we require all or none? This is useful for currency formatting.</param>
		/// <returns>The string value with the Decimal Number</returns>
		public static string cDecimalNumber(object oValue, int iNumDecimals, string sSeparator, bool bKeepDecimalZeros, bool bAllowStripSomeDecimals) {
			return cDecimalNumber(oValue, iNumDecimals, sSeparator, bKeepDecimalZeros, bAllowStripSomeDecimals, "");
		}
        public static string cDecimalNumber(object oValue, int iNumDecimals, string sSeparator, bool bKeepDecimalZeros, bool bAllowStripSomeDecimals, string sThousandSep) {
            return cDecimalNumber(oValue, iNumDecimals, sSeparator, bKeepDecimalZeros, bAllowStripSomeDecimals, sThousandSep, "");
        }
		public static string cDecimalNumber(object oValue, int iNumDecimals, string sSeparator, bool bKeepDecimalZeros, bool bAllowStripSomeDecimals, string sThousandSep, string currencyPrefix) {

			if (convert.cStr(sSeparator) == "") {
				sSeparator = ".";
                if (System.Configuration.ConfigurationManager.AppSettings["site.culture.id"] != null)
                    sSeparator = (convert.cDbl(0.00000001).ToString(new System.Globalization.CultureInfo(System.Configuration.ConfigurationManager.AppSettings["site.culture.id"])).IndexOf(".") > -1 ? "." : ",");
			}
			string sReturnValue = "0" + (bKeepDecimalZeros || !bAllowStripSomeDecimals ? sSeparator + "000000000000000000000000000000" : "");
			
			try 
			{
                double dValue = Math.Round(cDbl(oValue), iNumDecimals);
				sReturnValue = parse.replaceAll(dValue.ToString(), ".", sSeparator, ",", sSeparator);

                if ( sReturnValue.IndexOf( sSeparator ) == -1 )
                    sReturnValue += sSeparator;

				if (bKeepDecimalZeros || (!bAllowStripSomeDecimals && !sReturnValue.EndsWith(sSeparator))) {
					for (int i = 0; i < iNumDecimals; i++)
						sReturnValue += "0";
				}

				if (sReturnValue.EndsWith(sSeparator)) sReturnValue = sReturnValue.Substring(0, sReturnValue.Length - 1);

				if (sReturnValue.IndexOf(sSeparator) > -1)
					sReturnValue = sReturnValue.Substring( 0, sReturnValue.IndexOf( sSeparator ) + iNumDecimals + 1 );

				if (sThousandSep != "") {
					for (int x = (sReturnValue.IndexOf(sSeparator) > -1 ? sReturnValue.IndexOf(sSeparator) - 3 : sReturnValue.Length); x > 0; x = x - 3) {
						if (x > 0) sReturnValue = sReturnValue.Substring(0, x) + sThousandSep + sReturnValue.Substring(x);
					}
				}
			}
			catch{}

            if (currencyPrefix.IsNotNullOrEmpty()) 
                sReturnValue = (sReturnValue.StartsWith("-")?"-" : "") + currencyPrefix + parse.stripLeadingCharacter(sReturnValue,"-");
            
			return sReturnValue;
		}

        public static string cPadString(object oValue, int iTotalLength, string sPaddingCharacter, bool bLeftPadding) {
            return cPadString(oValue, iTotalLength, sPaddingCharacter, bLeftPadding, true);
        }
        /// <summary>
        /// Pads a string with specified character
        /// </summary>
        /// <param name="oValue">The object to pad</param>
        /// <param name="iTotalLength ">The total lenght of the padded string</param>
        /// <param name="sPaddingCharacter ">The padding character</param>
        /// <param name="bLeftPadding ">Left or right padding</param>
        /// <param name="bNegativeNumberDetection">Look for negative numbers that are being left-padded, and make negative sign left-most character in padded string</param>
        /// <returns>The padded string</returns>
		public static string cPadString( object oValue , int iTotalLength , string sPaddingCharacter, bool bLeftPadding, bool bNegativeNumberDetection ) {
			string sReturnValue		= cStr( oValue ).Trim();
            if (sPaddingCharacter == "") return sReturnValue;
            bool bNegativeNumberHandling = bNegativeNumberDetection && bLeftPadding && sReturnValue.StartsWith("-") && convert.isNumeric(sPaddingCharacter) && convert.isNumeric(sReturnValue) && iTotalLength>sReturnValue.Length;
            if (bNegativeNumberHandling) {
                iTotalLength--;
                sReturnValue = sReturnValue.Substring(1);
            }
			try {				                
				for ( int i = sReturnValue.Length ; i < iTotalLength ; i++ ){
					if ( bLeftPadding )
						sReturnValue	= sPaddingCharacter + sReturnValue;
					else
						sReturnValue	= sReturnValue + sPaddingCharacter ;
				}

				if ( bLeftPadding )
					sReturnValue		= sReturnValue.Substring(sReturnValue.Length - iTotalLength );
				else
					sReturnValue		= sReturnValue.Substring(0 , iTotalLength );
			}
			catch{}
            
			return (bNegativeNumberHandling?"-":"") + sReturnValue;
		}

        public static string cFillString( string sFillChar, int iLength ) {
            string sRetVal = "";
            for ( int x = 0; x < iLength; x++ )
                sRetVal = sRetVal + sFillChar;

            return sRetVal;
        }

        /// <summary>
        /// Left-pads with 0's a number
        /// </summary>
        /// <param name="oValue">The object to convert to a Zero-padded Number</param>
        /// <param name="iTotalLength">The length of the final string</param>
        /// <returns>The string value with the Left-padded number</returns>
        public static string cPaddedNumber( object oValue, int iTotalLength ) {

			return cPadString( parse.replaceAll( cStr( oValue ).Trim() , " " , "" ).Trim(), iTotalLength, "0", true );
		}

		/// <summary>
		/// Converts an object to a dollar amount
		/// </summary>
		/// <param name="oValue">The object to convert to a dollar string</param>
		/// <param name="iNumDecimals">The number of decimals to include</param>
		/// <returns>The string value with the dollar formatted string</returns>
		public static string cDollar( object oValue , int iNumDecimals ) {
			return "$" + cDecimalNumber( oValue , iNumDecimals  );			
		}

		public static string cDollar( object oValue ) 
		{
			return cDollar( oValue , 2  );			
		}

        /// <summary>
		/// Converts an object to a currency amount
		/// </summary>
		/// <param name="oValue">The object to convert to a dollar string</param>
		/// <param name="iNumDecimals">The number of decimals to include</param>
		/// <returns>The string value with the dollar formatted string</returns>
		public static string cCurrency( object oValue ) {

            System.Globalization.CultureInfo oCulture = (System.Configuration.ConfigurationManager.AppSettings["site.culture.id"] == null ? System.Globalization.CultureInfo.CurrentCulture : new System.Globalization.CultureInfo(System.Configuration.ConfigurationManager.AppSettings["site.culture.id"]));
            return String.Format( oCulture, "{0:C}", convert.cDbl(oValue) );
		}

		public static string cGroupedNumber(object oVal, int iGroupSize, string sSepChar) {
			string sVal = convert.cStr(convert.cInt(oVal)), sReturn = "";
			for (int x = sVal.Length-1; x >= 0; x--)
				sReturn = (x>0 && x < sVal.Length && (sVal.Length-x)%iGroupSize==0?sSepChar:"")+ sVal[x] + sReturn;
			return sReturn;
		}

		/// <summary>
		/// Converts an object to a string[]
		/// </summary>
		/// <param name="oValue">The object to convert to a string</param>
		/// <returns>Returns the string[] representation</returns>
		public static string[] cSplit(object oValue, string sSeparator)
		{
			string sValue		= cStr(oValue);
			
			return jlib.functions.parse.split(sValue, sSeparator);
		}


		/// <summary>
		/// Converts an object to a boolean
		/// </summary>
		/// <param name="oValue">The object to convert to a boolean</param>
		/// <returns>Returns the boolean representation</returns>
        public static bool cBool(params object[] oValues){
            if (oValues == null) return false;
            for (int x = 0; x < oValues.Length; x++) {
                string sValue = convert.cStr(oValues[x]).ToLower().Trim();
                if (sValue == "yes") return true;
                if (sValue == "1") return true;
                if (sValue == "on") return true;
                if (sValue == "true") return true;
                if (sValue == "y") return true;
            }            			
			return false;
		}

        public static string GetDayNumberSuffix(DateTime date)
        {
            if (date == null) return "";
                
            int day = date.Day;
            switch (day)
            {
                case 1:
                case 21:
                case 31:
                    return "st";

                case 2:
                case 22:
                    return "nd";

                case 3:
                case 23:
                    return "rd";

                default:
                    return "th";
            }
        }

        /// <summary>
        /// Converts an object to a DateTime value
        /// </summary>
        /// <param name="oValue">Date Time Value</param>
        /// <returns></returns>
        public static DateTime cDate(params object[] oValue) {
            return cDateNullable(false, oValue).Value;
        }
        public static DateTime? cDateNullable(bool nullable, params object[] oValue) {
            DateTime? nullableDate = null;

            if (oValue != null && oValue.Length > 0) {
                for (int x = 0; x < oValue.Length; x++)
                    if (oValue[x] != null) {
                        nullableDate = cDateNullable(true, oValue[x], System.Globalization.CultureInfo.CurrentCulture, DateTime.Now);
                        if (nullableDate != null) return nullableDate.Value;
                    }                
            }
            if(nullableDate==null && !nullable) return cDate(null, System.Globalization.CultureInfo.CurrentCulture);
            return nullableDate;
        }        

		public static DateTime cDate(object oValue, DateTime DefaultDate) {            
            return cDate(oValue, System.Globalization.CultureInfo.CurrentCulture, DefaultDate);
        }
        public static DateTime cDate(object oValue, bool bUseDisplayCulture) {
            return cDate(oValue, (convert.cStr(System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"]) == "" || !bUseDisplayCulture ? System.Globalization.CultureInfo.CurrentCulture : System.Globalization.CultureInfo.GetCultureInfo(System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"])));
        }
		public static DateTime cDate(object oValue, System.Globalization.CultureInfo oCulture)
		{
			return cDate(oValue, oCulture, DateTime.Now);
		}
        public static DateTime cDate(object oValue, System.Globalization.CultureInfo oCulture, DateTime DefaultDate) {
            return cDateNullable(false, oValue, oCulture, DefaultDate).Value;
        }
        public static DateTime? cDateNullable(bool nullable, object oValue, System.Globalization.CultureInfo oCulture, DateTime DefaultDate) {
            DateTime? nullableDate = null;
            if (oValue != null) {
                if (oValue.GetType() == typeof(DateTime)) return (DateTime)oValue;                
                string sValue = convert.cStr(oValue);
                DateTime d = DateTime.Now;
                if (DateTime.TryParse(sValue, oCulture.DateTimeFormat, DateTimeStyles.None, out d)) {
                    nullableDate = d;
                } else {
                    if (parse.removeXChars(sValue, "0123456789") == sValue) {
                        if (sValue.Length == 8)
                            sValue = sValue.Substring(0, 4) + "-" + sValue.Substring(4, 2) + "-" + sValue.Substring(6, 2);
                        else if (sValue.Length == 6)
                            sValue = sValue.Substring(0, 2) + "-" + sValue.Substring(2, 2) + "-" + sValue.Substring(4, 2);

                        if (DateTime.TryParse(sValue, oCulture.DateTimeFormat, DateTimeStyles.None, out d)) nullableDate = d;                       
                    }                
                }                                
            }
            if (nullableDate == null && !nullable) return DefaultDate;
            if (nullableDate == null) return null;
            return nullableDate.Value;
		}

        /// <summary>
        /// Converts an object to a DateTimeOffset value
        /// </summary>
        /// <param name="oValue">Date Time Value</param>
        /// <returns></returns>
        public static DateTimeOffset cDateOffset(params object[] oValue) {
            return cDateOffsetNullable(false, oValue).Value;
        }
        public static DateTimeOffset? cDateOffsetNullable(bool nullable, params object[] oValue) {
            DateTimeOffset? nullableDate = null;

            if (oValue != null && oValue.Length > 0) {
                for (int x = 0; x < oValue.Length; x++)
                    if (oValue[x] != null) {
                        nullableDate = cDateOffsetNullable(true, oValue[x], System.Globalization.CultureInfo.CurrentCulture, DateTimeOffset.Now);
                        if (nullableDate != null) return nullableDate.Value;
                    }
            }
            if (nullableDate == null && !nullable) return cDateOffset(null, System.Globalization.CultureInfo.CurrentCulture);
            return nullableDate;
        }

        public static DateTimeOffset cDateOffset(object oValue, DateTimeOffset DefaultDateOffset) {
            return cDateOffset(oValue, System.Globalization.CultureInfo.CurrentCulture, DefaultDateOffset);
        }
        public static DateTimeOffset cDateOffset(object oValue, bool bUseDisplayCulture) {
            return cDateOffset(oValue, (convert.cStr(System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"]) == "" || !bUseDisplayCulture ? System.Globalization.CultureInfo.CurrentCulture : System.Globalization.CultureInfo.GetCultureInfo(System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"])));
        }
        public static DateTimeOffset cDateOffset(object oValue, System.Globalization.CultureInfo oCulture) {
            return cDateOffset(oValue, oCulture, DateTimeOffset.Now);
        }
        public static DateTimeOffset cDateOffset(object oValue, System.Globalization.CultureInfo oCulture, DateTimeOffset DefaultDateOffset) {
            return cDateOffsetNullable(false, oValue, oCulture, DefaultDateOffset).Value;
        }
        public static DateTimeOffset? cDateOffsetNullable(bool nullable, object oValue, System.Globalization.CultureInfo oCulture, DateTimeOffset DefaultDateOffset) {
            DateTimeOffset? nullableDate = null;
            if (oValue != null) {
                if (oValue.GetType() == typeof(DateTimeOffset)) return (DateTimeOffset)oValue;
                string sValue = convert.cStr(oValue);
                DateTimeOffset d = DateTimeOffset.Now;
                if (DateTimeOffset.TryParse(sValue, oCulture.DateTimeFormat, DateTimeStyles.None, out d)) {
                    nullableDate = d;
                } else {
                    if (parse.removeXChars(sValue, "0123456789") == sValue) {
                        if (sValue.Length == 8)
                            sValue = sValue.Substring(0, 4) + "-" + sValue.Substring(4, 2) + "-" + sValue.Substring(6, 2);
                        else if (sValue.Length == 6)
                            sValue = sValue.Substring(0, 2) + "-" + sValue.Substring(2, 2) + "-" + sValue.Substring(4, 2);

                        if (DateTimeOffset.TryParse(sValue, oCulture.DateTimeFormat, DateTimeStyles.None, out d)) nullableDate = d;
                    }
                }
            }
            if (nullableDate == null && !nullable) return DefaultDateOffset;
            if (nullableDate == null) return null;
            return nullableDate.Value;
        }
		

		public static string cSQLDate(object oValue) {
			DateTime oDate = convert.cDate(oValue);
			return String.Format("{0:yyyy-MM-dd HH:mm:ss}.{1}", oDate, convert.cPaddedNumber( oDate.Millisecond, 3 ));
		}
        public static bool isDate(object oValue) {
            return isDate(oValue, System.Globalization.CultureInfo.CurrentCulture);
        }
        public static bool isDate(object oValue, bool bUseDisplayCulture) {
            return isDate(oValue, (convert.cStr(System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"]) == "" || !bUseDisplayCulture ? System.Globalization.CultureInfo.CurrentCulture : System.Globalization.CultureInfo.GetCultureInfo(System.Configuration.ConfigurationManager.AppSettings["site.display.culture.id"])));
        }
		public static bool isDate(object oValue, System.Globalization.CultureInfo oCulture){
			try{
				DateTime.Parse(oValue.ToString(), oCulture.DateTimeFormat);
				return true;
			}catch{
				return false;
			}			
		}

		public static bool isAllUppercase(string Value)
		{
			return ((Value ?? "") == (Value ?? "").ToUpper());
		}

		public static bool isAllLowercase(string Value)
		{
			return ((Value ?? "") == (Value ?? "").ToLower());
		}
		public static string toProperCase(string Value)
		{
			var textInfo = new CultureInfo("en-US", false).TextInfo;

			return textInfo.ToTitleCase((Value ?? "").ToLower());

		}
		public static double getFraction(object oValue) {
			return cDbl(oValue) - cInt(oValue);			
		}

        public static bool isEmail(object oEmail) {
            return isEmail(oEmail, true);
        }		
        public static bool isEmail(object oEmail, bool bAllowMultiple){
            try
            {
                string email = parse.replaceAll(oEmail, "�", "e", "�", "a", "�", "o", ";", ",");
                if (bAllowMultiple)
                {
                    email = parse.stripCharacter(email.Trim(), ",");
                    if (email.Contains(",")) email = parse.splitValue(email, ",", 0);
                }
                if (string.IsNullOrWhiteSpace(email))
                    return false;

                if (email.EndsWith("."))
                    return false;

                //Ref.: http://stackoverflow.com/questions/1365407/c-sharp-code-to-validate-email-address
                var m = new MailAddress(email);
                return m.Address == email;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Converts an object to a Literal Value
        /// </summary>
        /// <param name="oValue">Date Time Value</param>
        /// <returns></returns>
        public static System.Web.UI.WebControls.Literal cLiteral(object oValue)
		{
			System.Web.UI.WebControls.Literal oLiteral	= new System.Web.UI.WebControls.Literal();

			try
			{
				oLiteral.Text		= String.Format("{0}", oValue);
			}
			catch{}

			return oLiteral;
		}

        public static System.Collections.Generic.Dictionary<string, object> cDictionary(object []oValues) {
            System.Collections.Generic.Dictionary<string,object>o=new System.Collections.Generic.Dictionary<string,object>();
            for (int x = 0; x < oValues.Length; x = x + 2) o.Add(convert.cStr(oValues[x]), oValues[x + 1]);
            return o;
        }

        public static string getInnerText(XmlNode oNode, string sPath) 
		{
			try 
			{
				return oNode.SelectSingleNode( sPath ).InnerText;				

			}
			catch
			{
				return "";
			}			
		}

		public static System.Web.UI.WebControls.Unit cUnit(object oValue)
		{
			try
			{
				return System.Web.UI.WebControls.Unit.Parse(cStr(oValue));
			}
			catch
			{
				return System.Web.UI.WebControls.Unit.Empty;
			}
		}
        public static string cJoinModelString(string sModelString, DataRow oRow, string[] sFields) {
            return cJoinModelString(sModelString, oRow, sFields, 0);
        }
        public static string cJoinModelString(string sModelString, DataRow oRow, string[] sFields, int iRowCounter) {
            if (oRow == null) return null;
            object[] oValues = new object[sFields.Length];
            for (int x = 0; x < sFields.Length; x++) oValues[x] = oRow[sFields[x]];                
            return String.Format(parse.replaceAll(sModelString, "{counter}", iRowCounter.ToString()), oValues);
        }
        public static string cJoinModelString(string sModelString, DataTable oTable, string[] sFields) {
            StringBuilder oSB = new StringBuilder();
            for (int y = 0; y < oTable.Rows.Count; y++) oSB.Append(cJoinModelString(sModelString,oTable.Rows[y],sFields, y));            
            return oSB.ToString();
        }
        public static string cJoinModelString(string sModelString, DataTable[] oTables, string[] sFields) {
            StringBuilder oSB = new StringBuilder();
            for (int y = 0; y < oTables.Length; y++) oSB.Append(cJoinModelString(sModelString, oTables[y], sFields));
            return oSB.ToString();
        }
        public static List<string> cJoinModelStringList(string sModelString, XmlNodeList oNodes, string[] sFields) {
            List<string> list = new List<string>();            
            for (int y = 0; y < oNodes.Count; y++) {
                string sTemp = parse.replaceAll(sModelString, "{counter}", y.ToString());
                for (int x = 0; x < sFields.Length; x++)
                    sTemp = parse.replaceAll(sTemp, "{" + x + "}", (sFields[x].StartsWith("@") ? xml.getXmlAttributeValue(oNodes[y], sFields[x].Substring(1)) : xml.getXmlNodeTextValue(oNodes[y], sFields[x])));
                list.Add(sTemp);
            }
            return list;
        }
        public static string cJoinModelString(string sModelString, XmlNodeList oNodes, string[] sFields) {
            StringBuilder oSB = new StringBuilder();
            List<string> list = cJoinModelStringList(sModelString, oNodes, sFields);
            list.ForEach(x => oSB.Append(x));
            return oSB.ToString();
        }
        public static string cJoinModelString(string sModelString, params object[][] oValues) {
            string[] sTemp = new string[oValues[0].Length];
            for (int y = 0; y < oValues.Length; y++) {
                for (int x = 0; x < sTemp.Length; x++) {
                    if (sTemp[x] == null) sTemp[x] = sModelString;
                    sTemp[x] = parse.replaceAll(sTemp[x], "{" + y + "}", (oValues[y].Length > x ? convert.cStr(oValues[y][x]) : ""));
                }
            }
            return cJoin("", sTemp);
        }


        public static string cJoin(string sDelimiter, object []oValues) {
            return cJoin(sDelimiter, oValues, -1, -1);
        }
        public static string cJoin(string sDelimiter, object[] oValues, bool bUniqueOnly, bool bExcludeBlanks) {
            return cJoin(sDelimiter, oValues, "", -1, -1, null, bUniqueOnly, bExcludeBlanks);
        } 
        public static string cJoin(string sDelimiter, object[] oValues, string sPropertyName) {
            return cJoin(sDelimiter, oValues, sPropertyName, -1, -1);
        }
        public static string cJoin(string sDelimiter, object[] oValues, int iLowerIndex, int iUpperIndex) {
            return cJoin(sDelimiter, oValues,"", iLowerIndex, iUpperIndex);
        }
        public static string cJoin(string sDelimiter, object[] oValues, string sPropertyName, int iLowerIndex, int iUpperIndex) {
            return cJoin(sDelimiter, oValues, sPropertyName, iLowerIndex, iUpperIndex, null);
        }
        
        public static string cJoin(string sDelimiter, object[] oValues, string sPropertyName, int iLowerIndex, int iUpperIndex, object oBlankValue) {
            return cJoin(sDelimiter, oValues, sPropertyName, iLowerIndex, iUpperIndex, oBlankValue, false, false);
        }
        public static string cJoin(string sDelimiter, object[] oValues, string sPropertyName, int iLowerIndex, int iUpperIndex, object oBlankValue, bool bUniqueOnly, bool bExcludeBlanks) {
            string sRetVal = "";
            for (int x = (iLowerIndex>-1?iLowerIndex:0); x < (iUpperIndex==-1? oValues.Length : Math.Min(iUpperIndex,oValues.Length)); x++)
                if (oValues[x] != null) {
                    string s = convert.cStr((sPropertyName == "" ? oValues[x] : reflection.getPropertyValue(oValues[x], sPropertyName)), oBlankValue);
                    if ((s != "" || !bExcludeBlanks) && (!bUniqueOnly || (sDelimiter + sRetVal).IndexOf(sDelimiter + s + sDelimiter) == -1)) sRetVal += s + sDelimiter;
                }

            if (sRetVal != "")sRetVal = sRetVal.Substring(0, sRetVal.Length - sDelimiter.Length);

            return sRetVal;
        }
        public static string cJoin(string sDelimiter, params ArrayList[] oLists) {
            string sRet = "";
            for (int x = 0; x < oLists.Length; x++) sRet += convert.cIfValue(cJoin(sDelimiter, oLists[x], -1, -1), (sRet == "" ? "" : sDelimiter), "", "");
            return sRet;
		}
        public static string cJoin(string sDelimiter, ArrayList oList, int iLowerIndex, int iUpperIndex) {
            string sRetVal = "";
            for (int x = (iLowerIndex > -1 ? iLowerIndex : 0); x < (iUpperIndex == -1 ? oList.Count : Math.Min(iUpperIndex, oList.Count)); x++)
                if (convert.cStr(oList[x]) != "")
                    sRetVal = sRetVal + oList[x] + sDelimiter;

            if (sRetVal != "")
                sRetVal = sRetVal.Substring(0, sRetVal.Length - sDelimiter.Length);

            return sRetVal;
        }


        public static string cJoin(DataTable oDT, string sFieldName, string sSeparator) {
            return cJoin(oDT, sFieldName, sSeparator, false);
        }
        public static string cJoin(DataTable oDT, string sFieldName, string sSeparator, bool bUniqueOnly) {
            return cJoin(oDT, sFieldName, sSeparator, bUniqueOnly, true);
        }
        public static string cJoin(DataTable oDT, string sFieldName, string sSeparator, bool bUniqueOnly, bool bExcludeBlanks) {
            string sVal = sSeparator;
            for (int x = 0; x < oDT.Rows.Count; x++)
                if (!bUniqueOnly || sVal.IndexOf(sSeparator + oDT.Rows[x][sFieldName] + sSeparator) == -1)
                    sVal += oDT.Rows[x][sFieldName] + sSeparator;
            if (bExcludeBlanks) return parse.stripLeadingCharacter(parse.stripEndingCharacter(sVal, sSeparator), sSeparator);
            return (sVal == sSeparator ? "" : sVal.Substring(sSeparator.Length, sVal.Length - (sSeparator.Length * 2)));
        }
        public static string cJoin(Hashtable oTable, string sRowSeparator, string sColSeparator) {
            string s = "";
            foreach (DictionaryEntry o in oTable)
                s += o.Key + sColSeparator + o.Value + sRowSeparator;
            return s;
        }

        public static string cJoinDataRow(DataRow[] oRows, string sRowSeparator, string sColSeparator, params string[] sFieldNames) {
            return cJoinDataRow(oRows, sRowSeparator, sColSeparator, false, sFieldNames);
        }
        public static string cJoinDataRow(DataRow[] oRows, string sRowSeparator, string sColSeparator, bool bEscapeApostrophe, params string[] sFieldNames) {
            return cJoinDataRow(oRows, sRowSeparator, sColSeparator, false, false, sFieldNames);
        }
        public static string cJoinDataRow(DataRow[] oRows, string sRowSeparator, string sColSeparator, bool bEscapeApostrophe, bool bUniqueOnly, params string[] sFieldNames) {
            sColSeparator = convert.cStr(sColSeparator);
            sRowSeparator = convert.cStr(sRowSeparator);
            StringBuilder oSB = new StringBuilder();
            for (int x = 0; x < oRows.Length; x++) {
                string sRow = "";
                if (sFieldNames == null || sFieldNames.Length == 0) {
                    for (int y = 0; y < oRows[0].Table.Columns.Count; y++)
                        sRow += ((bEscapeApostrophe ? parse.replaceAll(oRows[x][y], "'", "''") : oRows[x][y]) + (y < oRows[0].Table.Columns.Count - 1 ? sColSeparator : ""));

                } else {
                    for (int y = 0; y < sFieldNames.Length; y++)
                        sRow += ((bEscapeApostrophe ? parse.replaceAll(oRows[x][sFieldNames[y]], "'", "''") : oRows[x][sFieldNames[y]]) + (y < sFieldNames.Length - 1 ? sColSeparator : ""));
                }
                if (!bUniqueOnly || oSB.ToString().IndexOf(sRow) == -1) oSB.Append(sRow.Substring(0, sRow.Length - sColSeparator.Length) + (x < oRows.Length - 1 ? sRowSeparator : ""));
            }
            return oSB.ToString();
        }

        public static object cMax(DataTable oDT, string sField, Type oType) {
            object o=null;
            for (int x = 0; x < oDT.Rows.Count; x++) {
                if (oType.FullName.StartsWith("System.Int") || oType.FullName == "System.Decimal" || oType.FullName == "System.Double" || oType.FullName == "System.Single")
                    o = Math.Max(convert.cDbl(o), convert.cDbl(oDT.Rows[x][sField]));
                else if (oType.Name == "DateTime") {
                    if (convert.cStr(oDT.Rows[x][sField]) != "") o = (o == null ? convert.cDate(oDT.Rows[x][sField]) : (convert.cDate(oDT.Rows[x][sField]).CompareTo((DateTime)o) > 0 ? convert.cDate(oDT.Rows[x][sField]) : o));
                } else if (oType.FullName == "System.String")
                    o = convert.cStr(o).CompareTo(convert.cStr(oDT.Rows[x][sField]))>0 ? o : convert.cStr(oDT.Rows[x][sField]);                
            }
            return o;
        }

        public static string[] cArray(params string[] sValues) {
            if( sValues == null ) return new string[0];
            return sValues;            
        }
        public static string[] cArray(object[] oValues, int iNewSize) {
            return cArray(oValues, iNewSize, null);
        }
        public static string[] cArray(object[] oValues, int iNewSize, string sInitialValue) {            
            string[] sValues = new string[iNewSize];            
            if(oValues!=null) for (int x = 0; x < iNewSize && x < oValues.Length; x++) sValues[x] = convert.cStr(oValues[x]);
            for (int x = (oValues == null ? 0 : oValues.Length); x < iNewSize; x++) sValues[x] = sInitialValue;            
            return sValues;
        }


        /// <summary>
        /// Convert Column names in a table to a string array
        /// </summary>
        /// <param name="oDT">The data table with the columns</param>
        /// <returns>A string array of all column names</returns>
        public static string[] cArray(DataTable oDT) {
            string[] sArr = new string[oDT.Columns.Count];
            for (int x = 0; x < oDT.Columns.Count; x++)
                sArr[x] = convert.cStr(oDT.Columns[x].ColumnName);

            return sArr;
        }


        /// <summary>
        /// Convert a field in a table to a string array
        /// </summary>
        /// <param name="oDT">The data table with the rows</param>
        /// <param name="sFieldName">The field name to use</param>
        /// <returns>A string array of all values for the selected field</returns>
        public static string[] cArray(DataTable oDT, string sFieldName) {
			string[] sArr = new string[oDT.Rows.Count];
			for (int x = 0; x < oDT.Rows.Count; x++)
				sArr[x] = convert.cStr(oDT.Rows[x][sFieldName]);

			return sArr;
		}

		public static ArrayList cArrayList(params object[] oValues) {
			ArrayList oArr = new ArrayList(oValues.Length);
			for( int x = 0; x < oValues.Length; x++ ) oArr.Add(oValues[x]);			
			return oArr;
		}

		public static string[] cArrayToString(Array oArr) {
			if (oArr == null) return new string[0];
			string[] sVal = new string[oArr.Length];
			for (int x = 0; x < sVal.Length; x++) sVal[x] = convert.cStr(oArr.GetValue(x));
			return sVal;
		}

		public static string[] cArrayToString(IList oList) {
			if (oList == null) return new string[0];
			string[] sVal = new string[oList.Count];
			for (int x = 0; x < sVal.Length; x++) sVal[x] = convert.cStr(oList[x]);
			return sVal;
		}

        public static string cArrayToString(IList oList, string sSeparator) {
            return cJoin(sSeparator, cArrayToString(oList));
        }

		public static Hashtable cHashtable(params object[] oValues) {
			Hashtable oTable = new Hashtable(oValues.Length / 2);
			for (int x = 0; x < oValues.Length; x = x + 2)
				oTable.Add(oValues[x], oValues[x + 1]);

			return oTable;
		}

		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//::  This function converts decimal degrees to radians             :::
		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		public static double deg2rad(double deg) 
		{
			return (deg * Math.PI / 180.0);
		}

		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//::  This function converts radians to decimal degrees             :::
		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		public static double rad2deg(double rad) 
		{
			return (rad / Math.PI * 180.0);
		}

        public static DateTime buildDate(DateTime oDatePart) {
            return buildDate(oDatePart, null);            
        }
        public static DateTime buildDate(DateTime oDatePart, DateTime oTimePart) {
            return buildDate(oDatePart, oTimePart.TimeOfDay);
        }
        public static DateTime buildDate(DateTime oDatePart, TimeSpan? oTimePart) {
            if (oTimePart == null)
                return new DateTime(oDatePart.Year, oDatePart.Month, oDatePart.Day);
            else {
                TimeSpan oTime = oTimePart.Value;
                return new DateTime(oDatePart.Year, oDatePart.Month, oDatePart.Day, oTime.Hours, oTime.Minutes, oTime.Seconds, oTime.Milliseconds);
            }
        }


		public static string convertUriToPath(string sUri) {
			if(sUri.StartsWith("file:///"))
				return sUri.Substring(8);

			if(sUri.StartsWith("file://"))
				return sUri.Substring(7);

			if(sUri.StartsWith("file:/"))
				return sUri.Substring(6);

			if(sUri.StartsWith("http:///"))
				return sUri.Substring(8);
			
			if(sUri.StartsWith("http://"))
				return sUri.Substring(7);

			if(sUri.StartsWith("http:/"))
				return sUri.Substring(6);

			return sUri;

		}

		public static string cByteToString(byte[] bBytes) {
			return cByteToString(bBytes, new System.Text.UTF7Encoding());
		}

		public static string cByteToString(byte[] bBytes, System.Text.Encoding oEncoding) {
			return oEncoding.GetString(bBytes);				
		}

        public static byte[] cStringToByte( object oData ) {
            return cStringToByte( oData, new System.Text.UTF7Encoding() );
        }

        public static byte[] cStringToByte( object oData, System.Text.Encoding oEncoding ) {
            return oEncoding.GetBytes( convert.cStr(oData) );
        }

        /* STREAM CONVERSIONS */

        public static byte[] cStreamToByteArray( System.IO.Stream oStream ) {

            try {
                if ( oStream == null || oStream.Length == 0 ) return new byte[0];
                oStream.Position = 0;
            } catch ( Exception ) { }

            System.IO.MemoryStream oMStream = new MemoryStream();
            int iPos = 0;
            int iRead = -1;

            while ( iRead != 0 ) {
                byte[] bArr = new byte[4096];
                iRead = oStream.Read( bArr, 0, bArr.Length );
                oMStream.Write( bArr, 0, iRead );
                iPos += iRead;
            }
            return oMStream.ToArray();
        }

        public static System.IO.Stream cByteArrayToStream(byte []Bytes) {
            try {
                if (Bytes == null || Bytes.Length == 0) return null;
                return new MemoryStream(Bytes);                
            } catch (Exception) { }
            return null;
        }

        public static string cStreamToString( System.IO.Stream oStream ) {
            return cStreamToString( oStream, new System.Text.UTF7Encoding() );
        }

        public static string cStreamToString( System.IO.Stream oStream, System.Text.Encoding oEncoding ) {
            byte[] oByte = cStreamToByteArray( oStream );
            return oEncoding.GetString( oByte );
        }

        public static Stream cStringToStream( object oData ) {
            return cStringToStream( convert.cStr(oData), new System.Text.UTF7Encoding() );
        }

        public static Stream cStringToStream( string sData, System.Text.Encoding oEncoding ) {
            MemoryStream oStream = new MemoryStream();
            byte[] bByte = cStringToByte( sData, oEncoding );
            oStream.Write( bByte, 0, bByte.Length );
            oStream.Position = 0;
            return oStream;
        }

        public static string cUrlEncode(string sUrl) {
            sUrl = parse.replaceAll(System.Web.HttpUtility.UrlEncode(sUrl), "%c3%a9", "%E9", "%c3%a8", "%E8", "%c3%a6", "%E6", "%c3%b8", "%F8", "%c3%a5", "%E5", "%c3%86", "%C6", "%c3%98", "%D8", "%c3%85", "%C5", "%c3%89", "%C9");            
            return sUrl;
        }

		public static string cJSON(IListSource Source) 
		{
			if (Source is DataTable)
				return cJSON((DataTable)Source, false);
			else if (Source is ICollection)
				return (Source as DataFramework.ICollection).ToJson();
			else
				return "";
        }

        public static string cJSON(DataTable oDT, bool bIncludeColNames) {
            return cJSON(oDT, bIncludeColNames, -1);
        }
        public static string cJSON(DataTable oDT, bool bIncludeColNames, int iColIndexAsArrayName) {
            return cJSON(oDT, 0, oDT.Rows.Count, bIncludeColNames, iColIndexAsArrayName);
        }
        public static string cJSON(DataTable oDT, int iStartRow, int iNumRows, bool bIncludeColNames, int iColIndexAsArrayName) {
            return cJSON(oDT, iStartRow, iNumRows, bIncludeColNames, iColIndexAsArrayName, null);
        }
        public static string cJSON(DataTable oDT, int iStartRow, int iNumRows, bool bIncludeColNames, int iColIndexAsArrayName, string []sColumnNames) {
            JSONConverter o = new JSONConverter();
            return o.cJSON(oDT, iStartRow, iNumRows, bIncludeColNames, iColIndexAsArrayName, sColumnNames);
        }

        public static string cJSON(helpers.structures.collection oCol) {
            return cJSON(oCol, false);
        }

        public static string cJSON(helpers.structures.collection oCol, bool bIncludeColNames) {
            JSONConverter o = new JSONConverter();
            return o.cJSON(oCol, bIncludeColNames);
        }

        public static object cCastObjectToType(object obj, Type type) {
            if (type.FullName == "System.String") {
                return (obj == null ? null : obj.Str());
            } else if (type.FullName == "System.Boolean") {
                return obj.Bln();
            } else if (type.Name == "DateTime" && jlib.functions.convert.cStr(obj) == "") {
                return System.DBNull.Value;
            } else if (type.Name == "DateTime") {
                return obj.Dte();
            } else if (type.FullName.StartsWith("System.Int")){
                return obj.Int();
            } else if (type.FullName == "System.Double") {
                return obj.Dbl();
            }else if(type.FullName == "System.Decimal") {
                return obj.Dec();
            } else if (type.FullName == "System.Byte") {
                return obj.Int();
            } else {
                return obj;
            }
        }

        public static JDataTable cXmlToDataTable(XmlNodeList xmlNodes, params string[] sFieldMappings) {
            JDataTable oDT = new JDataTable();
            for (int x = 0; x < sFieldMappings.Length; x = x + 2) oDT.Columns.Add(sFieldMappings[x]);
            for (int x = 0; x < xmlNodes.Count; x++) {
                DataRow oRow = oDT.NewRow();
                oDT.Rows.Add(oRow);
                for (int y = 0; (y * 2) < sFieldMappings.Length; y++) {
                    XmlNode oNode = xmlNodes[x].SelectSingleNode(sFieldMappings[y * 2 + 1]);
                    if (oNode != null) oRow[y] = (oNode.Value == null || oNode.Value == "" ? oNode.InnerText : oNode.Value);
                }
            }
            return oDT;
        }

        public static XmlNode cDataTableToXml(DataTable oDT, XmlNode xmlNode) {
            if(xmlNode==null){
                XmlDocument xmlDoc=new XmlDocument();
                xmlDoc.LoadXml("<query />");
                xmlNode = xmlDoc.DocumentElement;
            }
            for (int x = 0; x < oDT.Rows.Count; x++) xml.addXmlElementsFromRow(xml.addXmlElement(xmlNode, "item"), oDT.Rows[x]);            
            return xmlNode;
        }

        public static string []cDataRowsToArray(DataRow[] oRows, string sFieldName, bool bIncludeBlanks) {
            ArrayList oList = new ArrayList();
            for (int x = 0; x < oRows.Length; x++)
                if (bIncludeBlanks || convert.cStr(oRows[x][sFieldName]) != "") oList.Add(convert.cStr(oRows[x][sFieldName]));
            return (string[])oList.ToArray(typeof(string));
        }
        //public static DataTable cDataTable(XmlNodeList xmlNodes, params string[] sValues ){
        //    DataTable oDT=new DataTable();
        //    for (int x = 1; x < sValues.Length;x=x+2)oDT.Columns.Add(sValues[x]);
        //    for (int x = 0; x < xmlNodes.Count;x++){
        //        oDT.Rows.Add(oDT.NewRow());                
        //        for(int y=0;y<sValues.Length;y=y+2)
        //            oDT.Rows[oDT.Rows.Count-1][sValues[y+1]]= (sValues[y].StartsWith("@")?xml.getXmlAttributeValue(xmlNodes[x],sValues[y].Substring(1)) : xml.getXmlNodeTextValue(xmlNodes[x],sValues[y]));                
        //    }            
        //    return oDT;
        //}

        public static string cJoin(XmlNodeList xmlNodes, string sSeparator) {            
            StringBuilder oSB = new StringBuilder();
            for (int x = 0; x < xmlNodes.Count; x++)
                oSB.Append(xml.getXmlNodeTextValue(xmlNodes[x], null) + sSeparator);
            return parse.stripEndingCharacter(oSB.ToString(), sSeparator);
        }
        public static string cJoin(XmlNodeList xmlNodes, string sNode, string sSeparator) {
            return cJoin(xmlNodes, sNode, sSeparator, false);
        }
        public static string cJoin(XmlNodeList xmlNodes, string sNode, string sSeparator, bool bUniqueOnly) {
            if (xmlNodes.Count == 0) return "";
            string sVal=sSeparator;
            for (int x = 0; x < xmlNodes.Count; x++) {
                string s = (sNode.StartsWith("@") ? xml.getXmlAttributeValue(xmlNodes[x], sNode.Substring(1)) : xml.getXmlNodeTextValue(xmlNodes[x], sNode));
                if(!bUniqueOnly || sVal.IndexOf(sSeparator+s+sSeparator)==-1) sVal+=s + sSeparator;
            }
            return sVal.Substring(sSeparator.Length, sVal.Length - (sSeparator.Length * 2));
        }
        public static XmlNode cXmlNode(string sXml) {
            XmlDocument oDoc = new XmlDocument();
            oDoc.LoadXml("<data>" + sXml + "</data>");
            return oDoc.SelectSingleNode("data/*");
        }

        public static string makeSingular(object oValue, string sZeroOrPluralCharacter) {
            return makeSingular(oValue, sZeroOrPluralCharacter, "");

        }

        public static string makeSingular(object oValue, string sZeroOrPluralCharacter, string sSingularCharacter) {
            if (convert.cInt(oValue) == 1) return sSingularCharacter;
            return sZeroOrPluralCharacter;
        }
        public static string cQuotedPrintable(string sData) {
            StringBuilder oSB = new StringBuilder();
            for (int i = 0; i < sData.Length; i++) {
                int iAscii = sData[i];

                if (iAscii < 32 || iAscii == 61 || iAscii > 126) {
                    string sEncodedChar = convert.cHex(iAscii).ToUpper();
                    if (sEncodedChar.Length == 1) sEncodedChar = "0" + sEncodedChar;
                    oSB.Append("=" + sEncodedChar);
                } else {
                    oSB.Append(sData[i]);
                }
            }
            return oSB.ToString();
        }
        public static string cQuotedPrintableToString(string sData) {
            StringBuilder oSB = new StringBuilder();
            for (int i = 0; i < sData.Length; i++) {
                if(sData.Substring(i,1)=="="){
                    string sValue="";
                    if(sData.Substring(i+1,1)=="0"){
                        sValue=sData.Substring(i+2,1);
                    }else{
                        sValue=sData.Substring(i+1,2);
                    }
                    int iVal=cHexToInt("&H"+sValue);
                    if(cHex(iVal)==sValue){
                        oSB.Append(char.ConvertFromUtf32(iVal));
                        i+=2;
                    }else{
                        oSB.Append(sData.Substring(i,1));
                    }                    
                }else{
                    oSB.Append(sData.Substring(i,1));
                }                                
            }
            return oSB.ToString();            
        }

        public static bool HasProperty(dynamic obj, string property) {
            if (obj == null) return false;
            foreach (var s in obj.GetDynamicMemberNames())
                if (property == s) return true;
            return false;
        }
        public static object SafeGetProperty(dynamic obj, params string[] properties) {
            foreach(string property in properties){
                if (HasProperty(obj, property)) obj = obj[property];
                else return null;
            }                
            return obj;
        }
        ////Loops through all nested properties by name, and returns path and value in a dictionary (path will be on the form: Property>ChildProperty>GranddhildProperty
        //public static Dictionary<string,object> SafeGetChildProperties(dynamic obj, string currentPath, params string[] properties) {
        //    Dictionary<string, object> values = new Dictionary<string, object>();
        //    foreach (string s in obj.GetDynamicMemberNames()) {
        //        if (properties.Contains(s)) values.Add(currentPath + ">" + s, obj[s]);
        //        if()
        //    }

        //    foreach (string property in properties) {
        //        if (HasProperty(obj, property)) obj = obj[property];
        //        else return null;
        //    }
        //    return obj;
        //}
        public static List<dynamic> SafeGetList(dynamic obj, params string[] properties) {
            var ret = new List<dynamic>();
            foreach (string property in properties) {
                if (HasProperty(obj, property)) obj = obj[property];
                else return ret;
            }
            if (obj != null) {
                foreach (dynamic entry in obj)
                    ret.Add(entry);
            }
            return ret;
        }

		#endregion METHODS

		public static JDataTable ToJDataTable(this DataView DataView)
		{
			return DataView.ToJDataTable(null, false, new string[0]);
		}
		public static JDataTable ToJDataTable(this DataView DataView, string tableName)
		{
			return DataView.ToJDataTable(tableName, false, new string[0]);
		}
		public static JDataTable ToJDataTable(this DataView DataView, bool distinct, params string[] columnNames)
		{
			return DataView.ToJDataTable(null, distinct, columnNames);
		}
		public static JDataTable ToJDataTable(this DataView DataView, string tableName, bool distinct, params string[] columnNames)
		{
			if (columnNames == null)
			{
				throw new ArgumentNullException("columnNames");
			}
			JDataTable table = new JDataTable 
			{
				Locale = DataView.Table.Locale,
				CaseSensitive = DataView.Table.CaseSensitive,
				TableName = (tableName != null) ? tableName : DataView.Table.TableName,
				Namespace = DataView.Table.Namespace,
				Prefix = DataView.Table.Prefix
			};

			if (columnNames.Length == 0)
			{
				columnNames = new string[DataView.Table.Columns.Count];
				for (int j = 0; j < columnNames.Length; j++)
				{
					columnNames[j] = DataView.Table.Columns[j].ColumnName;
				}
			}
			int[] numArray = new int[columnNames.Length];
			List<object[]> arraylist = new List<object[]>();
			for (int i = 0; i < columnNames.Length; i++)
			{
				DataColumn column = DataView.Table.Columns[columnNames[i]];
				if (column == null)
				{
					//throw ExceptionBuilder.ColumnNotInTheUnderlyingTable(columnNames[i], DataView.Table.TableName);
				}
				table.Columns.Add(column.Clone());
				//table.Columns.Add(column);
				numArray[i] = DataView.Table.Columns.IndexOf(column);
			}
			foreach (DataRowView view in DataView)
			{
				object[] objectArray = new object[columnNames.Length];
				for (int k = 0; k < numArray.Length; k++)
				{
					objectArray[k] = view[numArray[k]];
				}
				if (!distinct || !RowExist(arraylist, objectArray))
				{
					table.Rows.Add(objectArray);
					arraylist.Add(objectArray);
				}
			}
			return table;
		}

		private static bool RowExist(List<object[]> arraylist, object[] objectArray)
		{
			for (int i = 0; i < arraylist.Count; i++)
			{
				object[] objArray = arraylist[i];
				bool flag = true;
				for (int j = 0; j < objectArray.Length; j++)
				{
					flag &= objArray[j].Equals(objectArray[j]);
				}
				if (flag)
				{
					return true;
				}
			}
			return false;
		}


		private static DataColumn Clone(this DataColumn originalColumn)
		{
			DataColumn column = new DataColumn();
			//column.SimpleType = originalColumn.SimpleType;
			column.AllowDBNull = originalColumn.AllowDBNull;

			column.AutoIncrement = originalColumn.AutoIncrement;
			column.AutoIncrementSeed = originalColumn.AutoIncrementSeed;
			column.AutoIncrementStep = originalColumn.AutoIncrementStep;

			column.Caption = originalColumn.Caption;
			column.ColumnName = originalColumn.ColumnName;
			//column._columnUri = originalColumn._columnUri;
			//column.._columnPrefix = originalColumn._columnPrefix;
			column.DataType = originalColumn.DataType;
			column.DefaultValue = originalColumn.DefaultValue;
			//column.defaultValueIsNull = (originalColumn.defaultValue == DBNull.Value) || (column.ImplementsINullable && DataStorage.IsObjectSqlNull(this.defaultValue));
			column.ColumnMapping = originalColumn.ColumnMapping;
			column.ReadOnly = originalColumn.ReadOnly;
			column.MaxLength = originalColumn.MaxLength;
			//column.dttype = originalColumn.dttype;
			column.DateTimeMode = originalColumn.DateTimeMode;
			if (originalColumn.ExtendedProperties != null)
			{
				foreach (object obj2 in originalColumn.ExtendedProperties.Keys)
				{
					column.ExtendedProperties[obj2] = originalColumn.ExtendedProperties[obj2];
				}
			}
			return column;
		}


		public static T GetEnumFromDescription<T>(string Description)
		{
			var type = typeof(T);
			if (!type.IsEnum) throw new InvalidOperationException();
			foreach (var field in type.GetFields())
			{
				var attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
				if (attribute != null)
				{
					if (attribute.Description == Description)
						return (T)field.GetValue(null);
				}
				else
				{
					if (field.Name == Description)
						return (T)field.GetValue(null);
				}
			}

			//Return default
			return default(T);
		}

		public static string GetEnumDescription<T>(T Value)
		{
			DescriptionAttribute attribute = Value.GetType()
				.GetField(Value.ToString())
				.GetCustomAttributes(typeof(DescriptionAttribute), false)
				.SingleOrDefault() as DescriptionAttribute;

			return attribute == null ? Value.ToString() : attribute.Description;

		}



	}
}
