using System;
using System.Xml;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using jlib.functions;
using System.ComponentModel;
using System.IO;

namespace jlib.functions
{
	/// <summary>
	/// Summary description for reflection.
	/// </summary>
	public class reflection
	{

		
		public static object instantiateObject( string sTypeName, params object[] oParams ) {

			string sAssemblyDLL						= buildAssemblyPath( sTypeName );
			return instantiateObject( sAssemblyDLL, sTypeName, oParams );

		}

		public static object instantiateObjectA( string sAssemblyFile, string sTypeName, params object[] oParams ) {

			string sPath	= Path.Combine(getExecutingDirectory(), sAssemblyFile);

			return instantiateObject( sPath, sTypeName, oParams );

		}

		public static object instantiateObject( string sAssemblyDLL, string sTypeName, params object[] oParams ) {		

            System.Reflection.Assembly oAssembly=null;
            if(convert.cStr(sAssemblyDLL)==""){
                Assembly[] oAssemblies = AppDomain.CurrentDomain.GetAssemblies();
                for (int x = 0; x < oAssemblies.Length; x++) {
                    if(oAssemblies[x].GetType(sTypeName)!=null){
                        oAssembly = oAssemblies[x];
                        break;
                    }
                }
            }else{
			    oAssembly	= System.Reflection.Assembly.LoadFrom( sAssemblyDLL );			
			    if(oAssembly.GetType( sTypeName ) == null) return null;
            }
            return instantiateObject(oAssembly, sTypeName, oParams);
		}

        public static object instantiateObject(Assembly oAssembly, string sTypeName, params object[] oParams) {
            Type[] oTypes = new Type[oParams.Length];
            for (int x = 0; x < oParams.Length; x++)
                if ((oParams[x] as Type) != null) {
                    oTypes[x] = oParams[x] as Type;
                    oParams[x] = null;
                } else
                    oTypes[x] = oParams[x].GetType();

            //return oAssembly.GetType( sTypeName ).GetConstructor( System.Type.EmptyTypes ).Invoke( oParams );
            return oAssembly.GetType(sTypeName).GetConstructor(oTypes).Invoke(oParams);	
        }


		public static Hashtable enumerateControlClientIDs(System.Web.UI.Control oParentControl, Hashtable oControls)
		{
			if(oControls == null)
			{
				oControls = new Hashtable();
			}

			foreach(System.Web.UI.Control oControl in oParentControl.Controls)
			{
				oControls.Add(oControl.ClientID, oControl);

				enumerateControlClientIDs(oControl, oControls);
			}

			return oControls;

		}

		public static Hashtable enumerateControlIDs(System.Web.UI.Control oParentControls)
		{
			return enumerateControlClientIDs(oParentControls, null);
		}

		public static Hashtable enumerateControlIDs(System.Web.UI.Control oParentControl, Hashtable oControls)
		{
			if(oControls == null)
			{
				oControls = new Hashtable();
			}

			foreach(System.Web.UI.Control oControl in oParentControl.Controls)
			{
				oControls.Add(oControl.ID, oControl);

				enumerateControlIDs(oControl, oControls);
			}

			return oControls;

		}

		public static ArrayList enumerateControls(System.Web.UI.Control oParentControl) 
		{
			return enumerateControls(oParentControl, null);
		}

		public static ArrayList enumerateControls(System.Web.UI.Control oParentControl, ArrayList oControls) 
		{
			if(oControls == null) 
			{
				oControls = new ArrayList();
			}

			foreach(System.Web.UI.Control oControl in oParentControl.Controls) 
			{
				oControls.Add(oControl);

				enumerateControls(oControl, oControls);
			}

			return oControls;
		}


		public static object getMemberValue(object oObject, string sName)
		{
			object oValue	= getPropertyValue(oObject, sName);
			if(oValue == null)
			{
				oValue		= getFieldValue(oObject, sName);
			}

			return oValue;
		}

		public static object getFieldValue(object oObject, string sName)
		{
			try
			{
				FieldInfo oField = oObject.GetType().GetField( sName , BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static );
				return oField.GetValue( oObject ); 
			}
			catch(Exception)
			{
				return null;
			}
		}

		public static object getPropertyControlValue(System.Web.UI.Control oControl, string sDataName)
		{
						
			string []sPropertyStack		= parse.split( sDataName, "." );
			if ( sDataName.Length < 2 || sPropertyStack[ 0 ].Trim() == "" )
				return null;
			
			object oDataSource			= getMemberValue( oControl.Page, sPropertyStack[ 0 ]);
			
			if ( oDataSource			== null)
			{
				System.Web.UI.Control oTempControl	= oControl.Parent;
				
				while( oDataSource == null && oTempControl != null )
				{
					oDataSource			= getMemberValue( oTempControl, sPropertyStack[ 0 ]);	
					oTempControl		= oTempControl.Parent;
				}
			}

			if ( oDataSource == null )
				return null;
						
			return getPropertyValue( oDataSource, string.Join( "." , sPropertyStack , 1 , sPropertyStack.Length - 1 ) );
		}

		public static bool doesPropertyExist(object oObject, string sPropertyPath)
		{
			try
			{		

				string []sPropertyStack		= parse.split(sPropertyPath, "." );
				PropertyInfo oProperty		= null;
			
				for ( int x = 0 ; x < sPropertyStack.Length ; x++ )
				{
					oProperty = oObject.GetType().GetProperty( sPropertyStack[ x ], BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static );

					if (oProperty == null) return false;

					if ( x < sPropertyStack.Length - 1 )
					{					
						oObject	= oProperty.GetValue( oObject , null );
					}
				}

				//Successful
				return true;
				
			}
			catch(Exception)
			{
				return false;
			}

			return false;
		}

		public static object getPropertyValue(object oObject, string sPropertPath)
		{
			try
			{		

				string []sPropertyStack		= parse.split(sPropertPath, "." );
				PropertyInfo oProperty		= null;
			
				for ( int x = 0 ; x < sPropertyStack.Length ; x++ )
				{
					oProperty = oObject.GetType().GetProperty( sPropertyStack[ x ], BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static );			
					if ( x < sPropertyStack.Length - 1 )
					{					
						oObject	= oProperty.GetValue( oObject , null );
					}
				}			

				//PropertyInfo oPropertyObject = oObject.GetType().GetProperty( sName , BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static );
				//return oPropertyObject.GetValue( oObject, null ); 
				return oProperty.GetValue( oObject, null ); 
			}
			catch(Exception)
			{
				return null;
			}
		}

        public static bool setPropertyValue(object oObject, string sPropertyPath, object oValue)
        {
            try
            {
                string[] sPropertyStack = parse.split(sPropertyPath, ".");
                if (sPropertyStack.Length > 1)
                {
                    oObject = getPropertyValue(oObject, sPropertyPath.Substr(0, sPropertyPath.LastIndexOf(".")));
                    sPropertyStack = new string[] { sPropertyStack[sPropertyStack.Length-1] };
                }
                return setValueOfProperty(oObject, sPropertyStack[0], oValue);                
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static PropertyInfo getPropertyInfo( object oObject, string sPropertyName ) {
            if (oObject == null) return null;
            try {
                return oObject.GetType().GetProperty(sPropertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Static);
            } catch (Exception) {
                return null;
            }            
        }		        

        public static void copyNamedObjectProperties( object oSource, object oDestination, params string[] sProps ) {
            for ( int x = 0; x < sProps.Length; x++ ) {
                try {
                    //PropertyInfo oPropInfo = getPropertyInfo( oSource, sProps[x] );
                    setValueOfObject( ref oDestination, sProps[x], getPropertyValue( oSource, sProps[x] ) );
                }catch(Exception){}
            }
        }
        public static void copyObjectProperties( object oSource, object oDestination, params string[] sExcludedProps ) {
            if ( sExcludedProps == null || sExcludedProps.Length == 0 ) sExcludedProps = new string[] { "MetaData", "ID", "Context", "MetaDataManager", "CustomIconPath", "TableName", "UseDeleted", "Version", "Users", "Table", "ItemArray", "isStructured", "isVersion", "HiddenContexts", "HasErrors", "Groups", "Deleted", "DataRowBuilder", "BaseContext", "EnableApprovalProcess", "EnableVersionTracking", "FrameworkIdentifier", "isNew", "Approved", "Item" };

            string sExcludedString = ";" + ( sExcludedProps == null ? "" : String.Join( ";", sExcludedProps ).ToLower() ) + ";";
            foreach ( PropertyInfo oProperty in oSource.GetType().GetProperties() ) {
                try {

                    if ( sExcludedString.IndexOf( ";" + oProperty.Name.ToLower() + ";" ) == -1 )
                        setValueOfObject( ref oDestination, oProperty.Name, oProperty.GetValue( oSource, null ) );

                } catch ( Exception ) { }

            }
        }

        public static void copyObjectFields( object oSource, object oDestination, params string[] sExcludedFields ) {
            //if ( sExcludedProps == null || sExcludedProps.Length == 0 ) sExcludedProps	= new string[]{"MetaData" , "ID", "Context", "MetaDataManager" , "CustomIconPath", "TableName", "UseDeleted", "Version", "Users", "Table", "ItemArray", "isStructured", "isVersion", "HiddenContexts", "HasErrors" , "Groups", "Deleted", "DataRowBuilder", "BaseContext", "EnableApprovalProcess", "EnableVersionTracking", "FrameworkIdentifier", "isNew", "Approved", "Item"};

            string sExcludedString = ";" + ( sExcludedFields == null ? "" : String.Join( ";", sExcludedFields ).ToLower() ) + ";";
            foreach ( FieldInfo oField in oSource.GetType().GetFields( BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static ) ) {
                try {

                    if ( sExcludedString.IndexOf( ";" + oField.Name.ToLower() + ";" ) == -1 )
                        oField.SetValue( oDestination, oField.GetValue( oSource ) );

                } catch ( Exception ) { }

            }
        }

        public static bool compareObjectProperties( object oSource, object oDestination, params string[] sExcludedProps ) {
            if( oSource.GetType() != oDestination.GetType() ) return false;

            if ( sExcludedProps == null || sExcludedProps.Length == 0 ) sExcludedProps = new string[] { "MetaData", "ID", "Context", "MetaDataManager", "CustomIconPath", "TableName", "UseDeleted", "Version", "Users", "Table", "ItemArray", "isStructured", "isVersion", "HiddenContexts", "HasErrors", "Groups", "Deleted", "DataRowBuilder", "BaseContext", "EnableApprovalProcess", "EnableVersionTracking", "FrameworkIdentifier", "isNew", "Approved", "Item" };

            string sExcludedString = ";" + ( sExcludedProps == null ? "" : String.Join( ";", sExcludedProps ).ToLower() ) + ";";
            foreach ( PropertyInfo oProperty in oSource.GetType().GetProperties() ) {
                try {

                    if ( sExcludedString.IndexOf( ";" + oProperty.Name.ToLower() + ";" ) == -1 )
                        if ( !oProperty.GetValue( oSource, null ).Equals( oProperty.GetValue( oDestination, null ) ) ) return false;                        

                } catch ( Exception ) { }
            }
            return true;
        }


        public static bool compareObjectFields( object oSource, object oDestination, params string[] sExcludedFields ) {
            if ( oSource.GetType() != oDestination.GetType() ) return false;

            string sExcludedString = ";" + ( sExcludedFields == null ? "" : String.Join( ";", sExcludedFields ).ToLower() ) + ";";
            foreach ( FieldInfo oField in oSource.GetType().GetFields( BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static ) ) {
                try {

                    if ( sExcludedString.IndexOf( ";" + oField.Name.ToLower() + ";" ) == -1 )
                        if( !oField.GetValue( oDestination ).Equals( oField.GetValue( oSource ) )) return false;

                } catch ( Exception ) { }

            }
            return true;
        }


		public static FieldInfo getFieldInfo(object oObject, string sName)
		{
			try
			{
				FieldInfo oField = oObject.GetType().GetField(sName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
				return oField;
			}
			catch (Exception)
			{
				return null;
			}
		}

		public static bool containsField(object oObject, string sName)
		{
			return getFieldInfo(oObject, sName) != null;
		}

        public static bool containsProperty(object oObject, string sName)
		{
            return getPropertyInfo(oObject, sName) != null;			
		}

		public static object getMethodValue(object oObject, string sName, params object[] oParameters){
			try{
				MethodInfo oMethodObject = oObject.GetType().GetMethod( sName , BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static );
				return oMethodObject.Invoke( oObject, oParameters ); 
			}catch(Exception){
				return null;
			}
		}

		public static object instantiateObject(System.Type typObject, params object[] oParameters){
			try{
				return Activator.CreateInstance(typObject, oParameters);
			}catch(Exception){
				return null;
			}
		}

		public static object getStaticMethodValue(System.Type typObject, string sName, params object[] oParameters){
			try{
				return typObject.InvokeMember(sName , BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.InvokeMethod, null, null, oParameters);
			}catch(Exception){
				return null;
			}
		}

		public static void populatePropertiesFromXml(ref object oObject, XmlNode xmlNode)
		{
			//Loop through Xml:Attributes
			foreach(XmlAttribute xmlAttribute in xmlNode.Attributes){
				//Get the Property Name
				string sNodeName		= xmlAttribute.Name;

				//Determine Correct Property Name
				string sPropertyName	= getBusinessSafePropertyName( sNodeName );
				
				//Get Handle To Property
				setValueOfObject(ref oObject, sPropertyName, xmlAttribute.Value);
				
			}

			//Loop through Xml:Nodes
			foreach(XmlNode xmlSubNode in xmlNode.ChildNodes)
			{
				//Get the Property Name
				string sNodeName		= xmlSubNode.Name;

				//Determine Correct Property Name
				string sPropertyName	= getBusinessSafePropertyName( sNodeName );
				
				//Get Handle To Property
				setValueOfObject(ref oObject, sPropertyName, xmlSubNode.InnerXml);
				
			}

		}

		public static void populatePropertiesFromDB(ref object oObject, DataRow drRow)
		{
			//Loop through DataRow:Columns
			foreach(DataColumn dcColumn in drRow.Table.Columns)
			{
				//Get the Property Name
				string sNodeName		= dcColumn.ColumnName;

				//Determine Correct Property Name
				string sPropertyName	= getBusinessSafePropertyName( sNodeName );
				
				//Get Handle To Property
				setValueOfObject(ref oObject, sPropertyName, drRow[ dcColumn ]);
				
			}

		}
		public static void copyDBtoDB(DataRow drToRow, DataRow drFromRow)
		{
			//Loop through DataRow:Columns
			foreach(DataColumn dcColumn in drFromRow.Table.Columns)
			{
				if(drToRow.Table.Columns.Contains(dcColumn.ColumnName))
					drToRow[dcColumn.ColumnName]	= drFromRow[dcColumn.ColumnName];
			}

		}

		public static void savePropertiesToDB(object oObject, ref DataRow drRow)
		{
			//Loop through DataRow:Columns
			foreach(DataColumn oColumn in drRow.Table.Columns)
			{
				//Determine Correct Property Name
				string sPropertyName	= getBusinessSafePropertyName( oColumn.ColumnName );
				
				//Get Handle To Property
				setValueOfDbColumn(oObject, sPropertyName, ref drRow, oColumn);
				
			}

		}

		public static void savePropertiesToXml(object oObject, ref XmlNode xmlNode)
		{
			//Loop through DataRow:Columns
			foreach(PropertyInfo oProperty in oObject.GetType().GetProperties())
			{
			
				//Get Handle To Property
				setValueOfXmlAttribute(oObject, oProperty.Name, ref xmlNode, oProperty.Name);
				
			}

		}

        public static Type getGenericTypeObject(Type oType) {            
            foreach (Type oInterfaceType in oType.GetInterfaces()) {
                if (oInterfaceType.IsGenericType) {
                    if (oInterfaceType.GetGenericTypeDefinition() == typeof(IList<>)) return oInterfaceType;
                    else if (oInterfaceType.GetGenericTypeDefinition().Name.StartsWith("IDictionary")) return oInterfaceType;                                         
                }
            }
            return null;
        }
        public static List<Type> getGenericDeclarationTypes(Type oType){
            Type oInterfaceType = getGenericTypeObject(oType);
            List<Type> oTypes = new List<Type>();
            if (oInterfaceType != null) {
                for (int x = 0; x < oInterfaceType.GetGenericArguments().Length; x++)
                    oTypes.Add(oInterfaceType.GetGenericArguments()[x]);
            }            
            return oTypes;
        }

		public static string getBusinessSafePropertyName(string sPropertyName)
		{
			sPropertyName					= "_" + sPropertyName;
			sPropertyName					= sPropertyName.Replace("__", "_");

			if (sPropertyName.EndsWith("_id"))
			{
				sPropertyName				= sPropertyName.Substring(0, sPropertyName.Length - 3) + "_ID";
			}
			MatchCollection oMatches		= Regex.Matches(sPropertyName, @"(_\w)");
			foreach(Match oMatch in oMatches)
			{
				sPropertyName	= sPropertyName.Replace( oMatch.Value, oMatch.Value.ToUpper().Replace("_", "") );
			}
			
			sPropertyName					= sPropertyName.Replace("_", "");

			return sPropertyName;
		}

		
		public static bool setPropertyControlValue( System.Web.UI.Control oControl, string sDataName, object oValue) 
		{		

			string []sPropertyStack		= parse.split( sDataName, "." );

			if ( sDataName.Length < 2 || sPropertyStack[ 0 ].Trim() == "" || sPropertyStack.Length == 1 )
				return false;

			object oDataSource			= getMemberValue( oControl.Page, sPropertyStack[ 0 ]);	
			
			if ( oDataSource			== null)
			{
				System.Web.UI.Control oTempControl	= oControl.Parent;
				
				while( oDataSource == null && oTempControl != null )
				{
					oDataSource			= getMemberValue( oTempControl, sPropertyStack[ 0 ]);	
					oTempControl		= oTempControl.Parent;
				}
			}
			
			if ( oDataSource == null )
				return false;
						
			return setValueOfObject( ref oDataSource, string.Join( "." , sPropertyStack , 1 , sPropertyStack.Length - 1 ), oValue );

		}

				
		public static bool setValueOfObject(ref Object oObject, string sPropertyPath, object oValue)
		{
			if(oObject == null)
				return false;

			string []sPropertyStack		= parse.split(sPropertyPath, "." );
			PropertyInfo oProperty		= null;
			
			for ( int x = 0 ; x < sPropertyStack.Length ; x++ )
			{
				try
				{
					oProperty = oObject.GetType().GetProperty( sPropertyStack[ x ], BindingFlags.IgnoreCase | BindingFlags.Public  | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Static );			
				}
				catch(Exception e){ throw new Exception( "Reflection Error: " + sPropertyPath, e ); }

				if ( x < sPropertyStack.Length - 1 )
				{					
					oObject	= oProperty.GetValue( oObject , null );
				}
			}						
			
			//If Property Can Be Found By Exact Match
			if(oProperty == null)
			{
				if((oObject as DataRow) != null && (oObject as DataRow).Table.Columns.Contains(sPropertyPath))
					(oObject as DataRow)[sPropertyPath]	= oValue;
			}
			else
			{
				return setValueOfProperty( ref oObject, oProperty, oValue );
			}

			return false;
		}

        public static bool setValueOfProperty(object oObject, string sPropertyName, object oValue) {
            try {
                if (oObject != null)
                {
                    PropertyInfo o = jlib.functions.reflection.getPropertyInfo(oObject, sPropertyName);
                    if (o != null && o.CanWrite)
                        return setValueOfProperty(ref oObject, o, oValue);
                }
            } catch (Exception) { }
            return false;
        }
		public static bool setValueOfProperty( ref Object oObject, PropertyInfo oProperty, object oValue )
		{
			try 
			{
                if (oProperty.CanRead) {
                    object OriginalValue = oProperty.GetValue(oObject);
                    if (OriginalValue == oValue || (oValue!=null && convert.cStr(oValue) == convert.cStr(OriginalValue)))
                        return true;
                }                              
                
				switch( oProperty.PropertyType.FullName ) 
				{
					case "System.String":
						oProperty.SetValue( oObject, convert.cStr(oValue), null );
						break;

					case "System.Int16":
					case "System.Int32":
					case "System.Int64":
						oProperty.SetValue( oObject, convert.cInt(oValue), null );
						break;

					case "System.Decimal":
						oProperty.SetValue( oObject, convert.cDec(oValue), null );
						break;

					case "System.Single":
						oProperty.SetValue( oObject, convert.cSng(oValue), null );
						break;

					case "System.Double":						
					case "System.Currency":
						oProperty.SetValue( oObject, convert.cDbl(oValue), null );
						break;

					case "System.DateTime":
						oProperty.SetValue( oObject, convert.cDate(oValue), null );
						break;

					case "System.Web.UI.WebControls.Unit":
						oProperty.SetValue( oObject, convert.cUnit(oValue), null );
						break;

					case "System.Boolean":
						oProperty.SetValue( oObject, convert.cBool(oValue), null );
						break;
				
					default:

						if ( oProperty.PropertyType != null && oProperty.PropertyType.BaseType != null && oProperty.PropertyType.BaseType.FullName	== "System.Enum" ) 
						{						
							oProperty.SetValue( oObject, System.Enum.Parse( oProperty.PropertyType  , convert.cStr( oValue ) , true ) , null );
						}
						else 
						{
							oProperty.SetValue( oObject, oValue, null );							
						}
						break;
				}

				return true;
			}
			catch(Exception e) 
			{					
							
				Console.WriteLine( e );
			}
			return false;
		}
        public static bool setValueOfField(object oObject, string fieldName, object oValue) {
            try {
                FieldInfo o = jlib.functions.reflection.getFieldInfo(oObject, fieldName);
                if (o != null) {
                    return setValueOfField(oObject, o, oValue);                                        
                }
            } catch (Exception) { }
            return false;
        }
        public static bool setValueOfField(object oObject, FieldInfo oFieldInfo, object oValue) {
            try {
                switch (oFieldInfo.FieldType.FullName) {
                    case "System.String":
                        oFieldInfo.SetValue(oObject, convert.cStr(oValue));
                        break;

                    case "System.Int16":
                    case "System.Int32":
                    case "System.Int64":
                        oFieldInfo.SetValue(oObject, convert.cInt(oValue));
                        break;

                    case "System.Decimal":
                        oFieldInfo.SetValue(oObject, convert.cDec(oValue));
                        break;

                    case "System.Single":
                        oFieldInfo.SetValue(oObject, convert.cSng(oValue));
                        break;

                    case "System.Double":
                    case "System.Currency":
                        oFieldInfo.SetValue(oObject, convert.cDbl(oValue));
                        break;

                    case "System.DateTime":
                        oFieldInfo.SetValue(oObject, convert.cDate(oValue));
                        break;

                    case "System.Web.UI.WebControls.Unit":
                        oFieldInfo.SetValue(oObject, convert.cUnit(oValue));
                        break;

                    case "System.Boolean":
                        oFieldInfo.SetValue(oObject, convert.cBool(oValue));
                        break;

                    default:

                        if (oFieldInfo.FieldType != null && oFieldInfo.FieldType.BaseType != null && oFieldInfo.FieldType.BaseType.FullName == "System.Enum") {
                            oFieldInfo.SetValue(oObject, System.Enum.Parse(oFieldInfo.FieldType, convert.cStr(oValue), true));
                        } else {
                            oFieldInfo.SetValue(oObject, oValue);
                        }
                        break;
                }

                return true;
            } catch (Exception e) {

                Console.WriteLine(e);
            }
            return false;
        }
		public static bool setValueOfDbColumn(Object oObject, string sPropertyName, ref DataRow drRow, DataColumn dcColumn)
		{
			//Get Handle To Property
			PropertyInfo oProperty		= oObject.GetType().GetProperty( sPropertyName , BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static );
			if(oProperty != null)
			{
				try
				{
					if ( dcColumn.DataType.FullName=="System.DateTime" )
					{
						
						DateTime oDate		= convert.cDate( oProperty.GetValue( oObject, null ) );
						if ( oDate.Year < 1753 )
							oDate			= DateTime.Parse( "01/01/1753 12:00:00 AM" );
						else if ( oDate.Year > 9999 )
							oDate			= DateTime.Parse( "12/31/9999 11:59:59 PM" );
						
						drRow[dcColumn]		= oDate;
					}
					else
						drRow[dcColumn]		= oProperty.GetValue( oObject, null );

					return true;
				}
				catch(Exception e)
				{
					Console.WriteLine( e );
				}
			}

			return false;
		}

		public static bool setValueOfXmlAttribute(Object oObject, string sPropertyName, ref XmlNode xmlNode, string sAttributeName)
		{
			//Get Handle To Property
			PropertyInfo oProperty		= oObject.GetType().GetProperty( sPropertyName , BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static );
			if(oProperty != null)
			{
				try
				{
					XmlAttribute xmlAttribute	= xmlNode.Attributes[sAttributeName];
					if(xmlAttribute == null)
					{
						xmlAttribute = xmlNode.OwnerDocument.CreateAttribute(sAttributeName);
						xmlNode.Attributes.Append( xmlAttribute );
					}

					xmlAttribute.Value		= convert.cStr( oProperty.GetValue( oObject, null ) );

					return true;
				}
				catch(Exception){}
			}

			return false;
		}

		

		public static object executeMethod( object oObject, string sMethodName , object []oParams )
		{
			try
			{
				return oObject.GetType().InvokeMember( sMethodName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.InvokeMethod , null, oObject, oParams );
			}
			catch( Exception ex )
			{
				return ex;
			}
		}
        public static object executeMethodUntyped(object oObject, string sMethodName, string[] sParams) {
            object []oParams = new object[sParams.Length];
            for (int x = 0; x < sParams.Length; x++) {
                string[] sDetails = parse.split(sParams[x], "|");
                if (sDetails[1].ToLower() == "system.string") {
                    oParams[x]=sDetails[0];
                } else if (sDetails[1].ToLower().StartsWith("system.int")) {
                    oParams[x]=convert.cInt(sDetails[0]);
                } else if (sDetails.Length > 2) {
                    Assembly oAssembly = System.Reflection.Assembly.LoadFrom(System.AppDomain.CurrentDomain.BaseDirectory + sDetails[2]);
                    Type oType = oAssembly.GetType(sDetails[1]);
                    if (oType.IsEnum) oParams[x]=Enum.Parse(oType, sDetails[0]);                                            
                }                
            }
            return executeMethod(oObject, sMethodName, oParams);
        }

        public static string getNamespace(string sNameSpace)
		{
			string[] oOctects = jlib.functions.parse.split(sNameSpace, ".");
			string sReturnValue		= "";
			
			for(int i=0;i<oOctects.Length-1;i++)
			{
				sReturnValue += oOctects[i];

				if(i<oOctects.Length-2)
					sReturnValue += ".";
			}

			return sReturnValue;
		}
		public static string getExecutingDirectory()
		{
			string sPath	= jlib.functions.util.convertUriToPath( System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
			sPath			= Path.GetDirectoryName(sPath); 

			return sPath;
		}
		public static string buildAssemblyPath(string sAssembly)
		{
            if (convert.cStr(sAssembly) == "") return "";
			sAssembly		= sAssembly.ToLower();

			if(!sAssembly.EndsWith(".dll"))
				sAssembly	= String.Format("{0}.dll", sAssembly);

			string sPath	= Path.Combine(getExecutingDirectory(), sAssembly);

			return sPath;
		}


	}
}
