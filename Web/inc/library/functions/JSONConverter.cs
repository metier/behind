﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Drawing;
using System.Threading;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace jlib.functions {
    public class JSONConverter {
        public delegate void OnBeforeRowRender(object oSender, StringBuilder oSB, DataTable oDT, int iIndex);
        public delegate void OnGetJSONSafeValue(object oSender, object oDataSource, int iIndex, string sFieldName, string sOriginalValue, ref string sJSONValue);
        public event OnBeforeRowRender BeforeRowRender;
        public event OnGetJSONSafeValue GetJSONSafeValue;


        public string cJSON(DataTable oDT, bool bIncludeColNames) {
            return cJSON(oDT, 0, oDT.Rows.Count, bIncludeColNames, -1);
        }

        public string cJSON(DataTable oDT, int iStartRow, int iNumRows, bool bIncludeColNames, int iColIndexAsArrayName) {
            return cJSON(oDT, iStartRow, iNumRows, bIncludeColNames, iColIndexAsArrayName, null);
        }

        public string cJSON(helpers.structures.collection oCol, bool bIncludeColNames) {
            string[] sColumnNames = new string[] { "ID", "Value", "Aux_1" };
            //StringBuilder oSB = new StringBuilder((bIncludeColNames ? "[" : "Array("));
            StringBuilder oSB = new StringBuilder("[");
            for (int x = 0; x < oCol.Count; x++) {
                oSB.Append((bIncludeColNames ? "{" : "["));
                for (int y = 0; y < oCol.ItemArray(x).Length; y++) {
                    string s = JSONConverter.cJSONSafeValue(oCol.ItemArray(x)[y], typeof(string));
                    if (GetJSONSafeValue != null) GetJSONSafeValue(this, oCol, x, sColumnNames[y], convert.cStr(oCol.ItemArray(x)[y]), ref s);
                    oSB.Append((bIncludeColNames ? "\"" + sColumnNames[y] + "\":" : "") + s + (y < oCol.ItemArray(x).Length - 1 ? "," : ""));
                }
                //oSB.Append((bIncludeColNames ? "}" : ")") + (x < oCol.Count - 1 ? "," : ""));
                oSB.Append((bIncludeColNames ? "}" : "]") + (x < oCol.Count - 1 ? "," : ""));
            }
            //oSB.Append((bIncludeColNames ? "]" : ")"));
            oSB.Append("]");
            return oSB.ToString();
        }
        
        public string cJSONObject(helpers.structures.collection oCol) {
            StringBuilder oSB = new StringBuilder("(");
            for (int x = 0; x < oCol.Count; x++) {
                string s = JSONConverter.cJSONSafeValue(oCol[x], typeof(string));
                if (GetJSONSafeValue != null) GetJSONSafeValue(this, oCol, x, oCol.Keys[x], convert.cStr(oCol[x]), ref s);
                oSB.Append("{\"" + oCol.Keys[x] + "\":" + s + "}" + (x < oCol.Count - 1 ? "," : ""));
            }
            return oSB.Append(")").ToString();
        }

		public string cJsonSingleObject(helpers.structures.collection oCol)
		{
			StringBuilder oSB = new StringBuilder("{");
			for (int x = 0; x < oCol.Count; x++)
			{
				if (oCol[x] is helpers.structures.collection)
				{
					oSB.Append("\"" + oCol.Keys[x] + "\":" + this.cJsonSingleObject(oCol[x] as helpers.structures.collection) + "" + (x < oCol.Count - 1 ? "," : ""));
				}

				else
				{
					string s = JSONConverter.cJSONSafeValue(oCol[x], typeof(string));
					if (GetJSONSafeValue != null) GetJSONSafeValue(this, oCol, x, oCol.Keys[x], convert.cStr(oCol[x]), ref s);
					oSB.Append("\"" + oCol.Keys[x] + "\":" + s + "" + (x < oCol.Count - 1 ? "," : ""));
				}
			}
			return oSB.Append("}").ToString();
		}

        //todo: JSON escape, support other datatypes
        public string cJSON(DataTable oDT, int iStartRow, int iNumRows, bool bIncludeColNames, int iColIndexAsArrayName, params string[] sColumnNames) {

            //StringBuilder oSB = new StringBuilder(( bIncludeColNames ? "[" : "Array(" ));
            StringBuilder oSB = new StringBuilder(iColIndexAsArrayName>-1?"{":"[");
            if (iStartRow < 0) iStartRow = 0;
            if (iNumRows < 0) iNumRows = oDT.Rows.Count-iStartRow;            
            for (int x = iStartRow; x < iNumRows + iStartRow; x++) {
                if (BeforeRowRender != null) BeforeRowRender(this, oSB, oDT, x);
                oSB.Append((iColIndexAsArrayName>-1 ? "\""+oDT.Rows[x][iColIndexAsArrayName]+"\":" : "" ) + (bIncludeColNames ? "{" : "["));
                for (int y = 0; ; y++) {
                    if (y != iColIndexAsArrayName) {
                        if (sColumnNames == null || sColumnNames.Length == 0) {
                            string s = JSONConverter.cJSONSafeValue(oDT.Rows[x][y], oDT.Columns[y].DataType);
                            if (GetJSONSafeValue != null) GetJSONSafeValue(this, oDT, x, oDT.Columns[y].ColumnName, convert.cStr(oDT.Rows[x][y]), ref s);
                            oSB.Append((bIncludeColNames ? "\"" + oDT.Columns[y].ColumnName + "\":" : "") + s + (y < oDT.Columns.Count - 1 ? "," : ""));
                            if (y == oDT.Columns.Count - 1) break;
                        } else {
                            string s = JSONConverter.cJSONSafeValue(oDT.Rows[x][sColumnNames[y]], oDT.Columns[sColumnNames[y]].DataType);
                            if (GetJSONSafeValue != null) GetJSONSafeValue(this, oDT, x, sColumnNames[y], convert.cStr(oDT.Rows[x][sColumnNames[y]]), ref s);
                            oSB.Append((bIncludeColNames ? "\"" + sColumnNames[y] + "\":" : "") + s + (y < sColumnNames.Length - 1 ? "," : ""));
                            if (y == sColumnNames.Length - 1) break;
                        }
                    }
                }
                oSB.Append((bIncludeColNames ? "}" : "]") + (x < iNumRows + iStartRow - 1 ? "," : ""));
            }
            //oSB.Append( ( bIncludeColNames ? "]" : ")") ); 
            oSB.Append(iColIndexAsArrayName > -1 ? "}" : "]");
            return oSB.ToString();
        }

        public static string cJSONArray(object[] oValues, Type oType) {
            StringBuilder oSB = new StringBuilder("[");
            if (oValues != null)
                for (int x = 0; x < oValues.Length; x++) oSB.Append((x > 0 ? "," : "") + cJSONSafeValue(oValues[x], oType, true));

            return oSB.Append("]").ToString();
        }

		public static string cJSONArray(IList oValues, Type oType) {
            StringBuilder oSB = new StringBuilder("[");

            if (oValues != null)
			{
				int count = 0;
				foreach(object item in oValues)
				{
					count++;
					if (count > 1) oSB.Append(",");
					oSB.Append(cJSONSafeValue(item, oType, true));
				}
			}

            return oSB.Append("]").ToString();
        }

        public static string cJSONSafeValue(object oData, System.Type oDataType) {
            return cJSONSafeValue(oData, oDataType, false);
        }
        public static string cJSONSafeValue(object oData, System.Type oDataType, bool bEnforceDataType) {
            if (oData is IList)
                return cJSONArray((IList)oData, typeof(string));
            else if (oData is helpers.structures.collection)
                return new JSONConverter().cJsonSingleObject(oData as helpers.structures.collection);
            else if (oDataType.FullName == "System.Boolean")
                return (convert.cBool(oData) ? "true" : "false");
            else if (oDataType.FullName == "System.Byte[]")
                oData = convert.cByteToString(oData as byte[]);
            else if ((oDataType.FullName != "System.String" || !bEnforceDataType) && convert.isNumeric(oData, false) && !convert.cStr(oData).StartsWith("0"))
                return (convert.cDbl(oData)).ToString();
            else if (oDataType.FullName == "jlib.functions.json.DynamicJson")
                return (oData as jlib.functions.json.DynamicJson).ToString();

            return "\"" + parse.replaceAll(oData, "<script", "&lt;script", "</script", "&lt;/script", "\\", "\\\\", "\r\n", "\n", "\n", "\\n", "\"", "\\\"", "\r", "\\n") + "\"";


            /*case System.Data.DbType.Currency:
            case System.Data.DbType.Decimal:
            case System.Data.DbType.Double:
            case System.Data.DbType.Int16:
            case System.Data.DbType.Int32:
            case System.Data.DbType.Int64:
            case System.Data.DbType.Single:
            case System.Data.DbType.UInt16:
            case System.Data.DbType.UInt64:
            case System.Data.DbType.VarNumeric:
             * case "System.Decimal":
*/

        }        
    }
}
