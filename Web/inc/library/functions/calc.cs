﻿using System;
using System.Collections.Generic;
using jlib.functions;
using System.Text;

namespace jlib.functions {
    public class calc {
        public static int mod10(object oData) {
            int iCounter = 0;
            string sData=convert.cStr(oData);
            for (int x = sData.Length-1; x >= 0; x--) {
                if( (sData.Length-x) % 2==0)
                    iCounter+=convert.cInt(sData.Substring(x,1));
                else{
                    if( convert.cInt(sData.Substring(x,1))*2<10)
                        iCounter += convert.cInt(sData.Substring(x, 1))*2;
                    else
                        iCounter += convert.cInt((convert.cInt(sData.Substring(x, 1)) * 2).ToString().Substring(0, 1)) + convert.cInt((convert.cInt(sData.Substring(x, 1)) * 2).ToString().Substring(1, 1));
                }
            }
            return Math.Min(9, 10-convert.cInt(parse.sRight(iCounter,1)));
        }
    }
}
