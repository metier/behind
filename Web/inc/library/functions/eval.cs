using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Reflection;
using System.Text;
using Microsoft.CSharp;
using System.Collections.Generic;
using System.Linq;

namespace jlib.functions
{
	/// <summary>
	/// Summary description for eval.
	/// </summary>
	public class eval
	{
		public eval()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static string execute( string sStr , string sFunctionParams , string sNameSpace, object []oFunctionParams, string []sUsing, string []sRefs ){
						
			StringBuilder oSB	= parseString( sStr , sFunctionParams, sNameSpace, sUsing );
			
			return executeStringBuilder( oSB , sNameSpace , oFunctionParams , sRefs );
		}

		public static StringBuilder parseString( string sStr , string sFunctionParams , string sNameSpace, string []sUsing ){
			
			StringBuilder oSB	= new StringBuilder();
			sStr				= parse.replaceAll( parse.replaceAll( sStr , "\r" , "\\r" ) , "\n" , "\\n" );
			string []sArr	= parse.split( sStr , "<%" ) ;
            
			oSB.Append( "using System; \n");
			oSB.Append( "using System.Data; \n");
			oSB.Append( "using System.Data.SqlClient; \n");
			oSB.Append( "using System.Data.OleDb; \n");
			oSB.Append( "using System.Xml; \n");
			oSB.Append( "using System.Text; \n");
			//oSB.Append( "using " + this.GetType().Namespace + "; \n" );

			if ( sUsing != null ){
				for ( int x = 0 ; x < sUsing.Length; x++ )
					if ( convert.cStr( sUsing[ x ] ) != "" )
						oSB.Insert( 0 , "using " + convert.cStr( sUsing[ x ] ) + "; \n" );
			}


			oSB.Append( "namespace " + sNameSpace + " { \n");
			oSB.Append( "public class _Evaluator { \n");

			oSB.Append( "public string Evaluate( "  + sFunctionParams + " ) { \n");

			oSB.Append( "StringBuilder oSB	= new StringBuilder();\n" );
			
			for ( int x = 0; x < sArr.Length; x++ ){
				//sArr[ x ]	= sArr[ x ].Trim();
				if ( sArr[ x ] != "" ){
					if ( sArr[ x ].IndexOf( "%>" ) > -1 ){
					
						string sExecute	= sArr[ x ].Substring( 0 , sArr[ x ].IndexOf( "%>" ) ).Trim();
						if ( sExecute.LastIndexOf( ";" ) == sExecute.Length - 1 )
							sExecute	= sExecute.Substring( 0 , sExecute.Length - 1 );
						
						sArr[ x ]		= sArr[ x ].Substring( sArr[ x ].IndexOf( "%>" ) + 2 );
						//oSB.Append( "oSB.Append( String.Format( \"{0}\" ,  sExecute ) ); " );
						if ( sExecute.IndexOf( "=" ) == 0 )
							oSB.Append( "oSB.Append( \"\" + (" + sExecute.Substring( 1 ) + "));\n" );
						else
							oSB.Append( sExecute + ";\n" );					

					}
					oSB.Append( "oSB.Append( \"" + sArr[ x ].Replace( "\"" , "\\\"" ) + "\"); \n" );
				}
			}
			oSB.Append( "return oSB.ToString(); \n " );
			oSB.Append( "}\n }\n }\n");
			return oSB;
		}

		public static string executeStringBuilder( StringBuilder oSB , string sNameSpace, object []oFunctionParams, string []sRefs ){

			ICodeCompiler oCompiler = (new CSharpCodeProvider().CreateCompiler());
			CompilerParameters oParameters = new CompilerParameters();
			oParameters.ReferencedAssemblies.Add("system.dll");
			oParameters.ReferencedAssemblies.Add("system.data.dll");
			oParameters.ReferencedAssemblies.Add("system.xml.dll");			
			oParameters.ReferencedAssemblies.Add("system.web.dll");
			//oParameters.ReferencedAssemblies.Add( this.GetType().Assembly.Location );

			if ( sRefs != null && sRefs.Length > 0 ){
				for ( int x = 0 ; x < sRefs.Length ; x++ )
					if ( convert.cStr( sRefs[ x ] ) != "" )
						oParameters.ReferencedAssemblies.Add( sRefs[ x ] );

			}
			oParameters.GenerateExecutable	= false;
			oParameters.GenerateInMemory	= true;				
			

			CompilerResults oCompilerResults = oCompiler.CompileAssemblyFromSource(oParameters, oSB.ToString());
			if (oCompilerResults.Errors.HasErrors) {
				StringBuilder oErrors = new StringBuilder();
				oErrors.Append("Error Compiling Expression: ");
				foreach (CompilerError oErr in oCompilerResults.Errors) {
					oErrors.AppendFormat("{0}", oErr.ErrorText );
					if ( oErr.Line > 0 ){
						oErrors.Append( " -- <b>Line: " );
						string []sArr	= parse.split( oSB.ToString() , "\n" );
						oErrors.Append( System.Web.HttpUtility.HtmlEncode( sArr[ oErr.Line - 1 ] ) + "</b>" );
					}
					oErrors.Append( "\n" );
				}
				throw new Exception("Error Compiling Expression: " + oErrors.ToString());
			}

			Assembly oAssembly	= oCompilerResults.CompiledAssembly;
			object oCompiled	= oAssembly.CreateInstance( sNameSpace + "._Evaluator");
			MethodInfo oMI		= oCompiled.GetType().GetMethod( "Evaluate" );			
			
			return convert.cStr( oMI.Invoke( oCompiled , oFunctionParams ));

		}
        private static Dictionary<int, System.Reflection.MethodInfo> customMethods = new Dictionary<int, System.Reflection.MethodInfo>();
        public static object EvaluateFunction(string script, string className, string methodName, object[] parameters){
            lock (customMethods) {
                System.Reflection.MethodInfo method = customMethods.SafeGetValue(script.GetHashCode());
                if (method == null) {
                    CSharpCodeProvider codeProvider = new CSharpCodeProvider();
                    CompilerParameters compilerParameters = new CompilerParameters();
                    compilerParameters.GenerateInMemory = true;
                    compilerParameters.GenerateExecutable = false;
                    compilerParameters.IncludeDebugInformation = true;

                    System.Reflection.Assembly CallingAsembly = System.Reflection.Assembly.GetCallingAssembly();
                    if (CallingAsembly == null) CallingAsembly = System.Reflection.Assembly.GetExecutingAssembly();
                    if (CallingAsembly != null) {
                        string location = Assembly.ReflectionOnlyLoad(CallingAsembly.FullName).Location;
                        compilerParameters.ReferencedAssemblies.Add(location);
                        List<string> referencedAssemblyNames = new List<string>(){System.IO.Path.GetFileName(location)};

                        CallingAsembly.GetReferencedAssemblies().ToList().ForEach(x => {
                            location = Assembly.ReflectionOnlyLoad(x.FullName).Location;
                            if (!referencedAssemblyNames.Contains(System.IO.Path.GetFileName(location))) {
                                compilerParameters.ReferencedAssemblies.Add(location);
                                referencedAssemblyNames.Add(System.IO.Path.GetFileName(location));
                            }

                        });
                    }

                    CompilerResults oCompilerResults = codeProvider.CompileAssemblyFromSource(compilerParameters, script);
                    if (oCompilerResults.Errors.HasErrors) {
                        string error = "";
                        foreach (CompilerError oError in oCompilerResults.Errors) error += "* Line " + oError.Line + ": " + oError.ErrorText + " (" + oError.ErrorNumber + ")\n";                        
                        throw new Exception("Can't evaluate code\n" + error + script);                        
                    }
                    Type type = oCompilerResults.CompiledAssembly.GetTypes().Where(p => p.Name == className).ToList().SafeGetValue(0);
                    if (type == null) throw new Exception("Class " + className + " not found!");

                    method = type.GetMethod(methodName);
                    if(method==null) throw new Exception("Method " + methodName + " not found!");
                    customMethods[script.GetHashCode()] = method;
                }
                return customMethods[script.GetHashCode()].Invoke(null,parameters);
            }            
        }
	}
}
