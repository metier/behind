﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace jlib.functions
{
	public class registry
	{

		#region STATIC PROPERTIES
		public static RegistryKey HKEY_CLASSES_ROOT = Microsoft.Win32.Registry.ClassesRoot;
		public static RegistryKey HKEY_CURRENT_USER = Microsoft.Win32.Registry.CurrentUser;
		public static RegistryKey HKEY_LOCAL_MACHINE = Microsoft.Win32.Registry.LocalMachine;
		public static RegistryKey HKEY_USERS = Microsoft.Win32.Registry.Users;
		public static RegistryKey HKEY_CURRENT_CONFIG = Microsoft.Win32.Registry.CurrentConfig;
		public static RegistryKey HKEY_PERFORMANCE_DATA = Microsoft.Win32.Registry.PerformanceData;
		public static RegistryKey HKEY_DYN_DATA = Microsoft.Win32.Registry.DynData;

		public static RegistryKey HKCR = Microsoft.Win32.Registry.ClassesRoot;
		public static RegistryKey HKCU = Microsoft.Win32.Registry.CurrentUser;
		public static RegistryKey HKLM = Microsoft.Win32.Registry.LocalMachine;
		public static RegistryKey HKU = Microsoft.Win32.Registry.Users;
		public static RegistryKey HKCC = Microsoft.Win32.Registry.CurrentConfig;
		public static RegistryKey HKPD = Microsoft.Win32.Registry.PerformanceData;
		public static RegistryKey HKDD = Microsoft.Win32.Registry.DynData;
		#endregion


		#region STATIC METHODS
		public static RegistryKey GetExecutingAssemblyRegistryKey()
		{
			return GetRegistryKey(registry.HKEY_LOCAL_MACHINE, "Software\\" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
		}
		public static string GetExecutingAssemblyRegistryValue(string Name)
		{
			return GetRegistryValue(registry.HKEY_LOCAL_MACHINE, "Software\\" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, Name);
		}
		public static void SetExecutingAssemblyRegistryValue(string Name, object Value)
		{
			SetRegistryValue(registry.HKEY_LOCAL_MACHINE, "Software\\" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, Name, Value);
		}
		public static string GetRegistryValue(RegistryKey RegistryHive, string RegistryPath, string Name)
		{
			return GetRegistryKey(RegistryHive, RegistryPath).GetValue(Name).ToString();
		}
		public static void SetRegistryValue(RegistryKey RegistryHive, string RegistryPath, string Name, object Value)
		{
			GetRegistryKey(RegistryHive, RegistryPath).SetValue(Name, Value);
		}
		public static RegistryKey GetRegistryKey(RegistryKey RegistryHive, string RegistryPath)
		{
			//Declare Return Registry Key
			RegistryKey RegistryKey = RegistryHive;

			//Split the Registry Path by Folder Breaks
			string[] RegistryPaths = RegistryPath.Split('\\');

			//Create Each Level
			foreach (string sRegistryLevel in RegistryPaths)
			{
				if (sRegistryLevel != "")
					RegistryKey = RegistryKey.CreateSubKey(sRegistryLevel);
			}

			//Return RegistryKey
			return RegistryKey;

		}
		#endregion
	}
}
