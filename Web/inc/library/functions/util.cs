using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Drawing;
using System.Threading;
using System.Text;

namespace jlib.functions
{
	/// <summary>
	/// Summary description for functions
	/// </summary>
	public class util
	{
		#region METHODS
		/// <summary>
		/// Validates a date against the possible SQL storable dates
		/// </summary>
		/// <param name="sDate">Date String</param>
		/// <returns>Boolean representing if the date is valid</returns>

        public static string convertUriToPath(string sUri) {
            if (sUri.StartsWith("file:///"))
                return sUri.Substring(8);

            if (sUri.StartsWith("file://"))
                return sUri.Substring(7);

            if (sUri.StartsWith("file:/"))
                return sUri.Substring(6);

            if (sUri.StartsWith("http:///"))
                return sUri.Substring(8);

            if (sUri.StartsWith("http://"))
                return sUri.Substring(7);

            if (sUri.StartsWith("http:/"))
                return sUri.Substring(6);

            return sUri;

        }

		
		
		/// <summary>
		/// Search threads for a thread that is identified by the given value
		/// </summary>
		/// <param name="oThreads">Thread Array</param>
		/// <param name="sValue">Thread Identifier to search for</param>
		/// <returns>Boolean representing if the thread was found</returns>
		public static bool search_threads(Thread[] oThreads, string sValue)
		{
			int i;
			bool rtnValue		= false;
			
			for( i=0; i<=oThreads.GetUpperBound(0); i++)
			{
				if (oThreads[i] != null)
				{
					if (oThreads[i].Name == sValue)
						rtnValue	= true;
				}
			}

			return rtnValue;
		}


		

		/// <summary>
		/// Search array for specified string
		/// </summary>
		/// <param name="sValues">Array of String Values</param>
		/// <param name="sValue">Value to Search For</param>
		/// <returns>Integer representing if the value was found</returns>
        public static int find_in_array(string[] sValues, string sValue) {
            return find_in_array(sValues, sValue, false);
        }
        public static int find_in_array(string[] sValues, string sValue, bool bPartialMatch) {
            return find_in_array(sValues, sValue, bPartialMatch, false);
        }
		public static int find_in_array(string[] sValues, string sValue, bool bPartialMatch, bool bCaseInsensitive){
            if (sValues == null) return -1;
			for( int i=0; i<=sValues.GetUpperBound(0); i++)
			{
                if (bPartialMatch) {
                    if (convert.cStr(sValues[i]).IndexOf(sValue, (bCaseInsensitive ? StringComparison.CurrentCultureIgnoreCase : StringComparison.CurrentCulture ))>-1)                                                
                        return i;
                } else {
                    if ( (bCaseInsensitive ? convert.cStr(sValues[i]).ToLower() : convert.cStr(sValues[i]) ) == (bCaseInsensitive ? sValue.ToLower() : sValue))
                        return i;
                }
			}
			return -1;
		}

        		
		/// <summary>
		/// Get The Period Representation of a Date
		/// </summary>
		/// <param name="oDate">The Date to represent</param>
		/// <returns>The Period</returns>
        //public static string get_date_period(DateTime oDate)
        //{
        //    DateTime oNow		= DateTime.Now;
        //    int iDayOfWeek		= format.weekDayIndex(oNow.DayOfWeek);
        //    DateTime oDate1		= oNow.AddDays(-iDayOfWeek);
        //    DateTime oDate2		= oNow.AddDays(-iDayOfWeek-7);
        //    DateTime oDate3		= oNow.AddDays(-iDayOfWeek-14);
        //    DateTime oDate4		= oNow.AddDays(-iDayOfWeek-21);
        //    DateTime oDate5		= oNow.AddDays(-iDayOfWeek-28);
        //    DateTime oDate6		= oNow.AddMonths(-1);

        //    if(oDate.Date == DateTime.Now.Date)
        //    {
        //        return "01Today";
        //    }
        //    else if(oDate.Date.AddDays(1) == DateTime.Now.Date)
        //    {
        //        return "02Yesterday";
        //    }
        //    else if(oDate >= oDate1)
        //    {
        //        return "03This Week";
        //    }
        //    else if(oDate >= oDate2 && oNow.Day > 7)
        //    {
        //        return "04Last Week";
        //    }
        //    else if(oDate >= oDate3 && oNow.Day > 14)
        //    {
        //        return "05Two Weeks Ago";
        //    }
        //    else if(oDate >= oDate4 && oNow.Day > 21)
        //    {
        //        return "06Three Weeks Ago";
        //    }
        //    else if(oDate >= oDate5 && oNow.Day > 28)
        //    {
        //        return "07Four Weeks Ago";
        //    }
        //    else if(oDate >= oDate6)
        //    {
        //        return "08Last Month";
        //    }
        //    else
        //    {
        //        return "99Older";
        //    }
        //}


		public static string getCookieValue(string sCookie)
		{
			System.Web.HttpRequest Request		= System.Web.HttpContext.Current.Request;

			if(Request.Cookies[sCookie] != null)
				return Request.Cookies[sCookie].Value;

			return "";

		}

        //public static string getDataTableAsArray(DataTable oDT, string sField, string sSeparator) {
        //    string sValue = "";
        //    for (int x = 0; x < oDT.Rows.Count; x++) {
        //        sValue = sValue + (sValue == "" ? "" : sSeparator) + convert.cStr(oDT.Rows[x][sField]);
        //    }
        //    return sValue;
        //}

		public static double calcDistance(double dLat1, double dLong1, double dLat2, double dLong2) 
		{
			return calcDistance(dLat1, dLong1, dLat2, dLong2, "M");
		}

		public static double calcDistance(double dLat1, double dLong1, double dLat2, double dLong2, string sUnits) 
		{
			double dTheta	= dLong1 - dLong2;
			double dDist	= Math.Sin(convert.deg2rad(dLat1)) * Math.Sin(convert.deg2rad(dLat2)) + Math.Cos(convert.deg2rad(dLat1)) * Math.Cos(convert.deg2rad(dLat2)) * Math.Cos(convert.deg2rad(dTheta));
			
			dDist			= Math.Acos(dDist);
			dDist			= convert.rad2deg(dDist);
			dDist			= dDist * 60 * 1.1515;
			
			switch(sUnits)
			{
				case "M":
					//dDist	= dDist;
					break;

				case "K":
					dDist	= dDist * 1.609344;
					break;

				case "N":
					dDist	= dDist * 0.8684;
					break;
			}
			
			return (dDist);
		}

        //public static int[] getRandomIntegers(int iMinInt, int iMaxInt, int iNumInts) {

        //    if (iMaxInt - iMinInt < 0) return new int[0];
        //    Random oRnd = new Random(System.DateTime.Now.Millisecond * System.DateTime.Now.Second * System.DateTime.Now.Minute);
        //    int[] iVals = new int[iNumInts];
        //    if (iMaxInt - iMinInt < iNumInts) iVals = new int[iMaxInt - iMinInt + 1];
        //    string sUsedIntegers = ",";
        //    for (int x = 0; x < iVals.Length; x++) {
        //        int iRnd = oRnd.Next( iMinInt,iMaxInt+1);
        //        if (sUsedIntegers.IndexOf("," + iRnd + ",") == -1) {
        //            sUsedIntegers += iRnd + ",";
        //            iVals[x] = iRnd;
        //        } else
        //            x--;
        //    }
        //    return iVals;
        //}

		//this function can be greatly optimized by only checking primes (right now we check every number)
		public static int[] integerFactorize(object oNumber) {
			
			StringBuilder oSB = new StringBuilder();
			double iVal = convert.cInt(oNumber);
			while (iVal > 1) {
				for (int x = 2; ; x++) {
					if (x == iVal) {
						oSB.Append("," + x);
						iVal = 1;
					} else if (iVal % x == 0) {
						iVal = iVal / x;
						oSB.Append("," + x);
					} else if (iVal == 1)
						break;
				}
			}
			string[] sVals = parse.split( parse.stripLeadingCharacter( oSB.ToString(), ","), ",");
			int []iVals = new int[sVals.Length];
			for (int x = 0; x < iVals.Length; x++)
				iVals[x] = convert.cInt(sVals[x]);

			return iVals;
		}

		public static int countNumFactorInstances(object oNumber, int iFactorToCheck) {
			int[] iVals = integerFactorize(oNumber);
			int iCounter = 0;
			for (int x = 0; x < iVals.Length; x++)
				if (iVals[x] == iFactorToCheck) iCounter++;

			return iCounter;
		}

		//returns the number of times 
		public static int countNumSquares(object oNumber, int iFactor, out int iRemainder) {
			int iVal = convert.cInt(oNumber);
			int x = 1, iCurrVal = iFactor;
			for (; iCurrVal * iFactor <= iVal; x++) {
				iCurrVal = iCurrVal * iFactor;
			}
			iRemainder = iVal - iCurrVal;
			return x;
		}

        //public static DataTable getApplicationDataTable(string sKey, string sSQL) {
        //    if (System.Web.HttpContext.Current.Application[sKey] as DataTable == null) 
        //        System.Web.HttpContext.Current.Application[sKey] = db.sqlbuilder.getDataTable((sSQL.ToLower().IndexOf("select") == 0 ? sSQL : "select * from " + sSQL + " where deleted = 0"));
			
        //    return System.Web.HttpContext.Current.Application[sKey] as DataTable;
        //}

		#endregion METHODS

		public class OneValueClass : System.IComparable {
			private object m_oValue1 = null;

			public OneValueClass() { }
			public OneValueClass(object oValue1) { m_oValue1=oValue1; }

			public object Value1{
				get{
					return m_oValue1;
				}
				set{
					m_oValue1 = value;
				}
			}
			public int CompareTo(object obj) {
				return convert.cStr(Value1).CompareTo(convert.cStr(obj));
			}
		}

		public class TwoValueClass : OneValueClass {			
			private object m_oValue2 = null;

			public TwoValueClass() { }
			public TwoValueClass(object oValue1, object oValue2) { Value1 = oValue1; m_oValue2 = oValue2; }


			public object Value2 {
				get {
					return m_oValue2;
				}
				set {
					m_oValue2 = value;
				}
			}
			
		}

		public class ThreeValueClass : TwoValueClass {			
			private object m_oValue3 = null;

			public ThreeValueClass() { }
			public ThreeValueClass(object oValue1, object oValue2, object oValue3) { Value1 = oValue1; Value2 = oValue2; m_oValue3 = oValue3; }

			public object Value3 {
				get {
					return m_oValue3;
				}
				set {
					m_oValue3 = value;
				}
			}
		}

		public class FourValueClass : ThreeValueClass {
			private object m_oValue4 = null;

			public FourValueClass() { }
			public FourValueClass(object oValue1, object oValue2, object oValue3, object oValue4) { Value1 = oValue1; Value2 = oValue2; Value3 = oValue3; m_oValue4 = oValue4; }

			public object Value4 {
				get {
					return m_oValue4;
				}
				set {
					m_oValue4 = value;
				}
			}
		}		
    }
}
