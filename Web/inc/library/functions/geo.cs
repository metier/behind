using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Drawing;
using System.Threading;
using System.Text.RegularExpressions;
using System.Xml;

namespace jlib.functions
{
	/// <summary>
	/// Summary description for ado_helper.
	/// </summary>
	public class geo
	{
        public class address {            
            public string Street1, Street2, State, County, Zip, City, CountryName, CountryCode;
            public int Accuracy, StatusCode;
            public address(string sStreet1, string sStreet2, string sState, string sCounty, string sZip, string sCity, string sCountryName, string sCountryCode, int iStatusCode, int iAccuracy) {
                Street1=sStreet1;
                Street2=sStreet2;
                State=sState;
                County=sCounty;
                Zip=sZip;
                City=sCity;
                CountryName=sCountryName;
                CountryCode = sCountryCode;
                StatusCode = iStatusCode;
                Accuracy = iAccuracy;
            }
        }
        //We need to strip out attention and name
        public static address addressLookup(string sAddress) {
            //http://maps.google.com/maps/geo?output=xml&q=120%20whispering%20way,ste%201233,%20atlanta,%20ga&key=ABQIAAAAiq3-lbKfQMkyDYNQ7KmleBSS-WiVSkjOwxcCQ5CjelpEfRIDDBRpQMiEQYb_JTlyRHN8Hni6MnL3Qw
            jlib.net.HTTP oHTTP = new net.HTTP();
            oHTTP.CacheMaxAge = DateTime.Now.AddYears(-20);
            string sXML=oHTTP.Get("http://maps.google.com/maps/geo?output=xml&q=" + System.Web.HttpUtility.UrlEncode(sAddress) + "&key=ABQIAAAAiq3-lbKfQMkyDYNQ7KmleBSS-WiVSkjOwxcCQ5CjelpEfRIDDBRpQMiEQYb_JTlyRHN8Hni6MnL3Qw");
            XmlDocument oDoc=new XmlDocument();
            oDoc.LoadXml(sXML);
            XmlNamespaceManager oNS=xml.createNameSpaceManager(oDoc);
            return new address(xml.getXmlNodeValue(oDoc, "//ThoroughfareName", oNS), "", xml.getXmlNodeValue(oDoc, "//AdministrativeAreaName", oNS), xml.getXmlNodeValue(oDoc, "//SubAdministrativeAreaName", oNS), xml.getXmlNodeValue(oDoc, "//PostalCodeNumber", oNS), xml.getXmlNodeValue(oDoc, "//LocalityName", oNS), xml.getXmlNodeValue(oDoc, "//CountryName", oNS), xml.getXmlNodeValue(oDoc, "//CountryNameCode", oNS), jlib.functions.convert.cInt(xml.getXmlNodeValue(oDoc, "//Status/code", oNS)), jlib.functions.convert.cInt(xml.getXmlAttributeValue(oDoc, "//AddressDetails", "Accuracy", oNS)));
        }
        public class convert {
            /* Ellipsoid model constants (actual values here are for WGS84) */
            static double sm_a = 6378137.0;
            static double sm_b = 6356752.314;
            //static double sm_EccSquared = 6.69437999013e-03;
            static double UTMScaleFactor = 0.9996;


            /*
    * ArcLengthOfMeridian
    *
    * Computes the ellipsoidal distance from the equator to a point at a
    * given latitude.
    *
    * Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
    * GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
    *
    * Inputs:
    *     phi - Latitude of the point, in radians.
    *
    * Globals:
    *     sm_a - Ellipsoid model major axis.
    *     sm_b - Ellipsoid model minor axis.
    *
    * Returns:
    *     The ellipsoidal distance of the point from the equator, in meters.
    *
    */
            public static double ArcLengthOfMeridian( double phi ) {

                double alpha, beta, gamma, delta, epsilon, n;
                double result;

                /* Precalculate n */
                n = ( sm_a - sm_b ) / ( sm_a + sm_b );

                /* Precalculate alpha */
                alpha = ( ( sm_a + sm_b ) / 2.0 ) * ( 1.0 + ( Math.Pow( n, 2.0 ) / 4.0 ) + ( Math.Pow( n, 4.0 ) / 64.0 ) );

                /* Precalculate beta */
                beta = ( -3.0 * n / 2.0 ) + ( 9.0 * Math.Pow( n, 3.0 ) / 16.0 ) + ( -3.0 * Math.Pow( n, 5.0 ) / 32.0 );

                /* Precalculate gamma */
                gamma = ( 15.0 * Math.Pow( n, 2.0 ) / 16.0 ) + ( -15.0 * Math.Pow( n, 4.0 ) / 32.0 );

                /* Precalculate delta */
                delta = ( -35.0 * Math.Pow( n, 3.0 ) / 48.0 ) + ( 105.0 * Math.Pow( n, 5.0 ) / 256.0 );

                /* Precalculate epsilon */
                epsilon = ( 315.0 * Math.Pow( n, 4.0 ) / 512.0 );

                /* Now calculate the sum of the series and return */
                result = alpha * ( phi + ( beta * Math.Sin( 2.0 * phi ) ) + ( gamma * Math.Sin( 4.0 * phi ) ) + ( delta * Math.Sin( 6.0 * phi ) ) + ( epsilon * Math.Sin( 8.0 * phi ) ) );

                return result;
            }

            /*
            * UTMCentralMeridian
            *
            * Determines the central meridian for the given UTM zone.
            *
            * Inputs:
            *     zone - An integer value designating the UTM zone, range [1,60].
            *
            * Returns:
            *   The central meridian for the given UTM zone, in radians, or zero
            *   if the UTM zone parameter is outside the range [1,60].
            *   Range of the central meridian is the radian equivalent of [-177,+177].
            *
            */
            public static double UTMCentralMeridian( int zone ) {
                return jlib.functions.convert.deg2rad( -183.0 + ( zone * 6.0 ) );
            }


            /*
            * FootpointLatitude
            *
            * Computes the footpoint latitude for use in converting transverse
            * Mercator coordinates to ellipsoidal coordinates.
            *
            * Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
            *   GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
            *
            * Inputs:
            *   y - The UTM northing coordinate, in meters.
            *
            * Returns:
            *   The footpoint latitude, in radians.
            *
            */

            public static double FootpointLatitude( double y ) {
                double y_, alpha_, beta_, gamma_, delta_, epsilon_, n;
                double result;

                /* Precalculate n (Eq. 10.18) */
                n = ( sm_a - sm_b ) / ( sm_a + sm_b );

                /* Precalculate alpha_ (Eq. 10.22) */
                /* (Same as alpha in Eq. 10.17) */
                alpha_ = ( ( sm_a + sm_b ) / 2.0 ) * ( 1 + ( Math.Pow( n, 2.0 ) / 4 ) + ( Math.Pow( n, 4.0 ) / 64 ) );

                /* Precalculate y_ (Eq. 10.23) */
                y_ = y / alpha_;

                /* Precalculate beta_ (Eq. 10.22) */
                beta_ = ( 3.0 * n / 2.0 ) + ( -27.0 * Math.Pow( n, 3.0 ) / 32.0 ) + ( 269.0 * Math.Pow( n, 5.0 ) / 512.0 );

                /* Precalculate gamma_ (Eq. 10.22) */
                gamma_ = ( 21.0 * Math.Pow( n, 2.0 ) / 16.0 ) + ( -55.0 * Math.Pow( n, 4.0 ) / 32.0 );

                /* Precalculate delta_ (Eq. 10.22) */
                delta_ = ( 151.0 * Math.Pow( n, 3.0 ) / 96.0 ) + ( -417.0 * Math.Pow( n, 5.0 ) / 128.0 );

                /* Precalculate epsilon_ (Eq. 10.22) */
                epsilon_ = ( 1097.0 * Math.Pow( n, 4.0 ) / 512.0 );

                /* Now calculate the sum of the series (Eq. 10.21) */
                result = y_ + ( beta_ * Math.Sin( 2.0 * y_ ) ) + ( gamma_ * Math.Sin( 4.0 * y_ ) ) + ( delta_ * Math.Sin( 6.0 * y_ ) ) + ( epsilon_ * Math.Sin( 8.0 * y_ ) );

                return result;
            }


            /*
    * MapLatLonToXY
    *
    * Converts a latitude/longitude pair to x and y coordinates in the
    * Transverse Mercator projection.  Note that Transverse Mercator is not
    * the same as UTM; a scale factor is required to convert between them.
    *
    * Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
    * GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
    *
    * Inputs:
    *    phi - Latitude of the point, in radians.
    *    lambda - Longitude of the point, in radians.
    *    lambda0 - Longitude of the central meridian to be used, in radians.
    *
    * Outputs:
    *    xy - A 2-element array containing the x and y coordinates
    *         of the computed point.
    *
    * Returns:
    *    The function does not return a value.
    *
    */

            public static void MapLatLonToXY( double phi, double lambda, double lambda0, out double x, out double y ) {
                double N, nu2, ep2, t, t2, l;
                double l3coef, l4coef, l5coef, l6coef, l7coef, l8coef;
                double tmp;

                /* Precalculate ep2 */
                ep2 = ( Math.Pow( sm_a, 2.0 ) - Math.Pow( sm_b, 2.0 ) ) / Math.Pow( sm_b, 2.0 );

                /* Precalculate nu2 */
                nu2 = ep2 * Math.Pow( Math.Cos( phi ), 2.0 );

                /* Precalculate N */
                N = Math.Pow( sm_a, 2.0 ) / ( sm_b * Math.Sqrt( 1 + nu2 ) );

                /* Precalculate t */
                t = Math.Tan( phi );
                t2 = t * t;
                tmp = ( t2 * t2 * t2 ) - Math.Pow( t, 6.0 );

                /* Precalculate l */
                l = lambda - lambda0;

                /* Precalculate coefficients for l**n in the equations below
                   so a normal human being can read the expressions for easting
                   and northing
                   -- l**1 and l**2 have coefficients of 1.0 */
                l3coef = 1.0 - t2 + nu2;

                l4coef = 5.0 - t2 + 9 * nu2 + 4.0 * ( nu2 * nu2 );

                l5coef = 5.0 - 18.0 * t2 + ( t2 * t2 ) + 14.0 * nu2 - 58.0 * t2 * nu2;

                l6coef = 61.0 - 58.0 * t2 + ( t2 * t2 ) + 270.0 * nu2 - 330.0 * t2 * nu2;

                l7coef = 61.0 - 479.0 * t2 + 179.0 * ( t2 * t2 ) - ( t2 * t2 * t2 );

                l8coef = 1385.0 - 3111.0 * t2 + 543.0 * ( t2 * t2 ) - ( t2 * t2 * t2 );

                /* Calculate easting (x) */
                x = N * Math.Cos( phi ) * l
                    + ( N / 6.0 * Math.Pow( Math.Cos( phi ), 3.0 ) * l3coef * Math.Pow( l, 3.0 ) )
                    + ( N / 120.0 * Math.Pow( Math.Cos( phi ), 5.0 ) * l5coef * Math.Pow( l, 5.0 ) )
                    + ( N / 5040.0 * Math.Pow( Math.Cos( phi ), 7.0 ) * l7coef * Math.Pow( l, 7.0 ) );

                /* Calculate northing (y) */
                y = ArcLengthOfMeridian( phi )
                    + ( t / 2.0 * N * Math.Pow( Math.Cos( phi ), 2.0 ) * Math.Pow( l, 2.0 ) )
                    + ( t / 24.0 * N * Math.Pow( Math.Cos( phi ), 4.0 ) * l4coef * Math.Pow( l, 4.0 ) )
                    + ( t / 720.0 * N * Math.Pow( Math.Cos( phi ), 6.0 ) * l6coef * Math.Pow( l, 6.0 ) )
                    + ( t / 40320.0 * N * Math.Pow( Math.Cos( phi ), 8.0 ) * l8coef * Math.Pow( l, 8.0 ) );                
            }


            /*
            * MapXYToLatLon
            *
            * Converts x and y coordinates in the Transverse Mercator projection to
            * a latitude/longitude pair.  Note that Transverse Mercator is not
            * the same as UTM; a scale factor is required to convert between them.
            *
            * Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
            *   GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
            *
            * Inputs:
            *   x - The easting of the point, in meters.
            *   y - The northing of the point, in meters.
            *   lambda0 - Longitude of the central meridian to be used, in radians.
            *
            * Outputs:
            *   philambda - A 2-element containing the latitude and longitude
            *               in radians.
            *
            * Returns:
            *   The function does not return a value.
            *
            * Remarks:
            *   The local variables Nf, nuf2, tf, and tf2 serve the same purpose as
            *   N, nu2, t, and t2 in MapLatLonToXY, but they are computed with respect
            *   to the footpoint latitude phif.
            *
            *   x1frac, x2frac, x2poly, x3poly, etc. are to enhance readability and
            *   to optimize computations.
            *
            */
            public static void MapXYToLatLon( double x, double y, double lambda0, out double lat, out double lon) {
                double phif, Nf, Nfpow, nuf2, ep2, tf, tf2, tf4, cf;
                double x1frac, x2frac, x3frac, x4frac, x5frac, x6frac, x7frac, x8frac;
                double x2poly, x3poly, x4poly, x5poly, x6poly, x7poly, x8poly;

                /* Get the value of phif, the footpoint latitude. */
                phif = FootpointLatitude( y );

                /* Precalculate ep2 */
                ep2 = ( Math.Pow( sm_a, 2.0 ) - Math.Pow( sm_b, 2.0 ) )
                      / Math.Pow( sm_b, 2.0 );

                /* Precalculate cos (phif) */
                cf = Math.Cos( phif );

                /* Precalculate nuf2 */
                nuf2 = ep2 * Math.Pow( cf, 2.0 );

                /* Precalculate Nf and initialize Nfpow */
                Nf = Math.Pow( sm_a, 2.0 ) / ( sm_b * Math.Sqrt( 1 + nuf2 ) );
                Nfpow = Nf;

                /* Precalculate tf */
                tf = Math.Tan( phif );
                tf2 = tf * tf;
                tf4 = tf2 * tf2;

                /* Precalculate fractional coefficients for x**n in the equations
                   below to simplify the expressions for latitude and longitude. */
                x1frac = 1.0 / ( Nfpow * cf );

                Nfpow *= Nf;   /* now equals Nf**2) */
                x2frac = tf / ( 2.0 * Nfpow );

                Nfpow *= Nf;   /* now equals Nf**3) */
                x3frac = 1.0 / ( 6.0 * Nfpow * cf );

                Nfpow *= Nf;   /* now equals Nf**4) */
                x4frac = tf / ( 24.0 * Nfpow );

                Nfpow *= Nf;   /* now equals Nf**5) */
                x5frac = 1.0 / ( 120.0 * Nfpow * cf );

                Nfpow *= Nf;   /* now equals Nf**6) */
                x6frac = tf / ( 720.0 * Nfpow );

                Nfpow *= Nf;   /* now equals Nf**7) */
                x7frac = 1.0 / ( 5040.0 * Nfpow * cf );

                Nfpow *= Nf;   /* now equals Nf**8) */
                x8frac = tf / ( 40320.0 * Nfpow );

                /* Precalculate polynomial coefficients for x**n.
                   -- x**1 does not have a polynomial coefficient. */
                x2poly = -1.0 - nuf2;

                x3poly = -1.0 - 2 * tf2 - nuf2;

                x4poly = 5.0 + 3.0 * tf2 + 6.0 * nuf2 - 6.0 * tf2 * nuf2
                    - 3.0 * ( nuf2 * nuf2 ) - 9.0 * tf2 * ( nuf2 * nuf2 );

                x5poly = 5.0 + 28.0 * tf2 + 24.0 * tf4 + 6.0 * nuf2 + 8.0 * tf2 * nuf2;

                x6poly = -61.0 - 90.0 * tf2 - 45.0 * tf4 - 107.0 * nuf2
                    + 162.0 * tf2 * nuf2;

                x7poly = -61.0 - 662.0 * tf2 - 1320.0 * tf4 - 720.0 * ( tf4 * tf2 );

                x8poly = 1385.0 + 3633.0 * tf2 + 4095.0 * tf4 + 1575 * ( tf4 * tf2 );

                /* Calculate latitude */
                lat = phif + x2frac * x2poly * ( x * x )
                    + x4frac * x4poly * Math.Pow( x, 4.0 )
                    + x6frac * x6poly * Math.Pow( x, 6.0 )
                    + x8frac * x8poly * Math.Pow( x, 8.0 );

                /* Calculate longitude */
                lon = lambda0 + x1frac * x
                    + x3frac * x3poly * Math.Pow( x, 3.0 )
                    + x5frac * x5poly * Math.Pow( x, 5.0 )
                    + x7frac * x7poly * Math.Pow( x, 7.0 );                
            }




            /*
            * LatLonToUTMXY
            *
            * Converts a latitude/longitude pair to x and y coordinates in the
            * Universal Transverse Mercator projection.
            *
            * Inputs:
            *   lat - Latitude of the point, in radians.
            *   lon - Longitude of the point, in radians.
            *   zone - UTM zone to be used for calculating values for x and y.
            *          If zone is less than 1 or greater than 60, the routine
            *          will determine the appropriate zone from the value of lon.
            *
            * Outputs:
            *   xy - A 2-element array where the UTM x and y values will be stored.
            *
            * Returns:
            *   The UTM zone used for calculating the values of x and y.
            *
            */
            public static void LatLonToUTM( double oLat, double oLon, out int iZone, out double oEasting, out double oNorthing ) {

                iZone = (int)Math.Floor( ( oLon + 180.0 ) / 6 ) + 1;
                MapLatLonToXY( jlib.functions.convert.deg2rad(oLat), jlib.functions.convert.deg2rad(oLon), UTMCentralMeridian( iZone ), out oEasting, out oNorthing );

                /* Adjust easting and northing for UTM system. */
                oEasting = oEasting * UTMScaleFactor + 500000.0;
                oNorthing = oNorthing * UTMScaleFactor;
                if ( oNorthing < 0.0 )
                    oNorthing = oNorthing + 10000000.0;                
            }



            /*
            * UTMXYToLatLon
            *
            * Converts x and y coordinates in the Universal Transverse Mercator
            * projection to a latitude/longitude pair.
            *
            * Inputs:
            *	x - The easting of the point, in meters.
            *	y - The northing of the point, in meters.
            *	zone - The UTM zone in which the point lies.
            *	southhemi - True if the point is in the southern hemisphere;
            *               false otherwise.
            *
            * Outputs:
            *	latlon - A 2-element array containing the latitude and
            *            longitude of the point, in radians.
            *
            * Returns:
            *	The function does not return a value.
            *
            */
            public static void UTMToLatLon( double oEasting, double oNorthing, int iUTMZone, bool bSouthHemisphere, out double oLat, out double oLon ) {

                oEasting -= 500000.0;
                oEasting /= UTMScaleFactor;

                /* If in southern hemisphere, adjust y accordingly. */
                if ( bSouthHemisphere )
                    oNorthing -= 10000000.0;

                oNorthing /= UTMScaleFactor;

                MapXYToLatLon( oEasting, oNorthing, UTMCentralMeridian( iUTMZone ), out oLat, out oLon );
                oLat = jlib.functions.convert.rad2deg( oLat );
                oLon = jlib.functions.convert.rad2deg( oLon );
            }
            public static double getDistanceMiles(object oLat1, object oLon1, object oLat2, object oLon2) {
                return getDistanceMiles(oLat1, oLon1, oLat2, oLon2) / 1.6122692113245702730030333670374;
            }
            public static double getDistanceKM(object oLat1, object oLon1, object oLat2, object oLon2) {

				//For accurate calculator, see: http://www.nhc.noaa.gov/gccalc.shtml
				//For code-example in C# and T-SQL, see http://www.codeproject.com/csharp/distancebetweenlocations.asp?df=100&forumid=245892&exp=0&select=1308949#xx1308949xx

                double dLat1 = jlib.functions.convert.cDbl( oLat1 );
                double dLon1 = jlib.functions.convert.cDbl( oLon1 );
                double dLat2 = jlib.functions.convert.cDbl( oLat2 );
                double dLon2 = jlib.functions.convert.cDbl( oLon2 );                    
                
				//double dTmp1 = Math.Sin( dLat1*Math.PI/180) * Math.Sin( dLat2*Math.PI/180) + Math.Cos(dLat1*Math.PI/180)*Math.Cos(dLat2*Math.PI/180)*Math.Cos(Math.Abs((dLon2*Math.PI/180)-(dLon1*Math.PI/180)));
				//if( dTmp1 ==  0 || dTmp1 > 1 ) return 99999;
				//dTmp1 = Math.Atan((Math.Sqrt(1-(dTmp1*dTmp1))) / dTmp1);
				//return Math.Abs( 1.852*60*((dTmp1/Math.PI)*180));                


				/*
					The Haversine formula according to Dr. Math.
					http://mathforum.org/library/drmath/view/51879.html
                
					dlon = lon2 - lon1
					dlat = lat2 - lat1
					a = (sin(dlat/2))^2 + cos(lat1) * cos(lat2) * (sin(dlon/2))^2
					c = 2 * atan2(sqrt(a), sqrt(1-a)) 
					d = R * c
                
					Where
						* dlon is the change in longitude
						* dlat is the change in latitude
						* c is the great circle distance in Radians.
						* R is the radius of a spherical Earth.
						* The locations of the two points in 
							spherical coordinates (longitude and 
							latitude) are lon1,dLat1 and lon2, lat2.
				*/
				double dDistance = Double.MinValue;
				double dLat1InRad = dLat1 * (Math.PI / 180.0);
				double dLong1InRad = dLon1 * (Math.PI / 180.0);
				double dLat2InRad = dLat2 * (Math.PI / 180.0);
				double dLong2InRad = dLon2 * (Math.PI / 180.0);

				double dLongitude = dLong2InRad - dLong1InRad;
				double dLatitude = dLat2InRad - dLat1InRad;

				// Intermediate result a.
				double a = Math.Pow(Math.Sin(dLatitude / 2.0), 2.0) +
						   Math.Cos(dLat1InRad) * Math.Cos(dLat2InRad) *
						   Math.Pow(Math.Sin(dLongitude / 2.0), 2.0);

				// Intermediate result c (great circle distance in Radians).
				double c = 2.0 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1.0 - a));

				// Distance.
				// const Double kEarthRadiusMiles = 3956.0;
				const Double kEarthRadiusKms = 6378.137; // 6376.5;
				dDistance = kEarthRadiusKms * c;

				return dDistance;
            }

            public static double getDrivingDistanceKM(object oLat1, object oLon1, object oLat2, object oLon2) {
                return getDistanceKM(oLat1, oLon1, oLat2, oLon2) * 1.375;
            }
        }

		public static void getPoint(double dLat, double dLon, double dDistance, double dAngle, out double dNewLat, out double dNewLon) {
			double b = dDistance / 6371;
			double a = Math.Acos(Math.Cos(b) * Math.Cos( jlib.functions.convert.deg2rad( 90 - dLat)) + Math.Sin(jlib.functions.convert.deg2rad( 90 - dLat)) * Math.Sin(b) * Math.Cos(jlib.functions.convert.deg2rad( dAngle )));
			double B = Math.Asin(Math.Sin(b)*Math.Sin(jlib.functions.convert.deg2rad(dAngle))/Math.Sin(a));
			dNewLat = jlib.functions.convert.rad2deg( (Math.PI / 2) - a);
			dNewLon = jlib.functions.convert.rad2deg( B ) + dLon;
		}

		public static string getBoundingBoxSQL(double dLat, double dLon, double dRadius, string sLatFieldName, string sLonFieldName) {
			if (dRadius > 18000) return " 1=1 ";
			System.Text.StringBuilder oSB = new System.Text.StringBuilder();
			double dE = 0, dN = 0, dW = 0, dS = 0;
			double d = 0;

			getPoint(dLat, dLon, dRadius, 0, out dN, out d);
			getPoint(dLat, dLon, dRadius, 180, out dS, out d);
			getPoint(dLat, dLon, dRadius, 90, out d, out dE);
			getPoint(dLat, dLon, dRadius, 270, out d, out dW);
			oSB.Append(sLatFieldName + " between " + dS + " and " + dN);
			oSB.Append(" AND ((" + sLonFieldName + " between " + dW + " and " + dE + ")" + (dW < -180 ? " OR ( " + sLonFieldName + " between " + (360 + dW) + " AND 180)" : "") + (dE > 180 ? " OR ( " + sLonFieldName + " between -180 AND " + (dE - 360 ) + ")" : "") + ")");
			
			return oSB.ToString();
		}

		public static double getHeading(double dLat1, double dLon1, double dLat2, double dLon2) {

			dLat1 = jlib.functions.convert.deg2rad(dLat1);
			dLat2 = jlib.functions.convert.deg2rad(dLat2);


			double dLon = jlib.functions.convert.deg2rad(dLon2 - dLon1);

			double y = Math.Sin(dLon) * Math.Cos(dLat2);
			double x = Math.Cos(dLat1) * Math.Sin(dLat2) -
					Math.Sin(dLat1) * Math.Cos(dLat2) * Math.Cos(dLon);
			return (jlib.functions.convert.rad2deg(Math.Atan2(y, x)) + 360) % 360;
			
		}
		public static string getHeadingName(double dHeading, bool bMinorTicks) {
			string []sHeadings = new string[]{"N","�","S","V"};
			string[] sMHeadings = new string[] { "N","N�", "�","S�", "S","SV", "V","NV" };
			if (dHeading >= 360) dHeading = dHeading % 360;
			if (!bMinorTicks) return sHeadings[jlib.functions.convert.cInt(dHeading / 90)];
			return sMHeadings[jlib.functions.convert.cInt(dHeading / 45)];
		}
	}
}
