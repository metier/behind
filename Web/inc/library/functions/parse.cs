using System;
using System.Threading;
using System.Text.RegularExpressions;
using System.Collections;
using System.Text;
using System.Xml;
using System.IO;
using System.Collections.Generic;

namespace jlib.functions
{
	/// <summary>
	/// Summary description for Parse.
	/// </summary>
	public class parse {
		#region CONSCTRUCTORS

		/// <summary>
		/// Creates an instance of the parsing class
		/// </summary>
		public parse() {
		}
		#endregion CONSTRUCTORS

		#region METHODS
		

		/// <summary>
		/// Joins an object array into single string
		/// </summary>
		/// <param name="Data">The object array to join</param>
		/// <param name="separator">The deliminator to join the string array into a string</param>
		/// <returns>The joined string</returns>
		public static string join( object[] Data, string separator) {
			if (Data == null) return "";
			string rtnValue="";
			int i;
			
			for (i=0; i<Data.Length; i++) {
				if (i!=0)
					rtnValue += separator;
				rtnValue += convert.cStr(Data[i]);
			}

			return rtnValue;

		}

		/// <summary>
		/// A method to resize a string array
		/// </summary>
		/// <param name="sData">The string array</param>
		/// <param name="iSize">The new size of the array</param>
		/// <returns>The resized array</returns>
		public static string[] resize(string[] sData, int iSize) {
            if (sData.Length == iSize) return sData;
			int i;

			string[] sTemp		= new string[iSize+1];
			
			for( i=0; i<=iSize; i++) {
				if (i>sData.GetUpperBound(0)) {
					sTemp[i]		= "";
				}
				else {
					sTemp[i]		= sData[i];
				}

			}

			return sTemp;
		}

		/// <summary>
		/// Searches a string array for a match
		/// </summary>
		/// <param name="sData">The string array to search</param>
		/// <param name="sValue">The value to search for</param>
		/// <returns>A boolean that reports if the string was found</returns>
		public static bool search(string[] sData, string sValue) {
            return search(sData, 0, sValue);            
		}

        public static bool search(string[] sData, int iStartIndex, string sValue) {
            return search_index(sData, 0, sValue) > -1;            
        }

        public static int search_index(string[] sData, int iStartIndex, string sValue) {            

            for (int i = iStartIndex; i <= sData.GetUpperBound(0); i++) {
                if (sData[i] == sValue)
                    return i;
            }
            return -1;
        }
        
		/// <summary>
		/// Counts the number of occurences of a substring in a string
		/// </summary>
		/// <param name="Data">The original string data</param>
		/// <param name="separator">The substring</param>
		/// <param name="bCaseSensitive">Flag whether this should be case sensitive</param>
		/// <returns>A string array of the results fromt he split</returns>
		public static int numOccurences(string Data, string separator, bool bCaseSensitive)
		{
			int iCounter=0;
			
			Data = (bCaseSensitive ? Data : Data.ToLower());
			separator = (bCaseSensitive ? separator : separator.ToLower());

			for (int x=0;x<Data.Length;){
				x=Data.IndexOf(separator,x);				
				
				if (x<0)
					break;

				x++;
				iCounter++;
			}
			return iCounter;
		}


		public static int splitIndex(string sData, string sValue) {
			return splitIndex(sData, ",", sValue);
		}

		public static int splitIndex(string sData, string sSeparator, string sValue) {
			string[] oValues	= split(sData, sSeparator);
			int iIndex			= util.find_in_array(oValues, sValue);

			return iIndex;
		}


		public static string splitValue(object oData, int iIndex) {
			return splitValue(oData, ",", iIndex);
		}

		public static string splitValue(object oData, string sSeparator, int iIndex) {
			string[] oValues	= split(oData, sSeparator);
			
			try {
				return oValues[(iIndex<0 ? oValues.Length + iIndex : iIndex)];
			}
			catch(Exception) {
				return "";
			}			
		}

		/// <summary>
		/// Splits a string by a string deliminator
		/// </summary>
		/// <param name="oData">The original string data</param>
		/// <param name="separator">The deliminator to split</param>
		/// <returns>A string array of the results fromt he split</returns>
        public static IList createList<T>(object []oArr) {
            //System.Collections.Generic.List<T> oList = new System.Collections.Generic.List<T>();
            Type oType = typeof(T);
            System.Collections.IList oList=null;
            if( oType==typeof(string))
                oList=new System.Collections.Generic.List<string>();
            else if (oType == typeof(int))
                oList = new System.Collections.Generic.List<int>();
            else if (oType == typeof(double))
                oList = new System.Collections.Generic.List<double>();
            else if (oType == typeof(bool))
                oList = new System.Collections.Generic.List<bool>();
            else if (oType == typeof(DateTime))
                oList = new System.Collections.Generic.List<DateTime>();

            for (int x = 0; x < oArr.Length; x++) {                
                oList.Add(convertSafeValue(oType, oArr[x]));                
            }
            return oList;// oList;
		}

        public static object convertSafeValue(Type oType, object oValue) {
                       
            if (oType == typeof(string)) 
                return convert.cStr(oValue);
             else if (oType == typeof(int))
                return jlib.functions.convert.cInt(oValue);
            else if ( oType == typeof(double)) {
                return jlib.functions.convert.cDbl(oValue);
            } else if (oType == typeof(bool)) {
                return convert.cBool(oValue);
            } else if (oType == typeof(DateTime) && jlib.functions.convert.cStr(oValue) == "") {
                return null;
            } else if (oType == typeof(DateTime)) {
                return jlib.functions.convert.cDate(oValue);            
            } else if (oType == typeof(byte)) {
                return jlib.functions.convert.cInt(oValue);
            } else {
                return oValue;
            }
        }
        public static List<KeyValuePair<string, string>> splitList(object oData, string sLineSep, string sColSep) {
            List<KeyValuePair<string, string>> oList = new List<KeyValuePair<string, string>>();
            string[] sArr = split(oData, sLineSep);
            for (int x = 0; x < sArr.Length; x++) 
                if (sArr[x].Trim() != "") oList.Add(new KeyValuePair<string, string>(sArr[x].IndexOf(sColSep) == -1 ? sArr[x] : sArr[x].Substring(0, sArr[x].IndexOf(sColSep)), sArr[x].IndexOf(sColSep) == -1 ? "": sArr[x].Substring(sArr[x].IndexOf(sColSep)+1) ));            
            return oList;
        }
        public static string[] split(object oData, string separator) {            
            return split( oData, separator, false );
		}
        public static string split( object oData, string separator, int iIndex ) {
            string[] sArr = split( oData, separator, false );
            return ( sArr.Length > iIndex ? sArr[iIndex] : "" );
        }
        public static string[] split( object oData, string separator, bool bIncludeToken ) {
			return split( oData, separator, bIncludeToken, true );
		}

		/// <summary>
		/// Splits a string by a string deliminator (CASE INSENSITIVE)
		/// </summary>
		/// <param name="Data">The original string data</param>
		/// <param name="separator">The deliminator to split</param>
		/// <returns>A string array of the results fromt he split</returns>
        public static string[] split_i( object oData, string separator ) {
            return split( oData, separator, false, false );
		}

        public static string[] split(object oData, string sSeparator, bool bIncludeToken, bool bCaseSensitive) {

            string sData = convert.cStr(oData);
            if (sData == "") return new string[] { };
            if (string.IsNullOrEmpty(sSeparator) || sData.IndexOf(sSeparator, bCaseSensitive ? StringComparison.CurrentCulture : StringComparison.CurrentCultureIgnoreCase) == -1) return new string[] { sData };
            if (!bIncludeToken && bCaseSensitive) return sData.Split(new string[] { sSeparator }, StringSplitOptions.None);

            int pos = 0;
            int last_pos = 0;
            
            System.Collections.Generic.List<string> oReturn = new System.Collections.Generic.List<string>(20001);            
            
            string sTemp = sData;
            if (!bCaseSensitive) {
                sTemp = sTemp.ToLower();
                sSeparator = sSeparator.ToLower();
            }

            pos = sTemp.IndexOf(sSeparator, pos);
            string s = "";
            while (pos > -1 && oReturn.Count < 20000) {                
                oReturn.Add(s + sData.Substring(last_pos, pos - last_pos));
                                               
                if (bIncludeToken && sSeparator == sTemp.Substring((pos - sSeparator.Length < 0 ? 0 : pos), sSeparator.Length))
                    s = sData.Substring((pos - sSeparator.Length < 0 ? 0 : pos), sSeparator.Length);

                last_pos = pos + sSeparator.Length;
                pos = sTemp.IndexOf(sSeparator, last_pos);
            }

            if (last_pos < sData.Length) oReturn.Add(s + sData.Substring(last_pos));
            return oReturn.ToArray(); 
        }

		public static string stripTags( string sData, string sStartTag, string sEndTag ){

			string []sArr	= split_i( sData.Trim() , sStartTag );
			if ( sArr.Length <= 1 ) return sData;

			string sResult	= sArr[ 0 ];
			
			if ( sResult.IndexOf( sStartTag ) == 0 )
				sResult		= sResult.Substring( sResult.IndexOf( sEndTag ) + sEndTag.Length );

			for ( int x = 1 ; x < sArr.Length; x++ ) {
				if ( sArr[ x ].IndexOf( sEndTag ) > -1 ){
					sResult += sArr[ x ].Substring(  sArr[ x ].IndexOf( sEndTag ) +  sEndTag.Length );
				}else{
					sResult += sArr[ x ];
				}

			}

			return sResult.Trim(); 
		}

        public static string findSeparator(object oData, params string[] sSeparators) {
            string sSeparator = "", sData = convert.cStr(oData);
            int iSeparatorIndex = sData.Length+1;
            for (int x = 0; x < sSeparators.Length; x++) {
                if (sData.IndexOf(sSeparators[x]) > -1 && sData.IndexOf(sSeparators[x]) < iSeparatorIndex) {
                    sSeparator = sSeparators[x];
                    iSeparatorIndex=sData.IndexOf(sSeparators[x]);
                }
            }
            return sSeparator;
        }

		
		/// <summary>
		/// Trims all leading and trailing whitechars from the string
		/// </summary>
		/// <param name="Data">The original string data</param>		
		/// <returns>A clenaed string </returns>
		public static string stripWhiteChars( object oData ) {
		
			string sData	= convert.cStr( oData );

            while (sData.StartsWith(" ") || sData.StartsWith("�") || sData.StartsWith("\t") || sData.StartsWith("\n") || sData.StartsWith("\r"))
					sData	= sData.Substring( 1 );

			while ( sData.EndsWith( " " ) || sData.EndsWith( "�" ) || sData.EndsWith( "\t" ) || sData.EndsWith( "\n" ) || sData.EndsWith( "\r" ))
					sData	= sData.Substring( 0, sData.Length - 1  );
					
			return sData; 
		}

        /// <summary>
        /// Strips all HTML formatting, except sTags
        /// </summary>
        /// <param name="oData">The original HTML string</param>		
        /// <param name="bStripStyleAttribute">Flag indicating whether style attribute should be removed for those tags that are not cleared</param>		
        /// <param name="sTags">List of tags to keep</param>		
        /// <returns>A clenaed string </returns>
        public static string stripXHTML(object oData, bool bStripStyleAttribute, List<string> oTags) {
            //return parse.stripHTML(oData, true);
            if (!oData.Str().Contains("<")) return oData.Str();
            XmlDocument oDoc = jlib.net.sgmlparser.SgmlReader.getXmlDoc("<html>" + oData.Str() + "</html>");
            XmlNodeList xmlNodes = oDoc.SelectNodes("//*");
            for (int x = 0; x < xmlNodes.Count; x++) {
                if (xmlNodes[x].Name != "#text" && xmlNodes[x] != oDoc.DocumentElement) {
                    if (!oTags.Contains(xmlNodes[x].Name)) {
                        while (xmlNodes[x].ChildNodes.Count > 0) 
                            xmlNodes[x].ParentNode.InsertBefore(xmlNodes[x].ChildNodes[0], xmlNodes[x]);
                                                    
                        xmlNodes[x].ParentNode.RemoveChild(xmlNodes[x]);
                    } else if (bStripStyleAttribute) {
                        if (xmlNodes[x].Attributes["style"] != null) xmlNodes[x].Attributes.RemoveNamedItem("style");
                        if (xmlNodes[x].Attributes["class"] != null) xmlNodes[x].Attributes.RemoveNamedItem("class");
                    }
                }
            }
            return oDoc.DocumentElement.InnerXml;            
        }

		/// <summary>
		/// Strips all HTML formatting from the string
		/// </summary>
		/// <param name="Data">The original string data</param>		
		/// <returns>A clenaed string </returns>
		public static string stripHTML( object oData ) {                        
			//string sResult	= stripTags( convert.cStr( oData ), "<" , ">" );
            string sResult = Regex.Replace(convert.cStr(oData), @"(?></?\w+)(?>(?:[^>'""]+|'[^']*'|""[^""]*"")*)>", String.Empty);
			//sResult			= stripTags( sResult, "&lt;settings&gt;" , "&lt;/settings&gt;" );
			//sResult			= stripTags( sResult, "&lt;preview&gt;" , "&lt;/preview&gt;" );
			sResult			= parse.replaceAll( sResult, "&nbsp;" , " " );
						
			return sResult.Trim(); 
		}
        public static string escapeHTMLCharacters(object oData) {
            return parse.replaceAll(oData, "�", "&#8364;", "�", "&#8218;", "�", "&#402;", "�", "&#8222;", "�", "&#8230;", "�", "&#8224;", "�", "&#8225;", "�", "&#710;", "�", "&#8240;", "�", "&#352;", "�", "&#8249;", "�", "&#338;", "�", "&#381;", "�", "&#8216;", "�", "&#8217;", "�", "&#8220;", "�", "&#8221;", "�", "&#8226;", "�", "&#8211;", "�", "&#8212;", "�", "&#732;", "�", "&#8482;", "�", "&#353;", "�", "&#8250;", "�", "&#339;", "�", "&#382;", "�", "&#376;", "�", "&#161;", "�", "&#162;", "�", "&#163;", "�", "&#164;", "�", "&#165;", "�", "&#166;", "�", "&#167;", "�", "&#168;", "�", "&#169;", "�", "&#170;", "�", "&#171;", "�", "&#172;", "", "&#173;", "�", "&#174;", "�", "&#175;", "�", "&#176;", "�", "&#177;", "�", "&#178;", "�", "&#179;", "�", "&#180;", "�", "&#181;", "�", "&#182;", "�", "&#183;", "�", "&#184;", "�", "&#185;", "�", "&#186;", "�", "&#187;", "�", "&#188;", "�", "&#189;", "�", "&#190;", "�", "&#191;", "�", "&#192;", "�", "&#193;", "�", "&#194;", "�", "&#195;", "�", "&#196;", "�", "&#197;", "�", "&#198;", "�", "&#199;", "�", "&#200;", "�", "&#201;", "�", "&#202;", "�", "&#203;", "�", "&#204;", "�", "&#205;", "�", "&#206;", "�", "&#207;", "�", "&#208;", "�", "&#209;", "�", "&#210;", "�", "&#211;", "�", "&#212;", "�", "&#213;", "�", "&#214;", "�", "&#215;", "�", "&#216;", "�", "&#217;", "�", "&#218;", "�", "&#219;", "�", "&#220;", "�", "&#221;", "�", "&#222;", "�", "&#223;", "�", "&#224;", "�", "&#225;", "�", "&#226;", "�", "&#227;", "�", "&#228;", "�", "&#229;", "�", "&#230;", "�", "&#231;", "�", "&#232;", "�", "&#233;", "�", "&#234;", "�", "&#235;", "�", "&#236;", "�", "&#237;", "�", "&#238;", "�", "&#239;", "�", "&#240;", "�", "&#241;", "�", "&#242;", "�", "&#243;", "�", "&#244;", "�", "&#245;", "�", "&#246;", "�", "&#247;", "�", "&#248;", "�", "&#249;", "�", "&#250;", "�", "&#251;", "�", "&#252;", "�", "&#253;", "�", "&#254;", "�", "&#255;");
        }

		/// <summary>
		/// Strips all HTML formatting from the string
		/// </summary>
		/// <param name="oData">The original string data</param>		
		/// <param name="bPreserveNewline">Flag indicating if newlines should be preserved</param>		
		/// <returns>A clenaed string </returns>
		public static string stripHTML( object oData, bool bPreserveNewline ) {
            string sData = convert.cStr( oData );
            if ( bPreserveNewline ){
				sData		= parse.replaceAll( sData, "\n" , "" , "\r" , "" );
				sData		= parse.replaceAll( sData, false, "<br>" , "\n" , "<br/>" , "\n" , "<br />" , "\n", "</p>", "\n");
				sData		= parse.stripLeadingCharacter( sData, "\n" );
				sData		= parse.stripEndingCharacter( sData, "\n" );
				 
			}
			return stripHTML( sData );
		}

        public static string stripSCRIPT(object oData) {
            return new System.Text.RegularExpressions.Regex(@"<script[\s]*[^>]*>.*?</script>", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline).Replace(convert.cStr(oData), "");
        }
        public static string extractSCRIPT(object oData, bool bStripScriptTags) {
            string sData = "";
            MatchCollection oMatches = new System.Text.RegularExpressions.Regex(@"<script[\s]*[^>]*>.*?</script>", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline).Matches(convert.cStr(oData));
            for (int x = 0; x < oMatches.Count; x++) sData += parse.inner_substring_i( oMatches[x].Value, "<script", ">", null, "</script>");
            return sData;
        }

		public static string sanitizeHTML(object oHTML, bool bConvertNewlines) {
			string sHTML = convert.cStr(oHTML).Trim();
			string sAllowedTags = ",html,b,i,img,a,br,hr,strike,s,sub,h1,h2,h3,h4,li,ul,u,em,ol,strong,sup,p,";
			string sAllowedAttributes = ",src,href,width,height,size,length,alt,hspace,vspace,";
			try {
				string[] sArr = parse.split(sHTML, "http://", true, false);
				for (int x = 0; x < sArr.Length; x++) {
					string sUrl = "";
					if (sArr[x].ToLower().IndexOf("http://") == 0) {
						sUrl = parse.replaceAll( parse.inner_substring(sArr[x], null, null, " ", null) , ",", "", ")", "", "(", "" ).Trim();
						if (sUrl.EndsWith(".")) sUrl = sUrl.Substring(0, sUrl.Length - 1);						
					}
					if (sUrl != "") {
						if (x == 0 || sArr[x - 1].EndsWith(" "))
							sArr[x] = "<a href=\"" + sUrl + "\">" + sUrl + "</a>" + sArr[x].Substring(sUrl.Length);

					}
				}
				sHTML = (bConvertNewlines ? parse.replaceAll(parse.join(sArr, "") ,"\n<br>", "\n", "\n<br />", "\n", "\n<br/>", "\n", "<br>\n", "\n", "<br />\n", "\n", "<br/>\n", "\n", "\n", "<br />") : parse.join(sArr, ""));

				jlib.net.sgmlparser.SgmlReader oReader = new jlib.net.sgmlparser.SgmlReader();
				if (sHTML.ToLower().IndexOf("<html") == -1) sHTML = "<html>" + sHTML + "</html>";
				if (sHTML.ToLower().IndexOf("<html") > 0) sHTML = "<html" + parse.inner_substring(sHTML, "<html", null, null, null);
				StringReader oSReader = new StringReader(sHTML);
				oReader.InputStream = oSReader;

				oReader.DocType = "HTML";
				oReader.CaseFolding = jlib.net.sgmlparser.CaseFolding.ToLower;

				XmlDocument oDoc = new XmlDocument();
				oDoc.Load(oReader);

				XmlNodeList oRows = oDoc.SelectNodes("//*");
				for (int x = 0; x < oRows.Count; x++) {
					XmlElement oNode = oRows[x] as XmlElement;
					if (sAllowedTags.IndexOf("," + oNode.Name.ToLower() + ",") == -1) {
						if (",script,link,".IndexOf("," + oNode.Name.ToLower() + ",") == -1) {
							if (oNode.Value != "") {
								XmlText oNode1 = oNode.OwnerDocument.CreateTextNode(oNode.Value);
								oNode.ParentNode.InsertBefore(oNode1, oNode);
							}
							while (oNode.ChildNodes.Count > 0)
								oNode.ParentNode.InsertAfter(oNode.ChildNodes[oNode.ChildNodes.Count - 1], oNode);
							//oNode1.AppendChild(oNode.ChildNodes[0]);
							//oNode.ParentNode.FirstChild.InnerText += ;
						}
						oNode.ParentNode.RemoveChild(oNode);
					} else {
						for (int y = 0; y < oNode.Attributes.Count; y++) {
							if (sAllowedAttributes.IndexOf("," + oNode.Attributes[y].Name.ToLower() + ",") == -1) {
								oNode.Attributes.RemoveAt(y);
								y--;
							} else if (oNode.Attributes[y].Name.ToLower() == "href") {
								if (oNode.Attributes[y].Value.ToLower().Trim().IndexOf("javascript") == 0) {
									oNode.Attributes.RemoveAt(y);
									y--;
								} else if (oNode.Attributes[y].Value.IndexOf(":") == -1 && !oNode.Attributes[y].Value.Trim().StartsWith("/"))
									oNode.Attributes[y].Value = "http://" + oNode.Attributes[y].Value.Trim();

							}
						}
						if (oNode.Name.ToLower() == "a") oNode.SetAttribute("target", "_blank");
					}
				}

				return parse.inner_substring(oDoc.OuterXml, "<html", ">", "</html>", null);
			} catch (Exception) {
				return parse.replaceAll(jlib.functions.parse.stripHTML(sHTML, true), "\n", "<br/>"); ;
			}
		}

        public static string isNull(params object[] oStrings) {
            for (int x = 0; x < oStrings.Length; x++)
                if (convert.cStr(oStrings[x]).Trim() != "") return convert.cStr(oStrings[x]);
            
            return "";
        }

		public static string stripLeadingCharacter( object oData, params string []sChars ){
			string sData = convert.cStr(oData).Trim();
			for ( int x = 0 ; x < sChars.Length; x++ ){
				if (sChars[x] != "") {
					while (sData.ToLower().StartsWith(sChars[x].ToLower())) {
						sData = sData.Substring(sChars[x].Length).Trim();
					}
				}
				if ( sData.Length == 0 ) return "";
			}
			if (convert.cStr(oData) != sData) return stripLeadingCharacter(sData, sChars);
			return sData;
		}

		public static string stripEndingCharacter( object oData, params string []sChars ){
			string sData = convert.cStr(oData);			
			for ( int x = 0 ; x < sChars.Length; x++ ){
				if (sChars[x] != "") {
					while (sData.ToLower().EndsWith(sChars[x].ToLower())) {
                        sData = sData.Substring(0, sData.Length - sChars[x].Length);
						//x		= -1;
					}
				}
				if ( sData.Length == 0 ) return "";
			}
			if (convert.cStr(oData) != sData) return stripEndingCharacter(sData, sChars);
			return sData;
		}
        public static string stripCharacter(object oData, params string[] sChars) {
            return stripEndingCharacter(stripLeadingCharacter(oData, sChars), sChars);
        }

		/// <summary>
		/// Strips out ending &lt;br /&gt; and &lt;br&gt;
		/// </summary>
		/// <param name="oData">The original data string</param>		
		/// <returns>The resulting left part of the string</returns>

        public static string stripEndingNewlines(object oData) {

            string[] sArr = parse.split(oData, "<");
            for (int x = sArr.Length - 1; x >= 0; x--) {

                //bool bHasBR = false;
                if (sArr[x].ToLower().StartsWith("br")) {
					if (parse.stripWhiteChars(sArr[x].Substring(sArr[x].IndexOf(">") + 1)) == ""){
						sArr[x] = parse.stripWhiteChars(sArr[x].Substring(sArr[x].IndexOf(">") + 1));
						//bHasBR = true;
					}
                }

                if (sArr[x] != "") {
                    string sTmp = parse.join(sArr, "<");
					return sTmp.Substring(0, sTmp.Length - (sArr.Length - x - 1 ));
                    //return sTmp.Substring(0, sTmp.Length - (sArr.Length - x - ( bHasBR ? 1 : 0 )));
                }
            }
            return "";
        }

		/// <summary>
		/// Returns the left part of a string before a given character
		/// </summary>
		/// <param name="sData">The original data string</param>
		/// <param name="sDelim">The character delimiter to match</param>
		/// <returns>The resulting left part of the string</returns>
		public static string sLeft(string sData, string sDelim) {
			int pos	= 0;

			pos	= sData.IndexOf(sDelim);

			if (pos == -1) {
				pos = 0;
			}

			return sData.Substring(0, pos);

		}

		/// <summary>
		/// Returns the left part of a string before a given character
		/// </summary>
		/// <param name="sData">The original data string</param>
		/// <param name="sDelim">The character delimiter to match</param>
		/// <param name="iOffset">The offset to set left(positive)</param>
		/// <returns>The resulting left part of the string</returns>
		public static string sLeft(string sData, string sDelim, int iOffset) {
			int pos	= 0;

			pos	= sData.IndexOf(sDelim) + 1;

			if (pos == -1) {
				pos = 0;
			}
			else {
				pos -= iOffset;
			}

			return sData.Substring(0, pos);

		}

		public static string sLeft(object oData, int iLen) {
			return convert.cEllipsis(oData, iLen, false);
		}

		public static string sLeft(object oData, int iLen, string sPadChar) {
			return convert.cEllipsis(oData + new String(sPadChar[0],iLen), iLen, false);
		}

        /// <summary>
        /// Returns the rightmost n # of characters
        /// </summary>
        /// <param name="sData">The original data string</param>
        /// <param name="iNumChars"># of characters to return</param>        
        /// <returns>The resulting right part of the string</returns>
        public static string sRight( object oData,int iNumChars ) {

            string sData = convert.cStr( oData );
            if ( iNumChars >= sData.Length )
                return sData;

            return sData.Substring( sData.Length - iNumChars );

        }

		/// <summary>
		/// Returns the right part of a string after a given character
		/// </summary>
		/// <param name="sData">The original data string</param>
		/// <param name="sDelim">The character delimiter to match</param>
		/// <returns>The resulting right part of the string</returns>
		public static string sRight(string sData, string sDelim) {
			int pos	= 0;

			pos	= sData.LastIndexOf(sDelim);

			if (pos == -1) {
				pos = sData.Length;
			}

			return sData.Substring(pos);

		}

		/// <summary>
		/// Returns the right part of a string after a given character
		/// </summary>
		/// <param name="sData">The original data string</param>
		/// <param name="sDelim">The character delimiter to match</param>
		/// <param name="iOffset">The offset to set right(positive)</param>
		/// <returns>The resulting right part of the string</returns>
		public static string sRight(string sData, string sDelim, int iOffset) {
			int pos	= 0;

			pos	= sData.LastIndexOf(sDelim);

			if (pos == -1) {
				pos = sData.Length;
			}
			else {
				pos += iOffset;
			}

			return sData.Substring(pos);

		}

		public static string removeInnerSubstring(string sData, string sS1, string sF1) 
		{
			return removeInnerSubstring(sData, sS1, "", sF1, "");
		}

		public static string removeInnerSubstring(string sData, string sS1, string sS2, string sF1, string sF2) 
		{
			string sTemp = inner_substring(ref sData, sS1, sS2, sF1, sF2, false, true);
			return parse.replaceAll(sData, sTemp, "");
		}


		/// <summary>
		/// Parsing engine to strip out a substring
		/// </summary>
		/// <param name="oData">original data</param>
		/// <param name="sS1">First string to match from pos=0</param>
		/// <param name="sS2">Second string to match from pos=iS1</param>
		/// <param name="sF1">First string to match from pos=iS2</param>
		/// <param name="sF2">Second string to match from pos=iF1</param>
		/// <returns>The parsed string</returns>
		public static string inner_substring(object oData, string sS1, string sS2, string sF1, string sF2) {            
            return inner_substring(oData, sS1, sS2, null, sF1, sF2 );
		}

		public static string inner_substring(object oData, string sS1, string sS2, string sS3, string sF1, string sF2) {
			string sData = convert.cStr(oData);
			if (sS1 != null && sS1 != "") {
				if (sData.IndexOf(sS1) == -1)
					sData = "";
				else
					sData = sData.Substring(sData.IndexOf(sS1) + sS1.Length);
			}
			return inner_substring(ref sData, sS2, sS3, sF1, sF2, false);
		}

		public static string inner_substring(ref string sData, string sS1, string sS2, string sF1, string sF2,bool incsData) 
		{
			return inner_substring(ref sData, sS1, sS2, sF1, sF2, incsData, false);
		}

		public static string inner_substring(ref string sData, string sS1, string sS2, string sF1, string sF2,bool incsData, bool bIncludeSelections ) {
			return inner_substring(ref sData, sS1, sS2, sF1, sF2, incsData, bIncludeSelections, true);
		}


		/// <summary>
		/// Parsing engine to strip out a substring (CASE INSENSITIVE)
		/// </summary>
		/// <param name="sData">original data</param>
		/// <param name="sS1">First string to match from pos=0</param>
		/// <param name="sS2">Second string to match from pos=iS1</param>
		/// <param name="sF1">First string to match from pos=iS2</param>
		/// <param name="sF2">Second string to match from pos=iF1</param>
		/// <returns>The parsed string</returns>
		public static string inner_substring_i(string sData, string sS1, string sS2, string sF1, string sF2) {
			return inner_substring_i(ref sData, sS1, sS2, sF1, sF2,false);
		}

		public static string inner_substring_i(ref string sData, string sS1, string sS2, string sF1, string sF2,bool incsData) {
			return inner_substring_i(ref sData, sS1, sS2, sF1, sF2, incsData, false);
		}

		public static string inner_substring_i(ref string sData, string sS1, string sS2, string sF1, string sF2,bool incsData, bool bIncludeSelections ) {
			return inner_substring(ref sData, sS1, sS2, sF1, sF2, incsData, bIncludeSelections, false);
		}

		/// <summary>
		/// Parsing engine to strip out the LAST inner substring (CASE SENSITIVE)
		/// </summary>
		/// <param name="sData">original data</param>
		/// <param name="sS1">First string to match from pos=0</param>
		/// <param name="sS2">Second string to match from pos=iS1</param>
		/// <param name="sF1">First string to match from pos=iS2</param>
		/// <param name="sF2">Second string to match from pos=iF1</param>
		/// <returns>The parsed string</returns>
		public static string last_inner_substring(string sData, string sS1, string sS2, string sF1, string sF2) {

			sData	= convert.cStr( sData );
			if ( sData == "" ) return "";

			int iStartingPoint	= sData.Length - 1;
			if ( convert.cStr( sS2 ) != "" )
				iStartingPoint	= sData.LastIndexOf( sS2 , iStartingPoint );

			if ( convert.cStr( sS1 ) != "" )
				iStartingPoint	= sData.LastIndexOf( sS1, iStartingPoint );
			
			if( iStartingPoint > -1 )
				sData	= sData.Substring( iStartingPoint );

			return inner_substring(ref sData, sS1, sS2, sF1, sF2,false);
		}

		/// <summary>
		/// Parsing engine to strip out a substring
		/// </summary>
		/// <param name="sData">original data</param>
		/// <param name="sS1">First string to match from pos=0</param>
		/// <param name="sS2">Second string to match from pos=iS1</param>
		/// <param name="sF1">First string to match from pos=iS2</param>
		/// <param name="sF2">Second string to match from pos=iF1</param>
		/// <param name="incsData">Should starting-point of sData be incremented to ending point of string returned?</param>
		/// <param name="bIncludeSelections">Also include searched-for words?</param>
		/// <param name="bCaseSensitive">Should comparison be case sensitve?</param>		
		/// <returns>The parsed string</returns>
		public static string inner_substring(ref string sData, string sS1, string sS2, string sF1, string sF2,bool incsData, bool bIncludeSelections, bool bCaseSensitive ) 
		{
			if (sData == null)
				return "";

			string sTemp	= sData;
			if ( !bCaseSensitive ){

				sTemp		= sTemp.ToLower();
				if ( sS1 != null )	sS1	= sS1.ToLower();
				if ( sS2 != null )	sS2	= sS2.ToLower();
				if ( sF1 != null )	sF1	= sF1.ToLower();
				if ( sF2 != null )	sF2	= sF2.ToLower();
			}

			int pos_i1=0;
			int pos_i2=0;
			int pos_f1=0;
			int pos_f2=0;
						

			//Determine the position of the last character of sS1
			if (sS1==null)
				pos_i1=0;
			else	
				pos_i1 = sTemp.IndexOf(sS1);
			
			if (pos_i1 == -1)
				pos_i1 = 0;
			else if(sS1!=null && !bIncludeSelections)
				pos_i1 = pos_i1 + sS1.Length;

			//Determine the position of the last character of sS2
			if (sS2==null && sS1==null)
				pos_i2 =0;
			else if(sS2==null)
				pos_i2=pos_i1;
			else
				pos_i2 = sTemp.IndexOf(sS2, pos_i1);
			
			if (pos_i2 == -1)
				pos_i2 = pos_i1;
			else if (sS2!=null && !bIncludeSelections)
				pos_i2 = pos_i2 + sS2.Length;


			//Determine the position of the first character of sF1
			if (sF1==null)
				pos_f1=sTemp.Length;			
			else				
				pos_f1 = sTemp.IndexOf(sF1, pos_i2);
				
			if (pos_f1 == -1)
				pos_f1 = pos_i2;
		
			if(bIncludeSelections && pos_f1 < sTemp.Length && sF1 != null)
			{
				pos_f1 += sF1.Length;
			}

				
			//Determine the position of the first character of sF2
			if (sF2==null && sF1==null)//are both null
				pos_f2 =sTemp.Length;
			else if(sF2==null)//sF1 is not null, is sF2
				pos_f2=pos_f1;
			else if(sF1==null)
				pos_f2 = sTemp.IndexOf(sF2, pos_i2);
			else
				pos_f2 = sTemp.LastIndexOf(sF2, pos_f1);
				
			if (pos_f2 == -1)
				pos_f2 = pos_f1;
		

			if (pos_f2 < pos_f1 && sF1!=null)
				pos_f2 = pos_f1;
			
			if(bIncludeSelections && pos_f2 < sTemp.Length && sF2 != null)
			{
				pos_f2 += sF2.Length;
			}

			string retval=sData.Substring(pos_i2, pos_f2-pos_i2);
			

			if (incsData)
				sData=sData.Substring(pos_f2);

			return retval;
		}

		/// <summary>
		/// Parsing engine to strip out a substring
		/// </summary>
		/// <param name="sData">original data</param>
		/// <param name="sS1">First string to match from pos=0</param>
		/// <param name="sS2">Second string to match from pos=iS1</param>
		/// <param name="sF1">First string to match in reverse from pos=len(sData)</param>
		/// <param name="sF2">Second string to match in revers from pos=iF1</param>
		/// <returns>The parsed string</returns>
		public static string outer_substring(string sData, string sS1, string sS2, string sF1, string sF2) {
			return outer_substring(ref sData, sS1, sS2, sF1, sF2,false);
		}

		/// <summary>
		/// Parsing engine to strip out a substring
		/// </summary>
		/// <param name="sData">original data</param>
		/// <param name="sS1">First string to match from pos=0</param>
		/// <param name="sS2">Second string to match from pos=iS1</param>
		/// <param name="sF1">First string to match in reverse from pos=len(sData)</param>
		/// <param name="sF2">Second string to match in revers from pos=iF1</param>		
		/// <param name="incsData">Should starting-point of sData be incremented to ending point of string returned?</param>
		/// <returns>The parsed string</returns>
		public static string outer_substring(ref string sData, string sS1, string sS2, string sF1, string sF2, bool incsData) {
			int pos_i1=0;
			int pos_i2=0;
			int pos_f1=0;
			int pos_f2=0;

			if (sData == null)
				return "";

			//Determine the position of the last character of sS1
			if (sS1==null)
				pos_i1 =0;	
			else
				pos_i1 = sData.IndexOf(sS1);

			if (pos_i1 == -1){
				pos_i1 = 0;
			}else if (sS1!=null){
				pos_i1 = pos_i1 + sS1.Length;
			}

			//Determine the position of the last character of sS2
			if (sS2==null)
				pos_i2 =pos_i1;
			else
				pos_i2= sData.IndexOf(sS2, pos_i1+1);

			if (pos_i2 == -1){
				pos_i2 = pos_i1;
			}else if (sS2!=null){
				pos_i2 += sS2.Length;
			}

			//Determine the position of the first character of sF1
			if (sF1==null)
				pos_f1 = sData.Length;
			else
				pos_f1 = sData.LastIndexOf(sF1);

			if (pos_f1 == -1){
				pos_f1 = sData.Length;
			}

			if (pos_f1 <= pos_i2){
				pos_f1 = sData.Length;
			}
	
			//Determine the position of the first character of sF2
			if (sF2==null)
				pos_f2 = pos_f1;
			else
				pos_f2 = sData.LastIndexOf(sF2, pos_f1-1);

			if (pos_f2 == -1){
				pos_f2 = pos_f1;
			}
			
			if (pos_f2 <= pos_f1 && sF1!=null){
				pos_f2 = pos_f1;
			}


			string retval= sData.Substring(pos_i2, pos_f2-pos_i2);
			
			if (incsData)
				sData=sData.Substring(pos_f2);

			return retval;

			
		}

		/// <summary>
		/// Replace all characters in sChars with a empty string value
		/// </summary>
		/// <param name="sData">String to have the operation performed on</param>
		/// <param name="sChars">The character string of characters to be replaced</param>
		/// <returns>sData with specified characters removed</returns>
		public static string replace_chars(object oData, string sChars, string sReplacement) {
            string sData = convert.cStr(oData);
			for(int i=0;i<=sChars.Length-1;i++) 
				sData								= sData.Replace(sChars.Substring(i, 1), sReplacement);
			
			return sData;
		}

		public static string removeChars(object oData, string sChars) {
			return replace_chars(oData, sChars, "");
		}

		

		/// <summary>
		/// Removes MS Office HTML Formatting from a String
		/// </summary>
		/// <param name="sData">MS Office Formatted HTML Code</param>
		/// <returns></returns>
		public static string remove_office_formatting(string sData) {
			string[] sPatterns			= {
											  "<o:[^>]*>",
											  "<span[^>]*>",
											  "</o:[^>]*>",
											  "</span[^>]*>",
											  "<![^>]*>",
											  "style=\"[^\"]*\"",
											  "style='[^']*'",
											  "class=[^(>| )]*"
										  };


			for(int i=sPatterns.GetLowerBound(0);i<=sPatterns.GetUpperBound(0);i++)
				sData	= Regex.Replace(sData, sPatterns[i], "");
	
			return sData;
		}
		
		
		/// <summary>
		/// Replace all instances of sStr1 with sStr2 (case insensitive)
		/// </summary>
		/// <param name="sData">String to have the operation performed on</param>
		/// <param name="sStr1">The string to be replaced</param>
		/// <param name="sStr2">The replacement string </param>		
		/// <returns>sData with specified string replaced</returns>
		public static string replaceAll( object oData, string sStr1, string sStr2) {
			return replaceAll( oData, sStr1, sStr2, false );
		}

		/// <summary>
		/// Replace all instances of sStr1 with sStr2 (case insensitive)
		/// </summary>
		/// <param name="sData">String to have the operation performed on</param>
		/// <param name="sStr1">The string to be replaced</param>
		/// <param name="sStr2">The replacement string </param>
		/// <param name="bCaseSensitive">Case sensitive </param>
		/// <returns>sData with specified string replaced</returns>

        static public string replaceAll(object oData, string sPattern, string sReplacement, bool bCaseSensitive) {
            if (oData == null) return null;
            string sData = convert.cStr(oData);
            if (String.IsNullOrEmpty(sPattern) || sData == "") return sData;            

            StringComparison oComparisonType = (bCaseSensitive ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase);            

            int iPosCurrent = 0;
            int iLenPattern = sPattern.Length;
            int iNext = sData.IndexOf(sPattern, oComparisonType);
            StringBuilder oSB = new StringBuilder(Math.Min(4096, sData.Length));

            while (iNext >= 0) {
                oSB.Append(sData, iPosCurrent, iNext - iPosCurrent);
                oSB.Append(sReplacement);
                iPosCurrent = iNext + iLenPattern;
                iNext = sData.IndexOf(sPattern, iPosCurrent, oComparisonType);
            }

            oSB.Append(sData, iPosCurrent, sData.Length - iPosCurrent);
            return oSB.ToString();
        }

//#if NET_2_0
//#else	


        /// <summary>
		/// Replace all instances of param[n] with param[n+1] (case insensitive)
		/// </summary>
		/// <param name="oData">Object to have the operation performed on</param>
		/// <param name="sReplacePairs">Param with strings to find/replace (case insensitive)</param>				
		/// <returns>sData with specified string replaced</returns>
		public static string replaceAll( object oData, params string [] sReplacePairs) {						
			return replaceAll( oData, false, sReplacePairs );
		}

        public static string replaceAll(object oData, bool bCaseSensitive, params string[] sReplacePairs) {
            return replaceAll(oData, bCaseSensitive, false, sReplacePairs);
        }


        /// <summary>
        /// Replace all instances of sStr1 with sStr2 (case insensitive)
        /// </summary>
        /// <param name="sData">String to have the operation performed on</param>
        /// <param name="bCaseSensitive">Flag controlling if replacement is case sensitive </param>
        /// <param name="bAgressiveMode">Flag controlling if replacement is run in agressive mode (recursive replacements)</param>
        /// <param name="sReplacePairs">Pairs of values to be replaced</param>
        /// <returns>sData with specified string replaced</returns>
        
        public static string replaceAll(object oData, bool bCaseSensitive, bool bAgressiveMode, params string[] sReplacePairs) {

            string sData = convert.cStr(oData);
            for (int x = 0; x < sReplacePairs.Length; x++)
                if (x == sReplacePairs.Length - 1)
                    sData = parse.replaceAll(sData, sReplacePairs[x], "", bCaseSensitive);
                else {
                    sData = parse.replaceAll(sData, sReplacePairs[x], sReplacePairs[++x], bCaseSensitive);
                    while (bAgressiveMode && sData.IndexOf(sReplacePairs[x-1], (bCaseSensitive ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase)) > -1)
                        sData = parse.replaceAll(sData, sReplacePairs[x-1], sReplacePairs[x], bCaseSensitive);
                }
            return sData;
        }

		public static string removeStrings(string sData, params string[] sValues) {
			foreach(string sValue in sValues) {
				sData = replaceAll(sData, sValue, "");
			}
			return sData;
		}

        public static object[] removeNulls(object[] oValues) {
            if(oValues==null||oValues.Length==0)return oValues;
            int iCounter = 0;
            object[] o = (object[])Array.CreateInstance(oValues[0].GetType(), oValues.Length);
            for( int x=0;x<oValues.Length;x++)
                if(oValues[x]!=null)o[iCounter++]=oValues[x];

            if (iCounter < oValues.Length) {
                oValues = (object[])Array.CreateInstance(oValues[0].GetType(), iCounter);
                for( int x=0;x<oValues.Length;x++)oValues[x]=o[x];
                o=oValues;
            }
            return o;
        }

		public static string removeXChars(object oData, string sCharacters) {
			string sData	= convert.cStr(oData);
			sCharacters		= convert.cStr(sCharacters);

			string sReturnValue	= "";

			for(int i=0;i<sData.Length;i++) {
				string sCharacter	= sData.Substring(i,1);

				if(sCharacters.IndexOf(sCharacter) != -1)
					sReturnValue += sCharacter;
			}

			return sReturnValue;
		}

		public static string replaceXChars( string sData, string sCharacters , string sReplacement ) {
			sData			= convert.cStr(sData);
			sCharacters		= convert.cStr(sCharacters);
			
            System.Text.StringBuilder oBuilder = new System.Text.StringBuilder( sData.Length );

			for(int i=0;i<sData.Length;i++) {
				string sCharacter	= sData.Substring(i,1);

				if(sCharacters.IndexOf(sCharacter) != -1)
					oBuilder.Append( sCharacter );
				else
					oBuilder.Append( sReplacement );
			}

            return oBuilder.ToString();
		}


		public static Hashtable parseQuerystring(string sQuerystring) {
			Hashtable oHashtable	= new Hashtable();

			string[] sItems	= parse.split(sQuerystring, "&");
			foreach(string sItem in sItems) {
				//Split Name, Value
				string[] sItemPair	= parse.split(sItem, "=");
					
				//Assign Name, Value
				string sName		= sItemPair[0];
				string sValue		= sItemPair.Length > 1 ? sItemPair[1] : "";

				//Decode Name, Value
				sName				= System.Web.HttpUtility.UrlDecode(sName);
				sValue				= System.Web.HttpUtility.UrlDecode(sValue);

				oHashtable.Add( sName, sValue );
			}

			return oHashtable;
		}
        public static Hashtable removeEntries(Hashtable oTable, params object[] oKeys) {
            for (int x = 0; x < oKeys.Length; x++)
                oTable.Remove(oKeys[x]);
            return oTable;
        }

		public static readonly Regex PARSECSV_REGEX = new Regex(@"(?m)(?x)(\w+\,\s\w+|[\d\/]*\,\d+\:\d*|[\w\d\:\s\-\/]+)", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline | RegexOptions.Singleline);
		public static List<string> ParseCsvLine(string line)
		{
			var fields = new List<string>();

			var matches = PARSECSV_REGEX.Matches(line);
			foreach(Match match in matches)
			{
				fields.Add(match.Value);
			}

			return fields;

		}

		#endregion METHODS

	}

}
