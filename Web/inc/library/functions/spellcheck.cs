using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;

namespace jlib.functions
{
	/// <summary>
	/// Summary description for spellcheck.
	/// </summary>
	public class spellcheck
	{
		#region DECLARATIONS
		private string m_sRawHtml			= "";
		#endregion

		#region CONSTRUCTORS
		/// <summary>
		/// Constructor for spellcheck object
		/// </summary>
		public spellcheck()
		{

		}
		#endregion
		
		#region METHODS
		/// <summary>
		/// This Method will determine if the word is correct will return Ture 
		/// or else if incorrect then it will return False
		/// </summary>
		public virtual bool CheckSpelling(string word) 
		{	
			bool result = false;

			// Initialize the process
			result = Initialize(word);
			
			if( result == true )
			{
				result = true;
			}
			else
				if (result == false)
			{
				result = false;
			}
			return result;

		}

		/// <summary>
		/// Method to return the collection of words
		/// </summary>
		public virtual StringCollection GetSpellingSuggestions(string word) 
		{
			StringCollection s = new StringCollection(); 
			bool result = false;

			try
			{
				result = Initialize(word);

				// if misspelled then process
				if( result == false )
				{
					// extractWords will return the string of StringCollection
					s = extractWords();							
				}
		
				return s;
			}
			catch
			{
				return s;
			}
		}

		/// <summary>
		/// This Method will initialize the process
		/// </summary>
		private bool Initialize(string word)
		{
			bool result = false;
			string url = "";
			string returnHtml = "";

			url = buildUrl(word);
			returnHtml = readHtmlPage(url);	

			// add returnHtml to rawHtml
			m_sRawHtml = returnHtml;

			result = statusWord(returnHtml);

			return result;
		}

		/// <summary>
		/// Build the URL by passing a word for a spell checker
		/// </summary>
		private string buildUrl(string word)
		{
			string url = "http://www.spellcheck.net/cgi-bin/spell.exe?action=CHECKWORD&string=";
			url += word;
			return url;
		}

		/// <summary>
		/// Method to find the index of the string indicated
		/// starting at the position passed in
		/// </summary>
		private int Find(string strSearch, int nStart)
		{
			return m_sRawHtml.IndexOf(strSearch, nStart);
		}
		
		/// <summary>
		/// Method to return the length of a string in integer
		/// </summary>
		private int Length(string str)
		{
			return(str.Length);
		}

		/// <summary>
		/// Connect to web site by passing a URL and word
		/// Method to return the result in HTML 
		/// </summary>
		private String readHtmlPage(string url)
		{
			string result = "";
			try 
			{
				WebRequest objRequest = HttpWebRequest.Create(url);
				WebResponse objResponse = objRequest.GetResponse();
				Stream stream = objResponse.GetResponseStream();
				StreamReader sr = new StreamReader(stream);
		
				result = sr.ReadToEnd();
			}
			catch(Exception e)
			{
				return e.Message;
			}

			// convert the result to lower case and return it  
			return(result).ToLower();
		}   

		/// <summary>
		/// This Method will determine if the word is 
		/// Correct or inCorrect
		/// </summary>
		private bool statusWord(string rawHtml)
		{	
			bool result = false;
			string found = "";
			string correct = "correctly.";
			string uncorrect = "misspelled.";
			
			Regex match = new Regex(@"(correctly.)|(misspelled.)");
			found = match.Match(rawHtml).ToString();
			
			if (found == correct)
			{
				result = true;
			}
			else
				if(found == uncorrect)
			{
				result = false;
			}
			return result;
		}
		
		/// <summary>
		/// Method to extract the words from the HTML page and return the 
		/// regular expression group as a string collection
		/// </summary>
		private StringCollection extractWords() 
		{	
			StringCollection s = new StringCollection(); 

			string found = "";
			string correct = "suggestions:";
			
			// uncomment these lines if you are using second technique
			//
			//int nIndexStart = 0;
			//int nIndexEnd = 0;
			//int nIndex = 0;
			//string suggestionTag = "suggestions:";
			//string blockquoteStartTag = "<blockquote>";
			//string blockquoteEndTag   = "</blockquote>";

			// look for the suggestion tag in HTML 
			Regex matchS = new Regex(@"(suggestions:)");
			// rawHtml is private string variable which was initialize earlier
			// within Initialize function
			found = matchS.Match(m_sRawHtml).ToString();
			
			// if we have suggestion Tag then process
			if (found == correct)
			{
				/*
				 * regular expression is used to parse the string 
				 * we are trying to parse the following string from the HTML page
				 * <blockquote>
				 * 	string1<br>
				 *	string2<br>
				 * 	string3<br>
				 *  ...
				 *  ...
				 * </blockquote>
				*/
				Regex rg = new Regex(
					@"<blockquote>           # match tag
								(?:\s*([^<]+)<br>\s*)+   # match spaces capture string, <br> spaces 
								</blockquote>            # match end tag", 
					RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);

				MatchCollection matches = rg.Matches(m_sRawHtml);
				if (matches.Count > 0) 
				{
					// Get the first match
					Match match = (Match) matches[0];

					// Get the second group in the match
					Group grp = match.Groups[1];
					
					// print each capture within group to debug window
					foreach (Capture cap in grp.Captures) 
					{
						// print out the output in debug window
						// Debug.WriteLine(cap.Value);
						
						// print out the output to DOS console window
						// Console.WriteLine(cap.Value);

						// add the values to the StringCollection
						s.Add(cap.Value);
					}
				}


			}
			
			// return the collection of strings to next function
			return s;
		}
		#endregion
	}
}

