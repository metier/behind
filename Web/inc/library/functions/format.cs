using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace jlib.functions {
	public class format {
        public static string descriptiveTime(int iSeconds) {
            string s = (iSeconds%60) + "s";
            iSeconds = (iSeconds - iSeconds % 60)/60;
            if (iSeconds > 0) s = (iSeconds%60) + "m " + s;
            iSeconds = (iSeconds - iSeconds % 60)/60;
            if (iSeconds > 0) s = iSeconds + "h " + s;            
            return s;
        }
        public static string cHHMMSS(int iSeconds, string sFormat) {
            int[] iTime = new int[3];
            iTime[0] = (iSeconds % 60);
            iSeconds = (iSeconds - iSeconds % 60) / 60;
            iTime[1] = (iSeconds % 60);
            iSeconds = (iSeconds - iSeconds % 60) / 60;
            iTime[2] = iSeconds;
            return String.Format(sFormat, iTime[0], iTime[1], iTime[2]);
        }
		public static string shortTime(object oDate) {
			return String.Format("{0:hh}:{0:mm} {0:tt}", convert.cDate(oDate));
		}

		public static string shortDate(object oDate) {
			return String.Format("{0:MM}/{0:dd}", convert.cDate(oDate));
		}

		public static string medDate(object oDate) {
			return String.Format("{0:MM}/{0:dd}/{0:yy}", convert.cDate(oDate));
		}

        public static string medDateA(object oDate)
        {
            return String.Format("{0:MM}/{0:dd}/{0:yyyy}", convert.cDate(oDate));
        }

        public static string MonthName(object oDate)
        {
            return MonthName(convert.cDate(oDate).Month);
        }

		/// <summary>
		/// Gets Month Name
		/// </summary>
		/// <param name="iMonth">index of month</param>		
		/// <returns>name of month</returns>

		public static string MonthName(int iMonth) {

			switch (iMonth) {
				case 1: return ("January");
				case 2: return ("February");
				case 3: return ("March");
				case 4: return ("April");
				case 5: return ("May");
				case 6: return ("June");
				case 7: return ("July");
				case 8: return ("August");
				case 9: return ("September");
				case 10: return ("October");
				case 11: return ("November");
				case 12: return ("December");
				default: return ("Illegal month");
			}
		}

        public static int weekNumber(DateTime oDate) {
            return CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(oDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);            
        }

        public static int weekDayIndex(DayOfWeek oDayOfWeek) {
            switch (oDayOfWeek) {
                case DayOfWeek.Sunday: return 1;
                case DayOfWeek.Monday: return 2;
                case DayOfWeek.Tuesday: return 3;
                case DayOfWeek.Wednesday: return 4;
                case DayOfWeek.Thursday: return 5;
                case DayOfWeek.Friday: return 6;
                case DayOfWeek.Saturday: return 7;                
                default: return -1;
            }
        }
		
        public static string parseTimeZone(object oDateTime) {
            string[][] TimeZones = new string[][] {
                new string[] {"AST", "-0400", "(US) Atlantic Standard"},
                new string[] {"EST", "-0500", "(US) Eastern Standard"},    
                new string[] {"CST", "-0600", "(US) Central Standard"},
                new string[] {"MST", "-0700", "(US) Mountain Standard"},
                new string[] {"PST", "-0800", "(US) Pacific Standard"},
                new string[] {"GST", "+1000", "Guam Standard"},
                new string[] {"HDT", "-0900", "Hawaii Daylight"},
                new string[] {"HST", "-1000", "Hawaii Standard"},
                new string[] {"CDT", "-0500", "(US) Central Daylight"},                
                new string[] {"CENTRAL", "-0600", "(US) Central Standard"},
                new string[] {"EDT", "-0400", "(US) Eastern Daylight"},
                new string[] {"EASTERN", "-0500", "(US) Eastern Standard"},
    new string[] {"MDT", "-0600", "(US) Mountain Daylight"},    
    new string[] {"MOUNTAIN", "-0700", "(US) Mountain Standard"},    
    new string[] {"PDT", "-0700", "(US) Pacific Daylight"},    
    new string[] {"PACIFIC", "-0800", "(US) Pacific Standard"},
    
    new string[] {"ACDT", "+1030", "Australian Central Daylight"},
    new string[] {"ACST", "+0930", "Australian Central Standard"},
    new string[] {"ADT", "-0300", "(US) Atlantic Daylight"},
    new string[] {"AEDT", "+1100", "Australian East Daylight"},
    new string[] {"AEST", "+1000", "Australian East Standard"},
    new string[] {"AHDT", "-0900", ""},
    new string[] {"AHST", "-1000", ""},    
    new string[] {"AT", "-0200", "Azores"},
    new string[] {"AWDT", "+0900", "Australian West Daylight"},
    new string[] {"AWST", "+0800", "Australian West Standard"},
    new string[] {"BAT", "+0300", "Bhagdad"},
    new string[] {"BDST", "+0200", "British Double Summer"},
    new string[] {"BET", "-1100", "Bering Standard"},
    new string[] {"BST", "-0300", "Brazil Standard"},
    new string[] {"BT", "+0300", "Baghdad"},
    new string[] {"BZT2", "-0300", "Brazil Zone 2"},
    new string[] {"CADT", "+1030", "Central Australian Daylight"},
    new string[] {"CAST", "+0930", "Central Australian Standard"},
    new string[] {"CAT", "-1000", "Central Alaska"},
    new string[] {"CCT", "+0800", "China Coast"},    
    new string[] {"CED", "+0200", "Central European Daylight"},
    new string[] {"CET", "+0100", "Central European"},        
    new string[] {"EAST", "+1000", "Eastern Australian Standard"},    
    new string[] {"EED", "+0300", "Eastern European Daylight"},
    new string[] {"EET", "+0200", "Eastern Europe"},
    new string[] {"EEST", "+0300", "Eastern Europe Summer"},
    new string[] {"FST", "+0200", "French Summer"},
    new string[] {"FWT", "+0100", "French Winter"},
    new string[] {"GMT", "-0000", "Greenwich Mean"},
    new string[] {"IDLE", "+1200", "Internation Date Line East"},
    new string[] {"IDLW", "-1200", "Internation Date Line West"},
    new string[] {"IST", "+0530", "Indian Standard"},
    new string[] {"IT", "+0330", "Iran"},
    new string[] {"JST", "+0900", "Japan Standard"},
    new string[] {"JT", "+0700", "Java"},    
    new string[] {"MED", "+0200", "Middle European Daylight"},
    new string[] {"MET", "+0100", "Middle European"},
    new string[] {"MEST", "+0200", "Middle European Summer"},
    new string[] {"MEWT", "+0100", "Middle European Winter"},
    new string[] {"MT", "+0800", "Moluccas"},
    new string[] {"NDT", "-0230", "Newfoundland Daylight"},
    new string[] {"NFT", "-0330", "Newfoundland"},
    new string[] {"NT", "-1100", "Nome"},
    new string[] {"NST", "+0630", "North Sumatra"},
    new string[] {"NZ", "+1100", "New Zealand "},
    new string[] {"NZST", "+1200", "New Zealand Standard"},
    new string[] {"NZDT", "+1300", "New Zealand Daylight "},
    new string[] {"NZT", "+1200", "New Zealand"},
    new string[] {"ROK", "+0900", "Republic of Korea"},
    new string[] {"SAD", "+1000", "South Australia Daylight"},
    new string[] {"SAST", "+0900", "South Australia Standard"},
    new string[] {"SAT", "+0900", "South Australia Standard"},
    new string[] {"SDT", "+1000", "South Australia Daylight"},
    new string[] {"SST", "+0200", "Swedish Summer"},
    new string[] {"SWT", "+0100", "Swedish Winter"},
    new string[] {"USZ3", "+0400", "USSR Zone 3"},
    new string[] {"USZ4", "+0500", "USSR Zone 4"},
    new string[] {"USZ5", "+0600", "USSR Zone 5"},
    new string[] {"USZ6", "+0700", "USSR Zone 6"},
    new string[] {"UT", "-0000", "Universal Coordinated"},
    new string[] {"UTC", "-0000", "Universal Coordinated"},
    new string[] {"UZ10", "+1100", "USSR Zone 10"},
    new string[] {"WAT", "-0100", "West Africa"},
    new string[] {"WET", "-0000", "West European"},
    new string[] {"WST", "+0800", "West Australian Standard"},
    new string[] {"YDT", "-0800", "Yukon Daylight"},
    new string[] {"YST", "-0900", "Yukon Standard"},
    new string[] {"ZP4", "+0400", "USSR Zone 3"},
    new string[] {"ZP5", "+0500", "USSR Zone 4"},
    new string[] {"ZP6", "+0600", "USSR Zone 5"}
};
            int iSplit = Math.Max(Math.Max(convert.cStr(oDateTime).LastIndexOf(" "), convert.cStr(oDateTime).LastIndexOf("-")), convert.cStr(oDateTime).LastIndexOf("+"));
            if (iSplit > 1) {
                string sOffset = parse.replaceAll(convert.cStr(oDateTime).Substring(iSplit),":","").Trim();
                for (int x = 0; x < TimeZones.Length; x++)
                    if (TimeZones[x][1] == sOffset) return TimeZones[x][0];
                        // return TimeZoneInfo.FindSystemTimeZoneById(TimeZones[x][2]);
            }
            return TimeZone.CurrentTimeZone.StandardName;
        }
        public static DateTime addWorkDays(DateTime oStartDate, int iNumDays) {
            int iIncrement=(iNumDays > 0 ? 1 : -1);
            while (iNumDays != 0) {
                if (isWorkday(oStartDate)) iNumDays += -iIncrement;
                oStartDate = oStartDate.AddDays(iIncrement);
            }
            return oStartDate;
        }
        public static double countWorkdays(DateTime oStartDate, DateTime oEndDate) {
            double dWorkDays = 0;
            while (oStartDate.Date <= oEndDate.Date) {
                if(isWorkday(oStartDate)) dWorkDays++;
                oStartDate=oStartDate.AddDays(1);
            }
            if (isWorkday(oEndDate) && oEndDate.TimeOfDay.TotalMinutes > 7.5 * 60) dWorkDays += Math.Min(oEndDate.TimeOfDay.TotalMinutes - (7.5 * 60), 8 * 60) / (8 * 60);
            if (isWorkday(oStartDate) && oStartDate.TimeOfDay.TotalMinutes > 7.5 * 60) dWorkDays -= Math.Min(oStartDate.TimeOfDay.TotalMinutes - (7.5 * 60), 8 * 60) / (8 * 60);

            return dWorkDays;
        }
        public static bool isWorkday(DateTime oDate) {
            if (oDate.DayOfWeek == DayOfWeek.Sunday || oDate.DayOfWeek == DayOfWeek.Saturday) return false;

            //Memorial Day: Last Monday of May. Monday which is greater than the 24th
            if (oDate.DayOfWeek == DayOfWeek.Monday && oDate.Month == 5 && oDate.Day > 24) return false;

            //Labor Day: First Monday of Sept
            if (oDate.DayOfWeek == DayOfWeek.Monday && oDate.Month == 9 && oDate.Day < 8) return false;

            //THANKSGIVING: Fourth Thursday in November. Thursday which is between 22 and 27th
            if (oDate.DayOfWeek == DayOfWeek.Thursday && oDate.Month == 11 && (oDate.Day >= 22 && oDate.Day <= 27)) return false;

            //THANKSGIVING: Fourth Friday in November. Friday which is between 23 and 28th
            if (oDate.DayOfWeek == DayOfWeek.Friday && oDate.Month == 11 && (oDate.Day >= 23 && oDate.Day <= 28)) return false;

            /* Fourth of July */

            //EXCLUDED DAYS: 7/4 is on a weekday
            if (oDate.Month == 7 && oDate.Day == 4) return false;

            //EXCLUDED DAYS: 7/4 is on a SAT (exclude the 3rd)
            if (oDate.Month == 7 && oDate.Day == 3 && oDate.DayOfWeek == DayOfWeek.Friday) return false;

            //EXCLUDED DAYS: 7/4 is on a SUN (exclude the 5th)
            if (oDate.Month == 7 && oDate.Day == 5 && oDate.DayOfWeek == DayOfWeek.Monday) return false;

            /* Christmas Day */

            //EXCLUDED DAYS: 12/25 is on a weekday
            if (oDate.Month == 12 && oDate.Day == 25) return false;

            //EXCLUDED DAYS: 12/25 is on a SAT (exclude the 24th)
            if (oDate.Month == 12 && oDate.Day == 24 && oDate.DayOfWeek == DayOfWeek.Friday) return false;

            //EXCLUDED DAYS: 12/25 is on a SUN (exclude the 26th)
            if (oDate.Month == 12 && oDate.Day == 26 && oDate.DayOfWeek == DayOfWeek.Monday) return false;

            /* NEW YEARS DAY */

            //EXCLUDED DAYS: 01/01 is on a weekday
            if (oDate.Month == 1 && oDate.Day == 1) return false;

            //EXCLUDED DAYS: 01/01 is on a SAT (exclude the 31st)
            if (oDate.Month == 12 && oDate.Day == 31 && oDate.DayOfWeek == DayOfWeek.Friday) return false;

            //EXCLUDED DAYS: 01/01 is on a SUN (exclude the 2nd)
            if (oDate.Month == 1 && oDate.Day == 2 && oDate.DayOfWeek == DayOfWeek.Monday) return false;

            return true;
        }

        private static Dictionary<string, string> m_oLocalizedLanguages;
        public static string lookupLocalizedLanguageName(string sLanguageCode_639_1){
            if (m_oLocalizedLanguages == null) {
                m_oLocalizedLanguages = new Dictionary<string, string>();
                lock (m_oLocalizedLanguages) {
                    System.Reflection.Assembly oAssembly = typeof(format).Assembly;
                    System.IO.Stream oStream = oAssembly.GetManifestResourceStream("jlib.functions.resources.languages.txt");
                    System.IO.StreamReader oReader = new System.IO.StreamReader(oStream);
                    string[] sLanguages = parse.split(oReader.ReadToEnd(), "\n");
                    for (int x = 0; x < sLanguages.Length; x++)
                        m_oLocalizedLanguages[parse.splitValue(sLanguages[x], "\t", 3)]=parse.splitValue(sLanguages[x], "\t", 2);
                }
            }
            lock (m_oLocalizedLanguages) return m_oLocalizedLanguages.SafeGetValue(sLanguageCode_639_1);
    }

        public class no {

            public static string MonthName(object oDate) {
                return MonthName(convert.cDate(oDate).Month);
            }

            public static string MonthName(int iMonth) {

                switch (iMonth) {
                    case 1: return ("Januar");
                    case 2: return ("Februar");
                    case 3: return ("Mars");
                    case 4: return ("April");
                    case 5: return ("Mai");
                    case 6: return ("Juni");
                    case 7: return ("Juli");
                    case 8: return ("August");
                    case 9: return ("September");
                    case 10: return ("Oktober");
                    case 11: return ("November");
                    case 12: return ("Desember");
                    default: return ("Ulovlig m�ned");
                }
            }

            public static string WeekDayName(int iWeekDay) {
                switch (iWeekDay) {
                    case 1: return ("Mandag");
                    case 2: return ("Tirsdag");
                    case 3: return ("Onsdag");
                    case 4: return ("Torsdag");
                    case 5: return ("Fredag");
                    case 6: return ("L�rdag");
                    case 7: return ("S�ndag");
                    default: return ("Ulovlig Ukedag");
                }
            }

            public static int WeekDayIndex(DayOfWeek oDayOfWeek) {
                switch (oDayOfWeek) {
                    case DayOfWeek.Monday: return 1;
                    case DayOfWeek.Tuesday: return 2;
                    case DayOfWeek.Wednesday: return 3;
                    case DayOfWeek.Thursday: return 4;
                    case DayOfWeek.Friday: return 5;
                    case DayOfWeek.Saturday: return 6;
                    case DayOfWeek.Sunday: return 7;
                    default: return -1;
                }
            }
            public static string WeekDayName(DayOfWeek oDayOfWeek) {
                return WeekDayName(WeekDayIndex(oDayOfWeek));
            }
            public static string cCurrency(object oVal) {
                return convert.cGroupedNumber(oVal, 3, " ") + "," + jlib.functions.parse.sRight("00" + convert.cInt((convert.cDbl(oVal) - convert.cInt(oVal)) * 100), 2);
            }

            public static string formatPhoneNumber(object oData) {
                string sTmp = parse.replaceAll(oData, "(", "", ")", "", "-", "", " ", "", "[", "", "]", "");
                if (sTmp.Length == 8) {
                    if (sTmp.StartsWith("8"))
                        return sTmp.Substring(0, 3) + " " + sTmp.Substring(3, 2) + " " + sTmp.Substring(5, 3);
                    else
                        return sTmp.Substring(0, 2) + " " + sTmp.Substring(2, 2) + " " + sTmp.Substring(4, 2) + " " + sTmp.Substring(6, 2);
                }
                return convert.cStr(oData);
            }

            public static string cDefaultDateShort(object oDate) {
                return String.Format("{0:dd}.{0:MM}", oDate);
            }

            public static string cDefaultDate(object oDate) {
                return String.Format("{0:dd}.{0:MM}.{0:yy}", oDate);
            }

            //Format 12. august
            public static string cSimpleDate(object oDate) {
                return convert.cDate(oDate).Day + ". " + MonthName(convert.cDate(oDate).Month);
            }

            public static string cDefaultDateMedium(object oDate) {
                return String.Format("{0:dd}.{0:MM}.{0:yyyy}", oDate);
            }

            public static string cDefaultTime(object oDate) {
                return String.Format("{0:HH}:{0:mm}", oDate);
            }

            public static string cDefaultTimeLong(object oDate) {
                return String.Format("{0:HH}:{0:mm}:{0:ss}", oDate);
            }

            public static string cDefaultDateLong(object oDate) {
                DateTime oDate1 = convert.cDate(oDate);
                return String.Format("{0}, {1:dd} {2} {1:yyyy}", WeekDayName(oDate1.DayOfWeek), oDate1, MonthName(oDate1.Month));
            }	
        }
	}
}
