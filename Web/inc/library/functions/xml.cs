using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Drawing;
using System.Threading;
using System.IO;
using System.Text;
using System.Xml;
using System.Reflection;
using Saxon.Api;
using System.Collections.Generic;

namespace jlib.functions
{
	/// <summary>
	/// Summary description for ado_helper.
	/// </summary>
	public class xml
	{
		#region METHODS
        public static void ExpandEmptyHTMLTags(XmlNode parentNode) {
            ExpandEmptyTags(parentNode, null, new List<string>() { "img", "br" });
        }
        public static void ExpandEmptyTags(XmlNode parentNode, List<string> tagsToExpand, List<string> tagsToIgnore) {
            XmlNodeList emptyNodes = parentNode.SelectNodes("//*[not(text() or count(*)>0)]");
            foreach (XmlNode node in emptyNodes) {
                if (node as XmlElement != null && ((tagsToExpand != null && tagsToExpand.Contains(node.Name)) || (tagsToIgnore != null && !tagsToIgnore.Contains(node.Name))))
                    (node as XmlElement).IsEmpty = false;                    
            }
        }
        public static XmlDocument loadXml(string sXML) {
            try {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(sXML);
                return xmlDoc;
            } catch (Exception e) {
                throw new Exception("Can't load XML: " + sXML);
            }
        }
        /// <summary>
		/// Loads an xml file given a path
		/// </summary>
		/// <param name="sFilePath">Xml File Path of Source XML</param>
		/// <param name="sSBSDirectory">Directory where SBS is located</param>
		/// <returns>Loaded Xml Document</returns>
		public static XmlDocument loadXmlFile(string sFilePath , string sSBSDirectory)
		{
			
			//Variable Declarations
			XmlDocument oXmlDocument	= new XmlDocument();
			bool bDocumentLoaded		= false;

			//Determine File Name Constructs
			string sFilename			= System.IO.Path.GetFileNameWithoutExtension(sFilePath);
			string sExtension			= System.IO.Path.GetExtension(sFilePath);

			//Load SBS XML

			if ( sSBSDirectory == null ){
				
				string sPath			= System.IO.Path.GetFullPath( sFilePath );
				for ( int x = 0 ; x < 10 ; x++ ){
					string sFile			= System.IO.Path.Combine( sPath , String.Format("{0}.{1}.{2}", sFilename, x , sExtension.Replace(".", "")) );
					
					//Check to see if SBS exists
					if(File.Exists(sFile)) {
						//Load SBS into XmlDocument
						oXmlDocument.Load( sFile );

						//Set Loaded Flag
						bDocumentLoaded		= true;
			
					}
				}

			}else if ( sSBSDirectory != "" ){
				//Generate New SBS Filename
				string sFile			= System.IO.Path.Combine( sSBSDirectory , String.Format("{0}.{1}", sFilename,  sExtension.Replace(".", "")) );

				//Check to see if SBS exists
				if(File.Exists(sFile)) {
					//Load SBS into XmlDocument
					oXmlDocument.Load( sFile );

					//Set Loaded Flag
					bDocumentLoaded		= true;
			
				}
			}
			

			if(!bDocumentLoaded)
			{
				//Load Original Document
				oXmlDocument.Load( sFilePath );
			}

			//Return XmlDocument
			return oXmlDocument;

		}

        public static void copyAttributeValues(XmlNode xmlSourceNode, ref XmlNode xmlTargetNode, params string[] sAttributes) {
            for (int x = 0; x < xmlSourceNode.Attributes.Count; x++) {
                if (sAttributes == null || jlib.functions.parse.search(sAttributes, xmlSourceNode.Attributes[x].Name)) {
                    xml.setXmlAttributeValue(xmlTargetNode, xmlSourceNode.Attributes[x].Name, xml.getXmlAttributeValue(xmlSourceNode, xmlSourceNode.Attributes[x].Name));
                }
            }
        }

        public static void copyXmlFiles(string sSourceDir, string sDestDir, bool bForceOverwrite, bool bFlushDest)
		{
			
			if ( bFlushDest )
			{
				string []sFiles	=	System.IO.Directory.GetFiles( sDestDir , "*.xml" );
				for ( int x = 0 ; x < sFiles.Length; x++ ){
					
					File.SetAttributes( sDestDir + "\\" + Path.GetFileName( sFiles[ x ] ) , FileAttributes.Normal );
					System.IO.File.Delete( sDestDir + "\\" + Path.GetFileName( sFiles[ x ] ) );
				}
			}

			string []sXmlFiles	=	System.IO.Directory.GetFiles( sSourceDir , "*.xml" );
			for ( int x = 0 ; x < sXmlFiles.Length; x++ )
			{
				if ( !File.Exists( sDestDir + "\\" + Path.GetFileName( sXmlFiles[ x ] ) ) || bForceOverwrite || File.GetLastWriteTime( sDestDir + "\\" + Path.GetFileName( sXmlFiles[ x ] ) ) != File.GetLastWriteTime( sXmlFiles[ x ] ) ){
					
					if ( File.Exists( sDestDir + "\\" + Path.GetFileName( sXmlFiles[ x ] ) )  )
						File.SetAttributes( sDestDir + "\\" + Path.GetFileName( sXmlFiles[ x ] ) , FileAttributes.Normal );

					System.IO.File.Copy( sXmlFiles[ x ] , sDestDir + "\\" + Path.GetFileName( sXmlFiles[ x ] ) , true );
				}
			}		

		}

        /// <summary>
        /// Safe method for adding a new XmlElement
        /// </summary>
        /// <param name="xmlNode">Node that contains attribute</param>
        /// <param name="sName">Node Name</param>
        /// <returns>Node Value, "" if node does not exist</returns>
		public static XmlElement addXmlElement(XmlNode xmlNode, string sSubNodeName) {
			return addXmlElement(xmlNode, sSubNodeName, "", false);
		}
        public static XmlElement addXmlElement(XmlNode xmlNode, string sSubNodeName, bool bReplaceExisting) {
            return addXmlElement(xmlNode, sSubNodeName, "", bReplaceExisting);
        }
        public static XmlElement addXmlElement(XmlNode xmlNode, string sSubNodeName, object oInnerText) {
            return addXmlElement(xmlNode, sSubNodeName, oInnerText, false);
        }
        public static XmlElement addXmlElement(XmlNode xmlNode, string sSubNodeName, object oInnerText, bool bReplaceExisting) {
            if (xmlNode == null) return null;
            XmlDocument xmlDoc = ((xmlNode as XmlDocument) == null ? xmlNode.OwnerDocument : (xmlNode as XmlDocument));
            XmlElement xmlSubNode = (sSubNodeName.Contains(":") ? xmlNode.SelectSingleNode(sSubNodeName, xml.createNameSpaceManager(xmlDoc)) : xmlNode.SelectSingleNode(sSubNodeName)) as XmlElement;

            if (!bReplaceExisting || xmlSubNode == null) xmlSubNode = selectSingleNode(xmlNode, sSubNodeName, true, true);               

            xmlSubNode.InnerText = convert.cStr(oInnerText);
            return xmlSubNode;
        }
        public static XmlElement selectSingleNode(XmlNode xmlStartNode, string sSubNodeName, bool bForcePathCreationIfNonExists) {
            return selectSingleNode(xmlStartNode, sSubNodeName, bForcePathCreationIfNonExists, false);
        }
        private static XmlElement createElement(XmlDocument xmlDoc, string sNodeName) {
            return (sNodeName.Contains(":") ? xmlDoc.CreateElement(sNodeName.Split(':')[0], sNodeName.Split(':')[1], sNodeName.Split(':')[0]) : xmlDoc.CreateElement(sNodeName));
        }
        public static XmlElement selectSingleNode(XmlNode xmlStartNode, string sSubNodeName, bool bForcePathCreationIfNonExists, bool bAddNewLastNode) {
            if (xmlStartNode == null) return null;
            if (!bForcePathCreationIfNonExists && bAddNewLastNode) bForcePathCreationIfNonExists = true;
            string[] sArr = parse.split(sSubNodeName, "/");
            XmlNamespaceManager xmlNS = (sSubNodeName.Contains(":") ? xml.createNameSpaceManager(xmlStartNode.OwnerDocument) : null);

            XmlDocument xmlDoc = ((xmlStartNode as XmlDocument) == null ? xmlStartNode.OwnerDocument : (xmlStartNode as XmlDocument));
            XmlElement xmlSubNode = xmlStartNode.SelectSingleNode(sSubNodeName, xmlNS) as XmlElement;
            if (xmlSubNode != null && bAddNewLastNode) return xmlSubNode.ParentNode.AppendChild(createElement(xmlDoc, sArr[sArr.Length - 1])) as XmlElement;

            if (xmlSubNode == null && bForcePathCreationIfNonExists) {
                
                for (int x = sArr.Length - 1; x > 0; x--) {
                    xmlSubNode = (xmlStartNode.SelectSingleNode(convert.cJoin("/", sArr, 0, x), xmlNS) as XmlElement);
                    if (xmlSubNode != null) {
                        for (int y = x; y < sArr.Length; y++) {
                            string[] sNameArr = parse.split(sArr[y], "[");
                            xmlSubNode = xmlSubNode.AppendChild(createElement(xmlDoc,sNameArr[0])) as XmlElement;
                            for (int z = 1; z < sNameArr.Length; z++)
                                xml.setXmlAttributeValue(xmlSubNode, parse.inner_substring(sNameArr[z], "@", null, "=", null), parse.inner_substring(sNameArr[z], "='", null, "'", null));
                        }
                        break;
                    }
                }
                if (xmlSubNode == null) {
                    xmlSubNode = xmlStartNode.AppendChild(createElement(xmlDoc, sArr[0])) as XmlElement;
                    for (int x = 1; x < sArr.Length; x++) {
                        xmlSubNode = xmlSubNode.AppendChild(createElement(xmlDoc, sArr[x])) as XmlElement;
                    }
                }
            }
            return xmlSubNode;
        }
        public static XmlNode addXmlElementsFromRow(XmlNode xmlNode, DataRow oRow, params string[] sValues) {
            return addXmlElementsFromRow(xmlNode, oRow as jlib.db.IDataRow, sValues);
        }
        public static XmlNode addXmlElementsFromRow(XmlNode xmlNode, jlib.db.IDataRow oRow, params string[] sValues) 
		{
			if (oRow == null) return xmlNode;

            if (sValues == null || sValues.Length == 0) {
                for (int x = 0; x < oRow.GetColumnCount(); x++)
                    if (!oRow.GetFieldNameByIndex(x).ToUpper().StartsWith("ROWID")) xml.addXmlElement(xmlNode, oRow.GetFieldNameByIndex(x).Replace(' ', '_'), convert.cStr(oRow[x]));
            } else {
                if (sValues.Length == 1 && sValues[0].IndexOf(",") > -1) sValues = parse.split(sValues[0], ",");
				for (int x = 0; x < sValues.Length; x = x + 2)
				{	
					//get value
					string value = (oRow == null) ? "" : (oRow.GetValueByFieldName(sValues[x + 1]) ?? "").ToString();

					xml.addXmlElement(xmlNode, sValues[x].Replace(' ', '_'), value);
				}
            }
            return xmlNode;
        }
        public static XmlNode setXmlAttributeValueFromRow(XmlNode xmlNode, DataRow oRow, params string[] sValues) {
            return setXmlAttributeValueFromRow(xmlNode, oRow as jlib.db.IDataRow, sValues);
        }
        public static XmlNode setXmlAttributeValueFromRow(XmlNode xmlNode, jlib.db.IDataRow oRow, params string[] sValues) {            
            if (sValues == null || sValues.Length == 0) {
                for (int x = 0; x < oRow.GetColumnCount(); x++) {
                    if (!oRow.GetFieldNameByIndex(x).ToUpper().StartsWith("ROWID")) xml.setXmlAttributeValue(xmlNode, oRow.GetFieldNameByIndex(x).Replace(' ', '_'), convert.cStr(oRow[x]));
                }
            } else {
                if (sValues.Length == 1 && sValues[0].IndexOf(",") > -1) sValues = parse.split(sValues[0], ",");
                for (int x = 0; x < sValues.Length; x = x + 2) xml.setXmlAttributeValue(xmlNode, sValues[x].Replace(' ', '_'), (oRow == null ? "" : convert.cStr(oRow[sValues[x + 1]])));
            }
            return xmlNode;
        }

		/// <summary>
		/// Safe method for retrieving an attribute
		/// </summary>
		/// <param name="xmlNode">Node that contains attribute</param>
		/// <param name="sName">Attribute Name</param>
		/// <returns>Attribute Value, "" if attribute does not exist</returns>
        public static string getXmlAttributeValue(XmlNode xmlNode, string sSubNode, string sName) {
            return getXmlAttributeValue(xmlNode, sSubNode, sName, null);
        }
        public static string getXmlAttributeValue(XmlNode xmlNode, string sSubNode, string sName, XmlNamespaceManager oNSMgr) {
            if (xmlNode == null) return "";
            return getXmlAttributeValue(xmlNode.SelectSingleNode(sSubNode, oNSMgr), sName);
        }
        
        public static string getXmlAttributeValue(XmlNode xmlNode, string sName)
		{
            if (xmlNode == null || xmlNode.Attributes==null)
				return "";

			XmlAttribute xmlAttribute	= xmlNode.Attributes[sName];

			if(xmlAttribute == null)
			{
				xmlAttribute	= xmlNode.Attributes[sName.ToLower()];
			}

			if(xmlAttribute != null)
			{
				return xmlAttribute.Value;
			}

			return "";
		}

		/// <summary>
		/// Safe method for retrieving a node's InnerXML value
		/// </summary>
		/// <param name="xmlNode">Node that contains attribute</param>
		/// <param name="sName">Node Name</param>
		/// <returns>Node Value, "" if node does not exist</returns>
        public static string getXmlNodeValue(XmlNode xmlNode) {
            if (xmlNode == null) return null;
            return xmlNode.InnerXml;
        }
        public static string getXmlNodeValue(XmlNode xmlNode, string sName) {
            return getXmlNodeValue(xmlNode, sName, null);
        }
        public static string getXmlNodeValue(XmlNode xmlNode, string sName, XmlNamespaceManager oNSMgr)
		{
            XmlNode xmlSubNode = xmlNode.SelectSingleNode(sName, oNSMgr);

			if(xmlSubNode != null)
			{
				return xmlSubNode.InnerXml;
			}

			return "";
		}

		/// <summary>
		/// Safe method for retrieving a node's InnerText value
		/// </summary>
		/// <param name="xmlNode">Node that contains text child node</param>
		/// <param name="sName">Node Name</param>
		/// <returns>Node Value, "" if node does not exist</returns>
        public static string getXmlNodeTextValue(XmlNode xmlNode) {
            return getXmlNodeTextValue(xmlNode, null);
        }
        public static string getXmlNodeTextValue(XmlNode xmlNode, string sSubNodeName) {
            return getXmlNodeTextValue(xmlNode, sSubNodeName, null);
        }
        public static string getXmlNodeTextValue(XmlNode xmlNode, string sSubNodeName, XmlNamespaceManager oNSMgr) {
			if (xmlNode == null) return "";

            if (convert.cStr(sSubNodeName) != "") xmlNode = xmlNode.SelectSingleNode(sSubNodeName, oNSMgr);

            if (xmlNode != null) {
                for (int x = 0; x < xmlNode.ChildNodes.Count; x++)
                    if (xmlNode.ChildNodes[x] as System.Xml.XmlText != null) return xmlNode.ChildNodes[x].Value;
                //if (xmlNode.InnerText == "")
                //    return xmlNode.InnerXml;

                //return xmlNode.InnerText;
			}

			return (xmlNode==null?"":convert.cStr(xmlNode.InnerText));
		}
        public static string getXmlChildNodeText(XmlNode xmlNode) {
            if (xmlNode != null) {
                for (int x = 0; x < xmlNode.ChildNodes.Count; x++)
                    if (xmlNode.ChildNodes[x] as System.Xml.XmlText != null) return xmlNode.ChildNodes[x].Value;
            }
            return "";
        }

        public static string getXmlValue(XmlNode xmlNode) {
            if (xmlNode == null) return "";
            if (xmlNode.Value != null) return xmlNode.Value;
            return xmlNode.InnerXml;
        }
		/// <summary>
		/// Safe method for retrieving an xml value
		/// </summary>
		/// <param name="xmlNode">Node that contains attribute</param>
		/// <param name="sName">Element Name</param>
		/// <returns>Element Value, "" if element does not exist, checks attribute first then node</returns>
		public static string getXmlValue(XmlNode xmlNode, string sName)
		{
			string sValue		= "";
			
			//First Load Attribute Value
			sValue				= getXmlAttributeValue(xmlNode, sName);
			
			//If Attribute Not Found, Checks Sub Node
			if(sValue == "")
			{
				sValue			= getXmlNodeTextValue(xmlNode, sName);
			}
			
			return sValue;
			
		}


		public static XmlNode setXmlValue(XmlNode xmlNode, string sName, string sValue)
		{
			if(xmlNode.SelectSingleNode(sName) != null){
				setXmlNodeValue(xmlNode, sName, sValue);
			}else{
				setXmlAttributeValue(xmlNode, sName, sValue);
			}
            return xmlNode;
		}

		/// <summary>
		/// Safe method for setting an Xml Attribute Value
		/// </summary>
		/// <param name="xmlNode">Node that should contain attribute</param>
		/// <param name="sName">Attribute Name</param>
		/// <param name="sValue">Value to set attribute</param>
		
        public static XmlNode setXmlAttributeValue(XmlNode xmlNode, params object[] oValues) {
            for (int x = 0; x < oValues.Length; x = x + 2) {
                XmlAttribute xmlAttribute = xmlNode.Attributes[convert.cStr(oValues[x])];
                if (oValues[x + 1] == null) {
                    if (xmlAttribute != null) xmlNode.Attributes.Remove(xmlAttribute);
                } else {
                    if (xmlAttribute == null) {
                        xmlAttribute = xmlNode.OwnerDocument.CreateAttribute(convert.cStr(oValues[x]));
                        xmlNode.Attributes.Append(xmlAttribute);
                    }
                    xmlAttribute.Value = convert.cStr(oValues[x + 1]);
                }
            }
            return xmlNode;
        }

        public static XmlNode setXmlAttributeInnerXml(XmlNode xmlNode, params object[] oValues) {
            for (int x = 0; x < oValues.Length; x = x + 2) {
                XmlAttribute xmlAttribute = xmlNode.Attributes[convert.cStr(oValues[x])];
                if (oValues[x + 1] == null) {
                    if (xmlAttribute != null) xmlNode.Attributes.Remove(xmlAttribute);
                } else {
                    if (xmlAttribute == null) {
                        xmlAttribute = xmlNode.OwnerDocument.CreateAttribute(convert.cStr(oValues[x]));
                        xmlNode.Attributes.Append(xmlAttribute);
                    }
                    xmlAttribute.InnerXml = convert.cStr(oValues[x + 1]);
                }
            }
            return xmlNode;
        }

        public static XmlNode setXmlNodeValue(XmlNode xmlParentNode, params object[] oValues)
		{
            XmlNode xmlNode = null;
            for (int x = 0; x < oValues.Length; x = x + 2) {
                string sNodeName = convert.cStr(oValues[x]); 
                string sValue = convert.cStr(oValues[x+1]);
                xmlNode = (convert.cStr(sNodeName) == "" ? xmlParentNode : (sNodeName.Contains(":")? xmlParentNode.SelectSingleNode(sNodeName, xml.createNameSpaceManager(xmlParentNode.OwnerDocument)) : xmlParentNode.SelectSingleNode(sNodeName)));

                if (xmlNode == null) {
                    xmlNode = createElement(xmlParentNode.OwnerDocument, sNodeName);
                    xmlParentNode.AppendChild(xmlNode);
                }

                xmlNode.InnerText = sValue;                
            }
            return xmlNode;
		}

		public static XmlNode createXmlNodePath(XmlNode xmlParentNode, string sNodeName) {

			XmlNode oCurrNode	= xmlParentNode;
			XmlNode oPrevNode	= xmlParentNode;

			if ( sNodeName.Substring( 0 , 1 ) == "/" ) sNodeName	= sNodeName.Substring( 1 );
			if ( sNodeName.Substring( 0 , 1 ) == "/" ) sNodeName	= sNodeName.Substring( 1 );
						
			string []sNodePath	=	parse.split( sNodeName, "/" );

			if ( oCurrNode.Name == sNodePath[ 0 ])
				sNodePath[ 0 ]	= "/" + sNodePath[ 0 ];

			for (int x = 0; x < sNodePath.Length; x++) {
				if (sNodePath[ x ] != ""){
					oCurrNode	= oPrevNode.SelectSingleNode( sNodePath[ x ]);
					if (oCurrNode == null){
						oCurrNode	= oPrevNode.OwnerDocument.CreateElement( sNodePath[ x ] );
						oPrevNode.AppendChild( oCurrNode );						
					}
					oPrevNode	= oCurrNode;
				}
			}
			return oPrevNode;			
		}

        public static string getXPath(XmlNode xmlNode){
            string sPath = string.Format("/{0}[{1}]", xmlNode.Name, getXPathNodeIndex(xmlNode) + 1);
			
			XmlNode xmlParent	= xmlNode.ParentNode;
			while(xmlParent != null && xmlParent.Name != "#document"){
				if(xmlParent.ParentNode != null && xmlParent.ParentNode.Name != "#document"){
                    sPath = String.Format("/{0}[{2}]{1}", xmlParent.Name, sPath, getXPathNodeIndex(xmlParent) + 1);
				}else{
					sPath = String.Format("/{0}{1}", xmlParent.Name, sPath);
				}				
				xmlParent = xmlParent.ParentNode;
			}
			return sPath;
		}

		public static int getXPathNodeIndex(XmlNode xmlNode)
		{
			if(xmlNode.ParentNode == null) return 0;

            int iCounter = 0;
            foreach (XmlNode xmlNode1 in xmlNode.ParentNode.ChildNodes) {
                if (xmlNode1 is XmlElement && xmlNode1.Name == xmlNode.Name) {
                    if (xmlNode1 == xmlNode) return iCounter;
                    iCounter++;
                }
            }			
			return 0;
		}

		/// <summary>
		/// Safe method for converting an XmlNodeList to an Array
		/// </summary>
		/// <param name="oNodes">The XmlNodeList that should be converted to an array</param>		
		/// <returns>An Xml Node Array</returns>
		public static Array cArray( ref XmlNodeList oNodes ){
			Array oArray	= Array.CreateInstance( typeof( System.Xml.XmlNode ), oNodes.Count );
			for ( int x = 0 ; x < oNodes.Count; x++ ){
				oArray.SetValue( oNodes[ x ] , x );
			}
			return oArray;
		}

        public static string[] cArray(XmlNodeList xmlNodes, string sNode) {
            string[] s = new string[xmlNodes.Count];
            for (int x = 0; x < xmlNodes.Count; x++) s[x] = (sNode.StartsWith("@") ? xml.getXmlAttributeValue(xmlNodes[x], sNode.Substring(1)) : xml.getXmlNodeTextValue(xmlNodes[x], sNode));
            return s;
        }

        public static XmlDocument cObjectToXml(object oObj) {
            XmlDocument oDoc = new XmlDocument();
            oDoc.LoadXml("<object type=\"" + oObj.GetType().FullName + "\"><properties></properties></object>");

            foreach (PropertyInfo oProperty in oObj.GetType().GetProperties()) {
                try {
                    if(oProperty.CanRead) xml.addXmlElement(oDoc.SelectSingleNode("//properties"), "property", convert.cStr(oProperty.GetValue(oObj, null)));
                } catch (Exception) { }
            }
            
            
            return oDoc;
        }

		public static XmlNode removeXmlNode(XmlNode oNode) {
			if (oNode == null) return oNode;
			oNode.ParentNode.RemoveChild(oNode);
			return oNode;
		}
        public static XmlNode removeXmlNode(XmlNodeList oNodes) {
            if (oNodes == null || oNodes.Count==0) return null;
            XmlNode oParent=null;
            for (int x = 0; x < oNodes.Count; x++) {
                oParent=oNodes[x].ParentNode;
                if (oParent == null && (oNodes[x] as XmlAttribute) != null) {
                    XmlAttribute oAtt = (oNodes[x] as XmlAttribute);
                    oParent = oAtt.OwnerElement;
                    oParent.Attributes.Remove(oAtt);
                }else
                    oParent.RemoveChild(oNodes[x]);
            }
            return oParent;
        }

		public static string stripXmlUri(string sData) {
			if (sData.IndexOf("xmlns=") > -1)
				return parse.inner_substring(ref sData, null, null, "xmlns=", null, true) + parse.inner_substring(sData, "\"", "\"", null, null);

			return sData;
		}

        //Note that this code assume that the namespace declarations are on the root element. 
        public static XmlNamespaceManager createNameSpaceManager(XmlDocument oDoc) {
            return createNameSpaceManager(oDoc, false);
        }
        public static XmlNamespaceManager createNameSpaceManager(XmlDocument oDoc, bool bDeep) {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(oDoc.NameTable);
			if (oDoc.DocumentElement != null)
			{
				if (bDeep)
				{
					foreach (XmlNode oNode in oDoc.SelectNodes("//*"))
						foreach (XmlAttribute attr in oNode.Attributes)
							if (attr.Prefix == "xmlns" || attr.LocalName == "xmlns") nsmgr.AddNamespace((attr.LocalName == "xmlns" ? "default-namespace" : attr.LocalName), attr.Value);
				}
				else
				{
					foreach (XmlAttribute attr in oDoc.SelectSingleNode("/*").Attributes)
						if (attr.Prefix == "xmlns" || attr.LocalName == "xmlns") nsmgr.AddNamespace((attr.LocalName == "xmlns" ? "default-namespace" : attr.LocalName), attr.Value);
				}
			}
            return nsmgr;
        }

        public static string xslTransform(XmlDocument oDoc, string sXslName, string sLanguage) {
            return xslTransform(oDoc, sXslName, sLanguage, false);
        }
        public static string xslTransform(XmlDocument oDoc, string sXslName, string sLanguage, bool bEscapeAmpersand) {
            return xslTransform(oDoc, sXslName, sLanguage, bEscapeAmpersand, false);
        }
        public static string xslTransform(XmlDocument oDoc, string sXslName, string sLanguage, bool bEscapeAmpersand, bool bStripXmlTag) {
            return xslTransform(oDoc, sXslName, sLanguage, bEscapeAmpersand, bStripXmlTag, false);
        }
        public static string xslTransform(XmlDocument oDoc, string sXslName, string sLanguage, bool bEscapeAmpersand, bool bStripXmlTag, bool bStripEmailHeaderFields) {
            bool bHTMLEmails = true;// (oUser == null || oUser.HTMLEmails);
            
			//Build XslUrl
			string sXslUrl = (sXslName.StartsWith("/") ? (System.Web.HttpContext.Current != null ? System.Web.HttpContext.Current.Request.PhysicalApplicationPath : System.AppDomain.CurrentDomain.BaseDirectory) : (sXslName.Contains(":\\") || sXslName.Contains("://") || sXslName.StartsWith(@"\\") ? "" : (convert.cStr(ConfigurationManager.AppSettings["xslt.path"]) == "" ? (System.Web.HttpContext.Current != null ? System.Web.HttpContext.Current.Request.PhysicalApplicationPath : System.AppDomain.CurrentDomain.BaseDirectory) + "/inc/xsl/" : ConfigurationManager.AppSettings["xslt.path"]))) + sXslName + (sXslName.Contains(".txt") || sXslName.Contains(".xsl") ? "" : (bHTMLEmails ? "" : ".txt") + ".xsl");
            if (sXslUrl.StartsWith("/")) sXslUrl = ConfigurationManager.AppSettings["site.url"] + sXslUrl;

			//Read Contents
			string XslContent = io.read_file(sXslUrl, System.Text.Encoding.UTF8);


			//Transform
			return xslTransform(oDoc, sXslName, XslContent, sLanguage, bEscapeAmpersand, bStripXmlTag, bStripEmailHeaderFields);

        }

		public static string xslTransform(XmlDocument XmlContent, string sXslName, string XslContent, string sLanguage, bool bEscapeAmpersand, bool bStripXmlTag, bool bStripEmailHeaderFields) 
		{
			 bool bHTMLEmails = true;// (oUser == null || oUser.HTMLEmails);

			//Build XslUrl
			string sXslUrl = (sXslName.StartsWith("/") ? (System.Web.HttpContext.Current != null ? System.Web.HttpContext.Current.Request.PhysicalApplicationPath : System.AppDomain.CurrentDomain.BaseDirectory) : (sXslName.Contains(":\\") || sXslName.Contains("://") || sXslName.StartsWith(@"\\") ? "" : (convert.cStr(ConfigurationManager.AppSettings["xslt.path"]) == "" ? (System.Web.HttpContext.Current != null ? System.Web.HttpContext.Current.Request.PhysicalApplicationPath : System.AppDomain.CurrentDomain.BaseDirectory) + "/inc/xsl/" : ConfigurationManager.AppSettings["xslt.path"]))) + sXslName + (sXslName.Contains(".txt") || sXslName.Contains(".xsl") ? "" : (bHTMLEmails ? "" : ".txt") + ".xsl");
            if (sXslUrl.StartsWith("/")) sXslUrl = ConfigurationManager.AppSettings["site.url"] + sXslUrl;
           
            try 
			{

                Processor oProcessor = new Processor();
                xml.setXmlAttributeValue(XmlContent.DocumentElement, "xsl", sXslName, "language", sLanguage);
                XdmNode oInput = oProcessor.NewDocumentBuilder().Wrap(XmlContent);

                XslContent = parse.replaceAll(XslContent, "&trade;", "&#8482;", "&reg;", "&#174;", "&nbsp;", "&#160;");
                if (bEscapeAmpersand) XslContent = parse.replaceAll(XslContent, "&", "<amp/>");
                XsltCompiler oCompiler = oProcessor.NewXsltCompiler();
                oCompiler.BaseUri = new Uri(sXslUrl);
                XsltTransformer oTransformer = oCompiler.Compile(new System.IO.StringReader(XslContent)).Load();
                //XsltTransformer oTransformer = oProcessor.NewXsltCompiler().Compile(new Uri(sXslUrl)).Load();
                oTransformer.InitialContextNode = oInput;

                Serializer oSerializer = new Serializer();
                System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
                oSerializer.SetOutputWriter(oStringWriter);

                // Transform the source XML to System.out.
                oTransformer.Run(oSerializer);

                string sValue = oStringWriter.ToString();
                if (XslContent.IndexOf("xsl:output method=\"xml\"") > -1) sValue = parse.replaceAll(sValue, "&#xD;", "", "&#xA;", "");
                else sValue = parse.replaceAll(sValue, "&#xD;", "\r", "&#xA;", "\n");

                if (bEscapeAmpersand) return parse.replaceAll(sValue, "<amp/>", "&");
                if (bStripXmlTag && sValue.IndexOf("?>") > -1) sValue = parse.stripWhiteChars(parse.inner_substring(sValue, "?>", null, null, null));
                if (bStripXmlTag && sValue.IndexOf(" xmlns:", StringComparison.CurrentCultureIgnoreCase) > -1) {
                    string[] sArr = parse.split(sValue, " xmlns:");
                    for (int x = 1; x < sArr.Length; x++) sArr[x] = convert.cIfValue(sArr[x].Substring(sArr[x].IndexOf("\"", sArr[x].IndexOf("\"")+1)+1).Trim()," ","","");
                    sValue = parse.join(sArr, "");
                }
                if (bStripEmailHeaderFields) {
                    string[] sTokens = new string[] { "From-email:", "Subject:", "To-email:", "Attach:", "Pagesize:", "BCC-email:", "Internal:" };
                    for (int x = 0; x < sTokens.Length; x++) {
                        if (sValue.StartsWith(sTokens[x])) {
                            string sSep = parse.findSeparator(sValue, "\n", "<");
                            if (sSep == "<" && sValue.IndexOf("<tran") == sValue.IndexOf("<")) sSep = "\n";
                            sValue = parse.stripWhiteChars(sValue.Substring(sValue.IndexOf(sSep)));
                            x = -1;
                        }
                    }
                }
                //if (oUser != null) 
                sValue = jlib.helpers.translation.translateHTML(sValue, "translate", sLanguage, sXslName);
                return sValue;
            } catch (Exception e) {
                throw new Exception("Can't compile stylesheet -- URL: " + sXslUrl + "\n\n" + XmlContent.OuterXml, e);
            }
        }

        public static string EscapeXmlExpression(object expression) {
            string exp = expression.Str();
            if (!exp.Contains("'") && !exp.Contains("\"")) return "'" + expression + "'";
            return "concat('','" + parse.replaceAll(expression,"'",((char)1).ToString(),"\"",((char)2).ToString(),((char)1).ToString(),"',\"'\",'",((char)2).ToString(),"','\"','") + "')";            
        }

		#endregion METHODS
	}
}
