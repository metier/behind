using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Drawing;
using System.Threading;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Cryptography;

namespace jlib.functions
{
	/// <summary>
	/// Summary description for ado_helper.
	/// </summary>
	public class io
	{
		#region METHODS
		/// <summary>
		/// Copies a file from one location to another
		/// </summary>
		/// <param name="sPath1">Source Location</param>
		/// <param name="sPath2">Destination Location</param>
		/// <returns>successful</returns>
		public static bool copy_file(string sPath1, string sPath2)
		{
			if (sPath1.StartsWith("/")) sPath1 = System.Web.HttpContext.Current.Server.MapPath(sPath1);
			if (sPath2.StartsWith("/")) sPath2 = System.Web.HttpContext.Current.Server.MapPath(sPath2);
			try
			{
				io.delete_file(sPath2);
				File.Copy( sPath1, sPath2, true );
				
				return true;
			}
			catch(Exception)
			{
				return false;
			}
		}

		public static bool copy_file(string []sPaths, string sDestDir) {
			bool bSuccess = true;
			for (int x = 0; x < sPaths.Length; x++) {
				if (!copy_file(sPaths[x], sDestDir + "\\" + Path.GetFileName(sPaths[x]))) bSuccess = false;

			}
			return bSuccess;
		}

        /// <summary>
        /// Moves a file from one location to another
        /// </summary>
        /// <param name="sPath1">Source Location</param>
        /// <param name="sPath2">Destination Location</param>
        /// <returns>successful</returns>
        public static bool move_file( string sPath1, string sPath2 ) {
            //try {
			if (sPath1.StartsWith("/")) sPath1 = System.Web.HttpContext.Current.Server.MapPath(sPath1);
			if (sPath2.StartsWith("/")) sPath2 = System.Web.HttpContext.Current.Server.MapPath(sPath2);
                if ( !System.IO.File.Exists( sPath1 ) )
                    return false;
                if (String.Compare( parse.replaceAll(sPath1, "\\", ""), parse.replaceAll(sPath2, "\\", ""), true)==0) return true;

                io.delete_file( sPath2 );
                File.Copy( sPath1, sPath2, true );
                if ( !System.IO.File.Exists( sPath2 ) )
                    return false;

                io.delete_file( sPath1 );

                return true;
            //} catch ( Exception e ) {
             //   return false;
            //}

        }

		/// <summary>
		/// Moves a file from one location to another
		/// </summary>
		/// <param name="sPaths">Source Location</param>
		/// <param name="sPath2">Destination Location</param>
		/// <returns>successful</returns>
		public static bool move_file(string[] sPaths, string sDestDir) {
			bool bSuccess = true;
			for (int x = 0; x < sPaths.Length; x++) {
				if (!move_file(sPaths[x], sDestDir + "\\" + System.IO.Path.GetFileName(sPaths[x]))) bSuccess = false;				
			}
			return bSuccess;
		}

		/// <summary>
		/// delete a file
		/// </summary>
		/// <param name="sPath">File Location</param>
		/// <returns>successful</returns>
		public static bool delete_file(string sPath)
		{
			if (sPath.StartsWith("/")) sPath = System.Web.HttpContext.Current.Server.MapPath(sPath);
			
			try{
				if(File.Exists(sPath) ){
					File.Delete( sPath );
				}
				
				return true;
			}
			catch(Exception)
			{
				return false;
			}

		}

		/// <summary>
		/// delete a file
		/// </summary>
		/// <param name="sPath">File Location</param>
		/// <returns>successful</returns>
		public static bool delete_file(string []sPaths) {
			bool bSuccess = true;
			for (int x = 0; x < sPaths.Length; x++) {
				try {
					if (!delete_file(sPaths[x])) bSuccess = false;
					
				} catch (Exception) {
					bSuccess = false;
				}
			}
			return bSuccess;
		}

		/// <summary>
		/// writes a text string to a file
		/// </summary>
		/// <param name="sPath">File Location</param>
		/// <param name="sData">Data to write</param>
		/// <returns>Successful</returns>
        public static bool write_file(string sPath, string sData) {
            return write_file(sPath, sData, false);
        }
        public static bool write_file(string sPath, string sData, bool bAppend) {
            return write_file(sPath, sData, bAppend, System.Text.Encoding.Unicode);
        }

        private static object textLogLock = new object();
        public static bool write_file(string sPath, string sData, bool bAppend, Encoding oEncoding)
		{
			try
			{
                lock (textLogLock) {
                    sPath = MapPath(sPath);

                    //Write File
                    FileStream oFileStream = new FileStream(sPath, (bAppend ? System.IO.FileMode.Append : System.IO.FileMode.Create), System.IO.FileAccess.Write, System.IO.FileShare.Read);
                    StreamWriter oStreamWriter = new StreamWriter(oFileStream, oEncoding);

                    oStreamWriter.Write(sData);
                    oStreamWriter.Flush();

                    oStreamWriter.Close();

                    oFileStream.Close();
                }

			}
			catch(Exception)
			{
				return false;
			}

			return true;
		}
        public static string MapPath(string sPath) {
            if (sPath.IsNullOrEmpty()) return "";
            if (sPath.StartsWith("/")) sPath = System.Web.HttpContext.Current.Server.MapPath(sPath);
            else if (sPath.StartsWith("~")) sPath = System.Web.HttpContext.Current.Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath + sPath.Substring(1));
            //Create Directories If Needed
            string sDirectory = Path.GetDirectoryName(sPath);
            createDirectory(sDirectory);
            return sPath;
        }
        /// <summary>
        /// writes a stream to a file
        /// </summary>
        /// <param name="sPath">File Location</param>
        /// <param name="sData">Data to write</param>
        /// <returns>Successful</returns>
        public static bool write_file( string sPath, System.IO.Stream oStream){
            return write_file(sPath, oStream, false);
        }
        public static bool write_file( string sPath, System.IO.Stream oStream, bool bAppend ) {
            FileStream oFileStream = null;
            try {
				if (sPath.StartsWith("/")) sPath = System.Web.HttpContext.Current.Server.MapPath(sPath);
                //Create Directories If Needed                
                createDirectory( Path.GetDirectoryName( sPath ) );

                //Write File
                oFileStream = new FileStream(sPath, (bAppend ? System.IO.FileMode.Append : System.IO.FileMode.Create), System.IO.FileAccess.Write, System.IO.FileShare.Read);

                byte []bBuffer  = new byte[ 4096 ];
                int iNumBytes   = oStream.Read( bBuffer, 0, bBuffer.Length );
                while( iNumBytes > 0 ){
                    oFileStream.Write( bBuffer, 0, iNumBytes );
                    iNumBytes   = oStream.Read( bBuffer, 0, bBuffer.Length );
                }

                oFileStream.Flush();
                oFileStream.Close();

            } catch ( Exception) {
                try {
                    oFileStream.Flush();
                    oFileStream.Close();    
                }catch(Exception){}
                return false;
            }

            return true;
        }
        public static bool write_file(string sPath, byte[] bBuffer) {
            return write_file(sPath, bBuffer, false);
        }
		public static bool write_file(string sPath, byte []bBuffer, bool bAppend) {
			try {
				if (sPath.StartsWith("/")) sPath = System.Web.HttpContext.Current.Server.MapPath(sPath);
				//Create Directories If Needed                
				createDirectory(Path.GetDirectoryName(sPath));
				//Write File
                FileStream oFileStream = new FileStream(sPath, (bAppend ? System.IO.FileMode.Append : System.IO.FileMode.Create), System.IO.FileAccess.Write, System.IO.FileShare.Read);

				oFileStream.Write(bBuffer, 0, bBuffer.Length);
				
				oFileStream.Flush();
				oFileStream.Close();

			} catch (Exception) {
				return false;
			}
			return true;
		}

		/// <summary>
		/// appends a text string to a file
		/// </summary>
		/// <param name="sPath">File Location</param>
		/// <param name="sData">Data to append</param>
		/// <returns>Successful</returns>
		public static bool append_file(string sPath, string sData)
		{
			try{

				if (sPath.StartsWith("/")) sPath = System.Web.HttpContext.Current.Server.MapPath(sPath);
				//Create Directories If Needed
				string sDirectory			= Path.GetDirectoryName(sPath);
				createDirectory(sDirectory);

				//Append File
				FileStream oFileStream		= new FileStream( sPath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite, System.IO.FileShare.Read);
				oFileStream.Seek(0, System.IO.SeekOrigin.End);

				StreamWriter oStreamWriter	= new StreamWriter( oFileStream );

				oStreamWriter.Write( sData );
				oStreamWriter.Flush();

				oStreamWriter.Close();

				oFileStream.Close();

			}
			catch(Exception)
			{
				return false;
			}

			return true;
		}


		/// <summary>
		/// reads a text file into a string. By defult, UNICODE format will be used
		/// </summary>
		/// <param name="sPath">File Location</param>
		/// <returns>File Contents</returns>
		public static string read_file(string sPath)
		{
			return read_file(sPath, System.Text.Encoding.UTF7);
		}

        public static string read_file(string sPath, long lStartIndex, long lLength) {
            return read_file(sPath, System.Text.Encoding.UTF7, lStartIndex, lLength);
        }

		/// <summary>
		/// reads a text file into a string
		/// </summary>
		/// <param name="sPath">File Location</param>
		/// <returns>File Contents</returns>
        public static string read_file(string sPath, Encoding oEncoding) {
            return read_file(sPath, oEncoding, -1, -1);
        }
		public static string read_file(string sPath, Encoding oEncoding, long lStartIndex, long lLength) {
			try {
				if (sPath.StartsWith("/")) sPath = System.Web.HttpContext.Current.Server.MapPath(sPath);
                Stream oStream=null;
                if (sPath.IndexOf("://") > -1) {
                    oStream=getStream(sPath);
                } else {
                    oStream = new FileStream(sPath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Read, System.IO.FileShare.Read);
                    oStream.Seek(0, System.IO.SeekOrigin.Begin);
                }

				StreamReader oStreamReader;
				if( oEncoding == null )
                    oStreamReader = new StreamReader(oStream, true);
				else
                    oStreamReader = new StreamReader(oStream, oEncoding);

                if(lStartIndex>0)oStreamReader.BaseStream.Position = lStartIndex;                
				string sContents = oStreamReader.ReadToEnd();
                if (lLength > 0) sContents = jlib.functions.convert.cEllipsis(sContents, convert.cInt(lLength), false);

				oStreamReader.Close();
                oStream.Close();

				return sContents;
			} catch (Exception) {
				return "";
			}
		}

		

        

		/// <summary>
		/// reads a binary file into a byte-array
		/// </summary>
		/// <param name="sPath">File Location</param>
		/// <returns>File Contents</returns>
		public static byte[] read_file_binary( string sPath ) {
			try {
				if (sPath.StartsWith("/")) sPath = System.Web.HttpContext.Current.Server.MapPath(sPath);				
				FileStream oFileStream		= new FileStream( sPath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Read, System.IO.FileShare.Read);
				System.IO.BufferedStream oBStream	= new BufferedStream( oFileStream );
				
				byte []bArr					= new byte[ oBStream.Length ];

				oBStream.Read( bArr , 0 , bArr.Length );
				
				
				oBStream.Close();
				oFileStream.Close();

				return bArr;
			
			}catch(Exception) {
				return new byte[ 0 ];
			}			
		}
        
        public static DirectoryInfo createDirectory( string sPath ) {
            return createDirectory( sPath, false );
        }

		public static DirectoryInfo createDirectory(string sPath, bool bIsFileName)
		{
			if (sPath.StartsWith("/")) sPath = System.Web.HttpContext.Current.Server.MapPath(sPath);				
            return Directory.CreateDirectory( bIsFileName ? System.IO.Path.GetDirectoryName( sPath ) : sPath );
		}
        public static string getValidFileName(string sFileName) {            
            foreach (char c in System.IO.Path.GetInvalidFileNameChars()) {
                sFileName = sFileName.Replace(c, '_');
            }
            return convert.cStr(sFileName, "UNKNOWN");
        }

        public static string getUniqueFileName( string sUrl ) {

            string sFileName = sUrl;
            if ( sFileName.IndexOf( "?" ) > -1 )
                sFileName = parse.inner_substring( sFileName, null, null, "?", null );

            if ( sFileName.IndexOf( "/" ) > -1 ) {
                try{
                    sFileName = System.Web.HttpContext.Current.Server.MapPath( sFileName );
                }catch(Exception){}
            }

            if (System.IO.Path.GetExtension(sFileName).Length>0 && ",.php,.php4,.php5,.asp,.aspx,.ashx,.js,".Contains(System.IO.Path.GetExtension(sFileName).ToLower())) sFileName = sFileName.Substring(0, sFileName.Length - System.IO.Path.GetExtension(sFileName).Length) + ".ext-" + System.IO.Path.GetExtension(sFileName).Substring(1);

            if ( System.IO.File.Exists( sFileName ) ) {
                for ( int x = 0; ; x++ ) {
                    sFileName = System.IO.Path.GetDirectoryName( sFileName ) + "\\" + System.IO.Path.GetFileNameWithoutExtension( sUrl ) + "_" + x + System.IO.Path.GetExtension( sFileName );
                    if ( !System.IO.File.Exists( sFileName ) ) return sFileName;
                }
            }
            return sFileName;
        }

        public static long file_size(string sPath) {
            System.IO.FileInfo oInfo = new FileInfo(sPath);
            return oInfo.Length;
        }

        public static System.IO.Stream getStream( string sUri ) {

			if (sUri.StartsWith("/") && !sUri.EndsWith(".jpg") && !sUri.EndsWith(".gif") && !sUri.EndsWith(".png")) sUri = System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Host + sUri;

            if ( sUri.ToLower().IndexOf( "http:" ) > -1 || sUri.ToLower().IndexOf( "https:" ) > -1 || sUri.ToLower().IndexOf( "ftp:" ) > -1 ) {

                System.Net.WebRequest oRequest;
                oRequest = System.Net.WebRequest.Create( sUri );
                System.Net.WebResponse oResponse = oRequest.GetResponse();

                System.IO.Stream oStream = oResponse.GetResponseStream();
                //oStream.SetLength( oResponse.Headers[
                return oStream;

            } else {

                if ( sUri.IndexOf( ":\\" ) == -1 && !sUri.StartsWith( "\\\\" )) {
                    try {                        
                        sUri = System.Web.HttpContext.Current.Server.MapPath( (sUri.StartsWith("/")?"":convert.cIfValue(System.Web.HttpContext.Current.Request.ApplicationPath,"","/","")) + sUri );
                    } catch ( Exception ) { }
                }
                    

                System.IO.StreamReader oReader = new StreamReader( sUri );
                return oReader.BaseStream;

            }
        }

        public static void writeToStream( ref Stream oOutputStream, Stream oInputStream ) {
            writeToStream( ref oOutputStream, convert.cStreamToByteArray( oInputStream ) );            
        }

        public static void writeToStream( ref Stream oOutputStream, byte[] bData ) {
            if( bData != null && bData.Length > 0 )
                oOutputStream.Write( bData, 0, bData.Length );            
        }

        public static string CalculateChecksum(string sFile) {
            BufferedStream oStream = new BufferedStream(File.OpenRead(sFile), 1200000);
            SHA256Managed SHA = new SHA256Managed();
            byte[] checksum = SHA.ComputeHash(oStream);
            return BitConverter.ToString(checksum).Replace("-", String.Empty);            
        }

        public static class TextFileEncodingDetector {
            /* Source : http://www.architectshack.com/TextFileEncodingDetector.ashx */

            const long _defaultHeuristicSampleSize = 0x10000; //completely arbitrary - inappropriate for high numbers of files / high speed requirements

            public static Encoding DetectTextFileEncoding(string InputFilename, Encoding DefaultEncoding) {
                using (FileStream textfileStream = File.OpenRead(InputFilename)) {
                    return DetectTextFileEncoding(textfileStream, DefaultEncoding, _defaultHeuristicSampleSize);
                }
            }

            public static Encoding DetectTextFileEncoding(FileStream InputFileStream, Encoding DefaultEncoding, long HeuristicSampleSize) {
                if (InputFileStream == null)
                    throw new ArgumentNullException("Must provide a valid Filestream!", "InputFileStream");

                if (!InputFileStream.CanRead)
                    throw new ArgumentException("Provided file stream is not readable!", "InputFileStream");

                if (!InputFileStream.CanSeek)
                    throw new ArgumentException("Provided file stream cannot seek!", "InputFileStream");

                Encoding encodingFound = null;

                long originalPos = InputFileStream.Position;

                InputFileStream.Position = 0;


                //First read only what we need for BOM detection

                byte[] bomBytes = new byte[InputFileStream.Length > 4 ? 4 : InputFileStream.Length];
                InputFileStream.Read(bomBytes, 0, bomBytes.Length);

                encodingFound = DetectBOMBytes(bomBytes);

                if (encodingFound != null) {
                    InputFileStream.Position = originalPos;
                    return encodingFound;
                }


                //BOM Detection failed, going for heuristics now.
                //  create sample byte array and populate it
                byte[] sampleBytes = new byte[HeuristicSampleSize > InputFileStream.Length ? InputFileStream.Length : HeuristicSampleSize];
                Array.Copy(bomBytes, sampleBytes, bomBytes.Length);
                if (InputFileStream.Length > bomBytes.Length)
                    InputFileStream.Read(sampleBytes, bomBytes.Length, sampleBytes.Length - bomBytes.Length);
                InputFileStream.Position = originalPos;

                //test byte array content
                encodingFound = DetectUnicodeInByteSampleByHeuristics(sampleBytes);

                if (encodingFound != null)
                    return encodingFound;
                else
                    return DefaultEncoding;
            }

            public static Encoding DetectTextByteArrayEncoding(byte[] TextData, Encoding DefaultEncoding) {
                if (TextData == null)
                    throw new ArgumentNullException("Must provide a valid text data byte array!", "TextData");

                Encoding encodingFound = null;

                encodingFound = DetectBOMBytes(TextData);

                if (encodingFound != null) {
                    return encodingFound;
                } else {
                    //test byte array content
                    encodingFound = DetectUnicodeInByteSampleByHeuristics(TextData);

                    if (encodingFound != null)
                        return encodingFound;
                    else
                        return DefaultEncoding;
                }


            }

            public static Encoding DetectBOMBytes(byte[] BOMBytes) {
                if (BOMBytes == null)
                    throw new ArgumentNullException("Must provide a valid BOM byte array!", "BOMBytes");

                if (BOMBytes.Length < 2)
                    return null;

                if (BOMBytes[0] == 0xff
                    && BOMBytes[1] == 0xfe
                    && (BOMBytes.Length < 4
                        || BOMBytes[2] != 0
                        || BOMBytes[3] != 0
                        )
                    )
                    return Encoding.Unicode;

                if (BOMBytes[0] == 0xfe
                    && BOMBytes[1] == 0xff
                    )
                    return Encoding.BigEndianUnicode;

                if (BOMBytes.Length < 3)
                    return null;

                if (BOMBytes[0] == 0xef && BOMBytes[1] == 0xbb && BOMBytes[2] == 0xbf)
                    return Encoding.UTF8;

                if (BOMBytes[0] == 0x2b && BOMBytes[1] == 0x2f && BOMBytes[2] == 0x76)
                    return Encoding.UTF7;

                if (BOMBytes.Length < 4)
                    return null;

                if (BOMBytes[0] == 0xff && BOMBytes[1] == 0xfe && BOMBytes[2] == 0 && BOMBytes[3] == 0)
                    return Encoding.UTF32;

                if (BOMBytes[0] == 0 && BOMBytes[1] == 0 && BOMBytes[2] == 0xfe && BOMBytes[3] == 0xff)
                    return Encoding.GetEncoding(12001);

                return null;
            }

            public static Encoding DetectUnicodeInByteSampleByHeuristics(byte[] SampleBytes) {
                long oddBinaryNullsInSample = 0;
                long evenBinaryNullsInSample = 0;
                long suspiciousUTF8SequenceCount = 0;
                long suspiciousUTF8BytesTotal = 0;
                long likelyUSASCIIBytesInSample = 0;

                //Cycle through, keeping count of binary null positions, possible UTF-8 
                //  sequences from upper ranges of Windows-1252, and probable US-ASCII 
                //  character counts.

                long currentPos = 0;
                int skipUTF8Bytes = 0;

                while (currentPos < SampleBytes.Length) {
                    //binary null distribution
                    if (SampleBytes[currentPos] == 0) {
                        if (currentPos % 2 == 0)
                            evenBinaryNullsInSample++;
                        else
                            oddBinaryNullsInSample++;
                    }

                    //likely US-ASCII characters
                    if (IsCommonUSASCIIByte(SampleBytes[currentPos]))
                        likelyUSASCIIBytesInSample++;

                    //suspicious sequences (look like UTF-8)
                    if (skipUTF8Bytes == 0) {
                        int lengthFound = DetectSuspiciousUTF8SequenceLength(SampleBytes, currentPos);

                        if (lengthFound > 0) {
                            suspiciousUTF8SequenceCount++;
                            suspiciousUTF8BytesTotal += lengthFound;
                            skipUTF8Bytes = lengthFound - 1;
                        }
                    } else {
                        skipUTF8Bytes--;
                    }

                    currentPos++;
                }

                //1: UTF-16 LE - in english / european environments, this is usually characterized by a 
                //  high proportion of odd binary nulls (starting at 0), with (as this is text) a low 
                //  proportion of even binary nulls.
                //  The thresholds here used (less than 20% nulls where you expect non-nulls, and more than
                //  60% nulls where you do expect nulls) are completely arbitrary.

                if (((evenBinaryNullsInSample * 2.0) / SampleBytes.Length) < 0.2
                    && ((oddBinaryNullsInSample * 2.0) / SampleBytes.Length) > 0.6
                    )
                    return Encoding.Unicode;


                //2: UTF-16 BE - in english / european environments, this is usually characterized by a 
                //  high proportion of even binary nulls (starting at 0), with (as this is text) a low 
                //  proportion of odd binary nulls.
                //  The thresholds here used (less than 20% nulls where you expect non-nulls, and more than
                //  60% nulls where you do expect nulls) are completely arbitrary.

                if (((oddBinaryNullsInSample * 2.0) / SampleBytes.Length) < 0.2
                    && ((evenBinaryNullsInSample * 2.0) / SampleBytes.Length) > 0.6
                    )
                    return Encoding.BigEndianUnicode;


                //3: UTF-8 - Martin D�rst outlines a method for detecting whether something CAN be UTF-8 content 
                //  using regexp, in his w3c.org unicode FAQ entry: 
                //  http://www.w3.org/International/questions/qa-forms-utf-8
                //  adapted here for C#.
                string potentiallyMangledString = Encoding.ASCII.GetString(SampleBytes);
                Regex UTF8Validator = new Regex(@"\A("
                    + @"[\x09\x0A\x0D\x20-\x7E]"
                    + @"|[\xC2-\xDF][\x80-\xBF]"
                    + @"|\xE0[\xA0-\xBF][\x80-\xBF]"
                    + @"|[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}"
                    + @"|\xED[\x80-\x9F][\x80-\xBF]"
                    + @"|\xF0[\x90-\xBF][\x80-\xBF]{2}"
                    + @"|[\xF1-\xF3][\x80-\xBF]{3}"
                    + @"|\xF4[\x80-\x8F][\x80-\xBF]{2}"
                    + @")*\z");
                if (UTF8Validator.IsMatch(potentiallyMangledString)) {
                    //Unfortunately, just the fact that it CAN be UTF-8 doesn't tell you much about probabilities.
                    //If all the characters are in the 0-127 range, no harm done, most western charsets are same as UTF-8 in these ranges.
                    //If some of the characters were in the upper range (western accented characters), however, they would likely be mangled to 2-byte by the UTF-8 encoding process.
                    // So, we need to play stats.

                    // The "Random" likelihood of any pair of randomly generated characters being one 
                    //   of these "suspicious" character sequences is:
                    //     128 / (256 * 256) = 0.2%.
                    //
                    // In western text data, that is SIGNIFICANTLY reduced - most text data stays in the <127 
                    //   character range, so we assume that more than 1 in 500,000 of these character 
                    //   sequences indicates UTF-8. The number 500,000 is completely arbitrary - so sue me.
                    //
                    // We can only assume these character sequences will be rare if we ALSO assume that this
                    //   IS in fact western text - in which case the bulk of the UTF-8 encoded data (that is 
                    //   not already suspicious sequences) should be plain US-ASCII bytes. This, I 
                    //   arbitrarily decided, should be 80% (a random distribution, eg binary data, would yield 
                    //   approx 40%, so the chances of hitting this threshold by accident in random data are 
                    //   VERY low). 

                    if ((suspiciousUTF8SequenceCount * 500000.0 / SampleBytes.Length >= 1) //suspicious sequences
                        && (
                        //all suspicious, so cannot evaluate proportion of US-Ascii
                               SampleBytes.Length - suspiciousUTF8BytesTotal == 0
                               ||
                               likelyUSASCIIBytesInSample * 1.0 / (SampleBytes.Length - suspiciousUTF8BytesTotal) >= 0.8
                           )
                        )
                        return Encoding.UTF8;
                }

                return null;
            }

            private static bool IsCommonUSASCIIByte(byte testByte) {
                if (testByte == 0x0A //lf
                    || testByte == 0x0D //cr
                    || testByte == 0x09 //tab
                    || (testByte >= 0x20 && testByte <= 0x2F) //common punctuation
                    || (testByte >= 0x30 && testByte <= 0x39) //digits
                    || (testByte >= 0x3A && testByte <= 0x40) //common punctuation
                    || (testByte >= 0x41 && testByte <= 0x5A) //capital letters
                    || (testByte >= 0x5B && testByte <= 0x60) //common punctuation
                    || (testByte >= 0x61 && testByte <= 0x7A) //lowercase letters
                    || (testByte >= 0x7B && testByte <= 0x7E) //common punctuation
                    )
                    return true;
                else
                    return false;
            }

            private static int DetectSuspiciousUTF8SequenceLength(byte[] SampleBytes, long currentPos) {
                int lengthFound = 0;

                if (SampleBytes.Length >= currentPos + 1
                    && SampleBytes[currentPos] == 0xC2
                    ) {
                    if (SampleBytes[currentPos + 1] == 0x81
                        || SampleBytes[currentPos + 1] == 0x8D
                        || SampleBytes[currentPos + 1] == 0x8F
                        )
                        lengthFound = 2;
                    else if (SampleBytes[currentPos + 1] == 0x90
                        || SampleBytes[currentPos + 1] == 0x9D
                        )
                        lengthFound = 2;
                    else if (SampleBytes[currentPos + 1] >= 0xA0
                        && SampleBytes[currentPos + 1] <= 0xBF
                        )
                        lengthFound = 2;
                } else if (SampleBytes.Length >= currentPos + 1
                      && SampleBytes[currentPos] == 0xC3
                      ) {
                    if (SampleBytes[currentPos + 1] >= 0x80
                        && SampleBytes[currentPos + 1] <= 0xBF
                        )
                        lengthFound = 2;
                } else if (SampleBytes.Length >= currentPos + 1
                      && SampleBytes[currentPos] == 0xC5
                      ) {
                    if (SampleBytes[currentPos + 1] == 0x92
                        || SampleBytes[currentPos + 1] == 0x93
                        )
                        lengthFound = 2;
                    else if (SampleBytes[currentPos + 1] == 0xA0
                        || SampleBytes[currentPos + 1] == 0xA1
                        )
                        lengthFound = 2;
                    else if (SampleBytes[currentPos + 1] == 0xB8
                        || SampleBytes[currentPos + 1] == 0xBD
                        || SampleBytes[currentPos + 1] == 0xBE
                        )
                        lengthFound = 2;
                } else if (SampleBytes.Length >= currentPos + 1
                      && SampleBytes[currentPos] == 0xC6
                      ) {
                    if (SampleBytes[currentPos + 1] == 0x92)
                        lengthFound = 2;
                } else if (SampleBytes.Length >= currentPos + 1
                      && SampleBytes[currentPos] == 0xCB
                      ) {
                    if (SampleBytes[currentPos + 1] == 0x86
                        || SampleBytes[currentPos + 1] == 0x9C
                        )
                        lengthFound = 2;
                } else if (SampleBytes.Length >= currentPos + 2
                      && SampleBytes[currentPos] == 0xE2
                      ) {
                    if (SampleBytes[currentPos + 1] == 0x80) {
                        if (SampleBytes[currentPos + 2] == 0x93
                            || SampleBytes[currentPos + 2] == 0x94
                            )
                            lengthFound = 3;
                        if (SampleBytes[currentPos + 2] == 0x98
                            || SampleBytes[currentPos + 2] == 0x99
                            || SampleBytes[currentPos + 2] == 0x9A
                            )
                            lengthFound = 3;
                        if (SampleBytes[currentPos + 2] == 0x9C
                            || SampleBytes[currentPos + 2] == 0x9D
                            || SampleBytes[currentPos + 2] == 0x9E
                            )
                            lengthFound = 3;
                        if (SampleBytes[currentPos + 2] == 0xA0
                            || SampleBytes[currentPos + 2] == 0xA1
                            || SampleBytes[currentPos + 2] == 0xA2
                            )
                            lengthFound = 3;
                        if (SampleBytes[currentPos + 2] == 0xA6)
                            lengthFound = 3;
                        if (SampleBytes[currentPos + 2] == 0xB0)
                            lengthFound = 3;
                        if (SampleBytes[currentPos + 2] == 0xB9
                            || SampleBytes[currentPos + 2] == 0xBA
                            )
                            lengthFound = 3;
                    } else if (SampleBytes[currentPos + 1] == 0x82
                          && SampleBytes[currentPos + 2] == 0xAC
                          )
                        lengthFound = 3;
                    else if (SampleBytes[currentPos + 1] == 0x84
                        && SampleBytes[currentPos + 2] == 0xA2
                        )
                        lengthFound = 3;
                }

                return lengthFound;
            }

        }

		#endregion METHODS
	}
}
