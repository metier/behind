﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Xml;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Configuration;
using jlib.db;
using jlib.functions;

namespace jlib.net
{
	public class JWebException : IHttpModule
	{

		public void Init(System.Web.HttpApplication Application)
		{
            Application.Error += OnError;
        }

		public void Dispose()
		{
        }

		protected virtual void OnError(object sender, EventArgs args)
		{
            HttpApplication app = (HttpApplication)sender;
            Handler ueh = new Handler();
            ueh.HandleException(app.Server.GetLastError(), false);
        }

        public class Handler 
		{

            private System.Collections.Specialized.NameValueCollection _ResultCollection = new System.Collections.Specialized.NameValueCollection();
            private string _strException = "";
            private string _strExceptionType = "";

            private string _strViewstate = "";
            private const string _strViewstateKey = "__VIEWSTATE";
            private const string _strRootException = "System.Web.HttpUnhandledException";
            private const string _strRootWsException = "System.Web.Services.Protocols.SoapException";

			public HttpContext Context { get { return HttpContext.Current; } }
			public HttpRequest Request { get { return this.Context.Request; } }
			public HttpResponse Response { get { return this.Context.Response; } }
			public HttpServerUtility Server { get { return this.Context.Server; } }
			public System.Web.SessionState.HttpSessionState Session { get { return this.Context.Session; } }
			public HttpApplicationState Application { get { return this.Context.Application; } }

            /// <summary>
            /// turns a single stack frame object into an informative string
            /// </summary>
			private string StackFrameToString(StackFrame sf)
			{
                StringBuilder sb = new StringBuilder();
                int intParam = 0;
                MemberInfo mi = sf.GetMethod();

                var _with1 = sb;
                //-- build method name
                _with1.Append("   ");
                if(mi.DeclaringType == null){
                    _with1.Append("MethidInfo.DeclaringType is null");                
                }else{
                    _with1.Append(mi.DeclaringType.Namespace);
                    _with1.Append(".");
                    _with1.Append(mi.DeclaringType.Name);                    
                }
                _with1.Append(".");
                _with1.Append(mi.Name);

                //-- build method params
                _with1.Append("(");
                intParam = 0;
				foreach (ParameterInfo param in sf.GetMethod().GetParameters())
				{
                    intParam += 1;
                    if (intParam > 1)
                        _with1.Append(", ");
                    _with1.Append(param.Name);
                    _with1.Append(" As ");
                    _with1.Append(param.ParameterType.Name);
                }
                _with1.Append(")");
                _with1.Append(Environment.NewLine);

                //-- if source code is available, append location info
                _with1.Append("       ");
				if (sf.GetFileName() == null || sf.GetFileName().Length == 0)
				{
                    _with1.Append("(unknown file)");
                    //-- native code offset is always available
                    _with1.Append(": N ");
                    _with1.Append(string.Format("{0:#00000}", sf.GetNativeOffset()));

				}
				else
				{
                    _with1.Append(System.IO.Path.GetFileName(sf.GetFileName()));
                    _with1.Append(": line ");
                    _with1.Append(string.Format("{0:#0000}", sf.GetFileLineNumber()));
                    _with1.Append(", col ");
                    _with1.Append(string.Format("{0:#00}", sf.GetFileColumnNumber()));
                    //-- if IL is available, append IL location info
					if (sf.GetILOffset() != StackFrame.OFFSET_UNKNOWN)
					{
                        _with1.Append(", IL ");
                        _with1.Append(string.Format("{0:#0000}", sf.GetILOffset()));
                    }
                }
                _with1.Append(Environment.NewLine);
                return sb.ToString();
            }

            /// <summary>
            /// enhanced stack trace generator
            /// </summary>
			private string EnhancedStackTrace(StackTrace st, string strSkipClassName = "")
			{
                int intFrame = 0;

                StringBuilder sb = new StringBuilder();

                sb.Append(Environment.NewLine);
                sb.Append("---- Stack Trace ----");
                sb.Append(Environment.NewLine);

				for (intFrame = 0; intFrame <= st.FrameCount - 1; intFrame++)
				{
                    StackFrame sf = st.GetFrame(intFrame);
                    MemberInfo mi = sf.GetMethod();

                    if (!string.IsNullOrEmpty(strSkipClassName) && (mi.DeclaringType==null || mi.DeclaringType.Name.IndexOf(strSkipClassName) > -1)) {
                        //-- don't include frames with this name
                    } else {
                        sb.Append(StackFrameToString(sf));
                    }
                }
                sb.Append(Environment.NewLine);

                return sb.ToString();
            }

            /// <summary>
            /// enhanced stack trace generator, using existing exception as start point
            /// </summary>
			private string EnhancedStackTrace(Exception ex)
			{
                return EnhancedStackTrace(new StackTrace(ex, true));
            }

            /// <summary>
            /// enhanced stack trace generator, using current execution as start point
            /// </summary>
			private string EnhancedStackTrace()
			{
                return EnhancedStackTrace(new StackTrace(true), "JUnhandledException");
            }


            /// <summary>
            /// ASP.Net web-service specific exception handler, to be called from UehSoapExtension
            /// </summary>
            /// <returns>
            /// string with "&lt;detail&gt;&lt;/detail&gt;" XML element, suitable for insertion into SOAP message
            /// </returns>
            /// <remarks>
            /// existing SOAP detail message, prior to insertion, looks like:
            /// 
            ///   &lt;soap:Fault&gt;
            ///     &lt;faultcode&gt;soap:Server&lt;/faultcode&gt;
            ///     &lt;faultstring&gt;Server was unable to process request.&lt;/faultstring&gt;
            ///     &lt;detail /&gt;    &lt;==  
            ///   &lt;/soap:Fault&gt;
            /// </remarks>
			public string HandleWebServiceException(System.Web.Services.Protocols.SoapMessage sm)
			{
                HandleException(sm.Exception, true);

                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                System.Xml.XmlNode DetailNode = doc.CreateNode(XmlNodeType.Element, SoapException.DetailElementName.Name, SoapException.DetailElementName.Namespace);

                System.Xml.XmlNode TypeNode = doc.CreateNode(XmlNodeType.Element, "ExceptionType", SoapException.DetailElementName.Namespace);
                TypeNode.InnerText = _strExceptionType;
                DetailNode.AppendChild(TypeNode);

                System.Xml.XmlNode MessageNode = doc.CreateNode(XmlNodeType.Element, "ExceptionMessage", SoapException.DetailElementName.Namespace);
                MessageNode.InnerText = sm.Exception.Message;
                DetailNode.AppendChild(MessageNode);

                System.Xml.XmlNode InfoNode = doc.CreateNode(XmlNodeType.Element, "ExceptionInfo", SoapException.DetailElementName.Namespace);
                InfoNode.InnerText = _strException;
                DetailNode.AppendChild(InfoNode);

                return DetailNode.OuterXml.ToString();
            }

            /// <summary>
            /// ASP.Net exception handler, to be called from UehHttpModule
            /// </summary>
            public void HandleException(Exception ex, bool bDisableLogToUIIfEnabled) {
                HandleException(ex, bDisableLogToUIIfEnabled, false);
            }
            public void HandleException(Exception ex, bool bDisableLogToUIIfEnabled, bool bDisableSmartRedirect)
			{
                //-- don't bother us with debug exceptions (eg those running on localhost)
				if (convert.cBool(getConfigSetting("IgnoreDebug")))
				{
                    if (System.Diagnostics.Debugger.IsAttached)
                        return;
					string strHost = this.Request.Url.Host.ToLower();
					if (strHost == "localhost" | strHost == "127.0.0.1" | this.Request.ServerVariables["REMOTE_ADDR"] == "127.0.0.1" | this.Request.ServerVariables["REMOTE_HOST"] == "127.0.0.1" | strHost == this.Server.MachineName | this.Request.ServerVariables["LOCAL_ADDR"] == this.Request.ServerVariables["REMOTE_ADDR"] | this.Request.ServerVariables["LOCAL_ADDR"] == this.Request.ServerVariables["REMOTE_HOST"])
					{
                        return;
                    }
                }

                //-- turn the exception into an informative string
				try
				{
                    _strException = ExceptionToString(ex);
                    _strExceptionType = ex.GetType().FullName;
                    //-- ignore root exceptions
					if (_strExceptionType == _strRootException | _strExceptionType == _strRootWsException)
					{
						if ((ex.InnerException != null))
						{
                            _strExceptionType = ex.InnerException.GetType().FullName;
                        }
                    }
				}
				catch (Exception e)
				{
                    _strException = "Error '" + e.Message + "' while generating exception string";
                }

                //-- some exceptions should be ignored: ones that match this regex
                //-- note that we are using the entire full-text string of the exception to test regex against
                //-- so any part of text can match.
				if ((getConfigSetting("IgnoreRegex") != null) && getConfigSetting("IgnoreRegex").Length > 0)
				{
					if (Regex.IsMatch(_strException, getConfigSetting("IgnoreRegex"), RegexOptions.IgnoreCase))
					{
                        return;
                    }
                }

                string sRealUrl = "";
                string sQueryString = "";

                if (!bDisableSmartRedirect) {

                    if (string.IsNullOrEmpty(sRealUrl) && System.Web.HttpUtility.UrlDecode(this.Request.ServerVariables["QUERY_STRING"]).StartsWith("404;")) {
                        sRealUrl = System.Web.HttpUtility.UrlDecode(this.Request.ServerVariables["QUERY_STRING"]);
                    sQueryString = "";

                    if (sRealUrl.IndexOf("?") > -1) {
                        sQueryString = jlib.functions.parse.inner_substring(sRealUrl, "?", null, null, null);
                        sRealUrl = "/" + jlib.functions.parse.inner_substring(sRealUrl, "://", "/", "?", null);
                    } else {
                        sRealUrl = "/" + jlib.functions.parse.inner_substring(sRealUrl, "://", "/", null, null);
                    }
                }

                if (string.IsNullOrEmpty(sRealUrl)) {
                        sRealUrl = this.Request.Url.AbsolutePath;
                        sQueryString = this.Request.Url.Query;
                }


                if (convert.cBool(getConfigSetting("AttemptUrlAliasLookup")) && sRealUrl.ToLower().IndexOf(".jpg") == -1 && sRealUrl.ToLower().IndexOf(".gif") == -1) {
                    DataTable oDT = jlib.db.sqlbuilder.executeSelect("d_url_alias", "url", sRealUrl + jlib.functions.convert.cStr((string.IsNullOrEmpty(sQueryString) ? "" : "?" + sQueryString)));
                    if (oDT.Rows.Count > 0) {
                            this.Server.ClearError();
                            this.Response.Redirect(jlib.functions.convert.cStr(oDT.Rows[0]["redirect_url"]));
                        return;
                    }
                }
                }

                //-- log this error to various locations
				try
				{
                    //-- event logging takes < 100ms
                    bool bLog = true;
                    if ((getConfigSetting("IgnoreSaving") != null) && getConfigSetting("IgnoreSaving").Length > 0 && Regex.IsMatch(_strException, getConfigSetting("IgnoreSaving"), RegexOptions.IgnoreCase)) bLog = false;
                    else if ((getConfigSetting("IgnoreSavingURL") != null) && getConfigSetting("IgnoreSavingURL").Length > 0 && Regex.IsMatch(WebCurrentUrl(), getConfigSetting("IgnoreSavingURL"), RegexOptions.IgnoreCase)) bLog = false;
                    
					if (bLog)
					{
                        if (convert.cBool(getConfigSetting("LogToEventLog"))) ExceptionToEventLog();
                        if (convert.cBool(getConfigSetting("LogToFile"))) ExceptionToFile();
                        if (convert.cBool(getConfigSetting("LogToEmail"))) ExceptionToEmail();
                    }
				}
				catch (Exception e)
				{
                    //-- generic catch because any exceptions inside the UEH
                    //-- will cause the code to terminate immediately
                }

                //-- display message to the user
                if (!bDisableLogToUIIfEnabled)
				{
                    if (convert.cBool(getConfigSetting("LogToUI"))) {
                        ExceptionToPage();
                    } else if (!bDisableSmartRedirect) {
                        if (getConfigSetting("404_Redirect_URL") != "" && this.Context.Error != null && this.Context.Error.Message.IndexOf("does not exist") > 0 && this.Response.StatusCode == 200) {
                            this.Context.Items["RealUrl"] = sRealUrl + jlib.functions.convert.cStr((string.IsNullOrEmpty(sQueryString) ? "" : "?" + sQueryString));
                            this.Context.ClearError();
                            this.Response.StatusCode = 404;

                            if (getConfigSetting("TransferMethod").Equals("Redirect", StringComparison.CurrentCultureIgnoreCase)) this.Response.Redirect(getConfigSetting("404_Redirect_URL"));
                            else this.Server.Transfer(getConfigSetting("404_Redirect_URL"));

                        } else if (getConfigSetting("500_Redirect_URL") != "" && this.Response.StatusCode == 200) {
                            this.Context.Items["RealUrl"] = sRealUrl + jlib.functions.convert.cStr((string.IsNullOrEmpty(sQueryString) ? "" : "?" + sQueryString));
                            this.Context.ClearError();
                            this.Response.StatusCode = 500;

                            if (getConfigSetting("TransferMethod").Equals("Redirect", StringComparison.CurrentCultureIgnoreCase)) this.Response.Redirect(getConfigSetting("500_Redirect_URL"));
                            else this.Server.Transfer(getConfigSetting("500_Redirect_URL"));
                    }
                }
            }
			}

            /// <summary>
            /// turns exception into a formatted string suitable for display to a (technical) user
            /// </summary>
			private string FormatExceptionForUser()
			{
                StringBuilder sb = new StringBuilder();
                string strBullet = "•";

                var _with2 = sb;
                _with2.Append(Environment.NewLine);
                _with2.Append("The following information about the error was automatically captured: ");
                _with2.Append(Environment.NewLine);
                _with2.Append(Environment.NewLine);
				if (convert.cBool(getConfigSetting("LogToEventLog")))
				{
                    _with2.Append(" ");
                    _with2.Append(strBullet);
                    _with2.Append(" ");
					if (string.IsNullOrEmpty(_ResultCollection["LogToEventLog"]))
					{
                        _with2.Append("an event was written to the application log");
					}
					else
					{
                        _with2.Append("an event could NOT be written to the application log due to an error:");
                        _with2.Append(Environment.NewLine);
                        _with2.Append("   '");
                        _with2.Append(_ResultCollection["LogToEventLog"]);
                        _with2.Append("'");
                        _with2.Append(Environment.NewLine);
                    }
                    _with2.Append(Environment.NewLine);
                }
				if (convert.cBool(getConfigSetting("LogToFile")))
				{
                    _with2.Append(" ");
                    _with2.Append(strBullet);
                    _with2.Append(" ");
					if (string.IsNullOrEmpty(_ResultCollection["LogToFile"]))
					{
                        _with2.Append("details were written to a text log at:");
                        _with2.Append(Environment.NewLine);
                        _with2.Append("   ");
                        _with2.Append(getConfigSetting("PathLogFile"));
                        _with2.Append(Environment.NewLine);
					}
					else
					{
                        _with2.Append("details could NOT be written to the text log due to an error:");
                        _with2.Append(Environment.NewLine);
                        _with2.Append("   '");
                        _with2.Append(_ResultCollection["LogToFile"]);
                        _with2.Append("'");
                        _with2.Append(Environment.NewLine);
                    }
                }
				if (convert.cBool(getConfigSetting("LogToEmail")))
				{
                    _with2.Append(" ");
                    _with2.Append(strBullet);
                    _with2.Append(" ");
					if (string.IsNullOrEmpty(_ResultCollection["LogToEmail"]))
					{
                        _with2.Append("an email was sent to: ");
                        _with2.Append(getConfigSetting("EmailTo"));
                        _with2.Append(Environment.NewLine);
					}
					else
					{
                        _with2.Append("email could NOT be sent due to an error:");
                        _with2.Append(Environment.NewLine);
                        _with2.Append("   '");
                        _with2.Append(_ResultCollection["LogToEmail"]);
                        _with2.Append("'");
                        _with2.Append(Environment.NewLine);
                    }
                }
                _with2.Append(Environment.NewLine);
                _with2.Append(Environment.NewLine);
                _with2.Append("Detailed error information follows:");
                _with2.Append(Environment.NewLine);
                _with2.Append(Environment.NewLine);
                _with2.Append(_strException);
                return sb.ToString();
            }

            /// <summary>
            /// write an exception to the Windows NT event log
            /// </summary>
			private bool ExceptionToEventLog()
			{
				try
				{
                    System.Diagnostics.EventLog.WriteEntry(WebCurrentUrl(), Environment.NewLine + _strException, EventLogEntryType.Error);
                    return true;
				}
				catch (Exception ex)
				{
                    _ResultCollection.Add("LogToEventLog", ex.Message);
                }
                return false;
            }

            /// <summary>
            /// replace generic constants in display strings with specific values
            /// </summary>
			private string FormatDisplayString(string strOutput)
			{
                string strTemp = null;
				if (strOutput == null)
				{
                    strTemp = "";
				}
				else
				{
                    strTemp = strOutput;
                }
                strTemp = strTemp.Replace("(app)", getConfigSetting("AppName"));
                strTemp = strTemp.Replace("(contact)", getConfigSetting("ContactInfo"));
                return strTemp;
            }

            /// <summary>
            /// writes text plus newline to http response stream
            /// </summary>
			private void WriteLine(string strAny)
			{
				this.Response.Write(strAny);
				this.Response.Write(Environment.NewLine);
            }

            /// <summary>
            /// writes current exception info to a web page
            /// </summary>

			private void ExceptionToPage()
			{
				System.Web.HttpResponse oResponse = this.Response;

				if (this.Context.Error != null && this.Response.StatusCode == 200)
				{
					if (this.Context.Error.Message.IndexOf("does not exist") > 0) this.Response.StatusCode = 404;
					else this.Response.StatusCode = 500;
                }
                
                oResponse.Clear();
                oResponse.ClearContent();

                oResponse.Write("<HTML><HEAD><TITLE>Website problem</TITLE>");
                oResponse.Write("<STYLE>body {font-family:\"Verdana\";font-weight:normal;font-size: .7em;color:black; background-color:white;}");
                oResponse.Write("b {font-family:\"Verdana\";font-weight:bold;color:black;margin-top: -5px}");
                oResponse.Write("H1 { font-family:\"Verdana\";font-weight:normal;font-size:18pt;color:red }");
                oResponse.Write("H2 { font-family:\"Verdana\";font-weight:normal;font-size:14pt;color:maroon }");
                oResponse.Write("pre {font-family:\"Lucida Console\";font-size: .9em}");
                oResponse.Write("</STYLE>");
                oResponse.Write("</HEAD>");
                oResponse.Write("<BODY>");
                oResponse.Write("<H1>");
                oResponse.Write(FormatDisplayString("This website encountered an unexpected problem"));
                oResponse.Write("<hr width=100% size=1 color=silver></H1>");
                oResponse.Write("<H2>What Happened:</H2>");
                oResponse.Write("<BLOCKQUOTE>");
                oResponse.Write(FormatDisplayString("There was an unexpected error in this website. This may be due to a programming bug."));
                oResponse.Write("</BLOCKQUOTE>");
                oResponse.Write("<H2>How this will affect you:</H2>");
                oResponse.Write("<BLOCKQUOTE>");
                oResponse.Write(FormatDisplayString("The current page will not load."));
                oResponse.Write("</BLOCKQUOTE>");
                oResponse.Write("<H2>What you can do about it:</H2>");
                oResponse.Write("<BLOCKQUOTE>");
                oResponse.Write(FormatDisplayString("Press the Refresh button in your browser to try loading the page again. If this doesn't work after a few attempts, close your browser, navigate back to this website, and try repeating your last action. Try alternative methods of performing the same action. If problems persist, contact (contact)."));
                oResponse.Write("</BLOCKQUOTE>");
                oResponse.Write("<INPUT type=button value=\"More Info &gt;&gt;\" onclick=\"this.style.display='none'; document.getElementById('MoreInfo').style.display='block'\">");
                oResponse.Write("<DIV style='display:none;' id='MoreInfo'>");
                oResponse.Write("<H2>More info:</H2><TABLE width=\"100%\" bgcolor=\"#ffffcc\"><TR><TD><CODE><PRE>");
                oResponse.Write(FormatExceptionForUser());
                oResponse.Write("</PRE></CODE><TD><TR></DIV></BODY></HTML>");
                oResponse.Flush();
                //-- 
                //-- If you use Server.ClearError to clear the exception in Application_Error, 
                //-- the defaultredirect setting in the Web.config file will not redirect the user 
                //-- because the unhandled exception no longer exists. If you want the 
                //-- defaultredirect setting to properly redirect users, do not clear the exception 
                //-- in Application_Error.
                //--
				this.Server.ClearError();
                oResponse.End();
            }

            /// <summary>
            /// write current exception info to a text file; 
            /// requires write permissions for the target folder
            /// </summary>
			private bool ExceptionToFile()
			{
				System.IO.StreamWriter sw = null;
				try
				{
					sw = new System.IO.StreamWriter(io.MapPath(getConfigSetting("PathLogFile")), true);
					sw.Write(_strException);
					sw.WriteLine();
					sw.Close();
					return true;
				}
				catch (Exception ex)
				{
					_ResultCollection.Add("LogToFile", ex.Message);
				}
				finally
				{
					if ((sw != null))
					{
						sw.Close();
					}
				}
                return false;
            }

            private static NameValueCollection oModuleSettings = null;
			public string getConfigSetting(string sKey)
			{
				if (oModuleSettings == null)
				{
					try
					{
                        oModuleSettings = (NameValueCollection)ConfigurationManager.GetSection("UnhandledException");
					}
					catch (Exception) { oModuleSettings = new NameValueCollection(); }
				}
				string value = "";
				try { value = convert.cStr(oModuleSettings[sKey]); }
				catch { }

				return value;
            }
            /// <summary>
            /// send current exception info via email
            /// </summary>
			private bool ExceptionToEmail()
			{
                if (convert.cStr(getConfigSetting("EmailLogDSN")) != "") sqlbuilder.executeInsert(new ado_helper(convert.cStr(getConfigSetting("EmailLogDSN"))), "d_email_log", "email_from", getConfigSetting("EmailFrom"), "email_to", getConfigSetting("EmailTo"), "subject", "Unhandled ASP Exception notification - " + _strExceptionType, "body", _strException + (_strViewstate.Length > 0 ? "\n\n\n---------- VIEWSTATE -------------\n" + _strViewstate : ""), "is_html", false);
                return jlib.net.SMTP.sendEmail(getConfigSetting("EmailFrom"), getConfigSetting("EmailTo"), "Unhandled ASP Exception notification - " + _strExceptionType, _strException + (_strViewstate.Length > 0 ? "\n\n\n---------- VIEWSTATE -------------\n" + _strViewstate : ""), System.Configuration.ConfigurationManager.AppSettings["smtp.server"]);
            }


            /// <summary>
            /// exception-safe WindowsIdentity.GetCurrent retrieval; returns "domain\username"
            /// </summary>
            /// <remarks>
            /// per MS, this can sometimes randomly fail with "Access Denied" on NT4
            /// </remarks>
			private string CurrentWindowsIdentity()
			{
				try
				{
                    return System.Security.Principal.WindowsIdentity.GetCurrent().Name;
				}
				catch (Exception)
				{
                    return "";
                }
            }

            /// <summary>
            /// exception-safe System.Environment "domain\username" retrieval
            /// </summary>
			private string CurrentEnvironmentIdentity()
			{
				try
				{
                    return System.Environment.UserDomainName + "\\" + System.Environment.UserName;
				}
				catch (Exception)
				{
                    return "";
                }
            }

            /// <summary>
            /// retrieve Process identity with fallback on error to safer method
            /// </summary>
			private string ProcessIdentity()
			{
                string strTemp = CurrentWindowsIdentity();
				if (string.IsNullOrEmpty(strTemp))
				{
                    return CurrentEnvironmentIdentity();
                }
                return strTemp;
            }

            /// <summary>
            /// returns current URL; "http://localhost:85/mypath/mypage.aspx?test=1&apples=bear"
            /// </summary>
			private string WebCurrentUrl()
			{
                string strUrl = null;
				var _with4 = this.Request.ServerVariables;
                strUrl = "http://" + _with4["server_name"];
                if (_with4["server_port"] != "80") strUrl += ":" + _with4["server_port"];

                strUrl += _with4["url"];
                if (convert.cStr(_with4["query_string"]).Length > 0) strUrl += "?" + _with4["query_string"];
                return strUrl;
            }

            /// <summary>
            /// returns brief summary info for all assemblies in the current AppDomain
            /// </summary>
			private string LogAssemblyDetails(StringBuilder sb)
			{
				//Header
				this.AppendSectionHeader(sb, "Assembly Details");
				this.AppendNameValue2(sb, "Assembly", "Version", "BuildDate");
				this.AppendNameValue2(sb, "---------------", "----------", "----------");

				//Values
				foreach (System.Reflection.Assembly a in AppDomain.CurrentDomain.GetAssemblies())
				{
					System.Collections.Specialized.NameValueCollection nvc = AssemblyAttribs(a);

                    //-- assemblies without versions are weird (dynamic?)
					if (nvc["Version"] != "0.0.0.0")
					{
						this.AppendNameValue2(sb, System.IO.Path.GetFileName(nvc["CodeBase"]), nvc["Version"], nvc["BuildDate"]);
                    }
                }

				//Ending
				this.AppendSectionEnding(sb);


                return sb.ToString();
            }

            /// <summary>
            /// returns more detailed information for a single assembly
            /// </summary>
			private string LogAssemblyDetails(StringBuilder sb, System.Reflection.Assembly a)
			{
                System.Collections.Specialized.NameValueCollection nvc = AssemblyAttribs(a);

				//Section
				this.AppendSectionHeader(sb, "Assembly Details");

				//Values
				try { this.AppendNameValue(sb, "Assembly Codebase", nvc["CodeBase"]); }catch { }
				try { this.AppendNameValue(sb, "Assembly Full Name", nvc["FullName"]); }catch { }
				try { this.AppendNameValue(sb, "Assembly Version", nvc["Version"]); }catch { }
				try { this.AppendNameValue(sb, "Assembly Build Date", nvc["BuildDate"]); }catch { }

				//Ending
				this.AppendSectionEnding(sb);

                return sb.ToString();
            }

            /// <summary>
            /// retrieve relevant assembly details for this exception, if possible
            /// </summary>
			private string AssemblyInfoToString(Exception ex)
			{
                //-- ex.source USUALLY contains the name of the assembly that generated the exception
                //-- at least, according to the MSDN documentation..
                System.Reflection.Assembly a = GetAssemblyFromName(ex.Source);
				if (a == null)
				{
					return LogAssemblyDetails(new StringBuilder());
                }
				else
				{
					return LogAssemblyDetails(new StringBuilder(), a);
            }
			}

            /// <summary>
            /// gather some system information that is helpful in diagnosing exceptions
            /// </summary>
			private string LogSystemInfo(StringBuilder sb, bool blnIncludeStackTrace = false)
			{
				//Header
				this.AppendSectionHeader(sb, "System Info");

				//Values
				this.AppendNameValue(sb, "Date", DateTime.Now);
				try { this.AppendNameValue(sb, "Machine Name", Environment.MachineName); } catch { }
				this.AppendNameValue(sb, "Process User", ProcessIdentity());
				this.AppendNameValue(sb, "Remote User", this.Request.ServerVariables["REMOTE_USER"]);
				this.AppendNameValue(sb, "Remote Address", this.Request.ServerVariables["REMOTE_ADDR"]);
				this.AppendNameValue(sb, "Remote Host", this.Request.ServerVariables["REMOTE_HOST"]);
				this.AppendNameValue(sb, "Url", WebCurrentUrl());
				this.AppendNameValue(sb, ".NET Runtime version", System.Environment.Version.ToString());
				try { this.AppendNameValue(sb, "Application Domain", System.AppDomain.CurrentDomain.FriendlyName); } catch { }

				//Ending
				this.AppendSectionEnding(sb);

                return sb.ToString();
            }

            /// <summary>
            /// translate an exception object to a formatted string, with additional system info
            /// </summary>
			public string ExceptionToString(Exception ex)
			{
                StringBuilder sb = new StringBuilder();

				this.LogException(sb, ex);
                //-- get ASP specific settings
				try
				{
					sb.Append(GetASPSettings());
                }
				catch (Exception e)
				{
					sb.Append(e.Message);
				}
				sb.Append(Environment.NewLine);

                return sb.ToString();
            }

            /// <summary>
            /// private version, called recursively for nested exceptions (inner, outer, etc)
            /// </summary>
			private string LogException(StringBuilder sb, Exception ex, bool blnIncludeSysInfo = true, string SectionName = "Exception")
			{
				if ((ex.InnerException != null))
				{
                    //-- sometimes the original exception is wrapped in a more relevant outer exception
                    //-- the detail exception is the "inner" exception
                    //-- see http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnbda/html/exceptdotnet.asp

                    //-- don't return the outer root ASP exception; it is redundant.
					if (ex.GetType().ToString() == _strRootException | ex.GetType().ToString() == _strRootWsException)
					{
						return this.LogException(sb, ex.InnerException);
                    }
					else
					{
						this.LogException(sb, ex.InnerException, false, "Outer Exception");
                }
				}

                //-- get general system and app information
                //-- we only really want to do this on the outermost exception in the stack
				if (blnIncludeSysInfo)
				{
					this.LogSystemInfo(sb);
					sb.Append(AssemblyInfoToString(ex));
					sb.Append(Environment.NewLine);
                }

				//Header
				this.AppendSectionHeader(sb, SectionName);

				//Values
				try { this.AppendNameValue(sb, "Exception Type", ex.GetType().FullName); } catch { }
				try { this.AppendNameValue(sb, "Exception Message", ex.Message); } catch { }
				try { this.AppendNameValue(sb, "Exception Source", ex.Source); } catch { }
				try { this.AppendNameValue(sb, "Exception Target Site", ex.TargetSite.Name); } catch { }
				try { this.AppendNameValue(sb, "Stack Trace", EnhancedStackTrace(ex)); } catch { }

                return sb.ToString();
            }

            /// <summary>
            /// exception-safe file attrib retrieval; we don't care if this fails
            /// </summary>
			private DateTime AssemblyLastWriteTime(System.Reflection.Assembly a)
			{
				try
				{
                    return System.IO.File.GetLastWriteTime(a.Location);
				}
				catch (Exception)
				{
                    return DateTime.MaxValue;
                }
            }

            /// <summary>
            /// returns build datetime of assembly, using calculated build time if possible, or filesystem time if not
            /// </summary>
			private DateTime AssemblyBuildDate(System.Reflection.Assembly a, bool blnForceFileDate = false)
			{

                System.Version v = a.GetName().Version;
                DateTime dt = default(DateTime);

				if (blnForceFileDate)
				{
                    dt = AssemblyLastWriteTime(a);
				}
				else
				{
                    dt = (jlib.functions.convert.cDate("01/01/2000")).AddDays(v.Build).AddSeconds(v.Revision * 2);
					if (TimeZone.IsDaylightSavingTime(dt, TimeZone.CurrentTimeZone.GetDaylightChanges(dt.Year)))
					{
                        dt = dt.AddHours(1);
                    }
					if (dt > DateTime.Now | v.Build < 730 | v.Revision == 0)
					{
                        dt = AssemblyLastWriteTime(a);
                    }
                }

                return dt;
            }

            /// <summary>
            /// returns string name / string value pair of all attribs for the specified assembly
            /// </summary>
            /// <remarks>
            /// note that Assembly* values are pulled from AssemblyInfo file in project folder
            ///
            /// Trademark       = AssemblyTrademark string
            /// Debuggable      = True
            /// GUID            = 7FDF68D5-8C6F-44C9-B391-117B5AFB5467
            /// CLSCompliant    = True
            /// Product         = AssemblyProduct string
            /// Copyright       = AssemblyCopyright string
            /// Company         = AssemblyCompany string
            /// Description     = AssemblyDescription string
            /// Title           = AssemblyTitle string
            /// </remarks>
			private NameValueCollection AssemblyAttribs(System.Reflection.Assembly a)
			{
                string Name = null;
                string Value = null;
                System.Collections.Specialized.NameValueCollection nvc = new System.Collections.Specialized.NameValueCollection();

				foreach (object attrib in a.GetCustomAttributes(false))
				{
                    Name = attrib.GetType().ToString();
                    Value = "";
					switch (Name)
					{
                        case "System.Diagnostics.DebuggableAttribute":
                            Name = "Debuggable";
                            Value = ((System.Diagnostics.DebuggableAttribute)attrib).IsJITTrackingEnabled.ToString();
                            break;
                        case "System.CLSCompliantAttribute":
                            Name = "CLSCompliant";
                            Value = ((System.CLSCompliantAttribute)attrib).IsCompliant.ToString();
                            break;
                        case "System.Runtime.InteropServices.GuidAttribute":
                            Name = "GUID";
                            Value = ((System.Runtime.InteropServices.GuidAttribute)attrib).Value.ToString();
                            break;
                        case "System.Reflection.AssemblyTrademarkAttribute":
                            Name = "Trademark";
                            Value = ((AssemblyTrademarkAttribute)attrib).Trademark.ToString();
                            break;
                        case "System.Reflection.AssemblyProductAttribute":
                            Name = "Product";
                            Value = ((AssemblyProductAttribute)attrib).Product.ToString();
                            break;
                        case "System.Reflection.AssemblyCopyrightAttribute":
                            Name = "Copyright";
                            Value = ((AssemblyCopyrightAttribute)attrib).Copyright.ToString();
                            break;
                        case "System.Reflection.AssemblyCompanyAttribute":
                            Name = "Company";
                            Value = ((AssemblyCompanyAttribute)attrib).Company.ToString();
                            break;
                        case "System.Reflection.AssemblyTitleAttribute":
                            Name = "Title";
                            Value = ((AssemblyTitleAttribute)attrib).Title.ToString();
                            break;
                        case "System.Reflection.AssemblyDescriptionAttribute":
                            Name = "Description";
                            Value = ((AssemblyDescriptionAttribute)attrib).Description.ToString();
                            break;
                        default:
                            break;
                        //Console.WriteLine(Name)
                    }
					if (!string.IsNullOrEmpty(Value))
					{
						if (string.IsNullOrEmpty(nvc[Name]))
						{
                            nvc.Add(Name, Value);
                        }
                    }
                }

                //-- add some extra values that are not in the AssemblyInfo, but nice to have
                var _with10 = nvc;
				try
				{
                    _with10.Add("CodeBase", a.CodeBase.Replace("file:///", ""));
				}
				catch (Exception ex)
				{
                    _with10.Add("CodeBase", ex.Message.ToString());
                }
				try
				{
                    _with10.Add("BuildDate", AssemblyBuildDate(a).ToString());
				}
				catch (Exception ex)
				{
                    _with10.Add("BuildDate", ex.Message.ToString());
                }
				try
				{
                    _with10.Add("Version", a.GetName().Version.ToString());
				}
				catch (Exception ex)
				{
                    _with10.Add("Version", ex.Message.ToString());
                }
				try
				{
                    _with10.Add("FullName", a.FullName);
				}
				catch (Exception ex)
				{
                    _with10.Add("FullName", ex.Message.ToString());
                }

                return nvc;
            }

            /// <summary>
            /// matches assembly by Assembly.GetName.Name; returns nothing if no match
            /// </summary>
			private System.Reflection.Assembly GetAssemblyFromName(string strAssemblyName)
			{
				foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
				{
                    if (a.GetName().Name == strAssemblyName) return a;
                }
                return null;
            }

            /// <summary>
            /// returns formatted string of all ASP.NET collections (QueryString, Form, Cookies, ServerVariables)
            /// </summary>
            private string GetASPSettings() {
                StringBuilder sb = new StringBuilder();

                const string strSuppressKeyPattern = "^ALL_HTTP|^ALL_RAW|VSDEBUGGER";


                //QueryString
                LogCollectionValues(sb, this.Request.QueryString, "QueryString");

                //Form
                LogCollectionValues(sb, this.Request.Form, "Form");

                //Input Stream
                this.LogRequestStream(sb, this.Request.InputStream);

                //Cookies
                LogCookieValues(sb, this.Request.Cookies);

                //Session
                LogSessionValues(sb, this.Session);

                //sb.Append(HttpVarsToString(this.Context.Cache));

                //Application Object
                //sb.Append(HttpVarsToString(this.Application));

                //Server Variables
                LogCollectionValues(sb, this.Request.ServerVariables, "ServerVariables", true, strSuppressKeyPattern);

                //Request Status
                this.LogRequestStatus(sb, this.Context);

                return sb.ToString();
            }

			private string LogRequestStatus(StringBuilder sb, HttpContext Context)
			{
				//Header
				this.AppendSectionHeader(sb, "Request Status");

				//Value
				this.AppendNameValue(sb, "Error Message", Context.Error.Message);
				this.AppendNameValue(sb, "Response Code", Context.Response.StatusCode);
				
				//Ending
				this.AppendSectionEnding(sb);

				return sb.ToString();
			}
		
			private string LogRequestStream(StringBuilder sb, System.IO.Stream stream)
			{
				//Header
				this.AppendSectionHeader(sb, "this.Request.InputStream");

				//Value
				this.AppendNameValue(sb, "", jlib.functions.convert.cStreamToString(stream));
				
				//Ending
				this.AppendSectionEnding(sb);

				return sb.ToString();
			}

            /// <summary>
            /// returns formatted string of all ASP.NET Cookies
            /// </summary>
			private string LogCookieValues(StringBuilder sb, HttpCookieCollection c)
			{
                if (c.Count == 0)
                    return "";

				//Header
				AppendSectionHeader(sb, "Cookies");

				//Values
				foreach (string strKey in c)
				{
					AppendNameValue(sb, strKey, c[strKey].Value);
                }

				//Ending
				this.AppendSectionEnding(sb);

                return sb.ToString();
            }

            /// <summary>
            /// returns formatted summary string of all ASP.NET app vars
            /// </summary>
			private string LogApplicationValues(StringBuilder sb, HttpApplicationState a)
			{
                if (a.Count == 0)
                    return "";

				//Header
				AppendSectionHeader(sb, "Application");

				//Values
				foreach (string strKey in a)
				{
					AppendNameValue(sb, strKey, a[strKey]);
                }

				//Ending
				this.AppendSectionEnding(sb);

                return sb.ToString();
            }

            /// <summary>
            /// returns formatted summary string of all ASP.NET Cache vars
            /// </summary>
			private string LogCacheValues(StringBuilder sb, System.Web.Caching.Cache c)
			{
                if (c.Count == 0)
                    return "";

				//Header
				AppendSectionHeader(sb, "Cache");

				//Values
				foreach (DictionaryEntry de in c)
				{
					AppendNameValue(sb, Convert.ToString(de.Key), de.Value);
                }

				//Ending
				this.AppendSectionEnding(sb);

                return sb.ToString();
            }

            /// <summary>
            /// returns formatted summary string of all ASP.NET Session vars
            /// </summary>
			private string LogSessionValues(StringBuilder sb, System.Web.SessionState.HttpSessionState s)
			{
                //-- sessions can be disabled
                if (s == null)
                    return "";
                if (s.Count == 0)
                    return "";

				//Header
				AppendSectionHeader(sb, "Session");

				//Values
				foreach (string strKey in s)
				{
					AppendNameValue(sb, strKey, s[strKey]);
                }

				//Ending
				this.AppendSectionEnding(sb);

                return sb.ToString();
            }

            /// <summary>
            /// returns formatted string of an arbitrary ASP.NET NameValueCollection
            /// </summary>
			private string LogCollectionValues(StringBuilder sb, System.Collections.Specialized.NameValueCollection nvc, string SectionName, bool blnSuppressEmpty = false, string strSuppressKeyPattern = "")
			{

                if (!nvc.HasKeys())
                    return "";

				//Header
				AppendSectionHeader(sb, SectionName);

                bool blnDisplay = false;

				//Values
				foreach (string strKey in nvc)
				{
                    blnDisplay = true;

					if (blnSuppressEmpty)
					{
                        blnDisplay = !string.IsNullOrEmpty(nvc[strKey]);
                    }

					if (strKey == _strViewstateKey)
					{
                        _strViewstate = nvc[strKey];
                        blnDisplay = false;
                    }

					if (blnDisplay && !string.IsNullOrEmpty(strSuppressKeyPattern))
					{
                        blnDisplay = !Regex.IsMatch(strKey, strSuppressKeyPattern);
                    }

					if (blnDisplay)
					{
						AppendNameValue(sb, strKey, nvc[strKey]);
                    }
                }

                sb.Append(Environment.NewLine);
                return sb.ToString();
            }

            /// <summary>
            /// attempts to coerce the value object using the .ToString method if possible, 
            /// then appends a formatted key/value string pair to a StringBuilder. 
            /// will display the type name if the object cannot be coerced.
            /// </summary>
			private void AppendNameValue(StringBuilder sb, string Key, object Value)
			{

                string strValue = null;
				if (Value == null)
				{
                    strValue = "(Nothing)";
				}
				else
				{
					try
					{
                        strValue = Value.ToString();
					}
					catch (Exception)
					{
                        strValue = "(" + Value.GetType().ToString() + ")";
                    }
                }

				AppendNameValue(sb, Key, strValue);
            }


            /// <summary>
            /// appends a formatted key/value string pair to a StringBuilder
            /// </summary>
			private void AppendNameValue(StringBuilder sb, string Name, string Value)
			{
				string label = Name.Str() + ":";

				sb.Append(string.Format("    {0, -30}{1}", label, Value));
				sb.AppendLine();
            }

			private void AppendNameValue2(StringBuilder sb, string Name, string Value1, string Value2)
			{
				string label = Name.Str() + ":";

				sb.Append(string.Format("    {0, -30} {1, -15} {2}", label, Value1, Value2));
				sb.AppendLine();
        }

			private void AppendSectionHeader(StringBuilder sb, string Name)
			{
				sb.AppendLine();
				sb.AppendLine("------------------------------------");
				sb.AppendLine("    " + Name.Str().ToUpper());
				sb.AppendLine("------------------------------------");
    }

			private void AppendSectionEnding(StringBuilder sb)
			{
				sb.AppendLine();
				sb.AppendLine();
}

		}

	}
}
