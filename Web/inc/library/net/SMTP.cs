using System;
using System.IO;
using System.Net.Mail;
using System.Collections.Generic;
using System.Collections;
using jlib.functions;
using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;


namespace jlib.net
{
	/// <summary>
	/// Summary description for SMTP.
	/// </summary>
	public class SMTP{
        
        public class EmailValidator {
            public EmailValidator() : this("noreply@bids.no") {}
            public EmailValidator(string mailFrom):this(mailFrom,25) {}
            public EmailValidator(string mailFrom, int port){               
                MailFrom = mailFrom;
                SmtpPort = port;
            }
            
            public bool CheckDomainName(string EmailOrDomain) {
                string domainName = this.GetDomainName(EmailOrDomain);
                try {
                    Dns.GetHostEntry(domainName);
                } catch (Exception) {
                    return false;
                }
                return true;
            }

            public bool CheckMailBox(string EmailAdress) {
                if (!EmailAdress.Contains("@") || this.CheckSyntax(EmailAdress)) {
                    foreach (string str in this.FindMXRecords(EmailAdress, false)) {
                        try {
                            IPEndPoint remoteEP = new IPEndPoint(Dns.GetHostEntry(str).AddressList[0], SmtpPort);
                            Socket s = new Socket(remoteEP.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                            s.Connect(remoteEP);
                            if (this.GetResponseCode(s) == 220) {
                                this.Senddata(s, string.Format("HELO {0}\r\n", Dns.GetHostName()));
                                if (this.GetResponseCode(s) == 250) {
                                    this.Senddata(s, string.Format("MAIL FROM:<{0}>\r\n", MailFrom));
                                    if (this.GetResponseCode(s) == 250) {
                                        this.Senddata(s, string.Format("RCPT TO:<{0}>\r\n", EmailAdress));
                                        if (this.GetResponseCode(s) == 250) {
                                            this.Senddata(s, "QUIT\r\n");
                                            s.Close();
                                            return true;
                                        }
                                        return false;
                                    }
                                }
                                return false;
                            }
                            s.Close();
                        } catch (Exception) {
                        }
                    }
                }
                return false;
            }

            public string CheckMailBoxError(string EmailAdress) {
                return CheckMailBoxError(EmailAdress, true);
            }
            public string CheckMailBoxError(string EmailAdress, bool attemptConnection) {
                if (EmailAdress.Contains("@") && !this.CheckSyntax(EmailAdress)) {
                    return "001 : Invalid email syntax";
                }
                string str = "003 : No MX records found";
                foreach (string str2 in this.FindMXRecords(EmailAdress, attemptConnection)) {
                    try {
                        if (!attemptConnection) return "";
                        IPEndPoint remoteEP = new IPEndPoint(Dns.GetHostEntry(str2).AddressList[0], SmtpPort);
                        Socket s = new Socket(remoteEP.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                        s.Connect(remoteEP);
                        str = this.GetResponse(s);
                        if (Convert.ToInt32(str.Substring(0, 3)) == 220) {
                            this.Senddata(s, string.Format("HELO {0}\r\n", Dns.GetHostName()));
                            str = this.GetResponse(s);
                            if (Convert.ToInt32(str.Substring(0, 3)) == 250) {
                                this.Senddata(s, string.Format("MAIL FROM:<{0}>\r\n", MailFrom));
                                str = this.GetResponse(s);
                                if (Convert.ToInt32(str.Substring(0, 3)) != 250) {
                                    return str;
                                }
                                this.Senddata(s, string.Format("RCPT TO:<{0}>\r\n", EmailAdress));
                                str = this.GetResponse(s);
                                if (Convert.ToInt32(str.Substring(0, 3)) == 250) {
                                    this.Senddata(s, "QUIT\r\n");
                                    s.Close();
                                    return "";
                                }
                            }
                            return str;
                        }
                    } catch (Exception e) {
                        if (str == "") {
                            str = "004 : Mx entry problem";
                        }
                    }
                }
                return str;
            }

            public bool CheckSMTP(string EmailAdress) {
                if (!EmailAdress.Contains("@") || this.CheckSyntax(EmailAdress)) {
                    foreach (string str in this.FindMXRecords(EmailAdress, false)) {
                        try {
                            IPEndPoint remoteEP = new IPEndPoint(Dns.GetHostEntry(str).AddressList[0], SmtpPort);
                            Socket s = new Socket(remoteEP.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                            s.Connect(remoteEP);
                            if (this.GetResponseCode(s) == 220) {
                                this.Senddata(s, string.Format("HELO {0}\r\n", Dns.GetHostName()));
                                if (this.GetResponseCode(s) == 250) {
                                    this.Senddata(s, "QUIT\r\n");
                                    s.Close();
                                    return true;
                                }
                            }
                            s.Close();
                        } catch (Exception) {
                        }
                    }
                }
                return false;
            }

            public bool CheckSyntax(string Mail_Adress) {
                Regex regex = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
                return regex.IsMatch(Mail_Adress);
            }

            public void Dispose() {
            }

            [DllImport("dnsapi", EntryPoint = "DnsQuery_W", CharSet = CharSet.Unicode, SetLastError = true, ExactSpelling = true)]
            private static extern int DnsQuery([MarshalAs(UnmanagedType.VBByRefStr)] ref string pszName, QueryTypes wType, QueryOptions options, int aipServers, ref IntPtr ppQueryResults, int pReserved);
            [DllImport("dnsapi", CharSet = CharSet.Auto, SetLastError = true)]
            private static extern void DnsRecordListFree(IntPtr pRecordList, int FreeType);
            public string[] FindMXRecords(string EmailOrDomain, bool addDomainIfError) {
                string domainName = this.GetDomainName(EmailOrDomain);
                ArrayList list = new ArrayList();
                try {
                    MXRecord record;
                    IntPtr zero = IntPtr.Zero;
                    IntPtr ptr = IntPtr.Zero;
                    if (Environment.OSVersion.Platform != PlatformID.Win32NT) {
                        throw new NotSupportedException();
                    }
                    int error = DnsQuery(ref domainName, QueryTypes.DNS_TYPE_MX, QueryOptions.DNS_QUERY_BYPASS_CACHE, 0, ref zero, 0);
                    if (error != 0) {
                        throw new Win32Exception(error);
                    }
                    for (ptr = zero; !ptr.Equals(IntPtr.Zero); ptr = record.pNext) {
                        record = (MXRecord)Marshal.PtrToStructure(ptr, typeof(MXRecord));
                        if (record.wType == 15) {
                            string str2 = Marshal.PtrToStringAuto(record.pNameExchange);
                            list.Add(str2);
                        }
                    }
                    DnsRecordListFree(zero, 0);
                } catch (Exception) {
                }
                if (addDomainIfError && list.Count == 0) {
                    list.Add(domainName);
                }
                return (string[])list.ToArray(typeof(string));
            }

            private string GetResponse(Socket s) {
                byte[] buffer = new byte[0x400];
                int num = 0;
                while (s.Available == 0) {
                    Thread.Sleep(100);
                    num++;
                    if (num > 30) {
                        s.Close();
                        return "000 : Timeout";
                    }
                }
                s.Receive(buffer, 0, s.Available, SocketFlags.None);
                string str = Encoding.ASCII.GetString(buffer);
                if (str.IndexOf(Environment.NewLine) > 0) {
                    return str.Substring(0, str.IndexOf(Environment.NewLine));
                }
                return str;
            }

            private int GetResponseCode(Socket s) {
                byte[] buffer = new byte[0x400];
                int num = 0;
                while (s.Available == 0) {
                    Thread.Sleep(100);
                    num++;
                    if (num > 30) {
                        s.Close();
                        return 0;
                    }
                }
                s.Receive(buffer, 0, s.Available, SocketFlags.None);
                return Convert.ToInt32(Encoding.ASCII.GetString(buffer).Substring(0, 3));
            }

            private string GetDomainName(string Mail_Adress) {
                if (Mail_Adress.Contains("@")) {
                    return Mail_Adress.Split(new char[] { '@' })[1];
                }
                return Mail_Adress;
            }

            public bool HasMXRecords(string Mail_Adress) {
                return (this.FindMXRecords(Mail_Adress, false).Length > 0);
            }

            private void Senddata(Socket s, string msg) {
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                s.Send(bytes, 0, bytes.Length, SocketFlags.None);
            }

            public string MailFrom { get; set; }
            public int SmtpPort { get; set; }

            [StructLayout(LayoutKind.Sequential)]
            private struct MXRecord {
                public IntPtr pNext;
                public string pName;
                public short wType;
                public short wDataLength;
                public int flags;
                public int dwTtl;
                public int dwReserved;
                public IntPtr pNameExchange;
                public short wPreference;
                public short Pad;
            }

            private enum QueryOptions {
                DNS_QUERY_ACCEPT_TRUNCATED_RESPONSE = 1,
                DNS_QUERY_BYPASS_CACHE = 8,
                DNS_QUERY_DONT_RESET_TTL_VALUES = 0x100000,
                DNS_QUERY_NO_HOSTS_FILE = 0x40,
                DNS_QUERY_NO_LOCAL_NAME = 0x20,
                DNS_QUERY_NO_NETBT = 0x80,
                DNS_QUERY_NO_RECURSION = 4,
                DNS_QUERY_NO_WIRE_QUERY = 0x10,
                DNS_QUERY_RESERVED = -16777216,
                DNS_QUERY_RETURN_MESSAGE = 0x200,
                DNS_QUERY_STANDARD = 0,
                DNS_QUERY_TREAT_AS_FQDN = 0x1000,
                DNS_QUERY_USE_TCP_ONLY = 2,
                DNS_QUERY_WIRE_ONLY = 0x100
            }

            private enum QueryTypes {
                DNS_TYPE_MX = 15
            }
        }
        
		public static bool sendEmail( string sFromEmail, string sToEmail, string sSubject, string sBody, string sServer )
		{
			return sendEmail(sFromEmail, sToEmail, sSubject, sBody, false, sServer);
		}
		public static bool sendEmail( string sFromEmail, string sToEmail, string sSubject, string sBody, bool bHTML, string sServer )
		{
			return sendEmail(sFromEmail, sFromEmail, sToEmail, sToEmail, "","",sSubject, sBody, bHTML, new string[0],  sServer, null, null);
		}
        //public static bool sendEmail( string sFromEmail, string sFromName, string sToEmail, string sToName, string sSubject, string sBody, bool bHTML, string[] sAttachmentFile, string sServer ) {
        //    return sendEmail( sFromEmail, sFromName, sToEmail, sToName, sSubject, sBody, bHTML, sAttachmentFile, sServer, null, null);
        //}

        public static bool sendEmail(string sFromEmail, string sToEmail, string sCCEmail, string sBCCEmail, string sSubject, string sBody, bool bHTML, string[] sAttachmentFile, string sServer, string sUsername, string sPassword) {
            return sendEmail(sFromEmail, "", sToEmail, "", sCCEmail, sBCCEmail, sSubject, sBody, bHTML, sAttachmentFile, sServer, sUsername, sPassword);
        }

        //public static bool sendEmail( string sFromEmail, string sFromName, string sToEmail, string sToName, string sSubject, string sBody, bool bHTML, string[] sAttachmentFile, string sServer, string sUsername, string sPassword ) {
        //    return sendEmail( sFromEmail, sFromName, sToEmail, sToName, "", "", sSubject, sBody, bHTML, sAttachmentFile, sServer, sUsername, sPassword);
        //}
        public static bool sendEmail(string sFromEmail, string sFromName, string sToEmail, string sToName, string sCCEmail, string sBCCEmail, string sSubject, string sBody, bool bHTML, string[] sAttachmentFile, string sServer, string sUsername, string sPassword) {
            List<KeyValuePair<string, byte[]>> oFiles = new List<KeyValuePair<string, byte[]>>();
            if (sAttachmentFile != null && sAttachmentFile.Length > 0) {
                string sAttachments = "";
                for (int x = 0; x < sAttachmentFile.Length; x++) {
                    if (convert.cStr(sAttachmentFile[x]).Trim() != "" && sAttachments.IndexOf(sAttachmentFile[x]) == -1)
                        try {
                            oFiles.Add(new KeyValuePair<string, byte[]>(Path.GetFileName(sAttachmentFile[x]), io.read_file_binary(sAttachmentFile[x])));                            
                            sAttachments += sAttachmentFile[x];
                        } catch (Exception) { }
                }		
            }
            return sendEmail(sFromEmail, sFromName, sToEmail, sToName, "", "", sSubject, sBody, bHTML, oFiles, sServer, sUsername, sPassword);
        }
        public static bool sendEmail(string sFromEmail, string sFromName, string sToEmail, string sToName, string sCCEmail, string sBCCEmail, string sSubject, string sBody, bool bHTML, List<KeyValuePair<string, byte[]>> oFiles, string sServer, string sUsername, string sPassword) {

            //Clean up email. Only use \r\n line endings
            sBody = parse.replaceAll(sBody, "\r", "", "\n", "\r\n");
            System.Net.Mail.MailMessage oMessage = new System.Net.Mail.MailMessage();
            oMessage.IsBodyHtml = bHTML;
            if (parse.removeXChars(sCCEmail, "abcdefghijklmnopqrstuvwxyz0123456789") != "") oMessage.CC.Add(parse.stripCharacter(parse.replaceAll(sCCEmail, " ", "", " ", "", ";", ",", ",,", ",", ",,", ","), ",", " "));
            if (parse.removeXChars(sBCCEmail, "abcdefghijklmnopqrstuvwxyz0123456789") != "") oMessage.Bcc.Add(parse.stripCharacter(parse.replaceAll(sBCCEmail, " ", "", " ", "", ";", ",", ",,", ",", ",,", ","), ",", " "));

            System.Net.Mail.SmtpClient oClient = new System.Net.Mail.SmtpClient(sServer);

            if (oFiles != null) {
                for (int x = 0; x < oFiles.Count; x++) {
                    MemoryStream oStream = new MemoryStream(oFiles[x].Value);                    
                    oMessage.Attachments.Add(new Attachment(oStream, oFiles[x].Key));                    
                }
            }

            if (convert.cStr(sUsername) != "" && convert.cStr(sPassword) != "")  oClient.Credentials = new System.Net.NetworkCredential(sUsername, sPassword);

            oMessage.From = new MailAddress(parse.split(parse.stripCharacter(parse.replaceAll(sFromEmail, " ", "", " ", "", ";", ",", ",,", ",", ",,", ","), ",", " "), ",")[0], convert.cStr(sFromName) == "" ? null : sFromName);
            if (parse.replaceAll(sToEmail, " ", "", ";", "", ",", "") != "") {
                if (convert.cStr(sToName) != "" && sToEmail.IndexOf(";") == -1 && sToEmail.IndexOf(",") == -1)
                    oMessage.To.Add(new MailAddress(sToEmail.Trim(), sToName.Trim()));
                else
                    oMessage.To.Add(parse.stripCharacter(parse.replaceAll(sToEmail, " ", "", " ", "", ";", ",", ",,", ",", ",,", ","), ",", " "));
            }
            //if (convert.cStr(sHTMLBaseUrl) != "") {                
            //    oMessage.UrlContentBase = sHTMLBaseUrl;
            //    oMessage.UrlContentLocation = sHTMLBaseUrl;
            //}

            //if (iPriority == -1)
            //    oMessage.Priority = System.Net.Mail.MailPriority.Low;
            //else if (iPriority == 1)
            //    oMessage.Priority = System.Net.Mail.MailPriority.High;

            oMessage.Subject = parse.replaceAll(sSubject, "\n", " ", "\r", "","\t"," ");
            oMessage.Body = sBody;
            oClient.Send(oMessage);
            for (int x = 0; x < oMessage.Attachments.Count; x++) {
                try {
                    oMessage.Attachments[x].Dispose();
                } catch (Exception) { }
            }
            return true;

        }
        //public static bool sendEmail1( string sFromEmail, string sFromName, string sToEmail, string sToName, string sCCEmail, string sBCCEmail, string sSubject, string sBody, bool bHTML, string[] sAttachmentFile, string sServer, string sUsername, string sPassword, string sHTMLBaseUrl, int iPriority ){
            
        //    //Clean up email. Only use \r\n line endings
        //    sBody = parse.replaceAll(sBody, "\r", "", "\n", "\r\n");
        //    MailMessage oMessage	= new MailMessage();
        //    System.Web.Mail.SmtpMail.SmtpServer  = sServer;
        //    sFromEmail = parse.stripEndingCharacter(parse.stripLeadingCharacter(sFromEmail, ";"),";");

        //    oMessage.BodyFormat		= ( bHTML ? System.Web.Mail.MailFormat.Html : System.Web.Mail.MailFormat.Text );
		
        //    if ( sAttachmentFile != null && sAttachmentFile.Length > 0 ){
        //        string sAttachments = "";
        //        for ( int x = 0 ; x < sAttachmentFile.Length ; x++ ){
        //            if ( convert.cStr(sAttachmentFile[ x ]).Trim() != "" && sAttachments.IndexOf(sAttachmentFile[x])==-1 )
        //                try {
        //                    oMessage.Attachments.Add(new System.Web.Mail.MailAttachment(sAttachmentFile[x]));
        //                    sAttachments += sAttachmentFile[x];
        //                } catch (Exception) { }
        //        }																			 
        //    }

        //    if ( convert.cStr( sUsername ) != "" && convert.cStr( sPassword ) != "" ){
        //        oMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");
        //        oMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sUsername );
        //        oMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", sPassword );
        //    }

        //    oMessage.From			= ( sFromEmail.IndexOf(";") > -1 ? parse.split(sFromEmail,";")[0] : sFromEmail);
        //    if (convert.cStr(sFromName) != "") oMessage.From = "\"" + parse.replaceAll(sFromName, "\"", "") + "\" <" + oMessage.From + ">";
        //    oMessage.To				= sToEmail;
        //    if( convert.cStr(sToName) != "" && sToEmail.IndexOf(";")==-1) oMessage.To = "\"" + parse.replaceAll(sToName, "\"", "") + "\" <" + oMessage.To + ">";

        //    oMessage.Cc				= convert.cStr( sCCEmail );
        //    oMessage.Bcc			= convert.cStr( sBCCEmail );
			
        //    if ( convert.cStr( sHTMLBaseUrl ) != "" ){
        //        oMessage.UrlContentBase		= sHTMLBaseUrl;
        //        oMessage.UrlContentLocation	= sHTMLBaseUrl;
        //    }

        //    if ( iPriority == -1 )
        //        oMessage.Priority	= System.Web.Mail.MailPriority.Low;
        //    else if ( iPriority == 1 )
        //        oMessage.Priority	= System.Web.Mail.MailPriority.High;

        //    oMessage.Subject		= sSubject;
        //    oMessage.Body			= sBody;

        //    System.Web.Mail.SmtpMail.Send( oMessage );
        //    return true;
		
        //}

	}
}

