﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using jlib.functions;
using jlib.db;
using System.Reflection;

namespace jlib.net {
    public class FTP {
        private string m_sUserName, m_sPassword, m_sPort, m_sServer,m_sCommandStatusDescription;
        private FtpStatusCode m_sCommandStatusCode = FtpStatusCode.Undefined;
        public FTP(string sUserName, string sPassword, string sServer, string sPort) {
            m_sUserName=sUserName;
            m_sPassword=sPassword;
            m_sPort=sPort;
            m_sServer = sServer;
        }
        public string CommandStatusDescription {
            get {
                return m_sCommandStatusDescription;
            }
        }
        public FtpStatusCode CommandStatusCode {
            get {
                return m_sCommandStatusCode;
            }
        }
        
        public string downloadTextFile(string sPath) {
            FtpWebRequest oRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + m_sServer + ":" + m_sPort + parse.replaceAll("/"+sPath,"//","/")));
            if(m_sUserName!="") oRequest.Credentials = new NetworkCredential(m_sUserName, m_sPassword);
            oRequest.Method = WebRequestMethods.Ftp.DownloadFile;
            FtpWebResponse oResponse = (FtpWebResponse)oRequest.GetResponse();
            m_sCommandStatusDescription = oResponse.StatusDescription;
            m_sCommandStatusCode = oResponse.StatusCode;
            return convert.cStreamToString(oResponse.GetResponseStream());            
        }
        public string deleteFile(string sPath) {
            FtpWebRequest oRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + m_sServer + ":" + m_sPort + parse.replaceAll("/" + sPath, "//", "/")));
            if (m_sUserName != "") oRequest.Credentials = new NetworkCredential(m_sUserName, m_sPassword);
            oRequest.Method = WebRequestMethods.Ftp.DeleteFile;
            FtpWebResponse oResponse = (FtpWebResponse)oRequest.GetResponse();
            m_sCommandStatusDescription = oResponse.StatusDescription;
            m_sCommandStatusCode = oResponse.StatusCode;
            return convert.cStreamToString(oResponse.GetResponseStream());
        }
        public List<string> listFiles(string sPath) {
            FtpWebRequest oRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + m_sServer + ":" + m_sPort + parse.replaceAll("/" + sPath, "//", "/")));
            if (m_sUserName != "") oRequest.Credentials = new NetworkCredential(m_sUserName, m_sPassword);
            oRequest.Method = WebRequestMethods.Ftp.ListDirectory;
            FtpWebResponse oResponse = (FtpWebResponse)oRequest.GetResponse();
            m_sCommandStatusDescription = oResponse.StatusDescription;
            m_sCommandStatusCode = oResponse.StatusCode;
            List<string> oFiles = new List<string>();
            StreamReader oReader = new StreamReader(oResponse.GetResponseStream());
            string sFile = oReader.ReadLine();
            while (sFile != null) {
                oFiles.Add(sFile);
                sFile = oReader.ReadLine();
            }
            return oFiles;
        }
    }
}
