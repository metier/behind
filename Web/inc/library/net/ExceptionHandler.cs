﻿using System;
using System.IO;
using System.Web;
using System.Configuration;
using System.Reflection;
using jlib.functions;

namespace jlib.net
{
    public class ExceptionHandler : IHttpModule
    {
        public static class Keys
        {
            public const string SMTP_SERVER = "smtp.server";
            public const string EXCEPTION_EMAILS = "exception.emails";
            public const string EXCEPTION_FROM_EMAIL = "exception.from.email";
            public const string EXCEPTION_SUBJECT_PREFIX = "exception.subject.prefix";
        }
        public void Init(System.Web.HttpApplication Application)
        {
            Application.Error += OnError;
        }

        public void Dispose()
        {
        }

        protected virtual void OnError(object sender, EventArgs args)
        {
            HttpApplication app = (HttpApplication)sender;
            var handler = new Handler();
            handler.HandleException(app.Server.GetLastError(), null);
        }
        public static void HandleException(Exception ex)
        {
            new Handler().HandleException(ex, null);
        }
		public static void HandleException(Exception ex, string Response)
        {
            new Handler().HandleException(ex, Response);
        }
        public class Handler
        {
            public HttpContext Context { get { return HttpContext.Current; } }
            public HttpRequest Request { get { return this.Context.Request; } }
            public HttpResponse Response { get { return this.Context.Response; } }
           

            private static string _ExceptionEmail = null;
            public static string ExceptionEmail
            {
                get
                {
                    if (_ExceptionEmail == null)
                    {                        
                        var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("jlib.net.ExceptionHandler.html");
                        var reader = new StreamReader(stream);
                        _ExceptionEmail = reader.ReadToEnd();
                        reader.Close();                        
                    }
                    return _ExceptionEmail;
                }
            }

            
            public void HandleException(Exception ex, string ResponseRaw)
            {
				//new System.Threading.Tasks.Task(() =>
				//{
					try
					{
						//Get Error Details
						var requestUrl = Request.RawUrl;
						var responseCode = Response.StatusCode;

						var requestRaw = Request.HttpMethod + " " + Request.Url.OriginalString + "\n\n" + Request.ServerVariables["ALL_RAW"];
						using (Stream stream = Request.InputStream)
						{
							stream.Position = 0;
							using (StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8))
							{
								requestRaw += "\n\n" + reader.ReadToEnd();
							}
						}

						var serverVariables = "";
						foreach (string key in Request.ServerVariables)
						{
							serverVariables += String.Format("{0}: {1}\n", key, Request.ServerVariables[key]);
						}

						var stackTrace = ex.StackTrace;
						if (ex.InnerException != null)
						{
							stackTrace += String.Format("\n\nINNER EXCEPTION:\n{0}\n\n", ex.InnerException.Message);
							stackTrace += ex.InnerException.StackTrace;
						}


						//Create Email
						var smtpServer = ConfigurationManager.AppSettings[Keys.SMTP_SERVER];
						var smtpTo = ConfigurationManager.AppSettings[Keys.EXCEPTION_EMAILS];
						var subjectPrefix = ConfigurationManager.AppSettings[Keys.EXCEPTION_SUBJECT_PREFIX];
						var subject = String.Format("{0} Exception - {1}", subjectPrefix, ex.Message);

						//Format Body
						string html = ExceptionEmail;
						html = html.Replace("{{ErrorMessage}}", ex.Message);
						html = html.Replace("{{ErrorType}}", ex.GetType().FullName);
						html = html.Replace("{{ErrorSource}}", ex.Source);
						html = html.Replace("{{StackTrace}}", stackTrace);
						html = html.Replace("{{RequestUrl}}", requestUrl);
						html = html.Replace("{{RequestRaw}}", requestRaw);
						html = html.Replace("{{ResponseRaw}}", ResponseRaw);
						html = html.Replace("{{ServerVariables}}", serverVariables);

						//Send Email
						jlib.net.SMTP.sendEmail(ConfigurationManager.AppSettings[Keys.EXCEPTION_FROM_EMAIL], sToEmail: smtpTo, sSubject: subject, sBody: html, bHTML: true, sServer: smtpServer);
					}
					catch(Exception e) { }
				//}).Start();
            }
        }
    }
}