using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Net.Sockets;
using System.IO;
using System.Threading;

using System.Text.RegularExpressions;
using System.Collections;
using System.Net.Security;


namespace jlib.net.pop{

	public class MyMD5 {
		public static string GetMD5Hash(String input) {
			MD5 md5=new MD5CryptoServiceProvider();
			//the GetBytes method returns byte array equavalent of a string
			byte []res=md5.ComputeHash(Encoding.Default.GetBytes(input),0,input.Length);
			char []temp=new char[res.Length];
			//copy to a char array which can be passed to a String constructor
			System.Array.Copy(res,temp,res.Length);
			//return the result as a string
			return new String(temp);
		}

		public static string GetMD5HashHex(String input) {
			MD5 md5=new MD5CryptoServiceProvider();
			DES des=new DESCryptoServiceProvider();
			//the GetBytes method returns byte array equavalent of a string
			byte []res=md5.ComputeHash(Encoding.Default.GetBytes(input),0,input.Length);

			String returnThis="";

			for(int i=0;i<res.Length;i++) {
				returnThis+=System.Uri.HexEscape((char)res[i]);
			}
			returnThis=returnThis.Replace("%","");
			returnThis=returnThis.ToLower();

			return returnThis;


		}

	}

	/// <summary>
	/// POPClient
	/// </summary>
	public class POPClient {


		/// <summary>
		/// Possible responses received from the server when performing an Authentication
		/// </summary>
		public enum  AuthenticationResponse {
			/// <summary>
			/// Authentication succeeded
			/// </summary>
			SUCCESS=0,
			/// <summary>
			/// Login doesn't exist on the POP3 server
			/// </summary>		
			INVALIDUSER=1,
			/// <summary>
			/// Password is invalid for the give login
			/// </summary>
			INVALIDPASSWORD=2,
			/// <summary>
			/// Invalid login and/or password
			/// </summary>
			INVALIDUSERORPASSWORD=3
		}		

		/// <summary>
		/// Authentication method to use
		/// </summary>
		/// <remarks>TRYBOTH means code will first attempt by using APOP method as its more secure.
		///  In case of failure the code will fall back to USERPASS method.
		/// </remarks>
		public enum  AuthenticationMethod {
			/// <summary>
			/// Authenticate using the USER/PASS method. USER/PASS is secure but all POP3 servers may not support this method
			/// </summary>
			USERPASS=0,
			/// <summary>
			/// Authenticate using the APOP method
			/// </summary>
			APOP=1,
			/// <summary>
			/// Authenticate using USER/PASS. In case USER/PASS fails then revert to APOP
			/// </summary>
			TRYBOTH=2
		}


		/// <summary>
		/// Thrown when the POP3 Server sends an error (-ERR) during intial handshake (HELO)
		/// </summary>
		public class PopServerNotAvailableException:Exception {
		}

		/// <summary>
		/// Thrown when the specified POP3 Server can not be found or connected with
		/// </summary>	
		public class PopServerNotFoundException:Exception {
		}

		/// <summary>
		/// Thrown when the attachment is not in a format supported by OpenPOP.NET
		/// </summary>
		/// <remarks>Supported attachment encodings are Base64,Quoted Printable,MS TNEF</remarks>
		public class AttachmentEncodingNotSupportedException:Exception {
		}

		/// <summary>
		/// Thrown when the supplied login doesn't exist on the server
		/// </summary>
		/// <remarks>Should be used only when using USER/PASS Authentication Method</remarks>
		public class InvalidLoginException:Exception {
		}

		/// <summary>
		/// Thrown when the password supplied for the login is invalid
		/// </summary>	
		/// <remarks>Should be used only when using USER/PASS Authentication Method</remarks>
		public class InvalidPasswordException:Exception {
		}

		/// <summary>
		/// Thrown when either the login or the password is invalid on the POP3 Server
		/// </summary>
		/// /// <remarks>Should be used only when using APOP Authentication Method</remarks>
		public class InvalidLoginOrPasswordException:Exception {
		}

		/// <summary>
		/// Thrown when the user mailbox is in a locked state
		/// </summary>
		/// <remarks>The mail boxes are locked when an existing session is open on the mail server. Lock conditions are also met in case of aborted sessions</remarks>
		public class PopServerLockException:Exception {
		}

		/// <summary>
		/// Event that fires when begin to connect with target POP3 server.
		/// </summary>
		public event EventHandler CommunicationBegan;
		/// <summary>
		/// Event that fires when connected with target POP3 server.
		/// </summary>
		public event EventHandler CommunicationOccured;

		/// <summary>
		/// Event that fires when disconnected with target POP3 server.
		/// </summary>
		public event EventHandler CommunicationLost;

		/// <summary>
		/// Event that fires when authentication began with target POP3 server.
		/// </summary>
		public event EventHandler AuthenticationBegan;

		/// <summary>
		/// Event that fires when authentication finished with target POP3 server.
		/// </summary>
		public event EventHandler AuthenticationFinished;

		/// <summary>
		/// Event that fires when message transfer has begun.
		/// </summary>		
		public event EventHandler MessageTransferBegan;
		
		/// <summary>
		/// Event that fires when message transfer has finished.
		/// </summary>
		public event EventHandler MessageTransferFinished;

		internal void OnCommunicationBegan(EventArgs e) {
			if (CommunicationBegan != null)
				CommunicationBegan(this, e);
		}

		internal void OnCommunicationOccured(EventArgs e) {
			if (CommunicationOccured != null)
				CommunicationOccured(this, e);
		}

		internal void OnCommunicationLost(EventArgs e) {
			if (CommunicationLost != null)
				CommunicationLost(this, e);
		}

		internal void OnAuthenticationBegan(EventArgs e) {
			if (AuthenticationBegan != null)
				AuthenticationBegan(this, e);
		}

		internal void OnAuthenticationFinished(EventArgs e) {
			if (AuthenticationFinished != null)
				AuthenticationFinished(this, e);
		}

		internal void OnMessageTransferBegan(EventArgs e) {
			if (MessageTransferBegan != null)
				MessageTransferBegan(this, e);
		}
		
		internal void OnMessageTransferFinished(EventArgs e) {
			if (MessageTransferFinished != null)
				MessageTransferFinished(this, e);
		}

		private const string RESPONSE_OK="+OK";
		//private const string RESPONSE_ERR="-ERR";
		private TcpClient clientSocket=null;		
		private StreamReader reader;
		private StreamWriter writer;
		private string _Error = "";
		private int _receiveTimeOut=60000;
		private int _sendTimeOut=60000;
		private int _receiveBufferSize=4090;
		private int _sendBufferSize=4090;
		private string _basePath=null;
		private bool _receiveFinish=false;
		private bool _autoDecodeMSTNEF=true;
		private int _waitForResponseInterval=200;
		private int _receiveContentSleepInterval=100;
		private string _aPOPTimestamp;
		private string _lastCommandResponse;
		private bool _connected=true;


		public bool Connected {			
			get{return _connected;}
		}

		public string APOPTimestamp {
			get{return _aPOPTimestamp;}
		}

		/// <summary>
		/// receive content sleep interval
		/// </summary>
		public int ReceiveContentSleepInterval {
			get{return _receiveContentSleepInterval;}
			set{_receiveContentSleepInterval=value;}
		}

		/// <summary>
		/// wait for response interval
		/// </summary>
		public int WaitForResponseInterval {
			get{return _waitForResponseInterval;}
			set{_waitForResponseInterval=value;}
		}

		/// <summary>
		/// whether auto decoding MS-TNEF attachment files
		/// </summary>
		public bool AutoDecodeMSTNEF {
			get{return _autoDecodeMSTNEF;}
			set{_autoDecodeMSTNEF=value;}
		}

		/// <summary>
		/// path to extract MS-TNEF attachment files
		/// </summary>
		public string BasePath {
			get{return _basePath;}
			set {				
				try {
					if(value.EndsWith("\\"))
						_basePath=value;
					else
						_basePath=value+"\\";
				}
				catch {
				}
			}
		}

		/// <summary>
		/// Receive timeout for the connection to the SMTP server in milliseconds.
		/// The default value is 60000 milliseconds.
		/// </summary>
		public int ReceiveTimeOut {
			get{return _receiveTimeOut;}
			set{_receiveTimeOut=value;}
		}

		/// <summary>
		/// Send timeout for the connection to the SMTP server in milliseconds.
		/// The default value is 60000 milliseconds.
		/// </summary>
		public int SendTimeOut {
			get{return _sendTimeOut;}
			set{_sendTimeOut=value;}
		}

		/// <summary>
		/// Receive buffer size
		/// </summary>
		public int ReceiveBufferSize {
			get{return _receiveBufferSize;}
			set{_receiveBufferSize=value;}
		}

		/// <summary>
		/// Send buffer size
		/// </summary>
		public int SendBufferSize {
			get{return _sendBufferSize;}
			set{_sendBufferSize=value;}
		}

		private void WaitForResponse(bool blnCondiction, int intInterval) {
			if(intInterval==0)
				intInterval=WaitForResponseInterval;
			while(!blnCondiction==true) {
				Thread.Sleep(intInterval);
			}
		}

		private void WaitForResponse(ref StreamReader rdReader, int intInterval) {
			if(intInterval==0)
				intInterval=WaitForResponseInterval;
			//while(rdReader.Peek()==-1 || !rdReader.BaseStream.CanRead)
			while(!rdReader.BaseStream.CanRead) {
				Thread.Sleep(intInterval);
			}
		}

		private void WaitForResponse(ref StreamReader rdReader) {
			DateTime dtStart=DateTime.Now;
			TimeSpan tsSpan;
			while(!rdReader.BaseStream.CanRead) {
				tsSpan=DateTime.Now.Subtract(dtStart);
				if(tsSpan.Milliseconds>_receiveTimeOut)
					break;
				Thread.Sleep(_waitForResponseInterval);
			}
		}

		private void WaitForResponse(ref StreamWriter wrWriter, int intInterval) {
			if(intInterval==0)
				intInterval=WaitForResponseInterval;
			while(!wrWriter.BaseStream.CanWrite) {
				Thread.Sleep(intInterval);
			}
		}

		/// <summary>
		/// Examines string to see if it contains a timestamp to use with the APOP command
		/// If it does, sets the ApopTimestamp property to this value
		/// </summary>
		/// <param name="strResponse">string to examine</param>
		private void ExtractApopTimestamp(string strResponse) {
			Match match = Regex.Match(strResponse, "<.+>");
			if (match.Success) {
				_aPOPTimestamp = match.Value;
			}
		}

		/// <summary>
		/// Tests a string to see if it's a "+OK" string
		/// </summary>
		/// <param name="strResponse">string to examine</param>
		/// <returns>true if response is an "+OK" string</returns>
		private bool IsOkResponse(string strResponse) {
			return (strResponse.Substring(0, 3) == RESPONSE_OK);
		}

		/// <summary>
		/// get response content
		/// </summary>
		/// <param name="strResponse">string to examine</param>
		/// <returns>response content</returns>
		private string GetResponseContent() {
			return _lastCommandResponse.Substring(3);
		}

		/// <summary>
		/// Sends a command to the POP server.
		/// </summary>
		/// <param name="strCommand">command to send to server</param>
		/// <param name="blnSilent">Do not give error</param>
		/// <returns>true if server responded "+OK"</returns>
		private bool SendCommand(string strCommand, bool blnSilent) {
			_lastCommandResponse = "";
			try {
				if(writer.BaseStream.CanWrite) {
					writer.WriteLine(strCommand);
					writer.Flush();
					//WaitForResponse(ref reader,WaitForResponseInterval);
					WaitForResponse(ref reader);
					_lastCommandResponse = reader.ReadLine();				
					return IsOkResponse(_lastCommandResponse);
				}
				else
					return false;
			}
			catch(Exception e) {
				if(!blnSilent) {
					_Error = strCommand + ":" +e.Message;
					Utility.LogError(_Error);
				}
				return false;
			}
		}

		/// <summary>
		/// Sends a command to the POP server.
		/// </summary>
		/// <param name="strCommand">command to send to server</param>
		/// <returns>true if server responded "+OK"</returns>
		private bool SendCommand(string strCommand) {
			return SendCommand(strCommand,false);
		}

		/// <summary>
		/// Sends a command to the POP server, expects an integer reply in the response
		/// </summary>
		/// <param name="strCommand">command to send to server</param>
		/// <returns>integer value in the reply</returns>
		private int SendCommandIntResponse(string strCommand) {
			int retVal = 0;
			if(SendCommand(strCommand)) {
				try {
					retVal = int.Parse(_lastCommandResponse.Split(' ')[1]);
				}
				catch(Exception e) {
					Utility.LogError(strCommand + ":" + e.Message);
				}
			}
			return retVal;
		}

		/// <summary>
		/// Construct new POPClient
		/// </summary>
		public POPClient() {
			Utility.Log=false;
		}		

		/// <summary>
		/// Construct new POPClient
		/// </summary>
		public POPClient(string strHost, int intPort, string strlogin, string strPassword, AuthenticationMethod authenticationMethod):this(strHost, intPort, strlogin, strPassword, authenticationMethod, false) {			
		}
		public POPClient( string strHost,int intPort,string strlogin,string strPassword, AuthenticationMethod authenticationMethod, bool bUseSSL) {
			Connect(strHost, intPort, bUseSSL);
			Authenticate(strlogin,strPassword,authenticationMethod);
		}

		/// <summary>
		/// connect to remote server
		/// </summary>
		/// <param name="strHost">POP3 host</param>
		/// <param name="intPort">POP3 port</param>
		public void Connect(string strHost, int intPort) {
			Connect(strHost, intPort, false);
		}
		public void Connect(string strHost,int intPort, bool bUseSSL) {
			OnCommunicationBegan(EventArgs.Empty);

			clientSocket=new TcpClient();
			clientSocket.ReceiveTimeout=_receiveTimeOut;
			clientSocket.SendTimeout=_sendTimeOut;
			clientSocket.ReceiveBufferSize=_receiveBufferSize;
			clientSocket.SendBufferSize=_sendBufferSize;

			try {
				clientSocket.Connect(strHost,intPort);				
			}
			catch(SocketException e) {				
				Disconnect();
				Utility.LogError("Connect():"+e.Message);
				throw new PopServerNotFoundException();
			}

			if (bUseSSL) {

				SslStream oSSLStream = new SslStream(clientSocket.GetStream(), false, delegate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors errors) { return true; });
				oSSLStream.AuthenticateAsClient(strHost);
				reader = new StreamReader(oSSLStream);
				writer = new StreamWriter(oSSLStream); 				
			} else {				
				reader = new StreamReader(clientSocket.GetStream(), Encoding.Default, true);
				writer = new StreamWriter(clientSocket.GetStream());				
			}
			writer.AutoFlush = true;

			WaitForResponse(ref reader,WaitForResponseInterval);

			string strResponse=reader.ReadLine();

			if(IsOkResponse(strResponse)) {
				ExtractApopTimestamp(strResponse);
				_connected=true;
				OnCommunicationOccured(EventArgs.Empty);
			}
			else {
				Disconnect();
				Utility.LogError("Connect():"+"Error when login, maybe POP3 server not exist");
				throw new PopServerNotAvailableException();
			}
		}

		/// <summary>
		/// Disconnect from POP3 server
		/// </summary>
		public void Disconnect() {
			DisconnectAndQuit(true);
		}
		public void DisconnectAndQuit(bool bSendQuit) {
			try {
				clientSocket.ReceiveTimeout=500;
				clientSocket.SendTimeout=500;
				if(bSendQuit) SendCommand("QUIT",true);
				clientSocket.ReceiveTimeout=_receiveTimeOut;
				clientSocket.SendTimeout=_sendTimeOut;
				reader.Close();
				writer.Close();
				clientSocket.GetStream().Close();
				clientSocket.Close();
			}
			catch {
				//Utility.LogError("Disconnect():"+e.Message);
			}
			finally {
				reader=null;
				writer=null;
				clientSocket=null;
			}
			OnCommunicationLost(EventArgs.Empty);
		}

		/// <summary>
		/// release me
		/// </summary>
		~POPClient() {
			Disconnect();
		}

		/// <summary>
		/// verify user and password
		/// </summary>
		/// <param name="strlogin">user name</param>
		/// <param name="strPassword">password</param>
		public void Authenticate(string strlogin,string strPassword) {
			Authenticate(strlogin,strPassword,AuthenticationMethod.USERPASS);
		}

		/// <summary>
		/// verify user and password
		/// </summary>
		/// <param name="strlogin">user name</param>
		/// <param name="strPassword">strPassword</param>
		/// <param name="authenticationMethod">verification mode</param>
		public void Authenticate(string strlogin,string strPassword,AuthenticationMethod authenticationMethod) {
			if(authenticationMethod==AuthenticationMethod.USERPASS) {
				AuthenticateUsingUSER(strlogin,strPassword);				
			}
			else if(authenticationMethod==AuthenticationMethod.APOP) {
				AuthenticateUsingAPOP(strlogin,strPassword);
			}
			else if(authenticationMethod==AuthenticationMethod.TRYBOTH) {
				try {
					AuthenticateUsingUSER(strlogin,strPassword);
				}
				catch(InvalidLoginException e) {
					Utility.LogError("Authenticate():"+e.Message);
				}
				catch(InvalidPasswordException e) {
					Utility.LogError("Authenticate():"+e.Message);
				}
				catch(Exception e) {
					Utility.LogError("Authenticate():"+e.Message);
					AuthenticateUsingAPOP(strlogin,strPassword);
				}
			}
		}

		/// <summary>
		/// verify user and password
		/// </summary>
		/// <param name="strlogin">user name</param>
		/// <param name="strPassword">password</param>
		private void AuthenticateUsingUSER(string strlogin,string strPassword) {				
			OnAuthenticationBegan(EventArgs.Empty);

			if(!SendCommand("USER " + strlogin)) {
				Utility.LogError("AuthenticateUsingUSER():wrong user");
				throw new InvalidLoginException();
			}
			
			WaitForResponse(ref writer,WaitForResponseInterval);

			if(!SendCommand("PASS " + strPassword)) {
				if(_lastCommandResponse.ToLower().IndexOf("lock")!=-1) {
					Utility.LogError("AuthenticateUsingUSER():maildrop is locked");
					throw new PopServerLockException();			
				}
				else {
					Utility.LogError("AuthenticateUsingUSER():wrong password or " + GetResponseContent());
					throw new InvalidPasswordException();
				}
			}
			
			OnAuthenticationFinished(EventArgs.Empty);
		}

		/// <summary>
		/// verify user and password using APOP
		/// </summary>
		/// <param name="strlogin">user name</param>
		/// <param name="strPassword">password</param>
		private void AuthenticateUsingAPOP(string strlogin,string strPassword) {
			OnAuthenticationBegan(EventArgs.Empty);

			if(!SendCommand("APOP " + strlogin + " " + MyMD5.GetMD5HashHex(strPassword))) {
				Utility.LogError("AuthenticateUsingAPOP():wrong user or password");
				throw new InvalidLoginOrPasswordException();		
			}

			OnAuthenticationFinished(EventArgs.Empty);
		}

		/*		private string GetCommand(string input)
				{			
					try
					{
						return input.Split(' ')[0];
					}
					catch(Exception e)
					{
						Utility.LogError("GetCommand():"+e.Message);
						return "";
					}
				}*/

		private string[] GetParameters(string input) {
			string []temp=input.Split(' ');
			string []retStringArray=new string[temp.Length-1];
			Array.Copy(temp,1,retStringArray,0,temp.Length-1);

			return retStringArray;
		}		

		/// <summary>
		/// get message count
		/// </summary>
		/// <returns>message count</returns>
		public int GetMessageCount() {			
			return SendCommandIntResponse("STAT");
		}

		/// <summary>
		/// Deletes message with given index when Close() is called
		/// </summary>
		/// <param name="intMessageIndex"> </param>
		public bool DeleteMessage(int intMessageIndex) {
			return SendCommand("DELE " + intMessageIndex.ToString());
		}

		/// <summary>
		/// Deletes messages
		/// </summary>
		public bool DeleteAllMessages() {
			int messageCount=GetMessageCount();
			for(int messageItem=messageCount;messageItem>0;messageItem--) {
				if (!DeleteMessage(messageItem))
					return false;
			}
			return true;
		}

		/// <summary>
		/// quit POP3 server
		/// </summary>
		public bool QUIT() {
			return SendCommand("QUIT");
		}

		/// <summary>
		/// keep server active
		/// </summary>
		public bool NOOP() {
			return SendCommand("NOOP");
		}

		/// <summary>
		/// keep server active
		/// </summary>
		public bool RSET() {
			return SendCommand("RSET");
		}

		/// <summary>
		/// identify user
		/// </summary>
		public bool USER() {
			return SendCommand("USER");

		}

		/// <summary>
		/// get messages info
		/// </summary>
		/// <param name="intMessageNumber">message number</param>
		/// <returns>Message object</returns>
		public Message GetMessageHeader(int intMessageNumber) {
			OnMessageTransferBegan(EventArgs.Empty);

			Message msg=FetchMessage("TOP "+intMessageNumber.ToString()+" 0", true);
			
			OnMessageTransferFinished(EventArgs.Empty);

			return msg;
		}

		/// <summary>
		/// get message uid
		/// </summary>
		/// <param name="intMessageNumber">message number</param>
		public string GetMessageUID(int intMessageNumber) {
			string[] strValues=null;
			if(SendCommand("UIDL " + intMessageNumber.ToString())) {
				strValues = GetParameters(_lastCommandResponse);
			}
			return strValues[1];			
		}

		/// <summary>
		/// get message uids
		/// </summary>
		public ArrayList GetMessageUIDs() {
			ArrayList uids=new ArrayList();
			if(SendCommand("UIDL")) {
				string strResponse=reader.ReadLine();
				while (strResponse!=".") {
					uids.Add(strResponse.Split(' ')[1]);
					strResponse=reader.ReadLine();
				}
				return uids;
			}
			else {
				return null;
			}
		}

		/// <summary>
		/// Get the sizes of all the messages
		/// CAUTION:  Assumes no messages have been deleted
		/// </summary>
		/// <returns>Size of each message</returns>
		public ArrayList LIST() {
			ArrayList sizes=new ArrayList();
			if(SendCommand("LIST")) {
				string strResponse=reader.ReadLine();
				while (strResponse!=".") {
					sizes.Add(int.Parse(strResponse.Split(' ')[1]));
					strResponse=reader.ReadLine();
				}
				return sizes;
			}
			else {
				return null;
			}
		}

		/// <summary>
		/// get the size of a message
		/// </summary>
		/// <param name="intMessageNumber">message number</param>
		/// <returns>Size of message</returns>
		public int LIST(int intMessageNumber) {
			return SendCommandIntResponse("LIST " + intMessageNumber.ToString());
		}

		/// <summary>
		/// read stream content
		/// </summary>
		/// <param name="intContentLength">length of content to read</param>
		/// <returns>content</returns>
		private string ReceiveContent(int intContentLength) {
			string strResponse=null;
			StringBuilder builder = new StringBuilder();
			
			WaitForResponse(ref reader,WaitForResponseInterval);

			strResponse = reader.ReadLine();
			int intLines=0;
			int intLen=0;

			while (strResponse!=".") {// || (intLen<intContentLength)) //(strResponse.IndexOf(".")==0 && intLen<intContentLength)
				builder.Append(strResponse + "\r\n");
				intLines+=1;
				intLen+=strResponse.Length+"\r\n".Length;
				
				WaitForResponse(ref reader,1);

				strResponse = reader.ReadLine();
				if((intLines % _receiveContentSleepInterval)==0) //make an interval pause to ensure response from server
					Thread.Sleep(1);
			}

			builder.Append(strResponse+ "\r\n");

			return builder.ToString();

		}

		/// <summary>
		/// get message info
		/// </summary>
		/// <param name="number">message number on server</param>
		/// <returns>Message object</returns>
		public Message GetMessage(int intNumber, bool blnOnlyHeader) {			
			OnMessageTransferBegan(EventArgs.Empty);

			Message msg=FetchMessage("RETR " + intNumber.ToString(), blnOnlyHeader);

			OnMessageTransferFinished(EventArgs.Empty);

			return msg;
		}

		/// <summary>
		/// fetches a message or a message header
		/// </summary>
		/// <param name="strCommand">Command to send to Pop server</param>
		/// <param name="blnOnlyHeader">Only return message header?</param>
		/// <returns>Message object</returns>
		public Message FetchMessage(string strCommand, bool blnOnlyHeader) {			
			_receiveFinish=false;
			if(!SendCommand(strCommand))			
				return null;

			try {
				string receivedContent=ReceiveContent(-1);

				Message msg=new Message(ref _receiveFinish,_basePath,_autoDecodeMSTNEF,receivedContent,blnOnlyHeader);

				WaitForResponse(_receiveFinish,WaitForResponseInterval);

				return msg;
			}
			catch(Exception e) {
				Utility.LogError("FetchMessage():"+e.Message);
				return null;
			}
		}

	}

	/// <summary>
	/// Utility functions
	/// </summary>
	public class Utility {
		/// <summary>
		/// Weather auto loggin is on or off
		/// </summary>
		private static bool m_blnLog=false;

		/// <summary>
		/// The file name in which the logging will be done
		/// </summary>
		private static string m_strLogFile = "OpenPOP.log";

		/// <summary>
		/// Turns file logging on and off.<font color="red"><h1>Change Property Name</h1></font>
		/// </summary>
		/// <remarks>Comming soon.</remarks>
		public static bool Log {
			get {
				return m_blnLog;
			}
			set {
				m_blnLog = value;
			}
		}

		/// <summary>
		/// Log an error to the log file
		/// </summary>
		/// <param name="strText">The error text to log</param>
		internal static void LogError(string strText) {
			//Log=true;
			if(Log) {
				FileInfo file = null;
				FileStream fs = null;
				StreamWriter sw = null;
				try {
					file = new FileInfo(m_strLogFile);
					sw = file.AppendText();
					//fs = new FileStream(m_strLogFile, FileMode.OpenOrCreate, FileAccess.Write);
					//sw = new StreamWriter(fs);
					sw.WriteLine(DateTime.Now);
					sw.WriteLine(strText);
					sw.WriteLine("\r\n");
					sw.Flush();
				}
				finally {
					if(sw != null) {
						sw.Close();
						sw = null;
					}
					if(fs != null) {
						fs.Close();
						fs = null;
					}
					
				}
			}
		}

	}

}
