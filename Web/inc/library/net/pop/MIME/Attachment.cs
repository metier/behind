
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace jlib.net.pop
{
	/// <summary>
	/// Summary description for Attachment.
	/// </summary>
	public class Attachment : IComparable
	{
		#region Member Variables
		private string _contentType=null;
		private string _contentCharset=null;
		private string _contentFormat=null;
		private string _contentTransferEncoding=null;
		private string _contentDescription=null;
		private string _contentDisposition=null;


		private string _defaultMSTNEFFileName="winmail.dat";
		private string _contentID=null;
		private long _contentLength=0;
		private string _rawAttachment=null;
		private bool _inBytes=false;
		private byte[] _rawBytes=null;
		private string _fileName	= null;
		#endregion


		#region Properties
		/// <summary>
		/// raw attachment content bytes
		/// </summary>
		public byte[] RawBytes
		{
			get{return _rawBytes;}
			set{_rawBytes=value;}
		}

		/// <summary>
		/// whether attachment is in bytes
		/// </summary>
		public bool InBytes
		{
			get{return _inBytes;}
			set{_inBytes=value;}
		}

		/// <summary>
		/// FileName
		/// </summary>
		public string FileName {
			get{return _fileName; }
			set{_fileName=value;}
		}

		/// <summary>
		/// Content length
		/// </summary>
		public long ContentLength
		{
			get{return _contentLength;}
		}

		/// <summary>
		/// verify the attachment whether it is a real attachment or not
		/// </summary>
		/// <remarks>this is so far not comprehensive and needs more work to finish</remarks>
		public bool NotAttachment
		{
			get
			{
				/*				if (_contentDisposition==null||_contentType==null)
									return true;
								else
									return (_contentDisposition.IndexOf("attachment")==-1 && _contentType.IndexOf("text/plain")!=-1); */
				/*				if (_contentType==null)
									return true;
								else
									return (_fileName!="");*/
				if ((_contentType==null||_fileName=="") && _contentID==null)//&&_contentType.ToLower().IndexOf("text/")!=-1)
					return true;
				else
					return false;

			}
		}

		/// <summary>
		/// Content format
		/// </summary>
		public string ContentFormat
		{
			get{return _contentFormat;}
		}

		/// <summary>
		/// Content charset
		/// </summary>
		public string ContentCharset
		{
			get{return _contentCharset;}
		}

		

		/// <summary>
		/// Content Type
		/// </summary>
		public string ContentType
		{
			get{return _contentType;}
		}

		/// <summary>
		/// Content Transfer Encoding
		/// </summary>
		public string ContentTransferEncoding
		{
			get{return _contentTransferEncoding;}
		}

		/// <summary>
		/// Content Description
		/// </summary>
		public string ContentDescription
		{
			get{return _contentDescription;}
		}

		
		/// <summary>
		/// Content Disposition
		/// </summary>
		public string ContentDisposition
		{
			get{return _contentDisposition;}
		}

		/// <summary>
		/// Content ID
		/// </summary>
		public string ContentID
		{
			get{return _contentID;}
		}

		/// <summary>
		/// Raw Attachment
		/// </summary>
		public string RawAttachment
		{
			get{return _rawAttachment;}
		}

		/// <summary>
		/// decoded attachment in bytes
		/// </summary>
		public byte[] DecodedAttachment
		{
			get
			{
				return DecodedAsBytes();
			}
		}
		#endregion


		/// <summary>
		/// release all objects
		/// </summary>
		~Attachment()
		{
			_rawBytes=null;
			_rawAttachment=null;
		}

		/// <summary>
		/// New Attachment
		/// </summary>
		/// <param name="bytAttachment">attachment bytes content</param>
		/// <param name="lngFileLength">file length</param>
		/// <param name="strFileName">file name</param>
		/// <param name="strContentType">content type</param>
		public Attachment(byte[] bytAttachment, long lngFileLength, string strFileName, string strContentType)
		{
			_inBytes=true;
			_rawBytes=bytAttachment;
			_contentLength=lngFileLength;
			_fileName=strFileName;
			_contentType=strContentType;
		}

		/// <summary>
		/// New Attachment
		/// </summary>
		/// <param name="bytAttachment">attachment bytes content</param>
		/// <param name="strFileName">file name</param>
		/// <param name="strContentType">content type</param>
		public Attachment(byte[] bytAttachment, string strFileName, string strContentType)
		{
			_inBytes=true;
			_rawBytes=bytAttachment;
			_contentLength=bytAttachment.Length;
			_fileName=strFileName;
			_contentType=strContentType;
		}

		/// <summary>
		/// New Attachment
		/// </summary>
		/// <param name="strAttachment">attachment content</param>
		/// <param name="strContentType">content type</param>
		/// <param name="blnParseHeader">whether only parse the header or not</param>
		public Attachment(string strAttachment,string strContentType, bool blnParseHeader)
		{
			if(!blnParseHeader)
			{
				_fileName=_defaultMSTNEFFileName;
				_contentType=strContentType;
			}
			this.NewAttachment(strAttachment,blnParseHeader);
		}

		/// <summary>
		/// New Attachment
		/// </summary>
		/// <param name="strAttachment">attachment content</param>
		public Attachment(string strAttachment)
		{	
			this.NewAttachment(strAttachment,true);
		}

		/// <summary>
		/// create attachment
		/// </summary>
		/// <param name="strAttachment">raw attachment text</param>
		/// <param name="blnParseHeader">parse header</param>
		private void NewAttachment(string strAttachment, bool blnParseHeader)
		{
			_inBytes=false;

			if(strAttachment==null)
				throw new ArgumentNullException("strAttachment");

			StringReader srReader=new StringReader(strAttachment);

			if(blnParseHeader)
			{
				string strLine=srReader.ReadLine();
				while(MIMEUtility.IsNotNullTextEx(strLine))
				{
					ParseHeader(srReader,ref strLine);
					if(MIMEUtility.IsOrNullTextEx(strLine))
						break;
					else
						strLine=srReader.ReadLine();
				}
			}

			this._rawAttachment=srReader.ReadToEnd();
			_contentLength=this._rawAttachment.Length;
		}

		/// <summary>
		/// Parse header fields and set member variables
		/// </summary>
		/// <param name="srReader">string reader</param>
		/// <param name="strLine">header line</param>
		private void ParseHeader(StringReader srReader,ref string strLine)
		{
			string []array=MIMEUtility.GetHeadersValue(strLine);//Regex.Split(strLine,":");
			string []values=Regex.Split(array[1],";");//array[1].Split(';');
			string strRet=null;

			switch(array[0].ToUpper())
			{
				case "CONTENT-TYPE":
					if(values.Length>0)
						_contentType=values[0].Trim();					
					if(values.Length>1)
					{
						_contentCharset=MIMEUtility.GetQuotedValue(values[1],"=","charset");
					}
					if(values.Length>2)
					{
						_contentFormat=MIMEUtility.GetQuotedValue(values[2],"=","format");
					}
					_fileName=MIMEUtility.ParseFileName(strLine);
					if(_fileName=="")
					{
						strRet=srReader.ReadLine();
						if(strRet=="")
						{
							strLine="";
							break;
						}
						_fileName=MIMEUtility.ParseFileName(strLine);
                        if (_fileName == "" && strRet!=null)
							ParseHeader(srReader,ref strRet);
					}
					break;
				case "CONTENT-TRANSFER-ENCODING":
					_contentTransferEncoding=MIMEUtility.SplitOnSemiColon(array[1])[0].Trim();
					break;
				case "CONTENT-DESCRIPTION":
					_contentDescription=MIMEUtility.DecodeText(MIMEUtility.SplitOnSemiColon(array[1])[0].Trim());
					break;
				case "CONTENT-DISPOSITION":
					if(values.Length>0)
						_contentDisposition=values[0].Trim();
					
					///<bug>reported by grandepuffo @ https://sourceforge.net/forum/message.php?msg_id=2589759
					//_fileName=values[1];
					if(values.Length>1) 
					{
						_fileName=values[1];
					} 
					else 
					{
						_fileName="";
					}
					
					if(_fileName=="")
						_fileName=srReader.ReadLine();

					_fileName=_fileName.Replace("\t","");
					_fileName=MIMEUtility.GetQuotedValue(_fileName,"=","filename");
					_fileName=MIMEUtility.DecodeText(_fileName);
					break;
				case "CONTENT-ID":
					_contentID=MIMEUtility.SplitOnSemiColon(array[1])[0].Trim('<').Trim('>');
					break;
			}
		}

		/// <summary>
		/// verify the encoding
		/// </summary>
		/// <param name="encoding">encoding to verify</param>
		/// <returns>true if encoding</returns>
		private bool IsEncoding(string encoding)
		{
			return _contentTransferEncoding.ToLower().IndexOf(encoding.ToLower())!=-1;
		}

		/// <summary>
		/// Decode the attachment to text
		/// </summary>
		/// <returns>Decoded attachment text</returns>
		public string DecodeAsText()
		{
			string decodedAttachment=null;
			
			if ( _rawAttachment == null )
				_rawAttachment	= System.Text.ASCIIEncoding.ASCII.GetString( _rawBytes );

			try{
				if(_contentType.ToLower()=="message/rfc822".ToLower())
					decodedAttachment=MIMEUtility.DecodeText(_rawAttachment);
				else if(_contentTransferEncoding!=null)
				{
					decodedAttachment=_rawAttachment;

					if(!IsEncoding("7bit"))
					{
						if(IsEncoding("8bit")&&_contentCharset!=null&_contentCharset!="")
							decodedAttachment=MIMEUtility.Change(decodedAttachment,_contentCharset);

						if(MIMEUtility.IsQuotedPrintable(_contentTransferEncoding))
							decodedAttachment=DecodeQP.ConvertHexContent(decodedAttachment);
						//else if(IsEncoding("8bit"))
							//decodedAttachment=decodedAttachment;
						else
							decodedAttachment=MIMEUtility.deCodeB64s(MIMEUtility.RemoveNonB64(decodedAttachment));
					}
				}
				else if(_contentCharset!=null)
					decodedAttachment=MIMEUtility.Change(_rawAttachment,_contentCharset);//Encoding.Default.GetString(Encoding.GetEncoding(_contentCharset).GetBytes(_rawAttachment));
				else
					decodedAttachment=_rawAttachment;
			}
			catch
			{
				decodedAttachment=_rawAttachment;
			}
			return decodedAttachment;
		}

		/// <summary>
		/// decode attachment to be a message object
		/// </summary>
		/// <returns>message</returns>
		public Message DecodeAsMessage(){
			bool blnRet=false;
			return new Message(ref blnRet,"",false ,_rawAttachment,false);
		}

		/// <summary>
		/// Save the attachment to a file
		/// </summary>
		/// <param name="sFileName">Filename to save file as</param>
		/// <param name="bOverWrite">Overwrite file if it already exists</param>
		/// <returns>Success of operation</returns>
		public bool SaveAttachment( string sFileName, bool bOverWrite ){

			if ( System.IO.File.Exists( sFileName ) && bOverWrite == false )
				return false;

			System.IO.FileStream oFS	= System.IO.File.Open( sFileName , System.IO.FileMode.Create , System.IO.FileAccess.Write );

			byte[] bContent	= DecodedAsBytes();

			oFS.Write( bContent, 0, bContent.Length );
			oFS.Flush();
			oFS.Close();

			return true;
		}


		/// <summary>
		/// Decode the attachment to bytes
		/// </summary>
		/// <returns>Decoded attachment bytes</returns>
		public byte[] DecodedAsBytes(){

						
			if( _rawAttachment == null && _rawBytes != null )
				return _rawBytes;
			
			if( _rawAttachment == null )
				return null;


			if(_fileName!="")
			{
				byte []decodedBytes=null;

				if(_contentType!=null && _contentType.ToLower()=="message/rfc822".ToLower())
					decodedBytes=Encoding.Default.GetBytes(MIMEUtility.DecodeText(_rawAttachment));
				else if(_contentTransferEncoding!=null)
				{
					string bytContent=_rawAttachment;

					if(!IsEncoding("7bit"))
					{
						if(IsEncoding("8bit")&&_contentCharset!=null&_contentCharset!="")
							bytContent=MIMEUtility.Change(bytContent,_contentCharset);

						if(MIMEUtility.IsQuotedPrintable(_contentTransferEncoding))
							decodedBytes=Encoding.Default.GetBytes(DecodeQP.ConvertHexContent(bytContent));
						else if(IsEncoding("8bit"))
							decodedBytes=Encoding.Default.GetBytes(bytContent);
						else
							decodedBytes=Convert.FromBase64String(MIMEUtility.RemoveNonB64(bytContent));
					}
					else
						decodedBytes=Encoding.Default.GetBytes(bytContent);
				}
				else if(_contentCharset!=null)
					decodedBytes=Encoding.Default.GetBytes(MIMEUtility.Change(_rawAttachment,_contentCharset));//Encoding.Default.GetString(Encoding.GetEncoding(_contentCharset).GetBytes(_rawAttachment));
				else
					decodedBytes=Encoding.Default.GetBytes(_rawAttachment);

				return decodedBytes;
			}
			else
			{
				return null;
			}
		}

		public int CompareTo(object attachment)
		{
			return (this.RawAttachment.CompareTo(((Attachment)(attachment)).RawAttachment));
		}
	}
}

