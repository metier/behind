
using System;

namespace jlib.net.pop
{
	/// <summary>
	/// 3 message importance types defined by RFC
	/// </summary>
	/// <remarks>
	/// </remarks>
	public enum MessageImportanceType
	{
		HIGH=5,NORMAL=3,LOW=1
	}

}
