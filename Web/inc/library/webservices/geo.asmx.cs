using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using jlib.net;
using jlib.functions;

namespace jlib.webservices {
    /// <summary>
    /// Summary description for ip
    /// </summary>
    [WebService( Namespace = "http://tempuri.org/" )]
    [WebServiceBinding( ConformsTo = WsiProfiles.BasicProfile1_1 )]
    [ToolboxItem( false )]
    public class geo : System.Web.Services.WebService {

		[WebMethod]
		public double getDistanceKM(double dLat1, double dLon1, double dLat2, double dLon2) {
			return jlib.functions.geo.convert.getDistanceKM(dLat1, dLon1, dLat2, dLon2);
		}
		[WebMethod]
		public double getDrivingDistanceKM(double dLat1, double dLon1, double dLat2, double dLon2) {
			return jlib.functions.geo.convert.getDrivingDistanceKM(dLat1, dLon1, dLat2, dLon2);
		}
		[WebMethod]
        public string getCoordinates( string sStreet, string sCity, string sZip ) {
            return geo.getCoordinatesStatic( sStreet, sCity, sZip );
        }
        public static string getCoordinatesStatic(string sStreet, string sCity, string sZip) {
            return getCoordinatesStatic(sStreet, sCity, sZip, null, null);
        }
        public static string getCoordinatesStatic( string sStreet, string sCity, string sZip, System.Net.CookieContainer oCookies, System.Net.WebProxy oProxy ) {

            jlib.db.ado_helper oDBBids = new jlib.db.ado_helper();
            DataTable oDT = jlib.db.sqlbuilder.getDataTable( oDBBids, String.Format("select * from d_reviewable_items where address1 = '{0}' AND city = '{1}' AND zip = '{2}' AND ( lat <> 0 AND lat > -200 ) AND address_lookup_attempt = 1", parse.replaceAll(sStreet, "'", "''"), parse.replaceAll(sCity, "'", "''"), parse.replaceAll(sZip, "'", "''")));
            if (oDT.Rows.Count > 0) {
                return oDT.Rows[0]["lat"] + "|" + oDT.Rows[0]["lon"];
            }
            

            sStreet = parse.replaceAll( System.Web.HttpUtility.UrlEncode( sStreet.ToUpper() ), "%c3%86" , "%C6", "%c3%98" , "%D8", "%c3%85", "%C5", "%c3%89", "%C9" ) ;
            sCity = parse.replaceAll( System.Web.HttpUtility.UrlEncode( sCity.ToUpper() ), "%c3%86", "%C6", "%c3%98", "%D8", "%c3%85", "%C5", "%c3%89", "%C9" );

            string sUrl = "http://kart.gulesider.no/kart/search.c?q=" + sStreet + "+" + sZip + "+" + sCity;           
            HTTP oHTTP = new HTTP();
            oHTTP.UseCache = true;
            oHTTP.CacheDSN = System.Configuration.ConfigurationManager.AppSettings["sql.dsn.httpcache"];

            string sData = oHTTP.Get( sUrl, oCookies, oProxy );

            float fEast = (convert.cFlt( parse.inner_substring( sData, "name=\"w\"", "\"", "\"", null ) ) + convert.cFlt( parse.inner_substring( sData, "name=\"e\"", "\"", "\"", null ) )) / 2;
            float fNorth = ( convert.cFlt( parse.inner_substring( sData, "name=\"s\"", "\"", "\"", null ) ) + convert.cFlt( parse.inner_substring( sData, "name=\"n\"", "\"", "\"", null ) ) ) / 2;
            
            if ( (fEast == 0 || fNorth == 0) && oHTTP.FromCache && oHTTP.CacheAge.AddDays( 20 ) < System.DateTime.Now ) {
                oHTTP.clearCache( sUrl );
                sData = oHTTP.Get( sUrl, oCookies, oProxy );
            }

            fEast = (convert.cFlt( parse.inner_substring( sData, "name=\"w\"", "\"", "\"", null ) ) + convert.cFlt( parse.inner_substring( sData, "name=\"e\"", "\"", "\"", null ) ))/2;
            fNorth = ( convert.cFlt( parse.inner_substring( sData, "name=\"s\"", "\"", "\"", null ) ) + convert.cFlt( parse.inner_substring( sData, "name=\"n\"", "\"", "\"", null ) ) ) / 2;

            if (fEast == 0 && fNorth == 0 && sData.IndexOf("ga f&oslash;lgende treff") > -1 && sData.IndexOf("<input type=\"submit\" value=\"Velg\" class=\"knapp\" />") > -1) {
                sUrl = "http://kart.gulesider.no/kart/map.c?imgt=MAP&id=" + parse.inner_substring(sData, "value=\"MAP\"", "value=\"", "\"", null);
                sData = oHTTP.Get(sUrl, oCookies, oProxy);
                fEast = (convert.cFlt(parse.inner_substring(sData, "name=\"w\"", "\"", "\"", null)) + convert.cFlt(parse.inner_substring(sData, "name=\"e\"", "\"", "\"", null))) / 2;
                fNorth = (convert.cFlt(parse.inner_substring(sData, "name=\"s\"", "\"", "\"", null)) + convert.cFlt(parse.inner_substring(sData, "name=\"n\"", "\"", "\"", null))) / 2;
            }

            //lat|lon
            return fNorth + "|" + fEast;                
        }

		public static string getCoordinatesStatic( string sStreet, string sCity, string sZip, string sCountry) {
			return parse.outer_substring(getCoordinatesStaticAccuracy(sStreet, sCity, sZip, sCountry), null, null, "|", null);			
		}

		public static string getCoordinatesStaticAccuracy(string sStreet, string sCity, string sZip, string sCountry) {
			HTTP oHTTP = new HTTP();
			oHTTP.UseCache = true;
			oHTTP.CacheDSN = System.Configuration.ConfigurationManager.AppSettings["sql.dsn.httpcache"];

			sStreet = parse.replaceAll(sStreet.ToLower(), "fr nansen", "fridtjof nansen", "fr. nansen", "fridtjof nansen", "thv ", "thorvald ", "thv. ", "thorvald ", "kr ", "kristian ", "kr. ", "kristian ", "chr. ", "christian ", "chr ", "christian ");
			if (sStreet.IndexOf("g. ") > -1) sStreet = parse.replaceAll(sStreet, "g. ", "gata ");
			if (sStreet.IndexOf("v. ") > -1) sStreet = parse.replaceAll(sStreet, "v. ", "veien ");
			if (sStreet.IndexOf("g ") > -1) {
				string[] sArr = parse.split(sStreet, "g");
				for (int x = 0; x < sArr.Length; x++) {
					if (sArr.Length > x + 1 && sArr[x + 1].Trim().Length > 0 && jlib.functions.convert.isNumeric(sArr[x + 1].Trim().Substring(0, 1))) {
						sArr[x + 1] = "ata" + sArr[x + 1];
					}
				}
				sStreet = parse.join(sArr, "g");
			}

			if (sStreet.IndexOf("v ") > -1) {
				string[] sArr = parse.split(sStreet, "v");
				for (int x = 0; x < sArr.Length; x++) {
					if (sArr.Length > x + 1 && sArr[x + 1].Trim().Length > 0 && jlib.functions.convert.isNumeric(sArr[x + 1].Trim().Substring(0, 1))) {
						sArr[x + 1] = "ei" + sArr[x + 1];
					}
				}
				sStreet = parse.join(sArr, "v");
			}


			////if street number spans several numbers.. ie. Torrggata 2-4. Google is smart enough to figure this out!
			//sStreet = parse.stripEndingCharacter(sStreet, "-", " ");			
			//if (sStreet.IndexOf("-") > -1 && convert.cInt((sStreet + " ").Substring(sStreet.LastIndexOf("-") + 1,1)) != 0) {
			//    string[] sArr = parse.split(sStreet, "-");
			//    sStreet = "";
			//    bool bFound = false;
			//    for (int x = sArr.Length - 1; x >= 0; x--) {
			//        if (!bFound && !convert.isNumeric(sArr[x])) bFound = true;
			//        if (bFound) sStreet = sArr[x] + ( sStreet != "" && x != 0 ? "-" : "" ) + sStreet;										
			//    }
			//}

			//string sData = oHTTP.Get("http://maps.google.com/maps/geo?q=" + HttpUtility.UrlEncode(sStreet) + "," + HttpUtility.UrlEncode(sZip) + "+" + HttpUtility.UrlEncode(sCity) + "," + HttpUtility.UrlEncode(sCountry) + "&output=xml&key=ABQIAAAAiq3-lbKfQMkyDYNQ7KmleBSU2mEepNNPdzIbTJ3ZRWTsYw3YTRT8FWTBt2YPSWkWQhE_t_uKnC9m-g");
			//if (sData.IndexOf(" xmlns=") > -1) sData = parse.inner_substring(sData, null, null, "xmlns=", null) + parse.inner_substring(sData, " xmlns=\"", "\"", null, null);
			//System.Xml.XmlDocument oDoc = new System.Xml.XmlDocument();
			//oDoc.LoadXml(sData);
			//System.Xml.XmlNode xmlNode = oDoc.SelectSingleNode("//coordinates");
			//if (xmlNode == null) return "0|0|0";
			//return parse.splitValue(xmlNode.InnerText, ",", 1) + "|" + parse.splitValue(xmlNode.InnerText, ",", 0);
			string sData = oHTTP.Get("http://maps.google.com/maps/geo?q=" + HttpUtility.UrlEncode(sStreet) + "," + HttpUtility.UrlEncode(sZip) + "+" + HttpUtility.UrlEncode(sCity) + "," + HttpUtility.UrlEncode(sCountry) + "&output=csv&key=ABQIAAAAiq3-lbKfQMkyDYNQ7KmleBSU2mEepNNPdzIbTJ3ZRWTsYw3YTRT8FWTBt2YPSWkWQhE_t_uKnC9m-g");
			if (!sData.StartsWith("200,")) return "0|0|0";
			return parse.split(sData, ",")[2] + "|" + parse.split(sData, ",")[3] + "|" + parse.split(sData, ",")[1];
		}
    }
}
