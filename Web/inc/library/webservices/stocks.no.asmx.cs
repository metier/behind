using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using jlib.net;
using jlib.functions;
using System.Xml;

namespace jlib.webservices.stocks {
    /// <summary>
    /// Summary description for ip
    /// </summary>
    [WebService( Namespace = "http://tempuri.org/" )]
    [WebServiceBinding( ConformsTo = WsiProfiles.BasicProfile1_1 )]
    [ToolboxItem( false )]
    public class no : System.Web.Services.WebService {

        [WebMethod]
        public XmlDocument getQuoteNetfonds( string sTicker ) {

            HTTP oHTTP = new HTTP();
            string sData = oHTTP.Get( "http://norma.netfonds.no/ppaper.php?paper=" + sTicker );

            if ( sData.IndexOf( "Feil Denne aksjen finnes ikke." ) > -1 ) return null;

            XmlDocument oDoc = new XmlDocument();
            oDoc.LoadXml( "<quote source=\"netfonds\" current_time=\"" + parse.inner_substring( sData, "Lokal tid i Norge: ", null, ".", null ) + "\" quote_time=\"" + parse.inner_substring( sData, "Sist oppdatert: ", null, "<", null ) + "\"><tick /><book /><news /></quote>" );
            
            XmlElement oEle = oDoc.SelectSingleNode( "//tick" ) as XmlElement;
            oEle.SetAttribute( "last", parse.stripHTML( parse.inner_substring( ref sData, "<th>Siste</th>", "<tr valign=top>", "</td>", null, true ) ) );
            oEle.SetAttribute( "change", parse.stripHTML( parse.inner_substring( ref sData, "<td", ">", "</td>", null, true ) ) );
            oEle.SetAttribute( "change_pct", parse.stripHTML( parse.inner_substring( ref sData, "<td", ">", "</td>", null, true ) ) );
            oEle.SetAttribute( "buy", parse.stripHTML( parse.inner_substring( ref sData, "<td", ">", "</td>", null, true ) ) );
            oEle.SetAttribute( "sell", parse.stripHTML( parse.inner_substring( ref sData, "<td", ">", "</td>", null, true ) ) );
            oEle.SetAttribute( "open", parse.stripHTML( parse.inner_substring( ref sData, "<td", ">", "</td>", null, true ) ) );
            oEle.SetAttribute( "hi", parse.stripHTML( parse.inner_substring( ref sData, "<td", ">", "</td>", null, true ) ) );
            oEle.SetAttribute( "low", parse.stripHTML( parse.inner_substring( ref sData, "<td", ">", "</td>", null, true ) ) );
            oEle.SetAttribute( "prev", parse.stripHTML( parse.inner_substring( ref sData, "<td", ">", "</td>", null, true ) ) );
            oEle.SetAttribute( "vol", parse.replaceAll( parse.stripHTML( parse.inner_substring( ref sData, "<td", ">", "</td>", null, true ) ) , " ", "" ));
            oEle.SetAttribute( "value", parse.replaceAll( parse.stripHTML( parse.inner_substring( ref sData, "<td", ">", "</td>", null, true ) ) , " " , "" ));
            oEle.SetAttribute( "trades", parse.replaceAll( parse.stripHTML( parse.inner_substring( ref sData, "<td", ">", "</td>", null, true ) ), " ", "" ));

            oEle = oDoc.SelectSingleNode( "//book" ) as XmlElement;
            string sDepth = parse.inner_substring( sData, "<th>Tilbud</th>", "<tr ", "Forholdstall", null );
            string[] sArr = parse.split( sDepth, "<tr " );
            for ( int x = 0; x < sArr.Length - 1; x++ ) {
                XmlElement oEle1 = oDoc.CreateElement( "order" );
                
                if ( parse.stripHTML( parse.inner_substring( sArr[x], "<td ", ">", "<", null ) ) != "" )
                    oEle.AppendChild( oEle1 );

                oEle1.SetAttribute( "type", "buy" + ( x == sArr.Length - 2 ? "_total" : "" ) );
                oEle1.SetAttribute( "num_orders", parse.replaceAll( parse.inner_substring( ref sArr[x], "<td ", ">", "<", null, true ), " " , "" ) );
                oEle1.SetAttribute( "vol", parse.replaceAll( parse.inner_substring( ref sArr[x], "<td ", ">", "<", null, true ) , " " , "" ));
                parse.inner_substring( ref sArr[x], "<td ", ">", "<", null, true );
                if( x != sArr.Length -2 )
                    oEle1.SetAttribute( "price", parse.replaceAll( parse.inner_substring( ref sArr[x], "<td ", ">", "<", null, true ), " " , "" ) );

                oEle1 = oDoc.CreateElement( "order" );
                if ( parse.stripHTML( parse.inner_substring( sArr[x], "<td ", ">", "<", null ) ) != "" )
                    oEle.AppendChild( oEle1 );


                oEle1.SetAttribute( "type", "sell" + ( x == sArr.Length - 2 ? "_total" : "" ) );
                if ( x != sArr.Length - 2 )
                    oEle1.SetAttribute( "price", parse.replaceAll( parse.inner_substring( ref sArr[x], "<td ", ">", "<", null, true ) , " " , "" ));
                parse.inner_substring( ref sArr[x], "<td ", ">", "<", null, true );

                oEle1.SetAttribute( "vol", parse.replaceAll( parse.inner_substring( ref sArr[x], "<td ", ">", "<", null, true ), " ", "" ) );
                oEle1.SetAttribute( "num_orders", parse.replaceAll( parse.inner_substring( ref sArr[x], "<td ", ">", "<", null, true ) , " " , "" ));                
                
            }

            return oDoc;
        }

    }
}


//http://norma.netfonds.no/ppaper.php?paper=cod


//https://www.nordnet.se/tux/lang/flash/sprak_flashquote.pl?oversattning=%5Btype+Function%5D&input=pan&marknad=Norge
//https://www.nordnet.se/tux/lang/flash/orderdjup_flash.pl?a1=NO0003054108&market=Norge
//https://www.nordnet.se/tux/lang/flash/aktievis_realtid_flash?inputText=PAN&kurs=5%2E56+NOK&marknad=Norge
//https://www.nordnet.se/tux/lang/m/nyheter_isin_snap.pl?isin=NO0003054108&market=Norge 



//POST /urs.asmx?MSPRU-Client-Key=V8MA86i76DUwYu1rTECy1g%3d%3d&MSPRU-Patented-Lock=d7t8SwxSmiI%3d HTTP/1.1
//POST /urs.asmx?MSPRU-Client-Key=V8MA86i76DUwYu1rTECy1g%3d%3d&MSPRU-Patented-Lock=d7t8SwxSmiI%3d HTTP/1.1


//POST /urs.asmx?MSPRU-Client-Key=V8MA86i76DUwYu1rTECy1g%3d%3d&MSPRU-Patented-Lock=d7t8SwxSmiI%3d HTTP/1.1
//Accept: text/*
//SOAPAction: "http://Microsoft.STS.STSWeb/Lookup"
//Content-Type: text/xml; charset=utf-8
//User-Agent: VCSoapClient
//Host: urs.microsoft.com
//Content-Length: 661
//Cache-Control: no-cache

//<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"><soap:Body><Lookup xmlns="http://Microsoft.STS.STSWeb/"><r soapenc:arrayType="xsd:string[1]"><string>https://www.nordnet.se/tux/lang/flash/sprak_flashquote.pl</string></r><ID>{B3BB5BBA-E7D5-40AB-A041-A5B1C0B26C8F}</ID><v soapenc:arrayType="xsd:string[5]"><string>7.0.6004.3</string><string>7.00.5824.16386</string><string>7.0.5730.11</string><string>5.1.2600.2.0</string><string>en-us</string></v></Lookup></soap:Body></soap:Envelope>
