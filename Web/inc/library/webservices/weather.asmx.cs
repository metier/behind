using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Xml;
using jlib.db;
using jlib.functions;
using jlib.net;


namespace jlib.webservices {
    /// <summary>
    /// Summary description for weather
    /// </summary>
    [WebService( Namespace = "http://tempuri.org/" )]
    [WebServiceBinding( ConformsTo = WsiProfiles.BasicProfile1_1 )]
    [ToolboxItem( false )]
    public class weather : System.Web.Services.WebService {

		public static XmlDocument getWeatherA(string sZip, string sPlaceID, string sPlaceName, string sPlaceCounty, double dLat, double dLon, int iMaxNumMinutesOld) {
			return getWeatherA(sZip, sPlaceID, sPlaceName, sPlaceCounty, dLat, dLon, iMaxNumMinutesOld, -9000,-9000, -10000,"", "") ;
		}
		public static XmlDocument getWeatherA(string sZip, string sPlaceID, string sPlaceName, string sPlaceCounty, double dLat, double dLon, int iMaxNumMinutesOld, double dNorthing, double dEasting, int iAltitude, string sLocationName, string sWeatherUrl) {

			XmlDocument oDoc = new XmlDocument();
			XmlElement oForecast = oDoc.CreateElement("forecast");
			oDoc.AppendChild(oForecast);

			XmlElement oLocation = oDoc.CreateElement("location");
			oForecast.AppendChild(oLocation);

			if (convert.cStr(sWeatherUrl) == "") {
				if (iAltitude < -1000) {
					ado_helper oData = new ado_helper();
					DataTable oDT = null;

					if (sZip != "") {
						oDT = sqlbuilder.executeSelect("a_zipcodes", "zip", sZip, new condition("lat", condition.ConditionTypes.GreaterThan, 0));
						if (oDT.Rows.Count == 0) oDT = sqlbuilder.executeSelect("a_zipcodes", new condition("zip", condition.ConditionTypes.Like, sZip.Substring(0, 3) + "%"), new condition("lat", condition.ConditionTypes.GreaterThan, 0));
					} else if (sPlaceID != "") {
						oDT = sqlbuilder.executeSelect("a_zipcodes", "id", sPlaceID);
					} else if (sPlaceName != "") {
						oDT = sqlbuilder.executeSelect("a_zipcodes", "name", sPlaceName, "county", sPlaceCounty);
					}

					if (oDT != null) {
						dLat = convert.cDbl(oDT.Rows[0]["lat"]);
						dLon = convert.cDbl(oDT.Rows[0]["lon"]);
					}

					DataTable oDT1 = oData.Execute_SQL("select top 1 * from a_regions where source = 'www.storm.no' order by dbo.sp_zipcode_distance( lat, lon, " + parse.replaceAll(dLat.ToString(), ",", ".") + ", " + parse.replaceAll(dLon.ToString(), ",", ".") + ")");
					iAltitude = convert.cInt(oDT1.Rows[0]["altitude"]);
					sLocationName = convert.cStr(oDT1.Rows[0]["name"]);

					for (int x = 0; x < oDT1.Columns.Count; x++)
						oLocation.SetAttribute(oDT1.Columns[x].Caption, convert.cStr(oDT1.Rows[0][x]));

					int iUtm;
					jlib.functions.geo.convert.LatLonToUTM(dLat, dLon, out iUtm, out dEasting, out dNorthing);
					dLat = convert.cDbl(oDT1.Rows[0]["lat"]);
					dLon = convert.cDbl(oDT1.Rows[0]["lon"]);
				} else {
					oLocation.SetAttribute("altitude", iAltitude.ToString());
					oLocation.SetAttribute("name", sLocationName);
					oLocation.SetAttribute("easting", dEasting.ToString());
					oLocation.SetAttribute("northing", dNorthing.ToString());
				}
			}
            

            HTTP oHTTP = new HTTP();
            oHTTP.UseCache = ( iMaxNumMinutesOld > 0 );
            oHTTP.CacheMaxAge = System.DateTime.Now.AddMinutes( -iMaxNumMinutesOld );
            oHTTP.CacheDSN = System.Configuration.ConfigurationManager.AppSettings["sql.dsn.httpcache"];

                                    
            //string sData = oHTTP.Get( "http://www.storm.no/tv2/Default.aspx?utmx=" + convert.cInt( dEasting ) + "&utmy=" + convert.cInt( dNorthing ) + "&meter=" + iAltitude + "&sted=" + sLocationName, null, null, "http://pub.tv2.no/nettavisen/timeout/fiveminutes/TV2/na/frontStorm.html" );
			string sData = "";
			if( convert.cStr(sWeatherUrl) == "" )
				sData = oHTTP.Get("http://full.storm.no/tv2/Default.aspx?utmx=" + convert.cInt(dEasting) + "&utmy=" + convert.cInt(dNorthing) + "&meter=" + iAltitude + "&sted=" + sLocationName, null, null, "http://pub.tv2.no/nettavisen/timeout/fiveminutes/TV2/na/frontStorm.html");
			else
				sData = oHTTP.Get(sWeatherUrl, null, null, "http://pub.tv2.no/nettavisen/timeout/fiveminutes/TV2/na/frontStorm.html");
				

            //sData               = parse.inner_substring( sData, 
            DateTime oStartDate = DateTime.MinValue;
            string[] sArr = parse.split( sData, "weathercell2" );
            for ( int x = 1; x < sArr.Length; x++ ) {
                XmlElement oDay = oDoc.CreateElement( "day" );
                oForecast.AppendChild( oDay );
                if ( oStartDate == DateTime.MinValue ) {
                    string sDay = parse.inner_substring( sArr[x], ">", " ", "<", null ) + "." + System.DateTime.Now.Year;
                    oStartDate = DateTime.Parse( sDay, new System.Globalization.CultureInfo( "nb-NO" ) );
                } else {
                    oStartDate = oStartDate.AddDays( 1 );
                }
                oDay.SetAttribute( "date", oStartDate.ToShortDateString() );

                string[] sArr1 = parse.split( sArr[x], "</TABLE>" );
                for ( int y = 1; y < sArr1.Length - 2; y++ ) {
                    if ( sArr1[y].IndexOf( "weathercell>" ) == -1 ) break;
                    XmlElement oTime = oDoc.CreateElement( "time" );
                    oDay.AppendChild( oTime );
                    oTime.SetAttribute( "time", parse.inner_substring( ref sArr1[y], "weathercell>", null, "<", null, true ) );

                    string sWeather = "";
                    int iWeather = convert.cInt(parse.inner_substring( ref sArr1[y], "imagesnew/", null, ".gif", null, true ));
                    if ( iWeather == 1 ) sWeather = "sun";                    
                    else if ( iWeather == 2 ) sWeather = "sun_small_cloud";
                    else if ( iWeather == 3 ) sWeather = "cloud_white_sun";
                    else if ( iWeather == 4 ) sWeather = "cloud_white";
                    else if ( iWeather == 5 ) sWeather = "cloud_grey_sun_2rain";
                    else if ( iWeather == 6 ) sWeather = "cloud_grey_sun_2rain_lightning";
                    else if ( iWeather == 7 ) sWeather = "cloud_grey_sun_rain_snow";
                    else if ( iWeather == 8 ) sWeather = "cloud_white_sun_snow";
                    else if ( iWeather == 9 || iWeather == 10 ) sWeather = "cloud_grey_3rain";
                    else if ( iWeather == 11 ) sWeather = "cloud_grey_3rain_lightning";
                    else if ( iWeather == 12 ) sWeather = "cloud_grey_3rain_snow";
                    else if ( iWeather == 13 ) sWeather = "cloud_white_snow";
                    else if ( iWeather == 14 ) sWeather = "cloud_grey_snow_lightning";
                    else if ( iWeather == 15 ) sWeather = "fog";
                    else if ( iWeather == 16 ) sWeather = "moon";
                    else if ( iWeather == 17 ) sWeather = "moon_small_cloud";
                    else if ( iWeather == 18 ) sWeather = "cloud_grey_moon_2rain";
                    else if ( iWeather == 19 ) sWeather = "cloud_white_moon_snow";                                        
                    
                    oTime.SetAttribute( "weather", sWeather );
                    oTime.SetAttribute( "cloudcover", parse.replaceAll( parse.inner_substring( ref sArr1[y], "Skydekke:", null, "'", null, true ), "&nbsp;", "" ) );
                    oTime.SetAttribute( "temperature", parse.replaceAll( parse.inner_substring( ref sArr1[y], "<td", ">", "<", null, true ), "&nbsp;", "" ) );
                    oTime.SetAttribute( "wind.direction", parse.replaceAll( parse.inner_substring( ref sArr1[y], "imagesnew/", null, ".", null, true ), "&nbsp;", "" ) );
                    oTime.SetAttribute( "wind.description", parse.replaceAll( parse.inner_substring( ref sArr1[y], "alt='", null, "'", null, true ), "&nbsp;", "" ) );
                    oTime.SetAttribute( "wind.strength", parse.replaceAll( parse.inner_substring( ref sArr1[y], "<td", ">", "<", null, true ), "&nbsp;", "", "\n", "", "\r", "" ) );
                }
            }
            return oDoc;
        }

        [WebMethod]
        public XmlDocument getWeather( string sZip, string sPlaceID, string sPlaceName, string sPlaceCounty, double dLat, double dLon, int iMaxNumMinutesOld ) {
            return weather.getWeatherA( sZip, sPlaceID, sPlaceName, sPlaceCounty, dLat, dLon, iMaxNumMinutesOld );            
        }

        [WebMethod]
        public XmlDocument convertLatLonToUTM( double dLat, double dLon  ) {
            XmlDocument oDoc = new XmlDocument();
            XmlElement oResult = oDoc.CreateElement( "result" );
            oDoc.AppendChild( oResult );

            double dEasting, dNorthing;
            int iUtm;
            jlib.functions.geo.convert.LatLonToUTM( dLat, dLon, out iUtm, out dEasting, out dNorthing );

            oResult.SetAttribute("easting", dEasting.ToString() );
            oResult.SetAttribute("northing", dNorthing.ToString() );
            oResult.SetAttribute("utm" , iUtm.ToString() );

            return oDoc;
        }
    }
}
