using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using jlib.net;
using jlib.db;
using jlib.functions;

namespace jlib.webservices {
    /// <summary>
    /// Summary description for ip
    /// </summary>
    [WebService( Namespace = "http://tempuri.org/" )]
    [WebServiceBinding( ConformsTo = WsiProfiles.BasicProfile1_1 )]
    [ToolboxItem( false )]
    public class ip : System.Web.Services.WebService {

        [WebMethod]
        public string getCityName( string sIP ) {

            string sUrl = "http://www.ipaddresslocation.org/ip-address-location.php?ip=" + sIP;
            HTTP oHTTP = new HTTP();
            oHTTP.UseCache = true;
            oHTTP.CacheDSN = System.Configuration.ConfigurationManager.AppSettings["sql.dsn.httpcache"];

            string sData = oHTTP.Get( sUrl );

            if ( sData.IndexOf( "Guessed City:" ) == -1 && oHTTP.FromCache && oHTTP.CacheAge.AddDays( 20 ) < System.DateTime.Now ) {
                oHTTP.clearCache( sUrl );
                sData = oHTTP.Get( sUrl );
            }

            if ( sData.IndexOf( "Guessed City:" ) > -1 ) {
                return parse.stripWhiteChars(parse.stripHTML( parse.inner_substring( sData, "Guessed City:", null, "<", null ) ));
            } else {                
                return "";
            }            
        }

        [WebMethod]
        public string getCityNameLocal( string sIP ) {

            double oIP  = convert.cDbl( parse.splitValue( sIP, ".", 0 )) * 16777216 
            + convert.cDbl( parse.splitValue( sIP, ".", 1 )) * 65536 
            + convert.cDbl( parse.splitValue( sIP, ".", 3 )) * 256 
            + convert.cDbl( parse.splitValue( sIP, ".", 3 )) ;


            ado_helper oData = new ado_helper( System.Configuration.ConfigurationManager.AppSettings["sql.dsn"] );
            DataTable oDT = oData.Execute_SQLF( "select loc_id from d_geoip_ips where start_ip <= {0} and end_ip >= {0}", oIP );
            if ( oDT.Rows.Count == 0 ) return "";

            oDT = oData.Execute_SQLF( "select * from d_geoip_locs where locID = {0}", oDT.Rows[0][0] );
            return oDT.Rows[0]["city"] + "|" + oDT.Rows[0]["lat"] + "|" + oDT.Rows[0]["lon"];

        }
    }
}
