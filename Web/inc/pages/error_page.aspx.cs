﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using jlib.functions;
using jlib.net;

public partial class inc_pages_error_page : jlib.components.webpage
{  
    protected void Page_Init(object sender, EventArgs e) {

        string sActualUrl = (convert.cStr(Request.Url.Query).StartsWith("?404;") ? "/" + parse.inner_substring(Request.Url.Query, "//", "/", null, null) : Request.Url.AbsolutePath);

        //if (convert.cStr(Request.Url.Query).StartsWith("?404;")) {
        //    Response.Status = "404 Not Found";
        //    if (!Request.Url.Query.ToLower().EndsWith(".jpg") && !Request.Url.Query.ToLower().EndsWith(".gif")) {
        //        sActualUrl = parse.replaceAll(sActualUrl, "%C3%A6", "æ", "%C3%B8", "ø", "%C3%A5", "å", "%C3%86", "Æ", "%C3%98", "Ø", "%C3%85", "Å");
        //        DataTable oDT = sqlbuilder.executeSelect("d_url_alias", "url", sActualUrl);
        //        if (oDT.Rows.Count > 0)
        //            Response.Redirect(convert.cStr(oDT.Rows[0]["redirect_url"]));
        //    }
        //}
        //if (sActualUrl.StartsWith("/_")) {
        //    //jlib.db.ado_helper oData = new jlib.db.ado_helper(ConfigurationManager.AppSettings["sql_dsn.emailer"]);
        //    //DataTable oRedirect = sqlbuilder.executeSelect(oData, "d_url_alias", "url", sActualUrl);

        //}
        if (this.QueryString("e") == "404") {
            Response.Status = "404 Not Found";
            Response.StatusCode = 404;
        } else if (this.QueryString("e") == "500") {
            Response.Status = "500 Internal Server Error";
            Response.StatusCode = 500;
        }
        
    }
}
