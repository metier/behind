﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using jlib.functions;
using jlib.components;
using System.Security.Cryptography;
using jlib.db;
using System.Linq;
namespace Phoenix.LearningPortal.Inc.Master
{
    public partial class MyMetier : Util.Classes.masterpage
    {
        public enum SiteSection
        {
            Overview = 0,
            Participants = 1,
            Merits = 2,
            //Forum = 3,
            Catalog = 3,
            Other = -1,
            Reporting = 4,
            Files = 5
        }
        private string m_sLoginPage = System.Web.HttpContext.Current.Request.ApplicationPath + "/login.aspx";
        private bool m_bDisableWrapper = false, m_bDisableLoginRedirect = false;
        private Util.Classes.user m_oActiveUser = null;        
        private SiteSection m_oSiteSection = SiteSection.Other;
        private List<string> m_oTranslationSubscriptions = new List<string>();


        public string LoginPage
        {
            get
            {
                return m_sLoginPage;
            }
            set
            {
                m_sLoginPage = value;
            }
        }
        public bool DisableWrapper
        {
            get
            {
                return m_bDisableWrapper;
            }
            set
            {
                m_bDisableWrapper = value;
            }
        }
        public bool DisableLoginRedirect
        {
            get
            {
                return m_bDisableLoginRedirect;
            }
            set
            {
                m_bDisableLoginRedirect = value;
            }
        }
        public SiteSection Section
        {
            get
            {
                return m_oSiteSection;
            }
            set
            {
                m_oSiteSection = value;
            }
        }

        public Util.Classes.user ActiveUser
        {
            get
            {
                if (m_oActiveUser == null) m_oActiveUser = Util.Classes.user.getUser(new Util.Phoenix.session().GetUserId());
                return m_oActiveUser;
            }
            set
            {
                m_oActiveUser = value;
            }
        }
        //public DataRow CurrentUser {
        //    get {
        //        if (ActiveUser.Row != null && ActiveUser.Row["id"].Int()==0) return null;
        //        return ActiveUser.Row;
        //    }
        //}
        public Util.Classes.organization CurrentOrg
        {
            get
            {
                return ActiveUser.Organization;
            }
        }
        public override void Page_PreInit(object sender, EventArgs e)
        {
            WebPage.LanguageCompanyAndSiteNeeded += WebPage_LanguageCompanyAndSiteNeeded;
        }

        private void WebPage_LanguageCompanyAndSiteNeeded(object sender, out string sLanguage, out string sSiteId, out string sCompanyId)
        {
            sLanguage = ActiveUser.UserLanguage;
            sSiteId = ActiveUser.DistributorId.Str();
            if (ActiveUser.Organization != null)
                sCompanyId = ActiveUser.Organization.MasterCompanyID.Str();
            else
                sCompanyId = "";
        }

        public void SubscribeTranslation(string url)
        {
            m_oTranslationSubscriptions.AddIfNotExist(url);
        }
        public override void Page_Init(object sender, EventArgs e)
        {
            string sValue = Util.Permissions.decryptStringWithHash(jlib.net.HTTP.getCookieValue(Request.Cookies["user-authentication"]));
            Session["user.id"] = null;
            if (sValue != "")
            {
                DateTime oExpiration = convert.cDate(parse.splitValue(sValue, " ", 1).Replace("+", " "));
                if (oExpiration > DateTime.Now)
                {
                    Util.Permissions.setLoginData(oExpiration < DateTime.Now.AddHours(1), convert.cInt(parse.splitValue(sValue, " ", 0)), convert.cInt(parse.splitValue(sValue, " ", 3)), (Util.Permissions.UserType)convert.cInt(parse.splitValue(sValue, " ", 2)), DateTime.Now.AddHours(2), convert.cInt(parse.splitValue(sValue, " ", 4)));
                    m_oActiveUser = null;
                }
            }

            if (System.Configuration.ConfigurationManager.AppSettings["maintenance.warning"].Bln() && !(System.Configuration.ConfigurationManager.AppSettings["mymetier.ips"]).Str().Contains(";" + (Page as webpage).IP + ";") && !Request.Url.AbsoluteUri.ContainsI("download.aspx"))
            {
                if (Request.Url.AbsoluteUri.ContainsI("tester=true")) Response.SetCookie(new HttpCookie("tester", "true"));
                if (jlib.net.HTTP.getCookieValue(Request.Cookies["tester"]).Bln() || jlib.net.HTTP.getCookieValue(Response.Cookies["tester"]).Bln())
                {
                    if (Request.Url.AbsoluteUri.ContainsI("/maintenance-warning.aspx")) Response.Redirect("login.aspx");
                }
                else
                {
                    if (!Request.Url.AbsoluteUri.ContainsI("/maintenance-warning.aspx") && !Request.Url.AbsoluteUri.ContainsI("/migrate/")) Response.Redirect("maintenance-warning.aspx");
                }
            }

            if (!Request.Url.PathAndQuery.Contains("finaltest.aspx"))
            {
                m_oTranslationSubscriptions.AddIfNotExist(parse.stripLeadingCharacter(Page.Request.Url.AbsolutePath, System.Web.HttpContext.Current.Request.ApplicationPath));
                m_oTranslationSubscriptions.AddIfNotExist("/inc/master/courseplayer.master");
            }

            //Todo: Support alternative means of authentication?
            //string sValue = Util.Permissions.decryptStringWithHash(jlib.net.HTTP.getCookieValue(Request.Cookies["user-authentication"]));        
            //Session["user.id"] = null;
            //if (sValue != "") {            
            //    DateTime oExpiration = convert.cDate(parse.splitValue(sValue, " ", 1).Replace("+", " "));

            //    if (oExpiration > DateTime.Now) 
            //        Util.Permissions.setLoginData(oExpiration < DateTime.Now.AddHours(1), convert.cInt(parse.splitValue(sValue, " ", 0)), convert.cInt(parse.splitValue(sValue, " ", 3)), (Util.Permissions.UserType)convert.cInt(parse.splitValue(sValue, " ", 2)), DateTime.Now.AddHours(2), convert.cInt(parse.splitValue(sValue, " ", 4)));                            
            //}

            //if (!DisableLoginRedirect && convert.cInt(Session["user.id"]) == 0) Response.Redirect(LoginPage + "?action=timeout&referrer=" + HttpUtility.UrlEncode(Request.Url.AbsoluteUri));        
            if (!DisableLoginRedirect && ActiveUser.UserObject == null) Response.Redirect(LoginPage + "?action=timeout&referrer=" + HttpUtility.UrlEncode(Request.Url.AbsoluteUri));
            Page.InitComplete += new EventHandler(Page_InitComplete);

            if (ActiveUser.UserObject != null)
            {
                //Todo
                lTabReports.Visible = ActiveUser.IsLearningPortalSuperUser;
                if (CurrentOrg != null && CurrentOrg.ID > 0) pTrackingNorwegianDistributor.Visible = CurrentOrg.DistributorId == 1194;                    
                
            }

            Page.Unload += new EventHandler(Page_Unload1);
            base.Page_Init(sender, e);
            
            if (ActiveUser.UserObject != null)
            {
                lTabCatalog.Visible = ActiveUser.AllowEnroll;
                hBehind.Visible = ActiveUser.IsBehindAdmin;
                hCMS.Visible = Util.Cms.HasCMSAccess(ActiveUser);
                HelloBarScriptVisibility = ActiveUser.Organization != null && ActiveUser.Organization.IsArbitrary && !Request["frame"].Bln() && ActiveUser.Organization.DistributorId == 1194;

                lUserWelcomeMessage.Text = String.Format(jlib.helpers.translation.translate((Page as webpage).Language, "", "/index.aspx",WebPage, "heading"), m_oActiveUser.FirstName + " " + m_oActiveUser.LastName);
                if (ActiveUser.PortalSettings != null)
                {                    
                    if (ActiveUser.PortalSettings.AnalyticsJS != null) lTrackingScript.Text = ActiveUser.PortalSettings.AnalyticsJS;
                    if (ActiveUser.PortalSettings.PageTitlePrefix != null) Page.Title = ActiveUser.PortalSettings.PageTitlePrefix;
                    if (ActiveUser.PortalSettings.LearningPortalCSSUrl != null)
                    {
                        lDistributorCss.Attributes["href"] = Request.ApplicationPath + ActiveUser.PortalSettings.LearningPortalCSSUrl;
                        lDistributorCss.Visible = true;
                    }
                }
            }
            if (Page.Request.QueryString["action"] == "portalemail")
            {
                string toEmail = "kurssporsmal@metier.no";
                                
                string body = "Message: " + Request["data"] +
                    String.Format("\r\nDate received: {0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);

                if (ActiveUser.UserObject != null)
                {
                    toEmail = convert.cStr(ActiveUser.DistributorEmail, toEmail);
                    body += "\r\nEmail: " + ActiveUser.Email
                    + "\r\nUsername: " + ActiveUser.Username
                    + "\r\nName: " + ActiveUser.FirstName + " " + ActiveUser.LastName;
                }

                if (ActiveUser.PortalSettings != null && ActiveUser.PortalSettings.PortalEmailTo.IsNotNullOrEmpty())
                    toEmail = ActiveUser.PortalSettings.PortalEmailTo;

                Util.Email.sendEmail(convert.cStr(ActiveUser.UserObject != null ? ActiveUser.Email : "", "kurssporsmal@metier.no"), toEmail, "", "", "Question from course participant inside Learning Portal!", body, false, null);
                Response.End();
            }
        }
        void Page_Unload1(object sender, EventArgs e)
        {
            this.PageView.UserID = ActiveUser.ID;
            CommitPageRequestLogger();
        }


        public MyMetier clearWrapper()
        {
            return clearWrapper(false);
        }
        public MyMetier clearWrapper(bool keepHead)
        {
            this.Controls.Clear();
            this.Controls.Add(c1);
            if (keepHead) this.Controls.AddAt(0, pHeader);
            this.Controls.AddAt(0, convert.cLiteral("<!DOCTYPE html>"));
            return this;
        }
        public void Page_InitComplete(object sender, EventArgs e)
        {
            if (this.Page != null)
            {
                if (DisableWrapper || convert.cBool(Request.QueryString["embed"]))
                {
                    frmMain.Controls.Clear();
                    frmMain.Controls.Add(c1);
                    cBody.Attributes["class"] = "no-bg-image";
                }
            }
        }
        public bool HelloBarScriptVisibility
        {
            get
            {
                return pHelloBarScript.Visible;
            }
            set
            {
                pHelloBarScript.Visible = value;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (m_oTranslationSubscriptions.Count > 0) Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "translation", "$(document).data(\"__translation\"," + jlib.helpers.translation.getEntries(WebPage.Language, WebPage, m_oTranslationSubscriptions, WebPage.TranslationSiteId, WebPage.TranslationCompanyId).ToJSON() + ");", true);
            if (m_oActiveUser != null)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "userinfo", "var myMetierUserDetails={\"Language\":\"" + m_oActiveUser.UserLanguage + "\"};", true);
            }
            lTrackingScript.Visible = !System.Configuration.ConfigurationManager.AppSettings["site.dev"].Bln();
            lTabFiles.Visible = m_oActiveUser != null && m_oActiveUser.FileFolderIDs.Count > 0;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Page.RegisterHiddenField("url", Request.Url.PathAndQuery);
            //Page.RegisterHiddenField("session", Session.SessionID + " :: IsNew: " + Session.IsNewSession + " :: Count: " + Session.Count + " alert: " + Session["alert.once"]);

            if (!convert.cBool(System.Configuration.ConfigurationManager.AppSettings["site.dev"])) Page.ClientScript.RegisterStartupScript(this.GetType(), "js-debug", "try{window.onerror=fnHandleWindowError;}catch(e){}", true);

            label[] oTabs = new label[] { lTabOverview, lTabParticipants, lTabMerits, lTabCatalog, lTabReports, lTabFiles };
            if ((int)m_oSiteSection > -1) oTabs[(int)m_oSiteSection].CssClass = "active";

            if (CurrentOrg.CompetitionMode == 2) lTabParticipants.Visible = false;
            lSettings2.Text = jlib.helpers.translation.translate(WebPage.Language, "", "/inc/master/mymetier.master", WebPage, "user-profile");
            if (m_oActiveUser.ID > 0 && m_oActiveUser.NagUserForExamCompetence)
            {
                lSettings2Link.NavigateUrl += "#competency";
                iNagAlert.Visible = true;
            }
            hLogout.Text = jlib.helpers.translation.translate(WebPage.Language, "", "/inc/master/mymetier.master", WebPage, "logout");
            hTabOverview.Text = jlib.helpers.translation.translate(WebPage.Language, "", "/inc/master/mymetier.master", WebPage, "tab-overview");
            hTabParticipants.Text = jlib.helpers.translation.translate(WebPage.Language, "", "/inc/master/mymetier.master", WebPage, "tab-participants");
            hTabMerits.Text = jlib.helpers.translation.translate(WebPage.Language, "", "/inc/master/mymetier.master", WebPage, "tab-merits");
            hTabCatalog.Text = jlib.helpers.translation.translate(WebPage.Language, "", "/inc/master/mymetier.master", WebPage, "tab-catalog");
            hTabFiles.Text = jlib.helpers.translation.translate(WebPage.Language, "", "/inc/master/mymetier.master", WebPage, "tab-files");
            hTabReports.Text = jlib.helpers.translation.translate(WebPage.Language, "", "/inc/master/mymetier.master", WebPage, "tab-reports");

            hLinkOverview.Text = hTabOverview.Text;
            hCookiePolicy.Text = jlib.helpers.translation.translate(WebPage.Language, "", "/inc/master/mymetier.master", WebPage, "link-cookie-policy");
            hContactUs.InnerText = jlib.helpers.translation.translate(WebPage.Language, "", "/inc/master/mymetier.master", WebPage, "link-contact-us");
            if (ActiveUser.PortalSettings != null && ActiveUser.PortalSettings.FaviconUrl.IsNotNullOrEmpty())
            {
                lFavIcon.Href = Request.ApplicationPath + ActiveUser.PortalSettings.FaviconUrl;
                lFavIcon.Visible = true;
            }
        }
        protected override void Render(HtmlTextWriter writer)
        {
            if (Session["alert.once"].Str() != "" && !Request.Url.AbsolutePath.Contains("navigator_frame.aspx"))
            {
                string type = "ok";
                string message = Session["alert.once"].Str();
                if (message.StartsWith("type:"))
                {
                    type = parse.inner_substring(message, "type:", null, ";", null);
                    message = message.Substring(message.IndexOf(";") + 1);
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert-once", "(function($) {$(function() {$.infoBar(\"" + parse.replaceAll(message, "\"", "\\\"", "\n", "", "\r", "") + "\", { ID: \"alert-once\", type: $.infoBar.type." + type + "}); }); })(jQuery);", true);
                Session["alert.once"] = null;
            }
            base.Render(writer);            
        }
    }
}