﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using jlib.functions;
using jlib.components;
using System.Security.Cryptography;
using jlib.db;
using AttackData = Phoenix.LearningPortal.Data;
namespace Phoenix.LearningPortal.Inc.Master
{
    public partial class Courseplayer : masterpage
    {
        private string m_sLoginPage = "/login.aspx", m_sCacheFileName;
        private bool m_bDisableWrapper = false, m_bDisableLoginRedirect = false;
        private Util.Classes.user m_oActiveUser = null;
        private jlib.components.webpage m_oWebPage;
        private DataTable m_oPageRequestLogger = null;
        private DateTime oPageLoadTime = System.DateTime.Now;
        AttackData.Folder CourseObject, LessonObject, PageObject;

        //private AttackData.Folder m_oCourse;
        //public AttackData.Folder Course
        //{
        //    get
        //    {
        //        if (m_oCourse == null)
        //        {
        //            int courseId = convert.cInt(Request["CourseID"], Request["course_id"]);
        //            if (courseId > 0) m_oCourse = AttackData.Folder.LoadByPk(courseId);
        //            if (m_oCourse == null) m_oCourse = new AttackData.Folder();
        //        }
        //        return m_oCourse;
        //    }
        //}

        public string LoginPage
        {
            get
            {
                return m_sLoginPage;
            }
            set
            {
                m_sLoginPage = value;
            }
        }
        public bool DisableWrapper
        {
            get
            {
                return m_bDisableWrapper;
            }
            set
            {
                m_bDisableWrapper = value;
            }
        }
        public bool DisableLoginRedirect
        {
            get
            {
                return m_bDisableLoginRedirect;
            }
            set
            {
                m_bDisableLoginRedirect = value;
            }
        }
        public string CacheFileName
        {
            get
            {
                if (m_sCacheFileName == null)
                {
                    if (ConfigurationManager.AppSettings["page.cache.path"].IsNullOrEmpty()
                        || Request["__PDF"].Bln()
                        || !Request["preview-data"].IsNullOrEmpty()
                        )
                    {
                        m_sCacheFileName = "";
                    }
                    else m_sCacheFileName = ConfigurationManager.AppSettings["page.cache.path"].Str() + "\\" + System.IO.Path.GetFileNameWithoutExtension(Request.Url.PathAndQuery) + "-" + (Request["PageID"].Int() > 0 ? "P" : "L") + "-" + convert.cInt(Request["PageID"], Request["LessonID"]) + ".htm";
                }
                return m_sCacheFileName;
            }
        }
        public Util.Classes.user ActiveUser
        {
            get
            {
                if (m_oActiveUser == null) m_oActiveUser = Util.Classes.user.getUser(new Util.Phoenix.session().GetUserId());
                return m_oActiveUser;
            }
        }

        public Util.Classes.organization CurrentOrg
        {
            get
            {
                return ActiveUser.Organization;
            }
        }
        public webpage WebPage
        {
            get
            {
                if (m_oWebPage == null) m_oWebPage = (Page as jlib.components.webpage);
                return m_oWebPage;
            }
        }

        public override void Page_PreInit(object sender, EventArgs e)
        {
            WebPage.LanguageCompanyAndSiteNeeded += WebPage_LanguageCompanyAndSiteNeeded;

        }

        private void WebPage_LanguageCompanyAndSiteNeeded(object sender, out string sLanguage, out string sSiteId, out string sCompanyId)
        {
            sCompanyId = "";
            sSiteId = "";
            sLanguage = convert.cStr(CourseObject.SafeGet().AuxField2, "en");
            if (ActiveUser != null)
            {                
                sSiteId = ActiveUser.DistributorId.Str();
                if (ActiveUser.Organization != null)
                    sCompanyId = ActiveUser.Organization.MasterCompanyID.Str();                
            }
        }

        public void LookupCourseObjects(out AttackData.Folder _CourseObject, out AttackData.Folder _LessonObject, out AttackData.Folder _PageObject)
        {
            if (CourseObject == null) CourseObject = AttackData.Folder.LoadByPk(Request.QueryString["CourseID"].Int());
            if (Request.QueryString["LessonID"].Int() > 0) LessonObject = AttackData.Folder.LoadByPk(Request.QueryString["LessonID"].Int());
            if (Request.QueryString["PageID"].Int() > 0) PageObject = AttackData.Folder.LoadByPk(Request.QueryString["PageID"].Int());
            if (PageObject == null && LessonObject != null) PageObject = LessonObject.GetChildren<AttackData.Folder>().FirstOrDefault(x => { return x.Context_Value == "content"; });
            if (PageObject == null)
            {
                LessonObject = CourseObject.GetChildren<AttackData.Folder>().FirstOrDefault();
                PageObject = LessonObject.GetChildren<AttackData.Folder>().FirstOrDefault(x => { return x.Context_Value == "content"; });
            }
            if (PageObject != null && LessonObject == null)
            {
                if (CourseObject != null)
                {
                    LessonObject = PageObject.GetParentUntil<AttackData.Folder>(10, new Func<AttackData.Folder, bool>(item => CourseObject.GetChildren<AttackData.Folder>().FirstOrDefault(x => { return x.ObjectID == item.ObjectID; }) != null));
                }
                if (LessonObject == null) LessonObject = PageObject.GetParents<AttackData.Folder>().FirstOrDefault();
            }
            if (CourseObject == null && LessonObject != null)
            {
                if (LessonObject.Context_Value == "course") CourseObject = LessonObject;
                else CourseObject = LessonObject.GetParentUntil<AttackData.Folder>(10, new Func<AttackData.Folder, bool>(item => item.Context_Value == "course"));
            }
            _CourseObject = CourseObject;
            _LessonObject = LessonObject;
            _PageObject = PageObject;
        }

        public void Page_Init(object sender, EventArgs e)
        {
            string sValue = Util.Permissions.decryptStringWithHash(jlib.net.HTTP.getCookieValue(Request.Cookies["user-authentication"]));
            Session["user.id"] = null;
            if (sValue != "")
            {
                DateTime oExpiration = convert.cDate(parse.splitValue(sValue, " ", 1).Replace("+", " "));
                if (oExpiration > DateTime.Now)
                {
                    Util.Permissions.setLoginData(oExpiration < DateTime.Now.AddHours(1), convert.cInt(parse.splitValue(sValue, " ", 0)), convert.cInt(parse.splitValue(sValue, " ", 3)), (Util.Permissions.UserType)convert.cInt(parse.splitValue(sValue, " ", 2)), DateTime.Now.AddHours(2), convert.cInt(parse.splitValue(sValue, " ", 4)));
                    m_oActiveUser = null;
                }
            }
            if (System.Configuration.ConfigurationManager.AppSettings["mymetier.ips"].Contains(";" + Request.ServerVariables["REMOTE_ADDR"] + ";")) DisableLoginRedirect = true;

            if (!DisableLoginRedirect && convert.cInt(Session["user.id"]) == 0) Response.Redirect(LoginPage + "?action=timeout&referrer=" + HttpUtility.UrlEncode(Request.Url.AbsoluteUri));
            Page.InitComplete += new EventHandler(Page_InitComplete);

            //only initiate logger record if we intend to commit it
            if (ConfigurationManager.AppSettings["page.logging.mode"] != "none" && (ConfigurationManager.AppSettings["page.logging.mode"] == "full" || (!this.IsPostBack && !(WebPage.IP.StartsWith("127.")))))
            {
                if (Session["last_log_date"] == null || convert.cDate(Session["last_log_date"]).AddSeconds(1) < System.DateTime.Now)
                {
                    if (ConfigurationManager.AppSettings["page.logging.mode"] != "full") Session["last_log_date"] = System.DateTime.Now;
                    m_oPageRequestLogger = sqlbuilder.getNewRecord(new ado_helper("sql.dsn.cms"), "d_page_views", "ip", WebPage.IP, "referrer", WebPage.Referrer, "item_id", convert.cInt(Request.QueryString["id"]), "post_data", (ConfigurationManager.AppSettings["page.logging.mode"] == "full" ? convert.cStreamToString(Request.InputStream) : ""), "url", convert.cEllipsis(Request.Url.PathAndQuery, 4096, false), "session_id", 0, "is_new", Session.IsNewSession, "user_agent", WebPage.UserAgent);
                }
            }
            Page.Unload += new EventHandler(Page_Unload1);
            if (Page.Request["write-cache"].Bln() && System.IO.File.Exists(CacheFileName)) io.delete_file(CacheFileName);
            if (CacheFileName != ""
                && System.IO.File.Exists(CacheFileName)
                )
            {
                Response.Write(io.read_file(CacheFileName));
                Response.End();
            }
        }



        void Page_Unload1(object sender, EventArgs e)
        {
            if (m_oPageRequestLogger != null)
            {
                if (convert.cInt(m_oPageRequestLogger.Rows[0]["load_time"]) == 0) m_oPageRequestLogger.Rows[0]["load_time"] = (System.DateTime.Now.Subtract(oPageLoadTime).Seconds * 1000) + System.DateTime.Now.Subtract(oPageLoadTime).Milliseconds;
                m_oPageRequestLogger.Rows[0]["user_id"] = ActiveUser.ID;
                try
                {
                    m_oPageRequestLogger.Rows[0]["page_title"] = Page.Title;
                }
                catch (Exception) { }
                ado_helper.update(m_oPageRequestLogger);
            }
        }

        public Courseplayer clearWrapper()
        {
            this.Controls.Clear();
            this.Controls.Add(c1);
            return this;
        }
        public void Page_InitComplete(object sender, EventArgs e)
        {
            if (this.Page != null)
            {
                if (DisableWrapper || convert.cBool(Request.QueryString["embed"]))
                {
                    //frmMain.Controls.Clear();
                    //frmMain.Controls.Add(c1);
                    //cBody.Attributes["class"] = "no-bg-image";
                }
            }
            pScriptContainer.Visible = !convert.cBool(Request.QueryString["noscript"]);
        }
        private IEnumerable<AttackData.Folder> FilterBasedOnCompanyID(IEnumerable<AttackData.Folder> Filtered, bool OnlyDictionariesWithoutCoursecode)
        {
            return Filtered.Where(x => ((OnlyDictionariesWithoutCoursecode ? (!x.Name.Str().StartsWith("PM-") && !x.Name.Str().StartsWith("M-")) : true) && (!x.Name.Str().Contains("+") || x.Name.Str().EndsWith("+" + convert.cInt(CurrentOrg.ParentCompanyID, CurrentOrg.ID)) || x.Name.Str().Contains("+" + convert.cInt(CurrentOrg.ParentCompanyID, CurrentOrg.ID) + "+"))));
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Request.QueryString["editing"].Bln())
                cBody.Attributes["class"] = convert.cIfValue(cBody.Attributes["class"], "", " ", "") + "_editing";
            
            pTrackingScript.Visible = !System.Configuration.ConfigurationManager.AppSettings["site.dev"].Bln();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

        }
        protected override void Render(HtmlTextWriter writer)
        {
            bool IsCachable = CacheFileName != "";

            if (convert.cStr(Session["alert.once"]) != "")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert-once", "(function($) {$(function() {$.infoBar(\"" + parse.replaceAll(convert.cStr(Session["alert.once"]), "\"", "\\\"", "\n", "", "\r", "") + "\", { ID: \"alert-once\", type: $.infoBar.type.ok}); }); })(jQuery);", true);
                Session["alert.once"] = null;
                IsCachable = false;
            }
            base.Render(writer);

            //    if (IsCachable && ",splash,quiz,".IndexOf("," + System.IO.Path.GetFileNameWithoutExtension(Request.Url.PathAndQuery) + ",") == -1) io.write_file(CacheFileName, HTML);


            //io.write_file(Server.MapPath("/html-log.txt"), "==================================================\nHTML Started for URL: " + Request.Url.AbsoluteUri + " -- " + System.DateTime.Now + "\n" + HTML, true);

        }

    }
}