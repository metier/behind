﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using jlib.functions;
using jlib.components;
using System.Security.Cryptography;
using jlib.db;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Inc.Master { 
    public partial class Default : Util.Classes.masterpage
    {
        //private string m_sNavigation = "";
        //public string Navigation {
        //    get {
        //        return m_sNavigation;
        //    }
        //    set {
        //        m_sNavigation = value;
        //    }
        //}
        private string m_sLoginPage = System.Web.HttpContext.Current.Request.ApplicationPath + "/login.aspx";
        private bool m_bDisableWrapper = false;

        private Util.Classes.user m_oActiveUser = null;
        public bool LookupUserFromHttpContext()
        {
            if (m_oActiveUser == null && Util.Permissions.UserIDFromHttpContext > 0)
            {
                m_oActiveUser = Util.Classes.user.getUser(Util.Permissions.UserIDFromHttpContext);
                return true;
            }

            return false;
        }
        public Util.Classes.user ActiveUser {
            get {
                if (m_oActiveUser == null) m_oActiveUser = Util.Classes.user.getUser(Util.Permissions.UserID);
                return m_oActiveUser;
            }
        }

        public string LoginPage {
            get {
                return m_sLoginPage;
            }
            set {
                m_sLoginPage = value;
            }
        }
        public bool DisableWrapper {
            get {
                return m_bDisableWrapper;
            }
            set {
                m_bDisableWrapper = value;
            }
        }
        public bool DisableLoginRedirect {
            get {
                return System.Web.HttpContext.Current.Items["DisableLoginRedirect"].Bln();
            }
            set {
                System.Web.HttpContext.Current.Items["DisableLoginRedirect"] = value;
            }
        }
        public override void Page_PreInit(object sender, EventArgs e) {
            WebPage.LanguageNeeded += new webpage.OnLanguageNeeded(oWebPage_LanguageNeeded);
        }
        void oWebPage_LanguageNeeded(object sender, out string sLanguage) {
            sLanguage = "en";
            if (ActiveUser.ID > 0 && ActiveUser.UserLanguage == "da") sLanguage = "da";
        }

        public override void Page_Init(object sender, EventArgs e) {
            AttackData.Context.incrementRequestDate();
            string sValue = Util.Permissions.decryptStringWithHash(jlib.net.HTTP.getCookieValue(Request.Cookies["user-authentication"]));
            Session["user.id"] = null;
            if (sValue != "") {
                DateTime oExpiration = convert.cDate(parse.splitValue(sValue, " ", 1).Replace("+", " "));
                if (oExpiration > DateTime.Now) {
                    Util.Permissions.setLoginData(oExpiration < DateTime.Now.AddHours(1), convert.cInt(parse.splitValue(sValue, " ", 0)), convert.cInt(parse.splitValue(sValue, " ", 3)), (Util.Permissions.UserType)convert.cInt(parse.splitValue(sValue, " ", 2)), DateTime.Now.AddHours(2), convert.cInt(parse.splitValue(sValue, " ", 4)));
                    m_oActiveUser = null;
                }
            }

            if (!DisableLoginRedirect) {
                Util.Phoenix.session session = new Util.Phoenix.session();
                if (!ActiveUser.IsBehindAdmin && !Util.Cms.HasCMSAccess(ActiveUser) && !Request.Url.AbsolutePath.Contains("/exam/")) Response.Redirect(LoginPage + "?action=timeout&referrer=" + HttpUtility.UrlEncode(Request.Url.AbsoluteUri));
            }
            if (ActiveUser == null) {
                pMainNav.Visible = false;
            } else {
                //todo manage settings
                hToolsSettings.Visible = ActiveUser.IsBehindSuperAdmin;

                hEmailLog.Visible = (ActiveUser.IsBehindAdmin || ActiveUser.IsBehindSuperAdmin);
                hDiplomaGenerator.Visible = (ActiveUser.IsBehindAdmin || ActiveUser.IsBehindSuperAdmin);
                hTranslator.Visible = (ActiveUser.IsBehindAdmin || ActiveUser.IsBehindSuperAdmin);
                pAdvanced.Visible = ActiveUser.IsBehindSuperAdmin;

                pNavCMS.Visible = Util.Cms.HasCMSAccess(ActiveUser);
                pEvaluations.Visible = (ActiveUser.IsBehindAdmin || ActiveUser.IsBehindSuperAdmin);

                pNavTools.Visible = ActiveUser.IsBehindAdmin || ActiveUser.IsBehindSuperAdmin;
                pNavReporting.Visible = ActiveUser.IsBehindAdmin || ActiveUser.IsBehindSuperAdmin;
                Session["user.id"] = ActiveUser.ID;
            }
            Page.InitComplete += new EventHandler(Page_InitComplete);


            Page.Unload += new EventHandler(Page_Unload1);
            base.Page_Init(sender, e);

            hLogout.Visible = pMainNav.Visible;
        }
        void Page_Unload1(object sender, EventArgs e) {
            CommitPageRequestLogger();
        }

        public Default clearWrapper() {
            this.Controls.Clear();
            this.Controls.Add(c1);
            return this;
        }
        public void Page_InitComplete(object sender, EventArgs e) {
            if (this.Page != null) {
                if (DisableWrapper) {
                    frmMain.Controls.Clear();
                    frmMain.Controls.Add(c1);
                    cBody.Attributes["class"] = "no-bg-image";
                }
            }
        }
        protected void Page_PreRender(object sender, EventArgs e) {
            List<jlib.components.iControl> oControls = jlib.helpers.control.getDynamicControls(this.Page, null, parse.replaceAll(Page.GetType().Name, "_aspx", ""));
            if (oControls.Count > 0) {
                jlib.components.hyperlink oLink = (oControls[0] as jlib.components.hyperlink);
                oLink.CssClass = "current";
                //for (int x = 0; x < oLink.Parent.Controls.Count; x++) {
                //    if (oLink.Parent.Controls[x] as jlib.components.hyperlink != null) {
                //        (oLink.Parent.Controls[x] as jlib.components.hyperlink).CssClass = "current";
                //        break;
                //    }
                //}
            }
            //jlib.helpers.control.populateProperty(this.Page, "nav1." + parse.splitValue(Page.GetType().Name, "_", 0), "CssClass", "current");
            //jlib.helpers.control.populateProperty(this.Page, "nav2." + parse.replaceAll(Page.GetType().Name, "_aspx",""), "CssClass", "current");
            if (!convert.cBool(System.Configuration.ConfigurationManager.AppSettings["site.dev"])) Page.ClientScript.RegisterStartupScript(this.GetType(), "js-debug", "try{window.onerror=fnHandleWindowError;}catch(e){}", true);

        }
        protected override void Render(HtmlTextWriter writer) {
            if (convert.cStr(Session["alert.once"]) != "") {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert-once", "(function($) {$(function() {$.infoBar(\"" + parse.replaceAll(convert.cStr(Session["alert.once"]), "\"", "\\\"", "\n", "", "\r", "") + "\", { ID: \"alert-once\", type: $.infoBar.type.ok}); }); })(jQuery);", true);
                Session["alert.once"] = null;
            }
            base.Render(writer);
        }
    }

}