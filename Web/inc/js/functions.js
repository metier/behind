﻿var __doPostBack;
$(function() {    
    if (__doPostBack != null) {
        __doPostBack = function(t, a) {
            $("#__EVENTTARGET").val(t);
            $("#__EVENTARGUMENT").val(a);
            $("form").submit();
        }
    }
});

function AJAXControlAction(sID,oOptions, fnSuccessFunction){
    $.post(window.location.href.split("#")[0] + (window.location.href.indexOf("?") == -1 ? "?" : "&") + "__EVENTTARGET="+sID+"&__EVENTARGUMENT=AJAXLOADCONTROL",
    $(document.forms[0]).serialize()+(oOptions!=null?"&"+$.param(oOptions):""),
    function(oData, textStatus) {            
        if (textStatus == "success") {   
            if(fnSuccessFunction!=null)fnSuccessFunction();         
        }
        //setTimeout(function(){$("#hImportPopup").dialog("close");alert('done');},1000);
    }, "script");
}

function trim(s) {
    return ltrim(rtrim(s));
}

function ltrim(s) {
    s=cStr(s);
    while (s.substring(0, 1) == ' ') s = s.substring(1, s.length);
    return s;
}

function rtrim(s) {
    s=cStr(s);
    while (s.substring(s.length - 1, s.length) == ' ') s = s.substring(0, s.length - 1);
    return s;
}
function SafeGetProperty() {
    var obj = SafeGetProperty.arguments[0];
    for (var x = 1; x < SafeGetProperty.arguments.length; x++) {
        if (!obj) return;
        obj = obj[SafeGetProperty.arguments[x]];
    }    
    return obj;
}
function stripStr() {
    var A = [];
    $.each(stripStr.arguments, function () { A.push(this); });
    A[0] = stripLeadingStr(A);    
    return stripEndingStr(A);
}
function stripLeadingStr() {
    var a = stripLeadingStr.arguments[0];
    if ($.type(a) !== "array") a = stripLeadingStr.arguments;
    var s = cStr(a[0]);
    for (var x = 1; x < a.length; x++)
        if (a[x] && s.toLowerCase().indexOf(a[x].toLowerCase()) == 0) {
            s = s.substring(a[x].length);
            x = 1;
        }
    return s;
}
function stripEndingStr() {
    var a = stripEndingStr.arguments[0];
    if ($.type(a) !== "array") a = stripEndingStr.arguments;
    var s = cStr(a[0]);
    for (var x = 1; x < a.length; x++)
        if (a[x] && s.toLowerCase().lastIndexOf(a[x].toLowerCase()) == s.length-a[x].length) {
            s = s.substring(0, s.length - a[x].length);
            x = 1;
        }
    return s;
}

String.prototype.trim = function () { return this.replace(/^\s+|\s+$/g, "");}
String.prototype.ltrim = function () {return this.replace(/^\s+/, "");}
String.prototype.rtrim = function () {return this.replace(/\s+$/, "");}
String.prototype.lpad = function (p, l) { var s = this; while (s.length < l) s = p + s; return s;}
String.prototype.rpad = function (p, l) { var s = this; while (s.length < l) s = s + p; return s;}
String.prototype.replaceAt = function (i, c) { return this.substr(0, i) + c + this.substr(i + c.length); }
String.prototype.safeSplit = function (s, i) { var t = cStr(this).split(s); return cStr(t[i < 0 ? t.length + i : i]); }
String.prototype.format = function () {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined'
          ? args[number]
          : match
        ;
    });
};

function cStr() {
    for (var x = 0; x < cStr.arguments.length; x++)
        if (cStr.arguments[x]!=null&&("" + cStr.arguments[x]) != "") return ("" + cStr.arguments[x]);
            
    return "";
}
function cIfValue(oVal, oPrefix, oSuffix, oBlankValue) {
    if (cStr(oVal) == "") return cStr(oBlankValue);
    return cStr(oPrefix + oVal + oSuffix);
}
function formatCurrency(sValue) {

    var sDecimalSep = (((1.12222) + "").indexOf(".") > -1 ? "." : ",");

    sValue = replaceAll(replaceAll(replaceAll(cStr(sValue).toLowerCase(), "$", ""), "nok", ""), "kr", "");

    if (sValue.lastIndexOf(",") > sValue.lastIndexOf("."))
        sValue = replaceAll(replaceAll(sValue, ".", ""), ",", ".");

    if (isNaN(sValue)) sValue = "0";

    if (sValue.indexOf(".") == -1) sValue += ".";
    sValue += "00";

    sValue = sValue.split(".")[0] + sDecimalSep + sValue.split(".")[1].substring(0, 2);
    for (x = sValue.length - 6; x > 0; x = x - 3)
        sValue = sValue.substring(0, x) + "," + sValue.substring(x);

    return sValue;

}
var fnDateFormatter = function (d, f) {
    return f.replace(/{(.+?)(?::(.*?))?}/g, function (v, c, p) {
        for (v = d["get" + c]() + /h/.test(c) + "";
                v.length < p;
                v = 0 + v);
        return v
    })
};
function fnMetierDate(Date) {
    if (!Date) return;
    return fnDateFormatter(Date,"{FullYear}-{Month:2}-{Date:2}");
}
function fnMetierTime(Date) {
    if (!Date) return;
    return fnDateFormatter(Date, "{Hours:2}:{Minutes:2}:{Seconds:2}");
}
function fnMetierDateTime(Date) {    
    return fnMetierDate(Date) + " " + fnMetierTime(Date);
}
function replaceAll() {
    var s = cStr(replaceAll.arguments[0]);
    for (var x = 1; x < replaceAll.arguments.length; x = x + 2) {
        a = cStr(replaceAll.arguments[x]);
        b = cStr(replaceAll.arguments[x + 1]);        
        s = s.split(a).join(b);
    }
    return s;
}
function replaceAllCharCode() {
    var s = cStr(replaceAllCharCode.arguments[0]);
    for (var x = 1; x < replaceAllCharCode.arguments.length; x = x + 2) {
        a = cStr(replaceAllCharCode.arguments[x]);
        b = cStr(replaceAllCharCode.arguments[x + 1]);
        a = (isNaN(a) || a.indexOf("\n") == 0 || a.indexOf("\r") == 0 || a.indexOf("\t") == 0 || a.indexOf("\\") == 0 ? a : ascToChar(a));
        b = (isNaN(b) || b.indexOf("\n") == 0 || b.indexOf("\r") == 0 || b.indexOf("\t") == 0 || b.indexOf("\\") == 0 ? b : ascToChar(b));
        s = s.split(a).join(b);
    }
    return s;
}

function ascToChar(sIn) {
    sIn = "" + sIn;
    if (trim(sIn) == "") return sIn;
    var s = "";
    for (var x = 0; x < sIn.length; x += 2) {
        a = parseInt(sIn.substr(x, [2])) + 23;
        a = unescape('%' + a.toString(16));
        s += a;
    }
    return unescape(s);
}


function removeXChars(sData, sValidUrlCharacters) {
    var sValue = "";
    for (x = 0; x < sData.length; x++) {
        if (sValidUrlCharacters.indexOf(sData.charAt(x)) > -1)
            sValue += sData.charAt(x);
    }
    return sValue;
}

function cEllipsis(sData, iMaxLen, bAddEllipsis) {
    if (sData.length > iMaxLen)
        sData = sData.substring(0, iMaxLen - (bAddEllipsis ? 3 : 0)) + (bAddEllipsis ? "..." : "");

    return sData;
}

function getSafeUrl(sData, iMaxLen) {
    var sValidUrlCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-/";
    //sData = replaceAllCharCode(sData, "&", "og", " " , "_", "ë", "e", "Ë", "E", "é", "e", "É", "E", "è", "e", "È", "E", "ø", "o", "Ø", "O", "å", "a", "Å", "A", "æ", "e", "Æ", "E", "ö", "o", "Ö", "O", "ä", "a", "Ä", "A", "á", "a", "Á", "A", "ü", "u", "Ü", "U", "c", "c", "C", "C", "d", "d", "Ð", "D", "š", "s", "Š", "S", "ž", "z", "Ž", "Z" );
    sData = replaceAllCharCode(sData, "&", "og", " ", "_", 144643, "e", 144443, "E", 144634, "e", 144434, "E", 144633, "e", 144433, "E", 144733, "o", 144533, "O", 144630, "a", 144430, "A", 144631, "e", 144431, "E", 144731, "o", 144531, "O", 144629, "a", 144429, "A", 144626, "a", 144426, "A", 144744, "u", 144544, "U", 149425262545, "c", 149425262544, "C", 149425262626, "d", 144525, "D", 149425262943, "n", 149425262942, "N", 149425263126, "s", 149425263125, "S", 149425263132, "t", 149425263131, "T", 149425263246, "z", 149425263245, "Z");
    return (iMaxLen > 0 ? cEllipsis(removeXChars(sData, sValidUrlCharacters), iMaxLen, false) : removeXChars(sData, sValidUrlCharacters));
}

//get the width and height of the entire document
function getContentSize() {

    var xScroll, yScroll;

    if (window.innerHeight && window.scrollMaxY) {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    } else if (document.body.scrollHeight > document.body.offsetHeight) { // all but Explorer Mac
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    } else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }
    return new Array(xScroll, yScroll);
}

//How many pixels the top left corner of the displayed content is from the top left corner of the document
function getScrollSize() {

    var xScroll, yScroll;

    if (window.pageYOffset) {
        yScroll = window.pageYOffset;
        xScroll = window.pageXOffset;
    } else {
        yScroll = pageScrollY();
        xScroll = pageScrollX();
    }

    return new Array(xScroll, yScroll);
}

function getWindowSize() {

    var windowWidth, windowHeight;
    if (self.innerHeight) {	// all except Explorer
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }

    return new Array(windowWidth, windowHeight);
}

function hideElementsUnder(sElementTag, oElement, bHide) {
    var oOverlap = new Array();
    var oElements = document.getElementsByTagName(sElementTag);
    for (i = 0; i < oElements.length; i++) {

        var oObj = oElements[i];
        if (!oObj || !oObj.offsetParent)
            continue;

        var bFound = false;
        var oParentElement = oObj.parentElement;
        while (oParentElement != null) {
            if (oParentElement == oElement) {
                bFound = true;
                break;
            }
            oParentElement = oParentElement.parentElement;
        }

        if (!bFound && checkOverlap(oElement, oObj)) {
            if (bHide || bHide == undefined)
                oObj.style.display = "none";

            oOverlap[oOverlap.length] = oObj;
        }
    }
    return oOverlap;
}

function checkOverlap(el1, el2) {
    if (el1 == null || el2 == null) return false;

    var layerTop = Position.cumulativeOffset(el1)[1];
    var layerBottom = parseInt(layerTop + el1.clientHeight)
    var layerLeft = Position.cumulativeOffset(el1)[0];
    var layerRight = layerLeft + el1.clientWidth;

    var fieldTop = Position.cumulativeOffset(el2)[1];
    var fieldBottom = fieldTop + el2.clientHeight;
    var fieldLeft = Position.cumulativeOffset(el2)[0];
    var fieldRight = fieldLeft + el2.clientWidth;

    if ((fieldBottom > layerTop && fieldTop < layerBottom) && (fieldLeft < layerRight && fieldRight > layerLeft))
        return true;

    return false;
}

function showElementsByTag(sElementTag) {
    var oElements = document.getElementsByTagName(sElementTag);
    for (i = 0; i < oElements.length; i++) oElements[i].style.display = "";
}


function setSelectionRange(element, start, end) {

    if (end === undefined) end = start;

    // firefox
    if ("selectionStart" in element) {
        element.setSelectionRange(start, end);
        element.focus(); // to make behaviour consistent with IE
    }
    // ie win
    else if (document.selection) {
        var range = element.createTextRange();
        range.collapse(true);
        range.moveStart("character", start);
        range.moveEnd("character", end - start);
        range.select();
    }
}

function getSelectionRange(element) {

    var result = { start: -1, end: -1 };

    // firefox
    if ("selectionStart" in element)
        result = { start: element.selectionStart, end: element.selectionEnd };
    // ie win
    else if (document.selection) {
        // inputs only
        var range = document.selection.createRange();
        if (range.parentElement() == element) {
            var rangeS = range.duplicate();
            rangeS.moveEnd("textedit", 1);
            var rangeE = range.duplicate();
            rangeE.moveStart("textedit", -1);
            result = { start: element.value.length - rangeS.text.length, end: rangeE.text.length };
        }
    }

    return result;
}

function cDbl() {
    for (var x = 0; x < cDbl.arguments.length; x++) {
        var num=cDbl.arguments[x];
        if (num) {
            var num = cStr(num).replace(/\$|\,/g, '');
            if (!isNaN(num) && Number(num) != 0) return Number(num);                
        }
    }    
    return 0;
}

function cInt() {
    for (var x = 0; x < cInt.arguments.length; x++) {
        var n = cDbl(cInt.arguments[x]);
        if (n != 0) return Number((n+"").safeSplit(".",0).safeSplit(",",0));                    
    }
    return 0;
}

function cDecimal(num, iNumDecimals) {
    return Math.round(cDbl(num) * Math.pow(10, iNumDecimals)) / Math.pow(10, iNumDecimals);
}


function getThumbnailUrl(sUrl, iWidth, iHeight) {
    if (iHeight == null) iHeight = 0;
    return "/learningportal/inc/library/exec/thumbnail.aspx?u=" + sUrl + "&w=" + iWidth + "&h=" + iHeight + "&n=1&m=&p=1&c=1&q=0";
}

function fnSetComboValue(oSelect, sValue, iDefaultIndex, bSupressEvents) {
    sValue = cStr(sValue);
    iDefaultIndex = cInt(iDefaultIndex);
    var y = -1;
    for (var x = 0; x < oSelect.options.length; x++) {
        if (oSelect.options[x].value == sValue) {
            oSelect.selectedIndex = x;
            y = x;
            break;
        }
    }
    if (y == -1) y = iDefaultIndex;
    if (y > -1 && y < oSelect.options.length) {
        oSelect.options.selectedIndex = y;
        if (!bSupressEvents && oSelect.getAttribute("onchange") != null) oSelect.onchange();
    }
    return y;
}

function fnGetComboValue(oSelect) {
    if (oSelect.selectedIndex == -1) return "";
    return oSelect[oSelect.selectedIndex].value;
}

function fnComboValueIndex(oSelect, sValue) {
    for (var x = 0; x < oSelect.length; x++) if (oSelect[x].value.toLowerCase() == sValue.toLowerCase()) return x;
    return -1;
}

function fnComboTextIndex(oSelect, sText) {
    for (var x = 0; x < oSelect.length; x++) if (oSelect[x].text.toLowerCase() == sText.toLowerCase()) return x;
    return -1;
}

function fnFindIn2DArray(oArr, sNeedle, iDim) {
    for (var x = 0; x < oArr.length; x++)
        if (cStr(oArr[x][iDim]) == sNeedle) return x;
    return -1;
}

function fnUpdateStatusMessage(sMsg, sElement, iTimer) {
    if (sElement == null) sElement = 'cStatus';
    var o=$("#"+sElement);
    if (iTimer == null) iTimer = 8000;
    if (cStr(sMsg) == "") { o.css('visibility', 'hidden'); return; }
    o.html(sMsg).css('visibility', 'visible').css("left", ($(window).width() - o.width()) / 2 + $(window).scrollLeft() + "px");    
    if (cInt(o[0].TimerID) != 0) clearTimeout(o[0].TimerID);
    if (iTimer > 0) o[0].TimerID = setTimeout(function () { o.css('visibility', 'hidden'); }, iTimer);
}

function fnGetComboText(A) { if (A.selectedIndex == -1) { return "" } return A[A.selectedIndex].text };

//iButtons: 1=OK/Cancel    
function fnDialogShow(iButtons, bDrawInputBox, sTitle, sText, oCallback, iWidth, iHeight, oSourceElement) {
    if (cInt(iWidth) == 0) iWidth = 800;
    if (cInt(iHeight) == 0) iHeight = 500;
    
    var sHTML = sText;
    if($('#dialogWindow').length==0)$(document.forms[0]).append("<div id='dialogWindow' style='display:none'></div>");
    if (bDrawInputBox) sHTML += "<br><br><input type='text' style='width:250px'><br><br>";
         
    $('#dialogWindow').dialog({ modal: true, width:iWidth, height:iHeight, buttons: { "Cancel": function() { fnRaiseDialogButton(this,0); }, "Ok": function() { fnRaiseDialogButton(this,1); } }, 
    open: function(ev,ui){ },
    close: function (ev, ui) { fnRaiseDialogButton(this, -1); }                 
    }).dialog("open").dialog("option","title",sTitle).html( "<div style='padding:10px 0 0 10px;color:black'> "+sHTML+"</div>");

    $('#dialogWindow').data("callback",oCallback);
    $('#dialogWindow').data("sourceElement", oSourceElement);
}

function fnRaiseDialogButton(oDialog, iButton) {
    var oWindow = $(oDialog);
    var bClose = true;
    if (oWindow[0] && oWindow.data("callback")) {
        var oValues = new Array();
        var oInputs = oWindow.find("input[type='hidden'],input[type='text'],input[type='checkbox'],input[type='radio'],select,textarea");
        for (var x = 0; x < oInputs.length; x++) oValues.push(new Array(oInputs[x].getAttribute("name"), oInputs[x].value));
        if (oWindow.data("callback")(oWindow, iButton, oValues)) bClose = false;
    }    
    if (bClose) $(oDialog).dialog("destroy");
}

function fnEventElement(e) {
    if (e== null) return null;
    return e.srcElement||e.target;
}
function fnEventButton(e) {
    if (e == null) return null;
    if ((fnGetIEVersion() < 0 && e.button == 0) || (fnGetIEVersion()>0 && e.button == 1)) return "left";
    else if (e.button == 2) return "right";
}
function fnSplitIndex(s, n, i) {
    return cStr(cStr(s).split(n)[i]);
}
function getQuerystring(key, default_){
  if (default_==null) default_=""; 
  key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
  var qs = regex.exec(window.location.href);
  if(qs == null)
    return default_;
  else
    return qs[1];
}
function getHashstring(key, default_) {
    if (default_ == null) default_ = "";
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[#&]" + key + "=([^&]*)");
    var qs = regex.exec(window.location.hash);
    if (qs == null)
        return default_;
    else
        return qs[1];
}
function fnQueryStringStripKeys(sUrl, sKeys) {
    if (!fnQueryStringStripKeys.arguments[0]) fnQueryStringStripKeys.arguments[0] = window.location.href;
    var s = fnQueryStringStripKeys.arguments[0];
    var r = s.split("?")[0] + "?";
    s = cStr(s.split("?")[1]);
    
    if (s == "") return s;
    $.each(s.split("&"), function () { 
        var sKey=this.split("=")[0];
        if (!$.inArray(sKey, fnQueryStringStripKeys.arguments)) r += this + "&";
    });
    
    return stripEndingStr(r,"&");
}
function fnRemoveEmptyTags(selector) {
    $(selector).each(function () {
        var $this = $(this);
        if ($this.html().replace(/\s|&nbsp;/g, '').length == 0)
            $this.remove();
    });
    return selector;
}
function fnSendEmail(sTo, sFrom, sSubject, bHTML, sBody) {
    $.ajax({ "type": "POST", "url": "/learningportal/tools/sendmail.aspx?__AJAXCALL=1", "data": { "to": sTo, "from": sFrom, "subject": sSubject, "html": bHTML, "body": sBody} });
}
function fnHandleWindowError(sMsg, sUrl, iLineNumber) {    
    if (window.prevError != (sMsg + iLineNumber)) {
        var s = "Msg: " + sMsg + " / Url: " + sUrl + " / Line: " + iLineNumber + "\n\n" + navigator.userAgent + "\n\n----------\nStack:\n" + printStackTrace().join('\n\n');
        fnSendEmail("phoenix@metier.zendesk.com", "debug@secure.bids.no", "JS Error: " + window.location.href, false, s);
        window.prevError = sMsg + iLineNumber;
    }
    return false;
}
//stacktrace
eval(function (p, a, c, k, e, d) { e = function (c) { return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36)) }; if (!''.replace(/^/, String)) { while (c--) { d[e(c)] = k[c] || e(c) } k = [function (e) { return d[e] } ]; e = function () { return '\\w+' }; c = 1 }; while (c--) { if (k[c]) { p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]) } } return p } ('6 B(o){7 v=(o&&o.e)?o.e:1t;7 O=(o&&o.O)?o.O:1v;7 p=r B.G();7 16=p.1d(v);5(O)?p.1l(16):16}B.G=6(){};B.G.X={1d:6(v){7 z=8.y||8.z();9(z===\'M\'){5 8.M(11.1E)}u{v=v||(6(){E{(0)()}F(e){5 e}})();5 8[z](v)}},z:6(){E{(0)()}F(e){9(e.11){5(8.y=\'19\')}u 9(e.a){5(8.y=\'1s\')}u 9(1q.P&&!(\'1F\'Q e)){5(8.y=\'P\')}}5(8.y=\'M\')},19:6(e){5 e.a.c(/^.*?\\n/,\'\').c(/^.*?\\n/,\'\').c(/^.*?\\n/,\'\').c(/^[^\\(]+?[\\n$]/N,\'\').c(/^\\s+1C\\s+/N,\'\').c(/^1x.<k>\\s*\\(/N,\'{k}()@\').H("\\n")},1s:6(e){5 e.a.c(/^.*?\\n/,\'\').c(/(?:\\n@:0)?\\s+$/m,\'\').c(/^\\(/N,\'{k}(\').H("\\n")},P:6(e){7 g=e.1z.H("\\n"),A=\'{k}\',1i=/1G\\s+(\\d+).*?1A\\s+(1y\\S+)(?:.*?Q\\s+6\\s+(\\S+))?/i,i,j,U;D(i=4,j=0,U=g.q;i<U;i+=2){9(1i.1h(g[i])){g[j++]=(l.$3?l.$3+\'()@\'+l.$2+l.$1:A+\'()@\'+l.$2+\':\'+l.$1)+\' -- \'+g[i+1].c(/^\\s+/,\'\')}}g.1B(j,g.q-j);5 g},M:6(h){7 A="{k}",17=/6\\s*([\\w\\-$]+)?\\s*\\(/i,a=[],j=0,V,f;7 18=10;1w(h&&a.q<18){V=17.1h(h.1f())?l.$1||A:A;f=1D.X.1H.1u(h[\'11\']);a[j++]=V+\'(\'+B.G.X.1g(f)+\')\';9(h===h.1e&&1q.P){1Y}h=h.1e}5 a},1g:6(f){D(7 i=0;i<f.q;++i){7 C=f[i];9(R C==\'1b\'){f[i]=\'#1b\'}u 9(R C==\'6\'){f[i]=\'#6\'}u 9(R C==\'1X\'){f[i]=\'"\'+C+\'"\'}}5 f.1W(\',\')},I:{},1j:6(b){7 x=8.Y();9(!x){5}x.1V(\'1Z\',b,20);x.24("23-22","K/1.0");x.21(\'\');5 x.1U},Y:6(){7 T,J=[6(){5 r 1I()},6(){5 r W("1T.K")},6(){5 r W("1K.K")},6(){5 r W("1J.K")}];D(7 i=0;i<J.q;i++){E{T=J[i]();8.Y=J[i];5 T}F(e){}}},13:6(b){9(!(b Q 8.I)){8.I[b]=8.1j(b).H("\\n")}5 8.I[b]},1l:6(a){D(7 i=0;i<a.q;++i){7 1m=/{k}\\(.*\\)@(\\w+:\\/\\/([-\\w\\.]+)+(:\\d+)?[^:]+):(\\d+):?(\\d+)?/;7 12=a[i],m=1m.14(12);9(m){7 Z=m[1],15=m[4];9(Z&&15){7 1n=8.1o(Z,15);a[i]=12.c(\'{k}\',1n)}}}5 a},1o:6(b,L){E{5 8.1a(L,8.13(b))}F(e){5\'13 1P 25 b: \'+b+\', 1Q: \'+e.1f()}},1a:6(L,1c){7 1k=/6 ([^(]*)\\(([^)]*)\\)/;7 1r=/[\'"]?([0-1S-1O-1M]+)[\'"]?\\s*[:=]\\s*(6|1L|r 1R)/;7 t="",1p=10;D(7 i=0;i<1p;++i){t=1c[L-i]+t;9(t!==1N){7 m=1r.14(t);9(m){5 m[1]}u{m=1k.14(t)}9(m&&m[1]){5 m[1]}}}5"(?)"}};', 62, 130, '|||||return|function|var|this|if|stack|url|replace|||args|lines|curr|||anonymous|RegExp|||options||length|new||line|else|ex||req|_mode|mode|ANON|printStackTrace|argument|for|try|catch|implementation|split|sourceCache|XMLHttpFactories|XMLHTTP|lineNo|other|gm|guess|opera|in|typeof||xmlhttp|len|fn|ActiveXObject|prototype|createXMLHTTPObject|file||arguments|frame|getSource|exec|lineno|result|fnRE|maxStackSize|chrome|guessFunctionNameFromLines|object|source|run|caller|toString|stringifyArguments|test|lineRE|ajax|reFunctionArgNames|guessFunctions|reStack|functionName|guessFunctionName|maxLines|window|reGuessFunction|firefox|null|call|true|while|Object|http|message|script|splice|at|Array|callee|stacktrace|Line|slice|XMLHttpRequest|Microsoft|Msxml3|eval|z_|undefined|Za|failed|exception|Function|9A|Msxml2|responseText|open|join|string|break|GET|false|send|Agent|User|setRequestHeader|with'.split('|'), 0, {}))


Array.prototype.remove = function (from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    this.push.apply(this, rest);
    return this;
};

Array.prototype.removeByValue = function (sKey, oValue) {
    for (var x = 0; x < this.length; x++) {
        if ((!sKey && this[x] == oValue) || (sKey && this[x] && this[x][sKey] == oValue)) {
            this.splice(x, 1);
            x--;
        }
    }
    return this;
};
Array.prototype.safeGetValue = function (i) {
    if (this == null || i < 0 || i >= this.length) return [];
    return this[i];
};
Array.prototype.safeFindValue = function (sNeedle, iDim) {
    for (var x = 0; x < this.length; x++)
        if (cStr(this[x][iDim]) == sNeedle) return this[x];
    return [];
};
/* JQuery hotkeys */
(function (jQuery) {

    jQuery.hotkeys = {
        version: "0.8",

        specialKeys: {
            8: "backspace", 9: "tab", 13: "return", 16: "shift", 17: "ctrl", 18: "alt", 19: "pause",
            20: "capslock", 27: "esc", 32: "space", 33: "pageup", 34: "pagedown", 35: "end", 36: "home",
            37: "left", 38: "up", 39: "right", 40: "down", 45: "insert", 46: "del",
            96: "0", 97: "1", 98: "2", 99: "3", 100: "4", 101: "5", 102: "6", 103: "7",
            104: "8", 105: "9", 106: "*", 107: "+", 109: "-", 110: ".", 111: "/",
            112: "f1", 113: "f2", 114: "f3", 115: "f4", 116: "f5", 117: "f6", 118: "f7", 119: "f8",
            120: "f9", 121: "f10", 122: "f11", 123: "f12", 144: "numlock", 145: "scroll", 191: "/", 224: "meta"
        },

        shiftNums: {
            "`": "~", "1": "!", "2": "@", "3": "#", "4": "$", "5": "%", "6": "^", "7": "&",
            "8": "*", "9": "(", "0": ")", "-": "_", "=": "+", ";": ": ", "'": "\"", ",": "<",
            ".": ">", "/": "?", "\\": "|"
        }
    };

    function keyHandler(handleObj) {
        // Only care when a possible input has been specified
        if (typeof handleObj.data !== "string") {
            return;
        }

        var origHandler = handleObj.handler,
			keys = handleObj.data.toLowerCase().split(" ");

        handleObj.handler = function (event) {
            // Don't fire in text-accepting inputs that we didn't directly bind to
            if (this !== event.target && (/textarea|select/i.test(event.target.nodeName) ||
				 event.target.type === "text")) {
                return;
            }

            // Keypress represents characters, not special keys
            var special = event.type !== "keypress" && jQuery.hotkeys.specialKeys[event.which],
				character = String.fromCharCode(event.which).toLowerCase(),
				key, modif = "", possible = {};

            // check combinations (alt|ctrl|shift+anything)
            if (event.altKey && special !== "alt") {
                modif += "alt+";
            }

            if (event.ctrlKey && special !== "ctrl") {
                modif += "ctrl+";
            }

            // TODO: Need to make sure this works consistently across platforms
            if (event.metaKey && !event.ctrlKey && special !== "meta") {
                modif += "meta+";
            }

            if (event.shiftKey && special !== "shift") {
                modif += "shift+";
            }

            if (special) {
                possible[modif + special] = true;

            } else {
                possible[modif + character] = true;
                possible[modif + jQuery.hotkeys.shiftNums[character]] = true;

                // "$" can be triggered as "Shift+4" or "Shift+$" or just "$"
                if (modif === "shift+") {
                    possible[jQuery.hotkeys.shiftNums[character]] = true;
                }
            }

            for (var i = 0, l = keys.length; i < l; i++) {
                if (possible[keys[i]]) {
                    return origHandler.apply(this, arguments);
                }
            }
        };
    }

    jQuery.each(["keydown", "keyup", "keypress"], function () {
        jQuery.event.special[this] = { add: keyHandler };
    });

})(jQuery);
/* INFOBAR ** http://www.jeroenvanwarmerdam.nl/content/resources/javascript/jquery/infobar/infobar.aspx */
; (function ($, doc, win, undefined) {
    $.infoBar = function (txt, options) {
        var text = txt || "&nbsp;",
			opt = $.extend({}, $.infoBar.defaults, options),
			random = parseInt(Math.random() * 10000),
			table = $("<table/>").attr("cellspacing", 0).attr("cellpadding", 0);

        if (!opt.ID) {
            opt.ID = "infobar_R" + random;
        } else {
            opt.ID = "infobar_" + opt.ID;
            if (opt.cookie && !!$.cookie) {
                if ($.cookie(opt.ID) != null) {
                    opt.hide = $.cookie(opt.ID) == "true" ? true : false;
                } else {
                    $.cookie(opt.ID, opt.hide);
                }
            }
        }
        // add infobar;
        var root = $('<div/>').addClass("infobar").attr("id", opt.ID).toggle(!opt.hide).prependTo($(doc.body));

        // add bar;
        $('<div/>').hide().addClass("infobar-bar").addClass(opt.type).append(
			$('<div/>').html(text).attr("title", "Click for more options...").hover(function () {
			    $(this).addClass("hover");
			}, function () {
			    $(this).removeClass("hover");
			}).click(function (e) {
			    var V = viewport(),
					W = $("#" + opt.ID + " .infobar-menu").width(),
					X = e.pageX - V.x,
					Y = e.pageY - V.y;
			    $("#" + opt.ID + " .infobar-menu").css("top", Y).css("left", (X <= V.w - W ? X : Math.min(X - W, X))).show();
			    V = W = X = Y = null;  // clean up;
			}).append(
				$("<input/>").attr("type", "button").attr("value", "x").attr("title", "Close InfoBar").click(function () {
				    hider();
				    return false;
				})
			)
		).appendTo(root).slideDown();

        // add menu & items;
        opt.menu.push(null, { txt: "Close" });
        $.each(opt.menu, function (i, val) {
            if (!!val) {
                var txt = !!val.txt ? val.txt.toString() : "",
					fn = !!val.fn ? val.fn : function () { };
                table.append(
					$("<tr/>").append(  // image;
						$("<th/>").html(!!val.icon ? '<img src="' + val.icon.toString() + '" title="' + txt + '" />' : "&nbsp;")
					).append(  // verticale divider;
						$("<td/>").addClass("dividerH").html("&nbsp;")
					).append(  // text;
						$("<td/>").text(txt)
					).click(function () {  // extra function;
					    if (typeof (fn) == "string" ? (function () { win.open(fn); return true; })() : (fn() == false ? false : true)) {
					        hider();
					    }
					})
				);
            } else {  // horizontale divider;
                table.append(
					$("<tr/>").addClass("dividerV").append(
						$("<th/>").html("&nbsp;")
					).append(
						$("<td/>").addClass("dividerH").html("<div><!-- no content fix for Chrome & empty div fix for IE --></div>")
					).append(
						$("<td/>").html("<div><!-- no content fix for Chrome & empty div fix for IE --></div>")
					)
				);
            }
        });

        // add menu;
        $("<div/>").hide().addClass("infobar-menu").append(table).appendTo(root).find("tr:not(.dividerV)").hover(function () {
            $(this).addClass("hover");
        }, function () {
            $(this).removeClass("hover");
        });

        function hider() {
            $("#" + opt.ID + " .infobar-bar").fadeOut("slow");
            $("#" + opt.ID + " .infobar-menu").hide("fast");
            if (opt.cookie && !!$.cookie && !opt.ID.match("infobar_R")) {
                $.cookie(opt.ID, !opt.hide);
            }
        }
        
        function viewport() {
            var _win = $(win);
            return {
                x: _win.scrollLeft(),
                y: _win.scrollTop(),
                w: _win.width(),
                h: _win.height()
            };
        };

        $("body").click(function (e) {
            if (!$(e.target).is("#" + opt.ID + " *")) {
                $("#" + opt.ID + " .infobar-menu").hide("fast");
            }
        });

        text = random = table = null;
        return root;
    };

    // public enum;
    $.infoBar.type = {
        exclamation: "exclamation",
        error: "error",
        help: "help",
        ok: "ok",
        warning: "warning"
    };

    // public defaults;
    $.infoBar.defaults = {
        ID: null,
        type: $.infoBar.type.warning,
        menu: [],
        hide: false,
        cookie: false,
        zOrder: 1
    };
})(jQuery, document, window);   // plugin code end;

function fnSendData(sUrl, sData, fnSuccess, oOptions) {
    var s = $("form").attr("action") || window.location.href;
    var oRequest = { "Attempts": 1 };
    var oFRequest = function () {
        var o = { "type": "POST", "url": s.split("#")[0] + (s.indexOf("?") == -1 ? "?" : "&") + sUrl, "data": sData, "success": oFSuccess, "error": oFError, "dataType": "script" };
        if(oOptions)$.extend(o,oOptions);
        var oAjax = $.ajax(o); }
        var oFSuccess = function (oData, textStatus) {            
            if (oData && oData.redirect) window.location.href = oData.redirect;
            oRequest.Status = textStatus;
            if (fnSuccess != null) fnSuccess();
        };
    var oFError = function (oData, textStatus) {
        oRequest.Attempts++;
        oRequest.Status = textStatus;
        if (oRequest.Attempts < 5) {
            oFRequest();
        } else {
            alert("An unknown error occured while attempting to send your request to the server.\nI've attempted the operation five times unsuccessfully. Our technicians have been notified.");
        }
    };
    oFRequest();
    return oRequest;
}

function fnGridAttach(oGrid) {
    //new fnScanListener();
    $(document).bind("scan", function (o, s) {
        if (s == "END") { oGrid.MetaData.Page = oGrid.MetaData.Pages - 1; oGrid.addRequest(oGrid.buildRequest("refresh")); }
        else if (s == "HOME") { oGrid.MetaData.Page = 0; oGrid.addRequest(oGrid.buildRequest("refresh")); }
        else if (s == "PAGEUP") { $('.grid_prev_page:eq(0)').trigger("click"); }
        else if (s == "PAGEDOWN") { $('.grid_next_page:eq(0)').trigger("click"); }
    });
    $('.page_number').change(function (event) { oGrid.MetaData.Page = cInt(this.value) - 1; oGrid.addRequest(oGrid.buildRequest("refresh")); });
    $('.page_size').change(function (event) { oGrid.Views[0].PageSize = cInt(fnEventElement(event).options[fnEventElement(event).selectedIndex].value); oGrid.addRequest(oGrid.buildRequest("refresh")); });
    $('.grid_prev_page').click(function (event) { if (oGrid.MetaData.Page > 0) { oGrid.MetaData.Page--; oGrid.addRequest(oGrid.buildRequest("refresh")); } });
    $('.grid_next_page').click(function (event) { if (oGrid.MetaData.Page < oGrid.MetaData.Pages - 1) { oGrid.MetaData.Page++; oGrid.addRequest(oGrid.buildRequest("refresh")); } });
    $(document).bind("keydown", "pageup", function () { $('.grid_prev_page:eq(0)').trigger("click"); });
    $(document).bind("keydown", "pagedown", function () { $('.grid_next_page:eq(0)').trigger("click"); });
    $(document).bind("keydown", "home", function () { oGrid.MetaData.Page = 0; oGrid.addRequest(oGrid.buildRequest("refresh")); });
    $(document).bind("keydown", "end", function () { oGrid.MetaData.Page = oGrid.MetaData.Pages - 1; oGrid.addRequest(oGrid.buildRequest("refresh")); });
    oGrid.registerEventListener("GridCallbackMetaData", function (oEvent) {
        $('.page_number').val(oEvent.Grid.MetaData.Page + 1);
        $('.page_status').html(oEvent.Grid.MetaData.Pages + " page" + (oEvent.Grid.MetaData.Pages == 1 ? "" : "s") + " (" + oEvent.Grid.MetaData.NumRows + " rows)");
        $('.page_size').val(oEvent.Grid.Views[0].PageSize);

        $('.grid_prev_page').attr("src", "../inc/images/icons/pager_arrow_left" + (oEvent.Grid.MetaData.Page == 0 ? "_off" : "") + ".gif");
        $('.grid_prev_page').css("cursor", (oEvent.Grid.MetaData.Page == 0 ? "default" : "pointer"));
        $('.grid_next_page').attr("src", "../inc/images/icons/pager_arrow_right" + (oEvent.Grid.MetaData.Page == oEvent.Grid.MetaData.Pages - 1 ? "_off" : "") + ".gif");
        $('.grid_next_page').css("cursor", (oEvent.Grid.MetaData.Page == oEvent.Grid.MetaData.Pages - 1 ? "default" : "pointer"));

        if (oEvent.Grid.MetaData.StatusMessage != "") fnUpdateStatusMessage(replaceAll(oEvent.Grid.MetaData.StatusMessage, "\n", "<br>"));
    });

    oGrid.registerEventListener("GridAjaxStart", function (oRequest) {
        fnUpdateStatusMessage("<img src='../inc/images/icons/ajax-loader.gif'/>&nbsp;Refreshing data", null, 0);
    });

    oGrid.registerEventListener("GridAjaxComplete", function (oRequest) {
        if (oRequest.Data.status < 200 && oRequest.Data.status >= 300) {
            fnUpdateStatusMessage("<img src='../inc/images/icons/exclamation.png'/>&nbsp;Server returned error:<br><span style='color:red'>" + oRequest.Data.responseText.split("\n")[0] + "</span>");
            alert("Error: " + oRequest.Data.status + "\n" + oRequest.Data.responseText);
        } else {
            fnUpdateStatusMessage(cStr(oRequest.Grid.MetaData.StatusMessage) != "" ? oRequest.Grid.MetaData.StatusMessage : "<img src='../inc/images/icons/accept.gif'/>&nbsp;Refresh complete");
        }
    });

    oGrid.registerEventListener("RowSelectionChange", function (oEvent) {
        $('.selection-count').html("<strong>" + oEvent.Grid.PersistedData.SelectionCount + "</strong> item" + (oEvent.Grid.PersistedData.SelectionCount == 1 ? "" : "s") + " selected");
    });

}
$.fn.setCursorPosition = function (pos) {
    this.each(function (index, elem) {
        if (elem.setSelectionRange) {
            elem.setSelectionRange(pos, pos);
        } else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    });
    return this;
};

function fnGetIEVersion() {
    if ((navigator.appName == 'Netscape') && (new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})").exec(navigator.userAgent))) return parseFloat(RegExp.$1);            
    var r = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    return (r.exec(navigator.userAgent) != null ? parseFloat(RegExp.$1) : -1);    
}
function fnGetFFVersion() {    
    var r = new RegExp("Firefox/([0-9]{1,}[\.0-9]{0,})");
    return (r.exec(navigator.userAgent) != null ? parseFloat(RegExp.$1) : -1);
}

function fnEnableJSEffects() {
    if (fnGetIEVersion()>0) {
        if (fnGetIEVersion() < 9) {

            $(".nine-rounded, .nine-rounded-dark").each(function (i, o) {
                $(o).append("<div class='lt' />").append("<div class='rt' />").append("<div class='lb' />").append("<div class='rb' />");
            });
            //$(".supertable td.first, .largelessonslist td.first, .smalllessonslist td.first, .achievementslist td.first").each(function (i, o) {
            $(".left-rounded").each(function (i, o) {
                $(o).append("<div id='rounded-lt' />").append("<div id='rounded-lb' />");
            });
            $(".right-rounded").each(function (i, o) {
                $(o).append("<div id='rounded-rt' />").append("<div id='rounded-rb' />");
            });
            //, .actionbutton input[type=submit]
            $(".actionbutton a").each(function (i, o) {
                $(o).parent().append("<table id='ie-blue-button'><tr><td class='left'></td><td class='middle'></td><td class='right'></td></tr></table>").find("td:eq(1)").append(o);
                $(o).parents(".actionbutton").removeClass("actionbutton").addClass("ie-actionbutton");
                //$(o).wrap("<span style='position:relative;padding:0;margin:0' " + (rv >= 8 ? " class='ie8'" : "") + "/>").parent().prepend("<div class='lt' />").prepend("<div class='rt' />").prepend("<div class='lb' />").prepend("<div class='rb' />").removeClass("actionbutton").parent().removeClass("actionbutton");
                //prepend("<div style='position:absolute;left:0;top:-17px;width:5px;height:6px;background: url(gfx/button-blue-lt.png) top left no-repeat;z-index:1' />").prepend("<div style='position:absolute;left:0;top:2px;width:5px;height:6px;background: url(gfx/button-blue-lb.png) bottom left no-repeat;z-index:1' />").prepend("<div style='position:absolute;right:0;top:-17px;width:5px;height:6px;background: url(gfx/button-blue-rt.png) top left no-repeat;z-index:1' />").prepend("<div style='position:absolute;right:0;top:2px;width:5px;height:6px;background: url(gfx/button-blue-rb.png) bottom left no-repeat;z-index:1' />");
            });
            $(".progressbar").each(function (i, o) {
                $(o).append("<div class='lt' />").append("<div class='rt' />");
            });

            $("#wrapper-globalmenu li.active a").each(function (i, o) {
                $(o).parent().removeClass("active").append("<table id='ie-nav-active'><tr><td class='left'></td><td class='middle'></td><td class='right'></td></tr></table>").find("td:eq(1)").append(o);
            });

        }
    } else {
        if (/firefox/.test(navigator.userAgent.toLowerCase())) {
            $(".miniprogrambox td.nine-rounded, .miniprogrambox td.empty").css("display", "block");
            $(".miniprogrambox h2").each(function (i, o) { var h = $(o).height() - 40 + 255; if ($(o).parents("td").height() < h) { $(o).parents("tr:eq(0)").find("td").each(function (j, p) { $(p).height(h); }); }; });
        }
        $(".miniprogrambox td .main").css("padding-top", "1px");
    }
    $(document.body).on("mouseenter", ".highlight, #ie-blue-button, #ie-nav-active", function () { if ($(this).closest(".disabled").length>0) return; $(this).addClass("hover"); }).on("mouseleave", ".highlight, #ie-blue-button, #ie-nav-active", function () { $(this).removeClass("hover"); });
}

function fnIsTouchDevice() {
    return (/iPhone|iPod|iPad|Android|Windows Phone/.test(navigator.userAgent));    
}    

(function ($) {
    $.cookie = function (key, value, options) {

        // key and at least value given, set cookie...
        if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {
            options = $.extend({}, options);

            if (value === null || value === undefined) {
                options.expires = -1;
            }

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = String(value);

            return (document.cookie = [
                encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path ? '; path=' + options.path : '',
                options.domain ? '; domain=' + options.domain : '',
                options.secure ? '; secure' : ''
            ].join(''));
        }

        // key and possibly options given, get cookie...
        options = value || {};
        var decode = options.raw ? function (s) { return s; } : decodeURIComponent;

        var pairs = document.cookie.split('; ');
        for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
            if (decode(pair[0]) === key) return decode(pair[1] || ''); // IE saves cookies with empty string as "c; ", e.g. without "=" as opposed to EOMB, thus pair[1] may be undefined
        }
        return null;
    };
})(jQuery);

function clsScorm2004Local(url, RcoId) {
    this.RcoId = RcoId;
    this._UseLocalStorage = typeof (Storage) !== "undefined";
    this.Initialize = function (sVal) { return "true"; }
    this.Terminate = function (sVal) { return "true"; }
    this.Commit = function (sVal) { return "true"; }
    this.GetDiagnostic = function (iCode) { return "No error"; }
    this.GetErrorString = function (iCode) { return "No error"; }
    this.GetLastError = function () { return 0; };
    this.SetValue = function (sKey, sValue) {
        if(this._UseLocalStorage)
            localStorage.setItem(this.RcoId + "-" + sKey, sValue);
        else 
            $.cookie(this.RcoId + "-" + sKey, sValue, { expires: 365 });
        return "true";
    }
    this.GetValue = function (sKey) {
        return (this._UseLocalStorage ? localStorage.getItem(this.RcoId + "-" + sKey) : $.cookie(this.RcoId + "-" + sKey));
    }
    this.SetAutoCommit = function (bValue) { return true; };
    this.GetAutoCommit = function () { return true; };
}

function clsScorm2004API(url, RcoId) {
    this._url = url;
    this._finished = false;
    this._intialized = false;
    this._autoCommit = true;
    this._errorNumber = 0;
    this._errorText = "";
    this._haveState = false;
    this._elements = null;
    this._dirtyElements = null;
    this._commLog = "";
    this.Initialize = function (sVal) {
        this._SetError(0, "");
        if (this._intialized) {
            this._SetError(101, "Initialize has already been called.");
            return "false";
        }
        if (this._finished) {
            this._SetError(104, "Finish has already been called.");
            return "false";
        }
        this._intialized = true;
        return "true";
    };
    this.Terminate = function (sVal) {
        this._SetError(0, "");
        if (!this._CheckOpen() || this.Commit("") == "false") return "false";        
        this._SendRequest("PUT","/terminate");
        this._finished = true;
        if (this.GetLastError() != 0) return "false";
        return "true";
    };
    this.Commit = function (sVal) {//OK                
        this._SetError(0, "");
        if (!this._CheckOpen()) return "false";
        if (this._dirtyElements == null) return "true";
        for (var sKey in this._dirtyElements){
            if (sKey == "cmi.suspend_data") this._SendRequest("PUT", "/suspenddata", this._dirtyElements[sKey]);
            else if(sKey=="cmi.completion_status") this._SendRequest("PUT","/status/"+this._dirtyElements[sKey]);
            else if(sKey=="cmi.score.raw") this._SendRequest("PUT","/score/"+this._dirtyElements[sKey]);            
        }        
        if (this.GetLastError() != 0) return "false";
        this._dirtyElements = null;
        return "true";
    };
    this.GetDiagnostic = function (iCode) { return this.GetErrorString(iCode) + (this._errorText == "" ? "" : "\n" + this._errorText); }; //OK

    this.GetErrorString = function (iCode) { //OK
        if (iCode == 0 || iCode == "") return "No error";
        // General Errors 100-199
        if (iCode == 101) return "General exception";
        if (iCode == 102) return "General Initialization Failure";
        if (iCode == 103) return "Already Initialized";
        if (iCode == 104) return "Content Instance Terminated";
        if (iCode == 111) return "General Termination Failure";
        if (iCode == 112) return "Termination Before Initialization";
        if (iCode == 113) return "Termination After Termination";
        if (iCode == 122) return "Retrieve Data Before Initialization";
        if (iCode == 123) return "Retrieve Data After Termination";
        if (iCode == 132) return "Store Data Before Initialization";
        if (iCode == 133) return "Store Data After Termination";
        if (iCode == 142) return "Commit Before Initialization";
        if (iCode == 143) return "Commit After Termination";
        // Syntax Errors 200-299
        if (iCode == 201) return "General Argument Error";
        if (iCode == 301) return "General Get Failure";
        if (iCode == 351) return "General Set Failure";
        if (iCode == 391) return "General Commit Failure";
        // Data Model Errors 400-499
        if (iCode == 401) return "Undefined Data Model Element";
        if (iCode == 402) return "Unimplemented Data Model Element";
        if (iCode == 403) return "Data Model Element Value Not Initialized";
        if (iCode == 404) return "Data Model Element Is Read Only";
        if (iCode == 405) return "Data Model Element Is Write Only";
        if (iCode == 406) return "Data Model Element Type Mismatch";
        if (iCode == 407) return "Data Model Element Value Out Of Range";
        if (iCode == 408) return "Data Model Dependency Not Established";
        // Implementation-defined Errors 1000-65535
        if (iCode == 1000) return "General communication failure (Ajax)";
        return "General exception";
    };
    this.GetLastError = function () { return this._errorNumber; }; //OK
    this.SetValue = function (sKey, sValue) {//OK        
        this._SetError(0, "");
        //Auto-initialize if this has not been done
        if (!this._intialized) this.Initialize("");
        if (!this._CheckOpen()) return "false";

        if (sValue == null) sValue = "";
        if (sKey == "cmi.completion_status" || sKey == "cmi.core.lesson_status") sValue = replaceAll(sValue, "passed", "P", "failed", "F", "completed", "C", "incomplete", "I", "browsed", "B", "not attempted", "N");
        if (sValue == this._GetElements()[sKey]) return "true";
        if (this._dirtyElements == null) this._dirtyElements = new Object();
        this._GetElements()[sKey] = sValue;
        this._dirtyElements[sKey] = sValue;
        if (this.GetAutoCommit()) return this.Commit("");
        return "true";
    };
    this.GetValue = function (sKey) { //OK
        this._SetError(0, "");
        //Auto-initialize if this has not been done
        if (!this._intialized) this.Initialize("");
        if (!this._CheckOpen()) return "";
        var actualKey=sKey;
        if (sKey.indexOf("metier.") > -1) sKey = sKey.split("metier.")[1];
        var val = this._GetElements()[sKey];
        if (actualKey.indexOf("metier.") > -1) return val;
        if (sKey == "cmi.completion_status" || sKey == "cmi.core.lesson_status") val = replaceAll(val, "I", "incomplete", "N", "not attempted", "C", "completed", "P", "passed", "F", "failed", "B", "browsed", "unknown", "incomplete");
        return val;
    };
    this.SetAutoCommit = function (bValue) {
        this._SetError(0, "");
        if (!this._CheckOpen()) return "false";
        this._autoCommit = bValue;
    };
    this.GetAutoCommit = function () {
        this._SetError(0, "");
        if (!this._CheckOpen()) return "false";
        return this._autoCommit;
    };

    this._CheckOpen = function () {
        if (!this._intialized) {
            this._SetError(103, "");
            return false;
        }
        if (this._finished) {
            this._SetError(104, "Finish has already been called.");
            return false;
        }
        return true;
    }
    this._SetError = function (iCode, sDesc) {
        this._errorNumber = iCode;
        this._errorText = sDesc;
        if (iCode == 303) {
            fnSendEmail("phoenix@metier.zendesk.com", "debug@mymetier.net", "SCORM Error: " + iCode + " " + sDesc, false, "URL: " + window.location.href + "\nLog:\n" + this._commLog);
            //if (!this._warned) {
            if (cStr(sDesc).indexOf("403 - Forbidden: Access is denied") > -1) sDesc = "Your session has timed out!";
            fnShowMessage("<h2>Your data could not be saved.</h2><br />The error returned from the server is: <br /><br /><i>" + sDesc + "</i><br /><br />Please try clicking 'Save & Close' in the upper right corner, and re-opening the course player again.", "<div class='middle actionbutton acknowledge'><a>OK</a></div>");
                this._warned = true;
            //}
        }
    };    
    this._GetElements = function () {
        if (this._elements == null) {            
            var o = this._SendRequest("GET");
            this._elements = {                
                "cmi.suspend_data":o.SuspendData,
                "cmi.completion_status": o.Status,
                "cmi.score.raw": o.Score
            };
        }
        return this._elements;
    }
    this._SendRequest = function (method, url, data)
    {
        if (url == "/suspenddata" && data) data = JSON.stringify(data);
        this._commLog += (new Date()).toString() + " REQUEST: " + method + " / " + scormSessionId + cStr(url);
        var _oAdapter = this;
        var oRequest, baseUrl = this._url, scormSessionId = this.ScormSessionId;
        var requestFunction = function (setError)
        {            
            var opts = { contentType: "application/json", dataType: 'json', cache: false, type: method, data: data, async: false, error: function (oRequest, sStatus, oError) { if (setError) _oAdapter._SetError(303, oRequest.responseText); } };
            oRequest = $.ajax(baseUrl + "?url=" + encodeURIComponent(scormSessionId + cStr(url)), opts);
        }
        var oResponse = {};
        requestFunction();
        if (!oRequest || oRequest.status!=200) {
            //iOS8 hack--for some reason initial attempt returns error
            requestFunction(true);            
        }
        this._commLog += "\n" + (new Date()).toString() + " RESPONSE: " + oRequest.status  + ": " + oRequest.responseText;
        if (method == "GET" && !url) {
            eval("var temp=" + oRequest.responseText);
            return temp;
        }
        return oRequest;
    }
}

function fnShowMessage(Msg, Buttons, Options) {
    if (!Options) Options = {};
    Msg=cStr(Msg);
    if(Msg=="")return;
    if (!Options.Width && Msg.length > 1000) Options.Width = 700;    
    var s = "", dialogWidth = Math.max(cInt(Options.Width), 460);
    if (Buttons == null) s = "<div class='middle actionbutton acknowledge'><a>" + _Translations["button-acknowledge"] + "</a></div>";
    else if ($.isArray(Buttons)) {
        Buttons = $.grep(Buttons, function (n) { return n != null; });
        $(Buttons).each(function (i, o) { s += "<div style='float:left;padding-right:20px' class='actionbutton'><a>" + this[0] + "</a></div>"; });
    } else s = Buttons;
    var Box = $("#metier-msg").detach();
    //Box = $("<div id='metier-msg'><div class='top'>" + (Options.NoClose ? "" : "<a class='close' />") + "</div><div class='fill'><div class='text'>" + Msg + "</div></div><div class='bottom'><div class='buttons'>" + s + "</div></div></div>").find(".close, .acknowledge a").click(function () { $("#metier-msg").detach(); }).end().appendTo(document.body).css("left", Math.max(0, ($(window).width() - 900) / 2)).css("top", $(window).scrollTop() - 100);
    Box = $("<div id='metier-msg' style='width:" + dialogWidth + "px'><div class='top'><div class='left' /><div class='mid' /><div class='right'>" + (Options.NoClose ? "" : "<a class='close' />") + "</div></div><div class='body'><div class='left' /><div class='text'>" + Msg + "</div><div class='right' /></div><div class='bottom'><div class='left' /><div class='mid' /><div class='buttons' style='width:" + (dialogWidth-100) + "px'>" + s + "</div><div class='right' /></div></div>").find(".close, .acknowledge a").click(function () { $("#metier-msg").detach(); }).end().appendTo(document.body).css("left", Math.max(0, ($(window).width() - dialogWidth) / 2)).css("top", $(window).scrollTop() - 200);
    if ($.isArray(Buttons)) $(Buttons).each(function (i, o) { { Box.find(".actionbutton a").eq(i).click(function () { if (o[1]) o[1](); if (!Options.NoAutoClose) $("#metier-msg").detach(); }); } });
    return Box;
}

function fnGetSelection(doc) {
    if (!doc) doc = document;
    var R = {};
    if (window.getSelection) {
        var oSelection = window.getSelection();
        if (oSelection.getRangeAt) {
            if (oSelection.rangeCount == 0) return null;            
            var oRange = oSelection.getRangeAt(0);
            R.Range = oRange;
        } else {
            var oRange = doc.createRange();
            oRange.setStart(oSelection.anchorNode, oSelection.anchorOffset);
            oRange.setEnd(oSelection.focusNode, oSelection.focusOffset);

            // Handle the case when the selection was selected backwards (from the end to the start in the document)
            if (oRange.collapsed !== oSelection.isCollapsed) {
                oRange.setStart(oSelection.focusNode, oSelection.focusOffset);
                oRange.setEnd(oSelection.anchorNode, oSelection.anchorOffset);
            }
        }
        R.Start = oRange["startContainer"];
        R.End = oRange["endContainer"];
        if (R.Start.nodeType === 3) R.Start = R.Start.parentNode;
        if (R.End.nodeType === 3) R.End = R.End.parentNode;
    } else if (doc.selection) {
        var oSelStart = doc.selection.createRange();
        var oSelEnd = doc.selection.createRange();
        oSelStart.collapse(true);
        oSelEnd.collapse(false);
        R.Start = oSelStart.parentElement();
        R.End = oSelEnd.parentElement();
    }
    return R;
}

function fnCreateXmlNode() {
    var node = arguments[0], child;
    if (node[0]) node = node[0];
    if (!node.ownerDocument) node = node.documentElement;
    var doc = node.ownerDocument;
    for(var i = 1; i < arguments.length; i++) {
        child = arguments[i];
        if (typeof child == 'string') child = doc.createElement(child);        
        node.appendChild(child);
    }
    return child;
};
function fnSetXmlAttribute() {
    var node = arguments[0];
    if (node && node[0]) node = node[0];
    for (var i = 1; i < arguments.length; i++) {
        if (node && node.nodeName && arguments[i])
            node.setAttribute(arguments[i], arguments[++i]);        
    }
    return node;
}
function fnSerializeXML(node) {
    if (typeof window.XMLSerializer != "undefined")
        return new window.XMLSerializer().serializeToString(node);
    else
        return node.xml;    
}
try {
    if (top.location.href.indexOf("navigator.aspx") > -1 && top.location != self.location) {
        if (self.location.href.indexOf("/navigator") == -1 && self.location.href.indexOf("player.aspx") == -1 && self.location.href.indexOf("enroll.aspx") == -1) top.location = self.location.href;
        else top.document.title = self.document.title;
    }
} catch (e) { }

/* Start myMetier specific */
var _Translations = {};
$(document).ready(function () {    
    _Translations = $(document).data("__translation");    
    if (_Translations)
        fnTranslateControl();
    else _Translations = {};
    $(".metier-tabs").tabs();
});
function fnTranslateControl(ctrl) {
    if (_Translations) {
        var c = (ctrl ? $(ctrl).find("[translationkey]") : $("[translationkey]"));
        for (var x = 0; x < c.length; x++) {
            var s=_Translations[$(c[x]).attr("translationkey")];
            if(s)c[x].innerHTML = s;
        }
    }
    return ctrl;
}
function fnPortalShowContactUs() {
    fnShowMessage("<b>" + _Translations["contact-heading"] + "</b><br />" + _Translations["contact-body"] + "<br /><br /><textarea style='width:350px;height:100px'></textarea>", "<div class='left actionbutton'><a href='javascript:void(0)'>" + _Translations["button-send"] + "</a></div><div class='right cancelbutton'><a href='javascript:void(0)'>" + _Translations["button-cancel"] + "</a></div>").find(".cancelbutton a").click(function () { $("#metier-msg").detach(); }).end().find(".actionbutton a").click(function () { var s = trim($("#metier-msg textarea").val()); if (s == "") { } else { $.post("/learningportal/index.aspx?action=portalemail&user_id=" + getQuerystring("user_id"), { "data": s }); fnShowMessage("<b>" + _Translations["contact-heading"] + "</b><br /><br />" + _Translations["contact-success"]); } });
}
/* End myMetier specific */

function fnFormatHMS(iSeconds){
    function two(x) {return ((x>9)?"":"0")+x};    
    var i=Math.abs(iSeconds);
    var m=Math.floor(i/60);    
    return (iSeconds<0?"-":"")+Math.floor(m/60)+":"+two(m%60)+":"+two(i%60);    
}
function fnFormatMS(iSeconds) {
    function two(x) { return ((x > 9) ? "" : "0") + x };
    var i = Math.abs(iSeconds);
    return (iSeconds < 0 ? "-" : "") + Math.floor(i / 60) + ":" + two(i % 60);
}
function fnEnroll(iILMID) {    
    if ($("#enroll-dialog").length > 0) {
        var dialog = $("#enroll-dialog");
        var iframe = dialog.find("iframe");
    } else {
        var iframe = $('<iframe frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe>');
        var dialog = $("<div id='enroll-dialog'></div>").append(iframe).appendTo("body").dialog({
            autoOpen: false,
            modal: true,
            resizable: false,
            width: "auto",
            height: "auto",
            close: function () {
                iframe.attr("src", "");
            }
        });
    }
    iframe.attr({
        width: 800,
        height: $(window).height() - 100,
        src: 'enroll.aspx?ilm_id=' + iILMID
    });
    dialog.dialog("option", "title", sDialogTitle).dialog("open");
        
    //$('<div id="enroll-dialog"></div>').appendTo(document.body).dialog({ modal: true, "height": $(window).height() - 100, "width": 800, position: ['center', 'center'] }).dialog('open').dialog('option', 'title', sDialogTitle).load('enroll.aspx?ilm_id=' + iILMID);
}
function ShowPDUInfo(id) {
    var rec = $.grep(__pduInfo, function (a) { return a.Id == id; })[0];
    if (rec) {        
        fnShowMessage(
            "<b>" + cStr(_Translations["pdu-heading"]).format(rec.Name) + "</b><br /><br />"
            + "<ul><li>" + cStr(_Translations["pdu-count"]).format(rec.PDU) + "</li>"
            + (rec.ProviderNumber ? "<li>" + cStr(_Translations["pdu-provider-number"]).format(rec.ProviderNumber) + "</li>" : "")
            + (rec.ProviderName ? "<li>" + cStr(_Translations["pdu-provider-name"]).format(rec.ProviderName) + "</li>" : "")
            + (rec.ActivityNumber ? "<li>" + cStr(_Translations["pdu-activity-number"]).format(rec.ActivityNumber) + "</li>" : "")
            + (rec.ActivityName ? "<li>" + cStr(_Translations["pdu-activity-name"]).format(rec.ActivityName) + "</li>" : "")
            + "</ul>");
    } else {
        alert("Information could not be located");
    }
}

function clsVideoPlayer(Files, iVideoWidth, iVideoHeight) {
    var sMP4, sOGV, sThumbnail;
    $.each(Files, function () {
        var filename = (this.Filename ? this.Filename : this);
        if (filename.indexOf("http") != 0) filename = "https://mymetier.net" + filename;
        if (!sMP4 && filename.indexOf(".mp4") > -1) sMP4 = filename;
        else if (!sOGV && filename.indexOf(".ogv") > -1) sOGV = filename;
        else if (!sThumbnail && (filename.indexOf(".png") > -1 || filename.indexOf(".gif") > -1 || filename.indexOf(".jpg") > -1)) sThumbnail = filename;
    });
    $this = this;
    $this.play = function () { fnShowVideoPlayer(sMP4, sOGV, cInt(iVideoWidth), cInt(iVideoHeight)); }
    if (cStr(sMP4) != "") {
        $("#button-open-video").click(function () { $this.play(); }).show().find(".image").css("background-image", "url(" + sThumbnail + ")");
    }

    $(document).on('click touchstart', '.ui-widget-overlay', function () {
        $('#video-player').dialog("close");
    });

    function fnShowVideoPlayer(sMP4Version, sOGVVersion, iVideoWidth, iVideoHeight) {
        if ($("#video-player").length == 0) $("<div id='video-player' />").appendTo($(document.body));
        if (iVideoWidth == 0) iVideoWidth = 800;
        if (iVideoHeight == 0) iVideoHeight = 450;
        //$("#video-player").dialog("destroy");
        $("#video-player").dialog({
            autoOpen: true,
            hide: 'fold',
            show: { effect: 'slide', direction: "up" },
            width: iVideoWidth + 30,
            height: iVideoHeight + 60,
            modal: true,
            resizable: false,
            position: { my: "center top", at: "center top+60", of: window },
            beforeClose: function (event, ui) { try { $f().stop().unload(); $("object").remove(); } catch (e) { } }
        }).show();
        setTimeout(function () {
            $("#video-player").html(
            '<video controls style="border:0px solid pink" width="' + iVideoWidth + '" height="' + iVideoHeight + '" preload="auto" autoplay="autoplay">'
            + '<source width="' + iVideoWidth + '" height="' + iVideoHeight + '" src="' + sMP4Version + '" />'
            + (cStr(sOGVVersion) == "" ? "" : '<source width="' + iVideoWidth + '" height="' + iVideoHeight + '" src="' + sOGVVersion + '"  />')
            + '<a href="' + sMP4Version + '" style="display:block;width:' + iVideoWidth + ';height:' + iVideoHeight + 'px;" id="player"></a>'
            + '<' + 'script language="JavaScript">'
            + 'flowplayer("player", "flowplayer/flowplayer-3.2.14.swf");'
            + '<' + '/script>'
            + '</video>');
        }, 1000);
    }
}
var ISO3166CountryCodes = [["AF", "Afghanistan"], ["AL", "Albania"], ["DZ", "Algeria"], ["AS", "American Samoa"], ["AD", "Andorra"], ["AO", "Angola"], ["AI", "Anguilla"], ["AQ", "Antarctica"], ["AG", "Antigua and Barbuda"], ["AR", "Argentina"], ["AM", "Armenia"], ["AW", "Aruba"], ["AU", "Australia"], ["AT", "Austria"], ["AZ", "Azerbaijan"], ["BS", "Bahamas"], ["BH", "Bahrain"], ["BD", "Bangladesh"], ["BB", "Barbados"], ["BY", "Belarus"], ["BE", "Belgium"], ["BZ", "Belize"], ["BJ", "Benin"], ["BM", "Bermuda"], ["BT", "Bhutan"], ["BO", "Bolivia"], ["BQ", "Bonaire"], ["BA", "Bosnia and Herzegovina"], ["BW", "Botswana"], ["BV", "Bouvet Island"], ["BR", "Brazil"], ["IO", "British Indian Ocean Territory"], ["VG", "British Virgin Islands"], ["BN", "Brunei Darussalam"], ["BG", "Bulgaria"], ["BF", "Burkina Faso"], ["BI", "Burundi"], ["KH", "Cambodia"], ["CM", "Cameroon"], ["CA", "Canada"], ["CV", "Cape Verde"], ["KY", "Cayman Islands"], ["CF", "Central African Republic"], ["TD", "Chad"], ["CL", "Chile"], ["CN", "China"], ["CX", "Christmas Island"], ["CC", "Cocos (Keeling) Islands"], ["CO", "Colombia"], ["KM", "Comoros"], ["CG", "Congo"], ["CK", "Cook Islands"], ["CR", "Costa Rica"], ["CI", "Côte d'Ivoire"], ["HR", "Croatia"], ["CU", "Cuba"], ["CW", "Curaçao"], ["CY", "Cyprus"], ["CZ", "Czech Republic"], ["KP", "Democratic People's Republic of Korea"], ["DK", "Denmark"], ["DJ", "Djibouti"], ["DM", "Dominica"], ["DO", "Dominican Republic"], ["EC", "Ecuador"], ["EG", "Egypt"], ["SV", "El Salvador"], ["GQ", "Equatorial Guinea"], ["ER", "Eritrea"], ["EE", "Estonia"], ["ET", "Ethiopia"], ["FK", "Falkland Islands (Malvinas)"], ["FO", "Faroe Islands"], ["FM", "Federated States of Micronesia"], ["FJ", "Fiji"], ["FI", "Finland"], ["FR", "France"], ["GF", "French Guiana"], ["PF", "French Polynesia"], ["TF", "French Southern Territories"], ["GA", "Gabon"], ["GM", "Gambia"], ["GE", "Georgia"], ["DE", "Germany"], ["GH", "Ghana"], ["GI", "Gibraltar"], ["GR", "Greece"], ["GL", "Greenland"], ["GD", "Grenada"], ["GP", "Guadeloupe"], ["GU", "Guam"], ["GT", "Guatemala"], ["GG", "Guernsey"], ["GN", "Guinea"], ["GW", "Guinea-Bissau"], ["GY", "Guyana"], ["HT", "Haiti"], ["HM", "Heard Island and McDonald Islands"], ["VA", "Holy See (Vatican City State)"], ["HN", "Honduras"], ["HK", "Hong Kong"], ["HU", "Hungary"], ["IS", "Iceland"], ["IN", "India"], ["ID", "Indonesia"], ["IR", "Iran"], ["IQ", "Iraq"], ["IE", "Ireland"], ["IM", "Isle of Man"], ["IL", "Israel"], ["IT", "Italy"], ["JM", "Jamaica"], ["JP", "Japan"], ["JE", "Jersey"], ["JO", "Jordan"], ["KZ", "Kazakhstan"], ["KE", "Kenya"], ["KI", "Kiribati"], ["XK", "Kosovo"], ["KW", "Kuwait"], ["KG", "Kyrgyzstan"], ["LA", "Lao People's Democratic Republic"], ["LV", "Latvia"], ["LB", "Lebanon"], ["LS", "Lesotho"], ["LR", "Liberia"], ["LY", "Libya"], ["LI", "Liechtenstein"], ["LT", "Lithuania"], ["LU", "Luxembourg"], ["MO", "Macao"], ["MK", "Macedonia"], ["MG", "Madagascar"], ["MW", "Malawi"], ["MY", "Malaysia"], ["MV", "Maldives"], ["ML", "Mali"], ["MT", "Malta"], ["MH", "Marshall Islands"], ["MQ", "Martinique"], ["MR", "Mauritania"], ["MU", "Mauritius"], ["YT", "Mayotte"], ["MX", "Mexico"], ["MD", "Moldova"], ["MC", "Monaco"], ["MN", "Mongolia"], ["ME", "Montenegro"], ["MS", "Montserrat"], ["MA", "Morocco"], ["MZ", "Mozambique"], ["MM", "Myanmar"], ["NA", "Namibia"], ["NR", "Nauru"], ["NP", "Nepal"], ["NL", "Netherlands"], ["NC", "New Caledonia"], ["NZ", "New Zealand"], ["NI", "Nicaragua"], ["NE", "Niger"], ["NG", "Nigeria"], ["NU", "Niue"], ["NF", "Norfolk Island"], ["MP", "Northern Mariana Islands"], ["NO", "Norway"], ["PS", "Occupied Palestinian Territory"], ["OM", "Oman"], ["PK", "Pakistan"], ["PW", "Palau"], ["PA", "Panama"], ["PG", "Papua New Guinea"], ["PY", "Paraguay"], ["PE", "Peru"], ["PH", "Philippines"], ["PN", "Pitcairn"], ["PL", "Poland"], ["PT", "Portugal"], ["PR", "Puerto Rico"], ["QA", "Qatar"], ["KR", "Republic of Korea"], ["RE", "Réunion"], ["RO", "Romania"], ["RU", "Russian Federation"], ["RW", "Rwanda"], ["BL", "Saint Barthélemy"], ["SH", "Saint Helena"], ["KN", "Saint Kitts and Nevis"], ["LC", "Saint Lucia"], ["MF", "Saint Martin (French part)"], ["PM", "Saint Pierre and Miquelon"], ["VC", "Saint Vincent and the Grenadines"], ["WS", "Samoa"], ["SM", "San Marino"], ["ST", "Sao Tome and Principe"], ["SA", "Saudi Arabia"], ["SN", "Senegal"], ["RS", "Serbia"], ["SC", "Seychelles"], ["SL", "Sierra Leone"], ["SG", "Singapore"], ["SX", "Sint Maarten (Dutch part)"], ["SK", "Slovakia"], ["SI", "Slovenia"], ["SB", "Solomon Islands"], ["SO", "Somalia"], ["ZA", "South Africa"], ["GS", "South Georgia and the South Sandwich Islands"], ["SS", "South Sudan"], ["ES", "Spain"], ["LK", "Sri Lanka"], ["SD", "Sudan"], ["SR", "Suriname"], ["SJ", "Svalbard and Jan Mayen"], ["SZ", "Swaziland"], ["SE", "Sweden"], ["CH", "Switzerland"], ["SY", "Syrian Arab Republic"], ["TW", "Taiwan"], ["TJ", "Tajikistan"], ["TZ", "Tanzania"], ["TH", "Thailand"], ["CD", "The Democratic Republic of the Congo"], ["TL", "Timor-Leste"], ["TG", "Togo"], ["TK", "Tokelau"], ["TO", "Tonga"], ["TT", "Trinidad and Tobago"], ["TN", "Tunisia"], ["TR", "Turkey"], ["TM", "Turkmenistan"], ["TC", "Turks and Caicos Islands"], ["TV", "Tuvalu"], ["VI", "U.S. Virgin Islands"], ["UG", "Uganda"], ["UA", "Ukraine"], ["AE", "United Arab Emirates"], ["GB", "United Kingdom"], ["US", "United States"], ["UM", "United States Minor Outlying Islands"], ["UY", "Uruguay"], ["UZ", "Uzbekistan"], ["VU", "Vanuatu"], ["VE", "Venezuela"], ["VN", "Viet Nam"], ["WF", "Wallis and Futuna"], ["EH", "Western Sahara"], ["YE", "Yemen"], ["ZM", "Zambia"], ["ZW", "Zimbabwe"], ["AX", "Åland Islands"]];
var ISO3166CountryCodesNO = [["AF", "Afghanistan"], ["AL", "Albania"], ["DZ", "Algerie"], ["AS", "Amerikansk Samoa"], ["AD", "Andorra"], ["AO", "Angola"], ["AI", "Anguilla"], ["AQ", "Antarktika"], ["AG", "Antigua og Barbuda"], ["AR", "Argentina"], ["AM", "Armenia"], ["AW", "Aruba"], ["AZ", "Aserbajdsjan"], ["AU", "Australia"], ["BS", "Bahamas"], ["BH", "Bahrain"], ["BD", "Bangladesh"], ["BB", "Barbados"], ["BE", "Belgia"], ["BZ", "Belize"], ["BJ", "Benin"], ["BM", "Bermuda"], ["BT", "Bhutan"], ["BO", "Bolivia"], ["BQ", "Bonaire, Sint Eustatius og Saba"], ["BA", "Bosnia-Hercegovina"], ["BW", "Botswana"], ["BV", "Bouvetøya"], ["BR", "Brasil"], ["BN", "Brunei"], ["BG", "Bulgaria"], ["BF", "Burkina Faso"], ["BI", "Burundi"], ["CA", "Canada"], ["KY", "Caymanøyene"], ["CL", "Chile"], ["CX", "Christmasøya"], ["CO", "Colombia"], ["CK", "Cookøyene"], ["CR", "Costa Rica"], ["CU", "Cuba"], ["CW", "Curaçao"], ["DK", "Danmark"], ["AE", "De forente arabiske emirater"], ["TF", "De franske sørterritorier"], ["PS", "De palestinske territoriene"], ["DO", "Den dominikanske republikk"], ["CF", "Den sentralafrikanske republikk"], ["IO", "Det britiske territoriet i Indiahavet"], ["DJ", "Djibouti"], ["DM", "Dominica"], ["EC", "Ecuador"], ["EG", "Egypt"], ["GQ", "Ekvatorial-Guinea"], ["CI", "Elfenbenskysten"], ["SV", "El Salvador"], ["ER", "Eritrea"], ["EE", "Estland"], ["ET", "Etiopia"], ["FK", "Falklandsøyene"], ["FJ", "Fiji"], ["PH", "Filippinene"], ["FI", "Finland"], ["FR", "Frankrike"], ["GF", "Fransk Guyana"], ["PF", "Fransk Polynesia"], ["FO", "Færøyene"], ["GA", "Gabon"], ["GM", "Gambia"], ["GE", "Georgia"], ["GH", "Ghana"], ["GI", "Gibraltar"], ["GD", "Grenada"], ["GL", "Grønland"], ["GP", "Guadeloupe"], ["GU", "Guam"], ["GT", "Guatemala"], ["GG", "Guernsey"], ["GN", "Guinea"], ["GW", "Guinea-Bissau"], ["GY", "Guyana"], ["HT", "Haiti"], ["HM", "Heard- og McDonaldøyene"], ["GR", "Hellas"], ["HN", "Honduras"], ["HK", "Hongkong"], ["BY", "Hviterussland"], ["IN", "India"], ["ID", "Indonesia"], ["IQ", "Irak"], ["IR", "Iran"], ["IE", "Irland"], ["IS", "Island"], ["IL", "Israel"], ["IT", "Italia"], ["JM", "Jamaica"], ["JP", "Japan"], ["YE", "Jemen"], ["JE", "Jersey"], ["VI", "Jomfruøyene, De amerikanske"], ["VG", "Jomfruøyene, De britiske"], ["JO", "Jordan"], ["KH", "Kambodsja"], ["CM", "Kamerun"], ["CV", "Kapp Verde"], ["KZ", "Kasakhstan"], ["KE", "Kenya"], ["CN", "Kina"], ["KG", "Kirgisistan"], ["KI", "Kiribati"], ["CC", "Kokosøyene"], ["KM", "Komorene"], ["CD", "Kongo, Den demokratiske republikken"], ["CG", "Kongo, Republikken"], ["XK", "Kosovo"], ["HR", "Kroatia"], ["KW", "Kuwait"], ["CY", "Kypros"], ["LA", "Laos"], ["LV", "Latvia"], ["LS", "Lesotho"], ["LB", "Libanon"], ["LR", "Liberia"], ["LY", "Libya"], ["LI", "Liechtenstein"], ["LT", "Litauen"], ["LU", "Luxembourg"], ["MO", "Macao"], ["MG", "Madagaskar"], ["MK", "Makedonia, Republikken"], ["MW", "Malawi"], ["MY", "Malaysia"], ["MV", "Maldivene"], ["ML", "Mali"], ["MT", "Malta"], ["IM", "Man"], ["MA", "Marokko"], ["MH", "Marshalløyene"], ["MQ", "Martinique"], ["MR", "Mauritania"], ["MU", "Mauritius"], ["YT", "Mayotte"], ["MX", "Mexico"], ["FM", "Mikronesiaføderasjonen"], ["MD", "Moldova"], ["MC", "Monaco"], ["MN", "Mongolia"], ["ME", "Montenegro"], ["MS", "Montserrat"], ["MZ", "Mosambik"], ["MM", "Myanmar"], ["NA", "Namibia"], ["NR", "Nauru"], ["NP", "Nepal"], ["NL", "Nederland"], ["NZ", "New Zealand"], ["NI", "Nicaragua"], ["NE", "Niger"], ["NG", "Nigeria"], ["NU", "Niue"], ["KP", "Nord-Korea"], ["MP", "Nord-Marianene"], ["NF", "Norfolkøya"], ["NO", "Norge"], ["NC", "Ny-Caledonia"], ["OM", "Oman"], ["PK", "Pakistan"], ["PW", "Palau"], ["PA", "Panama"], ["PG", "Papua Ny-Guinea"], ["PY", "Paraguay"], ["PE", "Peru"], ["PN", "Pitcairnøyene"], ["PL", "Polen"], ["PT", "Portugal"], ["PR", "Puerto Rico"], ["QA", "Qatar"], ["RE", "Réunion"], ["RO", "Romania"], ["RU", "Russland"], ["RW", "Rwanda"], ["BL", "Saint-Barthélemy"], ["SH", "St. Helena, Ascension og Tristan da Cunha"], ["KN", "Saint Kitts og Nevis"], ["LC", "Saint Lucia"], ["MF", "Saint-Martin"], ["PM", "Saint-Pierre og Miquelon"], ["VC", "Saint Vincent og Grenadinene"], ["SB", "Salomonøyene"], ["WS", "Samoa"], ["SM", "San Marino"], ["ST", "São Tomé og Príncipe"], ["SA", "Saudi-Arabia"], ["SN", "Senegal"], ["RS", "Serbia"], ["SC", "Seychellene"], ["SL", "Sierra Leone"], ["SG", "Singapore"], ["SX", "Sint Maarten"], ["SK", "Slovakia"], ["SI", "Slovenia"], ["SO", "Somalia"], ["ES", "Spania"], ["LK", "Sri Lanka"], ["GB", "Storbritannia"], ["SD", "Sudan"], ["SR", "Surinam"], ["SJ", "Svalbard og Jan Mayen"], ["CH", "Sveits"], ["SE", "Sverige"], ["SZ", "Swaziland"], ["SY", "Syria"], ["ZA", "Sør-Afrika"], ["GS", "Sør-Georgia og Sør-Sandwichøyene"], ["KR", "Sør-Korea"], ["SS", "Sør-Sudan"], ["TJ", "Tadsjikistan"], ["TW", "Republikken Kina"], ["TZ", "Tanzania"], ["TH", "Thailand"], ["TG", "Togo"], ["TK", "Tokelau"], ["TO", "Tonga"], ["TT", "Trinidad og Tobago"], ["TD", "Tsjad"], ["CZ", "Tsjekkia"], ["TN", "Tunisia"], ["TM", "Turkmenistan"], ["TC", "Turks- og Caicosøyene"], ["TV", "Tuvalu"], ["TR", "Tyrkia"], ["DE", "Tyskland"], ["UG", "Uganda"], ["UA", "Ukraina"], ["HU", "Ungarn"], ["UY", "Uruguay"], ["US", "USA"], ["UM", "USAs ytre småøyer"], ["UZ", "Usbekistan"], ["VU", "Vanuatu"], ["VA", "Vatikanstaten"], ["VE", "Venezuela"], ["EH", "Vest-Sahara"], ["VN", "Vietnam"], ["WF", "Wallis og Futuna"], ["ZM", "Zambia"], ["ZW", "Zimbabwe"], ["AT", "Østerrike"], ["TL", "Øst-Timor"], ["AX", "Åland"]];
function fnLoadCSS(href) {
    var cssLink = $("<link rel='stylesheet' type='text/css' href='" + href + "'>");
    $("head").append(cssLink);
};

function fnLoadJS(src, callback) {    
    var jsLink = $("<script type='text/javascript' src='" + src + "'>");
    $("head").append(jsLink);
    $.ajax({ dataType: "script", cache: true, url: src }).done(callback);
};
// Simple JavaScript Templating
// John Resig - http://ejohn.org/ - MIT Licensed
//(function () {
    var cache = {};

    this.tmpl = function tmpl(str, data) {
        // Figure out if we're getting a template, or if we need to
        // load the template - and be sure to cache the result.
        var fn = !/\W/.test(str) ?
          cache[str] = cache[str] ||
            tmpl(document.getElementById(str).innerHTML) :            
          // Generate a reusable function that will serve as a template
          // generator (and which will be cached).
          new Function("obj",
            "var p=[],print=function(){p.push.apply(p,arguments);};" +

            // Introduce the data as local variables using with(){}
            "with(obj){p.push('" +

            // Convert the template into pure JavaScript
            str
              .replace(/[\r\t\n]/g, " ")
              .split("{%").join("\t")
              .replace(/((^|%})[^\t]*)'/g, "$1\r")
              .replace(/\t=(.*?)%}/g, "',$1,'")
              .split("\t").join("');")
              .split("%}").join("p.push('")
              .split("\r").join("\\'")
          + "');}return p.join('');");        
        // Provide some basic currying to the user
        return data ? fn(data) : fn;
    };
//})();
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (obj, start) {
        for (var i = (start || 0), j = this.length; i < j; i++) {
            if (this[i] === obj) { return i; }
        }
        return -1;
    }
}

/**
 * @author Kyle Florence <kyle[dot]florence[at]gmail[dot]com>
 * @website https://github.com/kflorence/jquery-deserialize/
 * @version 1.3.3
 *
 * Dual licensed under the MIT and GPLv2 licenses.
 */
!function (e, t) { function n(t) { return t.map(function () { return this.elements ? e.makeArray(this.elements) : this }).filter(":input:not(:disabled)").get() } function i(n) { var i, a = {}; return e.each(n, function (e, n) { i = a[n.name], i === t && (a[n.name] = []), a[n.name].push(n) }), a } var a = Array.prototype.push, o = /^(?:radio|checkbox)$/i, l = /\+/g, r = /^(?:option|select-one|select-multiple)$/i, s = /^(?:button|color|date|datetime|datetime-local|email|hidden|month|number|password|range|reset|search|submit|tel|text|textarea|time|url|week)$/i; e.fn.deserialize = function (c, u) { var p, h, m = n(this), f = []; if (!c || !m.length) return this; if (e.isArray(c)) f = c; else if (e.isPlainObject(c)) { var d, g; for (d in c) e.isArray(g = c[d]) ? a.apply(f, e.map(g, function (e) { return { name: d, value: e } })) : a.call(f, { name: d, value: g }) } else if ("string" == typeof c) { var v; for (c = c.split("&"), p = 0, h = c.length; h > p; p++) v = c[p].split("="), a.call(f, { name: decodeURIComponent(v[0].replace(l, "%20")), value: decodeURIComponent(v[1].replace(l, "%20")) }) } if (!(h = f.length)) return this; var y, b, k, w, A, C, x, g, F, $, j, I, L = e.noop, N = e.noop, R = {}; for (u = u || {}, m = i(m), e.isFunction(u) ? N = u : (L = e.isFunction(u.change) ? u.change : L, N = e.isFunction(u.complete) ? u.complete : N), p = 0; h > p; p++) if (y = f[p], A = y.name, g = y.value, F = m[A], F && 0 !== F.length) if (R[A] === t && (R[A] = 0), j = R[A]++, F[j] && (b = F[j], x = (b.type || b.nodeName).toLowerCase(), s.test(x))) L.call(b, b.value = g); else for (k = 0, w = F.length; w > k; k++) if (b = F[k], x = (b.type || b.nodeName).toLowerCase(), C = null, o.test(x) ? C = "checked" : r.test(x) && (C = "selected"), C) { if (I = [], b.options) for ($ = 0; $ < b.options.length; $++) I.push(b.options[$]); else I.push(b); for ($ = 0; $ < I.length; $++) y = I[$], y.value == g && L.call(y, (y[C] = !0) && g) } return N.call(this), this } }(jQuery);

setTimeout(function () {
    if ($('.metierbar').length > 0) {
        $('body').css("margin-top", "53px");
        $('.metierbar').show().addClass("animated").addClass("animateIn");
    }
}, 500);