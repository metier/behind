﻿function MetaGrid(options)
{
	//Properties
	this.Container = "#GridSelector";
	this.$Container = null;
	this.Filters = [];
	this.SortColumn = -1;
	this.ClientPaging = true;
	this.PageIndex = 0;
	this.PageSize = 20;
	this.DataMode = "Client";
	this.Templates = [];
	this.FacetColumns = []; 
	this.SortColumns = []; 
	this.SearchColumns = []; 
	this.LocalData = null;
	this.Data = null;
	this.VisibleCount = 0;
	this.FacetOptions = {};
	
	//Methods	
	this.LoadOptions = function (options)
	{
		//Map options
		for(var option in options) 
		{
			this[option] = options[option];
		}
	};
	this.CalculatePageCount = function()
	{
		if (this.PageSize < 1) return 0;
		return Math.floor(this.VisibleCount / this.PageSize) + (this.VisibleCount % this.PageSize == 0 ? 0 : 1);
	};
	this.ValuesAreEqual = function (val1, val2, wildcard, caseSensetive)
	{
	    val2 = cStr(val2);
	    var values = ($.isArray(val1) ? val1 : [val1]);
	    for(var x=0;x<values.length;x++){		
	        val1 = cStr(values[x]);			
	        if (!caseSensetive)
	        {
	            val1 = val1.toUpperCase();
	            val2 = val2.toUpperCase();
	        }
	        if (wildcard && val1.indexOf(val2) > -1) return true;
	        if(val1 == val2) return true;
	    }
	    return false;
	};
	this.ApplyFilter = function ()
	{
		//Get Visble Count
		this.VisibleCount = this.LocalData.Records.length;

		var $grid = this;

		$.each(this.LocalData.Records, function (dataIndex, dataItem)
		{
			dataItem.Hidden = false;
			$.each($grid.Filters, function (filterIndex, filterItem)
			{
				//if not already hidden then process
				if (filterItem && !dataItem.Hidden)
				{
					if (filterItem.SearchIndex != null)
					{
						var search = $grid.SearchColumns[filterItem.SearchIndex];
						if (dataItem["_" + filterItem.ID] == null) dataItem["_" + filterItem.ID] = search.Method(dataItem);
						if (search.SearchFunction)
							dataItem.Hidden = !search.SearchFunction(row, filterItem.Value);
						else
							dataItem.Hidden = !$grid.ValuesAreEqual(dataItem["_" + filterItem.ID], filterItem.Value, filterItem.Wildcard, filterItem.CaseSensitive);
					} else
						dataItem.Hidden = !$grid.ValuesAreEqual(dataItem[filterItem.ID], filterItem.Value, filterItem.Wildcard, filterItem.CaseSensitive);
				}
			});
			if (dataItem.Hidden) $grid.VisibleCount--;
		});
	};
	this.CalculateFacets = function (columns)
	{
		var $grid = this;

		$.each(columns, function (facetIndex, facetItem)
		{
			$grid.CalculateFacet(facetIndex, facetItem);
			if (facetItem.Filter && facetItem.FacetColumns)
				$grid.CalculateFacets(facetItem.FacetColumns);
		});
	};
	this.CalculateFacet = function (facetIndex, facetItem)
	{
		var $grid = this;

		facetItem.Values = [];
		$.each(this.LocalData.Records, function (dataIndex, dataItem)
		{
			if (!dataItem.Hidden)
			{
			    var values = ($.isArray(dataItem[facetItem.ID]) ? dataItem[facetItem.ID] : [dataItem[facetItem.ID]]);
			    $.each(values, function () { 
				    var record = facetItem.Values[this];
				    if (!record)
				    {
				        record = { Count: 0, Caption: this };
					    facetItem.Values.push(record);
					    facetItem.Values[this] = record;
				    }
				    record.Count++;
			    });
			}
		});
		if (this.FacetOptions.Sort) facetItem.Values.sort(this.FacetOptions.Sort);
		else {
		    facetItem.Values.sort(function (a, b) {
		        return a.Caption.toUpperCase().localeCompare(b.Caption.toUpperCase());
		    });
		}		
	};

	this.RenderFacet = function (facetIndex, facetData, parentContainer)
	{	    
		var $grid = this;

		if (facetData.Values.length == 0) return false;

		var facet = $grid.Templates["facet"];
		var facetContainer = $(replaceAll($grid.Templates["facet-container"],"{0}","<div class='__container'/>"));
		
		if (facetData.Filter && $grid.Templates["facet-undo"]) facetContainer.find(".__container").append($($grid.Templates["facet-undo"]));

		$.each(facetData.Values, function (valueIndex, valueData)
		{
		    var hFacet = $(facet.format(valueData.Caption, valueData.Count));
		    hFacet.find(".facet-item").data("facet", facetData).data("facet-value",valueData.Caption);
		    facetContainer.find(".__container").append(hFacet);
		    var filter = $grid.GetFilter(facetData.ID, cStr(valueData.Caption));
		    if (filter) {
		        hFacet.addClass("active").find(".facet-item").data("filter", filter);
		    }
		});		
		facetContainer.appendTo(parentContainer).data("facet", facetData);
				
		if (facetData.Filter && facetData.FacetColumns != null)
		{
			$.each(facetData.FacetColumns, function (facetIndex, facetData)
			{
				$grid.RenderFacet(facetIndex, facetData, facetContainer);
			});
		}

		return true;
	}
	
	this.GetFilter = function (id,val) {
	    return $.grep(this.Filters, function (o) { return cStr(o.Value) == cStr(val) && o.ID == id; })[0];
	};
	this.ClearFacetFilter = function (facet)
	{
		var $grid = this;

		if (facet.Filter)
		{
		    $grid.Filters.removeByValue(null, facet.Filter);
			facet.Filter = null;
		}
		if (facet.FacetColumns)
		{
			$.each(facet.FacetColumns, function (facetIndex, facetData)
			{
				$grid.ClearFacetFilter(facetData);
			});
		}
	};
	this.RenderPaging = function ()
	{
		var $grid = this;

		if (!this.ClientPaging || this.PageSize < 1) return;
		var paging = $(this.Templates["paging"]);
		if (this.PageIndex < 1) paging.find(".prev").addClass("inactive");
		if ((this.PageIndex + 1) >= this.CalculatePageCount()) paging.find(".next").addClass("inactive");
		this.$Container.find(".paging-container").children().detach().end().append(paging);
		var pageNumber = paging.find(".page-number");
		var pagingIndex = Math.max(0, this.PageIndex - 3);
		var maxPaging = Math.min(this.CalculatePageCount(), this.PageIndex + 3);
		for (; pagingIndex < maxPaging; pagingIndex++)
		{
			var currentNumber = pageNumber.clone();
			var link = currentNumber.find(".paging-link").html(pagingIndex + 1);

			if (pagingIndex == this.PageIndex) link.contents().unwrap();
			else link.data("index", pagingIndex);
			pageNumber.parent().find(".page-number:last").after(currentNumber);

			if (pagingIndex == this.PageIndex && this.PageIndex == this.CalculatePageCount() - 1) currentNumber.find(".separator").detach();
		}
		pageNumber.detach();
	};
	this.RenderSearch = function ()
	{
		var $grid = this;

		if (cStr(this.Templates["search"]) == "") return;
		var searchContainer = this.$Container.find(".search-container").children().detach().end();
		$.each(this.SearchColumns, function (searchIndex, searchItem)
		{
			var search = $($grid.Templates["search"]).appendTo(searchContainer);
			var setSearchFilter = function ()
			{
				var value = search.find(".search").val();

				if (searchItem.Filter)
				    $grid.Filters.removeByValue(null, searchItem.Filter);

				if (value != "")
				{
					var filter = { Value: value, Wildcard: true, ID: "search-" + searchItem.ID, SearchIndex: searchIndex };
					$grid.Filters.push(filter);
					searchItem.Filter = filter;
				}
				$grid.Render();
			};
			search.find(".search").keypress(function (e)
			{
				if (e.which == 13)
				{
					setSearchFilter();
					return false;
				}
			});
			if (searchItem.SearchOnBlur)
				search.find(".search").change(function () { setSearchFilter(); });
			else search.find(".go").click(function ()
			{
				setSearchFilter();
			});
		});
	};
	this.SortData = function (index, desc)
	{
		var $grid = this;

		this.SortColumn = index;
		this.LocalData.Records.sort(function (a, b)
		{
		    var aa=$grid.SortColumns[index].Method(a), bb=$grid.SortColumns[index].Method(b);
		    return (aa<bb?-1:(aa>bb?1:0)) * (desc ? -1 : 1);
		});
	};

	this.Render = function ()
	{
	    this.$Container.trigger("renderStart", [$grid]);
		//Load Data
		this.LoadData();

		var $grid = this;

		this.ApplyFilter();
		this.RenderPaging();
		this.CalculateFacets(this.FacetColumns);
		this.$Container.find(".facet-container").detach();

        //Populate filter facets
		var hasFacets = false;
		$.each($grid.FacetColumns, function (facetIndex, facetData)
		{
			if ($grid.RenderFacet(facetIndex, facetData, $grid.$Container.find(".facets"))) hasFacets = true;
		});
		this.$Container.find(".facets").toggle(hasFacets);

        //Populate sort facet
		if (this.$Container.find(".sort-container").length>0) {
		    this.$Container.find(".sort-container").children().detach();
		    $.each($grid.SortColumns, function (sortIndex, sortData) {
		        var sortItem = $($grid.Templates["sort"].format(sortData.Caption));
		        if (sortIndex == $grid.SortColumn) sortItem.find("a.sort-item").contents().unwrap();
		        sortItem.appendTo($grid.$Container.find(".sort-container"));
		    });
		}
	    		
		$grid.$Container.find(".list").children().detach();		
		var rowCounter = 0;
		$.each(this.LocalData.Records, function (dataIndex, dataItem)
		{
			if (!dataItem.Hidden)
			{
				if (!$grid.ClientPaging || ((rowCounter >= ($grid.PageIndex * $grid.PageSize) && rowCounter < (($grid.PageIndex + 1) * $grid.PageSize))))
				{
				    var row = $grid.LocalData.Records[dataIndex]._Cache;
				    if (!row) {
				        row = $grid.Templates["row"];
				        for (var prop in dataItem) {
				            row = replaceAll(row, "#" + prop + "#", dataItem[prop]);
				        }
				        $grid.LocalData.Records[dataIndex]._Cache = row;
				    }
					$(row).toggleClass("odd", rowCounter % 2 == 1).appendTo($grid.$Container.find(".list"));
				}
				rowCounter++;
			}
		});
		if (cStr(this.Templates["title"]) != "") this.$Container.find(".title").html(this.Templates["title"].format(($grid.PageSize * $grid.PageIndex + 1), Math.min($grid.VisibleCount, ($grid.PageSize * (1 + $grid.PageIndex))), $grid.VisibleCount));
		this.$Container.trigger("renderEnd", [$grid]);
	};

	this.LoadData = function ()
	{	    
		//if alreayd loaded, exit
		if (this.LocalData != null) return;

		//Set Local Data
		var localData = this.Data;

		//If is function, then call
		if ($.isFunction(this.Data))
		{
			localData = this.Data();
		}

		//If is url, then load through ajax
		else if (this.Data.toString().indexOf("/") == 0)
		{
			$.ajax({

				url: this.Data,
				async: false,
				type: "POST",
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				success: function (data)
				{
					localData = eval(data.d);
				}
			});
		}

		//else, assume it is json
		else
			localData = this.Data;
        
	    //Set Local Value
		if (!localData.Records) localData = { Records: localData };
		this.LocalData = localData;
		if (this.SortColumn > -1) this.SortData(this.SortColumn);
	}

	//Initialize
	this.Initialize = function ()
	{
		var $grid = this;
		this.LoadOptions(options);
		this.$Container = $(this.Container);
		this.$Container.data("Grid",this);
				
		$.each(this.$Container.children(".templates").find("[class]"), function ()
		{
		    if (!$grid.Templates[$(this).attr("class")]) $grid.Templates[$(this).attr("class")] = replaceAll(this.outerHTML, "\r", "", "\n", "");
		});
		this.$Container.find(".templates").detach();

		this.RenderSearch();
        
		this.$Container
			.on("click", ".facet-item", function ()
			{
				var parent = $(this).parents(".facet-container:eq(0)");
				var facet = parent.data("facet");
				if (!facet) return;
				if ($(this).parents(".facet-undo").length > 0)
				{
					$grid.ClearFacetFilter(facet);
				}

				else
				{
				    var filter = { Value: $(this).data("facet-value"), Wildcard: false, ID: facet.ID, Facet:facet };
				    var duplicateFilter = $grid.GetFilter(filter.ID,cStr(filter.Value));
				    if (duplicateFilter) return;
					$grid.Filters.push(filter);
					facet.Filter = filter;
				}
				$grid.Render();
			})

			.on("click", ".sorts a.sort-item", function ()
			{
				var parent = $(this).parents(".sort-container");
				sortData(parent.find(".sort").index($(this).parents(".sort")), false);
				$grid.Render();
			})

			.on("click", ".paging-container .next, .paging-container .prev, .paging-container .paging-link", function ()
			{
				if ($(this).hasClass("inactive")) return;
				if ($(this).hasClass("prev") && $grid.PageIndex > 0) $grid.PageIndex--;
				if ($(this).hasClass("next") && $grid.PageIndex + 1 < $grid.CalculatePageCount()) $grid.PageIndex++;
				if ($(this).hasClass("paging-link")) $grid.PageIndex = $(this).data("index");
				$grid.Render();
			});

	    //Alternatively, attach to sort columns
		this.$Container.find(".sort-column").click(function () {
		    if ($(this).hasClass('active')) {
		        $(this).toggleClass('desc');
		    } else {
		        $grid.$Container.find(".sort-column").removeClass('active').removeClass('desc');
		        $(this).addClass('active');
		    }
		    $grid.SortData($grid.$Container.find(".sort-column").index($(this)), $(this).hasClass('desc'));
		    $grid.Render();
		});

		if ($grid.SortColumns.length == 0) {
		    $grid.$Container.find(".sort-column").each(function (i, o) {
		        if ($(this).attr("sort-function")) {
		            var f = eval("new Function('row','grid',\"" + $(this).attr("sort-function").split('"').join("'") + "\");");
		            $grid.SortColumns.push({ ID: "Col" + i, Method: f });
		        }
		    });
		}		
	}

	this.Initialize();	
};
