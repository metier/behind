﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.functions;
using System.Text;
using System.Security.Cryptography;
using session = Phoenix.LearningPortal.Util.Phoenix.session;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Badges
{
    public partial class Criteria : jlib.components.webpage
    {
        private string GetHostAndSchema()
        {
            return $"{this.Request.Url.Scheme}://{this.Request.Url.Host}";
        }
        public string CourseTitle, CourseDescription, CourseInfo, BadgeImageUrl;
        public void Page_PreRender(object sender, EventArgs e)
        {
            var userId = Request.QueryString["userid"].Int();
            var activityId = Request.QueryString["activityid"].Int();

            var user = Util.Classes.user.getUser(userId);
            user.PhoenixSession = new Util.Phoenix.session(true);

            var activity = new Util.Classes.Activity(user.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ActivityGet, activityId), true, true).DataObject, user);
            //var customerArticle = new Util.Classes.CustomerArticle(phoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.CustomerArticleGet, activity.CustomerArticleId), true, true).DataObject, null);
            //var description = parse.stripHTML(parse.replaceAll(activity.GetDescription() ?? "", "</div><div>","\n","<br>","\n", "<br />", "\n"));
            var description = parse.stripHTML(parse.replaceAll(activity.GetDescription() ?? "", "</div><div>", " ", "<br>", "\n", "<br />", "\n"));

            this.CourseTitle = $"{user.FirstName} {user.LastName} has completed {activity.ArticleCopyObject.Title}";
            this.CourseDescription = activity.GetDescription();
            this.CourseInfo = activity.GetCourseInfo();
            if (activity.IsElearning && activity.ELearnCourseObject != null)
            {
                var lessonNames = activity.ELearnCourseObject.Lessons.Where(l => !l.IsFinalTest).Select(l => l.Name).ToList();
                if (lessonNames.Count > 0)
                    this.CourseInfo += $"{convert.cIfValue(this.CourseInfo, "", "<br /><br />", "")}The course covers the following topics:<br />{string.Join("<br />", lessonNames)}";
            }



            this.BadgeImageUrl = $"{GetHostAndSchema()}{Util.MyMetier.GetBadgeImageUrl(activity)}";
        }
        public void Page_PreInit(object sender, EventArgs e)
        {
            (Page.Master as Inc.Master.MyMetier).clearWrapper().DisableLoginRedirect = true;
            this.Language = "en";
            
        }

        
    }
}