﻿<%@ Page Title="MetierOEC - Badge Criteria" Language="C#" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Badges.Criteria" MasterPageFile="~/inc/master/mymetier.master" Codebehind="criteria.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<style>
		html, body
        {
            height:100%;            
		}
        body{
            background: url(../gfx/bg-default.png) top repeat-x #FFF;
            font-family: "Helvetica Neue", Arial, Helvetica, sans-serif;
            font-size: 0.9em;
            color: #475d70;
        }
	</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>MetierOEC - Badge Criteria</title>

<script src="./inc/js/jquery.js?3" type="text/javascript"></script>
<script src="./inc/js/functions.js?14" type="text/javascript"></script>
<script type="text/javascript" src="./inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="~/inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.css" />
</head>

<body>
	<div class="" style="/* margin-top:200px; */min-height: 70%;border-radius: 16px;border: 1px solid silver;width: 800px;background-color: white;margin: 100px auto;">
<table>
    <tbody><tr>
        <td>
            <img src="<%=BadgeImageUrl%>" style="padding:25px 50px; width:100px">
        </td>
        <td><h1><%=CourseTitle%></h1></td>
    </tr>        
</tbody>

</table>
        <hr />
        <div style="line-height: 1.4;margin: 25px 50px;font-style: italic;">            
            <%=CourseDescription%>
        </div>
        <div style="line-height: 1.4;margin: 25px 50px;font-style: italic;">            
            <%=CourseInfo%>
        </div>
	</div>
</body>

</html>
</asp:Content>