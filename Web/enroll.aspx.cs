﻿//check CanEnroll to see whether enrollment form will go through for an exam -- Use LP-Token
//check IsQualified to see if user will be able to enroll, but with some critera not met
using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.functions;
using System.Text;
using System.Security.Cryptography;
using System.Linq;
using session = Phoenix.LearningPortal.Util.Phoenix.session;

namespace Phoenix.LearningPortal
{
    public partial class Enroll : jlib.components.webpage
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            (Page.Master as Inc.Master.MyMetier).clearWrapper(true);
            this.Language = (Page.Master as Inc.Master.MyMetier).ActiveUser.UserLanguage;
        }

        dynamic UserSettings { get; set; }
        dynamic OriginalUserCompetency { get; set; }
        dynamic UserCompetency { get; set; }
        dynamic ChangedObjects { get; set; }

        public void Page_Init(object sender, EventArgs e)
        {

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Util.Classes.user activeUser = (this.Master as Inc.Master.MyMetier).ActiveUser;
            if (!Request["change-org"].IsNullOrEmpty())
            {
                Util.Email.sendEmail("mailsender@metieracademy.com", convert.cStr(activeUser.DistributorEmail, "kurssporsmal@metier.no"), "", "espen.skaarer@metier.no", "A user has changed Organisation!", String.Format("The user: <b>{0}</b> has changed organisation to <b>{1}</b>.<br />Please update the information in myMetier and notify the user when the update is executed.<br /><br /><b>Important: This user might be waiting for your confirmation that the change has gone through before enrolling.</b>", activeUser.Username, Request["change-org"]), true, null);
                Response.End();
            }
            Util.Classes.ActivitySet activitySet = activeUser.AvailableCourses.FirstOrDefault(set => set.Id == this.QueryString("ilm_id").Int());
            if (activitySet == null)
            {
                lMessage.Text = String.Format("<b>Error</b><br />ActivitySet (ID={0}) not found.", this.QueryString("ilm_id"));
                pEnrollmentContent.Visible = false;
                return;
            }
            if (activitySet.Activities.FirstOrDefault(activity => activity.ArticleType == Util.Data.MetierClassTypes.ELearning && activity.IsImportedHistory) != null)
            {
                lMessage.Text = "<b>Error</b><br />This offering contains an elearning activity that has been archived, and you therefore can't be enrolled. Please contact your course coordinator.";
                pEnrollmentContent.Visible = false;
                return;
            }

            Util.Classes.Resource eCoach = null;
            bool availableSpots = true, isExam = (activitySet.Activities.Count == 1 && activitySet.Activities[0].ArticleType == Util.Data.MetierClassTypes.Exam);
            activitySet.Activities.ForEach(activity =>
            {
                if (activity.Ecoaches.Count > 0) eCoach = activity.Ecoaches[0];
                if (activity.MaxParticipants > 0 && activity.MaxParticipants <= activity.NumberOfParticipants) availableSpots = false;
            });
            if (!availableSpots)
            {
                pCourseFull.Visible = true;
                pCourseNotFull.Visible = false;
                lCourseFull.Text = String.Format(jlib.helpers.translation.translate(this.Language, "", this, (isExam ? "exam" : "course") + "-full"), "mailto:" + activeUser.DistributorEmail, activeUser.DistributorName);

                return;
            }
            if ((activitySet.EnrollmentFrom.HasValue && activitySet.EnrollmentFrom > DateTime.Now) || (activitySet.EnrollmentTo.HasValue && activitySet.EnrollmentTo < DateTime.Now))
            {
                pCourseFull.Visible = true;
                pCourseNotFull.Visible = false;
                lEnrollmentErrorHeading.Text = jlib.helpers.translation.translate(this.Language, "", this, "heading-" + (isExam ? "exam" : "course") + "-outside-enrollment-period");

                //Past the enrollment date
                if (activitySet.EnrollmentTo.HasValue && activitySet.EnrollmentTo < DateTime.Now) lCourseFull.Text = String.Format(jlib.helpers.translation.translate(this.Language, "", this, (isExam ? "exam" : "course") + "-outside-enrollment-period"), "mailto:" + activeUser.DistributorEmail, activeUser.DistributorName);
                else lCourseFull.Text = String.Format(jlib.helpers.translation.translate(this.Language, "", this, (isExam ? "exam" : "course") + "-before-enrollment-period"), "mailto:" + activeUser.DistributorEmail, activeUser.DistributorName, activitySet.EnrollmentFrom.Value);
                return;
            }
            
            UserSettings = jlib.functions.json.DynamicJson.Parse((Request["userSettings"].IsNullOrEmpty() ? convert.cStr(activeUser.PhoenixSession.PhoenixRequest("GET", session.Queries.UserGetCurrent, "", false, false).Data, "{}") : Request["userSettings"]));
            OriginalUserCompetency = (activeUser.ExamCompetence != null ? activeUser.ExamCompetence.Data : null);
            UserCompetency = (parse.replaceAll(Request["userCompetency"], "{}").IsNullOrEmpty() && OriginalUserCompetency != null ? jlib.functions.json.DynamicJson.Parse(OriginalUserCompetency.ToString()) : jlib.functions.json.DynamicJson.Parse(convert.cStr(Request["userCompetency"], "{}")));
            ChangedObjects = jlib.functions.json.DynamicJson.Parse(convert.cStr(Request["changedObjects"], "{}"));


            hILM.setValue(convert.cLong(this.QueryStringLong("ilm_id"), hILM.Value));


            if (activeUser.OrganizationID == 0)
            {
                lMessage.Text = "<b>Error</b><br />No Organization configured.<br />Our technical support team has been notified.";
                Util.Email.sendEmail("mailsender@metieracademy.com", convert.cStr((eCoach != null ? eCoach.ContactEmail : ""), "mailsender@metieracademy.com"), "", "", "User not able to enroll", "A user just encountered a problem enrolling to an integrated learning event, and was not able to enroll.<br><br>Name: <b>" + activeUser.FirstName + " " + activeUser.LastName + "</b><br>Organisation (id): <b>" + activeUser.OrganizationID + "</b><br>Email: <b><a href='mailto:" + activeUser.Email + "'>" + activeUser.Email + "</a></b><br>Telephone: <b>" + activeUser.PhoneNumber + "</b><br>Integrated learning event: <b>" + activitySet.Name + "</b><br><br>Please check the organization-properties as this is likely to cause this error.", true, null);
                pEnrollmentContent.Visible = false;
                return;
            }

            pCompetencyContainer.Visible = false;
            if (activeUser.IsAllowUserEcts && activitySet.Activities.FirstOrDefault(x => { return x.ArticleType == Util.Data.MetierClassTypes.Exam && x.ArticleCopyObject.IsUserCompetenceInfoRequired; }) != null)
            {
                pCompetencyContainer.Visible = true;
                if (!this.IsPostBack && UserCompetency != null) UserCompetency.IsTermsAccepted = false;
            }

            hActivitySetName.Text = activitySet.Name;

            lName.setValue(activeUser.FirstName + " " + activeUser.LastName);
            if (parse.stripHTML(activeUser.Organization.TermsAndConditions).Length > 5)
            {
                lDisclaimer.setValue("<input type=\"checkbox\" id=\"cTermsAccept\"><label for=\"cTermsAccept\">" + String.Format(parse.replaceAll(jlib.helpers.translation.translate(this.Language, "", this, "terms-declaration"), "\"", "&quot;"), "<a href=\"javascript:void(0)\" onclick=\"fnDisplayTermsDialog();\">" + parse.replaceAll(jlib.helpers.translation.translate(this.Language, "", this, "terms-link"), "\"", "&quot;") + "</a>") + "</label>");
                ClientScript.RegisterClientScriptBlock(this.GetType(), "fnDisplayTerms", "function fnDisplayTermsDialog(){$('#terms-dialog').detach();$(\"<div id='terms-dialog'>" + parse.replaceAll(activeUser.Organization.TermsAndConditions, "\"", "&quot;", "\r", "", "\n", "") + "</div>\").appendTo(document.body).dialog({ modal: true, 'height': 400, 'width': 400, position: ['center', 'center'] }).dialog('open').dialog('option', 'title', \"" + parse.replaceAll(jlib.helpers.translation.translate(this.Language, "", this, "terms-dialog-title"), "\"", "&quot;") + "\");}; var _sTermsAcceptanceValidation=\"" + parse.replaceAll(jlib.helpers.translation.translate(this.Language, "", this, "terms-not-checked"), "\"", "&quot;") + "\";", true);
            }

            pOrgLocked.Visible = true;
            tOrgNameLocked.setValue(activeUser.Organization.Name);

            if (this.IsPostBack)
            {

                bool enrolledSuccess = false;
                if (Request["submit-source"] != "competence-file")
                {
                    if (jlib.functions.convert.isEmail(tNewEmail.Text) && tNewEmail.Text.ToLower() != activeUser.Username.ToLower()) Util.Email.sendEmail("mailsender@metieracademy.com", convert.cStr(activeUser.DistributorEmail, "kurssporsmal@metier.no"), "", "espen.skaarer@metier.no", "A user has changed email address!", String.Format("The user: {0} has changed mail address to {1}!<br />Please update the username accordingly and notify the user when executed.", activeUser.Username, tNewEmail.Text), true, null);

                    Util.Phoenix.session.RequestResult userResult = null;
                    userResult = activeUser.PhoenixSession.PhoenixRequest("PUT", session.Queries.UserPut, UserSettings.ToString(), false, false);

                    if (userResult == null || !userResult.Error)
                    {

                        session.RequestResult enrollResult = activeUser.PhoenixSession.PhoenixPut("activitysets/" + hILM.Value + "/enroll?userId=" + activeUser.ID);
                        if (enrollResult.Error)
                        {
                            lFormError.Visible = true;
                            lFormError.Text = enrollResult.HttpErrorMessage;
                        }
                        else
                        {
                            Session["alert.once"] = jlib.helpers.translation.translate(this.Language, "", "/navigator.aspx", Page, (isExam ? "exam-enrollment-confirmation" : "alert-enrollment-confirmation"));

                            //Response.Write("<script>top.location='navigator.aspx?ilm_id=" + hILM.Value + "';</script>");
                            Response.Write("<script>top.location='index.aspx';</script>");
                            enrolledSuccess = true;
                        }
                        //Grab latest RowVersion from Competency, since it might have been updated by the enroll-action
                        if (UserCompetency.Id())
                        {
                            activeUser.ResetExamCompetence();
                            if (activeUser.ExamCompetence != null && activeUser.ExamCompetence.RowVersion != null) UserCompetency.RowVersion = activeUser.ExamCompetence.RowVersion;
                        }
                        //Util.Phoenix.session.RequestResult competencyResult = activeUser.PhoenixSession.PhoenixRequest( ? "PUT" : "POST"), String.Format(session.Queries.UserCompetenceGetPutPost, activeUser.ID), UserCompetency.ToString(), false, false);
                    }
                    else
                    {
                        lFormError.Visible = true;
                        lFormError.Text = userResult.HttpErrorMessage;
                        UserSettings = activeUser.PhoenixSession.PhoenixRequest("GET", session.Queries.UserGetCurrent, "", false, false).DataObject;
                    }
                }
                if (pCompetencyContainer.Visible) ProcessCompetenceForm(activeUser, activitySet);
                if (enrolledSuccess)
                {
                    activeUser.PhoenixSession.PhoenixClearCache();
                    activeUser = Util.Classes.user.getUser(activeUser.ID, true);
                    Response.End();
                }
            }
            if (!activeUser.AllowEnroll)
            {
                lMessage.Text += activeUser.Organization.TermsAndConditions;
            }
            else
            {
                if (activeUser.Organization.IsShowPricesToUserOnPortal && activitySet.Price > 0)
                {
                    var priceText = jlib.helpers.translation.translate(this.Language, "Price: {0}", "/navigator.aspx", Page, "price");
                    if (priceText.IsNotNullOrEmpty()) {
                        lPrice.Visible = true;
                        lPrice.setValue(String.Format(priceText, activitySet.Price + " " + activitySet.Activities[0].Currency));
                    }
                }
            }

            List<object> competencyAttachments = new List<object>();
            if (convert.HasProperty(UserCompetency, "Attachments"))
                foreach (dynamic attachment in UserCompetency.Attachments)
                    competencyAttachments.Add(new Util.Classes.Attachment(attachment));

            lCompetencyAgree.OnClientClick = parse.replaceAll(lCompetencyAgree.OnClientClick, "{0}", jlib.helpers.translation.translate(this.Language, "You need to accept the terms before you can continue", "/enroll.aspx", Page, "competence-accept-terms"));


            string dob = convert.SafeGetProperty(UserCompetency, "DateOfBirth");
            if (!dob.IsNullOrEmpty()) UserCompetency.DateOfBirth = dob.SafeSplit("T", 0);
            string ssn = convert.SafeGetProperty(UserCompetency, "SocialSecurityNumber");
            if (!ssn.IsNullOrEmpty()) UserCompetency.SocialSecurityNumber = ssn.Substr(0, 6) + "*****";
            lScripts.Text += "var userSettings=" + UserSettings.ToString() + ";var companySettings=" + convert.cStr(Request["companySettings"], activeUser.PhoenixSession.PhoenixGet(String.Format(session.Queries.CustomerGet, activeUser.OrganizationID)).Data, "{}") + ";"
                + "var userCompetency=" + UserCompetency.ToString() + ";\n"
                 + "var userCompetencyAttachments=" + jlib.functions.json.DynamicJson.Serialize(competencyAttachments) + ";\n"
            + "var NagUserForExamCompetence=" + (activeUser.IsAllowUserEcts).Str().ToLower() + ";\n";
        }

        private void ProcessCompetenceForm(Util.Classes.user activeUser, Util.Classes.ActivitySet activitySet)
        {
            if (fCompetenceUpload.HasFile)
            {
                dynamic image = Util.Phoenix.session.UploadFile(fCompetenceUpload.FileName, fCompetenceUpload.PostedFile.ContentType, fCompetenceUpload.FileBytes);
                dynamic attachment = new jlib.functions.json.DynamicJson();
                foreach (var member in image.GetDynamicMemberNames())
                    attachment[member] = image[member];

                List<string> attachments = new List<string>();
                if (UserCompetency.Attachments())
                {
                    foreach (dynamic att in UserCompetency.Attachments)
                        attachments.Add(att.ToString());
                }
                attachments.Add(attachment.ToString());
                UserCompetency.Attachments = null;
                UserCompetency = jlib.functions.json.DynamicJson.Parse(parse.replaceAll(UserCompetency.ToString(), "\"Attachments\":null", "\"Attachments\":[" + attachments.Join(",") + "]"));
            }
            //if (fCompetenceUpload.HasFile || convert.HasProperty(ChangedObjects, "userCompetency")) {
            UserCompetency.FirstName = UserSettings.User.FirstName;
            UserCompetency.LastName = UserSettings.User.LastName;
            UserCompetency.Email = UserSettings.Email;
            UserCompetency.StreetAddress = UserSettings.User.StreetAddress;
            UserCompetency.ZipCode = UserSettings.User.ZipCode;
            UserCompetency.Country = UserSettings.User.Country;
            UserCompetency.SocialSecurityNumber = parse.replaceAll(convert.SafeGetProperty(UserCompetency,"SocialSecurityNumber"), " ", "");
            if (convert.cStr(UserCompetency.SocialSecurityNumber).EndsWith("*****")) UserCompetency.SocialSecurityNumber = convert.SafeGetProperty(OriginalUserCompetency, "SocialSecurityNumber");
            if (convert.cStr(UserCompetency.SocialSecurityNumber).Length != 11) UserCompetency.SocialSecurityNumber = null;
            if (convert.cStr(convert.SafeGetProperty(UserCompetency, "DateOfBirth")) != "") UserCompetency.DateOfBirth = convert.cDate(UserCompetency.DateOfBirth);

            List<string> nullifyIfEmpty = new List<string>() { "SocialSecurityNumber", "EducationType", "GraduationYear" };
            nullifyIfEmpty.ForEach(field =>
            {
                if (convert.cStr(convert.SafeGetProperty(UserCompetency, field)) == "") UserCompetency[field] = null;
            });
            if (Request["submit-source"] == "competence-file") UserCompetency.IsTermsAccepted = false;

            Util.Phoenix.session.RequestResult competencyResult = activeUser.PhoenixSession.PhoenixRequest((convert.HasProperty(UserCompetency, "Id") ? "PUT" : "POST"), String.Format(session.Queries.UserCompetenceGetPutPost, activeUser.ID), UserCompetency.ToString(), false, false);

            activeUser.PhoenixSession.PhoenixClearCache(String.Format(session.Queries.UserCompetenceGetPutPost, activeUser.ID));
            //if (Request["submit-source"] == "competence" || Request["submit-source"] == "competence-file") {
            activeUser.PhoenixSession.PhoenixClearCache();
            activeUser = Util.Classes.user.getUser(activeUser.ID, true);
            //}        
            UserCompetency = (activeUser.ExamCompetence != null ? jlib.functions.json.DynamicJson.Parse(activeUser.ExamCompetence.Data.ToString()) : jlib.functions.json.DynamicJson.Parse("{}"));
            if (UserCompetency != null && convert.cStr(convert.SafeGetProperty(UserCompetency, "EducationType")) == "0" && convert.cBool(convert.SafeGetProperty(UserCompetency, "IsTermsAccepted"))
                        //&& convert.cBool(UserCompetency.IsAccreditedPracticalCompetenceSatisfied)
                        && (OriginalUserCompetency == null || !convert.cBool(OriginalUserCompetency.IsTermsAccepted)))
            {
                Session["alert.once"] = Session["alert.once"] + "<br />" + jlib.helpers.translation.translate(this.Language, "", "/enroll.aspx", Page, "competence-work-experience-check");
                if (!convert.cBool(UserCompetency.IsAccreditedPracticalCompetenceSatisfied))
                {
                    Util.Classes.Resource ecoach = activitySet.GetResources(Util.Data.ResourceTypes.ECoach, Util.Data.MetierClassTypes.Exam).SafeGetValue(0);
                    if (ecoach == null) ecoach = activitySet.GetResources(Util.Data.ResourceTypes.ECoach, null).SafeGetValue(0);
                    Util.Email.SendRazorEmail(new { User = activeUser.UserObject, ECoach = (ecoach == null ? null : ecoach.Data) }, "email-user-without-work-experience", "email-user-without-work-experience", "", true, "en");
                }
            }
            //}        
        }
    }
}