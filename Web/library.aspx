﻿<%@ Page Title="myMetier" Language="C#" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Library" MasterPageFile="~/inc/master/mymetier.master" Codebehind="library.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">
    
    <div id="wrapper-introduction">
    <common:label runat="server" TranslationKey="heading" Tag="h1" />   
</div>
    <div style="width:930px; margin:0 auto">
                  
       <common:label runat="server" ID="lFileSections" />
    <asp:PlaceHolder runat="server" ID="pDownloadSectionTemplate">
        <common:label runat="server" Tag="h1" runat="server" DataMember="heading" style="margin-bottom:10px" />    
        <common:label runat="server" Tag="ul" DataMember="list" style="padding:10px;margin-bottom:20px; border:1px solid #d0d6dc;background-color:white; border-radius: 9px;" CssClass="header nine-rounded" />
    </asp:PlaceHolder>

    <asp:PlaceHolder runat="server" ID="pDownloadItemTemplate">
    <common:hyperlink runat="server" CssClass="downloads highlight" DataMember="link" Target="_blank">
      <li>
      <common:label runat="server" tag="h3" DataMember="title" />
      <common:label runat="server" tag="p" DataMember="details" CssClass="infotext" />      
      </li>
      </common:hyperlink>
    </asp:PlaceHolder>
    
    </div>
        
</asp:Content>