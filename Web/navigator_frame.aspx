﻿<%@ Page Title="myMetier" Language="C#" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.NavigatorFrame" MasterPageFile="~/inc/master/mymetier.master" Codebehind="navigator_frame.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" Runat="Server">
<style>
html, body
{    
    background-color:White;
    height:100%;    
    overflow:hidden;
}
iframe
{
    height:2000px;
    width:100%;    
}
</style>

<common:label runat="server" ID="lIFrame" />
<img style="width:0;height:0" id="prevent-timeout" />
<script language="javascript" type="text/javascript">    
    <common:label runat="server" id="lScript" />

$(document).ready(function(){
$(window).resize();
});
$(window).resize(function(){    
    $("#content").height($(window).height());
});
setInterval(function () { $("#prevent-timeout")[0].src = "index.aspx?" + Math.floor(Math.random() * 100000); }, 1200000);

function fnSetLessonData(data){
    window.LessonData=$.extend(true,{},data);
}

    var iCurrentRCO = 0;
    var scormSessionId = "";
    var iClassroomID = 0;
    var URL = "";
    var _API = null;
    var _Player;
    var _ScrollTop = 0;
    $(window).unload(function () { fnMetierPlayRCO(0, 0, ''); });

    function fnMetierPlayRCO(iClass, iRCO, sUrl, iType, bInternal) {

        if (!bInternal) {
            if (_Player != null && !_Player.closed) {
                try {
                    fnFocusPlayer();
                    return;
                } catch (e) { }
            }
        }
        var isPlayer2 = cStr(sUrl).indexOf("/player2/") > -1;
        if (isPlayer2)
        {
            $("html,body").css("height", "auto");
        }
        //iType==1 Exam
        if (iType == 1) {                
            _ScrollTop = $($("#content")[0].contentWindow).scrollTop();
            $("#content")[0].src = sUrl;
            return;
        }

        iClassroomID = iClass;
        iCurrentRCO = 0;
        if (_API != null) _API.Terminate();

        if (iRCO == 0) return;

        var _bError = false;
        
        window.API_1484_11 = null;
        if(iUserID>0){
            var oRequest = $.ajax({ url:"./player/scorm.aspx?url=" + encodeURIComponent("create?rcoId="+iRCO), cache: false, type: "post", async: false, error: function (oRequest, sStatus, oError) { _bError = true; alert('An error occurred initiating attempt on server. Output: ' + sStatus + " (" + oError + ")"); } });                        
            if (_bError) return;
            if (oRequest.responseText.split("-").length<5) {
                alert("The returned value does not look like a valid GUID. Response from SCORM webservice was: " + oRequest.responseText);
                return;
            }
            scormSessionId = replaceAll(oRequest.responseText,'"','');
        }                

        iCurrentRCO = iRCO;
        if (iRCO > 0) {
            if(iUserID>0)
                window.API_1484_11 = new clsScorm2004API(sSCORMServer, iRCO);
            else
                window.API_1484_11 = new clsScorm2004Local(sSCORMServer, iRCO);
            _API = window.API_1484_11;
            
            _API.ScormSessionId = scormSessionId;
            if (!isPlayer2 && cStr(sUrl) != "" && sUrl.indexOf("course_id=") == -1) sUrl += (sUrl.indexOf("?") == -1 ? "?" : "&") + "course_id=" + LessonData.CourseID;
            URL = sUrl;
            
            if (sUrl == null) return true;
                
            if (scormSessionId && sUrl.indexOf("/exam/player.aspx")>-1){
                _API.Initialize("");
                _API.SetValue("cmi.completion_status","I");
                _API.Commit("");                    
            }
            if (!isPlayer2 && scormSessionId && (sUrl.indexOf("/exam/start.aspx")>-1 || sUrl.indexOf("")>-1)) sUrl += (sUrl.indexOf("?") == -1 ? "?" : "&") + "scorm_attempt=" + scormSessionId + "&class_id=" + iClass;
            if (sUrl.indexOf("/player/") > -1 || isPlayer2) {
                _ScrollTop = $($("#content")[0].contentWindow).scrollTop();                    
                $("#content")[0].src = sUrl;
            } else {
                var width = 1010;
                var height = 669;                
                _Player = window.open(sUrl, "pma_lesson_window", "width=" + width + ", height=" + height + ", left=" + (screen.width - width - 14) / 2 + ", top=" + (screen.height - height - 73) / 2 + ", directories=0, location=0, menubar=0, resizable=1, scrollbars=0, status=0, toolbar=0");
            }
            fnFocusPlayer();
            
            var oPlayerWindowClose = setInterval(function () {
                try {
                    if (_Player && _Player.closed) {
                        clearTimeout(oPlayerWindowClose);
                        if (_API != null) {
                            try{
                                _API.Terminate();
                                _API = null;
                            }catch(e){}
                        }
                        _Player = null;
                        window.location.href = (window.location.href + "&top=").split("&top=")[0] + "&reset=true&top=" + $(window).scrollTop();
                    }
                } catch (e) { }
            }, 200);
        }
    }
</script>
<script language="javascript" type="text/javascript">

    function fnRestoreNavigator() {
        fnMetierPlayRCO(0, 0, '');
        URL = "";
        $("#content")[0].src = sFrameURL + "&reset=true&top=" + _ScrollTop;        
    }

function fnFocusPlayer() {
    if (window.focus && _Player && !_Player.closed) {            
        _Player.focus();
        setTimeout(function () { if(_Player && !_Player.closed) _Player.focus(); }, 100);
    }
}

window.onbeforeunload = function () {    
    if (_Player && !_Player.closed && cStr(URL).indexOf("/exam/start.aspx?") == -1 && cStr(URL) != "") {
        fnFocusPlayer();
        return "You have an open course player window. Navigating away from this window will prevent your answers from being saved. Are you sure you want to continue?";
    } else if (cStr(URL).indexOf("/player/") > -1) {
        return "You should close the course player using the Save & Close button in the top-right corner. Navigating away from this window will prevent your answers from being saved. Are you sure you want to continue?";
    }
};
//$(window).focus(function(){fnFocusPlayer();});
if (fnIsTouchDevice()) {
    $("iframe").bind("load resize", function () {
        $(this.contentWindow.document.body).find("form:eq(0)").css({
            'height': $(window).height(),
            'width': $(window).width(),
            'overflow': 'scroll',
            '-webkit-overflow-scrolling': (/Android/.test(navigator.userAgent) ? 'touch' : 'scroll')
        });        
    });
};
</script>
</asp:Content>