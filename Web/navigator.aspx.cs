﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.components;
using jlib.db;
using jlib.functions;
using System.Text;
using System.Linq;
using System.Security.Cryptography;
using jlib.helpers;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal
{
    public partial class Navigator : jlib.components.webpage
    {
        private Util.Classes.organization m_oOrganization;
        private Util.Classes.user activeUser;
        private class PRINCE2ExamDateSwitchEligible
        {
            public bool IsFoundationEligble { get; private set; }
            public bool CanSwitchFoundation { get; private set; }
            public bool IsPractitionerEligble { get; private set; }
            public bool CanSwitchPractitioner { get; private set; }
            public bool IsViewingExamActivity { get; private set; }
            public Util.Classes.ActivitySet FoundationExamActivityset { get; private set; }
            public Util.Classes.ActivitySet PractitionerExamActivityset { get; private set; }
            public class Codes
            {
                public static string FExam = "EPM22";
                public static string PExam = "EPM23";
                public static string FElearn = "PM22E";
                public static string PElearn = "PM23E";
                public static string FPElearn = "PM24E";
            }

            public PRINCE2ExamDateSwitchEligible(bool isEnrolled, Util.Classes.user activeUser, List<Util.Classes.Activity> examActivities, List<Util.Classes.Activity> activitiesBeingViewed, Util.Classes.ActivitySet activitySet)
            {
                if (!isEnrolled) return;
                if (activeUser.Organization.DistributorId != 1194) return;
                if (activeUser.Organization.ID == 4369) return;

                //If not arbitrary, return unless explicitly turned on for customer 
                if (!activeUser.Organization.IsArbitrary &&
                    AttackData.Settings.GetByField(ObjectID: activeUser.OrganizationID, TableName: "Customers", KeyName: "ExamSwitcher", Value: "show").Execute().FirstOrDefault() == null)
                    return;

                //If not arbitrary, show unless explicitly hidden for customer
                if (activeUser.Organization.IsArbitrary &&
                    AttackData.Settings.GetByField(ObjectID: activeUser.OrganizationID, TableName: "Customers", KeyName: "ExamSwitcher", Value: "hide").Execute().FirstOrDefault()!=null)
                    return;

                //Hide for activitySet if explcitly hidden
                if (activitySet != null && AttackData.Settings.GetByField(ObjectID: activitySet.Id, TableName: "ActivitySets", KeyName: "ExamSwitcher", Value: "hide").Execute().FirstOrDefault() != null)
                    return;

                foreach (var activity in examActivities)
                    if (activity.ArticleNumber.StartsWith(Codes.FExam) || activity.ArticleNumber.StartsWith(Codes.PExam)) IsViewingExamActivity = true;

                if (IsViewingExamActivity)
                {
                    //if viewing an exam, we need to check enrollment in the corresponding P2 course
                    Util.Classes.Enrollment course = activeUser.Enrollments.FirstOrDefault(enrollment => { return enrollment.ArticleNumber.StartsWith(Codes.FPElearn) || enrollment.ArticleNumber.StartsWith(examActivities[0].ArticleNumber.Substr(1, 4) + "E"); });
                    if (course != null)
                    {
                        IsFoundationEligble = examActivities[0].ArticleNumber.StartsWith(Codes.FExam);
                        IsPractitionerEligble = examActivities[0].ArticleNumber.StartsWith(Codes.PExam);
                        if (activitySet.CanUnenroll && examActivities[0].Participation.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Enrolled)
                        {
                            if (IsFoundationEligble)
                            {
                                CanSwitchFoundation = true;
                                FoundationExamActivityset = activitySet;
                            }
                            if (IsPractitionerEligble)
                            {
                                CanSwitchPractitioner = true;
                                PractitionerExamActivityset = activitySet;
                            }
                        }
                    }
                }
                else
                {
                    //if not viewing an exam, make sure we're viewing a P2 elearning course
                    var course = activitiesBeingViewed.FirstOrDefault(activity => { return activity.ArticleNumber == Codes.FElearn || activity.ArticleNumber == Codes.PElearn || activity.ArticleNumber == Codes.FPElearn; });
                    if (course == null) return;                    
                    IsFoundationEligble = course.ArticleNumber == Codes.FElearn || (course.ArticleNumber == Codes.FPElearn && activitySet != null && activitySet.Name.IndexOf("Foundation", StringComparison.CurrentCultureIgnoreCase) > -1);
                    IsPractitionerEligble = course.ArticleNumber == Codes.PElearn || course.ArticleNumber == Codes.FPElearn;
                    Util.Classes.Enrollment currentFExam = activeUser.Enrollments.FirstOrDefault(enrollment => { return enrollment.ArticleNumber == Codes.FExam && enrollment.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Enrolled; }),
                        currentPExam = activeUser.Enrollments.FirstOrDefault(enrollment => { return enrollment.ArticleNumber == Codes.PExam && enrollment.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Enrolled; });
                    List<Util.Classes.Enrollment> startedFExams = activeUser.Enrollments.FindAll(enrollment => { return enrollment.ArticleNumber == Codes.FExam && enrollment.ParticipantStatusCodeValue != Util.Classes.Enrollment.ParticipantStatusCodes.Enrolled; }),
                        startedPExams = activeUser.Enrollments.FindAll(enrollment => { return enrollment.ArticleNumber == Codes.PExam && enrollment.ParticipantStatusCodeValue != Util.Classes.Enrollment.ParticipantStatusCodes.Enrolled; });

                    CanSwitchFoundation = IsFoundationEligble && startedFExams.Count < 2 && (currentFExam == null || currentFExam.ActivitySetObject.CanUnenroll);
                    CanSwitchPractitioner = IsPractitionerEligble && startedPExams.Count < 2 && (currentPExam == null || currentPExam.ActivitySetObject.CanUnenroll);
                    if (currentFExam != null) FoundationExamActivityset = currentFExam.ActivitySetObject;
                    if (currentPExam != null) PractitionerExamActivityset = currentPExam.ActivitySetObject;
                }
            }
        }

        private Util.Classes.ActivitySet ActivitySet { get; set; }
        private bool DisplayAsILM { get; set; }
        private Util.Classes.Resource ECoach { get; set; }
        private Util.Classes.Resource Instructor { get; set; }
        private Util.Classes.Resource Location { get; set; }
        private Util.Classes.Resource StudyAdvisor { get; set; }
        List<Util.Classes.Activity> ClassroomSession { get; set; }
        List<Util.Classes.Activity> ELearnSession { get; set; }
        List<Util.Classes.Activity> ExamSession { get; set; }
        private bool IsEnrolled { get; set; }//Is user enrolled in activity we are displaying?

        private void InitiateActivities()
        {
            List<int> activityIds = new List<int>();
            parse.split(this.QueryString("activity_ids"), ",").ToList().ForEach(x => { if (x.Int() > 0) activityIds.Add(x.Int()); });
            if (this.QueryString("ilm_id").Int() > 0)
            {
                ActivitySet = new Util.Classes.ActivitySet(activeUser.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ActivitySetGet, this.QueryString("ilm_id").Int())).DataObject, activeUser);
            }
            else if (this.QueryString("class_id").Int() > 0)
            {
                //attempt to load ActivitySet this activity belongs to. Clear all activities except for selected            
                Util.Classes.Enrollment enrollment = activeUser.Enrollments.FirstOrDefault(en => en.ActivityId == this.QueryString("class_id").Int());
                if (enrollment != null && enrollment.ActivitySetObject != null)
                {
                    ActivitySet = enrollment.ActivitySetObject;
                    for (int x = 0; x < ActivitySet.Activities.Count; x++)
                    {
                        if (ActivitySet.Activities[x].Id != this.QueryString("class_id").Int())
                        {
                            ActivitySet.Activities.RemoveAt(x);
                            x--;
                        }
                    }
                }
                //if not found, or not enrolled
                if (ActivitySet == null || ActivitySet.Activities.Count == 0) ActivitySet = activeUser.AvailableCourses.FirstOrDefault(x => x.Activities.Count == 1 && x.Activities[0].Id == this.QueryString("class_id").Int());
                if (ActivitySet == null) ActivitySet = new Util.Classes.ActivitySet(null, activeUser);
                if (ActivitySet.Activities.Count == 0)
                {
                    Util.Phoenix.session.RequestResult result = activeUser.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ActivityGet, this.QueryString("class_id").Int()));
                    if (!result.Error)
                        ActivitySet.Activities.Add(new Util.Classes.Activity(result.DataObject, activeUser));
                }
                DisplayAsILM = false;
            }

            //Remove activities from ActivitySet that were not passed in on the activity_ids-query string parameter
            if (ActivitySet != null && activityIds.Count > 0)
            {
                for (int x = 0; x < ActivitySet.Activities.Count; x++)
                {
                    if (!activityIds.Contains(ActivitySet.Activities[x].Id))
                    {
                        ActivitySet.Activities.RemoveAt(x);
                        x--;
                    }
                }
            }

            if (ActivitySet == null || ActivitySet.Activities.Count == 0)
            {
                Response.Write("Requested activity or activity set could not be found");
                Response.End();
            }
            InitiateResources();

            ClassroomSession = new List<Util.Classes.Activity>();
            ELearnSession = new List<Util.Classes.Activity>();
            ExamSession = new List<Util.Classes.Activity>();



            for (int x = 0; x < ActivitySet.Activities.Count; x++)
            {
                //Exams should not be displayed as part of an ILM -- if accessing page through ilm_id, exclude exams
                if (ActivitySet.Activities[x].ArticleType == Util.Data.MetierClassTypes.Classroom) ClassroomSession.Add(ActivitySet.Activities[x]);
                else if (ActivitySet.Activities[x].ArticleType == Util.Data.MetierClassTypes.ELearning)
                {
                    if (ActivitySet.Activities[x].ELearnCourseObject != null) ELearnSession.Add(ActivitySet.Activities[x]);
                    //if (ActivitySet.Activities[x]["elearn_end_date"].Str() != "") ActivitySet.Activities[x]["end_date"] = ActivitySet.Activities[x]["elearn_end_date"];
                }
                else if (ActivitySet.Activities[x].ArticleType == Util.Data.MetierClassTypes.Exam)
                {
                    if (!ActivitySet.Activities[x].IsEnrolled)
                    {
                        ExamSession.Add(ActivitySet.Activities[x]);
                    }
                    else
                    {
                        if ((this.QueryStringLong("class_id") > 0 && this.QueryStringInt("class_id") != ActivitySet.Activities[x].Id) || (this.QueryStringLong("ilm_id") > 0 && ActivitySet.Activities.Count > 1))
                        {
                            if (Util.MyMetier.IsPrince2SpecialHandling(ActivitySet.Activities[x].VersionNumber) || Util.MyMetier.IsPrince2SpecialHandling(ActivitySet.Activities[x].ArticleNumber)) DisplayAsILM = false;
                            ActivitySet.Activities.RemoveAt(x);
                            x--;
                            continue;
                        }
                        else
                        {
                            ExamSession.Add(ActivitySet.Activities[x]);
                            for (x = 0; x < ActivitySet.Activities.Count; x++)
                            {
                                if (ActivitySet.Activities[x].Id != ExamSession[0].Id)
                                {
                                    ActivitySet.Activities.RemoveAt(x);
                                    x--;
                                }
                            }
                            ClassroomSession.Clear();
                            ELearnSession.Clear();
                        }
                    }
                }
                if (ActivitySet.Activities[Math.Min(ActivitySet.Activities.Count - 1, x)].IsEnrolled) IsEnrolled = true;
                //if user is only enrolled in some of the activities of this activity set, display as non-ILM
                if (DisplayAsILM && x > 0 && ActivitySet.Activities[Math.Min(ActivitySet.Activities.Count - 1, x)].IsEnrolled != ActivitySet.Activities[0].IsEnrolled) DisplayAsILM = false;
            }
            if (ELearnSession.Count > 1)
            {
                for (int x = 1; x < ELearnSession.Count; x++)
                {
                    if (ELearnSession[x].IsEnrolled && ELearnSession[x].Id == this.QueryStringInt("class_id"))
                    {
                        ELearnSession.Swap(ELearnSession[x], ELearnSession[0]);
                        break;
                    }
                }
            }
            if (ELearnSession.Count > 1 && ClassroomSession.Count > 0)
            {
                for (int x = 0; x < ELearnSession.Count; x++)
                {
                    if (ELearnSession[x].ArticleNumber == parse.stripEndingCharacter(ClassroomSession[0].ArticleNumber, "C"))
                    {
                        if (x > 0) ELearnSession.Swap(ELearnSession[x], ELearnSession[0]);
                        break;
                    }
                }
            }
            if (IsEnrolled)
            {
                for (int x = 0; x < ExamSession.Count; x++)
                    if (ActivitySet.Activities.Count > 1 && (!ExamSession[x].IsEnrolled && (ExamSession[x].Id != this.QueryString("class_id").Int())))
                    {
                        ActivitySet.Activities.Remove(ExamSession[x]);
                        ExamSession.RemoveAt(x--);
                    }
                for (int x = 0; x < ClassroomSession.Count; x++)
                    if (ActivitySet.Activities.Count > 1 && (!ClassroomSession[x].IsEnrolled && (ClassroomSession[x].Id != this.QueryString("class_id").Int())))
                    {
                        ActivitySet.Activities.Remove(ClassroomSession[x]);
                        ClassroomSession.RemoveAt(x--);
                    }
                for (int x = 0; x < ELearnSession.Count; x++)
                    if (ActivitySet.Activities.Count > 1 && (!ELearnSession[x].IsEnrolled && (ELearnSession[x].Id != this.QueryString("class_id").Int())))
                    {
                        ActivitySet.Activities.Remove(ELearnSession[x]);
                        ELearnSession.RemoveAt(x--);
                    }

            }
            if (ExamSession.Count > 0 && ExamSession[0].ExamFKID == 0) ELearnSession.Add(ExamSession[0]);

            if (DisplayAsILM) DisplayAsILM = !(ExamSession.Count > 0);
        }
        private void InitiateResources()
        {
            ActivitySet.Activities.ForEach(activity =>
            {
                if (activity.Ecoaches.Count > 0) ECoach = activity.Ecoaches[0];
                if (activity.Instructors.Count > 0) Instructor = activity.Instructors[0];
                if (activity.Locations.Count > 0) Location = activity.Locations[0];
                if (activity.StudyAdvisors.Count > 0) StudyAdvisor = activity.StudyAdvisors[0];
            });
        }
        private void HandlePRINCE2ExamDateSwitch(PRINCE2ExamDateSwitchEligible PRINCE2ExamDateSwitchEligible)
        {
            int activitySetToEnroll = (bFoundationExamConfirm.IsClicked ? dFoundationExams.SelectedValue.Int() : dPractitionerExams.SelectedValue.Int());
            if (activitySetToEnroll > 0)
            {
                Util.Classes.ActivitySet activitySetToUnenroll = (bFoundationExamConfirm.IsClicked ? PRINCE2ExamDateSwitchEligible.FoundationExamActivityset : PRINCE2ExamDateSwitchEligible.PractitionerExamActivityset);
                Util.Phoenix.session.RequestResult result = null;
                if (activitySetToUnenroll != null)
                {
                    result = activeUser.PhoenixSession.PhoenixPut(String.Format(Util.Phoenix.session.Queries.ActivitySetUnenroll, activitySetToUnenroll.Id, activeUser.ID));
                    if (result.Error)
                        Session["alert.once"] = translation.translate(this.Language, "", this, "alert-unenroll-error");
                }
                if (result == null || !result.Error)
                {
                    result = new Util.Phoenix.session(true).PhoenixPut("ActivitySets/" + activitySetToEnroll + "/enroll?userId=" + activeUser.ID);

                    if (result.Error)
                    {
                        Session["alert.once"] = String.Format(translation.translate(this.Language, "", this, "alert-exam-switch-error"), result.HttpErrorMessage);
                    }
                    else
                    {
                        activeUser.PhoenixSession.PhoenixClearCache();
                        activeUser = Util.Classes.user.getUser(activeUser.ID, true);
                        Session["alert.once"] = translation.translate(this.Language, "", this, "alert-exam-switch-ok");
                        //if the activitySet the user enrolled in is take-home exam (has prevent unenrollment), send an email to Sumbal 
                        //var newActivitySet = new Util.Classes.ActivitySet(activeUser.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ActivitySetGet, activitySetToEnroll)).DataObject, activeUser);
                        //if (!newActivitySet.CanUnenroll)
                        //    Util.Email.sendEmail("noreply@mymetier.net", "sumbal.akhtar@metier.no;charlotte.wikdal.halle@metier.no", "jorgen@bids.no", "", "[mymetier.net] A user just switched to a P2 take-home exam?", "The user " + activeUser.Username + " just enrolled in " + newActivitySet.Name + ". This activityset is set as prevent unenrollment, which probably means it's a P2 take-home exam.", true, null);

                        //if viewing exam user was just unenrolled from, we need to redirect user to the new exam
                        if (PRINCE2ExamDateSwitchEligible.IsViewingExamActivity) Response.Redirect(jlib.net.HTTP.stripQueryStringParameters(Request.Url.PathAndQuery, "action", "class_id", "ilm_id") + "&ilm_id=" + activitySetToEnroll);
                    }
                }
            }
            if (bFoundationExamConfirm.IsClicked || bPractitionerExamConfirm.IsClicked) Response.Redirect(jlib.net.HTTP.stripQueryStringParameters(Request.Url.PathAndQuery, "action"));
        }

        private void UpdatePRINCE2ExamDateSwitchUI(PRINCE2ExamDateSwitchEligible PRINCE2ExamDateSwitchEligible)
        {
            lFoundationExamSwitcher.Visible = PRINCE2ExamDateSwitchEligible.CanSwitchFoundation;
            lPractitionerExamSwitcher.Visible = PRINCE2ExamDateSwitchEligible.CanSwitchPractitioner;
            lFoundationExamSwitchNotBooked.Visible = PRINCE2ExamDateSwitchEligible.FoundationExamActivityset == null;
            lPractitionerExamSwitchNotBooked.Visible = PRINCE2ExamDateSwitchEligible.PractitionerExamActivityset == null;
            lFoundationExamSwitchNotBooked1.Visible = lFoundationExamSwitchNotBooked.Visible;
            lPractitionerExamSwitchNotBooked1.Visible = lPractitionerExamSwitchNotBooked.Visible;
            if (!PRINCE2ExamDateSwitchEligible.CanSwitchFoundation && !PRINCE2ExamDateSwitchEligible.CanSwitchPractitioner) return;

            Util.Classes.organization onlineCustomer = Util.Classes.user.getOrganization(2338);
            lock (onlineCustomer)
            {
                List<Util.Classes.ActivitySet> sortedCalendar = onlineCustomer.CourseCalendar.ToList();
                sortedCalendar.Sort((x, y) => x.Activities.Count() > 0 && y.Activities.Count() > 0 ? x.Activities[0].ActivityStart.Date().CompareTo(y.Activities[0].ActivityStart.Date()) : 0);
                if (PRINCE2ExamDateSwitchEligible.IsFoundationEligble)
                {
                    if (PRINCE2ExamDateSwitchEligible.CanSwitchFoundation)
                    {
                        List<ListItem> items = new List<ListItem>();
                        sortedCalendar.ForEach(set =>
                        {
                            //&& activity.Locations.Count > 0
                            if (set.Activities.Count == 1 && set.Activities.FirstOrDefault(activity => { return activity.ArticleNumber == PRINCE2ExamDateSwitchEligible.Codes.FExam; }) != null) items.Add(new ListItem(set.Name, set.Id.Str()));
                        });
                        dFoundationExams.DataSource = items;
                        dFoundationExams.DataBind();
                        dFoundationExams.Items.Insert(0, translation.translate(this.Language, "", this, "exam-switch-choose-date"));
                        if (PRINCE2ExamDateSwitchEligible.FoundationExamActivityset != null) dFoundationExams.setValue(PRINCE2ExamDateSwitchEligible.FoundationExamActivityset.Id, true);
                        //} else {
                        //lFoundationExamSwitchPossible.Controls.Clear();
                        //lFoundationExamSwitchPossible.Text = translation.translate(this.Language, "", this, "exam-switch-not-possible") +
                        //    (PRINCE2ExamDateSwitchEligible.FoundationExamActivityset == null ? "" : String.Format(translation.translate(this.Language, "", this, "exam-switch-already-booked"), "<i>" + PRINCE2ExamDateSwitchEligible.FoundationExamActivityset.Name + "</i>"));
                    }
                }
                if (PRINCE2ExamDateSwitchEligible.IsPractitionerEligble)
                {
                    if (PRINCE2ExamDateSwitchEligible.CanSwitchPractitioner)
                    {
                        List<ListItem> items = new List<ListItem>();
                        sortedCalendar.ForEach(set =>
                        {
                            //    //&& activity.Locations.Count > 0
                            if (set.Activities.Count == 1 && set.Activities.FirstOrDefault(activity => { return activity.ArticleNumber == PRINCE2ExamDateSwitchEligible.Codes.PExam; }) != null) items.Add(new ListItem(set.Name, set.Id.Str()));
                        });
                        dPractitionerExams.DataSource = items;
                        dPractitionerExams.DataBind();
                        dPractitionerExams.Items.Insert(0, translation.translate(this.Language, "", this, "exam-switch-choose-date"));
                        if (PRINCE2ExamDateSwitchEligible.PractitionerExamActivityset != null) dPractitionerExams.setValue(PRINCE2ExamDateSwitchEligible.PractitionerExamActivityset.Id, true);
                        //} else {
                        //lPractitionerExamSwitchPossible.Controls.Clear();
                        //lPractitionerExamSwitchPossible.Text = translation.translate(this.Language, "", this, "exam-switch-not-possible") +
                        //    (PRINCE2ExamDateSwitchEligible.PractitionerExamActivityset == null ? "" : String.Format(translation.translate(this.Language, "", this, "exam-switch-already-booked"), "<i>" + PRINCE2ExamDateSwitchEligible.PractitionerExamActivityset.Name + "</i>"));
                    }
                }
            }
        }
        private void PopulateParticipants(List<Util.Classes.Participant> participants, int iNumPointsPossible)
        {
            for (int x = 0; x < participants.Count && x < 10; x++)
            {
                if (x < 3)
                {
                    Control oControl = jlib.helpers.control.cloneObject(pTop3Template) as Control;
                    pTop3Students.Controls.Add(oControl);
                    List<jlib.components.iControl> oControls = jlib.helpers.control.getDynamicControls(oControl, null);

                    image oImage = (jlib.helpers.control.findControlById(Page, oControl.Controls, "iAvatar")[0] as image);
                    oImage.ImageUrl = jlib.helpers.general.getThumbnailUrl(convert.cStr(participants[x].ImageUrl, "gfx/avatar.jpg"), (int)oImage.Width.Value, (int)oImage.Height.Value, true, jlib.helpers.general.FileStatus.None);

                    if (m_oOrganization.CompetitionMode == 0)
                    {
                        jlib.helpers.control.populateValue(oControls, "ranknumber", x + 1);
                        jlib.helpers.control.populateValue(oControls, "points", convert.cInt(participants[x].TotalScore) + " poeng");
                    }
                    else
                    {
                        (jlib.helpers.control.findControlsByDataMember(oControls, "ranknumber")[0] as label).Visible = false;
                        oImage.Style["margin-top"] = "0";
                    }
                    jlib.helpers.control.populateValue(oControls, "surname", participants[x].FirstName);
                    jlib.helpers.control.populateValue(oControls, "familyname", participants[x].LastName);

                    label oLabel = (jlib.helpers.control.findControlsByDataMember(oControls, "container")[0] as label);
                    oLabel.CssClass = "highlight student" + (x + 1);
                    oLabel.Attributes["onclick"] = "window.location='user.aspx?id=" + participants[x].Id + "'";

                }
                else
                {
                    Control oControl = jlib.helpers.control.cloneObject(pBottom7Template) as Control;
                    pBottom7Students.Controls.Add(oControl);
                    List<jlib.components.iControl> oControls = jlib.helpers.control.getDynamicControls(oControl, null);
                    if (m_oOrganization.CompetitionMode == 0)
                    {
                        jlib.helpers.control.populateValue(oControls, "ranknumber", (x + 1));
                        jlib.helpers.control.populateValue(oControls, "number", convert.cInt(participants[x].TotalScore));
                        label oLabel = (jlib.helpers.control.findControlsByDataMember(oControls, "progress")[0] as label);
                        oLabel.Width = convert.cUnit(Math.Min(100, 100 * convert.cInt(participants[x].TotalScore) / Math.Max(1, iNumPointsPossible)) + "%");
                    }
                    else
                    {
                        jlib.helpers.control.populateValue(oControls, "ranknumber", "&nbsp;");
                        label oLabel = (jlib.helpers.control.findControlsByDataMember(oControls, "progress-bar")[0] as label);
                        oLabel.Controls.Clear();
                        oLabel.CssClass = "";
                        oLabel.Text = "&nbsp;";
                    }

                    hyperlink oLink = jlib.helpers.control.findControlsByDataMember(oControls, "name")[0] as hyperlink;
                    oLink.NavigateUrl = "user.aspx?id=" + participants[x].Id;
                    oLink.setValue(participants[x].FirstName + " " + participants[x].LastName);
                }
            }
            lViewParticipants.Visible = participants.Count > 10;
            lViewParticipants.Text = String.Format(translation.translate(this.Language, "", this, "participants-view-all"), participants.Count);
            lViewParticipants.NavigateUrl += convert.cIfValue(this.QueryString("ilm_id"), "ilm_id=", "", "class_id=" + this.QueryStringLong("class_id"));
        }

        private string UpdateExamInfo(bool supressExamUnenrollment)
        {
            bool bResumableExamAttempt = false;

            string examUnenrollmentButton = "";
            if (ExamSession[0].Participation != null && (ExamSession[0].IsCaseExam || ExamSession[0].IsProjectAssignment))
            {
                pFileUploadContainer.Visible = true;
                if (!ExamSession[0].Participation.IsActive)
                {
                    lUploadFilesDescription.Text = String.Format(translation.translate(this.Language, "", this, "file-upload-availability"),
                        ExamSession[0].Participation.ParticipantAvailability.CalculatedAvailabilityStart.DteOffset().ToString(Util.MyMetier.getUserCulture(this.Language).DateTimeFormat.ShortTimePattern),
                        ExamSession[0].Participation.ParticipantAvailability.CalculatedAvailabilityStart.DteOffset().ToString(Util.MyMetier.getUserCulture(this.Language).DateTimeFormat.ShortDatePattern),
                        ExamSession[0].Participation.ParticipantAvailability.CalculatedAvailabilityEnd.DteOffset().ToString(Util.MyMetier.getUserCulture(this.Language).DateTimeFormat.ShortTimePattern),
                        ExamSession[0].Participation.ParticipantAvailability.CalculatedAvailabilityEnd.DteOffset().ToString(Util.MyMetier.getUserCulture(this.Language).DateTimeFormat.ShortDatePattern)
                        );
                }
                else
                {
                    pUploadNewFileContainer.Visible = true;
                    FileUpload[] uploads = new FileUpload[] { fUpload1 };
                    textbox[] titles = new textbox[] { tFileTitle1 };
                    int fileCounter = 0, uploadSuccessCounter = 0;
                    string uploadError = "";
                    for (int x = 0; x < uploads.Length; x++)
                    {
                        if (uploads[x].HasFile)
                        {
                            dynamic image = Util.Phoenix.session.UploadFile(uploads[x].FileName, uploads[x].PostedFile.ContentType, uploads[x].FileBytes);
                            dynamic file = jlib.functions.json.DynamicJson.Serialize(new
                            {
                                FileId = convert.cInt(image.FileId),
                                //Description = uploads[x].FileName,
                                Title = convert.cStr(titles[x].Text, uploads[x].FileName)
                            });
                            Util.Phoenix.session.RequestResult request = activeUser.PhoenixSession.PhoenixPut(String.Format(Util.Phoenix.session.Queries.ParticipantSubmitExam, ExamSession[0].Participation.Id), file);
                            if (!request.Error) uploadSuccessCounter++;
                            else if (request.HttpErrorJSON != null) uploadError += (uploadError.IsNullOrEmpty() ? "" : "<br />") + convert.cStr(convert.SafeGetList(request.HttpErrorJSON, "ValidationErrors").SafeGetValue(0));
                            fileCounter++;
                            //lUploadConfirmation.Text += "<li>" + String.Format(translation.translate(this.Language, "", this, "file-upload-item"), new Util.Classes.Attachment(file).Url, file.Title) + "</li>";
                        }
                    }
                    if (!uploadError.IsNullOrEmpty()) Session["alert.once"] = uploadError;
                    if (fileCounter > 0)
                    {
                        if (uploadSuccessCounter > 0)
                            Session["alert.once"] = String.Format(jlib.helpers.translation.translate(this.Language, "", this, "file-upload-success"), fileCounter);
                        activeUser.PhoenixSession.PhoenixClearCache();
                        activeUser = Util.Classes.user.getUser(activeUser.ID, true);
                    }
                    if (this.IsPostBack) Response.Redirect(Request.Url.PathAndQuery);
                }
                if (ExamSession[0].Participation.Attachments.Count > 0)
                {
                    lUploadedFiles.Text = "<ol>";
                    for (int x = 0; x < ExamSession[0].Participation.Attachments.Count; x++)
                    {
                        lUploadedFiles.Text += "<li><a target='_blank' href='" + ExamSession[0].Participation.Attachments[x].Url + "'>" + convert.cStr(ExamSession[0].Participation.Attachments[x].Title, ExamSession[0].Participation.Attachments[x].Description) + "</a></li>";
                    }
                    lUploadedFiles.Text += "</ul>";
                }
            }
            string description = ActivitySet.Activities[0].GetDescription();
            if (description != "") lCourseDescription.Text = "<div class='course-description'>" + parse.replaceAll(description, "<h3", "<br /><b", "h3>", "b>") + "</div>" + convert.cIfValue(lCourseDescription.Text, "", "<br />", "");

            if (ExamSession[0].Participation != null && ExamSession[0].ArticleType == Util.Data.MetierClassTypes.Exam)
            {

                lCourseDescription.Text += "<br /><br /><div><b> " + translation.translate(this.Language, "", this, "exam-heading") + "</b><br />";
                if (ExamSession[0].IsMPCExam)
                {
                    if (ExamSession[0].ActivityEnd == null)
                        lCourseDescription.Text += String.Format(Util.MyMetier.getUserCulture(this.Language), translation.translate(this.Language, "", this, "exam-mpc-info-definied-starttime"), ExamSession[0].NumberOfQuestions, ExamSession[0].Duration / 60000, ExamSession[0].ActivityStart);
                    else
                        lCourseDescription.Text += String.Format(Util.MyMetier.getUserCulture(this.Language), translation.translate(this.Language, "", this, "exam-mpc-info-defined-range"), ExamSession[0].NumberOfQuestions, ExamSession[0].Duration / 60000, ExamSession[0].ActivityStart, ExamSession[0].ActivityEnd);
                }
                else if (ExamSession[0].IsCaseExam)
                {
                    lCourseDescription.Text += String.Format(translation.translate(this.Language, "", this, "exam-case-info"), (ExamSession[0].CaseExamEndDate.HasValue ? ExamSession[0].CaseExamEndDate.Value.Subtract(ExamSession[0].ActivityStart.Value).TotalDays.Int() : 0));
                }
                else if (ExamSession[0].IsProjectAssignment)
                {
                    lCourseDescription.Text += String.Format(translation.translate(this.Language, "", this, "exam-assignment-info"));
                }

                //Only display unenroll button if user does not have exam switcher
                if (!supressExamUnenrollment && ActivitySet.CanUnenroll && ExamSession[0].Participation.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Enrolled)
                    examUnenrollmentButton = "<div style=\"float:left\" class=\"actionbutton\"><a href='javascript:void(0)' onclick='if(window.confirm(\"" + translation.translate(this.Language, "", this, "exam-unenroll-prompt") + "\")){window.location=\"navigator.aspx?action=unenroll&" + Request.QueryString + "\";}'>" + translation.translate(this.Language, "", this, "button-exam-unenroll") + "</a></div>";

                string startExamButton = (ExamSession[0].IsMPCExam && ExamSession[0].Participation.IsActive && ExamSession[0].Participation.ParticipantStatusCodeValue != Util.Classes.Enrollment.ParticipantStatusCodes.Completed && ExamSession[0].ExamInfoObject.TimeBeforeAvailableInSeconds.Dbl() < 1
                    ? "<div class=\"actionbutton\" style=\"float:left;padding-right:10px\"><a href='javascript:void(0)' onclick='fnMetierPlayRCO(" + ExamSession[0].Id + "," + 0 + ",\"" + System.Configuration.ConfigurationManager.AppSettings["phoenix.portal.url"] + "#/exam?activityId=" + ExamSession[0].Id + "\",1);'>" + translation.translate(this.Language, "", this, (bResumableExamAttempt ? "button-resume-exam" : "button-play-exam")) + "</a></div>" : "");

                if (examUnenrollmentButton + startExamButton != "") lCourseDescription.Text += "<br /><br /><br />" + startExamButton + examUnenrollmentButton + "<br style='clear:both' />";

                lCourseDescription.Text += "</div>";

            }
            return examUnenrollmentButton;
        }


        public void Page_Init(object sender, EventArgs e)
        {

            activeUser = (this.Master as Inc.Master.MyMetier).ActiveUser;
            m_oOrganization = (Page.Master as Inc.Master.MyMetier).CurrentOrg;
            if (this.QueryString("reset").Bln())
            {
                activeUser.PhoenixSession.PhoenixClearCache(String.Format(Util.Phoenix.session.Queries.UserPerformancesGet, activeUser.ID, "", ""));
                Util.Classes.user.getUser(activeUser.ID, true);
                Response.Redirect(jlib.net.HTTP.stripQueryStringParameters(Request.Url.PathAndQuery, "reset"));
            }
            string sClassroomLocation = "";
            string sClassroomPopupDownloads = "";

            InitiateActivities();


            if (ExamSession.Count > 0)
            {
                hEnroll.TranslationKey = "button-enroll-exam";
            }
            if (ELearnSession.Count > 0 && ELearnSession[0].IsMockExam)
            {
                lLessonsHeading.UpdateTranslationKey("test-exams-heading");
                //mockexam-not-available
            }
            if (ActivitySet.Activities.Count == 0)
            {
                Session["alert.once"] = translation.translate(this.Language, "", this, "alert-course-not-found");
                Response.Redirect("index.aspx?user_id=" + this.QueryStringInt("user_id"));
            }

            if (!this.QueryString("frame").Bln())
            {
                Response.Clear();
                Server.Transfer("navigator_frame.aspx" + Request.Url.Query, true);
                Response.End();
            }

            PRINCE2ExamDateSwitchEligible PRINCE2ExamDateSwitchEligible = new Navigator.PRINCE2ExamDateSwitchEligible(IsEnrolled, activeUser, ExamSession, ActivitySet.Activities, ActivitySet);

            HandlePRINCE2ExamDateSwitch(PRINCE2ExamDateSwitchEligible);

            if ((bFoundationExamConfirm.IsClicked && dFoundationExams.SelectedValue.Int() > 0) || (bPractitionerExamConfirm.IsClicked && dPractitionerExams.SelectedValue.Int() > 0))
            {
                Util.Classes.ActivitySet activitySetToUnenroll = (bFoundationExamConfirm.IsClicked ? PRINCE2ExamDateSwitchEligible.FoundationExamActivityset : PRINCE2ExamDateSwitchEligible.PractitionerExamActivityset);
                Util.Phoenix.session.RequestResult result = null;
                if (activitySetToUnenroll != null)
                    result = activeUser.PhoenixSession.PhoenixPut(String.Format(Util.Phoenix.session.Queries.ActivitySetUnenroll, activitySetToUnenroll.Id, activeUser.ID));

                if (result == null || !result.Error)
                    result = activeUser.PhoenixSession.PhoenixPut("ActivitySets/" + (bFoundationExamConfirm.IsClicked ? dFoundationExams.SelectedValue.Int() : dPractitionerExams.SelectedValue.Int()) + "/enroll?userId=" + activeUser.ID);

                if (result.Error)
                {
                    Session["alert.once"] = translation.translate(this.Language, "", this, "alert-unenroll-error");
                }
                else
                {
                    activeUser.PhoenixSession.PhoenixClearCache();
                    activeUser = Util.Classes.user.getUser(activeUser.ID, true);
                    Session["alert.once"] = translation.translate(this.Language, "", this, "alert-exam-switch-ok");
                }
                Response.Redirect(jlib.net.HTTP.stripQueryStringParameters(Request.Url.PathAndQuery, "action"));
            }

            if (Request["action"] == "unenroll" && ActivitySet.CanUnenroll && ExamSession[0].Participation.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Enrolled)
            {
                Util.Phoenix.session.RequestResult result = activeUser.PhoenixSession.PhoenixPut(String.Format(Util.Phoenix.session.Queries.ActivitySetUnenroll, ActivitySet.Id, activeUser.ID));
                if (result.Error)
                {
                    Session["alert.once"] = translation.translate(this.Language, "", this, "alert-unenroll-error");
                }
                else
                {
                    activeUser.PhoenixSession.PhoenixClearCache();
                    activeUser = Util.Classes.user.getUser(activeUser.ID, true);
                    Session["alert.once"] = translation.translate(this.Language, "", this, "alert-unenroll-ok");
                }
                Response.Redirect(jlib.net.HTTP.stripQueryStringParameters(Request.Url.PathAndQuery, "action"));
            }

            Util.Classes.iObjectWithLessons eLearnCourse = (ELearnSession.Count > 0 ? ELearnSession[0].ELearnCourseObject : null);
            List<Util.Classes.iLesson> lessons = (eLearnCourse == null || ELearnSession[0].IsImportedHistory ? new List<Util.Classes.iLesson>() : eLearnCourse.Lessons);
            
            pEnrolledProgress.Visible = IsEnrolled;
            pEnrollmentContainer.Visible = !IsEnrolled &&
                (
                (ExamSession.Count > 0 && activeUser.AllowExamEnroll)
                || (ExamSession.Count == 0 && activeUser.AllowCourseEnroll)
                )
                && (ELearnSession.Count == 0 || !ELearnSession[0].IsImportedHistory); //hide if imported history


            if (!IsEnrolled)
            {

                (Page.Master as Inc.Master.MyMetier).Section = Inc.Master.MyMetier.SiteSection.Catalog;
                pELearnLessonList.Visible = false;
                pCourseInfoContainer.Visible = true;

                foreach (Util.Classes.Activity oRow in ActivitySet.Activities)
                {
                    if (oRow.ArticleType == Util.Data.MetierClassTypes.Accommodation)
                        lEnrollmentPriceAccommodationDisclaimer.Visible = true;
                }
                if (m_oOrganization.IsShowPricesToUserOnPortal && ActivitySet.Price > 0)
                    lEnrollmentPrice.Text = String.Format(lEnrollmentPrice.Text, ActivitySet.Price + " " + ActivitySet.Activities[0].Currency);
                else
                {
                    lEnrollmentPrice.Visible = false;
                    lEnrollmentPriceAccommodationDisclaimer.Visible = false;
                }
                if (ExamSession.Count == 0)
                {
                    lCourseDescription.Text = "<div class='course-description'>" + ActivitySet.Activities[0].GetDescription() + "</div>";
                    lCourseInfo.Text = ActivitySet.Activities[0].GetCourseInfo();
                }
            }

            var set = new Util.Classes.EnrollmentDisplaySet(ActivitySet).Populate(activeUser, this);
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "var __Courses=" + jlib.functions.json.DynamicJson.Serialize(
                new
                {
                    Current = set,
                    AllowEnroll = pEnrollmentContainer.Visible,
                    Price = (lEnrollmentPrice.Visible ? lEnrollmentPrice.Text : ""),
                    EnrollmentPriceAccommodationDisclaimer = (lEnrollmentPriceAccommodationDisclaimer.Visible ? lEnrollmentPriceAccommodationDisclaimer.Text : "")
                }
            ) + ";", true);
            pUenrolledDetails.Visible = !IsEnrolled;

            bool webinarFlag = false;
            List<AttackData.Folder> pages = new List<AttackData.Folder>();
            foreach (var activity in ActivitySet.Activities)
            {
                if (AttackData.Settings.GetByField(ObjectID: activity.Id, TableName: "Activities", KeyName: "Type", Value: "webinar").Execute().FirstOrDefault() != null) webinarFlag = true;
            }
            foreach (var activity in ActivitySet.Activities)
            {
                if ((activity.ActivityStart == null || activity.ActivityStart.Value < DateTime.Now))
                {
                    AttackData.RowLinks.GetByField(Table1: "Activities", Table2: "d_folders", ID1: activity.Id).Execute().ForEach(link =>
                    {
                        if (link.Object2 as AttackData.Folder != null && link.Object2.GetParents<AttackData.Folder>().SafeGetValue(0).ObjectID == (IsEnrolled ? 119598 : 119599))
                        {
                            pages.Add(link.Object2 as AttackData.Folder);
                        }
                    });
                }
            }

            AttackData.RowLinks.GetByField(Table1: "ActivitySet", Table2: "d_folders", ID1: ActivitySet.Id).Execute().ForEach(link =>
            {
                if (link.Object2 as AttackData.Folder != null && link.Object2.GetParents<AttackData.Folder>().SafeGetValue(0).ObjectID == (IsEnrolled ? 119598 : 119599))
                {
                    pages.Add(link.Object2 as AttackData.Folder);
                }
            });
            foreach (var page in pages)
            {
                foreach (var content in page.GetChildren<AttackData.Content>())
                {
                    lCMSDescriptions.Text += "<div class='cms-content'>" + content.Xml + "</div>";
                }
            }

            if (webinarFlag)
            {
                lClassroomHeading.Text = "Webinar";
                lClassroomDetails.Text = "Show webinar details";
            }

            lCourseTitle.setValue(convert.cStr(DisplayAsILM || !IsEnrolled ? ActivitySet.Name : "", ActivitySet.Activities[0].Name));

            List<Util.Classes.Participant> participants = null;

            if (m_oOrganization.CompetitionMode != 2)
            {
                participants = new List<Util.Classes.Participant>();
                dynamic list = activeUser.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.UserPerformancesGetByActivity, ActivitySet.PrimaryActivity.Id), true, true).DataObject;
                if (list != null) foreach (dynamic participant in list) participants.Add(new Util.Classes.Participant(participant));
                if (m_oOrganization.CompetitionMode == 1) participants.Shuffle();
            }

            pClassroomContainer.Visible = ClassroomSession.Count > 0 && IsEnrolled;
            if (IsEnrolled) hClassroomLink.CssClass += " active";

            pECoach.Visible = ECoach != null && IsEnrolled;
            if (ECoach != null)
            {
                lECoachName.setValue(ECoach.Name);
                lECoachCourse.Text = String.Format(lECoachCourse.Text, lCourseTitle.Text);
                lECoachEmail.NavigateUrl = "mailto:" + convert.cStr(ECoach.ContactEmail, "kurssporsmal@metier.no");
                lECoachPhone.setValue(ECoach.ContactPhone);
                lECoachPhone.Visible = lECoachPhone.Text != "";
                iECoachImage.ImageUrl = jlib.helpers.general.getThumbnailUrl(convert.cStr(ECoach.Image, "gfx/avatar.jpg"), (int)iECoachImage.Width.Value, (int)iECoachImage.Height.Value, true, jlib.helpers.general.FileStatus.None);
                iECoachImage.AlternateText = lECoachName.Text;
            }

            int iNumPointsPossible = 0, iNumPointsScored = 0;
            bool ElearningNotAvailable = false;
            //Is Elearning Expired
            if (ELearnSession.Count > 0 && (ELearnSession[0].IsImportedHistory || (ELearnSession[0].Participation != null && !ELearnSession[0].Participation.IsActive)))
            {
                ElearningNotAvailable = true;
                lessons.ForEach(lesson => lesson.StartingUrl = "");
            }

            var playerSettingsString = convert.cStr(eLearnCourse == null ? "" : AttackData.Settings.GetByField(ObjectID: eLearnCourse.Id, TableName: "d_folders", KeyName: "PlayerSettings").Execute().FirstOrDefault().SafeGet().Value, "{}");
            dynamic PlayerSettings = jlib.functions.json.DynamicJson.Parse(playerSettingsString);
            int playerVersion = convert.cInt(convert.SafeGetProperty(PlayerSettings, "Player", "Id"));
            if (playerVersion == 2 && lessons != null && !ELearnSession[0].IsMockExam)
                lessons.ForEach(x => { if (x.StartingUrl.IsNotNullOrEmpty()) x.StartingUrl = "./portal2/#/course/" + eLearnCourse.Id + "/lesson/" + x.Id; });

            dynamic JSON = Util.MyMetier.GetLessonData(lessons, (eLearnCourse == null ? 0 : eLearnCourse.Id), this.Language);
            JSON.PlayerSettings = playerSettingsString;
            
            
            //If imported history
            if (ELearnSession.Count > 0 && ELearnSession[0].IsImportedHistory)
            {
                lElearnExpiredContainer.Visible = true;
                lElearnExpired.Text = translation.translate(this.Language, "", this, "course-archived");
                pELearnLessonList.Visible = false;
                lPercentageTaken.Visible = true;
                pFinalTestContainer.Visible = false;
                JSON.DiplomaUrl = "diploma.aspx?classroom_id=" + ELearnSession[0].Id;                
                if (!IsEnrolled) pDiplomaContainer.Visible = false;
            }
            else if (lessons.Count > 0)
            {
                lFinalTestUnlock.Text = String.Format(lFinalTestUnlock.Text, eLearnCourse.UnlockFinalPoints);
                if (convert.cBool(convert.SafeGetProperty(PlayerSettings, "FinalTest", "Unlock", "AllLessonsCompleted")))
                    lFinalTestUnlock.Text += " " + translation.translate(this.Language, "", this, "finaltest-lessons-completed");

                Util.Classes.iLesson finalTestLesson = null;
                bool bCompletedFinalTest = false;

                if (ELearnSession[0].Participation != null && !ELearnSession[0].Participation.IsActive)
                {
                    if (IsEnrolled)
                    {
                        lElearnExpiredContainer.Visible = true;
                        if (ELearnSession[0].Participation.ParticipantAvailability.CalculatedAvailabilityEnd.DteOffset() < DateTime.Now)
                            lElearnExpired.Text = parse.replaceAll(String.Format(translation.translate(this.Language, "", this, "elearning-expired"), convert.cStr(lECoachEmail.NavigateUrl, "kurssporsmal@metier.no"), lECoachName.Text), "  ", " ");
                        else
                        {
                            if (webinarFlag)
                                lElearnExpired.UpdateTranslationKey("webinar-not-available");
                            else if (ELearnSession[0].IsMockExam)
                                lElearnExpired.UpdateTranslationKey("mockexam-not-available");
                            else
                                lElearnExpired.UpdateTranslationKey("elearning-not-available");
                            lElearnExpired.Text = String.Format(Util.MyMetier.getUserCulture(this.Language).DateTimeFormat, lElearnExpired.Text, ELearnSession[0].Participation.ParticipantAvailability.CalculatedAvailabilityStart.DteOffset());
                        }
                    }
                }

                for (int x = 0; x < lessons.Count; x++)
                {
                    if (lessons[x].NumSubLessons == 0)
                    {
                        iNumPointsPossible += (100 * lessons[x].Weight);
                        iNumPointsScored += Math.Max(0, lessons[x].UserNumPoints * lessons[x].Weight);
                    }
                    if (lessons[x].IsFinalTest)
                    {
                        finalTestLesson = lessons[x];                        
                        if (lessons[x].UserLessonStatus == "C") bCompletedFinalTest = true;

                        if (!ElearningNotAvailable)
                        {
                            JSON.FinalTestUrl= "fnMetierPlayRCO(" + ELearnSession[0].Id + "," + lessons[x].Id + ",'" + lessons[x].StartingUrl + "',null);return false;";                            
                        }

                    }
                    else
                    {
                        if (!ELearnSession[0].IsEnrolled)
                        {
                            Control oControl = jlib.helpers.control.cloneObject(pUnenrolledLessonTemplate) as Control;
                            List<jlib.components.iControl> oControls = jlib.helpers.control.getDynamicControls(oControl, null);
                            jlib.helpers.control.populateValue(oControls, "name", lessons[x].Name);
                            lUnenrolledLessons.Controls.Add(oControl);
                        }
                    }
                }
                JSON.UnlockTest = eLearnCourse.UnlockFinalPoints;
                JSON.GoldPoints = eLearnCourse.GoldPoints;
                JSON.SilverPoints = eLearnCourse.SilverPoints;
                JSON.ActivityId = ELearnSession[0].Id;
                JSON.IsMockExam = ELearnSession[0].IsMockExam;
                JSON.NumIncompleteLessons = lessons.Where(lesson => lesson.UserLessonStatus != "C" && lesson.Weight > 0 && !lesson.IsFinalTest && lesson.StartingUrl.IsNotNullOrEmpty()).Count();
                JSON.NumUnstartedLessons = lessons.Where(lesson => lesson.UserLessonStatus == "N" && lesson.Weight > 0 && !lesson.IsFinalTest && lesson.StartingUrl.IsNotNullOrEmpty()).Count();

                if (finalTestLesson == null || !IsEnrolled)
                {
                    pFinalTestContainer.Visible = false;
                }
                else
                {
                    if (!IsEnrolled 
                        || (!bCompletedFinalTest && eLearnCourse.UnlockFinalPoints > iNumPointsScored)
                        || (convert.cBool(convert.SafeGetProperty(PlayerSettings, "FinalTest", "Unlock", "AllLessonsCompleted")) && JSON.NumIncompleteLessons > 0 )
                        )
                    {
                        JSON.FinalTestUrl = "";                        
                    }
                    else if (bCompletedFinalTest)
                    {
                        JSON.FinalTestClass = "completed";
                        lFinalTestUnlock.Text = String.Format(translation.translate(this.Language, "", this, "finaltest-completed"), finalTestLesson.UserNumPoints);
                    }
                    else
                    {
                        JSON.FinalTestClass = "active";                        
                    }
                }

                if (ELearnSession[0].RcoCourseId == 0 || !IsEnrolled)
                {
                    pDiplomaContainer.Visible = false;
                }
                else
                {
                    int finalTestMinScore = convert.cInt(convert.SafeGetProperty(PlayerSettings, "Diploma", "FinalTestMinScore")),
                        finalTestScore = (finalTestLesson == null ? 0 : finalTestLesson.UserNumPoints);

                    if ((bCompletedFinalTest || finalTestLesson == null)
                        && (finalTestMinScore == 0 || finalTestMinScore <=  finalTestScore)
                        && convert.cInt(JSON.NumUnstartedLessons)==0)
                    {                        
                        JSON.DiplomaUrl = "diploma.aspx?classroom_id=" + ELearnSession[0].Id;                        
                    }
                }

                if (Util.MyMetier.IsPrince2SpecialHandling(ELearnSession[0].VersionNumber) || Util.MyMetier.IsPrince2SpecialHandling(ELearnSession[0].ArticleNumber))
                {
                    pProgramPanelContainer.Visible = false;
                    lLessonsHeading.UpdateTranslationKey("test-exams-heading");
                }
                else
                {
                    JSON.ParticipantCount = (participants == null ? 0 : participants.Count);
                    JSON.Rank = (participants == null || m_oOrganization.CompetitionMode != 0 ? "" : (IsEnrolled && participants != null ? (participants.IndexOf(participants.FirstOrDefault(participant => participant.Id == activeUser.ID)) + 1).Str() : "N/A"));
                }
            }
            else
            {
                pDiplomaContainer.Visible = false;
                pFinalTestContainer.Visible = false;
                if (IsEnrolled) pElearnProgress.Visible = false;
                pELearnLessonList.Visible = false;
            }

            pUnenrolledLessonTemplate.Controls.Clear();

            pParticipantContainer.Visible = participants != null && (participants.Count > 0);
            if (pParticipantContainer.Visible) PopulateParticipants(participants, iNumPointsPossible);

            pTop3Template.Controls.Clear();
            pBottom7Template.Controls.Clear();

            if (IsEnrolled)
            {
                Util.Classes.Attachment caseAttachment = null;
                List<Util.Classes.Attachment> attachments = new List<Util.Classes.Attachment>();
                ActivitySet.Activities.ForEach(activity =>
                {
                    attachments = attachments.Concat(activity.Attachments).ToList();
                    if (caseAttachment == null) caseAttachment = activity.CaseAttachment;
                });

                if (caseAttachment != null && ExamSession.Count > 0)
                {
                    pCaseExamContainer.Visible = true;
                    if (ExamSession[0].Participation.IsActive)
                    {
                        Control file = BuildAttachmentDetails(caseAttachment);
                        lCaseExamDownloads.Controls.Add(file);
                        if (ExamSession[0].Participation.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Enrolled)
                        {
                            hyperlink link = (jlib.helpers.control.findControlsByDataMember(jlib.helpers.control.getDynamicControls(file, null), "link")[0] as hyperlink);
                            link.NavigateUrl += "&Log=true&ParticipantId=" + ExamSession[0].Participation.Id;
                        }
                    }
                    else
                        lCaseExamDownloads.Text = String.Format(translation.translate(this.Language, "", this, "exam-availability"),
                        ExamSession[0].Participation.ParticipantAvailability.CalculatedAvailabilityStart.DteOffset().ToString(Util.MyMetier.getUserCulture(this.Language).DateTimeFormat.ShortTimePattern),
                            ExamSession[0].Participation.ParticipantAvailability.CalculatedAvailabilityStart.DteOffset().ToString(Util.MyMetier.getUserCulture(this.Language).DateTimeFormat.ShortDatePattern),
                            ExamSession[0].Participation.ParticipantAvailability.CalculatedAvailabilityEnd.DteOffset().ToString(Util.MyMetier.getUserCulture(this.Language).DateTimeFormat.ShortTimePattern),
                            ExamSession[0].Participation.ParticipantAvailability.CalculatedAvailabilityEnd.DteOffset().ToString(Util.MyMetier.getUserCulture(this.Language).DateTimeFormat.ShortDatePattern));
                }

                if (attachments.Count > 0)
                {
                    pClassroomDownloads.Visible = true;
                    lDownloadFilesHeading.Text = translation.translate(this.Language, "", this, ExamSession.Count == 0 ? "course-material" : "exam-material");

                    //if (ExamSession.Count > 0 && !ExamSession[0].Participation.IsActive) {
                    //    lClassroomDownloads.Text = String.Format(translation.translate(this.Language, "", this, "exam-availability"),
                    //        ExamSession[0].ActivityStart.DteOffset().ToString(Util.MyMetier.getUserCulture(this.Language).DateTimeFormat.ShortDatePattern),
                    //        ExamSession[0].ActivityStart.DteOffset().ToString(Util.MyMetier.getUserCulture(this.Language).DateTimeFormat.ShortTimePattern));                                                
                    //} else {

                    for (int x = 0; x < attachments.Count; x++)
                    {
                        lClassroomDownloads.Controls.Add(BuildAttachmentDetails(attachments[x]));
                        sClassroomPopupDownloads += "<li><a href=\"" + attachments[x].Url + "\" target=\"_blank\">" + attachments[x].Title + "</a></li>";
                    }
                    //}                
                }
            }


            if (Instructor != null && IsEnrolled)
            {
                pInstructor.Visible = true;
                lInstructorName.setValue(Instructor.Name);
                lInstructorCourse.Text = String.Format(lInstructorCourse.Text, lCourseTitle.Text);
                //lInstructorEmail.NavigateUrl = "mailto:" + convert.cStr(oClassroomBookings.Rows[x]["EMAIL"], oClassroomBookings.Rows[x]["RESOURCE_ADDRESS"], "kurssporsmal@metier.no");
                //lInstructorPhone.setValue(oClassroomBookings.Rows[x]["PHONE_NUMBER"]);
                //lInstructorPhone.Visible = lInstructorPhone.Text != "";
                iInstructorImage.ImageUrl = jlib.helpers.general.getThumbnailUrl(convert.cStr(Instructor.Image, "gfx/avatar.jpg"), (int)iInstructorImage.Width.Value, (int)iInstructorImage.Height.Value, true, jlib.helpers.general.FileStatus.None);
                iInstructorImage.AlternateText = lInstructorName.Text;
            }
            if (StudyAdvisor != null)
            {
                pStudyAdvisor.Visible = true;
                lStudyAdvisorName.setValue(StudyAdvisor.Name);
                lStudyAdvisorCourse.Text = String.Format(lStudyAdvisorCourse.Text, lCourseTitle.Text);
                lStudyAdvisorEmail.NavigateUrl = "mailto:" + StudyAdvisor.ContactEmail;
                //lStudyAdvisorPhone.setValue(oClassroomBookings.Rows[x]["PHONE_NUMBER"]);
                //lStudyAdvisorPhone.Visible = lStudyAdvisorPhone.Text != "";
                iStudyAdvisorImage.ImageUrl = jlib.helpers.general.getThumbnailUrl(convert.cStr(StudyAdvisor.Image, "gfx/avatar.jpg"), (int)iStudyAdvisorImage.Width.Value, (int)iStudyAdvisorImage.Height.Value, true, jlib.helpers.general.FileStatus.None);
                iStudyAdvisorImage.AlternateText = lStudyAdvisorName.Text;
            }
            sClassroomLocation = (Location != null ? Location.Name : (ExamSession.Count > 0 && ExamSession[0].IsMPCExam ? "Online" : ""));
            if (!sClassroomLocation.IsNullOrEmpty())
            {
                lCourseDescription.Text += "<li style=\"padding-top:10px\"><b>" + translation.translate(this.Language, "", this, "classroom-location") + ":</b> " + sClassroomLocation + "</li>";
            }

            JSON.DistributorEmail = activeUser.DistributorEmail;
            JSON.EcoachEmail = (ECoach == null ? "" : ECoach.ContactEmail);
            JSON.UserEmail = activeUser.Email;

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "data", "window.parent.fnSetLessonData(" + JSON.ToString() + ");", true);

            Util.Classes.Activity activityWithDate = (ClassroomSession.Count > 0 ? ClassroomSession[0] : ExamSession.SafeGetValue(0));
            if (activityWithDate != null)
            {
                lClassroomMonth.setValue(Util.MyMetier.getUserCulture(this.Language).DateTimeFormat.MonthNames[(int)activityWithDate.ActivityStart.DteOffset().Month - 1].Substring(0, 3));
                lClassroomDay.setValue(activityWithDate.ActivityStart.DteOffset().Day);
                lClassroomSmall.setValue(
                    Util.MyMetier.MetierDate(this.Language, activityWithDate.ActivityStart) + " " + translation.translate(this.Language, "", this, "classroom-popup-clock") + String.Format(" {0:HH:mm}", activityWithDate.ActivityStart)
                    );

                string times = (ExamSession.Count > 0 ? translation.translate(this.Language, "", this, "exam-popup-when") : translation.translate(this.Language, "", this, "classroom-popup-when"));
                
                //Only display time if it has a valid label
                if (times.IsNotNullOrEmpty())
                {
                    times = "<td valign='top'><b>" + times + ":&nbsp;</b></td><td>";
                    foreach (var period in activityWithDate.ActivityPeriods)
                    {
                        times += String.Format(Util.MyMetier.MetierDate(this.Language, period.Start) + " " + translation.translate(this.Language, "", this, "classroom-popup-clock") + " {0:HH:mm}", period.Start);
                        if (ExamSession.Count == 0 || !(ExamSession[0].IsCaseExam || ExamSession[0].IsProjectAssignment))
                        {
                            times += String.Format(" - " + (period.Start != null && period.End != null && period.Start.Value.Date != period.End.Value.Date && period.End > period.Start ? Util.MyMetier.MetierDate(this.Language, period.End) + " " + translation.translate(this.Language, "", this, "classroom-popup-clock") + " " : "") + " {0:HH:mm}<br>", period.End);
                        }
                    }
                    if (activityWithDate.ActivityPeriods.Count > 0) lCourseDescription.Text += "<li style=\"padding-top:10px\"><table><tr>" + times + "</tr></table></li>";
                }
                if (ExamSession.Count > 0 && (ExamSession[0].IsCaseExam || ExamSession[0].IsProjectAssignment) && ExamSession[0].CaseExamEndDate != null)
                {
                    lCourseDescription.Text += "<li style=\"padding-top:10px\"><b>" + translation.translate(this.Language, "", this, "case-exam-deadline") + ":&nbsp;</b>" + Util.MyMetier.MetierDate(this.Language, ExamSession[0].CaseExamEndDate.Value) + " " + translation.translate(this.Language, "", this, "classroom-popup-clock") + String.Format(" {0:HH:mm}", ExamSession[0].CaseExamEndDate.Value) + "</li>";
                }

                hClassroomLink.NavigateUrl = "javascript:void(0)";
                hClassroomPopupContent.Value = "<h1>" + lCourseTitle.Text + "</h1>" + translation.translate(this.Language, "", this, "classroom-practical-info") + "<ul>" + convert.cIfValue(sClassroomLocation, "<li>" + translation.translate(this.Language, "", this, "classroom-popup-where") + ": ", "</li>", "") + "<li>" + times + "</li></ul>" + (sClassroomPopupDownloads == "" ? "" : "<br /><b>" + translation.translate(this.Language, "", this, "course-material") + "</b><ul>" + sClassroomPopupDownloads + "</ul>") + "<div style=\"width:50px;text-align:center;padding-top:30px\"><a style=\"font-size:10px;\" href=\"navigator.aspx?ical=true&ilm_id=" + this.QueryString("ilm_id") + "&activity_ids=" + this.QueryString("activity_ids") + "\"><img src=\"./inc/images/icons/add-to-calendar-large.png\"><br />" + translation.translate(this.Language, "", this, "classroom-add-to-calendar") + "</a></div>";

                hClassroomLink.OnClientClick = "$('#class-info').detach();$('<div id=\"class-info\"></div>').appendTo(document.body).html($('#" + hClassroomPopupContent.ClientID + "').val()).dialog({ modal: true, height: 400, width: 600, position: ['center', 'center'] }).dialog('open').dialog('option', 'title', '" + lClassroomHeading.Text + "');";
            }

            if (lCourseDescription.Text != "") lCourseDescription.Text = "<ul>" + lCourseDescription.Text + "</ul>";

            pProgramPanelContainer.Visible = IsEnrolled && (pProgramPanelContainer.Visible && (ELearnSession.Count > 0 && (lessons.Count > 0)) || ClassroomSession.Count > 0 || (ExamSession.Count > 0 && !IsEnrolled));
            lCourseDescription.Visible = IsEnrolled;

            string examUnenrollmentButton = "";
            if (ExamSession.Count > 0) examUnenrollmentButton += UpdateExamInfo(PRINCE2ExamDateSwitchEligible.IsFoundationEligble || PRINCE2ExamDateSwitchEligible.IsPractitionerEligble);

            //block for displaying PRINCE2 exam switcher for arbitrary customers
            UpdatePRINCE2ExamDateSwitchUI(PRINCE2ExamDateSwitchEligible);

            Util.Classes.Activity oClassroomImage = convert.cValue(ELearnSession.SafeGetValue(0), ActivitySet.Activities[0]);
            iCourseImage.ImageUrl = "./inc/library/exec/thumbnail.aspx?u=" + System.Web.HttpUtility.UrlEncode(Util.MyMetier.getCourseImage(activeUser.Organization == null ? 0 : convert.cInt(activeUser.Organization.ParentCompanyID, activeUser.Organization.ID), oClassroomImage)) + "&effect=mirror";

            if (convert.cBool(this.QueryString("ical")))
            {
                Response.Clear();
                Response.ContentType = "text/calendar";
                Response.AppendHeader("Content-Disposition", "attachment; filename=Event.ics");

                //Response.Write(jlib.helpers.net.getICalEvent(Util.Phoenix.Helpers.LocalizeDate(ClassroomSession[0].ActivityStart, ClassroomSession[0].MomentTimezoneName).DteOffset(), Util.Phoenix.Helpers.LocalizeDate(ClassroomSession[0].ActivityEnd, ClassroomSession[0].MomentTimezoneName).DteOffset(),  parse.stripHTML(ClassroomSession[0].GetDescription(), true), sClassroomLocation, "myMetier.net: " + lCourseTitle.Text, convert.cStr(lECoachName.Text, lInstructorName.Text), parse.replaceAll(lECoachEmail.NavigateUrl,"mailto:",""), "https://mymetier.net", "Course",""));
                Response.Write(jlib.helpers.net.getICalEvent(ClassroomSession[0].ActivityStart.DteOffset(), ClassroomSession[0].ActivityEnd.DteOffset(), parse.stripHTML(ClassroomSession[0].GetDescription(), true), sClassroomLocation, "myMetier.net: " + lCourseTitle.Text, convert.cStr(lECoachName.Text, lInstructorName.Text), parse.replaceAll(lECoachEmail.NavigateUrl, "mailto:", ""), "https://mymetier.net", "Course", ""));
                Response.End();
            }

            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "popup-title", "var sDialogTitle=\"" + parse.replaceAll(translation.translate(this.Language, "", "/enroll.aspx", Page, "details-heading"), "\"", "'") + "\";", true);
            if (ActivitySet.Activities.Count > 0)
            {
                hEnroll.OnClientClick = "fnEnroll(" + ActivitySet.Id + ",0);";
            }
            else
            {
                hEnroll.Visible = false;
            }

            //if user has not completed Competency requirement and this is an exam, display message and hide exam playback
            if (ExamSession.Count > 0 && ExamSession[0].Enrollment != null && ExamSession[0].Enrollment.IsUserCompetenceInfoRequired && activeUser.NagUserForExamCompetence)
            {
                pCaseExamContainer.Visible = false;
                pClassroomDownloads.Visible = false;
                pFileUploadContainer.Visible = false;
                lCourseDescription.Text = "<div class='exam-competency-missing'>" + translation.translate(this.Language, "", this, "exam-competency-missing") + " <a href='settings.aspx#competency'>" + translation.translate(this.Language, "", this, "exam-competency-missing-link") + "</a></div>"
                    + "<br /><br />" + examUnenrollmentButton;
            }
        }
        private Control BuildAttachmentDetails(Util.Classes.Attachment attachment)
        {
            Control oControl = jlib.helpers.control.cloneObject(pClassroomDownloadTemplate) as Control;
            List<jlib.components.iControl> oControls = jlib.helpers.control.getDynamicControls(oControl, null);
            jlib.helpers.control.populateValue(oControls, "title", convert.cStr(attachment.Title, "&nbsp;"));
            jlib.helpers.control.populateValue(oControls, "details", convert.cStr(attachment.Description, "&nbsp;"));
            hyperlink oLink = (jlib.helpers.control.findControlsByDataMember(oControls, "link")[0] as hyperlink);
            oLink.NavigateUrl = attachment.Url;
            return oControl;
        }
        public void Page_PreRender(object sender, EventArgs e)
        {
            lScript.Text += "var iUserID=" + activeUser.ID + ";\n";
            pClassroomDownloadTemplate.Controls.Clear();
            Page.Title += " - " + lCourseTitle.Text;
        }
    }
}