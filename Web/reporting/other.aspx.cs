﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.controls.jgrid;
using jlib.functions;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Reporting
{
    public partial class Other : jlib.components.webpage
    {        
        public void Page_Init(object sender, EventArgs e)
        {
            pExamPlayerOptions.Visible = dReportTemplate.SelectedValue == "exam-player";
            if (pExamPlayerOptions.Visible)
            {
                dExamPlayerExam.DataSource = AttackData.Folders.GetByField(Context_Value: "exam").OrderBy(AttackData.Folders.Columns.Name).Execute();
                dExamPlayerExam.DataBind();
                dExamPlayerExam.Items.Insert(0, new ListItem("(Please select)", ""));
            }
        }

        public void Page_Load(object sender, EventArgs e)
        {
            if (pExamPlayerOptions.Visible && dExamPlayerExam.SelectedValue != "")
            {

                AttackData.Releases Releases = AttackData.Releases.GetByField(ObjectID: dExamPlayerExam.SelectedValue.Int()).Execute();
                string sFileName = Util.Export.exportExamResponses(Releases, Session["user.id"].Int());
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment; filename=\"Exam%20Results.xlsx\"");
                Response.TransmitFile(sFileName);
                Response.End();
            }
            else if(dReportTemplate.SelectedValue == "peice-user-progress")
            {
                //string sFileName = Util.Export.exportExamResponses(Releases, Session["user.id"].Int());
                var ado = new ado_helper("sql.dsn.cms");
                var sql = @"select * from v_PEICE_User_Progress";
                var table = sqlbuilder.getDataTable(ado, sql);

                SpreadsheetGear.IWorkbook WorkbookFile = SpreadsheetGear.Factory.GetWorkbook();
                int SheetNumber = 0, RowCounter = 0;
                WorkbookFile.Worksheets[SheetNumber].Name = "Participants";
                var oValues = (SpreadsheetGear.Advanced.Cells.IValues)WorkbookFile.Worksheets[SheetNumber];
                
                for (int y = 0; y < table.Columns.Count; y++)
                {
                    WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, y].Font.Size = 12;
                    WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, y].Interior.Color = System.Drawing.Color.FromArgb(91, 0, 0);
                    WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, y].Font.Color = System.Drawing.Color.White;
                    WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, y].Font.Bold = true;
                    oValues.SetText(RowCounter, y, table.Columns[y].Caption);
                }
                RowCounter++;
                foreach(DataRow row in table.Rows)                 
                {
                    for (int y = 0; y < table.Columns.Count; y++)
                    {
                        if(convert.isNumeric(row[y]))
                            oValues.SetNumber(RowCounter, y, row[y].Dbl());
                        else
                            oValues.SetText(RowCounter, y, row[y].Str());                        
                    }                    
                    RowCounter++;
                }

                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment; filename=\"PSO%20Results.xlsx\"");
                WorkbookFile.SaveToStream(Response.OutputStream, SpreadsheetGear.FileFormat.OpenXMLWorkbook);
                Response.End();
            }
        }


    }
}