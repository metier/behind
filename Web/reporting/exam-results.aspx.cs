﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.controls.jgrid;
using jlib.functions;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Reporting
{
    public partial class ExamResults : jlib.components.webpage
    {
        
        private Util.Classes.user m_oActiveUser = null;
        public Util.Classes.user ActiveUser
        {
            get
            {
                if (m_oActiveUser == null) m_oActiveUser = (Page.Master as Inc.Master.Default).ActiveUser;
                return m_oActiveUser;
            }
        }
        public void Page_PreInit(object sender, EventArgs e)
        {

        }
        public void Page_Init(object sender, EventArgs e)
        {
            gGrid.GridCustomData += new OnGridCustomDataHandler(gGrid_GridCustomData);
            gGrid.GridViewsLoad += new OnGridViewsLoadHandler(gGrid_GridViewsLoad);
            gGrid.GridViewsSave += new OnGridViewsSaveHandler(gGrid_GridViewsSave);
            gGrid.QuerySource = AttackData.ExamResultImports.GetAll();
            gGrid.GridRowPrerender += new OnGridRowPrerenderHandler(gGrid_GridRowPrerender);
        }

        public void Page_Load(object sender, EventArgs e)
        {

        }

        void gGrid_GridViewsSave(jGrid oGrid)
        {
            Util.Permissions.setSetting("grid.view", "exam-results", ActiveUser.ID, oGrid.Views.serialize());
        }

        void gGrid_GridViewsLoad(jGrid oGrid)
        {
            gGrid.Views.parse(Util.Permissions.getSetting("grid.view", "exam-results", ActiveUser.ID, ""), true);
        }

        void gGrid_GridCustomData(object sender, GridCellEventArgs e)
        {
            if (e.Type == "export")
            {
                gGrid.DataSource = null;
                //gGrid_GridBeforeDatabind("excel", null);
                string sFileName = io.getUniqueFileName(System.Configuration.ConfigurationManager.AppSettings["temp.path"] + "\\Enrollments-" + Session["user.id"] + "-" + String.Format("{0:MM}{0:dd}{0:yy}-{0:HH}{0:mm}{0:ss}", DateTime.Now) + ".xlsx");
                int iRowCounter = 0;
                gGrid.SaveAsExcel("", null, sFileName, "Exam Results Export", 0, ref iRowCounter, "Exam Results Export");
                gGrid.DataSource = null;
                e.ClientJSAfter = "$('#hDownloadContainer').show();$('#lDownload').attr('href','../download.aspx?file=" + System.Web.HttpUtility.UrlEncode(System.IO.Path.GetFileName(sFileName)) + "');";
            }
        }

        void gGrid_GridRowPrerender(object sender, GridCellEventArgs e)
        {
            GridColumn[] oCols = new GridColumn[] { cPostedDate, cStartDate, cEndDate };
            for (int x = 0; x < oCols.Length; x++)
                if (gGrid.Columns.Contains(oCols[x]) && e.getColValue(oCols[x]).Str() != "") e.setColValue(oCols[x], String.Format("{0:yyyy}-{0:MM}-{0:dd} {0:HH}:{0:mm}:{0:ss}", convert.cDate(e.getColValue(oCols[x]))));

            oCols = new GridColumn[] { cDOB };
            for (int x = 0; x < oCols.Length; x++)
                if (gGrid.Columns.Contains(oCols[x]) && e.getColValue(oCols[x]).Str() != "") e.setColValue(oCols[x], String.Format("{0:yyyy}-{0:MM}-{0:dd}", convert.cDate(e.getColValue(oCols[x]))));

            oCols = new GridColumn[] { };
            for (int x = 0; x < oCols.Length; x++)
                if (gGrid.Columns.Contains(oCols[x]) && e.getColValue(oCols[x]).Str() != "") e.setColValue(oCols[x], jlib.functions.format.cHHMMSS(convert.cInt(e.getColValue(oCols[x])), "{2:0#}:{1:0#}:{0:0#}"));
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

        }
    }
}