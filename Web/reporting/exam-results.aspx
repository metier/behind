﻿<%@ Page Title="Metier Exam Results Report" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Reporting.ExamResults" Codebehind="exam-results.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server">
<asp:PlaceHolder runat="server" ID="pMyMetierIncludes">
<link rel="stylesheet" media="screen" href="https://mymetier.net/learningportal/inc/library/controls/jgrid/media/jgrid.css?2" type="text/css" />

</asp:PlaceHolder>

<h2><img src="https://mymetier.net/learningportal/inc/images/icons/tools_32.png" alt="Exam Results Report" /><common:label runat="server" ID="lTitle">Exam Results Report</common:label></h2>
<div id="hSearchTips" style="display:none">
<b>To help you search more efficiently, we have compiled some search tips.</b><br />
<br /> 
 <ul class="list">
<li>When searching for a value, the system will match any part of the field (not just from the beginning), to attempt to locate a match. (So, no need to use * or %).</li>
<li>Looking for all rows where a certain field is blank? Use two apostrophes '' to search!</li>
<li>Looking for all rows where a certain field is NOT blank? Use greather-than less-than and two apostrophes (&gt;&lt;'') to search!</li>
<li>Looking for all items that are greater or smaller than a certain value, use &gt; or &lt; in your search. 
<ul class="list">
<li>Ie, all users receiving more than 1500 points on their elearning, use: <u>&gt;1500</u></li>
</ul>
</li>
</ul>
</div>

<div id="cStatus" style="visibility:hidden;"><img src='https://mymetier.net/learningportal/inc/images/icons/accept.gif'/>&nbsp;Refresh complete</div>

<br style="clear: both;" />

<div style="float:left;padding-right:30px"><img src="https://mymetier.net/learningportal/inc/images/icons/table_save.png" /> <a href="javascript:void(0)" onclick="oGrid.addRequest(oGrid.buildRequest('custom','Type','export'));">Export report to Excel</a></div>
<div style="float:right;"><img src="https://mymetier.net/learningportal/inc/images/icons/help.png" style="position:relative;top:3px" /> <a href="javascript:void(0);" onclick="$('#hSearchTips').dialog({ modal: true, buttons: { 'Close': function() { $(this).dialog('close'); } } }).dialog('option', 'title', 'Search Tips').dialog('option','width',600).dialog('option','position',['center', 'center']).dialog('open');">Search Tips</a></div>
<br style="clear: both;" />
<div style="border:2px solid red;padding:7px; background-color:#eee; margin:8px 0; display:none" id="hDownloadContainer">
Download your file from <a href="" id="lDownload" onclick="$('#hDownloadContainer').hide();">here</a>.
</div>
<br style="clear: both;" />

<table cellpadding="0" cellspacing="0" border="0" class="listing assets" style="margin-bottom:0;">
	<colgroup></colgroup>
	<colgroup span="3" style="font-weight: bold;"></colgroup>
	<thead>
		<tr class="actions">
			<td colspan="11">
				<table cellpadding="0" cellspacing="0">
					<thead>
						<tr>
                            <td class="select">Select: <a href="javascript:void(0);" onclick="oGrid.setSelection(1);">All</a>, <a href="javascript:void(0);" onclick="oGrid.setSelection(0);">None</a> &nbsp;|&nbsp;<span class="selection-count"><strong>0</strong> items selected</span></td>
							<td>                        
                                <span id="hGridView"></span>
                            </td>
							
							
							<td class="paging">
	                            Page
	                            <img src="https://mymetier.net/learningportal/inc/images/icons/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow grid_prev_page" />

	                            <input type="text" value="1" class="text page page_number" />

	                            <a href="javascript:void(0);" title="Next page" ><img src="https://mymetier.net/learningportal/inc/images/icons/pager_arrow_right.gif" alt="Go to Next page" class="arrow grid_next_page" /></a>

	                            of <span class="page_status">3 pages</span>            <span class="separator">|</span>
	                            View            <select name="limit" class="page_size">

	                                <option value="20" selected="selected">20</option>
	                                <option value="30">30</option>
	                                <option value="50">50</option>
	                                <option value="100">100</option>
	                                <option value="200">200</option>
	                            </select>
	                            per page
			                </td>
						</tr>
					</thead>
				</table>
			</td>
		</tr>
	</thead>
	</table>    
<jgrid:jGridData runat="server" id="gGrid" DisableInitialFiltering="true" DisableInitialSorting="true" LoadViewContainer="hGridView" DataSourceType="MSSQL" Pagination="true" DataViewFiltering="false" CaseInsensitiveFilter="true" BindDropdownByID="false" style="width:956px;" SortOrder="asc" JSName="oGrid" AutoFilter="true" PageSize="50" SortCol="1" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets' style='border-top:0;margin-bottom:0'" DeleteConfirmation="Are you sure you wish to delete this portal?" DisplayFilter="true" DisplayAdd="false">
    <Cols>        
        <jgrid:col id="cCheck" CAtt="style='width:10px'" runat="server" HText="" ColType="Select" />        
        <jgrid:col runat="server" id="cID" HText="ID" ColType="Hide" PrimaryKey="true" FormatString="#ID#" SortField="ID" FField="ID" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cUserID" HText="UserID" ColType="Hide" PrimaryKey="true" FormatString="UserID" SortField="UserID" FField="UserID" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cPostedDate" HText="Import Date" ColType="Text" PrimaryKey="false" FormatString="PostedDate" SortField="PostedDate" FField="PostedDate" FPartialMatch="true" FInputAtt="class='text daterangepicker'" />
        <jgrid:col runat="server" id="cNamePrefix" HText="Name Prefix" ColType="Text" PrimaryKey="false" FormatString="NamePrefix" SortField="NamePrefix" FField="NamePrefix" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cNameFirst" HText="First Name" ColType="Text" PrimaryKey="false" FormatString="NameFirst" SortField="NameFirst" FField="NameFirst" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cNameLast" HText="Last Name" ColType="Text" PrimaryKey="false" FormatString="NameLast" SortField="NameLast" FField="NameLast" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cDOB" HText="DOB" ColType="Text" PrimaryKey="false" FormatString="DateOfBirth" SortField="DateOfBirth" FField="DateOfBirth" FPartialMatch="true" FInputAtt="class='text daterangepicker'" />
        <jgrid:col runat="server" id="cAddress" HText="Address" ColType="Text" PrimaryKey="false" FormatString="Address" SortField="Address" FField="Address" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cZip" HText="Zip" ColType="Text" PrimaryKey="false" FormatString="Zip" SortField="Zip" FField="Zip" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cCity" HText="City" ColType="Text" PrimaryKey="false" FormatString="City" SortField="City" FField="City" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cCountry" HText="Country" ColType="Text" PrimaryKey="false" FormatString="Country" SortField="Country" FField="Country" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cPhoneNumber" HText="Phone Number" ColType="Text" PrimaryKey="false" FormatString="PhoneNumber" SortField="PhoneNumber" FField="PhoneNumber" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cMobileNumber" HText="Mobile Number" ColType="Text" PrimaryKey="false" FormatString="MobileNumber" SortField="MobileNumber" FField="MobileNumber" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cEmail" HText="Email" ColType="Text" PrimaryKey="false" FormatString="Email" SortField="Email" FField="Email" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cModuleCode" HText="Module Code" ColType="Text" PrimaryKey="false" FormatString="ModuleCode" SortField="ModuleCode" FField="ModuleCode" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cGrade" HText="Grade" ColType="Text" PrimaryKey="false" FormatString="Grade" SortField="Grade" FField="Grade" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cGradeComment" HText="Grade Comment" ColType="Text" PrimaryKey="false" FormatString="GradeComment" SortField="GradeComment" FField="GradeComment" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cStartDate" HText="Start Date" ColType="Text" PrimaryKey="false" FormatString="StartDate" SortField="StartDate" FField="StartDate" FPartialMatch="true" FInputAtt="class='text daterangepicker'" />
        <jgrid:col runat="server" id="cEndDate" HText="End Date" ColType="Text" PrimaryKey="false" FormatString="EndDate" SortField="EndDate" FField="EndDate" FPartialMatch="true" FInputAtt="class='text daterangepicker'" />
        <jgrid:col runat="server" id="cYear" HText="Year" ColType="Text" PrimaryKey="false" FormatString="Year" SortField="Year" FField="Year" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cSemester" HText="Semester" ColType="Text" PrimaryKey="false" FormatString="Semester" SortField="Semester" FField="Semester" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cImportName" HText="Import Name" ColType="Text" PrimaryKey="false" FormatString="ImportName" SortField="ImportName" FField="ImportName" FPartialMatch="true" FInputAtt="class='text'" />
    </Cols>                
</jgrid:jGridData>

<table cellpadding="0" cellspacing="0" border="0" class="listing assets" style="border-top:0px;">
	<colgroup></colgroup>
	<colgroup span="3" style="font-weight: bold;"></colgroup>
	<thead>
		<tr class="actions">
			<td colspan="11">
				<table cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<td class="select">
							</td>
							
							<td class="paging">
	                            Page
	                            <img src="https://mymetier.net/learningportal/inc/images/icons/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow grid_prev_page" />

	                            <input type="text" value="1" class="text page page_number" />

	                            <a href="javascript:void(0);" title="Next page" ><img src="https://mymetier.net/learningportal/inc/images/icons/pager_arrow_right.gif" alt="Go to Next page" class="arrow grid_next_page" /></a>

	                            of <span class="page_status">3 pages</span>            <span class="separator">|</span>
	                            View            <select name="limit" class="page_size">

	                                <option value="20" selected="selected">20</option>
	                                <option value="30">30</option>
	                                <option value="50">50</option>
	                                <option value="100">100</option>
	                                <option value="200">200</option>
	                            </select>

	                            per page
			                </td>
						</tr>
					</thead>
				</table>
			</td>
		</tr>
	</thead>
</table>

<script language="javascript">
    fnGridAttach(oGrid);
    
<common:label runat="server" id="lScripts" />    
</script>

</asp:Content>

