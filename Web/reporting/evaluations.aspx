﻿<%@ Page Title="Metier Course Status Report" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Reporting.Evaluations" Codebehind="evaluations.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server">
<h2><img src="../inc/images/icons/tools_32.png" alt="Manage Users" />Course Evaluations</h2>
<div id="hSearchTips" style="display:none">
<b>To help you search more efficiently, we have compiled some search tips.</b><br />
<br /> 
 <ul class="list">
<li>When searching for a value, the system will match any part of the field (not just from the beginning), to attempt to locate a match. (So, no need to use * or %).</li>
<li>Looking for all rows where a certain field is blank? Use two apostrophes '' to search!</li>
<li>Looking for all items that are greater or smaller than a certain value, use &gt; or &lt; in your search. 
<ul class="list">
<li>Ie, all users receiving more than 1500 points on their elearning, use: <u>&gt;1500</u></li>
</ul>
</li>
</ul>
</div>
<div id="cStatus" style="visibility:hidden;"><img src='../inc/images/icons/accept.gif'/>&nbsp;Refresh complete</div>

<br style="clear: both;" />

<div style="float:left"><img src="../inc/images/icons/table_save.png" /> <a href="javascript:void(0)" onclick="oGrid.addRequest(oGrid.buildRequest('custom','Type','export'));">Export report to Excel</a></div>
<div style="float:right;"><img src="../inc/images/icons/help.png" style="position:relative;top:3px" /> <a href="javascript:void(0);" onclick="$('#hSearchTips').dialog({ modal: true, buttons: { 'Close': function() { $(this).dialog('close'); } } }).dialog('option', 'title', 'Search Tips').dialog('option','width',600).dialog('option','position',['center', 'center']).dialog('open');">Search Tips</a></div>
<br style="clear: both;" />
<div style="border:2px solid red;padding:7px; background-color:#eee; margin:8px 0; display:none" id="hDownloadContainer">
Download your file from <a href="" id="lDownload" onclick="$('#hDownloadContainer').hide();">here</a>.
</div>
<br style="clear: both;" />


<table cellpadding="0" cellspacing="0" border="0" class="listing assets" style="margin-bottom:0;"><thead><tr class="actions"><td><table cellpadding="0" cellspacing="0"><thead><tr>
<td class="select">Select: <a href="javascript:void(0);" onclick="oGrid.setSelection(1);">All</a>, <a href="javascript:void(0);" onclick="oGrid.setSelection(0);">None</a> &nbsp;|&nbsp;<span class="selection-count"><strong>0</strong> items selected</span></td>
<td><span id="hGridView"></span>&nbsp;<button type="button" onclick="fnMarkCheckedAsReviewed()">Mark checked rows as reviewed</button></td>
<td class="paging">Page <img src="../inc/images/icons/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow grid_prev_page" /> <input type="text" value="1" class="text page page_number" /> <a href="javascript:void(0);" title="Next page" ><img src="../inc/images/icons/pager_arrow_right.gif" alt="Go to Next page" class="arrow grid_next_page" /></a> of <span class="page_status">3 pages</span> <span class="separator">|</span> View <select name="limit" class="page_size"><option value="20" selected="selected">20</option><option value="30">30</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option></select> per page</td>
</tr></thead></table></td></tr></thead></table>							
    
<jgrid:jGridAdo runat="server" id="gGrid" DisableInitialFiltering="false" DateCastFunction="DATEADD(dd, 0, DATEDIFF(dd, 0, {0}))" DisableInitialSorting="false" LoadViewContainer="hGridView" Pagination="true" DataViewFiltering="false" CaseInsensitiveFilter="true" BindDropdownByID="false" style="width:956px;" SortOrder="desc" JSName="oGrid" AutoFilter="true" PageSize="50" SortCol="2" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets' style='border-top:0;margin-bottom:0'" DeleteConfirmation="Are you sure you wish to delete this portal?" DisplayFilter="true" DisplayAdd="false">
    <Cols>        
        <jgrid:col id="cCheck" HText="" runat="server" ColType="Select" SortField="#" />        
        <jgrid:col id="cReviewed" HText="Reviewed" runat="server" ColType="Text" SortField="d_evaluations.Reviewed" FColType="Hide" FormatString="#Reviewed#" />        
        <jgrid:col runat="server" id="cID" HText="ID" ColType="Hide" PrimaryKey="true" FormatString="#ID#" SortField="d_evaluations.ID" FPartialMatch="true" FInputAtt="class='text'" />
        <jgrid:col runat="server" id="cPostedDate" HText="Date" FormatString="posted_date" SortField="posted_date" FField="posted_date" FPartialMatch="true" FInputAtt="class='text daterangepicker'" ColType="Text" />   
        <jgrid:col runat="server" id="cCourseCode" HText="Course Code" FormatString="course_code" SortField="course_code" FField="course_code" FPartialMatch="true" FInputAtt="class='text'" ColType="Text" />
        <jgrid:col runat="server" id="cLessonNumber" HText="Lesson #" FormatString="lesson_number" SortField="lesson_number" FField="lesson_number" FPartialMatch="true" FInputAtt="class='text'" ColType="Text" ExcelColType="auto" />
        <jgrid:col runat="server" id="cScore" HText="Score" FormatString="score" SortField="score" FField="score" FPartialMatch="true" FInputAtt="class='text'" ColType="Text" ExcelColType="auto" />
        <jgrid:col runat="server" id="cUserName" HText="User" FormatString="username" SortField="d_evaluations.username" FField="d_evaluations.username" FPartialMatch="true" FInputAtt="class='text'" ColType="Text" />
        <jgrid:col runat="server" id="cCompanyName" HText="Company" FormatString="company_name" SortField="d_users.company_name" FField="d_users.company_name" FPartialMatch="true" FInputAtt="class='text'" ColType="Text" />        
        <jgrid:col runat="server" id="cComment" HText="Comment" FormatString="comment" SortField="cast(comment as varchar(max))" FField="comment" FPartialMatch="true" FInputAtt="class='text'" ColType="Text" />

    </Cols>                
</jgrid:jGridAdo>

<table cellpadding="0" cellspacing="0" border="0" class="listing assets" style="margin-bottom:0;"><thead><tr class="actions"><td><table cellpadding="0" cellspacing="0"><thead><tr>
<td></td>
<td class="paging">Page <img src="../inc/images/icons/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow grid_prev_page" /> <input type="text" value="1" class="text page page_number" /> <a href="javascript:void(0);" title="Next page" ><img src="../inc/images/icons/pager_arrow_right.gif" alt="Go to Next page" class="arrow grid_next_page" /></a> of <span class="page_status">3 pages</span> <span class="separator">|</span> View <select name="limit" class="page_size"><option value="20" selected="selected">20</option><option value="30">30</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option></select> per page</td>
</tr></thead></table></td></tr></thead></table>
<script language="javascript" type="text/javascript">
    fnGridAttach(oGrid);
    oGrid.registerEventListener("GridRenderCell", function (oEvent) {
        
        if(oEvent.Data.Index>-1&&oEvent.Data.ColObject.ID=="cCheck" && oGrid.getData(oEvent.Data.Index,"cReviewed")=="True") oEvent.Data.HTML="";            
    });
    function fnMarkCheckedAsReviewed(){        
        var s = oGrid.getSelection();
        if (s == "") alert("No rows selected to be marked.");
        else oGrid.addRequest(oGrid.buildRequest("custom", "Type", "set-as-reviewed", "Argument", s));
        oGrid.setSelection(0);
    }
<common:label runat="server" id="lScripts" />    
</script>
</asp:Content>

