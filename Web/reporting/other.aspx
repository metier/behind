﻿<%@ Page Title="Metier Other Reports" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Reporting.Other" Codebehind="other.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server">
<h2><img src="../inc/images/icons/tools_32.png" alt="Other Reports" />Other Reports</h2>
			
<br style="clear: both;" />

Please select Report Template: 
<common:dropdown runat="server" ID="dReportTemplate" AutoPostBack="true">
<asp:ListItem Value="">(Please select)</asp:ListItem>
<asp:ListItem Value="exam-player">Exam Player</asp:ListItem>
<asp:ListItem Value="peice-user-progress">PEICE Progress</asp:ListItem>
</common:dropdown>
<br /><br /><br />
<asp:PlaceHolder runat="server" ID="pExamPlayerOptions" Visible="false">
<div style="font-size:16px; font-weight:bold">Exam Player Options</div>
Please select exam: <common:dropdown runat="server" ID="dExamPlayerExam" DataTextField="name" DataValueField="ObjectID" AutoPostBack="true" />
</asp:PlaceHolder>

<script language="javascript">
  
<common:label runat="server" id="lScripts" />
</script>

</asp:Content>

