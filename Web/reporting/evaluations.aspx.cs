﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.controls.jgrid;
using jlib.functions;
namespace Phoenix.LearningPortal.Reporting
{
    public partial class Evaluations : jlib.components.webpage
    {
        private Util.Classes.user m_oActiveUser = null;
        public Util.Classes.user ActiveUser
        {
            get
            {
                if (m_oActiveUser == null) m_oActiveUser = (Page.Master as Inc.Master.Default).ActiveUser;
                return m_oActiveUser;
            }
        }
        public void Page_Init(object sender, EventArgs e)
        {
            gGrid.GridBeforeDatabind += new jlib.controls.jgrid.OnGridBeforeDatabindHandler(gGrid_GridBeforeDatabind);
            gGrid.GridCustomData += new OnGridCustomDataHandler(gGrid_GridCustomData);
            gGrid.GridRowPrerender += new OnGridRowPrerenderHandler(gGrid_GridRowPrerender);
            gGrid.GridViewsLoad += new OnGridViewsLoadHandler(gGrid_GridViewsLoad);
            gGrid.GridViewsSave += new OnGridViewsSaveHandler(gGrid_GridViewsSave);
            gGrid.DataHelper = new ado_helper("sql.dsn.cms");
        }

        void gGrid_GridRowPrerender(object sender, GridCellEventArgs e)
        {
            //col[] oCols = new col[] { cDateC, cDateEndC, cEnrollmentC, cDateE, cDateEndE, cEnrollmentE };
            //for (int x = 0; x < oCols.Length; x++)
            //    if (convert.cStr(e.TextValues[oCols[x].Index]) != "") e.TextValues[oCols[x].Index] = String.Format("{0:yyyy}-{0:MM}-{0:dd} {0:HH}:{0:mm}:{0:ss}", convert.cDate(e.TextValues[oCols[x].Index]));

            //oCols = new col[] { cUserExpires };
            //for (int x = 0; x < oCols.Length; x++)
            //    if (convert.cStr(e.TextValues[oCols[x].Index]) != "") e.TextValues[oCols[x].Index] = String.Format("{0:yyyy}-{0:MM}-{0:dd}", convert.cDate(e.TextValues[oCols[x].Index]));

            //oCols = new col[] { cTimeSpent };
            //for (int x = 0; x < oCols.Length; x++)
            //    if (convert.cStr(e.TextValues[oCols[x].Index]) != "") e.TextValues[oCols[x].Index] = jlib.functions.format.cHHMMSS(convert.cInt(e.TextValues[oCols[x].Index]), "{2:0#}:{1:0#}:{0:0#}");
        }

        void gGrid_GridViewsSave(jGrid oGrid)
        {
            Util.Permissions.setSetting("grid.view", "evaluations", ActiveUser.ID, oGrid.Views.serialize());
        }

        void gGrid_GridViewsLoad(jGrid oGrid)
        {
            gGrid.Views.parse(Util.Permissions.getSetting("grid.view", "evaluations", ActiveUser.ID, ""), true);
        }

        void gGrid_GridCustomData(object sender, GridCellEventArgs e)
        {
            if (e.Type == "export")
            {
                gGrid.DataSource = null;
                gGrid_GridBeforeDatabind("excel", null);
                string sFileName = io.getUniqueFileName(System.Configuration.ConfigurationManager.AppSettings["temp.path"] + "\\Evaluations-" + Session["user.id"] + "-" + String.Format("{0:MM}{0:dd}{0:yy}-{0:HH}{0:mm}{0:ss}", DateTime.Now) + ".xlsx");
                int iRowCounter = 0;
                gGrid.saveAsExcel("", null, sFileName, "Evaluations Export", 0, ref iRowCounter, "Metier Evaluations Export");
                gGrid.DataSource = null;
                e.ClientJSAfter = "$('#hDownloadContainer').show();$('#lDownload').attr('href','" + Request.ApplicationPath + "/download.aspx?file=" + System.Web.HttpUtility.UrlEncode(System.IO.Path.GetFileName(sFileName)) + "');";
            }
            else if (e.Type == "set-as-reviewed")
            {
                e.RefreshData = true;
                e.Return = true;
                sqlbuilder.getDataTable(gGrid.DataHelper, "update d_evaluations set Reviewed=1 where id in (" + parse.stripCharacter(parse.removeXChars(e.Argument, "0123456789,"), ",") + ")");
                e.ReturnValue = "Row(s) successfully marked";
            }
        }

        void gGrid_GridBeforeDatabind(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            gGrid.setDataSourcePaged("d_evaluations.*, d_users.company_name", "d_evaluations left outer join d_users on d_users.id=d_evaluations.user_id", "");
        }
    }
}