﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.functions;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Exam
{
    public partial class Register : jlib.components.webpage
    {
        private AttackData.Exam ExamObject = new AttackData.Exam();
        private AttackData.Release ReleaseObject = new AttackData.Release();
        private AttackData.User UserObject = new AttackData.User();
        protected void Page_PreInit(object sender, EventArgs e)
        {

            (Page.Master as Inc.Master.Default).clearWrapper().DisableLoginRedirect = true;
            if (!this.IsPostBack && this.QueryString("authentication") != "")
            {
                Util.Permissions.logInIfRequired(this.QueryString("authentication"));
                if (Util.Permissions.UserID == 0) Response.Redirect("timeout.aspx");
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //if (this.QueryString("release_id").Int() == 52) {
            //    Response.Write("Sorry, but the assessment is not currently available.");
            //    Response.End();
            //}
            List<jlib.components.iControl> controls = jlib.helpers.control.getDynamicControls(this.Page, null);
            controls.ForEach(x => { if (x.DataMember.Str().StartsWith("dynamic:")) { x.Visible = false; } });

            ado_helper oData = new ado_helper("sql.dsn.cms");
            ReleaseObject = AttackData.Releases.GetByField(ID: this.QueryString("release_id").Int(), UrlKey: this.QueryString("key")).Execute().FirstOrDefault();
            if (ReleaseObject == null) Response.Redirect("timeout.aspx");
            lTitle.Text = ReleaseObject.Name;
            if (ReleaseObject.Type == "exam")
            {
                DataTable oDBExam = sqlbuilder.getDataTable(oData, "select * from d_exams where ObjectID=@ObjectID and @ObjectDate between CreateDate and DeleteDate", "@ObjectID", ReleaseObject.ObjectID, "@ObjectDate", ReleaseObject.ObjectDate);
                if (oDBExam.Rows.Count > 0)
                {
                    var oExam = new Util.Exam.exam();
                    oExam.populate(oDBExam.Rows[0], ReleaseObject.ObjectDate);
                    foreach (KeyValuePair<string, string> oSetting in oExam.Settings)
                    {
                        if (oSetting.Key.StartsWith("Role_") && oSetting.Value != "") dRole.Items.Add(new ListItem(oSetting.Value, oSetting.Key));
                    }

                    if (oExam.Settings.SafeGetValue("Logo").IsNullOrEmpty())
                    {
                        iLogo.Visible = false;
                    }
                    else
                    {
                        iLogo.Src = jlib.helpers.general.getThumbnailUrl(Server.MapPath(oExam.Settings.SafeGetValue("Logo")), 102, 36, true, true, true, "", jlib.helpers.general.FileStatus.None, true, 0, 0, 0, 0);
                    }
                    System.Web.HttpContext.Current.Items["version.date"] = ReleaseObject.ObjectDate;
                    ExamObject = AttackData.Exam.LoadByPk(oDBExam.Rows[0]["ObjectID"].Int());
                    controls.ForEach(x =>
                    {
                        if (x.DataMember == "dynamic:" + ExamObject.PlayerType) { x.Visible = true; }
                        else if (x.DataMember.Str().StartsWith("Exam.Settings.")) { x.setValue(ExamObject.getSetting(x.DataMember.Str().Replace("Exam.Settings.", ""))); }
                        else if (x.DataMember.Str().StartsWith("Exam.")) { x.setValue(ExamObject.Fields.GetFieldValueByPropertyName(x.DataMember.Str().Replace("Exam.", ""))); }
                    });

                }
            }
            controls.ForEach(x => { x.DisableSave = !x.Visible; });
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            ado_helper oData = new ado_helper("sql.dsn.cms");
            if (this.IsPostBack)
            {
                this.ErrorLabel = lFormError;
                if (this.validateForm())
                {
                    DataTable oExistingUser = sqlbuilder.getDataTable(oData, "select * from d_users where email=@email", "@email", tEmail.Text);
                    if (oExistingUser.Rows.Count == 0)
                    {
                        oExistingUser = sqlbuilder.getNewRecord(oData, "d_behind_users");
                        sqlbuilder.setRowValues(oExistingUser.Rows[0], "first_name", tName.Text.Substring(0, (tName.Text + " ").LastIndexOf(" ")), "last_name", parse.replaceAll(tName.Text, tName.Text.Substring(0, (tName.Text + " ").LastIndexOf(" ")), "").Trim(), "email", tEmail.Text, "username", tEmail.Text, "id", convert.cInt(sqlbuilder.getDataTable(oData, "select min(id) from d_users where id<0").SafeGetValue(0, 0)) - 1);
                        ado_helper.update(oExistingUser);
                    }
                    Util.Permissions.setLoginData(true, oExistingUser.Rows[0]["id"].Int(), 0, Util.Permissions.UserType.User, DateTime.Now.AddHours(3), ReleaseObject.ID);
                    UserObject = AttackData.User.LoadByPk(oExistingUser.Rows[0]["id"].Int());
                    //check to make sure this user has not already taken the assessment
                    List<jlib.components.iControl> controls = jlib.helpers.control.getDynamicControls(this.Page, null);
                    controls.ForEach(x => {
                    if (x.Visible && x.DataMember.Str().StartsWith("Settings.")) {
                        var setting = Data.Settings.GetByField(ObjectID: UserObject.ID, TableName: "d_users", KeyName: x.DataMember.Replace("Settings.", "")).Select().FirstOrDefault();
                        if (setting == null)
                            setting = new Data.Setting { ObjectID = UserObject.ID, TableName = "d_users", KeyName = x.DataMember.Replace("Settings.", "")};

                            setting.Value = x.getValue().Str();
                            setting.Save();                    
                        }
                    });
                    if (ExamObject.PlayerType == "assessment-dnv")
                    {
                        if (!System.Configuration.ConfigurationManager.AppSettings["site.dev"].Bln())
                        {
                            //AttackData.Attempt userAttempt = AttackData.Attempts.GetByField(ReleaseID: ReleaseObject.ID, UserID: UserObject.ID).Execute().FirstOrDefault();
                            DataTable userAttempts = sqlbuilder.getDataTable(oData, "select * from d_users, d_attempts where d_attempts.deleted=0 and lower(email)=@email and d_users.id=d_attempts.user_id and release_id=@release_id", "@email", tEmail.Text.ToLower(), "@release_id", ReleaseObject.ID);
                            if (userAttempts.Rows.Count > 0 && !UserObject.Email.Str().Equals("otto.husby@metier.no")) lFormError.Text += "You have already taken this assessment. It is only available for you to take once.<br />";
                        }
                        //if (UserObject.getSetting("SupervisorEmail").Str().ToLower().Trim() == UserObject.Email.Str().ToLower().Trim())  lFormError.Text += "Your Supervisor's email address must be different than yours!<br />";
                        if (!lFormError.Text.IsNullOrEmpty())
                        {
                            lFormError.Visible = true;
                            return;
                        }
                    }                    

                    int iAttemptID = Util.Permissions.initiateAttempt(this.QueryStringInt("release_id"), UserObject.ID, 0, 0, "");
                    if (ExamObject.PlayerType == "assessment-dnv")
                        Response.Redirect("assessment.aspx?attempt_id=" + iAttemptID);

                    sqlbuilder.executeInsert(oData, "d_temp_data", "table_name", "d_attempts", "pk_id", iAttemptID, "key_name", "role", "value", parse.splitValue(dRole.getValue(), "_", 1), "is_temporary", 0);
                    sqlbuilder.executeInsert(oData, "d_temp_data", "table_name", "d_attempts", "pk_id", iAttemptID, "key_name", "role_name", "value", dRole.SelectedText, "is_temporary", 0);
                    Response.Redirect("player.aspx?attempt_id=" + iAttemptID);
                }
            }
        }
    }
}