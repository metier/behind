﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Phoenix.LearningPortal.Exam.Assessment" MasterPageFile="~/inc/master/default.master" Codebehind="assessment.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head runat="server">
    <title></title>
        <script src="../inc/js/jquery.js" type="text/javascript"></script> 
        <script src="../inc/js/json2.min.js" type="text/javascript"></script>
        <script src="../inc/js/daterangepicker.jQuery.js" type="text/javascript"></script>
        <script type="text/javascript" src="../inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="~/inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.css" />				
        <script src="../inc/js/functions.js?14" type="text/javascript"></script>
        <script src="../exam/js/quiz.js?2" type="text/javascript"></script>      
        <link rel="stylesheet" href="../player/css/tempstyle.css" type="text/css" />
    <link rel="stylesheet" href="../css/metier-common.css" type="text/css" />
        <style type="text/css">
            <common:label runat="server" id="lInlineCSS" /> 
            #content
            {
                margin:0 auto;
                width:900px;
                padding:50px 0 0 0;
            }
            .quiz-container
            {
                margin-top:40px;
            }
            #wrapper-submenu
            {
                top:40px;
                height:80%;
                overflow:scroll;
                right:30px;
            }
            .quiz-container .counter
            {
                font-size:30px;
                padding:10px 5px;
            }
            .quiz-container .problem-container
            {
                width:570px;
            }

            .rating-container{
                padding-top:20px;
                padding-bottom:10px;
            }
.rating-container .label{
  color: #95aec3;
  font-family: Arial;
  font-size: 10px;
  font-weight: bold;
  padding-top: 5px;
  text-transform: uppercase;
float:left;

}
</style>

    <script language="javascript" type="text/javascript">
        <common:label runat="server" id="lScript" />
            function fnSaveProgress(QuestionObject){
                var s="", answers={};
                $.each(QuestionObject.Answer.split(","),function(){
                    if(trim(cStr(this))!="") s+=(s==""?"":",")+$($(QuestionObject.Xml).find("answer").eq(cInt(this))).attr("id");
                });
                                
                answers[$(QuestionObject.Xml).attr("id")] = [0,'', s];
                //alert(QuestionObject.Answer);
                //alert($(QuestionObject.Xml).attr("id"));
                //var data=["__answers"];
                fnSendData("action=save-answers", {"_answers":JSON.stringify(answers)},function(){});  
            }     
        
        function fnEvalBarChange(i, bUIOnly) {
            if (i == $("#lesson-rating").data("value")) return;
            $("#lesson-rating").data("value", i);
            $("#lesson-rating .knob").css("left", 1 + (64 * i));
            $("#lesson-rating .bar").css("width", 13 + 1 + (64 * i));            
        }
        $(document).on("mousemove click mousedown touchstart touchmove","#lesson-rating", function (e) {
            if ((e.type == "mousedown" && e.which == 1) || e.type == "touchstart") $("#lesson-rating").data("state", "left");
            if ($("#lesson-rating").data("state") == "left" || e.type == "click") {
                var i = Math.round(((e.pageX | e.originalEvent.pageX) - $("#lesson-rating").offset().left - 13) / 64);
                if (i >= 0 && i <= 6) fnEvalBarChange(i);
            }
        });
        $(document).on("mouseleave mouseup touchend","#lesson-rating", function (e) { $("#lesson-rating").data("state", null); });

        $(function () {
            /*
            //re-order questions based on cAnswers={"101766":["0","",""],
            function FindQuestion(section, id,path){
                var ret;
                if(section && section.Questions){
                    $.each(section.Questions,function(i,o){
                        if(this.ObjectID==id)ret={Q:this,Path:path+"-"+i};
                    });
                }
                if(section && section.Sections){
                    $.each(section.Sections,function(i,o){
                        if(!ret) ret=FindQuestion(o,id,path+"-"+i);                        
                    });
                }
                return ret;
            }
            var TFSection, counter=0;            
            for(var answer in cAnswers){
                alert(answer);
                var q=FindQuestion(cExam,answer,"");
                if(q){
                    if(q.Q.QuestionType=="tf"){                        
                        TFSection=q.Q;                                                
                        cExam.Sections.remove(cInt(q.Path.split("-")[1]));                        
                    }else{
                        //alert(q.Path);
                        var current=cExam.Sections[cInt(q.Path.split("-")[1])];
                        cExam.Sections[cInt(q.Path.split("-")[1])]=cExam.Sections[counter];
                        cExam.Sections[counter]=current;
                        counter++;
                    }
                }
            }
            */
            $("h1:eq(0)").html(cExam.Name);
            var doc = $.parseXML("<quiz />");
            var node = fnCreateXmlNode(doc, "questions");
            var answers="";
            var addQuestions = function (question) {
                if (question.Questions) {
                    $.each(question.Questions, function (i, o) {
                        var q = fnSetXmlAttribute(fnCreateXmlNode(node, "question"), "type", (this.QuestionType=="tf"?"trueorfalse":"multiplechoice"), "weight", 1, "break-before", (i == 0 ? "1" : "0"),"id", this.ObjectID);
                        $(fnCreateXmlNode(q, "questiontext")).text(this.Text);
                        $(fnCreateXmlNode(q, "sectiontext")).text(question.Text);
                        var a = fnCreateXmlNode(q, "answers");
                        $.each(this.Answers, function (j,p) {
                            $(fnSetXmlAttribute(fnCreateXmlNode(a, "answer"), "correct", this.IsCorrect == 1 ? "yes" : "no", "id", this.ObjectID)).text(this.Text);
                            if(cAnswers[o.ObjectID] && (","+cAnswers[o.ObjectID][2]+",").indexOf(","+this.ObjectID+",")>-1)answers+=j+",";
                        });
                        answers+=";";                        
                    });
                }
            }
            var enumerateSections = function (s) {
                if (s.Sections) {
                    $.each(s.Sections, function () {
                        if (this.Sections) enumerateSections(this);
                        if (this.Questions) addQuestions(this);
                    });
                }
            }
            enumerateSections(cExam);            
            var o = { Data: [], FinalTest: true, Alerted: true };            
            o.Data.push({ Answers: answers, Score: 0, Xml: fnSerializeXML(doc), QuizID: 1 });            
            var QuizElement = $("#quiz-template").html();
            QuizElement = fnTranslateControl($(QuizElement)).appendTo($("#content"));            
            QuizElement.bind("changed",function(e,o){
                fnSaveProgress(o);
            }).bind("submitted",function(e,data){
                data.cancel=true;
                $(".timer").detach();
                clearInterval(cExam.Timer);
                $("._content, #wrapper-submenu").detach();
                var MCPoints=0, TFPoints=0, MCPointsPossible=0;                
                $.each(o.Quiz.Questions,function(i,o){                    
                    if(this.ProblemType.TrueOrFalse){
                        TFPoints+=(this.Score/100);
                    }else{
                        MCPoints+=this.NumCorrect;
                        MCPointsPossible+=$(this.Xml).find("answer[correct='yes']").length;
                    }
                });
                //var Pass=(MCPoints/MCPointsPossible>=0.75 && TFPoints>=8);
                //var Pass=((MCPoints+TFPoints)/(MCPointsPossible+12)>=0.75);
                var Pass=((MCPoints)/(MCPointsPossible)>=0.75);
                var Message=replaceAll((Pass ? cExam.Settings.DNVPass : cExam.Settings.DNVFail ),"{0}",MCPoints),
                    EmailMessage=replaceAll((Pass ? "Congratulations. Your score is {0}%. You have the required knowledge about how we plan and execute projects in Energy." : "Your score is {0}%. You did not fully meet the requirements for planning and executing projects in the Energy way. You can now enroll via My Learning into PM Essentials <b>(course code 42400-0001)</b> which is a practical e-learning course about the Energy’s way of managing projects." ),"{0}",Math.floor(MCPoints*100/MCPointsPossible)),
                Results="<br /><div><ul><li>Part 1 (Multiple Choice): You have achieved "+ MCPoints +" of "+MCPointsPossible+" points - " + Math.floor(MCPoints*100/MCPointsPossible) + "%</li><li>Part 2 (True/False): "+ TFPoints +" of 12 points (" + Math.floor(TFPoints*100/12) + "%)</li><li>Overall, you have achieved " + (MCPoints)  + " of " + (MCPointsPossible) + " points - "+ Math.floor((MCPoints)*100/(MCPointsPossible)) +"%. You need 75% or higher to pass.</li><li><b>Result: " + (Pass?"Pass":"You did not fully meet the requirements for planning and executing projects in the Energy way. You can now enroll via My Learning into PM Essentials which is a practical e-learning course about the Energy’s way of managing projects.") + "</b></li></ul></div>";
                
                fnSendData("action=close","data="+ encodeURIComponent(JSON.stringify({Pass:Pass,Message: EmailMessage + "\n\n<!-- " +  o.Quiz.Settings.Data[0].Answers+";quizscore:"+o.Quiz.Settings.Data[0].Score + ";score:"+Math.floor(MCPoints*100/MCPointsPossible) + ";" + o.Quiz.Settings.Data[0].Xml + " -->" })));
                //$("<div />").html(Message + "<br /><br />An email with your results have been sent to you and your line manager." + Results).appendTo($("#content"));
                $("<div />").html(Message + "<br />An email with your results have been sent to you." + Results).appendTo($("#content"));
                
                //Hvordan reflekterer dette profesional project management? Otto lager tekst
                //HA en 0-6 (a la p[ al'ringskurset) -- lagre poeng ved siden av evaluering i databasen
                //Fjern 
                
                var feedbackContainer = '<fieldset>'
+'<h2>Leave feedback</h2>'
+'  <br>'
+'<table><tr><td><b>1.</b></td><td>'
+'  The assessment revealed gaps in my project management knowledge.'
+'  <br>'
+'  <input type="radio" value="Disagree" name="The assessment revealed gaps in my project management knowledge" id="feedback-disagree"><label for="feedback-disagree">Disagree</label>&nbsp;<input type="radio" value="Agree" name="The assessment revealed gaps in my project management knowledge" id="feedback-agree"><label for="feedback-agree">Agree</label>'
+'  <br><br></td></tr><tr><td><b>2.</b></td><td>'
+'  The assessment included questions of varying difficulty from most project management knowledge areas. How well do you think the assessment reflected project management “best practices” (6 is best)?'
+'  <br>'
+'  <div class="rating-container">'
+'        <div id="lesson-rating"><div class="knob"></div><div class="bar"></div></div>'
+'        <div class="label">Not good</div>'
+'        <div class="label" style="padding-left:280px">Very good</div>'
+'</div>'
+'  <br>'
+'<br></td></tr><tr><td><b>3.</b></td><td>'
+'  List knowledge areas in project management where you believe you can improve:'
+'<textarea name="List knowledge areas in project management where you believe you can improve" style="width:600px;height:80px;"></textarea><br><button type="button">Submit feedback</button></td></tr></table>'
+'  </fieldset>';
                
                //<h2>Leave feedback</h2><textarea style='width:600px;height:80px;'></textarea><br /><button type='button'>Submit feedback</button>
                $("<div />").html("<br /><b>PLEASE NOTE:</b> The results of this test will be updated in MyLearning only once a week, so please be patient.<br /><br /><b><i>If you want to know the right answers you should enroll in Essential Project Management. It is a great learning experience for everybody working in projects.</i></b><br><br>" + feedbackContainer + "<br /><br /><h2>Exit assessment</h2><a href='http://www.metieracademy.com/dnvkema'>Return to the Learning Portal</a>").appendTo($("#content")).find("button").click(function(){

                    var t=$(this).parent().find("textarea");
                    if(t.val()==""){
                        alert("You need to type something");
                        return;
                    }
                    var s="Rating: " + $("#lesson-rating").data("value")+" of 6\n";
                    t.parents("fieldset").find("textarea, input:checked").each(function(){
                        s+=$(this).attr("name") + ": " + $(this).val() + "\n";
                    });
                    fnSendEmail("eval@bids.no","eval@bids.no","Assessment evaluation", false, s);
                    t.parents("fieldset").replaceWith("Your feedback has been sent. Thank you.");                    
                }); 
                
            }).bind("render",function(e,data){
                data.Container.find(".counter").html(data.Index>19?(data.Index-19)+"/12":(data.Index+1)+"/20");                
                data.Nav.find(".text strong").html((data.ProblemType.TrueOrFalse ? "TF " : "MC ")+ replaceAll(data.Container.find(".counter").html(),"/"," of "));
                if(data.Quiz.Initialized && data.ProblemType.TrueOrFalse && !QuizElement.data("alerted.TF")){
                    QuizElement.data("alerted.TF",true);                    
                    $("<div id='dialog' />").appendTo(document.body).html("<div>"+data.Xml.find("sectiontext").text()+"<br /><br /><br /><button onclick=$('#dialog').dialog('close'); type='button'>Acknowledge</button></div>").dialog({modal: true,position:['middle',100], width:700, height: 400, hide: 'fold', resizable: true, title: "Welcome to the true/false section"}).dialog("open");                
                }                                
                $(".submit-button-container").toggle(data.Index==data.Quiz.Questions.length-1);
            });
            o.Quiz = new clsQuiz(o, QuizElement);      
            $(document.body).data("__quiz", o);
            cExam.showMessage=function(bShow, sMsg,oCtrl){
                if(bShow)$("<div id='message-overlay' style='position:absolute;left:0px;top:0px;z-index:10000;width:100%;height:100%;background-color:white;display:table-cell; vertical-align:middle;text-align:center'><span>"+(cStr(sMsg)==""&&!oCtrl? "<b>"+$(".hPlaybackPaused").val()+"</b><br /><br /><a onclick='cExam.showMessage(false);' href='javascript:void(0)'><img src='../inc/images/Play1Normal.png' /></a>" : cStr(sMsg) )+"</span></div>").appendTo(document.body).append(oCtrl);
                else $("#message-overlay").detach();
                this.paused=bShow;
            };

            var i=(new Date());
            i.setSeconds(i.getSeconds()-cInt(cExam.TimeSpent));
            cExam.Timer = 
            setInterval(function(){
                if(cExam.paused){
                    i.setSeconds(i.getSeconds()+1);
                }
                var s=cInt(((new Date)-i)/1000);
                if(cExam.MaxTime>0){
                
                    var iRemain=cExam.MaxTime-s;
                    if(iRemain<=0){
                        if(cExam.Settings["disable-time-limit"]==1){
                            if(!$(document).data("time-limit-alert")){
                                $(document).data("time-limit-alert",true);
                                alert("<common:label runat='server' TranslationKey='Times-Up-Continue' />");
                            }
                        }else{
                            o.Quiz.save(o.Quiz.CurrentQuestion);
                            fnSaveProgress(o.Quiz.Questions[o.Quiz.CurrentQuestion], {"Timeout":true}); 
                            cExam.showMessage(true,"Your time is up. Your answers have been automatically saved.<br /><br /><a onclick='cExam.showMessage(false);' href='javascript:void(0)'>OK</a>");
                            QuizElement.trigger("submitted",{});
                            return;
                        }
                    }
                    $(".timer span.total").html(fnFormatMS(iRemain)).css("color",(iRemain<300&&cExam.MaxTime>0?"red":null));                    
                    if(iRemain<300 && !$(".timer span.total").data("alerted")){
                        $(".timer span.total").data("alerted",true);
                        alert("You have 5 minutes left to complete the rest of the assessment.");
                    }
                }                         
             },1000);
        });
            </script>
</head>
<body>
    <common:form id="frmMain" runat="server">
         <div id="wrapper-submenu"></div>        
        <div id="content">
            <div class="timer">                                        
                <div class="timer-clock">
                    <div class="clock">
                    Time remaining: <span class="total">-</span>
                    </div>
                    <br />
                </div>
            </div>                
            
            
            <h1>DNV GL - Energy</h1>

        </div>
    </common:form>
    </body>
    
    <script type="text/template" id="quiz-template">
<div class="_content">
    
    <div class="question-template" style="display:none">
<a href="javascript:void(0)" class="anchor"></a>
<div class="nine-rounded quiz-container">
<div class="head">
    <div class="counter">1</div>
    <div class="problem-container">
    <div class="problem"></div>
    </div>
    <br style="clear:both" />
</div>
<table style="width:100%" cellspacing="0">
<tr>
<td colspan="4" class="quiz-area">

<div class="quiz-DD">
<div class="questions"></div>

<div class="answers"></div>
<br style="clear:both" />
</div>

<div class="quiz-CL">
<div class="questions"></div>

<div class="answers"></div>
<br style="clear:both" />
</div>

<div class="quiz-FB">
<div class="questions"></div>

<div class="answers"></div>
<br style="clear:both" />
</div>

<div class="quiz-MC">
    <div class="questions"></div>
</div>

</td>
</tr>

<tr class="quiz-buttons">
<td colspan="2" style="padding:3px 0">
    <div class="highlight check-answers"><div class="text" translationkey="check-answer"></div></div> 
    <div class="progressbar large" style="width:150px">            
          <div class="progresscolor-taken" style="display:inline-block;"><div class="progressnumber-taken" style="width:150px"></div></div>
          <div class="rt"></div>
    </div>
</td>

    <td class="highlight display-key-container"><a href="javascript:void(0)" class="display-key" translationkey="display-answers">Vis fasit <span>(-2)</span></a></td>
    <td class="disabled reset-container"><a href="javascript:void(0)" class="reset" translationkey="start-over">Begynn på nytt</a></td>
</tr>

<tr class="finaltest-buttons">
<td colspan="3" style="height:30px"><div class="highlight prev-button player-button"><div class="left"></div><div class="right"></div><div class="text button-text-shadow" translationkey="prev-button">&lt;&lt; Previous</div></div> 
<div class="highlight next-button player-button"><div class="left"></div><div class="right"></div><div class="text button-text-shadow" translationkey="next-button">Next &gt;&gt;</div></div>

</td>
<td style="float:right" class="submit-button-container"><div class="highlight submit-button player-button"><div class="left"></div><div class="right"></div><div class="text button-text-shadow" translationkey="submit-button">Submit Test</div></div><div class="highlight new-attempt player-button"><div class="left"></div><div class="right"></div><div class="text button-text-shadow" translationkey="new-attempt">Submit Test</div></div></td>
</tr>

</table>
</div>
</div>

<div class="wrapper-main" style="width:720px">
</div>
<br /><br />

</div>
</script>

</html>
</asp:Content>