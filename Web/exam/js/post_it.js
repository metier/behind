﻿/*/
*****************************************************************************
* post_id.js
* Annotation, highlighting, and answer code for Metier PMA eLearning Courses
* © Metier PMA AS - All Rights Reserved.
*****************************************************************************
/*/

/*Language (set in generisk.htm) 0=EN, 1=FR, 2=NL, 3=NO, 4=SE, 5=DK, 6=DE */
function clsPostIt(bPersistAnnotations, sFrameName, oData, fnCallback, oContainer, iLanguageID) {
    this.Container = oContainer;
    var oCommentWindow = null;
                
    var intState = null;    
    var oBookmarks = new Array();
    var arrNotes = new Array(); // Bookmarks to note objects

    var bLeftButtonClicked = false;    
    var sFrameKey = "ptanke";
    if (oData == null) oData = {};
    if (m_oSLLang == undefined) var m_oSLLang = null;

    $(oContainer).unbind(".postIt").bind("mouseup.postIt", function (event) {

        if (event.which == 1) bLeftButtonClicked = false;
        if (!bPersistAnnotations) return;
        if (event.which == 1 && intState == "select") {
            fnHighlight();
            intState = null;
        }
        $("#exam-bookmarks .textarea").html($(".highlight-area").html().split("<").join("&lt;"));
    }).bind("mousedown.postIt", function (event) { if (event.which == 1) bLeftButtonClicked = true; })
        .bind("mousemove.postIt", function (event) { if (bLeftButtonClicked) intState = "select"; })
    .bind("dblclick.postIt", function (event) { fnHighlight(); })
    .bind("contextmenu.postIt", function (event) { fnInsertComment(event); });
                               
    oData = $.extend(true, {}, { x: oData }).x;
    $.data(this.Container, "control-helper", null);

    for (sKey in oData) {
        var sData = decodeURI(oData[sKey]);
        if (sKey.indexOf("answer_") == 0) {
            if ($(sKey) != null) $(sKey).value = sData;
        } else if (sKey.indexOf(sFrameName + ".") == 0) {

            if (sKey.indexOf(sFrameName + ".hl") == 0) {
                //moved this line to inside .hl block, since it messes with notes
                if(fnCallback!=undefined)fnCallback("set", sKey, "");
                
                if (trim(sData) != "") {
                    var iIndex = cInt(sKey.split(".hl")[1]);                    
                    var oBookmark=fnGetBookmarkData(this.Container,sFrameName + ".hl" + iIndex + ":" + sData);
                    oBookmarks[iIndex]=oBookmark;
                    fnHighlightText(this.Container, oBookmark);
                    if (fnCallback != undefined) fnCallback("set", oBookmark.Bookmark.split(":")[0], encodeURI(oBookmark.Bookmark.substring(oBookmark.Bookmark.indexOf(":") + 1)), oBookmarks);
                }

            } else if (sKey.indexOf(sFrameName + ".no") == 0) {
                if (trim(sData) != "") {                    
                    var s = sData.split("::"); var t = "";
                    for (var x = 0; x < 3; x++) t += s[x] + (x == 2 ? "" : "::");
                    fnAddNote(sData.substring(t.length+2), fnGetBookmarkData(this.Container, sKey + ":" + t));
                }
            }
        } else if (sKey == sFrameKey && sData == "1" && $('#chkSave').length>0) {
            $('#chkSave').attr("checked", true).change(function () { fnSaveText(); });
        }
    }

    //id starts with
    $("textarea[id^='answer_']").change(function () { fnSaveText(this); });

    function fnGetLanguageString(iID, sDefault) {
        if (m_oSLLang == undefined || iLanguageID == undefined || m_oSLLang[iLanguageID][iID] == null) return cStr(sDefault);
        return m_oSLLang[iLanguageID][iID];
    }

    function fnSaveText(oElement) {
        //Save All?
        if (oElement == null) {
            $("textarea[id^='answer_']").each(function (i, o) { fnSaveText(o); });
        } else {
            if (fnCallback != undefined) fnCallback("set", oElement.getAttribute("id"), encodeURI(oElement.value));                
        }        
    }

    /*/
    *****************************************************************************
    * Comments
    *****************************************************************************
    /*/
    
    function fnAddNote(sComment, oBookmark) {        
        //var obj = $("<span class='annotation' style='width:240px;color:white;padding:3px 10px 4px 20px;position:absolute;z-index:10;overflow:hidden;font-family:verdana;font-size:10px;background:#79b2ce url(https://mymetier.net/content/public/courseplayer2/gfx/pil.png) 0 50% no-repeat;top:" + ($(o.StartControl.Control).offset().top+30) + "px;left:" + ($(document.body).width() - 200) + "px;' id=\"note" + iNoteID + "\"></span>").appendTo(document.body);
        $(oBookmark.StartControl.Control).wrap("<span id='note_" + oBookmark.ID + "' class='bookmarked' title=\"" + sComment.split("\"").join("'").split("\n").join("\\n") + "\" />").parent().bind("contextmenu", function (event) { fnDeleteNote(event); });
                
        //obj.html(sComment.split("\n").join("<br>")).click(function(event){fnDeleteNote(event);}).bind("contextmenu", function (event) { fnInsertComment(event); });                
        arrNotes[cInt(oBookmark.ID)] = { "Bookmark": oBookmark, "Text": sComment, "Encoded": oBookmark.Bookmark + "::" + encodeURI(sComment) };
        if (fnCallback != undefined) fnCallback("set", oBookmark.Key, oBookmark.Value + "::" + encodeURI(sComment), arrNotes);
    }

    function fnDeleteNote(event) {
        var obj = event.currentTarget;
        if (obj && window.confirm(fnGetLanguageString(11,"Remove bookmark?"))) {
            var iNoteId = cInt(obj.id.substr(5));
            arrNotes[iNoteId] = null;
            $(obj).contents().unwrap();
            if (fnCallback != undefined) fnCallback("set", sFrameName + ".no" + iNoteId, "", arrNotes);
        }
        event.stopPropagation();        
    }    
    function fnCreateCommentWindow(event) {
        var oEditNote = null;
        var sComment = "";
        if (event.currentTarget != null && $(event.currentTarget).hasClass("annotation")) oEditNote = event.currentTarget;

        var iTop = event.pageY, iLeft = event.pageX;
        if (iLeft + 300 > $(window).width()) iLeft = $(window).width() - 300;
        
        if (oEditNote != null)
            sComment = $(oEditNote).html().split("<br>").join("\n").split("<BR>").join("\n");

        var oWindow=$("<div id='hCommentWindow' style='line-height:1.4;font-size:12px;font-family:verdana;padding:10px 18px;position:absolute;left:" + iLeft + "px;top:" + iTop + "px;border:2px dotted orange;width:200px;height:200px;text-align:center;background-color:#fff;z-index:10001'>" + fnGetLanguageString(9,"Enter your comment and click \"OK\" when done.") + "<br><textarea id='tComment' style='margin:10px 0;width:180px;height:100px'>" + sComment + "</textarea><br><input type='button' id='bOK' value='OK'>&nbsp;&nbsp;&nbsp;<input type='button' id='bCancel' value='" + fnGetLanguageString(10,"Cancel") + "'></div>").appendTo(document.body);

        oWindow.find('#bCancel').click(function () { $('#hCommentWindow').detach(); }).end()
        .find('#bOK').click(function () {
            if ($.data($('#hCommentWindow')[0], "replacesNote") != null) {
                if (fnCallback != undefined) fnCallback("set", sFrameName + ".no" + $.data($('#hCommentWindow')[0], "replacesNote").getAttribute("id").substring(4), "", arrNotes);
                $('#hCommentWindow').data("replacesNote").unwrap();
            }
            fnAddNote($('#tComment').val(), $('#hCommentWindow').data("bookmark")); $('#hCommentWindow').detach();
        });
        var oHelper = fnGetControlHelper(this.Container);
        var iCtrl = -1;
        $(oHelper.Controls).each(function (i, o) { var iOffset = $(o.Control).wrap("<span />").parent().offset().top; $(o.Control).unwrap(); if (iOffset >= iTop) { iCtrl = Math.max(0, i - 1); return false; } });
        if (iCtrl == -1) iCtrl = oHelper.Controls.length - 1;
        for (; iCtrl < oHelper.Controls.length - 1; iCtrl++) if (oHelper.Controls[iCtrl].Text.length > 10) break;
               
        //Build bookmark
        var sKey = fnClean(oHelper.Controls[iCtrl].Text).substring(0, 15);        
        if (sKey.indexOf("\n") > 0) sKey = sKey.split("\n")[0]; //For compatibility with IE, we can't have newlines in the KEY
        if (sKey.indexOf("\r") > 0) sKey = sKey.split("\r")[0];
        if (sKey.indexOf(":") > -1) {
            if (sKey.indexOf(":") < 4 && sKey.length > 7)
                sKey = sKey.substring(sKey.indexOf(":") + 1);
            else
                sKey = sKey.substring(0, sKey.indexOf(":"));
        }
        if (sKey == "") return;
        var sEndingText = ""; oHelper.Controls[iCtrl].Text.substring(15); ;
        for (var x = iCtrl; x < oHelper.Controls.length; x++) sEndingText += oHelper.Controls[x].Text;
        var iSelectIndex = getInstanceNumber(fnClean(oHelper.Text, true).split("\n").join("").split("\r").join("").split("\t").join(""), sKey, fnClean(trim(sEndingText), true).split("\n").join("").split("\r").join("").split("\t").join(""));
        
        if (sKey == ":") return;
        var sBookmark = sFrameName + ".no" + oBookmarks.length + ":" + sKey + "::" + iSelectIndex + "::0";
        var oNewBookmark = new fnGetBookmarkData(this.Container, sBookmark);
        //End Bookmark        
 
        oWindow.data("replacesNote", oEditNote).data("bookmark", oNewBookmark);
        
        oWindow.Top = iTop;
        oWindow.Left = iLeft;
        oWindow.destroy = function () { $('#hCommentWindow').detach(); oCommentWindow = null; };
        $('#tComment').focus();
        return oWindow;
    }

    // fnInsertComment - used for inserting notes.
    function fnInsertComment(event) {
        event.preventDefault();
        if (!bPersistAnnotations) return;        
        if (oCommentWindow != null) $(oCommentWindow).detach();
        oCommentWindow = fnCreateCommentWindow(event);
    }

    /*/
    *****************************************************************************
    * Yellow Highlighter
    *****************************************************************************
    /*/

    function fnGetBookmarkData(oContainer, sBookmark) {
        var o = new Object();
        o.Bookmark = sBookmark;
        o.Key = sBookmark.split(":")[0];
        o.Value = sBookmark.substring(o.Key.length + 1);
        o.ID = sBookmark.substring(sBookmark.indexOf(".")+3).split(":")[0];
        sBookmark = sBookmark.substring(sBookmark.indexOf(":") + 1);
        o.Text = sBookmark.split("::")[0];        
        o.Index = cInt(sBookmark.split("::")[1]);
        o.Len = o.Text.length + Math.max(cInt(sBookmark.split("::")[2]), 0);
        var oControlHelper = fnGetControlHelper(oContainer);
        var oArr=oControlHelper.Text.split(o.Text);
        for( var x=0;x<o.Index;x++)oArr.remove(0,0);
        o.DocumentCharOffset = oControlHelper.Text.length - oArr.join(o.Text).length - o.Text.length;
        for( var x=0;x<oControlHelper.Controls.length;x++){
            if (oControlHelper.Controls[x].Start + oControlHelper.Controls[x].Text.length > o.DocumentCharOffset) {
                o.StartControl = oControlHelper.Controls[x];
                break;
            }
        }
        return o;
    }

    // Highlight region
    function fnHighlightText(oContainer, oBookmark) {
        
        var oControlHelper = fnGetControlHelper(oContainer);
        var iControlCharOffset = oBookmark.DocumentCharOffset - oBookmark.StartControl.Start;
        var oCurrentControl = oBookmark.StartControl;
        for (var x = 0; x < oBookmark.Len && oCurrentControl; ) {
            //if (x > 0) x+=($.browser.msie ? 1 : 0); //IE inserts newline after each control in Range-object
            var sBeforeText = (x>0?"":oCurrentControl.Text.substring(0, iControlCharOffset));
            var sText = oCurrentControl.Text.substring(x == 0 ? iControlCharOffset : 0, (x == 0 ? iControlCharOffset : 0)+Math.min(oBookmark.Len - x, oCurrentControl.Text.length));
            var sAfterText = oCurrentControl.Text.substring(sBeforeText.length + sText.length);
            var oCtrls = [];
            if (sBeforeText != "") oCtrls.push({ "Start": oCurrentControl.Start, "Text": sBeforeText, "Control": $("<span id='bookmark_" + oBookmark.ID + "'></span>").text(sBeforeText).insertBefore(oCurrentControl.Control)[0] });
            oCtrls.push({ "Start": oCurrentControl.Start + sBeforeText.length, "Text": sText, "Control": $("<span class='highlighted' id='bookmark_" + oBookmark.ID + "'></span>").text(sText).insertBefore(oCurrentControl.Control).click(function () { fnRemoveBookmark(oContainer, this.id); })[0] });
            if (sAfterText != "") oCtrls.push({ "Start": oCurrentControl.Start + sBeforeText.length + sText.length, "Text": sAfterText, "Control": $("<span id='bookmark_" + oBookmark.ID + "'></span>").text(sAfterText).insertBefore(oCurrentControl.Control)[0] });
            
            for (var y = 0; y < oCtrls.length; y++) {
                oCtrls[y].Prev=(y==0?oCurrentControl.Prev:oCtrls[y-1]);
                oCtrls[y].Next = (y < oCtrls.length - 1 ? oCtrls[y + 1] : oCurrentControl.Next);
            }
            if (oCurrentControl.Prev) oCurrentControl.Prev.Next = oCtrls[0];
            if (oCurrentControl.Next) oCurrentControl.Next.Prev = oCtrls[oCtrls.length - 1];

            $.each(oControlHelper.Controls, function (i, o) {
                if (o == oCurrentControl) {
                    oControlHelper.Controls[i] = oCtrls[0];
                    for (var y = 1; y < oCtrls.length; y++) oControlHelper.Controls.splice(i + y, 0, oCtrls[y]);
                    return false;
                }
            });

            $(oCurrentControl.Control).detach();
            oCurrentControl = oCurrentControl.Next;
            if($.browser.msie)
                x += sText.length;
            else
                x += sText.split("\r").join(" ").split("\t").join(" ").split("\n").join(" ").split("  ").join(" ").split("  ").join(" ").split("  ").join(" ").split("  ").join(" ").length;
        }
        oBookmarks[cInt(oBookmark.ID)] = fnGetBookmarkData(oContainer, oBookmark.Bookmark);
        var s="";
        for (var x = 0; x < oBookmarks.length; x++) s += (oBookmarks[x]==null?"":oBookmarks[x].Bookmark + "||");
        //$("textarea").val(s);
    }

    function fnRemoveBookmark(oContainer, sID) {
        sID=sID.replace("bookmark_","");
        var oControlHelper = fnGetControlHelper(oContainer);
        var oBookmark = oBookmarks[cInt(sID)];
        var oControls = $(oContainer).find("#bookmark_" + sID);
        //oControls.contents().unwrap();        

        var oActualStart = oBookmark.StartControl;
        while (oActualStart.Control != oControls[0] && oActualStart.Prev) oActualStart = oActualStart.Prev;
        var iControlIndex = 0;
        var oControl = null;
        $.each(oControlHelper.Controls, function (i, o) {
            if (o == oActualStart) {
                iControlIndex=i;
                for (var y = 0; y < oControls.length; y++) oControlHelper.Controls.remove(i, i);
                return false;
            }
        });

        for (var x = 0; x < oControls.length; x++) {
            if (oControl == null || oControl.Control.parentNode != oControls[x].parentNode || $(oControl.Control).next().length == 0 || $(oControl.Control).next()[0] != oControls[x]) {
                //alert(oControl);
                oControl = { "Start": (iControlIndex==0?0: oControlHelper.Controls[iControlIndex - 1].Start + oControlHelper.Controls[iControlIndex - 1].Text.length), "Text": "", "Control": oControls[x].parentNode.insertBefore(document.createTextNode(""),oControls[x]), "Prev": (oControl ? oControl : oActualStart.Prev), "Next": oActualStart };
                oControlHelper.Controls.splice(iControlIndex++, 0, oControl);
                if (oControl.Prev) oControl.Prev.Next = oControl;
            }
            oControl.Text += oActualStart.Text;
            oControl.Next = oActualStart.Next;
            oActualStart = oActualStart.Next;
            oControl.Control.nodeValue = oControl.Text;            
            $(oControls[x]).detach();
        }
        
        oBookmarks[cInt(oBookmark.ID)] = null;
        if (fnCallback != undefined) fnCallback("set", oBookmark.Bookmark.split(":")[0], "", oBookmarks);
    }


    function getTextNodesIn(node, includeWhitespaceNodes) {
        var textNodes = [], whitespace = /^\s*$/;

        function getTextNodes(node) {
            if (node.nodeType == 3) {
                if ((includeWhitespaceNodes&&node.nodeValue!="") || !whitespace.test(node.nodeValue)) textNodes.push(node);                
            } else {
                for (var i = 0, len = node.childNodes.length; i < len; ++i) getTextNodes(node.childNodes[i]);                
            }
        }

        getTextNodes(node);
        return textNodes;
    }
    function fnGetControlHelper(oContainer) {
        var oControlHelper = $.data(oContainer, "control-helper");
        if (oControlHelper == null) {
            var oControlHelper = { "JQuery": getTextNodesIn(oContainer, true), "Text": "", "TextLen": 0, "Controls": [] };
            $.each(oControlHelper.JQuery, function (i, o) { oControlHelper.Controls.push({ "Prev": (i == 0 ? null : oControlHelper.Controls[i - 1]), "Control": o, "Start": oControlHelper.TextLen, "Text": o.nodeValue.split(String.fromCharCode(160)).join(" ") }); if(i>0)oControlHelper.Controls[i - 1].Next = oControlHelper.Controls[i]; oControlHelper.Text += oControlHelper.Controls[i].Text; oControlHelper.TextLen += oControlHelper.Controls[i].Text.length; });
            $.data(oContainer, "control-helper", oControlHelper);
        }
        return oControlHelper;
    }
    function fnHighlight() {
        var iSelectIndex = 0, iSelectRange = 0;

        if ($.browser.msie) {
            var oRange = document.selection.createRange();
            if(this.Container!=oRange.parentElement() && $(this.Container).has(oRange.parentElement()).length==0)return;            
            var sSelection = fnTrimEndingChar(fnTrimStartingChar(oRange.text, " ", "\n", "\r"), " ", "\n", "\r");
            
            var oRange1 = document.body.createTextRange();
            oRange1.moveToElementText(this.Container);
            oRange.setEndPoint("EndToEnd", oRange1);
            var sEndingText = oRange.text;
            document.selection.empty();
        }else{
            var oRange = window.getSelection().getRangeAt(0);
            if (this.Container != oRange.startContainer && $(this.Container).has(oRange.startContainer).length == 0) return;
            var sSelection = fnTrimEndingChar(fnTrimStartingChar(oRange.toString(), " ", "\n", "\r"), " ", "\n", "\r");
            window.getSelection().collapseToStart();
        }
            
        if (sSelection.length < 1) return;
        var oControlHelper = fnGetControlHelper(this.Container);
        
        if ($.browser.msie) {
            var oStartContainer = null;
            for( var x=0; x < oControlHelper.Controls.length; x++) {
                if (oControlHelper.Controls[x].Start + oControlHelper.Controls[x].Text.length > (oControlHelper.Text.length - sEndingText.length)) {
                    oStartContainer = oControlHelper.Controls[x].Control;
                    break;
                }
            }
        } else {
        
            var oStartContainer = oRange.startContainer;
            var iStartOffset = oRange.startOffset;
            var sEndingText = "";var bFound=false;
            for (var x = 0; x < oControlHelper.Controls.length; x++) {
                if (oControlHelper.Controls[x].Control == oStartContainer) {
                    sEndingText = oControlHelper.Controls[x].Text.substring(iStartOffset);
                    bFound = true;
                } else if (bFound) {
                    sEndingText += oControlHelper.Controls[x].Text;
                }
            }
        }

        var sKey = fnClean(sSelection).substring(0, 15);
        var iLen = fnClean(sSelection).split("\t").join(" ").split("\n").join(" ").split("\r").join("").split("  ").join(" ").split("  ").join(" ").split("  ").join(" ").length;
        if (sKey.indexOf("\n") > 0) sKey = sKey.split("\n")[0];//For compatibility with IE, we can't have newlines in the KEY
        if (sKey.indexOf("\r") > 0) sKey = sKey.split("\r")[0];
        if (sKey.indexOf(":") > -1) {
            if (sKey.indexOf(":") < 4 && sKey.length > 7)
                sKey = sKey.substring(sKey.indexOf(":") + 1);
            else
                sKey = sKey.substring(0, sKey.indexOf(":"));
        }
        if (sKey == "") return;

        iSelectIndex = getInstanceNumber(fnClean(oControlHelper.Text, true).split("\n").join("").split("\r").join("").split("\t").join(""), sKey, fnClean(trim(sEndingText), true).split("\n").join("").split("\r").join("").split("\t").join(""));                
        iSelectRange = Math.max(iLen - sKey.length, 0);            
        
        if (sKey == ":") return;
        var sBookmark = sFrameName + ".hl" + oBookmarks.length + ":" + sKey + "::" + iSelectIndex + "::" + iSelectRange;
        var oNewBookmark = new fnGetBookmarkData(this.Container,sBookmark);
        oBookmarks[oBookmarks.length]=oNewBookmark;

        for (var x = 0; x < oBookmarks.length; x++) {
            var oBookmark = oBookmarks[x];
            if (oBookmark && oBookmark != oNewBookmark && ((oBookmark.DocumentCharOffset < oNewBookmark.DocumentCharOffset && (oBookmark.DocumentCharOffset + oBookmark.Len) > oNewBookmark.DocumentCharOffset)
            || (oNewBookmark.DocumentCharOffset < oBookmark.DocumentCharOffset && (oNewBookmark.DocumentCharOffset + oNewBookmark.Len) > oBookmark.DocumentCharOffset))) {
                fnRemoveBookmark(this.Container, oBookmark.ID);
            }            
        }                       
        
        fnHighlightText(this.Container, oNewBookmark);
        if (fnCallback != undefined) fnCallback("set", sBookmark.split(":")[0], encodeURI(sBookmark.substring(sBookmark.indexOf(":") + 1)), oBookmarks);
    }

    /* UTILITY FUNCTIONS */

    /*
    This function returns the instance number of a given key
    @sText    = Markup of document
    @sKey     = Key used for this bookmark (3 first characters of string
    @sFullString  = Full string to look for
    @returns  = instance # of sKey (that belongs to sFullString)
    */

    function getInstanceNumber(sText, sKey, sFullString) {
        if (("" + sText) == "" || ("" + sFullString) == "") return -1;
        //xxx var oArr = (sText.indexOf(sFullString) == -1 ? [sText.substring(0, sText.length - sFullString.length)] : sText.split(sFullString));
        var oArr = sText.split(sFullString);
        if (oArr.length == 0) return -1;
        var iIndex = oArr[0].split(sKey).length;
        return iIndex;
    }
       
    function fnClean(sStr, bKeepEndingSpaces) {

        sStr = sStr.split("  ").join(" ").split("  ").join(" ").split("  ").join(" ").split("\r").join("").split("\n");
        if (!bKeepEndingSpaces)
            for (var x = 0; x < sStr.length; x++)
                sStr[x] = fnTrimEndingChar(sStr[x], " ");                      
            
        return sStr.join("\r\n");
    }

    function fnTrimStartingChar(sStr) {
        while (true) {
            var bFound = false;
            for (var x = 1; x < fnTrimStartingChar.arguments.length; x++) {
                if (sStr.indexOf(fnTrimStartingChar.arguments[x])==0) {
                    sStr = sStr.substring(fnTrimStartingChar.arguments[x].length);
                    bFound = true;
                }
            }
            if (!bFound) return sStr;
        }
    }

    function fnTrimEndingChar(sStr) {
        if (sStr == "") return "";
        while (true) {
            var bFound = false;
            for (var x = 1; x < fnTrimEndingChar.arguments.length; x++) {
                if (sStr.lastIndexOf(fnTrimEndingChar.arguments[x]) == sStr.length-fnTrimEndingChar.arguments[x].length) {
                    sStr = sStr.substring(0, sStr.length - fnTrimEndingChar.arguments[x].length);
                    bFound = true;
                }
            }
            if (!bFound) return sStr;
        }
    }
}
/* END UTILITY FUNCTIONS */