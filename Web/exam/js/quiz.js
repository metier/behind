﻿function clsQuiz(Settings, Section) {
    var _Quiz = this;
    var _Section = $(Section);    
    Settings.Quiz = this;
    Settings.QuestionOffset = cInt(Settings.QuestionOffset);
    if (!Settings.QuizArray) Settings.QuizArray = [Settings];
    this.Settings = Settings;    
    if (Settings.KeyMode == null) Settings.KeyMode = false;
    if(window.location.href.indexOf("&answers=true")>-1) this.Settings.KeyMode = true;
    this.CurrentQuestion="";     
    this.Menu = Settings.QuizArray[0].Quiz.Menu;
    if (this.Menu == null) this.Menu=$('<div class="container"><ul></ul>' + (this.Settings.FinalTest ? '' : '<ul class="tools"><li class="status"><div class="bar"></div><div class="text"><strong>' + _Translations["total-correct"] + '</strong>&nbsp;&nbsp;<span>17%</span></div></li></ul>') + '</div>').appendTo($("#wrapper-submenu"));                
    
    var Data=this.Settings.Data;
    if(this.Settings.FinalTest && !this.Settings.Alerted){
        var Resume="", NextQuiz=0;        
        $(Data).each(function(i,o){
            if(o.Answers){
                Resume+="<li><a class='resume_"+i+"' href='javascript:void(0)'>"+replaceAll(_Translations["show-attempt"],"{0}",(i+1),"{1}",o.Score)+"</a></li>";
                if(i==NextQuiz)NextQuiz++;
            }
        });
        this.Settings.Alerted = true;
        if(NextQuiz<Data.length)this.Data=$($.parseXML(Data[NextQuiz].Xml));
        var Box = fnShowMessage(replaceAll(_Translations["finaltest-" + (NextQuiz > 2 ? "done" : "welcome")] + (Resume ? "<ul>" + Resume + "</ul>" : ""), "{0}", (this.Data==null?"":this.Data.find("question").length) ),
        [(NextQuiz>2?null : [_Translations["finaltest-start"],function(){Settings.KeyMode=false; Settings.Index=NextQuiz; clsQuiz(Settings, _Section);}]),[_Translations["finaltest-exit"],function(){window.parent.fnRestoreNavigator()}]], {NoClose:true});
        $(Data).each(function(i,o){
            if (this.Answers) Box.find(".resume_" + i).click(function () { Settings.KeyMode = true; Settings.Index = i; clsQuiz(Settings, _Section); $("#metier-msg").detach(); });
        });
        return;
    }    
    if(cStr(Settings.Index)=="")Settings.Index=0;    
    Settings.Answers=Data[Settings.Index].Answers;
    this.Data = $($.parseXML(replaceAll(Data[Settings.Index].Xml, "&quot;", "\"")));
    this.Questions = [];
    this.render = fnRenderQuestion;
    this.save = fnSaveQuestion;
    var oQuestions = this.Data.find("question");
    var s = cStr(Settings.Answers);    
    if(s.indexOf(";score:")>-1)s=s.split(";score:")[0]+s.substring((s+";").indexOf(";",s.split(";score:").length));
    if(s.indexOf("q")==0) s=s.substring(s.indexOf(";")+1);    
    Settings.Answers = s.split(";");            
    for (var x = 0; x < oQuestions.length; x++) {
        var oQuestion = new clsQuestion(oQuestions[x], this, x, Settings.Answers[x]);
        this.Questions.push(oQuestion);
        this.render(x, !this.Settings.FinalTest);
        this.save(x, true, false);
    }
    if (this.Settings.FinalTest && this.Questions.length > 0) this.render(0, !this.Settings.FinalTest);
    
    function fnSaveQuestion(iIndex, bScoreOnly, bDisplayKey) {
        if (bDisplayKey == null) bDisplayKey = false;
        if(iIndex==null)iIndex=this.CurrentQuestion;
        if(cStr(iIndex)=="")return;
        if (this.Settings.KeyMode) bDisplayKey = true;
        var oQuestionObject = this.Questions[iIndex];
        var oQuestion = _Section.find(".question-" + iIndex);
        var iScore = oQuestionObject.Score, sAnswer = oQuestionObject.Answer;
        if (oQuestion.length > 0) {
            sAnswer = "";
            iScore = 0;
            var iNumCorrect = 0;
            if (oQuestionObject.ProblemType.MultipleChoice || oQuestionObject.ProblemType.TrueOrFalse) {
                var Answers = oQuestionObject.Xml.find("answer");
                oQuestion.find(".question").each(function (i, o) {
                    $(o).toggleClass("key", bDisplayKey && $(Answers[i]).attr("correct") == "yes");
                    if ($(o).hasClass("selected")) {
                        iScore += ($(Answers[i]).attr("correct") == "yes" ? 1 : -1);
                        sAnswer += (sAnswer == "" ? "" : ",") + i;
                    }
                    if ($(Answers[i]).attr("correct") == "yes") iNumCorrect++;
                });
                oQuestionObject.NumCorrect = Math.max(0, iScore);
                iScore = (iNumCorrect == 0 ? 0 : Math.max(0, iScore) * 100 / iNumCorrect);
            } else if (oQuestionObject.ProblemType.DragAndDrop) {
                var Drags = oQuestionObject.Xml.find("drag");
                var CorrectDrags = $.grep(Drags, function (o){return cStr($(o).attr("correct")) != "";}).length;
                var Drops = oQuestionObject.Xml.find("target");
                var GetCurrentMultiExpression = function (exp) {
                    exp = exp.split("multi:").join("").split("|");
                    for (var x = 0; x < exp.length; x++) {
                        var f = function (id) { return oQuestion.find(".answer").filter(function () { return $($(this).data("node")).hasClass(id); })[0]; };
                        var c = f(replaceAll(exp[x].split("=")[0], "[", "", "]", ""));
                        if (c && $(c).find(".question:contains(" + exp[x].split("=")[1].split(">")[0] + ")").length > 0) {
                            return exp[x].split(">")[1];
                        }
                    }
                };
                
                //oQuestion.find(".question").each(
                $(Drags).each(function (i) {
                    var Element = oQuestion.find(".question").filter(function () { return $(this).data("index") == i; });
                    var Container = Element.parents(".answer")[0];
                    var Drag = this;
                    var bCorrect = false;
                    if (Container != null) {
                        var exp = $(Drag).attr("correct");
                        if (exp && exp.indexOf("multi:") == 0) {                            
                            exp = GetCurrentMultiExpression(exp);
                            //jvt
                            bCorrect = cStr(exp).indexOf("[" + $(Drops[$(Container).data("index")]).attr("id") + "]") > -1;
                        } else {
                            bCorrect = $(Drops[$(Container).data("index")]).hasClass(exp);
                        }
                        iScore += (bCorrect ? 1 : -1);
                        sAnswer += Element.data("index") + ":" + $(Container).data("index") + ",";
                    }
                    if (oQuestionObject.KeyMode != bDisplayKey || _Quiz.Settings.FinalTest) {
                        Element.toggleClass("key", bDisplayKey).toggleClass("incorrect", bDisplayKey && !bCorrect);
                        if (!bCorrect || Element.data("previous-container") != null) {
                            if (bDisplayKey) {//move to correct container
                                Element.data("previous-container", Element.parent());
                                var c = $(Drag).attr("correct");
                                if (c.indexOf("multi:") > -1) {
                                    var exp = cStr(GetCurrentMultiExpression(c)).split("],[");
                                    for (var x = 0; x < exp.length; x++) {
                                        c = _Section.find(".answer_" + iIndex + "_" + replaceAll(exp[x], "[", "", "]", ""));
                                        if (c.parents(".answer").find(".question").length == 0 || x == exp.length - 1) {
                                            Element.detach().appendTo(c.parents(".answer"));
                                            break;
                                        }
                                    }
                                } else {
                                    Element.detach().appendTo(_Section.find(".answer_" + iIndex + "_" + c).parents(".answer"));
                                }
                            } else {//move back
                                Element.detach().appendTo(Element.data("previous-container")).data("previous-container", null);
                            }
                        }
                    }
                });
                iScore = (CorrectDrags == 0 ? 0 : iScore / CorrectDrags) * 100;
                oQuestionObject.AnswerRecalc();
            } else if (oQuestionObject.ProblemType.FillInTheBlanks) {                
                oQuestion.find(".answers .answer").each(function () {
                    if($(".question", this).length>0)
                        iScore +=( $(".question", this).text().toLowerCase()==$(this).data("answer").toLowerCase() ? 1 : -1);
                    sAnswer+=$(".question", this).text()+"|";
                });
                if (oQuestion.find(".question").length > 0)
                    iScore = iScore * 100 / oQuestion.find(".question").length;

                if (oQuestionObject.KeyMode != bDisplayKey) {                    
                    if (!bDisplayKey) {
                        oQuestion.find(".question.key").detach();
                        oQuestion.find(".answer:not(:has(.question))").removeClass("full");
                        oQuestion.find(".answer").removeClass("correct incorrect");
                    }else{
                        oQuestion.find(".answers .answer").each(function () {
                            var correct=$(".question", this).text().toLowerCase() == $(this).data("answer").toLowerCase();
                            $("<span class='question key' />").html($(this).data("answer")).appendTo(this);

                            $(this).addClass("full " + (correct ? "correct" : "incorrect"));
                        });
                    }
                }

            } else if (oQuestionObject.ProblemType.ConnectTheBoxes) {                
                if (oQuestionObject.jsPlumb) {
                    var processed = {};
                    oQuestion.find(".questions .question").each(function (i, q) {
                        var conn = jsPlumb.select({ element: q });
                        conn.each(function (o) {
                            if (!processed[o.id]) {
                                var id1 = $(o.source == q ? o.source : o.target).data("boxid"),
                                    id2 = $(o.source == q ? o.target : o.source).data("boxid");
                                iScore += (oQuestionObject.Xml.find("connection[id1=" + id1 + "][id2=" + id2 + "]").length + oQuestionObject.Xml.find("connection[id1=" + id2 + "][id2=" + id1 + "]").length > 0 ? 1 : -1);
                                sAnswer += id1 + ":" + id2 + ",";
                                processed[o.id] = o;
                            }
                        });
                    });
                    if (oQuestionObject.Xml.find("correctconnections connection").length > 0)
                        iScore = iScore * 100 / oQuestionObject.Xml.find("correctconnections connection").length;

                    if (oQuestionObject.KeyMode != bDisplayKey) {
                        //jsPlumb.detachEveryConnection();
                        jsPlumb.deleteEveryEndpoint();
                        if (!bDisplayKey) {
                            oQuestionObject.CreateEndpoints();
                            oQuestionObject.LoadAnswers();

                        } else {
                            $.each(oQuestionObject.Xml.find("correctconnections connection"), function (index, node) {
                                var correct = cStr(oQuestionObject.Answer).indexOf($(node).attr("id1") + ":" + $(node).attr("id2")) > -1;
                                jsPlumb.connect({
                                    source: oQuestion.find(".question").filter(function () { return $(this).data("boxid") == $(node).attr("id1"); })[0],
                                    target: oQuestion.find(".question").filter(function () { return $(this).data("boxid") == $(node).attr("id2"); })[0],
                                    paintStyle: { lineWidth: 10, strokeStyle: (correct ? '#72c028' : '#f56932') },
                                    endpointStyle: { fillStyle: (correct ? '#72c028' : '#f56932') },
                                    deleteEndpointsOnDetach: true
                                });
                            });
                        }
                    }
                } else {
                    sAnswer = oQuestionObject.Answer;
                }
            }
        }        
        iScore = Math.round(Math.max(0, iScore));               
        if (!oQuestionObject.KeyMode) {                  
            oQuestionObject.Score = iScore;
            var changed = oQuestionObject.Answer != sAnswer;
            oQuestionObject.Answer = sAnswer;
            var Answer = "", iQuizScore = 0;            
            $(_Quiz.Questions).each(function (i, o) { iQuizScore += o.Score; Answer += o.Answer + ";"; });
            iQuizScore = Math.round(iQuizScore / _Quiz.Questions.length);
            $.extend(_Quiz.Settings.Data[_Quiz.Settings.Index], { Answers: Answer, Score: iQuizScore });

            var iTotalScore = 0, iNumQuestions = 0;
            $.each(this.Settings.QuizArray, function () {
                iTotalScore += (this.Data[this.Index].Score*this.Quiz.Questions.length);
                iNumQuestions += this.Quiz.Questions.length;
            });            
            iTotalScore = iTotalScore / iNumQuestions;
            if (!bScoreOnly) {
                if (oQuestionObject.Score >= 50) oQuestionObject.CheckAnswerCounter++;
            }
            if (!this.Settings.FinalTest || bDisplayKey) {
                oQuestion.find(".progresscolor-taken").css("width", iScore + "%").find(".progressnumber-taken").html(replaceAll(_Translations["percent-correct"], "{0}", iScore))                
                
                _Quiz.Menu.find(".status span").html(iTotalScore + "%").end().find(".status .bar").css("width", iTotalScore + "%");
                oQuestionObject.Container.find(".display-key").toggleClass("disabled", oQuestionObject.CheckAnswerCounter < 2 || oQuestionObject.Score < 50).html(replaceAll(_Translations["display-answers"], "{0}", cStr(oQuestionObject.CheckAnswerCounter - 2)).split(oQuestionObject.CheckAnswerCounter >= 2 ? "(" : "___")[0]);
            }
            oQuestionObject.Nav.find("span").html(this.Settings.FinalTest && !bDisplayKey ? "" : oQuestionObject.Score + "%").end().find(".bar").css("width", (this.Settings.FinalTest ? (sAnswer ? 100 : 0) : oQuestionObject.Score) + "%");
            if (changed) _Section.trigger("changed", [oQuestionObject]);
        }
        
        oQuestionObject.KeyMode = bDisplayKey;
        oQuestion.toggleClass("keymode", bDisplayKey);
    }

    function fnRenderQuestion(iIndex, bAppend) {
        if (this.Settings.FinalTest && !this.Settings.KeyMode && cStr(this.CurrentQuestion) != "") this.save(this.CurrentQuestion);            
        this.CurrentQuestion = iIndex;
        
        var ExistingQuestion = _Section.find(".question-" + iIndex);
        if (!bAppend && ExistingQuestion.length == 0) _Section.find(".wrapper-main").html("");
        var oQuestionObject = this.Questions[iIndex];
        var oContainer = _Section.find(".question-template").clone().show()
            .find(".check-answers").click(function (e) {
                _Quiz.save(iIndex); $(this).parent()
                 .find(".progressbar").css({ "opacity": "0", "background": "#f00" }).animate({ opacity: 1 }, 100).animate({ "backgroundColor": "#aab8c4" }, 1000);                
            }).end()
            .find(".display-key").mousedown(function (e) { if (!oQuestionObject.KeyMode && $(this).hasClass("disabled")) return; _Quiz.save(iIndex, false, !oQuestionObject.KeyMode); e.stopPropagation(); e.stopImmediatePropagation(); return false; }).end()
            .find(".anchor").attr("name", "quiz_" + (iIndex + this.Settings.QuestionOffset)).end()
            .find(".prev-button").click(function () { if (_Quiz.CurrentQuestion > 0) _Quiz.render(_Quiz.CurrentQuestion - 1, false); }).end()
            .find(".next-button").click(function () { if (_Quiz.CurrentQuestion < _Quiz.Questions.length - 1) _Quiz.render(_Quiz.CurrentQuestion + 1, false); }).end()
            .find(".new-attempt").click(function () { _Quiz.Settings.Alerted = false; clsQuiz(Settings, _Section); }).end()
            .find(".submit-button").click(function () { if (_Quiz.Settings.KeyMode) { 
                _Section.trigger("save", []);
                _Section.trigger("close", []);
            } else { 
                var Submit = function () {                    
                    _Quiz.Settings.KeyMode = true;
                    $.each(_Quiz.Questions, function (i) { _Quiz.save(i); });
                    
                    _Section.trigger("save", []);
                    var e = {};
                    _Section.trigger("submitted", e);                                        
                    if (e.cancel) return;
                    _Quiz.render(0, false); 
                    
                    if(_Quiz.Settings.FinalTest){
                        fnShowMessage(replaceAll(_Translations["test-submitted"], "{0}", _Quiz.Settings.Data[_Quiz.Settings.Index].Score));                        
                    }
                };                                                                         
                if(_Quiz.Settings.FinalTest)
                    fnShowMessage(_Translations["submit-test-prompt"], [[_Translations["Yes"], function () { $("#metier-msg").detach(); Submit(); }], [_Translations["No"], function () { $("#metier-msg").detach(); }]], { NoAutoClose: true });
                else
                    Submit();                
            }       
            }).end()
            .find(".reset").click(function () {
                oQuestionObject.reset();
            }).end();
        if (ExistingQuestion.length > 0)
            ExistingQuestion.after(oContainer).detach();
        else
            oContainer.appendTo(_Section.find(".wrapper-main").append(iIndex == 0 || !bAppend ? "" : "<div><br /><br /><br /></div>"));

        if (this.Settings.FinalTest) {
            if(_Quiz.Settings.KeyMode)oContainer.find(".submit-button").removeClass("submit-button").find(".text").html(_Translations["close-button"]); 
            oContainer.find(".check-answers, .progressbar, .display-key, .quiz-buttons").remove();
            oContainer.find(".prev-button").toggleClass("disabled", iIndex == 0);
            oContainer.find(".next-button").toggleClass("disabled", this.Questions.length - 1 <= iIndex);            
            oContainer.find(".new-attempt").toggle(_Quiz.Settings.KeyMode && $.grep(_Quiz.Settings.Data,function(n,i){return n.Answers=="";}).length>0);
        } else oContainer.find(".finaltest-buttons").remove();
        
        if (oQuestionObject.Nav == null) oQuestionObject.Nav = $('<li><a class="roundbutton progress" href="#quiz_' + (iIndex+this.Settings.QuestionOffset) + '"><div class="bar"></div><div class="text"><strong>' + (iIndex + 1 + this.Settings.QuestionOffset) + '</strong>&nbsp;&nbsp;<span></span></div></a></li>').appendTo(_Quiz.Menu.find("ul:eq(0)")).find("a").click(function () { if (_Quiz.Settings.FinalTest) { _Quiz.render(iIndex, false); } });

        var oQuestion = oContainer.attr("class", "question-" + iIndex).find(oQuestionObject.ProblemType.MultipleChoice || oQuestionObject.ProblemType.TrueOrFalse ? ".quiz-MC" : (oQuestionObject.ProblemType.DragAndDrop ? ".quiz-DD" : (oQuestionObject.ProblemType.ConnectTheBoxes ? ".quiz-CL" : (oQuestionObject.ProblemType.FillInTheBlanks ? ".quiz-FB" : null))));
        oContainer.find(".quiz-area").children().each(function (i, o) {
            if (o != oQuestion[0]) $(o).remove();
        });
        oContainer.find(".counter").html(iIndex + 1 + this.Settings.QuestionOffset).end().find(".problem").html(oQuestionObject.Xml.find("questiontext").text()).end().mousedown(function (e) {
            if (oQuestionObject.KeyMode) { _Quiz.save(iIndex, false, !oQuestionObject.KeyMode); }
        });
        oQuestionObject.Container = oContainer;
        var contentHeight = 0;
        if (oQuestionObject.ProblemType.MultipleChoice || oQuestionObject.ProblemType.TrueOrFalse) {
            var hasBG = cStr(oQuestionObject.Xml.attr("background")) != "";
            if (hasBG) oQuestion.find(".questions").addClass("background");
            oQuestionObject.Xml.find("answer").each(function (i, o) {
                $("<div class='" + (hasBG ? "background" : "auto highlight") + " question" + (cStr(oQuestionObject.Answer) != "" && (("," + oQuestionObject.Answer + ",").indexOf("," + cStr(i) + ",") > -1) ? " selected" : "") + "'><div class='checkbox' /><div class='text' /><br /></div>").data("index", i).find(".text").html($(o).text()).end().appendTo(oQuestion.find(".questions"));
            });
            oQuestion.find(".question").click(function (e) {
                if (oQuestionObject.ProblemType.TrueOrFalse) {
                    oContainer.find(".question").removeClass("selected");
                    $(this).addClass("selected");
                } else {
                    $(this).toggleClass("selected");
                }
                });

            fnSetBackground(cStr(oQuestionObject.Xml.attr("background")), oQuestion, 0, 0);
        } else if (oQuestionObject.ProblemType.DragAndDrop) {
            var qAlign = oQuestionObject.Xml.find("drags").attr("align");
            var qWidth = cStr(oQuestionObject.Xml.find("drags").attr("width"), "225");
            var qHeight = oQuestionObject.Xml.find("drags").attr("height");            
            oQuestionObject.AnswerRecalc = function () {
                if (!this.Container) return;
                var Answer = $(this.Container).find(".quiz-DD .answer");
                $.each($(Answer), function () {
                    if ($(this).find(".question").length == 0) {
                        $(this).removeClass("has-items");
                    } else {
                        $(this).append($(this).find(".drop-area").height("6px").detach()).addClass("has-items").find(".question").css("position", "static");
                    }
                });
                var Questions = $(this.Container).find(".question");
                $.each(Questions, function () {
                    var p=$(this).parents(".answer");                    
                    $(this).css("width", (p.length == 0 ? qWidth : (p.width() - 13)) + "px").css("height", "auto").find("table").css("border-right",(p.length==0 || !$(p.data("node")).attr("color")?"":"3px solid " + $(p.data("node")).attr("color")));
                });
                var maxQuestionHeight = 0;
                $(this.Container).find(".questions .question").each(function () { maxQuestionHeight = Math.max(maxQuestionHeight, $(this).height()); });
                $(this.Container).find(".questions .question").each(function () { $(this).height(maxQuestionHeight + "px"); });                
            }
                        
            oQuestionObject.Xml.find("target").each(function (i, o) {
                var a = $("<div class='answer'><span class='text' /><div class='drop-area answer_" + iIndex + "_" + $(o).attr("id") + "'></div></div>").data("index", i).data("node", o).find(".text").html($(o).text()).end().appendTo(oQuestion.find(".answers"));
                
                if (oQuestionObject.Xml.find("targets").attr("position") == "absolute") {
                    oQuestion.find(".answers").css("position", "absolute");
                    oQuestion.addClass("invisible");
                    //oQuestion.addClass("visible");
                    a.css("position", "absolute").css("left", (cInt($(o).attr("x")) - 1) + "px").css("top", (cInt($(o).attr("y")) - 1) + "px").css("width", $(o).attr("width") + "px").css("height", $(o).attr("height") + "px");
                    contentHeight=Math.max(contentHeight,cInt($(o).attr("y")) + cInt($(o).attr("height")));                    
                } else if (oQuestionObject.Xml.find("targets").attr("position") == "vertical") {
                    oQuestion.find(".answers").addClass("vertical");
                    if (cStr($(o).attr("color")))
                        a.find(".text").css("color", $(o).attr("color"));                    
                    a.width((700 / oQuestionObject.Xml.find("target").length)-10);
                }else{
                    oQuestion.addClass("visible");
                    $("<br style='clear:both' />").appendTo(oQuestion.find(".answers"));
                }
            });

            oQuestionObject.Xml.find("drag").each(function (i, o) {
                var q = $("<div style='" + (qWidth ? "width:" + qWidth + "px;" : "") + (qHeight ? "height:" + qHeight + "px" : "") + "' class='question'><table><tr><td class='rubber'></td><td class='text'></td></tr></table></div>").data("index", i).find(".text").html($(o).text()).end().appendTo(oQuestion.find(".questions"));
                if (oQuestionObject.Xml.find("drags").attr("align") == "absolute") {
                    oQuestion.find(".questions").addClass("absolute");
                    q.css("left", $(this).attr("x") + "px").css("top", $(this).attr("y") + "px");
                }
                if (("," + oQuestionObject.Answer).indexOf("," + i + ":") > -1) {
                    var AnswerNode = oQuestionObject.Xml.find("target:eq(" + ("," + oQuestionObject.Answer).split("," + i + ":")[1].split(",")[0] + ")");
                    if (AnswerNode) _Section.find(".answer_" + iIndex + "_" + AnswerNode.attr("id")).parent().append(q);
                }
            });

            oQuestionObject.AnswerRecalc();
            if (qAlign == "top") oQuestion.find(".questions").addClass("top");
            var vShift = (qAlign == "top" && qAlign != "absolute" ? oQuestion.find(".questions").height() : 0);
            var hShift = (qAlign != "top" && qAlign != "absolute" ? oQuestion.find(".questions").width() : 0);

            if (oQuestionObject.Xml.find("targets").attr("position") == "absolute") oQuestion.find(".answers").css("top", vShift + 57);                        
           
            if (qAlign != "absolute") {
                var i = Math.max(cInt(oQuestion.find(".answers").height()), cInt(oQuestion.find(".question:last").height()) + cInt(oQuestion.find(".question:last").position().top)) - vShift-27;
                contentHeight = Math.max(i, contentHeight);
            }
            if (contentHeight > 0) oQuestion.height(contentHeight + vShift + 27);
            
            oQuestion.find(".answer").sortable({
                connectWith: ".quiz-DD .answer, .questions",
                items: "> .question",
                placeholder: "active",
                over: function (event, ui) {
                    $(this).addClass("active").find(".drop-area").animate({ height: ui.helper.height() + "px" }, 200);
                },
                out: function (event, ui) {
                    $(this).removeClass("active").find(".drop-area").height("6px");
                },
                receive: function (event, ui) {
                    oQuestionObject.AnswerRecalc(this, ui.item);
                },
                remove: function (event, ui) {
                    oQuestionObject.AnswerRecalc(this);
                },
                start: function (e, ui) { ui.helper.addClass("dragging"); $(document).data("disable-swipe", true); },
                stop: function (e, ui) { ui.item.removeClass("dragging"); $(document).data("disable-swipe", null); }
            });
            oQuestion.find(".questions").sortable({
                connectWith: ".quiz-DD .answer",
                placeholder: "active",
                revert: 200,
                start: function (e, ui) { ui.helper.addClass("dragging"); $(document).data("disable-swipe", true); },
                stop: function (e, ui) { ui.item.removeClass("dragging"); $(document).data("disable-swipe", null); },
                receive: function (event, ui) {
                    oQuestionObject.AnswerRecalc();
                }
            });
            
            oQuestion.find(".questions, .answers").disableSelection();                
            
            fnSetBackground(cStr(oQuestionObject.Xml.attr("background")), oQuestion, hShift, vShift);
        } else if (oQuestionObject.ProblemType.FillInTheBlanks) {
            var text = replaceAll(oQuestionObject.Xml.find("text").text(), "\n", "<br />");
            var re = /({)[^}]+(})/g, prevIndex = 0;
            for(var x=0;;x++){
                var match = re.exec(text);
                var a = $("<span class='text' />").html(text.substring(prevIndex, (match ? re.lastIndex - match[0].length : text.length))).appendTo(oQuestion.find(".answers"));
                if (match) {
                    var q = $("<div class='question'></div>").html(match[0].substring(1, match[0].length - 1)).appendTo(oQuestion.find(".questions"));
                    var a = $("<span class='answer' />").data("answer", q.html()).html("&nbsp;").appendTo(oQuestion.find(".answers"));                    
                }                
                prevIndex = re.lastIndex;
                if (!match) break;
            }
            $.each(oQuestionObject.Answer.split("|"), function (i,s) {
                if (s) {
                    var q = oQuestion.find(".questions .question:contains('" + s + "'):eq(0)");
                    if (q.length > 0) oQuestion.find(".answer").eq(i).append(q).addClass("full");
                }
            });
            oQuestion.find(".questions .question").sort(function () {
                return (Math.round(Math.random()) - 0.5)
            }).appendTo(oQuestion.find(".questions"));
            
            oQuestion.find(".answer").sortable({
                connectWith: ".quiz-FB .answer, .questions",
                items: "> .question",                
                over: function (event, ui) {
                    $(this).addClass("active");
                },
                out: function (event, ui) {
                    $(this).removeClass("active");
                },
                receive: function (event, ui) {
                    if ($(this).hasClass("full")) $(ui.sender).sortable('cancel');
                    $(this).addClass("full");
                },
                remove: function (event, ui) {
                    $(this).removeClass("full");
                }
            });

            oQuestion.find(".questions").sortable({
                connectWith: ".quiz-FB .answer"
            });

            oQuestion.find(".questions, .answers").disableSelection();
            oQuestion.find(".questions").height(oQuestion.height());

        } else if (oQuestionObject.ProblemType.ConnectTheBoxes) {            
            var arrange = oQuestionObject.Xml.find("boxes").attr("arrange");
            oQuestionObject.Xml.find("box").each(function (i, o) {                
                var q = $("<div class='question'><div class='text' /></div>").data("index", i).find(".text").html($(o).text()).end().appendTo(oQuestion.find($(this).attr("texton") == "left" ? ".questions" : ".answers")).data("boxid",$(this).attr("id"));                
                if (arrange != "auto") {
                    q.css("position", "absolute").css("left", ($(this).attr("x") - ($(this).attr("texton") == "left" ? (q).width() : 0)) + "px").css("top", ($(this).attr("y") - 20) + "px");
                    contentHeight = Math.max(contentHeight, cInt($(o).attr("y")) + 50);
                }
            });
            oQuestionObject.LoadAnswers = function () {                
                $.each(oQuestionObject.Answer.split(","), function (index, str) {
                    if (trim(str) != "") {
                        jsPlumb.connect({
                            source: oQuestion.find(".question").filter(function () { return $(this).data("boxid") == str.split(":")[0]; })[0],
                            target: oQuestion.find(".question").filter(function () { return $(this).data("boxid") == str.split(":")[1]; })[0]
                        });
                    }
                });
            }
            oQuestionObject.CreateEndpoints = function () {
                jsPlumb.addEndpoint(oQuestion.find(".questions .question"), {
                    isSource: true, isTarget: true, anchor: "RightMiddle", maxConnections: -1, deleteEndpointsOnDetach: false                    
                });

                jsPlumb.addEndpoint(oQuestion.find(".answers .question"), {
                    paintStyle: { width: 25, height: 21, fillStyle: '#666' },
                    isSource: true, isTarget: true,
                    anchor: "LeftMiddle",
                    deleteEndpointsOnDetach: false,
                    maxConnections: -1/*,
                    dropOptions: { activeClass: "active" },
                    hoverClass: "active",
                    cssClass:"active"*/
                });
                jsPlumb.makeSource(oQuestion.find(".questions .question"), { isSource: true, isTarget: true, anchor: "RightMiddle", maxConnections: -1, deleteEndpointsOnDetach: false });

                jsPlumb.makeTarget(oQuestion.find(".answers .question"),
                    { isSource: true, isTarget: true, deleteEndpointsOnDetach: false }
                  );
            };            
            jsPlumb.ready(function () {
                oQuestionObject.jsPlumb = true;
                jsPlumb.importDefaults({
                    Connector: ["Straight", { curviness: 150 }],
                    Anchors: ["RightMiddle", "LeftMiddle"],
                    ConnectionsDetachable: true                    
                });
                oQuestionObject.CreateEndpoints();
                                
                jsPlumb.bind("connection", function(info) {                    
                    $([info.source, info.target]).addClass("active");

                    info.connection.bind("click", function (conn, e) {
                        jsPlumb.detach(conn);                        
                    });

                });
                jsPlumb.bind("beforeDrop", function (info) {
                    var accept = true;
                    $.each([".questions", ".answers"], function (i,s) {                        
                        if ($("#" + info.sourceId).parents(s).length + $("#" + info.targetId).parents(s).length > 1) accept = false;                        
                    });                    
                    return accept;
                });

                jsPlumb.bind("connectionDetached", function (info) {
                    if (jsPlumb.getConnections({ source: info.source }).length == 0) $(info.source).removeClass("active");
                    if (jsPlumb.getConnections({ source: info.target }).length == 0) $(info.target).removeClass("active");                    
                });                
                oQuestionObject.LoadAnswers();
                _Quiz.save(iIndex, true, false);
            });
            if (contentHeight > 0) oQuestion.height(contentHeight + 27);
            if(oQuestionObject.Xml.find("boxes").attr("draw")=="false") oQuestion.addClass("invisible");
            fnSetBackground(cStr(oQuestionObject.Xml.attr("background")), oQuestion, 0, 0);            
        }        
        //Display correct answers
        if (this.Settings.FinalTest && this.Settings.KeyMode) this.save(this.CurrentQuestion);
        if (bAppend && iIndex > 0 && iIndex % 2 == 0) oContainer.css("page-break-before", "always");
        _Section.trigger("render", [oQuestionObject]);
        return oContainer;
    }

    function fnSetBackground(src, oQuestion, offsetX, offsetY) {
        if (src) {
            offsetX += 11;
            offsetY -= 3;
            var i = new Image();
            $(i).load(function () {
                var p = oQuestion.parents(".quiz-container");

                oQuestion.css("background", "url('" + src + "') no-repeat " + offsetX + "px " + offsetY + "px");
                offsetX = this.width + offsetX;
                if (offsetX > p.width()) {
                    p.css("position", "relative").css("left", (p.width() - offsetX) / 2).css("width", offsetX);
                }
                offsetY = this.height + offsetY;
                if (offsetY > oQuestion.height()) {
                    oQuestion.css("height", offsetY);
                }
            });
            i.src = src;
        }
    }
    function clsQuestion(xmlQuestion, Quiz, Index, Answer) {
        this.Xml = $(xmlQuestion);
        this.Quiz = Quiz;
        this.Index = Index;
        this.ProblemType = {
            DragAndDrop: this.Xml.attr("type") == "draganddrop",
            MultipleChoice: this.Xml.attr("type") == "multiplechoice",
            ConnectTheBoxes: this.Xml.attr("type") == "connecttheboxes",
            FillInTheBlanks: this.Xml.attr("type") == "fillintheblanks",
            TrueOrFalse: this.Xml.attr("type") == "trueorfalse"
        };
        this.reset = function (bInternal) {
            this.Answer = "";
            this.KeyMode = false;
            this.CheckAnswerCounter = 0;
            this.Score = 0;
            if (!bInternal) {
                this.Container = Quiz.render(this.Index);
                Quiz.save(this.Index, false, false);
            }
        };
        this.reset(true);
        this.Answer = cStr(Answer);
    }
    this.Initialized = true;
}
