﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.functions;
namespace Phoenix.LearningPortal.Exam
{
    public partial class Timeout : jlib.components.webpage
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            (Page.Master as Inc.Master.Default).clearWrapper().DisableLoginRedirect = true;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string sValue = Util.Permissions.decryptStringWithHash(jlib.net.HTTP.getCookieValue(Request.Cookies["user-authentication"]));
            if (sValue != "")
            {
                int iReleaseID = convert.cInt(parse.splitValue(sValue, " ", 4));
                if (iReleaseID > 0)
                {
                    ado_helper oData = new ado_helper("sql.dsn.cms");
                    DataTable oRelease = sqlbuilder.executeSelect(oData, "d_releases", "id", iReleaseID);
                    if (oRelease.Rows.Count > 0 && convert.cStr(oRelease.Rows[0]["type"]) == "exam")
                    {
                        DataTable oDBExam = sqlbuilder.getDataTable(oData, "select * from d_exams where ObjectID=@ObjectID and @ObjectDate between CreateDate and DeleteDate", "@ObjectID", oRelease.Rows[0]["ObjectID"], "@ObjectDate", oRelease.Rows[0]["ObjectDate"]);
                        if (oDBExam.Rows.Count > 0 && convert.cStr(oDBExam.Rows[0]["PlayerType"]) == "assessment" && convert.cStr(oRelease.Rows[0]["urlkey"]) != "")
                        {
                            pDefaultMessage.Visible = false;
                            pAssessmentMessage.Visible = true;
                            lLogin.NavigateUrl += "release_id=" + oRelease.Rows[0]["id"] + "&key=" + oRelease.Rows[0]["urlkey"];
                        }
                    }
                }
            }
        }
    }
}