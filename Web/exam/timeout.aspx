﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Phoenix.LearningPortal.Exam.Timeout" MasterPageFile="~/inc/master/default.master" Codebehind="timeout.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head runat="server">
    <title></title>
        <script src="../inc/js/jquery.js" type="text/javascript"></script> 
        <script src="../inc/js/json2.min.js" type="text/javascript"></script>
        <script src="../inc/js/daterangepicker.jQuery.js" type="text/javascript"></script>
        <script type="text/javascript" src="../inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
        <script src="../inc/js/functions.js?14" type="text/javascript"></script>
        <script src="js/post_it.js" type="text/javascript"></script>
        <script src="../inc/library/controls/jgrid/media/jgrid.js?2" type="text/javascript"></script>
        <link rel="stylesheet" media="screen" href="~/inc/library/controls/jcombo/media/jcombo.css" type="text/css" />
        <link rel="stylesheet" media="screen" href="css/exam.css" type="text/css" />
        <script type="text/javascript" src="../inc/library/controls/jcombo/media/jcombo.js"></script>        

</head>
<style type="text/css">

</style>
<body>
    <common:form id="frmMain" runat="server">    
    <h1>Your session has timed out</h1>
    <asp:PlaceHolder runat="server" ID="pDefaultMessage">
You will need to close this window, open <a href="http://mymetier.net">http://mymetier.net</a> in a new browser window, and play this exam again.
<br /><br /><button type="button" onclick="window.close();">Close Window</button>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="pAssessmentMessage" Visible="false">
You will need to log in again <common:hyperlink runat="server" ID="lLogin" NavigateUrl="/exam/register.aspx?" Text="here" />.
</asp:PlaceHolder>
    </common:form>
</body>
<script language="javascript">
    
    //alert(window.location);
</script>
</html>
</asp:Content>