﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.functions;
//using ExpertPdf.HtmlToPdf;
using EvoPdf;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Exam
{
    public partial class Player : jlib.components.webpage
    {
        Util.Classes.user m_oActiveUser;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_PreInit(object sender, EventArgs e)
        {
            try
            {
                (Page.Master as Inc.Master.Default).Page_PreInit(sender, e);
                (Page.Master as Inc.Master.Default).LookupUserFromHttpContext();

                m_oActiveUser = (Page.Master as Inc.Master.Default).ActiveUser;
                if (this.QueryString("pdf") == "true")
                {
                    PdfConverter oConverter = new PdfConverter();
                    oConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                    oConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                    oConverter.PdfDocumentOptions.JpegCompressionEnabled = false;
                    oConverter.LicenseKey = "72FyYHVwYHBgd25wYHNxbnFybnl5eXlgcA==";//"31FCUEVAUEFHQVBGXkBQQ0FeQUJeSUlJSVBA";

                    oConverter.PdfDocumentOptions.LeftMargin = 40;
                    oConverter.PdfDocumentOptions.RightMargin = 40;
                    oConverter.PdfDocumentOptions.TopMargin = 40;
                    oConverter.PdfDocumentOptions.BottomMargin = 40;
                    oConverter.PdfDocumentOptions.InternalLinksEnabled = true;
                    oConverter.PdfDocumentOptions.AvoidImageBreak = true;
                    oConverter.PdfDocumentOptions.AvoidTextBreak = true;
                    oConverter.PdfDocumentOptions.AvoidHtmlElementsBreakSelectors = new string[] { "table" };
                    oConverter.PdfDocumentOptions.ShowFooter = false;
                    oConverter.JavaScriptEnabled = true;
                    oConverter.ConversionDelay = 0;

                    string sHTML = Request["html"];// parse.replaceAll(Request["html"], "<head>", "<head><base href='" + Request.Url + "' />");
                    if (sHTML != "") sHTML = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n" + sHTML;
                    EvoPdf.Document doc = oConverter.ConvertHtmlToPdfDocumentObject(sHTML, Request.Url.ToString());
                    foreach (EvoPdf.PdfPage page in doc.Pages)
                    {
                        page.Margins.Bottom = 0;
                        page.Margins.Left = 0;
                        page.Margins.Right = 0;
                    }
                    Util.MyMetier.PDFAddFooter(doc, false, " ", true, true);
                    byte[] bPDF = doc.Save();

                    Response.ContentType = jlib.net.HTTP.lookupMimeType("Exam.pdf");
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + System.Web.HttpUtility.UrlEncode(this.QueryString("type") == "assessment" ? "Assessment.pdf" : "Exam.pdf"));
                    Response.BinaryWrite(bPDF);
                    Response.End();
                }
            (Page.Master as Inc.Master.Default).clearWrapper().LoginPage = "timeout.aspx";
                if (this.QueryString("chart") == "true")
                {
                    Response.ContentType = jlib.net.HTTP.lookupMimeType(".png");
                    cAssessmentChart.Series.Add("Actual");
                    cAssessmentChart.Series[0].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
                    cAssessmentChart.Series[0].BorderWidth = 2;
                    cAssessmentChart.Series.Add("Target");
                    cAssessmentChart.Series[1].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
                    cAssessmentChart.Series[1].BorderWidth = 2;
                    cAssessmentChart.Series[1].Color = System.Drawing.Color.Red;
                    //cAssessmentChart.Series[1].YAxisType = System.Web.UI.DataVisualization.Charting.AxisType.Secondary;
                    //cAssessmentChart.Series[1].BorderWidth = 2;
                    //

                    int iChartIndex = (this.QueryString("type").Contains("Operational") ? 0 : 1);
                    System.Drawing.Color[] oSeriesColor = new System.Drawing.Color[] { System.Drawing.Color.Navy, System.Drawing.Color.OrangeRed };
                    //cAssessmentChart.BackColor = System.Drawing.ColorTranslator.FromHtml(iChartIndex == 0 ? "#fefef9" : "#f9fcfe");
                    if (this.QueryString("bgcolor").Str() != "") cAssessmentChart.BackColor = System.Drawing.ColorTranslator.FromHtml(this.QueryString("bgcolor"));
                    //cAssessmentChart.BackColor = System.Drawing.ColorTranslator.FromHtml(iChartIndex == 0 ? "#d6e3bb" : "#e8eff9");


                    cAssessmentChart.Series[1].Color = (iChartIndex == 1 ? System.Drawing.Color.Navy : System.Drawing.Color.ForestGreen);
                    //if (iChartIndex == 1) {
                    //cAssessmentChart.Series[0].Color = oSeriesColor[0];
                    //    cAssessmentChart.Series[1].Color = oSeriesColor[1];
                    //}

                    for (int x = 0; x < Request.QueryString.Count; x++)
                    {
                        if (Request.QueryString.Keys[x].StartsWith("section_"))
                        {
                            cAssessmentChart.Series[0].Points.AddXY(parse.splitValue(Request.QueryString[x], "|", 0), convert.cInt(parse.splitValue(Request.QueryString[x], "|", 1)));
                            cAssessmentChart.Series[1].Points.AddXY(parse.splitValue(Request.QueryString[x], "|", 0), convert.cInt(parse.splitValue(Request.QueryString[x], "|", 2)));
                        }
                    }
                    cAssessmentChart.SaveImage(Response.OutputStream, System.Web.UI.DataVisualization.Charting.ChartImageFormat.Png);
                    Response.End();
                }
                if (!this.IsPostBack && this.QueryString("authentication") != "")
                {
                    Util.Permissions.logInIfRequired(this.QueryString("authentication"));
                    if (Util.Permissions.UserID == 0) Response.Redirect("timeout.aspx");
                }
                if (m_oActiveUser.ID == 0 && this.QueryString("action") == "save-answers")
                {
                    (Page.Master as Inc.Master.Default).Page_Init(null, null);
                    if (m_oActiveUser.ID == 0) Response.Write("{\"redirect\":\"timeout.aspx?timeout=true&referrer=" + HttpUtility.UrlEncode(Request.Url.AbsolutePath) + "\"}");
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bShowAnswers = convert.cBool(this.QueryString("answers", false));

                ado_helper oData = new ado_helper("sql.dsn.cms");
                AttackData.Attempt Attempt = null;
                if (this.QueryString("attempt_id").Int() > 0) Attempt = AttackData.Attempt.LoadById(this.QueryString("attempt_id").Int());
                if (Attempt == null && this.QueryString("attempt_id").Int() != -1)
                {
                    int rcoId = this.QueryStringInt("id");
                    //This only gets called when exam is loaded in preview mode from CMS
                    if (rcoId == 0 && this.QueryString("release_id").Int() > 0)
                        Response.Redirect("player.aspx?attempt_id=" + Util.Permissions.initiateAttempt(Request["release_id"].Int(), m_oActiveUser.ID, this.QueryStringInt("id"), this.QueryStringInt("course_id"), this.QueryString("scorm_attempt")));

                    Attempt = AttackData.Attempts.GetByField(UserID: m_oActiveUser.ID, RcoID: rcoId).OrderBy(AttackData.Attempts.Columns.CreateDate, jlib.DataFramework.QueryBuilder.OrderByDirections.Descending).Execute().FirstOrDefault();
                    if (Attempt != null) Response.Redirect("player.aspx?attempt_id=" + Attempt.ID);
                }

                if (this.QueryStringInt("attempt_id") > 0)
                {
                    if (Attempt.UserID != m_oActiveUser.ID && (!bShowAnswers || !m_oActiveUser.IsBehindAdmin))
                    {
                        if (m_oActiveUser.ID == 0)
                            Response.Write("You have been logged out due to inactivity.");
                        else
                            Response.Write("You're trying to resume an attempt that was initiated by another user.");

                        Response.End();
                    }
                    if (Attempt.StatusID == (int)Util.Data.AttemptState.Completed) bShowAnswers = true;
                }

                jlib.functions.json.JsonMapper.RegisterExporter<jlib.helpers.structures.collection_aux>(new jlib.functions.json.ExporterFunc<jlib.helpers.structures.collection_aux>(jlib.helpers.structures.collection.JSonSerialize));

                if (this.QueryString("action") == "save-answers")
                {
                    if (convert.cStr(Request.Form["_answers"]) != "")
                    {
                        decimal dPointsDelta = 0;
                        jlib.functions.json.JsonData oJSON = jlib.functions.json.JsonMapper.ToObject(Page.Request.Form["_answers"]);
                        for (int x = 0; x < oJSON.Dictionary.Count; x++)
                        {

                            DataTable oQuestion = sqlbuilder.getDataTable(oData, "select d_questions.Points, d_questions.QuestionType, d_question_options.ObjectID from d_questions LEFT OUTER JOIN l_row_links on id1=d_questions.ObjectID and table1='d_questions' and table2='d_question_options' and '" + Attempt.ObjectDate + "' BETWEEN l_row_links.CreateDate and l_row_links.DeleteDate LEFT OUTER JOIN d_question_options ON l_row_links.id2=d_question_options.ObjectID and d_question_options.IsCorrect=1 and '" + Attempt.ObjectDate + "' BETWEEN d_question_options.CreateDate AND d_question_options.DeleteDate where d_questions.ObjectID=" + oJSON.Dictionary[x].Key + " and '" + Attempt.ObjectDate + "' BETWEEN d_questions.CreateDate AND d_questions.DeleteDate");// and d_question_options.ObjectID is not null");
                            DataTable oAnswer = sqlbuilder.executeSelect(oData, "d_attempt_answers", "question_id", oJSON.Dictionary[x].Key, "attempt_id", this.QueryStringInt("attempt_id"));
                            if (oAnswer.Rows.Count == 0) oAnswer.Rows.Add(oAnswer.NewRow());

                            if (oQuestion.Rows[0]["QuestionType"].Str() == "text")
                            {
                                sqlbuilder.setRowValues(oAnswer.Rows[0], "attempt_id", this.QueryStringInt("attempt_id"), "question_id", oJSON.Dictionary[x].Key, "time_spent", convert.cInt(oJSON.Dictionary[x].Value[0]), "answer", oJSON.Dictionary[x].Value[1], "is_correct", true, "points", 0);
                            }
                            else
                            {
                                int iNumAnswers = 0, iNumCorrect = 0;
                                string[] sAnswers = parse.split(parse.stripCharacter(parse.replaceAll(oJSON.Dictionary[x].Value[2], ",,", ","), ","), ",");
                                for (int y = 0; y < sAnswers.Length; y++)
                                {
                                    if (sAnswers[y].Trim() != "")
                                    {
                                        if (oQuestion.Select("ObjectID=" + sAnswers[y]).Length > 0) iNumCorrect++;
                                        iNumAnswers++;
                                    }
                                }
                                decimal dScore = Math.Max(0, (iNumCorrect + (iNumCorrect - iNumAnswers)).Dec() / Math.Max(1, oQuestion.Select("ObjectID is not null").Length));
                                dPointsDelta += (dScore * convert.cDec(oQuestion.Rows.Count == 0 ? 0 : oQuestion.Rows[0]["Points"])) - convert.cDec(oAnswer.Rows[0]["points"]);
                                sqlbuilder.setRowValues(oAnswer.Rows[0], "attempt_id", this.QueryStringInt("attempt_id"), "question_id", oJSON.Dictionary[x].Key, "time_spent", convert.cInt(oJSON.Dictionary[x].Value[0]), "answer", oJSON.Dictionary[x].Value[1], "is_correct", (dScore == 1 ? true : false), "points", dScore * convert.cDec(oQuestion.Rows.Count == 0 ? 0 : oQuestion.Rows[0]["Points"]));
                            }

                            ado_helper.update(oAnswer);
                            sqlbuilder.getDataTable(oData, "delete from l_row_links where table1=@table1 and table2=@table2 and id1=@id1; insert into l_row_links (table1,table2,id1,id2, CreateDate, DeleteDate) select @table1,@table2,@id1, val, GetDate(), '" + DateTime.MaxValue + "' from SplitXml(@id2,',') ;", "@table1", "d_attempt_answers", "@table2", "d_question_options", "@id1", oAnswer.Rows[0]["id"], "@id2", parse.replaceXChars(parse.stripCharacter(oJSON.Dictionary[x].Value[2], ","), "0123456789,", ""));
                        }
                        if (dPointsDelta != 0)
                        {
                            Attempt.Points += dPointsDelta;
                            Attempt.Save();
                        }
                    }
                    if (convert.cStr(Request.Form["_suspend-data"]) != "") sqlbuilder.executeUpdate(oData, "d_attempts", "id", this.QueryStringInt("attempt_id"), "suspend_data", Request.Form["_suspend-data"]);
                    if (convert.cStr(Request.Form["_extra-data"]) != "")
                    {
                        jlib.functions.json.JsonData oJSON = jlib.functions.json.JsonMapper.ToObject(Page.Request.Form["_extra-data"]);
                        if (convert.cBool(oJSON["Finalize"]) || convert.cBool(oJSON["Timeout"]))
                        {
                            DataTable oDBExam = sqlbuilder.getDataTable(oData, "select * from d_exams where ObjectID in (select ObjectID from d_releases where id=" + Attempt.ReleaseID + ") and '" + (DateTime)Attempt.ObjectDate + "' between CreateDate and DeleteDate");
                            if (convert.cStr(oDBExam.Rows[0]["PlayerType"]) != "assessment" && (!convert.cBool(oDBExam.Rows[0]["Pausable"]) || convert.cBool(oJSON["Timeout"]))) sqlbuilder.executeUpdate(oData, "d_attempts", "id", Attempt.ID, "StatusID", (int)Util.Data.AttemptState.Completed);
                        }
                    }
                    Response.End();
                }

                if (Attempt == null) Response.Redirect("player.aspx?attempt_id=" + Util.Permissions.initiateAttempt(Request["release_id"].Int(), m_oActiveUser.ID, this.QueryStringInt("id"), this.QueryStringInt("course_id"), this.QueryString("scorm_attempt")));
                if (!this.QueryString("scorm_attempt").IsNullOrEmpty() && Attempt.ScormSession != this.QueryString("scorm_attempt"))
                {
                    Attempt.ScormSession = this.QueryString("scorm_attempt");
                    Attempt.Save();
                }

                DataTable oOffering = sqlbuilder.executeSelect(oData, "d_releases", "id", Attempt.ReleaseID);
                DataTable oDBExam1 = sqlbuilder.getDataTable(oData, "select * from d_exams where ObjectID=" + oOffering.Rows[0]["ObjectID"] + " and '" + Attempt.ObjectDate + "' between CreateDate and DeleteDate");
                var oExam = new Util.Exam.exam();
                oExam.populate(oDBExam1.Rows[0], Attempt.ObjectDate);
                oExam.MaxTime = Attempt.MaxTime;
                pAssessmentDetailsContainer.Visible = oExam.PlayerType == "assessment";

                if (oExam.Settings.ContainsKey("css")) lInlineCSS.Text = convert.cStr(oExam.Settings["css"]);
                if (!bShowAnswers && oExam.PlayerType != "case-test") oExam.removeAnswers(null);

                if (this.QueryString("action") == "close")
                {
                    DataTable oMaxValues = sqlbuilder.getDataTable(oData, "select sum(time_spent) as TOTAL_TIME, max(CASE WHEN d_attempts.MaxPoints=0 then 0 ELSE d_attempts.Points*100/d_attempts.MaxPoints END) as POINTS from d_attempts left outer join d_attempt_answers on attempt_id=d_attempts.id where deleted=0 and release_id=" + Attempt.ReleaseID + " and user_id=" + Attempt.UserID);
                    var statusUpdate = new { RcoId = Attempt.RcoID, UserId = Attempt.UserID, Score = oMaxValues.Rows[0]["points"].Int(), TimeUsedInSeconds = oMaxValues.Rows[0]["total_time"].Int(), CompletedDate = DateTime.Now, Status = "C", Id = Attempt.UserID };
                    Util.Phoenix.session session = new Util.Phoenix.session();
                    new Util.Phoenix.session().PhoenixPut(String.Format(Util.Phoenix.session.Queries.PerformanceRegister, statusUpdate.UserId, statusUpdate.RcoId), jlib.functions.json.DynamicJson.Serialize(statusUpdate));
                    session.PhoenixClearCache();
                    Util.Classes.user.getUser(Attempt.UserID, true);

                    Response.End();
                }

                DataTable oAnswers = sqlbuilder.getDataTable(oData, "select question_id, time_spent, answer, (SELECT rtrim(COALESCE(cast(id2 as varchar(255)), '')) + ',' from l_row_links WHERE table1='d_attempt_answers' and table2='d_question_options' and id1=d_attempt_answers.ID FOR XML PATH('')) as answer_ids from d_attempt_answers where answer is not null and attempt_id=@id", "@id", Attempt.ID);
                if (oExam.PlayerType == "case-test")
                {

                }
                else if (oExam.PlayerType == "prince_2f")
                {
                    var Questions = new List<Util.Exam.exam_question>();
                    oExam.Sections.ForEach(Section =>
                    {
                        Section.Questions.ForEach(Question =>
                        {
                            Questions.Add(Question);
                        });
                    });

                    AttackData.AttemptAnswers Order = AttackData.AttemptAnswers.GetByField(AttemptID: Attempt.ID).OrderBy(AttackData.AttemptAnswers.Columns.QuestionOrder).Execute();
                    if (Order.Count > 0 && Order[Order.Count - 1].QuestionOrder > 0)
                    {
                        foreach (var Answer in Order)
                        {
                            if (Answer.QuestionOrder < Questions.Count)
                            {
                                var Question = Questions.FirstOrDefault(x => x.ObjectID == Answer.QuestionID);
                                if (Question != null)
                                {
                                    Questions[Questions.IndexOf(Question)] = Questions[Answer.QuestionOrder];
                                    Questions[Answer.QuestionOrder] = Question;
                                }
                            }
                        }
                    }

                    //reduce # of questions, if set up this way
                    int limitMCQuestions = oExam.Settings.SafeGetValue("limit-mc-questions").Int();
                    if (limitMCQuestions > 0 && limitMCQuestions < Questions.Count)
                        Questions.RemoveRange(limitMCQuestions, Questions.Count - limitMCQuestions);

                    var oSectionCollection = new List<Util.Exam.exam_section>();
                    var oSection = new Util.Exam.exam_section();

                    pPMPBreakdown.Visible = bShowAnswers && Questions.Count > 0 && Questions[0].KnowledgeArea.IsNotNullOrEmpty();


                    if (pPMPBreakdown.Visible)
                    {
                        List<dynamic> scoreBreakdown = new List<dynamic>();
                        var knowledgeAreas = Questions.GroupBy(x => x.KnowledgeArea).OrderBy(x => x.Key).ToList();
                        knowledgeAreas.ForEach(area =>
                        {
                            scoreBreakdown.Add(new
                            {
                                Name = area.Key,
                                Questions = Questions.Where(q => q.KnowledgeArea == area.Key).Count(),
                                Correct = Questions.Where(q => q.KnowledgeArea == area.Key && (q.Answers.FirstOrDefault(answer => answer.IsCorrect != 0) == null || oAnswers.Select("question_id=" + q.ObjectID + " and ',' + answer_ids+',' like '%," + q.Answers.FirstOrDefault(answer => answer.IsCorrect != 0).ObjectID + ",%'").Length > 0)).Count()
                            });

                        });

                        //scoreBreakdown.Add(new { Name: "});
                        lPMPBreakdown.Text = "var _PMPScoreBreakdown={\"Sections\":" + jlib.functions.json.DynamicJson.Serialize(scoreBreakdown) + "};";
                    }

                    int maxPoints = 0;
                    foreach (var Question in Questions)
                    {
                        maxPoints += Question.Points;
                        if (oSection.Questions.Count == 0)
                        {
                            oSection.Name = (oSectionCollection.Count * 5 + 1) + "-";
                            oSectionCollection.Add(oSection);
                        }
                        if (oSectionCollection.Count == 1 && oExam.Sections.Count > 0)
                        {
                            oSection.Text = oExam.Sections[0].Text;
                        }
                        oSection.Questions.Add(Question);
                        if (oSection.Questions.Count == 5 || Questions[Questions.Count - 1] == Question)
                        {
                            oSection.Name += (((oSectionCollection.Count - 1) * 5) + oSection.Questions.Count);
                            oSection = new Util.Exam.exam_section();
                        }
                    }
                    //Update MaxPoints possible to get on exam. This is needed for proper score reporting to Phoenix
                    Attempt.MaxPoints = maxPoints;
                    Attempt.Save();
                    oExam.Sections = oSectionCollection;
                }

                lScript.Text += "var bShowAnswers=" + (bShowAnswers ? "true" : "false") + ";";
                lScript.Text += "var cExam=" + jlib.functions.json.JsonMapper.ToJson(oExam) + ";";

                DataTable oUser = sqlbuilder.executeSelect(oData, "d_users", "id", Attempt.UserID);
                DataTable oAttemptSettings = sqlbuilder.executeSelectFields(oData, "key_name, value", "d_temp_data", "table_name", "d_attempts", "pk_id", Attempt.ID);
                lScript.Text += "var cUser=" + convert.cJSON(oUser, true) + ";";
                lScript.Text += "var cTempData=" + convert.cJSON(oAttemptSettings, false) + ";";

                if (!bShowAnswers && !convert.cBool(oDBExam1.Rows[0]["Resumable"]) && oAnswers.Rows.Count > 0 && convert.cStr(oDBExam1.Rows[0]["PlayerType"]) != "assessment")
                {
                    Response.Clear();
                    Response.Write("This exam can not be resumed. Please close this window, and resume a different exam.");
                    Response.End();
                }
                else if (bShowAnswers && !convert.cBool(oDBExam1.Rows[0]["ReviewAnswers"]))
                {
                    Response.Clear();
                    Response.Write("This exam does not allow you to review your answers. Please close this window, and review answers of a different exam.");
                    Response.End();
                }
                lScript.Text += "var cAnswers=" + convert.cJSON(oAnswers, false, 0) + ";var cSuspendData=" + convert.cStr(Attempt.SuspendData, "{}") + ";";// +(bShowAnswers ? "var cPoints=" + oAttempt.Rows[0]["Points"] + ";var cMaxPoints=" + oAttempt.Rows[0]["MaxPoints"] + ";" : "");
                string sMajorPlayerType = (oDBExam1.Rows[0]["PlayerType"].Str() == "assessment" ? "assessment" : (oDBExam1.Rows[0]["PlayerType"].Str() == "case-test" ? "case" : "exam"));

                bSubmitButton.InnerText = jlib.helpers.translation.translate(this.Language, "Submit Exam", this, "Submit-" + convert.cCapitalizedStr(sMajorPlayerType));
                bPrintButton.InnerText = jlib.helpers.translation.translate(this.Language, "Print Exam", this, "Print-" + convert.cCapitalizedStr(sMajorPlayerType));
                hPlaybackPaused.Value = jlib.helpers.translation.translate(this.Language, "", this, "Your-" + sMajorPlayerType + "-is-paused");
                hTimeIsUp.Value = jlib.helpers.translation.translate(this.Language, "", this, "Time-is-up") + "<br /><br /><a href='javascript:void(0)' onclick='window.close();'>" + jlib.helpers.translation.translate(this.Language, "", this, "Click-here-to-exit") + "</a>";
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }

        }
    }
}