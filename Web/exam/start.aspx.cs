﻿//URL: https://secure.bids.no/exam/start.aspx?release_id=2&rco_id=3273664&classroom_id=3273672&scorm_attempt=1312266934453&home_url=https%3A%2F%2Fmymetier.net%2Filearn%2Fen%2Flearner%2Fjsp%2Fuser_home.jsp%3Fstart%3D20%26sortby%3D%26sortasc%3D%26searchText%3D%26slctstart%3D20
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Linq;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.functions;
using AttackData = Phoenix.LearningPortal.Data;
namespace Phoenix.LearningPortal.Exam
{
    public partial class Start : jlib.components.webpage
    {
        private DataTable m_oReleaseObject;
        private AttackData.Exam m_oExamObject;
        private AttackData.Folder m_oExamContainer;
        private ado_helper m_oData = new ado_helper("sql.dsn.cms");

        public Util.Classes.user activeUser;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            (Page.Master as Inc.Master.Default).clearWrapper();
            (Page.Master as Inc.Master.Default).Page_PreInit(sender, e);
            activeUser = (Page.Master as Inc.Master.Default).ActiveUser;
        }

        public DataTable Release
        {
            get
            {
                if (m_oReleaseObject == null)
                {
                    m_oReleaseObject = sqlbuilder.getDataTable(m_oData, "select top 1 * from d_releases where Deleted=0 and ObjectDate='9999-12-31 23:59:59.997' and ObjectID=@id order by id", "@id", ExamObject.ObjectID);
                    if (m_oReleaseObject.Rows.Count == 0)
                    {
                        //auto-create a release object
                        m_oReleaseObject.Rows.Add(sqlbuilder.setRowValues(m_oReleaseObject.NewRow(), "CreateDate", DateTime.Now, "Type", "exam", "ObjectDate", "9999-12-31 23:59:59.997", "ObjectID", ExamObject.ObjectID, "Name", "Latest"));
                        ado_helper.update(m_oReleaseObject);
                    }
                }
                return m_oReleaseObject;
            }
        }
        public AttackData.Exam ExamObject
        {
            get
            {
                if (m_oExamObject == null)
                {
                    m_oExamObject = AttackData.Folder.LoadByPk(Request["id"].Int()).GetChildren<AttackData.Exam>().FirstOrDefault();
                }
                return m_oExamObject;
            }
        }
        public AttackData.Folder ExamContainer
        {
            get
            {
                if (m_oExamContainer == null) m_oExamContainer = AttackData.Folder.LoadByPk(convert.cInt(Request["exam_id"], Request["course_id"]));
                return m_oExamContainer;
            }
        }
        public string CourseObjectType
        {
            get
            {
                return "exam";
            }
        }
        public string CoursePlayerType
        {
            get
            {
                return ExamObject.PlayerType;
            }
        }
        //public AttackData.Exam CourseObject {
        //    get {
        //        if (m_oCourseObject == null) {
        //            m_oCourseObject = AttackData.Exams.GetByField(ObjectID : Release.Rows[0]["ObjectID"].Int()).OrderByDescending(AttackData.Exams.Columns.DeleteDate).Execute().FirstOrDefault();                
        //        }
        //        return m_oCourseObject;
        //    }
        //}    

        //http://phoenix1/Web/exam/start.aspx?id=79855&course_id=107827&scorm_attempt=b4983da2-0e6d-4db7-8b13-8abe8a4aa044&class_id=1108
        protected void Page_Init(object sender, EventArgs e)
        {
            gAttempts.GridRowClientDel += new jlib.controls.jgrid.OnGridRowClientDelHandler(gAttempts_GridRowClientDel);
            gAttempts.GridBeforeDatabind += new jlib.controls.jgrid.OnGridBeforeDatabindHandler(gAttempts_GridBeforeDatabind);
            gAttempts.GridRowPrerender += new jlib.controls.jgrid.OnGridRowPrerenderHandler(gAttempts_GridRowPrerender);
            gAttempts.GridCustomData += new jlib.controls.jgrid.OnGridCustomDataHandler(gAttempts_GridCustomData);
        }

        void gAttempts_GridCustomData(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            if (e.Type.StartsWith("export-responses"))
            {
                int iReleaseID = Release.Rows[0]["ID"].Int();
                if (iReleaseID > 0 && (activeUser.IsBehindAdmin || activeUser.IsBehindSuperAdmin))
                {
                    AttackData.Releases Releases = AttackData.Releases.GetByField(ID: iReleaseID).Execute();
                    string sFileName = Util.Export.exportExamResponses(Releases, activeUser.ID);
                    e.ClientJSAfter = "$.infoBar(\"Download file from <a href='../download.aspx?file=" + System.Web.HttpUtility.UrlEncode(System.IO.Path.GetFileName(sFileName)) + "' target='_blank'>here</a>\", { ID: \"alert-once\", type: $.infoBar.type.ok});";
                    e.Return = true;
                }
            }
        }

        void gAttempts_GridRowPrerender(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            e.setColValue(cTimeSpent, format.descriptiveTime(convert.cInt(e.DBRow["total_time"])));
            if (gAttempts.Columns.Contains(cScore)) e.setColValue(cScore, convert.cInt(e.DBRow["Points"]) + " of " + convert.cInt(e.DBRow["MaxPoints"]) + " (" + convert.cInt(100 * convert.cInt(e.DBRow["Points"]) / convert.cInt(e.DBRow["MaxPoints"], 1)) + "%)");
            //e.setColValue(cOptions.Index,parse.replaceAll(e.getColValue(cOptions.Index),"#authentication#", System.Web.HttpUtility.UrlEncode(Authentication)));            
        }

        void gAttempts_GridBeforeDatabind(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            if (CourseObjectType == "exam" && activeUser.ID > 0)
            {
                gAttempts.Visible = true;
                gAttempts.DataSource = sqlbuilder.getDataTable(m_oData, "select d_attempts.*, (select sum(time_spent) from d_attempt_answers where attempt_id=d_attempts.id) as total_time from d_attempts where deleted=0 and release_id=" + Release.Rows[0]["id"] + " and user_id=" + activeUser.ID + " order by d_attempts.id desc");

                //Fix RcoIDs of past attempts, if the release has been changed to point to different RcoID
                foreach (DataRow row in gAttempts.DataSource.Rows)
                {
                    if (Request.QueryString["id"].Int() > 0 && Request.QueryString["id"].Int() != row["Rco_ID"].Int()) row["Rco_ID"] = Request.QueryString["id"].Int();
                }
                ado_helper.update(gAttempts.DataSource);
            }
        }

        void gAttempts_GridRowClientDel(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            sqlbuilder.executeUpdate(m_oData, "d_attempts", new object[] { "deleted", 1 }, "id", e.PK, "user_id", activeUser.ID, "release_id", Release.Rows[0]["id"]);
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            bool MultiAttempt = false;
            if (ExamContainer != null) MultiAttempt = ExamContainer.getSetting("Multiple-attempts").Bln();

            //if Danish case-test player, open window immediately        
            if (CoursePlayerType == "case-test" || !MultiAttempt)
            {
                DataTable oLatestAttempt = sqlbuilder.getDataTable(m_oData, "select top 1 d_attempts.* from d_attempts where deleted=0 and release_id=" + Release.Rows[0]["id"] + " and user_id=" + activeUser.ID + " order by d_attempts.id desc");
                if (oLatestAttempt.Rows.Count > 0 && Request.QueryString["id"].Int() > 0 && Request.QueryString["id"].Int() != oLatestAttempt.Rows[0]["Rco_ID"].Int())
                {
                    oLatestAttempt.Rows[0]["Rco_ID"] = Request.QueryString["id"].Int();
                    ado_helper.update(oLatestAttempt);
                }
                Response.Redirect("player.aspx?multiattempt=" + (MultiAttempt ? 1 : 0) + "&player_type=" + CoursePlayerType + "&" + (CoursePlayerType == "case-test" ? "answers=true&" : "") + (oLatestAttempt.Rows.Count == 0 ? "release_id=" + Release.Rows[0]["id"] : "attempt_id=" + oLatestAttempt.Rows[0]["id"]));
            }

            bStartButton.OnClientClick = "window.open('player.aspx?attempt_id=-1&" + this.Request.QueryString + "','_blank','menubar=0,location=0,status=0,directories=0,toolbar=0,resizable=1,fullscreen=1');";

            if (CourseObjectType == "exam")
            {
                string sType = (CoursePlayerType == "assessment" ? "assessment" : "exam");
                if (CoursePlayerType == "assessment")
                {
                    gAttempts.removeColumn(cScore);
                }
                bStartButton.Text = parse.replaceAll(bStartButton.Text, "exam", sType);
                bExamCompleted.Text = parse.replaceAll(bExamCompleted.Text, "exam", sType);
            }
            gAttempts.DeleteConfirmation = jlib.helpers.translation.translate(this.Language, "", this, "Attempt-delete-confirmation");
            cDate.HText = jlib.helpers.translation.translate(this.Language, "", this, "Start-Date");
            cTimeSpent.HText = jlib.helpers.translation.translate(this.Language, "", this, "Time-Spent");
            cScore.HText = jlib.helpers.translation.translate(this.Language, "", this, "Score");
            cOptions.HText = jlib.helpers.translation.translate(this.Language, "", this, "Options");

            pDownloadResponses.Visible = activeUser.IsBehindAdmin || activeUser.IsBehindSuperAdmin;
            lScript.Text += "var bResumable=" + (ExamObject.Resumable ? 1 : 0) + "; var bReviewAnswers = " + (ExamObject.ReviewAnswers ? 1 : 0) + ";";

            if(activeUser.ID == 0)
            {
                Response.Clear();
                Response.Write("You have been logged out due to inactivity.");
                Response.End();
            }
        }
    }
}