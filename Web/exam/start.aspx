﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Phoenix.LearningPortal.Exam.Start" MasterPageFile="~/inc/master/default.master" Codebehind="start.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head runat="server">
    <title></title>
        <script src="../inc/js/jquery.js" type="text/javascript"></script> 
        <script src="../inc/js/json2.min.js" type="text/javascript"></script>
        <script src="../inc/js/daterangepicker.jQuery.js" type="text/javascript"></script>
        <script type="text/javascript" src="../inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
        <script src="../inc/js/functions.js?14" type="text/javascript"></script>
        
        <script src="../inc/library/controls/jgrid/media/jgrid.js?2" type="text/javascript"></script>
        <link rel="stylesheet" media="screen" href="~/inc/library/controls/jcombo/media/jcombo.css" type="text/css" />        
        <link rel="stylesheet" media="screen" href="~/inc/css/new/tables.css" type="text/css" />
        <link rel="stylesheet" media="screen" href="css/exam.css" type="text/css" />
        <script type="text/javascript" src="../inc/library/controls/jcombo/media/jcombo.js"></script>        

</head>
<style type="text/css">

</style>
<body style="padding:10px">
    <common:form id="frmMain" runat="server">    
    <h1><common:label runat="server" id="tName" /></h1>
    <br />
    <common:button.button runat="server" id="bStartButton" buttonmode="buttoninput" style="background:url(images/start_lesson_arrow.png) #75be34 no-repeat 10px 50%; color:#fff; border:0; padding:1px 10px 2px 30px" text="Start exam" />    
        <input id="closeExam" type="button" style="background-color:#75be34; color:#fff; border:0; padding:1px 10px 2px 10px" value="Close exam" onclick="window.parent.fnRestoreNavigator()">
    <br /><br />
    
    <common:label runat="server" TranslationKey="Your-previous-attempts" Tag="h3" />

<jgrid:jGridAdo runat="server" DisplayFilter="false" DisplayAdd="false" id="gAttempts" Pagination="false" DataViewFiltering="false" BindDropdownByID="true" style="width:956px;" SortOrder="desc" JSName="oGrid" AutoFilter="true" SortCol="0" TAtt="class='listing assets' style='border-bottom:1px solid silver;'">
    <Cols>        
        <jgrid:col runat="server" id="cID" HText="ID" DataMember="#" ColType="Hide" PrimaryKey="true" FormatString="ID" SortField="ID" />
        <jgrid:col runat="server" id="cDate" HText="Start Date" DataMember="CreateDate" ColType="Text" FormatString="{0:d} {0:t}" FormatStringArguments="CreateDate" SortField="CreateDate" FField="CreateDate" FPartialMatch="true" FInputAtt="class='text daterangepicker'" />
        <jgrid:col runat="server" id="cTimeSpent" HText="Time Spent" DataMember="total_time" ColType="Text" FormatString="total_time" SortField="total_time" FField="total_time" FPartialMatch="true" CInputAtt="class='text'" />
        <jgrid:col runat="server" id="cScore" HText="Score" ColType="Text" FormatString="Points"  FPartialMatch="true" CInputAtt="class='text'" visible="false" />
        <jgrid:col runat="server" id="cStatusID" ColType="Hide" FormatString="StatusID"  FPartialMatch="true" CInputAtt="class='text'" />
                                    
        <jgrid:col id="cOptions" HAtt="class='device-info'" CAtt="class='functions'" runat="server" HText="Options" ColType="Options" />
    </Cols>
</jgrid:jGridAdo>  

<br />
<!--
<common:button.button runat="server" id="bExamCompleted" buttonmode="submitinput" style="background:url(images/start_lesson_arrow.png) #75be34 no-repeat 10px 50%; color:#fff; border:0; padding:1px 10px 2px 30px" TranslationKey="Set-exam-as-completed" />
-->
<asp:PlaceHolder runat="server" ID="pDownloadResponses">
<br /><br />
<a href="javascript:void(0)" onclick="oGrid.addRequest(oGrid.buildRequest('custom', 'Type','export-responses'));">Download all users' responses on this offering</a>
</asp:PlaceHolder>


<script language="javascript">
    oGrid.registerEventListener("GridRenderCell", function (oEvent) {
        if (oEvent.Data.Index > -1 && oEvent.Data.ColObject.ID == "cOptions") {
            oEvent.Data.HTML = 
            (bResumable&&oGrid.getData(oEvent.Data.Index,"cStatusID")!=2?"<a href='javascript:void(0);' onclick='window.open(&quot;player.aspx?attempt_id=" + oGrid.getData(oEvent.Data.Index, "cID") + "&quot;,&quot;_blank&quot;,&quot;menubar=0,location=0,status=0,directories=0,toolbar=0,resizable=1,left=0,top=0,width=" + (screen.availWidth-10) + ",height="+(screen.availHeight-60)+"&quot;);'><img src='../inc/images/icons/control_play.png' alt='" + $(".hResumeAttempt").val() + "' title='" + $(".hResumeAttempt").val() + "' /></a>  ":"")
            +(bReviewAnswers? "<a href='javascript:void(0);' onclick='window.open(&quot;player.aspx?answers=true&attempt_id=" + oGrid.getData(oEvent.Data.Index, "cID") + "&quot;,&quot;_blank&quot;,&quot;menubar=0,location=0,status=0,directories=0,toolbar=0,resizable=1,left=0,top=0,width=" + (screen.availWidth-10) + ",height="+(screen.availHeight-60)+"&quot;)'><img src='../inc/images/icons/book_open.png' alt='" + $(".hReviewAnswers").val() + "'  title='" + $(".hReviewAnswers").val() + "' /></a>  ":"");          
            // <a href='javascript:void(0);' onclick='fnJGridRowDel(this);'><img src='../inc/images/icons/cancel.png' alt='Delete this attempt'  title='Delete this attempt' /></a>";
        }
    });
    <common:label runat="server" id="lScript" />
        var button=$("input[id*='ctl00_c1_bStartButton']");
    button.attr("onclick",cStr(button.attr("onclick")).split(",fullscreen=1").join(",left=0,top=0,width=" + (screen.availWidth-10) + ",height="+(screen.availHeight-60)));
    var button = $("input[id*='ctl00_c1_bStartButton']");
    $("#closeExam").toggle(window.parent && window.parent.fnRestoreNavigator != null);
</script>
<common:hidden runat="server" CssClass="hResumeAttempt" TranslationKey="Resume-Attempt" />
<common:hidden runat="server" CssClass="hReviewAnswers" TranslationKey="Review-Answers" />
</common:form>
</body>
</html>
</asp:Content>