﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.functions;
using AttackData = Phoenix.LearningPortal.Data;
namespace Phoenix.LearningPortal.Exam
{
    public partial class Assessment : jlib.components.webpage
    {
        private Util.Classes.user activeUser = null;
        protected void Page_PreInit(object sender, EventArgs e)
        {

            (Page.Master as Inc.Master.Default).Page_PreInit(sender, e);

            (Page.Master as Inc.Master.Default).clearWrapper().LoginPage = "timeout.aspx";
            if (!this.IsPostBack && this.QueryString("authentication") != "")
            {
                Util.Permissions.logInIfRequired(this.QueryString("authentication"));
                if (Util.Permissions.UserID == 0) Response.Redirect("timeout.aspx");
            }
            activeUser = (Page.Master as Inc.Master.Default).ActiveUser;

            if (activeUser.ID == 0 && this.QueryString("action") == "save-answers")
            {
                Response.Write("{\"redirect\":\"timeout.aspx?timeout=true&referrer=" + HttpUtility.UrlEncode(Request.Url.AbsolutePath) + "\"}");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            bool bShowAnswers = convert.cBool(this.QueryString("answers", false));

            ado_helper oData = new ado_helper("sql.dsn.cms");
            AttackData.Attempt Attempt = AttackData.Attempt.LoadById(this.QueryString("attempt_id").Int());
            if (this.QueryStringInt("attempt_id") > 0)
            {
                if (Attempt.UserID != activeUser.ID && (!bShowAnswers || !activeUser.IsBehindAdmin))
                {
                    Response.Write("You're trying to resume an attempt that was initiated by another user.");
                    Response.End();
                }
                if (Attempt.StatusID == (int)Util.Data.AttemptState.Completed) bShowAnswers = true;
            }

            jlib.functions.json.JsonMapper.RegisterExporter<jlib.helpers.structures.collection_aux>(new jlib.functions.json.ExporterFunc<jlib.helpers.structures.collection_aux>(jlib.helpers.structures.collection.JSonSerialize));

            if (this.QueryString("action") == "save-answers")
            {
                if (convert.cStr(Request.Form["_answers"]) != "")
                {
                    decimal dPointsDelta = 0;
                    jlib.functions.json.JsonData oJSON = jlib.functions.json.JsonMapper.ToObject(Page.Request.Form["_answers"]);
                    for (int x = 0; x < oJSON.Dictionary.Count; x++)
                    {

                        DataTable oQuestion = sqlbuilder.getDataTable(oData, "select d_questions.Points, d_questions.QuestionType, d_question_options.ObjectID from d_questions LEFT OUTER JOIN l_row_links on id1=d_questions.ObjectID and table1='d_questions' and table2='d_question_options' and '" + Attempt.ObjectDate + "' BETWEEN l_row_links.CreateDate and l_row_links.DeleteDate LEFT OUTER JOIN d_question_options ON l_row_links.id2=d_question_options.ObjectID and d_question_options.IsCorrect=1 and '" + Attempt.ObjectDate + "' BETWEEN d_question_options.CreateDate AND d_question_options.DeleteDate where d_questions.ObjectID=" + oJSON.Dictionary[x].Key + " and '" + Attempt.ObjectDate + "' BETWEEN d_questions.CreateDate AND d_questions.DeleteDate");// and d_question_options.ObjectID is not null");
                        DataTable oAnswer = sqlbuilder.executeSelect(oData, "d_attempt_answers", "question_id", oJSON.Dictionary[x].Key, "attempt_id", this.QueryStringInt("attempt_id"));
                        if (oAnswer.Rows.Count == 0) oAnswer.Rows.Add(oAnswer.NewRow());

                        if (oQuestion.Rows[0]["QuestionType"].Str() == "text")
                        {
                            sqlbuilder.setRowValues(oAnswer.Rows[0], "attempt_id", this.QueryStringInt("attempt_id"), "question_id", oJSON.Dictionary[x].Key, "time_spent", convert.cInt(oJSON.Dictionary[x].Value[0]), "answer", oJSON.Dictionary[x].Value[1], "is_correct", true, "points", 0);
                        }
                        else
                        {
                            int iNumAnswers = 0, iNumCorrect = 0;
                            string[] sAnswers = parse.split(parse.stripCharacter(parse.replaceAll(oJSON.Dictionary[x].Value[2], ",,", ","), ","), ",");
                            for (int y = 0; y < sAnswers.Length; y++)
                            {
                                if (sAnswers[y].Trim() != "")
                                {
                                    if (oQuestion.Select("ObjectID=" + sAnswers[y]).Length > 0) iNumCorrect++;
                                    iNumAnswers++;
                                }
                            }
                            decimal dScore = Math.Max(0, (iNumCorrect + (iNumCorrect - iNumAnswers)).Dec() / Math.Max(1, oQuestion.Select("ObjectID is not null").Length));
                            dPointsDelta += (dScore * convert.cDec(oQuestion.Rows.Count == 0 ? 0 : oQuestion.Rows[0]["Points"])) - convert.cDec(oAnswer.Rows[0]["points"]);
                            sqlbuilder.setRowValues(oAnswer.Rows[0], "attempt_id", this.QueryStringInt("attempt_id"), "question_id", oJSON.Dictionary[x].Key, "time_spent", convert.cInt(oJSON.Dictionary[x].Value[0]), "answer", oJSON.Dictionary[x].Value[1], "is_correct", (dScore == 1 ? true : false), "points", dScore * convert.cDec(oQuestion.Rows.Count == 0 ? 0 : oQuestion.Rows[0]["Points"]));
                        }

                        ado_helper.update(oAnswer);
                        sqlbuilder.getDataTable(oData, "delete from l_row_links where table1=@table1 and table2=@table2 and id1=@id1; insert into l_row_links (table1,table2,id1,id2, CreateDate, DeleteDate) select @table1,@table2,@id1, val, GetDate(), '" + DateTime.MaxValue + "' from SplitXml(@id2,',') ;", "@table1", "d_attempt_answers", "@table2", "d_question_options", "@id1", oAnswer.Rows[0]["id"], "@id2", parse.replaceXChars(parse.stripCharacter(oJSON.Dictionary[x].Value[2], ","), "0123456789,", ""));
                    }
                    if (dPointsDelta != 0)
                    {
                        Attempt.Points += dPointsDelta;
                        Attempt.Save();
                    }
                }
                (Page.Master as Inc.Master.Default).InitiatePageRequestLogger(true);
                Response.End();
            }

            if (Attempt == null) Response.Redirect("assessment.aspx?attempt_id=" + Util.Permissions.initiateAttempt(this.QueryStringInt("release_id"), activeUser.ID, 0, 0, ""));

            if (Attempt != null && Attempt.RcoID > 0 && activeUser.ID > 0)
            {

                DataTable oMaxValues = sqlbuilder.getDataTable(oData, "select sum(time_spent) as TOTAL_TIME, max(CASE WHEN d_attempts.MaxPoints=0 then 0 ELSE d_attempts.Points*100/d_attempts.MaxPoints END) as POINTS from d_attempts left outer join d_attempt_answers on attempt_id=d_attempts.id where deleted=0 and release_id=" + Attempt.ReleaseID + " and user_id=" + activeUser.ID);
                var statusUpdate = new { RcoId = Attempt.RcoID, UserId = activeUser.ID, Score = oMaxValues.Rows[0]["points"].Int(), TimeUsedInSeconds = oMaxValues.Rows[0]["total_time"].Int(), CompletedDate = DateTime.Now, Status = "I", Id = activeUser.ID };
                Util.Phoenix.session session = new Util.Phoenix.session();
                new Util.Phoenix.session().PhoenixPut(String.Format(Util.Phoenix.session.Queries.PerformanceRegister, statusUpdate.UserId, statusUpdate.RcoId), jlib.functions.json.DynamicJson.Serialize(statusUpdate));
                session.PhoenixClearCache();
                Util.Classes.user.getUser(activeUser.ID, true);
            }

            DataTable oOffering = sqlbuilder.executeSelect(oData, "d_releases", "id", Attempt.ReleaseID);
            DataTable oDBExam1 = sqlbuilder.getDataTable(oData, "select * from d_exams where ObjectID=" + oOffering.Rows[0]["ObjectID"] + " and '" + Attempt.ObjectDate + "' between CreateDate and DeleteDate");
            Util.Exam.exam oExam = new Util.Exam.exam();
            oExam.populate(oDBExam1.Rows[0], Attempt.ObjectDate);
            oExam.MaxTime = Attempt.MaxTime;

            lInlineCSS.Text = oExam.Settings.SafeGetValue("css").Str();
            //if(!bShowAnswers && oExam.PlayerType != "case-test") oExam.removeAnswers(null);


            if (this.QueryString("action") == "close")
            {

                if (oExam.PlayerType == "assessment-dnv")
                {
                    dynamic data = jlib.functions.json.DynamicJson.Parse(Request.Form["data"]);
                    Util.Email.sendEmail("no-reply@metier.no", Attempt.User.Email, "", "Louise.Palm@dnvgl.com", oExam.Name + " results for " + Attempt.User.Email, "<h1>" + oExam.Name + " results</h1>Dear " + (Attempt.User.FirstName + " " + Attempt.User.LastName).Trim() + ",<br />The result of your assessment is included below.<br /><br />" + jlib.functions.convert.SafeGetProperty(data, "Message"), true, null);
                }

                if (activeUser.ID > 0)
                {

                    DataTable oMaxValues = sqlbuilder.getDataTable(oData, "select sum(time_spent) as TOTAL_TIME, max(CASE WHEN d_attempts.MaxPoints=0 then 0 ELSE d_attempts.Points*100/d_attempts.MaxPoints END) as POINTS from d_attempts left outer join d_attempt_answers on attempt_id=d_attempts.id where deleted=0 and release_id=" + Attempt.ReleaseID + " and user_id=" + activeUser.ID);
                    var statusUpdate = new { RcoId = Attempt.RcoID, UserId = activeUser.ID, Score = oMaxValues.Rows[0]["points"].Int(), TimeUsedInSeconds = oMaxValues.Rows[0]["total_time"].Int(), CompletedDate = DateTime.Now, Status = "C", Id = activeUser.ID };
                    Util.Phoenix.session session = new Util.Phoenix.session();
                    new Util.Phoenix.session().PhoenixPut(String.Format(Util.Phoenix.session.Queries.PerformanceRegister, statusUpdate.UserId, statusUpdate.RcoId), jlib.functions.json.DynamicJson.Serialize(statusUpdate));
                    session.PhoenixClearCache();
                    Util.Classes.user.getUser(activeUser.ID, true);

                    //AttackData.Folder RCO = AttackData.Folder.LoadByPk(Attempt.RcoID);

                    //bool bUpdateEnrollment = (RCO == null || RCO.ParentRcoID == 0);
                    //if (!bUpdateEnrollment) {
                    //    oMaxValues = util.executeDefaultFrontendConnection("select COUNT(*) AS NUM_LESSONS, SUM(CASE WHEN P.STATUS='C' THEN 1 ELSE 0 END) AS NUM_COMPLETE, SUM(CASE WHEN P.SCORE>0 THEN P.SCORE ELSE 0 END) AS POINTS, SUM(CASE WHEN P.TIME>0 THEN P.TIME ELSE 0 END) AS TOTAL_TIME from RCO R LEFT OUTER JOIN PERFORMANCE P ON P.RCO_ID=R.ID AND P.USER_ID=" + User.ID + " WHERE R.IS_PUBLISHED='Y' AND R.IS_DELETED='N' AND PARENT_RCO_ID=" + RCO.ParentRcoID);
                    //    if (convert.cInt(oMaxValues.Rows[0]["NUM_LESSONS"]) == convert.cInt(oMaxValues.Rows[0]["NUM_COMPLETE"])) {
                    //        util.executeDefaultFrontendConnection(string.Format("UPDATE PERFORMANCE SET STATUS='C', COMPLETED_DATE=SYSDATE, SCORE={2}, TIME={3} WHERE USER_ID={0} AND RCO_ID={1}", User.ID, RCO.ParentRcoID, convert.cDbl(oMaxValues.Rows[0]["points"]), convert.cDbl(oMaxValues.Rows[0]["total_time"])));
                    //        bUpdateEnrollment = true;
                    //    }
                    //}
                }
                Response.End();
            }

            lScript.Text += "var bShowAnswers=" + (bShowAnswers ? "true" : "false") + ";";

            DataTable oUser = sqlbuilder.executeSelect(oData, "d_users", "id", Attempt.UserID);
            DataTable oAttemptSettings = sqlbuilder.executeSelectFields(oData, "key_name, value", "d_temp_data", "table_name", "d_attempts", "pk_id", Attempt.ID);
            lScript.Text += "var cUser=" + convert.cJSON(oUser, true) + ";";
            lScript.Text += "var cTempData=" + convert.cJSON(oAttemptSettings, false) + ";";

            DataTable oAnswers = sqlbuilder.getDataTable(oData, "select question_id, time_spent, answer, (SELECT rtrim(COALESCE(cast(id2 as varchar(255)), '')) + ',' from l_row_links WHERE table1='d_attempt_answers' and table2='d_question_options' and id1=d_attempt_answers.ID FOR XML PATH('')) as answer_ids from d_attempt_answers where answer is not null and attempt_id=@id", "@id", Attempt.ID);
            if (!bShowAnswers && !convert.cBool(oDBExam1.Rows[0]["Resumable"]) && oAnswers.Rows.Count > 0)
            {// && convert.cStr(oDBExam1.Rows[0]["PlayerType"])!="assessment-dnv") {
                Response.Clear();
                Response.Write("This assessment can not be resumed.");
                Response.End();
            }
            else if (bShowAnswers && !convert.cBool(oDBExam1.Rows[0]["ReviewAnswers"]))
            {
                Response.Clear();
                Response.Write("This assessment does not allow you to review your answers. Please close this window, and review answers of a different assessment.");
                Response.End();
            }
            oAnswers = sqlbuilder.getDataTable(oData, "select question_id, time_spent, answer, (SELECT rtrim(COALESCE(cast(id2 as varchar(255)), '')) + ',' from l_row_links WHERE table1='d_attempt_answers' and table2='d_question_options' and id1=d_attempt_answers.ID FOR XML PATH('')) as answer_ids from d_attempt_answers where attempt_id=@id order by question_order", "@id", Attempt.ID);
            if (oExam.Settings["randomize-questions"].Bln())
            {
                Util.Exam.exam_section trueFalseSection = null;
                int counter = 0;
                foreach (DataRow answer in oAnswers.Rows)
                {
                    string path = FindQuestion(oExam.Sections, answer["question_id"].Int(), "");
                    if (!path.IsNullOrEmpty())
                    {
                        Util.Exam.exam_section section = oExam.Sections[path.SafeSplit("-", 1).Int()];
                        if (section.GetAllQuestions(null).Count > 0 && section.GetAllQuestions(null)[0].QuestionType == "tf")
                        {
                            trueFalseSection = section;
                            oExam.Sections.Remove(section);
                        }
                        else
                        {
                            oExam.Sections[path.SafeSplit("-", 1).Int()] = oExam.Sections[counter];
                            oExam.Sections[counter] = section;
                            counter++;
                        }
                    }
                }
                int limitMCQuestions = oExam.Settings["limit-mc-questions"].Int();
                if (limitMCQuestions > 0) while (oExam.Sections.Count > limitMCQuestions) oExam.Sections.RemoveAt(limitMCQuestions);
                oExam.Sections.Add(trueFalseSection);

                //remove all questions associated with this exam that will not be asked.
                AttackData.AttemptAnswers answers = AttackData.AttemptAnswers.GetByField(AttemptID: Attempt.ID).Execute();
                List<Util.Exam.exam_question> questions = new List<Util.Exam.exam_question>();
                oExam.Sections.ForEach(section => section.GetAllQuestions(questions));
                answers.ForEach(answer => { if (questions.Find(question => question.ObjectID == answer.QuestionID) == null) answer.Delete(); });
            }
            lScript.Text += "var cExam=" + jlib.functions.json.JsonMapper.ToJson(oExam) + ";";
            lScript.Text += "var cAnswers=" + convert.cJSON(oAnswers, false, 0) + ";var cSuspendData=" + convert.cStr(Attempt.SuspendData, "{}") + ";";// +(bShowAnswers ? "var cPoints=" + oAttempt.Rows[0]["Points"] + ";var cMaxPoints=" + oAttempt.Rows[0]["MaxPoints"] + ";" : "");

        }
        private string FindQuestion(List<Util.Exam.exam_section> sections, int id, string path)
        {
            string ret = null;
            for (int y = 0; y < sections.Count; y++)
            {
                var section = sections[y];
                for (var x = 0; x < section.Questions.Count; x++)
                {
                    if (section.Questions[x].ObjectID == id) return path + "-" + y + "-" + x;
                }
                ret = FindQuestion(section.Sections, id, path + "-" + y);
                if (!ret.IsNullOrEmpty()) return ret;
            }
            return ret;
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "translation", "$(document).data(\"__translation\"," + jlib.helpers.translation.getEntries("en", Page, new List<string>() { "/exam/assessment.aspx", Page.Request.Url.AbsolutePath, "/inc/master/courseplayer.master" }).ToJSON() + ");", true);
        }
    }
}