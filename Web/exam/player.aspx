﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Phoenix.LearningPortal.Exam.Player" MasterPageFile="~/inc/master/default.master" Codebehind="player.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head runat="server">
    <title></title>
        <script src="<%=ResolveClientUrl("~/inc/js/jquery.js")%>" type="text/javascript"></script> 
        <script src="<%=ResolveClientUrl("~/inc/js/json2.min.js")%>" type="text/javascript"></script>
        <script src="<%=ResolveClientUrl("~/inc/js/daterangepicker.jQuery.js")%>" type="text/javascript"></script>
        <script type="text/javascript" src="<%=ResolveClientUrl("~/inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.js")%>"></script>
        <script src="<%=ResolveClientUrl("~/inc/js/functions.js?14")%>" type="text/javascript"></script>
        
        <script src="js/post_it.js" type="text/javascript"></script>
        <script src="<%=ResolveClientUrl("~/inc/library/controls/jgrid/media/jgrid.js?2")%>" type="text/javascript"></script>
        <link rel="stylesheet" href="<%=ResolveClientUrl("~/inc/library/controls/jcombo/media/jcombo.css")%>" type="text/css" />
        <link rel="stylesheet" href="css/exam.css?3" type="text/css" />
        <script type="text/javascript" src="<%=ResolveClientUrl("~/inc/library/controls/jcombo/media/jcombo.js")%>"></script>  
        <script src="<%=ResolveClientUrl("~/inc/library/ckeditor/ckeditor.js")%>" type="text/javascript"></script>      
        <script type="text/javascript" src="<%=ResolveClientUrl("~/inc/library/ckeditor/adapters/jquery.js")%>"></script>
        <style type="text/css">
        <common:label runat="server" id="lInlineCSS" />
        </style>
</head>
<body>    
    <common:form id="frmMain" runat="server">
    <common:chart Visible="false" id="cAssessmentChart" runat="server" Width="570px" Height="400px" Palette="SemiTransparent" BorderlineColor="#e5e5e5" BorderlineWidth="2" BorderlineDashStyle="Solid" backcolor="#f9fcfe" imagetype="Png">	
    <legends>
		<asp:legend LegendStyle="Table" IsTextAutoFit="False" Docking="Right" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8.25pt, style=Bold" Alignment="Center"></asp:legend>
	</legends>
	
	<chartareas>
		<asp:chartarea Name="ChartArea1" BorderColor="64, 64, 64, 64" BorderDashStyle="Solid" BackSecondaryColor="White" BackColor="Transparent" ShadowColor="Transparent">
			<area3dstyle Rotation="10" perspective="10" Inclination="15" IsRightAngleAxes="False" wallwidth="0" IsClustered="False"></area3dstyle>			
            
			<axisy linecolor="64, 64, 64, 64" IsLabelAutoFit="False" IsStartedFromZero="true" Title="" Interval="1">
				<labelstyle font="Trebuchet MS, 8.25pt, style=Bold" />
				<majorgrid interval="1" linecolor="64, 64, 64, 64" />
				<majortickmark interval="1" enabled="true" />                
			</axisy>
            
			<axisx linecolor="64, 64, 64, 64" IsLabelAutoFit="False" Title="Section">
				<labelstyle font="Trebuchet MS, 8.25pt" Angle="-90" Interval="1" />
				<majorgrid interval="1" linecolor="64, 64, 64, 64" />				
                <majortickmark interval="1" enabled="True" />
			</axisx>
		</asp:chartarea>
	</chartareas>
</common:chart>

    <table class="player">
    <tr class="top">
        <td class="left"></td>
        <td class="middle"></td>
        <td class="right"></td>
    </tr>
    <tr class="middle" style="height:100%">
        <td class="left"></td>
        <td class="middle">
            <div class="header">            
                <h1 id="exam-title"></h1>
                <div id="logo"></div>
                <div class="timer">                                        
                    <div class="timer-clock">
                        <div class="clock">
                        <common:label runat='server' TranslationKey="Test" />: <span class="total">-</span>&nbsp;<a href="javascript:void(0);" class="pause-button"><img src="../inc/images/icons/control_pause.png" alt="Pause Exam" style="border:0;position:relative;top:3px" /></a>&nbsp;
                        </div>
                        <div style="float:left;width:80px;"><common:label runat='server' TranslationKey="Progress" />: </div><div id="progress-bar" style="float:left; margin:2px 0 0 4px; border:1px solid black; height:10px; background-color:#d1d1d1; width:64px;"><div style="width:50%; background-color:green; height:10px;"></div></div><div id="progress-text" style="float:left;font-size:10px"></div>
                        <div class="score">
                            <br /><div style="float:left;width:80px;"><common:label runat='server' TranslationKey="Score">Score</common:label>: </div><div id="score-bar" style="float:left; margin:2px 0 0 4px; border:1px solid black; height:10px; background-color:#d1d1d1; width:64px;"><div style="width:50%; background-color:green; height:10px;"></div></div><div id="score-text" style="float:left;font-size:10px"></div>
                        </div>
                        <br />
                    </div>
                    <button id="bSubmitButton" runat="server" class="submit-exam" type="button" style="padding:0;margin:0;height:18px;font-size:10px" />
                    <button id="bPrintButton" runat="server" class="print-exam" type="button" style="padding:0;margin:0;height:18px;font-size:10px" />                    
                    <button id="closeExam" type="button" style="padding:0;margin:0;height:18px;font-size:10px" onclick="window.parent.fnRestoreNavigator()">Close exam</button>
                </div>                
            </div>
            <div class="player-frame">
            <div class="player-content" style="bottom:10px;">
             <asp:PlaceHolder runat="server" ID="pPMPBreakdown" Visible="false">                    
                 <div id="pmpScoreBreakdownContainer">                     
                    <h2>Score by knowledge area</h2><a onclick="$('#pmpScoreBreakdown').toggle();$(window).resize()" style="float:left; margin:5px 0 0 10px;" href="#">[show/hide]</a>
                    <table id="pmpScoreBreakdown"  >                        
                        <thead>
                            <tr><th>Knowledge Area</th><th>Number of questions</th><th>Number of correct answers</th><th>% Correct</th></tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <script>
                        <common:label runat="server" ID="lPMPBreakdown" />
                            if(_PMPScoreBreakdown && _PMPScoreBreakdown.Sections){
                                var questions=0, correct=0;
                                $.each(_PMPScoreBreakdown.Sections,function(){
                                    var percent=Math.round(this.Questions>0? (100*this.Correct/this.Questions) : 100);
                                    $("#pmpScoreBreakdown tbody").append("<tr class='" + (percent>=80?"good":"bad") + "'><td>" + this.Name + "</td><td>" + this.Questions + "</td><td>" + this.Correct + "</td><td>" + percent + "%</td></tr>");
                                    questions+= this.Questions;
                                    correct+= this.Correct;                                    
                                });
                                var percent=Math.round(questions>0? (100*correct/questions) : 100);
                                $("#pmpScoreBreakdown tbody").append("<tr class='" + (percent>=80?"good":"bad") + "'><td><b>Total</td><td>" + questions + "</td><td><b>" + correct + "</b></td><td><b>" + percent + "%</b></td></tr>");
                            }                            
                    </script>
                     </a>
            </asp:PlaceHolder>
            <table style="width:100%;" class="auto-size player-container" absolute-height-adjustment="17">
            <tr>
            <td style="width:80%;">
                <div style="height:450px" min-height="200" class="player-section auto-size">
                    <div id="exam-content" class="auto-size textarea" height-adjustment="10" onselectstart="return false;" ondragstart="return false;" style="overflow:scroll; height:300px;margin:5px;"></div>
                    <div id="question-nav"><br style="clear:both" /></div>                    
                </div>
                <div style="height:3px;border-top:1px solid #b9d1ea;border-bottom:1px solid #d4d9dc; background-color:#a0a0a0; cursor:n-resize" class="draggable-divider-v"></div>
                <div style="padding:3px 10px 3px 10px; margin-right:5px" min-height="100" class="player-section" id="exam-notes">
                    <div class="player-section-header"><common:label runat='server' TranslationKey="My-Notes" /></div><div class="player-section-resize" style="float:right;position:relative;left:5px"><div class="min"></div><div class="max"></div></div><br style="clear:both" />
                    <textarea class="auto-size textarea" id="user-notes" absolute-height-adjustment="35" style="width:100%;"></textarea>                    
                </div>
            </td>
            <td style="width:3px;border-left:1px solid #b9d1ea;border-right:1px solid #d4d9dc; background-color:#a0a0a0; cursor:e-resize" class="draggable-divider-h"></td>
            <td>
                <div min-height="200" style="padding:3px 10px; margin-right:5px;" class="player-section exam-text" id="exam-text" absolute-height-adjustment="40">
                    
                    <div style="float:right;" id="case-print-exam-text"><img src="../inc/images/icons/printer.png" onclick="cExam.print('<div class=\'exam-text\'>' + $('#exam-text .textarea').html() + '</div>');" /></div>
                    <div class="player-section-header" style="width:200px">                    
                        <ul id="exam-text-selector" class="select-menu no-icons" _onchange="fnUpdateExamText($(this).find('a:eq(0)').attr('value'));">
                            <li>
                            <a class="active" href="javascript:void(0)"></a>
                            <ul>            
                                <li><a id="text-scenario-booklet"><common:label runat="server" TranslationKey="Scenario-Booklet" /></a></li>            
                                <li><a id="text-project-scenario"><common:label runat="server" TranslationKey="Project-Scenario" /></a></li>
                                <li style="display:none"><a id="text-additional-information"><common:label runat="server" TranslationKey="Additional-Information" /></a></li>
                                <li><a id="text-help"><common:label runat="server" TranslationKey="Help" /></a></li>
                            </ul>
                            </li>
                        </ul>                        
                    </div>
                    <div class="player-section-resize" style="float:right;position:relative;left:5px"><div class="min"></div><div class="max"></div></div><br style="clear:both" />
                    <div class="auto-size textarea highlight-area" height-adjustment="-5" style="overflow-y:scroll; height:300px; width:100%;margin-top:5px;"></div>
                    
                </div>
                <div style="height:3px;border-top:1px solid #b9d1ea;border-bottom:1px solid #d4d9dc; background-color:#a0a0a0; cursor:n-resize" class="draggable-divider-v"></div>
                
                <div min-height="100" style="padding:3px 10px; margin-right:5px" class="player-section" id="exam-bookmarks"><common:label runat='server' TranslationKey="My-Bookmarks" Tag="div" CssClass="player-section-header" /> <div class="player-section-resize" style="float:right;position:relative;left:5px"><div class="min"></div><div class="max"></div></div><br style="clear:both" />
                    <div class="auto-size textarea" absolute-height-adjustment="35" style="overflow-y:scroll; width:100%"><common:label runat="server" TranslationKey="Bookmark-instructions" /></div>
                </div>
                <asp:PlaceHolder runat="server" ID="pAssessmentDetailsContainer">
                    <div style="padding:13px 10px; margin-right:5px;color:Black" class="player-section" id="assessment-details">
                    <b>Name:</b> <span class="d_users.first_name"></span> <span class="d_users.last_name"></span>
                    <br /><b>Role:</b> <span class="d_temp_data.role_name"></span>
                    <!--<br /><b>Site:</b> Rome
                    <br /><b>Manager:</b> F. Concari
                    <br /><b>Project:</b> Deep Sea-->
                    <br />
                    <br />
                    <div id="assessment-nav"></div>                                
                    <br />
                    <br />
                    <b>Legend:</b>
                    <br />1. <b>F</b>undamental
                    <br />2. <b>O</b>perational
                    <br />3. <b>M</b>aster
                    <br />4. <b>E</b>xpert
                    </div>
                </asp:PlaceHolder>               
            </td>
            </tr>
            </table>
                
            </div>
            </div>
        </td>
        <td class="right"></td>
    </tr>
    <tr class="bottom">
        <td class="left"></td>
        <td class="middle"></td>
        <td class="right"></td>
    </tr>
    </table>
    
    </common:form>
</body>

<script language="javascript" type="text/javascript">
    $("#closeExam").toggle(window.parent && window.parent.fnRestoreNavigator != null);

    setInterval(function () { $.get("../index.aspx?" + new Date().getTime()); }, 1200000);
    var iMinWidth = 1010; var iMinHeight = 469;
    var iCurrHeight = 0; var iCurrWidth = 0;
    $(".draggable-divider-h,.draggable-divider-v").mousedown(function (e) { $(document).data("draggable", this); });
    $(document).mouseup(function () { $(this).data("draggable", null); }).mousemove(function (e) {
        if ($(this).data("draggable") && e.which == 0) $(this).data("draggable", null);
        if ($(this).data("draggable")) {
            var oDrag = $($(this).data("draggable"));
            var o = oDrag.parent(); var p = oDrag.prev(); var n = oDrag.next();
            if (oDrag.hasClass("draggable-divider-v")) {
                var i = (Math.max(e.pageY - p.offset().top, cInt(p.attr("min-height"))));
                var j = iCurrHeight - o.offset().top-oDrag.height()-cInt(n.attr("min-height"))-cInt(o.find(".player-container").attr("absolute-height-adjustment"));
                p.height(Math.min(i, j) + "px");                
            } else {
                p.width((e.pageX - p.offset().left) + "px");
            }
            fnResizeAutoSizedControls();
        }
    });
    $(window).resize(function(){
        if(cExam.printMode)return;
        iCurrHeight = Math.max(iMinHeight, $(window).height());
        iCurrWidth = Math.max(iMinWidth, $(window).width());
        $("table.player").width(($(window).width() - 0) + "px").height(($(window).height() - (fnGetIEVersion()>0 && fnGetIEVersion()< 8 ? 100 : 0)) + "px"); var bOverflow = $(window).height() < iMinHeight || $(window).width() < iMinWidth; $(document.body).css("overflow", (bOverflow ? "scroll" : "hidden"));
        fnResizeAutoSizedControls();
        $(".draggable-divider-h,.draggable-divider-v").each(function (i, o) {
            var oDrag = $(o);
            var o = oDrag.parent(); var p = oDrag.prev(); var n = oDrag.next();
            if (oDrag.hasClass("draggable-divider-v")) {
                var i = (Math.max(p.height(), cInt(p.attr("min-height"))));
                var j = iCurrHeight - o.offset().top - oDrag.height() - cInt(n.attr("min-height")) - cInt(o.find(".player-container").attr("absolute-height-adjustment"));
                p.height(Math.min(i, j) + "px");                
            } else {
                p.width(p.width() + "px");
            }
        });
    });
    
    function fnResizeAutoSizedControls() {    
        $(".auto-size:visible").each(function (i, o) {        
            o = $(o);            
            if (o.attr("height-adjustment")) {
                var x = 0; o.siblings("div:visible~div:visible").each(function (j, p) { x += $(p).height(); });
                var i=cInt(o.attr("resize-parent-nesting"));
                if((i==2?o.parent().parent():o.parent()).css("height")=="100%")o.css("height","100%");
                else o.height(Math.max(20, (i==2?o.parent().parent().height():o.parent().height()) - o[0].offsetTop - x - Number(o.attr("height-adjustment"))) + "px");                
            }
            if (o.attr("absolute-height-adjustment")) o.height(Math.max(20, Math.max($(window).height(), iMinHeight) - o.offset().top - Number(o.attr("absolute-height-adjustment"))) + "px"); 
            if (o.attr("absolute-width-adjustment")) o.width(Math.max(20, Math.max($(window).width(), iMinWidth) - o.offset().left - Number(o.attr("absolute-width-adjustment"))) + "px"); 
        });        
    }
    //fnResizeAutoSizedControls();
    
    $(".select-menu ul li a").click(function () {
        var oUL=$(this).parents(".select-menu:eq(0)");
        oUL.find("a.active").text($(this).text()).attr("value", this.id);        
        if (oUL.data("onchange_set") == null)
            oUL.bind("onchange", function () { eval(oUL.attr("_onchange")) }).data("onchange_set", "1");
               
        oUL.trigger("onchange");
    });
    $(".player-section-resize .min").click(function () { var o = $(this).parents(".player-section:eq(0)"); if (o.data("state") == "max") { o.css("position", "static").css("z-index", "0"); o.data("state", null); } else { if(o.data("state")!="min")o.data("original-height", o.height()).data("state", "min"); o.height("14px"); o.siblings(".player-section").height(iCurrHeight-o.parent().offset().top-o.siblings(".draggable-divider-v").height()-cInt(o.parent().find(".player-container").attr("absolute-height-adjustment")) + "px");
    
    fnResizeAutoSizedControls(); } });
    $(".player-section-resize .max").click(function () { var o = $(this).parents(".player-section:eq(0)"); if (o.data("state") == "min") { o.height(o.data("original-height") + "px"); o.data("state", null); } else { if(o.data("state")!="max")o.data("original-height", o.height()).data("state", "max"); o.css("position", "absolute").css("z-index", "1000").css("top", "10px").css("bottom", "10px").css("left", "10px").css("right", "10px").css("height","auto");o.find(".auto-size").css("height","96%"); } });
<common:label runat="server" id="lScript" />

function fnUpdateExamText(sSection, bFlash, sText){
    if($("#exam-text").length==0)return;
    if(sSection=="text-help")$("#exam-text .textarea").html(sText?sText:"<b>Help not currently available</b>");
    else if(sSection=="text-additional-information"){    
        var oCtrl=$(document).data("active-section");
        if(sText==null)sText=($(oCtrl).data("object").Text!=""? $(oCtrl).data("object").Text : ($(oCtrl).data("object").Settings["parent-additional-info"]=="1"? $(oCtrl).parents("div.box").data("object").Text:""));
        $("#exam-text .textarea").html(sText!=""?sText: "No section is currently active");
        //clsPostIt(true,"section-"+$(oCtrl).parents("div.box").data("object").ObjectID,cSuspendData["post-it"],fnPostItCallback,$(".highlight-area")[0],2);
    }else if(sSection=="text-scenario-booklet"){
        $("#exam-text .textarea").html(sText?sText:cExam.IntroText);
        //clsPostIt(true,"scenario-booklet",cSuspendData["post-it"],fnPostItCallback,$(".highlight-area")[0],2);
    }else if(sSection=="text-project-scenario"){
        $("#exam-text .textarea").html(sText?sText:cExam.ExamText);
        //clsPostIt(true,"project-scenario",cSuspendData["post-it"],fnPostItCallback,$(".highlight-area")[0],2);
    }
    if(bFlash && $("#exam-text .textarea").html()!="")$("#exam-text .textarea").css("background-color","#9eef7e").animate({backgroundColor:'#efefef'},1500);
    fnJGridUComboSetByID(null,$("#exam-text-selector"),sSection,true);

    //var oData={};
//"exam.hl0:consist of::1::8||exam.hl1:label design - ::1::168||exam.hl2:3 will include ::1::41||exam.hl3:£6,000. A chang::1::77||exam.hl4:number of alter::1::662||"
//$.each("exam.hl5:customers.::2::4||".split("||"),function(i,o){if(o!="")oData[o.split(":")[0]]=o.substring(o.split(":")[0].length+1);});
    
}

function fnSaveProgress(oCtrl,oExtra,oAjaxOptions){
    var oData={};
    if(oCtrl&&(oCtrl.length==null||oCtrl.length>0)){
        var oDiff={};
        var iID=cInt($(oCtrl).attr("id").split("_")[1]);
        var oQuestion=cExam.findQuestion(iID);
               
        var sVal="", sMCVal="";
        if(oCtrl.tagName && oCtrl.tagName.toLowerCase()=="textarea")sVal=$(oCtrl).val();
        else $(oCtrl).parents(".question-container").find("input:checked").each(function(i,o){sMCVal+=$(o).attr("value")+",";})                        

        //if(oQuestion&&oQuestion.QuestionType.indexOf("text")==0){

        var iInterval=((new Date())-cExam.TimeQuestionStart)/1000;
        cExam.TimeQuestionStart=new Date();
        cExam.TimeSpent+=iInterval;
        cAnswers[iID]=[cInt(cAnswers[iID]?cAnswers[iID][0]:0)+iInterval,sVal,sMCVal];    
        $.each(cAnswers,function(i,o){if(cAnswersStatus[i]==null || JSON.stringify(cAnswersStatus[i])!=JSON.stringify(cAnswers[i]))oDiff[i]=cAnswers[i];})
        if(!$.isEmptyObject(oDiff))oData["_answers"]=JSON.stringify(oDiff);
        var o=$(document).data("active-section");
        fnUpdateStatus(o);        
    }
    if(JSON.stringify(cSuspendDataStatus)!=JSON.stringify(cSuspendData))oData["_suspend-data"]=JSON.stringify(cSuspendData);
    if(oExtra)oData["_extra-data"]=JSON.stringify(oExtra);
    if(oData["_answers"] || oData["_suspend-data"]|| oData["_extra-data"]){
        var cAnswersStatus1=$.extend(true, {}, cAnswers);
        var cSuspendDataStatus1=$.extend(true, {}, cSuspendData);
        fnSendData("action=save-answers", oData,function(){cAnswersStatus=cAnswersStatus1;cSuspendDataStatus=cSuspendDataStatus1;},oAjaxOptions);  
    }        
}
function fnRecalcSectionStatus(oSection){
    if(cExam.NumQuestions==null)cExam.NumQuestions=0;
    var NumAnswered=0;var NumQuestions=0;var NumPoints=0;var Score=0;
    $.each(oSection.Sections,function(a,b){b.Index=a;b.Parent=oSection;fnRecalcSectionStatus(b);NumAnswered+=b.NumAnswered;NumQuestions+=b.NumQuestions;NumPoints+=b.NumPoints;Score+=b.Score;});
    if(oSection.Questions){
        $.each(oSection.Questions, function(k,q){
            if(q.Index==null)q.Index=(cExam.NumQuestions++);
            NumQuestions++;
            NumPoints+=q.Points;
            if(cAnswers[q.ObjectID]!=null&&cStr(cAnswers[q.ObjectID][2])!=""){
                NumAnswered++;
                var _QNumCorrect=0, _QNumAnswers=0;
                $.each(q.Answers,function(l,r){                
                    if(r.IsCorrect)_QNumAnswers++;
                    if((","+cAnswers[q.ObjectID][2]).indexOf(","+r.ObjectID+",")>-1){
                        _QNumCorrect+=(r.IsCorrect?1:-1);
                    }else if(r.IsCorrect){
                        _QNumCorrect--;
                    }                                        
                });
                Score+=(_QNumAnswers==0?0:( q.Points * (Math.max(0,_QNumCorrect)/_QNumAnswers) ));                
            }
        });
        oSection.Status=NumAnswered==0?"":NumAnswered==NumQuestions?"completed":"in-progress";
    }
    oSection.NumAnswered=NumAnswered;
    oSection.NumQuestions=NumQuestions;
    oSection.NumPoints=NumPoints;
    oSection.Score=Score;
    return oSection;
}
function fnUpdateStatus(oSection){
    if(oSection!=null){        
        var p=fnRecalcSectionStatus($(oSection).data("object"));
        $(oSection).attr("class",p.Status + " question-nav-item");        
    }
    cExam.NumAnswered=0;    
    $.each(cAnswers, function(i,o){if(cStr(o[2])!="")cExam.NumAnswered++;});    
    $("#progress-bar").find("div").css("width",(cExam.NumAnswered*100/(cExam.NumQuestions==0?1:cExam.NumQuestions))+"%");
    $("#progress-text").html("&nbsp;("+cExam.NumAnswered+" of " +cExam.NumQuestions+")");    
    
    if(!bShowAnswers || cExam.NumPoints==0) $(".timer-clock .score").hide();    
    $("#score-bar").find("div").css("width",(cExam.Score*100/(cExam.NumPoints==0?1:cExam.NumPoints))+"%");
    $("#score-text").html("&nbsp;("+Math.round(cExam.Score)+" of " +cExam.NumPoints+" points)");        
}

function fnLoadSection(oCtrl){    
    $.each(CKEDITOR.instances, function(i,o){var p=o.element.$;o.destroy(); fnSaveProgress(p); });
    
    var o=$(document).data("active-section");
    fnUpdateStatus(o);
    $(document).data("active-section",oCtrl);
    var s=cStr($(oCtrl).data("object").Text!=""? $(oCtrl).data("object").Text : ($(oCtrl).data("object").Settings["parent-additional-info"]=="1"? $(oCtrl).parents("div.box").data("object").Text:""));
    $("#text-additional-information").parents("li:eq(0)").toggle(s!="");
    if(o!=null){
        if(s!="")fnUpdateExamText("text-additional-information", true,s);
        else fnUpdateExamText("text-project-scenario");
    }
    
    $(oCtrl).addClass("current");
    if(cExam.PlayerType=="assessment"){
        $("li.current").removeClass("current");
        $(oCtrl).parent().addClass("current");
    }

    $("#exam-content").html("").scrollTop(0);
    var iPoints=0;
                
    $("#exam-content").append(fnRenderSection(0,$(oCtrl).data("object"),0));
    $.each($(oCtrl).data("object").Sections,function(i,o){$("#exam-content").append(fnRenderSection(i,o,1));});    
    var p,n,r=$("a.question-nav-item");
    r.each(function(i,j){if(j==oCtrl){if(i>0)p=r[i-1];if(i<r.length-1)n=r[i+1]; return false;}});    
    if(cExam.PlayerType=="case-test") $("#exam-content").append("<div class='exam-navigator' style='text-align:center;margin:0 auto'><img src='../inc/images/icons/information1.png' style='position:relative;top:3px' />&nbsp;<a href='javascript:void(0)' onclick='var b=$(\".text-content:visible\").length>0; $(\".text-answer\").toggle(b); $(\".text-content\").toggle(!b).each(function(i,o){ $(\"#answer_\" + $(o).find(\".content_editor_small\").attr(\"id\").split(\"_\")[1] + \"\").html($(o).find(\".content_editor_small\").val()); });'><common:label runat='server' TranslationKey='Show-hide-suggested-answer' /></a></div>");
    
    if(p||n)$("#exam-content").append("<div class='exam-navigator' style='text-align:center;margin:0 auto'><div style='width:50%;text-align:right;float:left'>"+(p?"<a href='javascript:void(0)' class='prev'><img src='../inc/images/icons/resultset_previous.png' style='position:relative;top:3px'> <common:label runat='server' TranslationKey='Previous' /></a>":"")+"</div><div style='width:50%;float:right;text-align:left'>&nbsp;&nbsp;"+(n&&p?"|":"")+"&nbsp;&nbsp;"+(n? "<a href='javascript:void(0)' class='next'><common:label runat='server' TranslationKey='Next' /> <img style='position:relative;top:3px' src='../inc/images/icons/resultset_next.png'></a>":"")+"</div></div>").find(".exam-navigator .prev").click(function(){if(p)fnLoadSection(p);}).end().find(".exam-navigator .next").click(function(){if(n)fnLoadSection(n);});            
    if(n==null)$("#exam-content").append("<br style='clear:both' /><div style='text-align:center;padding-top:60px'><button type='button'>" + $(".submit-exam").html() + "</button></div>").find("button").click(function(){$(".submit-exam").click();});    
    if(cExam.PlayerType=="case-test"){
        clearTimeout(cExam._ContentEditorTimer);
        cExam._ContentEditorTimer=setTimeout(function(){ $(".content_editor_small").ckeditor({ skin: 'kama', toolbar: 'Lexicon', height: '100px', width: '700px', toolbarStartupExpanded:true }).each(function(i,o){        
            CKEDITOR.instances[$(o).attr("id")].on("blur",function(){ fnSaveProgress(this.element.$);})        
        });
        },1);
    }
}

function fnRenderSection(iIndex,o,iLevel){
    var c=$("<div>" + (iIndex>0?"<br />":"")+ ((cExam.PlayerType=="prince_2p"&&o.Questions.length==0)||o.Name==""||cExam.PlayerType=="prince_2f"?"": "<h2 class='question-heading_"+iLevel+"' style='"+(cExam.PlayerType=="assessment"?"background-color:"+m_oBackgroundColors[o.Parent.Parent.Index]+";":"")+"'>" + o.Name + "</h2>")+(o.Questions.length==0?"":cStr(o.Text))+"</div>");
    if(cExam.PlayerType=="prince_2p" && iIndex==0 &&o.Questions.length==0)c.prepend("<table id='prince2p-question-head'><tr><th>Syllabus Area</th><th>Question Number</th><th>Part</th><th>Marks</th></tr><tr><td>"+o.Parent.Name+"</td><td>"+(o.Parent.Index+1)+"</td><td>"+o.Name+"</td><td>" + o.NumPoints + "</td></tr></table>");
    $.each(o.Questions,function(j,p){           
        if(",mc_single_p2,mc_multi_p2,".indexOf(","+p.QuestionType+",")>-1){
            if(j==0||",mc_single_p2,mc_multi_p2,".indexOf(","+o.Questions[j-1].QuestionType+",")==-1){
                var iMaxOptions=0;
                for(var x=j;x<o.Questions.length&&",mc_single_p2,mc_multi_p2,".indexOf(","+o.Questions[x].QuestionType+",")>-1;x++)iMaxOptions=o.Questions[x].Answers.length;
                c.append("<div class='question p2' style='background-color:white;width:100%;margin-top:5px'><table cellpadding='0' cellspacing='0' style='width:100%;border:0;table-layout:fixed'><tr><td style='width:20px'></td></tr></table></div><br />");
                for(var x=0;x<iMaxOptions;x++)c.find("table:last tr").append("<th style='width:30px'>"+String.fromCharCode(x+65)+"</th>");                    
            }
            $.each(p.Answers,function(k,q){
                if(k==0)c.find("table:last").append("<tr class='question-container "+(bShowAnswers?"answer-row":"question-row")+"'><td><h3 style='margin-left:4px; color:#9A0000; font-size:14px; font-weight:bold'>"+((cExam.PlayerType=="prince_2f"? p.Index : j)+1)+"</h3></td></tr>");
                c.find("table:last tr:last").append((bShowAnswers?"<td class='option " + (cAnswers[p.ObjectID]&&(","+cAnswers[p.ObjectID][2]).indexOf(","+q.ObjectID+",")>-1 ? (q.IsCorrect?"correct-checked":"wrong-checked") : (q.IsCorrect?"correct-blank":"wrong-blank") ) + "' style='" + (bShowAnswers&&q.IsCorrect==1?";background-color:#e1fae1":"")+ ";'>": "<td class='option' onclick='var o=$(this).find(\"input\");o.attr(\"checked\",\"checked\");if(o.length>0){fnSaveProgress(o[0]);}'><input type='" + (p.QuestionType.indexOf("mc_multi")==0?"checkbox":"radio") + "' name='question_"+p.ObjectID+"' value='"+q.ObjectID+"' id='question_"+p.ObjectID+"_"+k+"'" + (cAnswers[p.ObjectID]&&(","+cAnswers[p.ObjectID][2]).indexOf(","+q.ObjectID+",")>-1?" checked='checked'":"") + ">") + "</td>");
            });
            if(j==0)c.find("table:last tr:last").append("<th style='width:auto'></th>");            
                
        }else{        
            c.append("<div class='question question-container' style='background-color:white;width:100%;margin-top:5px'><div class='question-numbering'>"+((cExam.PlayerType=="prince_2f"? p.Index : j)+1)+":&nbsp;</div>" + (cExam.PlayerType=="case-test"?"<div style='float:left'>": "<h3 class='question-text'>") + fnAddBRsIfNecessary(p.Text)+(cExam.PlayerType=="case-test"?"</div>": "</h3>")+"<br style='clear:both'/>"+(cExam.PlayerType=="assessment"&&p.SubText!=""?"<div class='question-sub-text'>"+p.SubText+"</div>":"")+"<table cellpadding='0' cellspacing='0'></table></div><br />");
            if(p.QuestionType.indexOf("mc_single")==0 || p.QuestionType.indexOf("mc_multi")==0 || p.QuestionType.indexOf("tf")==0){
                $.each(p.Answers,function(k,q){
                    c.find("table:last").append($("<tr class='"+(bShowAnswers?"answer-row":"question-row")+"'>" +(bShowAnswers?"<td class='option " + (cAnswers[p.ObjectID]&&(","+cAnswers[p.ObjectID][2]).indexOf(","+q.ObjectID+",")>-1 ? (q.IsCorrect?"correct-checked":"wrong-checked") : (q.IsCorrect?"correct-blank":"wrong-blank") ) + "'>": "<td class='option'><input type='" + (p.QuestionType.indexOf("mc_multi")==0?"checkbox":"radio") + "' name='question_"+p.ObjectID+"' value='"+q.ObjectID+"' id='question_"+p.ObjectID+"_"+k+"'" + (cAnswers[p.ObjectID]&&(","+cAnswers[p.ObjectID][2]).indexOf(","+q.ObjectID+",")>-1?" checked='checked'":"") + ">") + "</td><td class='question'><div style='"+(bShowAnswers&&q.IsCorrect==1?";background-color:#e1fae1":"")+ "'>" + q.Text + "</div>"+(bShowAnswers&&q.Reason!=""?"<i>"+q.Reason+"</i>":"")+"</td></tr>").click(function(e){var o=$(this).find("input");if(o.length>0){if(e.target!=o[0])o.attr("checked",o.attr("checked")?"":"checked");fnSaveProgress(o[0]);}}));
                });
            }else if(p.QuestionType.indexOf("text")==0){                                                    
                c.find("div:last").parent().append("<div class='text-answer' style='display:none'><table><tr><td style='width:50%'><b><common:label runat='server' TranslationKey='Your-Answer' />:</b></td><td></td><td><b><common:label runat='server' TranslationKey='Suggested-Answer' />:</b></td></tr><tr><td id='answer_" + p.ObjectID + "'>" + (cAnswers[p.ObjectID]?cAnswers[p.ObjectID][1]:"") + "</td><td></td><td>" + p.Reason + "</td></tr></table></div>");                
                c.find("div:last").parent().append("<div style='padding-bottom:10px;' class='text-content'><b><common:label runat='server' TranslationKey='Answer' />:</b><br /><textarea style='height:50px' class='content_editor_small' id='question_"+p.ObjectID+"'>" +(cAnswers[p.ObjectID]?cAnswers[p.ObjectID][1].split("<").join("&lt;").split(">").join("&gt;"):"")+"</textarea></div>");
            }
        }       
        if(bShowAnswers){            
            if(p.Reference || p.Domain || p.KnowledgeArea || p.Reason) c.find("table:last").append("<tr><td style='padding-bottom:5px'></td></td></tr>");
            if(p.Reference!="")c.find("table:last").append("<tr><td></td><td colspan='"+(100)+ "'><b>Reference:</b> <i>"+p.Reference+"</i></td></tr>");
            if(p.Domain!="")c.find("table:last").append("<tr><td></td><td colspan='"+(100)+ "'><b>Domain:</b> <i>"+p.Domain+"</i></td></tr>");
            if(p.KnowledgeArea!="")c.find("table:last").append("<tr><td></td><td colspan='"+(100)+ "'><b>Knowledge Area:</b> <i>"+p.KnowledgeArea+"</i></td></tr>");
            if(p.Reason!="")c.find("table:last").append("<tr><td></td><td colspan='"+(100)+ "'>" + (cStr(p.Reason).indexOf("Reason") == -1 ? "<b>Reason:</b> ":"")+"<i>" + p.Reason+"</i></td></tr>");
        }else{            
            if(p.Hint!=""){                                
                c.find("table:last").append($("<tr><td class='hint-container' colspan='101'><div><a href='javascript:void(0)'><img src='/learningportal/inc/images/icons/information1.png'>Click to view hint</a></div></td></tr>").find("a").click(function(){$(this).parents("div:eq(0)").replaceWith("<div><img src='/learningportal/inc/images/icons/accept.png'><i>" + p.Hint + "</i></div>");}).end());                
            }
        }
    });        
    return c;
}

function fnAddBRsIfNecessary(s){
    s=cStr(s);
    if(s.indexOf("<")>-1)return s;
    return s.split("\n").join("<br />");
}
    
var m_oBackgroundColors=["#d6e3bb","#e8eff9"];
var cAnswersStatus, cSuspendDataStatus;
$(function () {
    cExam.showMessage=function(bShow, sMsg,oCtrl){
        if(bShow)$("<div id='message-overlay' style='position:absolute;left:0px;top:0px;z-index:10000;width:100%;height:100%;background-color:white;display:table-cell; vertical-align:middle;text-align:center'><span>"+(cStr(sMsg)==""&&!oCtrl? "<b>"+$(".hPlaybackPaused").val()+"</b><br /><br /><a onclick='cExam.showMessage(false);' href='javascript:void(0)'><img src='../inc/images/Play1Normal.png' /></a>" : cStr(sMsg) )+"</span></div>").appendTo(document.body).append(oCtrl);
        else $("#message-overlay").detach();
        this.paused=bShow;
    };
    cExam.renderAssessmentAnswers=function(){
        var oCharts=[];
        var c=$("<div><h1>"+cExam.Name+"</h1><table id='assessment-answers'><tr><th></th><th><common:label runat='server' TranslationKey='Your-Answer' /></th><th><common:label runat='server' TranslationKey='Target-Answer(s)' /></th><th><common:label runat='server' TranslationKey='Delta' /></th></tr></table></div>");
        $.each(cExam.Sections,function(i,o){                            
            c.find("tr:last").after("<tr class='section'><td colspan='4'>Section: "+o.Name+"</td></tr>");            
            var iUserLevel=0;
            $.each(cTempData,function(i,o){if(o[0]=='role'){iUserLevel=o[1];return false;}});                        
            var iTotalDelta=0;
            $.each(o.Sections,function(j,p){
                var iSectionDelta=0;
                c.find("tr:last").after("<tr class='sub-section'><td colspan='4' id='sub-section-"+p.ObjectID+"'>Sub-Section: "+p.Name+"</td></tr>");
                $.each(p.Sections,function(k,q){
                    $.each(q.Questions,function(l,r){
                        var d=$("<tr class='question'><td class='text'><b>" + q.Name +": " + fnAddBRsIfNecessary(r.Text) + "</b><div style='font-style:italic;padding-left:0'>"+ fnAddBRsIfNecessary(r.SubText)+"</div><ol /></td></tr>").insertAfter(c.find("tr:last")).find("td");
                        var iAnswer=-1;var iQuestionDelta=9999;var oTargetAnswers=[];
                        $.each(r.Answers,function(m,s){
                            if(cAnswers[r.ObjectID]&&(","+cAnswers[r.ObjectID][2]).indexOf(","+s.ObjectID+",")>-1)iAnswer=(m+1);
                            d.find("ol").append($("<li style='" +(iAnswer==m+1?"text-decoration:underline":"") +"'>"+fnAddBRsIfNecessary(s.Text).substring(s.Text.indexOf((m+1)+". ")==0?((m+1)+". ").length:0)+"</li>"));                                
                            if(s.Settings.Role&&(","+s.Settings.Role+",").indexOf(","+(iUserLevel)+",")>-1)oTargetAnswers.push(m+1);
                        });
                        if(iAnswer==-1)iAnswer=0;
                        $.each(oTargetAnswers,function(m,s){if(Math.abs(iQuestionDelta)>Math.abs(iAnswer-s))iQuestionDelta=iAnswer-s;});
                        if(iQuestionDelta==9999)iQuestionDelta=0;                        
                        d.parent().append("<td class='answer'>" +(iAnswer>-1?iAnswer:"")+ "</td>").append("<td class='target'>"+oTargetAnswers.join(",")+"</td>").append("<td class='delta' style=';" +(iAnswer==-1?"":"color:"+(iQuestionDelta==0? "green":"red"))+"'>" + (iAnswer==-1?"N/A":iQuestionDelta) + "</td>");                            
                        iSectionDelta+=iQuestionDelta;
                        iTotalDelta+=iQuestionDelta;
                        if(oCharts[i]==null)oCharts[i]={"Name":o.Name,"Data":""};
                        oCharts[i].Data+="&section_"+q.ObjectID+"="+escape(q.Name)+"|"+iAnswer+"|"+oTargetAnswers[0];
                    });                        
                });            
                c.find("tr:last").after("<tr class='sub-section end'><td colspan='3'>Total Sub-section: "+p.Name+"</td><td class='delta' style='"+(iSectionDelta==0?"color:green":"color:red")+"'>"+iSectionDelta+"</td></tr>");
                
            });
            c.find("tr:last").after("<tr class='section end'><td colspan='3'>Total: "+o.Name+"</td><td class='delta' style='"+(iTotalDelta==0?"color:green":"color:red")+"'>"+iTotalDelta+"</td></tr>");               
        });
        for(var x=oCharts.length-1;x>=0;x--){        
            c.find("tr:first").before('<tr><td colspan="4" style="page-break-inside:avoid;"><h2>'+oCharts[x].Name+'</h2><br /><img src="player.aspx?chart=true&bgcolor='+escape(m_oBackgroundColors[x])+'&type='+escape(oCharts[x].Name)+oCharts[x].Data+'" /></td></tr>');
        }
        return c;
    };

    cExam.print=function(sHTML){    
        this.printMode=true;                 
        $(document.body).find("*").detach();
        if(cExam.PlayerType=="assessment"&&bShowAnswers){
            var c=cExam.renderAssessmentAnswers();
        }else{     
            if(cStr(sHTML)==""){
                var c=$("<div style='text-align:left' id='exam-content'><h1 class='exam-title'>"+cExam.Name+"</h1>" + (cExam.IntroText==""?"":"<div style='page-break-after:always;padding-bottom:30px' class='exam-text'>"+cExam.IntroText+"</div>") + (cExam.ExamText==""?"":"<div style='page-break-after:always;padding-bottom:30px' class='exam-text'>"+cExam.ExamText+"</div>")+ (cSuspendData && cStr(cSuspendData["user-notes"])!=""?"<div style='page-break-after:always;padding-bottom:30px'><u><b><common:label runat='server' TranslationKey='Your-notes' />:</b></u><br />"+cSuspendData["user-notes"].split("\n").join("<br />")+"</div>":"") + "</div>");                    
                $.each(cExam.Sections,function(i,o){
                if(cExam.PlayerType=="prince_2f" || cExam.PlayerType=="case-test")c.append(fnRenderSection(0,o,0));
                else if(cExam.PlayerType=="prince_2p")c.append("<h2>"+o.Name+"</h2>"+(o.Text==""?"":"<b><u><common:label runat='server' TranslationKey='Additional-Information' />: </u></b>"+o.Text+"<br />"));        
                    $.each(o.Sections,function(j,p){c.append(fnRenderSection(0,p,0)); 
                        if(p.Text!=""&&cExam.PlayerType=="prince_2p")c.append("<b><u><common:label runat='server' TranslationKey='Additional-Information' />: </u></b>"+p.Text+"<br />");
                        $.each(p.Sections,function(k,q){c.append(fnRenderSection(k,q,1)); });            
                    });
                });
            }else{
                var c=$(sHTML);
            }
        }
        
        $(document.body).css("overflow", "hidden");                
        $(document.body).append(c);
		if(cStr(cExam.Name).toLowerCase().indexOf("prince2")>-1)
			$(c).append("<div><i>PRINCE2® is a registered trade mark of AXELOS Limited, used under permission of AXELOS Limited. All rights reserved. The Swirl logo™ is a trade mark of AXELOS Limited, used under permission of AXELOS Limited. All rights reserved.</i></div>");
			
        $(".text-content").hide();
        $(".text-answer").show();
        //cExam.showTextAnswers(true);
        $(document.body).append("<form method='post' action='player.aspx?pdf=true&type="+cExam.PlayerType+"'><input type='hidden' name='html' /></form>").find("input[name='html']").val($.find('html')[0].outerHTML);        
        $(document.body).css("overflow", "scroll");
        c.before("<div "+(cExam.PlayerType=="assessment"?"":"style='position:absolute;right:0;top:0;width:200px;text-align:right'")+"><button type='button' onclick='window.location.href=window.location.href;'><common:label runat='server' TranslationKey='Close' /></button><button type='button' onclick=\"$('form').submit();\"><common:label runat='server' TranslationKey='Print-to-PDF' /></button></div>");                                        
    };
    //cExam.showTextAnswers=function(bShow){$(".text-content").each(function(i,o){ if(bShow){if($(o).find(".text-answer").length>0)return; alert(o);$(".text-content").hide(); $("<div class='text-answer'>Suggested Answer<table><tr><td><b>Your Answer:</b></td><td></td><td><b>Suggested Answer:</b></td></tr><tr><td>" + $(o).find("textarea").val() + "</td><td></td><td>" + $(o).find("textarea").attr("id") + "</td></tr></table></div>").appendTo($(o)); }else{$(o).find(".text-answer").detach(); $(".text-content").show(); }});};
    cExam.Settings["ui-bookmarks"]=0;
    if(cExam.PlayerType=="assessment"||cExam.Settings["ui-notes"]==0){$("#exam-notes").siblings(".draggable-divider-v").remove().end().remove();$("#exam-content").attr("height-adjustment","15").parents(".player-section").attr("absolute-height-adjustment","10");}
    if(cExam.PlayerType=="assessment"||cExam.Settings["ui-bookmarks"]==0)$("#exam-bookmarks").siblings(".draggable-divider-v").remove().end().remove();
    if(cExam.PlayerType=="assessment"||cExam.Settings["ui-text"]==0)$("#exam-text").siblings(".draggable-divider-v").remove().end().remove();
    if(cExam.PlayerType!="assessment"&&cExam.Settings["ui-text"]==0&&cExam.Settings["ui-bookmarks"]==0){$(".draggable-divider-h").siblings("td:gt(0)").remove().end().remove(); $("#exam-content").attr("absolute-width-adjustment","30").attr("resize-parent-nesting","2");}
        
    if($("#exam-text").siblings(":visible").length==0){
        $("#exam-text").addClass("auto-size").find(".auto-size").each(function(i,o){$(o).removeClass("auto-size").css("height","95%");});;
    }

    if(cExam.PlayerType=="assessment"||cExam.PlayerType=="case-test") $(".timer-clock").hide();
    if(cExam.MaxTime==0)$(".timer-clock .clock").hide();
    
    $("#case-print-exam-text").toggle(cExam.PlayerType=="case-test");
    if(cExam.PlayerType=="assessment") $(".print-exam, .submit-exam").each(function(i,o){$(o).html($(o).html().split("Exam").join("Assessment"));});            
    else if(cExam.PlayerType=="case-test"){
        $(".print-exam, .submit-exam").each(function(i,o){$(o).html($(o).html().split("Exam").join("Case"));});            
        $("#text-project-scenario").html("<common:label runat='server' TranslationKey='Case-Scenario' />");
    }
        
    $("#user-notes").val(cStr(cSuspendData["user-notes"])).change(function(){cSuspendData["user-notes"]=this.value;fnSaveProgress();});
    cAnswersStatus=$.extend(true, {}, cAnswers);
    cSuspendDataStatus=$.extend(true, {}, cSuspendData);
    $("#exam-title").html(cExam.Name);        
    if(cExam.IntroText==""){
        $("#text-scenario-booklet").parents("li:eq(0)").remove();
        $("#text-project-scenario").click();
    }else $("#text-scenario-booklet").click()
    if(cExam.ExamText=="")$("#text-project-scenario").parents("li:eq(0)").remove();

    cExam.NumAnswered=0;cExam.NumQuestions=0;cExam.TimeSpent=0;cExam.TimeQuestionStart=new Date();
    $.each(cAnswers,function(i,o){cExam.TimeSpent+=cInt(o[0]);});
    if(bShowAnswers){
        $(".timer span.total").html(cExam.MaxTime>0?"<common:label runat='server' TranslationKey='Time-left' />: " + fnFormatHMS(cExam.MaxTime-cExam.TimeSpent):"<common:label runat='server' TranslationKey='Time-spent' />: " + cExam.TimeSpent);
    }else if(cExam.PlayerType!="assessment"){
        var i=(new Date());
        i.setSeconds(i.getSeconds()-cExam.TimeSpent);
        setInterval(function(){
            if(cExam.paused){
                i.setSeconds(i.getSeconds()+1);
                if(cExam.TimeQuestionStart)
                    cExam.TimeQuestionStart.setSeconds(cExam.TimeQuestionStart.getSeconds()+1);
                }else{ 
                    var s=cInt(((new Date)-i)/1000);
                    if(cExam.MaxTime>0){
                        if(cExam.PlayerType=="prince_2p"&&cExam.NumAnswered!=null&&cExam.NumQuestions&&cExam.NumQuestions>0){
                            var iTimeProgress=600+(cExam.NumAnswered*(cExam.MaxTime-600)/cExam.NumQuestions);
                            $("#progress-bar").css("background-color",(iTimeProgress>=s?"#d1d1d1":"#ff" + Math.max(0, 255-Math.floor((s-iTimeProgress)/4.6875)).toString(16).lpad('0',2) + "00")).mouseover(function(){$(this).attr("title", $(iTimeProgress>=s?".hAheadOfSchedule":".hBehindSchedule").val().replace("{0}", Math.floor(iTimeProgress>=s?iTimeProgress-s:s-iTimeProgress)) );});                            
                        }
                        var iRemain=cExam.MaxTime-s;
                        if(iRemain<=0){
                            if(cExam.Settings["disable-time-limit"]==1){
                                if(!$(document).data("time-limit-alert")){
                                    $(document).data("time-limit-alert",true);
                                    alert("<common:label runat='server' TranslationKey='Times-Up-Continue' />");
                                }
                            }else{
                                fnSaveProgress($("tr.question-row td.option input:eq(0)"), {"Timeout":true}); 
                                cExam.showMessage(true,$(".hTimeIsUp").val());
                            }
                        }
                        $(".timer span.total").html(fnFormatHMS(iRemain)).css("color",(iRemain<300&&cExam.MaxTime>0?"red":null));                    
                    }                     
                }
        },1000);
    }
    fnRecalcSectionStatus(cExam);
    cExam.findQuestion=function(iID, oSection){if(oSection==null)oSection=cExam;if(oSection.Questions){for(var x=0;x<oSection.Questions.length;x++)if(oSection.Questions[x].ObjectID==iID)return oSection.Questions[x];} for(var x=0;x<oSection.Sections.length;x++)return cExam.findQuestion(iID,oSection.Sections[x]);};
    $.each(cExam.Sections,function(i,o){
        if(cExam.PlayerType=="assessment"){
            var c=$("<div class='container'><b>"+(o.ShortName!=""?o.ShortName:o.Name)+"</b><ul class='box' style='background-color:"+m_oBackgroundColors[i]+"'></ul></div>").data("object",o);
                $.each(o.Sections,function(j,p){                
                $("<li style='color:"+m_oBackgroundColors[i]+"' />").appendTo(c.find("ul")).append(
                    $("<a href='javascript:void(0)' class='"+p.Status+" question-nav-item'>"+p.Name+"</a>").click(function(){if(bShowAnswers)$('#exam-content').animate({'scrollTop':$('#exam-content').scrollTop()+$('#sub-section-'+$(this).data("object").ObjectID).position().top-80}); else fnLoadSection(this);}).data("object",p).data("object-p",j>0?o.Sections[j-1]:null).data("object-n",j+1<o.Sections.length?o.Sections[j+1]:null));
            });                    
            $("#assessment-nav").append(c);
            if(bShowAnswers){
                if(i==0){
                    $("#exam-content").children().remove().end().append(cExam.renderAssessmentAnswers());                    	
                }
            }else
                if($("#assessment-nav a:first").length>0)fnLoadSection($("#assessment-nav a:first")[0]);        
            
        }else{
            if(cExam.PlayerType=="prince_2p"){
                var c=$("<div class='box'>"+(i+1)+". "+(o.ShortName!=""?o.ShortName:o.Name)+"<br /><div></div></div>").data("object",o);
                $.each(o.Sections,function(j,p){                            
                    $("<a href='javascript:void(0)' class='"+p.Status+" question-nav-item'>"+p.Name+"</a>").appendTo(c.find("div") ).click(function(){fnLoadSection(this);}).data("object",p).data("object-p",j>0?o.Sections[j-1]:null).data("object-n",j+1<o.Sections.length?o.Sections[j+1]:null);
                });        
            }else if(cExam.PlayerType=="prince_2f" || cExam.PlayerType=="case-test"){        
                var c=$("<div class='box'><a href='javascript:void(0)' class='"+o.Status+" question-nav-item'>"+o.Name+"</a></div>").find("a").click(function(){fnLoadSection(this);}).data("object",o).data("object-p",i>0?cExam.Sections[i-1]:null).data("object-n",i+1<cExam.Sections.length?cExam.Sections[i+1]:null).end().data("object",o);
                //$("#question-nav").toggle(cExam.Sections.length>1);
            }
            $("#question-nav br:last").before(c);
            if($("#question-nav a:first").length>0)fnLoadSection($("#question-nav a:first")[0]);
        }
    });    
	if(cStr(cExam.Name).toLowerCase().indexOf("prince2")>-1)
		$("#question-nav").append("<div>PRINCE2® is a registered trade mark of AXELOS Limited, used under permission of AXELOS Limited. All rights reserved. The Swirl logo™ is a trade mark of AXELOS Limited, used under permission of AXELOS Limited. All rights reserved.</div>");
		
    fnUpdateStatus();
    
    $(window).resize();
    
    $("a.pause-button").toggle(!bShowAnswers&&cExam.Pausable==1).click(function(){cExam.showMessage(true);});    
    $("button.submit-exam").toggle(!bShowAnswers||cExam.PlayerType=="case-test").click(function(){(cExam.PlayerType=="assessment"?window.location.href=window.location.href+(window.location.href.indexOf("&answers=true")>-1?"":"&answers=true"):cExam.showMessage(true,"<b><common:label runat='server' TranslationKey='Answers-saved-End-prompt' /></b><br /><br /><a href='javascript:void(0)' onclick='fnSendData(\"action=close\");" +(cExam.PlayerType=="assessment"||(cExam.ReviewAnswers==1)?"window.location.href=window.location.href+(window.location.href.indexOf(\"&answers=true\")>-1?\"\":\"&answers=true\");":"window.close();")+"'><common:label runat='server' TranslationKey='Yes' /></a> &nbsp;<a href='javascript:void(0)' onclick='cExam.showMessage(false);'><common:label runat='server' TranslationKey='No' /></a>"));});
    $("button.print-exam").toggle(bShowAnswers).click(function(){cExam.print();});
    
    $(this).bind("contextmenu", function(e) {e.preventDefault();});
    //todo
    //if(!bShowAnswers) $(window).unload(function(){fnSaveProgress($("tr.question-row td.option input:eq(0)"),{"Finalize":true},{"async":false});try{window.opener.location.reload();}catch(e){}});    
    $(window).unload(function(){try{if(window.opener.location.href.indexOf("start.aspx")>-1) window.opener.location.reload();}catch(e){}});    
    

    $("span[class^='d_users.']").each(function(){$(this).html(cUser[0][$(this).attr("class").substring(8)]);});
    $("span[class^='d_temp_data.']").each(function(){var h=$(this); $.each(cTempData,function(i,o){if(o[0]==h.attr("class").substring(12)){h.html(o[1]);return false;}}); });    
});

function fnPostItCallback(sAction,sKey,sValue,oArr){
    if(!cSuspendData["post-it"])cSuspendData["post-it"]={};
    cSuspendData["post-it"][sKey]=(sValue==""?null:sValue);
    if(sKey.indexOf(".no")>-1){
        var c=$("#exam-bookmarks .textarea").html("<ol></ol>").find("ol");
        $.each(oArr,function(i,o){
            if(o&&o!="") c.append("<li><a href='javascript:void(0)'>" + o.Text + "</a></li>");
        });
    }    
}
</script>
<script language="javascript" type="text/javascript">    CKEDITOR.config.toolbarStartupExpanded = false; CKEDITOR.config.uiColor = '#eee'; CKEDITOR.basePath = '/learningportal/inc/library/ckeditor/';</script>  
<common:hidden runat="server" ID="hPlaybackPaused" CssClass="hPlaybackPaused" />
<common:hidden runat="server" CssClass="hAheadOfSchedule" TranslationKey="Ahead-of-schedule" />
<common:hidden runat="server" CssClass="hBehindSchedule" TranslationKey="Behind-schedule" />
<common:hidden ID="hTimeIsUp" runat="server" CssClass="hTimeIsUp" />

</html>
</asp:Content>