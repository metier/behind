﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Phoenix.LearningPortal.Exam.Register" MasterPageFile="~/inc/master/default.master" Codebehind="register.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head runat="server">
    <title></title>
        <script src="../inc/js/jquery.js" type="text/javascript"></script> 
        <script src="../inc/js/json2.min.js" type="text/javascript"></script>
        <script src="../inc/js/daterangepicker.jQuery.js" type="text/javascript"></script>
        <script type="text/javascript" src="../inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="~/inc/css/jquery-ui-1.11.4.custom/jquery-ui.min.css" />				
        <script src="../inc/js/functions.js?14" type="text/javascript"></script>
                
        <link rel="stylesheet" href="css/exam.css?3" type="text/css" />
       
       <style type="text/css">
           #outer {width: 100%;}
           #inner {width: 450px; margin-left: auto; margin-right: auto;}
#outer {height: 400px; overflow: hidden; position: relative;}
#outer[id] {display: table; position: static;}

#middle {position: absolute; top: 30%;} /* for explorer only*/
#middle[id] {display: table-cell; vertical-align: middle; width: 100%;}
           .form_error
           {
               margin-bottom:20px;               
           }
     /*      #splash{
  min-height: 100%;
  min-width: 1024px;
	
  width: 100%;
  height: auto;
	
  position: fixed;
  top: 0;
  left: 0;
  }*/

/* optional: #inner[id] {position: static;} */
</style> 
    <div id="splash-container" style="display:none">
        <div style="position:absolute;left:0;top:0;bottom:0;right:0;z-index:1;background-color:black; opacity:0.4;filter:alpha(opacity=40);"></div>
        <img src="../exam/images/dnv-kema-splash.png" style="position:absolute;left:0;top:0;z-index:2" id="splash" />
        <div style="cursor:pointer;z-index:10002;position:absolute;" id="button"></div>
    </div>
    <script lanuage="javascript">        
        
        <common:label DataMember="dynamic:assessment-dnv" runat="server">
            $(function() {                  
                                
                $("#splash").click(function(e){
                    if(e.pageX>$("#button").offset().left && e.pageY>$("#button").offset().top
                        && e.pageX<$("#button").offset().left + $("#button").width() && e.pageY<$("#button").offset().top+$("#button").height()){
                        $("#button").click();
                    }
                });
                $("#button").click(function(){$("#splash-container").hide(); });
                if($(".welcome-text-alerted").val()==""){            
                    $("#splash-container").show();
                    $(".welcome-text-alerted").val("1");
                }
                $(window).resize(function(){
                    var ratio=1772/1063;
                    var winRatio=$(window).width()/$(window).height();
            
                    //width it constraint
                    if(ratio>winRatio) $("#splash").css("width",$(window).width()).css("height","auto").css("left",0).css("top",($(window).width())*(ratio-winRatio)/4);
                    else $("#splash").css("width","auto").css("height",$(window).height()).css("top",0).css("left",$(window).height()*(winRatio-ratio)/2);

                    $("#button").css("left",$("#splash").position().left+($("#splash").width()*.31)).css("top",$("#splash").position().top+($("#splash").height()*.92)).width($("#splash").width()*.20).height($("#splash").height()*.06);
            
                });
                $(window).resize().resize();
                setTimeout(function(){ $(window).resize().resize(); },1000);
            });
            /*
            $(function() {                   
                if($(".welcome-text-alerted").val()==""){
                    var d=$("#dialog").detach();
                    d=$("<div id='dialog' />").appendTo(document.body).html($(".welcome-text").val()).dialog({modal: true, hide: 'fold', width: Math.max(830,$(window).width()-150), height: Math.max(510,$(window).height()-150), resizable: true, open: function () {$(this).scrollTop(0); } }).dialog("open");                
                    $(".welcome-text-alerted").val("1");
                }                
            });
            */
        </common:label>
    </script>    
</head>
<body>
<common:form id="frmMain" runat="server">    
    <common:hidden runat="server" CssClass="welcome-text" DataMember="Exam.IntroText" />
    <common:hidden runat="server" CssClass="welcome-text-alerted" />

   <div id="outer">
  <div id="middle">  
    <div id="inner" style="border:5px solid silver;padding:15px">
    <common:label runat="server" ID="lFormError" Visible="false" CssClass="form_error" />
        <img src="images/mymetier_logo.png" style="float:right;width:100px" />
        <img src="images/technip_logo.png" style="float:left" runat="server" id="iLogo" />
        <br style="clear:both" />
        <br />
        <common:label runat="server" ID="lTitle" Tag="h3" />
                
        <br />
      <table>
      <tr>
      <td style="width:180px">Your Name *</td>
      <td><common:textbox ID="tName" runat="server" datamember="d_users.name" Required="true" FieldDesc="Name" Width="250px" /></td>
      </tr>
      <tr>
      <td>Your Email *</td>
      <td><common:textbox runat="server" ID="tEmail" datamember="d_users.email" Required="true" FieldDesc="Email" ValidateAs="email" Width="250px" /></td>
      </tr>
          <common:label DataMember="dynamic:assessment" runat="server">
    
        <tr>
          <td>Your Role *</td>
          <td><common:dropdown ID="dRole" runat="server" datamember="role" Required="true" FieldDesc="Role" Width="250px">
          <asp:ListItem Value="">(Please select)</asp:ListItem>
          </common:dropdown></td>
          </tr>
    </common:label>
          <common:label DataMember="dynamic:asssessment-dnv" runat="server">
              <tr>
              <td>Your Line Manager's Name *</td>
              <td><common:textbox runat="server" datamember="Settings.SupervisorName" Required="true" FieldDesc="Supervisor's Name" Width="250px" /></td>
              </tr>
              <tr>
              <td>Your Line Manager's Email *</td>
              <td><common:textbox runat="server" datamember="Settings.SupervisorEmail" Required="true" FieldDesc="Supervisor's Email" ValidateAs="email" Width="250px"  /></td>
              </tr>
          </common:label>
          
       <tr><td colspan="2">
<br />
       <br />
       <div style="float:left"><common:button.button runat="server" id="bStartButton" cssclass="button-start" buttonmode="submitinput" style="background:url(images/start_lesson_arrow.png) #75be34 no-repeat 10px 50%; color:#fff; border:0; padding:1px 10px 2px 30px" text="Start assessment" /></div>
       <common:label DataMember="dynamic:assessment" runat="server">
           <div style="float:right">Go to <a href="https://mymetier.net">MyMetier.net</a></div>
       </common:label>
       
       </td></tr>
      </table>
    </div>
  </div>
</div>
</common:form>
    </body>
</html>
</asp:Content>