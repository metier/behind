﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Phoenix.LearningPortal.Exam.Export" MasterPageFile="~/inc/master/default.master" Codebehind="export.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">

<h2 class="page-title"><img src="../inc/images/icons/tools_32.png" alt="Backup and Restore Data" />Export Exam Attempts</h2>
<div id="cStatus" style="visibility:hidden;"><img src='../inc/images/icons/accept.gif'/>&nbsp;Refresh complete</div>             
<br style="clear: both;" />

<div style="float:left"><img src="../inc/images/icons/table_save.png" /> <a href="javascript:void(0)" onclick="oGrid.addRequest(oGrid.buildRequest('custom','Type','export'));">Export report to Excel</a></div>
<br style="clear: both;" />
<div style="border:2px solid red;padding:7px; background-color:#eee; margin:8px 0; display:none" id="hDownloadContainer">
Download your file from <a href="" id="lDownload" onclick="$('#hDownloadContainer').hide();">here</a>.
</div>
<br style="clear: both;" />

<table cellpadding="0" cellspacing="0" border="0" class="listing assets" style="margin-bottom:0;">
	<colgroup></colgroup>
	<colgroup span="3" style="font-weight: bold;"></colgroup>
	<thead>
		<tr class="actions">
			<td colspan="11">
				<table cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<td>                            
                            </td>
													
							<td class="paging">
	                            Page
	                            <img src="../inc/images/icons/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow grid_prev_page" />

	                            <input type="text" value="1" class="text page page_number" />

	                            <a href="javascript:void(0);" title="Next page" ><img src="../inc/images/icons/pager_arrow_right.gif" alt="Go to Next page" class="arrow grid_next_page" /></a>

	                            of <span class="page_status">3 pages</span>            <span class="separator">|</span>
	                            View            <select name="limit" class="page_size">

	                                <option value="20" selected="selected">20</option>
	                                <option value="30">30</option>
	                                <option value="50">50</option>
	                                <option value="100">100</option>
	                                <option value="200">200</option>
	                            </select>

	                            per page
			                </td>
						</tr>
					</thead>
				</table>
			</td>
		</tr>
	</thead>
	</table>      
<jgrid:jGridAdo runat="server" DisplayFilter="true" DisplayAdd="false" id="gAttempts" Pagination="true" DataViewFiltering="false" BindDropdownByID="true" style="width:956px;" SortOrder="desc" JSName="oGrid" AutoFilter="true" SortCol="0" TAtt="class='listing assets' style='border-bottom:1px solid silver;'" DeleteConfirmation="Delete this attempt? This action can NOT be undone!">
    <Cols>        
        <jgrid:col runat="server" id="cID" HText="ID" ColType="Text" PrimaryKey="true" FormatString="ID" SortField="d_attempts.ID" FField="d_attempts.ID" />
        <jgrid:col runat="server" id="cReleaseID" HText="Release ID" ColType="Hide" PrimaryKey="false" FormatString="release_id" SortField="d_attempts.release_id" />
        <jgrid:col runat="server" id="cExam" HText="Exam" ColType="Text" FormatString="Name" FPartialMatch="true" CInputAtt="class='text'" SortField="d_releases.Name" FField="d_releases.Name" />
        <jgrid:col runat="server" id="cDate" HText="Start Date" ColType="Text" FormatString="{0:d} {0:t}" FormatStringArguments="CreateDate" SortField="d_attempts.CreateDate" FField="d_attempts.CreateDate" FPartialMatch="true" FInputAtt="class='text daterangepicker'" />
        <jgrid:col runat="server" id="cTimeSpent" HText="Time Spent (sec)" ColType="Text" FormatString="#total_time#" SortField="(select sum(time_spent) from d_attempt_answers where attempt_id=d_attempts.id)" FField="(select sum(time_spent) from d_attempt_answers where attempt_id=d_attempts.id)" FPartialMatch="true" CInputAtt="class='text'" />
        <jgrid:col runat="server" id="cScore" HText="Score" ColType="Text" FormatString="Points" SortField="d_attempts.Points" FField="d_attempts.Points" FPartialMatch="true" CInputAtt="class='text'" />
        <jgrid:col runat="server" id="cPercentage" HText="Percentage" ColType="Text" FormatString="#Percentage#%" SortField="#" FField="" FPartialMatch="true" CInputAtt="class='text'" />
        <jgrid:col runat="server" id="cEmail" HText="Email" ColType="Text" FormatString="Email" SortField="d_users.Email" FField="d_users.Email" FPartialMatch="true" CInputAtt="class='text'" />
        <jgrid:col runat="server" id="cCompany" HText="Company" ColType="Text" FormatString="company_name" SortField="d_users.company_name" FField="d_users.company_name" FPartialMatch="true" CInputAtt="class='text'" />
        <jgrid:col id="cOptions" HAtt="class='device-info'" CAtt="class='functions'" runat="server" HText="Options" ColType="Options" FormatString="<a href='javascript:void(0);' onclick='window.open(&quot;player.aspx?answers=true&attempt_id=#ID#&quot;,null,&quot;menubar=0,location=0,status=0,directories=0,toolbar=0,resizable=1&quot;)'><img src='../inc/images/icons/book_open.png' alt='Review Answers'  title='Review Answers' /></a>  <a href='javascript:void(0);' onclick='fnJGridRowDel(this);'><img src='../inc/images/icons/cancel.png' alt='Delete this attempt? This action can NOT be undone!'  title='Delete this setting' /></a>" />
    </Cols>
</jgrid:jGridAdo>  

<script language="javascript">
    $('.page_number').change(function (event) { oGrid.MetaData.Page = cInt(this.value) - 1; oGrid.addRequest(oGrid.buildRequest("refresh")); });
    $('.page_size').change(function (event) { oGrid.Views[0].PageSize = cInt(fnEventElement(event).options[fnEventElement(event).selectedIndex].value); oGrid.addRequest(oGrid.buildRequest("refresh")); });
    $('.grid_prev_page').click(function (event) { if (oGrid.MetaData.Page > 0) { oGrid.MetaData.Page--; oGrid.addRequest(oGrid.buildRequest("refresh")); } });
    $('.grid_next_page').click(function (event) { if (oGrid.MetaData.Page < oGrid.MetaData.Pages) { oGrid.MetaData.Page++; oGrid.addRequest(oGrid.buildRequest("refresh")); } });

    oGrid.registerEventListener("GridCallbackMetaData", function (oEvent) {

        $('.page_number').val(oEvent.Grid.MetaData.Page + 1);
        $('.page_status').html(oEvent.Grid.MetaData.Pages + " page" + (oEvent.Grid.MetaData.Pages == 1 ? "" : "s") + " (" + oEvent.Grid.MetaData.NumRows+" rows)");
        $('.page_size').val(oEvent.Grid.Views[0].PageSize);

        $('.grid_prev_page').attr("src", "../inc/images/icons/pager_arrow_left" + (oEvent.Grid.MetaData.Page == 0 ? "_off" : "") + ".gif");
        $('.grid_prev_page').css("cursor", (oEvent.Grid.MetaData.Page == 0 ? "default" : "pointer"));
        $('.grid_next_page').attr("src", "../inc/images/icons/pager_arrow_right" + (oEvent.Grid.MetaData.Page == oEvent.Grid.MetaData.Pages - 1 ? "_off" : "") + ".gif");
        $('.grid_next_page').css("cursor", (oEvent.Grid.MetaData.Page == oEvent.Grid.MetaData.Pages - 1 ? "default" : "pointer"));

        if (oEvent.Grid.MetaData.StatusMessage != "") fnUpdateStatusMessage(replaceAll(oEvent.Grid.MetaData.StatusMessage, "\n", "<br>"));
    });

    oGrid.registerEventListener("GridAjaxComplete", function (oRequest) {
        if (oRequest.Data.status < 200 && oRequest.Data.status >= 300) {
            fnUpdateStatusMessage("<img src='../inc/images/icons/exclamation.png'/>&nbsp;Server returned error:<br><span style='color:red'>" + oRequest.Data.responseText.split("\n")[0] + "</span>");
            alert("Error: " + oRequest.Data.status + "\n" + oRequest.Data.responseText);
        } else {
            fnUpdateStatusMessage("<img src='../inc/images/icons/accept.gif'/>&nbsp;Refresh complete");
        }
    });

    <common:label runat="server" id="lScript" />
</script>

</asp:Content>