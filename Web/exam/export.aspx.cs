﻿//with a as (select * from fn_get_links(1086,'d_exams','2011-04-11 07:18:47.653',0,'') )
//select a.*,Name,ShortName, '' from d_exam_sections, a where a.ObjectID=d_exam_sections.ObjectID and a.TableName='d_exam_sections' and '2011-04-11 07:18:47.653' between d_exam_sections.CreateDate and d_exam_sections.DeleteDate
//union select a.*,cast(Text as varchar(max)),CAST(d_attempt_answers.Points as varchar(255)),CAST(d_attempt_answers.time_spent as varchar(255)) from d_questions left outer join d_attempt_answers on d_attempt_answers.question_id=d_questions.ObjectID and attempt_id=33, a where a.ObjectID=d_questions.ObjectID and a.TableName='d_questions' and '2011-04-11 07:18:47.653' between d_questions.CreateDate and d_questions.DeleteDate
//--union select a.*,cast(Text as varchar(max)),'','' from d_question_options left outer join d_attempt_answers on d_attempt_answers.question_id=d_questions.ObjectID and attempt_id=33, a where a.ObjectID=d_questions.ObjectID and a.TableName='d_questions' and '2011-04-11 07:18:47.653' between d_questions.CreateDate and d_questions.DeleteDate
//--union select * from a
//order by sort


using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.functions;
using SpreadsheetGear;
namespace Phoenix.LearningPortal.Exam
{
    public partial class Export : jlib.components.webpage
    {
        private DataTable m_oExamStructure;
        private int m_iExamReleaseID = -1;

        protected void Page_Init(object sender, EventArgs e)
        {
            gAttempts.GridBeforeDatabind += new jlib.controls.jgrid.OnGridBeforeDatabindHandler(gAttempts_GridBeforeDatabind);
            gAttempts.DataHelper = new ado_helper("sql.dsn.cms");
            gAttempts.GridCustomData += new jlib.controls.jgrid.OnGridCustomDataHandler(gAttempts_GridCustomData);
            gAttempts.GridRowPrerender += new jlib.controls.jgrid.OnGridRowPrerenderHandler(gAttempts_GridRowPrerender);
            gAttempts.GridRowClientDel += GAttempts_GridRowClientDel;
        }

        private void GAttempts_GridRowClientDel(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            //sqlbuilder.executeDelete(gAttempts.DataHelper, "d_attempts", "id", e.PK);
            sqlbuilder.executeUpdate(gAttempts.DataHelper, "d_attempts", "id", e.PK, "deleted", 1);
            e.Return = true;
            e.ReturnValue = "Attempt ID " + e.PK + " was successfully deleted.";
        }

        void gAttempts_GridBeforeDatabind(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            //note the difference in calculating score after d_attempts.ID>26960
            gAttempts.setDataSourcePaged(@"d_attempts.release_id, d_releases.ObjectID, d_releases.Name, d_attempts.ID, d_attempts.CreateDate, d_attempts.Points, d_users.email, d_users.company_name, isnull((select sum(time_spent) from d_attempt_answers where attempt_id=d_attempts.id),0) as total_time, 
        CAST(CASE WHEN release_id=52 then (select 100*sum(d_attempt_answers.points)/ sum(d_questions.Points) from d_attempt_answers, d_questions where attempt_id=d_attempts.id and d_questions.ObjectID=question_id and d_questions.DeleteDate>GETDATE()             
            and (d_attempts.ID>26960 or ID in (select id1 from l_row_links where table1='d_attempt_answers' and Table2='d_question_options')) 
            and d_questions.Points>0) 
        else (100*d_attempts.Points/CASE WHEN d_attempts.MaxPoints=0 THEN 1 else d_attempts.MaxPoints END) end as int)  as Percentage
", "d_releases, d_attempts, d_users", "d_releases.deleted=0 AND d_attempts.deleted=0 AND d_releases.ID=d_attempts.release_id and d_users.id=d_attempts.user_id");
            //gAttempts.DataSource = sqlbuilder.getDataTable(m_oData, "select * from d_releases, d_attempts, d_users where d_releases.ID=d_attempts.release_id and d_attempts.release_id=8 and d_users.id=d_attempts.user_id

        }
        void gAttempts_GridCustomData(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            if (e.Type == "export")
            {
                int iOldSortCol = gAttempts.SortCol;
                iOldSortCol = 1;
                gAttempts.DataSource = null;
                //gAttempts_GridBeforeDatabind("excel", null);
                string sFileName = io.getUniqueFileName(System.Configuration.ConfigurationManager.AppSettings["temp.path"] + "\\ExamReport-" + Session["user.id"] + "-" + String.Format("{0:MM}{0:dd}{0:yy}-{0:HH}{0:mm}{0:ss}", DateTime.Now) + ".xlsx");
                int iRowCounter = 0;
                gAttempts.saveAsExcel("", null, sFileName, "Exam Export", 0, ref iRowCounter, "Metier Exam Report");
                if (gAttempts.DataSource != null && gAttempts.DataSource.SafeGetValue(0, "Name") == "PM Skills Assessor – PM Essentials")
                {
                    IWorkbook workbookFile = SpreadsheetGear.Factory.GetWorkbookSet().Workbooks.Open(sFileName);
                    workbookFile.Worksheets.Add();
                    workbookFile.Worksheets[1].Name = "Assessment Comments";
                    var values = (SpreadsheetGear.Advanced.Cells.IValues)workbookFile.Worksheets[1];
                    string[] cols = new string[] { "Date", "Comment" };

                    for (int y = 0; y < cols.Length; y++)
                    {
                        workbookFile.Worksheets[1].Cells[0, y].Font.Size = 12;
                        workbookFile.Worksheets[1].Cells[0, y].Interior.Color = System.Drawing.Color.FromArgb(91, 0, 0);
                        workbookFile.Worksheets[1].Cells[0, y].Font.Color = System.Drawing.Color.White;
                        workbookFile.Worksheets[1].Cells[0, y].Font.Bold = true;
                        values.SetText(0, y, cols[y]);
                    }
                    workbookFile.Worksheets[1].Cells[0, 0].ColumnWidth = 20;
                    workbookFile.Worksheets[1].Cells[0, 1].ColumnWidth = 100;
                    DataTable evalEmails = sqlbuilder.getDataTable(new ado_helper("sql.dsn.email"), "select * from d_email_log where email_to like '%eval@bids.no%' order by posted_date desc");
                    for (int x = 0; x < evalEmails.Rows.Count; x++)
                    {
                        values.SetText(x + 1, 0, String.Format("{0:yyyy}-{0:MM}-{0:dd}", evalEmails.Rows[x]["posted_date"]));
                        values.SetText(x + 1, 1, parse.inner_substring(evalEmails.Rows[x]["body"], null, null, "---", null));
                    }
                    workbookFile.Save();
                }
                gAttempts.DataSource = null;
                gAttempts.SortCol = iOldSortCol;
                e.ClientJSAfter = "$('#hDownloadContainer').show();$('#lDownload').attr('href','" + Request.ApplicationPath + "/download.aspx?file=" + System.Web.HttpUtility.UrlEncode(System.IO.Path.GetFileName(sFileName)) + "');";
            }
        }
        void gAttempts_GridRowPrerender(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            if (e.ExcelFile != null)
            {
                if (m_oExamStructure == null || m_iExamReleaseID != convert.cInt(e.DBRow["release_id"]))
                    m_oExamStructure = sqlbuilder.getDataTable(gAttempts.DataHelper, String.Format("with a as (select * from fn_get_links({0},'d_exams','{1}',0,'') ) select a.*,Name,ShortName from d_exam_sections, a where a.ObjectID=d_exam_sections.ObjectID and a.TableName='d_exam_sections' and '{1}' between d_exam_sections.CreateDate and d_exam_sections.DeleteDate union select a.*,cast(Text as varchar(max)),CAST(d_questions.Points as varchar(255)) from d_questions, a where a.ObjectID=d_questions.ObjectID and a.TableName='d_questions' and '{1}' between d_questions.CreateDate and d_questions.DeleteDate order by sort", e.DBRow["ObjectID"], e.DBRow["CreateDate"]));

                if (m_iExamReleaseID > 0) e.RowIndex += 2;
                if (e.RowIndex == 0 || m_iExamReleaseID > 0)
                {//Prepare column headings

                }
                m_iExamReleaseID = convert.cInt(e.DBRow["release_id"]);
                //e.ExcelFile.Worksheets[e.ExcelSheetIndex].cr
            }
        }
    }
}