﻿using System;
using System.Xml;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.components;
using jlib.functions;
using System.Text;
using System.Security.Cryptography;
namespace Phoenix.LearningPortal
{
    public partial class Participants : jlib.components.webpage
    {

        private Util.Classes.organization m_oOrganization;
        private Util.Classes.user m_oActiveUser;
        private class ParticipantData
        {
            public ParticipantData(Util.Classes.Participant participant)
            {
                Participant = participant;
            }
            public Util.Classes.Participant Participant;
            public List<string> EnrolledCourses = new List<string>();
            public int NumPoints;
        }
        public void Page_Load(object sender, EventArgs e)
        {
            m_oActiveUser = (this.Master as Inc.Master.MyMetier).ActiveUser;
            m_oOrganization = m_oActiveUser.Organization;

            if (m_oOrganization.CompetitionMode == 2) Response.Redirect("error.aspx");
            (Page.Master as Inc.Master.MyMetier).Section = Inc.Master.MyMetier.SiteSection.Participants;

            if (!this.IsPostBack && this.QueryStringLong("class_id") > 0) hFilterClass.Value = "," + this.QueryStringLong("class_id") + ",";
            if (!this.IsPostBack && this.QueryStringLong("ilm_id") > 0) hFilterIlm.Value = "," + this.QueryStringLong("ilm_id") + ",";

            Dictionary<int, ParticipantData> participants = new Dictionary<int, ParticipantData>();
            m_oActiveUser.Colleagues.ForEach(participant =>
            {
                participants.Add(participant.Id, new ParticipantData(participant));
            });

            Util.Phoenix.session.RequestResult result = m_oActiveUser.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.CourseCalendar, m_oActiveUser.OrganizationID), true, true);
            if (!result.Error)
            {
                foreach (dynamic course in result.DataObject)
                {
                    Util.Classes.ActivitySet set = new Util.Classes.ActivitySet(course, m_oActiveUser);
                    //if (parse.removeXChars(hFilterIlm.Value, "0123456789").IsNullOrEmpty() || ("," + hFilterIlm.Value + ",").Contains("," + set.Id + ",")) {
                    set.Activities.ForEach(activity =>
                    {
                        Util.Phoenix.session.RequestResult activityPerformances = m_oActiveUser.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.UserPerformancesGetByActivity, activity.Id, true, true));
                        if (!activityPerformances.Error)
                        {
                            foreach (dynamic performance in activityPerformances.DataObject)
                            {
                                ParticipantData participantData = null;
                                if (participants.TryGetValue(convert.cInt(performance.UserId), out participantData))
                                {
                                    participantData.EnrolledCourses.Add(activity.Name);
                                    participantData.NumPoints += performance.TotalScore;
                                }
                            }
                        }
                    });
                    //}
                }
            }

            bool showPoints = m_oOrganization.CompetitionMode == 0;
            List<dynamic> participantList = new List<dynamic>();
            participants.ToList().ForEach(participant =>
            {
                participantList.Add(new { Id = participant.Key, Points = (showPoints ? participant.Value.NumPoints : -1), Courses = participant.Value.EnrolledCourses, FName = participant.Value.Participant.FirstName, LName = participant.Value.Participant.LastName });
            });
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "data", "var participants=" + jlib.functions.json.DynamicJson.Serialize(participantList) + ";", true);

        }
    }
}