﻿<%@ Page Title="Metier Course Exporter" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Tools.Tools" Codebehind="tools.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
<h2 class="page-title"><img src="../inc/images/icons/tools_32.png" alt="Import Data" />Tools</h2>
<table>
<tr>
<td>Method</td>
<td><common:dropdown runat="server" ID="dAction" AutoPostBack="true">
<asp:ListItem Value="transform" Text="Transform" />

</common:dropdown></td>
</tr>
<tr runat="server" id="rXsl">
<td>Xsl</td>
<td>
<common:dropdown runat="server" ID="dXsl">
<asp:ListItem Value="email-class-enroll.xsl" Text="Email: MLS - Classroom Enrollment Notification" />
<asp:ListItem Value="email-class-reminder.xsl" Text="Email: MLS - Classroom Reminder" />
<asp:ListItem Value="email-schedule.xsl" Text="-Email: Elearning Schedule" />
<asp:ListItem Value="email-elearn-course-completion.xsl" Text="Email: MLS - eLearning Course Completion" />
<asp:ListItem Value="email-elearn-enroll.xsl" Text="Email: MLS - eLearning Course Enrollment" />
<asp:ListItem Value="email-elearn-ahead-of-schedule.xsl" Text="Email: MLS - eLearning Ahead Schedule" />
<asp:ListItem Value="email-elearn-behind-schedule.xsl" Text="Email: MLS - eLearning Behind Schedule" />
<asp:ListItem Value="email-exam-case-enroll.xsl" Text="Email: MLS - Case Exam Enrollment" />
<asp:ListItem Value="email-exam-mc-enroll.xsl" Text="Email: MLS - Multiple Choice Exam Enrollment" />

<asp:ListItem Value="email-password-reset.xsl" Text="Email: LMT Password Reset Template" />
<asp:ListItem Value="email-registration-with-password.xsl" Text="Email: Registration Email with Password" />

<asp:ListItem Value="report-email-enrollment.xsl" Text="Email: Enrollment Report" />

<asp:ListItem Value="report-email-crediting.xsl" Text="Email: Credited Report" />
<asp:ListItem Value="screen-invoice-preview.xsl" Text="Screen: Invoice Preview" />
    <asp:ListItem Value="email-forsvaret-sertifisert-prosjektleder.cshtml" Text="Email: Forsvaret Sertifisert Prosjektleder" />
    <asp:ListItem Value="email-forsvaret-sertifisert-prosjektmedarbeider.cshtml" Text="Email: Forsvaret Sertifisert Prosjektmedarbeider" />
    <asp:ListItem Value="email-forsvaret-merkantilt-grunnleggende.cshtml" Text="Email: Forsvaret Merkantilt Grunnleggende" />
    


</common:dropdown> 

&nbsp;(Or, XSL: <common:textbox runat="server" ID="tXsl" Width="450px" />) (<common:checkbox runat="server" ID="cPDF" Text="As PDF" />, <common:checkbox runat="server" ID="cStripEmailHeaderFields" Text="Strip Email Header Fields" />)
</td>
</tr>
<tr>
    <td>Language: </td>
    <td><common:dropdown runat="server" ID="dLanguage">
    <asp:ListItem Value="" Text="Auto" />
    <asp:ListItem Value="en" Text="English" />
    <asp:ListItem Value="no" Text="Norwegian" />
    <asp:ListItem Value="sv" Text="Swedish" />
    <asp:ListItem Value="da" Text="Danish" />
    <asp:ListItem Value="nl" Text="Dutch" />
    <asp:ListItem Value="de" Text="German" />
    <asp:ListItem Value="fr" Text="French" />
    <asp:ListItem Value="es" Text="Spanish" />
    <asp:ListItem Value="pt" Text="Portugese" />
    <asp:ListItem Value="pl" Text="Polish" />
    
    </common:dropdown></td>
</tr>

<tr>
    <td><common:label runat="server" ID="lText1" Text="Url" /></td>
    <td><common:textbox runat="server" ID="tXmlUrl" Text="/webservices/data.aspx?mode=enrollment&user_id=11536&ilm_id=1555165" Width="450px" /></td>
</tr>
<tr>
    <td>Custom Settings/Manual XML</td>
    <td>
    <common:textbox runat="server" ID="tSettings" TextMode="MultiLine" style="height:50px;width:200px" />
    <br />(Custom Settings: Gets added to DocumentRoot as attributes. Should use syntax: mode="batch" One on each line. <br />If value starts with &lt;, then this gets processed as input XML (URL is ignored))
    </td>
</tr>
</table>
<br />
<common:button.button buttonmode="submitbutton" text="Submit" runat="server" />
<common:button.button buttonmode="submitbutton" text="Test all" runat="server" id="bTestAll" />
<br /><br />
<common:button.button buttonmode="submitbutton" text="Reset cache" runat="server" id="bResetCache" />
</asp:Content>