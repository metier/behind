﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using jlib.functions;
using jlib.db;
using jlib.net;
using System.Data;

namespace Phoenix.LearningPortal.Tools
{
    public partial class EmailLog : jlib.components.webpage
    {

        public void Page_Init(object sender, EventArgs e)
        {
            gGrid.GridViewsLoad += new jlib.controls.jgrid.OnGridViewsLoadHandler(gGrid_GridViewsLoad);
            gGrid.GridViewsSave += new jlib.controls.jgrid.OnGridViewsSaveHandler(gGrid_GridViewsSave);
            gGrid.GridBeforeDatabind += new jlib.controls.jgrid.OnGridBeforeDatabindHandler(gGrid_GridBeforeDatabind);
            gGrid.GridCustomData += new jlib.controls.jgrid.OnGridCustomDataHandler(gGrid_GridCustomData);
            gGrid.GridBeforeFilter += new jlib.controls.jgrid.OnGridBeforeFilterHandler(gGrid_GridBeforeFilter);
            //gGrid.GridRowPrerender += new jlib.controls.jgrid.OnGridRowPrerenderHandler(gGrid_GridRowPrerender);
        }
        private Util.Classes.user m_oActiveUser = null;
        public Util.Classes.user ActiveUser
        {
            get
            {
                if (m_oActiveUser == null) m_oActiveUser = (Page.Master as Inc.Master.Default).ActiveUser;
                return m_oActiveUser;
            }
        }

        void gGrid_GridBeforeFilter(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            string sFilter = gGrid.getFilter("cSent");
            if (sFilter != "") e.Filter.Add("sent", new condition("sent=" + (sFilter.Bln() ? "1" : "0")));
        }
        public void Page_Load(object sender, EventArgs e)
        {
            if (this.Request["action"] == "download_attachment" && this.QueryString("filename") != "")
            {
                Response.ContentType = jlib.net.HTTP.lookupMimeType(this.QueryString("filename"));
                Response.BinaryWrite(io.read_file_binary(System.Configuration.ConfigurationManager.AppSettings["email.attachment.path"] + "\\" + this.QueryString("filename")));
                Response.End();
            }
            if (this.QueryString("action") == "render_email")
            {
                DataTable oDT = sqlbuilder.executeSelect(new ado_helper("sql.dsn.email"), "d_email_log", "id", this.QueryStringInt("email_id"));
                Response.Clear();

                if (oDT.Rows.Count > 0)
                {
                    string sAttachments = "";
                    string[] s = parse.split(oDT.Rows[0]["attachments"], "\n");
                    for (int x = 0; x < s.Length; x++)
                    {
                        if (s[x].Trim() != "")
                        {
                            sAttachments += (sAttachments == "" ? "" : " ") + "<a href=\"/tools/email_log.aspx?action=download_attachment&filename=" + System.IO.Path.GetFileName(s[x]) + "\" target=\"blank\">" + parse.inner_substring(System.IO.Path.GetFileName(s[x]), "-", null, null, null) + "</a>";
                        }
                    }
                    //body, td{font-family:arial,sans-serif;font-size:80%}
                    //a:link, a:active, a:visited{color:#189f31}
                    string sEmail = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><title>Metier&reg; Email: " + oDT.Rows[0]["subject"] + "</title><style type=\"text/css\">" + @"
body, td{font-family:arial,sans-serif;}

img{border:0}
body { margin: 0; " + (Request.Browser.Browser == "IE" ? "padding-right: 20px;" : "") + @" }
pre.header table td{ font-size: 13px;}
pre.header table td a{ font-size: 11px; }"
    + (this.QueryString("render") != "pdf" ? "" : "pre.header table td.options{display:none;}") +
    @"pre.header {background-color: #e5e5e5; border-bottom: 1px solid #ccc; margin: 0 0 15px; padding: 10px 15px;}
pre.body{font-size: 13px;margin: 0 15px 15px 0;padding: 10px 15px;}
div.body{margin: 0 15px 15px 0;padding: 10px 15px;}
</style></head><body>
<pre class='header'><table style='width:100%'><tr><td style='white-space: nowrap' valign='top'>Email to: </td><td style='width:100%' colspan='2'><strong>" + parse.replaceAll(oDT.Rows[0]["email_to"], "; ", ";", ";", "<br>", "<br><br>", "<br>") + @"</strong></td><td style='white-space: nowrap' valign='top'>Date:&nbsp;&nbsp;<b>" + String.Format("{0:ddd M/d/yyyy HH:mm:ss}", oDT.Rows[0]["posted_date"]) + @"</b></td></tr>
<tr><td style='white-space: nowrap' valign='top'>Email from:&nbsp;&nbsp;</td><td><strong>" + parse.splitValue(parse.stripLeadingCharacter(oDT.Rows[0]["email_from"], " ", ";"), ";", 0) + @"</strong></td></tr>
<tr><td>Subject: </td><td colspan='2'><strong>" + oDT.Rows[0]["subject"] + @"</strong></td><td align='right' class='options' style='float:right'><a href='javascript:void()' onclick='window.open(" + "\"" + Request.Url + "&render=pdf\");'>export to PDF</a></td></tr>" + (sAttachments == "" ? "" : "<tr><td>Attachments:&nbsp;&nbsp;</td><td>" + sAttachments + "</td></tr>")
                    + "</table></pre>" + (convert.cBool(oDT.Rows[0]["is_html"]) ? "<div class='body'>" : "<pre class='body'>") + oDT.Rows[0]["body"] + (convert.cBool(oDT.Rows[0]["is_html"]) ? "</div>" : "</pre>") + "</body></html>";

                    if (this.QueryString("render") == "pdf")
                    {
                        byte[] oPFD = Util.Cms.cPDF(sEmail, "", "");
                        Response.Clear();
                        Response.AddHeader("Content-Type", "application/pdf");
                        Response.AddHeader("Content-Disposition", "attachment; filename=Email.pdf; size=" + oPFD.Length.ToString());
                        Response.BinaryWrite(oPFD);
                    }
                    else
                    {
                        Response.Write(sEmail);
                    }
                }
                Response.End();
            }
        }


        void gGrid_GridCustomData(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            if (e.Type == "resend-emails")
            {
                DataTable oEmails = sqlbuilder.getDataTable(new ado_helper("sql.dsn.email"), "select * from d_email_log where id in (" + parse.removeXChars(e.Argument, "0123456789,") + ")");
                for (int x = 0; x < oEmails.Rows.Count; x++)
                {
                    List<KeyValuePair<string, byte[]>> oFiles = new List<KeyValuePair<string, byte[]>>();
                    string[] sFiles = parse.split(oEmails.Rows[x]["attachments"], "\n");
                    for (int y = 0; y < sFiles.Length; y++)
                    {
                        if (sFiles[y].Trim() != "")
                        {
                            byte[] bByte = io.read_file_binary(sFiles[y]);
                            if (bByte.Length > 0) oFiles.Add(new KeyValuePair<string, byte[]>(parse.inner_substring(System.IO.Path.GetFileName(sFiles[y]), "-", null, null, null), bByte));
                        }
                    }
                    Util.Email.sendEmail(convert.cStr(oEmails.Rows[x]["email_from"]), convert.cStr(oEmails.Rows[x]["email_to"]), convert.cStr(oEmails.Rows[x]["email_cc"]), convert.cStr(oEmails.Rows[x]["email_bcc"]), convert.cStr(oEmails.Rows[x]["subject"]), convert.cStr(oEmails.Rows[x]["body"]), convert.cStr(oEmails.Rows[x]["body"]).IndexOf("<") > -1, oFiles, true, 0, "sql.dsn.cms");
                }
                e.Return = true;
                e.ReturnValue = oEmails.Rows.Count + " successfully queued.";
            }
        }


        void gGrid_GridViewsSave(jlib.controls.jgrid.jGrid oGrid)
        {
            Util.Permissions.setSetting("grid.view", "email-log", ActiveUser.ID, oGrid.Views.serialize());
        }

        void gGrid_GridViewsLoad(jlib.controls.jgrid.jGrid oGrid)
        {
            gGrid.Views.parse(Util.Permissions.getSetting("grid.view", "email-log", ActiveUser.ID, ""), true);
        }

        void gGrid_GridBeforeDatabind(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            gGrid.DataHelper = new ado_helper("sql.dsn.cms");
            gGrid.setDataSourcePaged("*", "d_email_log", "deleted=0");
        }

    }
}