﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Linq;
using jlib.functions;
using jlib.db;
using jlib.net;
using System.Data;
using System.Configuration;

namespace Phoenix.LearningPortal.Tools
{
    public partial class EzHandler : jlib.components.webpage
    {

        public new void Page_PreInit(object sender, EventArgs e)
        {
            (Page.Master as Inc.Master.Default).DisableLoginRedirect = true;
            (Page.Master as Inc.Master.Default).DisableWrapper = true;
            base.Page_PreInit(sender, e);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            (Page.Master as Inc.Master.Default).InitiatePageRequestLogger(true);
            if (Request.HttpMethod == "POST")
            {
                //handle request from ez User Create

                Util.Phoenix.session session = new Util.Phoenix.session(System.Configuration.ConfigurationManager.AppSettings["ez.api.token"]);
                Util.Phoenix.session.RequestResult result = session.PhoenixRequest("GET", Util.Phoenix.session.Queries.CountryCodesGet, null, 60 * 60 * 24, true, false);
                Dictionary<string, string> codes = new Dictionary<string, string>();
                foreach (dynamic code in result.DataObject)
                    codes.Add(convert.cStr(code.Value).ToUpper(), convert.cStr(code.Display).ToUpper());

                string country = Request["adr-country"];
                int distributorId = 1194, languageId = 45;
                if (Request["source"].Str().EndsWith(".se"))
                {
                    distributorId = 1195;
                    country = "SE";
                    languageId = 46;
                }

                country = country.Str().Trim().ToUpper();
                if (!country.IsNullOrEmpty() && !codes.ContainsKey(country))
                {
                    if (codes.ContainsValue(country)) country = codes.FindKeysByValue(country).ToList()[0];
                    else
                    {
                        country = parse.replaceAll(country, "SVERIGE", "SV", "DANMARK", "DK", "NORGE", "NO", "DEUTSCHLAND", "DE");
                        //                    if (!codes.ContainsValue(payment.AddressCountry)) payment.AddressCountry = "NO";
                    }
                }

                //These are not mapped in Phoenix: "Invoice name", "Allow enroll","User notifications", , "Invoice account id", "Billable", "User notifications", "Comments", "Notification level"
                session = new Util.Phoenix.session(true);
                int userId, customerId = Request["extension-userOrgId"].Int();
                string department = Request["extension-CusAttr-Department"].Str();
                if (department.Contains("|") && convert.cInt(department.SafeSplit("|", 0)) > 0)
                {
                    customerId = convert.cInt(department.SafeSplit("|", 0));
                    department = department.Substring(department.SafeSplit("|", 0).Length + 1);
                }
                //if this is coming from a portal belonging to a Corporate customer
                Util.Classes.organization organization = null;
                if (customerId > 0)
                {
                    organization = Util.Classes.user.getOrganization(customerId);
                    var newUser = new
                    {
                        CustomerId = customerId,
                        Username = Request["email"].Trim(),
                        password = "test",
                        passwordRepeat = "test",
                        Email = Request["email"].Trim(),
                        Roles = new List<string>() { "LearningPortalUser" },
                        DistributorId = (null as string),
                        User = new
                        {
                            MembershipUserId = Request["email"].Trim(),
                            FirstName = Request["extension-firstName"],
                            LastName = Request["extension-lastName"],
                            PhoneNumber = Request["extension-CusAttr-Telephone"],
                            StreetAddress = Request["adr-extadd"],
                            ZipCode = Request["adr-pcode"],
                            City = convert.cStr(Request["adr-region"], Request["adr-locality"]),
                            Country = country,
                            PreferredLanguageId = convert.cInt(Request["preferred-language"], organization.LanguageId, 43),
                            CustomerId = customerId,
                            UserRegistrationNotificationDate = System.DateTime.Now.AddDays(-1),
                            UserNotificationType = 0,
                            UserInfoElements = new List<object>() { }
                        }
                    };

                    //process user creation                
                    result = session.PhoenixGet(String.Format(Util.Phoenix.session.Queries.UserGetByUsername, System.Web.HttpUtility.UrlEncode(newUser.Username)), false, false);
                    if (result.HttpResponseCode == 400 || result.HttpResponseCode == 404) result = session.PhoenixGet(String.Format(Util.Phoenix.session.Queries.UserGetByEmail, System.Web.HttpUtility.UrlEncode(newUser.Username)), false, false);
                    if (result.HttpResponseCode == 404 || result.HttpResponseCode == 500 || result.HttpResponseCode == 400)
                    {
                        if (organization.OrganizationObject == null)
                        {
                            throw new Exception("Organization ID=" + newUser.CustomerId + " not found!");
                        }
                        else
                        {
                            List<string> ilearnFields = new List<string>() { "Order reference 1", "Order reference 2", "Immediate superior name", "Immediate superior email", "Position1", "Department", "Employee ID" };
                            int[] values = new int[] { 11, 12, 16, 17, 15, 14, 13 };
                            foreach (string field in Request.Form)
                            {
                                string key = parse.replaceAll(field, "extension-CusAttr-", "");
                                if (ilearnFields.Contains(key))
                                {
                                    newUser.User.UserInfoElements.Add(new
                                    {
                                        LeadingTextId = values[ilearnFields.IndexOf(key)],
                                        InfoText = (key == "Department" ? department : Request.Form[field])
                                    });
                                }
                            }
                        }
                        result = session.PhoenixPost(Util.Phoenix.session.Queries.UserCreate, jlib.functions.json.DynamicJson.Serialize(newUser));
                        if (result.Error) throw new Exception("User create failed! " + result.HttpErrorMessage, result.RequestError);
                        userId = convert.cInt(result.DataObject.Id);
                        customerId = convert.cInt(result.DataObject.CustomerId);
                    }
                    else
                    {
                        userId = convert.cInt(result.DataObject.User.Id);
                        //Keep customerId value from query and use for enrollment (since we want to bill to customerId)
                        if (customerId < 1) customerId = convert.cInt(result.DataObject.CustomerId);

                        //Send password reminder email
                        var resetResult = session.PhoenixPut(String.Format(Util.Phoenix.session.Queries.UserPasswordReset, result.DataObject.Username));
                    }
                }
                else
                {
                    //create new arbitrary customer (from metier.se)

                    var request = new
                    {
                        DistributorId = distributorId,
                        CompanyName = convert.cStr(Request["extension-CusAttr-Invoice name"], Request["extension-firstName"] + " " + Request["extension-lastName"]).Trim(),
                        CompanyRegistrationNumber = Request["CompanyRegistrationNumber"],
                        OurRef = "Charlotte Wikdal Halle",
                        Email = Request["email"].Trim(),
                        FirstName = Request["extension-firstName"],
                        LastName = Request["extension-lastName"],
                        Phone = Request["extension-CusAttr-Telephone"],
                        StreetAddress1 = Request["adr-extadd"],
                        StreetAddress2 = "",
                        ZipCode = convert.cStr(Request["adr-pcode"], Request["pcode"]).Trim(),
                        City = convert.cStr(Request["adr-region"], Request["adr-locality"]).Trim(),
                        Country = convert.cStr(country, Request["country"], "NO").Trim(),
                        LanguageId = languageId,
                        PreferredLanguageId = languageId
                    };

                    result = session.PhoenixPost(Util.Phoenix.session.Queries.ArbitraryUserCreate, jlib.functions.json.DynamicJson.Serialize(request));
                    if (result.HttpResponseCode != 200) throw new Exception("ArbitraryUserCreate threw an error. Code: " + result.HttpResponseCode + " / message: " + result.HttpErrorMessage + "\n\nData:\n" + jlib.functions.json.DynamicJson.Serialize(request));
                    userId = convert.cInt(result.DataObject.UserId);
                    customerId = convert.cInt(result.DataObject.ArbitraryCustomerId);
                }

                //process optional enrollment
                string activityErrors = "";
                List<int> activitySetIds = new List<int>();
                for (int x = 0; x < Request.Form.Keys.Count; x++)
                {
                    if (Request.Form.Keys[x].StartsWith("_enroll:"))
                    {
                        List<string> ids = parse.split(Request.Form[x], ",").ToList();
                        ids.ForEach(item =>
                        {
                            if (item.StartsWith("i-")) activitySetIds.AddIfNotExist(item.Substring(2).Int());
                            else if (item.StartsWith("c-")) activityErrors += item.Substring(2) + ",";
                        });
                    }
                }
                if (!activityErrors.IsNullOrEmpty())
                {
                    Util.Email.sendEmail("noreply@metier.no", "phoenix@metier.zendesk.com", "", "", "Trying to enroll user directly in activity", "The user " + Request["email"] + " directly in activities: " + activityErrors + ".<br />This is not supported in Phoenix.", true, null);
                }
                if (activitySetIds.Count > 0)
                {
                    string errors = "";

                    string data = "";
                    if (!Request["extension-CusAttr-Order reference 1"].IsNullOrEmpty())
                    {
                        var enrollmentDetails = new
                        {
                            InfoElements = new List<object> {
                                new {
                                    InfoText = Request["extension-CusAttr-Order reference 1"],
                                    LeadingTextId = 11
                                }
                            }
                        };
                        
                        data = jlib.functions.json.DynamicJson.Serialize(enrollmentDetails);
                    }

                    string url = "activitysets/enroll?userId=" + userId + "&customerId=" + customerId + "&activitySetId=" + activitySetIds.Join("&activitySetId=");
                    Util.Phoenix.session.RequestResult enrollResult = session.PhoenixPut(url, data);
                    if (enrollResult.HttpResponseCode != 200) errors += "* URL: " + url + " / Message: " + convert.SafeGetProperty(enrollResult.HttpErrorJSON, "ValidationErrors") + "<br />";
                    if (!errors.IsNullOrEmpty())
                        Util.Email.sendEmail("noreply@metier.no", "phoenix@metier.zendesk.com", "", "", "Enrollment error!", "Couldn't enroll user " + Request["email"] + " in activitysets:<br />" + errors + ".<br />", true, null);
                }

                //Send receipt email, if included in POST
                string receiptEmail = Request["email-receipt"].Str();
                if (receiptEmail.IsNotNullOrEmpty())
                {
                    Util.Email.SendRazorEmail(new { Request = Request, Template = receiptEmail }, receiptEmail, receiptEmail, "", true, "");
                }
            }
        }
    }

}