﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Tools
{
    public partial class PageViews : jlib.components.webpage
    {

        public void Page_Init(object sender, EventArgs e)
        {
            gGrid.GridViewsLoad += new jlib.controls.jgrid.OnGridViewsLoadHandler(gGrid_GridViewsLoad);
            gGrid.GridViewsSave += new jlib.controls.jgrid.OnGridViewsSaveHandler(gGrid_GridViewsSave);
            //gGrid.GridBeforeDatabind += new jlib.controls.jgrid.OnGridBeforeDatabindHandler(gGrid_GridBeforeDatabind);
            gGrid.GridCustomData += new jlib.controls.jgrid.OnGridCustomDataHandler(gGrid_GridCustomData);
            //gGrid.GridRowPrerender += new jlib.controls.jgrid.OnGridRowPrerenderHandler(gGrid_GridRowPrerender);
            gGrid.QuerySource = AttackData.PageViews.GetAllForGrid();
        }
        private Util.Classes.user m_oActiveUser = null;
        public Util.Classes.user ActiveUser
        {
            get
            {
                if (m_oActiveUser == null) m_oActiveUser = (Page.Master as Inc.Master.Default).ActiveUser;
                return m_oActiveUser;
            }
        }
        public void Page_Load(object sender, EventArgs e)
        {

        }


        void gGrid_GridCustomData(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {

        }


        void gGrid_GridViewsSave(jlib.controls.jgrid.jGrid oGrid)
        {
            Util.Permissions.setSetting("grid.view", "page-views", ActiveUser.ID, oGrid.Views.serialize());
        }

        void gGrid_GridViewsLoad(jlib.controls.jgrid.jGrid oGrid)
        {
            gGrid.Views.parse(Util.Permissions.getSetting("grid.view", "page-views", ActiveUser.ID, ""), true);
        }
    }
}