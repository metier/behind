﻿<%@ Page Title="Metier Course Exporter" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Tools.Settings" Codebehind="settings.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
<h2 class="page-title"><img src="../inc/images/icons/tools_32.png" alt="Email Log" />Manage Settings</h2>

<style>
hr{    
    display:block;
}
</style>

<div id="cStatus" style="visibility:hidden"><img src='../inc/images/icons/accept.gif'/>&nbsp;Refresh complete</div>
<br style="clear: both;" />

<table cellpadding="0" cellspacing="0" border="0" class="listing assets" style="margin-bottom:0;"><thead><tr class="actions"><td><table cellpadding="0" cellspacing="0"><thead><tr>
<td><span id="hGridView"></span></td>
<td class="paging">Page <img src="../inc/images/icons/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow grid_prev_page" /> <input type="text" value="1" class="text page page_number" /> <a href="javascript:void(0);" title="Next page" ><img src="../inc/images/icons/pager_arrow_right.gif" alt="Go to Next page" class="arrow grid_next_page" /></a> of <span class="page_status">3 pages</span> <span class="separator">|</span> View <select name="limit" class="page_size"><option value="20" selected="selected">20</option><option value="30">30</option><option value="50">50</option><option value="100">100</option><option value="200">200</option></select> per page</td>
</tr></thead></table></td></tr></thead></table>

<jgrid:jGridAdo DisableInitialFiltering="true" runat="server" id="gGrid" LoadViewContainer="hGridView" BindDropdownByID="true" OnRowDoubleClick="fnJGridRowEdit(this);" style="width:956px;" SortOrder="desc" JSName="oGrid" AutoFilter="true" PageSize="50" SortCol="0" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets' style='border-top:0;margin-bottom:0'" DeleteConfirmation="Are you sure you wish to delete this setting?" DisplayFilter="true" DisplayAdd="true">
    <Cols>
        <jgrid:col id="cID" runat="server" HText="ID" ColType="Hide" PrimaryKey="true" FormatString="ID" SortField="key_name, sub_key" />
        <jgrid:col id="cKey" runat="server" CInputAtt="jsname='cKey' gettext='return oRow[0];' watermark='(Please select...)' class='JCombo' style='width:150px;' listwidth='250px' arrow='true' data='JComboKeys' multi='false'" HText="Key" FormatString="#key_name#" SortField="key_name" FField="key_name" FPartialMatch="true" FInputAtt="class='text'" FColType="Text" ColType="JCombo" datamember="key_name" />   
        <jgrid:col id="cSubKey" CInputAtt="style='width:130px' class='text'" runat="server" HText="Sub-key" FormatString="sub_key" SortField="sub_key" FField="sub_key" FPartialMatch="true" ColType="Text" FInputAtt="class='text'" FColType="Text" datamember="sub_key" />   
        <jgrid:col id="cValue" CInputAtt="style='width:130px' class='text'" runat="server" HText="Value" FormatString="value" SortField="#" FField="value" FPartialMatch="true" ColType="Text" FInputAtt="class='text'" FColType="Text" datamember="value" />
        <jgrid:col id="cUser" CInputAtt="style='width:130px' class='text'" runat="server" HText="User ID" FormatString="user_id" SortField="user_id" FField="user_id" FPartialMatch="true" ColType="Text" FInputAtt="class='text'" FColType="Text" datamember="user_id" />   
                        
        <jgrid:col id="cOptions" HAtt="class='functions'" CAtt="class='functions' style='white-space:nowrap;width:80px'" runat="server" HText="" ColType="Options" FormatString="<a href='javascript:void(0);' onclick='fnJGridRowEdit(this);'><img src='../inc/images/icons/pencil.png' alt='Edit Setting' title='Edit Setting' /></a>&nbsp;<a href='javascript:void(0);' onclick='fnJGridRowDel(this);'><img src='../inc/images/icons/cancel.png' alt='Delete this setting' title='Delete this setting' /></a>" />              
    </Cols>                
</jgrid:jGridAdo>

<table cellpadding="0" cellspacing="0" border="0" class="listing assets" style="margin-bottom:0;"><thead><tr class="actions"><td><table cellpadding="0" cellspacing="0"><thead><tr>
<td></td>
<td class="paging">Page <img src="../inc/images/icons/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow grid_prev_page" /> <input type="text" value="1" class="text page page_number" /> <a href="javascript:void(0);" title="Next page" ><img src="../inc/images/icons/pager_arrow_right.gif" alt="Go to Next page" class="arrow grid_next_page" /></a> of <span class="page_status">3 pages</span> <span class="separator">|</span> View <select name="limit" class="page_size"><option value="20" selected="selected">20</option><option value="30">30</option><option value="50">50</option><option value="100">100</option><option value="200">200</option></select> per page</td>
</tr></thead></table></td></tr></thead></table>
	
<script>
    fnGridAttach(oGrid);    
    oGrid.registerEventListener("GridRenderCell", function (oEvent) {
        if (oEvent.Data.Index >= 0 && oEvent.Data.ColObject.ID == "cValue") {
            if (oEvent.Data.Editing)
                oEvent.Data.HTML = "<textarea style='width:180px' jid='" + oEvent.Data.ColObject.ID + "'>" + oEvent.Data.Grid.getEditingData(oEvent.Data.ID, oEvent.Data.Col) + "</textarea>";
            else
                oEvent.Data.HTML = oEvent.Data.HTML.split("<").join("&lt;").split(">").join("&gt;").split("\n").join("<br/>");
        }
    });
</script>


</asp:Content>