﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using jlib.functions;
using jlib.db;
using jlib.net;
using System.Data;
using System.Net;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Tools
{
    public partial class TerminalLog : jlib.components.webpage
    {

        private Util.Classes.user m_oActiveUser = null;
        public Util.Classes.user ActiveUser
        {
            get
            {
                if (m_oActiveUser == null) m_oActiveUser = (Page.Master as Inc.Master.Default).ActiveUser;
                return m_oActiveUser;
            }
        }
        public void Page_Init(object sender, EventArgs e)
        {
            gGrid.GridViewsLoad += new jlib.controls.jgrid.OnGridViewsLoadHandler(gGrid_GridViewsLoad);
            gGrid.GridViewsSave += new jlib.controls.jgrid.OnGridViewsSaveHandler(gGrid_GridViewsSave);
            gGrid.GridBeforeDatabind += new jlib.controls.jgrid.OnGridBeforeDatabindHandler(gGrid_GridBeforeDatabind);
            gGrid.GridCustomData += new jlib.controls.jgrid.OnGridCustomDataHandler(gGrid_GridCustomData);
            gGrid.GridBeforeFilter += new jlib.controls.jgrid.OnGridBeforeFilterHandler(gGrid_GridBeforeFilter);
            //gGrid.GridRowPrerender += new jlib.controls.jgrid.OnGridRowPrerenderHandler(gGrid_GridRowPrerender);
        }

        void gGrid_GridBeforeFilter(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            string sFilter = gGrid.getFilter("cSent");
            if (sFilter != "") e.Filter.Add("sent", new condition("sent=" + (sFilter.Bln() ? "1" : "0")));
        }
        public void Page_Load(object sender, EventArgs e)
        {
            if (this.Request["action"] == "download_attachment" && this.QueryString("filename") != "")
            {
                Response.ContentType = jlib.net.HTTP.lookupMimeType(this.QueryString("filename"));
                Response.BinaryWrite(io.read_file_binary(System.Configuration.ConfigurationManager.AppSettings["email.attachment.path"] + "\\" + this.QueryString("filename")));
                Response.End();
            }

        }


        void gGrid_GridCustomData(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            if (e.Type == "reverse-transactions")
            {
                //DataTable oEmails = sqlbuilder.getDataTable(new ado_helper("sql.dsn.email"), "select * from d_email_log where id in (" + parse.removeXChars(e.Argument, "0123456789,") + ")");
                //for (int x = 0; x < oEmails.Rows.Count; x++) {
                //    List<KeyValuePair<string, byte[]>> oFiles = new List<KeyValuePair<string, byte[]>>();
                //    string[] sFiles = parse.split(oEmails.Rows[x]["attachments"], "\n");
                //    for (int y = 0; y < sFiles.Length; y++) {
                //        if (sFiles[y].Trim() != "") {
                //            byte[] bByte = io.read_file_binary(sFiles[y]);
                //            if (bByte.Length > 0) oFiles.Add(new KeyValuePair<string, byte[]>(parse.inner_substring(System.IO.Path.GetFileName(sFiles[y]), "-", null, null, null), bByte));
                //        }
                //    }
                //    Util.Email.sendEmail(convert.cStr(oEmails.Rows[x]["email_from"]), convert.cStr(oEmails.Rows[x]["email_to"]), "", "", convert.cStr(oEmails.Rows[x]["subject"]), convert.cStr(oEmails.Rows[x]["body"]), convert.cStr(oEmails.Rows[x]["body"]).IndexOf("<") > -1, oFiles, true, 0, "sql.dsn.cms");
                //}
                //e.Return = true;
                //e.ReturnValue = oEmails.Rows.Count + " successfully queued.";


                DataTable oEmails = sqlbuilder.getDataTable(new ado_helper("sql.dsn.cms"), "select * from d_payments where id in (" + parse.removeXChars(e.Argument, "0123456789,") + ")");
                string message = "Reversing:<br>";
                for (int x = 0; x < oEmails.Rows.Count; x++)
                {
                    message += "* " + oEmails.Rows[x]["id"] + " - " + SendTransaction(oEmails.Rows[x]["id"].Int(), true) + "<br>";
                }
                e.Return = true;
                e.ReturnValue = message;
            }
            else if (e.Type == "resend-transactions")
            {

                DataTable oEmails = sqlbuilder.getDataTable(new ado_helper("sql.dsn.cms"), "select * from d_payments where id in (" + parse.removeXChars(e.Argument, "0123456789,") + ")");
                string message = "Resending:<br>";
                for (int x = 0; x < oEmails.Rows.Count; x++)
                {
                    message += "* " + oEmails.Rows[x]["id"] + " - " + SendTransaction(oEmails.Rows[x]["id"].Int(), false) + "<br>";
                }
                e.Return = true;
                e.ReturnValue = message;
            }
        }


        void gGrid_GridViewsSave(jlib.controls.jgrid.jGrid oGrid)
        {
            Util.Permissions.setSetting("grid.view", "terminal-log", ActiveUser.ID, oGrid.Views.serialize());
        }

        void gGrid_GridViewsLoad(jlib.controls.jgrid.jGrid oGrid)
        {
            gGrid.Views.parse(Util.Permissions.getSetting("grid.view", "terminal-log", ActiveUser.ID, ""), true);
        }

        void gGrid_GridBeforeDatabind(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            gGrid.DataHelper = new ado_helper("sql.dsn.cms");
            gGrid.setDataSourcePaged("*", "d_payments", "");
        }
        private string SendTransaction(int id, bool reverse)
        {
            AttackData.Payment payment = AttackData.Payment.LoadById(id);//10383
            Dictionary<string, string> QueryString = jlib.net.HTTP.decodeRawQueryString(payment.QueryString, false);
            dynamic RequestJSON = jlib.functions.json.DynamicJson.Parse(System.Web.HttpUtility.UrlDecode(QueryString["payment"]));

            Util.Ecommerce.GoogleLogger logger = new Util.Ecommerce.GoogleLogger(payment.SiteCode, payment.GoogleCookieID);
            double rebateTotal = 0;
            foreach (dynamic line in RequestJSON.Lines)
            {
                if (convert.cDbl(convert.SafeGetProperty(line, "Price")) < 0)
                    rebateTotal += convert.cDbl(convert.SafeGetProperty(line, "Price"));
            }

            logger.LogTransaction(payment.ID, payment.OrderAmount.Dbl() * (reverse ? -1 : 1), payment.OrderCurrency);
            foreach (dynamic line in RequestJSON.Lines)
            {
                if (convert.cStr(convert.SafeGetProperty(line, "Desc")) != "" && convert.cDbl(convert.SafeGetProperty(line, "Price")) > 0)
                {
                    logger.LogTransactionItem(payment.ID, convert.cStr(convert.SafeGetProperty(line, "Desc")), "", "", convert.cDbl(convert.SafeGetProperty(line, "Price")) + rebateTotal, 1 * (reverse ? -1 : 1), payment.OrderCurrency);
                    rebateTotal = 0;
                }
            }

            return "OK - " + payment.OrderCurrency + " " + payment.OrderAmount + " " + (reverse ? "reversed" : "resent");
            //if (convert.HasProperty(RequestJSON, "Lines")) {


            //    GoogleRequest.GoogleRequest request = new GoogleRequest.GoogleRequest("UA-45293503-1");

            //    request.Culture = "nb-no";
            //    request.HostName = "http://www.prince2.no";
            //    //System.Web.HttpContext.Current.Request.Url.ToString();
            //    request.PageTitle = "Reverse charge";
            //    decimal total = 0;
            //    List<GoogleRequest.TransactionItem> items = new List<GoogleRequest.TransactionItem>();
            //    foreach (dynamic line in RequestJSON.Lines) {
            //        if (convert.cStr(convert.SafeGetProperty(line, "Desc")) != "") {
            //            GoogleRequest.TransactionItem item = new GoogleRequest.TransactionItem("", convert.cStr(convert.SafeGetProperty(line, "Desc")), convert.cDec(convert.SafeGetProperty(line, "Price")), (reverse ? -1 : 1) , "");
            //            items.Add(item);
            //        }
            //        total += convert.cDec(convert.SafeGetProperty(line, "Price"));
            //    }
            //    GoogleRequest.Transaction trans = new GoogleRequest.Transaction(payment.ID, "", "", "", "", 0, (reverse ? -total : total), 0);
            //    items.ForEach(x => { trans.AddTransactionItem(x); });

            //    //Now just make a request for this transaction object
            //    request.SendRequest(trans);
            //    return "OK - " + total + "kr " + (reverse ? "reversed" : "resent");
            //}
            //return "Not a reverseable transaction";
        }


    }



    //Borrowed from http://www.codeproject.com/Articles/493455/Server-side-Google-Analytics-Transactions
    namespace GoogleRequest
    {
        public class GoogleRequest
        {
            private const string BASE_URL = "http://www.google-analytics.com/__utm.gif?";

            private const string ANALYTICS_VERSION = "5.3.7";
            private const string LANGUAGE_ENCODING = "UTF-8";
            private const string BROWSER_JAVA_ENABLED = "0";

            //Required parameters but not necessary for us, so just post a default
            private const string SCREEN_RESOLUTION = "1680x1050";
            private const string SCREEN_COLOR_DEPTH = "32-bit";
            private const string FLASH_VERSION = "11.5%20r31";
            private const string REFERAL = "0";

            //Internal request counter. Max requests = 500 per session
            private int _RequestCount = 0;

            private Random _random;

            /// <summary>
            /// Initialize a new GoogleRequest
            /// </summary>
            /// <param name="accountCode">Your Google tracking code (e.g. UA-12345678-1)</param>
            public GoogleRequest(string accountCode)
            {
                _random = new Random();

                _RequestCount = 0;
                AccountCode = accountCode;
            }

            /// <summary>
            /// Initialize a new GoogleRequest with campaign and referer support
            /// </summary>
            /// <param name="accountCode">Your Google tracking code (e.g. UA-12345678-1)</param>
            /// <param name="request">the current HttpRequestBase of the webapplication</param>
            public GoogleRequest(string accountCode, HttpRequestBase request)
            {
                _random = new Random();

                _RequestCount = 0;
                _CurrentRequest = request;
                AccountCode = accountCode;
            }

            /// <summary>
            /// Send the request to the google servers!
            /// </summary>
            /// <param name="eventObject">A corresponding Transaction, Page or Event</param>
            public void SendRequest(IGoogleEvent eventObject)
            {
                string requestUrl = BASE_URL + CreateParameterString() + "&" + eventObject.CreateParameterString();

                FireRequest(requestUrl);

                //A transaction also has subrequests for the TransactionItems
                if (eventObject is Transaction)
                {
                    Transaction trans = eventObject as Transaction;

                    foreach (TransactionItem transItem in trans.Items)
                    {
                        FireRequest(BASE_URL + CreateParameterString() + "&" + transItem.CreateParameterString());
                    }
                }
            }

            private void FireRequest(string url)
            {
                if (_RequestCount < 500)
                {
                    _RequestCount++;

                    WebRequest GaRequest = WebRequest.Create(url);

                    GaRequest.BeginGetResponse(r =>
                    {
                        try
                        {
                            // we don't need the response so this is the end of the request
                            var reponse = GaRequest.EndGetResponse(r);
                        }
                        catch (Exception e)
                        {
                            var m = e.Message;
                            //eat the error 
                        }
                    }, null);
                }
            }

            private string CreateParameterString()
            {
                return string.Format("utmwv={0}&utms={1}&utmn={2}&utmhn={3}&utmsr{4}&utmvp={5}&utmsc={6}&utmul={7}&utmje={8}&utmfl={9}&utmhid={10}&utmr={11}&utmp={12}&utmac={13}&utmcc={14}",
                                      ANALYTICS_VERSION,
                                      _RequestCount,
                                      GenerateRandomId(),
                                      HostName,
                                      SCREEN_RESOLUTION,
                                      SCREEN_RESOLUTION,
                                      SCREEN_COLOR_DEPTH,
                                      Culture,
                                      BROWSER_JAVA_ENABLED,
                                      FLASH_VERSION,
                                      GenerateRandomId(),
                                      "-",
                                      PageTitle,
                                      AccountCode,
                                      GetUtmcCookieString());
            }

            /// <summary>
            /// (utmn) A random id for each gif to prevent caching
            /// </summary>
            /// <returns></returns>
            private string GenerateRandomId()
            {
                string randomId = "";

                for (int i = 0; i < 10; i++)
                {
                    randomId += _random.Next(9).ToString();
                }

                return randomId;
            }

            private int? _DomainHash;
            private int DomainHash
            {
                get
                {
                    if (!_DomainHash.HasValue)
                    {
                        if (HostName != null)
                        {
                            int a = 1;
                            int c = 0;
                            int h;
                            char chrCharacter;
                            int intCharacter;

                            a = 0;
                            for (h = HostName.Length - 1; h >= 0; h--)
                            {
                                chrCharacter = char.Parse(HostName.Substring(h, 1));
                                intCharacter = (int)chrCharacter;
                                a = (a << 6 & 268435455) + intCharacter + (intCharacter << 14);
                                c = a & 266338304;
                                a = c != 0 ? a ^ c >> 21 : a;
                            }

                            _DomainHash = a;
                        }
                        _DomainHash = 0;
                    }

                    return _DomainHash.Value;
                }
            }

            private string _UtmcCookieString = null;
            //The cookie collection string
            private string GetUtmcCookieString()
            {
                if (_UtmcCookieString == null)
                {
                    //create the unix timestamp
                    TimeSpan span = (DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
                    int timeStampCurrent = (int)span.TotalSeconds;

                    //fake the utma
                    string utma = String.Format("{0}.{1}.{2}.{3}.{4}.{5}",
                                                DomainHash,
                                                int.Parse(_random.Next(1000000000).ToString()),
                                                timeStampCurrent,
                                                timeStampCurrent,
                                                timeStampCurrent,
                                                "2");
                    string utmz;

                    //referral information
                    if (CurrentRequest != null && CurrentRequest.Cookies["__utmz"] != null)
                    {
                        utmz = CurrentRequest.Cookies["__utmz"].Value;
                    }
                    else
                    {
                        //fake it
                        utmz = String.Format("{0}.{1}.{2}.{3}.utmcsr={4}|utmccn={5}|utmcmd={6}",
                                                    DomainHash,
                                                    timeStampCurrent,
                                                    "1",
                                                    "1",
                                                    "(direct)",
                                                    "(direct)",
                                                    "(none)");
                    }

                    _UtmcCookieString = Uri.EscapeDataString(String.Format("__utma={0};+__utmz={1};", utma, utmz));
                }

                return (_UtmcCookieString);
            }

            #region get set

            private string _AccountCode;
            /// <summary>                           
            /// Your Google tracking code (e.g. UA-12345678-1)
            /// </summary>        
            public string AccountCode
            {
                get
                {
                    return _AccountCode;
                }
                set
                {
                    _AccountCode = value;
                }
            }

            private string _Culture;
            /// <summary>
            /// The language of the customer (e.g. nl-NL)
            /// </summary>
            public string Culture
            {
                get
                {
                    if (string.IsNullOrWhiteSpace(_Culture))
                    {
                        _Culture = "nl-NL";
                    }

                    return _Culture;
                }
                set
                {
                    _Culture = value;
                }
            }

            private Uri _HostName;
            /// <summary>
            /// The hostname of the website making the request (e.g. www.google.com)
            /// </summary>        
            public string HostName
            {
                get
                {
                    return _HostName.Host;
                }
                set
                {
                    _HostName = new Uri(value);
                }
            }

            private string _PageTitle;
            /// <summary>
            /// The title of the page making the request
            /// </summary>
            public string PageTitle
            {
                get
                {
                    if (string.IsNullOrWhiteSpace(_PageTitle))
                    {
                        _PageTitle = "ShoppingCart";
                    }

                    return _PageTitle;
                }
                set
                {
                    _PageTitle = value;
                }
            }

            private HttpRequestBase _CurrentRequest;
            /// <summary>
            /// The GA __utmz cookie (retrieved by Request.Cookies["__utmz"])
            /// </summary>
            public HttpRequestBase CurrentRequest
            {
                private get
                {
                    return _CurrentRequest;
                }
                set
                {
                    _CurrentRequest = value;
                }
            }

            #endregion
        }

        public class Transaction : IGoogleEvent
        {
            private readonly string _utmt = "tran";

            private int _orderId;           //(utmtid)

            private string _utmtci;        //Billing city
            private string _utmtco;        //Billing country
            private string _utmtrg;        //Billing region
            private string _utmtst;        //Store name / affiliation
            private string _utmtsp;        //Shipping costs
            private string _utmtto;        //Ordertotal
            private string _utmttx;        //Tax costs

            /// <summary>
            /// Create a new Ecommerce Transaction
            /// </summary>
            /// <param name="storeName">Name of the webstore or affiliation</param>
            /// <param name="schippingCosts">Total shipping costs for this order/transaction</param>
            /// <param name="orderTotal">Total costs of this order/transaction including tax and shipping costs</param>
            /// <param name="taxCosts">Total tax costs on this order/transaction</param>
            public Transaction(int orderId, string billingCity, string country, string region, string storeName, decimal shippingCosts, decimal orderTotal, decimal taxCosts)
            {
                _items = new List<TransactionItem>();

                _orderId = orderId;

                _utmtci = Uri.EscapeDataString(billingCity);
                _utmtco = Uri.EscapeDataString(country);
                _utmtrg = Uri.EscapeDataString(region);
                _utmtst = Uri.EscapeDataString(storeName);
                _utmtsp = shippingCosts.ToString("F").Replace(',', '.');
                _utmtto = orderTotal.ToString("F").Replace(',', '.');
                _utmttx = taxCosts.ToString("F").Replace(',', '.');
            }

            public void AddTransactionItem(TransactionItem item)
            {
                item.OrderId = _orderId;

                Items.Add(item);
            }

            public string CreateParameterString()
            {
                return string.Format("utmt={0}&utmtci={1}&utmtco={2}&utmtrg={3}&utmtid={4}&utmtst={5}&utmtsp={6}&utmtto={7}&utmttx={8}",
                                     _utmt,
                                     _utmtci,
                                     _utmtco,
                                     _utmtrg,
                                     _orderId.ToString(),
                                     _utmtst,
                                     _utmtsp,
                                     _utmtto,
                                     _utmttx);
            }

            private List<TransactionItem> _items;
            public List<TransactionItem> Items
            {
                get
                {
                    return _items;
                }
                private set
                {
                    _items = value;
                }
            }
        }

        public class TransactionItem
        {
            private readonly string _utmt = "item";

            private string _utmtid;     //OrderId
            private string _utmipc;     //Product code
            private string _utmipn;     //Product name
            private string _utmipr;     //Product price (unit price)
            private string _utmiqt;     //Quantity
            private string _utmiva;     //Product category

            /// <summary>
            /// Create a new TransactionItem
            /// </summary>
            /// <param name="productPrice">The unit price of the item</param>
            /// <param name="category">The product category or varition</param>
            public TransactionItem(string productCode, string productName, decimal productPrice, int quantity, string category)
            {
                _utmipc = Uri.EscapeDataString(productCode);
                _utmipn = Uri.EscapeDataString(productName);
                _utmipr = productPrice.ToString("F");
                _utmiqt = quantity.ToString();
                _utmiva = Uri.EscapeDataString(category);
            }

            public string CreateParameterString()
            {
                return string.Format("utmt={0}&utmtid={1}&utmipc={2}&utmipn={3}&utmipr={4}&utmiqt={5}&utmiva={6}",
                                     _utmt,
                                     _utmtid,
                                     _utmipc,
                                     _utmipn,
                                     _utmipr,
                                     _utmiqt,
                                     _utmiva);
            }

            #region get set

            /// <summary>
            /// The orderId will be set automatically when the item is being added to the Transaction
            /// </summary>
            public int OrderId
            {
                set
                {
                    _utmtid = value.ToString();
                }
            }

            #endregion
        }

        public class Helpers
        {
            /// <summary>
            /// Converts a DateTime to a UNIX timestamp.
            /// </summary>
            public static int ConvertToUnixTimestamp(DateTime value)
            {
                //create Timespan by subtracting the value provided from the Unix Epoch
                TimeSpan span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());

                //return the total seconds (which is a UNIX timestamp)
                return (int)span.TotalSeconds;
            }
        }
        public interface IGoogleEvent
        {
            string CreateParameterString();
        }
    }
}