﻿using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.db;
using jlib.functions;
using System.Security.Cryptography;
using System.Text;
namespace Phoenix.LearningPortal.Tools
{
    public partial class EzImage : jlib.components.webpage
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.Browser.Type.ToUpper().Contains("IE") || Request.Browser.MajorVersion > 8) Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Clear();
            if (Request["FileId"].Int() > 0 && parse.replaceAll(Request["secret"], " ", "") == BitConverter.ToString(MD5CryptoServiceProvider.Create().ComputeHash(ASCIIEncoding.ASCII.GetBytes(Request["FileId"] + "woeifnsodiusdofhsdoij"))).Replace("-", "").ToLower())
            {
                Util.Phoenix.session.RequestResult result = new Util.Phoenix.session().PhoenixRequest("GET", String.Format(Util.Phoenix.session.Queries.FileGet, Request["FileId"]), null, false, false, true);
                string fileName = "";
                if (result.WebResponse.Headers["Content-Disposition"] != null)
                    fileName = parse.inner_substring(result.WebResponse.Headers["Content-Disposition"] + ";", "filename=", null, ";", "");

                Response.ContentType = convert.cStr(jlib.net.HTTP.lookupMimeType(fileName), result.WebResponse.ContentType);
                //if (result.WebResponse.Headers["Content-Disposition"] != null) Response.AddHeader("Content-Disposition", result.WebResponse.Headers["Content-Disposition"]);
                Response.BinaryWrite(convert.cStreamToByteArray(result.WebResponse.GetResponseStream()));
            }
            Response.End();
        }
    }
}