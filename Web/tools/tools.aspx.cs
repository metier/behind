﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using jlib.functions;
using jlib.db;
using jlib.net;
using System.Data;
using System.Linq;
using System.Configuration;
using System.Net;
using System.IO;

namespace Phoenix.LearningPortal.Tools
{
    public partial class Tools : jlib.components.webpage
    {

        public new void Page_PreInit(object sender, EventArgs e)
        {
            if (this.QueryString("action") == "proxy")
            {
                Response.Clear();
                var acceptedUrls = new List<string> { "www.prosjektbloggen.no" };
                var url = this.QueryString("url");
                if (acceptedUrls.Any(x => url.Contains(x)))
                {

                    string html = System.Runtime.Caching.MemoryCache.Default.Get(url).Str();
                    string contentType = System.Runtime.Caching.MemoryCache.Default.Get(url+"-ContentType").Str();
                    if (html.IsNullOrEmpty() || contentType.IsNullOrEmpty())
                    {                        
                        var request = HttpWebRequest.Create(url);
                        var response = request.GetResponse();
                        if (response.ContentType.Str().IndexOf("html") > -1 || response.ContentType.Str().IndexOf("xml") > -1)
                        {
                            contentType = response.ContentType;
                            var stringReader = new StreamReader(response.GetResponseStream());
                            html = stringReader.ReadToEnd();
                            System.Runtime.Caching.MemoryCache.Default.Set(url, html, DateTimeOffset.Now.AddHours(2));
                            System.Runtime.Caching.MemoryCache.Default.Set(url + "-ContentType", contentType, DateTimeOffset.Now.AddHours(2));
                        }
                        else
                        {
                            //Non-html, don't cache
                            Response.ContentType = response.ContentType;
                            var stream = response.GetResponseStream();
                            stream.CopyTo(Response.OutputStream);                            
                            Response.End();
                        }
                    }
                    Response.ContentType = contentType;
                    Response.Write(html);
                    
                }
                Response.End();
            }

            if (Request.IsLocal) (Page.Master as Inc.Master.Default).DisableLoginRedirect = true;
            base.Page_PreInit(sender, e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!this.IsPostBack)
            {
                if (this.QueryString("mode") != "") dAction.setValue(this.QueryString("mode"));
                if (this.QueryString("xml") != "") tXmlUrl.setValue(this.QueryString("xml"));
                if (this.QueryString("xsl") != "") tXsl.setValue(this.QueryString("xsl"));
                if (convert.cBool(this.QueryString("pdf"))) cPDF.Checked = true;
                if (convert.cBool(this.QueryString("strip-email-header"))) cStripEmailHeaderFields.Checked = true;

                //if (this.QueryString("username") != "") tUsername.setValue(this.QueryString("username"));
                //if (this.QueryString("password") != "") tPassword.setValue(this.QueryString("password"));
            }
            rXsl.Visible = convert.cStr(dAction.getValue()) == "transform";
            if (convert.cStr(dAction.getValue()) == "generate_ra_email") lText1.Text = "RA #s (or IDs)";
            //if (convert.cStr(dAction.getValue()) == "execute_mysql_command") lText1.Text = "Query to execute";
            if (this.Request["action"] == "download_attachment" && this.QueryString("filename") != "")
            {
                Response.ContentType = jlib.net.HTTP.lookupMimeType(this.QueryString("filename"));
                Response.BinaryWrite(io.read_file_binary(ConfigurationManager.AppSettings["email.attachment.path"] + "\\" + this.QueryString("filename")));
                Response.End();
            }
            if (this.Request["action"] == "render_email" && this.QueryStringInt("email_id") > 0)
            {
                DataTable oDT = sqlbuilder.executeSelect(new ado_helper("sql.dsn.email"), "d_email_log", "id", this.QueryStringInt("email_id"));
                if (oDT.Rows.Count > 0)
                {
                    string sAttachments = "";
                    string[] s = parse.split(oDT.Rows[0]["attachments"], "\n");
                    for (int x = 0; x < s.Length; x++)
                    {
                        if (s[x].Trim() != "")
                        {
                            //"<img src=\"" + (System.IO.File.Exists(Server.MapPath("/images/icons/files/" + System.IO.Path.GetExtension(s[x]).Substring(1) + ".gif")) ? "/images/icons/files/" + System.IO.Path.GetExtension(s[x]).Substring(1) + ".gif" : "/images/icons/files/default.icon.gif") + "\">
                            sAttachments += (sAttachments == "" ? "" : " ") + "<a href=\"/admin/tools.aspx?action=download_attachment&filename=" + System.IO.Path.GetFileName(s[x]) + "\" target=\"blank\">" + parse.inner_substring(System.IO.Path.GetFileName(s[x]), "-", null, null, null) + "</a>";
                        }
                    }
                    string sEmail = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><title>RepairEngine&reg; Email: " + oDT.Rows[0]["subject"] + "</title><style type=\"text/css\">" + @"
body, td{font-family:arial,sans-serif;font-size:80%}a:link, a:active, a:visited{color:#189f31}
img{border:0}
body { margin: 0; padding: 0; }
pre.header table td{
font-size: 13px;
}
pre.header table td a{
font-size: 11px;
}
" + (this.QueryString("render") != "pdf" ? "" :
    @"pre.header table td.options{display:none;}
") +
    @"pre.header {	
	background-color: #e5e5e5;
	border-bottom: 1px solid #ccc;
	margin: 0 0 15px;
	padding: 10px 15px;
}
pre.body{
font-size: 13px;
margin: 0 0 15px;
padding: 10px 15px;
}
div.body{
margin: 0 0 15px;
padding: 10px 15px;
" + (this.QueryString("render") == "pdf" || this.QueryString("source") != "emaillog" ? "" : "width:99%;") + @"
}
</style>
</head><body>	
<pre class='header'><table style='width:95%'><tr><td style='white-space: nowrap' valign='top'>Email to: </td><td style='width:100%'><strong>" + parse.replaceAll(oDT.Rows[0]["email_to"], "; ", ";", ";", "<br>", "<br><br>", "<br>") + @"</strong></td><td style='white-space: nowrap' valign='top'>Date: </td><td style='white-space: nowrap' valign='top'><b>" + String.Format("{0:ddd M/d/yyyy hh:mm tt}", oDT.Rows[0]["posted_date"]) + @"</b></td></tr>
<tr><td style='white-space: nowrap' valign='top'>Email from: </td><td><strong>" + parse.splitValue(parse.stripLeadingCharacter(oDT.Rows[0]["email_from"], " ", ";"), ";", 0) + @"</strong></td></tr>
<tr><td>Subject: </td><td><strong>" + oDT.Rows[0]["subject"] + @"</strong></td><td align='right' class='options' colspan='2'><a href='javascript:void()' onclick='window.open(" + "\"" + Request.Url + "&render=pdf\");'>export to PDF</a></td></tr>"
                                    + (sAttachments == "" ? "" : "<tr><td>Attachments: </td><td>" + sAttachments + "</td></tr>")
                    + "</table></pre>" +
                   (convert.cBool(oDT.Rows[0]["is_html"]) ? "<div class='body'>" : "<pre class='body'>") + oDT.Rows[0]["body"]
                   + (convert.cBool(oDT.Rows[0]["is_html"]) ? "</div>" : "</pre>") + "</body></html>";

                    if (this.QueryString("render") == "pdf")
                    {


                        byte[] oPFD = Util.Cms.cPDF(sEmail, "", "");
                        Response.Clear();
                        Response.AddHeader("Content-Type", "application/pdf");

                        Response.AddHeader("Content-Disposition", "attachment; filename=Email.pdf; size=" + oPFD.Length.ToString());
                        Response.BinaryWrite(oPFD);

                    }
                    else
                    {
                        Response.Write(sEmail);
                    }
                }

                Response.End();
            }

            if (bResetCache.IsClicked)
            {
                Util.Data.reset();
                ado_helper.getTableDefintion(null, null);
                jlib.helpers.translation.reload();
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            string sLanguage = convert.cStr(dLanguage.getValue());
            if (this.QueryString("mode") != "" || this.IsPostBack)
            {

                if (convert.cStr(dAction.getValue()) == "transform" && convert.cStr(tXmlUrl.Text, tSettings.Text) != "")
                {
                    XmlDocument oDoc = new XmlDocument();
                    if (!tXmlUrl.Text.StartsWith("http")) tXmlUrl.Text = ConfigurationManager.AppSettings["site.url"] + tXmlUrl.Text;
                    //oDoc.LoadXml(HTTP.HtmlGet(tXmlUrl.Text + (tXmlUrl.Text.IndexOf("username") == -1 ? "&username=" + tUsername.Text + "&password=" + tPassword.Text : "")));
                    HTTP oHTTP = new HTTP();
                    oHTTP.Timeout = 999;
                    string sXml = "";
                    string sXslUrl = convert.cStr(tXsl.Text.Trim(), dXsl.getValue());
                    if (tSettings.Text.StartsWith("<"))
                        sXml = tSettings.Text;
                    else
                        sXml = oHTTP.Get((tXmlUrl.Text.StartsWith("http") ? "" : Request.Url.Scheme + "://" + Request.Url.Host) + tXmlUrl.Text);
                    jlib.helpers.translation.reload();
                    try
                    {
                        if (sXslUrl.EndsWith(".cshtml"))
                        {
                            Response.Write(Util.Email.RenderRazorTemplate(new { }, System.IO.Path.GetFileNameWithoutExtension(sXslUrl), sLanguage , true));
                        }
                        else
                        {
                            oDoc.LoadXml(sXml);
                            if (sLanguage == "") sLanguage = xml.getXmlNodeValue(oDoc, "queries/query/user/LANGUAGE");

                            List<KeyValuePair<string, byte[]>> oFiles = new List<KeyValuePair<string, byte[]>>();
                            xml.setXmlAttributeValue(oDoc.DocumentElement, "language", sLanguage);
                            if (!tSettings.Text.StartsWith("<") && tSettings.Text != "")
                            {
                                string[] sArr = parse.split(parse.replaceAll(tSettings.Text, "\r", ""), "\n");
                                for (int x = 0; x < sArr.Length; x++)
                                {
                                    XmlNode xmlNode = oDoc.DocumentElement;
                                    xml.setXmlAttributeValue(xmlNode, sArr[x].Substring(0, sArr[x].IndexOf("=")), parse.replaceAll(sArr[x].Substring(sArr[x].IndexOf("\"")), "\"", ""));
                                }
                            }
                            if (bTestAll.IsClicked)
                            {
                                for (int x = 0; x < dXsl.Items.Count; x++)
                                {
                                    string sData = jlib.functions.xml.xslTransform(oDoc, dXsl.Items[x].Value, null, false, true, cStripEmailHeaderFields.Checked);

                                    Response.Write(dXsl.Items[x].Value + "<br/><div style='border:3px solid black'>" + (dXsl.Items[x].Value.IndexOf(".txt.") > -1 ? "<pre>" : "") + sData + (dXsl.Items[x].Value.IndexOf(".txt.") > -1 ? "</pre>" : "") + "</div><hr>");
                                }
                            }
                            else
                            {
                                if (sXslUrl.IndexOf(".txt.") > -1) Response.ContentType = "text/plain";
                                string sData = jlib.functions.xml.xslTransform(oDoc, sXslUrl, sLanguage, false, true, cStripEmailHeaderFields.Checked);

                                if (cPDF.Checked)
                                {
                                    byte[] bPDF = Util.Cms.cPDF(sData, "", "", false);
                                    Response.AddHeader("Content-Type", "application/pdf");
                                    Response.AddHeader("Content-Disposition", "attachment; filename=Email.pdf; size=" + bPDF.Length.ToString());
                                    Response.BinaryWrite(bPDF);

                                    Response.End();
                                }
                                else
                                {
                                    Response.Write(sData);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception("XML Error: " + sXml, e);
                    }
                    Response.End();
                }

            }

            base.Render(writer);
        }

    }
}