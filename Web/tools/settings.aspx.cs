﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using jlib.functions;
using jlib.db;
using jlib.net;
using System.Data;
namespace Phoenix.LearningPortal.Tools
{
    public partial class Settings : jlib.components.webpage
    {

        bool bCustomerNumbersMode = false;
        private Util.Classes.user m_oActiveUser = null;
        public Util.Classes.user ActiveUser
        {
            get
            {
                if (m_oActiveUser == null) m_oActiveUser = (Page.Master as Inc.Master.Default).ActiveUser;
                return m_oActiveUser;
            }
        }
        public void Page_Init(object sender, EventArgs e)
        {
            gGrid.GridRowClientDel += new jlib.controls.jgrid.OnGridRowClientDelHandler(gGrid_GridRowClientDel);
            gGrid.GridBeforeDatabind += new jlib.controls.jgrid.OnGridBeforeDatabindHandler(gGrid_GridBeforeDatabind);
            gGrid.GridRowClientUpdate += new jlib.controls.jgrid.OnGridRowClientUpdateHandler(gGrid_GridRowClientUpdate);
            gGrid.GridRowPrerender += new jlib.controls.jgrid.OnGridRowPrerenderHandler(gGrid_GridRowPrerender);
            gGrid.GridViewsLoad += new jlib.controls.jgrid.OnGridViewsLoadHandler(gGrid_GridViewsLoad);
            gGrid.GridViewsSave += new jlib.controls.jgrid.OnGridViewsSaveHandler(gGrid_GridViewsSave);
            gGrid.DataHelper = new ado_helper("sql.dsn.cms");
            bCustomerNumbersMode = this.QueryString("mode") == "customer_numbers" || !ActiveUser.IsBehindSuperAdmin;
            if (bCustomerNumbersMode)
            {
                gGrid.DisplayAdd = false;
                gGrid.removeColumn(cKey);
                gGrid.removeColumn(cUser);
            }
        }
        void gGrid_GridViewsSave(jlib.controls.jgrid.jGrid oGrid)
        {
            Util.Permissions.setSetting("grid.view", "admin-settings", ActiveUser.ID, oGrid.Views.serialize(false));
        }

        void gGrid_GridViewsLoad(jlib.controls.jgrid.jGrid oGrid)
        {
            gGrid.Views.parse(convert.cStr(Util.Permissions.getSetting("grid.view", "admin-settings", ActiveUser.ID, "")), true);
        }

        void gGrid_GridRowPrerender(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            //if (convert.cInt(e.DBRow["portal_id"]) == 0) e.setColValue("cPortal", ""); else if (convert.cInt(e.DBRow["portal_id"]) > 0 && convert.cStr(e.DBRow["portal_name"]) == "") e.setColValue("cPortal", e.DBRow["portal_id"] + "\n*** ID " + e.DBRow["portal_id"] + " not found ***");
            //if (convert.cInt(e.DBRow["partner_id"]) == 0) e.setColValue("cPartner", ""); else if (convert.cInt(e.DBRow["partner_id"]) > 0 && convert.cStr(e.DBRow["partner_name"]) == "") e.setColValue("cPartner", e.DBRow["partner_id"] + "\n*** ID " + e.DBRow["partner_id"] + " not found ***");
            //if (convert.cInt(e.DBRow["company_id"]) == 0) e.setColValue("cCompany", ""); else if (convert.cInt(e.DBRow["company_id"]) > 0 && convert.cStr(e.DBRow["company_name"]) == "") e.setColValue("cCompany", e.DBRow["company_id"] + "\n*** ID " + e.DBRow["company_id"] + " not found ***");
            //if (convert.cInt(e.DBRow["user_id"]) == 0) e.setColValue("cUser", ""); else if (convert.cInt(e.DBRow["user_id"]) > 0 && convert.cStr(e.DBRow["user_name"]) == "") e.setColValue("cUser", e.DBRow["user_id"] + "\n*** ID " + e.DBRow["user_id"] + " not found ***");
        }

        void gGrid_GridRowClientUpdate(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            try
            {
                DataTable oDT = sqlbuilder.executeSelect(gGrid.DataHelper, "d_user_settings", "id", e.PK);
                if (oDT.Rows.Count == 0) oDT.Rows.Add(oDT.NewRow());
                e.setRowValues(oDT.Rows[0]);
                oDT.Rows[0]["value"] = parse.replaceAll(oDT.Rows[0]["value"], "\r\n", "\n", "\r", "\n");
                ado_helper.update(oDT);
            }
            catch (Exception)
            {
                e.Return = false;
                e.ReturnValue = "Row could not be saved because of a duplicate key-subkey-user-company value.";
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "permission_data", ";\nvar JComboKeys=" + jlib.functions.convert.cJSON(sqlbuilder.getDataTable(gGrid.DataHelper, "select distinct key_name as a,key_name as b from d_user_settings where d_user_settings.key_name<>'admin' order by key_name"), false)
              , true);
        }

        void gGrid_GridBeforeDatabind(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            gGrid.setDataSourcePaged("*", "d_user_settings", "d_user_settings.key_name<>'admin'" + (bCustomerNumbersMode ? " and d_user_settings.key_name='agresso.customers'" : ""));
        }

        void gGrid_GridRowClientDel(object sender, jlib.controls.jgrid.GridCellEventArgs e)
        {
            sqlbuilder.executeDelete(gGrid.DataHelper, "d_user_settings", "id", e.PK);
        }
    }
}