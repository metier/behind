﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using jlib.functions;
using jlib.net;
namespace Phoenix.LearningPortal.Tools
{
    public partial class Translator : jlib.components.webpage
    {
        XmlDocument m_oTranslations;
        protected void Page_Init(object sender, EventArgs e)
        {
            m_oTranslations = new XmlDocument();
            m_oTranslations.LoadXml(io.read_file(Server.MapPath(Request.ApplicationPath + "/inc/pages/translation.xml")));
            XmlNodeList xmlNodes = m_oTranslations.SelectNodes("pages/translations/template");
            dTranslatorTemplate.Items.Add(new ListItem("Please select", ""));
            XmlNode xmlCurrentTemplate = null;
            for (int x = 0; x < xmlNodes.Count; x++) dTranslatorTemplate.Items.Add(xml.getXmlAttributeValue(xmlNodes[x], "name"));
            if (dTranslatorTemplate.getValue().Str() != "")
            {
                xmlCurrentTemplate = m_oTranslations.SelectSingleNode("pages/translations/template[@name='" + dTranslatorTemplate.getValue() + "']");
                xmlNodes = xmlCurrentTemplate.SelectNodes("languages/language");
                dTranslatorLanguage.Items.Add(new ListItem("Please select", ""));
                for (int x = 0; x < xmlNodes.Count; x++) dTranslatorLanguage.Items.Add(new ListItem(xml.getXmlNodeValue(xmlNodes[x]), xml.getXmlAttributeValue(xmlNodes[x], "code")));
                pLanguage.Visible = true;
            }
            if (dTranslatorTemplate.getValue().Str() != "" && dTranslatorLanguage.getValue().Str() != "")
            {
                lTranslateAll.Visible = xml.getXmlAttributeValue(xmlCurrentTemplate, "default-language") != dTranslatorLanguage.getValue().Str();
                xmlNodes = xmlCurrentTemplate.SelectNodes("pages/page");
                for (int x = 0; x < xmlNodes.Count; x++)
                {
                    XmlNode xmlPage = m_oTranslations.SelectSingleNode("pages/page[@lang='" + dTranslatorLanguage.getValue() + "'][@url='" + xml.getXmlAttributeValue(xmlNodes[x], "url") + "']");
                    lTranslations.Text += "<tr><td colspan='4'><h2>" + xml.getXmlNodeValue(xmlNodes[x]) + (xml.getXmlAttributeValue(xmlPage, "redirect-language") == "" ? "" : " <span style='color:red'>[Not in use! Redirects to language: " + xml.getXmlAttributeValue(xmlPage, "redirect-language") + "]</span>") + "</h2></td></tr>";
                    XmlNodeList xmlTexts = m_oTranslations.SelectNodes("pages/page[@lang='" + xml.getXmlAttributeValue(xmlCurrentTemplate, "default-language") + "'][@url='" + xml.getXmlAttributeValue(xmlNodes[x], "url") + "']/item");
                    lTranslations.Text += "<tr><th>Key</th>" + (xml.getXmlAttributeValue(xmlCurrentTemplate, "default-language") == dTranslatorLanguage.getValue().Str() ? "" : "<th>" + xml.getXmlNodeValue(xmlCurrentTemplate, "languages/language[@code='" + xml.getXmlAttributeValue(xmlCurrentTemplate, "default-language") + "']") + "</th>") + "<th>" + dTranslatorLanguage.SelectedText + "</th><th></th>";
                    for (int y = 0; y < xmlTexts.Count; y++)
                    {
                        string sXmlPath = "pages/page[@lang='" + dTranslatorLanguage.getValue() + "'][@url='" + xml.getXmlAttributeValue(xmlNodes[x], "url") + "']/item[@name='" + xml.getXmlAttributeValue(xmlTexts[y], "name") + "']";
                        if (bSave.IsClicked)
                        {
                            XmlNode xmlTarget = m_oTranslations.SelectSingleNode(sXmlPath);
                            if (xmlTarget == null)
                            {
                                xmlTarget = m_oTranslations.SelectSingleNode("pages/page[@lang='" + dTranslatorLanguage.getValue() + "'][@url='" + xml.getXmlAttributeValue(xmlNodes[x], "url") + "']");
                                if (xmlTarget == null) xmlTarget = xml.setXmlAttributeValue(xml.addXmlElement(m_oTranslations.SelectSingleNode("pages"), "page"), "lang", dTranslatorLanguage.getValue(), "url", xml.getXmlAttributeValue(xmlNodes[x], "url"));
                                xmlTarget = xml.setXmlAttributeValue(xml.addXmlElement(xmlTarget, "item"), "name", xml.getXmlAttributeValue(xmlTexts[y], "name"));
                            }
                            xmlTarget.InnerText = Request[sXmlPath];
                        }
                        string sValue = convert.cStr(Request[sXmlPath], xml.getXmlNodeValue(m_oTranslations, sXmlPath), xml.getXmlNodeValue(m_oTranslations, "pages/page[@url='" + xml.getXmlAttributeValue(xmlNodes[x], "url") + "'][@lang='" + dTranslatorLanguage.getValue() + "'][@name='" + xml.getXmlAttributeValue(xmlTexts[y], "name") + "']"));
                        if (Request["suggest-" + sXmlPath].Str() != "" || bSuggestAll.IsClicked || (bSuggestEmpty.IsClicked && sValue == ""))
                        {
                            if (xml.getXmlNodeValue(xmlTexts[y]).IndexOf("Lorem ipsum", StringComparison.CurrentCultureIgnoreCase) > -1)
                            {
                                sValue = xml.getXmlNodeValue(xmlTexts[y]);
                            }
                            else
                            {
                                HTTP oHTTP = new HTTP();
                                oHTTP.CacheMaxAge = DateTime.Now.AddYears(-20);
                                oHTTP.CacheDSN = "sql.dsn.url-cache";
                                string sData = oHTTP.Get("https://www.googleapis.com/language/translate/v2?key=AIzaSyAkxL7k5EW8WtkSjIHdrklwjNmUQDn1-Sk=&q=" + System.Web.HttpUtility.UrlEncode(xml.getXmlNodeValue(xmlTexts[y])) + "&source=" + xml.getXmlAttributeValue(xmlCurrentTemplate, "default-language") + "&target=" + dTranslatorLanguage.getValue());
                                if (!sData.IsNullOrEmpty()) sValue = jlib.functions.json.JsonMapper.ToObject(sData)["data"]["translations"][0][0].Str();
                            }
                        }
                        lTranslations.Text += "<tr><td>" + xml.getXmlAttributeValue(xmlTexts[y], "name") + "</td>" + (xml.getXmlAttributeValue(xmlCurrentTemplate, "default-language") == dTranslatorLanguage.getValue().Str() ? "" : "<td><textarea disabled='disabled'>" + xml.getXmlNodeValue(xmlTexts[y]) + "</textarea></td>") + "<td><textarea name='" + System.Web.HttpUtility.HtmlEncode(sXmlPath) + "'>" + sValue + "</textarea></td>" + (xml.getXmlAttributeValue(xmlCurrentTemplate, "default-language") == dTranslatorLanguage.getValue().Str() ? "" : "<td><input type='submit' value='Suggest' name='suggest-" + System.Web.HttpUtility.HtmlEncode(sXmlPath) + "'></td>") + "</tr>";
                    }
                }
                pSaveContainer.Visible = true;
                if (bSave.IsClicked)
                {
                    io.write_file(Server.MapPath(Request.ApplicationPath + "/inc/pages/translation.xml"), System.Xml.Linq.XElement.Parse(m_oTranslations.OuterXml).ToString());
                    jlib.helpers.translation.reload();
                }
            }
        }
    }
}