﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using jlib.functions;
using jlib.net;
using jlib.db;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Net;
using System.Linq;
namespace Phoenix.LearningPortal.Tools
{
    public partial class Proxy : jlib.components.webpage
    {

        public new void Page_PreInit(object sender, EventArgs e)
        {
            (Page.Master as Inc.Master.Default).DisableLoginRedirect = true;
            (Page.Master as Inc.Master.Default).DisableWrapper = true;
            base.Page_PreInit(sender, e);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            (Page.Master as Inc.Master.Default).InitiatePageRequestLogger(true);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Request["url"]);
            request.Method = Request.HttpMethod;
            foreach (var key in Request.Headers.AllKeys)
            {
                try
                {
                    request.Headers[key] = Request.Headers[key];
                }
                catch (Exception) { }
            }
            request.ContentLength = Request.ContentLength;
            request.ContentType = Request.ContentType;
            byte[] byteArray = convert.cStreamToByteArray(Request.InputStream);
            if (request.Method == "POST" || request.Method == "PUT")
            {
                request.ContentLength = byteArray.Length;
                request.GetRequestStream().Write(byteArray, 0, byteArray.Length);
            }

            WebResponse response = request.GetResponse();
            foreach (var key in response.Headers.AllKeys)
                Response.Headers[key] = response.Headers[key];

            byteArray = convert.cStreamToByteArray(response.GetResponseStream());
            Response.BinaryWrite(byteArray);

            Response.End();
        }
    }

}