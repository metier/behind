using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Xml;
using System.Text;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using jlib.components.button;
using jlib.db;
using jlib.net;
using jlib.functions;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Tools
{
    public partial class Sendmail : jlib.components.webpage
    {

        public void Page_Init(object sender, EventArgs e)
        {
            if (convert.cStr(Request["action"]) == "eval")
            {
                Util.Classes.user activeUser = Util.Classes.user.getUser(new Util.Phoenix.session().GetUserId());
                if (activeUser != null)
                {
                    AttackData.Evaluation Eval = AttackData.Evaluations.GetByField(Username: activeUser.Username, CourseCode: Request["course_code"], LessonNumber: (short)convert.cInt(Request["lesson_number"])).Execute().FirstOrDefault();
                    if (Eval == null)
                    {
                        Eval = new AttackData.Evaluation()
                        {
                            Username = activeUser.Username,
                            CourseCode = Request["course_code"],
                            LessonNumber = (short)convert.cInt(Request["lesson_number"]),
                            //todo
                            UserID = convert.cInt(activeUser.ID)
                        };
                    }
                    Eval.Comment = Request["comment"];
                    Eval.Score = convert.cInt(Request["score"]);
                    Eval.PostedDate = DateTime.Now;
                    Eval.Save();
                    if (Request["CourseId"].Int() > 0)
                    {
                        var Course = AttackData.Folder.LoadByPk(Request["CourseId"].Int());
                        var Lesson = AttackData.Folder.LoadByPk(Request["LessonId"].Int());
                        if (Course != null && Course.getSetting("PlayerSettings").IsNotNullOrEmpty())
                        {
                            var Settings = jlib.functions.json.DynamicJson.Parse(Course.getSetting("PlayerSettings"));
                            string EvalEmailTo = convert.cStr(convert.SafeGetProperty(Settings, "Player", "EvalEmailTo"));
                            if (EvalEmailTo.IsNotNullOrEmpty())
                            {
                                Util.Email.sendEmail("noreply@mymetier.net", EvalEmailTo, "", "", "myMetier Lesson Eval: " + Course.Name + " [" + Eval.CourseCode + "] / Lesson: " + (Lesson == null ? Eval.LessonNumber.Str() : Lesson.Name),
                                    "Username: " + Eval.Username + "<br />"
                                    + "UserId: " + Eval.UserID + "<br />"
                                    + "EvalId: " + Eval.ID + "<br />"
                                    + "Rating: " + Eval.Score + "<br />"
                                    + "Date: " + Eval.PostedDate + "<br />"
                                    + "Comment: " + Eval.Comment + "<br />"
                                    , true, null, true);
                            }
                        }
                    }
                }
            }
            else
            {
                if (convert.cStr(Request["to"]).IndexOf("@bids.no") > -1 || convert.cStr(Request["to"]).IndexOf("@rpsgroup.com") > -1 || convert.cStr(Request["to"]).IndexOf("@metier") > -1 || convert.cStr(Request["to"]).IndexOf("@afiniti.co.uk") > -1)
                {
                    bool bHTML = convert.cBool(Request["html"]);
                    string sBody = convert.cStr(Request["body"]) + (bHTML ? "<br /><br /><hr /><br /><br />" : "\n\n-------\n\n");

                    for (int x = 0; x < Request.ServerVariables.Count; x++)
                        sBody += Request.ServerVariables.AllKeys[x] + ": " + Request.ServerVariables[x] + (bHTML ? "<br />" : "\n");

                    if (Request["from"] == "player2-error.noreply@metier.no")
                    {
                        if(System.Configuration.ConfigurationManager.AppSettings["player2.error.log"].IsNotNullOrEmpty())
                            io.write_file(System.Configuration.ConfigurationManager.AppSettings["player2.error.log"], DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString() + ": " + Request["subject"] + "\n" + parse.replaceAll(sBody, "<br />","\n") + "\n\n ---------- \n\n", true);
                    }
                    else
                    {
                        Util.Email.sendEmail(Request["from"], Request["to"], "", "", Request["subject"], sBody, bHTML, null, true);
                    }
                    Response.Write("OK");
                }
                else
                {
                    Response.Write("To-address is not a valid domain.");
                }
            }
            Response.End();
        }
    }
}