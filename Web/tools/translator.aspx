﻿<%@ Page Title="Metier Translator" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Tools.Translator" Codebehind="translator.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
<style>
form textarea
{
    height:75px;
    width:400px;    
    border:0;
    color:Black;
}
table td
{ 
    vertical-align:middle;
    background-color:White;
    border:1px solid silver;
}
th
{
    font-weight:bold;
    font-size:14px;
}
</style>
<h2 class="page-title"><img src="../inc/images/icons/tools_32.png" alt="Translator" />Translator</h2>

<table style="width:950px;border:3px solid silver" border="1"><tr><td><b>Select Template: </b></td><td colspan="3"><common:dropdown runat="server" ID="dTranslatorTemplate" AutoPostBack="true" /></td></tr>
<asp:PlaceHolder runat="server" ID="pLanguage" Visible="false">
<tr><td><b>Select Language: </b></td><td colspan="3"><div style="float:left"><common:dropdown runat="server" ID="dTranslatorLanguage" AutoPostBack="true" /></div><common:label runat="server" ID="lTranslateAll" Tag="div" Visible="false" style="float:right"><common:button.button runat="server" buttonmode="submitinput" text="Suggest Empty" id="bSuggestEmpty" style="margin-top:0" onclientclick="return window.confirm('Are you sure you want suggestions for all empty values?');" />&nbsp;<common:button.button onclientclick="return window.confirm('Are you sure you want suggestions for all? This will overwrite existing values.');" runat="server" buttonmode="submitinput" text="Suggest All" id="bSuggestAll" style="margin-top:0" /></common:label></td></tr>
<common:label runat="server" ID="lTranslations" />
<asp:PlaceHolder runat="server" Visible="false" ID="pSaveContainer">
<tr><td colspan="3"><common:button.button runat="server" buttonmode="submitinput" text="Save" id="bSave" /></td></tr>
</asp:PlaceHolder>
</asp:PlaceHolder>
</table>
</asp:Content>