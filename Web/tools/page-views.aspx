﻿<%@ Page Title="Behind myMetier :: Page Views" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Tools.PageViews" Codebehind="page-views.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
<h2 class="page-title"><img src="../inc/images/icons/tools_32.png" alt="Email Log" />Page View Log</h2>

<style>
hr{    
    display:block;
}
</style>

<div id="cStatus" style="visibility:hidden"><img src='../inc/images/icons/accept.gif'/>&nbsp;Refresh complete</div>
<br style="clear: both;" />

<table cellpadding="0" cellspacing="0" border="0" class="listing assets" style="margin-bottom:0;">
	<colgroup></colgroup>
	<colgroup span="3" style="font-weight: bold;"></colgroup>
	<thead>
		<tr class="actions">
			<td colspan="11">
				<table cellpadding="0" cellspacing="0">
					<thead>
						<tr>							
							<td><span id="hGridView"></span></td>
							<td class="paging">
	                            Page
	                            <img src="../inc/images/icons/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow grid_prev_page" />
	                            <input type="text" value="1" class="text page page_number" />
	                            <a href="javascript:void(0);" title="Next page" ><img src="../inc/images/icons/pager_arrow_right.gif" alt="Go to Next page" class="arrow grid_next_page" /></a>

	                            of <span class="page_status">3 pages</span>            <span class="separator">|</span>
	                            View            <select name="limit" class="page_size">

	                                <option value="20" selected="selected">20</option>
	                                <option value="30">30</option>
	                                <option value="50">50</option>
	                                <option value="100">100</option>
	                                <option value="200">200</option>
	                            </select>

	                            per page
			                </td>
						</tr>
					</thead>
				</table>
			</td>
		</tr>
	</thead>
	</table>
<jgrid:jGridData DisableInitialFiltering="true" runat="server" id="gGrid" LoadViewContainer="hGridView" BindDropdownByID="true" style="width:956px;" SortOrder="desc" JSName="oGrid" AutoFilter="true" PageSize="50" SortCol="0" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets' style='border-top:0;margin-bottom:0'" DeleteConfirmation="Are you sure you wish to delete this row?" DisplayFilter="true">
    <Cols>
        <jgrid:col id="cID" runat="server" HText="ID" FPartialMatch="true" ColType="Hide" PrimaryKey="true" cinputatt="class='text'" FormatString="#ID#" SortField="ID" />                
        <jgrid:col id="cDate" runat="server" HText="Date" FPartialMatch="true" FormatString="{0:yyyy}-{0:MM}-{0:dd} {0:HH}:{0:mm}:{0:ss}" FormatStringArguments="PostedDate" FInputAtt="class='text daterangepicker'" ColType="Text" SortField="PostedDate" />   
        <jgrid:col id="cUser" runat="server" HText="User" FPartialMatch="true" FormatString="UserSortKey" FInputAtt="class='text'" ColType="Text" SortField="UserSortKey" FField="UserSortKey" />   
        <jgrid:col id="cUrl" runat="server" HText="Url" FPartialMatch="true" FormatString="Url" FInputAtt="class='text'" ColType="Text" SortField="Url" />           
        <jgrid:col id="cPageTitle" runat="server" HText="Page Title" FPartialMatch="true" FormatString="PageTitle" FInputAtt="class='text'" ColType="Text" SortField="PageTitle" />           
        <jgrid:col id="cIP" runat="server" HText="IP" FPartialMatch="true" FormatString="Ip" FInputAtt="class='text'" ColType="Text" SortField="Ip" />   
        <jgrid:col id="cUserAgent" runat="server" HText="User Agent" FPartialMatch="true" FormatString="UserAgent" FInputAtt="class='text'" ColType="Text" SortField="UserAgent" />           
        <jgrid:col id="cLoadTime" runat="server" HText="Load Time" FPartialMatch="true" FormatString="LoadTime" FInputAtt="class='text'" ColType="Text" SortField="LoadTime" />           
        <jgrid:col id="cPostData" runat="server" HText="Post Data" FPartialMatch="true" FormatString="PostData" FInputAtt="class='text'" ColType="Hide" FColType="Text" GroupTypes="Text" SortField="#" FField="PostData" />                                   
    </Cols>                
</jgrid:jGridData>
<jgrid:col id="cOptions" HText="" ColType="Options" />                           


<table cellpadding="0" cellspacing="0" border="0" class="listing assets" style="border-top:0px;">
	<colgroup></colgroup>
	<colgroup span="3" style="font-weight: bold;"></colgroup>
	<thead>
		<tr class="actions">
			<td colspan="11">
				<table cellpadding="0" cellspacing="0">
					<thead>
						<tr>							
							<td class="paging">
	                            Page
	                            <img src="../inc/images/icons/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow grid_prev_page" />
	                            <input type="text" value="1" class="text page page_number" />
	                            <a href="javascript:void(0);" title="Next page" ><img src="../inc/images/icons/pager_arrow_right.gif" alt="Go to Next page" class="arrow grid_next_page" /></a>

	                            of <span class="page_status">3 pages</span>            <span class="separator">|</span>
	                            View            <select name="limit" class="page_size">

	                                <option value="20" selected="selected">20</option>
	                                <option value="30">30</option>
	                                <option value="50">50</option>
	                                <option value="100">100</option>
	                                <option value="200">200</option>
	                            </select>

	                            per page
			                </td>
						</tr>
					</thead>
				</table>
			</td>
		</tr>
	</thead>
	</table>	
<script>
    fnGridAttach(oGrid);
   
</script>

</asp:Content>