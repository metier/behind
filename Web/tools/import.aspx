﻿<%@ Page Title="Metier Oracle Backup and Restore" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Tools.Import" Codebehind="import.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
<style>
.raGroup {
background-color: #f0f0f0; 
border: 1px solid #ccc;
padding: 10px 15px 15px;
margin-bottom: 20px;
}
.raGroup h3 {
	font-size: 14px;
	font-weight: bold;
}
.raGroup label {
font-weight: bold;
margin-top: 15px;
display: block;
font-size: 11px;
}
.raGroup input {
background-color: #fff;
background-image: none;
}
.raGroup span.checkbox input{
background-color: transparent;
}
div.raGroup textarea {
width: 98%;
height: 100px;
}
.raGroup label.inline
 {
	display: inline;
	font-weight: normal;
	font-size: 11px;
}
.jimport-instructions
{
    border:3px solid silver;
    padding:10px;
}
.jimport-instructions ol
{
    margin-top:10px;
    padding-left:30px;
    list-style-type:decimal;
}
.jimport-instructions li
{
    line-height:1.6;
}
span.form_error
{
    display:block;
    line-height:1.4;
}
table#jimport-controls
{
    width:auto;    
    margin-top:20px;
 /*   border:3px solid silver;*/
    
}
table#jimport-controls tr
{
    background-color:transparent;    
}
table#jimport-controls td
{    
    vertical-align:middle; 
}
</style>
<h2 class="page-title"><img src="../inc/images/icons/tools_32.png" alt="Import Data" />Data Importer</h2>

<common:importer runat="server" id="oImporter" xmlfile="~/tools/templates.xml" HeaderRow="true" DSN="sql.dsn.cms" />

<script>    
    <common:label runat="server" id="lScripts" />
</script>
</asp:Content>