﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Linq;
using System.Linq.Expressions;
using jlib.functions;
using jlib.db;
using jlib.net;
using System.Data;
using System.Configuration;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Tools
{
    public partial class Terminal : jlib.components.webpage
    {


        //Register
        //=========
        //Merchant ID
        //Token
        //Order Number
        //Currency Code
        //Amount
        //Web Service Platform
        //Order Description
        //Language
        //Redirect URL success
        //Redirect URL failure?
        //Terminal SinglePage?
        //Terminal Layout?

        //PRINCE2.no (NOK): 503269
        //Metieroga.com (USD): 503264
        //metier.no (NOK): 501729

        //https://epayment.bbs.no/Netaxept/Register.aspx?merchantId=[MERCHANTID]&token=[TOKEN]&orderNumber=12345&amount=1000&CurrencyCode=NOK&redirectUrl=http://localhost/Test/

        public enum PaymentMethods
        {
            Invoice = 1,
            CreditCard = 2
        }
        public new void Page_PreInit(object sender, EventArgs e)
        {
            (Page.Master as Inc.Master.Default).DisableLoginRedirect = true;
            (Page.Master as Inc.Master.Default).DisableWrapper = true;
            base.Page_PreInit(sender, e);
        }

        private void Page_Error(object sender, EventArgs e)
        {
            if (Request.Url.Query.Contains("motti"))
            {
                Exception ex = Server.GetLastError();
                jlib.net.JWebException.Handler handler = new JWebException.Handler();

                Response.Write("<h1>Error</h1><pre>" + handler.ExceptionToString(ex) + "</pre>");
                Server.ClearError();
                Response.End();
            }
        }

        private void ProcessResponseFromPaymentProvider()
        {
            AttackData.Payment payment = AttackData.Payment.LoadById(this.QueryString("order_id").Int());
            if (payment == null)
            {
                Response.Write("AttackData.Payment ID " + this.QueryString("order_id").Int() + " not found");
                Response.End();
            }
            else if (payment.TransactionID != this.QueryString("transactionId"))
            {// || payment.TransactionID != Session["terminal.transaction-id"].Str()) {
                Response.Redirect("terminal.aspx" + payment.QueryString);
            }
            Dictionary<string, string> QueryString = jlib.net.HTTP.decodeRawQueryString(payment.QueryString, false);

            if (RequestJSON == null && QueryString.ContainsKey("payment"))
                RequestJSON = jlib.functions.json.DynamicJson.Parse(System.Web.HttpUtility.UrlDecode(QueryString["payment"]));

            Util.Nets.terminal terminal;
            if (payment.IsTest)
                terminal = new Util.Nets.devterminal(payment);
            else
                terminal = new Util.Nets.prodterminal(payment);

            terminal.Process();

            if (ShouldCreateUserInPhoenix())
            {
                CreateUserIfNotExists(payment);
                EnrollUserInProgram(payment);
            }
            SendOrderConfirmation(payment);
            ProcessOrderSuccessLogging(payment);
            if (RequestJSON != null)
            {
                if (RequestJSON.SuccessURL())
                    Response.Redirect(RequestJSON.SuccessURL);
                else
                {
                    Response.Clear();
                    Response.StatusCode = 200;
                    Response.End();
                }
            }
            else
                Response.Redirect(System.Web.HttpUtility.UrlDecode(QueryString.SafeGetValue("success")));
        }
        //Add description to Lines[]-array based on names of activitysets
        private void _buildRequestJSON(AttackData.Payment payment)
        {
            Util.Phoenix.session session = new Util.Phoenix.session(true);

            if (RequestJSON != null)
            {
                foreach (var line in RequestJSON.Lines)
                {
                    string description = "";// convert.SafeGetProperty(line, "Desc");
                    if (description.IsNullOrEmpty())
                    {
                        if (convert.HasProperty(line, "ILMID"))
                        {
                            if (line.ILMID as jlib.functions.json.DynamicJson != null && line.ILMID.IsArray)
                            {
                                foreach (var id in line.ILMID)
                                    description += (description.IsNullOrEmpty() ? "" : "<br />") + new Util.Classes.ActivitySet(session.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ActivitySetGet, convert.cInt(id))).DataObject, null).Name;
                            }
                            else
                            {
                                foreach (var s in parse.split(line.ILMID, ","))
                                {
                                    if (convert.cInt(s) > 0)
                                        description += (description.IsNullOrEmpty() ? "" : "<br />") + new Util.Classes.ActivitySet(session.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ActivitySetGet, convert.cInt(s))).DataObject, null).Name;
                                }
                            }
                        }
                        if (description.IsNullOrEmpty()) description = convert.SafeGetProperty(line, "Desc");
                        line.Description = description;
                        payment.OrderDescription += (payment.OrderDescription.IsNullOrEmpty() ? "" : ", ") + description;
                    }
                }
            }
        }
        private void SendOrderConfirmation(AttackData.Payment payment)
        {
            Dictionary<string, string> QueryString = jlib.net.HTTP.decodeRawQueryString(payment.QueryString, false);
            if (RequestJSON == null && QueryString.ContainsKey("payment"))
                RequestJSON = jlib.functions.json.DynamicJson.Parse(System.Web.HttpUtility.UrlDecode(QueryString["payment"]));

            _buildRequestJSON(payment);
            List<KeyValuePair<string, byte[]>> files = new List<KeyValuePair<string, byte[]>>();
            if (RequestJSON != null && (RequestJSON.Source == "PLNO" || RequestJSON.Source == "P2NO" || RequestJSON.Source == "BPNO"))
            {
                string html = Util.Email.RenderRazorTemplate(new { Order = payment, RequestJSON = RequestJSON }, "pdf-angrerett", "no");
                files.Add(new KeyValuePair<string, byte[]>("Angrerett ved forbrukersalg.pdf", Util.Cms.cPDF(html, null, null)));
            }
            Util.Email.SendRazorEmail(new { Order = payment, RequestJSON = RequestJSON }, "email-order-confirmation", "email-order-confirmation", "", true, (payment.OrderCurrency == "NOK" ? "no" : (payment.OrderCurrency == "SEK" ? "sv" : (payment.OrderCurrency == "DKK" ? "da" : (payment.SiteCode == "P2DE" ? "de" : "en")))), files);
        }
        private string GenerateOrderConfirmation(AttackData.Payment payment)
        {
            Dictionary<string, string> QueryString = jlib.net.HTTP.decodeRawQueryString(payment.QueryString, false);
            if (RequestJSON == null && QueryString.ContainsKey("payment"))
                RequestJSON = jlib.functions.json.DynamicJson.Parse(System.Web.HttpUtility.UrlDecode(QueryString["payment"]));

            _buildRequestJSON(payment);

            return Util.Email.RenderRazorTemplate(new { Order = payment, RequestJSON = RequestJSON }, "email-order-confirmation", (payment.OrderCurrency == "NOK" ? "no" : (payment.OrderCurrency == "SEK" ? "sv" : (payment.OrderCurrency == "DKK" ? "da" : (payment.SiteCode == "P2DE" ? "de" : "en")))), true);
        }

        private AttackData.Payment CreatePaymentObject()
        {

            string Amount = parse.replaceAll(this.QueryString("amount"), " ", "", ",-", "", "$", "", "USD", "", "NOK", "", "SEK", "", "EUR", "", "£", "", "€", "", "GBP", "").Trim();
            string Separator = parse.findSeparator(Amount, ",", ".");
            if (Separator == ",") Amount = parse.replaceAll(Amount, ".", "", Separator, ".");

            AttackData.Payment payment;

            if (RequestJSON == null)
            {
                payment = new AttackData.Payment()
                {
                    QueryString = Request.Url.Query,
                    OrderDescription = this.QueryString("desc"),
                    OrderAmount = Amount.Dec(),
                    OrderCurrency = this.QueryString("currency").Str().ToUpper(),
                    UserEmail = this.QueryString("email").Str().ToLower(),
                    UserFirstName = this.QueryString("extension-firstName"),
                    UserLastName = this.QueryString("extension-lastName"),
                    UserPhone = this.QueryString("extension-CusAttr-Telephone"),
                    Company = this.QueryString("extension-CusAttr-Invoice name"),
                    AddressStreet = this.QueryString("adr-extadd"),
                    AddressZip = convert.cStr(this.QueryString("pcode"), this.QueryString("adr-pcod")),
                    AddressCity = this.QueryString("adr-locality"),
                    AddressCountry = this.QueryString("adr-country"),
                    InvoiceReference = this.QueryString("extension-CusAttr-Order reference 1"),
                    PaymentMethodID = 2, //credit card
                    IsTest = this.QueryString("test").Bln()
                };
            }
            else
            {
                Amount = "0";
                string orderDescription = "";
                int prevAttendee = -1;
                foreach (var line in RequestJSON.Lines)
                {
                    string desc = convert.cIfValue(convert.SafeGetProperty(line, "Desc"), "", "<br />", "");
                    if (!desc.IsNullOrEmpty())
                    {
                        if (RequestJSON.Attendees.Count() > 1 && prevAttendee != convert.cInt(line.Attendee))
                        {
                            prevAttendee = convert.cInt(line.Attendee);
                            desc += "<b>" + RequestJSON.Attendees[prevAttendee].FName + " " + RequestJSON.Attendees[prevAttendee].LName + "</b></br />";
                        }
                        orderDescription += desc;
                    }
                    Amount = convert.cStr(convert.cDbl(Amount) + Math.Round(convert.cDbl(convert.SafeGetProperty(line, "Price")) * (1 + convert.cDbl(convert.SafeGetProperty(line, "VATRate")) / 100)));
                }
                payment = new AttackData.Payment()
                {
                    QueryString = Request.Url.Query,
                    OrderDescription = orderDescription,
                    OrderAmount = Amount.Dec(),
                    OrderCurrency = convert.cStr(RequestJSON.Currency).ToUpper(),
                    UserEmail = convert.cStr(RequestJSON.Attendees[0].Email).ToLower().Trim(),
                    UserFirstName = RequestJSON.Attendees[0].FName,
                    UserLastName = RequestJSON.Attendees[0].LName,
                    UserPhone = RequestJSON.Attendees[0].Phone,
                    Company = convert.SafeGetProperty(RequestJSON.Company, "Name"),
                    InvoiceReference = convert.SafeGetProperty(RequestJSON.Company, "YourRef"),
                    PaymentMethodID = convert.cInt(RequestJSON.Method),
                    IsTest = convert.cBool(RequestJSON.Test),
                    GoogleCookieID = convert.cStr(convert.SafeGetProperty(RequestJSON, "UA")),
                    SiteCode = convert.cStr(convert.SafeGetProperty(RequestJSON, "Source"))
                };
                if (convert.HasProperty(RequestJSON.Company, "Address"))
                {
                    payment.AddressStreet = RequestJSON.Company.Address.Street;
                    payment.AddressZip = RequestJSON.Company.Address.Zip;
                    payment.AddressCity = RequestJSON.Company.Address.City;
                    payment.AddressCountry = RequestJSON.Company.Address.Country;
                }
            }

            if (Request.HttpMethod == "POST" && Request.QueryString["payment"].IsNullOrEmpty()) payment.QueryString = "?" + convert.cStreamToString(Request.InputStream, Request.ContentEncoding);

            payment.Save();
            return payment;
        }
        private void InitiatePaymentRequest(AttackData.Payment payment)
        {
            Util.Nets.terminal terminal;
            if (payment.IsTest)
                terminal = new Util.Nets.devterminal(payment);
            else
                terminal = new Util.Nets.prodterminal(payment);

            terminal.Register();
        }
        private bool ShouldCreateUserInPhoenix()
        {
            if (RequestJSON == null || RequestJSON.Attendees.Count() == 0) return true;
            bool create = false;
            foreach (var attendee in RequestJSON.Attendees)
            {
                if (!attendee.PreventCreation) create = true;
            }
            return create;
        }

        private void CreateUserIfNotExists(AttackData.Payment payment)
        {
            //accounts?username={username}
            //accounts?email={email}

            //        {
            // "DistributorId": 1194, // "Id" fra "GET distributors"
            // "CompanyName": "A company that makes everything",
            // "Email": "admin@acme.com",
            // "FirstName": "Someone",
            // "LastName": "Smith",
            // "Phone": "99887766",
            // "StreetAddress1": "Testgata 2",
            // "StreetAddress2": "Oppgang 5",
            // "ZipCode": "1234",
            // "City": "Oslo",
            // "Country": "NO", // "Value" fra "GET codes?type=countrycodes"
            // "LanguageId": 45 // "Id" fra "GET languages"
            //}


            //Response (201 Created):
            //{
            // "UserId": 1234,
            // "ArbitraryCustomerId": 9876
            //}
            //Og URLen er: "POST accounts/arbitrary"

            //check country
            Util.Phoenix.session session = new Util.Phoenix.session(true);
            Util.Phoenix.session.RequestResult result = session.PhoenixRequest("GET", Util.Phoenix.session.Queries.CountryCodesGet, null, 60 * 60 * 24, true, false);
            Dictionary<string, string> codes = new Dictionary<string, string>();
            foreach (dynamic code in result.DataObject)
                codes.Add(convert.cStr(code.Value).ToUpper(), convert.cStr(code.Display).ToUpper());

            payment.AddressCountry = payment.AddressCountry.Str().Trim().ToUpper();
            if (!codes.ContainsKey(payment.AddressCountry))
            {
                if (codes.ContainsValue(payment.AddressCountry)) payment.AddressCountry = codes.FindKeysByValue(payment.AddressCountry).ToList()[0];
                else
                {
                    payment.AddressCountry = parse.replaceAll(payment.AddressCountry, "SVERIGE", "SV", "DANMARK", "DK", "NORGE", "NO", "DEUTSCHLAND", "DE");
                    if (!codes.ContainsValue(payment.AddressCountry)) payment.AddressCountry = "NO";
                }
            }

            //language
            result = session.PhoenixRequest("GET", Util.Phoenix.session.Queries.LanguagesGet, null, 60 * 60 * 24, true, false);
            Dictionary<string, int> languages = new Dictionary<string, int>();
            foreach (dynamic code in result.DataObject)
                languages.Add(convert.cStr(code.Identifier).ToUpper(), convert.cInt(code.Id));

            Dictionary<string, string> countryLangMapping = new Dictionary<string, string>(){
        {"DK","DA"}
        };

            int languageId = languages.SafeGetValue(payment.AddressCountry);
            int distributorId = 1194;
            if (languageId == 0)
            {

                languageId = languages.SafeGetValue(countryLangMapping.SafeGetValue(payment.AddressCountry));
                if (languageId == 0) languageId = languages["EN"];
            }

            if (payment.SiteCode.Str().EndsWith("SE"))
            {
                distributorId = 1195;
                languageId = 46;
            }
            else if (payment.SiteCode.Str().EndsWith("NO"))
            {
                distributorId = 1194;
                languageId = 45;
            }
            else if (payment.SiteCode.Str().EndsWith("DE"))
            {
                distributorId = 11129;
                languageId = 44;
            }
            else if (payment.SiteCode.Str().EndsWith("DA") || payment.SiteCode.Str().EndsWith("DK"))
            {
                distributorId = 3448;
                languageId = 41;
            }

			int customerIdFromRequest = convert.cInt( (RequestJSON == null ? "" : convert.SafeGetProperty(RequestJSON, "Company", "CustomerId")));

			if (customerIdFromRequest == 0)
			{
				var request = new
				{
					DistributorId = distributorId,
					CompanyName = convert.cStr(payment.Company, payment.UserFirstName + " " + payment.UserLastName),
					CompanyRegistrationNumber = (RequestJSON == null ? "" : convert.SafeGetProperty(RequestJSON, "Company", "RegNumber")),
					OurRef = (RequestJSON == null ? "" : convert.SafeGetProperty(RequestJSON, "Company", "OurRef")),
					Email = payment.UserEmail,
					FirstName = payment.UserFirstName,
					LastName = payment.UserLastName,
					Phone = payment.UserPhone,
					StreetAddress1 = payment.AddressStreet,
					StreetAddress2 = "",
					ZipCode = payment.AddressZip,
					City = payment.AddressCity,
					Country = payment.AddressCountry,
					LanguageId = languageId,  //--- Both are used in arbitrary call
					PreferredLanguageId = languageId //--- Both are used in arbitrary call
				};
				result = session.PhoenixPost(Util.Phoenix.session.Queries.ArbitraryUserCreate, jlib.functions.json.DynamicJson.Serialize(request));
				if (result.HttpResponseCode != 200) throw new Exception("ArbitraryUserCreate threw an error. Code: " + result.HttpResponseCode + " / message: " + result.HttpErrorMessage + "\n\nData:\n" + jlib.functions.json.DynamicJson.Serialize(request));
				payment.UserID = convert.cInt(result.DataObject.UserId);
				payment.CustomerID = convert.cInt(result.DataObject.ArbitraryCustomerId);
			}
			else
			{
				payment.CustomerID = customerIdFromRequest;

				result = session.PhoenixGet(string.Format(Util.Phoenix.session.Queries.CustomerGet, customerIdFromRequest));
				if (result.HttpResponseCode == 200)
					languageId = convert.cInt(result.DataObject.LanguageId);
			}



            if (RequestJSON != null && convert.HasProperty(RequestJSON, "Attendees"))
            {
                RequestJSON.Attendees[0].UserID = payment.UserID;

                for (int x = (customerIdFromRequest > 0 ? 0 : 1); x < RequestJSON.Attendees.Count(); x++)
                {
                    var attendee = RequestJSON.Attendees[x];
                    var newUser = new
                    {
                        CustomerId = payment.CustomerID,
                        Username = attendee.Email.Trim(),
                        password = "test",
                        passwordRepeat = "test",
                        Email = attendee.Email.Trim(),
                        Roles = new List<string>() { "LearningPortalUser" },
                        DistributorId = (null as string),
                        User = new
                        {
                            MembershipUserId = attendee.Email.Trim(),
                            FirstName = attendee.FName.Trim(),
                            LastName = attendee.LName.Trim(),
                            PhoneNumber = attendee.Phone.Trim(),
                            //StreetAddress = "",
                            //ZipCode = "",
                            //City = "",
                            //Country = request.Country,
                            PreferredLanguageId = languageId,
                            CustomerId = payment.CustomerID,
                            UserRegistrationNotificationDate = System.DateTime.Now.AddDays(-1),
                            UserNotificationType = 0,
                            UserInfoElements = new List<object>() { }
                        }
                    };
                    result = session.PhoenixGet(String.Format(Util.Phoenix.session.Queries.UserGetByUsername, System.Web.HttpUtility.UrlEncode(newUser.Username)), false, false);
                    if (result.HttpResponseCode == 400 || result.HttpResponseCode == 404) result = session.PhoenixGet(String.Format(Util.Phoenix.session.Queries.UserGetByEmail, System.Web.HttpUtility.UrlEncode(newUser.Username)), false, false);
                    if (result.HttpResponseCode == 404 || result.HttpResponseCode == 500 || result.HttpResponseCode == 400)
                    {
                        result = session.PhoenixPost(Util.Phoenix.session.Queries.UserCreate, jlib.functions.json.DynamicJson.Serialize(newUser));
                        if (result.Error) throw new Exception("User create failed! " + result.HttpErrorMessage, result.RequestError);
                    }
					var userId = convert.cInt(convert.SafeGetProperty(result.DataObject, "Id"), convert.SafeGetProperty(result.DataObject, "User", "Id"));
					attendee.UserID = userId;
					if (payment.UserID < 1) payment.UserID = userId;

				}

                Dictionary<string, string> queryString = jlib.net.HTTP.decodeRawQueryString(payment.QueryString, false);
                queryString["payment"] = System.Web.HttpUtility.UrlEncode(RequestJSON.ToString());
                payment.QueryString = jlib.net.HTTP.EncodeRawQueryString(queryString);
            }
            payment.Save(payment.UserID);
        }
        public class AttendeeDetails
        {
            public AttendeeDetails(int CurrencyId)
            {
                this.CurrencyId = CurrencyId;
            }
            public int Index { get; set; }
            public List<AttendeeActivitySet> ActivitySets { get; set; } = new List<AttendeeActivitySet>();
            public decimal DjangoPrice { get; set; }
            public int CurrencyId { get; set; }
            public decimal GetPhoenixPrice()
            {
                return ActivitySets.Sum(x=>x.GetPhoenixPrice());
            }
            public int GetPhoenixCurrencyId()
            {
                //Select first activity with a price, and return currency code
                var ids = ActivitySets.SelectMany(set => set.Activites.Where(a => a.PhoenixPrice > 0).Select(x => x.PhoenixCurrencyCodeId)).ToList();
                if (ids.Count > 0) return ids.First();

                //If not activities have a price, select first activity and return currency code
                return ActivitySets.SelectMany(set => set.Activites.Select(x => x.PhoenixCurrencyCodeId)).ToList().First();
            }

            public List<object> GetParticipantPayments()
            {
                var participantPayments = new List<object>();
                if (ActivitySets.SelectMany(x => x.Activites).Count() == 0 || (GetPhoenixPrice() == DjangoPrice && CurrencyId == GetPhoenixCurrencyId()))
                    return participantPayments;

                var activitiesWithPhoenixPrices = ActivitySets.SelectMany(x => x.Activites).Where(x => x.PhoenixPrice > 0).ToList();
                if (activitiesWithPhoenixPrices.Count == 0)
                    activitiesWithPhoenixPrices.Add(ActivitySets.SelectMany(x => x.Activites).First());

                var remainingAmountToDistribute = DjangoPrice;
                for(var x=0;x< activitiesWithPhoenixPrices.Count; x++)
                {
                    var amount = remainingAmountToDistribute;
                    if (x < activitiesWithPhoenixPrices.Count - 1)
                        amount = Math.Round((DjangoPrice / GetPhoenixPrice()) * activitiesWithPhoenixPrices[x].PhoenixPrice);

                    remainingAmountToDistribute -= amount;

                    participantPayments.Add(new
                    {
                        ActivityId = activitiesWithPhoenixPrices[x].ActivityId,
                        Price = amount,
                        CurrencyCodeId = CurrencyId
                    });
                }
                return participantPayments;
            }
        }
        public class AttendeeActivitySet
        {
            public int ActivitySetId { get; set; }
            public List<AttendeeActivity> Activites { get; set; } = new List<AttendeeActivity>();
            public decimal GetPhoenixPrice()
            {
                return Activites.Sum(x => x.PhoenixPrice);
            }
        }
        public class AttendeeActivity
        {
            public int ActivityId { get; set; }
            public decimal PhoenixPrice { get; set; }
            public int PhoenixCurrencyCodeId { get; set; }
            //public decimal CalculatedPrice { get; set; }
        }
        private void EnrollUserInProgram(AttackData.Payment payment)
        {
            var session = new Util.Phoenix.session(true);
            var result = session.PhoenixRequest("GET", Util.Phoenix.session.Queries.CurrencyCodesGet, null, 60 * 60 * 24, true, false);
            var codes = new Dictionary<string, int>();
            foreach (dynamic code in result.DataObject)
                codes.Add(convert.cStr(code.Value).ToUpper(), convert.cInt(code.Id));

            var currencyId = convert.cInt(codes.SafeGetValue(payment.OrderCurrency.ToUpper()), 262);


            var user = Util.Classes.user.getUser(payment.UserID);
            string errors = "";

            if (RequestJSON != null)
            {
                List<int> classroomIDs = new List<int>();

                var attendeeILMs = new Dictionary<int, AttendeeDetails>();

                foreach (var line in RequestJSON.Lines)
                {
                    int attendeeIndex = convert.cInt(line.Attendee);
                    if (!attendeeILMs.ContainsKey(attendeeIndex)) attendeeILMs.Add(attendeeIndex, new AttendeeDetails(currencyId));
                    attendeeILMs[attendeeIndex].DjangoPrice += convert.cDec(convert.SafeGetProperty(line, "Price"));
                    if (convert.HasProperty(line, "ClassroomID"))
                    {
                        if (line.ClassroomID as jlib.functions.json.DynamicJson != null && line.ClassroomID.IsArray)
                        {
                            foreach (var id in line.ClassroomID)
                                jlib.functions.extensions.AddIfNotExist(classroomIDs, convert.cInt(id));
                        }
                        else
                        {
                            foreach (var s in parse.split(line.ClassroomID, ","))
                            {
                                if (convert.cInt(s) > 0) jlib.functions.extensions.AddIfNotExist(classroomIDs, convert.cInt(s));
                            }
                        }
                    }
                    if (convert.HasProperty(line, "ILMID"))
                    {
                        if (line.ILMID as jlib.functions.json.DynamicJson != null && line.ILMID.IsArray)
                        {
                            foreach (var id in line.ILMID)
                            {
                                var activitySetId = convert.cInt(id);
                                attendeeILMs[attendeeIndex].ActivitySets.Add(new AttendeeActivitySet { ActivitySetId = activitySetId, });
                            }
                        }
                        else
                        {
                            foreach (var s in parse.split(line.ILMID, ","))
                            {
                                var activitySetId = convert.cInt(s);
                                if (activitySetId > 0 && !attendeeILMs[attendeeIndex].ActivitySets.Any(x => x.ActivitySetId == activitySetId))
                                    attendeeILMs[attendeeIndex].ActivitySets.Add(new AttendeeActivitySet { ActivitySetId = activitySetId, });
                            }
                        }
                    }
                }

                var activitySetsToEnroll = new List<Phoenix.LearningPortal.Util.Classes.ActivitySet>();
                attendeeILMs.ToList().ForEach(x =>
                {
                    x.Value.ActivitySets.ForEach(set =>
                    {
                        var existing = activitySetsToEnroll.FirstOrDefault(y => y.Id == set.ActivitySetId);
                        if (existing == null)
                        {
                            existing = new Util.Classes.ActivitySet(session.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ActivitySetGet, set.ActivitySetId)).DataObject, null);
                            activitySetsToEnroll.Add(existing);
                        }
                        existing.Activities.ForEach(activity =>
                        {
                            set.Activites.Add(new AttendeeActivity
                            {
                                ActivityId = activity.Id,
                                PhoenixPrice = activity.UnitPrice.Dec(),
                                PhoenixCurrencyCodeId = activity.CurrencyCodeId
                            });
                        });
                    });
                });


                for (int attendeeIndex = 0; attendeeIndex < RequestJSON.Attendees.Count(); attendeeIndex++)
                {
                    var attendee = RequestJSON.Attendees[attendeeIndex];
                    if (attendeeILMs.ContainsKey(attendeeIndex) && attendeeILMs[attendeeIndex].ActivitySets.Count > 0)
                    {
                        var maxRetrys = 2;
                        for (var retryCounter = 0; retryCounter < maxRetrys; retryCounter++)
                        {
                            var infoElements = new List<object>();
                            if (!payment.InvoiceReference.IsNullOrEmpty())
                                infoElements.Add(new
                                {
                                    InfoText = payment.InvoiceReference,
                                    LeadingTextId = 11
                                });


                            var enrollmentDetails = new
                            {
                                InfoElements = infoElements,
                                ParticipantPayments = attendeeILMs[attendeeIndex].GetParticipantPayments()
                            };
                            string requestBody = jlib.functions.json.DynamicJson.Serialize(enrollmentDetails);

                            string url = "activitysets/enroll?userId=" + convert.cInt(attendee.UserID) + "&paymentReference=" + payment.CCAuthorizationID + "&customerId=" + payment.CustomerID + "&activitySetId=" + attendeeILMs[attendeeIndex].ActivitySets.Select(x => x.ActivitySetId).ToList().Join("&activitySetId=");
                            result = session.PhoenixPut(url, requestBody);
                            if (result.HttpResponseCode != 200)
                            {
                                if(retryCounter== maxRetrys-1)
                                    errors += "* URL: " + url + "\n * RequestBody: " + requestBody.ToString() + " / Message: " + convert.SafeGetProperty(result.HttpErrorJSON, "ValidationErrors") + " -- " + result.Data + ". <a href='https://mymetier.net/learningportal/tools/terminal.aspx?action=retry&url=" + System.Web.HttpUtility.UrlEncode(url) + "&post=" + System.Web.HttpUtility.UrlEncode(requestBody.ToString()) + "'>Retry now</a>.<br />";
                            }else
                            {
                                break;
                            }
                        }
                    }
                }

                if (classroomIDs.Count > 0)
                    Util.Email.sendEmail("noreply@metier.no", "phoenix@metier.zendesk.com", "", "", "Trying to enroll user directly in activity", "The user " + payment.UserEmail + " directly in activities: " + classroomIDs.Join(", ") + ".<br />This is not supported in Phoenix.", true, null);

                if (!errors.IsNullOrEmpty())
                    Util.Email.sendEmail("noreply@metier.no", "phoenix@metier.zendesk.com", "", "", "Enrollment error!", "Couldn't enroll user " + payment.UserEmail + " in activitysets:<br />" + errors + ".<br />", true, null);
            }
        }

        protected dynamic RequestJSON { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.QueryString("action") == "retry")
            {
                var url = this.QueryString("url");
                var post = this.QueryString("post");
                var session = new Util.Phoenix.session(true);
                var result = session.PhoenixPut(url, post);
                if (result.HttpResponseCode != 200)
                {
                    Response.Write("Error! Code " + result.HttpResponseCode + " / Message: " + convert.SafeGetProperty(result.HttpErrorJSON, "ValidationErrors") + " -- " + result.Data + ".");
                }
                else
                {
                    Response.Write("User was successfully enrolled!");
                }

                Response.End();
            }
            if (this.QueryString("action") == "chimp")
            {
                if (!this.QueryString("list").IsNullOrEmpty()) ChimpSubscribe(Request["email"], Request["list"], Request["interest"], Request["phone"]);
                if (this.QueryString("log").Bln())
                {
                    if (Request["id"].Int() > 0) ChimpLog(Request["id"], "", "", "", "");
                    else ChimpLog("", Request["email"], convert.cStr(Request["source"], parse.replaceAll(this.Referrer, "http", "", "://", "", "www.", "").SafeSplit("/", 0)), Request["cid"], Request["eid"]);
                }
                Response.End();
            }
            else if (this.QueryString("action") == "receipt")
            {
                if ((Page.Master as Inc.Master.Default).ActiveUser != null && (Page.Master as Inc.Master.Default).ActiveUser.IsBehindSuperAdmin)
                {
                    var payment = AttackData.Payment.LoadById(this.QueryString("id").Int());
                    if (this.QueryString("email").Bln())
                    {
                        SendOrderConfirmation(payment);
                        Response.Write("sent.");
                    }
                    else
                    {
                        Response.Write(GenerateOrderConfirmation(payment));
                    }
                }
                else
                {
                    Response.Write("You need to be logged in as a Super Admin to perform this action");
                }
                Response.End();
            }

            if (!this.QueryString("payment").IsNullOrEmpty())
                RequestJSON = jlib.functions.json.DynamicJson.Parse(this.QueryString("payment"));
            else if (!Request["payment"].IsNullOrEmpty())
                RequestJSON = jlib.functions.json.DynamicJson.Parse(Request["payment"]);
            else if (Request.ContentType == "application/json")
                RequestJSON = jlib.functions.json.DynamicJson.Parse( convert.cStreamToString(Request.InputStream) );

            if (this.QueryString("success").Bln())
            {
                ProcessResponseFromPaymentProvider();
            }
            else
            {
                AttackData.Payment payment = CreatePaymentObject();
                if (payment.UserEmail.Str().Trim().IsNullOrEmpty())
                {
                    throw new Exception("Can't process order from user without a supplied email address.\n\nURL: " + Request.RawUrl);
                }
                //If PaymentMethodID==2, then it's a CC purchase
                if (payment.PaymentMethodID == (int)PaymentMethods.Invoice || payment.OrderAmount == 0)
                {
                    if (ShouldCreateUserInPhoenix())
                    {
                        CreateUserIfNotExists(payment);
                        EnrollUserInProgram(payment);
                    }
                    ProcessOrderSuccessLogging(payment);
                    SendOrderConfirmation(payment);

                    if(RequestJSON.SuccessURL())
                        Response.Redirect(RequestJSON.SuccessURL);
                    else
                    {
                        Response.Clear();
                        Response.StatusCode = 200;
                        Response.End();
                    }
                }

                InitiatePaymentRequest(payment);
            }
        }
        private bool ChimpSubscribe(string email, string listId, string interest, string phone)
        {
            dynamic request = jlib.functions.json.DynamicJson.Parse(jlib.functions.json.DynamicJson.Serialize(
                new
                {
                    email = new { email = email },
                    apikey = "df8efb35711ef54f3a4fc8832c1381d8-us1",
                    id = listId,
                    double_optin = false,
                    email_type = "html"
                })
            );
            if (!phone.IsNullOrEmpty() || !interest.IsNullOrEmpty())
            {
                request.merge_vars = new object();
                if (!phone.IsNullOrEmpty()) request.merge_vars.phone = phone;
                if (!interest.IsNullOrEmpty()) request.merge_vars.groupings = new List<object>() { new { name = "Interests", groups = new List<object>() { interest } } };
            }
            jlib.net.HTTP http = new HTTP();
            http.Debug = true;
            http.LogFile = Server.MapPath("/jlib.http.txt");
            http.RetryAttemps = 1;
            Response.Write(request.ToString());
            http.Post("https://us1.api.mailchimp.com/2.0/lists/subscribe.json", request.ToString());
            return true;
        }
        private void ProcessOrderSuccessLogging(AttackData.Payment payment)
        {
            if (payment.IsTest) return;
            double orderTotalExVat = payment.OrderAmount.Dbl();
            if (RequestJSON != null)
            {
                if (convert.cStr(convert.SafeGetProperty(RequestJSON, "List")) != "")
                {
                    foreach (var attendee in RequestJSON.Attendees)
                    {
                        ChimpSubscribe(attendee.Email, convert.SafeGetProperty(RequestJSON, "List"), convert.SafeGetProperty(RequestJSON, "Interest"), attendee.Phone);
                    }
                }
                orderTotalExVat = 0;
                foreach (dynamic line in RequestJSON.Lines)
                    orderTotalExVat += convert.cDbl(convert.SafeGetProperty(line, "Price"));
            }
            ChimpLog(payment.ID.Str(), "", "", "", "");
            if (payment.GoogleCookieID.IsNotNullOrEmpty())
            {
                Util.Ecommerce.GoogleLogger logger = new Util.Ecommerce.GoogleLogger(payment.SiteCode, payment.GoogleCookieID);
                logger.LogTransaction(payment.ID, orderTotalExVat, payment.OrderCurrency);
                double rebateTotal = 0;
                foreach (dynamic line in RequestJSON.Lines)
                {
                    if (convert.cDbl(convert.SafeGetProperty(line, "Price")) < 0)
                        rebateTotal += convert.cDbl(convert.SafeGetProperty(line, "Price"));
                }

                foreach (dynamic line in RequestJSON.Lines)
                {
                    if (convert.cStr(convert.SafeGetProperty(line, "Desc")) != "" && convert.cDbl(convert.SafeGetProperty(line, "Price")) > 0)
                    {
                        logger.LogTransactionItem(payment.ID, convert.cStr(convert.SafeGetProperty(line, "Desc")), "", "", convert.cDbl(convert.SafeGetProperty(line, "Price")) + rebateTotal, 1, payment.OrderCurrency);
                        rebateTotal = 0;
                    }
                }
            }
        }
        private bool ChimpLog(string orderID, string email, string source, string cID, string eID)
        {
            List<object> products = new List<object>();
            double orderTotal = 0;
            List<Util.Classes.Activity> activities = new List<Util.Classes.Activity>();
            List<int> ilmIDs = new List<int>();
            jlib.helpers.structures.OrderedDictionary<string, double> lineItemsNotInIlearn = new jlib.helpers.structures.OrderedDictionary<string, double>();

            AttackData.Payment payment = null;

            payment = AttackData.Payment.LoadById(orderID.Int());
            if (RequestJSON == null)
            {
                Dictionary<string, string> QueryString = jlib.net.HTTP.decodeRawQueryString(payment.QueryString, false);
                RequestJSON = jlib.functions.json.DynamicJson.Parse(System.Web.HttpUtility.UrlDecode(QueryString["payment"]));
            }
            email = payment.UserEmail;
            if (cID.IsNullOrEmpty()) cID = convert.SafeGetProperty(RequestJSON, "CID");
            if (eID.IsNullOrEmpty()) eID = convert.SafeGetProperty(RequestJSON, "EID");
            if (source.IsNullOrEmpty()) source = convert.SafeGetProperty(RequestJSON, "Source");
            if (source.IsNullOrEmpty()) source = "unknown";

            foreach (var line in RequestJSON.Lines)
            {
                if (convert.HasProperty(line, "ILMID"))
                {
                    if (line.ILMID as jlib.functions.json.DynamicJson != null && line.ILMID.IsArray)
                    {
                        foreach (var id in line.ILMID)
                            if (convert.cInt(id) > 0) ilmIDs.Add(convert.cInt(id));
                    }
                    else
                    {
                        foreach (var s in parse.split(line.ILMID, ","))
                        {
                            if (convert.cInt(s) > 0) ilmIDs.Add(convert.cInt(s));
                        }
                    }
                }
                if (!convert.HasProperty(line, "ClassroomID") && !convert.HasProperty(line, "ILMID"))
                {
                    if (convert.cStr(line.Desc) != "") lineItemsNotInIlearn[convert.cStr(line.Desc)] = convert.cDbl(convert.SafeGetProperty(line, "Price")) + jlib.functions.extensions.SafeGetValue(lineItemsNotInIlearn, convert.cStr(line.Desc));
                }
            }

            Util.Phoenix.session session = new Util.Phoenix.session(true);
            foreach (int activitySetId in ilmIDs)
            {
                new Util.Classes.ActivitySet(session.PhoenixGet(String.Format(Util.Phoenix.session.Queries.ActivitySetGet, convert.cInt(activitySetId))).DataObject, null).Activities.ForEach(activity =>
                {
                    activities.Add(activity);
                    activity.CurrentSession = session;
                });
            }


            foreach (Util.Classes.Activity activity in activities)
            {

                products.Add(new
                {
                    //product_id = activity.ArticleOriginalObject.Id,
                    product_id = activity.Id,
                    //sku = activity.ArticleNumber,
                    product_name = activity.Name,
                    category_id = 1,
                    category_name = "E-learning",
                    qty = 1,
                    cost = activity.UnitPrice.Dbl()
                });
                orderTotal += activity.UnitPrice.Dbl();
            }

            //handle situation where user is paying for something not in ilearn
            if (orderTotal == 0)
            {
                foreach (var item in lineItemsNotInIlearn)
                {
                    products.Add(new
                    {
                        product_id = 1,
                        sku = "UNKNOWN",
                        product_name = item.Key,
                        category_id = 1,
                        category_name = "E-learning",
                        qty = 1,
                        cost = item.Value
                    });
                    orderTotal += item.Value;
                }
            }
            var request = new
            {
                apikey = "6fc8fdc5a3b5281bdf5c41f5e39d7141-us1",
                order = new
                {
                    id = orderID,
                    campaign_id = cID,
                    email_id = eID,
                    email = email,
                    total = orderTotal,
                    order_date = DateTime.Now,
                    shipping = 0,
                    tax = 0,
                    store_id = source,
                    items = products
                }
            };
            jlib.net.HTTP http = new HTTP();
            http.Debug = true;
            http.LogFile = Server.MapPath("/jlib.http.txt");
            http.RetryAttemps = 1;
            string post = parse.replaceAll(jlib.functions.json.DynamicJson.Serialize(request), "\"null\"", "null");
            Response.Write("<br /><br />About to POST: \n" + post + "<br /><br />");
            http.Post("https://us1.api.mailchimp.com/2.0/ecomm/order-add.json", post);
            return true;
        }

    }
}