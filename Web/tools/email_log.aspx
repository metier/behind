﻿<%@ Page Title="Behind myMetier :: Email Log" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Tools.EmailLog" Codebehind="email_log.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
<h2 class="page-title"><img src="../inc/images/icons/tools_32.png" alt="Email Log" />Email Log</h2>

<style>
hr{    
    display:block;
}
</style>

<div id="cStatus" style="visibility:hidden"><img src='../inc/images/icons/accept.gif'/>&nbsp;Refresh complete</div>
<br style="clear: both;" />

<table cellpadding="0" cellspacing="0" border="0" class="listing assets" style="margin-bottom:0;">
	<colgroup></colgroup>
	<colgroup span="3" style="font-weight: bold;"></colgroup>
	<thead>
		<tr class="actions">
			<td colspan="11">
				<table cellpadding="0" cellspacing="0">
					<thead>
						<tr>							
							<td><span id="hGridView"></span> | <button type="button" onclick="if(window.confirm('Are you sure you want to resend ' + oGrid.getSelection().split(',').length+' email(s)?')){oGrid.addRequest(oGrid.buildRequest('custom', 'Type', 'resend-emails', 'Argument', oGrid.getSelection()));}" >Resend Checked</button></td>
							<td class="paging">
	                            Page
	                            <img src="../inc/images/icons/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow grid_prev_page" />
	                            <input type="text" value="1" class="text page page_number" />
	                            <a href="javascript:void(0);" title="Next page" ><img src="../inc/images/icons/pager_arrow_right.gif" alt="Go to Next page" class="arrow grid_next_page" /></a>

	                            of <span class="page_status">3 pages</span>            <span class="separator">|</span>
	                            View            <select name="limit" class="page_size">

	                                <option value="20" selected="selected">20</option>
	                                <option value="30">30</option>
	                                <option value="50">50</option>
	                                <option value="100">100</option>
	                                <option value="200">200</option>
	                            </select>

	                            per page
			                </td>
						</tr>
					</thead>
				</table>
			</td>
		</tr>
	</thead>
	</table>
<jgrid:jGridAdo DisableInitialFiltering="true" runat="server" id="gGrid" LoadViewContainer="hGridView" BindDropdownByID="true" style="width:956px;" SortOrder="desc" JSName="oGrid" AutoFilter="true" PageSize="50" SortCol="0" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets' style='border-top:0;margin-bottom:0'" DeleteConfirmation="Are you sure you wish to delete this row?" DisplayFilter="true">
    <Cols>
        <jgrid:col id="cID" runat="server" HText="ID" FPartialMatch="true" ColType="Hide" PrimaryKey="true" cinputatt="class='text'" FormatString="#ID#" SortField="ID" />        
        <jgrid:col id="cCheck" CAtt="style='width:10px'" FPartialMatch="true" CInputAtt="class='checkbox'" runat="server" HText="" ColType="Select" />               
        <jgrid:col id="cFrom" runat="server" HText="From" FPartialMatch="true" FormatString="email_from" FInputAtt="class='text'" ColType="Text" SortField="email_from" />   
        <jgrid:col id="cTo" runat="server" HText="To" FPartialMatch="true" FormatString="email_to" FInputAtt="class='text'" ColType="Text" SortField="email_to" />   
        <jgrid:col id="cCC" runat="server" HText="CC" FPartialMatch="true" FormatString="email_cc" FInputAtt="class='text'" ColType="Text" SortField="email_cc" />   
        <jgrid:col id="cBCC" runat="server" HText="BCC" FPartialMatch="true" FormatString="email_bcc" FInputAtt="class='text'" ColType="Text" SortField="email_bcc" />   
        <jgrid:col id="cDate" runat="server" HText="Date" FPartialMatch="true" FormatString="{0:yyyy}-{0:MM}-{0:dd} {0:HH}:{0:mm}:{0:ss}" FormatStringArguments="posted_date" FInputAtt="class='text daterangepicker'" ColType="Text" SortField="posted_date" />   
        <jgrid:col id="cSubject" runat="server" HText="Subject" FPartialMatch="true" FormatString="subject" FInputAtt="class='text'" ColType="Text" SortField="subject" />           
        <jgrid:col id="cEmailKey" runat="server" HText="Key" FPartialMatch="true" FormatString="email_key" FInputAtt="class='text'" ColType="Text" SortField="email_key" />           
        <jgrid:col id="cPreview" runat="server" HText="Preview" ColType="NoEdit" SortField="#" />        
        <jgrid:col id="cErrorMessage" runat="server" HText="Error" FPartialMatch="true" FormatString="error_message" FInputAtt="class='text'" ColType="Hide" FColType="Text" SortField="error_message" />   
        <jgrid:col id="cSent" runat="server" HText="Is Sent" FPartialMatch="true" FormatString="sent" FInputAtt="class='text'" ColType="Hide" FColType="Text" SortField="sent" FField="" />   
    </Cols>                
</jgrid:jGridAdo>


<table cellpadding="0" cellspacing="0" border="0" class="listing assets" style="border-top:0px;">
	<colgroup></colgroup>
	<colgroup span="3" style="font-weight: bold;"></colgroup>
	<thead>
		<tr class="actions">
			<td colspan="11">
				<table cellpadding="0" cellspacing="0">
					<thead>
						<tr>							
							<td class="paging">
	                            Page
	                            <img src="../inc/images/icons/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow grid_prev_page" />
	                            <input type="text" value="1" class="text page page_number" />
	                            <a href="javascript:void(0);" title="Next page" ><img src="../inc/images/icons/pager_arrow_right.gif" alt="Go to Next page" class="arrow grid_next_page" /></a>

	                            of <span class="page_status">3 pages</span>            <span class="separator">|</span>
	                            View            <select name="limit" class="page_size">

	                                <option value="20" selected="selected">20</option>
	                                <option value="30">30</option>
	                                <option value="50">50</option>
	                                <option value="100">100</option>
	                                <option value="200">200</option>
	                            </select>

	                            per page
			                </td>
						</tr>
					</thead>
				</table>
			</td>
		</tr>
	</thead>
	</table>
	<div id="dialogWindow" style="display:none;width:800px; overflow:scroll"></div>	
<script>
    fnGridAttach(oGrid);
    oGrid.registerEventListener("GridRenderCell", function (oEvent) {
        if (oEvent.Data.Index >= 0) {
            if (oEvent.Data.ColObject.ID == "cTo") oEvent.Data.HTML = oEvent.Data.HTML.split(";").join("<br />").split(",").join("<br />"); ;
            if (oEvent.Data.ColObject.ID == "cFrom") oEvent.Data.HTML = oEvent.Data.HTML.split(";")[0].split(",")[0];
            if (oEvent.Data.ColObject.ID == "cPreview") oEvent.Data.HTML = "<a href='javascript:void(0)' onclick='showMessage(" + oGrid.getData(oEvent.Data.Index, "cID") + ")'>Preview</a>";
        }
    });

    function showMessage(iID) {
        //$('#dialogWindow').dialog({ modal: true, position: [50, 50], width: 1050, height: 700, buttons: { "Close": function () { $(this).dialog("close"); } } }).dialog("open").dialog("option", "title", "View Email").load("/tools/email_log.aspx?action=render_email&email_id=" + iID);
        $('#dialogWindow').dialog({ modal: true, position: [50, 50], width: 1050, height: 700, buttons: { "Close": function () { $(this).dialog("close"); } } }).dialog("open").dialog("option", "title", "View Email").html("<iframe style='width:100%;height:100%' scrolling='no' frameborder='0' onload='$(this).height($(this).contents().find(\"body\").prop(\"scrollHeight\"));' src='email_log.aspx?action=render_email&email_id=" + iID + "'></iframe>");
    }
</script>

</asp:Content>