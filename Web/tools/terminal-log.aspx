﻿<%@ Page Title="Behind myMetier :: Terminal Log" Language="C#" MasterPageFile="~/inc/master/default.master" AutoEventWireup="True" Inherits="Phoenix.LearningPortal.Tools.TerminalLog" Codebehind="terminal-log.aspx.cs" %>
<asp:Content ContentPlaceHolderID="c1" Runat="Server" Navigation="test">
<h2 class="page-title"><img src="/inc/images/icons/tools_32.png" alt="Terminal Log" />Terminal Log</h2>

<style>
hr{    
    display:block;
}
.terminal-icon-desc td{
    padding-right:10px;
    padding-bottom:5px;
}
</style>
     <!--
    test=grey<br />
    credit card=italic<br />
    success=green<br />
    fail=red<br />-->
    <table class="terminal-icon-desc">
        
    <tr><td><img src='../inc/images/icons/emoticon_smile.png' alt='Test transaction' /></td><td>Test transaction</td></tr>
    <tr><td><img alt="Success" src="../inc/images/icons/accept.png" /></td><td>Success</td></tr>
    <tr><td><img alt="Failure" src="../inc/images/icons/delete.png" /></td><td>Failure</td></tr>
    <tr><td><img alt="Credit card" src="../inc/images/icons/visa_card.png" /></td><td>Credit card</td></tr>
        </table>
<div id="cStatus" style="visibility:hidden"><img src='/inc/images/icons/accept.gif'/>&nbsp;Refresh complete</div>
<br style="clear: both;" />

<table cellpadding="0" cellspacing="0" border="0" class="listing assets" style="margin-bottom:0;">
	<colgroup></colgroup>
	<colgroup span="3" style="font-weight: bold;"></colgroup>
	<thead>
		<tr class="actions">
			<td colspan="11">
				<table cellpadding="0" cellspacing="0">
					<thead>
						<tr>							
							<td><span id="hGridView"></span> 
                                | <button type="button" onclick="if(window.confirm('Are you sure you want to reverse ' + oGrid.getSelection().split(',').length+' transaction(s)?')){oGrid.addRequest(oGrid.buildRequest('custom', 'Type', 'reverse-transactions', 'Argument', oGrid.getSelection()));}" >Reverse Checked to Google eCommerce Logging</button>
                                | <button type="button" onclick="if(window.confirm('Are you sure you want to resend ' + oGrid.getSelection().split(',').length+' transaction(s)?')){oGrid.addRequest(oGrid.buildRequest('custom', 'Type', 'resend-transactions', 'Argument', oGrid.getSelection()));}" >RESEND Checked to Google eCommerce Logging</button>

							</td>
							<td class="paging">
	                            Page
	                            <img src="/inc/images/icons/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow grid_prev_page" />
	                            <input type="text" value="1" class="text page page_number" />
	                            <a href="javascript:void(0);" title="Next page" ><img src="/inc/images/icons/pager_arrow_right.gif" alt="Go to Next page" class="arrow grid_next_page" /></a>

	                            of <span class="page_status">3 pages</span>            <span class="separator">|</span>
	                            View            <select name="limit" class="page_size">

	                                <option value="20" selected="selected">20</option>
	                                <option value="30">30</option>
	                                <option value="50">50</option>
	                                <option value="100">100</option>
	                                <option value="200">200</option>
	                            </select>

	                            per page
			                </td>
						</tr>
					</thead>
				</table>
			</td>
		</tr>
	</thead>
	</table>
   
    
<jgrid:jGridAdo DisableInitialFiltering="true" runat="server" id="gGrid" LoadViewContainer="hGridView" BindDropdownByID="true" style="width:956px;" SortOrder="desc" JSName="oGrid" AutoFilter="true" PageSize="50" SortCol="1" TAtt="cellpadding='0' cellspacing='0' border='0' class='listing assets' style='border-top:0;margin-bottom:0'" DeleteConfirmation="Are you sure you wish to delete this row?" DisplayFilter="true">
    <Cols>
        <jgrid:col id="cCheck" CAtt="style='width:10px'" FPartialMatch="true" CInputAtt="class='checkbox'" runat="server" HText="" ColType="Select" />               
        <jgrid:col id="cID" runat="server" HText="ID" FPartialMatch="true" ColType="Text" PrimaryKey="true" cinputatt="class='text'" HAtt=" style1='width:100px'" FormatString="#ID#" SortField="ID" />                
        <jgrid:col id="cPaymentStatus" runat="server" HText="Status" FPartialMatch="true" ColType="NoEdit"  cinputatt="class='text'" HAtt=" style='width:100px'" SortField="#" />                
        <jgrid:col id="cIsTest" runat="server" HText="Is Test" FPartialMatch="true" FormatString="IsTest" FInputAtt="class='text'" ColType="NoEdit" FColType="Text" SortField="IsTest" />   
        <jgrid:col id="cEmail" runat="server" HText="Email" FPartialMatch="true" FormatString="UserEmail" FInputAtt="class='text'" ColType="Text" SortField="UserEmail" />   
        <jgrid:col id="cFirst" runat="server" HText="First name" FPartialMatch="true" FormatString="UserFirstName" FInputAtt="class='text'" ColType="Text" SortField="UserFirstName" />   
        <jgrid:col id="cLast" runat="server" HText="Last name" FPartialMatch="true" FormatString="UserLastName" FInputAtt="class='text'" ColType="Text" SortField="UserLastName" />   
        <jgrid:col id="cPhone" runat="server" HText="Phone" FPartialMatch="true" FormatString="UserPhone" FInputAtt="class='text'" ColType="Text" SortField="UserPhone" />   
        <jgrid:col id="cCompany" runat="server" HText="Company" FPartialMatch="true" FormatString="Company" FInputAtt="class='text'" ColType="Text" SortField="Company" />   
        <jgrid:col id="cAddressStreet" runat="server" HText="Address" FPartialMatch="true" FormatString="AddressStreet" FInputAtt="class='text'" ColType="Text" SortField="AddressStreet" />   
        <jgrid:col id="cAddressCity" runat="server" HText="City" FPartialMatch="true" FormatString="AddressCity" FInputAtt="class='text'" ColType="Text" SortField="AddressCity" />   
        <jgrid:col id="cAddressCountry" runat="server" HText="Country" FPartialMatch="true" FormatString="AddressCountry" FInputAtt="class='text'" ColType="Text" SortField="AddressCountry" />   
        <jgrid:col id="cReference" runat="server" HText="Invoice Reference" FPartialMatch="true" FormatString="InvoiceReference" FInputAtt="class='text'" ColType="Text" SortField="InvoiceReference" />   
        <jgrid:col id="cOrderDescription" runat="server" HText="Description" FPartialMatch="true" FormatString="OrderDescription" FInputAtt="class='text'" ColType="Text" SortField="OrderDescription" />   
        <jgrid:col id="cOrderCurrency" runat="server" HText="Currency" FPartialMatch="true" FormatString="OrderCurrency" FInputAtt="class='text'" ColType="Text" SortField="OrderCurrency" />   
        <jgrid:col id="cOrderAmount" runat="server" HText="Amount" FPartialMatch="true" FormatString="OrderAmount" FInputAtt="class='text'" ColType="Text" SortField="OrderAmount" />   
                

        <jgrid:col id="cPreview" runat="server" HText="Receipt" ColType="NoEdit" SortField="#" />        
        <jgrid:col id="cCCResponseText" runat="server" HText="CCResponseText" FPartialMatch="true" FormatString="CCResponseText" FInputAtt="class='text'" ColType="NoEdit" FColType="Text" SortField="CCResponseText" />   
        <jgrid:col id="cCCResponseCode" runat="server" HText="CCResponseCode" FPartialMatch="true" FormatString="CCResponseCode" FInputAtt="class='text'" ColType="NoEdit" FColType="Text" SortField="CCResponseCode" />   
        <jgrid:col id="cSiteCode" runat="server" HText="Site" FPartialMatch="true" FormatString="SiteCode" FInputAtt="class='text'" ColType="NoEdit" FColType="Text" SortField="SiteCode" />   
        <jgrid:col id="cUser" runat="server" HText="User" FPartialMatch="true" FormatString="UserID" FInputAtt="class='text'" ColType="Hide" SortField="#" />           
        <jgrid:col id="cPaymentMethodID" runat="server" HText="PaymentMethodID" FPartialMatch="true" FormatString="PaymentMethodID" FInputAtt="class='text'" ColType="Hide" FColType="Text" SortField="PaymentMethodID" />           
    </Cols>                
</jgrid:jGridAdo>


<table cellpadding="0" cellspacing="0" border="0" class="listing assets" style="border-top:0px;">
	<colgroup></colgroup>
	<colgroup span="3" style="font-weight: bold;"></colgroup>
	<thead>
		<tr class="actions">
			<td colspan="11">
				<table cellpadding="0" cellspacing="0">
					<thead>
						<tr>							
							<td class="paging">
	                            Page
	                            <img src="/inc/images/icons/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow grid_prev_page" />
	                            <input type="text" value="1" class="text page page_number" />
	                            <a href="javascript:void(0);" title="Next page" ><img src="/inc/images/icons/pager_arrow_right.gif" alt="Go to Next page" class="arrow grid_next_page" /></a>

	                            of <span class="page_status">3 pages</span>            <span class="separator">|</span>
	                            View            <select name="limit" class="page_size">

	                                <option value="20" selected="selected">20</option>
	                                <option value="30">30</option>
	                                <option value="50">50</option>
	                                <option value="100">100</option>
	                                <option value="200">200</option>
	                            </select>

	                            per page
			                </td>
						</tr>
					</thead>
				</table>
			</td>
		</tr>
	</thead>
	</table>
	<div id="dialogWindow" style="display:none;width:800px; overflow:scroll"></div>	
<script>
    fnGridAttach(oGrid);
    oGrid.registerEventListener("GridRenderCell", function (oEvent) {
        if (oEvent.Data.Index >= 0) {
            /*if (oEvent.Data.ColObject.ID == "cTo") oEvent.Data.HTML = oEvent.Data.HTML.split(";").join("<br />").split(",").join("<br />"); ;
            if (oEvent.Data.ColObject.ID == "cFrom") oEvent.Data.HTML = oEvent.Data.HTML.split(";")[0].split(",")[0];
            
            */
            var style = "";
            var success = (oGrid.getData(oEvent.Data.Index, "cPaymentMethodID") == "1" || oGrid.getData(oEvent.Data.Index, "cCCResponseCode") == "OK");
            if (oGrid.getData(oEvent.Data.Index, "cIsTest") == "1") {
                style += "color:#555;";
            } else {
                style += (oGrid.getData(oEvent.Data.Index, "cPaymentMethodID") == "1" || oGrid.getData(oEvent.Data.Index, "cCCResponseCode") == "OK" ? "color:green;" : "color:red;");
            }
            //if (oGrid.getData(oEvent.Data.Index, "cPaymentMethodID") == "2") style += "font-style: italic;";//CC

            if (oEvent.Data.ColObject.ID == "cPaymentStatus") oEvent.Data.HTML += (oGrid.getData(oEvent.Data.Index, "cIsTest") == "True" ? "<img src='../inc/images/icons/emoticon_smile.png' alt='Test transaction' />&nbsp;" : "") + "<img alt='" + (success ? 'Success' : 'Failure') + "' src='../inc/images/icons/" + (success ? "accept.png" : "delete.png") + "' />" + (oGrid.getData(oEvent.Data.Index, "cPaymentMethodID") == "2" ? "&nbsp;<img src='../inc/images/icons/visa_card.png' alt='Credit card' />" : "");
            oEvent.Data.TagStart = oEvent.Data.TagStart.split("<td").join("<td style='" + style + "'");
            if (oEvent.Data.ColObject.ID == "cPreview") oEvent.Data.HTML = "<a href='javascript:void(0)' onclick='showMessage(" + oGrid.getData(oEvent.Data.Index, "cID") + ")'>View Receipt</a>";
        }
    });

    function showMessage(iID) {
        $('#dialogWindow').dialog({ modal: true, position: [50, 50], width: 1050, height: 700, buttons: { "Close": function () { $(this).dialog("close"); } } }).dialog("open").dialog("option", "title", "View Terminal").html("<iframe style='width:100%;height:100%' scrolling='no' frameborder='0' onload='$(this).height($(this).contents().find(\"body\").prop(\"scrollHeight\"));' src='terminal.aspx?action=receipt&id=" + iID + "'></iframe>");
    }
</script>

</asp:Content>