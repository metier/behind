﻿using System;
using System.Configuration;
using System.Xml;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Data;
using System.Data.OleDb;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using jlib.db;
using jlib.functions;
using System.Security.Cryptography;
using System.Text;
using SpreadsheetGear;
namespace Phoenix.LearningPortal.Util
{
    public class Data
    {
        public enum MedalTypes
        {
            Gold = 0,
            Silver = 1,
            Bronze = 2
        }
        public enum ResourceTypes
        {
            Instructor = 1,
            Location = 2,
            ECoach = 3,
            Partner = 4,
            Examiner = 5,
            Invigilator = 6,
            StudyAdvisor = 7
        }
        public enum MetierClassTypes
        {
            ELearning = 0,
            Classroom = 1,
            Exam = 3,
            Accommodation = 4,
            Other = 7,
            IntegratedLearning = 98,
            Unknown = 99
        }

        public enum AttemptState
        {
            NotAttempted = 0,
            Incomplete = 1,
            Completed = 2
        }
        public static void reset()
        {
            Util.Classes.user.resetCache(true);
        }
    }

}