﻿using System;
using System.Configuration;
using System.Xml;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Data;
using System.Data.OleDb;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using jlib.db;
using jlib.functions;
using System.Security.Cryptography;
using System.Text;
using SpreadsheetGear;
namespace Phoenix.LearningPortal.Util
{
    public class Import : jlib.controls.jimport.plugin
    {
        Exam.exam_section m_oSection = new Exam.exam_section();
        //Exam.exam_question[] m_oQuestions = new Exam.exam_question[0];
        private jlib.controls.jimport.index m_oImporter;
        private DataTable m_oDTValues;
        public override void attach(jlib.controls.jimport.index oImporter)
        {
            oImporter.BeforeSettingValues += new jlib.controls.jimport.OnBeforeSettingValues(oImporter_BeforeSettingValues);
            oImporter.AfterSave += new jlib.controls.jimport.OnAfterSave(oImporter_AfterSave);
            oImporter.DataValidate += new jlib.controls.jimport.OnDataValidate(oImporter_DataValidate);
            oImporter.RowValidate += new jlib.controls.jimport.OnRowValidate(oImporter_RowValidate);
            m_oImporter = oImporter;
        }

        void oImporter_RowValidate(object sender, string sTemplateID, XmlNode[] xmlColumns, DataRow drImportValues)
        {
            jlib.helpers.structures.collection oUser = m_oImporter.getDataMapping(drImportValues)["~ILA_USER"] as jlib.helpers.structures.collection;
            if (sTemplateID == "accounting-data-import")
            {
                oUser = m_oImporter.getDataMapping(drImportValues)["~d_invoice_line"] as jlib.helpers.structures.collection;
                DateTime oInvoiceDate = convert.cDate(oUser.SafeGetValue("INVOICE_DATE"), true);
                if (DateTime.Now.Subtract(oInvoiceDate).TotalSeconds < 10) oInvoiceDate = convert.cDate(oUser.SafeGetValue("INVOICE_DATE"), false);
                if (DateTime.Now.Subtract(oInvoiceDate).TotalSeconds < 10)
                {
                    drImportValues["_error"] = "Fakturadato må være en gyldig dato.";
                }
            }
            else if (sTemplateID == "accounting-invoices-import")
            {
                oUser = m_oImporter.getDataMapping(drImportValues)["~d_invoice_line"] as jlib.helpers.structures.collection;
                DateTime oInvoiceDate = convert.cDate(oUser.SafeGetValue("DATA_INVOICEABLE_DATE"), true);
                if (DateTime.Now.Subtract(oInvoiceDate).TotalSeconds < 10) oInvoiceDate = convert.cDate(oUser.SafeGetValue("DATA_INVOICEABLE_DATE"), false);
                if (DateTime.Now.Subtract(oInvoiceDate).TotalSeconds < 10)
                {
                    drImportValues["_error"] = "Invoice Date must be a valid date.";
                }
                else
                {
                    oUser["DATA_INVOICEABLE_DATE"] = oInvoiceDate;
                }
            }
            else if (sTemplateID == "exam-results")
            {
                int iDOBIndex = m_oImporter.getColIndex("d_exam_result_imports.date_of_birth");
                if (iDOBIndex > -1)
                {
                    if (drImportValues[iDOBIndex].Str() != "")
                    {
                        drImportValues[iDOBIndex] = convert.cDate(drImportValues[iDOBIndex], true);
                    }
                }
            }
        }

        void oImporter_DataValidate(object sender, string sTemplateID, XmlNode[] xmlColumns, DataTable dtImportValues, out bool bError)
        {
            bError = false;
        }

        void oImporter_BeforeSettingValues(object sender, string sTemplateID, XmlNode[] xmlColumns, DataRow drImportValues, jlib.helpers.structures.collection oDataSets)
        {
            if (sTemplateID == "question-import" || sTemplateID == "accounting-invoices-import" || sTemplateID == "accounting-data-import")
            {
                drImportValues["_imported"] = "1";

            }
            else if (sTemplateID == "exam-results")
            {

            }

        }

        void oImporter_AfterSave(object sender, string sTemplateID, XmlNode[] xmlColumns, DataRow drImportValues, jlib.helpers.structures.collection oDataSets, int iRow)
        {
            if (sTemplateID == "question-import")
            {
                jlib.helpers.structures.collection oQuestion = oDataSets["~d_questions"] as jlib.helpers.structures.collection;
                if (convert.cStr(oQuestion["section"]) != "")
                {//||convert.cStr(oQuestion["sub_section"]) != ""||convert.cStr(oQuestion["sub_sub_section"]) != "") {

                    if (m_oSection.Sections.Count == 0 || m_oSection.Sections[m_oSection.Sections.Count - 1].Name != convert.cStr(oQuestion["section"])) m_oSection.Sections.Add(new Exam.exam_section().populate("Name", oQuestion["section"]) as Exam.exam_section);
                    Exam.exam_section oCurrSection = m_oSection.Sections[m_oSection.Sections.Count - 1];
                    if (oCurrSection.Sections.Count == 0 || oCurrSection.Sections[oCurrSection.Sections.Count - 1].Name != convert.cStr(oQuestion["sub_section"])) oCurrSection.Sections.Add(new Exam.exam_section().populate("Name", oQuestion["sub_section"]) as Exam.exam_section);
                    oCurrSection = oCurrSection.Sections[oCurrSection.Sections.Count - 1];
                    if (oCurrSection.Sections.Count == 0 || oCurrSection.Sections[oCurrSection.Sections.Count - 1].Name != convert.cStr(oQuestion["sub_sub_section"])) oCurrSection.Sections.Add(new Exam.exam_section().populate("Name", oQuestion["sub_sub_section"]) as Exam.exam_section);
                    oCurrSection = oCurrSection.Sections[oCurrSection.Sections.Count - 1];
                    Exam.exam_question oCurrQuestion = new Exam.exam_question().populate("Text", oQuestion["text"], "SubText", oQuestion["subtext"], "Reason", oQuestion["reason"],
                        "Hint", oQuestion["hint"],
                        "ForeignKey", oQuestion["foreignkey"],
                        "Reference", oQuestion["reference"],
                        "Domain", oQuestion["domain"],
                        "KnowledgeArea", oQuestion["knowledgearea"],
                        "QuestionType", oQuestion["type"], "Points", oQuestion["points"]) as Exam.exam_question;
                    oCurrSection.Questions.Add(oCurrQuestion);

                    string s = "abcdefghijkl";
                    for (int z = 0; z < s.Length; z++)
                    {
                        if (convert.cStr(oQuestion["option_" + s[z]]) != "")
                        {
                            Exam.exam_question_response oResponse = new Exam.exam_question_response().populate("Text", oQuestion["option_" + s[z]], "Reason", oQuestion["reason_" + s[z]], "IsCorrect", (convert.cStr(oQuestion["answer"]).ToLower().Contains(s[z].ToString()) ? 1 : 0)) as Exam.exam_question_response;
                            if (convert.cStr(oQuestion["option_role_" + s[z]]) != "") oResponse.Settings.Add("Role", convert.cStr(oQuestion["option_role_" + s[z]]));
                            oCurrQuestion.Answers.Add(oResponse);
                        }
                    }
                    //for (int z = 1; z < 10; z++) 
                    //    if (convert.cStr(oQuestion["response_role_" + z]) != "") oCurrQuestion.Settings.Add("response_role_" + z, convert.cStr(oQuestion["response_role_" + z]));

                    if (iRow == drImportValues.Table.Rows.Count - 1) (sender as jlib.controls.jimport.index).OnRenderScript += "$.each(" + jlib.functions.json.JsonMapper.ToJson(m_oSection) + ".Sections,function(i,o){cExam.Sections.push(o);});fnLoadExam(cExam);";
                }
                else
                {

                    (sender as jlib.controls.jimport.index).OnRenderScript += parse.replaceAll("var b=[];"
                        + (convert.cStr(oQuestion["option_a"]) != "" ? "b.push({\"Reason\":\"" + parse.replaceAll(oQuestion["reason_a"], "\"", "'") + "\",\"Text\":\"" + parse.replaceAll(oQuestion["option_a"], "\"", "'") + "\", \"IsCorrect\":" + (convert.cStr(oQuestion["answer"]).ToLower().Contains("a") ? 1 : 0) + "});" : "")
                        + (convert.cStr(oQuestion["option_b"]) != "" ? "b.push({\"Reason\":\"" + parse.replaceAll(oQuestion["reason_b"], "\"", "'") + "\",\"Text\":\"" + parse.replaceAll(oQuestion["option_b"], "\"", "'") + "\", \"IsCorrect\":" + (convert.cStr(oQuestion["answer"]).ToLower().Contains("b") ? 1 : 0) + "});" : "")
                        + (convert.cStr(oQuestion["option_c"]) != "" ? "b.push({\"Reason\":\"" + parse.replaceAll(oQuestion["reason_c"], "\"", "'") + "\",\"Text\":\"" + parse.replaceAll(oQuestion["option_c"], "\"", "'") + "\", \"IsCorrect\":" + (convert.cStr(oQuestion["answer"]).ToLower().Contains("c") ? 1 : 0) + "});" : "")
                        + (convert.cStr(oQuestion["option_d"]) != "" ? "b.push({\"Reason\":\"" + parse.replaceAll(oQuestion["reason_d"], "\"", "'") + "\",\"Text\":\"" + parse.replaceAll(oQuestion["option_d"], "\"", "'") + "\", \"IsCorrect\":" + (convert.cStr(oQuestion["answer"]).ToLower().Contains("d") ? 1 : 0) + "});" : "")
                        + (convert.cStr(oQuestion["option_e"]) != "" ? "b.push({\"Reason\":\"" + parse.replaceAll(oQuestion["reason_e"], "\"", "'") + "\",\"Text\":\"" + parse.replaceAll(oQuestion["option_e"], "\"", "'") + "\", \"IsCorrect\":" + (convert.cStr(oQuestion["answer"]).ToLower().Contains("e") ? 1 : 0) + "});" : "")
                        + (convert.cStr(oQuestion["option_f"]) != "" ? "b.push({\"Reason\":\"" + parse.replaceAll(oQuestion["reason_f"], "\"", "'") + "\",\"Text\":\"" + parse.replaceAll(oQuestion["option_f"], "\"", "'") + "\", \"IsCorrect\":" + (convert.cStr(oQuestion["answer"]).ToLower().Contains("f") ? 1 : 0) + "});" : "")
                        + (convert.cStr(oQuestion["option_g"]) != "" ? "b.push({\"Reason\":\"" + parse.replaceAll(oQuestion["reason_g"], "\"", "'") + "\",\"Text\":\"" + parse.replaceAll(oQuestion["option_g"], "\"", "'") + "\", \"IsCorrect\":" + (convert.cStr(oQuestion["answer"]).ToLower().Contains("g") ? 1 : 0) + "});" : "")
                        + (convert.cStr(oQuestion["option_h"]) != "" ? "b.push({\"Reason\":\"" + parse.replaceAll(oQuestion["reason_h"], "\"", "'") + "\",\"Text\":\"" + parse.replaceAll(oQuestion["option_h"], "\"", "'") + "\", \"IsCorrect\":" + (convert.cStr(oQuestion["answer"]).ToLower().Contains("h") ? 1 : 0) + "});" : "")
                        + (convert.cStr(oQuestion["option_i"]) != "" ? "b.push({\"Reason\":\"" + parse.replaceAll(oQuestion["reason_i"], "\"", "'") + "\",\"Text\":\"" + parse.replaceAll(oQuestion["option_i"], "\"", "'") + "\", \"IsCorrect\":" + (convert.cStr(oQuestion["answer"]).ToLower().Contains("i") ? 1 : 0) + "});" : "")

                        + "oGrid.Data.push(['_' + (new Date().getTime()),\"" + parse.replaceAll(oQuestion["text"], "\"", "'", "\\n", "\n") + "\",\"" + parse.replaceAll(oQuestion["subtext"], "\"", "'", "\\n", "\n")
                        + "\",\"" + parse.replaceAll(oQuestion["reason"], "\"", "'")
                        + "\",\"" + parse.replaceAll(oQuestion["hint"], "\"", "'")
                        + "\",\"" + parse.replaceAll(oQuestion["foreignkey"], "\"", "'")
                        + "\",\"" + parse.replaceAll(oQuestion["reference"], "\"", "'")
                        + "\",\"" + parse.replaceAll(oQuestion["domain"], "\"", "'")
                        + "\",\"" + parse.replaceAll(oQuestion["knowledgearea"], "\"", "'")
                        + "\", '" + convert.cStr(oQuestion["type"]).ToLower() + "'," + convert.cInt(oQuestion["points"]) + ",b]);", "\n", "\\n");
                    (sender as jlib.controls.jimport.index).OnRenderScript = parse.replaceAll((sender as jlib.controls.jimport.index).OnRenderScript, "oGrid.raiseEvent('GridRowAfterSave',null);oGrid.render();", "") + "oGrid.raiseEvent('GridRowAfterSave',null);oGrid.render();";
                }
            }
            else if (sTemplateID == "accounting-data-import" || sTemplateID == "accounting-invoices-import")
            {
                jlib.helpers.structures.collection oLine = oDataSets["~d_invoice_line"] as jlib.helpers.structures.collection;
                if (m_oDTValues == null || iRow == 0)
                {
                    m_oDTValues = new DataTable();
                    for (int x = 0; x < oLine.Count; x++) m_oDTValues.Columns.Add(new DataColumn(oLine.Keys[x]));
                }
                m_oDTValues.Rows.Add(m_oDTValues.NewRow());
                for (int x = 0; x < oLine.Count; x++) m_oDTValues.Rows[m_oDTValues.Rows.Count - 1][oLine.Keys[x]] = oLine[x];
            }
        }
    }

}