﻿using System;
using System.Configuration;
using System.Collections.Generic;
using jlib.functions;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Util
{
    public class Nets
    {

        public class terminal
        {
            public terminal(AttackData.Payment payment)
            {
                this.Payment = payment;
                InitiateMerchantData(Payment.IsTest, Payment.OrderCurrency);
            }
            protected AttackData.Payment Payment;
            protected List<string> merchantCurrencies = new List<string>() { "Prosjektledelse.no virker ikke", "NOK", "USD", "EUR" };
            protected List<string> merchantIDs = new List<string>() { "482348", "768305", "503264", "532154" };
            protected List<string> merchantTokens = new List<string>() { "n+3K2P?w", "Mq8?t-P", "jW/7=x6H", "bY=3G4z_" };

            protected string MerchantID { get; set; }
            protected string MerchantToken { get; set; }
            protected string TerminalURL { get; set; }

            protected void InitiateMerchantData(bool test, string currency)
            {
                if (test)
                {
                    MerchantID = "482348";
                    MerchantToken = "2x!H?8Wo";
                    TerminalURL = "https://epayment-test.bbs.no/epay/default.aspx?merchantId={0}&transactionId={1}";
                }
                else
                {
                    MerchantID = merchantIDs[merchantCurrencies.IndexOf(currency)];
                    MerchantToken = merchantTokens[merchantCurrencies.IndexOf(currency)];
                    TerminalURL = "https://epayment.bbs.no/epay/default.aspx?merchantId={0}&transactionId={1}";
                }

				//Force TLS1.2
				System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
			}
            public virtual void Process()
            {
                throw new Exception("Process() needs to be overridden in sub class");
            }
            protected virtual string GetProcessExceptionMessage(Exception ex)
            {
                throw new Exception("GetProcessExceptionMessage() needs to be overridden in sub class");
            }
            protected void ProcessException(Exception ex)
            {
                string ErrorMessage = GetProcessExceptionMessage(ex);

                Payment.CCResponseText = convert.cIfValue(ErrorMessage, "", "\n", "") + ex.Message + "\n" + ex.ToString();
                Payment.Save();
                string errorMessage = (ErrorMessage.IsNullOrEmpty()
                                            ? "An error occurred attempting to process your payment. "
                                            : ErrorMessage) + "Try again by clicking <a href=\"terminal.aspx?" +
                                        Payment.QueryString + "\">here</a>.";

                System.Web.HttpContext.Current.Response.Write(ex.Message);
                Util.Email.sendEmail("metier-terminal-error@bids.no",
                                        "metier-terminal-error@bids.no;jon.arve.walberg@metier.no", "", "",
                                        "Metier SOFT Terminal Error -- Order #" + Payment.ID + " || Data: " + Payment.QueryString,
                                        ex.Message, true, null);
                System.Web.HttpContext.Current.Response.End();
            }
            public virtual void Register()
            {
                throw new Exception("Register() needs to be overridden in sub class");
            }
            protected void RegisterException(Exception ex)
            {
                System.Web.HttpContext.Current.Response.Write("An error has occurred: <br /><pre>" + ex.ToString() + "</pre>");
                if (ex as System.Threading.ThreadAbortException == null)
                {
                    Util.Email.sendEmail("metier-terminal-error@bids.no",
                                         "metier-terminal-error@bids.no;jon.arve.walberg@metier.no", "", "",
                                         "Metier Terminal Error -- Order #" + Payment.ID + " || Data: " + Payment.QueryString,
                                         "An error has occurred: <br /><pre>" + ex.ToString() + "</pre>", true, null);
                    System.Web.HttpContext.Current.Response.End();
                }
            }
        }

        public class prodterminal : terminal
        {
            public prodterminal(AttackData.Payment payment) : base(payment) { }
            public override void Process()
            {
                try
                {
                    var Client = new BBS.Prod.NetaxeptClient();
                    var AuthorizationResult = Client.Process(MerchantID, MerchantToken,
                    new BBS.Prod.ProcessRequest
                    {
                        Operation = "SALE",
                        TransactionId = Payment.TransactionID
                    });
                    Payment.CCAuthorizationID = AuthorizationResult.AuthorizationId;
                    Payment.CCExecutionTime = AuthorizationResult.ExecutionTime;
                    Payment.CCResponseCode = AuthorizationResult.ResponseCode;
                    Payment.CCResponseSource = AuthorizationResult.ResponseSource;
                    Payment.CCResponseText = AuthorizationResult.ResponseText;
                    Payment.Save();
                }
                catch (Exception ex)
                {
                    ProcessException(ex);
                }
            }
            public override void Register()
            {
                try
                {
                    var Client = new BBS.Prod.NetaxeptClient();
                    var TerminalResponse = Client.Register(MerchantID, MerchantToken, new BBS.Prod.RegisterRequest
                    {
                        Terminal = new BBS.Prod.Terminal
                        {
                            OrderDescription = Payment.OrderDescription,
                            RedirectUrl = ConfigurationManager.AppSettings["site.url"] + "/tools/terminal.aspx?success=true&order_id=" + Payment.ID,
                            Language = "en_GB",
                            AutoAuth = "false"
                        },
                        Order = new BBS.Prod.Order
                        {
                            Amount = (Payment.OrderAmount * 100).Str().SafeSplit(".", 0),
                            CurrencyCode = Payment.OrderCurrency,
                            OrderNumber = Payment.ID.Str()
                        },
                        Environment = new BBS.Prod.Environment
                        {
                            WebServicePlatform = "WCF"
                        }
                    });
                    Payment.TransactionID = TerminalResponse.TransactionId;
                    //Session["terminal.transaction-id"] = TerminalResponse.TransactionId;
                    Payment.Save();
                    System.Web.HttpContext.Current.Response.Redirect(String.Format(TerminalURL, MerchantID, TerminalResponse.TransactionId));
                    System.Web.HttpContext.Current.Response.End();
                }
                catch (Exception ex)
                {
                    RegisterException(ex);
                }

            }
            protected override string GetProcessExceptionMessage(Exception ex)
            {
                System.ServiceModel.FaultException<BBS.Prod.BBSException> Exception =
                    (ex as System.ServiceModel.FaultException<BBS.Prod.BBSException>);
                if (Exception != null && Exception.Detail != null && Exception.Detail.Result != null)
                {
                    if (Exception.Detail.Result.ResponseCode == "99")
                        return "Your bank reports your account has insufficient funds. ";
                    else
                        return Exception.Detail.Result.ResponseText + ". ";
                }
                return "";
            }
        }
        public class devterminal : terminal
        {
            public devterminal(AttackData.Payment payment) : base(payment) { }
            public override void Process()
            {
                try
                {
                    var Client = new BBS.Dev.NetaxeptClient();
                    var AuthorizationResult = Client.Process(MerchantID, MerchantToken,
                    new BBS.Dev.ProcessRequest
                    {
                        Operation = "SALE",
                        TransactionId = Payment.TransactionID
                    });
                    Payment.CCAuthorizationID = AuthorizationResult.AuthorizationId;
                    Payment.CCExecutionTime = AuthorizationResult.ExecutionTime;
                    Payment.CCResponseCode = AuthorizationResult.ResponseCode;
                    Payment.CCResponseSource = AuthorizationResult.ResponseSource;
                    Payment.CCResponseText = AuthorizationResult.ResponseText;
                    Payment.Save();
                }
                catch (Exception ex)
                {
                    ProcessException(ex);
                }
            }
            public override void Register()
            {
                try
                {
                    var Client = new BBS.Dev.NetaxeptClient();
                    var TerminalResponse = Client.Register(MerchantID, MerchantToken, new BBS.Dev.RegisterRequest
                    {
                        Terminal = new BBS.Dev.Terminal
                        {
                            OrderDescription = Payment.OrderDescription,
                            RedirectUrl = ConfigurationManager.AppSettings["site.url"] + "/tools/terminal.aspx?success=true&order_id=" + Payment.ID,
                            Language = "en_GB",
                            AutoAuth = "false"
                        },
                        Order = new BBS.Dev.Order
                        {
                            Amount = (Payment.OrderAmount * 100).Str().SafeSplit(".", 0),
                            CurrencyCode = Payment.OrderCurrency,
                            OrderNumber = Payment.ID.Str()
                        },
                        Environment = new BBS.Dev.Environment
                        {
                            WebServicePlatform = "WCF"
                        }
                    });
                    Payment.TransactionID = TerminalResponse.TransactionId;
                    Payment.Save();
                    System.Web.HttpContext.Current.Response.Redirect(String.Format(TerminalURL, MerchantID, TerminalResponse.TransactionId));
                    System.Web.HttpContext.Current.Response.End();
                }
                catch (Exception ex)
                {
                    RegisterException(ex);
                }

            }
            protected override string GetProcessExceptionMessage(Exception ex)
            {
                System.ServiceModel.FaultException<BBS.Dev.BBSException> Exception =
                    (ex as System.ServiceModel.FaultException<BBS.Dev.BBSException>);
                if (Exception != null && Exception.Detail != null && Exception.Detail.Result != null)
                {
                    if (Exception.Detail.Result.ResponseCode == "99")
                        return "Your bank reports your account has insufficient funds. ";
                    else
                        return Exception.Detail.Result.ResponseText + ". ";
                }
                return "";
            }
        }
    }

}