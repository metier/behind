﻿using System;
using System.Configuration;
using System.Xml;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Data;
using System.Text;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using jlib.db;
using jlib.functions;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

using SpreadsheetGear;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Util
{
    public class MyMetier
    {
        public static string MetierDate(string language, DateTimeOffset? date)
        {
            if (date == null) return "";
            return Util.MyMetier.getUserCulture(language).DateTimeFormat.DayNames[(int)date.Value.LocalDateTime.DayOfWeek].Substring(0, 3).ToCamelCase() + " " + date.Value.LocalDateTime.Day + " " + Util.MyMetier.getUserCulture(language).DateTimeFormat.MonthNames[(int)date.Value.LocalDateTime.Month - 1].Substring(0, 3).ToLower();
        }
        public static int CalculateGoldScore(int numLessonsWithQuizes)
        {
            if (numLessonsWithQuizes > 10) return (numLessonsWithQuizes + 3) * 100;
            else return (numLessonsWithQuizes + CalculateFinalTestWeight(numLessonsWithQuizes)) * 90;
        }
        public static int CalculateSilverScore(int numLessonsWithQuizes)
        {
            if (numLessonsWithQuizes > 10) return (numLessonsWithQuizes) * 100;
            else return (numLessonsWithQuizes + CalculateFinalTestWeight(numLessonsWithQuizes)) * 80;
        }
        public static int CalculateUnlockTest(int numLessonsWithQuizes)
        {
            if (numLessonsWithQuizes > 10) return (numLessonsWithQuizes - 5) * 100;
            else return numLessonsWithQuizes * 75;
        }
        public static int CalculateFinalTestWeight(int numLessonsWithQuizes)
        {
            if (numLessonsWithQuizes > 10) return 6;
            return convert.cInt(numLessonsWithQuizes / 3.3333, 1);
        }
        private static List<string> m_oPrince2SpecialHandlingCodes = new List<string>() { "PRINCE2", "EXPM22", "EXPM23", "TEPM22", "TEPM23", "TPM22", "TPM23", "TPM24", "TPM25", "TPM26" };
        public static bool IsPrince2SpecialHandling(object code)
        {
            lock (m_oPrince2SpecialHandlingCodes) return m_oPrince2SpecialHandlingCodes.Contains(code.Str());
        }
        public static string Prince2SpecialHandlingSQLValues()
        {
            lock (m_oPrince2SpecialHandlingCodes) return "'" + m_oPrince2SpecialHandlingCodes.Join("','") + "'";
        }
        public static int GetDictionaryForCourse(string Code, int MasterCompanyId)
        {
            int DictionaryID = 0;
            AttackData.Folder DictionaryFolder = AttackData.Folder.LoadByPk(91698);
            if (DictionaryFolder != null)
            {
                List<AttackData.Folder> Dictionaries = DictionaryFolder.GetChildren<AttackData.Folder>();
                IEnumerable<AttackData.Folder> Filtered = null;
                if (!Code.IsNullOrEmpty())
                {

                    //ie. PM-0203NO
                    string rootCode = Code.SafeSplit("-", 0) + "-" + Code.SafeSplit("-", 1);
                    //ie. BA-STATSBYGG
                    string customerCode = parse.stripLeadingCharacter(Code.Replace(rootCode, ""), "-");

                    //ie. PM-02??EN
                    string regexCode = rootCode.SafeSplit("-", 0) + "-" + rootCode.SafeSplit("-", 1).Substr(0, 2) + "\\d{2}" + rootCode.SafeSplit("-", 1).Substr(-2);

                    //Get dictionary with exact course code match
                    Filtered = FilterBasedOnCompanyID(Dictionaries.Where(x => Code.Equals(x.Name, StringComparison.CurrentCultureIgnoreCase) || x.Name.StartsWith(Code + "+", StringComparison.CurrentCultureIgnoreCase)), false, MasterCompanyId);
                    if (Filtered.Count() == 0)
                        Filtered = FilterBasedOnCompanyID(Dictionaries.Where(x => Code.StartsWith(x.Name, StringComparison.CurrentCultureIgnoreCase) || x.Name.StartsWith(Code + "+", StringComparison.CurrentCultureIgnoreCase)), false, MasterCompanyId);

                    //If not found and this course is customer tailored (ie. PM-0203EN-SKANSKA), look for course that matches the standard course code (PM-0203EN)
                    if (Filtered.Count() == 0 && Code.Split('-').Length > 2)
                    {
                        Filtered = FilterBasedOnCompanyID(Dictionaries.Where(x => rootCode.StartsWith(x.Name, StringComparison.CurrentCultureIgnoreCase) || x.Name.StartsWith(rootCode + "+", StringComparison.CurrentCultureIgnoreCase)), false, MasterCompanyId);
                    }

                    //if no match and customer tailored, get all courses where suffix can be matched to a dictionary (ie. match -SIEMENS to %SIEMENS%)
                    if (Filtered.Count() == 0 && Code.Split('-').Length > 2)
                        Filtered = FilterBasedOnCompanyID(Dictionaries.Where(x => Code.SafeSplit("-", 2).ContainsI(x.Name)), false, MasterCompanyId);

                    //If no, match by CompanyID only
                    if (Filtered.Count() == 0)
                    {
                        Filtered = FilterBasedOnCompanyID(Dictionaries, true, MasterCompanyId);
                    }

                    //If not found, look for course that matches the standard course code (PM-02??EN)
                    if (Filtered.Count() == 0)
                    {
                        //If customer tailored, match based on "PM-02??NO-CUSTOMER"
                        if (!customerCode.IsNullOrEmpty()) Filtered = Dictionaries.Where(x => x.Name.EndsWith(customerCode, StringComparison.CurrentCultureIgnoreCase) && Regex.Match(x.Name, regexCode, RegexOptions.Singleline).Success);

                        //If not customer tailored, or no match previously, try "PM-02??NO"
                        if (Filtered.Count() == 0) Filtered = Dictionaries.Where(x => x.Name.Split('-').Length == 2 && Regex.Match(x.Name, regexCode, RegexOptions.Singleline).Success);
                    }
                    if (Filtered.Count() > 0) DictionaryID = Filtered.ElementAt(0).ObjectID;

                }

            }
            return DictionaryID;
        }
        private static IEnumerable<AttackData.Folder> FilterBasedOnCompanyID(IEnumerable<AttackData.Folder> Filtered, bool OnlyDictionariesWithCompanyID, int MasterCompanyId)
        {
            return Filtered.Where(x => ((OnlyDictionariesWithCompanyID ? (!x.Name.Str().StartsWith("PM-") && !x.Name.Str().StartsWith("M-") && !x.Name.Str().StartsWith("U-") && x.Name.Str().Contains("+")) : true) && (!x.Name.Str().Contains("+") || x.Name.Str().EndsWith("+" + MasterCompanyId) || x.Name.Str().Contains("+" + MasterCompanyId + "+"))));
        }
        public static IEnumerable<KeyValuePair<string, string>> GetSplashImages(string code, int masterCompanyId, int companyId, int lessonIndex, List<string> includedFileNames, List<string> excludedFileNames)
        {

            List<KeyValuePair<string, string>> SplashImages = AttackData.Folder.LoadByPk(18781).GetFiles(false);
            IEnumerable<KeyValuePair<string, string>> SplashImagesFiltered = SplashImages.Where(x => (x.Value.Str().ContainsI("+" + masterCompanyId + "/") || x.Value.Str().ContainsI("+" + masterCompanyId + "+") || x.Value.Str().ContainsI("+" + companyId + "/") || x.Value.Str().ContainsI("+" + companyId + "+")));
            if (includedFileNames == null) includedFileNames = new List<string>();
            if (excludedFileNames == null) excludedFileNames = new List<string>();

            //Filter out unwanted matches
            includedFileNames.ForEach(x => { SplashImagesFiltered = SplashImagesFiltered.Where(y => y.Value.Str().ContainsI(x)); });
            excludedFileNames.ForEach(x => { SplashImagesFiltered = SplashImagesFiltered.Where(y => !y.Value.Str().ContainsI(x)); });

            if (!code.IsNullOrEmpty() && SplashImagesFiltered.Where(x => x.Value.Str().ContainsI(code)).Count() > 0) SplashImagesFiltered = SplashImagesFiltered.Where(x => x.Value.Str().ContainsI(code));

            if (SplashImagesFiltered.Where(x => x.Value.Str().ContainsI("/" + lessonIndex + ".jpg") || x.Value.Str().ContainsI("/" + lessonIndex + ".png")).Count() > 0) SplashImagesFiltered = SplashImagesFiltered.Where(x => x.Value.Str().ContainsI("/" + lessonIndex + ".jpg") || x.Value.Str().ContainsI("/" + lessonIndex + ".png"));
            if (SplashImagesFiltered.Count() == 0)
            {
                if (code != "")
                {
                    SplashImagesFiltered = SplashImages.Where(x => x.Value.Str().ContainsI(code + "/") && !x.Value.Str().Contains("+"));

                    //Try a reverse match, where the folder name gets matched against the course code
                    if (SplashImagesFiltered.Count() == 0)
                        SplashImagesFiltered = SplashImages.Where(x => !string.IsNullOrWhiteSpace(x.Value) && code.ContainsI(x.Value.Split('/')[x.Value.Split('/').Length - 2]) && !x.Value.Str().Contains("+"));

                    //No match on full course version matching. Try matching company-part, if version string contains company name (ie. 'SIEMENS/')
                    if (SplashImagesFiltered.Count() == 0 && code.Split('-').Length > 2)
                        SplashImagesFiltered = SplashImages.Where(x => x.Value.Str().ContainsI(code.SafeSplit("-", 2) + "/") && !x.Value.Str().Contains("+"));
                }
                if (SplashImagesFiltered.Count() == 0)
                {
                    if (code.Split('-').Length > 2) code = code.SafeSplit("-", 0) + "-" + code.SafeSplit("-", 1);
                    if (!code.IsNullOrEmpty()) SplashImagesFiltered = SplashImages.Where(x => x.Value.Str().ContainsI(code + "/") && !x.Value.Str().Contains("+"));
                }

                //Filter out unwanted matches
                includedFileNames.ForEach(x => { SplashImagesFiltered = SplashImagesFiltered.Where(y => y.Value.Str().ContainsI(x)); });
                excludedFileNames.ForEach(x => { SplashImagesFiltered = SplashImagesFiltered.Where(y => !y.Value.Str().ContainsI(x)); });
                if (SplashImagesFiltered.Where(x => x.Value.Str().ContainsI("/" + lessonIndex + ".jpg") || x.Value.Str().ContainsI("/" + lessonIndex + ".png")).Count() > 0) SplashImagesFiltered = SplashImagesFiltered.Where(x => x.Value.Str().ContainsI("/" + lessonIndex + ".jpg") || x.Value.Str().ContainsI("/" + lessonIndex + ".png"));
            }
            return SplashImagesFiltered;
        }

        public static dynamic GetLessonData(List<Util.Classes.iLesson> lessons, int iCmsCourseID, string Language)
        {
            dynamic JSON = new jlib.functions.json.DynamicJson();
            if (lessons.Count > 0)
            {
                List<object> Lessons = new List<object>();
                for (int x = 0; x < lessons.Count; x++)
                {
                    Lessons.Add(new { RCOID = lessons[x].Id, Url = lessons[x].StartingUrl, Title = (iCmsCourseID == lessons[x].ParentId ? "" : "&nbsp;&nbsp;") + lessons[x].Name, Score = Math.Max(0, lessons[x].UserNumPoints), Weight = lessons[x].Weight, Status = lessons[x].UserLessonStatus, Type = lessons[x].IsFinalTest ? "finaltest" : "lesson", CompletedDate = (lessons[x].UserLessonCompletedDate.HasValue ? String.Format("{0:dd} " + Util.MyMetier.getUserCulture(Language).DateTimeFormat.MonthNames[(int)lessons[x].UserLessonCompletedDate.Value.Month - 1] + " {0:yyyy}", lessons[x].UserLessonCompletedDate) : null), NumSubLessons = lessons[x].NumSubLessons });
                }
                JSON.Lessons = Lessons;
            }
            else
            {
                JSON.Lessons = new List<object>();
            }
            if (iCmsCourseID > 0) JSON.CourseID = iCmsCourseID;
            return JSON;
        }
        public static void LogPageView(jlib.components.webpage Page, string PageTitle)
        {
            sqlbuilder.executeInsert(new ado_helper("sql.dsn.cms"), "d_page_views", "user_id", Page.Request["user_id"].Lng(), "ip", Page.IP, "referrer", Page.Referrer, "item_id", Page.Request.QueryString["id"].Int(), "post_data", (ConfigurationManager.AppSettings["page.logging.mode"] == "full" ? convert.cStreamToString(Page.Request.InputStream) : ""), "url", Page.Request["url"], "session_id", 0, "is_new", Page.Session.IsNewSession, "page_title", PageTitle, "user_agent", Page.UserAgent);
        }

        public static int GetPDFFooterHeight(string copyRightMessage)
        {
            if (copyRightMessage.IsNullOrEmpty())
            {
                return 25;
            }
            else
            {
                return 25 + Math.Max(0, 10 * ((copyRightMessage.Length / 125).Int() - 1));
            }
        }
        public static void AddWatermark(EvoPdf.Document Doc, string WatermarkUrl)
        {
            var watermarkElement = new EvoPdf.HtmlToPdfElement(System.Configuration.ConfigurationManager.AppSettings["site.url"] + WatermarkUrl);
            watermarkElement.Blending = EvoPdf.Blending.Darken;
            foreach (EvoPdf.PdfPage page in Doc.Pages)
                page.AddElement(watermarkElement);

        }
        public static void PDFAddFooter(EvoPdf.Document doc, bool skipFirstPage, string copyRightMessage, bool addLine, bool addNumbering)
        {
            int copyRightHeight = GetPDFFooterHeight(copyRightMessage);
            if (copyRightMessage.IsNullOrEmpty())
            {
                copyRightMessage = "Copyright {0:yyyy} - Metier OEC AS";
            }
            int numPages = doc.Pages.Count;
            for (int pageNum = (skipFirstPage ? 1 : 0); pageNum < numPages; pageNum++)
            {
                EvoPdf.PdfPage page = doc.Pages[pageNum];
                float vAlign = page.PageSize.Height - page.Margins.Top - copyRightHeight;

                if (copyRightMessage.IsNotNullOrEmpty())
                {
                    EvoPdf.HtmlToPdfElement footerText = new EvoPdf.HtmlToPdfElement(80, vAlign - 3, 400, "<style>a, div{font: 14pt arial;color:rgba(36, 53, 67, 0.5);padding:0;margin:0}</style><div style='text-align: center;'>" + String.Format(copyRightMessage, DateTime.Now) + "</div>", "");
                    page.AddElement(footerText);
                    //EvoPdf.TextElement footerText = new EvoPdf.TextElement(80, vAlign, 400, String.Format(copyRightMessage, DateTime.Now),                        
                    //page.Document.AddFont(new System.Drawing.Font(new System.Drawing.FontFamily("Arial"), 7, System.Drawing.GraphicsUnit.Point)), new EvoPdf.PdfColor(Color.FromArgb(50, 36, 53, 67)));
                    //footerText.EmbedSysFont = true;
                    //footerText.TextAlign = EvoPdf.HorizontalTextAlign.Center;
                    //page.AddElement(footerText);
                }

                if (addLine)
                {
                    EvoPdf.LineElement line = new EvoPdf.LineElement(40, vAlign - 5, page.PageSize.Width - 40, vAlign - 5);
                    line.LineStyle = new EvoPdf.LineStyle(1);
                    line.BackColor = new EvoPdf.PdfColor(Color.Black);
                    page.AddElement(line);
                }

                if (addNumbering)
                {
                    EvoPdf.TextElement footerText = new EvoPdf.TextElement(0, vAlign, "Page " + (pageNum + 1) + " of " + numPages + "                     ",
                    page.Document.AddFont(new System.Drawing.Font(new System.Drawing.FontFamily("Arial"), 7, System.Drawing.GraphicsUnit.Point)), new EvoPdf.PdfColor(Color.FromArgb(50, 36, 53, 67)));
                    footerText.EmbedSysFont = true;
                    footerText.TextAlign = EvoPdf.HorizontalTextAlign.Right;
                    page.AddElement(footerText);
                }
            }
        }
        public class PrintPage
        {
            private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            public PrintPage(string Url, string PostData, string Template, int FooterHeight, string HttpVerb = "POST", string ContentType = null, bool AutoTOC = false)
            {
                this.Url = Url;
                this.PostData = PostData;
                this.Template = Template;
                this.FooterHeight = FooterHeight;
                this.HttpVerb = HttpVerb;
                this.ContentType = ContentType;
                this.AutoTOC = AutoTOC;
            }
            public bool Completed { get; set; }
            public string Url { get; set; }
            public string Template { get; set; }
            public string PostData { get; set; }
            public int FooterHeight { get; set; }
            public string HTML { get; private set; }
            public string HttpVerb { get; private set; }
            public string ContentType { get; private set; }
            public bool AutoTOC { get; set; }


            public EvoPdf.Document Document { get; private set; }
            public System.Threading.Thread Thread { get; set; }
            private static object downloadLockObject = new object();
            public void work()
            {
                var printLog = "";
                var startTime = DateTime.Now;
                try
                {

                    jlib.net.HTTP HTTP = new jlib.net.HTTP();
                    //HTTP.LogMode = jlib.net.HTTP.log_mode.text;                    
                    //HTTP.Debug = true;
                    //HTTP.DelayBetweenRequests = 20;
                    lock (downloadLockObject)
                    {
                        if (this.ContentType.IsNotNullOrEmpty())
                            HTTP.ContentTypeRequest = this.ContentType;

                        if (this.HttpVerb == "GET")
                            HTML = parse.stripWhiteChars(HTTP.Get(Url));
                        else
                            HTML = parse.stripWhiteChars(HTTP.Post(Url, PostData));
                    }

                    printLog += "\n * " + Url + ": Download took: " + (DateTime.Now.Subtract(startTime).TotalMilliseconds) + " ms";
                    startTime = DateTime.Now;

                    var converter = new EvoPdf.PdfConverter();
                    converter.HtmlElementsMappingOptions.HtmlElementSelectors = new string[] { "body" };
                    converter.PdfDocumentOptions.PdfPageSize = EvoPdf.PdfPageSize.A4;
                    //converter.PdfDocumentOptions.PdfCompressionLevel = EvoPdf.PdfCompressionLevel.Best;
                    //converter.PdfDocumentOptions.JpegCompressionEnabled = true;
                    log.Info("Template: " + Template);
                    if (Template == "goals" || Template == "checklist")
                    {
                    }
                    else
                    {
                        converter.PdfDocumentOptions.LeftMargin = 0;
                        converter.PdfDocumentOptions.RightMargin = 0;
                        converter.PdfDocumentOptions.TopMargin = 30;
                        converter.PdfDocumentOptions.BottomMargin = this.FooterHeight + 5;
                    }
                    converter.PdfDocumentOptions.InternalLinksEnabled = true;
                    converter.PdfDocumentOptions.AvoidImageBreak = true;
                    converter.PdfDocumentOptions.AvoidTextBreak = true;
                    converter.PdfDocumentOptions.ShowFooter = false;
                    //converter.JavaScriptEnabled = true; //used to be commented out
                    log.Info("URL : " + this.Url);
                    if (this.Url.IndexOf("portal2") > -1)
                    {
                        converter.TriggeringMode = EvoPdf.TriggeringMode.Manual;
                    }
                    else
                    {
                        converter.ConversionDelay = 1; //1
                    }
                    converter.PdfDocumentOptions.ImagesScalingEnabled = true; //true

                    if (this.AutoTOC)
                    {
                        converter.TableOfContentsOptions.AutoTocItemsEnabled = false;

                        converter.TableOfContentsOptions.Title = "Table of Contents";
                        converter.TableOfContentsOptions.GlobalStyle = "body{padding:130px 100px;font-family:Calibri, \"Helvetica Neue\", Helvetica, Arial, sans-serif;color:black;}";
                        converter.TableOfContentsOptions.TitleStyle = "font-size: 38px;      font-weight: bold;      color: black;";
                        string level1TextStyle = "font-size: 28px;      color: black";
                        converter.TableOfContentsOptions.SetItemStyle(1, level1TextStyle);

                        converter.TableOfContentsOptions.SetPageNumberStyle(1, level1TextStyle + ";padding-right:100px;");

                    }
                   
                    //oConverter.InterruptSlowJavaScript = true;					
                    Document = converter.GetPdfDocumentObjectFromHtmlString(HTML, Url);

                    printLog += "\n * " + Url + ": Conversion took: " + (DateTime.Now.Subtract(startTime).TotalMilliseconds) + " ms";
                    startTime = DateTime.Now;

                    //Remove empty pages where a newline at the end of a page has caused an extra page to be added
                    for (int x = converter.HtmlElementsMappingOptions.HtmlElementsMappingResult[0].PdfRectangles.Length - 1; x >= 0; x--)
                        if (converter.HtmlElementsMappingOptions.HtmlElementsMappingResult[0].PdfRectangles[x].Rectangle.Height < (Template == "theory" || Template == "examples" || Template == "pitfalls" || Template == "intro" ? 15 : 45))
                            Document.RemovePage(converter.HtmlElementsMappingOptions.HtmlElementsMappingResult[0].PdfRectangles[x].PageIndex);

                    foreach (EvoPdf.PdfPage page in Document.Pages) page.Margins.Bottom = 0;
                }
                catch (Exception e)
                {
                    log.Error("Error : " + e.Message + " " + e.StackTrace);

                    if (System.Configuration.ConfigurationManager.AppSettings["player2.error.log"].IsNotNullOrEmpty())
                        io.write_file(System.Configuration.ConfigurationManager.AppSettings["player2.error.log"], DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString() + ": PDF Conversion Error: " + e.Message + "\n" + e.StackTrace + "\n\n ---------- \n\n", true);
                }

                printLog += "\n * " + Url + ": Wrap-up took: " + (DateTime.Now.Subtract(startTime).TotalMilliseconds) + " ms";
                startTime = DateTime.Now;
                log.Info(printLog);

                //io.write_file("C:\\proj\\Clients\\metier\\Phoenix\\lp\\pdf-print-log-admin.txt", printLog, true);

                Completed = true;
            }
        }


        public class PrintPageHiQ
        {
            public PrintPageHiQ(string Url, string PostData, string Template, int FooterHeight, string HttpVerb = "POST", string ContentType = null, bool AutoTOC = false)
            {
                this.Url = Url;
                this.PostData = PostData;
                this.Template = Template;
                this.FooterHeight = FooterHeight;
                this.HttpVerb = HttpVerb;
                this.ContentType = ContentType;
                this.AutoTOC = AutoTOC;
            }
            public bool Completed { get; set; }
            public string Url { get; set; }
            public string Template { get; set; }
            public string PostData { get; set; }
            public int FooterHeight { get; set; }
            public string HTML { get; private set; }
            public string HttpVerb { get; private set; }
            public string ContentType { get; private set; }
            public bool AutoTOC { get; set; }


            public EvoPdf.Document Document { get; private set; }
            public System.Threading.Thread Thread { get; set; }
            private static object downloadLockObject = new object();
            public void work()
            {
                var printLog = "";
                var startTime = DateTime.Now;
                try
                {

                    jlib.net.HTTP HTTP = new jlib.net.HTTP();
                    lock (downloadLockObject)
                    {
                        if (this.ContentType.IsNotNullOrEmpty())
                            HTTP.ContentTypeRequest = this.ContentType;

                        if (this.HttpVerb == "GET")
                            HTML = parse.stripWhiteChars(HTTP.Get(Url));
                        else
                            HTML = parse.stripWhiteChars(HTTP.Post(Url, PostData));
                    }

                    printLog += "\n * " + Url + ": Download took: " + (DateTime.Now.Subtract(startTime).TotalMilliseconds) + " ms";
                    startTime = DateTime.Now;

                    //var converter = new HiQPdf.HtmlToPdf();
                    //converter.Document.PageSize = HiQPdf.PdfPageSize.A4;
                    //converter.Document.PdfCompressionLevel = EvoPdf.PdfCompressionLevel.Best;
                    //converter.Document.JpegCompressionEnabled = true;

                    //if (Template == "goals" || Template == "checklist")
                    //{
                    //}
                    //else
                    //{
                    //	converter.Document.LeftMargin = 0;
                    //	converter.Document.RightMargin = 0;
                    //	converter.Document.TopMargin = 30;
                    //	converter.Document.BottomMargin = this.FooterHeight + 5;
                    //}
                    //converter.Document.InternalLinksEnabled = true;
                    //converter.Document.AvoidImageBreak = true;
                    //converter.Document.AvoidTextBreak = true;
                    //converter.Document.ShowFooter = false;
                    //converter.JavaScriptEnabled = true;
                    //converter.ConversionDelay = 1; //1
                    //converter.Document.ImagesScalingEnabled = true;

                    //if (this.AutoTOC)
                    //{
                    //	//converter.TableOfContentsOptions.AutoTocItemsEnabled = false;

                    //	//// Optionally set the table of contents title
                    //	//converter.TableOfContentsOptions.Title = "Table of Contents";
                    //	//converter.TableOfContentsOptions.GlobalStyle = "body{padding:130px 100px;font-family:Calibri, \"Helvetica Neue\", Helvetica, Arial, sans-serif;color:black;}";

                    //	//converter.TableOfContentsOptions.TitleStyle = "font-size: 38px;      font-weight: bold;      color: black;";

                    //	//// Optionally set the title style using CSS sttributes
                    //	////converter.TableOfContentsOptions.TitleStyle = "color:navy; font-family:'Times New Roman'; font-size:28px; font-weight:normal";

                    //	//// Optionally set the style of level 1 items in table of contents
                    //	//string level1TextStyle = "font-size: 28px;      color: black";
                    //	//converter.TableOfContentsOptions.SetItemStyle(1, level1TextStyle);

                    //	//converter.TableOfContentsOptions.SetPageNumberStyle(1, level1TextStyle + ";padding-right:100px;");

                    //}
                    ////oConverter.InterruptSlowJavaScript = true;
                    ////oConverter.PdfDocumentOptions.sc
                    //Document = converter.GetPdfDocumentObjectFromHtmlString(HTML, Url);

                    //printLog += "\n * " + Url + ": Conversion took: " + (DateTime.Now.Subtract(startTime).TotalMilliseconds) + " ms";
                    //startTime = DateTime.Now;

                    ////Remove empty pages where a newline at the end of a page has caused an extra page to be added
                    //for (int x = converter.HtmlElementsMappingOptions.HtmlElementsMappingResult[0].PdfRectangles.Length - 1; x >= 0; x--)
                    //	if (converter.HtmlElementsMappingOptions.HtmlElementsMappingResult[0].PdfRectangles[x].Rectangle.Height < (Template == "theory" || Template == "examples" || Template == "pitfalls" || Template == "intro" ? 15 : 45))
                    //		Document.RemovePage(converter.HtmlElementsMappingOptions.HtmlElementsMappingResult[0].PdfRectangles[x].PageIndex);

                    foreach (EvoPdf.PdfPage page in Document.Pages) page.Margins.Bottom = 0;

                }
                catch (Exception e)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["player2.error.log"].IsNotNullOrEmpty())
                        io.write_file(System.Configuration.ConfigurationManager.AppSettings["player2.error.log"], DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString() + ": PDF Conversion Error: " + e.Message + "\n" + e.StackTrace + "\n\n ---------- \n\n", true);
                }

                printLog += "\n * " + Url + ": Wrap-up took: " + (DateTime.Now.Subtract(startTime).TotalMilliseconds) + " ms";
                startTime = DateTime.Now;
                io.write_file("D:\\proj\\Clients\\metier\\Phoenix\\lp\\pdf-print-log-admin.txt", printLog, true);

                Completed = true;
            }
        }
        public static string MapMyMetierPath(string virtualPath)
        {
            return System.Web.HttpContext.Current.Request.ApplicationPath + virtualPath;
        }
        public static Dictionary<string, string> GetOrgSettings(int masterCompanyId, int companyID)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            List<KeyValuePair<string, AttackData.Folder>> orgSettings = AttackData.Folder.LoadByPk(111286).GetFolders(false);
            if (companyID > 0)
            {
                orgSettings.Where(x => (x.Key.Str().Contains("+" + companyID + "/") || x.Key.Str().Contains("+" + companyID + "+") || x.Key.Str().EndsWith("+" + companyID))).ToList().ForEach(x => {
                    x.Value.Settings.ForEach(setting => {
                        values.AddIfNotExist(setting.KeyName, setting.Value);
                    });
                });
            }
            if (masterCompanyId > 0 && masterCompanyId != companyID)
            {
                orgSettings.Where(x => (x.Key.Str().Contains("+" + masterCompanyId + "/") || x.Key.Str().Contains("+" + masterCompanyId + "+") || x.Key.Str().EndsWith("+" + masterCompanyId))).ToList().ForEach(x => {
                    x.Value.Settings.ForEach(setting => {
                        values.AddIfNotExist(setting.KeyName, setting.Value);
                    });
                });
            }

            return values;
        }
        private static string _getCourseImage(int iMasterCompanyID, string sArticleNumber)
        {
            string[] sFiles = new string[0];
            sFiles = System.IO.Directory.GetDirectories(System.Configuration.ConfigurationManager.AppSettings["cms.upload.path"] + "/courseimages/", "*+" + iMasterCompanyID, System.IO.SearchOption.TopDirectoryOnly);
            if (sFiles.Length > 0) sFiles = System.IO.Directory.GetFiles(sFiles[0], "*" + sArticleNumber + "+*");
            if (sFiles.Length == 0) sFiles = System.IO.Directory.GetFiles(System.Configuration.ConfigurationManager.AppSettings["cms.upload.path"] + "/courseimages/", "*" + sArticleNumber + "+*");
            if (sFiles.Length > 0) return "/images/" + parse.replaceAll(sFiles[0], parse.stripEndingCharacter(System.Configuration.ConfigurationManager.AppSettings["cms.upload.path"], "\\"), "", "\\", "/");
            return "";
        }

        public static string getCourseImage(int iMasterCompanyID, Util.Classes.ActivityBase activity)
        {
            string image = activity.ProductDescriptionObject.ImageUrl;
            if (!image.IsNullOrEmpty()) return image;
            string version = activity.VersionNumber;
            if (activity.ArticleType == Util.Data.MetierClassTypes.ELearning) version = convert.cStr(AttackData.Folder.LoadByPk(activity.RcoCourseId).SafeGet().AuxField1, version);
            return getCourseImage(iMasterCompanyID, activity.ArticleNumber, version, activity.ArticleType);
        }
        public static string getCourseImage(int iMasterCompanyID, string sArticleNumber, string sRCOVersion, Util.Data.MetierClassTypes oType)
        {
            string sFileName = "";
            if (sArticleNumber == "" && oType == Util.Data.MetierClassTypes.Exam) sArticleNumber = "EXAM";
            if (sArticleNumber == "" && oType == Util.Data.MetierClassTypes.Classroom) sArticleNumber = "CLASSROOM";
            if (sArticleNumber == "") sArticleNumber = sRCOVersion;
            if (sArticleNumber.EndsWith("-")) sArticleNumber = sArticleNumber.Substr(0, -1);
            sArticleNumber = parse.stripEndingCharacter(sArticleNumber, "C", "E");
            if (sArticleNumber != "")
            {
                if (sRCOVersion.Split('-').Length == 3)
                {
                    if (sFileName == "" && sArticleNumber.StartsWith("EX")) sFileName = _getCourseImage(iMasterCompanyID, "exam-" + sRCOVersion.Split('-')[2]);
                    if (sFileName == "") sFileName = _getCourseImage(iMasterCompanyID, sArticleNumber + "-" + sRCOVersion.Split('-')[2]);
                }
                if (sFileName == "" && sArticleNumber.StartsWith("EX")) sFileName = _getCourseImage(iMasterCompanyID, "exam");
                if (sFileName == "" && !sRCOVersion.IsNullOrEmpty()) sFileName = _getCourseImage(iMasterCompanyID, sRCOVersion);
                if (sFileName == "") sFileName = _getCourseImage(iMasterCompanyID, sArticleNumber);
            }
            if (sFileName == "") sFileName = _getCourseImage(iMasterCompanyID, "unavailable");
            return MapMyMetierPath(convert.cStr(sFileName, "/gfx/thumbnail_source.jpg"));
        }

        public static System.Globalization.CultureInfo getUserCulture(string sUserLanguage)
        {
            try
            {
                return System.Globalization.CultureInfo.GetCultureInfo(sUserLanguage);
            }
            catch (Exception) { }
            return System.Globalization.CultureInfo.CurrentCulture;
        }
        public static int? getLanguageID(string sLanguageCode)
        {
            string id = LanguageMapping.FindKeysByValue(sLanguageCode).FirstOrDefault(x => x.Int() > 0);
            if (id.Int() > 0) return id.Int();
            return null;
        }
        public static string getLanguageCode(int iIlearnLanguageID)
        {
            //101==N/A
            return LanguageMapping.SafeGetValue(iIlearnLanguageID.Str());
        }

        public static string getUserLanguage(string sUserLanguage)
        {
            return getUserLanguage(sUserLanguage, 0);
        }

        private static Dictionary<string, string> m_oLanguageMapping = new Dictionary<string, string>() {
            {"nn","no"},
            {"nb","no"},
            {"45", "no"},
            {"46", "sv"},
            {"41", "da"},
            {"42", "nl"},
            {"44", "de"},
            {"121", "fr"},
            {"163", "es"},
            {"183", "pl"},
            {"43", "en"},
            {"21", "en"},
            {"223", "pt"},
            {"203", "it"}
        };
        public static Dictionary<string, string> LanguageMapping
        {
            get
            {
                lock (m_oLanguageMapping) return m_oLanguageMapping;
            }
        }
        public static string getUserLanguage(string sUserLanguage, int iParentCompanyLanguage)
        {
            string sLanguage = parse.splitValue(convert.cStr(sUserLanguage, (System.Web.HttpContext.Current.Request.UserLanguages == null || System.Web.HttpContext.Current.Request.UserLanguages.Length == 0 ? "" : System.Web.HttpContext.Current.Request.UserLanguages[0]), "en").ToLower(), "-", 0);
            if (LanguageMapping.ContainsKey(sLanguage)) sLanguage = LanguageMapping[sLanguage];
            if (sLanguage.IsNullOrEmpty() && LanguageMapping.ContainsKey(iParentCompanyLanguage.Str())) sLanguage = LanguageMapping[iParentCompanyLanguage.Str()];

            if (sLanguage == "" || convert.isNumeric(sLanguage)) sLanguage = (System.Web.HttpContext.Current.Request.UserLanguages == null || System.Web.HttpContext.Current.Request.UserLanguages.Length == 0 ? "en" : parse.splitValue(System.Web.HttpContext.Current.Request.UserLanguages[0], "-", 0));
            List<string> supportedLanguages = new List<string>() { "en", "no", "it", "sv", "pt", "de", "fr", "da", "pl", "nl", "es" };
            if (!supportedLanguages.Contains(sLanguage)) sLanguage = "en";
            return sLanguage;
        }
        public static string GetBadgeImageUrl(Util.Classes.Activity activity)
        {
            //Lookup most specific badgeImage for this activity
            var activityVersionRaw = $"{activity.ArticleNumber?.Substr(0, -4)}-{activity.ArticleNumber?.Substr(activity.ArticleNumber?.Substr(0, -4).Length ?? 0, 2)}00{activity.VersionNumber.SafeSplit("-",1)}";
            
            if (activity.IsElearning && !string.IsNullOrWhiteSpace( convert.cStr(activity.ELearnCourseObject?.Data?.Course?.Version)))
            {
                activityVersionRaw = activity.ELearnCourseObject.Data.Course.Version;                
            }

            var prefix = activityVersionRaw.SafeSplit("-", 0);
            var courseNumber = activityVersionRaw.SafeSplit("-", 1);
            var suffix = activityVersionRaw.SafeSplit("-", 2);

            var activityArticleNumber = $"{prefix}{courseNumber.Substr(0,2)}{courseNumber.Substr(-2)}";
            var activityArticleNumberNoLanguage = $"{prefix}{courseNumber.Substr(0, 2)}";

            var activityVersion = $"{prefix}{courseNumber}{suffix}";
            var activityVersionNoCompany = $"{prefix}{courseNumber}";
            var activityVersionNoLanguage = $"{prefix}{courseNumber.Substr(0, 4)}";

            var badgeImages = System.IO.Directory.GetFiles($"{System.AppDomain.CurrentDomain.BaseDirectory}/badges/images/", "*.png");
            var badgeImage = badgeImages.FirstOrDefault(i => System.IO.Path.GetFileNameWithoutExtension(i).EndsWith($"{activityVersion}", StringComparison.CurrentCultureIgnoreCase));
            if (badgeImage.IsNullOrEmpty()) badgeImage = badgeImages.FirstOrDefault(i => System.IO.Path.GetFileNameWithoutExtension(i).EndsWith($"{activityVersionNoCompany}", StringComparison.CurrentCultureIgnoreCase));
            if (badgeImage.IsNullOrEmpty()) badgeImage = badgeImages.FirstOrDefault(i => System.IO.Path.GetFileNameWithoutExtension(i).EndsWith($"{activityVersionNoLanguage}", StringComparison.CurrentCultureIgnoreCase));


            if (badgeImage.IsNullOrEmpty()) badgeImage = badgeImages.FirstOrDefault(i => System.IO.Path.GetFileNameWithoutExtension(i).EndsWith($"{activityArticleNumber}", StringComparison.CurrentCultureIgnoreCase));
            if (badgeImage.IsNullOrEmpty()) badgeImage = badgeImages.FirstOrDefault(i => System.IO.Path.GetFileNameWithoutExtension(i).EndsWith($"{activityArticleNumberNoLanguage}", StringComparison.CurrentCultureIgnoreCase));

            return "/learningportal/badges/images/" + (badgeImage.IsNotNullOrEmpty() ? $"{System.IO.Path.GetFileName(badgeImage)}" : "default.png");
        } 
    }


}