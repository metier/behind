﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public class Enrollment : ActivityBase
    {
        public override int ArticleTypeId
        {
            get
            {
                var articleType = convert.cStr(Data.ArticleType).ToLower();
                if (articleType == "e-learning") return 1;
                if (articleType == "classroom") return 2;
                if (articleType == "multiple choice") return 3;
                if (articleType == "case exam") return 4;
                if (articleType == "project assignment") return 5;
                if (articleType == "internal certification") return 6;
                if (articleType == "other") return 7;
                if (articleType == "mockexam") return 8;
                if (articleType == "external certification") return 9;
                return 7; //other
            }
        }
        public override string ArticleNumber { get { string s = convert.cStr(Data.ArticlePath); return s.Substr(0, 5); } }
        public override string VersionNumber { get { return Data.ArticlePath; } }
        public override DateTimeOffset? ActivityStart { get { return (convert.cStr(Data.ActivityStart) == "" ? null : convert.cDateOffset(Data.ActivityStart)); } }
        public override DateTimeOffset? ActivityEnd { get { return (convert.cStr(Data.ActivityEnd) == "" ? null : convert.cDateOffset(Data.ActivityEnd)); } }
        
        public int MedalType
        {
            get
            {
                if (this.ArticleType == Util.Data.MetierClassTypes.ELearning)
                {
                    if (this.IsImportedHistory)
                    {
                        if (this.NumberOfPointsAchieved >= 90) return (int)Util.Data.MedalTypes.Gold;
                        if (this.NumberOfPointsAchieved >= 80) return (int)Util.Data.MedalTypes.Silver;
                        return (int)Util.Data.MedalTypes.Bronze;
                    }
                    else
                    {
                        var elearn = this.ELearnCourseObject;
                        if (elearn != null)
                        {
                            if (elearn.GoldPoints.Int() > 0)
                            {
                                if (this.NumberOfPointsAchieved >= elearn.GoldPoints.Int()) return (int)Util.Data.MedalTypes.Gold;
                                if (this.NumberOfPointsAchieved >= elearn.SilverPoints.Int()) return (int)Util.Data.MedalTypes.Silver;
                                return (int)Util.Data.MedalTypes.Bronze;
                            }
                        }
                    }
                }
                return -1;
            }
        }
        public sealed class ParticipantStatusCodes
        {
            public static string Enrolled = "ENROLLED";
            public static string Completed = "COMPLETED";
            public static string NoShow = "NOSHOW";
            public static string Dispensation = "DISPENSATION";
            public static string Passed = "PASSED";
            public static string Cancelled = "CANCELLED";
            public static string OnHold = "ONHOLD";
        }
        public Enrollment(dynamic data, user user)
        {
            this.InitializeData(data);
            m_oUser = user;
        }

        public int ActivitySetId
        {
            get
            {
                return convert.cInt(Data.ActivitySetId);
            }
        }        
        private int? m_iActivitySetGroupingId;
        public int ActivitySetGroupingId
        {
            get
            {
                if (m_iActivitySetGroupingId == null) m_iActivitySetGroupingId = ActivitySetId;
                return m_iActivitySetGroupingId.Value;
            }
            set
            {
                m_iActivitySetGroupingId = value;
            }
        }
        public override int ActivityId { get { return convert.cInt(Data.ActivityId); } }
        public override int LanguageId { get { return convert.cInt(Data.LanguageId); } }        
        public int Id { get { return convert.cInt(Data.Id); } }
        public int CustomerArticleId { get { return convert.cInt(Data.CustomerArticleId); } }
        public int ArticleId { get { return convert.cInt(Data.ArticleId); } }
        public bool IsUserCompetenceInfoRequired { get { return convert.cBool(Data.IsUserCompetenceInfoRequired); } }
        public DateTimeOffset? CompletedDate
        {
            get
            {
                if (ELearnCourseObject != null && ELearnCourseObject.Course != null)
					return ELearnCourseObject.Course.UserLessonCompletedDate;
				else
					return convert.cDateNullable(true, Participant.CompletedDate);
				
			}
        }
        
        private ActivityAvailability m_oParticipantAvailability;
        public ActivityAvailability ParticipantAvailability
        {
            get
            {
                if (m_oParticipantAvailability == null)
                {
                    Phoenix.session.RequestResult result = m_oUser.PhoenixSession.PhoenixGet(String.Format(Phoenix.session.Queries.ParticipantAvailability, Id), false, false);
                    m_oParticipantAvailability = new ActivityAvailability(result.DataObject);
                }
                return m_oParticipantAvailability;
            }
        }


        public bool IsActive
        {
            get
            {
                //if (this.IsMPCExam) return (this.ActivityStart.Date() < DateTime.Now && (this.ActivityEnd == null || this.ActivityEnd.Date() > DateTime.Now));
                return ParticipantAvailability.IsAvailable;
            }
        }
        //Try to avoid instantiating the Activity
        private Activity m_oActivityObject;
        private Activity ActivityObject
        {
            get
            {
                lock (this)
                {
                    if (m_oActivityObject == null) m_oActivityObject = new Activity(m_oUser.PhoenixSession.PhoenixGet(String.Format(Phoenix.session.Queries.ActivityGet, ActivityId), true, true).DataObject, m_oUser);
                    return m_oActivityObject;
                }
            }
        }
        public override List<ActivityPeriod> ActivityPeriods
        {
            get
            {
                return (this.ActivityObject == null ? new List<ActivityPeriod>() : this.ActivityObject.ActivityPeriods);
            }
        }
        private CustomerArticle m_oCustomerArticleObject;
        public CustomerArticle CustomerArticleObject
        {
            get
            {
                lock (this)
                {
                    if (m_oCustomerArticleObject == null) m_oCustomerArticleObject = new CustomerArticle(m_oUser.PhoenixSession.PhoenixGet(String.Format(Phoenix.session.Queries.CustomerArticleGet, CustomerArticleId), true, true).DataObject, m_oUser);
                    return m_oCustomerArticleObject;
                }
            }
        }

        private Article m_oArticleCopyObject;
        public Article ArticleCopyObject
        {
            get
            {
                lock (this)
                {
                    if (m_oArticleCopyObject == null && CustomerArticleObject != null)
                    {
                        //changed from CustomerArticleObject.ArticleOriginalId on 2014-06-13 by JVT
                        m_oArticleCopyObject = new Article(new Phoenix.session().PhoenixGet(String.Format(Phoenix.session.Queries.ArticleGet, CustomerArticleObject.ArticleCopyId), true, true).DataObject, m_oUser);
                    }
                    return m_oArticleCopyObject;
                }
            }
        }

        private ActivitySet m_oActivitySetObject;
        public ActivitySet ActivitySetObject
        {
            get
            {
                lock (this)
                {
                    if (ActivitySetId == 0) return null;
                    if (m_oActivitySetObject == null) m_oActivitySetObject = new ActivitySet(m_oUser.PhoenixSession.PhoenixGet(String.Format(Phoenix.session.Queries.ActivitySetGet, ActivitySetId), true, true).DataObject, m_oUser);
                    return m_oActivitySetObject;
                }
            }
        }


        public string ParticipantStatusCodeValue { get { return Data.ParticipantStatusCodeValue; } }        

        private string m_sActivitySetName;
        public string ActivitySetName
        {
            get
            {
                if (m_sActivitySetName == null) m_sActivitySetName = Data.ActivitySetName;
                return m_sActivitySetName;
            }
            set
            {
                m_sActivitySetName = value;
            }
        }
        public override string ActivityName
        {
            get
            {
                return Data.ActivityName;                
            }         
        }
        public override int ProductDescriptionId { get { return convert.cInt(Data.ProductDescriptionId); } }

        private int? m_iLessonsCompleted;
        public int LessonsCompleted
        {
            get
            {
                if (this.ArticleType != Util.Data.MetierClassTypes.ELearning) return 0;
                if (m_iLessonsCompleted == null)
                {
                    m_iLessonsCompleted = 0;
                    if (ELearnCourseObject != null)
                    {
                        foreach (var lesson in ELearnCourseObject.Lessons)
                            if (lesson.UserLessonStatus == "C" && !lesson.IsFinalTest) m_iLessonsCompleted++;
                    }
                }
                return m_iLessonsCompleted.Value;
            }
        }
        private int? m_iNumberOfPointsAchieved;
        public int NumberOfPointsAchieved
        {
            get
            {
                if (this.ArticleType != Util.Data.MetierClassTypes.ELearning) return 0;
                if (m_iNumberOfPointsAchieved == null)
                {
                    m_iNumberOfPointsAchieved = 0;
                    if (ELearnCourseObject != null)
                    {
                        if (this.IsImportedHistory)
                        {
                            m_iNumberOfPointsAchieved = Math.Max(0, ELearnCourseObject.Course.UserNumPoints);
                        }
                        else
                        {
                            foreach (var lesson in ELearnCourseObject.Lessons)
                                if (lesson.UserNumPoints > 0) m_iNumberOfPointsAchieved += (lesson.UserNumPoints * lesson.Weight);
                        }
                    }
                }
                return convert.cInt(m_iNumberOfPointsAchieved.Value);
            }
        }                
		private dynamic _Participant;
		private dynamic Participant
		{
			get
			{
				if(_Participant == null)				
					_Participant = new Phoenix.session().PhoenixGet(String.Format(Phoenix.session.Queries.ParticipantGet, this.Id)).DataObject;
				
				return _Participant;
			}
		}
        public List<Attachment> Attachments
        {
            get
            {                
				var attachments = new List<Attachment>();
				foreach (dynamic attachment in Participant.Attachments)
				{
					attachments.Add(new Attachment(attachment));
				}
				return attachments;
            }
        }
		
	}
}