﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public class EnrollmentDisplaySet : DisplaySet
    {
        public EnrollmentDisplaySet(Util.Classes.ActivitySet Set) : base(Set)
        {
        }
        public override DisplaySet Populate(Util.Classes.user User, jlib.components.webpage Page)
        {
            Activities = new List<DisplayActivity>();
            Set.ActivityBases.ForEach(activity => {
                Activities.Add(new EnrollmentDisplayActivity(activity, User, Page));
            });
            return this;
        }

    }
}