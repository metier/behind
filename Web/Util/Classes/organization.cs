﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public partial class organization
    {
        public DateTimeOffset LoadDate = DateTime.Now;
        private List<ActivitySet> m_oCourseCalendar;


        private object _SettingsLock = new object();
        private dynamic m_oOrganization;
        public organization(int id)
        {
            this.ID = id;
        }
        public organization(dynamic organization)
        {
            m_oOrganization = organization;
            this.ID = convert.cInt(organization.Id);
        }
        public int ID { get; set; }
        public string Name { get { return convert.cStr(convert.SafeGetProperty(OrganizationObject, "Name")); } }
        public bool IsArbitrary { get { return convert.cBool(convert.SafeGetProperty(OrganizationObject, "IsArbitrary")); } }

		//Diploma stuff
		public bool DiplomaShowElearningLessons { get { return convert.cBool(convert.SafeGetProperty(OrganizationObject, "DiplomaShowElearningLessons")); } }
		public bool DiplomaShowCustomerLogo { get { return convert.cBool(convert.SafeGetProperty(OrganizationObject, "DiplomaShowCustomerLogo")); } }
		public bool DiplomaShowCustomSignature { get { return convert.cBool(convert.SafeGetProperty(OrganizationObject, "DiplomaShowCustomSignature")); } }
		public string DiplomaCustomSignatureText { get { return convert.cStr(convert.SafeGetProperty(OrganizationObject, "DiplomaCustomSignatureText")); } }
		public string DiplomaCustomSignatureTitle { get { return convert.cStr(convert.SafeGetProperty(OrganizationObject, "DiplomaCustomSignatureTitle")); } }
		public string DiplomaLogoAttachmentUrl {
			get {
				return Util.Phoenix.Helpers.GetFileUrl(OrganizationObject.DiplomaLogoAttachment);
			}
		}
		public string DiplomaSignatureAttachmentUrl
		{
			get
			{
				return Util.Phoenix.Helpers.GetFileUrl(OrganizationObject.DiplomaSignatureAttachment);
			}
		}
		

		public int CompetitionMode { get { return convert.cInt(convert.SafeGetProperty(OrganizationObject, "ParticipantInfoDefinition", "CompetitionMode")); } }
        //*0=full
        //*1=classmates
        //*2=none

        public bool IsAllowUsersToSelfEnrollToCourses { get { return convert.cBool(convert.SafeGetProperty(OrganizationObject, "ParticipantInfoDefinition", "IsAllowUsersToSelfEnrollToCourses")); } }
        public bool IsAllowUsersToSelfEnrollToExams { get { return convert.cBool(convert.SafeGetProperty(OrganizationObject, "ParticipantInfoDefinition", "IsAllowUsersToSelfEnrollToExams")); } }
        public bool IsShowPricesToUserOnPortal { get { return convert.cBool(convert.SafeGetProperty(OrganizationObject, "ParticipantInfoDefinition", "IsShowPricesToUserOnPortal")); } }
        public int ParentCompanyID { get { return convert.cInt(convert.SafeGetProperty(OrganizationObject, "ParentId")); } }
        public int MasterCompanyID
        {
            get
            {
                var org = this;
                while (org.ParentCompanyID != 0)
                    org = user.getOrganization(org.ParentCompanyID);
                return org.ID;
            }
        }
        public int LanguageId { get { return convert.cInt(convert.SafeGetProperty(OrganizationObject, "LanguageId")); } }
        public string TermsAndConditions { get { return convert.cStr(convert.SafeGetProperty(OrganizationObject, "ParticipantInfoDefinition", "TermsAndConditionsText")); } }
        public string EmailConfirmationAddress { get { return convert.cStr(convert.SafeGetProperty(OrganizationObject, "ParticipantInfoDefinition", "EmailConfirmationAddress")); } }
        public int DistributorId { get { return convert.cInt(OrganizationObject.DistributorId); } }
        public Distributor Distributor { get { return Distributor.GetDistributor(DistributorId); } }
        public dynamic OrganizationObject
        {
            get
            {
                lock (this)
                {
                    if (m_oOrganization == null && this.ID > 0)
                    {
                        Phoenix.session session = new Phoenix.session();
                        m_oOrganization = session.PhoenixGet(String.Format(Phoenix.session.Queries.CustomerGet, this.ID)).DataObject;
                    }
                    return m_oOrganization;
                }
            }
        }

        public List<ActivitySet> CourseCalendar
        {
            get
            {
                if (m_oCourseCalendar == null)
                {
                    m_oCourseCalendar = new List<ActivitySet>();
                    Phoenix.session session = new Phoenix.session(System.Configuration.ConfigurationManager.AppSettings["ez.api.token"]);
                    Phoenix.session.RequestResult result = session.PhoenixGet(String.Format(Phoenix.session.Queries.CourseCalendar, this.ID), true, true);
                    if (!result.Error)
                    {
                        foreach (dynamic course in result.DataObject)
                            m_oCourseCalendar.Add(new ActivitySet(course, null));
                    }
                }
                return m_oCourseCalendar;
            }
        }
    }
}