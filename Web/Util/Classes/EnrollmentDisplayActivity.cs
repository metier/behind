﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public class EnrollmentDisplayActivity : DisplayActivity
    {
        public List<Util.Classes.Resource> Locations { get; set; }
        public List<Util.Classes.Resource> Ecoaches { get; set; }
        public List<Util.Classes.Resource> Instructors { get; set; }
        public List<Util.Classes.Resource> StudyAdvisors { get; set; }
        public List<string> Periods { get; set; }
        public EnrollmentDisplayActivity(Util.Classes.ActivityBase Activity, Util.Classes.user User, jlib.components.webpage Page) : base(Activity, User, Page)
        {
            Locations = Activity.Locations;
            Ecoaches = Activity.Ecoaches;
            Instructors = Activity.Instructors;
            StudyAdvisors = Activity.StudyAdvisors;
            Periods = new List<string>();

            foreach (var period in Activity.ActivityPeriods)
            {
                if (period.Start != null || period.End != null)
                {
                    var time = String.Format(Util.MyMetier.MetierDate(Page.Language, period.Start) + " " + jlib.helpers.translation.translate(Page.Language, "", Page, "classroom-popup-clock") + " {0:HH:mm}", period.Start);
                    time += String.Format(" - " + (period.Start != null && period.End != null && period.Start.Value.Date != period.End.Value.Date && period.End > period.Start ? Util.MyMetier.MetierDate(Page.Language, period.End) + " " + jlib.helpers.translation.translate(Page.Language, "", Page, "classroom-popup-clock") + " " : "") + " {0:HH:mm}<br>", period.End);
                    Periods.Add(time);
                }
            }
        }
    }

}