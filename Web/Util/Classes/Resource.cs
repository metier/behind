﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public class Resource : PhoenixObject
    {
        public Resource(dynamic data)
        {
            this.InitializeData(data);
        }
        public int ResourceTypeId { get { return convert.cInt(Data.ResourceTypeId); } }
        public string Name { get { return Data.Name; } }
        public int Id { get { return convert.cInt(Data.Id); } }
        public string ContactEmail { get { return Data.ContactEmail; } }
        public string ContactPhone { get { return Data.ContactPhone; } }

        private string m_sImage;
        public string Image
        {
            get
            {
                if (m_sImage == null)
                {
                    m_sImage = "";
                    string fileId = "";
                    if (convert.cInt(convert.SafeGetProperty(Data, "UserId")) > 0)
                    {
                        dynamic user = new Phoenix.session().PhoenixGet(String.Format(Phoenix.session.Queries.UserGet, Data.UserId), true, true).DataObject;
                        fileId = convert.cStr(convert.SafeGetProperty(user, "User", "Attachment", "FileId"));
                    }
                    if (fileId.IsNullOrEmpty())
                    {
                        dynamic resource = new Phoenix.session().PhoenixGet(String.Format(Phoenix.session.Queries.ResourceGet, Data.Id), true, true).DataObject;
                        fileId = convert.cStr(convert.SafeGetProperty(extensions.SafeGetValue(convert.SafeGetList(resource, "Attachments"), 0), "FileId"));
                    }
                    if (!fileId.IsNullOrEmpty()) m_sImage = System.Web.HttpContext.Current.Request.ApplicationPath + "/download.aspx?FileId=" + fileId + "&secret=" + Util.Permissions.encryptString(fileId + System.Configuration.ConfigurationManager.AppSettings["phoenix.api.token"]);
                }
                return m_sImage;
            }
        }
    }
}