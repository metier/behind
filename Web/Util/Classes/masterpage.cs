﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Xml;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using System.Linq;
using System.Data;
using System.Data.OleDb;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using jlib.db;
using jlib.functions;
using System.Security.Cryptography;
using System.Text;
using SpreadsheetGear;
using AttackData = Phoenix.LearningPortal.Data;

/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public abstract class masterpage : jlib.components.masterpage
    {
        public AttackData.PageView PageView { get; protected set; }
        public DateTime PageLoadTime = System.DateTime.Now;
        private jlib.components.webpage m_oWebPage;



        public jlib.components.webpage WebPage
        {
            get
            {
                if (m_oWebPage == null) m_oWebPage = (Page as jlib.components.webpage);
                return m_oWebPage;
            }
        }

        public void InitiatePageRequestLogger(bool forcePostDataLog)
        {
            PageView = new AttackData.PageView(WebPage, Request, Session)
            {
                ItemID = convert.cInt(Request.QueryString["id"])
            };
            if (forcePostDataLog && PageView.PostData.IsNullOrEmpty()) PageView.PostData = convert.cStreamToString(Request.InputStream, Request.ContentEncoding);
        }

        public virtual void Page_Init(object sender, EventArgs e)
        {
            //only initiate logger record if we intend to commit it
            if (Request.QueryString["action"] != "save-answers" && ConfigurationManager.AppSettings["page.logging.mode"] != "none" && (ConfigurationManager.AppSettings["page.logging.mode"] == "full" || (!this.IsPostBack && !(WebPage.IP.StartsWith("127.")))))
            {
                if (Session["last_log_date"] == null || convert.cDateOffset(Session["last_log_date"]).AddSeconds(1) < System.DateTime.Now)
                {
                    if (ConfigurationManager.AppSettings["page.logging.mode"] != "full") Session["last_log_date"] = System.DateTime.Now;
                    InitiatePageRequestLogger(false);
                }
            }
        }
        public bool CommitPageRequestLogger()
        {
            if (PageView != null)
            {
                if (PageView.LoadTime <= 0) PageView.LoadTime = (System.DateTime.Now.Subtract(PageLoadTime).Seconds * 1000) + System.DateTime.Now.Subtract(PageLoadTime).Milliseconds;
                try
                {
                    PageView.PageTitle = Page.Title;
                }
                catch (Exception) { }
                PageView.Save();
                return true;
            }
            return false;
        }
    }
}