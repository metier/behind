﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public abstract class PhoenixObject
    {
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public dynamic Data { get; set; }
        public void InitializeData(dynamic data)
        {
            this.Data = data;
        }
        public string RowVersion
        {
            get
            {
                if (this.Data != null && this.Data.RowVersion()) return this.Data.RowVersion;
                return null;
            }
        }
    }
}