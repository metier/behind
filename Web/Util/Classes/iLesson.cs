﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public interface iLesson
    {
        string Name { get; }
        bool IsFinalTest { get; }
        int Id { get; }
        string StartingUrl { get; set; }
        int ParentId { get; set; }
        int Weight { get; }
        string UserLessonStatus { get; }
        DateTimeOffset? UserLessonCompletedDate { get; }
        int UserNumPoints { get; }
        int NumSubLessons { get; }
    }
}