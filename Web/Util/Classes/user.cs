﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Xml;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using System.Linq;
using System.Data;
using System.Data.OleDb;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using jlib.db;
using jlib.functions;
using System.Security.Cryptography;
using System.Text;
using SpreadsheetGear;

/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public class user
    {
        private object _lockCalculateStatus = new object(), _lockEnrollments = new object(), _lockAvailableCourses = new object(), _lockReports = new object();
        private dynamic m_oUser;
        private DataTable m_oReports;
        private List<Participant> m_oColleagues;
        private List<ActivitySet> m_oAvailableCourses, m_oAvailableCoursesRaw;
        private List<Enrollment> m_oEnrollments;

        public DateTimeOffset LoadDate = DateTime.Now;
        private int m_iID = 0;
        private int[] m_iAvailableCourseTypes = new int[100];
        private int[] m_iMedalCount = new int[3];
        private int m_iPointCount = -1, m_iElearnLessonsAvailable, m_iElearnLessonsCompleted, m_iElearnEnrollments, m_iElearnEnrollmentsCompleted;
        private List<int> m_oOrgIDs, m_oFileFolderIDs;
        private Util.Phoenix.session m_oPhoenixSession;
        private Dictionary<int, iObjectWithLessons> m_oRcoObjects;
        private Util.Customization.Settings.SettingsHelper m_oPortalSettings;
        public Util.Customization.Settings.SettingsHelper PortalSettings
        {
            get
            {
                if (m_oPortalSettings == null)
                    m_oPortalSettings = Util.Customization.Settings.GetSettings(this);
                return m_oPortalSettings;
            }
        }
        
        public Dictionary<int, iObjectWithLessons> RcoObjects
        {
            get
            {
                if (m_oRcoObjects == null)
                {
                    m_oRcoObjects = new Dictionary<int, iObjectWithLessons>();
                    Util.Phoenix.session.RequestResult result = this.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.UserPerformancesGet, this.ID, "", ""));
                    if (!result.Error && !result.Data.IsNullOrEmpty())
                    {
                        foreach (var rco in result.DataObject)
                        {
                            iObjectWithLessons ObjectWithLessons = null;
                            if (rco.ExamContainer == null)
                                ObjectWithLessons = new ELearnCourse(rco);
                            else
                                ObjectWithLessons = new MockExamCourse(rco);
                            if(!m_oRcoObjects.ContainsKey(ObjectWithLessons.Id))
                                m_oRcoObjects.Add(ObjectWithLessons.Id, ObjectWithLessons);
                        }
                    }
                }
                return m_oRcoObjects;
            }
            set
            {
                m_oRcoObjects = value;
            }
        }

        public bool IsBehindSuperAdmin { get { return this.UserRoles.Contains("BehindSuperAdmin"); } }
        public bool IsBehindContentAdmin { get { return IsBehindSuperAdmin || this.UserRoles.Contains("BehindContentAdmin"); } }
        public bool IsBehindAdmin { get { return IsBehindSuperAdmin || IsBehindContentAdmin || this.UserRoles.Contains("BehindAdmin"); } }
		public bool IsPhoenixUser { get { return this.UserRoles.Contains("PhoenixUser"); } }
		public bool IsLearningPortalSuperUser { get { return this.UserRoles.Contains("LearningPortalSuperUser"); } }
        public bool IsAllowUserEcts { get { return convert.cBool(convert.SafeGetProperty(this.UserObject.User, "IsAllowUserEcts")); } }

        private UserCompetence m_oExamCompetence;
        public UserCompetence ExamCompetence
        {
            get
            {
                if (m_oExamCompetence == null)
                {
                    Util.Phoenix.session.RequestResult result = this.PhoenixSession.PhoenixGet(String.Format(Util.Phoenix.session.Queries.UserCompetenceGetPutPost, this.ID));
                    if (result.Error || result.Data.IsNullOrEmpty())
                    {
                        //indication that no competence has been registered for user
                        m_oExamCompetence = new UserCompetence(null);
                    }
                    else
                    {
                        if (convert.cStr(result.DataObject.DateOfBirth) != "") result.DataObject.DateOfBirth = extensions.SafeSplit(convert.cStr(result.DataObject.DateOfBirth), "T", 0);
                        m_oExamCompetence = new UserCompetence(result.DataObject);
                    }
                }
                if (m_oExamCompetence.Data == null) return null;
                return m_oExamCompetence;
            }
        }
        public void ResetExamCompetence()
        {
            m_oExamCompetence = null;
            this.PhoenixSession.PhoenixClearCache(String.Format(Util.Phoenix.session.Queries.UserCompetenceGetPutPost, this.ID));
        }
        public bool IsExamCompetenceComplete
        {
            get
            {
                if (ExamCompetence == null) return false;
                if (!ExamCompetence.Data.IsTermsAccepted() || !ExamCompetence.Data.IsTermsAccepted) return false;
                return true;
            }
        }
        public bool IsExamCompetenceInitiated
        {
            get
            {
                if (IsExamCompetenceComplete) return true;
                if (ExamCompetence == null) return false;
                if (!string.IsNullOrEmpty(convert.cStr(convert.SafeGetProperty(ExamCompetence.Data, "FirstName")))
                 || !string.IsNullOrEmpty(convert.cStr(convert.SafeGetProperty(ExamCompetence.Data, "LastName")))
                 || !string.IsNullOrEmpty(convert.cStr(convert.SafeGetProperty(ExamCompetence.Data, "Email")))
                 || !string.IsNullOrEmpty(convert.cStr(convert.SafeGetProperty(ExamCompetence.Data, "Title")))
                 || !string.IsNullOrEmpty(convert.cStr(convert.SafeGetProperty(ExamCompetence.Data, "InstitutionName")))
                 )
                    return true;
                return false;
            }
        }
        public bool NagUserForExamCompetence
        {
            get
            {
                return (IsAllowUserEcts
                    && !IsExamCompetenceComplete
                    && this.Enrollments.FirstOrDefault(x => { return x.ParticipantStatusCodeValue == Enrollment.ParticipantStatusCodes.Enrolled && x.IsUserCompetenceInfoRequired; }) != null
                );
                //
            }
        }
        public Util.Phoenix.session PhoenixSession
        {
            get
            {
                if (m_oPhoenixSession == null) m_oPhoenixSession = new Phoenix.session();
                return m_oPhoenixSession;
            }
            set
            {
                m_oPhoenixSession = value;
            }
        }
        private int m_iUserRank = 0;
        public user(int iID)
        {
            m_iID = iID;
        }
        public user(dynamic userObject)
        {
            m_oUser = userObject;
            m_iID = convert.cInt(userObject.User.Id);
        }
        public int ID
        {
            get
            {
                return m_iID;
            }
        }
        public int PointCount
        {
            get
            {
                calculateStatus();
                return m_iPointCount;
            }
        }
        public int ElearnLessonsAvailable
        {
            get
            {
                calculateStatus();
                return m_iElearnLessonsAvailable;
            }
        }
        public int ElearnLessonsCompleted
        {
            get
            {
                calculateStatus();
                return m_iElearnLessonsCompleted;
            }
        }
        public int ElearnEnrollments
        {
            get
            {
                calculateStatus();
                return m_iElearnEnrollments;
            }
        }
        public int ElearnEnrollmentsCompleted
        {
            get
            {
                calculateStatus();
                return m_iElearnEnrollmentsCompleted;
            }
        }


        public int[] MedalCount
        {
            get
            {
                calculateStatus();
                return m_iMedalCount;
            }
        }
        private void calculateStatus()
        {
            lock (_lockCalculateStatus)
            {
                if (m_iPointCount == -1)
                {
                    m_iPointCount = 0;
                    List<int> articlesProcessed = new List<int>();
                    //m_iMedalCount[0] += this.ImportedCourses.Rows.Count;
                    foreach (Enrollment enrollment in Enrollments)
                    {
                        if (!enrollment.IsMockExam)
                        {
                            if (enrollment.ArticleType == Util.Data.MetierClassTypes.ELearning)
                            {
                                //If Imported history, treat differently
                                if (enrollment.IsImportedHistory)
                                {
                                    if (enrollment.ParticipantStatusCodeValue == Enrollment.ParticipantStatusCodes.Completed)
                                    {
                                        if (enrollment.MedalType > -1)
                                            m_iMedalCount[enrollment.MedalType]++;                                        
                                    }                                    
                                }
                                else
                                {
                                    var elearn = enrollment.ELearnCourseObject;
                                    if (elearn != null && !articlesProcessed.Contains(elearn.Id))
                                    {
                                        articlesProcessed.Add(elearn.Id);
                                        if (enrollment.ParticipantStatusCodeValue == Enrollment.ParticipantStatusCodes.Completed && elearn.GoldPoints.Int() > 0)
                                        {
                                            if (enrollment.MedalType > -1) m_iMedalCount[enrollment.MedalType]++;
                                            m_iElearnEnrollmentsCompleted++;
                                        }
                                        m_iElearnEnrollments++;
                                        m_iElearnLessonsAvailable += elearn.Lessons.FindAll(lesson => !lesson.IsFinalTest).Count;
                                        m_iElearnLessonsCompleted += enrollment.LessonsCompleted;
                                        m_iPointCount += enrollment.NumberOfPointsAchieved;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        public List<Enrollment> Enrollments
        {
            get
            {
                lock (_lockEnrollments)
                {
                    if (m_oEnrollments == null)
                    {
                        Phoenix.session.RequestResult result = PhoenixSession.PhoenixGet(String.Format(Phoenix.session.Queries.ParticipantsSearch, this.ID, "", "0", "1000"));
                        if (result.Error) return new List<Enrollment>();
                        m_oEnrollments = new List<Enrollment>();
                        foreach (dynamic enrollment in result.DataObject.Items)
                        {
                            Enrollment e = new Enrollment(enrollment, this);
                            //Don't add exempted courses
                            if (e.ParticipantStatusCodeValue != Enrollment.ParticipantStatusCodes.Dispensation
                                && e.ParticipantStatusCodeValue != Enrollment.ParticipantStatusCodes.OnHold
                                && e.ParticipantStatusCodeValue != Enrollment.ParticipantStatusCodes.Cancelled
                                && e.ArticleType != Util.Data.MetierClassTypes.Other
                                && e.ArticleType != Util.Data.MetierClassTypes.Unknown
                                )
                                m_oEnrollments.Add(e);
                        }
                        m_oEnrollments.Sort((a, b) => a.ActivityStart.Date().CompareTo(b.ActivityStart.Date()));
                    }
                    return m_oEnrollments;
                }
            }
        }

        public organization Organization
        {
            get
            {
                return getOrganization(OrganizationID);
            }
        }
        public int OrganizationID
        {
            get
            {
                if (UserObject == null) return 0;
                return convert.cInt(jlib.functions.convert.SafeGetProperty(UserObject, "CustomerId"));
            }
        }
        public int UserRank
        {
            get
            {
                if (m_iUserRank == 0) m_iUserRank = (Organization.CompetitionMode == 0 ? Colleagues.IndexOf(Colleagues.FirstOrDefault(participant => participant.Id == ID)) + 1 : -1);
                return Math.Max(0, m_iUserRank);
            }
        }
        public List<Participant> Colleagues
        {
            get
            {
                lock (this)
                {
                    if (m_oColleagues == null)
                    {
                        m_oColleagues = new List<Participant>();
                        dynamic colleagues = new Phoenix.session().PhoenixGet(String.Format(Phoenix.session.Queries.UserPerformancesGetByCustomer, this.OrganizationID), true, true).DataObject;
                        if (colleagues != null)
                            foreach (var colleague in colleagues) m_oColleagues.Add(new Participant(colleague));
                    }
                    return m_oColleagues;
                }
            }
        }
        private List<string> m_oUserRoles;
        public List<string> UserRoles
        {
            get
            {
                if (m_oUserRoles == null)
                {
                    m_oUserRoles = new List<string>();
                    if (UserObject != null)
                        foreach (string role in UserObject.Roles) m_oUserRoles.Add(role);
                }
                return m_oUserRoles;
            }
        }
        public string Email
        {
            get
            {
                return UserObject.Email;
            }
        }
        public string PhoneNumber
        {
            get
            {
                return convert.cStr(convert.SafeGetProperty(UserObject, "User", "PhoneNumber"));
            }
        }
        public string FirstName
        {
            get
            {
                return convert.cStr(convert.SafeGetProperty(UserObject, "User", "FirstName"));
            }
        }
        public string LastName
        {
            get
            {
                return convert.cStr(convert.SafeGetProperty(UserObject, "User", "LastName"));
            }
        }
        public string Username
        {
            get
            {
                return convert.cStr(convert.SafeGetProperty(UserObject, "User", "MembershipUserId"));
            }
        }
        public string StreetAddress
        {
            get
            {
                return convert.cStr(convert.SafeGetProperty(UserObject, "User", "StreetAddress"));
            }
        }
        public string StreetAddress2
        {
            get
            {
                return convert.cStr(convert.SafeGetProperty(UserObject, "User", "StreetAddress2"));
            }
        }
        public string City
        {
            get
            {
                return convert.cStr(convert.SafeGetProperty(UserObject, "User", "City"));
            }
        }
        public string Country
        {
            get
            {
                return convert.cStr(convert.SafeGetProperty(UserObject, "User", "Country"));
            }
        }
        public string ZipCode
        {
            get
            {
                return convert.cStr(convert.SafeGetProperty(UserObject, "User", "ZipCode"));
            }
        }
        public bool AllowEnroll
        {
            get
            {
                return AllowCourseEnroll || AllowExamEnroll;
                //2014-04-02 After conversation with Åse, allow enrollment on the user will be discontinued
                //&& convert.cBool(convert.SafeGetProperty(UserObject, "User", "IsAllowUsersToSelfEnrollToCourses"));
            }
        }
        public bool AllowCourseEnroll
        {
            get
            {
                return Organization.IsAllowUsersToSelfEnrollToCourses;
            }
        }
        public bool AllowExamEnroll
        {
            get
            {
                return Organization.IsAllowUsersToSelfEnrollToExams;
            }
        }
        public string DistributorEmail
        {
            get
            {
                if (OrganizationID > 0 && Organization.Distributor != null) return Organization.Distributor.ContactEmail;
                return null;
            }
        }
        public int DistributorId
        {
            get
            {
                if (OrganizationID > 0 && Organization.Distributor != null) return Organization.Distributor.Id;
                return 0;
            }
        }
        public string DistributorName
        {
            get
            {
                if (OrganizationID > 0 && Organization.Distributor != null) return Organization.Distributor.Name;
                return null;
            }
        }
        public int PreferredLanguageId
        {
            get
            {
                return convert.cInt(convert.SafeGetProperty(UserObject, "User", "PreferredLanguageId"));
            }
        }
        public string ImageUrl
        {
            get
            {
                string fileId = convert.cStr(convert.SafeGetProperty(UserObject, "User", "Attachment", "FileId"));
                if (fileId.IsNullOrEmpty()) return "";
                return System.Web.HttpContext.Current.Request.ApplicationPath + "/download.aspx?FileId=" + fileId + "&secret=" + Util.Permissions.encryptString(fileId + System.Configuration.ConfigurationManager.AppSettings["phoenix.api.token"]);
            }
        }
        public dynamic UserObject
        {
            get
            {
                lock (this)
                {
                    if (m_oUser == null && (PhoenixSession.GetUserId() > 0 || PhoenixSession.AlwaysSendToken))
                    {
                        m_oUser = PhoenixSession.PhoenixGet(String.Format(Phoenix.session.Queries.UserGet, this.ID), true, true).DataObject;
                    }
                    return m_oUser;
                }
            }
        }

        public DataTable Reports
        {
            get
            {
                lock (_lockReports)
                {
                    if (m_oReports == null) m_oReports = sqlbuilder.getDataTable(new ado_helper("sql.dsn.cms"), "select d_views.* from d_views, l_row_links where l_row_links.id1=d_views.id and l_row_links.id2=@user_id and d_views.deleted=0 and l_row_links.table1='d_views' and l_row_links.table2='d_users' order by d_views.name", "@user_id", ID);

                    return m_oReports;
                }
            }
        }
        public string UserLanguage
        {
            get
            {
                if (UserObject == null) return "";
                return Util.MyMetier.getUserLanguage(this.PreferredLanguageId.Str(), this.Organization.LanguageId);
            }
        }
        public int[] AvailableCourseTypes
        {
            get
            {
                List<ActivitySet> o = AvailableCourses;
                return m_iAvailableCourseTypes;
            }
        }
        public List<ActivitySet> AvailableCoursesRaw
        {
            get
            {
                lock (_lockAvailableCourses)
                {
                    if (m_oAvailableCoursesRaw == null)
                    {
                        List<ActivitySet> temp = AvailableCourses;
                    }
                    return m_oAvailableCoursesRaw;
                }
            }
        }
        public List<ActivitySet> AvailableCourses
        {
            get
            {
                lock (_lockAvailableCourses)
                {
                    if (m_oAvailableCourses == null)
                    {
                        Phoenix.session.RequestResult result = PhoenixSession.PhoenixGet(String.Format(Phoenix.session.Queries.CourseCalendar, this.OrganizationID), true, true);
                        if (result.Error) return new List<ActivitySet>();
                        m_oAvailableCourses = new List<ActivitySet>();
                        m_oAvailableCoursesRaw = new List<ActivitySet>();
                        foreach (dynamic course in result.DataObject)
                        {
                            //Exclude Sets without activities
                            if (convert.SafeGetList(course, "Activities").Count > 0)
                            {
                                m_oAvailableCourses.Add(new ActivitySet(course, this));
                                m_oAvailableCoursesRaw.Add(new ActivitySet(course, this));
                            }
                        }
                     
                        //Remove individual rows that are not user-enrollable, or courses where user is already enrolled
                        for (int x = 0; x < m_oAvailableCourses.Count; x++)
                        {
                            bool found = false;
                            for (int y = 0; y < m_oAvailableCourses[x].Activities.Count; y++)
                            {
                                var activity = m_oAvailableCourses[x].Activities[y];
                                if (Enrollments.FirstOrDefault(enrollment => enrollment.ActivityId == activity.Id) != null || Enrollments.FirstOrDefault(enrollment => enrollment.ArticleNumber == activity.ArticleNumber && enrollment.ParticipantStatusCodeValue != Enrollment.ParticipantStatusCodes.NoShow) != null) found = true;
                                //Remove daypacks and other/unknown activities
                                if (activity.ArticleType == Util.Data.MetierClassTypes.Unknown || activity.ArticleType == Util.Data.MetierClassTypes.Other)
                                {
                                    m_oAvailableCourses[x].Activities.RemoveAt(y);
                                    y--;
                                }
                            }
                            //m_oAvailableCourses[x].Activities.Sort((x, y) => x.Start.Dte().CompareTo(y.Start.Dte()));
                            if (found || m_oAvailableCourses[x].Activities.Count == 0)
                            {
                                m_oAvailableCourses.RemoveAt(x);
                                x--;
                            }
                        }


                        for (int x = 0; x < m_oAvailableCourses.Count; x++)
                        {
                            if (m_oAvailableCourses[x].GetActivityByType(Util.Data.MetierClassTypes.Classroom).Count > 0 && m_oAvailableCourses[x].GetActivityByType(Util.Data.MetierClassTypes.ELearning).Count > 0) m_iAvailableCourseTypes[(int)Util.Data.MetierClassTypes.IntegratedLearning]++;
                            else if (m_oAvailableCourses[x].Activities.Count > 0)
                            {
                                m_iAvailableCourseTypes[m_oAvailableCourses[x].Activities[0].ArticleType.Int()]++;
                            }
                        }
                    }
                    return m_oAvailableCourses;
                }
            }
        }
        public List<int> OrgIDs
        {
            get
            {
                if (m_oOrgIDs == null)
                {
                    m_oOrgIDs = new List<int>();
                    m_oOrgIDs.Add(Organization.ID);
                    if (Organization.ParentCompanyID > 0) m_oOrgIDs.Add(Organization.ParentCompanyID);
                }
                return m_oOrgIDs;
            }
        }
        public List<int> FileFolderIDs
        {
            get
            {
                if (m_oFileFolderIDs == null)
                {
                    m_oFileFolderIDs = new List<int>();
                    if (OrgIDs.Count > 0)
                    {
                        string SQL = "";
                        OrgIDs.ForEach(x => { SQL += (SQL.IsNullOrEmpty() ? "" : " OR ") + "d_folders.name+'+' like '%+" + x + "+%'"; });
                        DataTable FolderIDs = sqlbuilder.getDataTable(new ado_helper("sql.dsn.cms"), @"select d_folders.ObjectId from dbo.fn_get_links(82826,'d_folders',getdate(),0,'') links, d_folders where d_folders.ObjectId=links.ObjectID and links.TableName='d_folders' and getdate() between d_folders.CreateDate and d_folders.DeleteDate and (" + SQL + ")");
                        foreach (DataRow FolderID in FolderIDs.Rows) m_oFileFolderIDs.Add(FolderID[0].Int());
                    }
                }
                return m_oFileFolderIDs;
            }
        }

        /* STATIC Cache-methods */
        private static Dictionary<int, user> m_oUsers = new Dictionary<int, user>();
        private static DateTimeOffset m_oLastPurgeRun = DateTime.Now;

        private static Dictionary<int, organization> m_oOrganizations = new Dictionary<int, organization>();
        public static organization getOrganization(int id)
        {
            return getOrganization(id, false);
        }
        public static organization getOrganization(int id, bool bForceReload)
        {
            if (m_oLastPurgeRun < DateTime.Now.AddMinutes(-5)) resetCache(false);
            lock (m_oOrganizations)
            {
                if (!bForceReload && m_oOrganizations.ContainsKey(id) && m_oOrganizations[id].LoadDate > DateTime.Now.AddMinutes(-15)) return m_oOrganizations[id];
                m_oOrganizations[id] = new organization(id);
                return m_oOrganizations[id];
            }
        }
        public static user getUser(int id)
        {
            return getUser(id, false);
        }
        public static user getUser(int id, bool bForceReload)
        {
            if (m_oLastPurgeRun < DateTime.Now.AddMinutes(-5)) resetCache(false);
            lock (m_oUsers)
            {
                if (!bForceReload && m_oUsers.ContainsKey(id) && m_oUsers[id].LoadDate > DateTime.Now.AddMinutes(-1)) return m_oUsers[id];
                m_oUsers[id] = new user(id);
                return m_oUsers[id];
            }
        }
        public static void resetCache(bool bFull)
        {
            if (bFull)
            {
                lock (m_oUsers) m_oUsers.Clear();
                lock (m_oOrganizations) m_oOrganizations.Clear();
                Phoenix.Helpers.ResetCache();
            }
            else
            {
                lock (m_oUsers)
                {
                    List<int> oKeys = new List<int>(m_oUsers.Keys);
                    foreach (int iID in oKeys) if (m_oUsers[iID].LoadDate.AddMinutes(1) < DateTime.Now) m_oUsers.Remove(iID);
                }
                lock (m_oOrganizations)
                {
                    List<int> oKeys = new List<int>(m_oOrganizations.Keys);
                    foreach (int iID in oKeys) if (m_oOrganizations[iID].LoadDate.AddMinutes(1) < DateTime.Now) m_oOrganizations.Remove(iID);
                }
            }
            m_oLastPurgeRun = DateTime.Now;
        }
    }
}