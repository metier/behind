﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public class DisplaySet
    {
        public virtual List<DisplayActivity> Activities { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        protected Util.Classes.iActivitySetBase Set;
        public DisplaySet(Util.Classes.iActivitySetBase Set)
        {
            this.Id = Set.Id;
            this.Name = Set.Name;
            this.Set = Set;
        }
        public virtual DisplaySet Populate(Util.Classes.user User, jlib.components.webpage Page)
        {
            Activities = new List<DisplayActivity>();
            Set.ActivityBases.ForEach(activity => {
                Activities.Add(new DisplayActivity(activity, User, Page));
            });
            return this;
        }
    }
}