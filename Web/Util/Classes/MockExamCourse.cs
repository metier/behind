﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public class MockExamCourse : PhoenixObject, iObjectWithLessons
    {
        private MockExamLesson m_oExam;
        public MockExamCourse(dynamic data)
        {
            this.InitializeData(data.IsArray ? data[0] : data);
        }
        private List<iLesson> m_oLessons;
        public List<iLesson> Lessons
        {
            get
            {
                EnumerateLessons();
                return m_oLessons;
            }
        }
        private void EnumerateLessons()
        {
            if (m_oLessons == null)
            {
                m_oLessons = new List<iLesson>();
                if (Data.ExamContainer != null)
                {
                    foreach (dynamic lesson in this.Data.ExamContainer.Children)
                    {
                        dynamic lessonStatus = null;
                        foreach (dynamic status in this.Data.Performances)
                            if (status.RcoId == lesson.Id)
                                lessonStatus = status;

                        var lessonObject = new MockExamLesson(lesson, lessonStatus);
                        lessonObject.ParentId = this.Id;
                        if (lessonObject.Id == this.Id)
                        {
                            m_oExam = lessonObject;
                        }
                        else
                        {
                            m_oLessons.Add(lessonObject);
                        }
                    }
                }
            }
        }
        public int Id
        {
            get
            {
                if (Data.ExamContainer == null) return 0;
                return convert.cInt(Data.ExamContainer.Id);
            }
        }
        public int GoldPoints { get { return 0; } }
        public int SilverPoints { get { return 0; } }
        public int UnlockFinalPoints { get { return 0; } }
        public iLesson Course { get { return new MockExamLesson(this.Data.ExamContainer, null); } }
    }
}