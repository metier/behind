﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public class Participant : PhoenixObject
    {
        public Participant(dynamic data)
        {
            this.InitializeData(data);
        }
        public string FirstName
        {
            get
            {
                return convert.cStr(Data.FirstName);
            }
        }
        public string LastName
        {
            get
            {
                return convert.cStr(Data.LastName);
            }
        }
        public string ImageUrl
        {
            get
            {
                string fileId = convert.cStr(Data.UserProfileImageFileId);
                if (fileId.IsNullOrEmpty()) return "";
                return System.Web.HttpContext.Current.Request.ApplicationPath + "/download.aspx?FileId=" + fileId + "&secret=" + Util.Permissions.encryptString(fileId + System.Configuration.ConfigurationManager.AppSettings["phoenix.api.token"]);
            }
        }
        public int TotalScore
        {
            get
            {
                return convert.cInt(Data.TotalScore);
            }
        }
        public int Id { get { return convert.cInt(Data.UserId); } }
    }
}