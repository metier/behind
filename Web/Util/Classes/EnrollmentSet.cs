﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Object that holds a collection of performances that represents an ActivitySet
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public class EnrollmentSet: iActivitySetBase
    {
        public List<ActivityBase> ActivityBases { get; set; }
        public EnrollmentSet(List<ActivityBase> ActivityBases)
        {
            this.ActivityBases = ActivityBases;
            //If more than 2 activities in Set, sort alphabetically
            //The sorting fails if user is not logged in
            try
            {
                if (ActivityBases.Count(activity => activity.ArticleType == Util.Data.MetierClassTypes.ELearning || activity.ArticleType == Util.Data.MetierClassTypes.Classroom || activity.ArticleType == Util.Data.MetierClassTypes.Exam) > 2)
                    ActivityBases.Sort((a, b) => { return a.ActivityName.Str().CompareTo(b.ActivityName.Str()); });
                else
                    ActivityBases.Sort((a, b) => { return a.ArticleType == Util.Data.MetierClassTypes.ELearning && b.ArticleType != Util.Data.MetierClassTypes.ELearning ? -1 : a.ArticleType != Util.Data.MetierClassTypes.ELearning && b.ArticleType == Util.Data.MetierClassTypes.ELearning ? 1 : 0; });
            }
            catch (Exception) { }
        }
        private List<Enrollment> m_oEnrollments;
        private List<Enrollment> Enrollments {
            get
            {
                if (m_oEnrollments == null) m_oEnrollments = ActivityBases.Select(activity => { return activity as Enrollment; }).ToList();                
                return m_oEnrollments;
            }
        }
        public int Id {
            get {
                return Enrollments.Count == 0 ? 0 : Enrollments[0].ActivitySetId;
            }
        }
        public string Name
        {
            get
            {
                return Enrollments.Count == 0 ? "" : Enrollments[0].ActivitySetName;
            }
        }
    }
}