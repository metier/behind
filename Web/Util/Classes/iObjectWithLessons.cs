﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public interface iObjectWithLessons
    {
        List<iLesson> Lessons { get; }
        dynamic Data { get; }
        int Id { get; }
        int GoldPoints { get; }
        int SilverPoints { get; }
        int UnlockFinalPoints { get; }
        iLesson Course { get; }
    }
}