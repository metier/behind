﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public class ELearnCourse : PhoenixObject, iObjectWithLessons
    {
        public ELearnCourse(dynamic data)
        {
            this.InitializeData(data.IsArray ? data[0] : data);
        }
        public int Id { get { return convert.cInt(Data.Course.Id); } }
        public int GoldPoints { get { return convert.cInt(Data.Course.GoldPoints); } }
        public int SilverPoints { get { return convert.cInt(Data.Course.SilverPoints); } }
        public int UnlockFinalPoints { get { return convert.cInt(Data.Course.UnlockFinalPoints); } }

        private List<iLesson> m_oLessons;
        private iLesson m_oCourse;
        public List<iLesson> Lessons
        {
            get
            {
                EnumerateLessons();
                return m_oLessons;
            }
        }
        private void EnumerateLessons()
        {
            if (m_oLessons == null)
            {
                m_oLessons = new List<iLesson>();
                if (Data.Course != null)
                {
                    foreach (dynamic lesson in Data.Course.Lessons)
                    {
                        var lessonObject = new ELearnLesson(lesson, this);
                        lessonObject.ParentId = this.Id;
                        if (lessonObject.Id == this.Id)
                        {
                            m_oCourse = lessonObject;
                        }
                        else
                        {
                            m_oLessons.Add(lessonObject);
                            foreach (dynamic subLesson in lesson.Lessons)
                            {
                                m_oLessons.Add(new ELearnLesson(subLesson, this));
                                m_oLessons[m_oLessons.Count - 1].ParentId = lessonObject.Id;
                            }
                        }
                    }
                    if (m_oCourse == null) m_oCourse = new ELearnLesson(Data.Course, this);
                }
            }
        }
        public iLesson Course
        {
            get
            {
                EnumerateLessons();
                return m_oCourse;
            }
        }
    }
}