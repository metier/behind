﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public class Distributor : PhoenixObject
    {
        public Distributor(dynamic data)
        {
            this.InitializeData(data);
        }
        public int Id { get { return convert.cInt(Data.Id); } }
        public string Name { get { return convert.cStr(Data.Name); } }
        public string ContactEmail { get { return convert.cStr(Data.ContactEmail); } }

        private static List<Distributor> m_oDistributors;
        private static object _LockObject = new object();
        public static Distributor GetDistributor(int distributorId)
        {
            lock (_LockObject)
            {
                if (m_oDistributors == null)
                {
                    m_oDistributors = new List<Distributor>();
                    lock (m_oDistributors)
                    {
                        dynamic distributors = new Phoenix.session().PhoenixGet(Phoenix.session.Queries.DistributorsGet, true, true).DataObject;
                        foreach (dynamic distributor in distributors)
                            m_oDistributors.Add(new Distributor(distributor));
                    }
                }
                lock (m_oDistributors) return m_oDistributors.FirstOrDefault(dist => dist.Id == distributorId);
            }
        }
    }
}