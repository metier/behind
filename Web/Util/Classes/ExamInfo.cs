﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public class ExamInfo : PhoenixObject
    {
        public ExamInfo(dynamic data)
        {
            this.InitializeData(data);
        }
        //public string Name { get; set; }
        //public DateTimeOffset? StartStartTime { get; set; }
        //public DateTimeOffset? StartEndTime { get; set; }
        public long? TimeBeforeAvailableInSeconds
        {
            get
            {
                return convert.cLongNullable(Data.TimeBeforeAvailableInSeconds);
            }
        }
        public long DurationInSeconds
        {
            get
            {
                return convert.cLong(Data.DurationInSeconds);
            }
        }
        public int NumberOfQuestions
        {
            get
            {
                return convert.cInt(Data.NumberOfQuestions);
            }
        }
        //public int NumberOfQuestionsPerPage { get; set; }
        //public string Language { get; set; }
        //public Exam Exam { get; set; }
        //public ExamResult Result { get; set; }
    }
}