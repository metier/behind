﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public class ActivityPeriod : PhoenixObject
    {
        public ActivityPeriod(dynamic data)
        {
            this.InitializeData(data);
        }
        public DateTimeOffset? Start { get { return convert.cDateOffsetNullable(true, Data.Start); } }
        public DateTimeOffset? End { get { return convert.cDateOffsetNullable(true, Data.End); } }
    }
}