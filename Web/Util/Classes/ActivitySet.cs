﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public class ActivitySet : PhoenixObject, iActivitySetBase
    {
        public ActivitySet(dynamic data, user user)
        {
            this.InitializeData(data);
            m_oUser = user;
        }
        private user m_oUser;
        public DateTimeOffset? EnrollmentTo
        {
            get
            {
                return convert.cDateOffsetNullable(true, Data.EnrollmentTo);
            }
        }
        public DateTimeOffset? EnrollmentFrom
        {
            get
            {
                return convert.cDateOffsetNullable(true, Data.EnrollmentFrom);
            }
        }
        public int CustomerId { get { return convert.cInt(Data.CustomerId); } }
        public string Name { get { return Data == null ? null : Data.Name; } }
        public int Id { get { return (Data == null ? 0 : convert.cInt(Data.Id)); } }
        private List<Activity> m_oActivity;
        public List<Activity> Activities
        {
            get
            {
                if (m_oActivity == null)
                {
                    m_oActivity = new List<Activity>();
                    if (Data != null) foreach (dynamic activity in Data.Activities) m_oActivity.Add(new Activity(activity, m_oUser));
                    //If more than 2 activities in Set, sort alphabetically
                    //The sorting fails if user is not logged in
                    try
                    {
                        if (m_oActivity.Count(activity => activity.ArticleType == Util.Data.MetierClassTypes.ELearning || activity.ArticleType == Util.Data.MetierClassTypes.Classroom || activity.ArticleType == Util.Data.MetierClassTypes.Exam) > 2)
                            m_oActivity.Sort((a, b) => { return a.ActivityName.Str().CompareTo(b.ActivityName.Str()); });
                        else
                            m_oActivity.Sort((a, b) => { return a.ArticleType == Util.Data.MetierClassTypes.ELearning && b.ArticleType != Util.Data.MetierClassTypes.ELearning ? -1 : a.ArticleType != Util.Data.MetierClassTypes.ELearning && b.ArticleType == Util.Data.MetierClassTypes.ELearning ? 1 : 0; });
                    }
                    catch (Exception) { }

                }
                return m_oActivity;
            }
            //set
            //{
            //    m_oActivity = value;
            //}
        }
        public List<ActivityBase> ActivityBases
        {
            get
            {
                return Activities.Select(activity => { return activity as ActivityBase; }).ToList();
            }
        }
        public double Price
        {
            get
            {
                double price = 0;
                bool activityWithoutPrice = false;
                Activities.ForEach(activity => {
                    if (!activity.UnitPriceNotBillable 
                        && activity.UnitPrice.Dbl() == 0
                        && activity.ArticleType != Util.Data.MetierClassTypes.Accommodation 
                        && activity.ArticleType != Util.Data.MetierClassTypes.Other 
                        && !activity.IsMockExam) activityWithoutPrice = true;
                    //if( && activity.UnitPrice.Dbl()==0) activityWithoutPrice = true;
                    price += activity.UnitPrice.Dbl();
                });
                return (activityWithoutPrice ? 0 : price);
            }
        }
        public List<Activity> GetActivityByType(Util.Data.MetierClassTypes type)
        {
            return GetActivityByType(type.Int());
        }
        public List<Activity> GetActivityByType(int type)
        {
            return Activities.FindAll(activity => activity.ArticleType.Int() == type);
        }
        public Activity PrimaryActivity
        {
            get
            {
                if (Activities.Count == 0) return null;
                return GetActivityByType(Util.Data.MetierClassTypes.ELearning).FirstOrDefault(activity => !activity.IsMockExam) ??
                    GetActivityByType(Util.Data.MetierClassTypes.Classroom).SafeGetValue(0) ??
                    GetActivityByType(Util.Data.MetierClassTypes.Accommodation).SafeGetValue(0) ??
                    GetActivityByType(Util.Data.MetierClassTypes.Exam).SafeGetValue(0) ??
                        Activities[0];
            }
        }
        public List<Util.Classes.Resource> GetResources()
        {
            return GetResources(null, null);
        }
        public List<Util.Classes.Resource> GetResources(Util.Data.ResourceTypes? resourceType, Util.Data.MetierClassTypes? classType)
        {
            List<Util.Classes.Resource> resources = new List<Resource>();
            List<Activity> activities = this.Activities;
            if (classType.HasValue) activities = this.GetActivityByType(classType.Value);
            activities.ForEach(activity => {
                if (resourceType.HasValue) resources.AddRange(activity.ExtendedActivityObject.GetResourcesByType(resourceType.Value));
                else resources.AddRange(activity.ExtendedActivityObject.Resources);
            });
            return resources;
        }
        public bool IsUnenrollmentAllowed
        {
            get
            {
                return this.Data != null && convert.cBool(this.Data.IsUnenrollmentAllowed);
            }
        }
        public DateTimeOffset? UnenrollmentDeadline
        {
            get
            {
                if (this.Data == null || extensions.IsNullOrEmpty(this.Data.UnenrollmentDeadline)) return null;
                return convert.cDateOffset(this.Data.UnenrollmentDeadline);
            }
        }
        public bool CanUnenroll
        {
            get
            {
                return (this.IsUnenrollmentAllowed && this.UnenrollmentDeadline.Date().Date >= DateTime.Now.Date);
            }
        }

        ////Activities come as two different object types -- a skinny object returned from the activities search (course calendar), and a thick object from querying a single activity directly
        //private Activity1 m_oExtendedActivityData;
        //public Activity1 ExtendedActivityData {
        //    get {
        //        if (m_oExtendedActivityData == null) m_oExtendedActivityData = new Activity1(new Phoenix.session().PhoenixGet(String.Format(Phoenix.session.Queries.ActivityGet, ActivityId), true, true).DataObject);
        //        return m_oExtendedActivityData;
        //    }
        //}


        //public double ActivityId { get { return Data.ActivityId; } }
        //public double ActivitySetId { get { return Data.ActivitySetId; } }


        //public double CustomerId { get { return Data.CustomerId; } }

        //public double DistributorId { get { return Data.DistributorId; } }
        //public double ArticleOriginalId { get { return Data.ArticleOriginalId; } }

        //private ActivitySet m_oActivitySetObject;
        //public ActivitySet ActivitySetObject {
        //    get {
        //        lock (this) {
        //            if (ActivitySetId == 0) return null;
        //            if (m_oActivitySetObject == null) m_oActivitySetObject = new ActivitySet(new Phoenix.session().PhoenixGet(String.Format(Phoenix.session.Queries.ActivitySetGet, ActivitySetId), true, true).DataObject);
        //            return m_oActivitySetObject;
        //        }
        //    }
        //}

    }
}