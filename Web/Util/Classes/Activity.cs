﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public class Activity : ActivityBase
    {
        public Activity(dynamic data, user user)
        {
            this.InitializeData(data);
            m_oUser = user;
        }
        public Activity(dynamic data, Phoenix.session session)
        {
            this.InitializeData(data);
            m_oSession = session;
        }


        private ExamInfo m_oExamInfo;
        public ExamInfo ExamInfoObject
        {
            get
            {
                //if (m_oExamInfo == null) 
                m_oExamInfo = new ExamInfo(CurrentSession.PhoenixGet(String.Format(Phoenix.session.Queries.ExamInfoGet, Id), false, false).DataObject);
                return m_oExamInfo;

            }
        }
        public override int ArticleTypeId
        {
            get
            {
                return convert.cInt(this.CustomerArticleObject.Data.ArticleTypeId);
            }
        }
        public bool IsEnrolled
        {
            get
            {
                return Participation != null;
            }
        }
        public Enrollment Participation
        {
            get
            {
                return m_oUser.Enrollments.FirstOrDefault(enrollment => enrollment.ActivityId == this.Id);
            }
        }

        public int Duration
        {
            get
            {
                return convert.cInt(this.Data.Duration);
            }
        }
        private List<Attachment> m_oAttachments;
        public List<Attachment> Attachments
        {
            get
            {
                if (m_oAttachments == null)
                {
                    m_oAttachments = new List<Attachment>();
                    foreach (dynamic attachment in ExtendedActivityObject.Data.Attachments) m_oAttachments.Add(new Attachment(attachment));
                }

                return m_oAttachments;
            }
        }
        public Attachment CaseAttachment
        {
            get
            {
                if (convert.HasProperty(ExtendedActivityObject.Data, "CaseAttachment") && ExtendedActivityObject.Data.CaseAttachment != null) return new Attachment(ExtendedActivityObject.Data.CaseAttachment);
                return null;
            }
        }
        public int MaxParticipants { get { return convert.cInt(Data.MaxParticipants); } }
        public int Id { get { return convert.cInt(Data.Id); } }
        public override int ActivityId { get { return this.Id; } }
        public double? UnitPrice { get { return Data.UnitPrice; } }
        public bool UnitPriceNotBillable { get { return Data.UnitPriceNotBillable; } }
        public int CurrencyCodeId { get { return convert.cInt(Data.CurrencyCodeId); } }
        public string Currency { get { return Phoenix.Helpers.GetCurrency(CurrencyCodeId.Int()); } }
        public string Name { get { return Data.Name; } }
        public override string ActivityName { get { return Name; } }

        public int CustomerArticleId { get { return convert.cInt(Data.CustomerArticleId); } }

        private CustomerArticle m_oCustomerArticleObject;
        public CustomerArticle CustomerArticleObject
        {
            get
            {
                lock (this)
                {
                    //if (m_oCustomerArticleObject == null) 
                    if (m_oCustomerArticleObject == null)
                    {
                        if (this.Data.CustomerArticle != null)
                            m_oCustomerArticleObject = new CustomerArticle(this.Data.CustomerArticle, m_oUser);
                        else
                            m_oCustomerArticleObject = new CustomerArticle(CurrentSession.PhoenixGet(String.Format(Phoenix.session.Queries.CustomerArticleGet, CustomerArticleId), true, true).DataObject, m_oUser);
                    }
                    return m_oCustomerArticleObject;
                }
            }
        }
        private Article m_oArticleOriginalObject;
        public Article ArticleOriginalObject
        {
            get
            {
                lock (this)
                {
                    if (m_oArticleOriginalObject == null && CustomerArticleObject != null && CustomerArticleObject.ArticleOriginalId > 0)
                        m_oArticleOriginalObject = new Article(CurrentSession.PhoenixGet(String.Format(Phoenix.session.Queries.ArticleGet, CustomerArticleObject.ArticleOriginalId), true, true).DataObject, m_oUser);

                    return m_oArticleOriginalObject;
                }
            }
        }

        public override int LanguageId
        {
            get
            {
                return convert.cInt(this.CustomerArticleObject.Data.LanguageId);
            }
        }



        public override int ProductDescriptionId
        {
            get
            {
                return convert.cInt(this.CustomerArticleObject.Data.ProductDescriptionId);
            }
        }
        public int ProductId
        {
            get
            {
                return convert.cInt(this.CustomerArticleObject.Data.ProductId);
            }
        }


        //Use this object for referencing Article for a customer
        private Article m_oArticleCopyObject;
        public Article ArticleCopyObject
        {
            get
            {
                lock (this)
                {
                    if (m_oArticleCopyObject == null && CustomerArticleObject != null && CustomerArticleObject.ArticleCopyId > 0)
                        m_oArticleCopyObject = new Article(CurrentSession.PhoenixGet(String.Format(Phoenix.session.Queries.ArticleGet, CustomerArticleObject.ArticleCopyId), true, true).DataObject, m_oUser);

                    return m_oArticleCopyObject;
                }
            }
        }


        public string GetDescription()
        {
            return ProductDescriptionObject.GetTextPart(this.LanguageId, 1);
        }
        public string GetCourseInfo()
        {
            return ProductDescriptionObject.GetTextPart(this.LanguageId, 30);
        }

        public override string ArticleNumber { get { return CustomerArticleObject.ArticleNumber; } }
        public override string VersionNumber { get { return CustomerArticleObject.VersionNumber; } }
        
        public DateTimeOffset? CaseExamEndDate
        {
            get
            {
                if (ActivityStart == null && ActivityEnd == null) return null;
                DateTimeOffset? caseDueDate = this.ActivityEnd;
                if (!caseDueDate.HasValue) caseDueDate = this.ActivityStart.Value.AddSeconds(this.Duration / 1000);
                return caseDueDate;
            }
        }
        public DateTimeOffset? EndDate
        {
            get
            {
                //todo calculate based on duration
                if (Data.EndDate != null) return convert.cDateOffset(Data.EndDate);
                return null;
            }
        }        
        public override DateTimeOffset? ActivityStart
        {
            get
            {
                foreach (var period in Data.ActivityPeriods)
                {
                    string s = period.Start;
                    if (!s.IsNullOrEmpty()) return s.DteOffset();
                }
                return null;
            }
        }
        public override DateTimeOffset? ActivityEnd
        {
            get
            {
                string s = "";
                foreach (var period in Data.ActivityPeriods)
                    if (convert.cStr(period.End) != "") s = period.End;
                if (s.IsNullOrEmpty()) return null;
                else return s.DteOffset();
            }
        }
        private List<ActivityPeriod> m_oActivityPeriods;
        public override List<ActivityPeriod> ActivityPeriods
        {
            get
            {
                if (m_oActivityPeriods == null)
                {
                    m_oActivityPeriods = new List<ActivityPeriod>();
                    foreach (var period in Data.ActivityPeriods)
                        m_oActivityPeriods.Add(new ActivityPeriod(period));

                    m_oActivityPeriods.Sort((x, y) => x.Start.Dte().CompareTo(y.Start.Dte()));
                }
                return m_oActivityPeriods;
            }
        }
        //todo
        public int ExamFKID { get { return 0; } }
    }
}