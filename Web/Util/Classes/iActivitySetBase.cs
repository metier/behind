﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public interface iActivitySetBase
    {
        int Id { get; }
        string Name { get; }
        List<ActivityBase> ActivityBases { get; }
    }
}