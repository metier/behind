﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public class CustomerArticle : PhoenixObject
    {
        public CustomerArticle(dynamic data, user user)
        {
            this.InitializeData(data);
            m_oUser = user;
        }
        private user m_oUser;
        public int ArticleOriginalId { get { return convert.cInt(Data.ArticleOriginalId); } }
        public int ArticleCopyId { get { return convert.cInt(Data.ArticleCopyId); } }
        public string ArticleNumber { get { string s = convert.cStr(Data.ArticlePath); return s.Substr(0, 5); } }
        public string VersionNumber { get { return Data.ArticlePath; } }
    }
}