﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Xml;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using System.Linq;
using System.Data;
using System.Data.OleDb;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using jlib.db;
using jlib.functions;
using System.Security.Cryptography;
using System.Text;
using SpreadsheetGear;

/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public abstract class ActivityBase : PhoenixObject
    {
        protected user m_oUser;
        protected Phoenix.session m_oSession;
        public Phoenix.session CurrentSession
        {
            get
            {
                if (m_oUser != null) return m_oUser.PhoenixSession;
                if (m_oSession == null) return new Phoenix.session();
                return m_oSession;
            }
            set
            {
                m_oSession = value;
            }
        }

        public string MomentTimezoneName { get { return Data.MomentTimezoneName; } }
        public virtual DateTimeOffset? ActivityStart
        {
            get
            {
                return null;
            }
        }
        public virtual DateTimeOffset? ActivityEnd
        {
            get
            {                
                return null;
            }
        }

        private ExtendedActivity m_oExtendedActivity;
        public ExtendedActivity ExtendedActivityObject
        {
            get
            {
                if (m_oExtendedActivity == null) m_oExtendedActivity = new ExtendedActivity(CurrentSession.PhoenixGet(String.Format(Phoenix.session.Queries.ActivityGet, ActivityId), true, true).DataObject);
                return m_oExtendedActivity;
            }
        }
        public List<Resource> Ecoaches
        {
            get
            {
                return ExtendedActivityObject.GetResourcesByType(Util.Data.ResourceTypes.ECoach);
            }
        }
        public List<Resource> Instructors
        {
            get
            {
                return ExtendedActivityObject.GetResourcesByType(Util.Data.ResourceTypes.Instructor);
            }
        }
        public List<Resource> Locations
        {
            get
            {
                return ExtendedActivityObject.GetResourcesByType(Util.Data.ResourceTypes.Location);
            }
        }
        public List<Resource> Invigilators
        {
            get
            {
                return ExtendedActivityObject.GetResourcesByType(Util.Data.ResourceTypes.Invigilator);
            }
        }
        public List<Resource> StudyAdvisors
        {
            get
            {
                return ExtendedActivityObject.GetResourcesByType(Util.Data.ResourceTypes.StudyAdvisor);
            }
        }        
        public bool IsCaseExam
        {
            get
            {
                return ArticleTypeId == 4;// "Case Exam";
            }
        }
        public bool IsProjectAssignment
        {
            get
            {
                return ArticleTypeId == 5; // "Project Assignment";
            }
        }
        public bool IsMPCExam
        {
            get
            {
                return ArticleTypeId == 3 // "Multiple Choice" 
                    || ArticleTypeId == 6;// "Certification";
            }
        }
        public bool IsExternalCertification
        {
            get
            {
                return ArticleTypeId == 9; // "External Certification"                         
            }
        }
        public bool IsMockExam
        {
            get
            {
                return ArticleTypeId == 8;
            }
        }
        public bool IsElearning
        {
            get
            {
                return ArticleTypeId == 1;
            }
        }
        public Util.Data.MetierClassTypes ArticleType
        {
            get
            {
                var article = ArticleTypeId;
                if (article == 7) return Util.Data.MetierClassTypes.Other;
                if (article == 1 || IsMockExam) return Util.Data.MetierClassTypes.ELearning;
                if (article == 2) return Util.Data.MetierClassTypes.Classroom;
                if (IsCaseExam || IsProjectAssignment || IsMPCExam || IsExternalCertification) return Util.Data.MetierClassTypes.Exam;
                if (Data.ArticleType.Name == "Daypack") return Util.Data.MetierClassTypes.Accommodation;
                return Util.Data.MetierClassTypes.Unknown;
            }
        }
        

        public bool IsImportedHistory
        {
            get
            {
                return (RcoCourseId > 0 && ELearnCourseObject != null && ELearnCourseObject.Lessons.Count == 0);
            }
        }
        public int RcoCourseId { get { return convert.cInt(convert.SafeGetProperty(Data, "RcoCourseId")); } }
        public int RcoExamContainerId { get { return convert.cInt(convert.SafeGetProperty(Data, "RcoExamContainerId")); } }
        private iObjectWithLessons m_oELearnCourseObject;
        public iObjectWithLessons ELearnCourseObject
        {
            get
            {
                if (convert.cInt(RcoCourseId, RcoExamContainerId) > 0)
                {
                    if (m_oELearnCourseObject == null)
                        //Todo: Phoenix should return Child-Performances for mock exams
                        //m_oELearnCourseObject = m_oUser.RcoObjects.SafeGetValue(convert.cInt(RcoCourseId, RcoExamContainerId));
                        m_oELearnCourseObject = m_oUser.RcoObjects.SafeGetValue(convert.cInt(RcoCourseId));

                    //The User has no progress on the RCO, so it's not included in RcoObjects
                    if (m_oELearnCourseObject == null)
                    {
                        dynamic rco = CurrentSession.PhoenixGet(String.Format(Phoenix.session.Queries.UserPerformancesGet, m_oUser.ID, "", this.ActivityId), true, false).DataObject;
                        if (rco != null)
                        {
                            if (RcoCourseId > 0) m_oELearnCourseObject = new ELearnCourse(rco);
                            else m_oELearnCourseObject = new MockExamCourse(rco);
                        }
                    }
                    if (m_oELearnCourseObject != null && (m_oELearnCourseObject.Data.Course == null && m_oELearnCourseObject.Data.ExamContainer == null)) return null;
                }
                return m_oELearnCourseObject;
            }
        }
        public int NumberOfQuestions
        {
            get
            {
                return convert.cInt(convert.SafeGetProperty(ExtendedActivityObject.Data, "NumberOfQuestions"));
            }
        }

        public int NumberOfParticipants
        {
            get
            {
                return ExtendedActivityObject.Data.Participants.Count();
            }
        }
        public int CustomerId
        {
            get
            {
                return m_oUser.OrganizationID;
            }
        }
        private ProductDescription m_oProductDescriptionObject;
        public ProductDescription ProductDescriptionObject
        {
            get
            {
                if (this.ProductDescriptionId == 0) return new ProductDescription(null);
                lock (this)
                {
                    if (m_oProductDescriptionObject == null) m_oProductDescriptionObject = new ProductDescription(CurrentSession.PhoenixGet(String.Format(Phoenix.session.Queries.ProductDescriptionGet, this.ProductDescriptionId, CustomerId), true, true).DataObject);
                    return m_oProductDescriptionObject;
                }
            }
        }
        public Enrollment Enrollment {
            get {
                if (this as Enrollment != null) return this as Enrollment;
                return (this as Activity).Participation;
            }
        }    
        public int? EnrollmentId {
            get {
                if (Enrollment == null) return null;
                return Enrollment.Id;
            }
        }
        public virtual int ArticleTypeId { get { return -1; } }
        public virtual int ProductDescriptionId { get { return 0; } }
        public virtual int LanguageId { get { return 0; } }
        public virtual int ActivityId { get { return 0; } }        
        public virtual string ActivityName { get { return ""; } }
        public virtual string ArticleNumber { get { return ""; } }
        public virtual string VersionNumber { get { return ""; } }
        public virtual List<ActivityPeriod> ActivityPeriods { get { return null; } }
    }
}