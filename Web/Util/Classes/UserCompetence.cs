﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public class UserCompetence : PhoenixObject
    {
        public UserCompetence(dynamic data)
        {
            this.InitializeData(data);
        }
        public DateTimeOffset? DateOfBirth { get { if (convert.cStr(Data.DateOfBirth).IsNullOrEmpty()) return null; else return convert.cDateOffset(Data.DateOfBirth); } }
        public string CountryOfBirth { get { return Data.CountryOfBirth; } }
        public string Nationality { get { return Data.Nationality; } }
        public string Comment { get { return Data.Comment; } }

    }
}