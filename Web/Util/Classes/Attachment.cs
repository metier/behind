﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public class Attachment : PhoenixObject
    {
        public Attachment(dynamic data)
        {
            this.InitializeData(data);
        }
        public string Title { get { return Data.Title; } }
        public string Description { get { return Data.Description; } }
        //public int Size { get { return convert.cInt(Data.Size); } }
        public string Url
        {
            get
            {
                return System.Web.HttpContext.Current.Request.ApplicationPath + "/download.aspx?FileId=" + Data.FileId + "&secret=" + Util.Permissions.encryptString(Data.FileId + System.Configuration.ConfigurationManager.AppSettings["phoenix.api.token"]) + "&Desc=" + System.Web.HttpUtility.UrlEncode(this.Description);
            }
        }
    }
}