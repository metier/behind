﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public class ExtendedActivity : PhoenixObject
    {
        public ExtendedActivity(dynamic data)
        {
            this.InitializeData(data);
        }
        private List<Resource> m_oResources;
        public List<Resource> Resources
        {
            get
            {
                if (m_oResources == null)
                {
                    m_oResources = new List<Resource>();
                    foreach (dynamic resource in Data.Resources) m_oResources.Add(new Resource(resource));
                }
                return m_oResources;
            }
        }
        public List<Resource> GetResourcesByType(Util.Data.ResourceTypes type)
        {
            return Resources.FindAll(resource => { return resource.ResourceTypeId == (int)type; });
        }
    }
}