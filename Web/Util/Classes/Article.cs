﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;

/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public class Article : PhoenixObject
    {
        public Article(dynamic data, user user)
        {
            this.InitializeData(data);
            m_oUser = user;            
        }
        private user m_oUser;        

        public int PDU { get { return convert.cInt(convert.SafeGetProperty(Data, "PDU")); } }
        public string ActivityNumber { get { return convert.cStr(convert.SafeGetProperty(Data, "ActivityNumber")); } }
        public string ActivityName { get { return convert.cStr(convert.SafeGetProperty(Data, "ActivityName")); } }
        public string ProviderNumber { get { return convert.cStr(convert.SafeGetProperty(Data, "ProviderNumber")); } }
        public string ProviderName { get { return convert.cStr(convert.SafeGetProperty(Data, "ProviderName")); } }


        public int NumberOfQuestions
        {
            get
            {
                return convert.cInt(convert.SafeGetProperty(Data, "NumberOfQuestions"));
            }
        }
        public int Duration
        {
            get
            {
                return convert.cInt(convert.SafeGetProperty(Data, "Duration"));
            }
        }
        public bool IsUserCompetenceInfoRequired
        {
            get
            {
                return convert.cBool(convert.SafeGetProperty(Data, "IsUserCompetenceInfoRequired"));
            }
        }
        //public string ArticleNumber { get { string s = convert.cStr(Data.ArticlePath); return s.Substr(0, 5); } }
        //public string VersionNumber { get { return Data.ArticlePath; } }
        public string Title { get { return Data.Title; } }
        public int Id { get { return convert.cInt(Data.Id); } }
        
        private List<Attachment> m_oAttachments;
        public List<Attachment> Attachments
        {
            get
            {
                if (m_oAttachments == null)
                {
                    m_oAttachments = new List<Attachment>();
                    foreach (dynamic attachment in Data.Attachments) m_oAttachments.Add(new Attachment(attachment));
                }
                return m_oAttachments;
            }
        }
    }
}