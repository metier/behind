﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public class ProductDescription : PhoenixObject
    {
        public ProductDescription(dynamic data)
        {
            this.InitializeData(data);
        }
        public string GetTextPart(int languageId, int partId)
        {
            if (this.Data != null)
            {
                foreach (dynamic description in this.Data.ProductDescriptionTexts)
                {
                    //if (convert.HasProperty(descriptions, "ProductDescriptionTexts")) {
                    //foreach (dynamic description in descriptions.ProductDescriptionTexts) {
                    if (convert.HasProperty(description, "ProductTemplatePart"))
                    {
                        if (convert.cInt(description.ProductTemplatePart.LanguageId) == languageId && convert.cInt(description.ProductTemplatePart.Order) == partId) return description.Text;
                    }
                    //  }
                    //}
                }
            }
            return "";
        }
        public string ImageUrl
        {
            get
            {
                string fileId = convert.cStr(convert.SafeGetProperty(Data, "Attachment", "FileId"));
                if (fileId.IsNullOrEmpty()) return "";
                return System.Web.HttpContext.Current.Request.ApplicationPath + "/download.aspx?FileId=" + fileId + "&secret=" + Util.Permissions.encryptString(fileId + System.Configuration.ConfigurationManager.AppSettings["phoenix.api.token"]);
            }
        }
    }
}