﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public class ELearnLesson : PhoenixObject, iLesson
    {
        public ELearnLesson(dynamic data, ELearnCourse course)
        {
            this.InitializeData(data);
            m_oCourse = course;
            foreach (dynamic performance in course.Data.Performances)
            {
                if (convert.cInt(performance.RcoId) == Id)
                {
                    m_oPerformance = performance;
                    break;
                }
            }
            if(NumSubLessons==0) this.StartingUrl = System.Web.HttpContext.Current.Request.ApplicationPath + "/player/player.aspx?CourseID=" + m_oCourse.Id + "&LessonID=" + this.Id;
        }
        private dynamic m_oPerformance;
        private ELearnCourse m_oCourse;
        public string StartingUrl { get; set; }
        public string Name { get { return Data.Name; } }
        public bool IsFinalTest { get { return convert.cBool(convert.SafeGetProperty(Data, "IsFinalTest")); } }
        public int Id { get { return convert.cInt(Data.Id); } }
        public int ParentId { get; set; }
        public int Weight { get { return (convert.SafeGetProperty(Data, "Weight") == null ? 1 : convert.cInt(Data.Weight)); } }
        public string UserLessonStatus
        {
            get
            {
                return (m_oPerformance == null ? "" : m_oPerformance.Status);
            }
        }
        public DateTimeOffset? UserLessonCompletedDate
        {
            get
            {
                return (m_oPerformance == null || m_oPerformance.CompletedDate == null || UserLessonStatus == "N" ? null : convert.cDateOffset(m_oPerformance.CompletedDate));
            }
        }
        public int UserNumPoints
        {
            get
            {
                return (m_oPerformance == null ? 0 : convert.cInt(m_oPerformance.Score));
            }
        }
        public int NumSubLessons
        {
            get
            {
                return convert.cInt(Data.Lessons.Count());
            }
        }

    }
}