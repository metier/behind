﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;

/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public class ActivityAvailability : PhoenixObject
    {
        public ActivityAvailability(dynamic data)
        {
            this.InitializeData(data);
        }

        private List<ActivityPeriod> m_oActivityPeriods;
        public List<ActivityPeriod> ActivityPeriods
        {
            get
            {
                if (m_oActivityPeriods == null)
                {
                    m_oActivityPeriods = new List<ActivityPeriod>();
                    foreach (var period in Data.ActivityPeriods)
                        m_oActivityPeriods.Add(new ActivityPeriod(period));

                    m_oActivityPeriods.Sort((x, y) => x.Start.Dte().CompareTo(y.Start.Dte()));
                }
                return m_oActivityPeriods;
            }
        }
        public ActivityPeriod CurrentOrNextActivityPeriod
        {
            get
            {
                foreach (var period in ActivityPeriods)
                    if (period.Start >= DateTime.Now) return period;
                return null;
            }
        }
        public bool IsAvailable
        {
            get
            {
                return convert.cBool(convert.SafeGetProperty(this.Data, "IsAvailable"));
            }
        }
        public DateTimeOffset? AvailabilityStart { get { return (convert.cStr(convert.SafeGetProperty(Data, "AvailabilityStart")) == "" ? null : convert.cDateOffset(Data.AvailabilityStart)); } }
        public DateTimeOffset? AvailabilityEnd { get { return (convert.cStr(convert.SafeGetProperty(Data, "AvailabilityEnd")) == "" ? null : convert.cDateOffset(Data.AvailabilityEnd)); } }
        public DateTimeOffset? CalculatedAvailabilityStart { get { return (convert.cStr(convert.SafeGetProperty(Data, "AvailabilityStart")) != "" ? convert.cDateOffset(Data.AvailabilityStart) : (CurrentOrNextActivityPeriod != null ? CurrentOrNextActivityPeriod.Start : null)); } }
        public DateTimeOffset? CalculatedAvailabilityEnd { get { return (convert.cStr(convert.SafeGetProperty(Data,"AvailabilityEnd")) != "" ? convert.cDateOffset(Data.AvailabilityEnd) : (ActivityPeriods.Count() > 0 ? ActivityPeriods[ActivityPeriods.Count() - 1].End : null)); } }
    }
}