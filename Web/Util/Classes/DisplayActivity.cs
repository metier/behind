﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{
    public class DisplayActivity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string ButtonTitle { get; set; }
        public string ActivityType { get; set; }
        public int ELessons { get; set; }
        public int ELessonsCompleted { get; set; }
        public string CMonth { get; set; }
        public int CDay { get; set; }
        public bool Enrolled { get; set; }
        public bool Completed { get; set; }
        public string Status { get; set; }

        public DisplayActivity(Util.Classes.ActivityBase Activity, Util.Classes.user User, jlib.components.webpage Page)
        {
            var enrollment = User.Enrollments.FirstOrDefault(e => e.ActivityId == Activity.ActivityId);
            this.Id = Activity.ActivityId;
            this.Name = Activity.ActivityName;

            this.ImageUrl = "./inc/library/exec/thumbnail.aspx?z=1&u=" + System.Web.HttpUtility.UrlEncode(Util.MyMetier.getCourseImage(convert.cInt(User.Organization.ParentCompanyID, User.Organization.ID), Activity)) + "&w=240&h=120";
            this.ButtonTitle = jlib.helpers.translation.translate(Page.Language, "", Page, Activity.ArticleType == Util.Data.MetierClassTypes.Exam ? "button-exam-info" : "button-course-info");
            this.ActivityType = Activity.ArticleType == Util.Data.MetierClassTypes.Exam ? "exam" : Activity.ArticleType == Util.Data.MetierClassTypes.Classroom ? "classroom" : Activity.ArticleType == Util.Data.MetierClassTypes.ELearning ? "elearning" : "other";
            this.Enrolled = enrollment != null;
            if (this.Enrolled)
            {
                if (enrollment.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.NoShow) this.Completed = false;
                else if (enrollment.ParticipantStatusCodeValue == Util.Classes.Enrollment.ParticipantStatusCodes.Completed) this.Completed = true;
                else if (Activity.ArticleType == Util.Data.MetierClassTypes.Classroom && ((Activity.ActivityStart != null && Activity.ActivityStart.Dte() > DateTime.Now) || (Activity.ActivityEnd != null && Activity.ActivityEnd.Dte() > DateTime.Now))) this.Completed = false;
                this.Status = enrollment.ParticipantStatusCodeValue;
            }

            if (Activity.ArticleType == Util.Data.MetierClassTypes.Classroom)
            {
                this.CMonth = convert.cEllipsis(Util.MyMetier.getUserCulture(Page.Language).DateTimeFormat.MonthNames[(int)convert.cDate(Activity.ActivityStart).Month - 1], 3, false);
                this.CDay = convert.cDate(Activity.ActivityStart).Day;
            }
            else if (Enrolled && Activity.ArticleType == Util.Data.MetierClassTypes.ELearning && Activity.ELearnCourseObject != null)
            {
                if (!Activity.IsImportedHistory)
                {
                    this.ELessons = Activity.ELearnCourseObject.Lessons.FindAll(lesson => { return !lesson.IsFinalTest; }).Count;
                    this.ELessonsCompleted = enrollment.LessonsCompleted;
                }                
            }

        }
    }
}