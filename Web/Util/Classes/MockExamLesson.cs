﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.db;
using jlib.functions;


/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Classes
{

    public class MockExamLesson : PhoenixObject, iLesson
    {
        public dynamic LessonStatus { get; private set; }
        public MockExamLesson(dynamic data, dynamic status)
        {
            this.InitializeData(data);
            LessonStatus = status;
            this.StartingUrl = System.Web.HttpContext.Current.Request.ApplicationPath + "/exam/start.aspx?id=" + this.Id;
        }
        public int Id { get { return convert.cInt(Data.Id); } }
        public string Name { get { return convert.cStr(Data.Name); } }
        public string StartingUrl { get; set; }
        public bool IsFinalTest { get { return false; } }
        public int ParentId { get; set; }
        public int Weight { get { return 1; } }
        public string UserLessonStatus { get { return (LessonStatus == null ? "" : convert.cStr(LessonStatus.Status)); } }
        public DateTimeOffset? UserLessonCompletedDate
        {
            get
            {
                if (LessonStatus == null || convert.cStr(LessonStatus.CompletedDate) == "" || UserLessonStatus == "N") return null;
                return convert.cDateOffset(LessonStatus.CompletedDate);
            }
        }
        public int UserNumPoints { get { return (LessonStatus == null ? 0 : convert.cInt(LessonStatus.Score)); } }
        public int NumSubLessons { get { return 0; } }
    }
}