﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Data;
using jlib.db;
using jlib.functions;
using System.Security.Cryptography;
using System.Text;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Util
{
    public class Permissions
    {
        public static bool logInIfRequired(string sAuthentication)
        {
            return logInIfRequired(sAuthentication, 0);
        }
        public static bool logInIfRequired(string sAuthentication, int iReleaseID)
        {
            sAuthentication = decryptStringWithHash(sAuthentication);
            if (sAuthentication == "") return false;
            if (convert.cInt(parse.splitValue(sAuthentication, "|", 0)) != UserID)
            {
                //DataTable oUser = util.executeDefaultFrontendConnection("SELECT ILA_USER.*, GET_PARENT_COMPANY(ILA_USER.USER_GROUP_ID) AS PARENT_ORG_ID FROM ILA_USER WHERE ILA_USER.ID=" + convert.cInt(parse.splitValue(sAuthentication, "|", 0)));
                //DataTable oPermissionLevel = util.executeDefaultFrontendConnection("SELECT LOGICAL_GROUP_ID FROM CONDITION_MATCH, CONDITION WHERE CONDITION.ID=CONDITION_MATCH.CONDITION_ID AND CONDITION.LOGICAL_GROUP_ID IN (1602280,1602281) AND CONDITION_MATCH.USER_ID=" + oUser.Rows[0]["id"] + " ORDER BY CONDITION.LOGICAL_GROUP_ID");
                //permissions.setLoginData(true, convert.cInt(oUser.Rows[0]["ID"]), convert.cInt(oUser.Rows[0]["PARENT_ORG_ID"]), (oPermissionLevel.Rows.Count == 0 ? permissions.UserType.User : (convert.cInt(oPermissionLevel.Rows[0]["LOGICAL_GROUP_ID"]) == 1602280 ? permissions.UserType.SuperAdmin : permissions.UserType.Admin)), DateTime.Now.AddHours(3), iReleaseID);
                Permissions.setLoginData(true, convert.cInt(parse.splitValue(sAuthentication, "|", 0)), 0, UserType.User, DateTime.Now.AddHours(3), iReleaseID);
                return true;
            }
            return false;
        }
        public static string setLoginData(bool bUpdateCookie, int iUserID, int iUserGroupID, Permissions.UserType oUserType, DateTime oCookieExpiration, int iLastReleaseID)
        {
            string sValue = iUserID + " " + oCookieExpiration.ToString().Replace(" ", "+") + " " + (int)oUserType + " " + iUserGroupID + " " + iLastReleaseID;
            if (bUpdateCookie)
            {
                HttpCookie oCookie = new HttpCookie("user-authentication", encryptStringWithHash(sValue));
                System.Web.HttpContext.Current.Response.Cookies.Add(oCookie);
                System.Web.HttpContext.Current.Request.Cookies.Remove(oCookie.Name);
                System.Web.HttpContext.Current.Request.Cookies.Add(oCookie);
            }
            System.Web.HttpContext.Current.Session["user.id"] = iUserID;
            //System.Web.HttpContext.Current.Session["user.type"] = (int)oUserType;
            //System.Web.HttpContext.Current.Session["user.parent_org_id"] = iUserGroupID;
            System.Web.HttpContext.Current.Session["user.last_release_id"] = iLastReleaseID;
            return sValue;
        }


        public static string encryptStringWithHash(string s)
        {
            return s + "|" + Convert.ToBase64String(new MD5CryptoServiceProvider().ComputeHash(ASCIIEncoding.Default.GetBytes(s + "|" + ConfigurationManager.AppSettings["sql.dsn.frontend"])));
        }
        public static string decryptStringWithHash(string s)
        {
            s = convert.cStr(s);
            if (s == "" || s.IndexOf("|") == -1) return "";
            string sValue = s.Substring(0, s.LastIndexOf("|"));
            if (encryptStringWithHash(sValue) == s) return sValue;
            return "";
        }
        public static string encryptString(string s)
        {
            return Convert.ToBase64String(new MD5CryptoServiceProvider().ComputeHash(ASCIIEncoding.Default.GetBytes(s)));
        }
        public static int UserIDFromHttpContext
        {
            get
            {
                return convert.cInt(System.Web.HttpContext.Current.Session["user.id"]);
            }
        }
        public static int UserID
        {
            get
            {
                return convert.cInt(new Util.Phoenix.session().GetUserId(), UserIDFromHttpContext);
            }
        }
        //public static UserType Usertype {
        //    get {
        //        return (UserType)convert.cInt(System.Web.HttpContext.Current.Session["user.type"]);
        //    }
        //}

        public enum UserType
        {
            User = 3,
            SuperAdmin = 1,
            Admin = 2
        }
        public static void setSetting(string sKey, string sSubKey, int userId, object oValue)
        {
            ado_helper oData = new ado_helper("sql.dsn.cms");
            sqlbuilder.getDataTable(oData, "delete from d_user_settings where key_name=@key and sub_key=@sub_key and user_id=@user_id; insert into d_user_settings(key_name,sub_key,user_id,value)values(@key,@sub_key,@user_id,@value);", "@key", sKey, "@sub_key", sSubKey, "@user_id", userId, "@value", oValue);
        }
        public static string getSetting(string sKey, string sSubKey, int userId, object oDefaultValue)
        {
            ado_helper oData = new ado_helper("sql.dsn.cms");
            return convert.cStr(sqlbuilder.executeSelectValue(oData, "d_user_settings", "value", "key_name", sKey, "sub_key", sSubKey, "user_id", userId));
        }
        public static int initiateAttempt(int iReleaseID, int userId, int rcoId, int classroomId, string scormSession)
        {
            ado_helper oData = new ado_helper("sql.dsn.cms");
            AttackData.Release Release = null;
            if (iReleaseID == 0 && rcoId > 0)
            {
                AttackData.RowLink examLink = AttackData.RowLinks.GetByField(ID1: rcoId, Table1: "d_folders", Table2: "d_exams").Execute().FirstOrDefault();

                if (examLink != null)
                {
                    //AttackData.Exam exam = (link.Object2 as AttackData.Exam);
                    Release = AttackData.Releases.GetByField(ObjectID: examLink.ID2).OrderBy(AttackData.Releases.Columns.CreateDate, jlib.DataFramework.QueryBuilder.OrderByDirections.Descending).Execute().FirstOrDefault();
                }
            }
            if (iReleaseID > 0)
            {
                Release = AttackData.Release.LoadByPk(iReleaseID);
            }
            if (Release == null) throw new Exception("Release for RcoID " + rcoId + " not found");
            DataTable oDBExam = sqlbuilder.getDataTable(oData, "select * from d_exams where ObjectID=@ObjectID and @ObjectDate between CreateDate and DeleteDate", "@ObjectID", Release.ObjectID, "@ObjectDate", Release.ObjectDate);
            if (rcoId == 0) rcoId = AttackData.RowLinks.GetByField(Table1: "d_folders", Table2: "d_exams", ID2: Release.ObjectID).Execute().FirstOrDefault().ID1;
            var oExam = new Util.Exam.exam();            
            oExam.populate(oDBExam.Rows[0], Release.ObjectDate);
            AttackData.Attempt Attempt = new AttackData.Attempt();
            Attempt.MaxPoints = sqlbuilder.getDataTable(oData, "select SUM(Points) from d_questions where ObjectID in (select ObjectID from fn_get_links(" + Release.ObjectID + ", 'd_exams','" + Release.ObjectDate + "',0,'') where TableName='d_questions') AND '" + Release.ObjectDate + "' BETWEEN CreateDate and DeleteDate").SafeGetValue(0, 0).Int();
            Attempt.StatusID = (int)Util.Data.AttemptState.Incomplete;
            Attempt.MaxTime = convert.cInt(Release.MaxTime, oDBExam.Rows[0]["MaxTime"]);
            Attempt.UserID = userId;
            Attempt.ReleaseID = Release.ID;
            Attempt.CreateDate = DateTime.Now;
            Attempt.ObjectDate = (Release.ObjectDate > DateTime.Now ? DateTime.Now : Release.ObjectDate);
            Attempt.RcoID = rcoId;
            Attempt.ClassroomID = classroomId;
            Attempt.ScormSession = scormSession;
            Attempt.Save();

            if ((oExam.PlayerType == "prince_2f" || oExam.PlayerType == "assessment-dnv") && oExam.Settings.SafeGetValue("randomize-questions").Bln())
            {
                List<Exam.exam_question> Questions = new List<Exam.exam_question>();
                oExam.Sections.ForEach(Section => {
                    AddQuestionsFromSection(Questions, Section);
                });
                Questions.Shuffle();
                for (int x = 0; x < Questions.Count; x++)
                {
                    new AttackData.AttemptAnswer()
                    {
                        AttemptID = Attempt.ID,
                        QuestionID = Questions[x].ObjectID,
                        QuestionOrder = x
                    }.Save();
                }
            }
            return Attempt.ID;
        }
        public static void AddQuestionsFromSection(List<Exam.exam_question> questions, Exam.exam_section section)
        {
            section.Questions.ForEach(Question => { questions.Add(Question); });
            section.Sections.ForEach(Sec => { AddQuestionsFromSection(questions, Sec); });
        }
    }


}