﻿using System;
using System.Collections.Generic;
using System.Data;
using jlib.db;
using jlib.functions;
using AttackData = Phoenix.LearningPortal.Data;

/// <summary>
/// Summary description for export
/// </summary>
namespace Phoenix.LearningPortal.Util{          
    public class Export {
        public static string exportExamResponses(AttackData.Releases Releases, int iActiveUserID) {
            ado_helper oData = new ado_helper("sql.dsn.cms");
            SpreadsheetGear.IWorkbook WorkbookFile = SpreadsheetGear.Factory.GetWorkbook();
            int SheetNumber = 0, RowCounter = 0;
            WorkbookFile.Worksheets[SheetNumber].Name = "Answers";
            var oValues = (SpreadsheetGear.Advanced.Cells.IValues)WorkbookFile.Worksheets[SheetNumber];

            string[] sColHeadings = new string[] { "Attempt Date", "Username", "First Name", "Last Name", "Organization", "Offering", "Question Section", "Question", "Answer", "Time", "Points", "Attempt Status", "Release Name" };

            for (int y = 0; y < sColHeadings.Length; y++) {
                WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, y].Font.Size = 12;
                WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, y].Interior.Color = System.Drawing.Color.FromArgb(91, 0, 0);
                WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, y].Font.Color = System.Drawing.Color.White;
                WorkbookFile.Worksheets[SheetNumber].Cells[RowCounter, y].Font.Bold = true;
                oValues.SetText(RowCounter, y, sColHeadings[y]);
            }
            RowCounter++;

            Releases.ForEach(oRelease => {

                AttackData.Attempts oAttempts = AttackData.Attempts.GetByField(ReleaseID: oRelease.ID).Execute();
                Dictionary<int, Util.Classes.Activity> Classrooms = new Dictionary<int, Util.Classes.Activity>();
                List<int> AttemptedClassroomLookups = new List<int>();
                for (int x = 0; x < oAttempts.Count; x++) {
                    DataTable oDBExam1 = sqlbuilder.getDataTable(oData, "select * from d_exams where ObjectID=@ObjectID and @ObjectDate between CreateDate and DeleteDate", "@ObjectID", oRelease.ObjectID, "@ObjectDate", oAttempts[x].ObjectDate);
                    DataTable oExamQuestions = sqlbuilder.getDataTable(oData,
    @"select d_exam_sections.Name as ExamSection,
    d_questions.QuestionType, d_questions.Text, d_questions.Reason, d_attempt_answers.Time_Spent, d_attempt_answers.Points, d_attempt_answers.Answer, d_attempt_answers.Is_Correct, d_question_options.Text as MCText
from 
	d_questions
    left outer join l_row_links l_question_section on l_question_section.id2=d_questions.ObjectID and l_question_section.table2='d_questions' and l_question_section.table1='d_exam_sections' and @ObjectDate between l_question_section.CreateDate and l_question_section.DeleteDate
    left outer join d_exam_sections on d_exam_sections.ObjectID=l_question_section.id1 and @ObjectDate between d_exam_sections.CreateDate and d_exam_sections.DeleteDate
	left outer join d_attempt_answers on d_attempt_answers.question_id=d_questions.ObjectID and attempt_id=@AttemptID
	left outer join l_row_links on l_row_links.id1=d_attempt_answers.id and l_row_links.table1='d_attempt_answers' and l_row_links.table2='d_question_options' and @ObjectDate between l_row_links.CreateDate and l_row_links.DeleteDate
	left outer join d_question_options on d_question_options.ObjectID=l_row_links.id2 and @ObjectDate between d_question_options.CreateDate and d_question_options.DeleteDate
    , dbo.fn_get_links(@ObjectID, 'd_exams', @ObjectDate, 0,0) link_table where link_table.TableName='d_questions'
    and d_questions.ObjectID=link_table.ObjectID and @ObjectDate between d_questions.CreateDate and d_questions.DeleteDate order by link_table.Sort", "@AttemptID", oAttempts[x].ID, "@ObjectID", oRelease.ObjectID, "@ObjectDate", oAttempts[x].ObjectDate);

                    int iQuestionCounter = 0;
                    while (true) {
                        oValues.SetText(RowCounter, 0, String.Format("{0:MM}{0:dd}{0:yy}-{0:HH}{0:mm}{0:ss}", oAttempts[x].CreateDate));
                        if (oAttempts[x].User != null) {
                            oValues.SetText(RowCounter, 1, oAttempts[x].User.Username);
                            oValues.SetText(RowCounter, 2, oAttempts[x].User.FirstName);
                            oValues.SetText(RowCounter, 3, oAttempts[x].User.LastName);
                            oValues.SetText(RowCounter, 4, oAttempts[x].User.CompanyName.Str());
                        }
                        if (oAttempts[x].ClassroomID > 0) {                            
                            Util.Classes.Activity Classroom = Classrooms.SafeGetValue(oAttempts[x].ClassroomID);
                            if (Classroom == null && oAttempts[x].ClassroomID > 0 && !AttemptedClassroomLookups.Contains(oAttempts[x].ClassroomID)) {
                                Phoenix.session session = new Phoenix.session(true);
                                Phoenix.session.RequestResult result= session.PhoenixGet(String.Format(Phoenix.session.Queries.ActivityGet, oAttempts[x].ClassroomID));
                                AttemptedClassroomLookups.Add(oAttempts[x].ClassroomID);
                                if (!result.Error) {
                                    Classroom = new Classes.Activity(result.DataObject, null);
                                    Classrooms.Add(Classroom.Id, Classroom);
                                }
                            }
                            if (Classroom != null) oValues.SetText(RowCounter, 5, Classroom.Name);
                        }
                        //oValues.SetText(RowCounter, 5, oRelease.Name);
                        if (oExamQuestions.Rows.Count > 0) {
                            oValues.SetText(RowCounter, 6, parse.replaceAll(System.Web.HttpUtility.HtmlDecode(parse.stripHTML(oExamQuestions.Rows[iQuestionCounter]["ExamSection"], true)), false, true, "\t", " ", "  ", " "));
                            oValues.SetText(RowCounter, 7, parse.replaceAll(System.Web.HttpUtility.HtmlDecode(parse.stripHTML(oExamQuestions.Rows[iQuestionCounter]["Text"], true)), false, true, "\t", " ", "  ", " "));
                            oValues.SetText(RowCounter, 8, parse.replaceAll(System.Web.HttpUtility.HtmlDecode(parse.stripHTML(convert.cStr(oExamQuestions.Rows[iQuestionCounter]["Answer"], oExamQuestions.Rows[iQuestionCounter]["MCText"]), true)), false, true, "\t", " ", "  ", " "));
                            oValues.SetText(RowCounter, 9, oExamQuestions.Rows[iQuestionCounter]["Time_Spent"].Str());
                            oValues.SetText(RowCounter, 10, convert.cDecimalNumber(oExamQuestions.Rows[iQuestionCounter]["Points"], 0));

                        }
                        oValues.SetText(RowCounter, 11, oAttempts[x].Status.Name);
                        oValues.SetText(RowCounter, 12, oRelease.Name);
                        RowCounter++;
                        iQuestionCounter++;
                        if (oExamQuestions.Rows.Count <= iQuestionCounter) break;
                    }
                }
            });
            string sFileName = io.getUniqueFileName(System.Configuration.ConfigurationManager.AppSettings["temp.path"] + "\\ExamResponses-" + iActiveUserID + "-" + String.Format("{0:MM}{0:dd}{0:yy}-{0:HH}{0:mm}{0:ss}", DateTime.Now) + ".xlsx");
            WorkbookFile.SaveAs(sFileName, SpreadsheetGear.FileFormat.OpenXMLWorkbook);
            return sFileName;
        }
    }
}