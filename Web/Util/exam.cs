﻿using System;
using System.Collections.Generic;
using System.Linq;
using jlib.helpers.structures;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using jlib.db;
using jlib.functions;

using AttackData = Phoenix.LearningPortal.Data;
namespace Phoenix.LearningPortal.Util.Exam
{
    /// <summary>
    /// Summary description for exam
    /// </summary>
    public class TableAttribute : Attribute
    {
        private string m_sName = "";
        public TableAttribute(string sName)
        {
            m_sName = sName;
        }
        public string Name
        {
            get
            {
                return m_sName;
            }
        }
    }
    public class NoAutoPopulateAttribute : Attribute { }

    public class data_object
    {
        public int ObjectID = 0;
        private DataRow m_oRow;
        private static Dictionary<Type, System.Reflection.FieldInfo[]> p_oFieldLookup = new Dictionary<Type, System.Reflection.FieldInfo[]>();
        private static Dictionary<Type, Type> p_oListLookup = new Dictionary<Type, Type>();
        private static Dictionary<Type, ConstructorInfo> p_oConstructorLookup = new Dictionary<Type, ConstructorInfo>();
        private static Dictionary<Type, string> p_oTableLookup = new Dictionary<Type, string>();
        private static Dictionary<Type, MethodInfo> p_oPopulateLookup = new Dictionary<Type, MethodInfo>();
        private static System.Reflection.FieldInfo[] getFields(Type oType)
        {
            //if (!p_oFieldLookup.ContainsKey(oType)) p_oFieldLookup.Add(oType, oType.GetFields(BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static));
            if (!p_oFieldLookup.ContainsKey(oType)) p_oFieldLookup.Add(oType, oType.GetFields(BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static));
            return p_oFieldLookup[oType];
        }
        private static Type getList(Type oType)
        {
            if (!p_oListLookup.ContainsKey(oType)) p_oListLookup.Add(oType, reflection.getGenericDeclarationTypes(oType)[0]);
            return p_oListLookup[oType];
        }
        private static ConstructorInfo getConstructor(Type oType)
        {
            if (!p_oConstructorLookup.ContainsKey(oType)) p_oConstructorLookup.Add(oType, oType.GetConstructor(new Type[] { }));
            return p_oConstructorLookup[oType];
        }
        private static string getTable(Type oType)
        {
            if (!p_oTableLookup.ContainsKey(oType)) p_oTableLookup.Add(oType, (oType.GetCustomAttributes(typeof(TableAttribute), false)[0] as TableAttribute).Name);
            return p_oTableLookup[oType];
        }
        private static MethodInfo getPopulate(Type oType)
        {
            if (!p_oPopulateLookup.ContainsKey(oType)) p_oPopulateLookup.Add(oType, oType.GetMethod("populate", new Type[] { typeof(DataRow), typeof(DateTime) }));
            return p_oPopulateLookup[oType];
        }
        public int CompareTo(object oObj)
        {
            Type oType = oObj.GetType();
            for (int x = 0; x < GetFields().Length; x++)
            {
                object oVal1 = GetFields()[x].GetValue(this); object oVal2 = GetFields()[x].GetValue(oObj);
                if (oVal1 != null && oVal2 != null)
                {
                    if ((oVal1 == null && oVal2 != null) || (oVal1 != null && oVal2 == null)) return -1;

                    //Are we dealing with a list?            
                    if (oVal1 != null && oVal1.GetType().IsGenericType)
                    {
                        System.Collections.IList oValList1 = oVal1 as System.Collections.IList;
                        System.Collections.IList oValList2 = oVal2 as System.Collections.IList;
                        if (oValList1.Count != oValList2.Count) return -1;
                        for (int y = 0; y < oValList1.Count; y++)
                            if ((oValList1[y] as data_object).CompareTo(oValList2[y]) != 0) return -1;
                    }
                    else
                    {
                        if (((oVal1 == null && oVal2 != null) || !oVal1.Equals(oVal2))) return -1;
                    }
                }

            }
            return 0;
        }
        public bool validate()
        {
            return true;
        }
        public bool save(DateTime oRevisionDate)
        {
            ado_helper oData = new ado_helper("sql.dsn.cms");
            if (m_oRow == null && ObjectID > 0) m_oRow = sqlbuilder.getDataTable(oData, "SELECT * FROM " + getTable(this.GetType()) + " WHERE ObjectID=@object_id AND @revision_date BETWEEN CREATEDATE AND DELETEDATE", "@object_id", ObjectID, "@revision_date", oRevisionDate).SafeGetRow(0);
            if (m_oRow == null)
            {
                var fkField = this.GetFields().ToList().FirstOrDefault(field => field.Name == "ForeignKey");
                if (fkField != null)
                {
                    var fkValue = fkField.GetValue(this);
                    if (fkValue.IsNotNullOrEmpty()) m_oRow = sqlbuilder.getDataTable(oData, "SELECT * FROM " + getTable(this.GetType()) + " WHERE ForeignKey=@ForeignKey AND @revision_date BETWEEN CREATEDATE AND DELETEDATE", "@ForeignKey", fkValue, "@revision_date", oRevisionDate).SafeGetRow(0);
                    if (m_oRow != null) ObjectID = m_oRow["ObjectID"].Int();
                }
            }
            Type oType = this.GetType();
            bool bChanged = false;
            DataRow oNewRow = null;
            if (m_oRow == null)
            {
                ObjectID = AttackData.Context.nextObjectID();
                oNewRow = sqlbuilder.setRowValues(oData.new_Record(getTable(this.GetType()), true).Rows[0], "ObjectID", ObjectID, "CreateDate", DateTime.Now, "DeleteDate", DateTime.MaxValue);
            }
            else
            {
                oNewRow = ado_helper.cloneDataRow(m_oRow, m_oRow.Table.NewRow(), "");
                m_oRow.Table.Rows.Add(oNewRow);
            }
            for (int x = 0; x < GetFields().Length; x++)
            {
                if (!GetFields()[x].FieldType.IsGenericType)
                {
                    object oVal = GetFields()[x].GetValue(this);
                    DataColumn oCol = oNewRow.Table.Columns[GetFields()[x].Name];
                    if ((convert.cStr(oVal) != convert.cStr(oNewRow[oCol]))
                        && (!(oCol.DataType.FullName.Contains("System.Int") || oCol.DataType.FullName.Contains("System.Boolean") || oCol.DataType.FullName == "System.Decimal" || oCol.DataType.FullName == "System.Double" || oCol.DataType.FullName == "System.Single") || convert.cDbl(oVal) != convert.cDbl(oNewRow[oCol])))
                    {
                        bChanged = true;
                        sqlbuilder.setValues(oNewRow, oCol.ColumnName, oVal);
                    }
                }
            }
            if (m_oRow == null) ado_helper.update(oNewRow.Table);
            else if (bChanged)
            {
                m_oRow["DeleteDate"] = DateTime.Now;
                sqlbuilder.setValues(oNewRow, "CreateDate", m_oRow["DeleteDate"], "DeleteDate", DateTime.MaxValue, "ObjectID", m_oRow["ObjectID"]);
                ado_helper.update(m_oRow.Table);
            }
            m_oRow = oNewRow;

            //save links
            for (int x = 0; x < GetFields().Length; x++)
            {
                if (GetFields()[x].FieldType.IsGenericType)
                {
                    if (GetFields()[x].Name == "Settings")
                    {
                        DataTable oDBSettings = sqlbuilder.getDataTable(oData, "select d_settings.* from d_settings where @revision_date between CreateDate and DeleteDate AND TableName=@table_name and ObjectID=@object_id", "@table_name", getTable(this.GetType()), "@object_id", this.ObjectID, "@revision_date", oRevisionDate);
                        OrderedDictionary<string, string> oSettings = GetFields()[x].GetValue(this) as OrderedDictionary<string, string>;
                        foreach (KeyValuePair<string, string> oEntry in oSettings)
                        {
                            DataRow[] oRow = oDBSettings.Select("KeyName='" + ado_helper.PrepareDB(oEntry.Key) + "'");
                            if (oRow.Length > 0 && convert.cStr(oRow[0]["Value"]) == oEntry.Value)
                            {
                                oDBSettings.Rows.Remove(oRow[0]);
                            }
                            else
                            {//added or modified
                                if (oRow.Length > 0) oRow[0]["DeleteDate"] = oRevisionDate; //modified                            
                                oDBSettings.Rows.Add(sqlbuilder.setRowValues(oDBSettings.NewRow(), "KeyName", oEntry.Key, "Value", oEntry.Value, "ObjectId", this.ObjectID, "TableName", getTable(this.GetType()), "CreateDate", DateTime.Now, "DeleteDate", DateTime.MaxValue));
                            }
                        }

                        for (int y = 0; y < oDBSettings.Rows.Count; y++)
                            if (oDBSettings.Rows[y].RowState == DataRowState.Unchanged) oDBSettings.Rows[y].Delete();

                        ado_helper.update(oDBSettings);
                    }
                    else
                    {

                        Type oListItemType = getList(GetFields()[x].FieldType);
                        DataTable oChildren = sqlbuilder.getDataTable(oData, "select l_row_links.* from l_row_links where @revision_date between l_row_links.CreateDate and l_row_links.DeleteDate AND l_row_links.table1=@table1 and l_row_links.table2=@table2 and l_row_links.id1=@object_id order by sequence", "@table2", getTable(oListItemType), "@table1", getTable(this.GetType()), "@revision_date", oRevisionDate, "@object_id", this.ObjectID);
                        System.Collections.IList oList = GetFields()[x].GetValue(this) as System.Collections.IList;
                        int iChildCounter = 0;
                        for (int y = 0; y < oChildren.Rows.Count; y++) oChildren.Rows[y]["DeleteDate"] = oRevisionDate;
                        for (int y = 0; y < oList.Count; y++)
                        {

                            data_object o = (oList[y] as data_object);
                            o.save(oRevisionDate);
                            if (oChildren.Rows.Count > iChildCounter && convert.cInt(oChildren.Rows[iChildCounter]["id2"]) == o.ObjectID)
                            {
                                oChildren.Rows[iChildCounter].RejectChanges();
                            }
                            else oChildren.Rows.Add(sqlbuilder.setValues(oChildren.NewRow(), "id1", ObjectID, "id2", o.ObjectID, "table1", getTable(this.GetType()), "table2", getTable(o.GetType()), "sequence", (oChildren.Rows.Count == 0 ? 1 : convert.cInt(oChildren.Rows[oChildren.Rows.Count - 1]["sequence"]) + 1), "CreateDate", oRevisionDate, "DeleteDate", DateTime.MaxValue));

                            iChildCounter++;
                        }
                        ado_helper.update(oChildren);
                    }
                }
            }
            return true;
        }

        public data_object populate(params object[] oInitialValues)
        {
            Type oType = this.GetType();
            Dictionary<string, object> oValues = convert.cDictionary(oInitialValues);
            for (int x = 0; x < GetFields().Length; x++)
                if (!GetFields()[x].FieldType.IsGenericType && oValues.ContainsKey(GetFields()[x].Name)) reflection.setValueOfField(this, GetFields()[x], oValues[GetFields()[x].Name]);
            return this;
        }
        public void populateSettings(DataRow[] settings, FieldInfo fieldInfo)
        {
            reflection.setValueOfField(this, "_SettingsPopulated", true);
            OrderedDictionary<string, string> oSettings = fieldInfo.GetValue(this) as OrderedDictionary<string, string>;
            if (oSettings.Count > 0) return;
            for (int y = 0; y < settings.Length; y++)
                oSettings.Add(convert.cStr(settings[y]["KeyName"]), convert.cStr(settings[y]["Value"]));

        }
        private Type _CurrentType;
        private Type CurrentType
        {
            get
            {
                if (_CurrentType == null) _CurrentType = this.GetType();
                return _CurrentType;
            }
        }
        public FieldInfo[] GetFields()
        {
            return getFields(CurrentType);
        }
        public virtual bool populate(DataRow oRow, DateTime oRevisionDate)
        {
            ado_helper oData = new ado_helper("sql.dsn.cms");
            m_oRow = oRow;
            if (oRow != null && oRow.Table.Rows.Count > 1)
            {
                DataTable oTempDT = (oRow.Table as JDataTable).Clone();
                oTempDT.ImportRow(oRow);
                m_oRow = oTempDT.Rows[0];
            }

            for (int x = 0; x < getFields(CurrentType).Length; x++)
            {
                if (!GetFields()[x].FieldType.IsGenericType) reflection.setValueOfField(this, GetFields()[x], oRow[GetFields()[x].Name]);
            }
            for (int x = 0; x < GetFields().Length; x++)
            {
                FieldInfo fieldInfo = GetFields()[x];
                Type oFieldType = fieldInfo.FieldType;
                if (oFieldType.IsGenericType)
                {
                    if (fieldInfo.GetCustomAttribute(typeof(NoAutoPopulateAttribute), false) == null)
                    {
                        if (GetFields()[x].Name == "Settings")
                        {
                            if (!reflection.getFieldValue(this, "_SettingsPopulated").Bln())
                            {
                                DataTable oDBSettings = sqlbuilder.getDataTable(oData, "select d_settings.* from d_settings where @revision_date between CreateDate and DeleteDate AND TableName=@table_name and ObjectID=@object_id", "@table_name", getTable(this.GetType()), "@object_id", this.ObjectID, "@revision_date", oRevisionDate);
                                this.populateSettings(oDBSettings.Select(), GetFields()[x]);
                            }
                        }
                        else
                        {
                            Type oListItemType = getList(oFieldType);
                            DataTable oChildren = sqlbuilder.getDataTable(oData, String.Format("select {0}.* from {0}, l_row_links where @revision_date between {0}.CreateDate and {0}.DeleteDate AND @revision_date between l_row_links.CreateDate and l_row_links.DeleteDate AND l_row_links.table1=@table1 and l_row_links.table2=@table2 and l_row_links.id1=@object_id and l_row_links.id2={0}.ObjectID order by sequence", getTable(oListItemType), getTable(this.GetType())), "@table1", getTable(this.GetType()), "@table2", getTable(oListItemType), "@revision_date", oRevisionDate, "@object_id", this.ObjectID);
                            Dictionary<int, List<DataRow>> childSettings = null;
                            FieldInfo childSettingsField = null;
                            bool bulkPopulateChildSettings = false;

                            System.Collections.IList oValueList = GetFields()[x].GetValue(this) as System.Collections.IList;
                            for (int y = 0; y < oChildren.Rows.Count; y++)
                            {
                                data_object child = getConstructor(oListItemType).Invoke(new object[] { }) as data_object;

                                {//Custom section for populating settings on all child objects
                                    if (childSettings == null)
                                    {
                                        childSettings = new Dictionary<int, List<DataRow>>();
                                        childSettingsField = reflection.getFieldInfo(child, "Settings");
                                        //FieldInfo childSettingsPrivateField = reflection.getFieldInfo(child, "_Settings");
                                        if (childSettingsField != null && !reflection.getFieldValue(child, "_SettingsPopulated").Bln())
                                        {//childSettingsField.GetCustomAttribute(typeof(NoAutoPopulateAttribute), false) != null) {                                        
                                            bulkPopulateChildSettings = true;
                                            DataTable tempSettings = sqlbuilder.getDataTable(oData, String.Format("select d_settings.* from d_settings, l_row_links where @revision_date between d_settings.CreateDate and d_settings.DeleteDate AND @revision_date between l_row_links.CreateDate and l_row_links.DeleteDate AND l_row_links.table1=@table1 and l_row_links.table2=@table2 and l_row_links.id1=@object_id and l_row_links.id2=d_settings.ObjectID and d_settings.TableName=l_row_links.table2", getTable(oListItemType), getTable(this.GetType())), "@table1", getTable(this.GetType()), "@table2", getTable(oListItemType), "@revision_date", oRevisionDate, "@object_id", this.ObjectID);
                                            foreach (DataRow row in tempSettings.Rows)
                                            {
                                                if (childSettings.ContainsKey(row["ObjectId"].Int())) childSettings[row["ObjectId"].Int()].Add(row);
                                                else childSettings.Add(row["ObjectId"].Int(), new List<DataRow>() { row });
                                            }
                                        }
                                    }
                                    if (bulkPopulateChildSettings)
                                    {
                                        if (childSettings.Count > 0 && childSettings.ContainsKey(oChildren.Rows[y]["ObjectId"].Int()))
                                            child.populateSettings(childSettings[oChildren.Rows[y]["ObjectId"].Int()].ToArray(), childSettingsField);
                                        else
                                            reflection.setValueOfField(child, "_SettingsPopulated", true);
                                    }
                                }

                                oValueList.Add(child);
                                getPopulate(oListItemType).Invoke(child, new object[] { oChildren.Rows[y], oRevisionDate });
                            }
                        }
                    }
                }
            }
            return true;
        }
    }
    [TableAttribute("d_exams")]
    public class exam : data_object
    {
        public string Name, IntroText, ExamText, Language, PlayerType, ExamID;
        public int MaxTime, Pausable, Resumable, ReviewAnswers;
        public List<exam_section> Sections = new List<exam_section>();


        public OrderedDictionary<string, string> Settings = new OrderedDictionary<string, string>();


        public bool populate(int iReleaseID)
        {
            ado_helper oData = new ado_helper("sql.dsn.cms");
            DataTable oRelease = sqlbuilder.executeSelect(oData, "d_releases", "id", iReleaseID);
            return populate((int)oRelease.Rows[0]["ObjectID"], (DateTime)oRelease.Rows[0]["ObjectDate"]);
        }
        public bool populate(int iObjectID, DateTime oRevisionDate)
        {
            ado_helper oData = new ado_helper("sql.dsn.cms");
            DataTable oExam = sqlbuilder.getDataTable(oData, "select * from d_exams where ObjectID=" + iObjectID + " and '" + oRevisionDate + "' between CreateDate and DeleteDate");
            return populate(oExam.Rows.Count == 0 ? oExam.NewRow() : oExam.Rows[0], oRevisionDate);
        }
        public void removeAnswers(List<exam_section> oSections)
        {
            if (oSections == null) oSections = this.Sections;
            for (int x = 0; x < oSections.Count; x++)
            {
                for (int y = 0; y < oSections[x].Questions.Count; y++)
                {
                    for (int z = 0; z < oSections[x].Questions[y].Answers.Count; z++)
                    {
                        oSections[x].Questions[y].Answers[z].IsCorrect = 0;
                        oSections[x].Questions[y].Answers[z].Reason = "";
                    }
                }
                if (oSections[x].Sections != null && oSections[x].Sections.Count > 0) removeAnswers(oSections[x].Sections);
            }
        }        
    }
    [TableAttribute("d_exam_sections")]
    public class exam_section : data_object
    {
        public string Name, ShortName, Text;
        /*public int MaxTime;*/
        public List<exam_question> Questions = new List<exam_question>();

        public List<exam_section> Sections = new List<exam_section>();


        public OrderedDictionary<string, string> Settings = new OrderedDictionary<string, string>();

        private bool _SettingsPopulated = false;
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public bool SettingsPopulated
        {
            get { return _SettingsPopulated; }
            set { _SettingsPopulated = value; }
        }

        //public exam_section() {
        //    Questions = new List<exam_question>();
        //}
        public List<exam_question> GetAllQuestions(List<exam_question> questions)
        {
            if (questions == null) questions = new List<exam_question>();
            Questions.ForEach(x => questions.Add(x));
            Sections.ForEach(x => x.GetAllQuestions(questions));
            return questions;
        }
        public bool populate(DataRow oRow, DateTime oRevisionDate)
        {
            bool result = base.populate(oRow, oRevisionDate);
            if (this.Questions.Count > 0)
            {
                ado_helper oData = new ado_helper("sql.dsn.cms");
                Dictionary<int, exam_question> questions = new Dictionary<int, exam_question>();
                Questions.ForEach(x => { questions.Add(x.ObjectID, x); });
                DataTable responses = sqlbuilder.getDataTable(new ado_helper("sql.dsn.cms"), @"select * from dbo.fn_get_links(@object_id,'d_exam_sections',@revision_date,0,'') links, 
                    d_question_options where d_question_options.ObjectID=links.ObjectID and links.TableName='d_question_options' and @revision_date between d_question_options.CreateDate and d_question_options.DeleteDate
                    and (links.TableName='d_question_options' and level=1) order by sort", "@revision_date", oRevisionDate, "@object_id", this.ObjectID);

                foreach (DataRow row in responses.Rows)
                {
                    exam_question_response response = new exam_question_response();
                    //response.populateSettings(null, reflection.getFieldInfo(response, "Settings"));
                    response.SettingsPopulated = true;
                    response.populate(row, oRevisionDate);
                    questions[row["ParentID"].Int()].Answers.Add(response);
                }

            }
            return result;
        }
    }
    [TableAttribute("d_questions")]
    public class exam_question : data_object
    {
        public string Text, SubText, Reason, Hint, QuestionType, ForeignKey, Reference, Domain, KnowledgeArea; //Reason and correct is only populated when reviewing answers!
        public int Points;
        [NoAutoPopulateAttribute]
        public List<exam_question_response> Answers = new List<exam_question_response>();

        //private List<exam_question_response> _Answers = new List<exam_question_response>();
        //public List<exam_question_response> Answers {
        //	get { return _Answers; }
        //	set { _Answers = value; }
        //}

        public OrderedDictionary<string, string> Settings = new OrderedDictionary<string, string>();


        private bool _SettingsPopulated = false;
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public bool SettingsPopulated
        {
            get { return _SettingsPopulated; }
            set { _SettingsPopulated = value; }
        }
    }
    [TableAttribute("d_question_options")]
    public class exam_question_response : data_object
    {
        public string Reason, Text;
        public int IsCorrect;

        public OrderedDictionary<string, string> Settings = new OrderedDictionary<string, string>();

        private bool _SettingsPopulated = false;
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public bool SettingsPopulated
        {
            get { return _SettingsPopulated; }
            set { _SettingsPopulated = value; }
        }
    }
    public class exam_attempt
    {
        public string UserNotes, UserComments, UserHighlights;
        public class exam_attempt_answer
        {
            public string Answer;
            public int QuestionID, TimeSpent, Status;
        }
    }
}