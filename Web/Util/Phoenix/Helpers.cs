﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Xml;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using System.Linq;
using System.Data;
using System.Data.OleDb;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using jlib.db;
using jlib.functions;
using System.Security.Cryptography;
using System.Text;
using AttackData = Phoenix.LearningPortal.Data;

/// <summary>
/// Summary description for ActivityBase
/// </summary>
namespace Phoenix.LearningPortal.Util.Phoenix
{
    public class Helpers
    {
		public static string GetFileUrl(dynamic File)
		{
			if (File == null) return null;
			int fileId = convert.cInt(File.FileId);
			string rowVersion = convert.cStr(File.RowVersion);
			var secret = convert.cStr(File.ShareGuid);

			if(!string.IsNullOrEmpty(secret))
				return System.Configuration.ConfigurationManager.AppSettings["phoenix.url"] + "/files/" + fileId + "/" + secret;

			return System.Configuration.ConfigurationManager.AppSettings["phoenix.url"] + "/files/" + fileId + "/" + HttpUtility.UrlEncode(rowVersion.Replace('/', '_'));
		}
        public static void DeletePhoenixSession(System.Web.HttpRequest request, System.Web.HttpResponse response)
        {
            jlib.net.HTTP.DeleteCookie(request, response, "PhoenixAuth");
            jlib.net.HTTP.DeleteCookie(request, response, "user-authentication");
        }
        public static void HandleError(Exception e, Phoenix.session session)
        {
            string errorMessage = "", url = "";
            int responseCode = 0;
            if (e as WebException != null)
            {
                errorMessage = new StreamReader(((System.Net.WebException)e).Response.GetResponseStream()).ReadToEnd();
                responseCode = parse.inner_substring((e as System.Net.WebException).Message, null, "(", ")", null).Int();
                url = (e as System.Net.WebException).Response.ResponseUri.AbsoluteUri;
            }

            if (responseCode == 401 && !System.Web.HttpContext.Current.Items["DisableLoginRedirect"].Bln() && !System.Web.HttpContext.Current.Request.Path.Contains("login.aspx"))
            {
                if (session == null) session = new Phoenix.session();
                if (!session.GetPhoenixCookie().IsNullOrEmpty())
                {

                    //reuse session, if possible. the GetCookie() method gets confused between the Request coming in from the browser and the request used to query Phoenix API, and the Cookies[] collection is unreliable
                    //check cookie validity
                    if (!url.IsNullOrEmpty() && !url.Contains(Util.Phoenix.session.Queries.UserGetCurrent))
                    {
                        Util.Phoenix.session.RequestResult sessionCheck = session.PhoenixRequest("GET", Util.Phoenix.session.Queries.UserGetCurrent, null, 0, false, true);
                        if (!sessionCheck.Error)
                        {
                            System.Web.HttpContext.Current.Session["alert.once"] = "Your request resulted in a permission-error. You might want to <a href='login.aspx'>log in again</a>. The message returned from the server was: " + convert.cStr(errorMessage, e.Message);
                            System.Web.HttpContext.Current.Response.Redirect(System.Web.HttpContext.Current.Request.ApplicationPath + "/error.aspx");
                        }
                    }
                    Phoenix.Helpers.DeletePhoenixSession(System.Web.HttpContext.Current.Request, System.Web.HttpContext.Current.Response);
                }
                //if (!System.Web.HttpContext.Current.Request.RawUrl.Contains("/login.aspx")) System.Web.HttpContext.Current.Session["alert.once"] = "You have been logged out due to inactivity.";
                System.Web.HttpContext.Current.Response.Redirect(System.Web.HttpContext.Current.Request.ApplicationPath + "/login.aspx");
            }

            //JVT: 2017-12-05: Clear Phoenix cookie -- it looks like it has expired
            //if (responseCode == 401 && System.Web.HttpContext.Current.Items["DisableLoginRedirect"].Bln() && !System.Web.HttpContext.Current.Request.Path.Contains("login.aspx"))
            //    Phoenix.Helpers.DeletePhoenixSession(System.Web.HttpContext.Current.Request, System.Web.HttpContext.Current.Response);
        }
        public static void ResetCache()
        {
            m_oCurrencies = null;

        }
        //            public static DateTimeOffset? LocalizeDate(DateTimeOffset? date, string zone) {
        //                if (date == null) return null;
        //                return date.Value.AddHours(GetTimezoneOffset(zone)-1);
        //            }
        //            public static double GetTimezoneOffset(string zone) {
        //                Dictionary<string, double> zones = new Dictionary<string, double>() {
        //                {"pacific_midway", -11},
        //{"pacific_pago_pago", -11},
        //{"pacific_niue", -11},
        //{"pacific_rarotonga", -10},
        //{"america_adak", -10},
        //{"pacific_honolulu", -10},
        //{"pacific_tahiti", -10},
        //{"america_anchorage", -9},
        //{"america_juneau", -9},
        //{"america_nome", -9},
        //{"america_sitka", -9},
        //{"america_yakutat", -9},
        //{"pacific_gambier", -9},
        //{"america_whitehorse", -8},
        //{"america_dawson", -8},
        //{"america_vancouver", -8},
        //{"america_santa_isabel", -8},
        //{"america_tijuana", -8},
        //{"america_los_angeles", -8},
        //{"america_metlakatla", -8},
        //{"pacific_pitcairn", -8},
        //{"america_edmonton", -7},
        //{"america_cambridge_bay", -7},
        //{"america_inuvik", -7},
        //{"america_yellowknife", -7},
        //{"america_chihuahua", -7},
        //{"america_mazatlan", -7},
        //{"america_ojinaga", -7},
        //{"america_denver", -7},
        //{"america_boise", -7},
        //{"america_hermosillo", -7},
        //{"america_phoenix", -7},
        //{"america_creston", -7},
        //{"america_dawson_creek", -7},
        //{"america_belize", -6},
        //{"america_rainy_river", -6},
        //{"america_winnipeg", -6},
        //{"america_resolute", -6},
        //{"america_rankin_inlet", -6},
        //{"pacific_easter", -6},
        //{"america_costa_rica", -6},
        //{"america_guatemala", -6},
        //{"america_tegucigalpa", -6},
        //{"america_mexico_city", -6},
        //{"america_bahia_banderas", -6},
        //{"america_merida", -6},
        //{"america_cancun", -6},
        //{"america_monterrey", -6},
        //{"america_managua", -6},
        //{"america_el_salvador", -6},
        //{"america_chicago", -6},
        //{"america_indiana_tell_city", -6},
        //{"america_indiana_knox", -6},
        //{"america_menominee", -6},
        //{"america_matamoros", -6},
        //{"america_north_dakota_center", -6},
        //{"america_north_dakota_new_salem", -6},
        //{"america_north_dakota_beulah", -6},
        //{"america_swift_current", -6},
        //{"pacific_galapagos", -6},
        //{"america_regina", -6},
        //{"america_montreal", -5},
        //{"america_toronto", -5},
        //{"america_thunder_bay", -5},
        //{"america_nipigon", -5},
        //{"america_pangnirtung", -5},
        //{"america_iqaluit", -5},
        //{"america_bogota", -5},
        //{"america_havana", -5},
        //{"america_port-au-prince", -5},
        //{"america_lima", -5},
        //{"america_grand_turk", -5},
        //{"america_indiana_indianapolis", -5},
        //{"america_kentucky_louisville", -5},
        //{"america_indiana_marengo", -5},
        //{"america_detroit", -5},
        //{"america_indiana_vincennes", -5},
        //{"america_indiana_petersburg", -5},
        //{"america_indiana_winamac", -5},
        //{"america_indiana_vevay", -5},
        //{"america_new_york", -5},
        //{"america_kentucky_monticello", -5},
        //{"america_nassau", -5},
        //{"america_jamaica", -5},
        //{"america_atikokan", -5},
        //{"america_guayaquil", -5},
        //{"america_panama", -5},
        //{"america_cayman", -5},
        //{"america_barbados", -4},
        //{"america_cuiaba", -4},
        //{"america_campo_grande", -4},
        //{"america_goose_bay", -4},
        //{"america_moncton", -4},
        //{"america_halifax", -4},
        //{"america_glace_bay", -4},
        //{"america_santiago", -4},
        //{"america_asuncion", -4},
        //{"america_argentina_san_luis", -4},
        //{"america_thule", -4},
        //{"atlantic_bermuda", -4},
        //{"america_eirunepe", -4},
        //{"america_santo_domingo", -4},
        //{"america_martinique", -4},
        //{"america_guyana", -4},
        //{"america_manaus", -4},
        //{"america_boa_vista", -4},
        //{"america_la_paz", -4},
        //{"america_puerto_rico", -4},
        //{"america_rio_branco", -4},
        //{"america_porto_velho", -4},
        //{"america_blanc-sablon", -4},
        //{"america_aruba", -4},
        //{"america_curacao", -4},
        //{"america_st_vincent", -4},
        //{"america_antigua", -4},
        //{"america_anguilla", -4},
        //{"america_tortola", -4},
        //{"america_dominica", -4},
        //{"america_port_of_spain", -4},
        //{"america_st_thomas", -4},
        //{"america_montserrat", -4},
        //{"america_st_kitts", -4},
        //{"america_guadeloupe", -4},
        //{"america_st_lucia", -4},
        //{"america_grenada", -4},
        //{"america_argentina_mendoza", -3},
        //{"america_argentina_tucuman", -3},
        //{"america_argentina_cordoba", -3},
        //{"america_argentina_buenos_aires", -3},
        //{"america_araguaina", -3},
        //{"america_sao_paulo", -3},
        //{"america_miquelon", -3},
        //{"america_godthab", -3},
        //{"america_montevideo", -3},
        //{"america_argentina_jujuy", -3},
        //{"america_argentina_la_rioja", -3},
        //{"america_argentina_catamarca", -3},
        //{"america_argentina_san_juan", -3},
        //{"america_argentina_ushuaia", -3},
        //{"america_argentina_rio_gallegos", -3},
        //{"america_argentina_salta", -3},
        //{"america_maceio", -3},
        //{"america_recife", -3},
        //{"america_paramaribo", -3},
        //{"atlantic_stanley", -3},
        //{"america_fortaleza", -3},
        //{"america_bahia", -3},
        //{"america_santarem", -3},
        //{"america_belem", -3},
        //{"america_cayenne", -3},
        //{"america_noronha", -2},
        //{"atlantic_south_georgia", -2},
        //{"atlantic_azores", -1},
        //{"america_scoresbysund", -1},
        //{"atlantic_cape_verde", -1},
        //{"africa_monrovia", 0},
        //{"africa_conakry", 0},
        //{"africa_nouakchott", 0},
        //{"africa_banjul", 0},
        //{"atlantic_reykjavik", 0},
        //{"america_danmarkshavn", 0},
        //{"atlantic_st_helena", 0},
        //{"africa_sao_tome", 0},
        //{"africa_dakar", 0},
        //{"africa_bissau", 0},
        //{"africa_bamako", 0},
        //{"africa_ouagadougou", 0},
        //{"africa_lome", 0},
        //{"africa_abidjan", 0},
        //{"africa_el_aaiun", 0},
        //{"europe_london", 0},
        //{"europe_dublin", 0},
        //{"europe_lisbon", 0},
        //{"atlantic_madeira", 0},
        //{"atlantic_canary", 0},
        //{"atlantic_faroe", 0},
        //{"africa_accra", 0},
        //{"africa_casablanca", 0},
        //{"africa_freetown", 0},
        //{"africa_algiers", 1},
        //{"africa_niamey", 1},
        //{"africa_ndjamena", 1},
        //{"africa_porto-novo", 1},
        //{"africa_malabo", 1},
        //{"africa_luanda", 1},
        //{"africa_kinshasa", 1},
        //{"africa_bangui", 1},
        //{"africa_lagos", 1},
        //{"africa_libreville", 1},
        //{"africa_douala", 1},
        //{"africa_brazzaville", 1},
        //{"europe_brussels", 1},
        //{"africa_ceuta", 1},
        //{"europe_vienna", 1},
        //{"europe_luxembourg", 1},
        //{"europe_paris", 1},
        //{"europe_warsaw", 1},
        //{"europe_stockholm", 1},
        //{"europe_copenhagen", 1},
        //{"europe_budapest", 1},
        //{"europe_rome", 1},
        //{"europe_malta", 1},
        //{"europe_amsterdam", 1},
        //{"europe_belgrade", 1},
        //{"europe_prague", 1},
        //{"europe_monaco", 1},
        //{"europe_berlin", 1},
        //{"europe_gibraltar", 1},
        //{"europe_andorra", 1},
        //{"europe_tirane", 1},
        //{"europe_madrid", 1},
        //{"europe_zurich", 1},
        //{"europe_vaduz", 1},
        //{"europe_oslo", 1},
        //{"africa_tripoli", 1},
        //{"africa_windhoek", 1},
        //{"africa_tunis", 1},
        //{"africa_gaborone", 2},
        //{"africa_harare", 2},
        //{"africa_kigali", 2},
        //{"africa_maputo", 2},
        //{"africa_bujumbura", 2},
        //{"africa_lubumbashi", 2},
        //{"africa_lusaka", 2},
        //{"africa_blantyre", 2},
        //{"africa_mbabane", 2},
        //{"africa_maseru", 2},
        //{"africa_cairo", 2},
        //{"europe_riga", 2},
        //{"europe_vilnius", 2},
        //{"europe_simferopol", 2},
        //{"europe_chisinau", 2},
        //{"europe_tallinn", 2},
        //{"europe_uzhgorod", 2},
        //{"europe_kiev", 2},
        //{"europe_sofia", 2},
        //{"europe_istanbul", 2},
        //{"europe_zaporozhye", 2},
        //{"europe_bucharest", 2},
        //{"europe_athens", 2},
        //{"europe_helsinki", 2},
        //{"asia_nicosia", 2},
        //{"asia_amman", 2},
        //{"asia_beirut", 2},
        //{"asia_gaza", 2},
        //{"asia_hebron", 2},
        //{"africa_johannesburg", 2},
        //{"asia_damascus", 2},
        //{"asia_jerusalem", 2},
        //{"asia_bahrain", 3},
        //{"asia_qatar", 3},
        //{"asia_riyadh", 3},
        //{"asia_aden", 3},
        //{"asia_kuwait", 3},
        //{"africa_kampala", 3},
        //{"africa_nairobi", 3},
        //{"africa_mogadishu", 3},
        //{"africa_asmara", 3},
        //{"africa_dar_es_salaam", 3},
        //{"indian_antananarivo", 3},
        //{"africa_juba", 3},
        //{"africa_khartoum", 3},
        //{"africa_addis_ababa", 3},
        //{"africa_djibouti", 3},
        //{"indian_comoro", 3},
        //{"indian_mayotte", 3},
        //{"europe_minsk", 3},
        //{"europe_kaliningrad", 3},
        //{"asia_baghdad", 3},
        //{"asia_yerevan", 4},
        //{"asia_tbilisi", 4},
        //{"asia_dubai", 4},
        //{"asia_muscat", 4},
        //{"europe_moscow", 4},
        //{"indian_reunion", 4},
        //{"europe_samara", 4},
        //{"indian_mahe", 4},
        //{"europe_volgograd", 4},
        //{"asia_baku", 4},
        //{"indian_mauritius", 4},
        //{"asia_aqtau", 5},
        //{"asia_aqtobe", 5},
        //{"indian_maldives", 5},
        //{"asia_oral", 5},
        //{"indian_kerguelen", 5},
        //{"asia_dushanbe", 5},
        //{"asia_ashgabat", 5},
        //{"asia_samarkand", 5},
        //{"asia_tashkent", 5},
        //{"asia_karachi", 5},
        //{"asia_almaty", 6},
        //{"asia_thimphu", 6},
        //{"indian_chagos", 6},
        //{"asia_bishkek", 6},
        //{"asia_qyzylorda", 6},
        //{"asia_yekaterinburg", 6},
        //{"asia_dhaka", 6},
        //{"indian_christmas", 7},
        //{"asia_vientiane", 7},
        //{"asia_ho_chi_minh", 7},
        //{"asia_phnom_penh", 7},
        //{"asia_bangkok", 7},
        //{"asia_novokuznetsk", 7},
        //{"asia_novosibirsk", 7},
        //{"asia_omsk", 7},
        //{"asia_jakarta", 7},
        //{"asia_pontianak", 7},
        //{"asia_hovd", 7},
        //{"asia_brunei", 8},
        //{"asia_makassar", 8},
        //{"asia_krasnoyarsk", 8},
        //{"asia_kuala_lumpur", 8},
        //{"asia_kuching", 8},
        //{"asia_singapore", 8},
        //{"australia_perth", 8},
        //{"asia_hong_kong", 8},
        //{"asia_choibalsan", 8},
        //{"asia_ulaanbaatar", 8},
        //{"asia_manila", 8},
        //{"asia_harbin", 8},
        //{"asia_kashgar", 8},
        //{"asia_urumqi", 8},
        //{"asia_chongqing", 8},
        //{"asia_macau", 8},
        //{"asia_shanghai", 8},
        //{"asia_taipei", 8},
        //{"asia_jayapura", 9},
        //{"asia_irkutsk", 9},
        //{"asia_pyongyang", 9},
        //{"pacific_palau", 9},
        //{"asia_dili", 9},
        //{"asia_tokyo", 9},
        //{"asia_seoul", 9},
        //{"pacific_saipan", 10},
        //{"pacific_guam", 10},
        //{"pacific_chuuk", 10},
        //{"pacific_port_moresby", 10},
        //{"asia_khandyga", 10},
        //{"asia_yakutsk", 10},
        //{"australia_sydney", 10},
        //{"australia_brisbane", 10},
        //{"australia_hobart", 10},
        //{"australia_currie", 10},
        //{"australia_melbourne", 10},
        //{"australia_lindeman", 10},
        //{"pacific_kosrae", 11},
        //{"pacific_pohnpei", 11},
        //{"asia_sakhalin", 11},
        //{"pacific_guadalcanal", 11},
        //{"asia_ust-nera", 11},
        //{"asia_vladivostok", 11},
        //{"pacific_noumea", 11},
        //{"pacific_efate", 11},
        //{"asia_anadyr", 12},
        //{"pacific_tarawa", 12},
        //{"asia_magadan", 12},
        //{"pacific_kwajalein", 12},
        //{"pacific_majuro", 12},
        //{"pacific_nauru", 12},
        //{"asia_kamchatka", 12},
        //{"pacific_funafuti", 12},
        //{"pacific_wake", 12},
        //{"pacific_wallis", 12},
        //{"pacific_fiji", 12},
        //{"pacific_auckland", 12},
        //{"pacific_enderbury", 13},
        //{"pacific_fakaofo", 13},
        //{"pacific_tongatapu", 13},
        //{"pacific_apia", 13},
        //{"pacific_kiritimati", 14},
        //{"australia_lord_howe", 10.5},
        //{"pacific_norfolk", 11.5},
        //{"pacific_chatham", 12.75},
        //{"asia_tehran", 3.5},
        //{"america_st_johns", -3.5},
        //{"asia_kabul", 4.5},
        //{"america_caracas", -4.5},
        //{"asia_colombo", 5.5},
        //{"asia_kolkata", 5.5},
        //{"asia_kathmandu", 5.75},
        //{"indian_cocos", 6.5},
        //{"asia_rangoon", 6.5},
        //{"australia_eucla", 8.75},
        //{"australia_broken_hill", 9.5},
        //{"australia_adelaide", 9.5},
        //{"australia_darwin", 9.5},
        //{"pacific_marquesas", -9.5}
        //                };
        //                return zones[zone];
        //            }
        private static Dictionary<int, string> m_oCurrencies;
        public static string GetCurrency(int currencyId)
        {
            if (m_oCurrencies == null)
            {
                m_oCurrencies = new Dictionary<int, string>();
                lock (m_oCurrencies)
                {
                    dynamic currencies = new Phoenix.session().PhoenixGet(Phoenix.session.Queries.CurrencyCodesGet, true, true).DataObject;
                    foreach (dynamic currency in currencies)
                        m_oCurrencies.Add(convert.cInt(currency.Id), currency.Value);
                }
            }
            lock (m_oCurrencies) return m_oCurrencies.SafeGetValue(currencyId);
        }
        //public class Constants {
        //    public static int ImportedHistoryCourseId = 107429;
        //    public static int ExemptCourseId = 107430;
        //}
    }
    public class session
    {
        public session() { }
        public session(bool alwaysSendToken)
        {
            AlwaysSendToken = alwaysSendToken;
        }
        public session(string overrideToken)
        {
            if (!overrideToken.IsNullOrEmpty())
            {
                OverrideToken = overrideToken;
                AlwaysSendToken = true;
            }
        }
        public bool AlwaysSendToken { get; set; }
        public string OverrideToken { get; set; }
        public static class Queries
        {
            public static string UserLogout = "accounts/logout";
            public static string UserLogon = "accounts/{0}/logon";
            public static string UserGet = "accounts?userId={0}";
            public static string UserGetByEmail = "accounts?email={0}";
            public static string UserGetByUsername = "accounts?username={0}";
            public static string UserGetCurrent = "accounts/current";
            public static string UserPut = "accounts";
            public static string UserUpdatePassword = "accounts/{0}/changepassword";
            public static string UserPasswordReset = "accounts/requestpasswordreset?email={0}&source=LearningPortal";
            public static string UserPasswordResetFromToken = "accounts/resetpassword?email={0}&token={1}&newPassword={2}";
            public static string CustomerGet = "customers/{0}";
            public static string CustomerLeadingtextGet = "customers/{0}/leadingtexts";

            public static string DistributorsGet = "distributors";
            public static string LanguagesGet = "languages";
            public static string ParticipantsSearch = "participants?userId={0}&orderDirection={1}&skip={2}&limit={3}";
            public static string ParticipantAvailability = "participants/{0}/availability";

            public static string CourseCalendar = "activitysets?customerId={0}&courseCalendar=true";
            public static string ActivityGet = "activities/{0}?searchable=false";
            public static string ActivitySetGet = "activitysets/{0}";
            public static string ActivitySetUnenroll = "activitysets/{0}/unenroll?userId={1}";
            public static string ProductsInCoursePrograms = "productsincourseprogram?courseprogramId={0}";
            public static string CustomerCoursePrograms = "courseprograms?customerId={0}";


            public static string ArticleGet = "articles/{0}";
            public static string FileGet = "files/{0}";
            public static string CustomerArticleGet = "customerarticles/{0}";
            public static string ELearnCourseGet = "rcos/courses/{0}";
            public static string ProductDescriptionGet = "productdescriptions/{0}?withoutReferences=true";
            public static string CurrencyCodesGet = "codes?type=currencycodes";
            public static string ScormServer = "scorm/";
            public static string ScormSessionGet = "scorm/{0}";

            public static string ParticipantsGet = "users?customerId={0}&activityId={1}&activitySetId={2}";
            public static string ParticipantGet = "participants/{0}";
            public static string ParticipantSetStatus = "participants/{0}/status/{1}";

            public static string UserPerformancesGet = "users/{0}/performances?articleId={1}&activityId={2}";
            public static string UserCompetenceGetPutPost = "users/{0}/competence";

            public static string UserPerformancesGetByCustomer = "customers/{0}/performances";
            public static string UserPerformancesGetByActivity = "activities/{0}/performances";
            public static string ArbitraryUserCreate = "accounts/arbitrary";
            public static string UserCreate = "accounts?autoGeneratePassword=true&source=LearningPortal";

            public static string ParticipantSubmitExam = "participants/{0}/submitexam";
            public static string PerformanceRegister = "performances/register/{0}/{1}"; //{userId}/{rcoId}

            public static string MockExamGet = "rcos/exams/{0}";
            public static string ExamInfoGet = "exams/info?activityId={0}";

            public static string CountryCodesGet = "codes?type=countrycodes";
            public static string ResourceGet = "resources/{0}";

            //public static string ActivitiesSearch = "activities?customerId={0}&limit={1}&orderBy={2}&orderDirection={3}&skip={4}";
            //public static string CourseCalendar = "activities?customerId={0}&courseCalendar=true";
            //public static string ProductDescriptionGet = "productdescriptions?customerId={1}&genericOnly=false&productId={0}&simple=false";

        }
        public static bool ShouldSendToken(string verb, string url)
        {
            //if (verb == "GET" && url.StartsWith("customerarticles/")) return true;
            if (verb == "GET")
            {
                if (url.StartsWith("accounts?")) return true;
                if (url.StartsWith("customers/")) return true;
                if (url.EndsWith("leadingtexts")) return true;
                if (url.StartsWith("files/")) return true;

                if (url.StartsWith("participants/")) return true;    //ParticipantGet
                if (url.StartsWith("rcos/exams/")) return true;
                if (url.StartsWith("resources/")) return true;
                if (url.StartsWith("courseprograms?")) return true;
                if (url.StartsWith("productsincourseprogram?")) return true;
            }
            if (url.EndsWith("/competence")) return true;

            if ((verb == "PUT" || verb == "POST") && url.StartsWith("accounts") && !url.EndsWith("/logon") && !url.EndsWith("/changepassword")) return true;
            //if (verb == "PUT" && url.EndsWith("submitexam")) return true;
            if (verb == "PUT")
            {
                if (url.StartsWith("participants/") && url.Contains("/status/")) return true;
                if (url.StartsWith("performances/register/")) return true;
                if (url.StartsWith("activitysets/") && url.Contains("enroll?")) return true;
            }
            return false;
        }
        public static dynamic UploadFile(string fileName, string contentType, byte[] data)
        {
            jlib.net.HTTP http = new jlib.net.HTTP();
            string url = System.Configuration.ConfigurationManager.AppSettings["phoenix.url"] + "/files";
            //System.Net.CookieContainer cookies = new System.Net.CookieContainer();
            string result = convert.cStreamToString(http.HttpUploadFile(url, null, new List<jlib.net.HTTP.HttpUploadFileItem>() { new jlib.net.HTTP.HttpUploadFileItem("file", fileName, contentType, data) }, null, new Dictionary<string, string>() { { "PhoenixAuthToken", System.Configuration.ConfigurationManager.AppSettings["phoenix.api.token"] } }));
            if (http.Error)
            {
                Util.Phoenix.Helpers.HandleError(http.ErrorObject, null);
                return null;
            }
            return jlib.functions.json.DynamicJson.Parse(result);
        }
        public class RequestResult
        {
            public RequestResult()
            {
                this.HttpResponseCode = 200;
            }
            public void SetError(Exception e)
            {
                RequestError = e;
                if (e as WebException != null)
                {
                    System.Net.WebException ex = (System.Net.WebException)e;
                    if (ex.Response != null)
                    {
                        HttpErrorMessage = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                        if (HttpErrorMessage.Str().StartsWith("{"))
                        {
                            try
                            {
                                HttpErrorJSON = jlib.functions.json.DynamicJson.Parse(HttpErrorMessage);
                            }
                            catch (Exception) { }
                        }
                    }
                    HttpResponseCode = parse.inner_substring(ex.Message, null, "(", ")", null).Int();
                }
                if (e != null && HttpResponseCode == 200) HttpResponseCode = 0;
            }
            public bool Error
            {
                get
                {
                    return HttpResponseCode != 200;
                }
            }
            public Exception RequestError { get; private set; }
            public string Data { get; set; }
            public int HttpResponseCode { get; set; }
            public string HttpErrorMessage { get; private set; }
            public jlib.functions.json.DynamicJson HttpErrorJSON { get; private set; }
            private dynamic m_oDataObject;
            public dynamic DataObject
            {
                get
                {
                    if (Data.IsNullOrEmpty()) return null;
                    if (m_oDataObject == null) m_oDataObject = jlib.functions.json.DynamicJson.Parse(Data);
                    return m_oDataObject;
                }
            }
            public WebResponse WebResponse { get; set; }
        }
        public void PhoenixClearCache()
        {
            int userId = this.GetUserId();
            if (userId > 0)
            {
                AttackData.UrlCaches.GetByField(Username: userId.Str()).Execute().Delete();
                PhoenixClearCache(String.Format(Phoenix.session.Queries.UserGet, userId));
            }
        }
        public void PhoenixClearCache(string url)
        {
            if (!url.StartsWith("http")) url = System.Configuration.ConfigurationManager.AppSettings["phoenix.url"] + "/" + url;
            AttackData.UrlCaches.GetByField(Url: url).Execute().Delete();
        }
        public RequestResult PhoenixGet(string url)
        {
            return PhoenixGet(url, true, false);
        }
        public RequestResult PhoenixGet(string url, bool allowCache, bool globalCache)
        {
            return PhoenixRequest("GET", url, null, allowCache, globalCache);
        }
        public RequestResult PhoenixPost(string url, object postData)
        {
            return PhoenixRequest("POST", url, postData, false, false);
        }
        public RequestResult PhoenixPut(string url)
        {
            return PhoenixPut(url, null);
        }
        public RequestResult PhoenixPut(string url, object putData)
        {
            return PhoenixRequest("PUT", url, putData, false, false);
        }

        public RequestResult PhoenixRequest(string verb, string url, object postData, bool allowCache, bool globalCache)
        {
            return PhoenixRequest(verb, url, postData, allowCache, globalCache, false);
        }
        public RequestResult PhoenixRequest(string verb, string url, object postData, bool allowCache, bool globalCache, bool returnAsWebResponse)
        {
            return PhoenixRequest(verb, url, postData, (allowCache ? System.Configuration.ConfigurationManager.AppSettings["phoenix.data.cache"].Int() : 0), globalCache, returnAsWebResponse);
        }
        public RequestResult PhoenixRequest(string verb, string url, object postData, int cacheLength, bool globalCache, bool returnAsWebResponse)
        {
            return PhoenixRequest(verb, url, postData, cacheLength, globalCache, returnAsWebResponse, null);
        }
        public RequestResult PhoenixRequest(string verb, string url, object postData, int cacheLength, bool globalCache, bool returnAsWebResponse, string phoenixCookieValue)
        {
            var userId = GetUserId(false);
            bool isTokenRequest = false;
            if (returnAsWebResponse) cacheLength = 0;
            string phoenixUrl = System.Configuration.ConfigurationManager.AppSettings["phoenix.url"];
            //AttackData.PhoenixSession session = GetSession();
            System.Net.CookieContainer cookies = new System.Net.CookieContainer();
            if (phoenixCookieValue == null) phoenixCookieValue = GetPhoenixCookie();
            if (!url.EndsWith("/logon") && !phoenixCookieValue.IsNullOrEmpty()) cookies.Add(new System.Net.Cookie("PhoenixAuth", phoenixCookieValue, "/", new System.Uri(phoenixUrl).Host));

            string jsonUrl = phoenixUrl + "/" + url;
            string jsonContent = (postData == null ? "" : (postData.GetType() == typeof(string) ? postData as string : jlib.functions.json.DynamicJson.Serialize(postData)));
            RequestResult result = new RequestResult();
            AttackData.UrlCach cache = null;
            if (cacheLength > 0 && (userId > 0 || globalCache))
            {
                var caches = AttackData.UrlCaches.GetByField(Url: jsonUrl, Method: verb, Username: (globalCache ? null : userId.Str()), PostDataChecksum: (verb == "POST" ? jsonContent.GetHashCode() : 0));
                cache = caches.Execute().FirstOrDefault();
                if (cache != null && cache.PostedDate > DateTime.Now.AddSeconds(-cacheLength) && !cache.Response.IsNullOrEmpty())
                {
                    result.Data = cache.Response;
                    return result;
                }
            }
            AttackData.Log logEntry = null;
            if (ConfigurationManager.AppSettings["site.dev"].Bln())
            {
                logEntry = new AttackData.Log() { Event = url.Substr(0, 50) };
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
                io.write_file(System.Configuration.ConfigurationManager.AppSettings["webservice.comm.log"], "\n\n" + verb + " " + jsonUrl + "\n" + postData.Str(), true);
            }
            try
            {
                Stopwatch timer = new Stopwatch();
                timer.Start();
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(jsonUrl);
                request.Method = verb;
                if (this.AlwaysSendToken || ShouldSendToken(verb, url))
                {
                    isTokenRequest = true;
                    request.Headers.Add("PhoenixAuthToken", convert.cStr(OverrideToken, System.Configuration.ConfigurationManager.AppSettings["phoenix.api.token"]));
                }
                else
                    request.CookieContainer = cookies;
                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                if (verb == "POST" || verb == "PUT") request.ContentLength = 0;
                if (!jsonContent.IsNullOrEmpty())
                {
                    byte[] byteArray = postData as byte[];
                    if (byteArray == null)
                    {
                        byteArray = encoding.GetBytes(jsonContent);
                        request.ContentType = @"application/json";
                    }
                    else
                    {
                        string boundary = "----------" + DateTime.Now.Ticks.ToString("x");
                        request.ContentType = @"multipart/form-data; boundary=" + boundary;
                    }
                    request.ContentLength = byteArray.Length;
                    request.GetRequestStream().Write(byteArray, 0, byteArray.Length);
                }
                try
                {
                    result.WebResponse = request.GetResponse();
                    if (!returnAsWebResponse)
                    {
                        result.Data = new StreamReader(result.WebResponse.GetResponseStream(), Encoding.UTF8).ReadToEnd();
                    }
                }
                catch (WebException ex)
                {
                    result.SetError(ex);

                    if (result.HttpResponseCode == 401)
                    {
                        if (logEntry != null)
                        {
                            timer.Stop();
                            logEntry.Status = "Status: " + result.HttpResponseCode + " / Elapsed: " + timer.Elapsed;
                            logEntry.Save();
                            io.write_file(System.Configuration.ConfigurationManager.AppSettings["webservice.comm.log"], "\n\n" + "Status: " + result.HttpResponseCode + " / " + result.HttpErrorMessage + " / Elapsed: " + timer.Elapsed, true);
                        }
                    }
                    Util.Phoenix.Helpers.HandleError(ex, this);
                }
                timer.Stop();
                if (logEntry != null)
                {
                    logEntry.Status = "Status: " + result.HttpResponseCode + " / Elapsed: " + timer.Elapsed;
                    logEntry.Save();
                    io.write_file(System.Configuration.ConfigurationManager.AppSettings["webservice.comm.log"], "\n\n" + "Status: " + result.HttpResponseCode + " / Elapsed: " + timer.Elapsed + "\n" + (result.Error ? result.HttpErrorMessage : result.Data), true);
                }
            }
            catch (Exception e)
            {
                result.SetError(e);
            }
            if (result.RequestError != null) return result;


            //if (System.Configuration.ConfigurationManager.AppSettings["site.portal2"].Bln() && m_oActiveUser != null && m_oActiveUser.DistributorId > 0 && m_oActiveUser.DistributorId != 10000)
            //    Response.Redirect("portal2/#/portal/home");

            var isPSO = false;
            if(System.Web.HttpContext.Current !=null)
                isPSO = jlib.net.HTTP.getCookieValue(System.Web.HttpContext.Current.Request.Cookies, "DistributorId", "").Int() == 10000;

            if (!isTokenRequest && (phoenixCookieValue.IsNullOrEmpty() || isPSO))
            {
                System.Net.CookieCollection collection = cookies.GetCookies(new Uri(phoenixUrl));
                string previousPhoenixCookieValue = phoenixCookieValue;
                for (int x = 0; x < collection.Count; x++)
                {
                    //Did user just log in, or is a new cookie value returned?
                    if (collection[x].Name == "PhoenixAuth" && previousPhoenixCookieValue != collection[x].Value && phoenixCookieValue != collection[x].Value)
                    {
                        var shouldSetDomain = System.Web.HttpContext.Current.Request.Url.OriginalString.IndexOf("psoetraining.com") == -1;
                        SetPhoenixCookie(collection[x].Value, shouldSetDomain ? collection[x].Domain : null);
                        phoenixCookieValue = collection[x].Value;
                    }
                }
                //if (!phoenixCookieValue.IsNullOrEmpty() && url.EndsWith("/logon") && result.HttpResponseCode == 200)
                //{
                //    session = AttackData.PhoenixSessions.GetByField(PhoenixCookie: phoenixCookieValue).Execute().FirstOrDefault();
                //    if (session == null) new AttackData.PhoenixSession() { PhoenixCookie = phoenixCookieValue, Username = parse.inner_substring(url, "accounts/", "", "/logon", "") }.Save();
                //}
                //else if (previousPhoenixCookieValue != phoenixCookieValue)
                //{
                //    //If the cookie value was changed, create a new PhoenixSession database record
                //    var oldSession = AttackData.PhoenixSessions.GetByField(PhoenixCookie: previousPhoenixCookieValue).Execute().FirstOrDefault();
                //    session = AttackData.PhoenixSessions.GetByField(PhoenixCookie: phoenixCookieValue).Execute().FirstOrDefault();
                //    if (session == null && oldSession != null)
                //    {
                //        session = new AttackData.PhoenixSession();
                //        session.Username = oldSession.Username;
                //        session.PhoenixCookie = phoenixCookieValue;
                //        session.Save();
                //    }
                //}
            }
            if (result.HttpResponseCode == 200 && cacheLength > 0 && (userId > 0 || globalCache))
            {
                if (cache == null)
                    cache = new AttackData.UrlCach()
                    {
                        Url = jsonUrl,
                        Method = verb,
                        PostDataChecksum = (verb == "POST" ? jsonContent.GetHashCode() : 0),
                        Username = (globalCache ? null : userId.Str())
                    };
                cache.Response = result.Data;
                cache.PostedDate = DateTime.Now;
                cache.Save();
            }
            return result;
        }
        private string m_sPhoenixCookie;
        public void SetPhoenixCookie(string value, string domain)
        {
            if (value == null) m_sPhoenixCookie = "";
            else m_sPhoenixCookie = value;
            Helpers.DeletePhoenixSession(System.Web.HttpContext.Current.Request, System.Web.HttpContext.Current.Response);
            if (value.IsNotNullOrEmpty())
            {
                HttpCookie cookie = new HttpCookie("PhoenixAuth", value);
                cookie.Expires = DateTime.Now.AddHours(5);
                if(!string.IsNullOrWhiteSpace(domain)) cookie.Domain = domain;
                System.Web.HttpContext.Current.Response.Cookies.Add(cookie);

                //var requestUrl = System.Web.HttpContext.Current.Request.RawUrl;
                ////var responseCode = System.Web.HttpContext.Current.Response.StatusCode;

                //var requestRaw = System.Web.HttpContext.Current.Request.HttpMethod + " " + System.Web.HttpContext.Current.Request.Url.OriginalString + "\n\n" + System.Web.HttpContext.Current.Request.ServerVariables["ALL_RAW"];
                //using (Stream stream = System.Web.HttpContext.Current.Request.InputStream)
                //{
                //    stream.Position = 0;
                //    using (StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8))
                //    {
                //        requestRaw += "\n\n" + reader.ReadToEnd();
                //    }
                //}

                //var serverVariables = "";
                //foreach (string key in System.Web.HttpContext.Current.Request.ServerVariables)
                //{
                //    serverVariables += String.Format("{0}: {1}\n", key, System.Web.HttpContext.Current.Request.ServerVariables[key]);
                //}

                //jlib.net.SMTP.sendEmail("noreply@metier.no", "jorgen@vontangen.com", "Phoenix cookie issued", "Cookie: " + value + " - Domain: " + domain + "\nCurrent time: " + DateTime.Now
                //    + $"requestUrl: {requestUrl}\nrequestRaw: {requestRaw}\nserverVariables: {serverVariables}"
                //    , System.Configuration.ConfigurationManager.AppSettings["smtp.server"]);

                //jlib.net.ExceptionHandler.HandleException(new Exception("Phoenix cookie issued - Cookie: " + value + " - Domain: " + domain));
            }
        }
        public string GetPhoenixCookie()
        {
            if (m_sPhoenixCookie == null)
            {
                m_sPhoenixCookie = jlib.net.HTTP.getCookieValue(System.Web.HttpContext.Current.Response.Cookies, "PhoenixAuth", "");
                if (m_sPhoenixCookie.IsNullOrEmpty()) m_sPhoenixCookie = jlib.net.HTTP.getCookieValue(System.Web.HttpContext.Current.Request.Cookies, "PhoenixAuth", "");
            }
            return m_sPhoenixCookie;
        }
        //private AttackData.PhoenixSession m_oSession;
        //public AttackData.PhoenixSession GetSession()
        //{
        //    if (GetPhoenixCookie().IsNullOrEmpty()) return null;
        //    if (m_oSession == null || m_oSession.PhoenixCookie != GetPhoenixCookie())
        //    {
        //        m_oSession = AttackData.PhoenixSessions.GetAll().Where(x=> x.PhoenixCookie == GetPhoenixCookie()).Execute().FirstOrDefault();
        //    }
        //    //Phoenix cookie was not found, try creating session
        //    if (m_oSession == null)
        //    {
        //        m_oSession = new AttackData.PhoenixSession { PhoenixCookie = GetPhoenixCookie() };
        //        var result = PhoenixGet(Queries.UserGetCurrent);
        //        if (result.HttpResponseCode == 200)
        //        {
        //            var updatedSession = AttackData.PhoenixSessions.GetAll().Where(x => x.PhoenixCookie == GetPhoenixCookie()).Execute().FirstOrDefault();
        //            if (updatedSession != null) m_oSession = updatedSession;
        //            m_oSession.Username = convert.SafeGetProperty(result.DataObject, "Username");
        //            m_oSession.Save();
        //        }
        //        else
        //        {
        //            m_oSession = null;
        //        }

        //        //new AttackData.Log {
        //        //    Event="Cookie Not Found - logging out",
        //        //    Status = "The Phoenix cookie was not found in d_phoenix_cookies. This should never happen. Logging out -- Cookie: " + GetPhoenixCookie()
        //        //}.Save();
        //    }
        //    return m_oSession;
        //}
        public int GetUserId(bool CallPhoenixApiIfNecessary = true)
        {
            var key = "";
            var userId = 0;

            if (GetPhoenixCookie().IsNotNullOrEmpty() && HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                key = "Cookie-to-UserId-" + GetPhoenixCookie();
                userId = HttpContext.Current.Session[key].Int();
            }

            if (CallPhoenixApiIfNecessary && userId == 0 && GetPhoenixCookie().IsNotNullOrEmpty())
            {
                //AttackData.PhoenixSession session = GetSession();
                //if (session == null) return 0;
                var result = PhoenixGet(Queries.UserGetCurrent);
                if (result.HttpResponseCode == 200) userId = convert.cInt(convert.SafeGetProperty(result.DataObject, "User", "Id"));
                if (key.IsNotNullOrEmpty() && userId > 0 && HttpContext.Current != null && HttpContext.Current.Session != null) HttpContext.Current.Session[key] = userId;
            }
            return userId;
        }
    }
}