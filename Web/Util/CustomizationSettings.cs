﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using jlib.functions;

/// <summary>
/// Summary description for DistributorHelper
/// </summary>
namespace Phoenix.LearningPortal.Util.Customization
{

    public class Settings
    {
        private static Dictionary<int, DistributorSettings> _CachedDistributorSettings = new Dictionary<int, DistributorSettings>();
        private static Dictionary<int, CustomerSettings> _CachedCustomerSettings = new Dictionary<int, CustomerSettings>();
        internal static DistributorSettings GetDistributorSettings(int DistributorId)
        {
            lock (_CachedDistributorSettings)
            {
                var DistributorSettings = _CachedDistributorSettings.SafeGetValue(DistributorId);
                DistributorSettings = null;
                if (DistributorSettings != null && DistributorSettings.LoadDate < DateTime.Now.AddMinutes(-5)) DistributorSettings = null;
                if (DistributorSettings == null)
                {
                    DistributorSettings = new DistributorSettings(DistributorId);
                    _CachedDistributorSettings[DistributorId] = DistributorSettings;
                }
                return DistributorSettings;
            }
        }
        internal static CustomerSettings GetCustomerSettings(int CustomerId)
        {
            lock (_CachedCustomerSettings)
            {
                var CustomerSettings = _CachedCustomerSettings.SafeGetValue(CustomerId);
                CustomerSettings = null;
                if (CustomerSettings != null && CustomerSettings.LoadDate < DateTime.Now.AddMinutes(-5)) CustomerSettings = null;
                if (CustomerSettings == null)
                {
                    CustomerSettings = new CustomerSettings(CustomerId);
                    _CachedCustomerSettings[CustomerId] = CustomerSettings;
                }
                return CustomerSettings;
            }
        }
        private static List<int> _DistributorIdsWithCustomUI = new List<int>();
        private static DateTime _DistributorIdsWithCustomUIDate = DateTime.MinValue;
        public static List<int> GetDistributorIdsWithCustomUI()
        {
            lock (_DistributorIdsWithCustomUI)
            {
                if(_DistributorIdsWithCustomUIDate < DateTime.Now.AddMinutes(-60))
                {
                    _DistributorIdsWithCustomUI.Clear();
                    System.IO.Directory.GetDirectories(System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "customizations\\distributor\\").ToList().ForEach(path => {
                        path = path.Substr(path.LastIndexOf("\\")+1);
                        if (convert.cInt(path) > 0) _DistributorIdsWithCustomUI.Add(convert.cInt(path));
                    });
                    _DistributorIdsWithCustomUIDate = DateTime.Now;
                }                
                return _DistributorIdsWithCustomUI;
            }
        }

        public static int GetDistributorIdForDomain(string Domain)
        {
            if (Domain.IsNullOrEmpty()) return 0;
            return GetDistributorIdsWithCustomUI().FirstOrDefault(distId => {
                return GetSettings(distId, null).GetSetting("LearningPortalDomain").Str().ToLower() == Domain.ToLower();                
            });            
        }

        public static SettingsHelper GetSettings(Util.Classes.user User) 
        {
            int? distirbutorId = null, customerId = null;
            if (User != null)
            {
                distirbutorId = User.DistributorId;
                if (User.Organization != null) customerId = User.Organization.MasterCompanyID;
                else customerId = User.OrganizationID;
            }

            return GetSettings(distirbutorId, customerId);

        }
        public static SettingsHelper GetSettings(int? DistributorId, int? CustomerId)
        {
            return new SettingsHelper(DistributorId, CustomerId);
        }
        public class SettingsHelper
        {
            private DistributorSettings _DistributorSettings;
            private CustomerSettings _CustomerSettings;
            public SettingsHelper(int? DistributorId, int? CustomerId)
            {
                if (DistributorId.HasValue)
                    _DistributorSettings = Settings.GetDistributorSettings(DistributorId.Int());
                if (CustomerId.HasValue)
                    _CustomerSettings = Settings.GetCustomerSettings(CustomerId.Int());
            }
            
            public string GetSetting(string Key)
            {
                string value=null;
                if (_CustomerSettings != null)
                    value = _CustomerSettings.GetSetting(Key);
                if (value != null) return value;
                if (_DistributorSettings != null)
                    value = _DistributorSettings.GetSetting(Key);
                return value;
            }
            public string PageTitlePrefix
            {
                get
                {
                    return GetSetting("PageTitlePrefix");
                }
            }
            public string AnalyticsJS
            {
                get
                {
                    return GetSetting("AnalyticsJS");
                }
            }
            public string LearningPortalCSSUrl
            {
                get
                {
                    return GetSetting("LearningPortalCSSUrl");
                }
            }
            public string FaviconUrl
            {
                get
                {
                    return GetSetting("FaviconUrl");
                }
            }
            public string PDFWatermark
            {
                get
                {
                    return GetSetting("PDFWatermark");
                }
            }
            public string PortalEmailTo
            {
                get
                {
                    return GetSetting("PortalEmailTo");
                }
            }            
        }
 
        internal abstract class BaseSettings
        {
            protected Dictionary<string, string> _Settings;
            public string GetSetting(string Key)
            {
                string value=null;
                lock (_Settings)
                {
                    _Settings.TryGetValue(Key, out value);
                    return value;
                }                    
            }
            public DateTime LoadDate { get; protected set; }
        }

        internal class DistributorSettings: BaseSettings
        {            
            public DistributorSettings(int DistributorId)
            {
                this.DistributorId = DistributorId;
                LoadDate = DateTime.Now;
                _Settings = new Dictionary<string, string>();
                if (System.IO.Directory.Exists(System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "customizations\\distributor\\" + DistributorId + "\\"))
                {
                    var path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "customizations\\distributor\\" + DistributorId + "\\settings.xml";
                    if (System.IO.File.Exists(path))
                    {
                        var doc = xml.loadXml(System.IO.File.ReadAllText(path));
                        foreach (XmlNode setting in doc.SelectNodes("distributor/settings/setting"))
                        {
                            _Settings[xml.getXmlAttributeValue(setting, "key")] = setting.InnerText;
                        }
                    }
                }

            }
            public int DistributorId { get; private set; }            
        }
        internal class CustomerSettings : BaseSettings
        {
            public int CustomerId { get; private set; }
            public CustomerSettings(int CustomerId)
            {
                LoadDate = DateTime.Now;
                this.CustomerId = CustomerId;
                _Settings = new Dictionary<string, string>();
                if (System.IO.Directory.Exists(System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "customizations\\customer\\" + CustomerId + "\\"))
                {
                    var path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "customizations\\customer\\" + CustomerId + "\\settings.xml";
                    if (System.IO.File.Exists(path))
                    {
                        var doc = xml.loadXml(System.IO.File.ReadAllText(path));
                        foreach (XmlNode setting in doc.SelectNodes("distributor/settings/setting"))
                        {
                            _Settings[xml.getXmlAttributeValue(setting, "key")] = setting.InnerText;
                        }
                    }
                }

            }
        }
    }
}