﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.functions;

/// <summary>
/// Summary description for ecommerce
/// </summary>
namespace Phoenix.LearningPortal.Util
{
    public class Ecommerce
    {
        public Ecommerce()
        {

        }
        public class GoogleLogger
        {
            public static string GetDomain(string siteCode)
            {
                if (siteCode == "P2NO") return "www.prince2.no";
                if (siteCode == "P2SE") return "www.prince2.se";
                if (siteCode == "BPNO") return "www.bedreprosjekter.no";
                if (siteCode == "PLNO") return "www.metieroec.no";
                if (siteCode == "MPEN") return "www.metieroecacademy.com";
                if (siteCode == "P2DK") return "kursus.metier.dk";
                if (siteCode == "P2DE") return "www.prince2-kurse.de";
                return "";
            }
            public static string GetUACode(string siteCode)
            {
                if (siteCode == "P2NO") return "UA-45293503-7";
                if (siteCode == "P2SE") return "UA-45293503-8";
                if (siteCode == "PLNO") return "UA-45293503-4";
                if (siteCode == "MPEN") return "UA-53165966-13";
                return "UA-45293503-1";
            }
            public string SiteCode { get; private set; }
            public string ClientID { get; private set; }
            public GoogleLogger(string siteCode, string clientID)
            {
                SiteCode = siteCode;
                ClientID = clientID;
                //Remove GA-prefix, if present
                if (ClientID.Str().StartsWith("GA"))
                    ClientID = ClientID.SafeSplit(".", -2) + "." + ClientID.SafeSplit(".", -1);

                if (ClientID.IsNullOrEmpty()) ClientID = "555";
            }
            private jlib.net.HTTP m_oHTTPObject;
            public jlib.net.HTTP HTTPObject
            {
                get
                {
                    if (m_oHTTPObject == null)
                    {
                        m_oHTTPObject = new jlib.net.HTTP();
                        m_oHTTPObject.Debug = true;
                        m_oHTTPObject.LogFile = System.Web.HttpContext.Current.Server.MapPath("/jlib.http.txt");
                        m_oHTTPObject.RetryAttemps = 1;
                    }
                    return m_oHTTPObject;
                }
            }
            public string LogTransaction(int orderNumber, double orderTotal, string currency)
            {
                return HTTPObject.Post("http://www.google-analytics.com/collect",
                    "v=1"              // Version.
    + "&tid=" + GetUACode(SiteCode)   // Tracking ID / Property ID.
    + "&cid=" + ClientID         // Anonymous Client ID.
    + "&dh=" + parse.stripLeadingCharacter(GetDomain(SiteCode), "www.")     // Document Host Name
    + "&t=transaction"   // Transaction hit type.
    + "&ti=" + orderNumber        // transaction ID. Required.
                                  //+"&ta=westernWear"  // Transaction affiliation.
    + "&tr=" + orderTotal        // Transaction revenue.
                                 //+"&ts=32.00"        // Transaction shipping.
                                 //+"&tt=12.00"        // Transaction tax.
    + "&cu=" + currency          // Currency code.
                    , null, "http://" + GetDomain(SiteCode), null, 60);//, new string[]{"Host: " +  parse.stripLeadingCharacter(GetDomain(SiteCode),"www.")});
            }
            public string LogTransactionItem(int orderNumber, string name, string sku, string category, double price, int quantity, string currency)
            {
                return HTTPObject.Post("http://www.google-analytics.com/collect",
                    "v=1"              // Version.
    + "&tid=" + GetUACode(SiteCode)   // Tracking ID / Property ID.
    + "&cid=" + ClientID         // Anonymous Client ID.
    + "&dh=" + parse.stripLeadingCharacter(GetDomain(SiteCode), "www.")     // Document Host Name
    + "&t=item"   // Item hit type.
    + "&ti=" + orderNumber        // transaction ID. Required.
    + "&in=" + System.Web.HttpUtility.UrlEncode(name)         // Item name. Required.
    + "&ip=" + price          // Item price.
    + "&iq=" + quantity       // Item quantity.
    + "&ic=" + sku     // Item code / SKU.
    + "&iv=" + category    // Item variation / category.
    + "&cu=" + currency          // Currency code.
                    , null, "http://" + GetDomain(SiteCode), null, 60);//, new string[]{"Host: " +  parse.stripLeadingCharacter(GetDomain(SiteCode),"www.")});

            }
        }
    }
}
