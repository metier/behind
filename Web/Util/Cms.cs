﻿using System;
using System.Configuration;
using System.Xml;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Data;
using System.Data.OleDb;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using jlib.db;
using jlib.functions;
using System.Security.Cryptography;
using System.Text;
using ExpertPdf.HtmlToPdf;
using SpreadsheetGear;
using AttackData = Phoenix.LearningPortal.Data;

namespace Phoenix.LearningPortal.Util
{
    public class Cms
    {
        public static byte[] cPDF(string sHTML, string sHeaderText, string sFooterText)
        {
            return cPDF(sHTML, sHeaderText, sFooterText, true);
        }
        public static byte[] cPDF(string sHTML, string sHeaderText, string sFooterText, bool bPagination)
        {
            PdfConverter oConverter = new PdfConverter();

            oConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            oConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;

            if (convert.cStr(sHeaderText) != "")
            {
                oConverter.PdfDocumentOptions.ShowHeader = true;
                oConverter.PdfHeaderOptions.HeaderText = sHeaderText;
            }
            if (convert.cStr(sFooterText) != "")
            {
                oConverter.PdfDocumentOptions.ShowFooter = true;
                oConverter.PdfFooterOptions.FooterText = sFooterText;
            }
            oConverter.PdfDocumentOptions.LeftMargin = 40;
            oConverter.PdfDocumentOptions.RightMargin = 40;
            oConverter.PdfDocumentOptions.TopMargin = 40;
            oConverter.PdfDocumentOptions.BottomMargin = 40;
            oConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            oConverter.AvoidImageBreak = true;
            if (!bPagination)
            {
                oConverter.PdfDocumentOptions.ShowHeader = false;
                oConverter.PdfDocumentOptions.ShowFooter = false;
                oConverter.PdfDocumentOptions.LeftMargin = 10;
                oConverter.PdfDocumentOptions.RightMargin = 10;
                oConverter.PdfDocumentOptions.TopMargin = 10;
                oConverter.PdfDocumentOptions.BottomMargin = 10;
                oConverter.AvoidImageBreak = false;
            }
            //oConverter.PdfDocumentOptions.FitWidth = true;
            //oConverter.PdfDocumentOptions.SinglePage = false;



            //oConverter.PdfDocumentOptions.ShowHeader = false;
            //oConverter.PdfHeaderOptions.HeaderText = "Sample header: " + TxtURL.Text;
            //oConverter.PdfHeaderOptions.HeaderTextColor = Color.Blue;
            //oConverter.PdfHeaderOptions.HeaderSubtitleText = string.Empty;
            //oConverter.PdfHeaderOptions.DrawHeaderLine = false;

            //oConverter.PdfFooterOptions.FooterText = "Sample footer: " + TxtURL.Text + ". You can change color, font and other options";

            //oConverter.PdfFooterOptions.FooterTextColor = Color.Blue;
            //oConverter.PdfFooterOptions.DrawFooterLine = false;
            //oConverter.PdfFooterOptions.PageNumberText = "Page";
            //oConverter.PdfFooterOptions.ShowPageNumber = true;

            //oConverter.LicenseKey = "UYsNc1YW9AZtxG7QMr+xEtX/ntDnFDuTMqfRb5fGpIiVIC7bzkXlZIdQYDglkl8R";
            oConverter.PdfDocumentOptions.JpegCompressionEnabled = false;
            oConverter.LicenseKey = "hq20pr6mtLKmvqi2prW3qLe0qL+/v78=";

            return oConverter.GetPdfBytesFromHtmlString(sHTML);
        }
        public static bool HasCMSAccess(Util.Classes.user User)
        {
            if (User.IsBehindSuperAdmin || User.IsBehindContentAdmin) return true;
            return (new ado_helper("sql.dsn.cms")).Execute_SQL("select count(*) from t_permissions where UserID=@user_id", "@user_id", User.ID).SafeGetValue(0, 0).Int() > 0;
        }
        public static void RecalculateUserPermission(Util.Classes.user User)
        {
            if (User.IsBehindSuperAdmin || User.IsBehindContentAdmin) return;
            new SqlHelper(System.Configuration.ConfigurationManager.AppSettings["sql.dsn.cms"]).ExecuteSproc("sp_recalc_permissions", "@user_id", User.ID);
        }
        public static string FixImportedQuiz(AttackData.Folder course, string content)
        {

            try
            {
                XmlDocument xml = jlib.functions.xml.loadXml(content);
                if (course != null)
                {
                    XmlNodeList backgrounds = xml.SelectNodes("//question[@background]");
                    foreach (XmlNode node in backgrounds)
                    {
                        if (!jlib.functions.xml.getXmlAttributeValue(node, "background").IsNullOrEmpty() && !jlib.functions.xml.getXmlAttributeValue(node, "background").StartsWith("http"))
                            jlib.functions.xml.setXmlAttributeValue(node, "background", "https://mymetier.net/content/public/" + course.getSetting("destdir") + "/oppgaver/" + parse.replaceAll(jlib.functions.xml.getXmlAttributeValue(node, "background"), ".swf", ".png"));
                    }
                }

                XmlNodeList connecttheboxes = xml.SelectNodes("//question[@type='connecttheboxes']");
                foreach (XmlNode node in connecttheboxes)
                    jlib.functions.xml.setXmlAttributeValue(node.SelectSingleNode("boxes"), "arrange", "absolute", "draw", "true");

                XmlNodeList draganddrop = xml.SelectNodes("//question[@type='draganddrop'][@background]");
                foreach (XmlNode node in draganddrop)
                {
                    jlib.functions.xml.setXmlAttributeValue(node.SelectSingleNode("drags"), "align", "top");
                    jlib.functions.xml.setXmlAttributeValue(node.SelectSingleNode("targets"), "position", "absolute");
                }
                return xml.OuterXml;
            }
            catch (Exception) { }
            return content;
        }

        public static Tuple<bool,string> GenerateFinalTest(AttackData.Folder Course, bool CreateFinaltestIfNotExists)
        {
            var DisableAutoGeneration = false;
            try {
                DisableAutoGeneration = convert.cBool(convert.SafeGetProperty(jlib.functions.json.DynamicJson.Parse(convert.cStr(Course.getSetting("PlayerSettings"))), "Quiz", "DisableFinaltestAutoGeneration"));
            }catch (Exception) { }
            var NumFinalTestQuestions = 20;
            try
            {
                NumFinalTestQuestions = convert.cInt(convert.SafeGetProperty(jlib.functions.json.DynamicJson.Parse(convert.cStr(Course.getSetting("PlayerSettings"))), "Quiz", "NumFinalTestQuestions"),20);
            }
            catch (Exception) { }
            object versioning = System.Web.HttpContext.Current.Items["versioning"];
            System.Web.HttpContext.Current.Items["versioning"] = false;
            AttackData.Folder Finaltest = Course.GetChildren<AttackData.Folder>().Find(x => x.AuxField1 == "finaltest");
            if (DisableAutoGeneration && Finaltest != null) return new Tuple<bool, string>(false, "The course is set to Disable Finaltest Auto Generation. Finaltests could therefor not be generated.");
            if (Finaltest == null)
            {
                if (!CreateFinaltestIfNotExists) return new Tuple<bool, string>(false, "The parameter CreateFinaltestIfNotExists is true, and since no final tests exists in the course I am prevented from creating a new one.");
                Finaltest = new AttackData.Folder()
                {
                    Name = "Final Test",
                    Context_Value = "lesson",
                    AuxField1 = "finaltest"
                };
                Finaltest.setSetting("Weight", "6");
                Course.addChild(Finaltest);
            }
            //Get all questions from course
            var Folders = AttackData.Folders.GetNestedFolders(Course.ObjectID).Execute();
            //.SelectMany(folder => folder.GetChildren<AttackData.Content>().FindAll(content => content.AuxField1 == "quiz")).ToList();
                //.Where(AttackData.Folders.Columns.AuxField1 == "quiz").Execute();
            List<string> Questions = new List<string>();
            Folders.ForEach(x=> {
                
                if (x.GetParents<AttackData.Folder>().Count > 0 && x.GetParents<AttackData.Folder>()[0].AuxField1 != "finaltest" && !x.Name.Contains(".xml"))
                {
                    var Contents = x.GetChildren<AttackData.Content>().FindAll(content => content.AuxField1 == "quiz").ToList();
                    Contents.ForEach(Content =>
                    {
                        if (Content != null && Content.Xml.Str() != "")
                        {
                            XmlDocument xmlQuiz = jlib.net.sgmlparser.SgmlReader.getXmlDoc(Content.Xml);// xml.loadXml(Content.Xml);
                            XmlNodeList xmlQuestions = xmlQuiz.SelectNodes("//quiz/questions/question");
                            foreach (XmlNode xmlQuestion in xmlQuestions) Questions.AddIfNotExist(xmlQuestion.OuterXml);
                        }
                    });
                }
            });
            for (int x = 0; x < 10; x++)
            {
                AttackData.Folder Test = Finaltest.GetChildren<AttackData.Folder>().SafeGetValue(x);
                if (Test == null)
                {
                    Test = new AttackData.Folder()
                    {
                        Context_Value = "content",
                        Name = Finaltest.Name,
                        AuxField1 = "quiz"
                    };
                    Finaltest.addChild(Test);
                    Test.Save();
                }
                AttackData.Content Content = Test.GetChildren<AttackData.Content>().FirstOrDefault();
                if (Content == null)
                {
                    Content = new AttackData.Content()
                    {
                        Filename = "oppgaver\\quiz_100" + x + ".xml",
                        Name = "Finaltest",
                        Type = 2,
                        AuxField1 = "quiz"
                    };
                    Content.addParent(Test);
                }
                XmlDocument xmlQuiz = xml.loadXml("<quiz><questions /></quiz>");
                Questions.Shuffle();
                for (int y = 0; y < Questions.Count && y < NumFinalTestQuestions; y++)
                    xmlQuiz.SelectSingleNode("quiz/questions").AppendChild(xmlQuiz.ImportNode(xml.loadXml(Questions[y]).DocumentElement, true));

                Content.Xml = System.Xml.Linq.XElement.Parse(xmlQuiz.OuterXml).ToString();
                Content.Save();
            }

            Finaltest.Save();
            Course.Save();
            System.Web.HttpContext.Current.Items["versioning"] = versioning;
            return new Tuple<bool, string>(true, "Finaltests succesfully generated");
        }
        public static string SaveFile(AttackData.Folder CourseObject, AttackData.Folder LessonObject, AttackData.Folder PageObject, string Extension, byte[] Image, string FileName, bool CropImage, string FileMode)
        {
            int userID = System.Web.HttpContext.Current.Session["user.id"].Int();
            if (jlib.net.HTTP.GetFileExtensionFromMime(Extension) != ".???") Extension = jlib.net.HTTP.GetFileExtensionFromMime(Extension);
            else if (jlib.net.HTTP.lookupMimeType(Extension) == "") Extension = "png";
            Extension = parse.replaceAll(Extension, ".", "");

            AttackData.Folder ImageFolder = AttackData.Folder.LoadByPk(77640);
            AttackData.Folder CourseFolder = ImageFolder.GetChildren<AttackData.Folder>().Find(x => x.Name == convert.cStr(PageObject.Context_Value == "exam" ? PageObject.Name : "", CourseObject.AuxField1, CourseObject.Name, "Object: " + CourseObject.ObjectID));
            if (CourseFolder == null)
            {
                CourseFolder = new AttackData.Folder()
                {
                    Name = convert.cStr(PageObject.Context_Value == "exam" ? PageObject.Name : "", CourseObject.AuxField1, CourseObject.Name, "Object: " + CourseObject.ObjectID),
                    Context_Value = "file-folder"
                };
                CourseFolder.addParent(ImageFolder);
                CourseFolder.Save(userID);
            }
            AttackData.Folder LessonFolder = CourseFolder.GetChildren<AttackData.Folder>().Find(x => x.Name == convert.cStr(LessonObject.Name, "Object: " + LessonObject.ObjectID));
            if (LessonFolder == null && PageObject.Context_Value == "exam") LessonFolder = CourseFolder;
            if (LessonFolder == null)
            {
                LessonFolder = new AttackData.Folder()
                {
                    Name = convert.cStr(LessonObject.Name, "Object: " + LessonObject.ObjectID),
                    Context_Value = "file-folder"
                };
                LessonFolder.addParent(CourseFolder);
                LessonFolder.Save(userID);
            }

            string sFolder = parse.replaceAll(System.Configuration.ConfigurationManager.AppSettings["cms.upload.path"] + "\\uploads\\", "\\\\", "\\");
            AttackData.Content FileObject = new AttackData.Content() { Type = (int)AttackData.Contents.Types.FileUrl };
            if (FileMode == "file")
                FileObject.Name = convert.cStr(FileName, "File " + (LessonFolder.Children.Count + 1)) + "." + Extension;
            else
                FileObject.Name = convert.cStr(FileName, "Image " + (LessonFolder.Children.Count + 1)) + "." + Extension;

            FileObject.Filename = io.getUniqueFileName(sFolder + io.getValidFileName(FileObject.Name));
            if (FileMode == "file")
            {
                if (io.write_file(FileObject.Filename, Image))
                {
                    FileObject.setSetting("Size", Image.Length.Str());
                    FileObject.addParent(LessonFolder);
                    FileObject.Save(userID);
                    return System.Configuration.ConfigurationManager.AppSettings["site.url"] + parse.replaceAll(FileObject.Filename, sFolder, "/images/uploads/", "\\", "/");
                }
            }
            else
            {
                System.Drawing.Image Bitmap = System.Drawing.Image.FromStream(new System.IO.MemoryStream(Image));
                if (CropImage)
                {
                    Bitmap = jlib.helpers.graphics.Crop((System.Drawing.Bitmap)Bitmap);
                    Image = new System.Drawing.ImageConverter().ConvertTo(Bitmap, typeof(byte[])) as byte[];
                }
                if (io.write_file(FileObject.Filename, Image))
                {
                    FileObject.setSetting("Size", Image.Length.Str());
                    FileObject.addParent(LessonFolder);
                    FileObject.Save(userID);
                    return System.Configuration.ConfigurationManager.AppSettings["site.url"] + (Bitmap.Width > 1500 || Bitmap.Height > 1200 ? "inc/library/exec/thumbnail.aspx?w=1500&h=1200&u=" + System.Web.HttpContext.Current.Request.ApplicationPath + "/" : "") + parse.replaceAll(FileObject.Filename, sFolder, "images/uploads/", "\\", "/");
                }
            }
            return null;
        }
    }
}