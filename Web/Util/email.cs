﻿using System;
using System.Configuration;
using System.Xml;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Data;
using System.Data.OleDb;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using jlib.db;
using jlib.functions;
using System.Security.Cryptography;
using System.Text;
using SpreadsheetGear;
namespace Phoenix.LearningPortal.Util
{
    public class Email
    {
        public static bool sendEmail(string sFromEmail, string sToEmail, string sCCEmail, string sBCCEmail, string sSubject, string sBody, bool bHTML, List<KeyValuePair<string, byte[]>> oFiles)
        {
            return sendEmail(sFromEmail, sToEmail, sCCEmail, sBCCEmail, sSubject, sBody, bHTML, oFiles, true, 0, null);
        }

        public static bool sendEmail(string sFromEmail, string sToEmail, string sCCEmail, string sBCCEmail, string sSubject, string sBody, bool bHTML, string[] sAttatchmentFiles, bool bLogEmail)
        {
            return sendEmail(sFromEmail, sToEmail, sCCEmail, sBCCEmail, sSubject, sBody, bHTML, sAttatchmentFiles, bLogEmail, 0);
        }

        public static bool sendEmail(string sFromEmail, string sToEmail, string sCCEmail, string sBCCEmail, string sSubject, string sBody, bool bHTML, string[] sAttatchmentFiles, bool bLogEmail, int iUserID)
        {
            return sendEmail(sFromEmail, sToEmail, sCCEmail, sBCCEmail, sSubject, sBody, bHTML, sAttatchmentFiles, bLogEmail, iUserID, null);
        }

        public static bool sendEmail(string sFromEmail, string sToEmail, string sCCEmail, string sBCCEmail, string sSubject, string sBody, bool bHTML, string[] sAttatchmentFiles, bool bLogEmail, int iUserID, string sDSN)
        {
            List<KeyValuePair<string, byte[]>> oFiles = new List<KeyValuePair<string, byte[]>>();
            if (sAttatchmentFiles != null && sAttatchmentFiles.Length > 0)
            {
                string sAttachments = "";
                for (int x = 0; x < sAttatchmentFiles.Length; x++)
                {
                    if (convert.cStr(sAttatchmentFiles[x]).Trim() != "" && sAttachments.IndexOf(sAttatchmentFiles[x]) == -1)
                        try
                        {
                            oFiles.Add(new KeyValuePair<string, byte[]>(System.IO.Path.GetFileName(sAttatchmentFiles[x]), io.read_file_binary(sAttatchmentFiles[x])));
                            sAttachments += sAttatchmentFiles[x];
                        }
                        catch (Exception) { }
                }
            }
            return sendEmail(sFromEmail, sToEmail, sCCEmail, sBCCEmail, sSubject, sBody, bHTML, oFiles, bLogEmail, iUserID, sDSN);
        }
        public static bool sendEmail(string sFromEmail, string sToEmail, string sCCEmail, string sBCCEmail, string sSubject, string sBody, bool bHTML, List<KeyValuePair<string, byte[]>> oFiles, bool bLogEmail, int iUserID, string sDSN)
        {
            return sendEmail(sFromEmail, sToEmail, sCCEmail, sBCCEmail, sSubject, sBody, bHTML, oFiles, bLogEmail, iUserID, sDSN, null);
        }
        public static bool sendEmail(string sFromEmail, string sToEmail, string sCCEmail, string sBCCEmail, string sSubject, string sBody, bool bHTML, List<KeyValuePair<string, byte[]>> oFiles, bool bLogEmail, int iUserID, string sDSN, string sEmailKey)
        {
            return sendEmail(sFromEmail, sToEmail, ConfigurationManager.AppSettings["smtp.override.to"], sCCEmail, sBCCEmail, sSubject, sBody, bHTML, oFiles, true, bLogEmail, iUserID, sDSN, sEmailKey);
        }
        public static bool sendEmail(string sFromEmail, string sToEmail, string sOverrideToEmail, string sCCEmail, string sBCCEmail, string sSubject, string sBody, bool bHTML, List<KeyValuePair<string, byte[]>> oFiles, bool bSendEmail, bool bLogEmail, int iUserID, string sDSN, string sEmailKey)
        {
            if (convert.cBool(ConfigurationManager.AppSettings["site.demo"])) sSubject = "Demo: " + sSubject;
            sToEmail = convert.cStr(sToEmail);
            sCCEmail = convert.cStr(sCCEmail);
            sBCCEmail = convert.cStr(sBCCEmail);
            sFromEmail = convert.cStr(sFromEmail);
            if ((sToEmail.IndexOf("@") == -1 && sCCEmail.IndexOf("@") == -1 && sBCCEmail.IndexOf("@") == -1) || sFromEmail.IndexOf("@") == -1) return false;
            string sError = (bSendEmail ? "" : "bSendEmail is false -- email not attempted sent.");

            if (sOverrideToEmail.Str() != "" || convert.cStr(ConfigurationManager.AppSettings["smtp.override.from"]) != "")
                sBody = "To: " + sToEmail + (bHTML ? "<br>" : "\n") + "From: " + sFromEmail + (bHTML ? "<br>" : "\n") + (sCCEmail == "" ? "" : "CC: " + sCCEmail + (bHTML ? "<br>" : "\n")) + (sBCCEmail == "" ? "" : "BCC: " + sBCCEmail + (bHTML ? "<br>" : "\n")) + (bHTML ? "<hr><br>" : "-----------\n") + sBody;

            if (convert.cStr(ConfigurationManager.AppSettings["smtp.override.from"]) != "") sFromEmail = ConfigurationManager.AppSettings["smtp.override.from"];
            if (sOverrideToEmail.Str() != "")
            {
                sToEmail = sOverrideToEmail;
                sCCEmail = "";
                sBCCEmail = "";
            }
            if (convert.cStr(ConfigurationManager.AppSettings["smtp.add.bcc"]) != "") sBCCEmail += ";" + ConfigurationManager.AppSettings["smtp.add.bcc"];

            try
            {
                if (bSendEmail && !convert.cBool(ConfigurationManager.AppSettings["smtp.disable"])) jlib.net.SMTP.sendEmail(sFromEmail, "", sToEmail, "", sCCEmail, sBCCEmail, convert.cEllipsis(sSubject, 255, true), sBody, bHTML, oFiles, ConfigurationManager.AppSettings["smtp.server"], ConfigurationManager.AppSettings["smtp.username"], ConfigurationManager.AppSettings["smtp.password"]);
            }
            catch (Exception ex)
            {
                sError = ex.Message + "\n" + ex.StackTrace + "\n" + ex.ToString();
            }
            if (bLogEmail)
            {
                DataTable oDT = sqlbuilder.executeInsert(new ado_helper((convert.cStr(sDSN) == "" ? ConfigurationManager.AppSettings["sql.dsn.email"] : sDSN)), "d_email_log", "subject", convert.cEllipsis(sSubject, 255, false), "body", sBody, "email_from", convert.cEllipsis(sFromEmail, 255, false), "email_to", convert.cEllipsis(sToEmail, 255, false), "email_cc", convert.cEllipsis(sCCEmail, 255, false), "email_bcc", convert.cEllipsis(sBCCEmail, 255, false), "is_html", (bHTML ? 1 : 0), "error_message", convert.cEllipsis(sError, 500, false), "sent", (sError == ""), "user_id", iUserID, "email_key", sEmailKey);
                if (oFiles != null && oFiles.Count > 0 && convert.cStr(ConfigurationManager.AppSettings["email.attachment.path"]) != "")
                {
                    string sAttachments = "";
                    for (int x = 0; x < oFiles.Count; x++)
                    {
                        string sFileName = io.getUniqueFileName(ConfigurationManager.AppSettings["email.attachment.path"] + "\\" + oDT.Rows[0]["id"] + "-" + oFiles[x].Key);
                        if (io.write_file(sFileName, oFiles[x].Value)) sAttachments += (sAttachments == "" ? "" : "\n") + sFileName;
                    }
                    if (sAttachments != "")
                    {
                        oDT.Rows[0]["attachments"] = sAttachments;
                        ado_helper.update(oDT);
                    }
                }
            }
            return sError == "";
        }
        public static bool sendXslEmail(XmlDocument xmlDoc, string sEmailName, string sEmailKey)
        {
            return sendXslEmail(xmlDoc, sEmailName, sEmailKey, true);
        }
        public static bool sendXslEmail(XmlDocument xmlDoc, string sEmailName, string sEmailKey, bool bSendEmail)
        {
            return sendXslEmail(xmlDoc, sEmailName, sEmailKey, xml.getXmlNodeValue(xmlDoc, "queries/query/user/EMAIL"), bSendEmail, xml.getXmlNodeValue(xmlDoc, "queries/query/user/LANGUAGE"));
        }
        public static bool sendXslEmail(XmlDocument xmlDoc, string sEmailName, string sEmailKey, string sToEmails, bool bSendEmail, string sLanguage)
        {
            string sEmail = jlib.functions.xml.xslTransform(xmlDoc, sEmailName + ".xsl", sLanguage, false, true);
            return SendTemplateEmail(sEmail, sEmailKey, sToEmails, bSendEmail, sLanguage);
        }
        private static bool SendTemplateEmail(string sEmail, string sEmailKey, string sToEmails, bool bSendEmail, string sLanguage)
        {
            return SendTemplateEmail(sEmail, sEmailKey, sToEmails, bSendEmail, sLanguage, null);
        }
        private static bool SendTemplateEmail(string sEmail, string sEmailKey, string sToEmails, bool bSendEmail, string sLanguage, List<KeyValuePair<string, byte[]>> files)
        {
            return SendTemplateEmail(sEmail, sEmailKey, sToEmails, bSendEmail, sLanguage, "", "", "", files);
        }

        private static bool SendTemplateEmail(string sEmail, string sEmailKey, string sToEmails, bool bSendEmail, string sLanguage, string sSubject, string sFromEmail, string sBCCEmail, List<KeyValuePair<string, byte[]>> files)
        {

            sEmail = parse.stripWhiteChars(sEmail);
            if (files == null)
                files = new List<KeyValuePair<string, byte[]>>();

            string[] sTokens = new string[] { "From-email:", "Subject:", "To-email:", "Attach:", "BCC-email:" };
            for (int x = 0; x < sTokens.Length; x++)
            {
                if (sEmail.StartsWith(sTokens[x]))
                {
                    if (sTokens[x] == "From-email:" && sFromEmail.IsNullOrEmpty()) sFromEmail = parse.inner_substring(sEmail, sTokens[x], null, parse.findSeparator(sEmail, "\n", "<"), null).Trim();
                    else if (sTokens[x] == "Subject:" && sSubject.IsNullOrEmpty()) sSubject = parse.replaceAll(parse.inner_substring(sEmail, sTokens[x], null, parse.findSeparator(sEmail, "\n", "<"), null).Trim(), "&amp;", "&");
                    else if (sTokens[x] == "To-email:" && sToEmails.IsNullOrEmpty()) sToEmails = parse.inner_substring(sEmail, sTokens[x], null, parse.findSeparator(sEmail, "\n", "<"), null).Trim();
                    else if (sTokens[x] == "BCC-email:" && sBCCEmail.IsNullOrEmpty()) sBCCEmail = parse.inner_substring(sEmail, sTokens[x], null, parse.findSeparator(sEmail, "\n", "<"), null).Trim();
                    else if (sTokens[x] == "Attach:")
                    {
                        string[] sAttachments = parse.split(parse.inner_substring(sEmail, sTokens[x], null, parse.findSeparator(sEmail, "\n", "<"), null).Trim(), "|");
                        for (int y = 0; y < sAttachments.Length; y++)
                        {
                            if (sAttachments[y].Trim() != "")
                            {
                                byte[] bBytes = io.read_file_binary((sAttachments[y].Trim().StartsWith("/") ? ConfigurationManager.AppSettings["repairengine.path"] : "") + sAttachments[y].Trim());
                                if (bBytes.Length > 0) files.Add(new KeyValuePair<string, byte[]>(System.IO.Path.GetFileName(sAttachments[y]), bBytes));
                            }
                        }
                    }
                    sEmail = parse.stripWhiteChars(sEmail.Substring(sEmail.IndexOf(parse.findSeparator(sEmail, "\n", "<"))));
                    x = -1;
                }
            }

            return sendEmail(sFromEmail, sToEmails, ConfigurationManager.AppSettings["smtp.override.to"].Str(), "", sBCCEmail, convert.cEllipsis(sSubject, 255), sEmail, true, files, bSendEmail, true, 0, null, sEmailKey);
        }
        public static string RenderRazorTemplate<T>(T Parameters, string sTemplateName, string sLanguage)
        {
            return RenderRazorTemplate<T>(Parameters, sTemplateName, sLanguage, true);
        }
        public static string RenderRazorTemplate<T>(T Parameters, string sTemplateName, string sLanguage, bool bStripTokens)
        {
            string sEmail = parse.stripWhiteChars(jlib.helpers.Razor.RenderRazorTemplate("/inc/xsl/" + sTemplateName + ".cshtml", Parameters));
            if (bStripTokens)
            {
                string[] sTokens = new string[] { "From-email:", "Subject:", "To-email:", "Attach:", "Pagesize:", "BCC-email:" };
                for (int x = 0; x < sTokens.Length; x++)
                {
                    if (sEmail.StartsWith(sTokens[x]))
                    {
                        string sSep = parse.findSeparator(sEmail, "\n", "<");
                        if (sSep == "<" && sEmail.IndexOf("<tran") == sEmail.IndexOf("<")) sSep = "\n";
                        sEmail = parse.stripWhiteChars(sEmail.Substring(sEmail.IndexOf(sSep)));
                        x = -1;
                    }
                }
            }
            if (!sLanguage.IsNullOrEmpty()) sEmail = jlib.helpers.translation.translateHTML(sEmail, "translate", sLanguage, sTemplateName + ".cshtml");
            return sEmail;
        }
        public static bool SendRazorEmail<T>(T Parameters, string sEmailName, string sEmailKey, string sToEmails, bool bSendEmail, string sLanguage)
        {
            return SendRazorEmail(Parameters, sEmailName, sEmailKey, sToEmails, bSendEmail, sLanguage, null);
        }
        public static bool SendRazorEmail<T>(T Parameters, string sEmailName, string sEmailKey, string sToEmails, bool bSendEmail, string sLanguage, List<KeyValuePair<string, byte[]>> files)
        {
            return SendRazorEmail(Parameters, sEmailName, sEmailKey, sToEmails, bSendEmail, sLanguage, "", "", "", files);
        }
        public static bool SendRazorEmail<T>(T Parameters, string sEmailName, string sEmailKey, string sToEmails, bool bSendEmail, string sLanguage, string sSubject, string sFromEmail, string sBCCEmail, List<KeyValuePair<string, byte[]>> files)
        {
            string sEmail = jlib.helpers.Razor.RenderRazorTemplate("/inc/xsl/" + sEmailName + ".cshtml", Parameters);
            if (!sLanguage.IsNullOrEmpty()) sEmail = jlib.helpers.translation.translateHTML(sEmail, "translate", sLanguage, sEmailName + ".cshtml");
            return SendTemplateEmail(sEmail, sEmailKey, sToEmails, bSendEmail, sLanguage, sSubject, sFromEmail, sBCCEmail, files);
        }
    }


}