// page init
function AngularViewInit(){
  initCustomForms();
  initCycleCarousel();  
}

function AngularAfterViewInit(){
  initMaps();
}
/*

jQuery(window).on('load', function(){
  initMaps();
});
*/
var isTouchDevice = /Windows Phone/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

function initMaps() {  
  var mapLoader = setInterval(function(){
    if(typeof google === 'undefined')return;
    clearInterval(mapLoader);    
    jQuery('.map-area.hasmap').createGeoMarker({
      wrapMap: '.map-wrap',
      activeClass: 'active',
      overlay: '.overlay',
      jsonAttr: 'rel',
      mapOptions: {
        zoom: 14,
        gestureHandling: 'cooperative',
        scrollwheel:true,
        panControl: true,
        zoomControl: true,
        scaleControl: true,
        mapTypeControl: true,
        streetViewControl: true,
        draggable: isTouchDevice ? false : true
      }
    });
  },100);
}

// initialize custom form elements
function initCustomForms() {  
  jcf.setOptions('Select', {
    wrapNative: false,
    wrapNativeOnMobile: false,
    useCustomScroll: false
  });
  jcf.replaceAll();  
}

// cycle scroll gallery init
function initCycleCarousel() {
  jQuery('.cycle-gallery').scrollAbsoluteGallery({
    mask: '.mask',
    slider: '.slideset',
    slides: '.slide',
    btnPrev: '.btn-prev',
    btnNext: '.btn-next',
    generatePagination: '.indicators',
    stretchSlideToMask: true,
    pauseOnHover: true,
    maskAutoSize: true,
    autoRotation: true,
    switchTime: 3000,
    animSpeed: 500
  });
}

/*
 * jQuery CreateGeoMarker plugin
 */
;(function($) {
  function CreateGeoMarker(options) {
    this.options = $.extend({
      holderMap: null,
      wrapMap: '.map-wrap',
      activeClass: 'active',
      overlay: '.overlay',
      address: '.address',
      jsonAttr: 'data-style',
      markerSrc: 'assets/portal/images/pin.png',
      mapOptions: {
        zoom: 13,
        gestureHandling: 'cooperative',
        scrollwheel: true,
        panControl: false,
        zoomControl: false,
        scaleControl: false,
        mapTypeControl: false,
        streetViewControl: false,
        draggable: true
      }
    }, options);

    this.init();
  }
  CreateGeoMarker.prototype = {
    init: function() {
      this.findElements();
      this.makeCallback('onInit', this);
      this.createMap();
      this.attachEvents();
    },
    findElements: function() {
      this.holder = jQuery(this.options.holder);
      this.wrap = this.holder.find(this.options.wrapMap);
      this.address = this.holder.find(this.options.address);
      this.overlay = this.wrap.find(this.options.overlay);
      this.win = jQuery(window);
    },
    createMap: function() {
      var self = this;
      this.map = new google.maps.Map(self.wrap[0], self.options.mapOptions);
      this.geocoder = new google.maps.Geocoder();
      var url = this.holder.attr(this.options.jsonAttr);

      if (url !== undefined) {
        jQuery.ajax({
          type:'get',
          url: url,
          cache: false,
          dataType: 'text',
          success:function(response) {
            var data = jQuery.parseJSON(response);
            self.createMarkerIcon();
            self.map.setOptions({
              styles: data.styles
            });
          },
          error:function() {
            // alert('AJAX Error!');
          }
        });
      }
    },
    createMarkerIcon: function() {
      var self = this;

      self.geocoder.geocode({'address': this.address.text() }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK){
          self.coord = results[0].geometry.location;
          self.map.setCenter(self.coord);

          var marker = new google.maps.Marker({
            position: self.coord,
            map: self.map,
            icon: self.options.markerSrc
          });

          setTimeout(function() {
            self.overlay.fadeOut();
          }, 1000);
        } else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
          /*
          setTimeout(function() {
            self.createMarkerIcon();
          }, 200);
          */
        } else {
          // alert('Geocode was not successful for the following reason: ' + status);
        }
      });
    },
    attachEvents: function() {
      var self = this;

      // flexible layout handler
      this.resizeHandler = function() {
        self.map.setCenter(self.coord);
      };

      this.resizeHandler();
      this.win.on('resize orientationchange', this.resizeHandler);
    },
    makeCallback: function(name) {
      if (typeof this.options[name] === 'function') {
        var args = Array.prototype.slice.call(arguments);
        args.shift();
        this.options[name].apply(this, args);
      }
    }
  };

  var isTouchDevice = /Windows Phone/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

  $.fn.createGeoMarker = function(options) {
    return this.each(function() {
      $(this).data('CreateGeoMarker', new CreateGeoMarker($.extend(options, { holder:this })));
    });
  };
})(jQuery);

/*
 * jQuery Cycle Carousel plugin
 */
;(function($){
  function ScrollAbsoluteGallery(options) {
    this.options = $.extend({
      activeClass: 'active',
      mask: 'div.slides-mask',
      slider: '>ul',
      slides: '>li',
      btnPrev: '.btn-prev',
      btnNext: '.btn-next',
      pagerLinks: 'ul.pager > li',
      generatePagination: false,
      pagerList: '<ul>',
      pagerListItem: '<li><a href="#"></a></li>',
      pagerListItemText: 'a',
      galleryReadyClass: 'gallery-js-ready',
      currentNumber: 'span.current-num',
      totalNumber: 'span.total-num',
      maskAutoSize: false,
      autoRotation: false,
      pauseOnHover: false,
      stretchSlideToMask: false,
      switchTime: 3000,
      animSpeed: 500,
      handleTouch: true,
      swipeThreshold: 15,
      vertical: false
    }, options);
    this.init();
  }
  ScrollAbsoluteGallery.prototype = {
    init: function() {
      if(this.options.holder) {
        this.findElements();
        this.attachEvents();
        this.makeCallback('onInit', this);
      }
    },
    findElements: function() {
      // find structure elements
      this.holder = $(this.options.holder).addClass(this.options.galleryReadyClass);
      this.mask = this.holder.find(this.options.mask);
      this.slider = this.mask.find(this.options.slider);
      this.slides = this.slider.find(this.options.slides);
      this.btnPrev = this.holder.find(this.options.btnPrev);
      this.btnNext = this.holder.find(this.options.btnNext);

      // slide count display
      this.currentNumber = this.holder.find(this.options.currentNumber);
      this.totalNumber = this.holder.find(this.options.totalNumber);

      // create gallery pagination
      if(typeof this.options.generatePagination === 'string') {
        this.pagerLinks = this.buildPagination();
      } else {
        this.pagerLinks = this.holder.find(this.options.pagerLinks);
      }

      // define index variables
      this.sizeProperty = this.options.vertical ? 'height' : 'width';
      this.positionProperty = this.options.vertical ? 'top' : 'left';
      this.animProperty = this.options.vertical ? 'marginTop' : 'marginLeft';

      this.slideSize = this.slides[this.sizeProperty]();
      this.currentIndex = 0;
      this.prevIndex = 0;

      // reposition elements
      this.options.maskAutoSize = this.options.vertical ? false : this.options.maskAutoSize;
      if(this.options.vertical) {
        this.mask.css({
          height: this.slides.innerHeight()
        });
      }
      if(this.options.maskAutoSize){
        this.mask.css({
          height: this.slider.height()
        });
      }
      this.slider.css({
        position: 'relative',
        height: this.options.vertical ? this.slideSize * this.slides.length : '100%'
      });
      this.slides.css({
        position: 'absolute'
      }).css(this.positionProperty, -9999).eq(this.currentIndex).css(this.positionProperty, 0);
      this.refreshState();
    },
    buildPagination: function() {
      var pagerLinks = $();
      if(!this.pagerHolder) {
        this.pagerHolder = this.holder.find(this.options.generatePagination);
      }
      if(this.pagerHolder.length) {
        this.pagerHolder.empty();
        this.pagerList = $(this.options.pagerList).appendTo(this.pagerHolder);
        for(var i = 0; i < this.slides.length; i++) {
          $(this.options.pagerListItem).appendTo(this.pagerList).find(this.options.pagerListItemText).text(i+1);
        }
        pagerLinks = this.pagerList.children();
      }
      return pagerLinks;
    },
    attachEvents: function() {
      // attach handlers
      var self = this;
      if(this.btnPrev.length) {
        this.btnPrevHandler = function(e) {
          e.preventDefault();
          self.prevSlide();
        };
        this.btnPrev.click(this.btnPrevHandler);
      }
      if(this.btnNext.length) {
        this.btnNextHandler = function(e) {
          e.preventDefault();
          self.nextSlide();
        };
        this.btnNext.click(this.btnNextHandler);
      }
      if(this.pagerLinks.length) {
        this.pagerLinksHandler = function(e) {
          e.preventDefault();
          self.numSlide(self.pagerLinks.index(e.currentTarget));
        };
        this.pagerLinks.click(this.pagerLinksHandler);
      }

      // handle autorotation pause on hover
      if(this.options.pauseOnHover) {
        this.hoverHandler = function() {
          clearTimeout(self.timer);
        };
        this.leaveHandler = function() {
          self.autoRotate();
        };
        this.holder.bind({mouseenter: this.hoverHandler, mouseleave: this.leaveHandler});
      }

      // handle holder and slides dimensions
      this.resizeHandler = function() {
        if(!self.animating) {
          if(self.options.stretchSlideToMask) {
            self.resizeSlides();
          }
          self.resizeHolder();
          self.setSlidesPosition(self.currentIndex);
        }
      };
      $(window).bind('load resize orientationchange', this.resizeHandler);
      if(self.options.stretchSlideToMask) {
        self.resizeSlides();
      }

      // handle swipe on mobile devices
      if(this.options.handleTouch && window.Hammer && this.mask.length && this.slides.length > 1 && isTouchDevice) {
        this.swipeHandler = new Hammer.Manager(this.mask[0]);
        this.swipeHandler.add(new Hammer.Pan({
          direction: self.options.vertical ? Hammer.DIRECTION_VERTICAL : Hammer.DIRECTION_HORIZONTAL,
          threshold: self.options.swipeThreshold
        }));

        this.swipeHandler.on('panstart', function() {
          if(self.animating) {
            self.swipeHandler.stop();
          } else {
            clearTimeout(self.timer);
          }
        }).on('panmove', function(e) {
          self.swipeOffset = -self.slideSize + e[self.options.vertical ? 'deltaY' : 'deltaX'];
          self.slider.css(self.animProperty, self.swipeOffset);
          clearTimeout(self.timer);
        }).on('panend', function(e) {
          if(e.distance > self.options.swipeThreshold) {
            if(e.offsetDirection === Hammer.DIRECTION_RIGHT || e.offsetDirection === Hammer.DIRECTION_DOWN) {
              self.nextSlide();
            } else {
              self.prevSlide();
            }
          } else {
            var tmpObj = {};
            tmpObj[self.animProperty] = -self.slideSize;
            self.slider.animate(tmpObj, {duration: self.options.animSpeed});
            self.autoRotate();
          }
          self.swipeOffset = 0;
        });
      }

      // start autorotation
      this.autoRotate();
      this.resizeHolder();
      this.setSlidesPosition(this.currentIndex);
    },
    resizeSlides: function() {
      this.slideSize = this.mask[this.options.vertical ? 'height' : 'width']();
      this.slides.css(this.sizeProperty, this.slideSize);
    },
    resizeHolder: function() {
      if(this.options.maskAutoSize) {
        this.mask.css({
          height: this.slides.eq(this.currentIndex).outerHeight(true)
        });
      }
    },
    prevSlide: function() {
      if(!this.animating && this.slides.length > 1) {
        this.direction = -1;
        this.prevIndex = this.currentIndex;
        if(this.currentIndex > 0) this.currentIndex--;
        else this.currentIndex = this.slides.length - 1;
        this.switchSlide();
      }
    },
    nextSlide: function(fromAutoRotation) {
      if(!this.animating && this.slides.length > 1) {
        this.direction = 1;
        this.prevIndex = this.currentIndex;
        if(this.currentIndex < this.slides.length - 1) this.currentIndex++;
        else this.currentIndex = 0;
        this.switchSlide();
      }
    },
    numSlide: function(c) {
      if(!this.animating && this.currentIndex !== c && this.slides.length > 1) {
        this.direction = c > this.currentIndex ? 1 : -1;
        this.prevIndex = this.currentIndex;
        this.currentIndex = c;
        this.switchSlide();
      }
    },
    preparePosition: function() {
      // prepare slides position before animation
      this.setSlidesPosition(this.prevIndex, this.direction < 0 ? this.currentIndex : null, this.direction > 0 ? this.currentIndex : null, this.direction);
    },
    setSlidesPosition: function(index, slideLeft, slideRight, direction) {
      // reposition holder and nearest slides
      if(this.slides.length > 1) {
        var prevIndex = (typeof slideLeft === 'number' ? slideLeft : index > 0 ? index - 1 : this.slides.length - 1);
        var nextIndex = (typeof slideRight === 'number' ? slideRight : index < this.slides.length - 1 ? index + 1 : 0);

        this.slider.css(this.animProperty, this.swipeOffset ? this.swipeOffset : -this.slideSize);
        this.slides.css(this.positionProperty, -9999).eq(index).css(this.positionProperty, this.slideSize);
        if(prevIndex === nextIndex && typeof direction === 'number') {
          var calcOffset = direction > 0 ? this.slideSize*2 : 0;
          this.slides.eq(nextIndex).css(this.positionProperty, calcOffset);
        } else {
          this.slides.eq(prevIndex).css(this.positionProperty, 0);
          this.slides.eq(nextIndex).css(this.positionProperty, this.slideSize*2);
        }
      }
    },
    switchSlide: function() {
      // prepare positions and calculate offset
      var self = this;
      var oldSlide = this.slides.eq(this.prevIndex);
      var newSlide = this.slides.eq(this.currentIndex);
      this.animating = true;

      // resize mask to fit slide
      if(this.options.maskAutoSize) {
        this.mask.animate({
          height: newSlide.outerHeight(true)
        }, {
          duration: this.options.animSpeed
        });
      }

      // start animation
      var animProps = {};
      animProps[this.animProperty] = this.direction > 0 ? -this.slideSize*2 : 0;
      this.preparePosition();
      this.slider.animate(animProps,{duration:this.options.animSpeed, complete:function() {
        self.setSlidesPosition(self.currentIndex);

        // start autorotation
        self.animating = false;
        self.autoRotate();

        // onchange callback
        self.makeCallback('onChange', self);
      }});

      // refresh classes
      this.refreshState();

      // onchange callback
      this.makeCallback('onBeforeChange', this);
    },
    refreshState: function(initial) {
      // slide change function
      this.slides.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);
      this.pagerLinks.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);

      // display current slide number
      this.currentNumber.html(this.currentIndex + 1);
      this.totalNumber.html(this.slides.length);

      // add class if not enough slides
      this.holder.toggleClass('not-enough-slides', this.slides.length === 1);
    },
    autoRotate: function() {
      var self = this;
      clearTimeout(this.timer);
      if(this.options.autoRotation) {
        this.timer = setTimeout(function() {
          self.nextSlide();
        }, this.options.switchTime);
      }
    },
    makeCallback: function(name) {
      if(typeof this.options[name] === 'function') {
        var args = Array.prototype.slice.call(arguments);
        args.shift();
        this.options[name].apply(this, args);
      }
    },
    destroy: function() {
      // destroy handler
      this.btnPrev.unbind('click', this.btnPrevHandler);
      this.btnNext.unbind('click', this.btnNextHandler);
      this.pagerLinks.unbind('click', this.pagerLinksHandler);
      this.holder.unbind('mouseenter', this.hoverHandler);
      this.holder.unbind('mouseleave', this.leaveHandler);
      $(window).unbind('load resize orientationchange', this.resizeHandler);
      clearTimeout(this.timer);

      // destroy swipe handler
      if(this.swipeHandler) {
        this.swipeHandler.destroy();
      }

      // remove inline styles, classes and pagination
      this.holder.removeClass(this.options.galleryReadyClass);
      this.slider.add(this.slides).removeAttr('style');
      if(typeof this.options.generatePagination === 'string') {
        this.pagerHolder.empty();
      }
    }
  };

  // detect device type
  var isTouchDevice = /Windows Phone/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

  // jquery plugin
  $.fn.scrollAbsoluteGallery = function(opt){
    return this.each(function(){
      $(this).data('ScrollAbsoluteGallery', new ScrollAbsoluteGallery($.extend(opt,{holder:this})));
    });
  };
}(jQuery));

/*!
 * JavaScript Custom Forms
 *
 * Copyright 2014-2015 PSD2HTML - http://psd2html.com/jcf
 * Released under the MIT license (LICENSE.txt)
 *
 * Version: 1.2.3
 */
;(function(root, factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(require('jquery'));
  } else {
    root.jcf = factory(jQuery);
  }
}(this, function($) {
  'use strict';

  // define version
  var version = '1.2.3';

  // private variables
  var customInstances = [];

  // default global options
  var commonOptions = {
    optionsKey: 'jcf',
    dataKey: 'jcf-instance',
    rtlClass: 'jcf-rtl',
    focusClass: 'jcf-focus',
    pressedClass: 'jcf-pressed',
    disabledClass: 'jcf-disabled',
    hiddenClass: 'jcf-hidden',
    resetAppearanceClass: 'jcf-reset-appearance',
    unselectableClass: 'jcf-unselectable'
  };

  // detect device type
  var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch,
    isWinPhoneDevice = /Windows Phone/.test(navigator.userAgent);
  commonOptions.isMobileDevice = !!(isTouchDevice || isWinPhoneDevice);

  // create global stylesheet if custom forms are used
  var createStyleSheet = function() {
    var styleTag = $('<style>').appendTo('head'),
      styleSheet = styleTag.prop('sheet') || styleTag.prop('styleSheet');

    // crossbrowser style handling
    var addCSSRule = function(selector, rules, index) {
      index = index || 0;
      if (styleSheet.insertRule) {
        styleSheet.insertRule(selector + '{' + rules + '}', index);
      } else {
        styleSheet.addRule(selector, rules, index);
      }
    };

    // add special rules
    addCSSRule('.' + commonOptions.hiddenClass, 'position:absolute !important;left:-9999px !important;height:1px !important;width:1px !important;margin:0 !important;border-width:0 !important;-webkit-appearance:none;-moz-appearance:none;appearance:none');
    addCSSRule('.' + commonOptions.rtlClass + ' .' + commonOptions.hiddenClass, 'right:-9999px !important; left: auto !important');
    addCSSRule('.' + commonOptions.unselectableClass, '-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; -webkit-tap-highlight-color: rgba(0,0,0,0);');
    addCSSRule('.' + commonOptions.resetAppearanceClass, 'background: none; border: none; -webkit-appearance: none; appearance: none; opacity: 0; filter: alpha(opacity=0);');

    // detect rtl pages
    var html = $('html'), body = $('body');
    if (html.css('direction') === 'rtl' || body.css('direction') === 'rtl') {
      html.addClass(commonOptions.rtlClass);
    }

    // handle form reset event
    html.on('reset', function() {
      setTimeout(function() {
        api.refreshAll();
      }, 0);
    });

    // mark stylesheet as created
    commonOptions.styleSheetCreated = true;
  };

  // simplified pointer events handler
  (function() {
    var pointerEventsSupported = navigator.pointerEnabled || navigator.msPointerEnabled,
      touchEventsSupported = ('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch,
      eventList, eventMap = {}, eventPrefix = 'jcf-';

    // detect events to attach
    if (pointerEventsSupported) {
      eventList = {
        pointerover: navigator.pointerEnabled ? 'pointerover' : 'MSPointerOver',
        pointerdown: navigator.pointerEnabled ? 'pointerdown' : 'MSPointerDown',
        pointermove: navigator.pointerEnabled ? 'pointermove' : 'MSPointerMove',
        pointerup: navigator.pointerEnabled ? 'pointerup' : 'MSPointerUp'
      };
    } else {
      eventList = {
        pointerover: 'mouseover',
        pointerdown: 'mousedown' + (touchEventsSupported ? ' touchstart' : ''),
        pointermove: 'mousemove' + (touchEventsSupported ? ' touchmove' : ''),
        pointerup: 'mouseup' + (touchEventsSupported ? ' touchend' : '')
      };
    }

    // create event map
    $.each(eventList, function(targetEventName, fakeEventList) {
      $.each(fakeEventList.split(' '), function(index, fakeEventName) {
        eventMap[fakeEventName] = targetEventName;
      });
    });

    // jQuery event hooks
    $.each(eventList, function(eventName, eventHandlers) {
      eventHandlers = eventHandlers.split(' ');
      $.event.special[eventPrefix + eventName] = {
        setup: function() {
          var self = this;
          $.each(eventHandlers, function(index, fallbackEvent) {
            if (self.addEventListener) self.addEventListener(fallbackEvent, fixEvent, false);
            else self['on' + fallbackEvent] = fixEvent;
          });
        },
        teardown: function() {
          var self = this;
          $.each(eventHandlers, function(index, fallbackEvent) {
            if (self.addEventListener) self.removeEventListener(fallbackEvent, fixEvent, false);
            else self['on' + fallbackEvent] = null;
          });
        }
      };
    });

    // check that mouse event are not simulated by mobile browsers
    var lastTouch = null;
    var mouseEventSimulated = function(e) {
      var dx = Math.abs(e.pageX - lastTouch.x),
        dy = Math.abs(e.pageY - lastTouch.y),
        rangeDistance = 25;

      if (dx <= rangeDistance && dy <= rangeDistance) {
        return true;
      }
    };

    // normalize event
    var fixEvent = function(e) {
      var origEvent = e || window.event,
        touchEventData = null,
        targetEventName = eventMap[origEvent.type];

      e = $.event.fix(origEvent);
      e.type = eventPrefix + targetEventName;

      if (origEvent.pointerType) {
        switch (origEvent.pointerType) {
          case 2: e.pointerType = 'touch'; break;
          case 3: e.pointerType = 'pen'; break;
          case 4: e.pointerType = 'mouse'; break;
          default: e.pointerType = origEvent.pointerType;
        }
      } else {
        e.pointerType = origEvent.type.substr(0, 5); // "mouse" or "touch" word length
      }

      if (!e.pageX && !e.pageY) {
        touchEventData = origEvent.changedTouches ? origEvent.changedTouches[0] : origEvent;
        e.pageX = touchEventData.pageX;
        e.pageY = touchEventData.pageY;
      }

      if (origEvent.type === 'touchend') {
        lastTouch = { x: e.pageX, y: e.pageY };
      }
      if (e.pointerType === 'mouse' && lastTouch && mouseEventSimulated(e)) {
        return;
      } else {
        return ($.event.dispatch || $.event.handle).call(this, e);
      }
    };
  }());

  // custom mousewheel/trackpad handler
  (function() {
    var wheelEvents = ('onwheel' in document || document.documentMode >= 9 ? 'wheel' : 'mousewheel DOMMouseScroll').split(' '),
      shimEventName = 'jcf-mousewheel';

    $.event.special[shimEventName] = {
      setup: function() {
        var self = this;
        $.each(wheelEvents, function(index, fallbackEvent) {
          if (self.addEventListener) self.addEventListener(fallbackEvent, fixEvent, false);
          else self['on' + fallbackEvent] = fixEvent;
        });
      },
      teardown: function() {
        var self = this;
        $.each(wheelEvents, function(index, fallbackEvent) {
          if (self.addEventListener) self.removeEventListener(fallbackEvent, fixEvent, false);
          else self['on' + fallbackEvent] = null;
        });
      }
    };

    var fixEvent = function(e) {
      var origEvent = e || window.event;
      e = $.event.fix(origEvent);
      e.type = shimEventName;

      // old wheel events handler
      if ('detail'      in origEvent) { e.deltaY = -origEvent.detail;      }
      if ('wheelDelta'  in origEvent) { e.deltaY = -origEvent.wheelDelta;  }
      if ('wheelDeltaY' in origEvent) { e.deltaY = -origEvent.wheelDeltaY; }
      if ('wheelDeltaX' in origEvent) { e.deltaX = -origEvent.wheelDeltaX; }

      // modern wheel event handler
      if ('deltaY' in origEvent) {
        e.deltaY = origEvent.deltaY;
      }
      if ('deltaX' in origEvent) {
        e.deltaX = origEvent.deltaX;
      }

      // handle deltaMode for mouse wheel
      e.delta = e.deltaY || e.deltaX;
      if (origEvent.deltaMode === 1) {
        var lineHeight = 16;
        e.delta *= lineHeight;
        e.deltaY *= lineHeight;
        e.deltaX *= lineHeight;
      }

      return ($.event.dispatch || $.event.handle).call(this, e);
    };
  }());

  // extra module methods
  var moduleMixin = {
    // provide function for firing native events
    fireNativeEvent: function(elements, eventName) {
      $(elements).each(function() {
        var element = this, eventObject;
        if (element.dispatchEvent) {
          eventObject = document.createEvent('HTMLEvents');
          eventObject.initEvent(eventName, true, true);
          element.dispatchEvent(eventObject);
        } else if (document.createEventObject) {
          eventObject = document.createEventObject();
          eventObject.target = element;
          element.fireEvent('on' + eventName, eventObject);
        }
      });
    },
    // bind event handlers for module instance (functions beggining with "on")
    bindHandlers: function() {
      var self = this;
      $.each(self, function(propName, propValue) {
        if (propName.indexOf('on') === 0 && $.isFunction(propValue)) {
          // dont use $.proxy here because it doesn't create unique handler
          self[propName] = function() {
            return propValue.apply(self, arguments);
          };
        }
      });
    }
  };

  // public API
  var api = {
    version: version,
    modules: {},
    getOptions: function() {
      return $.extend({}, commonOptions);
    },
    setOptions: function(moduleName, moduleOptions) {
      if (arguments.length > 1) {
        // set module options
        if (this.modules[moduleName]) {
          $.extend(this.modules[moduleName].prototype.options, moduleOptions);
        }
      } else {
        // set common options
        $.extend(commonOptions, moduleName);
      }
    },
    addModule: function(proto) {
      // proto is factory function
      if ($.isFunction(proto)) {
        proto = proto($, window);
      }

      // add module to list
      var Module = function(options) {
        // save instance to collection
        if (!options.element.data(commonOptions.dataKey)) {
          options.element.data(commonOptions.dataKey, this);
        }
        customInstances.push(this);

        // save options
        this.options = $.extend({}, commonOptions, this.options, getInlineOptions(options.element), options);

        // bind event handlers to instance
        this.bindHandlers();

        // call constructor
        this.init.apply(this, arguments);
      };

      // parse options from HTML attribute
      var getInlineOptions = function(element) {
        var dataOptions = element.data(commonOptions.optionsKey),
          attrOptions = element.attr(commonOptions.optionsKey);

        if (dataOptions) {
          return dataOptions;
        } else if (attrOptions) {
          try {
            return $.parseJSON(attrOptions);
          } catch (e) {
            // ignore invalid attributes
          }
        }
      };

      // set proto as prototype for new module
      Module.prototype = proto;

      // add mixin methods to module proto
      $.extend(proto, moduleMixin);
      if (proto.plugins) {
        $.each(proto.plugins, function(pluginName, plugin) {
          $.extend(plugin.prototype, moduleMixin);
        });
      }

      // override destroy method
      var originalDestroy = Module.prototype.destroy;
      Module.prototype.destroy = function() {
        this.options.element.removeData(this.options.dataKey);

        for (var i = customInstances.length - 1; i >= 0; i--) {
          if (customInstances[i] === this) {
            customInstances.splice(i, 1);
            break;
          }
        }

        if (originalDestroy) {
          originalDestroy.apply(this, arguments);
        }
      };

      // save module to list
      this.modules[proto.name] = Module;
    },
    getInstance: function(element) {
      return $(element).data(commonOptions.dataKey);
    },
    replace: function(elements, moduleName, customOptions) {
      var self = this,
        instance;

      if (!commonOptions.styleSheetCreated) {
        createStyleSheet();
      }

      $(elements).each(function() {
        var moduleOptions,
          element = $(this);

        instance = element.data(commonOptions.dataKey);
        if (instance) {
          instance.refresh();
        } else {
          if (!moduleName) {
            $.each(self.modules, function(currentModuleName, module) {
              if (module.prototype.matchElement.call(module.prototype, element)) {
                moduleName = currentModuleName;
                return false;
              }
            });
          }
          if (moduleName) {
            moduleOptions = $.extend({ element: element }, customOptions);
            instance = new self.modules[moduleName](moduleOptions);
          }
        }
      });
      return instance;
    },
    refresh: function(elements) {
      $(elements).each(function() {
        var instance = $(this).data(commonOptions.dataKey);
        if (instance) {
          instance.refresh();
        }
      });
    },
    destroy: function(elements) {
      $(elements).each(function() {
        var instance = $(this).data(commonOptions.dataKey);
        if (instance) {
          instance.destroy();
        }
      });
    },
    replaceAll: function(context) {
      var self = this;
      $.each(this.modules, function(moduleName, module) {
        $(module.prototype.selector, context).each(function() {
          if (this.className.indexOf('jcf-ignore') < 0) {
            self.replace(this, moduleName);
          }
        });
      });
    },
    refreshAll: function(context) {
      if (context) {
        $.each(this.modules, function(moduleName, module) {
          $(module.prototype.selector, context).each(function() {
            var instance = $(this).data(commonOptions.dataKey);
            if (instance) {
              instance.refresh();
            }
          });
        });
      } else {
        for (var i = customInstances.length - 1; i >= 0; i--) {
          customInstances[i].refresh();
        }
      }
    },
    destroyAll: function(context) {
      if (context) {
        $.each(this.modules, function(moduleName, module) {
          $(module.prototype.selector, context).each(function(index, element) {
            var instance = $(element).data(commonOptions.dataKey);
            if (instance) {
              instance.destroy();
            }
          });
        });
      } else {
        while (customInstances.length) {
          customInstances[0].destroy();
        }
      }
    }
  };

  // we need to make JCF available globally if we're in AMD environment
  if (typeof define === 'function' && define.amd) {
    window.jcf = api;
  }

  return api;
}));


/*!
 * JavaScript Custom Forms : Select Module
 *
 * Copyright 2014-2015 PSD2HTML - http://psd2html.com/jcf
 * Released under the MIT license (LICENSE.txt)
 *
 * Version: 1.2.3
 */

(function(jcf) {

jcf.addModule(function($, window) {
  'use strict';

  var module = {
    name: 'Select',
    selector: 'select',
    options: {
      element: null,
      multipleCompactStyle: false
    },
    plugins: {
      ListBox: ListBox,
      ComboBox: ComboBox,
      SelectList: SelectList
    },
    matchElement: function(element) {
      return element.is('select');
    },
    init: function() {
      this.element = $(this.options.element);
      this.createInstance();
    },
    isListBox: function() {
      return this.element.is('[size]:not([jcf-size]), [multiple]');
    },
    createInstance: function() {
      if (this.instance) {
        this.instance.destroy();
      }
      if (this.isListBox() && !this.options.multipleCompactStyle) {
        this.instance = new ListBox(this.options);
      } else {
        this.instance = new ComboBox(this.options);
      }
    },
    refresh: function() {
      var typeMismatch = (this.isListBox() && this.instance instanceof ComboBox) ||
                (!this.isListBox() && this.instance instanceof ListBox);

      if (typeMismatch) {
        this.createInstance();
      } else {
        this.instance.refresh();
      }
    },
    destroy: function() {
      this.instance.destroy();
    }
  };

  // combobox module
  function ComboBox(options) {
    this.options = $.extend({
      wrapNative: true,
      wrapNativeOnMobile: true,
      fakeDropInBody: true,
      useCustomScroll: true,
      flipDropToFit: true,
      maxVisibleItems: 10,
      fakeAreaStructure: '<span class="jcf-select"><span class="jcf-select-text"></span><span class="jcf-select-opener"></span></span>',
      fakeDropStructure: '<div class="jcf-select-drop"><div class="jcf-select-drop-content"></div></div>',
      optionClassPrefix: 'jcf-option-',
      selectClassPrefix: 'jcf-select-',
      dropContentSelector: '.jcf-select-drop-content',
      selectTextSelector: '.jcf-select-text',
      dropActiveClass: 'jcf-drop-active',
      flipDropClass: 'jcf-drop-flipped'
    }, options);
    this.init();
  }
  $.extend(ComboBox.prototype, {
    init: function() {
      this.initStructure();
      this.bindHandlers();
      this.attachEvents();
      this.refresh();
    },
    initStructure: function() {
      // prepare structure
      this.win = $(window);
      this.doc = $(document);
      this.realElement = $(this.options.element);
      this.fakeElement = $(this.options.fakeAreaStructure).insertAfter(this.realElement);
      this.selectTextContainer = this.fakeElement.find(this.options.selectTextSelector);
      this.selectText = $('<span></span>').appendTo(this.selectTextContainer);
      makeUnselectable(this.fakeElement);

      // copy classes from original select
      this.fakeElement.addClass(getPrefixedClasses(this.realElement.prop('className'), this.options.selectClassPrefix));

      // handle compact multiple style
      if (this.realElement.prop('multiple')) {
        this.fakeElement.addClass('jcf-compact-multiple');
      }

      // detect device type and dropdown behavior
      if (this.options.isMobileDevice && this.options.wrapNativeOnMobile && !this.options.wrapNative) {
        this.options.wrapNative = true;
      }

      if (this.options.wrapNative) {
        // wrap native select inside fake block
        this.realElement.prependTo(this.fakeElement).css({
          position: 'absolute',
          height: '100%',
          width: '100%'
        }).addClass(this.options.resetAppearanceClass);
      } else {
        // just hide native select
        this.realElement.addClass(this.options.hiddenClass);
        this.fakeElement.attr('title', this.realElement.attr('title'));
        this.fakeDropTarget = this.options.fakeDropInBody ? $('body') : this.fakeElement;
      }
    },
    attachEvents: function() {
      // delayed refresh handler
      var self = this;
      this.delayedRefresh = function() {
        setTimeout(function() {
          self.refresh();
          if (self.list) {
            self.list.refresh();
            self.list.scrollToActiveOption();
          }
        }, 1);
      };

      // native dropdown event handlers
      if (this.options.wrapNative) {
        this.realElement.on({
          focus: this.onFocus,
          change: this.onChange,
          click: this.onChange,
          keydown: this.delayedRefresh
        });
      } else {
        // custom dropdown event handlers
        this.realElement.on({
          focus: this.onFocus,
          change: this.onChange,
          keydown: this.onKeyDown
        });
        this.fakeElement.on({
          'jcf-pointerdown': this.onSelectAreaPress
        });
      }
    },
    onKeyDown: function(e) {
      if (e.which === 13) {
        this.toggleDropdown();
      } else if (this.dropActive) {
        this.delayedRefresh();
      }
    },
    onChange: function() {
      this.refresh();
    },
    onFocus: function() {
      if (!this.pressedFlag || !this.focusedFlag) {
        this.fakeElement.addClass(this.options.focusClass);
        this.realElement.on('blur', this.onBlur);
        this.toggleListMode(true);
        this.focusedFlag = true;
      }
    },
    onBlur: function() {
      if (!this.pressedFlag) {
        this.fakeElement.removeClass(this.options.focusClass);
        this.realElement.off('blur', this.onBlur);
        this.toggleListMode(false);
        this.focusedFlag = false;
      }
    },
    onResize: function() {
      if (this.dropActive) {
        this.hideDropdown();
      }
    },
    onSelectDropPress: function() {
      this.pressedFlag = true;
    },
    onSelectDropRelease: function(e, pointerEvent) {
      this.pressedFlag = false;
      if (pointerEvent.pointerType === 'mouse') {
        this.realElement.focus();
      }
    },
    onSelectAreaPress: function(e) {
      // skip click if drop inside fake element or real select is disabled
      var dropClickedInsideFakeElement = !this.options.fakeDropInBody && $(e.target).closest(this.dropdown).length;
      if (dropClickedInsideFakeElement || e.button > 1 || this.realElement.is(':disabled')) {
        return;
      }

      // toggle dropdown visibility
      this.selectOpenedByEvent = e.pointerType;
      this.toggleDropdown();

      // misc handlers
      if (!this.focusedFlag) {
        if (e.pointerType === 'mouse') {
          this.realElement.focus();
        } else {
          this.onFocus(e);
        }
      }
      this.pressedFlag = true;
      this.fakeElement.addClass(this.options.pressedClass);
      this.doc.on('jcf-pointerup', this.onSelectAreaRelease);
    },
    onSelectAreaRelease: function(e) {
      if (this.focusedFlag && e.pointerType === 'mouse') {
        this.realElement.focus();
      }
      this.pressedFlag = false;
      this.fakeElement.removeClass(this.options.pressedClass);
      this.doc.off('jcf-pointerup', this.onSelectAreaRelease);
    },
    onOutsideClick: function(e) {
      var target = $(e.target),
        clickedInsideSelect = target.closest(this.fakeElement).length || target.closest(this.dropdown).length;

      if (!clickedInsideSelect) {
        this.hideDropdown();
      }
    },
    onSelect: function() {
      this.refresh();

      if (this.realElement.prop('multiple')) {
        this.repositionDropdown();
      } else {
        this.hideDropdown();
      }

      this.fireNativeEvent(this.realElement, 'change');
    },
    toggleListMode: function(state) {
      if (!this.options.wrapNative) {
        if (state) {
          // temporary change select to list to avoid appearing of native dropdown
          this.realElement.attr({
            size: 4,
            'jcf-size': ''
          });
        } else {
          // restore select from list mode to dropdown select
          if (!this.options.wrapNative) {
            this.realElement.removeAttr('size jcf-size');
          }
        }
      }
    },
    createDropdown: function() {
      // destroy previous dropdown if needed
      if (this.dropdown) {
        this.list.destroy();
        this.dropdown.remove();
      }

      // create new drop container
      this.dropdown = $(this.options.fakeDropStructure).appendTo(this.fakeDropTarget);
      this.dropdown.addClass(getPrefixedClasses(this.realElement.prop('className'), this.options.selectClassPrefix));
      makeUnselectable(this.dropdown);

      // handle compact multiple style
      if (this.realElement.prop('multiple')) {
        this.dropdown.addClass('jcf-compact-multiple');
      }

      // set initial styles for dropdown in body
      if (this.options.fakeDropInBody) {
        this.dropdown.css({
          position: 'absolute',
          top: -9999
        });
      }

      // create new select list instance
      this.list = new SelectList({
        useHoverClass: true,
        handleResize: false,
        alwaysPreventMouseWheel: true,
        maxVisibleItems: this.options.maxVisibleItems,
        useCustomScroll: this.options.useCustomScroll,
        holder: this.dropdown.find(this.options.dropContentSelector),
        multipleSelectWithoutKey: this.realElement.prop('multiple'),
        element: this.realElement
      });
      $(this.list).on({
        select: this.onSelect,
        press: this.onSelectDropPress,
        release: this.onSelectDropRelease
      });
    },
    repositionDropdown: function() {
      var selectOffset = this.fakeElement.offset(),
        fakeElementBounds = this.fakeElement[0].getBoundingClientRect(),
        selectWidth = fakeElementBounds.width || fakeElementBounds.right - fakeElementBounds.left,
        selectHeight = this.fakeElement.outerHeight(),
        dropHeight = this.dropdown.css('width', selectWidth).outerHeight(),
        winScrollTop = this.win.scrollTop(),
        winHeight = this.win.height(),
        calcTop, calcLeft, bodyOffset, needFlipDrop = false;

      // check flip drop position
      if (selectOffset.top + selectHeight + dropHeight > winScrollTop + winHeight && selectOffset.top - dropHeight > winScrollTop) {
        needFlipDrop = true;
      }

      if (this.options.fakeDropInBody) {
        bodyOffset = this.fakeDropTarget.css('position') !== 'static' ? this.fakeDropTarget.offset().top : 0;
        if (this.options.flipDropToFit && needFlipDrop) {
          // calculate flipped dropdown position
          calcLeft = selectOffset.left;
          calcTop = selectOffset.top - dropHeight - bodyOffset;
        } else {
          // calculate default drop position
          calcLeft = selectOffset.left;
          calcTop = selectOffset.top + selectHeight - bodyOffset;
        }

        // update drop styles
        this.dropdown.css({
          width: selectWidth,
          left: calcLeft,
          top: calcTop
        });
      }

      // refresh flipped class
      this.dropdown.add(this.fakeElement).toggleClass(this.options.flipDropClass, this.options.flipDropToFit && needFlipDrop);
    },
    showDropdown: function() {
      // do not show empty custom dropdown
      if (!this.realElement.prop('options').length) {
        return;
      }

      // create options list if not created
      if (!this.dropdown) {
        this.createDropdown();
      }

      // show dropdown
      this.dropActive = true;
      this.dropdown.appendTo(this.fakeDropTarget);
      this.fakeElement.addClass(this.options.dropActiveClass);
      this.refreshSelectedText();
      this.repositionDropdown();
      this.list.setScrollTop(this.savedScrollTop);
      this.list.refresh();

      // add temporary event handlers
      this.win.on('resize', this.onResize);
      this.doc.on('jcf-pointerdown', this.onOutsideClick);
    },
    hideDropdown: function() {
      if (this.dropdown) {
        this.savedScrollTop = this.list.getScrollTop();
        this.fakeElement.removeClass(this.options.dropActiveClass + ' ' + this.options.flipDropClass);
        this.dropdown.removeClass(this.options.flipDropClass).detach();
        this.doc.off('jcf-pointerdown', this.onOutsideClick);
        this.win.off('resize', this.onResize);
        this.dropActive = false;
        if (this.selectOpenedByEvent === 'touch') {
          this.onBlur();
        }
      }
    },
    toggleDropdown: function() {
      if (this.dropActive) {
        this.hideDropdown();
      } else {
        this.showDropdown();
      }
    },
    refreshSelectedText: function() {
      // redraw selected area
      var selectedIndex = this.realElement.prop('selectedIndex'),
        selectedOption = this.realElement.prop('options')[selectedIndex],
        selectedOptionImage = selectedOption ? selectedOption.getAttribute('data-image') : null,
        selectedOptionText = '',
        selectedOptionClasses,
        self = this;

      if (this.realElement.prop('multiple')) {
        $.each(this.realElement.prop('options'), function(index, option) {
          if (option.selected) {
            selectedOptionText += (selectedOptionText ? ', ' : '') + option.innerHTML;
          }
        });
        if (!selectedOptionText) {
          selectedOptionText = self.realElement.attr('placeholder') || '';
        }
        this.selectText.removeAttr('class').html(selectedOptionText);
      } else if (!selectedOption) {
        if (this.selectImage) {
          this.selectImage.hide();
        }
        this.selectText.removeAttr('class').empty();
      } else if (this.currentSelectedText !== selectedOption.innerHTML || this.currentSelectedImage !== selectedOptionImage) {
        selectedOptionClasses = getPrefixedClasses(selectedOption.className, this.options.optionClassPrefix);
        this.selectText.attr('class', selectedOptionClasses).html(selectedOption.innerHTML);

        if (selectedOptionImage) {
          if (!this.selectImage) {
            this.selectImage = $('<img>').prependTo(this.selectTextContainer).hide();
          }
          this.selectImage.attr('src', selectedOptionImage).show();
        } else if (this.selectImage) {
          this.selectImage.hide();
        }

        this.currentSelectedText = selectedOption.innerHTML;
        this.currentSelectedImage = selectedOptionImage;
      }
    },
    refresh: function() {
      // refresh fake select visibility
      if (this.realElement.prop('style').display === 'none') {
        this.fakeElement.hide();
      } else {
        this.fakeElement.show();
      }

      // refresh selected text
      this.refreshSelectedText();

      // handle disabled state
      this.fakeElement.toggleClass(this.options.disabledClass, this.realElement.is(':disabled'));
    },
    destroy: function() {
      // restore structure
      if (this.options.wrapNative) {
        this.realElement.insertBefore(this.fakeElement).css({
          position: '',
          height: '',
          width: ''
        }).removeClass(this.options.resetAppearanceClass);
      } else {
        this.realElement.removeClass(this.options.hiddenClass);
        if (this.realElement.is('[jcf-size]')) {
          this.realElement.removeAttr('size jcf-size');
        }
      }

      // removing element will also remove its event handlers
      this.fakeElement.remove();

      // remove other event handlers
      this.doc.off('jcf-pointerup', this.onSelectAreaRelease);
      this.realElement.off({
        focus: this.onFocus
      });
    }
  });

  // listbox module
  function ListBox(options) {
    this.options = $.extend({
      wrapNative: true,
      useCustomScroll: true,
      fakeStructure: '<span class="jcf-list-box"><span class="jcf-list-wrapper"></span></span>',
      selectClassPrefix: 'jcf-select-',
      listHolder: '.jcf-list-wrapper'
    }, options);
    this.init();
  }
  $.extend(ListBox.prototype, {
    init: function() {
      this.bindHandlers();
      this.initStructure();
      this.attachEvents();
    },
    initStructure: function() {
      this.realElement = $(this.options.element);
      this.fakeElement = $(this.options.fakeStructure).insertAfter(this.realElement);
      this.listHolder = this.fakeElement.find(this.options.listHolder);
      makeUnselectable(this.fakeElement);

      // copy classes from original select
      this.fakeElement.addClass(getPrefixedClasses(this.realElement.prop('className'), this.options.selectClassPrefix));
      this.realElement.addClass(this.options.hiddenClass);

      this.list = new SelectList({
        useCustomScroll: this.options.useCustomScroll,
        holder: this.listHolder,
        selectOnClick: false,
        element: this.realElement
      });
    },
    attachEvents: function() {
      // delayed refresh handler
      var self = this;
      this.delayedRefresh = function(e) {
        if (e && (e.which === 16 || e.ctrlKey || e.metaKey || e.altKey)) {
          // ignore modifier keys
          return;
        } else {
          clearTimeout(self.refreshTimer);
          self.refreshTimer = setTimeout(function() {
            self.refresh();
            self.list.scrollToActiveOption();
          }, 1);
        }
      };

      // other event handlers
      this.realElement.on({
        focus: this.onFocus,
        click: this.delayedRefresh,
        keydown: this.delayedRefresh
      });

      // select list event handlers
      $(this.list).on({
        select: this.onSelect,
        press: this.onFakeOptionsPress,
        release: this.onFakeOptionsRelease
      });
    },
    onFakeOptionsPress: function(e, pointerEvent) {
      this.pressedFlag = true;
      if (pointerEvent.pointerType === 'mouse') {
        this.realElement.focus();
      }
    },
    onFakeOptionsRelease: function(e, pointerEvent) {
      this.pressedFlag = false;
      if (pointerEvent.pointerType === 'mouse') {
        this.realElement.focus();
      }
    },
    onSelect: function() {
      this.fireNativeEvent(this.realElement, 'change');
      this.fireNativeEvent(this.realElement, 'click');
    },
    onFocus: function() {
      if (!this.pressedFlag || !this.focusedFlag) {
        this.fakeElement.addClass(this.options.focusClass);
        this.realElement.on('blur', this.onBlur);
        this.focusedFlag = true;
      }
    },
    onBlur: function() {
      if (!this.pressedFlag) {
        this.fakeElement.removeClass(this.options.focusClass);
        this.realElement.off('blur', this.onBlur);
        this.focusedFlag = false;
      }
    },
    refresh: function() {
      this.fakeElement.toggleClass(this.options.disabledClass, this.realElement.is(':disabled'));
      this.list.refresh();
    },
    destroy: function() {
      this.list.destroy();
      this.realElement.insertBefore(this.fakeElement).removeClass(this.options.hiddenClass);
      this.fakeElement.remove();
    }
  });

  // options list module
  function SelectList(options) {
    this.options = $.extend({
      holder: null,
      maxVisibleItems: 10,
      selectOnClick: true,
      useHoverClass: false,
      useCustomScroll: false,
      handleResize: true,
      multipleSelectWithoutKey: false,
      alwaysPreventMouseWheel: false,
      indexAttribute: 'data-index',
      cloneClassPrefix: 'jcf-option-',
      containerStructure: '<span class="jcf-list"><span class="jcf-list-content"></span></span>',
      containerSelector: '.jcf-list-content',
      captionClass: 'jcf-optgroup-caption',
      disabledClass: 'jcf-disabled',
      optionClass: 'jcf-option',
      groupClass: 'jcf-optgroup',
      hoverClass: 'jcf-hover',
      selectedClass: 'jcf-selected',
      scrollClass: 'jcf-scroll-active'
    }, options);
    this.init();
  }
  $.extend(SelectList.prototype, {
    init: function() {
      this.initStructure();
      this.refreshSelectedClass();
      this.attachEvents();
    },
    initStructure: function() {
      this.element = $(this.options.element);
      this.indexSelector = '[' + this.options.indexAttribute + ']';
      this.container = $(this.options.containerStructure).appendTo(this.options.holder);
      this.listHolder = this.container.find(this.options.containerSelector);
      this.lastClickedIndex = this.element.prop('selectedIndex');
      this.rebuildList();

      // save current selection in multiple select
      if (this.element.prop('multiple')) {
        this.previousSelection = this.getSelectedOptionsIndexes();
      }
    },
    attachEvents: function() {
      this.bindHandlers();
      this.listHolder.on('jcf-pointerdown', this.indexSelector, this.onItemPress);
      this.listHolder.on('jcf-pointerdown', this.onPress);

      if (this.options.useHoverClass) {
        this.listHolder.on('jcf-pointerover', this.indexSelector, this.onHoverItem);
      }
    },
    onPress: function(e) {
      $(this).trigger('press', e);
      this.listHolder.on('jcf-pointerup', this.onRelease);
    },
    onRelease: function(e) {
      $(this).trigger('release', e);
      this.listHolder.off('jcf-pointerup', this.onRelease);
    },
    onHoverItem: function(e) {
      var hoverIndex = parseFloat(e.currentTarget.getAttribute(this.options.indexAttribute));
      this.fakeOptions.removeClass(this.options.hoverClass).eq(hoverIndex).addClass(this.options.hoverClass);
    },
    onItemPress: function(e) {
      if (e.pointerType === 'touch' || this.options.selectOnClick) {
        // select option after "click"
        this.tmpListOffsetTop = this.list.offset().top;
        this.listHolder.on('jcf-pointerup', this.indexSelector, this.onItemRelease);
      } else {
        // select option immediately
        this.onSelectItem(e);
      }
    },
    onItemRelease: function(e) {
      // remove event handlers and temporary data
      this.listHolder.off('jcf-pointerup', this.indexSelector, this.onItemRelease);

      // simulate item selection
      if (this.tmpListOffsetTop === this.list.offset().top) {
        this.listHolder.on('click', this.indexSelector, { savedPointerType: e.pointerType }, this.onSelectItem);
      }
      delete this.tmpListOffsetTop;
    },
    onSelectItem: function(e) {
      var clickedIndex = parseFloat(e.currentTarget.getAttribute(this.options.indexAttribute)),
        pointerType = e.data && e.data.savedPointerType || e.pointerType || 'mouse',
        range;

      // remove click event handler
      this.listHolder.off('click', this.indexSelector, this.onSelectItem);

      // ignore clicks on disabled options
      if (e.button > 1 || this.realOptions[clickedIndex].disabled) {
        return;
      }

      if (this.element.prop('multiple')) {
        if (e.metaKey || e.ctrlKey || pointerType === 'touch' || this.options.multipleSelectWithoutKey) {
          // if CTRL/CMD pressed or touch devices - toggle selected option
          this.realOptions[clickedIndex].selected = !this.realOptions[clickedIndex].selected;
        } else if (e.shiftKey) {
          // if SHIFT pressed - update selection
          range = [this.lastClickedIndex, clickedIndex].sort(function(a, b) {
            return a - b;
          });
          this.realOptions.each(function(index, option) {
            option.selected = (index >= range[0] && index <= range[1]);
          });
        } else {
          // set single selected index
          this.element.prop('selectedIndex', clickedIndex);
        }
      } else {
        this.element.prop('selectedIndex', clickedIndex);
      }

      // save last clicked option
      if (!e.shiftKey) {
        this.lastClickedIndex = clickedIndex;
      }

      // refresh classes
      this.refreshSelectedClass();

      // scroll to active item in desktop browsers
      if (pointerType === 'mouse') {
        this.scrollToActiveOption();
      }

      // make callback when item selected
      $(this).trigger('select');
    },
    rebuildList: function() {
      // rebuild options
      var self = this,
        rootElement = this.element[0];

      // recursively create fake options
      this.storedSelectHTML = rootElement.innerHTML;
      this.optionIndex = 0;
      this.list = $(this.createOptionsList(rootElement));
      this.listHolder.empty().append(this.list);
      this.realOptions = this.element.find('option');
      this.fakeOptions = this.list.find(this.indexSelector);
      this.fakeListItems = this.list.find('.' + this.options.captionClass + ',' + this.indexSelector);
      delete this.optionIndex;

      // detect max visible items
      var maxCount = this.options.maxVisibleItems,
        sizeValue = this.element.prop('size');
      if (sizeValue > 1 && !this.element.is('[jcf-size]')) {
        maxCount = sizeValue;
      }

      // handle scrollbar
      var needScrollBar = this.fakeOptions.length > maxCount;
      this.container.toggleClass(this.options.scrollClass, needScrollBar);
      if (needScrollBar) {
        // change max-height
        this.listHolder.css({
          maxHeight: this.getOverflowHeight(maxCount),
          overflow: 'auto'
        });

        if (this.options.useCustomScroll && jcf.modules.Scrollable) {
          // add custom scrollbar if specified in options
          jcf.replace(this.listHolder, 'Scrollable', {
            handleResize: this.options.handleResize,
            alwaysPreventMouseWheel: this.options.alwaysPreventMouseWheel
          });
          return;
        }
      }

      // disable edge wheel scrolling
      if (this.options.alwaysPreventMouseWheel) {
        this.preventWheelHandler = function(e) {
          var currentScrollTop = self.listHolder.scrollTop(),
            maxScrollTop = self.listHolder.prop('scrollHeight') - self.listHolder.innerHeight();

          // check edge cases
          if ((currentScrollTop <= 0 && e.deltaY < 0) || (currentScrollTop >= maxScrollTop && e.deltaY > 0)) {
            e.preventDefault();
          }
        };
        this.listHolder.on('jcf-mousewheel', this.preventWheelHandler);
      }
    },
    refreshSelectedClass: function() {
      var self = this,
        selectedItem,
        isMultiple = this.element.prop('multiple'),
        selectedIndex = this.element.prop('selectedIndex');

      if (isMultiple) {
        this.realOptions.each(function(index, option) {
          self.fakeOptions.eq(index).toggleClass(self.options.selectedClass, !!option.selected);
        });
      } else {
        this.fakeOptions.removeClass(this.options.selectedClass + ' ' + this.options.hoverClass);
        selectedItem = this.fakeOptions.eq(selectedIndex).addClass(this.options.selectedClass);
        if (this.options.useHoverClass) {
          selectedItem.addClass(this.options.hoverClass);
        }
      }
    },
    scrollToActiveOption: function() {
      // scroll to target option
      var targetOffset = this.getActiveOptionOffset();
      if (typeof targetOffset === 'number') {
        this.listHolder.prop('scrollTop', targetOffset);
      }
    },
    getSelectedOptionsIndexes: function() {
      var selection = [];
      this.realOptions.each(function(index, option) {
        if (option.selected) {
          selection.push(index);
        }
      });
      return selection;
    },
    getChangedSelectedIndex: function() {
      var selectedIndex = this.element.prop('selectedIndex'),
        self = this,
        found = false,
        targetIndex = null;

      if (this.element.prop('multiple')) {
        // multiple selects handling
        this.currentSelection = this.getSelectedOptionsIndexes();
        $.each(this.currentSelection, function(index, optionIndex) {
          if (!found && self.previousSelection.indexOf(optionIndex) < 0) {
            if (index === 0) {
              found = true;
            }
            targetIndex = optionIndex;
          }
        });
        this.previousSelection = this.currentSelection;
        return targetIndex;
      } else {
        // single choice selects handling
        return selectedIndex;
      }
    },
    getActiveOptionOffset: function() {
      // calc values
      var currentIndex = this.getChangedSelectedIndex();

      // selection was not changed
      if (currentIndex === null) {
        return;
      }

      // find option and scroll to it if needed
      var dropHeight = this.listHolder.height(),
        dropScrollTop = this.listHolder.prop('scrollTop'),
        fakeOption = this.fakeOptions.eq(currentIndex),
        fakeOptionOffset = fakeOption.offset().top - this.list.offset().top,
        fakeOptionHeight = fakeOption.innerHeight();

      // scroll list
      if (fakeOptionOffset + fakeOptionHeight >= dropScrollTop + dropHeight) {
        // scroll down (always scroll to option)
        return fakeOptionOffset - dropHeight + fakeOptionHeight;
      } else if (fakeOptionOffset < dropScrollTop) {
        // scroll up to option
        return fakeOptionOffset;
      }
    },
    getOverflowHeight: function(sizeValue) {
      var item = this.fakeListItems.eq(sizeValue - 1),
        listOffset = this.list.offset().top,
        itemOffset = item.offset().top,
        itemHeight = item.innerHeight();

      return itemOffset + itemHeight - listOffset;
    },
    getScrollTop: function() {
      return this.listHolder.scrollTop();
    },
    setScrollTop: function(value) {
      this.listHolder.scrollTop(value);
    },
    createOption: function(option) {
      var newOption = document.createElement('span');
      newOption.className = this.options.optionClass;
      newOption.innerHTML = option.innerHTML;
      newOption.setAttribute(this.options.indexAttribute, this.optionIndex++);

      var optionImage, optionImageSrc = option.getAttribute('data-image');
      if (optionImageSrc) {
        optionImage = document.createElement('img');
        optionImage.src = optionImageSrc;
        newOption.insertBefore(optionImage, newOption.childNodes[0]);
      }
      if (option.disabled) {
        newOption.className += ' ' + this.options.disabledClass;
      }
      if (option.className) {
        newOption.className += ' ' + getPrefixedClasses(option.className, this.options.cloneClassPrefix);
      }
      return newOption;
    },
    createOptGroup: function(optgroup) {
      var optGroupContainer = document.createElement('span'),
        optGroupName = optgroup.getAttribute('label'),
        optGroupCaption, optGroupList;

      // create caption
      optGroupCaption = document.createElement('span');
      optGroupCaption.className = this.options.captionClass;
      optGroupCaption.innerHTML = optGroupName;
      optGroupContainer.appendChild(optGroupCaption);

      // create list of options
      if (optgroup.children.length) {
        optGroupList = this.createOptionsList(optgroup);
        optGroupContainer.appendChild(optGroupList);
      }

      optGroupContainer.className = this.options.groupClass;
      return optGroupContainer;
    },
    createOptionContainer: function() {
      var optionContainer = document.createElement('li');
      return optionContainer;
    },
    createOptionsList: function(container) {
      var self = this,
        list = document.createElement('ul');

      $.each(container.children, function(index, currentNode) {
        var item = self.createOptionContainer(currentNode),
          newNode;

        switch (currentNode.tagName.toLowerCase()) {
          case 'option': newNode = self.createOption(currentNode); break;
          case 'optgroup': newNode = self.createOptGroup(currentNode); break;
        }
        list.appendChild(item).appendChild(newNode);
      });
      return list;
    },
    refresh: function() {
      // check for select innerHTML changes
      if (this.storedSelectHTML !== this.element.prop('innerHTML')) {
        this.rebuildList();
      }

      // refresh custom scrollbar
      var scrollInstance = jcf.getInstance(this.listHolder);
      if (scrollInstance) {
        scrollInstance.refresh();
      }

      // refresh selectes classes
      this.refreshSelectedClass();
    },
    destroy: function() {
      this.listHolder.off('jcf-mousewheel', this.preventWheelHandler);
      this.listHolder.off('jcf-pointerdown', this.indexSelector, this.onSelectItem);
      this.listHolder.off('jcf-pointerover', this.indexSelector, this.onHoverItem);
      this.listHolder.off('jcf-pointerdown', this.onPress);
    }
  });

  // helper functions
  var getPrefixedClasses = function(className, prefixToAdd) {
    return className ? className.replace(/[\s]*([\S]+)+[\s]*/gi, prefixToAdd + '$1 ') : '';
  };
  var makeUnselectable = (function() {
    var unselectableClass = jcf.getOptions().unselectableClass;
    function preventHandler(e) {
      e.preventDefault();
    }
    return function(node) {
      node.addClass(unselectableClass).on('selectstart', preventHandler);
    };
  }());

  return module;
});

}(jcf));


/*!
 * JavaScript Custom Forms : Scrollbar Module
 *
 * Copyright 2014-2015 PSD2HTML - http://psd2html.com/jcf
 * Released under the MIT license (LICENSE.txt)
 *
 * Version: 1.2.3
 */

(function(jcf) {

jcf.addModule(function($, window) {
  'use strict';

  var module = {
    name: 'Scrollable',
    selector: '.jcf-scrollable',
    plugins: {
      ScrollBar: ScrollBar
    },
    options: {
      mouseWheelStep: 150,
      handleResize: true,
      alwaysShowScrollbars: false,
      alwaysPreventMouseWheel: false,
      scrollAreaStructure: '<div class="jcf-scrollable-wrapper"></div>'
    },
    matchElement: function(element) {
      return element.is('.jcf-scrollable');
    },
    init: function() {
      this.initStructure();
      this.attachEvents();
      this.rebuildScrollbars();
    },
    initStructure: function() {
      // prepare structure
      this.doc = $(document);
      this.win = $(window);
      this.realElement = $(this.options.element);
      this.scrollWrapper = $(this.options.scrollAreaStructure).insertAfter(this.realElement);

      // set initial styles
      this.scrollWrapper.css('position', 'relative');
      this.realElement.css('overflow', 'hidden');
      this.vBarEdge = 0;
    },
    attachEvents: function() {
      // create scrollbars
      var self = this;
      this.vBar = new ScrollBar({
        holder: this.scrollWrapper,
        vertical: true,
        onScroll: function(scrollTop) {
          self.realElement.scrollTop(scrollTop);
        }
      });
      this.hBar = new ScrollBar({
        holder: this.scrollWrapper,
        vertical: false,
        onScroll: function(scrollLeft) {
          self.realElement.scrollLeft(scrollLeft);
        }
      });

      // add event handlers
      this.realElement.on('scroll', this.onScroll);
      if (this.options.handleResize) {
        this.win.on('resize orientationchange load', this.onResize);
      }

      // add pointer/wheel event handlers
      this.realElement.on('jcf-mousewheel', this.onMouseWheel);
      this.realElement.on('jcf-pointerdown', this.onTouchBody);
    },
    onScroll: function() {
      this.redrawScrollbars();
    },
    onResize: function() {
      // do not rebuild scrollbars if form field is in focus
      if (!$(document.activeElement).is(':input')) {
        this.rebuildScrollbars();
      }
    },
    onTouchBody: function(e) {
      if (e.pointerType === 'touch') {
        this.touchData = {
          scrollTop: this.realElement.scrollTop(),
          scrollLeft: this.realElement.scrollLeft(),
          left: e.pageX,
          top: e.pageY
        };
        this.doc.on({
          'jcf-pointermove': this.onMoveBody,
          'jcf-pointerup': this.onReleaseBody
        });
      }
    },
    onMoveBody: function(e) {
      var targetScrollTop,
        targetScrollLeft,
        verticalScrollAllowed = this.verticalScrollActive,
        horizontalScrollAllowed = this.horizontalScrollActive;

      if (e.pointerType === 'touch') {
        targetScrollTop = this.touchData.scrollTop - e.pageY + this.touchData.top;
        targetScrollLeft = this.touchData.scrollLeft - e.pageX + this.touchData.left;

        // check that scrolling is ended and release outer scrolling
        if (this.verticalScrollActive && (targetScrollTop < 0 || targetScrollTop > this.vBar.maxValue)) {
          verticalScrollAllowed = false;
        }
        if (this.horizontalScrollActive && (targetScrollLeft < 0 || targetScrollLeft > this.hBar.maxValue)) {
          horizontalScrollAllowed = false;
        }

        this.realElement.scrollTop(targetScrollTop);
        this.realElement.scrollLeft(targetScrollLeft);

        if (verticalScrollAllowed || horizontalScrollAllowed) {
          e.preventDefault();
        } else {
          this.onReleaseBody(e);
        }
      }
    },
    onReleaseBody: function(e) {
      if (e.pointerType === 'touch') {
        delete this.touchData;
        this.doc.off({
          'jcf-pointermove': this.onMoveBody,
          'jcf-pointerup': this.onReleaseBody
        });
      }
    },
    onMouseWheel: function(e) {
      var currentScrollTop = this.realElement.scrollTop(),
        currentScrollLeft = this.realElement.scrollLeft(),
        maxScrollTop = this.realElement.prop('scrollHeight') - this.embeddedDimensions.innerHeight,
        maxScrollLeft = this.realElement.prop('scrollWidth') - this.embeddedDimensions.innerWidth,
        extraLeft, extraTop, preventFlag;

      // check edge cases
      if (!this.options.alwaysPreventMouseWheel) {
        if (this.verticalScrollActive && e.deltaY) {
          if (!(currentScrollTop <= 0 && e.deltaY < 0) && !(currentScrollTop >= maxScrollTop && e.deltaY > 0)) {
            preventFlag = true;
          }
        }
        if (this.horizontalScrollActive && e.deltaX) {
          if (!(currentScrollLeft <= 0 && e.deltaX < 0) && !(currentScrollLeft >= maxScrollLeft && e.deltaX > 0)) {
            preventFlag = true;
          }
        }
        if (!this.verticalScrollActive && !this.horizontalScrollActive) {
          return;
        }
      }

      // prevent default action and scroll item
      if (preventFlag || this.options.alwaysPreventMouseWheel) {
        e.preventDefault();
      } else {
        return;
      }

      extraLeft = e.deltaX / 100 * this.options.mouseWheelStep;
      extraTop = e.deltaY / 100 * this.options.mouseWheelStep;

      this.realElement.scrollTop(currentScrollTop + extraTop);
      this.realElement.scrollLeft(currentScrollLeft + extraLeft);
    },
    setScrollBarEdge: function(edgeSize) {
      this.vBarEdge = edgeSize || 0;
      this.redrawScrollbars();
    },
    saveElementDimensions: function() {
      this.savedDimensions = {
        top: this.realElement.width(),
        left: this.realElement.height()
      };
      return this;
    },
    restoreElementDimensions: function() {
      if (this.savedDimensions) {
        this.realElement.css({
          width: this.savedDimensions.width,
          height: this.savedDimensions.height
        });
      }
      return this;
    },
    saveScrollOffsets: function() {
      this.savedOffsets = {
        top: this.realElement.scrollTop(),
        left: this.realElement.scrollLeft()
      };
      return this;
    },
    restoreScrollOffsets: function() {
      if (this.savedOffsets) {
        this.realElement.scrollTop(this.savedOffsets.top);
        this.realElement.scrollLeft(this.savedOffsets.left);
      }
      return this;
    },
    getContainerDimensions: function() {
      // save current styles
      var desiredDimensions,
        currentStyles,
        currentHeight,
        currentWidth;

      if (this.isModifiedStyles) {
        desiredDimensions = {
          width: this.realElement.innerWidth() + this.vBar.getThickness(),
          height: this.realElement.innerHeight() + this.hBar.getThickness()
        };
      } else {
        // unwrap real element and measure it according to CSS
        this.saveElementDimensions().saveScrollOffsets();
        this.realElement.insertAfter(this.scrollWrapper);
        this.scrollWrapper.detach();

        // measure element
        currentStyles = this.realElement.prop('style');
        currentWidth = parseFloat(currentStyles.width);
        currentHeight = parseFloat(currentStyles.height);

        // reset styles if needed
        if (this.embeddedDimensions && currentWidth && currentHeight) {
          this.isModifiedStyles |= (currentWidth !== this.embeddedDimensions.width || currentHeight !== this.embeddedDimensions.height);
          this.realElement.css({
            overflow: '',
            width: '',
            height: ''
          });
        }

        // calculate desired dimensions for real element
        desiredDimensions = {
          width: this.realElement.outerWidth(),
          height: this.realElement.outerHeight()
        };

        // restore structure and original scroll offsets
        this.scrollWrapper.insertAfter(this.realElement);
        this.realElement.css('overflow', 'hidden').prependTo(this.scrollWrapper);
        this.restoreElementDimensions().restoreScrollOffsets();
      }

      return desiredDimensions;
    },
    getEmbeddedDimensions: function(dimensions) {
      // handle scrollbars cropping
      var fakeBarWidth = this.vBar.getThickness(),
        fakeBarHeight = this.hBar.getThickness(),
        paddingWidth = this.realElement.outerWidth() - this.realElement.width(),
        paddingHeight = this.realElement.outerHeight() - this.realElement.height(),
        resultDimensions;

      if (this.options.alwaysShowScrollbars) {
        // simply return dimensions without custom scrollbars
        this.verticalScrollActive = true;
        this.horizontalScrollActive = true;
        resultDimensions = {
          innerWidth: dimensions.width - fakeBarWidth,
          innerHeight: dimensions.height - fakeBarHeight
        };
      } else {
        // detect when to display each scrollbar
        this.saveElementDimensions();
        this.verticalScrollActive = false;
        this.horizontalScrollActive = false;

        // fill container with full size
        this.realElement.css({
          width: dimensions.width - paddingWidth,
          height: dimensions.height - paddingHeight
        });

        this.horizontalScrollActive = this.realElement.prop('scrollWidth') > this.containerDimensions.width;
        this.verticalScrollActive = this.realElement.prop('scrollHeight') > this.containerDimensions.height;

        this.restoreElementDimensions();
        resultDimensions = {
          innerWidth: dimensions.width - (this.verticalScrollActive ? fakeBarWidth : 0),
          innerHeight: dimensions.height - (this.horizontalScrollActive ? fakeBarHeight : 0)
        };
      }
      $.extend(resultDimensions, {
        width: resultDimensions.innerWidth - paddingWidth,
        height: resultDimensions.innerHeight - paddingHeight
      });
      return resultDimensions;
    },
    rebuildScrollbars: function() {
      // resize wrapper according to real element styles
      this.containerDimensions = this.getContainerDimensions();
      this.embeddedDimensions = this.getEmbeddedDimensions(this.containerDimensions);

      // resize wrapper to desired dimensions
      this.scrollWrapper.css({
        width: this.containerDimensions.width,
        height: this.containerDimensions.height
      });

      // resize element inside wrapper excluding scrollbar size
      this.realElement.css({
        overflow: 'hidden',
        width: this.embeddedDimensions.width,
        height: this.embeddedDimensions.height
      });

      // redraw scrollbar offset
      this.redrawScrollbars();
    },
    redrawScrollbars: function() {
      var viewSize, maxScrollValue;

      // redraw vertical scrollbar
      if (this.verticalScrollActive) {
        viewSize = this.vBarEdge ? this.containerDimensions.height - this.vBarEdge : this.embeddedDimensions.innerHeight;
        maxScrollValue = Math.max(this.realElement.prop('offsetHeight'), this.realElement.prop('scrollHeight')) - this.vBarEdge;

        this.vBar.show().setMaxValue(maxScrollValue - viewSize).setRatio(viewSize / maxScrollValue).setSize(viewSize);
        this.vBar.setValue(this.realElement.scrollTop());
      } else {
        this.vBar.hide();
      }

      // redraw horizontal scrollbar
      if (this.horizontalScrollActive) {
        viewSize = this.embeddedDimensions.innerWidth;
        maxScrollValue = this.realElement.prop('scrollWidth');

        if (maxScrollValue === viewSize) {
          this.horizontalScrollActive = false;
        }
        this.hBar.show().setMaxValue(maxScrollValue - viewSize).setRatio(viewSize / maxScrollValue).setSize(viewSize);
        this.hBar.setValue(this.realElement.scrollLeft());
      } else {
        this.hBar.hide();
      }

      // set "touch-action" style rule
      var touchAction = '';
      if (this.verticalScrollActive && this.horizontalScrollActive) {
        touchAction = 'none';
      } else if (this.verticalScrollActive) {
        touchAction = 'pan-x';
      } else if (this.horizontalScrollActive) {
        touchAction = 'pan-y';
      }
      this.realElement.css('touchAction', touchAction);
    },
    refresh: function() {
      this.rebuildScrollbars();
    },
    destroy: function() {
      // remove event listeners
      this.win.off('resize orientationchange load', this.onResize);
      this.realElement.off({
        'jcf-mousewheel': this.onMouseWheel,
        'jcf-pointerdown': this.onTouchBody
      });
      this.doc.off({
        'jcf-pointermove': this.onMoveBody,
        'jcf-pointerup': this.onReleaseBody
      });

      // restore structure
      this.saveScrollOffsets();
      this.vBar.destroy();
      this.hBar.destroy();
      this.realElement.insertAfter(this.scrollWrapper).css({
        touchAction: '',
        overflow: '',
        width: '',
        height: ''
      });
      this.scrollWrapper.remove();
      this.restoreScrollOffsets();
    }
  };

  // custom scrollbar
  function ScrollBar(options) {
    this.options = $.extend({
      holder: null,
      vertical: true,
      inactiveClass: 'jcf-inactive',
      verticalClass: 'jcf-scrollbar-vertical',
      horizontalClass: 'jcf-scrollbar-horizontal',
      scrollbarStructure: '<div class="jcf-scrollbar"><div class="jcf-scrollbar-dec"></div><div class="jcf-scrollbar-slider"><div class="jcf-scrollbar-handle"></div></div><div class="jcf-scrollbar-inc"></div></div>',
      btnDecSelector: '.jcf-scrollbar-dec',
      btnIncSelector: '.jcf-scrollbar-inc',
      sliderSelector: '.jcf-scrollbar-slider',
      handleSelector: '.jcf-scrollbar-handle',
      scrollInterval: 300,
      scrollStep: 400 // px/sec
    }, options);
    this.init();
  }
  $.extend(ScrollBar.prototype, {
    init: function() {
      this.initStructure();
      this.attachEvents();
    },
    initStructure: function() {
      // define proporties
      this.doc = $(document);
      this.isVertical = !!this.options.vertical;
      this.sizeProperty = this.isVertical ? 'height' : 'width';
      this.fullSizeProperty = this.isVertical ? 'outerHeight' : 'outerWidth';
      this.invertedSizeProperty = this.isVertical ? 'width' : 'height';
      this.thicknessMeasureMethod = 'outer' + this.invertedSizeProperty.charAt(0).toUpperCase() + this.invertedSizeProperty.substr(1);
      this.offsetProperty = this.isVertical ? 'top' : 'left';
      this.offsetEventProperty = this.isVertical ? 'pageY' : 'pageX';

      // initialize variables
      this.value = this.options.value || 0;
      this.maxValue = this.options.maxValue || 0;
      this.currentSliderSize = 0;
      this.handleSize = 0;

      // find elements
      this.holder = $(this.options.holder);
      this.scrollbar = $(this.options.scrollbarStructure).appendTo(this.holder);
      this.btnDec = this.scrollbar.find(this.options.btnDecSelector);
      this.btnInc = this.scrollbar.find(this.options.btnIncSelector);
      this.slider = this.scrollbar.find(this.options.sliderSelector);
      this.handle = this.slider.find(this.options.handleSelector);

      // set initial styles
      this.scrollbar.addClass(this.isVertical ? this.options.verticalClass : this.options.horizontalClass).css({
        touchAction: this.isVertical ? 'pan-x' : 'pan-y',
        position: 'absolute'
      });
      this.slider.css({
        position: 'relative'
      });
      this.handle.css({
        touchAction: 'none',
        position: 'absolute'
      });
    },
    attachEvents: function() {
      this.bindHandlers();
      this.handle.on('jcf-pointerdown', this.onHandlePress);
      this.slider.add(this.btnDec).add(this.btnInc).on('jcf-pointerdown', this.onButtonPress);
    },
    onHandlePress: function(e) {
      if (e.pointerType === 'mouse' && e.button > 1) {
        return;
      } else {
        e.preventDefault();
        this.handleDragActive = true;
        this.sliderOffset = this.slider.offset()[this.offsetProperty];
        this.innerHandleOffset = e[this.offsetEventProperty] - this.handle.offset()[this.offsetProperty];

        this.doc.on('jcf-pointermove', this.onHandleDrag);
        this.doc.on('jcf-pointerup', this.onHandleRelease);
      }
    },
    onHandleDrag: function(e) {
      e.preventDefault();
      this.calcOffset = e[this.offsetEventProperty] - this.sliderOffset - this.innerHandleOffset;
      this.setValue(this.calcOffset / (this.currentSliderSize - this.handleSize) * this.maxValue);
      this.triggerScrollEvent(this.value);
    },
    onHandleRelease: function() {
      this.handleDragActive = false;
      this.doc.off('jcf-pointermove', this.onHandleDrag);
      this.doc.off('jcf-pointerup', this.onHandleRelease);
    },
    onButtonPress: function(e) {
      var direction, clickOffset;
      if (e.pointerType === 'mouse' && e.button > 1) {
        return;
      } else {
        e.preventDefault();
        if (!this.handleDragActive) {
          if (this.slider.is(e.currentTarget)) {
            // slider pressed
            direction = this.handle.offset()[this.offsetProperty] > e[this.offsetEventProperty] ? -1 : 1;
            clickOffset = e[this.offsetEventProperty] - this.slider.offset()[this.offsetProperty];
            this.startPageScrolling(direction, clickOffset);
          } else {
            // scrollbar buttons pressed
            direction = this.btnDec.is(e.currentTarget) ? -1 : 1;
            this.startSmoothScrolling(direction);
          }
          this.doc.on('jcf-pointerup', this.onButtonRelease);
        }
      }
    },
    onButtonRelease: function() {
      this.stopPageScrolling();
      this.stopSmoothScrolling();
      this.doc.off('jcf-pointerup', this.onButtonRelease);
    },
    startPageScrolling: function(direction, clickOffset) {
      var self = this,
        stepValue = direction * self.currentSize;

      // limit checker
      var isFinishedScrolling = function() {
        var handleTop = (self.value / self.maxValue) * (self.currentSliderSize - self.handleSize);

        if (direction > 0) {
          return handleTop + self.handleSize >= clickOffset;
        } else {
          return handleTop <= clickOffset;
        }
      };

      // scroll by page when track is pressed
      var doPageScroll = function() {
        self.value += stepValue;
        self.setValue(self.value);
        self.triggerScrollEvent(self.value);

        if (isFinishedScrolling()) {
          clearInterval(self.pageScrollTimer);
        }
      };

      // start scrolling
      this.pageScrollTimer = setInterval(doPageScroll, this.options.scrollInterval);
      doPageScroll();
    },
    stopPageScrolling: function() {
      clearInterval(this.pageScrollTimer);
    },
    startSmoothScrolling: function(direction) {
      var self = this, dt;
      this.stopSmoothScrolling();

      // simple animation functions
      var raf = window.requestAnimationFrame || function(func) {
        setTimeout(func, 16);
      };
      var getTimestamp = function() {
        return Date.now ? Date.now() : new Date().getTime();
      };

      // set animation limit
      var isFinishedScrolling = function() {
        if (direction > 0) {
          return self.value >= self.maxValue;
        } else {
          return self.value <= 0;
        }
      };

      // animation step
      var doScrollAnimation = function() {
        var stepValue = (getTimestamp() - dt) / 1000 * self.options.scrollStep;

        if (self.smoothScrollActive) {
          self.value += stepValue * direction;
          self.setValue(self.value);
          self.triggerScrollEvent(self.value);

          if (!isFinishedScrolling()) {
            dt = getTimestamp();
            raf(doScrollAnimation);
          }
        }
      };

      // start animation
      self.smoothScrollActive = true;
      dt = getTimestamp();
      raf(doScrollAnimation);
    },
    stopSmoothScrolling: function() {
      this.smoothScrollActive = false;
    },
    triggerScrollEvent: function(scrollValue) {
      if (this.options.onScroll) {
        this.options.onScroll(scrollValue);
      }
    },
    getThickness: function() {
      return this.scrollbar[this.thicknessMeasureMethod]();
    },
    setSize: function(size) {
      // resize scrollbar
      var btnDecSize = this.btnDec[this.fullSizeProperty](),
        btnIncSize = this.btnInc[this.fullSizeProperty]();

      // resize slider
      this.currentSize = size;
      this.currentSliderSize = size - btnDecSize - btnIncSize;
      this.scrollbar.css(this.sizeProperty, size);
      this.slider.css(this.sizeProperty, this.currentSliderSize);
      this.currentSliderSize = this.slider[this.sizeProperty]();

      // resize handle
      this.handleSize = Math.round(this.currentSliderSize * this.ratio);
      this.handle.css(this.sizeProperty, this.handleSize);
      this.handleSize = this.handle[this.fullSizeProperty]();

      return this;
    },
    setRatio: function(ratio) {
      this.ratio = ratio;
      return this;
    },
    setMaxValue: function(maxValue) {
      this.maxValue = maxValue;
      this.setValue(Math.min(this.value, this.maxValue));
      return this;
    },
    setValue: function(value) {
      this.value = value;
      if (this.value < 0) {
        this.value = 0;
      } else if (this.value > this.maxValue) {
        this.value = this.maxValue;
      }
      this.refresh();
    },
    setPosition: function(styles) {
      this.scrollbar.css(styles);
      return this;
    },
    hide: function() {
      this.scrollbar.detach();
      return this;
    },
    show: function() {
      this.scrollbar.appendTo(this.holder);
      return this;
    },
    refresh: function() {
      // recalculate handle position
      if (this.value === 0 || this.maxValue === 0) {
        this.calcOffset = 0;
      } else {
        this.calcOffset = (this.value / this.maxValue) * (this.currentSliderSize - this.handleSize);
      }
      this.handle.css(this.offsetProperty, this.calcOffset);

      // toggle inactive classes
      this.btnDec.toggleClass(this.options.inactiveClass, this.value === 0);
      this.btnInc.toggleClass(this.options.inactiveClass, this.value === this.maxValue);
      this.scrollbar.toggleClass(this.options.inactiveClass, this.maxValue === 0);
    },
    destroy: function() {
      // remove event handlers and scrollbar block itself
      this.btnDec.add(this.btnInc).off('jcf-pointerdown', this.onButtonPress);
      this.handle.off('jcf-pointerdown', this.onHandlePress);
      this.doc.off('jcf-pointermove', this.onHandleDrag);
      this.doc.off('jcf-pointerup', this.onHandleRelease);
      this.doc.off('jcf-pointerup', this.onButtonRelease);
      this.stopSmoothScrolling();
      this.stopPageScrolling();
      this.scrollbar.remove();
    }
  });

  return module;
});

}(jcf));



/*! Hammer.JS - v2.0.8 - 2016-04-23
 * http://hammerjs.github.io/
 *
 * Copyright (c) 2016 Jorik Tangelder;
 * Licensed under the MIT license */
!function(a,b,c,d){"use strict";function e(a,b,c){return setTimeout(j(a,c),b)}function f(a,b,c){return Array.isArray(a)?(g(a,c[b],c),!0):!1}function g(a,b,c){var e;if(a)if(a.forEach)a.forEach(b,c);else if(a.length!==d)for(e=0;e<a.length;)b.call(c,a[e],e,a),e++;else for(e in a)a.hasOwnProperty(e)&&b.call(c,a[e],e,a)}function h(b,c,d){var e="DEPRECATED METHOD: "+c+"\n"+d+" AT \n";return function(){var c=new Error("get-stack-trace"),d=c&&c.stack?c.stack.replace(/^[^\(]+?[\n$]/gm,"").replace(/^\s+at\s+/gm,"").replace(/^Object.<anonymous>\s*\(/gm,"{anonymous}()@"):"Unknown Stack Trace",f=a.console&&(a.console.warn||a.console.log);return f&&f.call(a.console,e,d),b.apply(this,arguments)}}function i(a,b,c){var d,e=b.prototype;d=a.prototype=Object.create(e),d.constructor=a,d._super=e,c&&la(d,c)}function j(a,b){return function(){return a.apply(b,arguments)}}function k(a,b){return typeof a==oa?a.apply(b?b[0]||d:d,b):a}function l(a,b){return a===d?b:a}function m(a,b,c){g(q(b),function(b){a.addEventListener(b,c,!1)})}function n(a,b,c){g(q(b),function(b){a.removeEventListener(b,c,!1)})}function o(a,b){for(;a;){if(a==b)return!0;a=a.parentNode}return!1}function p(a,b){return a.indexOf(b)>-1}function q(a){return a.trim().split(/\s+/g)}function r(a,b,c){if(a.indexOf&&!c)return a.indexOf(b);for(var d=0;d<a.length;){if(c&&a[d][c]==b||!c&&a[d]===b)return d;d++}return-1}function s(a){return Array.prototype.slice.call(a,0)}function t(a,b,c){for(var d=[],e=[],f=0;f<a.length;){var g=b?a[f][b]:a[f];r(e,g)<0&&d.push(a[f]),e[f]=g,f++}return c&&(d=b?d.sort(function(a,c){return a[b]>c[b]}):d.sort()),d}function u(a,b){for(var c,e,f=b[0].toUpperCase()+b.slice(1),g=0;g<ma.length;){if(c=ma[g],e=c?c+f:b,e in a)return e;g++}return d}function v(){return ua++}function w(b){var c=b.ownerDocument||b;return c.defaultView||c.parentWindow||a}function x(a,b){var c=this;this.manager=a,this.callback=b,this.element=a.element,this.target=a.options.inputTarget,this.domHandler=function(b){k(a.options.enable,[a])&&c.handler(b)},this.init()}function y(a){var b,c=a.options.inputClass;return new(b=c?c:xa?M:ya?P:wa?R:L)(a,z)}function z(a,b,c){var d=c.pointers.length,e=c.changedPointers.length,f=b&Ea&&d-e===0,g=b&(Ga|Ha)&&d-e===0;c.isFirst=!!f,c.isFinal=!!g,f&&(a.session={}),c.eventType=b,A(a,c),a.emit("hammer.input",c),a.recognize(c),a.session.prevInput=c}function A(a,b){var c=a.session,d=b.pointers,e=d.length;c.firstInput||(c.firstInput=D(b)),e>1&&!c.firstMultiple?c.firstMultiple=D(b):1===e&&(c.firstMultiple=!1);var f=c.firstInput,g=c.firstMultiple,h=g?g.center:f.center,i=b.center=E(d);b.timeStamp=ra(),b.deltaTime=b.timeStamp-f.timeStamp,b.angle=I(h,i),b.distance=H(h,i),B(c,b),b.offsetDirection=G(b.deltaX,b.deltaY);var j=F(b.deltaTime,b.deltaX,b.deltaY);b.overallVelocityX=j.x,b.overallVelocityY=j.y,b.overallVelocity=qa(j.x)>qa(j.y)?j.x:j.y,b.scale=g?K(g.pointers,d):1,b.rotation=g?J(g.pointers,d):0,b.maxPointers=c.prevInput?b.pointers.length>c.prevInput.maxPointers?b.pointers.length:c.prevInput.maxPointers:b.pointers.length,C(c,b);var k=a.element;o(b.srcEvent.target,k)&&(k=b.srcEvent.target),b.target=k}function B(a,b){var c=b.center,d=a.offsetDelta||{},e=a.prevDelta||{},f=a.prevInput||{};b.eventType!==Ea&&f.eventType!==Ga||(e=a.prevDelta={x:f.deltaX||0,y:f.deltaY||0},d=a.offsetDelta={x:c.x,y:c.y}),b.deltaX=e.x+(c.x-d.x),b.deltaY=e.y+(c.y-d.y)}function C(a,b){var c,e,f,g,h=a.lastInterval||b,i=b.timeStamp-h.timeStamp;if(b.eventType!=Ha&&(i>Da||h.velocity===d)){var j=b.deltaX-h.deltaX,k=b.deltaY-h.deltaY,l=F(i,j,k);e=l.x,f=l.y,c=qa(l.x)>qa(l.y)?l.x:l.y,g=G(j,k),a.lastInterval=b}else c=h.velocity,e=h.velocityX,f=h.velocityY,g=h.direction;b.velocity=c,b.velocityX=e,b.velocityY=f,b.direction=g}function D(a){for(var b=[],c=0;c<a.pointers.length;)b[c]={clientX:pa(a.pointers[c].clientX),clientY:pa(a.pointers[c].clientY)},c++;return{timeStamp:ra(),pointers:b,center:E(b),deltaX:a.deltaX,deltaY:a.deltaY}}function E(a){var b=a.length;if(1===b)return{x:pa(a[0].clientX),y:pa(a[0].clientY)};for(var c=0,d=0,e=0;b>e;)c+=a[e].clientX,d+=a[e].clientY,e++;return{x:pa(c/b),y:pa(d/b)}}function F(a,b,c){return{x:b/a||0,y:c/a||0}}function G(a,b){return a===b?Ia:qa(a)>=qa(b)?0>a?Ja:Ka:0>b?La:Ma}function H(a,b,c){c||(c=Qa);var d=b[c[0]]-a[c[0]],e=b[c[1]]-a[c[1]];return Math.sqrt(d*d+e*e)}function I(a,b,c){c||(c=Qa);var d=b[c[0]]-a[c[0]],e=b[c[1]]-a[c[1]];return 180*Math.atan2(e,d)/Math.PI}function J(a,b){return I(b[1],b[0],Ra)+I(a[1],a[0],Ra)}function K(a,b){return H(b[0],b[1],Ra)/H(a[0],a[1],Ra)}function L(){this.evEl=Ta,this.evWin=Ua,this.pressed=!1,x.apply(this,arguments)}function M(){this.evEl=Xa,this.evWin=Ya,x.apply(this,arguments),this.store=this.manager.session.pointerEvents=[]}function N(){this.evTarget=$a,this.evWin=_a,this.started=!1,x.apply(this,arguments)}function O(a,b){var c=s(a.touches),d=s(a.changedTouches);return b&(Ga|Ha)&&(c=t(c.concat(d),"identifier",!0)),[c,d]}function P(){this.evTarget=bb,this.targetIds={},x.apply(this,arguments)}function Q(a,b){var c=s(a.touches),d=this.targetIds;if(b&(Ea|Fa)&&1===c.length)return d[c[0].identifier]=!0,[c,c];var e,f,g=s(a.changedTouches),h=[],i=this.target;if(f=c.filter(function(a){return o(a.target,i)}),b===Ea)for(e=0;e<f.length;)d[f[e].identifier]=!0,e++;for(e=0;e<g.length;)d[g[e].identifier]&&h.push(g[e]),b&(Ga|Ha)&&delete d[g[e].identifier],e++;return h.length?[t(f.concat(h),"identifier",!0),h]:void 0}function R(){x.apply(this,arguments);var a=j(this.handler,this);this.touch=new P(this.manager,a),this.mouse=new L(this.manager,a),this.primaryTouch=null,this.lastTouches=[]}function S(a,b){a&Ea?(this.primaryTouch=b.changedPointers[0].identifier,T.call(this,b)):a&(Ga|Ha)&&T.call(this,b)}function T(a){var b=a.changedPointers[0];if(b.identifier===this.primaryTouch){var c={x:b.clientX,y:b.clientY};this.lastTouches.push(c);var d=this.lastTouches,e=function(){var a=d.indexOf(c);a>-1&&d.splice(a,1)};setTimeout(e,cb)}}function U(a){for(var b=a.srcEvent.clientX,c=a.srcEvent.clientY,d=0;d<this.lastTouches.length;d++){var e=this.lastTouches[d],f=Math.abs(b-e.x),g=Math.abs(c-e.y);if(db>=f&&db>=g)return!0}return!1}function V(a,b){this.manager=a,this.set(b)}function W(a){if(p(a,jb))return jb;var b=p(a,kb),c=p(a,lb);return b&&c?jb:b||c?b?kb:lb:p(a,ib)?ib:hb}function X(){if(!fb)return!1;var b={},c=a.CSS&&a.CSS.supports;return["auto","manipulation","pan-y","pan-x","pan-x pan-y","none"].forEach(function(d){b[d]=c?a.CSS.supports("touch-action",d):!0}),b}function Y(a){this.options=la({},this.defaults,a||{}),this.id=v(),this.manager=null,this.options.enable=l(this.options.enable,!0),this.state=nb,this.simultaneous={},this.requireFail=[]}function Z(a){return a&sb?"cancel":a&qb?"end":a&pb?"move":a&ob?"start":""}function $(a){return a==Ma?"down":a==La?"up":a==Ja?"left":a==Ka?"right":""}function _(a,b){var c=b.manager;return c?c.get(a):a}function aa(){Y.apply(this,arguments)}function ba(){aa.apply(this,arguments),this.pX=null,this.pY=null}function ca(){aa.apply(this,arguments)}function da(){Y.apply(this,arguments),this._timer=null,this._input=null}function ea(){aa.apply(this,arguments)}function fa(){aa.apply(this,arguments)}function ga(){Y.apply(this,arguments),this.pTime=!1,this.pCenter=!1,this._timer=null,this._input=null,this.count=0}function ha(a,b){return b=b||{},b.recognizers=l(b.recognizers,ha.defaults.preset),new ia(a,b)}function ia(a,b){this.options=la({},ha.defaults,b||{}),this.options.inputTarget=this.options.inputTarget||a,this.handlers={},this.session={},this.recognizers=[],this.oldCssProps={},this.element=a,this.input=y(this),this.touchAction=new V(this,this.options.touchAction),ja(this,!0),g(this.options.recognizers,function(a){var b=this.add(new a[0](a[1]));a[2]&&b.recognizeWith(a[2]),a[3]&&b.requireFailure(a[3])},this)}function ja(a,b){var c=a.element;if(c.style){var d;g(a.options.cssProps,function(e,f){d=u(c.style,f),b?(a.oldCssProps[d]=c.style[d],c.style[d]=e):c.style[d]=a.oldCssProps[d]||""}),b||(a.oldCssProps={})}}function ka(a,c){var d=b.createEvent("Event");d.initEvent(a,!0,!0),d.gesture=c,c.target.dispatchEvent(d)}var la,ma=["","webkit","Moz","MS","ms","o"],na=b.createElement("div"),oa="function",pa=Math.round,qa=Math.abs,ra=Date.now;la="function"!=typeof Object.assign?function(a){if(a===d||null===a)throw new TypeError("Cannot convert undefined or null to object");for(var b=Object(a),c=1;c<arguments.length;c++){var e=arguments[c];if(e!==d&&null!==e)for(var f in e)e.hasOwnProperty(f)&&(b[f]=e[f])}return b}:Object.assign;var sa=h(function(a,b,c){for(var e=Object.keys(b),f=0;f<e.length;)(!c||c&&a[e[f]]===d)&&(a[e[f]]=b[e[f]]),f++;return a},"extend","Use `assign`."),ta=h(function(a,b){return sa(a,b,!0)},"merge","Use `assign`."),ua=1,va=/mobile|tablet|ip(ad|hone|od)|android/i,wa="ontouchstart"in a,xa=u(a,"PointerEvent")!==d,ya=wa&&va.test(navigator.userAgent),za="touch",Aa="pen",Ba="mouse",Ca="kinect",Da=25,Ea=1,Fa=2,Ga=4,Ha=8,Ia=1,Ja=2,Ka=4,La=8,Ma=16,Na=Ja|Ka,Oa=La|Ma,Pa=Na|Oa,Qa=["x","y"],Ra=["clientX","clientY"];x.prototype={handler:function(){},init:function(){this.evEl&&m(this.element,this.evEl,this.domHandler),this.evTarget&&m(this.target,this.evTarget,this.domHandler),this.evWin&&m(w(this.element),this.evWin,this.domHandler)},destroy:function(){this.evEl&&n(this.element,this.evEl,this.domHandler),this.evTarget&&n(this.target,this.evTarget,this.domHandler),this.evWin&&n(w(this.element),this.evWin,this.domHandler)}};var Sa={mousedown:Ea,mousemove:Fa,mouseup:Ga},Ta="mousedown",Ua="mousemove mouseup";i(L,x,{handler:function(a){var b=Sa[a.type];b&Ea&&0===a.button&&(this.pressed=!0),b&Fa&&1!==a.which&&(b=Ga),this.pressed&&(b&Ga&&(this.pressed=!1),this.callback(this.manager,b,{pointers:[a],changedPointers:[a],pointerType:Ba,srcEvent:a}))}});var Va={pointerdown:Ea,pointermove:Fa,pointerup:Ga,pointercancel:Ha,pointerout:Ha},Wa={2:za,3:Aa,4:Ba,5:Ca},Xa="pointerdown",Ya="pointermove pointerup pointercancel";a.MSPointerEvent&&!a.PointerEvent&&(Xa="MSPointerDown",Ya="MSPointerMove MSPointerUp MSPointerCancel"),i(M,x,{handler:function(a){var b=this.store,c=!1,d=a.type.toLowerCase().replace("ms",""),e=Va[d],f=Wa[a.pointerType]||a.pointerType,g=f==za,h=r(b,a.pointerId,"pointerId");e&Ea&&(0===a.button||g)?0>h&&(b.push(a),h=b.length-1):e&(Ga|Ha)&&(c=!0),0>h||(b[h]=a,this.callback(this.manager,e,{pointers:b,changedPointers:[a],pointerType:f,srcEvent:a}),c&&b.splice(h,1))}});var Za={touchstart:Ea,touchmove:Fa,touchend:Ga,touchcancel:Ha},$a="touchstart",_a="touchstart touchmove touchend touchcancel";i(N,x,{handler:function(a){var b=Za[a.type];if(b===Ea&&(this.started=!0),this.started){var c=O.call(this,a,b);b&(Ga|Ha)&&c[0].length-c[1].length===0&&(this.started=!1),this.callback(this.manager,b,{pointers:c[0],changedPointers:c[1],pointerType:za,srcEvent:a})}}});var ab={touchstart:Ea,touchmove:Fa,touchend:Ga,touchcancel:Ha},bb="touchstart touchmove touchend touchcancel";i(P,x,{handler:function(a){var b=ab[a.type],c=Q.call(this,a,b);c&&this.callback(this.manager,b,{pointers:c[0],changedPointers:c[1],pointerType:za,srcEvent:a})}});var cb=2500,db=25;i(R,x,{handler:function(a,b,c){var d=c.pointerType==za,e=c.pointerType==Ba;if(!(e&&c.sourceCapabilities&&c.sourceCapabilities.firesTouchEvents)){if(d)S.call(this,b,c);else if(e&&U.call(this,c))return;this.callback(a,b,c)}},destroy:function(){this.touch.destroy(),this.mouse.destroy()}});var eb=u(na.style,"touchAction"),fb=eb!==d,gb="compute",hb="auto",ib="manipulation",jb="none",kb="pan-x",lb="pan-y",mb=X();V.prototype={set:function(a){a==gb&&(a=this.compute()),fb&&this.manager.element.style&&mb[a]&&(this.manager.element.style[eb]=a),this.actions=a.toLowerCase().trim()},update:function(){this.set(this.manager.options.touchAction)},compute:function(){var a=[];return g(this.manager.recognizers,function(b){k(b.options.enable,[b])&&(a=a.concat(b.getTouchAction()))}),W(a.join(" "))},preventDefaults:function(a){var b=a.srcEvent,c=a.offsetDirection;if(this.manager.session.prevented)return void b.preventDefault();var d=this.actions,e=p(d,jb)&&!mb[jb],f=p(d,lb)&&!mb[lb],g=p(d,kb)&&!mb[kb];if(e){var h=1===a.pointers.length,i=a.distance<2,j=a.deltaTime<250;if(h&&i&&j)return}return g&&f?void 0:e||f&&c&Na||g&&c&Oa?this.preventSrc(b):void 0},preventSrc:function(a){this.manager.session.prevented=!0,a.preventDefault()}};var nb=1,ob=2,pb=4,qb=8,rb=qb,sb=16,tb=32;Y.prototype={defaults:{},set:function(a){return la(this.options,a),this.manager&&this.manager.touchAction.update(),this},recognizeWith:function(a){if(f(a,"recognizeWith",this))return this;var b=this.simultaneous;return a=_(a,this),b[a.id]||(b[a.id]=a,a.recognizeWith(this)),this},dropRecognizeWith:function(a){return f(a,"dropRecognizeWith",this)?this:(a=_(a,this),delete this.simultaneous[a.id],this)},requireFailure:function(a){if(f(a,"requireFailure",this))return this;var b=this.requireFail;return a=_(a,this),-1===r(b,a)&&(b.push(a),a.requireFailure(this)),this},dropRequireFailure:function(a){if(f(a,"dropRequireFailure",this))return this;a=_(a,this);var b=r(this.requireFail,a);return b>-1&&this.requireFail.splice(b,1),this},hasRequireFailures:function(){return this.requireFail.length>0},canRecognizeWith:function(a){return!!this.simultaneous[a.id]},emit:function(a){function b(b){c.manager.emit(b,a)}var c=this,d=this.state;qb>d&&b(c.options.event+Z(d)),b(c.options.event),a.additionalEvent&&b(a.additionalEvent),d>=qb&&b(c.options.event+Z(d))},tryEmit:function(a){return this.canEmit()?this.emit(a):void(this.state=tb)},canEmit:function(){for(var a=0;a<this.requireFail.length;){if(!(this.requireFail[a].state&(tb|nb)))return!1;a++}return!0},recognize:function(a){var b=la({},a);return k(this.options.enable,[this,b])?(this.state&(rb|sb|tb)&&(this.state=nb),this.state=this.process(b),void(this.state&(ob|pb|qb|sb)&&this.tryEmit(b))):(this.reset(),void(this.state=tb))},process:function(a){},getTouchAction:function(){},reset:function(){}},i(aa,Y,{defaults:{pointers:1},attrTest:function(a){var b=this.options.pointers;return 0===b||a.pointers.length===b},process:function(a){var b=this.state,c=a.eventType,d=b&(ob|pb),e=this.attrTest(a);return d&&(c&Ha||!e)?b|sb:d||e?c&Ga?b|qb:b&ob?b|pb:ob:tb}}),i(ba,aa,{defaults:{event:"pan",threshold:10,pointers:1,direction:Pa},getTouchAction:function(){var a=this.options.direction,b=[];return a&Na&&b.push(lb),a&Oa&&b.push(kb),b},directionTest:function(a){var b=this.options,c=!0,d=a.distance,e=a.direction,f=a.deltaX,g=a.deltaY;return e&b.direction||(b.direction&Na?(e=0===f?Ia:0>f?Ja:Ka,c=f!=this.pX,d=Math.abs(a.deltaX)):(e=0===g?Ia:0>g?La:Ma,c=g!=this.pY,d=Math.abs(a.deltaY))),a.direction=e,c&&d>b.threshold&&e&b.direction},attrTest:function(a){return aa.prototype.attrTest.call(this,a)&&(this.state&ob||!(this.state&ob)&&this.directionTest(a))},emit:function(a){this.pX=a.deltaX,this.pY=a.deltaY;var b=$(a.direction);b&&(a.additionalEvent=this.options.event+b),this._super.emit.call(this,a)}}),i(ca,aa,{defaults:{event:"pinch",threshold:0,pointers:2},getTouchAction:function(){return[jb]},attrTest:function(a){return this._super.attrTest.call(this,a)&&(Math.abs(a.scale-1)>this.options.threshold||this.state&ob)},emit:function(a){if(1!==a.scale){var b=a.scale<1?"in":"out";a.additionalEvent=this.options.event+b}this._super.emit.call(this,a)}}),i(da,Y,{defaults:{event:"press",pointers:1,time:251,threshold:9},getTouchAction:function(){return[hb]},process:function(a){var b=this.options,c=a.pointers.length===b.pointers,d=a.distance<b.threshold,f=a.deltaTime>b.time;if(this._input=a,!d||!c||a.eventType&(Ga|Ha)&&!f)this.reset();else if(a.eventType&Ea)this.reset(),this._timer=e(function(){this.state=rb,this.tryEmit()},b.time,this);else if(a.eventType&Ga)return rb;return tb},reset:function(){clearTimeout(this._timer)},emit:function(a){this.state===rb&&(a&&a.eventType&Ga?this.manager.emit(this.options.event+"up",a):(this._input.timeStamp=ra(),this.manager.emit(this.options.event,this._input)))}}),i(ea,aa,{defaults:{event:"rotate",threshold:0,pointers:2},getTouchAction:function(){return[jb]},attrTest:function(a){return this._super.attrTest.call(this,a)&&(Math.abs(a.rotation)>this.options.threshold||this.state&ob)}}),i(fa,aa,{defaults:{event:"swipe",threshold:10,velocity:.3,direction:Na|Oa,pointers:1},getTouchAction:function(){return ba.prototype.getTouchAction.call(this)},attrTest:function(a){var b,c=this.options.direction;return c&(Na|Oa)?b=a.overallVelocity:c&Na?b=a.overallVelocityX:c&Oa&&(b=a.overallVelocityY),this._super.attrTest.call(this,a)&&c&a.offsetDirection&&a.distance>this.options.threshold&&a.maxPointers==this.options.pointers&&qa(b)>this.options.velocity&&a.eventType&Ga},emit:function(a){var b=$(a.offsetDirection);b&&this.manager.emit(this.options.event+b,a),this.manager.emit(this.options.event,a)}}),i(ga,Y,{defaults:{event:"tap",pointers:1,taps:1,interval:300,time:250,threshold:9,posThreshold:10},getTouchAction:function(){return[ib]},process:function(a){var b=this.options,c=a.pointers.length===b.pointers,d=a.distance<b.threshold,f=a.deltaTime<b.time;if(this.reset(),a.eventType&Ea&&0===this.count)return this.failTimeout();if(d&&f&&c){if(a.eventType!=Ga)return this.failTimeout();var g=this.pTime?a.timeStamp-this.pTime<b.interval:!0,h=!this.pCenter||H(this.pCenter,a.center)<b.posThreshold;this.pTime=a.timeStamp,this.pCenter=a.center,h&&g?this.count+=1:this.count=1,this._input=a;var i=this.count%b.taps;if(0===i)return this.hasRequireFailures()?(this._timer=e(function(){this.state=rb,this.tryEmit()},b.interval,this),ob):rb}return tb},failTimeout:function(){return this._timer=e(function(){this.state=tb},this.options.interval,this),tb},reset:function(){clearTimeout(this._timer)},emit:function(){this.state==rb&&(this._input.tapCount=this.count,this.manager.emit(this.options.event,this._input))}}),ha.VERSION="2.0.8",ha.defaults={domEvents:!1,touchAction:gb,enable:!0,inputTarget:null,inputClass:null,preset:[[ea,{enable:!1}],[ca,{enable:!1},["rotate"]],[fa,{direction:Na}],[ba,{direction:Na},["swipe"]],[ga],[ga,{event:"doubletap",taps:2},["tap"]],[da]],cssProps:{userSelect:"none",touchSelect:"none",touchCallout:"none",contentZooming:"none",userDrag:"none",tapHighlightColor:"rgba(0,0,0,0)"}};var ub=1,vb=2;ia.prototype={set:function(a){return la(this.options,a),a.touchAction&&this.touchAction.update(),a.inputTarget&&(this.input.destroy(),this.input.target=a.inputTarget,this.input.init()),this},stop:function(a){this.session.stopped=a?vb:ub},recognize:function(a){var b=this.session;if(!b.stopped){this.touchAction.preventDefaults(a);var c,d=this.recognizers,e=b.curRecognizer;(!e||e&&e.state&rb)&&(e=b.curRecognizer=null);for(var f=0;f<d.length;)c=d[f],b.stopped===vb||e&&c!=e&&!c.canRecognizeWith(e)?c.reset():c.recognize(a),!e&&c.state&(ob|pb|qb)&&(e=b.curRecognizer=c),f++}},get:function(a){if(a instanceof Y)return a;for(var b=this.recognizers,c=0;c<b.length;c++)if(b[c].options.event==a)return b[c];return null},add:function(a){if(f(a,"add",this))return this;var b=this.get(a.options.event);return b&&this.remove(b),this.recognizers.push(a),a.manager=this,this.touchAction.update(),a},remove:function(a){if(f(a,"remove",this))return this;if(a=this.get(a)){var b=this.recognizers,c=r(b,a);-1!==c&&(b.splice(c,1),this.touchAction.update())}return this},on:function(a,b){if(a!==d&&b!==d){var c=this.handlers;return g(q(a),function(a){c[a]=c[a]||[],c[a].push(b)}),this}},off:function(a,b){if(a!==d){var c=this.handlers;return g(q(a),function(a){b?c[a]&&c[a].splice(r(c[a],b),1):delete c[a]}),this}},emit:function(a,b){this.options.domEvents&&ka(a,b);var c=this.handlers[a]&&this.handlers[a].slice();if(c&&c.length){b.type=a,b.preventDefault=function(){b.srcEvent.preventDefault()};for(var d=0;d<c.length;)c[d](b),d++}},destroy:function(){this.element&&ja(this,!1),this.handlers={},this.session={},this.input.destroy(),this.element=null}},la(ha,{INPUT_START:Ea,INPUT_MOVE:Fa,INPUT_END:Ga,INPUT_CANCEL:Ha,STATE_POSSIBLE:nb,STATE_BEGAN:ob,STATE_CHANGED:pb,STATE_ENDED:qb,STATE_RECOGNIZED:rb,STATE_CANCELLED:sb,STATE_FAILED:tb,DIRECTION_NONE:Ia,DIRECTION_LEFT:Ja,DIRECTION_RIGHT:Ka,DIRECTION_UP:La,DIRECTION_DOWN:Ma,DIRECTION_HORIZONTAL:Na,DIRECTION_VERTICAL:Oa,DIRECTION_ALL:Pa,Manager:ia,Input:x,TouchAction:V,TouchInput:P,MouseInput:L,PointerEventInput:M,TouchMouseInput:R,SingleTouchInput:N,Recognizer:Y,AttrRecognizer:aa,Tap:ga,Pan:ba,Swipe:fa,Pinch:ca,Rotate:ea,Press:da,on:m,off:n,each:g,merge:ta,extend:sa,assign:la,inherit:i,bindFn:j,prefixed:u});var wb="undefined"!=typeof a?a:"undefined"!=typeof self?self:{};wb.Hammer=ha,"function"==typeof define&&define.amd?define(function(){return ha}):"undefined"!=typeof module&&module.exports?module.exports=ha:a[c]=ha}(window,document,"Hammer");
//# sourceMappingURL=hammer.min.js.map



function clsScorm2004API(url, RcoId) {
    this._url = url;
    this._finished = false;
    this._intialized = false;
    this._autoCommit = true;
    this._errorNumber = 0;
    this._errorText = "";
    this._haveState = false;
    this._elements = null;
    this._dirtyElements = {};
    this._commLog = "";
    this.Initialize = function (sVal) {
        this._SetError(0, "");
        if (this._intialized) {
            this._SetError(101, "Initialize has already been called.");
            return "false";
        }
        if (this._finished) {
            this._SetError(104, "Finish has already been called.");
            return "false";
        }
        this._intialized = true;
        return "true";
    };
    this.Terminate = function (sVal) {
        this._SetError(0, "");
        if (!this._CheckOpen() || this.Commit("") == "false") return "false";        
        this._SendRequest("PUT","/terminate");
        this._finished = true;
        if (this.GetLastError() != 0) return "false";
        return "true";
    };
    this.Commit = function (sVal, async) {//OK                
        this._SetError(0, "");
        if (!this._CheckOpen()) return "false";

        for (var sKey in this._dirtyElements)
        {
            if (sKey == "cmi.suspend_data") this._SendRequest("PUT", "/suspenddata", this._dirtyElements[sKey], async, sKey);
            else if(sKey=="cmi.completion_status") this._SendRequest("PUT","/status/"+this._dirtyElements[sKey], null, async, sKey);
            else if(sKey=="cmi.score.raw") this._SendRequest("PUT","/score/"+this._dirtyElements[sKey], null, async, sKey);            
        }        
        if (this.GetLastError() != 0) return "false";        
        return "true";
    };
    this.MetierKeepAlive = function()
    {        
        if (!this._CheckOpen() || !this.GetValue("cmi.suspend_data")) return null;        
        return this._SendRequest("PUT", "/suspenddata", this.GetValue("cmi.suspend_data"), true, "cmi.suspend_data");                
    }
    this.GetDiagnostic = function (iCode) { return this.GetErrorString(iCode) + (this._errorText == "" ? "" : "\n" + this._errorText); }; //OK

    this.GetErrorString = function (iCode) { //OK
        if (iCode == 0 || iCode == "") return "No error";
        // General Errors 100-199
        if (iCode == 101) return "General exception";
        if (iCode == 102) return "General Initialization Failure";
        if (iCode == 103) return "Already Initialized";
        if (iCode == 104) return "Content Instance Terminated";
        if (iCode == 111) return "General Termination Failure";
        if (iCode == 112) return "Termination Before Initialization";
        if (iCode == 113) return "Termination After Termination";
        if (iCode == 122) return "Retrieve Data Before Initialization";
        if (iCode == 123) return "Retrieve Data After Termination";
        if (iCode == 132) return "Store Data Before Initialization";
        if (iCode == 133) return "Store Data After Termination";
        if (iCode == 142) return "Commit Before Initialization";
        if (iCode == 143) return "Commit After Termination";
        // Syntax Errors 200-299
        if (iCode == 201) return "General Argument Error";
        if (iCode == 301) return "General Get Failure";
        if (iCode == 351) return "General Set Failure";
        if (iCode == 391) return "General Commit Failure";
        // Data Model Errors 400-499
        if (iCode == 401) return "Undefined Data Model Element";
        if (iCode == 402) return "Unimplemented Data Model Element";
        if (iCode == 403) return "Data Model Element Value Not Initialized";
        if (iCode == 404) return "Data Model Element Is Read Only";
        if (iCode == 405) return "Data Model Element Is Write Only";
        if (iCode == 406) return "Data Model Element Type Mismatch";
        if (iCode == 407) return "Data Model Element Value Out Of Range";
        if (iCode == 408) return "Data Model Dependency Not Established";
        // Implementation-defined Errors 1000-65535
        if (iCode == 1000) return "General communication failure (Ajax)";
        return "General exception";
    };
    this.GetLastError = function () { return this._errorNumber; }; //OK
    this.SetValue = function (sKey, sValue) {//OK        
        this._SetError(0, "");
        //Auto-initialize if this has not been done
        if (!this._intialized) this.Initialize("");
        if (!this._CheckOpen()) return "false";

        if (sValue == null) sValue = "";
        if (sKey == "cmi.completion_status" || sKey == "cmi.core.lesson_status") sValue = replaceAll(sValue, "passed", "P", "failed", "F", "completed", "C", "incomplete", "I", "browsed", "B", "not attempted", "N");
        if (sValue == this._GetElements()[sKey]) return "true";
        console.log("Scorm.SetValue (old vs new)", this._GetElements()[sKey], sValue )
        this._GetElements()[sKey] = sValue;
        this._dirtyElements[sKey] = sValue;
        if (this.GetAutoCommit()) return this.Commit("", true);
        return "true";
    };
    this.GetValue = function (sKey) { //OK
        this._SetError(0, "");
        //Auto-initialize if this has not been done
        if (!this._intialized) this.Initialize("");
        if (!this._CheckOpen()) return "";
        var actualKey=sKey;
        if (sKey.indexOf("metier.") > -1) sKey = sKey.split("metier.")[1];
        var val = this._GetElements()[sKey];
        if (actualKey.indexOf("metier.") > -1) return val;
        if (sKey == "cmi.completion_status" || sKey == "cmi.core.lesson_status") val = replaceAll(val, "I", "incomplete", "N", "not attempted", "C", "completed", "P", "passed", "F", "failed", "B", "browsed", "unknown", "incomplete");
        return val;
    };
    this.SetAutoCommit = function (bValue) {
        this._SetError(0, "");
        if (!this._CheckOpen()) return "false";
        this._autoCommit = bValue;
    };
    this.GetAutoCommit = function () {
        this._SetError(0, "");
        if (!this._CheckOpen()) return "false";
        return this._autoCommit;
    };

    this._CheckOpen = function () {
        if (!this._intialized) {
            this._SetError(103, "");
            return false;
        }
        if (this._finished) {
            this._SetError(104, "Finish has already been called.");
            return false;
        }
        return true;
    }
    this._SetError = function (iCode, sDesc) {
        this._errorNumber = iCode;
        this._errorText = sDesc;
        if (iCode == 303) {
            fnSendEmail("phoenix@metier.zendesk.com", "debug@mymetier.net", "SCORM Error: " + iCode + " " + sDesc, false, "URL: " + window.location.href + "\nLog:\n" + this._commLog);
            if (!this._alertDate || (new Date()-this._alertDate) / 1000 > 2) 
            {
              if (cStr(sDesc).indexOf("403 - Forbidden: Access is denied") > -1) sDesc = "Your session has timed out!";
              alert("Your data could not be saved.\n\nThe error returned from the server is:\n\n" + sDesc + "\n\nPlease try clicking 'Save & Close' in the upper right corner, and re-opening the course player again.");
            }

            this._alertDate=new Date();                
            //}
        }
    };    
    this._GetElements = function () {
        if (this._elements == null) {            
            var o = this._SendRequest("GET").responseJSON;
            this._elements = {                
                "cmi.suspend_data":o.SuspendData,
                "cmi.completion_status": o.Status,
                "cmi.score.raw": o.Score
            };
            //if(o.SessionId)$this.ScormSessionId=o.SessionId;
        }
        return this._elements;
    }
    this._RunningRequests={};
    this._SendRequest = function (method, url, data, async, key) {
        var $this=this;
        var requestKey = JSON.stringify({m:method, u:url, d: data});
        var dirtyElementValue = $this._dirtyElements[key];

        if(key && $this._RunningRequests[requestKey]) return $this._RunningRequests[requestKey];

        if(url == "/suspenddata" && data)data = JSON.stringify(data);
        $this._commLog += "\n" + (new Date()).toString() + " REQUEST: " + method + " / " + $this.ScormSessionId + cStr(url);
        
        var baseUrl = $this._url;
        var fnHandleSuccess=function(data, status, xhr, key)
        {

          if(key && dirtyElementValue == $this._dirtyElements[key]) delete $this._dirtyElements[key];
          delete $this._RunningRequests[requestKey];
          
          console.log("Response:",data);
          $this._commLog += "\n" + "RESPONSE: " + status  + ": " + xhr.responseText;
          
          return xhr;
        }
        
        console.log("Request:", method + " " + url + " /// " + data);
        var opts={ contentType:"application/json",dataType: 'json', cache: false, type: method, data: data, async: (async?true:false)};
        var request = $.ajax(baseUrl + "/" + $this.ScormSessionId + cStr(url), opts)
          .done(function(data, status, xhr)
          {
            return fnHandleSuccess(data,status, xhr, key );
            
          }).fail(function(xhr, status, err)
          {
            console.log("Retrying request ", baseUrl + "/" + $this.ScormSessionId + cStr(url))
            return $.ajax(baseUrl + "/" + $this.ScormSessionId + cStr(url), opts)
              .done(function(data, status, xhr)
              {
                return fnHandleSuccess(data, status, xhr, key );
                
              }).fail(function(xhr, status, err)
              {
                //Force log-out
                if(xhr.status == 401){
                  alert("Your data could not be saved. Please try starting the lesson playback again. You are now being redirected.");
                  window.location.href="/";
                }       
                $this._SetError(303, xhr.responseText);
                if(key)
                  delete $this._RunningRequests[requestKey];                        
              });

          });
        if(key && async) $this._RunningRequests[requestKey] = request;
       
        return request;
    }
}

function clsScorm2004Local(url, RcoId) {
    this.RcoId = RcoId;
    this._UseLocalStorage = typeof (Storage) !== "undefined";
    this.Initialize = function (sVal) { return "true"; }
    this.Terminate = function (sVal) { return "true"; }
    this.Commit = function (sVal) { return "true"; }
    this.GetDiagnostic = function (iCode) { return "No error"; }
    this.GetErrorString = function (iCode) { return "No error"; }
    this.GetLastError = function () { return 0; };
    this.SetValue = function (sKey, sValue) {
        sKey=this.RcoId + "-" + sKey;
        if(this._UseLocalStorage)
            localStorage.setItem(sKey, sValue);
        else 
            $.cookie(sKey, sValue, { expires: 365 });
        console.log("Writing to " + sKey + ":" + sValue);
        return "true";
    }
    this.GetValue = function (sKey) {
        sKey=this.RcoId + "-" + sKey;
        var val=(this._UseLocalStorage ? localStorage.getItem(sKey) : $.cookie(sKey));        
        return val;
    }
    this.SetAutoCommit = function (bValue) { return true; };
    this.GetAutoCommit = function () { return true; };
}

String.prototype.trim = function () { return this.replace(/^\s+|\s+$/g, "");}
String.prototype.ltrim = function () {return this.replace(/^\s+/, "");}
String.prototype.rtrim = function () {return this.replace(/\s+$/, "");}
String.prototype.lpad = function (p, l) { var s = this; while (s.length < l) s = p + s; return s;}
String.prototype.rpad = function (p, l) { var s = this; while (s.length < l) s = s + p; return s;}
String.prototype.replaceAt = function (i, c) { return this.substr(0, i) + c + this.substr(i + c.length); }
String.prototype.safeSplit = function (s, i) { var t = cStr(this).split(s); return cStr(t[i < 0 ? t.length + i : i]); }

function cStr() {
    for (var x = 0; x < cStr.arguments.length; x++)
        if (cStr.arguments[x]!=null&&("" + cStr.arguments[x]) != "") return ("" + cStr.arguments[x]);
            
    return "";
}

function cDbl() {
    for (var x = 0; x < cDbl.arguments.length; x++) {
        var num=cDbl.arguments[x];
        if (num) {
            var num = cStr(num).replace(/\$|\,/g, '');
            if (!isNaN(num) && Number(num) != 0) return Number(num);                
        }
    }    
    return 0;
}

function cInt() {
    for (var x = 0; x < cInt.arguments.length; x++) {
        var n = cDbl(cInt.arguments[x]);
        if (n != 0) return Number((n+"").safeSplit(".",0).safeSplit(",",0));                    
    }
    return 0;
}
function replaceAll() {
    var s = cStr(replaceAll.arguments[0]);
    for (var x = 1; x < replaceAll.arguments.length; x = x + 2) {
        a = cStr(replaceAll.arguments[x]);
        b = cStr(replaceAll.arguments[x + 1]);        
        s = s.split(a).join(b);
    }
    return s;
}

 function fnGetNearestWord(y) {
    var StartX = $(".theory.container").offset().left;
    var Width = $(".theory.container").width();
    var checker = function (y) {
        for (var x = StartX; x < StartX + Width; x = x + 10) {
            var oElement = document.elementFromPoint(x, y);
            if (oElement && $(oElement).parents(".prevent-hl").length==0) return oElement;
        }
    };
    var Element = null;
    var InitialY = y;
    while (y < InitialY + 400 && (!Element || $(Element).parents(".prevent-hl").length>0)) {
        Element = checker(y);
        y = y + 15;
    }
    return Element;
}
function fnSendEmail(sTo, sFrom, sSubject, bHTML, sBody) {
    $.ajax({
        url: "../tools/sendmail.aspx",
        async: false,
        type: "POST",
        data: { to: sTo, from: sFrom, subject: sSubject, html: (bHTML ? '1' : '0'), body: sBody }
    });
}
//Object.assign PolyFill for IE
if (typeof Object.assign != 'function') {
  (function () {
    Object.assign = function (target) {
      'use strict';
      // We must check against these specific cases.
      if (target === undefined || target === null) {
        throw new TypeError('Cannot convert undefined or null to object');
      }

      var output = Object(target);
      for (var index = 1; index < arguments.length; index++) {
        var source = arguments[index];
        if (source !== undefined && source !== null) {
          for (var nextKey in source) {
            if (source.hasOwnProperty(nextKey)) {
              output[nextKey] = source[nextKey];
            }
          }
        }
      }
      return output;
    };
  })();
}
//Promise PolyFill for IE - https://github.com/taylorhakes/promise-polyfill/blob/master/promise.min.js
!function(e){function n(){}function t(e,n){return function(){e.apply(n,arguments)}}function o(e){if("object"!=typeof this)throw new TypeError("Promises must be constructed via new");if("function"!=typeof e)throw new TypeError("not a function");this._state=0,this._handled=!1,this._value=void 0,this._deferreds=[],s(e,this)}function i(e,n){for(;3===e._state;)e=e._value;return 0===e._state?void e._deferreds.push(n):(e._handled=!0,void o._immediateFn(function(){var t=1===e._state?n.onFulfilled:n.onRejected;if(null===t)return void(1===e._state?r:u)(n.promise,e._value);var o;try{o=t(e._value)}catch(i){return void u(n.promise,i)}r(n.promise,o)}))}function r(e,n){try{if(n===e)throw new TypeError("A promise cannot be resolved with itself.");if(n&&("object"==typeof n||"function"==typeof n)){var i=n.then;if(n instanceof o)return e._state=3,e._value=n,void f(e);if("function"==typeof i)return void s(t(i,n),e)}e._state=1,e._value=n,f(e)}catch(r){u(e,r)}}function u(e,n){e._state=2,e._value=n,f(e)}function f(e){2===e._state&&0===e._deferreds.length&&o._immediateFn(function(){e._handled||o._unhandledRejectionFn(e._value)});for(var n=0,t=e._deferreds.length;n<t;n++)i(e,e._deferreds[n]);e._deferreds=null}function c(e,n,t){this.onFulfilled="function"==typeof e?e:null,this.onRejected="function"==typeof n?n:null,this.promise=t}function s(e,n){var t=!1;try{e(function(e){t||(t=!0,r(n,e))},function(e){t||(t=!0,u(n,e))})}catch(o){if(t)return;t=!0,u(n,o)}}var a=setTimeout;o.prototype["catch"]=function(e){return this.then(null,e)},o.prototype.then=function(e,t){var o=new this.constructor(n);return i(this,new c(e,t,o)),o},o.all=function(e){var n=Array.prototype.slice.call(e);return new o(function(e,t){function o(r,u){try{if(u&&("object"==typeof u||"function"==typeof u)){var f=u.then;if("function"==typeof f)return void f.call(u,function(e){o(r,e)},t)}n[r]=u,0===--i&&e(n)}catch(c){t(c)}}if(0===n.length)return e([]);for(var i=n.length,r=0;r<n.length;r++)o(r,n[r])})},o.resolve=function(e){return e&&"object"==typeof e&&e.constructor===o?e:new o(function(n){n(e)})},o.reject=function(e){return new o(function(n,t){t(e)})},o.race=function(e){return new o(function(n,t){for(var o=0,i=e.length;o<i;o++)e[o].then(n,t)})},o._immediateFn="function"==typeof setImmediate&&function(e){setImmediate(e)}||function(e){a(e,0)},o._unhandledRejectionFn=function(e){"undefined"!=typeof console&&console&&console.warn("Possible Unhandled Promise Rejection:",e)},o._setImmediateFn=function(e){o._immediateFn=e},o._setUnhandledRejectionFn=function(e){o._unhandledRejectionFn=e},"undefined"!=typeof module&&module.exports?module.exports=o:e.Promise||(e.Promise=o)}(this);
