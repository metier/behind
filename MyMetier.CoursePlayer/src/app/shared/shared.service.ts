import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {
    private Spinners=[];           
    //private _SpinChange = new Subject<boolean>();
    //public SpinChange = this._SpinChange.asObservable();

    static StaticInstance: SharedService;
    constructor() {
        //SharedService.StaticInstance = "Inside Contructor";
    }

    public static GetInstance(): SharedService {
        if(!SharedService.StaticInstance) SharedService.StaticInstance = new SharedService();
        return SharedService.StaticInstance;
    }

    public ClearSpinners(){
        this.Spinners=[];            
        console.log("Clear All Spinners: " + (new Date()));
        //this._SpinChange.next(this.IsSpinning());        
    }
	public ClearSpinner(Event){
    	if(this.Spinners.indexOf(Event)>-1)
    		this.Spinners.splice(this.Spinners.indexOf(Event),1);
    	console.log("ClearSpinner: " + Event + " - Spinners: " + this.Spinners + " " + (new Date()));
        //this._SpinChange.next(this.IsSpinning());    	
    }
    public AddSpinner(Event){    	
    	this.Spinners.push(Event);
        //this._SpinChange.next(this.IsSpinning());        
    	console.log("AddSpinner: " + Event + " " + (new Date()));    	
    }
    public IsSpinning(Event=null){
        if(Event) return this.Spinners.indexOf(Event)>-1;
    	return this.Spinners.length>0;
    }    
    public XmlToJson(xml):any {            
        // Create the return object
        var obj = {};

        if (xml.nodeType == 1) { // element
            // do attributes
            if (xml.attributes.length > 0) {
            //obj["@attributes"] = {};
                for (var j = 0; j < xml.attributes.length; j++) {
                    var attribute = xml.attributes.item(j);
                    //obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
                    obj["@" + attribute.nodeName] = attribute.nodeValue;
                }
            }
        } else if (xml.nodeType == 3) { // text
            obj = xml.nodeValue;
        }

        // do children
        if (xml.hasChildNodes()) {
            for(var i = 0; i < xml.childNodes.length; i++) {
                var item = xml.childNodes.item(i);
                var nodeName = item.nodeName;
                if (typeof(obj[nodeName]) == "undefined") {
                    obj[nodeName] = this.XmlToJson(item);
                } else {
                    if (typeof(obj[nodeName].push) == "undefined") {
                        var old = obj[nodeName];
                        obj[nodeName] = [];
                        obj[nodeName].push(old);
                    }
                    obj[nodeName].push(this.XmlToJson(item));
                }
            }
        }
        return obj;
    };    
    
}