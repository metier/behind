import { registerLocaleData } from "@angular/common";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import localeDa from "@angular/common/locales/da";
import localeDe from "@angular/common/locales/de";
import localeEn from "@angular/common/locales/en";
import localeEs from "@angular/common/locales/es";
import localeFr from "@angular/common/locales/fr";
import localeIt from "@angular/common/locales/it";
import localeNb from "@angular/common/locales/nb";
import localeNl from "@angular/common/locales/nl";
import localePl from "@angular/common/locales/pl";
import localePt from "@angular/common/locales/pt";
import localeSv from "@angular/common/locales/sv";
import { ErrorHandler, NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import {
  MissingTranslationHandler,
  TranslateLoader,
  TranslateModule
} from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { LocalStorageModule } from "angular-2-local-storage";
import { RoundProgressModule } from "angular-svg-round-progressbar";
import { Angulartics2Module } from "angulartics2";
import { NgxPageScrollCoreModule } from "ngx-page-scroll-core";
import { Autosize } from "../assets/ng-autosize/src/autosize.directive";
import { UiSortableComponent } from "../assets/ui-sortable/src/sortable";
import { AppComponent } from "./app.component";
import { AppLog } from "./app.log";
import { ROUTES } from "./app.routes";
import { MyErrorHandler } from "./error.handler";
import { MyMissingTranslationHandler } from "./missingtranslation.handler";
import {
  HourMinuteSecondsPipe,
  MinuteSecondsPipe,
  SafePipe,
  OrderByPipe,
} from "./pipes.modules";
import {
  CaseComponent,
  ChecklistComponent,
  CompleteComponent,
  FinaltestComponent,
  GoalsComponent,
  PedagogicalComponent,
  PitfallsComponent,
  PlayerComponent,
  PlayerNavigationComponent,
  QuizComponent,
  ReflectionComponent,
  SplashComponent,
  TheoryComponent
} from "./player";
import {
  ActivityDetailsComponent,
  AttachmentComponent,
  EcoachComponent, ElearningAttachmentsComponent, ElearningContactPersonComponent, InstructorComponent,
  PortalBaseExam2Component,
  PortalCatalogComponent,
  PortalClassroomComponent,
  PortalElearningComponent,
  PortalElearningDemoComponent,
  PortalExamComponent,
  PortalHomeComponent,
  PortalLoginComponent,
  PortalMetierplusComponent,
  PortalMockexamAttemptsComponent,
  PortalMockexamComponent,
  PortalMockexamConfirmComponent,
  PortalMockexamExamResultComponent,
  PortalMockexamQuestionsComponent,
  PortalMockexamScenarioComponent,
  PortalMockexamSummaryComponent,
  PortalPageComponent,
  PortalProfileComponent,
  PortalProfileComponentInner,
  PortalReportsComponent,
  PortalSeminarComponent,
  PortalWebinarComponent
} from "./portal";
import { CompletedCoursesComponent } from './portal/completed-courses/completed-courses.component';
import { UserDiplomasComponent } from "./portal/user-diplomas/user-diplomas.component";
import { LocalizedDatePipe } from "./shared/localized-date.pipe";
import { ObjectFilterPipe } from "./shared/objectfilter";
+registerLocaleData(localeFr);
registerLocaleData(localeDe);
registerLocaleData(localeDa);
registerLocaleData(localeNb);
registerLocaleData(localeEn);
registerLocaleData(localeSv);
registerLocaleData(localeEs);
registerLocaleData(localeIt);
registerLocaleData(localeNl);
registerLocaleData(localePl);
registerLocaleData(localePt);

// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}
@NgModule({
  declarations: [
    AppComponent,
    PlayerNavigationComponent,
    SplashComponent,
    GoalsComponent,
    ReflectionComponent,
    ElearningAttachmentsComponent,
    ElearningContactPersonComponent,
    TheoryComponent,
    PitfallsComponent,
    ChecklistComponent,
    CaseComponent,
    PlayerComponent,
    QuizComponent,
    FinaltestComponent,
    CompleteComponent,
    PedagogicalComponent,
    Autosize,
    SafePipe,
    OrderByPipe,
    HourMinuteSecondsPipe,
    MinuteSecondsPipe,
    UiSortableComponent,
    PortalPageComponent,
    PortalLoginComponent,
    PortalHomeComponent,
    PortalProfileComponent,
    PortalProfileComponentInner,
    PortalCatalogComponent,
    PortalElearningComponent,
    PortalElearningDemoComponent,
    PortalClassroomComponent,
    PortalMockexamComponent,
    PortalBaseExam2Component,
    PortalMockexamQuestionsComponent,
    PortalMockexamSummaryComponent,
    PortalMockexamScenarioComponent,
    PortalMockexamConfirmComponent,
    PortalMockexamExamResultComponent,
    PortalMockexamAttemptsComponent,
    PortalExamComponent,
    ActivityDetailsComponent,
    EcoachComponent,
    InstructorComponent,
    AttachmentComponent,
    PortalReportsComponent,
    PortalSeminarComponent,
    PortalWebinarComponent,
    PortalMetierplusComponent,
    ObjectFilterPipe,
    LocalizedDatePipe,
    CompletedCoursesComponent,
    UserDiplomasComponent
  ],
  imports: [
    RoundProgressModule,

    LocalStorageModule.forRoot({
      prefix: "mymetier",
      storageType: "localStorage",
    }),
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, {
      scrollPositionRestoration: "enabled",
      useHash: true,
    }),
    TranslateModule.forRoot({
      missingTranslationHandler: {
        provide: MissingTranslationHandler,
        useClass: MyMissingTranslationHandler,
      },
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
    FormsModule,
    Angulartics2Module.forRoot(),
    NgxPageScrollCoreModule.forRoot({ duration: 1000 }),
  ],
  bootstrap: [AppComponent],
  exports: [LocalizedDatePipe],
  providers: [AppLog, { provide: ErrorHandler, useClass: MyErrorHandler }],
})
export class AppModule { }
