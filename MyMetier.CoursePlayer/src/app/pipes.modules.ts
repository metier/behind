import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { orderBy } from 'lodash';


@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
	constructor(private sanitizer: DomSanitizer) { }

	transform(style) {
		return this.sanitizer.bypassSecurityTrustHtml(style);
	}
}

@Pipe({
	name: 'minuteSeconds'
})
export class MinuteSecondsPipe implements PipeTransform {
	transform(value: number): string {
		if (isNaN(value)) return "";
		var negative = value < 0;
		value = Math.abs(value);
		const minutes: number = Math.floor(value / 60);
		return (negative ? "-" : "") + ("0" + minutes).slice(-2) + ':' +
			("0" + (value - minutes * 60)).slice(-2);
	}
}

@Pipe({
	name: 'hourMinuteSeconds'
})
export class HourMinuteSecondsPipe implements PipeTransform {
	transform(value: number): string {
		if (isNaN(value)) return "";
		var negative = value < 0;
		value = Math.abs(value);
		const hours: number = Math.floor(value / (60 * 60));
		value -= (hours * 60 * 60);
		const minutes: number = Math.floor(value / 60);
		return (negative ? "-" : "") + (hours > 9 ? hours : ("0" + hours).slice(-2)) + ':' +
			("0" + minutes).slice(-2) + ':' +
			("0" + (value - minutes * 60)).slice(-2);
	}
}

@Pipe({
	name: "orderBy"
})
export class OrderByPipe implements PipeTransform {
	transform(array: any, sortBy: string, order?: any): any[] {
		
		const sortOrder = order ? order : "asc"; // setting default ascending order

		return orderBy(array, [sortBy], [sortOrder]);
	}
}