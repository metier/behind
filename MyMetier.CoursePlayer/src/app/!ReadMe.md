Todo:
* minify scorm data
x * Sluttest
x	* why do i get Expression has changed after it was checked. Previous value: in final test. this is preventing a new attempt from starting
x	--> try creating quiz as a component that can have a method called from FinalTest (ie. submit -- and this will perform scoring synchronously)
x	* paginate finaltest
* Consider making GoToUrl not block
? * expire cache when save/preview in CMS
x * IE kurslasting
x * dynamically generate header links
x * hide header links when final test
x * IE: custom cursor , color selector checkmark
x * print
x * vis fasit for "dra og slipp"
x * start new scorm session when navigating to a new lesson
x * spinner ved save and close
x * why can't i get spinner to run when performing synchronous scorm operations
x * sette score i quiz
x * set lesson as in progress if not attempted
x * unchecking m/c
x * ipad selecting text. add button "accept selection"
x * quiz - drag-drop
x * prevent highlight in notes, reflection answers. can we somehow exclude these from selection?
x * header - functions
x * spinner
x * kontakt
x * complete: email
x *Load/save/init scorm
x * image resize+auto-caption
x * nav: selected item should be green
x * notes
x * Country/language dropdowns
x * contact
x * Save user
x * scorm.Win.LessonData.EcoachEmail, scorm.Win.LessonData.DistributorEmail
x NO: When navigating away from quiz, need to re-calc score 
x * Diploma
x * RSS feed sample: https://www.becompany.ch/en/blog/2016/09/19/angular2-rss-reader (will need to convert XML to JSON)
x * Percentage indicator elearning
x * Have return to Evry Academy on login page based on cookie from last logged in session
x * Lage felt i Phoenix: "Kundeportalnavn" (f.eks. Evry Academy vs. Every AS)
x * Add fnMetierPlayRCO as a function in course details window (Old course player)
x * Mockexam
x * Phoenix Exam
x * Password reset
x * CMS navigation for CMS-only users - select count(*) from t_permissions where UserID=@user_id", "@user_id", User.ID
x * Login from customer portals
x * To get leading texts for enrollment: GET customers/{id} - https://mymetier.net/api/Help/Api/GET-customers-id 
x * Have Back to Home when on subpage
x * Case exam
x * PRINCE2 Exam Switcher
x * Ecoach/instructor image
x * Last flere kurs
x * warning for IE 10 and older
x * every time visiting home, get user (to check session)
x * fjern "kurs du kan være interessert i"
x * bruk distributør email fallback på ecoach. bruk distributør email som instruktørepost
x * fjern telefon for begge
x * kart virker ikke
x * Translations
x * consider adding shim for older IE support? https://github.com/zloirock/core-js#basic
x * TermsAndConditions on enrollment
x * Change validation on Accept terms on enrollment
x * Not validating mandatory custom fields on enrollment
x * Cookies
x * Course catalog doesnt display after login
x * sarawak skal ha egen logo -- 10493
x * PSO redirect on logout
x * sjekk hvordan org 4533 ser ut (skal ikke ha katalog)
x * Skema skal ikke ha diplom
x * Forsvaret egen case innleveringstekst
? * email ecoaches of all acitvities user is enrolled about missing work experience -- this should happen in Phoenix!!

   
Later:
* Competency form -- ref Espen error
x * Mock exam -- progress/update 
* Course full warning on enrollment
* Handle accommmodations in course calendar (<item name="price-accommodation-extra">* Dagpakkekostnad kommer i tillegg til kursavgiften</item>)
* Unenroll (kun for eksamener)
* Competency form: collapse on settings
* When printing highlights, they shift
* In view mode in final test, we shouldn't have mouse arrow. Trying to drag and drop gives a weird exception
* Generate roundhouse checksums for 2 Up scripts

Go-live:
x * Ecoach/instructor image must be uploaded to the resource
x * Ta bort medalje på diplom


TextHighlighter: http://mir3z.github.io/texthighlighter/demos/simple.html
https://github.com/timdown/rangy

//"rangy": "^1.3.0", -- We use this one instead: https://github.com/Cavva79/rangy until author can merge changes into main repo

Add this line to package.json for angular2-autosize:
"typings": "angular2-autosize.ts",

Service URL: http://phoenix1.vontangen.com/learningportal/api/player/5
Package for prod: https://github.com/angular/angular-cli/issues/755 AND http://stackoverflow.com/questions/37631098/angular-2-how-to-bundle-for-production

C:\proj\Clients\metier\Phoenix\lp\MyMetier.CoursePlayer\npm start (no longer needed: --aot)
To build for deployment: node_modules\.bin\ng build --prod --bh ./

http://phoenix1.vontangen.com/learningportal/player2/#/course/151223/lesson/151222

node_modules\.bin\ng2-translate-extract --dir ./src --output ./ --format=json --clean

Vi bruker ikke denne:
* Create translations: node_modules\.bin\ng-xi18n -p src\tsconfig.json
* ng build --aot
* node_modules\.bin\ng build --prod --i18n-file=src/messages.no.xlf --locale=no --i18n-format=xlf --aot

npm outdated
npm install
npm update 