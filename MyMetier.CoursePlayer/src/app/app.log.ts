import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class AppLog {
    constructor (private _httpClient: HttpClient) {}

    logMessageAsJson(message: string, type: string) {
        let headers = new HttpHeaders({ "Content-Type": "application/json" })
        let jsonMessage = {"type": type, "message": message}
        console.log("Error:",type, message);
        return this._httpClient.post(JSON.stringify(jsonMessage), headers)
    }
    logMessageAsEmail(message: string, details: string) {
        let headers = new HttpHeaders({ "Content-Type": "application/x-www-form-urlencoded" })
        
        let body = new URLSearchParams();
        body.set("from","player2-error.noreply@metier.no");
        body.set("to","error@bids.no");
    	body.set("subject",message + " - " + window.location.href);
    	body.set("body",details);
        console.log("Error:",message, details);
        return this._httpClient.post("../Tools/sendmail.aspx",body.toString(), { headers : headers })
    }
    logMessageToConsole(message) {        
        console.log("Error:", message);        
    }
}