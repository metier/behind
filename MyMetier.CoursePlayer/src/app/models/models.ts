export interface ParticipantsData {
    Id: number;
    CustomerId:	number;
    UserId:	number;
    ActivitySetId: number;
    ActivityId: number;
    ActivityOwnerCustomerId: number;
    ActivityOwnerCustomerName: string;
    CustomerArticleId: number;
    CustomerArticleTitle: number;
    ArticleId: number;
    OrderId: number;
    FirstName: string;
    LastName: string;
    CustomerName: string;
    ArticlePath: string;
    ArticleType: string;
    ArticleTypeId: number;
    UnitPrice: number;
    CurrencyCode: string;
    ActivitySetName: string;
    ActivityName: string;
    IsInvoiced: boolean;
    OrderNumber: number;
    InvoiceNumber: number;
    ParticipantStatusCodeValue: string;
    ParticipantCompletedDate: string;
    OrderStatus: unknown;
    MomentTimezoneName: string;
    HideActivityDates: boolean;
    ActivityStart: string;
    ActivityEnd: string;
    RcoCourseId: number;
    RcoExamContainerId: number | null;
    LanguageId: number;
    ProductDescriptionId: number;
    IsUserCompetenceInfoRequired: boolean;
    EnrollmentId: number;
    EnrolledActivitySetId: number;
    EnrolledActivitySetName: string;
    RcoLessonCount: number;
    RcoLessonCompletedCount: number;
    RcoExamCount: number;
    RcoExamCompletedCount: number;
    ParticipantInfoElements: unknown;
}

export interface ActivitySet {
    Activities: Activity[];​
    ActivitySetId: string;
    Completed: boolean;
}

export interface Activity {
    ActivityEnd: string;
    ActivityId: number;
    ActivityName: string;
    ActivityOwnerCustomerId: number;
    ActivityOwnerCustomerName: string;
    ActivitySetId: number;
    ActivitySetName: string;
    ActivityStart: string;
    ArticleId: number;
    ArticlePath: string;
    ArticleType: string;
    ArticleTypeId: number;
    CurrencyCode: string;
    CustomerArticleId: number;
    CustomerArticleTitle: string;
    CustomerId: number;
    CustomerName: string;
    EnrolledActivitySetId: number;
    EnrolledActivitySetName: string;
    EnrollmentId: number;
    FirstName: string;
    HideActivityDates: boolean;
    Id: number;
    InvoiceNumber: any;
    IsInvoiced: boolean;
    IsUserCompetenceInfoRequired: boolean;
    LanguageId: number;
    LastName: string;
    MomentTimezoneName: string;
    OrderId: any; 
    OrderNumber: any;
    OrderStatus: any;
    ParticipantCompletedDate: string;
    ParticipantInfoElements: any;
}