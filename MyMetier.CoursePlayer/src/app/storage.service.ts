import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private CacheRules = { "PerformanceData": 10, "ParticipationData": 10, "CustomSettings": 30, "CourseCalendar": 10, "SubscriptionData": 30 , "UserDiplomasData": 30};

  constructor(private localStorageService: LocalStorageService, private router: Router) { }

  public getStorageData(key: string, fnOnStale: any = null): unknown {
		const data = this.localStorageService.get(key);
		const cacheTime = this.CacheRules[key];
		if (fnOnStale != null && (!data || cacheTime > 0)) {
			//Auto-renew old cached data
			if (this.isDemo()) return { length: 0 };
			var cacheDate: any = this.localStorageService.get(key + "-Date");
			if (!data || !cacheDate || new Date(cacheDate) < new Date((new Date().getTime()) - cacheTime * 60000)) {
				fnOnStale();
			}
		}
		return data;
	} 

	public setStorageData(key: string, data: any, updateCacheDate: boolean = true): void {
		const cacheTime = this.CacheRules[key];
		this.localStorageService.set(key, data);
		if (cacheTime > 0 && updateCacheDate)
			this.localStorageService.set(key + "-Date", new Date());
	}

  public removeStorageData(...keys: string[]): void {
      keys.forEach(res => this.localStorageService.remove(res));
  }

  private isDemo(): boolean {
    return this.router.url.indexOf("/demo/") > -1;
  }
}