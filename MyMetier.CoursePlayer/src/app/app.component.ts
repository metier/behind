import { Component, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { Angulartics2GoogleTagManager } from "angulartics2/gtm";
import { PlayerService } from "./player.service";
@Component({
    selector: 'app-root',
    templateUrl: './template.player2.html',
    styleUrls: ['../assets/player/css/player2.css', '../assets/portal/css/main.css', '../assets/exam2/css/main.css'],
    encapsulation: ViewEncapsulation.None,
    providers: [PlayerService]
  })
  export class AppComponent {
    title = 'app works!';
    constructor(translate: TranslateService, PlayerService:PlayerService, public router: Router, angulartics2GoogleTagManager: Angulartics2GoogleTagManager) {
        PlayerService.TranslateService=translate;
        angulartics2GoogleTagManager.startTracking();
      }
    
      public IsNotPrintView() {
        return !(window.location.href.indexOf('PrintRequest') >= 0); 
      }
  }
  