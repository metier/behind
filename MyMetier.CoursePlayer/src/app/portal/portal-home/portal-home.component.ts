import { Component } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { Activity, ActivitySet, ParticipantsData } from "app/models/models";
import * as _ from "lodash";
import { orderBy } from "lodash";
import { ActivityDetailsComponent } from "../activity-details/activity-details.component";
import { BasePortalPageComponent } from "../base-portal-page.component";
import { PortalService } from "../portal.service";

@Component({
	selector: 'portal-home',
	templateUrl: './home.component.html',
	providers: [PortalService]
})
export class PortalHomeComponent extends BasePortalPageComponent {
	public ActivitySetFilter: any = { Completed: false };
	public participationData: ActivitySet[];
	public CoursesOfInterestToYou = [
		//{Name:"PRINCE2® Foundation",Description:"Podcasting operational change management inside of workflows to establish a framework.",Image:"assets/portal/images/img11.jpg",Url:""},
		//{Name:"PRINCE2® Foundation",Description:"Podcasting operational change management inside of workflows to establish a framework.",Image:"assets/portal/images/img11.jpg",Url:""}
	];

	constructor(private portalService: PortalService, public Router: Router, private ActivatedRoute: ActivatedRoute) {
		super(portalService, Router);
		PortalService.HideWrapper = false;
		this.participationData = this.portalService.ParticipationData;
		this.sortActivitiesByDate();
		this.portalService.GetAccount().subscribe(res => { });

	}

	private sortActivitiesByDate(): void {
		this.participationData.forEach(d => {
			d.Activities.sort((a, b) => new Date(a.ActivityStart).getTime() - new Date(b.ActivityStart).getTime());
		});
	};

	public GetCompletedActivities(Activities) {
		return _.filter(Activities, { ParticipantStatusCodeValue: "COMPLETED" });
	}

	public GetActivityLanguage(Activity) {
		return _.find(this.PortalService.CodeData['Languages'], { Id: Activity.LanguageId }) || {};
	}

	public GetElearningProgress(Activity) {
		if (Activity.ParticipantStatusCodeValue == 'COMPLETED') return 100;
		if (Activity.RcoLessonCount == 0) return 0;
		return Math.round(100 * Activity.RcoLessonCompletedCount / Activity.RcoLessonCount);
	}

	public LoadMoreCoursesProsjektledelse() {
		if (this.PortalService.GetSettingValue("HideCatalog")) return;
		return (this.PortalService.UserData.User.Customer.IsArbitrary && this.PortalService.UserData.User.Customer.DistributorId == 1194);
	}
}