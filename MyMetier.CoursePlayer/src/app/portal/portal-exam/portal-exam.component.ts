import { Component, Input } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { PlayerService } from '../../player.service';
import { PortalBaseCourseComponent } from "../base-course.component";
import { PortalService } from "../portal.service";

@Component({
	selector: 'portal-exam',
	templateUrl: './exam.component.html',
	providers: [PortalService]
})
export class PortalExamComponent extends PortalBaseCourseComponent {
	public FileUploadProgress: number = 0;
	public FileToUpload: any;
	public CaseFileTitle = "";
	public Participation: any;

	@Input() set content(content) {
		const $this = this;

	}
	constructor(service: PortalService, Route: ActivatedRoute, Router: Router, PlayerService: PlayerService, TranslateService: TranslateService) {
		super(service, Route, Router, PlayerService, TranslateService);
		const $this = this;
		$this.service.UploadProgress$.subscribe(
			data => {
				$this.FileUploadProgress = data;
			});
	}

	public IsExamCompetenceMissing() {
		return this.service.NagUserForExamCompetence() && this.ActivityParticipation.ParticipantStatusCodeValue == "ENROLLED" && this.ActivityParticipation.IsUserCompetenceInfoRequired;
	}

	public IsMPCExam() {
		return this.ActivityParticipation.ArticleType == "Multiple Choice" || this.ActivityParticipation.ArticleType == "Internal Certification";
	}

	public IsCaseExam() {
		return this.ActivityParticipation.ArticleType == "Case Exam";
	}

	public IsProjectAssignment() {
		return this.ActivityParticipation.ArticleType == "Project Assignment";
	}

	public IsExternalCertification() {
		return this.ActivityParticipation.ArticleType == "External Certification";
	}

	public CanPlayMPCExam() {
		if (this.IsMPCExam() && this.ActivityParticipation.ParticipantStatusCodeValue != "COMPLETED" && this.ParticipantAvailability.IsAvailable && new Date(this.ParticipantAvailability.AvailabilityStart) < new Date())
			return true;
	}

	public AvailabilityDays() {
		if (this.ParticipantAvailability.AvailabilityEnd && this.ParticipantAvailability.AvailabilityStart)
			return Math.round(Math.abs((new Date(this.ParticipantAvailability.AvailabilityEnd).getTime() - new Date(this.ParticipantAvailability.AvailabilityStart).getTime()) / (24 * 60 * 60 * 1000)));
		return 0;
	}

	public CaseFileSelected(Event) {
		this.FileToUpload = (Event.target || Event.srcElement).files[0];
	}

	public UploadCaseExam() {
		var $this = this;
		$this.FileUploadProgress = 1;
		console.log('UploadCaseExam');

		$this.service.UploadFile(this.FileToUpload, this.CaseFileTitle, null).subscribe(file => {
			$this.FileUploadProgress = 100;
			$this.service.SubmitCaseExam($this.ActivityParticipation.Id, file).subscribe(res => {
				console.log(res);
				$this.Participation = null;
			});
			console.log('sent');
			$this.FileToUpload = null;
		});
	}

	public GetParticipation() {
		var $this = this;
		if ($this.Participation) return this.Participation;
		if ($this.ActivityParticipation.Id) {
			$this.Participation = { Attachments: [] };
			$this.service.GetParticipation($this.ActivityParticipation.Id).subscribe(res => {
				$this.Participation = res;
			});
		}
		return { Attachments: [] };
	}
}