import { Component, Input } from '@angular/core';
import { PortalService } from "../../portal.service";

@Component({
  selector: 'elearning-contactperson',
  templateUrl: './elearning-contactpersons.component.html',
  styleUrls: ['./elearning-contactpersons.component.scss']
})
export class ElearningContactPersonComponent {

  @Input() ContactPersons = [];
	@Input() Activity = {};
	@Input() Distributor = {};

  public contactPersonIMG; 

	constructor(private portalService: PortalService) {}

  public openMailClient(mailAdress: string): void {
    window.location.href = `mailto:${mailAdress}`;
  }

  public getFileUrl(attachment: any) {
    return this.portalService.GetFileUrl(attachment);
  }
}
  