import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElearningContactPersonComponent } from './elearning-contactpersons.component';

describe('ElearningContactPersonComponent', () => {
  let component: ElearningContactPersonComponent;
  let fixture: ComponentFixture<ElearningContactPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElearningContactPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElearningContactPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
