import { Component, Input } from '@angular/core';
import { PortalService } from "../../portal.service";

@Component({
  selector: 'elearning-attachments',
  templateUrl: './elearning-attachments.component.html',
  styleUrls: ['./elearning-attachments.component.scss']
})
export class ElearningAttachmentsComponent {

  @Input() Attachments = [];
	@Input() Activity = {};

	constructor(private portalService: PortalService) { }

  public openDownloadUrl(attachment: any): void {
    window.location.href = this.portalService.GetFileUrl(attachment);
  }
}
