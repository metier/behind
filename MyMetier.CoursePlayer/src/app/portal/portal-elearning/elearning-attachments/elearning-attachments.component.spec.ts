import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElearningAttachmentsComponent } from './elearning-attachments.component';

describe('ElearningAttachmentsComponent', () => {
  let component: ElearningAttachmentsComponent;
  let fixture: ComponentFixture<ElearningAttachmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElearningAttachmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElearningAttachmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
