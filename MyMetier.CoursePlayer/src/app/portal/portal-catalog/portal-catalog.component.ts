import { AfterViewInit, Component, Input, ViewChild } from "@angular/core";
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as _ from "lodash";
import { BasePortalPageComponent } from "../base-portal-page.component";
import { PortalProfileComponentInner } from "../portal-profile-inner/portal-profile-inner.component";
import { PortalService } from "../portal.service";

declare const $: any;
@Component({
	selector: 'portal-catalog',
	templateUrl: './catalog.component.html',
	providers: [PortalService]
})
export class PortalCatalogComponent extends BasePortalPageComponent implements AfterViewInit {
	@ViewChild(PortalProfileComponentInner)
	private profileComponentInner: PortalProfileComponentInner;

	public CoursePrograms = [];
	public CourseProgramIndex=0;
	public ActiveCourseProgramSteps=[];
	public ActiveCourseModule=null;
	private ProductDescriptions={};
	public ActivitySetToEnroll=null;

	public ValidationError=null;
	public InformationAlert=null;
	public Terms="";
	public Price="";
	public IsTermsAccepted=false;
	public ActivitySetIsFull=false;
	public ActivitySetOutsideEnrollmentPeriod=false;
	public EnrollmentSuccess=false;
	public TermsAcceptanceValidationError=false;
	@Input() set content(content) {
		const $this = this;
	}

	constructor(service: PortalService, public Router: Router, public TranslateService: TranslateService) {
		super(service, Router);
		const $this = this;
		$this.LoadCourseProgram();

		const terms = this.PortalService.UserData.User.Customer.ParticipantInfoDefinition.TermsAndConditionsText;
		if (terms && terms.length > 50) this.Terms = terms;
	}

	public SetActiveCourseProgram(Index) {
		var $this = this;
		if ($this.CoursePrograms.length <= Index) return;
		$this.CourseProgramIndex = Index;
		$this.ActiveCourseProgramSteps = $this.CoursePrograms[Index].Steps;
	}

	public SetActiveCourseModule(Module) {
		var $this = this;
		$this.ActiveCourseModule = Module;
		if (!Module.ProductDescription) {
			Module.ProductDescription = {};
			var activities = _.filter(Module.ActivitySets[0].Activities, function (a) { return a.CustomerArticle.ProductDescriptionId > 0 });
			if (activities.length > 0) {
				var id = _.sortBy(activities, [function (o) { return o.CustomerArticle.ArticleTypeId; }])[0].CustomerArticle.ProductDescriptionId;
				if (!$this.ProductDescriptions[id]) {
					$this.ProductDescriptions[id] = {};
					$this.PortalService.GetProductDescription(id).subscribe(res => {
						$.extend(true, $this.ProductDescriptions[id], res);
					});
				}
				Module.ProductDescription = $this.ProductDescriptions[id];
			}
		}
	}

	public GetActiveCourseModuleDescription() {
		var $this = this;
		if (!$this.ActiveCourseModule || !$this.ActiveCourseModule.ProductDescription.ProductDescriptionTexts) return;
		var desc = _.filter($this.ActiveCourseModule.ProductDescription.ProductDescriptionTexts, function (o) { return o.ProductTemplatePart.Order == 1 && o.Text; });
		if (desc.length > 0)
			return _.sortBy(desc, [function (o: any) { return o.ProductTemplatePart.LanguageId == $this.PortalService.UserData.User.PreferredLanguageId ? 0 : 1 }])[0].Text;
	}

	public GetActivityLanguage(Activity) {
		return _.find(this.PortalService.CodeData['Languages'], { Id: Activity.CustomerArticle.LanguageId }) || {};
	}

	public ShowEnrollDialog(ActivitySet) {
		if (this.ActivitySetToEnroll != ActivitySet) {
			this.ActivitySetToEnroll = ActivitySet;
			this.ActivitySetIsFull = false;
			this.ActivitySetOutsideEnrollmentPeriod = (ActivitySet.EnrollmentFrom && new Date(ActivitySet.EnrollmentFrom) > new Date()) || (ActivitySet.EnrollmentTo && new Date(ActivitySet.EnrollmentTo) < new Date());
		}

		this.ValidationError = null;
		this.InformationAlert = null;
		this.EnrollmentSuccess = false;
		this.TermsAcceptanceValidationError = false;
		this.Price = "";

		if (this.PortalService.UserData.User.Customer.ParticipantInfoDefinition.IsShowPricesToUserOnPortal
			&& this.PortalService.CalculateActivitySetPrice(ActivitySet) > 0)
			this.Price = this.PortalService.CalculateActivitySetPrice(ActivitySet) + " " + this.PortalService.GetActivitySetCurrencyCode(ActivitySet);
	}

	public PerformEnrollment() {
		var $this = this;
		if (this.Terms && !this.IsTermsAccepted) {
			$this.TermsAcceptanceValidationError = true;
			return;
		}
		$this.TermsAcceptanceValidationError = false;

		this.profileComponentInner.SaveUser()
			.then(res => {
				if (res == "success") {
					//Perform Enrollment
					this.PortalService.EnrollInActivitySet(this.ActivitySetToEnroll.Id).then(function () {
						$this.EnrollmentSuccess = true;
						$this.LoadCourseProgram();
					}).catch(function (res) {
						$this.TranslateService.get('CATALOG.ENROLLMENT_FAILED').subscribe(trans => {
							$this.profileComponentInner.AddValidationError(trans + " " + res, true);
						});
					});
				}
			});
	}

	public GetResourceByType(Activity, Type: number) {
		if (Activity == null || !Activity.Resources) return [];
		return _.filter(Activity.Resources, { ResourceTypeId: Type });
	}

	private LoadCourseProgram() {
		var $this = this;
		$this.PortalService.GetCoursePrograms().subscribe(res => {
			$this.CoursePrograms = res;
			var calendar = $this.PortalService.GetCourseCalendar().slice(0);
			_.each(res, function (program: any) {
				_.each(program.Steps, function (step: any) {
					_.each(step.Modules, function (module: any) {
						module.ActivitySets = _.filter(calendar, function (o): any {
							return _.find(o.Activities, { CustomerArticle: { ModuleId: module.Id } });
						});
					});
					_.remove(step.Modules, function (o: any) { return o.ActivitySets.length == 0; });

					_.each(step.Exams, function (exam: any) {
						exam.ActivitySets = _.filter(calendar, function (o): any {
							return _.find(o.Activities, { CustomerArticle: { ExamId: exam.Id } });
						});
					});
					_.remove(step.Exams, function (o: any) { return o.ActivitySets.length == 0; });

				});
				_.remove(program.Steps, function (o: any) { return o.Modules.length == 0 && o.Exams.length == 0; });
			});
			_.remove(res, function (o: any) { return o.Steps.length == 0; });

			var activitySetsInPyramid = _.flatMap(res, function (o) {
				return _.flatMap(_.flatMap(o.Steps, function (step) {
					return [_.flatMap(step.Modules, function (module: any) { return module.ActivitySets; }), _.flatMap(step.Exams, function (exam: any) { return exam.ActivitySets; })]
				}));
			});
			_.pullAll(calendar, activitySetsInPyramid);

			if (calendar.length > 0) {
				var modules = _.groupBy(calendar, function (o) {
					return o.Activities[0].CustomerArticle.ModuleId;
				});
				var program = {
					Name: "Other",
					Steps: []
				};

				var exams = [], courses = [];
				_.mapKeys(modules, function (o, k): any {
					if (k == "null") courses.push({ Name: "Other", ActivitySets: o });
					else if (o[0].Activities[0].CustomerArticle.ExamId > 0) exams.push({ Name: o[0].Activities[0].CustomerArticle.Module.Name, ActivitySets: o });
					else courses.push({ Name: o[0].Activities[0].CustomerArticle.Module.Name, ActivitySets: o });

				});

				if (courses.length > 0) program.Steps.push({ Name: "Courses", Modules: courses });
				if (exams.length > 0) program.Steps.push({ Name: "Exams", Exams: exams });
				res.push(program);
			}
			$this.SetActiveCourseProgram(0);
			$this.ActiveCourseModule = null;
		});
	}
}