import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { PlayerService } from "app/player.service";
import { BasePortalPageComponent } from "../base-portal-page.component";
import { PortalService } from "../portal.service";
import { version } from "../../../../package.json";

declare const $: any;

@Component({
  selector: "portal-page",
  templateUrl: "./portal-page.html",
  providers: [PortalService],
})
export class PortalPageComponent extends BasePortalPageComponent {
  public ValidationError: string = "";
  public PageClass: string = "";
  public ContactMessage = null;
  public FromEmail = null;
  public MessageSent: boolean = false;
  public version: string = version;
  public currentYear: number = new Date().getFullYear();

  constructor(
    private router: Router,
    private playerService: PlayerService,
    private portalService: PortalService
  ) {
    super(portalService, router);
    var $this = this;
    router.events.subscribe((url: any) => {
      portalService.Init();
      $this.PageClass = $this.router.url.split("/").join(" ");
    });
  }

  public submitMessage(): void {
    var playerService = this.playerService;
    playerService.SendMessage({
      From: this.FromEmail || this.portalService.UserData.Email,
      Subject: "Question from user inside the LearningPortal",
      Body: this.ContactMessage,
    });
    this.FromEmail = null;
  }

  public shouldShowHeaderElements(): boolean {
    return (
      this.portalService.UserData.User.Id &&
      this.PageClass.indexOf("login") == -1 &&
      !this.portalService.IsDemo
    );
  }

  public displayAdminCmsLink(): boolean {
    return this.portalService.HasCMSAccess;
  }

  public displayAdminBehindLink(): boolean {
    return (
      this.portalService.UserData.Roles.indexOf("BehindAdmin") > -1 ||
      this.portalService.UserData.Roles.indexOf("BehindSuperAdmin") > -1
    );
  }

  public goToMymetierPlus(): void {
    //Append s=1 to force reload of route, if already on route. onSameUrlNavigation: 'reload' did not work
    //this.Router.navigateByUrl('/portal/plus?url=/' + (window.location.href.indexOf('&s=') == -1 ? '&s=1':''));
    this.router.navigateByUrl("/portal/plus?url=/");
    $("#metierPlusIFrame").attr("src", "https://plus.mymetier.net/");
  }
}
