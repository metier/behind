import { Component, Input } from "@angular/core";
import { PortalService } from "../portal.service";

@Component({
	selector: 'instructor',
	templateUrl: './instructor.component.html',
	providers: [PortalService]
})
export class InstructorComponent {
	@Input() Instructors = [];
	@Input() Activity = {};
	@Input() Distributor = {};

	constructor(public PortalService: PortalService) { }
}