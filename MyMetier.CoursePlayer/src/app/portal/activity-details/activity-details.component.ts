import { Component, Input } from "@angular/core";
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { PortalService } from "../portal.service";

declare var AngularViewInit: any;
declare var OpenBadges: any;

@Component({
	selector: 'activity-details',
	templateUrl: './activity-details.component.html',
	providers: [PortalService]
})
export class ActivityDetailsComponent {
	public PortalService: any;
	public UserActivityDetails: any = {};
	public IsChangingFoundationExam = false;
	public IsChangingPractitionerExam = false;
	public ActivityId;
	private _InternalActivity;
	@Input() public ParticipantAvailability: any = {};
	@Input() set Activity(Activity) {
		var $this = this;
		$this.ActivityId = Activity.Id;
		$this._InternalActivity = Activity;
		if ($this.ActivityId > 0) {
			$this.PortalService.GetActivityDetails($this.ActivityId).subscribe(res => {
				$this.UserActivityDetails = res;
				$this.UserActivityDetails.NewFoundationExamActivitysetId = $this.UserActivityDetails.FoundationExamActivitysetId;
				$this.UserActivityDetails.NewPractitionerExamActivitysetId = $this.UserActivityDetails.PractitionerExamActivitysetId;

				var content = (!!$this.UserActivityDetails.PageContent ? `<div>${$this.UserActivityDetails.PageContent}</div>` : ``);

				if (!!$this.Activity.EnrolledUserActivityInformation)
					content += (!!content ? `<br />` : ``) + `<div>${$this.Activity.EnrolledUserActivityInformation}</div>`;

				if (!!content)
					$this.UserActivityDetails.PageContent = $this.DomSanitizer.bypassSecurityTrustHtml(content);

				setTimeout(
					function () {
						AngularViewInit();
					}, 500);
			});
		}
	}

	constructor(protected service: PortalService, public Router: Router, private DomSanitizer: DomSanitizer) {

		var $this = this;
		$this.PortalService = service;
		service.Init();

	}

	get Activity() {
		return this._InternalActivity;
	}

	public IssueBadge() {
		OpenBadges.issue(['https://mymetier.net/learningportal/api/badge/award/activity/' + this.ActivityId + '/user/' + this.PortalService.UserData.User.Id], function (errors, successes) {
			if (errors && errors.length > 0)
				alert("Your badge could not be added. The reason reported by the badge service is: " + errors[0].reson)
		});
	}

	public Save(Type) {
		var $this = this;
		var toActivitySetId = (Type == "P" ? $this.UserActivityDetails.NewPractitionerExamActivitysetId : $this.UserActivityDetails.NewFoundationExamActivitysetId)
		$this.PortalService.SwitchPRINCE2ExamDate($this.ActivityId, toActivitySetId).then(res => {

			$this.UserActivityDetails = res;
			$this.UserActivityDetails.NewFoundationExamActivitysetId = $this.UserActivityDetails.FoundationExamActivitysetId;
			$this.UserActivityDetails.NewPractitionerExamActivitysetId = $this.UserActivityDetails.PractitionerExamActivitysetId;
			if ($this.UserActivityDetails.PageContent) $this.UserActivityDetails.PageContent = $this.DomSanitizer.bypassSecurityTrustHtml($this.UserActivityDetails.PageContent);

			$this.IsChangingFoundationExam = false;
			$this.IsChangingPractitionerExam = false;

			setTimeout(
				function () {
					AngularViewInit();
				}, 500);

			$this.PortalService.GotoUrl("/portal/exam/" + (Type == "F" ? res.FoundationExamActivityId : res.PractitionerExamActivityId), false);
		});
	}
}