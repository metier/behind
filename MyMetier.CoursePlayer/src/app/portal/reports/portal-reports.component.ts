import { Component } from "@angular/core";
import { Router } from '@angular/router';
import * as _ from "lodash";
import { BasePortalPageComponent } from "../base-portal-page.component";
import { PortalService } from "../portal.service";

@Component({
	selector: 'portal-reports',
	templateUrl: './reports.component.html',
	providers: [PortalService]
})
export class PortalReportsComponent extends BasePortalPageComponent {
	public ProductCodes = [];
	public ProductId = "";
	constructor(service: PortalService, public Router: Router) {
		super(service, Router);
		var $this = this;

		//Check session
		$this.PortalService.GetCoursePrograms().subscribe(res => {
			_.each(_.uniqBy(_.flatMapDeep(this.PortalService.CourseCalendarRaw, function (activitySet) {
				return _.flatMap(activitySet.Activities, function (activity) { return activity.CustomerArticle });
			}), "ProductId"), function (o) {
				$this.ProductCodes.push({ Id: o.ProductId, Name: (o.Module ? (o.Module.Description || o.Module.Name) : "") + (o.ArticlePath ? " (" + o.ArticlePath.split("-")[0] + ")" : "") });
			});
			_.each(res, function (program) {
				_.each(program.Steps, function (step) {
					_.each(step.Modules, function (module) {
						_.each(module.CustomerArticles, function (article) {
							if (!_.find($this.ProductCodes, { Id: article.ProductId }))
								$this.ProductCodes.push({ Id: article.ProductId, Name: (module.Description || module.Name) + (article.ArticlePath ? " (" + article.ArticlePath.split("-")[0] + ")" : "") });
						});
					});
					_.each(step.Exams, function (module) {
						_.each(module.CustomerArticles, function (article) {
							if (!_.find($this.ProductCodes, { Id: article.ProductId }))
								$this.ProductCodes.push({ Id: article.ProductId, Name: (module.Description || module.Name) + (article.ArticlePath ? " (" + article.ArticlePath.split("-")[0] + ")" : "") });
						});
					});
				});
			});
			$this.ProductCodes = $this.ProductCodes.sort(function (a, b) { return a.Name < b.Name ? -1 : (a.Name > b.Name ? 1 : 0); });
		});
	}
	
	public GenerateReport() {
		window.location.href = this.PortalService.PhoenixApiUrl + '/reports/progressstatus/xlsx?customerId=' + this.PortalService.GetCustomerId() + (this.ProductId ? '&productId=' + this.ProductId : '');
	}

}