import { Component, Input } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { PlayerService } from '../../player.service';
import { PortalBaseCourseComponent } from "../base-course.component";
import { PortalService } from "../portal.service";

@Component({
	selector: 'portal-classroom',
	templateUrl: './classroom.component.html',
	providers: [PortalService]
})
export class PortalClassroomComponent extends PortalBaseCourseComponent {
	@Input() set content(content) {
		const $this = this;

	}
	constructor(service: PortalService, Route: ActivatedRoute, Router: Router, PlayerService: PlayerService, TranslateService: TranslateService) {
		super(service, Route, Router, PlayerService, TranslateService);
	}
	hasAttachmentsOrContacts() {
		var r = ((this.Activity.Attachments && this.Activity.Attachments.length > 0) || (this.getCourseContactPersons() && this.getCourseContactPersons().length > 0));
		return r;
	}
}