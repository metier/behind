import { DOCUMENT } from '@angular/common';
import { Component, Inject, Input } from "@angular/core";
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as _ from "lodash";
import { PageScrollService } from 'ngx-page-scroll-core';
import { map } from 'rxjs/operators';
import { BasePortalPageComponent } from "../base-portal-page.component";
import { PortalService } from "../portal.service";


declare const $: any;
declare const AngularViewInit: any;

@Component({
	selector: 'portal-profile-inner',
	templateUrl: './profile.component.inner.html',
	styleUrls: ['../portal-profile/portal-profile.component.scss'],
	providers: [PortalService]
})
export class PortalProfileComponentInner extends BasePortalPageComponent {
	public UserData: any = { User: {} };
	public UserDataBackup;

	//Competence Form
	public CompetenceForm: any = { Attachments: [] };
	private CompetenceFormBackup: any = { Attachments: [] };
	public EducationTypes = [];
	public NorwegianChecked: boolean = true;
	public IsTermsAccepted: boolean = false;
	public CompetenceValidationError: string;
	public FileUploadProgress: number = 0;

	public ValidationError: string = "";
	public InformationAlert: string = "";
	public UserPassword1: string;
	public UserPassword2: string;

	@Input() set content(content) { }
	@Input() HidePasswordSection: boolean;
	@Input() HideLanguageSection: boolean;

	constructor(private service: PortalService, public Router: Router, private TranslateService: TranslateService, private pageScrollService: PageScrollService, @Inject(DOCUMENT) private document: any) {
		super(service, Router);
		var $this = this;
		service.GetAccount().subscribe(
			success => {
				$this.UserData = $.extend({}, service['UserData']);
				$this.UserDataBackup = $.extend({}, service['UserData']);
				$this.ExtendUserInfoElements();

			});

		if (service.IsAllowUserEcts()) {
			service.GetExamCompetenceRequest(true).then(res => {
				if (res && res.DateOfBirth) res.DateOfBirth = res.DateOfBirth.split("T")[0];

				$.extend(true, $this.CompetenceForm, res);
				$this.CompetenceFormBackup = $.extend(true, {}, $this.CompetenceForm);

				if (!$this.CompetenceForm.Nationality) $this.CompetenceForm.Nationality = 'Norwegian';
				$this.NorwegianChecked = $this.CompetenceForm.Nationality == 'Norwegian';

				setTimeout(
					function () {
						if ($this.PortalService.NagUserForExamCompetence())
							$this.pageScrollService.scroll({ document: this.document, scrollTarget: '#competencyForm' })
						AngularViewInit();
					}, 500);

				$this.service.UploadProgress$.subscribe(
					data => {
						$this.FileUploadProgress = data;
					});
			});

			$this.TranslateService.get(["PROFILE.COMPETENCY_UNIVERSITY", "PROFILE.COMPETENCY_UNIVERSITY_COLLEGE", "PROFILE.COMPETENCY_SECONDARY_SCHOOL", "PROFILE.COMPETENCY_NONE_OF_THE_ABOVE"]).subscribe((trans: string) => {
				$this.EducationTypes.push({ Id: 1, Value: trans["PROFILE.COMPETENCY_UNIVERSITY"] });
				$this.EducationTypes.push({ Id: 2, Value: trans["PROFILE.COMPETENCY_UNIVERSITY_COLLEGE"] });
				$this.EducationTypes.push({ Id: 3, Value: trans["PROFILE.COMPETENCY_SECONDARY_SCHOOL"] });
				$this.EducationTypes.push({ Id: 0, Value: trans["PROFILE.COMPETENCY_NONE_OF_THE_ABOVE"] });
			});
		}
	}

	public GetVisibleUserInfoElements() {
		return _.filter(this.UserData.User.UserInfoElements, { LeadingText: { IsVisible: true } });
	}

	public AddValidationError(Message, ClearExisting: boolean = false) {
		this.ValidationError = (ClearExisting ? "" : this.ValidationError) + Message + "\n";
		this.pageScrollService.scroll({ document: this.document, scrollTarget: '#validationError' })
		this.GetScrollParent($("#validationError")[0]).scrollTop = 0;

	}

	private GetScrollParent(Node) {
		if (Node == null) return null;

		if (Node.scrollHeight > Node.clientHeight) return Node;
		else return this.GetScrollParent(Node.parentNode);
	}

	public DOBChanged() {
		if (this.CompetenceForm.DateOfBirth && this.CompetenceForm.DateOfBirth.indexOf("-") == -1) {
			var fnIsValid = function (dob) { var d = new Date(); return dob.getFullYear() > 1920 && dob.getFullYear() < d.getFullYear() - 10; }
			var formatDate = function (d) { return [d.getFullYear(), ('0' + (d.getMonth() + 1)).slice(-2), ('0' + d.getDate()).slice(-2)].join('-'); }

			var dob = new Date(this.CompetenceForm.DateOfBirth);
			if (!fnIsValid(dob)) {
				var token = this.CompetenceForm.DateOfBirth.indexOf("-") > -1 ? "-" : (this.CompetenceForm.DateOfBirth.indexOf(".") > -1 ? "." : "/");
				var pieces = this.CompetenceForm.DateOfBirth.split(token);
				//Parse as norwegian date
				dob = new Date(Number(pieces[2]) + (Number(pieces[2]) < 1900 ? 1900 : 0), (Number(pieces[1]) < 13 ? Number(pieces[1]) : Number(pieces[0])) - 1, Number(pieces[1]) >= 13 ? Number(pieces[1]) : Number(pieces[0]));
			}
			if (fnIsValid(dob)) this.CompetenceForm.DateOfBirth = formatDate(dob);
			else {
				alert(this.CompetenceForm.DateOfBirth + " is not a valid date format. Please specify your data of birth using the format YYYY-MM-DD (year-month-day).")
				this.CompetenceForm.DateOfBirth = "";
			}

		}
	}

	public SaveUser(ValidateCustomFields: boolean = true): any {
		var $this = this;
		$this.ValidationError = "";
		$this.InformationAlert = "";

		//If $this.UserData.User.PreferredLanguageId is not set, copy from organization
		if (!$this.UserData.User.PreferredLanguageId)
			$this.UserData.User.PreferredLanguageId = $this.UserData.User.Customer.LanguageId;

		return $this.TranslateService.get(["PROFILE.EMAIL_REQUIRED", "PROFILE.FIRST_NAME_REQUIRED", "PROFILE.LAST_NAME_REQUIRED", "PROFILE.LANGUAGE_REQUIRED", "PROFILE.FIELD_IS_REQUIRED", "LOGIN.PASSWORD_RETYPE_MISMATCH", "LOGIN.PASSWORD_TOO_SHORT", "GLOBAL.CHANGES_SAVED"])
			.pipe(
				map((trans: string) => {
					if (!$this.UserData.User.FirstName) $this.ValidationError += trans["PROFILE.FIRST_NAME_REQUIRED"] + "\n";
					if (!$this.UserData.User.LastName) $this.ValidationError += trans["PROFILE.LAST_NAME_REQUIRED"] + "\n";
					if (!$this.UserData.User.PreferredLanguageId) $this.ValidationError += trans["PROFILE.LANGUAGE_REQUIRED"] + "\n";
					if (!$this.UserData.Email) $this.ValidationError += trans["PROFILE.EMAIL_REQUIRED"] + "\n";

					if (ValidateCustomFields) {
						_.each(_.filter($this.UserData.User.UserInfoElements, { LeadingText: { IsVisible: true, IsMandatory: true } }), function (o: any) {
							if (!o.InfoText)
								$this.ValidationError += o.LeadingText.Text + " " + trans["PROFILE.FIELD_IS_REQUIRED"] + "\n";
						});
					}

					if ($this.UserPassword1) {
						if ($this.UserPassword1 != $this.UserPassword2)
							$this.ValidationError += trans["LOGIN.PASSWORD_RETYPE_MISMATCH"] + "\n";
						else if ($this.UserPassword1.length < 4)
							$this.ValidationError += trans["LOGIN.PASSWORD_TOO_SHORT"] + "\n";
						else
							$this.UserData.Password = $this.UserPassword1;
					}

					if ($this.ValidationError) {
						setTimeout(() => {
							this.pageScrollService.scroll({ document: this.document, scrollTarget: '#validationError' })
							$this.GetScrollParent($("#validationError")[0]).scrollTop = 0;
						}, 100);
						return;
					}

					var userDataToSave = $.extend(true, {}, $this.UserData);
					//Remove LeadingText
					_.each(userDataToSave.User.UserInfoElements, function (o: any) {
						o.LeadingText = null;
					});
					return $this.service.SaveUser(userDataToSave)
						.then((
							user: any) => {
							$this.UserData = user;
							$this.ExtendUserInfoElements();
							$this.service.SetPortalLanguage(user.User.PreferredLanguageId);

							//When used in a dialog
							if ($("#competencyForm").parents(".modal-body").length > 0)
								$this.GetScrollParent($("#informationAlert")[0]).scrollTop = 0;
							else
								this.pageScrollService.scroll({ document: this.document, scrollTarget: 'portal-profile-inner' })

							$this.InformationAlert = trans["GLOBAL.CHANGES_SAVED"];
							if ($this.UserPassword1)
								return $this.service.ChangePassword(user.Username, $this.UserPassword1).then(function () { return "success"; })

							return "success";
						}).catch(
							error => {
								$this.ValidationError = <any>error;
								this.pageScrollService.scroll({ document: this.document, scrollTarget: '#validationError' })
								$this.GetScrollParent($("#validationError")[0]).scrollTop = 0;
							}
						);

				})).toPromise();

	}

	public UploadFilesFromEvent(Event) {
		var $this = this;
		$this.FileUploadProgress = 1;
		console.log('onChange');
		console.log(Event);
		var files = (Event.target || Event.srcElement).files;
		$this.service.UploadFile(files[0], null, null).subscribe(data => {
			$this.FileUploadProgress = 100;
			console.log('sent');
			if (!$this.CompetenceForm.Attachments) $this.CompetenceForm.Attachments = [];
			$this.CompetenceForm.Attachments.push(data);
			//$this.
			$this.SaveCompetenceForm(true);
		});
	}

	public SaveCompetenceForm(IsUpload = false) {
		var $this = this;

		this.CompetenceForm.PhoneNumber = this.UserData.User.PhoneNumber;
		this.CompetenceForm.FirstName = this.UserData.User.FirstName;
		this.CompetenceForm.LastName = this.UserData.User.LastName;
		this.CompetenceForm.Email = this.UserData.User.FirstName;
		this.CompetenceForm.StreetAddress = this.UserData.User.StreetAddress;
		this.CompetenceForm.ZipCode = this.UserData.User.ZipCode;
		this.CompetenceForm.City = this.UserData.User.City;
		this.CompetenceForm.Country = this.UserData.User.Country;

		$this.TranslateService.get([
			"PROFILE.PHONE_REQUIRED",
			"PROFILE.ADDRESS_REQUIRED",
			"PROFILE.ZIP_REQUIRED",
			"PROFILE.CITY_REQUIRED",
			"PROFILE.COUNTRY_REQUIRED",
			"PROFILE.TITLE_REQUIRED",
			"PROFILE.NATIONALITY_REQUIRED",
			"PROFILE.SSN_REQUIRED",
			"PROFILE.COB_REQUIRED",
			"PROFILE.EDUCATION_TYPE_REQUIRED",
			"PROFILE.EDUCATION_LEVEL_REQUIRED",
			"PROFILE.GRADUATION_YEAR_REQUIRED",
			"PROFILE.INSTITUTION_NAME_REQUIRED",
			"PROFILE.COMPETENCY_VERIFY_ACCREDITED",
			"PROFILE.PRACTICAL_DESCRIPTION_REQUIRED",
			"PROFILE.DOB_REQUIRED",
			"PROFILE.SSN_FORMAT_ERROR",
			"PROFILE.COMPETENCY_CONFIRM_INFO",
			"PROFILE.COMPETENCY_RESUME_REQUIRED",
			"PROFILE.COMPETENCY_DIPLOMA_REQUIRED",
			"PROFILE.COMPETENCY_SAVED",

		]

		).subscribe((trans: string) => {

			//Only validate if this is not from the file upload operation
			if (!IsUpload) {
				//if (this.IsTermsAccepted || this.CompetenceForm.IsTermsAccepted){
				this.CompetenceValidationError = "";
				$this.ValidationError = "";
				$this.InformationAlert = "";
				//Perform Validation

				if (!this.CompetenceForm.PhoneNumber) this.CompetenceValidationError += trans["PROFILE.PHONE_REQUIRED"] + "\n";
				if (!this.CompetenceForm.StreetAddress) this.CompetenceValidationError += trans["PROFILE.ADDRESS_REQUIRED"] + "\n";
				if (!this.CompetenceForm.ZipCode) this.CompetenceValidationError += trans["PROFILE.ZIP_REQUIRED"] + "\n";
				if (!this.CompetenceForm.City) this.CompetenceValidationError += trans["PROFILE.CITY_REQUIRED"] + "\n";
				if (!this.CompetenceForm.Country) this.CompetenceValidationError += trans["PROFILE.COUNTRY_REQUIRED"] + "\n";
				if (!this.CompetenceForm.Title) this.CompetenceValidationError += trans["PROFILE.TITLE_REQUIRED"] + "\n";
				if (!this.CompetenceForm.Nationality) this.CompetenceValidationError += trans["PROFILE.NATIONALITY_REQUIRED"] + "\n";
				//if(this.NorwegianChecked && !this.CompetenceForm.SocialSecurityNumber)this.CompetenceValidationError += trans["PROFILE.SSN_REQUIRED"] + "\n";	
				if (!this.CompetenceForm.CountryOfBirth) this.CompetenceValidationError += trans["PROFILE.COB_REQUIRED"] + "\n";
				if (this.CompetenceForm.EducationType === null || this.CompetenceForm.EducationType === "") this.CompetenceValidationError += trans["PROFILE.EDUCATION_TYPE_REQUIRED"] + "\n";

				if (this.CompetenceForm.EducationType == 0) {

					if (this.CompetenceForm.IsAccreditedPracticalCompetenceSatisfied === null)
						this.CompetenceValidationError += trans["PROFILE.COMPETENCY_VERIFY_ACCREDITED"] + "\n";

					if (!$this.CompetenceForm.PracticalCompetenceDescription)
						$this.CompetenceValidationError += trans["PROFILE.PRACTICAL_DESCRIPTION_REQUIRED"] + "\n";
				} else {
					if (!this.CompetenceForm.EducationLevel) this.CompetenceValidationError += trans["PROFILE.EDUCATION_LEVEL_REQUIRED"] + "\n";
					if (!this.CompetenceForm.GraduationYear) this.CompetenceValidationError += trans["PROFILE.GRADUATION_YEAR_REQUIRED"] + "\n";
					if (!this.CompetenceForm.InstitutionName) this.CompetenceValidationError += trans["PROFILE.INSTITUTION_NAME_REQUIRED"] + "\n";

				}

				if (!this.CompetenceForm.DateOfBirth) {
					this.CompetenceValidationError += trans["PROFILE.DOB_REQUIRED"] + "\n";
				}
				
				if (!this.IsTermsAccepted && !this.CompetenceForm.IsTermsAccepted) {
					this.CompetenceValidationError += trans["PROFILE.COMPETENCY_CONFIRM_INFO"] + "\n";
				}
				if (!$this.CompetenceForm.Attachments || $this.CompetenceForm.Attachments.length == 0) {
					if ($this.CompetenceForm.EducationType == 0) this.CompetenceValidationError += trans["PROFILE.COMPETENCY_RESUME_REQUIRED"] + "\n";
					else this.CompetenceValidationError += trans["PROFILE.COMPETENCY_DIPLOMA_REQUIRED"] + "\n";
				}
				if (this.CompetenceValidationError) {
					//this.CompetenceValidationError = "Validation of competence form failed\n" + this.CompetenceValidationError;
					//When used in a dialog
					if ($("#competencyForm").parents(".modal-body").length > 0)
						$this.GetScrollParent($("#informationAlert")[0]).scrollTop += $("#competencyForm").offset().top - 110;
					else
						this.pageScrollService.scroll({ document: this.document, scrollTarget: '#competencyForm' })
					return;
				}

				if (this.IsTermsAccepted) this.CompetenceForm.IsTermsAccepted = true;
			}

			//Save and re-get Competence 		
			this.PortalService.SaveExamCompetence(this.CompetenceForm).subscribe(
				res => {
					if (res && res.DateOfBirth) res.DateOfBirth = res.DateOfBirth.split("T")[0];
					$this.CompetenceForm = res;

					//When used in a dialog
					if ($("#competencyForm").parents(".modal-body").length > 0)
						$this.GetScrollParent($("#informationAlert")[0]).scrollTop = 0;
					else
						this.pageScrollService.scroll({ document: this.document, scrollTarget: 'portal-profile-inner' })
					$this.InformationAlert = trans["PROFILE.COMPETENCY_SAVED"];
				}
			);
		});
	}

	private ExtendUserInfoElements() {
		var $this = this;
		if (!$this.UserData.User.UserInfoElements) $this.UserData.User.UserInfoElements = [];
		this.PortalService.GetCustomLeadingTexts().subscribe(res => {
			//Merge $this.UserData.User.UserInfoElements with res
			_.each(res, function (o: any) {
				var existing: any = _.find($this.UserData.User.UserInfoElements, { LeadingTextId: o.LeadingTextId });
				if (existing) existing.LeadingText = o;
				else $this.UserData.User.UserInfoElements.push({ InfoText: "", LeadingTextId: o.LeadingTextId, LeadingText: o, UserId: $this.UserData.User.Id })
			});
		});
		setTimeout(
			function () {
				AngularViewInit();
			}, 500);
	}
}