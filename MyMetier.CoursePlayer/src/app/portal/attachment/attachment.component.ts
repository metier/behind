import { Component, Input } from "@angular/core";
import { PortalService } from "../portal.service";

@Component({
	selector: 'attachment',
	templateUrl: './attachment.component.html',
	providers: [PortalService]
})
export class AttachmentComponent {
	@Input() Attachments = [];
	@Input() Activity = {};

	constructor(public PortalService: PortalService) { }
}