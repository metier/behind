
import { HttpClient } from '@angular/common/http';
import { ApplicationRef, Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as _ from "lodash";
import { empty, forkJoin, Observable, of, throwError } from 'rxjs';
import { catchError, flatMap, map, mergeMap, share, take } from "rxjs/operators";
import { SharedService } from '../shared/shared.service';
import { StorageService } from '../storage.service';
import * as packageJson  from "../../../package.json";

declare const escape: any;
declare const $: any;
declare const initCycleCarousel: any;
declare const UAParser: any;
declare const window: any;

export interface IUserDiploma {
	Created : string,
	FileId : number,
	Id : string,
	RowVersion : string,
	ShareGuid : string,
	Title : string,
	UserId : number
};

@Injectable()
export class PortalService {
	constructor(private Router: Router, private httpClient: HttpClient, private TranslateService: TranslateService, private DomSanitizer: DomSanitizer, private ApplicationRef: ApplicationRef, private storageService: StorageService) {
		this.Init();
		this.UploadProgress$ = new Observable<any>(observer => {
			this.UploadProgressObserver = observer
		})
			.pipe(share());
	}

	public UserData: any = { Roles: [], User: { Customer: { ParticipantInfoDefinition: {} } } };
	public ParticipationData: any = [];
	public static UserDiplomasData: IUserDiploma [] |  any = [];
	public CodeData: any = {};
	public PhoenixApiUrl = "./../../api";
	public Shared: SharedService = SharedService.GetInstance();
	public UploadProgress$;
	public HasCMSAccess: any = false;
	public IsDemo = false;
	public static hideFooter: boolean = false;
	public static CourseCalendar: any = [];
	public static CourseCalendarRaw: any = [];
	public static HideWrapper = false;
	private LanguageCode;
	private BlogStories: any = null;
	private UploadProgressObserver;
	private ParticipationDataRaw: any = [];
	private CustomLeadingTexts: any;
	private LogoutExecuted: boolean = false;
	private PerformanceData: any = null;
	private ExamCompetenceData: any = { Attachments: [] };
	private _BrowserFeatures: any = null;
	private CustomSettings = null;
	private SessionData: any = {};
	private static _IFrameVisibility = false;
	private static CustomSettingsApplied = false;
	private static CustomTranslationsTimeout = null;
	private static SubscriptionData: any = [];
	private static RunningPromises = {};
	private static KeepAliveTimer = null;
	
	public IsWrapperHidden() {
		return PortalService.HideWrapper;
	}

	public IsIFrameVisible() {
		return PortalService._IFrameVisibility;
	}
	public IsMetierPlus() {
		return (PortalService.SubscriptionData || []).length > 0;
	}

	public shouldHideFooter(): boolean {
		return this.Router.url != "/portal/login"
		// return PortalService.hideFooter;
	}
	public SetIFrameVisibility(visible) {
		var $this = this;
		PortalService._IFrameVisibility = visible;
		window.onbeforeunloadPrompt = visible;
		if (!visible) {
			setTimeout(function () {
				var frame = $("#contentIframe");
				if (frame.length > 0) {
					$("#contentIframe").detach();
					$this.ApplicationRef.tick();
				}
			}, 500);
		}
		setTimeout(function () {
			var frame = $("#contentIframe")[0];
			if (frame) {
				frame.contentWindow.document.open();
				frame.contentWindow.document.write("<div style='display: flex; align-items: center; justify-content: center;'><div>Loading... Please wait</div></div>");
				frame.contentWindow.document.close();
			}
		}, 1);
	}
	public Init() {
		const $this = this;
		$this.IsDemo = $this.Router.url.indexOf('/demo/') > -1;
		$this.TranslateService.setDefaultLang('en');
		$this.UserData = $this.IsDemo ?
			{ "Username": "demo", "Email": "demo@metier.no", "CustomerId": 1, "Roles": ["LearningPortalUser"], "User": { "Customer": { "ParticipantInfoDefinition": {}, "IsArbitrary": true, "ParentId": 1, "Name": "Demo", "IsRoot": false, "DistributorId": 1194, "Id": 1 }, "CustomerId": 1, "MembershipUserId": "demo", "FirstName": "Demo", "LastName": "User", "Country": "NO", "UserInfoElements": [], "Id": 1 }, "UserContext": { "DefaultDistributor": { "Name": "Metier Academy Norway", "ContactEmail": "kurssporsmal@metier.no", "AutomailSender": "noreply@mymetier.net", "LogoUrl": "https://mymetier.net/admin/app/assets/img/metier_logo.png", "Id": 1194 } } }
			: ($this.storageService.getStorageData("UserData") || $this.UserData);
		try {
			if($this.UserData && $this.UserData.UserContext && $this.UserData.UserContext.DefaultDistributor)
				if(document.cookie.indexOf("distributor-mymetier") < 0)
					document.cookie = "distributor-mymetier="+ encodeURI(JSON.stringify($this.UserData.UserContext.DefaultDistributor))	+ "; path=/";
		} catch (errorCookie) {
			console.error(errorCookie);
		}
		
		$this.PerformanceData = $this.storageService.getStorageData("PerformanceData", function () { if ($this.UserData.User.Id) { $this.GetPerformances(); } });
		$this.ParticipationData = $this.storageService.getStorageData("ParticipationData", function () { if ($this.UserData.User.Id) { $this.GetParticipations(); } }) || [];
		$this.ParticipationDataRaw = $this.storageService.getStorageData("ParticipationDataRaw") || [];
		$this.CustomSettings = $this.storageService.getStorageData("CustomSettings", function () { $this.GetCustomSettings(); }) || {};
		PortalService.SubscriptionData = $this.storageService.getStorageData("SubscriptionData", function () { if ($this.UserData.User.Id) { $this.GetSubscriptions(); } }) || [];
		PortalService.UserDiplomasData =  $this.storageService.getStorageData("UserDiplomasData", function () { if ($this.UserData.User.Id) { $this.GetUserDiplomas(); } }) || [];
		PortalService.CourseCalendar = $this.storageService.getStorageData("CourseCalendar", function () { if ($this.UserData.User.Id) { $this.ReloadCourseCalendar(); } }) || PortalService.CourseCalendar;
		PortalService.CourseCalendarRaw = $this.storageService.getStorageData("CourseCalendarRaw") || [];

		$this.CustomLeadingTexts = $this.storageService.getStorageData("CustomLeadingTexts");
		$this.SessionData = $this.storageService.getStorageData("SessionData") || {};
		$this.ExamCompetenceData = $this.storageService.getStorageData("ExamCompetenceData") || { Attachments: [] };
		$this.HasCMSAccess = $this.storageService.getStorageData("HasCMSAccess");
		$this.CodeData = $this.storageService.getStorageData("CodeData") || {};
		$this.LanguageCode = $this.storageService.getStorageData("PortalLanguage") || "en";
		$this.SetPortalLanguage();

		console.log("--- Portal service version ---");
		console.log(packageJson.version);
		console.log("-------------------------------");


		//Todo -- catch 401 exceptions and redirect to login page
		if (!$this.storageService.getStorageData("LoginInProgress") && ($this.Router.url.indexOf("/portal/login") == -1 && !$this.LogoutExecuted)) {

			if (!$this.UserData.User.Id) {
				$this.LoginSuccess();
			}
		}

		if (!PortalService.KeepAliveTimer && !$this.IsDemo)
			PortalService.KeepAliveTimer = setInterval(function () { $this.KeepAlive(); }, 10 * 60 * 1000);

		$this.ApplyCustomSettings();

		//Load custom translations
		$this.TranslateService.onLangChange.subscribe(event => {
			clearTimeout(PortalService.CustomTranslationsTimeout);
			PortalService.CustomTranslationsTimeout = setTimeout(function () {
				$this.ApplyCustomSettings(true);
			}, 100);
		});
	}

	public IsPromiseRunning(PromiseName) {
		return PortalService.RunningPromises[PromiseName] != null;
	}
	public GetRunningPromise(PromiseName) {
		return PortalService.RunningPromises[PromiseName];
	}

	public ShowCourseCalendar() {
		var $this = this;
		if (!$this.UserData.User.Customer.ParticipantInfoDefinition.IsAllowUsersToSelfEnrollToCourses && !$this.UserData.User.Customer.ParticipantInfoDefinition.IsAllowUsersToSelfEnrollToExams)
			return false;
		if (!PortalService.CourseCalendar) return false;
		return PortalService.CourseCalendar.length > 0;
	}

	private ReloadCourseCalendar() {
		const $this = this;
		if (!PortalService.CourseCalendar) PortalService.CourseCalendar = [];
		const promise = PortalService.RunningPromises["ReloadCourseCalendar"];
		if (promise) return promise;

		return this.AddRunningPromise("ReloadCourseCalendar", $this.httpClient.get($this.PhoenixApiUrl + "/activitysets?customerId=" + $this.UserData.CustomerId + "&courseCalendar=true&includeResources=true")
			.pipe(
				map(response => {
					PortalService.CourseCalendar.length = 0;
					$.extend(true, PortalService.CourseCalendar, response);

					PortalService.CourseCalendarRaw.length = 0;
					$.extend(true, PortalService.CourseCalendarRaw, response);
					$this.storageService.setStorageData("CourseCalendarRaw", PortalService.CourseCalendarRaw);
					$this.PostProcessCourseCalendar();
					$this.RemoveRunningPromise("ReloadCourseCalendar");
					$this.storageService.setStorageData("CourseCalendar", PortalService.CourseCalendar);
					return "success";
				}),
				catchError(response => {
					return of(of($this.HandleHttpError(response, true)));
				}))
		).toPromise()
	}

	private PostProcessCourseCalendar() {
		var $this = this;
		if (!PortalService.CourseCalendar || PortalService.CourseCalendar.length == 0) return;

		//Remove individual rows that are not user-enrollable		
		//Elearning, Classroom, MockExam, Multiple Choice, Case Exam, Project Assignment, Internal Certification, External Certification				
		//Sort Activities in ActivitySets so that eLearning comes first
		var articleTypes = [1, 2, 8, 3, 4, 5, 6, 9];
		_.each(PortalService.CourseCalendar, function (a) {
			_.remove(a.Activities, function (o: any) { return !o.CustomerArticle || articleTypes.indexOf(o.CustomerArticle.ArticleTypeId) == -1; });
			a.Activities.sort(function (a, b) {
				if (a.CustomerArticle && a.CustomerArticle.ArticleTypeId == 1) return -1;
				if (b.CustomerArticle && b.CustomerArticle.ArticleTypeId == 1) return 1;
				return 0;
			});
		});

		//Remove activitysets with no activities
		//or where one activity's end date has passed
		//Or courses where user is already enrolled
		_.remove(PortalService.CourseCalendar, function (activitySet: any) {
			return activitySet.Activities.length == 0
				|| _.find(activitySet.Activities, function (a) { return a.ActivityPeriods && a.ActivityPeriods.length > 0 && a.ActivityPeriods[0].End != null && new Date(a.ActivityPeriods[0].End) < new Date() }) != null
				|| _.intersectionWith(activitySet.Activities, $this.ParticipationDataRaw, function (a: any, b: any) {
					return !a.CustomerArticle || !a.CustomerArticle.ArticlePath || !b.ArticlePath || (a.CustomerArticle.ArticlePath.split("-")[0] == b.ArticlePath.split("-")[0] && b.ParticipantStatusCodeValue != "NOSHOW");
				}).length > 0
				;
		});


		$this.storageService.setStorageData("CourseCalendar", PortalService.CourseCalendar, false);
	}

	public hideDiploma(): boolean {
		return (this.GetSettingValue("HideDiploma"));
	}
	
	public hideUserDiplomas(): boolean {
		return PortalService.UserDiplomasData && PortalService.UserDiplomasData.length > 0 ? false : true;//(this.GetSettingValue("HideDiploma"));
	}
	public getShowCompletionCertificate(): boolean {
		return this.GetSettingValue("ShowCompletionCertificate");
	}

	public GetSettingValue(Key, CustomerId: number = 0, DistributorId: number = 0) {
		var settings = this.GetSettings(CustomerId, DistributorId);
		if (settings) return _.remove(_.map(settings, function (o) { return o[Key]; }), undefined)[0];
	}

	public GetSettings(CustomerId, DistributorId: number = 0) {
		var $this = this;
		if (!CustomerId && $this.CustomSettings) CustomerId = $this.UserData.CustomerId;
		if (!DistributorId && $this.CustomSettings) DistributorId = $this.UserData.User.Customer.DistributorId;
		if ($this.CustomSettings && !$.isEmptyObject($this.CustomSettings)) {
			return _.filter($this.CustomSettings.Settings, function (o) {
				return (o.CustomerIds && o.CustomerIds.indexOf(CustomerId) > -1) || (o.DistributorIds && o.DistributorIds.indexOf(DistributorId) > -1);
			});
		}
	}

	private ApplyCustomSettings(TranslationOnly: boolean = false) {
		var $this = this;
		if ((TranslationOnly || !PortalService.CustomSettingsApplied) && $this.CustomSettings && !$.isEmptyObject($this.CustomSettings) && $this.UserData.CustomerId && $this.UserData.User.Customer.DistributorId) {
			if (!TranslationOnly) PortalService.CustomSettingsApplied = true;
			var settings = $this.GetSettings($this.UserData.CustomerId, $this.UserData.User.Customer.DistributorId);
			//Apply settings
			_.each(settings, function (o) {
				if (!TranslationOnly && o.Styles) _.each(o.Styles, function (s) {
					$('head').append('<link rel="stylesheet" type="text/css" href="./assets/customizations/' + s.Url + '">');
				});
				if (o.Translations)
					_.each(o.Translations, function (t) {
						if ($this.TranslateService.getLangs().indexOf(t.Code) == -1) {
							var lang = $this.TranslateService.currentLang;
							var pending = $this.TranslateService.use(t.Code);
							if (typeof pending !== "undefined") {
								pending.pipe(take(1)).subscribe((res: any) => {

									setTimeout(function () {
										$this.TranslateService.use(lang);
									}, 100);
								});
							}
						}
						$this.TranslateService.setTranslation(t.Code, t.Translations, true);
					});
			});
		}
	}

	public GotoUrl(Url) {
		var $this = this;
		$this.Shared.AddSpinner("GotoUrl");
		$this.Router.navigateByUrl(Url).then(x => {
			$this.Shared.ClearSpinner("GotoUrl");
		});
	}

	private HandleSuccess() {
		return "success";
	}

	private HandleSuccessReturnJSON(Response: any) {
		return Response;
	}

	private HandleSuccessReturnText(Response: any) {
		return Response.toString();
	}

	public ClearUserData(ClearStorageService: boolean) {
		this.UserData = { Roles: [], User: { Customer: { ParticipantInfoDefinition: {} } } };
		this.SessionData = {};
		this.PerformanceData = null;
		this.ParticipationData = null;
		this.ParticipationDataRaw = null;
		PortalService.UserDiplomasData = null; 
		this.ExamCompetenceData = { Attachments: [] };
		this.CodeData = {};
		PortalService.SubscriptionData = null;
		PortalService.CourseCalendar = null;
		if (ClearStorageService)
			this.storageService.removeStorageData("UserData", "SessionData", "PerformanceData", "ParticipationData", "ExamCompetenceData", "HasCMSAccess", "LoginInProgress", "CustomLeadingTexts", "ParticipationDataRaw", "CourseCalendar", "CourseCalendarRaw", "PortalLanguage", "SubscriptionData", "UserDiplomasData");
	}

	public Logout(Inactive: boolean) {
		const $this = this;
		PortalService.hideFooter = false;
		if ($this.Router.url == "/portal/login") return;
		$this.Shared.AddSpinner("Logout");
		$this.LogoutExecuted = true;
		$this.ClearUserData(false);

		const phoenixLogout = $this.httpClient.post($this.PhoenixApiUrl + "/accounts/logout", null)
			.pipe(map(() => $this.HandleSuccess()))
			.pipe(catchError(response => of(this.HandleHttpError(response, false))));

		forkJoin([phoenixLogout])
			.subscribe(res => {
				$this.ClearUserData(true);
				$this.Router.navigateByUrl("/portal/login" + (Inactive ? "?inactive=true" : ""));
				$this.Shared.ClearSpinner("Logout");
			});
	}

	public LoginSuccess() {
		const $this = this;
		$this.LogoutExecuted = false;
		$this.storageService.setStorageData("LoginInProgress", 1);
		$this.GetAccount()
			.subscribe(() => {
				const reqs = [$this.GetParticipations(), $this.GetPerformances(), $this.GetCodes("CountryCodes"), $this.GetLanguages(), $this.GetUserDiplomas(), $this.GetCodes("CurrencyCodes"), $this.GetHasCMSAccess(), $this.GetSubscriptions()];
				PortalService.hideFooter = true;

				if ($this.IsAllowUserEcts()){
					reqs.push($this.GetExamCompetenceRequest(true));
				}

				forkJoin(reqs)
					.subscribe(() => {
						if ($this.NagUserForExamCompetence())
							$('#missing-competency').modal('show');

						var url = ($this.IsMetierPlus() ? "/portal/plus?url=/" : "/portal/home");
						$this.Router.navigateByUrl(url).then(x => {
							$this.Shared.ClearSpinner("Login");
							$this.storageService.removeStorageData("LoginInProgress");
						});
					});
			},
				error => {
					alert(<any>error);
					$this.storageService.removeStorageData("LoginInProgress");
					this.Logout(true);
				}
			);
		return "success";
	}

	public LoginFailed(error) {
		const $this = this;
		$this.Shared.ClearSpinner("Login");
		throw(error);
	}

	public GetErrorMessage(Error: Response | any): string {
		let errMsg: string;
		if (Error instanceof Response) {
			var body: any = Error;

			try { body = Error; } catch (e) { }
			if (body.ValidationErrors && body.ValidationErrors.length > 0)
				errMsg = body.ValidationErrors.join("\n");
			else {
				const err = body.error || JSON.stringify(body);
				errMsg = `${Error.status} - ${Error.statusText || ''} ${err}`;
			}
		} else {
			errMsg = Error.message ? Error.message : Error.toString();
		}
		return errMsg;
	}

	private HandleHttpError(Error: Response | any, Logout: boolean) {
		this.RemoveRunningPromise();
		let errMsg: string = this.GetErrorMessage(Error);
		if (Error instanceof Response && Logout) {
			this.Logout(true);
		}
		this.Shared.ClearSpinners();
		throwError(errMsg);
	}

	Login(Username: string, Password: string): Observable<any> {
		var $this = this;
		$this.Shared.AddSpinner("Login");

		return $this.httpClient.post($this.PhoenixApiUrl + "/accounts/" + escape(Username) + "/logon", { Password: Password })
			.pipe(
				map(response => $this.LoginSuccess()),
				catchError(err => of($this.LoginFailed(err), $this.HandleHttpError(err, false)))
			);
	}

	SaveUser(UserData: any) {
		var $this = this;
		$this.Shared.AddSpinner("UserSave");

		return $this.httpClient.put($this.PhoenixApiUrl + "/accounts", UserData)
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("UserSave");
					$this.UserData = $this.HandleSuccessReturnJSON(response);
					$this.storageService.setStorageData("UserData", $this.UserData);
					return $this.UserData;
				}),
				catchError(response => of($this.HandleHttpError(response, false))))
			.toPromise();

	}

	ChangePassword(Username: string, NewPassword: string) {
		var $this = this;
		$this.Shared.AddSpinner("UserSave");

		return $this.httpClient.post($this.PhoenixApiUrl + "/accounts/" + Username + "/changepassword", { NewPassword: NewPassword })
			.pipe(
				map(() => {
					$this.Shared.ClearSpinner("UserSave");
					return;
				}),
				catchError(response => of($this.HandleHttpError(response, false))))
			.toPromise();
	}

	private RequestPasswordResetSuccess() {
		var $this = this;
		$this.Shared.ClearSpinner("PasswordReset");
		return "success";
	}

	RequestPasswordReset(Email: string): Observable<any> {
		var $this = this;
		$this.Shared.AddSpinner("PasswordReset");
		return $this.httpClient.put($this.PhoenixApiUrl + "/accounts/requestpasswordreset?email=" + escape(Email) + "&source=LearningPortal", null)
			.pipe(
				map(() => $this.RequestPasswordResetSuccess()),
				catchError(response => of($this.HandleHttpError(response, false)))
			)
	}

	PerformPasswordReset(Email: string, Token: string, NewPassword: string): Observable<any> {
		var $this = this;
		$this.Shared.AddSpinner("PasswordReset");
		return $this.httpClient.put($this.PhoenixApiUrl + "/accounts/resetpassword?email=" + escape(Email) + "&token=" + escape(Token) + "&newPassword=" + escape(NewPassword) + "", null)
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("PasswordReset");
					return response;
				}),
				catchError(response => of($this.HandleHttpError(response, false)))
			)
	}

	private GetCodesSuccess(Data: any, CodeName: string) {
		var $this = this;
		if (!$this.CodeData) $this.CodeData = {};
		$this.CodeData[CodeName] = Data;
		$this.storageService.setStorageData("CodeData", $this.CodeData);
		return "success";
	}

	GetCodes(CodeName: string) {
		const $this = this;
		return $this.httpClient.get($this.PhoenixApiUrl + "/codes?type=" + CodeName)
			.pipe(
				map(response => $this.GetCodesSuccess(response, CodeName)),
				catchError(response => of($this.HandleHttpError(response, false)))
			).toPromise()
	}

	GetLanguages() {
		const $this = this;
		return $this.httpClient.get($this.PhoenixApiUrl + "/languages")
			.pipe(
				map((response: any) => {
					var data = response;
					_.each(data, function (o) {
						o.PhoenixIdentifier = o.Identifier;
					});
					_.find(data, { Identifier: 'NO' }).Identifier = 'NB';
					$this.GetCodesSuccess(data, "Languages");
					$this.SetPortalLanguage();
				}),
				catchError(response => of($this.HandleHttpError(response, false)))
			)
			.toPromise();
	}

	SetPortalLanguageCode(Language: string) {
		this.LanguageCode = Language;
		this.storageService.setStorageData("PortalLanguage", Language);
		this.TranslateService.use(this.LanguageCode);
		console.log("Setting language " + this.LanguageCode);
	}

	SetPortalLanguage(LanguageId: number = null) {
		var $this = this;
		var languages = this.CodeData["Languages"];
		if (!LanguageId) {
			var data = this.storageService.getStorageData("UserData");
			if (data)
				LanguageId = <number>data['User'].PreferredLanguageId;
		}

		if (LanguageId && languages) {
			var newLanguage = ((_.find(languages, { Id: LanguageId }) || {})["Identifier"] || "en").toLowerCase();
			console.log("Switching language from " + this.LanguageCode + " to " + newLanguage);
			this.LanguageCode = newLanguage;
			this.TranslateService.use(this.LanguageCode);
			this.storageService.setStorageData("PortalLanguage", this.LanguageCode);
		}
		else if (this.LanguageCode) {
			this.TranslateService.use(this.LanguageCode);
			console.log("Setting language " + this.LanguageCode);
		}
	}

	public KeepAlive() {
		var $this = this;
		if (!$this.UserData || !$this.UserData.Username || $this.IsDemo) return;
		return $this.httpClient.get($this.PhoenixApiUrl + "/accounts/current")
			.pipe(
				map((response: any) => {
					if ($this.UserData && $this.UserData.Username && $this.UserData.Username != response.Username) {
						$this.Logout(true);
						return;
					}
					$this.UserData = response;
					$this.storageService.setStorageData("UserData", $this.UserData);
				})
			)
			.subscribe(
				success => { },
				error => {
					$this.Logout(true);
				}
			);
	}

	GetAccount() {
		var $this = this;
		$this.Shared.AddSpinner("GetAccount");
		return $this.httpClient.get($this.PhoenixApiUrl + "/accounts/current")
			.pipe(
				map((response: any) => {
					if ($this.UserData && $this.UserData.Username && $this.UserData.Username != response.Username) {
						$this.Logout(true);
						return;
					}
					$this.UserData = response;
					$this.storageService.setStorageData("UserData", $this.UserData);
					$this.Shared.ClearSpinner("GetAccount");
					if ($this.UserData.User.Customer.ParticipantInfoDefinition && $this.UserData.User.Customer.ParticipantInfoDefinition.CustomerPortalUrl) {
						var url = "" + $this.UserData.User.Customer.ParticipantInfoDefinition.CustomerPortalUrl;
						$this.storageService.setStorageData("CustomerPortalUrl", (url.indexOf("://") == -1 ? "http://" : "") + url);
						var name = $this.UserData.User.Customer.ParticipantInfoDefinition.CustomerPortalFriendlyName;
						if (!name && !$this.UserData.User.Customer.IsArbitrary)
							name = $this.UserData.User.Customer.Name;
						if (!name)
							name = url.split("://")[1].split("/")[0];

							$this.storageService.setStorageData("CustomerPortalName", name);
					} else {
						$this.storageService.removeStorageData("CustomerPortalUrl");
						$this.storageService.removeStorageData("CustomerPortalName");
					}
					$this.storageService.setStorageData("CustomerName", $this.UserData.User.Customer.Name);
					$this.storageService.setStorageData("CustomerId", $this.UserData.User.Customer.Id);
					document.cookie = "DistributorId="+ $this.UserData.User.Customer.DistributorId+ "; path=/";
					//document.cookie = "DistributorId=" + $this.UserData.User.Customer.DistributorId;
					$this.PushDataLayerVariable("distributorId", $this.UserData.User.Customer.DistributorId);
					$this.PushDataLayerVariable("customerId", $this.UserData.User.Customer.Id);
					$this.PushDataLayerVariable("isArbitrary", $this.UserData.User.Customer.IsArbitrary);
					$this.PushDataLayerVariable("languageId", $this.UserData.User.PreferredLanguageId);
					$this.PushEnrolledCoursesToDataLayer();
					window["LessonData"] = { UserEmail: $this.UserData.Email, EcoachEmail: "", DistributorEmail: $this.UserData.UserContext.DefaultDistributor.ContactEmail };
					return "success";
				}),
				catchError(response => of($this.HandleHttpError(response, true)))
			);
	}

	GetPerformances() {
		const $this = this;

		const promise = PortalService.RunningPromises["GetPerformances"];
		if (promise) return promise;
		if (!$this.PerformanceData) $this.PerformanceData = [];

		// return this.AddRunningPromise("GetPerformances",
		return $this.httpClient.get($this.PhoenixApiUrl + "/users/" + $this.UserData.User.Id + "/performances")
			.pipe(
				map(response => {
					if (!$this.PerformanceData) $this.PerformanceData = [];
					$this.PerformanceData.length = 0;
					$.extend(true, $this.PerformanceData, response);
					$this.storageService.setStorageData("PerformanceData", $this.PerformanceData);

					// $this.RemoveRunningPromise("GetPerformances");
					return "success";
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, true));
				})
				//)
			);
	}

	private GetHasCMSAccess() {
		var $this = this;
		$this.storageService.removeStorageData("HasCMSAccess");

		console.warn('hasCMSAccess');
		var deferred = new Observable(observer => {
			observer.next({ json: function () { return true; } });
			observer.complete();
		});

		if ($this.UserData.Roles.indexOf("BehindContentAdmin") == -1 && $this.UserData.Roles.indexOf("BehindSuperAdmin") == -1) {
			$this.Shared.AddSpinner("GetCMSAccess");
			deferred = $this.httpClient.get("/../learningportal/api/permission/cms/partial/" + $this.UserData.User.Id);
		}

		return deferred.pipe(
			map(response => {
				$this.Shared.ClearSpinner("GetCMSAccess");
				$this.HasCMSAccess = response;
				$this.storageService.setStorageData("HasCMSAccess", $this.HasCMSAccess);
				var x = response;
				return response;
			}),
			catchError(response => {
				of($this.HandleHttpError(response, false));
				return response;
			})
		).toPromise();
	}

	private GetParticipationsSuccess(Response: any) {
		const $this = this;

		const items = _(Response.Items)
			.tap(function (array) { _.remove(array, function (x) { return x['ParticipantStatusCodeValue'] == "DISPENSATION" || x['ParticipantStatusCodeValue'] == "ONHOLD" || x['ParticipantStatusCodeValue'] == "CANCELLED" || x['ArticleType'] == "Other" || x['ArticleType'] == "Subscription" }); })
			.sortBy(x => x.ActivityStart)
			.groupBy(x => x.ActivitySetId)
			.map((value, key) => ({ ActivitySetId: key, Activities: value, Completed: (value.length == _.filter(value, { ParticipantStatusCodeValue: "COMPLETED" } as any).length) }))
			.value();

		$this.ParticipationData.length = 0;
		$.extend(true, $this.ParticipationData, items);
		$this.storageService.setStorageData("ParticipationData", $this.ParticipationData);
		$this.ParticipationDataRaw.length = 0;
		$.extend(true, $this.ParticipationDataRaw, Response.Items);
		$this.storageService.setStorageData("ParticipationDataRaw", $this.ParticipationDataRaw);
		$this.RemoveRunningPromise("GetParticipations");
		$this.PostProcessCourseCalendar();
		$this.PushEnrolledCoursesToDataLayer();
		return "success";
	}
	private GetParticipations() {
		var $this = this;
		var promise = PortalService.RunningPromises["GetParticipations"];
		if (promise) return promise;
		if (!$this.ParticipationData) $this.ParticipationData = [];
		if (!$this.ParticipationDataRaw) $this.ParticipationDataRaw = [];
		return this.AddRunningPromise("GetParticipations", $this.httpClient.get($this.PhoenixApiUrl + "/participants?userId=" + $this.UserData.User.Id + "&orderDirection=&skip=0&limit=1000")
			.pipe(
				map(response => $this.GetParticipationsSuccess(response)),
				catchError(response => {
					return of($this.HandleHttpError(response, true));
				}))
		)
			.toPromise()
	}

	private GetUserDiplomas() {
		var $this = this;
		var promise = PortalService.RunningPromises["GetUserDiplomas"];
		if (promise) return promise;
		if (!PortalService.UserDiplomasData) PortalService.UserDiplomasData = [];
		return this.AddRunningPromise("GetUserDiplomas", $this.httpClient.get($this.PhoenixApiUrl + "/users/" + $this.UserData.User.Id + "/diplomas")
			.pipe(
				map((response: any) => {
					$.extend(true, PortalService.UserDiplomasData, response);
					$this.storageService.setStorageData("UserDiplomasData", PortalService.UserDiplomasData);
					$this.RemoveRunningPromise("GetUserDiplomas");
					return "success";
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, true));
				}))
		)
			.toPromise()
	}

	private GetCustomSettings() {
		var $this = this;
		var promise = PortalService.RunningPromises["GetCustomSettings"];
		if (promise) return promise;
		if (!$this.CustomSettings) $this.CustomSettings = {};
		return this.AddRunningPromise("GetCustomSettings", $this.httpClient.get("./assets/settings.json")
			.pipe(
				map(response => {
					$.extend(true, $this.CustomSettings, response);
					$this.storageService.setStorageData("CustomSettings", $this.CustomSettings);
					$this.RemoveRunningPromise("GetCustomSettings");
					$this.ApplyCustomSettings();
					return "success";
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, true));
				})))
			.toPromise()
	}

	private GetSubscriptions() {
		var $this = this;
		var promise = PortalService.RunningPromises["GetSubscriptions"];
		if (promise) return promise;

		return this.AddRunningPromise("GetSubscriptions", $this.httpClient.get($this.PhoenixApiUrl + "/accounts/subscriptions")
			.pipe(
				map((response: any) => {
					$.extend(true, PortalService.SubscriptionData, response.Items);
					$this.storageService.setStorageData("SubscriptionData", PortalService.SubscriptionData);
					$this.RemoveRunningPromise("GetSubscriptions");
					return "success";
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, true));
				}))
		).toPromise()
	}

	public GetDemoData() {
		var $this = this;
		var promise = PortalService.RunningPromises["GetDemoData"];
		if (promise) return promise;
		return this.AddRunningPromise("GetDemoData", $this.httpClient.get("./assets/demo.json")
			.pipe(
				map(response => {
					$this.RemoveRunningPromise("GetDemoData");
					return response;
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, true));
				})).toPromise()
		);
	}

	private AddRunningPromise(Key, Promise) {
		this.Shared.AddSpinner(Key);
		PortalService.RunningPromises[Key] = Promise;
		return Promise;
	}

	private RemoveRunningPromise(Key = null) {
		if (Key) {
			PortalService.RunningPromises[Key] = null;
			this.Shared.ClearSpinner(Key);
		}
		else PortalService.RunningPromises = {};
	}

	GetActivityParticipationData(AcivityId: any): any {
		var a = _.find(_.flatMap(this.ParticipationData, function (o) { return o['Activities']; }), { ActivityId: AcivityId });
		//this.ParticipationData can sometimes get stale
		if (!a) {
			const storageParticipationData = this.storageService.getStorageData("ParticipationData") as any;
			a = _.find(_.flatMap(storageParticipationData || [], function (o) { return o['Activities']; }), { ActivityId: AcivityId });
		}
		return a;
	}

	ExpireParticipationData() {
		this.storageService.removeStorageData("ParticipationData-Date");
	}

	GetActivity(ActivityId: number) {
		if (ActivityId == 0) throwError("ActivityId can't be 0");
		var $this = this;
		$this.Shared.AddSpinner("GetActivity-" + ActivityId);
		//Get Activity. Also populate Activity.CustomerArticle.ArticleCopy (to get CustomerArticle Title)
		var activity;
		return $this.httpClient.get($this.PhoenixApiUrl + "/activities/" + ActivityId + "?includeParticipantsWithOrderInformation=false")
			.pipe(
				mergeMap(response => {
					activity = response;
					return $this.httpClient.get($this.PhoenixApiUrl + "/customerarticles/" + activity.CustomerArticleId);
				}),
				map(response => {
					activity.CustomerArticle = response;
					return activity;
				}),
				flatMap(activity => $this.httpClient.get($this.PhoenixApiUrl + "/articles/" + activity.CustomerArticle.ArticleCopyId)),
				map(response => {
					$this.Shared.ClearSpinner("GetActivity-" + ActivityId);
					activity.CustomerArticle.ArticleCopy = response;
					return activity;
				}),
				catchError(response => of($this.HandleHttpError(response, true)))
			);
	}

	private GetActivityPerformancesSuccess(Response: any) {
		var $this = this;
		$this.Shared.ClearSpinner("GetActivityPerformances");
		return Response[0];
	}

	GetActivityPerformances(ActivityId: number) {
		if (ActivityId == 0) throwError("ActivityId can't be 0");
		var $this = this;
		$this.Shared.AddSpinner("GetActivityPerformances");
		return $this.httpClient.get($this.PhoenixApiUrl + "/users/" + $this.UserData.User.Id + "/performances?articleId=&activityId=" + ActivityId)
			.pipe(
				map(response => $this.GetActivityPerformancesSuccess(response)),
				catchError(response => of($this.HandleHttpError(response, false)))
			);

	}

	public GetParticipantAvailability(ParticipantId) {
		var $this = this;
		$this.Shared.AddSpinner("GetParticipantAvailability");
		return $this.httpClient.get($this.PhoenixApiUrl + "/participants/" + ParticipantId + "/availability")
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("GetParticipantAvailability");
					return response;
				}),
				catchError(response => of($this.HandleHttpError(response, false)))
			);
	}

	private GetRcoLessonSuccess(Response: any) {
		var $this = this;
		$this.Shared.ClearSpinner("GetRcoLesson");
		return Response;
	}

	GetRcoLesson(CourseId: number, LessonId) {
		var $this = this;
		$this.Shared.AddSpinner("GetRcoLesson");
		return $this.httpClient.get($this.PhoenixApiUrl + "/../learningportal/api/player/course/" + CourseId + "/lesson/" + LessonId)
			.pipe(
				map(response => $this.GetRcoLessonSuccess(response)),
				catchError(response => of($this.HandleHttpError(response, true)))
			);
	}

	public IsAllowUserEcts() {
		return this.UserData.User.IsAllowUserEcts;
	}

	IsExamCompetenceInitiated() {
		var competenceForm = this.GetExamCompetence();
		if (this.IsExamCompetenceComplete()) return true;
		if (!competenceForm.Id) return false;

		return competenceForm.FirstName || competenceForm.LastName || competenceForm.Email || competenceForm.Title || competenceForm.InstitutionName;
	}

	IsExamCompetenceComplete() {
		if (!this.GetExamCompetence().IsTermsAccepted) return false;
		return true;
	}

	IsUserExamCompetencyVisible() {
		return this.NagUserForExamCompetence() || this.GetExamCompetence().Id;
	}

	NagUserForExamCompetence() {
		return this.IsAllowUserEcts()
			&& !this.IsExamCompetenceComplete()
			&& this.ParticipationData
			&& _.find(this.ParticipationData, { Activities: [{ IsUserCompetenceInfoRequired: true, ParticipantStatusCodeValue: "ENROLLED" }] }) != null;
	}

	public GetExamCompetenceRequest(Reload = false) {
		var $this = this;
		if (!Reload) {
			return Observable.create(observer => {
				observer.next($this.ExamCompetenceData);
				observer.complete();
			});
		}
		return this.httpClient.get(this.PhoenixApiUrl + "/users/" + this.UserData.User.Id + "/competence")
			.pipe(
				map(response => {
					if (!$this.ExamCompetenceData) $this.ExamCompetenceData = {};
					$.extend(true, $this.ExamCompetenceData, response);
					$this.storageService.setStorageData("ExamCompetenceData", $this.ExamCompetenceData);
					return $this.ExamCompetenceData;
				}),
				catchError(response => {
					return of(this.HandleHttpError(response, false));
				}))
			.toPromise();
	}

	SaveExamCompetence(competenceForm) {
		const $this = this;
		const competenceURL: string = $this.PhoenixApiUrl + "/users/" + $this.UserData.User.Id + "/competence";
		$this.Shared.AddSpinner("SaveCompetence");
		var deferred: any;
		if (competenceForm.Id) deferred = $this.httpClient.put(competenceURL, competenceForm);
		else deferred = $this.httpClient.post(competenceURL, competenceForm);

		return deferred.pipe(
			map(response => {
				if (!$this.ExamCompetenceData) $this.ExamCompetenceData = {};
				$.extend(true, $this.ExamCompetenceData, response);
				$this.storageService.setStorageData("ExamCompetenceData", $this.ExamCompetenceData);
				$this.Shared.ClearSpinner("SaveCompetence");
				return $this.ExamCompetenceData;
			}),
			catchError(response => {
				return of($this.HandleHttpError(response, false));
			})
		)
			
	}

	GetExamCompetence() {
		return this.ExamCompetenceData;
	}

	GetFileUrl(File) {
		return this.PhoenixApiUrl + "/files/" + File.FileId + "/" + escape(File.RowVersion.split('/').join('_'));
	}

	GetCompetencyFileUrl(File) {
		return this.PhoenixApiUrl + "/users/" + this.UserData.User.Id + "/competence/file/" + File.FileId;
	}

	GetParticipation(ParticipationId) {
		var $this = this;
		$this.Shared.AddSpinner("GetParticipation");
		return $this.httpClient.get($this.PhoenixApiUrl + "/participants/" + ParticipationId)
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("GetParticipation");
					return response;
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, false));
				})
			)
	}

	GetCustomerPortalName() {
		return this.storageService.getStorageData("CustomerPortalName");
	}

	GetCustomerPortalUrl() {
		return this.storageService.getStorageData("CustomerPortalUrl");
	}

	GetCustomerId() {
		return this.storageService.getStorageData("CustomerId");
	}

	GetCustomLeadingTexts() {
		var $this = this;
		if (!$this.CustomLeadingTexts) {
			$this.CustomLeadingTexts = [];
			$this.Shared.AddSpinner("CustomLeadingTexts");
			return $this.httpClient.get($this.PhoenixApiUrl + "/customers/" + $this.UserData.User.Customer.Id + "/leadingtexts")
				.pipe(
					map(response => {
						$.extend(true, $this.CustomLeadingTexts, response);
						$this.storageService.setStorageData("CustomLeadingTexts", $this.CustomLeadingTexts);
						$this.Shared.ClearSpinner("CustomLeadingTexts");
						return $this.CustomLeadingTexts;
					}),
					catchError(response => {
						return of($this.HandleHttpError(response, false));
					})
				)
		}

		return new Observable(observer => {
			observer.next($this.CustomLeadingTexts);
			observer.complete();
		});


	}

	GetBlogStories() {
		var $this = this;
		if (!$this.BlogStories) {
			$this.BlogStories = [];
			if ($this.UserData.User.PreferredLanguageId == 45) {
				$this.httpClient.get($this.PhoenixApiUrl + "/../learningportal/tools/tools.aspx?action=proxy&url=http%3A%2F%2Fwww.prosjektbloggen.no%2Frss.xml")
					.pipe(

						map(response => {
							var data = $this.HandleSuccessReturnText(response);
							var posts = $this.Shared.XmlToJson($.parseXML(data));
							if (posts && posts.rss && posts.rss.channel && posts.rss.channel.item) {
								for (var x = 0; x < posts.rss.channel.item.length; x++) {
									var item = posts.rss.channel.item[x];
									var body = item['description'] ? item['description']["#text"] : "";
									var image;
									//Parse image from body
									if (body && body.indexOf("hs-featured-image-wrapper") > -1) {
										image = body.split("<img src=\"")[1];

										if (image) image = $this.DomSanitizer.bypassSecurityTrustStyle("url('" + '../tools/tools.aspx?action=proxy&url=' + escape(image.split("\"")[0]) + "')");
										body = body.split("</div>")[1];
									}
									$this.BlogStories.push({
										title: item['title'] ? item['title']["#text"] : "",
										url: item['link'] ? item['link']["#text"] : "",
										image: image,
										body: body
									});
								}
								setTimeout(function () {
									initCycleCarousel();
								}, 100);
							}
						}),
						catchError(response => of($this.HandleHttpError(response, false)))
					)
					.subscribe();
			}
		}
		return $this.BlogStories;
	}

	GetActivityDetails(ActivityId) {
		var $this = this;
		var activity = this.GetActivityParticipationData(ActivityId);
		$this.Shared.AddSpinner("GetActivityDetails");
		return $this.httpClient.get($this.PhoenixApiUrl + "/../learningportal/api/user/" + $this.UserData.User.Id + "/activity/" + ActivityId + "/activityset/" + activity.ActivitySetId + "/details/true")
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("GetActivityDetails");
					return response;
				}),
				catchError(response => of($this.HandleHttpError(response, false)))
			);
	}

	SwitchPRINCE2ExamDate(CurrentActivityId, ToActivitySetId) {
		var $this = this;
		var activity = this.GetActivityParticipationData(CurrentActivityId);
		$this.Shared.AddSpinner("SwitchPRINCE2ExamDate");
		return new Promise((resolve, reject) => {
			$this.httpClient.post($this.PhoenixApiUrl + "/../learningportal/api/user/" + $this.UserData.User.Id + "/activity/" + CurrentActivityId + "/activityset/" + activity.ActivitySetId + "/switch/to/" + ToActivitySetId, "").toPromise()
				.then(response => {

					//Refresh enrollments
					$this.GetParticipations().then(res => {

						$this.Shared.ClearSpinner("SwitchPRINCE2ExamDate");
						resolve(response);
					});
				})
				.catch(response => {
					of($this.HandleHttpError(response, false));
					reject();
				});
		});

	}

	UploadFile(File: File, Title, Description) {
		var $this = this;
		return Observable.create(observer => {
			let formData: FormData = new FormData(),
				xhr: XMLHttpRequest = new XMLHttpRequest();

			formData.append("file", File, File.name);
			if (Title) formData.append("title", Title);
			if (Description) formData.append("description", Description);

			xhr.onreadystatechange = () => {
				if (xhr.readyState === 4) {
					if (xhr.status === 200) {
						observer.next(JSON.parse(xhr.response));
						observer.complete();
					} else {
						observer.error(xhr.response);
					}
				}
			};

			xhr.upload.onprogress = (event) => {
				var progress = Math.round(event.loaded / event.total * 100);
				this.UploadProgressObserver.next(progress);
			};
			xhr.open('POST', $this.PhoenixApiUrl + "/files", true);
			xhr.send(formData);
		});
	}

	SubmitCaseExam(ParticipationId, File) {
		var $this = this;
		$this.Shared.AddSpinner("SubmitExam");
		return $this.httpClient.put($this.PhoenixApiUrl + "/participants/" + ParticipationId + "/submitexam", File)
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("SubmitExam");
					return response;
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, false));
				})
			)
	}

	SetParticipantStatus(ParticipationId, Status) {
		var $this = this;
		$this.Shared.AddSpinner("SetParticipantStatus");
		return $this.httpClient.put($this.PhoenixApiUrl + "/participants/" + ParticipationId + "/status/" + Status, null)
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("SetParticipantStatus");
					$this.GetParticipations().then();
					return response;
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, false));
				})
			)
	}

	public GetBrowserWarning(): any {
		return {
			OldBrowser: this.ShowOldBrowserWarning(),
			MobileBrowser: this.ShowMobileWarning(),
			Os: this.BrowserFeatures().os.name,
		};
	}

	public ShowOldBrowserWarning() {
		if (this.SessionData.DismissOldBrowserWarning) return false;
		if (this.ShowMobileWarning()) return false;
		if (this.BrowserFeatures().browser.name == "Chromium" && Number(this.BrowserFeatures().browser.version.split(".")[0]) >= 60) return false;
		if (this.BrowserFeatures().browser.name == "Chrome" && Number(this.BrowserFeatures().browser.version.split(".")[0]) >= 60) return false;
		if (this.BrowserFeatures().browser.name == "Chrome Webview" && Number(this.BrowserFeatures().browser.version.split(".")[0]) >= 60) return false;
		if (this.BrowserFeatures().browser.name == "Firefox" && Number(this.BrowserFeatures().browser.version.split(".")[0]) >= 55) return false;
		if (this.BrowserFeatures().browser.name == "Opera" && Number(this.BrowserFeatures().browser.version.split(".")[0]) >= 48) return false;
		if (this.BrowserFeatures().browser.name == "Edge" && Number(this.BrowserFeatures().browser.version.split(".")[0]) >= 14) return false;
		if (this.BrowserFeatures().browser.name == "Safari" && Number(this.BrowserFeatures().browser.version.split(".")[0]) >= 10) return false;
		if (this.BrowserFeatures().browser.name == "Mobile Safari" && Number(this.BrowserFeatures().browser.version.split(".")[0]) >= 10) return false;
		if (this.BrowserFeatures().browser.name == "IE" && Number(this.BrowserFeatures().browser.version.split(".")[0]) >= 11) return false;
		if (this.BrowserFeatures().browser.name == "IEMobile" && Number(this.BrowserFeatures().browser.version.split(".")[0]) >= 11) return false;
		return true;
	}

	public ShowMobileWarning() {
		if (this.SessionData.DismissMobileWarning) return false;
		return this.BrowserFeatures().device.type == "mobile";
	}

	public BrowserFeatures() {
		if (!this._BrowserFeatures) {
			var parser = new UAParser();
			this._BrowserFeatures = parser.getResult();
		}
		return this._BrowserFeatures;
	}

	public DismissOldBrowserWarning() {
		this.SessionData.DismissOldBrowserWarning = true;
		this.storageService.setStorageData("SessionData", this.SessionData);
	}

	public DismissMobileWarning() {
		this.SessionData.DismissMobileWarning = true;
		this.storageService.setStorageData("SessionData", this.SessionData);
	}

	public GetCoursePrograms() {
		var $this = this;
		$this.Shared.AddSpinner("GetCoursePrograms");
		return $this.httpClient.get($this.PhoenixApiUrl + "/courseprograms?customerId=" + $this.UserData.CustomerId)
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("GetCoursePrograms");
					return response;
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, false));
				})
			)
	}

	public GetProductDescription(ProductDescriptionId) {
		var $this = this;
		$this.Shared.AddSpinner("GetProductDescription");
		return $this.httpClient.get($this.PhoenixApiUrl + "/productdescriptions/" + ProductDescriptionId + "?withoutReferences=false&includeDeleted=false")
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("GetProductDescription");
					return response;
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, false));
				})
			)
	}
	public CalculateActivitySetPrice(ActivitySet) {
		//Todo: "Daypack"
		var articleTypes = [7, 8];
		if (_.find(ActivitySet.Activities, function (o) {
			return !o.UnitPriceNotBillable && !o.UnitPrice && o.CustomerArticle && articleTypes.indexOf(o.CustomerArticle.ArticleTypeId) == -1;
		}) != null) return;
		return _.sumBy(ActivitySet.Activities, 'UnitPrice');
	}

	public GetActivitySetCurrencyCode(ActivitySet) {
		var code = (_.find(ActivitySet.Activities, function (o) { return o.UnitPrice > 0 }) || {}).CurrencyCodeId;
		if (code && this.CodeData.CurrencyCodes)
			return (_.find(this.CodeData.CurrencyCodes, function (o) { return o.Id == code }) || {}).Value;
	}

	public EnrollInActivitySet(Id) {
		var $this = this;
		$this.Shared.AddSpinner("EnrollInActivitySet");
		return $this.httpClient.put($this.PhoenixApiUrl + "/activitysets/" + Id + "/enroll?userId=" + $this.UserData.User.Id, null)
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("EnrollInActivitySet");
					return Promise.all([$this.ReloadCourseCalendar(), $this.GetParticipations()]).then(function () {
						return response;
					})
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, false));
				}))
			.toPromise();
	}

	public GetCourseCalendar() {
		if (this.GetSettingValue("HideCatalog")) return [];
		return PortalService.CourseCalendar || [];
	}

	public PushDataLayerVariable(Key, Value) {
		window.dataLayer = window.dataLayer || [];
		var v = {};
		v[Key] = Value;
		window.dataLayer.push(v);
		console.log("PushDataLayerVariable", v)
	}

	private PushEnrolledCoursesToDataLayer() {
		var $this = this;
		if (!$this.ParticipationData) return;

		//Enrolled courses
		var enrolledCourses = _.uniq(_.compact(_.map(_.flatten(_.map($this.ParticipationData, function (o) { return o.Activities; })), function (o) { return o.ArticlePath ? o.ArticlePath.split('-')[0].replace(/E$/, '').replace(/C$/, '').replace(/MC$/, '').replace(/M$/, '') : ""; }))).join(",");
		$this.PushDataLayerVariable("enrolledCourseCodes", enrolledCourses);
	}

	public GetDurationString(Duration: number) {
		var minutes = Duration / 60000;
		var weeks = Math.floor(minutes / 10080);
		minutes -= (weeks * 10080);
		var days = Math.floor(minutes / 1440);
		minutes -= (days * 1440);
		var hours = Math.floor(minutes / 60);
		minutes -= (hours * 60);
		return this.TranslateService.get(["EXAM.WEEK" + (weeks > 1 ? "S" : ""), "EXAM.DAY" + (days > 1 ? "S" : ""), "EXAM.HOUR" + (hours > 1 ? "S" : ""), "EXAM.MINUTE" + (minutes > 1 ? "S" : "")])
			.pipe(
				map((trans: string) => {
					var s = "";
					if (weeks > 0) s += weeks + " " + (trans["EXAM.WEEKS"] || trans["EXAM.WEEK"]);
					if (days > 0) s += (s ? ", " : "") + days + " " + (trans["EXAM.DAYS"] || trans["EXAM.DAY"]);
					if (hours > 0) s += (s ? ", " : "") + hours + " " + (trans["EXAM.HOURS"] || trans["EXAM.HOUR"]);
					if (minutes > 0) s += (s ? ", " : "") + minutes + " " + (trans["EXAM.MINUTES"] || trans["EXAM.MINUTE"]);
					return s;
				}));
	}

	public CanExamAttemptBeResumed(ExamId: number) {
		var $this = this;
		$this.Shared.AddSpinner("GetExam2");
		return $this.httpClient.get($this.PhoenixApiUrl + "/exams/" + ExamId + "resumable")
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("GetExam2");
					return response;
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, false));
				})
			)
	}

	public GetExamAttempts(ActivityId: number, ExamId: number = null) {
		var $this = this;
		$this.Shared.AddSpinner("GetExam2");
		return $this.httpClient.get($this.PhoenixApiUrl + "/exams/attempts/info?activityId=" + ActivityId + (ExamId > 0 ? "&examId=" + ExamId : ""))
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("GetExam2");
					var exam = response;
					return exam;
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, false));
				})
			)
	}

	public StartExam(ActivityId) {
		var $this = this;
		$this.Shared.AddSpinner("StartExam2");
		return $this.httpClient.post($this.PhoenixApiUrl + "/exams/start?activityId=" + ActivityId, null)
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("StartExam2");
					return response;
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, false));
				}));
	}

	public GetExamStatus(ExamId) {
		var $this = this;
		$this.Shared.AddSpinner("StatusExam2");
		return $this.httpClient.get($this.PhoenixApiUrl + "/exams/" + ExamId + "/status")
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("StatusExam2");
					return response;
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, false));
				}))
	}

	public SubmitAnswer(ExamId, Answer) {
		function getMultipleChoiceAnswersFromBindable(answer) {
			return _.chain(answer.BindableMultipleChoiceAnswers)
				.reject(function (a) { return a === undefined || a === null || a === 'null'; })
				.map(function (a) { return parseInt(a, 10); })
				.value();
		}

		var $this = this;
		$this.Shared.AddSpinner("SubmitAnswerExam2");
		return $this.httpClient.put($this.PhoenixApiUrl + "/exams/" + ExamId + "/answer", Answer)
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("SubmitAnswerExam2");
					return; //NO CONTENT
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, false));
				}));
	}

	public SubmitExam(ExamId) {
		var $this = this;
		$this.Shared.AddSpinner("SubmitExam2");
		return $this.httpClient.put($this.PhoenixApiUrl + "/exams/" + ExamId + "/submit", null)
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("SubmitExam2");
					return response; //ExamResult
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, false));
				}));
	}

	public PauseExam(ExamId) {
		var $this = this;
		$this.Shared.AddSpinner("PauseExam2");
		return $this.httpClient.put($this.PhoenixApiUrl + "/exams/" + ExamId + "/pause", null)
			.pipe(
				map(response => {
					$this.Shared.ClearSpinner("PauseExam2");
					return response; //ExamResult
				}),
				catchError(response => {
					return of($this.HandleHttpError(response, false));
				}))
	};
}