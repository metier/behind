import { Component } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { BasePortalPageComponent } from "../base-portal-page.component";
import { PortalService } from "../portal.service";

declare const $: any;

@Component({
	selector: 'portal-metierplus',
	templateUrl: './metierplus.component.html',
	providers: [PortalService]
})
export class PortalMetierplusComponent extends BasePortalPageComponent {
	public Url = null;
	private QuerySubscription = null;
	private fnResizeIframe = () => { $("#metierPlusIFrame, #metierPlusLoader").css("height", Math.max(100, $(window).height() - $("header").outerHeight(true) - $("footer").outerHeight(true)) + "px"); };

	constructor(service: PortalService, public Router: Router, private ActivatedRoute: ActivatedRoute) {
		super(service, Router);
		PortalService.HideWrapper = false;

		$(window).resize(this.fnResizeIframe);

		setTimeout(() => { $(window).resize(); }, 1);

		var $this = this;
		$this.QuerySubscription = ActivatedRoute.queryParams.subscribe(params => {

			if (!!params['url']) {
				setTimeout(function () {
					$("#metierPlusIFrame").attr("src", `https://plus.mymetier.net${params['url']}`);
					$("#metierPlusIFrame").on("load", () => {
						$("#metierPlusLoader").hide();
						$("#metierPlusIFrame").show();
						$(window).resize();
					});
				}, 1);

			}
		});
	}

	public Destroy() {
		this.QuerySubscription.unsubscribe();
		$(window).off("resize", this.fnResizeIframe);
	}
}