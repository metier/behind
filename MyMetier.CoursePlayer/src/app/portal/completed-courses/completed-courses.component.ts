import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { isPlainObject } from 'lodash';
import { Activity, ActivitySet } from '../../models/models';
import { PortalService } from "../portal.service";

@Component({
  selector: 'app-completed-courses',
  templateUrl: './completed-courses.component.html',
  styleUrls: ['./completed-courses.component.scss'],
})

export class CompletedCoursesComponent {

  public completedActivitySets: ActivitySet[];
  public pmCompletedActivitySets: ActivitySet[];
  public downloadButtonLabel = this.portalService.getShowCompletionCertificate() ? "COMPLETED_COURSES.COMPLETION_SERTIFICATE_DOWNLOAD" : 'COMPLETED_COURSES.DIPLOMA_DOWNLOAD';
  private pmSubString: string = "PM2";
  
  constructor(private portalService: PortalService, private router: Router) { 
    this.completedActivitySets = this.portalService.ParticipationData.filter(res => res.Completed);
    this.pmCompletedActivitySets = this.portalService.ParticipationData.filter(res => res.Activities[0].ArticlePath.includes(this.pmSubString));
  }

  public navigateHome(): void {
    this.router.navigateByUrl('/portal/home');
  }

  public generateDiplomaUrl(activitySet: ActivitySet, isPMCourse: boolean): void {
    if (this.portalService.GetSettingValue("HideDiploma")) return;
    const templateQuery = this.getTemplateQuery(isPMCourse);
    const articleId = activitySet.Activities.length > 1 ? this.getClassroomIdIfExist(activitySet.Activities) : activitySet.Activities[0].ActivityId;
    window.open(`../../learningportal/diploma.aspx?template=${templateQuery}&classroom_id=${articleId}`, '_blank')
  }
  
  private getTemplateQuery(isPmCourse: boolean): string {
    if (isPmCourse) {
      return 'prince2';
    }

    if (this.portalService.getShowCompletionCertificate()) {
      return 'skemaCompletion';
    }
    return '';
  }

  private getClassroomIdIfExist(activities: Activity[]) {
    const classroomActivity = activities.filter((act: Activity) => act.ArticleTypeId === 2);
    return classroomActivity.length ? classroomActivity[0].ActivityId : activities[0].ActivityId;
  }
}
