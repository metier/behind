import { Component, Input } from "@angular/core";
import { PortalService } from "../portal.service";

@Component({
	selector: 'ecoach',
	templateUrl: './ecoach.component.html',
	providers: [PortalService]
})
export class EcoachComponent {
	@Input() Ecoaches = [];
	@Input() Activity = {};
	@Input() Distributor = {};

	constructor(public PortalService: PortalService) { }
}