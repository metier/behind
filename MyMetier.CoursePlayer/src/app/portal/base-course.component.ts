import { Input, Directive } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import * as _ from "lodash";
import { PlayerService } from "../player.service";
import { BasePortalPageComponent } from "./base-portal-page.component";
import { PortalService } from "./portal.service";

declare const $: any;

@Directive()
export class PortalBaseCourseComponent extends BasePortalPageComponent {
  public Activity: any = {};
  public totalActivityDurationExpression: string = "";
  public additionalActivityDurationExpression: string = "";
  public ActivityParticipation: any = {};
  public Lessons = [];
  public MockExams = [];
  public RcoObject = null;
  public Performances = [];
  public Resources = {};
  public ActivityId: number = 0;
  public ValidationError: string = "";
  public ParticipantAvailability: any = {};
  public Distributor: any = null;
  protected PlayerUrl: string = "";
  protected PlayerSettings: any = {};
  protected PlayerVersion: number = 1;
  protected RcoCourse: any;
  private totalTime: number;
  private RcoExamContainer: any;
  private NavSubscription = null;

  @Input() set content(content) {
    var $this = this;
  }

  public GetCustomerArticleTitle() {
    if (this.Activity && this.Activity.CustomerArticle)
      return this.Activity.CustomerArticle.Title;
  }

  constructor(
    protected service: PortalService,
    protected Route: ActivatedRoute,
    protected Router: Router,
    public PlayerService: PlayerService,
    public TranslateService: TranslateService
  ) {
    super(service, Router);
    var $this = this;
    $this.Distributor = $this.service.UserData.UserContext
      ? $this.service.UserData.UserContext.DefaultDistributor
      : {};

    var playerService = PlayerService;
    $this.NavSubscription = $this.Route.params.subscribe((params) => {
      $this.ActivityId = Number(params["CourseId"]);
      playerService.CurrentActivityId = $this.ActivityId;

      if (!service.IsDemo) {
        $this.ActivityParticipation =
          $this.service.GetActivityParticipationData($this.ActivityId);
        if (!$this.ActivityParticipation) {
          $this.PortalService.GotoUrl("/portal/home", false);
          return;
        }
        $this.service
          .GetParticipantAvailability($this.ActivityParticipation.Id)
          .subscribe((res) => {
            $this.ParticipantAvailability = res;
            service.GetActivity($this.ActivityId).subscribe((res) => {
              $this.Activity = res;
              if (
                (($this.Activity.CustomerArticle || {}).ArticleCopy || {})
                  .IsNewExamPlayer
              )
                PortalService.HideWrapper = true;

              if ($this.Activity.Duration) {
                $this.totalTime = $this.Activity.Duration;
                if ($this.ParticipantAvailability.AdditionalTime) {
                  $this.totalTime +=
                    $this.ParticipantAvailability.AdditionalTime;
                  $this.service
                    .GetDurationString(
                      $this.ParticipantAvailability.AdditionalTime
                    )
                    .subscribe(
                      (additionalString: string) =>
                        ($this.additionalActivityDurationExpression =
                          additionalString)
                    );
                }
                $this.service
                  .GetDurationString($this.totalTime)
                  .subscribe(
                    (durationString: string) =>
                      ($this.totalActivityDurationExpression = durationString)
                  );
              }
            });
          });
        this.GetActivityPerformances();
      }
    });
  }

  private GetActivityPerformances() {
    var $this = this;
    //This is for elearning and mock exam. What about exam?
    this.service
      .GetActivityPerformances($this.ActivityId)
      .subscribe((res: any) => {
        $this.Performances = res.Performances;

        //Make sure there are no negative scores
        _.each($this.Performances, function (o) {
          o.Score = Math.max(o.Score, 0);
        });

        if (res.Course) {
          $this.Lessons = res.Course.Lessons;

          $this.RcoCourse = res.Course;
          this.service
            .GetRcoLesson($this.RcoCourse.Id, 0)
            .subscribe((res: any) => {
              $this.RcoObject = res;
              try {
                $this.PlayerSettings = JSON.parse(res.Settings);
              } catch (e) {}

              if (
                $this.PlayerSettings &&
                $this.PlayerSettings.Player &&
                $this.PlayerSettings.Player.Id == 2
              ) {
                $this.PlayerUrl =
                  "/course/" + $this.RcoCourse.Id + "/lesson/{LessonId}";
                $this.PlayerVersion = 2;
              } else {
                $this.PlayerUrl =
                  "./../player/player.aspx?CourseID=" +
                  $this.RcoCourse.Id +
                  "&LessonID={LessonId}&course_id=" +
                  $this.RcoCourse.Id +
                  "&class_id=" +
                  $this.ActivityId;
              }

              //This is used by the course player to calculate progress
              window["LessonData"] = {
                Lessons: $this.Lessons,
                UserEmail: $this.service.UserData.Email,
                EcoachEmail: ($this.GetResourceByType(3)[0] || {}).Email,
                DistributorEmail:
                  $this.service.UserData.UserContext.DefaultDistributor
                    .ContactEmail,
              };
              _.each($this.Lessons, function (o) {
                var lesson: any = _.find(($this.RcoObject || []).Folders, {
                  ObjectID: o.Id,
                });
                $.extend(o, {
                  RCOID: o.Id,
                  Url: $this.PlayerUrl.split("{LessonId}").join(o.Id),
                  Type: o.IsFinalTest ? "finaltest" : "lesson",
                  Weight: o.Weight == null ? 1 : o.Weight,
                  Score: $this.GetPerformance(o.Id).Score,
                  Status: $this.GetPerformance(o.Id).Status,
                  CompletedDate: $this.GetPerformance(o.Id).CompletedDate,
                });

                try {
                  var settings = JSON.parse(lesson.Settings);
                  if (settings.Player.Url)
                    o.Url = settings.Player.Url.split("{LessonId}").join(o.Id);
                  if (settings.Player.CustomHtml)
                    o.CustomHtml = settings.Player.CustomHtml;
                } catch (e) {}
              });
            });
        } else if (res.ExamContainer) {
          $this.MockExams = res.ExamContainer.Children;
          $this.RcoExamContainer = res.ExamContainer;
          $this.PlayerUrl =
            "./../exam/start.aspx?id={LessonId}&exam_id=" +
            $this.RcoExamContainer.Id;
        }
      });
  }

  public Destroy() {
    this.NavSubscription.unsubscribe();
  }

  public GetPerformance(RcoId: any): any {
    return _.find(this.Performances, { RcoId: RcoId }) || {};
  }

  public GetLesson(RcoId: any) {
    return _.find(this.Lessons, { RCOID: RcoId }) || {};
  }

  public GetFileUrl(File) {
    return this.PortalService.GetFileUrl(File);
  }

  public getCourseContactPersons() {
    const instructorTypeID = 1; 
    const ecoachTypeID = 3; 
    if (this.Activity["Resources"] == null) return [];
    return this.Activity["Resources"].filter(
      resource => resource.ResourceTypeId == instructorTypeID || resource.ResourceTypeId == ecoachTypeID)
  }

  public GetResourceByType(Type: number) {
    if (!this.Resources|| this.Activity["Resources"] == null) return [];
    if (!this.Resources[Type])
      this.Resources[Type] = _.filter(this.Activity["Resources"], {
        ResourceTypeId: Type,
      });
    return this.Resources[Type];
  }

  public PlayLesson(Lesson) {
    if (!this.IsPlayable(Lesson)) return;

    //Prevent a user from playing back the same lesson with less than 10 second interval
    if (Lesson.IsStarting) return;
    Lesson.IsStarting = true;
    setTimeout(function () {
      Lesson.IsStarting = false;
    }, 10 * 1000);
    $("#lesson-custom-html").remove();

    var RcoId = Lesson.Id;
    var $this = this;
    if ($this.PlayerUrl) {
      var exam = false;
      if (exam) {
        window.location.href = $this.PlayerUrl;
        return;
      }
      var url = Lesson.Url || $this.PlayerUrl.split("{LessonId}").join(RcoId);
      var currentPageUrl = window.location.href;
      if(currentPageUrl.indexOf("www.") > -1) {
        if(url.indexOf("www.") <= -1){ 
          url = url.replace("https://", "https://www.");
        }
      } else {
        if(url.indexOf("www.") > -1){ 
          url = url.replace("https://www.", "https://");
        }
      }


      var playerService = this.PlayerService;
      playerService.Unload();
      var isMockExam = url.indexOf("/exam/") > -1;
      var popup;
      var playerVersion = $this.PlayerVersion;

      //If custom lesson url, force player 1 (iframe)
      if (!!Lesson.Url && Lesson.Url.indexOf("http") == 0) playerVersion = 1;
      if (playerVersion == 1) {
        $this.PortalService.SetIFrameVisibility(true); // .ShowIFrame=true;
        //popup = window.open("", "pma_lesson_window");
      } else if (isMockExam) {
        var width = 1010;
        var height = 669;
        popup = window.open(
          "",
          "pma_lesson_window",
          "width=" +
            width +
            ", height=" +
            height +
            ", left=" +
            (screen.width - width - 14) / 2 +
            ", top=" +
            (screen.height - height - 73) / 2 +
            ", directories=0, location=0, menubar=0, resizable=1, scrollbars=0, status=0, toolbar=0"
        );
      }

      playerService.ScormCreate(RcoId).subscribe(
        (scorm: any) => {
          debugger;
          console.log(scorm);
          console.log("Here it fails?");
          var scormSessionId = scorm.ScormSessionId;
          console.log(Lesson);
          if (Lesson.CustomHtml)
            $("<div id='lesson-custom-html' />")
              .html(Lesson.CustomHtml)
              .appendTo($("body"));
              console.log("Player version= .jls");
          console.log(playerVersion);
          //If old course player
          if (playerVersion == 1) {
            url +=
              (url.indexOf("?") == -1 ? "?" : "&") +
              "scorm_attempt=" +
              scormSessionId;
            var frame = $("#contentIframe")[0];
            if (!frame)
              frame = $(
                '<iframe id="contentIframe" frameborder="0" style="width:100%;height:100%"></iframe>'
              ).appendTo($("#contentIframeContainer"))[0];

            //popup.location.href = url;
            frame.src = url;
          }
          //mock exam player
          else if (isMockExam) {
            if (scormSessionId && url.indexOf("/exam/player.aspx") > -1) {
              scorm.Initialize("");
              scorm.SetValue("cmi.completion_status", "I");
              scorm.Commit("");
            }
            url += "&scorm_attempt=" + scormSessionId;
            popup.location.href = url;
          } else {
            console.log("NEW PLAYER");
            //New course player or
            //var isPlayer2=url.indexOf("/player2/")>-1;
            //if (!isPlayer2 && !url && url.indexOf("course_id=") == -1) url += (url.indexOf("?") == -1 ? "?" : "&") + "course_id=" + $this.RcoCourse.Id;
            $this.PortalService.GotoUrl(url);
          }

          if (popup || $this.PortalService.IsIFrameVisible()) {
            if (popup) popup.focus();
            window["fnRestoreNavigator"] = function () {
              //terminate, to update parent course status
              scorm.Terminate();

              $("#lesson-custom-html").remove();
              if (popup) popup.close();
              $this.PortalService.SetIFrameVisibility(false);
              $this.GetActivityPerformances();
              //Refresh portal data
              //$this.PortalService.ExpireParticipationData();
              $this.PortalService.GetParticipations();
              $this.PortalService.Shared.ClearSpinner("GetParticipations");
            };
            window["fnMetierPlayRCO"] = function (
              iClass,
              iRCO,
              sUrl,
              iType,
              bInternal
            ) {
              if (window["iCurrentRCO"] == iRCO) return;
              playerService.ScormCreateSynchronous(iRCO);
            };
            if (isMockExam) {
              var oPlayerWindowClose = setInterval(function () {
                $("#lesson-custom-html").remove();
                try {
                  if (popup && popup.closed) {
                    clearTimeout(oPlayerWindowClose);
                    if (scorm != null) {
                      try {
                        scorm.Terminate();
                        scorm = null;
                      } catch (e) {}
                    }
                    popup = null;
                    //Reload progress
                    $this.GetActivityPerformances();
                  }
                } catch (e) {}
              }, 200);
            }
          }
        },
        (error) => {
          $this.ValidationError = <any>error;
          $this.ValidationError +=
            "<br />This probably happened because you've been inactive for too long. Retry your request, or try <a href='/learningportal/portal2/#/portal/login'>logging in again.</a>";
          if (popup) popup.close();
        }
      );
    }
  }

  public DiplomaLink() {
    if (this.PortalService.hideDiploma()) return;
    if (this.IsCourseCompleted())
      return (
        "../../learningportal/diploma.aspx?classroom_id=" + this.ActivityId
      );
  }

  private IsCourseCompleted() {
    var $this = this;
    //If Elearning
    if (this.RcoObject && this.Lessons) {
      var test: any = _.find(this.Lessons, { IsFinalTest: true } as any);
      if (test) return this.GetPerformance(test.Id).Status == "C";
      else
        return (
          _.find(this.Lessons, function (o) {
            return $this.GetPerformance(o.Id).Status != "C";
          }) == null
        );
    }

    //If Classroom
    if (
      !this.RcoObject &&
      this.ActivityParticipation &&
      this.ActivityParticipation.ParticipantStatusCodeValue == "COMPLETED"
    )
      return true;
  }

  public ShowBadgeDownload() {
    if (
      !this.PortalService.GetSettingValue("ShowBadgeDownload") ||
      !this.IsCourseCompleted()
    )
      return;
    return true;
  }

  public IsPlayable(Lesson) {
    var $this = this;
    if (!$this.ParticipantAvailability.IsAvailable) return false;
    return true;
  }

  public SetCourseStatus(Status) {
    var $this = this;
    if ($this.ActivityParticipation.ParticipantStatusCodeValue != Status) {
      $this.PortalService.SetParticipantStatus(
        $this.ActivityParticipation.Id,
        Status
      ).subscribe((res) => {
        $this.ActivityParticipation.ParticipantStatusCodeValue = Status;
      });
    }
  }
}
