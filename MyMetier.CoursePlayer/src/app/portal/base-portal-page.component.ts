import { AfterViewInit, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PortalService } from './portal.service';

declare var $: any;
declare var jcf: any;
declare var AngularViewInit: any;
declare var AngularAfterViewInit: any;

export class BasePortalPageComponent implements OnInit, AfterViewInit {
	public PortalService = null;

	constructor(service: PortalService, Router: Router) {
		var $this = this;
		$this.PortalService = service;
	}

	ngOnInit() {
		AngularViewInit();
	}

	ngAfterViewInit() {
		var $this = this;
		AngularAfterViewInit();
		setTimeout(() => {
			jcf.refreshAll();
		}, 100);
	}

	public GetCustomerPortalUrl() {
		return this.PortalService.GetCustomerPortalUrl();
	}
	public GetCustomerPortalName() {
		return this.PortalService.GetCustomerPortalName();
	}
}
