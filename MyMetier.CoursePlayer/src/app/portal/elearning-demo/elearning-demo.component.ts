import { DOCUMENT } from '@angular/common';
import { Component, Inject } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import * as _ from "lodash";
import { PageScrollService } from 'ngx-page-scroll-core';
import { PlayerService } from '../../player.service';
import { PortalBaseCourseComponent } from "../base-course.component";
import { PortalService } from "../portal.service";

declare const $: any;
declare const clsScorm2004Local: any;

@Component({
	selector: 'portal-elearning-demo',
	templateUrl: './elearning-demo.component.html',
	providers: [PortalService]
})
export class PortalElearningDemoComponent extends PortalBaseCourseComponent {
	public DemoInfo = "";

	constructor(service: PortalService, Route: ActivatedRoute, Router: Router, PlayerService: PlayerService, TranslateService: TranslateService, private pageScrollService: PageScrollService, @Inject(DOCUMENT) private document: any) {
		super(service, Route, Router, PlayerService, TranslateService);

		const $this = this;
		$this.ParticipantAvailability = { IsAvailable: true };
		service.GetDemoData().then(function (response) {
			const demo: any = _.find(response.Demos, { Id: $this.ActivityId });
			if (demo) {
				$this.Activity = demo.Activity;
				$this.DemoInfo = demo.Information;
				if (demo.Language) $this.service.SetPortalLanguageCode(demo.Language);

				$this.RcoObject = demo.RcoCourse;
				_.each($this.RcoObject.Folders, function (o) {
					if (o.AuxField1 != "global") $this.Lessons.push({ Id: o.ObjectID, Name: o.Name, IsFinalTest: o.AuxField1 == "finaltest", Folders: o.Folders })
				});

				try {
					$this.PlayerSettings = JSON.parse($this.RcoObject.Settings);
				} catch (e) { }

				$this.PlayerUrl = "/course/demo/" + $this.RcoObject.ObjectID + "/lesson/{LessonId}";
				$this.PlayerVersion = 2;

				//This is used by the course player to calculate progress			
				window["LessonData"] = { Lessons: $this.Lessons, UserEmail: $this.service.UserData.Email, EcoachEmail: ($this.GetResourceByType(3)[0] || {}).Email, DistributorEmail: $this.service.UserData.UserContext.DefaultDistributor.ContactEmail };
				_.each($this.Lessons, function (o) {
					$.extend(o, { RCOID: o.Id, Url: $this.PlayerUrl.split("{LessonId}").join(o.Id), Type: (o.IsFinalTest ? "finaltest" : "lesson"), Weight: (o.Weight == null ? 1 : o.Weight), Score: $this.GetPerformance(o.Id).Score, Status: $this.GetPerformance(o.Id).Status, CompletedDate: $this.GetPerformance(o.Id).CompletedDate });
				})


			}
			console.log(response)
		});
	}

	public PlayLesson(Lesson) {
		var $this = this;
		$this.ValidationError = "";
		if (Lesson.Folders.length == 0) {
			this.TranslateService.get('DEMO.LESSON_NOT_AVAILABLE').subscribe((res: string) => {
				$this.ValidationError = res;
				this.pageScrollService.scroll({ document: this.document, scrollTarget: '#ValidationError' });
			});
			return;
		}
		var url = $this.PlayerUrl.split("{LessonId}").join(Lesson.Id);
		var playerService = this.PlayerService;
		playerService.Unload();
		playerService.ScormReset();

		var scorm = new clsScorm2004Local("", Lesson.Id);
		if (!scorm.GetValue("cmi.completion_status")) scorm.SetValue("cmi.completion_status", "incomplete");
		window["API_1484_11"] = scorm;
		window["iCurrentRCO"] = Lesson.Id;
		$this.service.GotoUrl(url);
	}

	public GetPerformance(RcoId) {
		var scorm = new clsScorm2004Local("", RcoId);
		return { Score: scorm.GetValue("cmi.score.raw") || 0, Status: scorm.GetValue("cmi.completion_status") || "N", CompletedDate: scorm.GetValue("xx") };
	}
}