import { Component, Input } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { PlayerService } from '../../player.service';
import { PortalBaseCourseComponent } from "../base-course.component";
import { PortalService } from "../portal.service";

@Component({
	selector: 'portal-seminar',
	templateUrl: './seminar.component.html',
	providers: [PortalService]
})
export class PortalSeminarComponent extends PortalBaseCourseComponent {
	@Input() set content(content) {
		var $this = this;
	}

	constructor(service: PortalService, Route: ActivatedRoute, Router: Router, PlayerService: PlayerService, TranslateService: TranslateService) {
		super(service, Route, Router, PlayerService, TranslateService);
	}
}