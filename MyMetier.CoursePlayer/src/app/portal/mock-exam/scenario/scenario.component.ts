import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { PlayerService } from '../../../player.service';
import { PortalService } from "../../portal.service";
import { PortalMockexamBaseComponent } from "../mock-exam-base.component";
import { PortalBaseExam2Component } from "../portal-mockexam-base.component";

@Component({
	selector: 'portal-mockexam-scenario',
	templateUrl: './scenario.component.html',
	providers: [PortalService]
})
export class PortalMockexamScenarioComponent extends PortalMockexamBaseComponent {
	constructor(Parent: PortalBaseExam2Component, service: PortalService, Route: ActivatedRoute, Router: Router, PlayerService: PlayerService, TranslateService: TranslateService) {
		super(Parent, service, Route, Router, PlayerService, TranslateService);

	}
}