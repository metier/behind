import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { PlayerService } from '../../../player.service';
import { PortalService } from "../../portal.service";
import { PortalMockexamBaseComponent } from "../mock-exam-base.component";
import { PortalBaseExam2Component } from "../portal-mockexam-base.component";

@Component({
	selector: 'portal-mockexam-attempts',
	templateUrl: './attempts.component.html',
	providers: [PortalService]
})
export class PortalMockexamAttemptsComponent extends PortalMockexamBaseComponent {
	public SortField: string = 'date';
	public SortAscending: boolean = true;

	constructor(Parent: PortalBaseExam2Component, service: PortalService, Route: ActivatedRoute, Router: Router, PlayerService: PlayerService, TranslateService: TranslateService) {
		super(Parent, service, Route, Router, PlayerService, TranslateService);
		//TODO: Download attempts and display them
		//this.service.GetExamAttempts()
	}

	public SetSort(field): void {
		if (this.SortField == field)
			this.SortAscending = !this.SortAscending;
		else
			this.SortField = field;
	}

	public HasMoreAttempts(): boolean {
		return (((this.Parent || {})['Exam']) || {}).CanStartNewAttempt;
	}

	public StartNewAttempt(): void {
		var $this = this;
		this.service.StartExam(this.ActivityId).subscribe((exam: any) => {
			$this.Parent.Exam.Exam = exam;
			$this.Router.navigateByUrl('/portal/mockexam/' + this.ActivityId + '/play/questions/' + exam.Id);

		}, error => {
			/*	 
			if(error && (error.indexOf("already has an active exam")>-1))
			{
				this.service.GetExamAttempts($this.ActivityId).subscribe(exam=>
				{
					$this.Exam = exam;
					$this.Router.navigateByUrl('/portal/mockexam/' + this.ActivityId + '/play/questions/' + exam.Exam.Id);
				});
			}else*/
			//$this.ValidationError = <any>error;
		});
	}
}