import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { PlayerService } from '../../../player.service';
import { PortalService } from "../../portal.service";
import { PortalMockexamBaseComponent } from "../mock-exam-base.component";
import { PortalBaseExam2Component } from "../portal-mockexam-base.component";

@Component({
	selector: 'portal-mockexam-confirm',
	templateUrl: './confirm.component.html',
	providers: [PortalService]
})
export class PortalMockexamConfirmComponent extends PortalMockexamBaseComponent {
	constructor(Parent: PortalBaseExam2Component, service: PortalService, Route: ActivatedRoute, Router: Router, PlayerService: PlayerService, TranslateService: TranslateService) {
		super(Parent, service, Route, Router, PlayerService, TranslateService);
	}
	
	public SubmitExam() {
		this.service.SubmitExam(this.ExamId).subscribe(
			res => {
				this.Parent.ReloadAttempts();
				this.Router.navigateByUrl('/portal/mockexam/' + this.ActivityId + '/play/result/' + this.ExamId);
			},
			err => { alert(err); });
	}
}