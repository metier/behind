import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { PlayerService } from "../../player.service";
import { BasePortalPageComponent } from "../base-portal-page.component";
import { PortalService } from "../portal.service";

@Component({
	selector: 'portal-mockexam-base-component',
	//templateUrl: './mockexam.component.html',	
	template: '<router-outlet></router-outlet>',
	providers: [PortalService]
})
export class PortalBaseExam2Component extends BasePortalPageComponent {
	public QuestionIndex: number = 0;
	private NavSubscription = null;
	public ActivityId: number = 0;
	public Activity: any = {};
	public ExamId: number = 0;
	public Exam: any;

	set content(content) {
		var $this = this;
	}

	constructor(protected service: PortalService, protected Route: ActivatedRoute, private Router: Router, public PlayerService: PlayerService, public TranslateService: TranslateService) {
		super(service, Router);
		var $this = this;

		var playerService = PlayerService;
		$this.NavSubscription = $this.Route.params.subscribe(params => {

			$this.ActivityId = Number(params['CourseId']);
			$this.ExamId = Number(params['ExamId']);
			playerService.CurrentActivityId = $this.ActivityId;
			$this.ReloadAttempts();
			service.GetActivity($this.ActivityId).subscribe(res => {
				$this.Activity = res;
			});
		});
		PortalService.HideWrapper = true;
	}

	public ReloadAttempts() {
		var $this = this;
		var examId = ($this.Exam ? $this.Exam.Exam.Id : null);
		return this.service.GetExamAttempts($this.ActivityId, examId).subscribe(exam => {
			$this.Exam = exam;
		});
	}

	public Destroy(): void {
		this.NavSubscription.unsubscribe();
	}

}