import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { PlayerService } from '../../../player.service';
import { PortalService } from "../../portal.service";
import { PortalMockexamBaseComponent } from "../mock-exam-base.component";
import { PortalBaseExam2Component } from "../portal-mockexam-base.component";

@Component({
	selector: 'portal-mockexam-summary',
	templateUrl: './summary.component.html',
	providers: [PortalService]
})
export class PortalMockexamSummaryComponent extends PortalMockexamBaseComponent {
	constructor(Parent: PortalBaseExam2Component, service: PortalService, Route: ActivatedRoute, Router: Router, PlayerService: PlayerService, TranslateService: TranslateService) {
		super(Parent, service, Route, Router, PlayerService, TranslateService);

	}

	public GotoQuestion(Index: number): void {
		this.QuestionIndex = Index;
		this.Router.navigateByUrl('/portal/mockexam/' + this.ActivityId + '/play/questions/' + this.ExamId);
	}
}