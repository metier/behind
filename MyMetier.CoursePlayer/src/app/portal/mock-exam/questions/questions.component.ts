import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, Inject } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import * as _ from "lodash";
import { PageScrollService } from 'ngx-page-scroll-core';
import { PlayerService } from '../../../player.service';
import { PortalService } from "../../portal.service";
import { PortalMockexamBaseComponent } from "../mock-exam-base.component";
import { PortalBaseExam2Component } from "../portal-mockexam-base.component";

declare const $: any;

@Component({
	selector: 'portal-mockexam-questions',
	templateUrl: './questions.component.html',
	providers: [PortalService]
})
export class PortalMockexamQuestionsComponent extends PortalMockexamBaseComponent implements AfterViewInit {
	private PreviousAnswerState: any;
	public IsCheckLater: boolean;
	public ReviewMode: boolean = true;
	public TimeRemaining: number = null;
	private TimeRemainingTimer: any;
	constructor(Parent: PortalBaseExam2Component, service: PortalService, Route: ActivatedRoute, Router: Router, PlayerService: PlayerService, TranslateService: TranslateService, private pageScrollService: PageScrollService, @Inject(DOCUMENT) private document: any) {
		super(Parent, service, Route, Router, PlayerService, TranslateService);

		var $this = this;
		this.ReviewMode = window.location.href.indexOf("/review/") > -1;
		this.StartClockIfNotAlreadyStarted();
		setTimeout(() => {
			$this.pageScrollService.scroll({ document: this.document, scrollTarget: '#exam2'});
		}, 500);
	}

	ngAfterViewInit() {
		var $this = this;
		$('#questionNav').on('show.bs.dropdown', function () {
			$("#backdrop").show();
		})
			.on('hide.bs.dropdown', function () {
				$("#backdrop").hide();
			});
	}

	public ToggleMarkedForReview() {
		var q = this.GetCurrentAnswer();
		q.IsMarkedForReview = !q.IsMarkedForReview;
		if (q.IsMarkedForReview) {//“Check Later”
			this.TranslateService.get('EXAM2.ADDED_TO_CHECK_LATER').subscribe((res: string) => {
				var m = $("<div id='hoverMessage'>" + res + "</div>");
				$("#exam2").append(m);
				m.delay(2000).fadeOut(500);
			});
		}
		this.SaveProgress();
	}

	public GetCurrentQuestion() {
		var q = this.GetCurrentQuestionAtIndex(this.QuestionIndex) || {};
		return q;
	}

	public GetCurrentAnswer() {
		var a = this.GetAnswerAtIndex(this.QuestionIndex) || {};
		if (a.QuestionId > 0 && (!this.PreviousAnswerState || (a && a.QuestionId && a.QuestionId != this.PreviousAnswerState.QuestionId)))
			this.PreviousAnswerState = _.cloneDeep(a);

		return a;
	}

	public StartClockIfNotAlreadyStarted() {
		if (this.Exam && this.Exam.Exam && this.TimeRemaining === null) {
			var $this = this;

			this.TimeRemaining = this.Exam.Exam.Status.TimeRemainingInSeconds;
			if (!this.ReviewMode)
				this.TimeRemainingTimer = setInterval(function () { $this.TimeRemaining--; }, 1000);
		}
	}

	public HasNext() {
		this.StartClockIfNotAlreadyStarted();
		var questions = this.GetQuestions();
		return this.QuestionIndex < questions.length - 1;
	}

	public Next() {
		if (!this.HasNext()) return;
		this.SaveProgress();
		this.QuestionIndex++;
		this.pageScrollService.scroll({ document: this.document, scrollTarget: '#exam2'})
	}

	public HasPrev() {
		this.StartClockIfNotAlreadyStarted();
		return this.QuestionIndex > 0;
	}

	public Prev() {
		if (!this.HasPrev()) return;
		this.SaveProgress();
		this.QuestionIndex--;
		this.pageScrollService.scroll({ document: this.document, scrollTarget: '#exam2'})
	}

	private SaveProgress(): Promise<any> {
		var $this = this;
		if (!this.ReviewMode) {
			var a = this.GetCurrentAnswer();
			if (this.PreviousAnswerState && this.PreviousAnswerState.QuestionId == a.QuestionId && !_.isEqual(a, this.PreviousAnswerState)) {
				return this.service.SubmitAnswer(this.ExamId, a)
					.toPromise()
					.then(r => { }, e => { alert($this.service.GetErrorMessage(e)); });
			}
		}
		return Promise.resolve();
	}

	public PauseExam() {
		var $this = this;
		this.SaveProgress().then(r => {
			$this.service.PauseExam(this.ExamId).subscribe(r => {
				this.Router.navigateByUrl('/portal/mockexam/' + this.ActivityId + '/play/attempts/' + this.ExamId);
			});
		});
	}
}