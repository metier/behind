import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as _ from "lodash";
import { PlayerService } from '../../player.service';
import { PortalService } from '../portal.service';
import { PortalBaseExam2Component } from "./portal-mockexam-base.component";

export class PortalMockexamBaseComponent {
	public QuestionFilter: number = null;
	public TranslateService;

	constructor(protected Parent: PortalBaseExam2Component, protected service: PortalService, public Route: ActivatedRoute, public Router: Router, PlayerService: PlayerService, TranslateService: TranslateService) {
		const $this = this;
		const playerService = PlayerService;
		this.TranslateService = TranslateService;
	}

	public get ActivityId(): number {
		return ((this.Parent || {})['ActivityId']) || 0;
	}
	public get QuestionIndex(): number {
		return ((this.Parent || {})['QuestionIndex']) || 0;
	}
	public set QuestionIndex(value: number) {
		if (this.Parent) this.Parent.QuestionIndex = value;
	}
	public get ExamId(): number {
		return ((((this.Parent || {})['Exam']) || {}).Exam || {}).Id;
	}
	public get Exam(): any {
		return (((this.Parent || {})['Exam']) || {});
	}
	public get Activity(): any {
		return (((this.Parent || {})['Activity']) || {});
	}

	public GetQuestions() {
		return ((((this.Parent || {})['Exam'] || {}).Exam || {}).Questions || []);
	}

	protected GetAnswers() {
		return ((((this.Parent || {})['Exam'] || {}).Exam || {}).Answers || []);
	}

	public GetCurrentQuestionAtIndex(Index) {
		var questions = this.GetQuestions();
		if (questions.length > 0) return questions[Index];
	}

	public GetAnswerAtIndex(Index): any {
		var question = this.GetCurrentQuestionAtIndex(Index);
		if (question) {
			var answers = this.GetAnswers();
			var answer = _.find(answers, { QuestionId: question.Id });
			if (!answer) {
				answer = { QuestionId: question.Id };
				answers.push(answer);
			}
			return answer;
		}
	}

	public CountQuestionsAnswered() {
		return _.filter(this.GetAnswers(), (a) => { return a.SingleChoiceAnswer > 0; }).length;
	}

	public CountQuestionsMarkedForReview() {
		return _.filter(this.GetAnswers(), (a) => { return a.IsMarkedForReview == true; }).length;
	}

	public GetAttempts(SortField: string, SortAscending: boolean = true) {
		var attempts = ((this.Parent || {})['Exam'] || {}).Attempts;
		//Default or no SortField: SortField = 'date';
		if (!SortField) SortField = 'date';
		attempts = _.orderBy(attempts, function (o) { return (SortField == 'timeremaining' ? o.Exam.Status.TimeRemainingInSeconds : (SortField == 'score' ? o.Exam.Result.ScorePercentage : o.Exam.Status.ModifiedDate)); }, [(SortAscending ? 'asc' : 'desc')]);
		return attempts;
	}
}
