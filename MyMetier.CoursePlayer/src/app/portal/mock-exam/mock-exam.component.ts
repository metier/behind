import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { PlayerService } from '../../player.service';
import { PortalBaseCourseComponent } from "../base-course.component";
import { PortalService } from "../portal.service";

@Component({
	selector: 'portal-mockexam',
	templateUrl: './mock-exam.component.html',
	providers: [PortalService]
})
export class PortalMockexamComponent extends PortalBaseCourseComponent {
	public ExamDurationExpression: string;
	private NavSubscriptionLocal: any;
	private Exam: any;

	constructor(service: PortalService, Route: ActivatedRoute, Router: Router, PlayerService: PlayerService, TranslateService: TranslateService) {
		super(service, Route, Router, PlayerService, TranslateService);

		//StartExam
		const $this = this;
		const playerService = PlayerService;
		$this.NavSubscriptionLocal = $this.Route.params.subscribe(params => {

			service.GetExamAttempts($this.ActivityId).subscribe(exam => {
				$this.Exam = exam;
				$this.service.GetDurationString($this.Exam.DurationInSeconds * 1000).subscribe(s => $this.ExamDurationExpression = s);
			});
		});

	}

	public Destroy() {
		this.NavSubscriptionLocal.unsubscribe();
	}

	public StartNewAttempt() {
		const $this = this;
		this.service.StartExam(this.ActivityId).subscribe((exam: any) => {
			$this.Exam = { Exam: exam, Attempts: [] };
			$this.Router.navigateByUrl('/portal/mockexam/' + this.ActivityId + '/play/questions/' + exam.Id);

		}, error => {
			if (error && (error.indexOf("already has an active exam") > -1)) {
				this.service.GetExamAttempts($this.ActivityId).subscribe((exam: any) => {
					$this.Exam = exam;
					$this.Router.navigateByUrl('/portal/mockexam/' + this.ActivityId + '/play/questions/' + exam.Exam.Id);
				});
			} else
				$this.ValidationError = <any>error;
		});
	}

	public ShowPastAttempts() {
		const $this = this;
		$this.Router.navigateByUrl('/portal/mockexam/' + this.ActivityId + '/play/attempts/' + this.Exam.Exam.Id);
	}

}