import { Component, Input, ViewChild } from "@angular/core";
import { Router } from '@angular/router';
import { BasePortalPageComponent } from "../base-portal-page.component";
import { PortalProfileComponentInner } from "../portal-profile-inner/portal-profile-inner.component";
import { PortalService } from "../portal.service";

@Component({
	selector: 'portal-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./portal-profile.component.scss'],
	providers: [PortalService]
})
export class PortalProfileComponent extends BasePortalPageComponent {
	@ViewChild(PortalProfileComponentInner, { static: true })
	private profileComponentInner: PortalProfileComponentInner;

	constructor(service: PortalService, Router: Router) {
		super(service, Router);
	}

	public SaveUser() {
		this.profileComponentInner.SaveUser();
	}

	@Input() set content(content) {
		var $this = this;
	}
}