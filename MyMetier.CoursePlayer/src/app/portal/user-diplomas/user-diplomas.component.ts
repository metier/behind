import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PortalService, IUserDiploma } from "../portal.service";

@Component({
  selector: 'app-user-diplomas',
  templateUrl: './user-diplomas.component.html',
  styleUrls: ['./user-diplomas.component.scss'],
})

export class UserDiplomasComponent {

  public userDiplomas: IUserDiploma[];
  public downloadButtonLabel =  "USER_DIPLOMAS.DOWNLOAD";
  
  constructor(private portalService: PortalService, private router: Router) { 
    this.userDiplomas = PortalService.UserDiplomasData;
  }

  public navigateHome(): void {
    this.router.navigateByUrl('/portal/home');
  }

  public generateDiplomaUrl(diploma: IUserDiploma): void {
    window.open(this.portalService.PhoenixApiUrl + `/users/${diploma.UserId}/transcriptfiles/${diploma.FileId}`, '_blank')
  }
  
}
