import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDiplomasComponent } from './user-diplomas.component';

describe('UserDiplomasComponent', () => {
  let component: UserDiplomasComponent;
  let fixture: ComponentFixture<UserDiplomasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDiplomasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDiplomasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
