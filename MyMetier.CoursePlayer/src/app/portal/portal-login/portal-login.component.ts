import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import * as _ from "lodash";
import { BasePortalPageComponent } from "../base-portal-page.component";
import { PortalService } from "../portal.service";

declare const $: any;

@Component({
	selector: 'portal-login',
	templateUrl: './login.component.html',
	providers: [PortalService]
})
export class PortalLoginComponent extends BasePortalPageComponent {
	public Mode: string = '';
	public Username: string = '';
	public Password: string = '';
	public PasswordReminderEmail: string = '';
	public ValidationError: string;
	public isValidationInformation: boolean = false;
	public LoginSuccess: string;
	public PasswordResetEmail: string = '';
	public PasswordResetToken: string = '';
	public PasswordResetPassword1: string = '';
	public PasswordResetPassword2: string = '';
	private QuerySubscription = null;

	constructor(service: PortalService, public Router: Router, protected Route: ActivatedRoute, private TranslateService: TranslateService) {
		super(service, Router);
		var $this = this;
		$this.QuerySubscription = $this.Route.queryParams.subscribe(params => {
			if (params['inactive'] == 'true') {
				$this.TranslateService.get('LOGIN.SESSION_TIMEOUT').subscribe((res: string) => {
					$this.ValidationError = res;
				});
			}
			//We're performing a password reset		
			if (params['token']) {
				$this.Mode = 'PasswordReset';
				$this.PasswordResetToken = params['token'];
			}
			//This is an external login request
			if (params['login'] == "true") {
				$this.PortalService.LoginSuccess();
			}
		});

		//Check if session is in progress. If so, log out		
		service.ClearUserData(true);

		setTimeout(function () {
			$("#name").focus();
		}, 1000);

		if (window.location.href.indexOf("training.psoetraining.com") > -1)
			window.location.href = "https://psoetraining.com/login.aspx";
	}

	public Login() {
		var $this = this;
		$this.ValidationError = '';
		$this.isValidationInformation = false;
		$this.PortalService
			.Login($this.Username, $this.Password)
			.subscribe(
				success => {
					$this.LoginSuccess = success;
				},
				err =>{
						$this.Password = "";
						if(err && err.status == 404)
							$this.TranslateService.get('LOGIN.WRONG_USER_PASSWORD').subscribe((res: string) => {
								$this.ValidationError = res;
							});
						else if(err && err.error && err.error.ValidationErrors.length > 0)
							$this.TranslateService.get('LOGIN.WRONG_USER_PASSWORD').subscribe((res: string) => {
								$this.ValidationError = res;
							});
						else
							$this.ValidationError = "Unexcepted error has ocurred. Please, try again or contact your administrator.";

						// 	if(err && err.status == 404)
						// 	$this.ValidationError = "User does not exist. Please, contact the administrator to confirm this information.";
						// else if(err && err.error && err.error.ValidationErrors.length > 0)
						// 	$this.ValidationError = err.error.ValidationErrors.join(", ");
						// else
						// 	$this.ValidationError = "User does not exist or password is wrong.";
					},
				() => console.log('HTTP request completed.'),
			);
	}

	public RequestPasswordReminder() {
		var $this = this;
		$this.ValidationError = '';
		$this.isValidationInformation = false;
		$this.PortalService
			.RequestPasswordReset($this.PasswordReminderEmail)
			.subscribe(
				success => $this.PasswordReminderSent(),
				error => $this.ValidationError = <any>error
			);
	}

	private PasswordReminderSent() {
		var $this = this;
		$this.Mode = '';
		$this.TranslateService.get('LOGIN.REMINDER_SENT', { email: $this.PasswordReminderEmail }).subscribe((res: string) => {
			$this.ValidationError = res;
			$this.isValidationInformation = true;
		});
	}

	public PerformPasswordReset() {
		var $this = this;
		$this.ValidationError = '';
		$this.isValidationInformation = false;
		$this.TranslateService.get('LOGIN.REMINDER_SENT', { email: $this.PasswordReminderEmail }).subscribe((res: string) => {
			$this.ValidationError = res;
			$this.isValidationInformation = true;
		});
		var errors = [];
		if (!$this.PasswordResetEmail) errors.push("LOGIN.EMAIL_REQUIRED");
		if (!$this.PasswordResetToken) errors.push("LOGIN.TOKEN_REQUIRED");
		if (!$this.PasswordResetPassword1) errors.push("LOGIN.NEW_PASSWORD");
		if ($this.PasswordResetPassword1 != $this.PasswordResetPassword2) errors.push("LOGIN.PASSWORD_RETYPE_MISMATCH");

		if (errors.length > 0) {
			$this.TranslateService.get(errors).subscribe((translations: string) => {
				$this.ValidationError = _.values(translations).join("\n");
				$this.isValidationInformation = false;
			});
			return;
		}

		$this.PortalService
			.PerformPasswordReset($this.PasswordResetEmail, $this.PasswordResetToken, $this.PasswordResetPassword1)
			.subscribe(
				success => $this.PasswordResetSuccess(),
				error => $this.ValidationError = <any>error
			);
	}

	private PasswordResetSuccess() {
		var $this = this;
		$this.Mode = '';
		$this.TranslateService.get('LOGIN.PASSWORD_RESET_SUCCESS', { email: $this.PasswordResetEmail }).subscribe((res: string) => {
			$this.isValidationInformation = true;
			$this.ValidationError = res;
		});
	}

	public Destroy() {
		this.QuerySubscription.unsubscribe();
	}

	private ClearValidationError() {
		var $this = this;
		$this.ValidationError = "";
		$this.isValidationInformation = false;
	}
}