import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { AppLog } from './app.log';

@Injectable()
export class MyErrorHandler implements ErrorHandler {
  private _appLog: AppLog;
  private static MessagesLogged={};
  constructor (injector:Injector) {
    setTimeout(() => this._appLog = injector.get(AppLog));
  }
  private CalculateChecksum(s)
  {
    if(!s)return"";
    var chk = 0x12345678;
    var len = s.length;
    for (var i = 0; i < len; i++) {
        chk += (s.charCodeAt(i) * (i + 1));
    }

    return (chk & 0xffffffff).toString(16);
  }
  public handleError( error: any ) : void {
  	if(error.status!=404){
      var checksum=this.CalculateChecksum(error.stack || error.message || error);
      if(!MyErrorHandler.MessagesLogged[checksum]){
        MyErrorHandler.MessagesLogged[checksum]=true;
  	  	this._appLog.logMessageAsEmail(error.message, error.stack || error).subscribe(sub=>{});
      }else{
        this._appLog.logMessageToConsole(error);
      }
	  	//alert("An exception occurred and our technical staff has been notified.\n\n\nThis is what was reported: \n* Message: " + error.message + "\n* Stack trace: " + error.stack);
	  }
  	//alert(error);
  }  
}