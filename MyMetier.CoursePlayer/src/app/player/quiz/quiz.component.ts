import { AfterViewInit, Component, Input } from "@angular/core";
import { AppLog } from "app/app.log";
import { PlayerService } from "app/player.service";
import * as _ from "lodash";
import { BaseContentComponent } from "../basecontent/basecontent.component";

declare var $: any;
declare var cInt: any;

@Component({
	selector: 'quiz-content',
	templateUrl: './quiz.component.html',
	styleUrls: ['./quiz.component.scss'],
	providers: [AppLog]  	
})
export class QuizComponent extends BaseContentComponent implements AfterViewInit{
	@Input() CurrentQuestion: number=-1;
	@Input() IsFinalTest: boolean;
	@Input() IsSubmitted: boolean;
	public Questions = null;
	private _UpdateScormScoreTimer = null;
	
	constructor(private appLog: AppLog, playerService: PlayerService) {	
		super(playerService);			
		var $this=this;		
	  }

	ngAfterViewInit() {
		this.PlayerService.SetElementLoaded("Content");		
	}

	public sortableOptions = {
	    placeholder: "active",
	    items:"> .quiz-dd-drag",
	    connectWith: ".apps-container",
	      receive: function (event, ui) {
                
                $(".quiz .drop-zone").height("6px");
                //Are we dragging back to drags container. If so, update manually
                if($(event.target).is(".drags") && ui.item.sortable.sourceModel.length>0)
                {
                	ui.item.sortable.droptargetModel.push(ui.item.sortable.sourceModel[0]);
                }
            },
           over: function (event, ui) {
                console.log("dragging over",this);
                $(this).addClass("active").find(".drop-zone").animate({ height: ui.helper.height() + "px" }, 200);
            },
            out: function (event, ui) {
                $(this).removeClass("active");
                $(".quiz .drop-zone").height("6px");
            },
        	start: function (e, ui) { ui.helper.addClass("dragging"); PlayerService.BlockTouchScroll=true;	       },
            stop: function (e, ui) { ui.item.removeClass("dragging"); PlayerService.BlockTouchScroll=false; }
	  };

	 
	SubmitFinalTest(){
		var $this=this;		
		$.each($this.Questions,function(i,o){
			$this.QuizCheckAnswer(o);
		});			
	}
	ShowAnswers(question){		
		var show=this.IsSubmitted || this.IsShowingAnswers(question);
		if(show && question.Answer.Score==null)this.QuizCheckAnswer(question);
		return show;
	}
	QuizShowAnswers(question, event){
		question.showanswers=(question.showanswers!=true);
		event.cancelBubble=true;
		event.preventDefault();
	}
	QuizRestart(question){
		question.showanswers=false;		
		Object.keys(question.Answer).forEach(function(key) { question.Answer = []; });		
		this.InitializeQuestion(question);
	}
	QuizCheckAnswer(question){
		question.showanswers=false;
	
		var $this=this;
		var score=0,numCorrect=0;
		if(question['@type']=='multiplechoice'){			
			$.each(question.answers.answer, function(i,option){
				if($this.IsOptionCorrect(question,option))numCorrect++;
				if($this.IsOptionChecked(question,option))
					score+=($this.IsOptionCorrect(question,option) ? 1 : -1);									
			});			
		}else{
			$.each(question.drags.drag, function(i,drag){
				//It is only correct if it has a @correct attribute, and the target exists.
				if(drag["@correct"] && question.Answer[drag["@correct"]])numCorrect++;
				if(question.UnusedOptions.indexOf(i)==-1)
					score += (question.Answer[drag["@correct"]] && question.Answer[drag["@correct"]].indexOf(i) > -1 ? 1 : -1);
			});			
		}		
		question.Answer.Score=Math.max(0,Math.round(numCorrect==0 ? 100 : score*100/numCorrect));
		
		//Add a delay to UpdateScormScore
		if($this._UpdateScormScoreTimer) clearTimeout($this._UpdateScormScoreTimer);
		$this._UpdateScormScoreTimer =
		 	setTimeout(function()
			{
				$this.UpdateScormScore();	
				$this._UpdateScormScoreTimer=null;
			},200);		
	}

	public UpdateScormScore()
	{
		//If terminated final test, exit
		if(this.IsFinalTest && this.PlayerService.GetRcoIdForApi() == -1)
			return;
		
		//Update SCORM score
		var score=0;
		$.each(this.Questions,function (i,o) {
			if(o.Answer && cInt(o.Answer.Score)>0)score+=o.Answer.Score;
		});
		score=(this.Questions.length==0 ? 100 : score/this.Questions.length);
		score=Math.round(score);
		if(cInt(this.PlayerService.GetScormScore())<=score)
			this.PlayerService.SetScormScore(score);			
						
		if(cInt(this.PlayerService.GetScormScore()) <= score || this.IsFinalTest)
		{
			this.PlayerService.GetScormContentData(this.Content.ObjectID).Score = score;			
			this.PlayerService.ScormCommit();
			console.log("QuizCheckAnswer " + this.Content.ObjectID + " score: " + score);
		}
	}
	
	private InitializeQuestion(question)
	{
		question.showanswers=false;
		if(question['@type']=='draganddrop'){

			if(question.UnusedOptions)
				question.UnusedOptions.length=0;							
			else
				question.UnusedOptions=[];
			
			for(var x=0; x < question.drags.drag.length; x++ ) question.UnusedOptions.push(x);
			$.each(question.targets.target,function(i,target)
			{
				if(!question.Answer[target["@id"]])
					question.Answer[target["@id"]] = [];

				else question.Answer[target["@id"]] = $.grep(question.Answer[target["@id"]],function(o) {return o!=null;});

				$.each(question.Answer[target["@id"]],function (j,questionIndex) {
					if(question.UnusedOptions.indexOf(questionIndex)>-1) question.UnusedOptions.splice(question.UnusedOptions.indexOf(questionIndex),1);
				})
			});


		}
	}

	//For DragDrop choice questions
	private DDSanitizeOptions(question,index)
	{
		_.pull(question.UnusedOptions, undefined);
		Object.keys(question.Answer).forEach(function(key) { 
			_.pullAll(question.Answer[key], question.UnusedOptions); 
		});		
	}

	IsShowingAnswers(question){
		if(this.PlayerService.PrintRequest) return this.Content.ShowAnswers;
		return question.showanswers;
	}

	DDGetUnusedOptions(question)
	{			
		if(this.ShowAnswers(question)){
			var options=[];	
			$.each(question.drags.drag, function(i,o) {
				if(!o["@correct"])options.push(i);
			});
			return options;
		}
		return question.UnusedOptions;
	}

	DDGetTargetOptions(question,targetId)
	{
		if(!targetId)return;
		if(this.ShowAnswers(question)){
			var options=question.Answer[targetId].slice();
			$.each(question.drags.drag,function(i,o){
				if(o["@correct"]==targetId && options.indexOf(i)==-1)options.push(i);
			});
			return options;
		}

		return question.Answer[targetId];
	}

	GetQuestionText(question)
	{
		return (question.questiontext['#text'] + "").split("\n").join("<br />");
	}

	GetIntroText(question)
	{
		if(question.introduction && question.introduction['#text'])
			return question.introduction['#text'];	
		return "";
	}

	DDGetDragText(question,index)
	{
		this.DDSanitizeOptions(question,index);
		if(!question.drags.drag[index]) 
			console.log("Index out of range!");
		else
			return question.drags.drag[index]['#text'];
	}
	
	IsDDCorrect(question,index,targetId)
	{		
		if(!targetId || index===null)return 0;

		//Clean question.Answer[targetId] for nulls. This is an issue in IE11
		if(question.Answer[targetId] && question.Answer[targetId].indexOf(null)>-1) question.Answer[targetId] = $.grep(question.Answer[targetId],function(o) {return o!=null});

		var drag=question.drags.drag[index];		
		if(!drag)return 0;
		//Incorrectly placed by user
		if(drag["@correct"]!=targetId)return -1;
		//Correctly placed by user
		if(drag["@correct"]==targetId && question.Answer[targetId].indexOf(index)>-1)return 1;
		//Showing where it should have been placed
		return 0;
	}
	//For multiple choice type questions
	IsOptionCorrect(question,option)
	{
		if(question['@type']=='multiplechoice') return option["@correct"]=="yes";
	}

	ToggleOption(question,option) 
	{
		if(this.IsSubmitted)return;
		var i=question.answers.answer.indexOf(option);
		if(!question.Answer.Checks)question.Answer.Checks=[];
		var index = question.Answer.Checks.indexOf(i);
		if(index==-1)question.Answer.Checks.push(i);
		else question.Answer.Checks.splice(index,1);
	}

	IsOptionChecked(question,option) 
	{
		var i=question.answers.answer.indexOf(option);
		return question.Answer.Checks && question.Answer.Checks.indexOf(i)>-1;
	}
	
	@Input() set content(content) {				
		var $this = this;			
		$this.Content = content;			
		$this.Init();		
		if(content==null)return;
		var quiz = this.PlayerService.Shared.XmlToJson($.parseXML(content.Xml));
		var questions = quiz.quiz.questions.question;
		if(questions && !$.isArray(questions))questions=[questions];
		console.log("Content.ObjectID: " + $this.Content.ObjectID);
		var scormData = $this.PlayerService.GetScormContentData($this.Content.ObjectID);
		if(!scormData.Quiz)scormData.Quiz=[];
		
		$.each(questions,function(i,question){
			if(!scormData.Quiz[i])scormData.Quiz.push({Score:null});
			if(!scormData.Quiz[i].Answer)scormData.Quiz[i].Answer={};
			question.Answer=scormData.Quiz[i].Answer;
			if(question.targets && question.targets.target && question.targets.target.constructor !== Array)question.targets.target=[question.targets.target];		

			$this.InitializeQuestion(question);
		});
		$this.Questions = questions;
		console.log(JSON.stringify($this.Questions))	
		$this.Init();			
		$this.UpdateScormScore();
	}

}