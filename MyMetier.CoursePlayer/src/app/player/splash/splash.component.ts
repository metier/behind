import { Component, Input } from "@angular/core";
import { PlayerService } from "app/player.service";
import { BaseContentComponent } from "../basecontent/basecontent.component";

declare var $: any;
@Component({
	selector: 'splash-content',
	templateUrl: './splash.component.html',
	styleUrls: ['./splash.component.scss'],
	providers: [PlayerService]
})
export class SplashComponent extends BaseContentComponent {
	public videoUrl = null;
	public course: string = null;	
	public lesson: string = null;
	public imageUrl: string = null;	
	public logoUrl: string = null;
	@Input() set content(content) {
		var $this = this;
		$this.Init();
		
		if($this.PlayerService.CourseData) $this.course = $this.PlayerService.CourseData.Name;
		if($this.PlayerService.Lesson) $this.lesson = $this.PlayerService.Lesson.Name;
		$this.imageUrl=($.grep(content.Content,function(o){return o.AuxField1=="splash";})[0] || {}).Filename;

	}
	constructor(public PlayerService:PlayerService){
		super(PlayerService);
	}
}