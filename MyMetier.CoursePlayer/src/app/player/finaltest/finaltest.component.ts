import { Component, ViewChild } from "@angular/core";
import { PlayerService } from "app/player.service";
import { BaseContentComponent } from "../basecontent/basecontent.component";
import { QuizComponent } from "../quiz/quiz.component";

declare var $: any;
@Component({
	selector: 'finaltest-content',
	templateUrl: './finaltest.component.html',	  	
})
export class FinaltestComponent extends BaseContentComponent {
	@ViewChild(QuizComponent) 
	private quizComponent: QuizComponent;
	public Content = null;	    
	public Step=null;
	public AttemptIndex=null;
	public IsSubmitted=false;
	private _Quizes=null;
	private _Attempts=[];
	public CurrentQuestion=0;
	public Questions=[];

	constructor(PlayerService: PlayerService) {	
		super(PlayerService);					
	}

	GetStep(){		
		if(this.PlayerService.PrintRequest){
			var quizes=this.PlayerService.Page.Content;
			if(quizes==null) return;
			var quiz = quizes[0];
			this.Content = quiz;
			this.CurrentQuestion=-1;
			return "Printing";
		}
		
		if(this.Step)return this.Step;
		if(!this.GetAttempts())return;
		if(this.GetRemainingAttempts()==0)return "Welcome-done";
		return "Welcome";		
	}
	
	GetNumberOfQuestions()
	{
		var quizes=this.GetQuizes();
		if(quizes!=null && quizes[0])return quizes[0].quiz.questions.question.length;
	}

	GetAttempts()
	{
		var $this=this;
		var scores=[];
		if(this.PlayerService){
			$.each(this.PlayerService.Page.Content,function(i,o)
			{
				var data=$this.PlayerService.GetScormContentData(o.ObjectID);				
				var dataFolder = $this.PlayerService.GetScormContentData(o.Filename);
				if(dataFolder.Quiz && (!data.Quiz || JSON.stringify(dataFolder).length > JSON.stringify(data).length))
				{
					//Legacy bug: Sometimes the final test attempt is tracked against the d_folders.ObjectID instead of d_content.ObjectID
					data = dataFolder;
					o.ObjectID = o.Filename;
				} 
				
				if(!data.Quiz) data=$this.PlayerService.GetScormContentData(o.ObjectID);			
				if(o.AuxField1=="quiz" && data && data.Score!=null){					
					scores.push({Index:scores.length,Score:data.Score || 0, ObjectID:o.ObjectID});
				}
			});			
		}
		if(JSON.stringify(this._Attempts)!=JSON.stringify(scores)) this._Attempts=scores;
		return this._Attempts;
	}

	ShowAttempt(Index)
	{
		this.IsSubmitted=true;		
		this.Step="Reviewing";		
		var attempt=this.GetAttempts()[Index];
		var quizes=this.PlayerService.Page.Content;
		if(quizes==null) return;
		this.AttemptIndex=Index;
		var quiz = $.grep(quizes,function(o){return o.ObjectID==attempt.ObjectID;})[0];
		this.Content = quiz;
		this.Questions = this.PlayerService.Shared.XmlToJson($.parseXML(quiz.Xml)).quiz.questions.question;
	}

	GotoStart(){
		this.AttemptIndex=null;
		this.Step="";
		this.IsSubmitted=false;
	}

	GetQuizes(){		
		var $this=this;
		if(!$this._Quizes && $this.PlayerService && $this.PlayerService.Page.Content){
			$this._Quizes=[];
			$.each($this.PlayerService.Page.Content,function(i,o){
				$this._Quizes.push($this.PlayerService.Shared.XmlToJson($.parseXML(o.Xml)));
			});
		}
		return $this._Quizes;
	}

	GetNumberOfFinalTestAttempts()
	{
		return (this.PlayerService ? this.PlayerService.GetNumberOfFinalTestAttempts() : 3);
	}

	GetRemainingAttempts()
	{
		return this.GetNumberOfFinalTestAttempts() - this.GetAttempts().length;
	}

	GetAttemptScore(Index){
		var attempts = this.GetAttempts();
		if(attempts!=null&&attempts.length>Index)return attempts[Index].Score;
	}

	StartNewAttempt(){		
		var $this=this;
		this.IsSubmitted=false;		
		var quizes=this.PlayerService.Page.Content;
		if(quizes==null) return;
		var quiz = $.grep(quizes,function(o){return !$this.PlayerService.GetScormContentData(o.ObjectID) || !$this.PlayerService.GetScormContentData(o.ObjectID).Score;})[0];		
		if(quiz){
			this.Step="Testing";			
			this.AttemptIndex = $this.GetAttempts().length;
			this.CurrentQuestion=0;
			this.Content = quiz;
			this.Questions = this.PlayerService.Shared.XmlToJson($.parseXML(quiz.Xml)).quiz.questions.question;
		
		}
	}

	SubmitTest(){
		var $this=this;
		this.quizComponent.SubmitFinalTest();
		this.Step="Submitted";
		this.IsSubmitted=true;
		this.CurrentQuestion=0;
		$(window).scrollTop(0);
		setTimeout(function()
		{
			$this.PlayerService.SetScormStatus('completed');
			$this.PlayerService.ScormCommit();
			//ScormCommit is async. Wait 10 seconds for it to finish
			setTimeout(() => 
			{
				$this.PlayerService.ScormTerminate();
			}, 10000);		
		},50);		
	}
}