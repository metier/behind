import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PlayerService } from '../../player.service';

declare var $: any;
@Component({
	selector: 'player-component',
	templateUrl: './player.component.html',
	providers: [PlayerService]
})
export class PlayerComponent implements OnInit, OnDestroy{			
	public DownloadOptions=[{id:'Notes','checked':true},{id:'Highlights','checked':true},{id:'ChecklistItems','checked':true}];
	public DownloadFormat="Download";
	public ContactMessage=null;
	public MessageSent=false;
	public DownloadResult=null;
	public DialogContent:any={};

	constructor(private route: ActivatedRoute,private router: Router, public translate: TranslateService, public PlayerService:PlayerService) { 
		var $this=this;
	}

	ngOnInit() {
		var $this = this;		
		$this.PlayerService.Initialize(this.route, this.router);
	}

	ngOnDestroy() {
		this.PlayerService.Destroy();		
	}

	public DownloadLesson(){
		this.PlayerService.ScormCommit();		
		var options=[];
		$.each(this.DownloadOptions,function(i,o){if(o.checked)options.push(o.id);});
		//If object structure changes, we also have to update CMS PDF conversion
		var request={
				PageId:this.PlayerService.PageId,
				LessonId:this.PlayerService.LessonId,
				Command:"Lesson",
				Options:options,				
				ScormData:this.PlayerService.GetScormSuspendData(),
				ToEmail: (this.DownloadFormat=="Email"?this.PlayerService.GetUserEmail():"") 
			};
		this.PlayerService.CreateDownload(request).then(json=>{
			this.DownloadResult=json;
		});	
	}

	public Visited = function(page){
		if(!this.PlayerService.Pages)return;
		return (this.PlayerService.Pages.indexOf(page) <= this.PlayerService.Pages.indexOf(this.PlayerService.Page));
	}
	public PrevPage = function(page){
		if(!this.PlayerService.Pages)return;
		var p=this.PlayerService.Pages.indexOf(page);
		if(p>0)return this.PlayerService.Pages[p-1];		
	}
	public NextPage = function(page){
		if(!this.PlayerService.Pages)return;
		var p=this.PlayerService.Pages.indexOf(page);
		if(p<this.PlayerService.Pages.length-1)return this.PlayerService.Pages[p+1];		
	}
	public SaveAndClose = function() {
		this.PlayerService.SaveAndClose();
	}
	public SubmitMessage = function () {	
		this.PlayerService.SendMessage({Body: this.ContactMessage});
	}	
}
