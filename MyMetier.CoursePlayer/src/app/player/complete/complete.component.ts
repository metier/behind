import { AfterViewInit, Component, Input } from "@angular/core";
import { PlayerService } from "app/player.service";
import { BaseContentComponent } from "../basecontent/basecontent.component";

declare var $: any;
@Component({
	selector: 'complete-content',
	templateUrl: './complete.component.html', 
	styleUrls: ['./complete.component.scss']
})
export class CompleteComponent extends BaseContentComponent implements AfterViewInit {		
	public CommentClicked=false;
	public CommentSent=false;
	public Comment="";

	constructor(public PlayerService:PlayerService){
		super(PlayerService);
	}

	ngAfterViewInit(){
		this.PlayerService.SetElementLoaded("Content");
		$('.stars input').change(function () {			
		  var $radio = $(this);
		  $('.stars .selected').removeClass('selected');
		  $radio.closest('label').addClass('selected');
		});
	}
	
	public SaveAndClose = function() {
		this.PlayerService.SaveAndClose();
	}

	public SubmitFeedback=function () 
	{
		var $this=this;
		setTimeout(function()
		{			
			$.ajax({
			    url: "../tools/sendmail.aspx?action=eval",
			    async: false,
			    type: "POST",
			    data: { comment: $this.Comment, score: $(".stars label.selected").attr("data-value"), username: $this.PlayerService.GetScormLearnerId(), course_code: $this.PlayerService.GetCourseCode(), CourseId: $this.PlayerService.CourseId, LessonId: $this.PlayerService.LessonId, lesson_number: ($this.PlayerService.GetLessonIndex()+1) }
            });	
		 },100);
	}

	@Input() set content(content) {
		var $this = this;		
		$this.Content = content;
		$this.Init();			
	}		
}