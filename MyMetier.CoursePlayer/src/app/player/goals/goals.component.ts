import { Component, Input } from "@angular/core";
import { PlayerService } from "app/player.service";
import { BaseContentComponent } from "../basecontent/basecontent.component";

declare var $: any;

@Component({
	selector: 'goals-content',
	templateUrl: './goals.component.html',
    providers: [PlayerService]
})

export class GoalsComponent extends BaseContentComponent {
	public goals = null;
	public intro: string = null;	
	
	constructor(public PlayerService:PlayerService){
		super(PlayerService);
	}
	
	@Input() set content(content) {
		var $this = this;
		$this.Content = content;
		
		$this.goals = [];		
		var html = "<div>" + content.Xml + "</div>";
		$(html).find("ul:not(li ul)>li").each(function(i, o) {
			$this.goals.push($(this).html());
		});
		$this.intro = $(html).find("ul").detach().end().html();
		$this.Init();
	}
}