import { Component, Input } from "@angular/core";
import { PlayerService } from "app/player.service";
import { BaseContentComponent } from "../basecontent/basecontent.component";

declare var $: any;
@Component({
	selector: 'pitfalls-content',
	templateUrl: './pitfalls.component.html',
	styleUrls: ['./pitfalls.component.scss'],
    providers: [PlayerService]
})
export class PitfallsComponent extends BaseContentComponent {
	public pitfalls = null;
	constructor(public PlayerService:PlayerService){
		super(PlayerService);
	}
	
	@Input() set content(content) {
		var $this = this;
		$this.Content = content;
		$this.pitfalls = [];

		$("<div>" + content.Xml + "</div>").find("ul:not(li ul)>li").each(function(i, o) { 
			$this.pitfalls.push($(this).html());
		});
		$this.Init();
	}
}