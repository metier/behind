export * from './case/case.component';
export * from './checklist/checklist.component';
export * from './complete/complete.component';
export * from './finaltest/finaltest.component';
export * from './goals/goals.component';
export * from './pedagogical/pedagogical.component';
export * from './pitfalls/pitfalls.component';
export * from './player-navigation/player-navigation.component';
export * from './player/player.component';
export * from './quiz/quiz.component';
export * from './reflection/reflection.component';
export * from './splash/splash.component';
export * from './theory/theory.component';

