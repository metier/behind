import { Component, Input } from "@angular/core";
import { PlayerService } from "app/player.service";
import { BaseContentComponent } from "../basecontent/basecontent.component";

declare var $: any;
@Component({
	selector: 'reflection-content',
	templateUrl: './reflection.component.html',
	styleUrls: ['./reflection.component.scss'],
    providers: [PlayerService]
})
export class ReflectionComponent extends BaseContentComponent {
	public reflections = null;
	public intro: string = null;
	constructor(public PlayerService:PlayerService){
		super(PlayerService);
	}
	
	@Input() set content(content) {
		var $this = this;
		$this.Content = content;
		
		$this.reflections = [];
		var html = "<div>" + content.Xml + "</div>";
		$(html).find("ul:not(li ul)>li").each(function(i, o) {
			$this.reflections.push({text:$(this).html(), id: $(o).attr("target")});
		});
		$this.intro = $(html).find("ul").detach().end().html();
		$this.Init();		
		if(!$this.ScormData.Reflections)$this.ScormData.Reflections={};
	}
}