import { AfterViewInit } from "@angular/core";
import { PlayerService } from "app/player.service";
export class BaseContentComponent implements AfterViewInit {	
	public ScormData = null;
	public Content = null;
	
	constructor(public PlayerService:PlayerService){}

	ngAfterViewInit() {
		this.PlayerService.SetElementLoaded("Content");		
	}

	public GetTitle = function () {
		if(this.Content && this.Content.Mode!="nohead")return this.Content.Name;
	};

	public GetPageTitle = function () {		
		if(this.PlayerService && this.PlayerService.Page)return this.PlayerService.Page.Name;
	};

	public ScormSave = function () {}

	public Init() {
		var $this = this;			
		
		if($this.Content)
			$this.ScormData = $this.PlayerService.GetScormContentData($this.Content.ObjectID);		
	}
}