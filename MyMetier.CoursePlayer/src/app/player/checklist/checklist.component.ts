import { Component, Input } from "@angular/core";
import { PlayerService } from "app/player.service";
import { BaseContentComponent } from "../basecontent/basecontent.component";

declare var $: any;
@Component({
	selector: 'checklist-content',
	templateUrl: './checklist.component.html',
	styleUrls: ['./checklist.component.scss'],
})
export class ChecklistComponent extends BaseContentComponent {
	public Checklist = null;
	public Intro: string = null;
	private ChecklistTextMap={};

	constructor(public PlayerService:PlayerService){
		super(PlayerService);
	}

	public ChecklistDelete = function(check){
		this.Checklist.splice(this.Checklist.indexOf(check),1);
		delete this.ScormData.Checklist[check.id];
		this.ChecklistChange();		
	}
	public ChecklistChange = function () {		
		if(!this.Checklist[this.Checklist.length-1] || (this.Checklist[this.Checklist.length-1].locked || this.Checklist[this.Checklist.length-1].text))
			this.AddToChecklist("");			
	}
	public GetTextForLockedCheck=function (check) {
		return this.ChecklistTextMap[check.id];
	}

	@Input() set content(content) {		
		var $this = this;
		$this.Content = content;
						
		$this.Init();			
		if(!$this.ScormData.Checklist) $this.ScormData.Checklist={};

		$this.Checklist = [];
		var html = "<div>" + content.Xml + "</div>";
		$(html).find("ul:not(li ul)>li").each(function(i, o) {
			var id=""+i;
			var existing=$this.ScormData.Checklist[id];
			$this.AddToChecklist($(this).html(),id,(existing ? existing.checked : false), (existing ? existing.locked : true));
		});	
		if(!this.PlayerService.PrintRequest || !this.PlayerService.PrintRequest.Options || this.PlayerService.PrintRequest.Options.indexOf("ChecklistItems")>-1)
			for(var key in $this.ScormData.Checklist){
				var check = $this.ScormData.Checklist[key];
				if(key.indexOf("-")>-1 && check.text)
					this.AddToChecklist(check.text,check.id,check.checked,check.locked);
			}
		if(!this.PlayerService.PrintRequest) this.ChecklistChange();
		$this.Intro = $(html).find("ul").detach().end().html();
	}

	private AddToChecklist= function(text,id, checked,locked){
		//If Id exists, reuse
		id = (id?id:this.PlayerService.GenerateGuid());
		var item=$.grep(this.Checklist,function(i,o){return o.id==id;})[0];
		
		if(!item){
			item={checked:(checked?true:false), id: id,locked:locked};			
			this.Checklist.push(item);	
		}

		if(!locked)item.text=text;

		this.ScormData.Checklist[item.id]=item;
		this.ChecklistTextMap[item.id]=text;
		
		return item;
	};
}