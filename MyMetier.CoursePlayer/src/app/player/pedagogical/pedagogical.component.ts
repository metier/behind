import { Component, OnInit } from "@angular/core";
import { PlayerService } from "app/player.service";

declare var $: any;

@Component({
	selector: 'pedagogical-tools',
	templateUrl: './pedagogical.component.html'	
})
export class PedagogicalComponent implements OnInit{	
	public highlight={active:false,color:'blue'};
	public note={active:false};
	
	constructor(public PlayerService: PlayerService){}

	ngOnInit(){
		this.PlayerService.PedagogicalTools=this;
	}
	
	public HighlightClick=function () {
		this.note.active=false;
		this.highlight.active=!this.highlight.active;			
		this.SetBodyClass(this.highlight.active ? "highlight":"");		
	}

	public NoteClick=function (status:string = null) {		
		this.highlight.active=false;
		if(this.PlayerService.IsTouchDevice()){
			if(status==null){
				var element = this.PlayerService.GetNearestWord($(window).height()/2);				
				if(element){
					var content = $(element).parents(".container");
					var contentId=content.attr("data-id");
					var index=$(content).find(".word").index(element);
					this.PlayerService.AddNote(contentId, index,null);
				}
			}
		}else{
			if(status!=null)
				this.note.active=status;
			else
				this.note.active=!this.note.active;								
		}
		this.SetBodyClass(this.note.active ? "note":"");
	}

	public SetColor=function (color) {
		this.highlight.color=color;
	}

	public SetBodyClass=function(className){
		this.PlayerService.SetBodyClass(className);	
	}

	public Reset=function() {
		this.highlight.active=false;
		this.note.active=false;
		this.SetBodyClass("");
	}
}