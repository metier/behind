import { Component, AfterViewInit, Input } from "@angular/core";
import { PlayerService } from "app/player.service";
import { BaseContentComponent } from "../basecontent/basecontent.component";

declare var $: any;
declare var replaceAll: any;
@Component({
	selector: 'theory-content',
	templateUrl: './theory.component.html',
	providers: [PlayerService]
})
export class TheoryComponent extends BaseContentComponent implements AfterViewInit {
	public html: string = null;			
	private AfterViewInitCalled:boolean = false;

	constructor(public PlayerService:PlayerService){
		super(PlayerService);
	}

	ngAfterViewInit(){
		this.AfterViewInitCalled=true;
		if(this.html)this.PostLoad();
	}

	@Input() set content(content){
		var $this = this;
		$this.Content = content;
		$this.Init();
		
		var html = $("<div>" + content.Xml + "</div>");
        
		//Cms-questions
		var reflections = this.PlayerService.GetScormContentDataByType("Reflections");
		html.find(".cms-question").each(function(i,o) {
			var id=$(o).addClass("prevent-hl").attr("id");
			var reflection="";
			$.each(reflections,function(i,o){if(o[id])reflection=o[id]; });
			$(o).append($("<div class='answer'>" + replaceAll(reflection,"\n","<br />") + "</div>"));						
			if(!reflection && !$this.PlayerService.Preview)
				$(o).addClass("hide");						
		});
		
		
		this.PlayerService.TranslateService.get("THEORY.YOUR_ANSWER").subscribe((res: string) => {
			html.find(".cms-question .title").html(res);
		
			html.find("[contenteditable]").removeAttr("contenteditable");


			 //Initialize
		    var text_nodes = $(html).contents().filter(function () {
		        return this.nodeType == 3 && this.nodeValue.match(/[a-zA-Z]{2,}/);
		    });
		    text_nodes.replaceWith(function () {
		        return "<span>" + $(this).text() + "</span>";        
		    });
		    text_nodes = $(html).find("*:not(iframe)").not(".prevent-hl *").contents().filter(function () {
		        return this.nodeType == 3 && this.nodeValue.match(/[a-zA-Z]{2,}/);
		    });
		    if (text_nodes.length > 0) {
		        var words = text_nodes.replaceWith(function (i) {	           
		        	var isSpace=function(str,index){
		        		return [32,160].indexOf(str.charCodeAt(index))>-1;
		        	}	        	
		            var t = $(this).text();
		            var s = t.split("\n").join(" ").split("\r").join(" ").split("\t").join("").split("        ").join(" ").split("    ").join(" ").split("  ").join(" ").split("  ").join(" ").split("  ").join(" ").split("  ").join(" ").split("  ").join(" ").split("  ").join(" ").split("  ").join(" ").trim();
		            return "<span class='word'>" + (isSpace(t,0) ? " " : "") + s.split(" ").join(" </span><span class='word'>") + (isSpace(t, t.length - 1) ? " " : "") + "</span>";
		        });
		    }

			$this.html = html.html();
			if($this.AfterViewInitCalled) $this.PostLoad();
		});	

	};
	
	private PostLoad(){
		var $this=this;
		setTimeout(function()
		{			
			$this.PlayerService.SetElementLoaded("Content");
			$this.PlayerService.AddBlockToPedagogicalTools($(".container[data-id=" + $this.Content.ObjectID + "]")[0], $this.Content.ObjectID);		
			$this.PlayerService.PrepareImg($(".container[data-id=" + $this.Content.ObjectID + "]").find("img:not(.no-process), iframe:not(.no-process)"));   
			$this.PlayerService.PrepareTable($(".container[data-id=" + $this.Content.ObjectID + "]").find("table"));     					
			$this.PlayerService.AddDictionaryToSection($(".container[data-id=" + $this.Content.ObjectID + "]").find(".word"));
		},1);
	}
}