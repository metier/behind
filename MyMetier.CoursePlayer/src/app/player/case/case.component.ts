import { Component, Input } from "@angular/core";
import { PlayerService } from "app/player.service";
import { BaseContentComponent } from "../basecontent/basecontent.component";

declare var $: any;
@Component({
	selector: 'case-content',
	templateUrl: './case.component.html',
	styleUrls: ['./case.component.scss']	
})
export class CaseComponent extends BaseContentComponent {
	public cases = null;
	public intro = null;
	constructor(public PlayerService:PlayerService){
		super(PlayerService);
	}
	
	@Input() set content(content) {
		var $this = this;
		$this.Content = content;
		$this.Init();

		$this.cases = [];
		var html = $("<div>" + content.Xml + "</div>");

		var q=html.find("div.case-question");
		var a=html.find("div.case-answer");
		
		q.each(function(i, o) { 
			$this.cases.push({question: $(this).html(), answer:a.eq(i).html(), id: 'case' + i, _showAnswer: $this.PlayerService.PrintRequest});
		});
		q.remove();
		a.remove();
	 	$this.intro = html.html();
	 	
	 	if(!$this.ScormData.Cases)$this.ScormData.Cases={};
	}

	public tryToMoveScroll(sectionName, attempt) {
		var section = document.getElementById(sectionName);
		if(section)
			window.scrollTo(0, section.offsetTop);
		else{
			if(attempt < 5) {
				var $this = this;
				setTimeout(function () {
					$this.tryToMoveScroll(sectionName, attempt++);
				}, 100);
			}
		}
	}

	public goToSection(sectionName, selectedCase): void {
		selectedCase._showAnswer=!selectedCase._showAnswer;
		if(selectedCase._showAnswer) {
			var $this = this;
			$this.tryToMoveScroll(sectionName, 0);	
		}
		
	}
}