import { Component, Input } from "@angular/core";
import { PlayerService } from "app/player.service";
@Component({
	selector: 'player-navigation',
	templateUrl: './player-navigation.component.html',
    providers: [PlayerService]
})
export class PlayerNavigationComponent {	
	@Input() page;
	public hover=false;

	constructor(public PlayerService:PlayerService){}

	public GetIconUrl = function(hover) {

		var file = "";
		if (this.page.AuxField1 == 'complete') file="fullfor";		
		if (this.page.AuxField1 == 'examples') file="eksempler";		
		if (this.page.AuxField1 == 'intro') file = "innledning";		
		if (this.page.AuxField1 == 'quiz') file = "quiz";		
		if (this.page.AuxField1 == 'checklist') file = "sjekkliste";		
		if (this.page.AuxField1 == 'reflection') file = "tankevekkere";		
		if (this.page.AuxField1 == 'theory') file = "teori";
		if (this.page.AuxField1 == 'pitfalls') file = "pitfalls";
		if (this.page.AuxField1 == 'case') file = "case";
		if(file)
			return "./assets/player/img/nav_icons/" + file + (hover?"_active":"") + ("pitfalls,case".indexOf(file)>-1?".png": ".svg");		
	}	

	public GotoPage(PageId){
		this.PlayerService.GotoPage(PageId);
	}
}