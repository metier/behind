import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { LocalStorageService } from "angular-2-local-storage";
import { Observable, Subject, throwError } from "rxjs";
import { catchError, map, timeout } from "rxjs/operators";
import { SharedService } from "./shared/shared.service";

declare var _: any;
declare var $: any;
declare var rangy: any;
declare var replaceAll: any;
declare var cInt: any;
declare var cStr: any;
declare var fnSendEmail: any;
declare var document: any;
declare var window: any;
declare var clsScorm2004API: any;
declare var clsScorm2004Local: any;
declare var evoPdfConverter: any;
@Injectable()
export class PlayerService {
  static instance: PlayerService;
  static isCreating: Boolean = false;
  public static BlockTouchScroll = false;
  public PedagogicalTools = null;
  private PedagogicalBlocks = [];

  public PageId = null;
  public LessonId = null;
  private DownloadedLessonId = null;
  public Lesson = null;
  public CourseId = null;
  public CourseData = null;
  public PlayerSettings = null;
  public Page = null;
  public Pages = null;
  private PagesToPrint = null;
  public SplashPage = null;
  public Dialogs = [];
  public PrintRequest: any = null;
  private PageLoadStatus = {};
  public LanguageCode = "en";

  private Route = null;
  private Router = null;
  //private Http=null;

  private ScormData = null;
  private ScormLookupPerformed = false;
  private ScormAPI = null;
  private ScormKeepAliveTimer = null;
  public Preview = false;
  private API = null;
  private _CourseChange = new Subject<string>();
  private Loaded = false;
  private TableCounter = 0;
  private VideoCounter = 0;
  private ImageCounter = 0;
  private NonLoadedImages = 0;

  private NavSubscription;
  private QuerySubscription;
  private CourseSubscription;
  private AutoScroller;
  private MousePosition;

  public CompletedLessons = 0;
  public Lessons = [];
  public TotalLessons = 0;
  public CourseScore = 0;
  public CourseTotalScore = 0;
  public TranslateService;
  public Shared: SharedService = new SharedService();
  public CurrentActivityId = 0;
  private PhoenixApiUrl = "../../api";
  private LearningPortalApiUrl = "../../learningportal/api";
  public IsDemo = false;
  private CourseDictionary = null;

  CourseChange = this._CourseChange.asObservable();

  constructor(
    private HttpClient: HttpClient,
    private StorageService: LocalStorageService
  ) {
    if (PlayerService.instance) return PlayerService.instance;
    PlayerService.instance = this;

    var $this = this;
    $this.ScormData = { Content: {} };
    $this.Lesson = {};
    $this.Page = {};

    var dimmedTimer;
    var fnSetDimTimerIfNecessary = function () {
      if (dimmedTimer) clearTimeout(dimmedTimer);
      $("#arrow-left, #arrow-right").removeClass("dimmed");
      if ($(window).width() < 1024)
        dimmedTimer = setTimeout(function () {
          $("#arrow-left, #arrow-right").addClass("dimmed");
        }, 1000);
    };
    $(window)
      .resize(function () {
        fnSetDimTimerIfNecessary();
        var w = $(".metierblock-content").width();
        if (w) {
          var m = Math.max(0, ($(window).width() - w) / 2);
          $(".metierblock-content")
            .css("padding-left", m)
            .css("padding-right", m);
          $(".metierblock").css("margin-left", -m).css("margin-right", -m);
        }
      })
      .scroll(function () {
        fnSetDimTimerIfNecessary();
      });
    $(document)
      .on("mouseover", "#arrow-left, #arrow-right", function () {
        $(this).css("opacity", 1);
      })
      .on("mouseout", "#arrow-left, #arrow-right", function () {
        $(this).css("opacity", "");
      });
    //Touch support
    if ($this.IsTouchDevice()) {
      $(window).on("touchmove", function (e) {
        if (PlayerService.BlockTouchScroll) {
          e.preventDefault();
        }
      });
      $(window).on("touchend", function (e) {
        PlayerService.BlockTouchScroll = false;
      });
      $(document).on("touchend", ".colors", function (e) {
        $(e.target).trigger("click", e);
      });
      $(document).on("touchstart", ".colors", function (e) {
        return false;
      });
      $(window).on("touchend", function (e) {
        console.log(e);
        if (
          e.target.id != "button-confirm-hl" &&
          $(e.target).parents(".colors").length == 0
        )
          $("#confirm-hl").detach();
      });
      document.addEventListener("selectionchange", function (e) {
        //console.log(e);
        //$("h1").html((new Date()).getSeconds());
        var selection = window.getSelection
          ? window.getSelection()
          : document.selection;
        if (selection.rangeCount > 0) {
          var element = $(selection.getRangeAt(0).startContainer);
          $.each(PlayerService.instance.PedagogicalBlocks, function (i, o) {
            if (element.parents(o).length > 0)
              $(o).trigger("metierselectionchange");
          });
        }
      });
    } else {
      $(window).on("mousemove", function (e) {
        PlayerService.instance.MousePosition = { X: e.pageX, Y: e.pageY };
      });
      $(window).on("mousedown", function (e) {
        clearInterval(PlayerService.instance.AutoScroller);
        PlayerService.instance.AutoScroller = setInterval(function () {
          if (
            PlayerService.BlockTouchScroll &&
            PlayerService.instance.MousePosition
          ) {
            var distanceFromBottom =
              $(document).scrollTop() +
              $(window).height() -
              PlayerService.instance.MousePosition.Y;
            var distanceFromTop =
              PlayerService.instance.MousePosition.Y - $(document).scrollTop();
            if (distanceFromBottom < 100)
              $(window).scrollTop($(window).scrollTop() + 30);
            if (distanceFromTop < 200)
              $(window).scrollTop($(window).scrollTop() - 30);
          }
        }, 200);
      });
      $(window).on("mouseup", function (e) {
        clearInterval(PlayerService.instance.AutoScroller);
      });
    }

    /* Notes stuff */
    $("body")
      .on("mouseup.hl touchend.hl", function (e) {
        var note = $(document).data("dragging-note");
        if (note) {
          var scormNote = $(note).data("scormNote");
          var element = $this.GetNearestWord(e.clientY);
          var content = $(element).parents(".container");
          scormNote.WordIndex = $(content).find(".word").index(element);
          scormNote.ContentId = content.attr("data-id");

          //if (Element) { var Key = fnGetKey(Element, Element); $(o).data("note").Key = Key.Instance + "§" + Key.Text; }
          $(document).data("dragging-note", null);
          //todo:commit
        }
      })
      .on("mousemove.hl touchmove.hl", function (e) {
        var o = $(document).data("dragging-note");
        if (o) {
          e.preventDefault();
          $(o).data("pos", {
            y:
              e.clientY |
              (e.originalEvent &&
              e.originalEvent.touches &&
              e.originalEvent.touches.length > 0
                ? e.originalEvent.touches[0].clientY
                : 0),
          });
          var Element = $this.GetNearestWord($(o).data("pos").y);
          if (Element) $(Element).after(o);
          if (window.getSelection) window.getSelection().removeAllRanges();
          else document.selection.empty();
        }
      })
      .mousemove(function (e) {
        if ($("body").hasClass("note")) {
          $("#add-new-note").detach();
          var element = $this.GetNearestWord(e.clientY);
          //console.log("element: " +(element==null?"null" : $(element.)
          if (element) {
            $(element).after(
              "<div id='add-new-note'><i class='material-icons'>note</i></div>"
            );
          }
        }
      })
      .mousedown(function (e) {
        if ($("body").hasClass("note") && !$(e.target).is("#note")) {
          var element = $this.GetNearestWord(e.clientY);
          if (element) {
            var content = $(element).parents(".container");
            var contentId = content.attr("data-id");
            var index = $(content).find(".word").index(element);
            $this.AddNote(contentId, index, null);
          }
          $this.PedagogicalTools.NoteClick(false);
          //Todo: Unselect note function
        }
      });

    //$(".wrapper.main-text").on("mouseup.hl", function (e) { var s = cStr($("body").attr("class")); fnMouseUp(s); if (s.indexOf("cursor-hl-") > -1) fnHighlight(_this.Selection.Key, s); });
  }
  /*
        static GetInstance() {
            if (PlayerService.instance == null) {
                PlayerService.isCreating = true;
                PlayerService.instance = new PlayerService();
                PlayerService.isCreating = false;            
            }
     
            return PlayerService.instance;
        }   */

  public Initialize(Route, Router) {
    var $this = this;
    this.Route = Route;
    this.Router = Router;
    this.IsDemo = this.Router.url.indexOf("/demo/") > -1;

    this.QuerySubscription = this.Route.queryParams.subscribe((params) => {
      if (params["PrintRequest"]) {
        $this.PrintRequest = JSON.parse(params["PrintRequest"]);
        $this.ScormData = this.ParseSuspendData($this.PrintRequest.ScormData);

        $this.ScormLookupPerformed = true;

        $this.ScormAPI = {
          OfflineData: $this.ScormData,
          APIFields: {
            SESSION_TIME: "cmi.session_time",
            LOCATION: "cmi.location",
            SCORE_RAW: "cmi.score.raw",
            SUSPEND_DATA: "cmi.suspend_data",
            COMPLETION_STATUS: "cmi.completion_status",
            ENTRY: "cmi.entry",
            LEARNER_NAME: "cmi.learner_name",
            LEARNER_ID: "cmi.learner_id",
            CREDIT: "cmi.credit",
            TOTAL_TIME: "cmi.total_time",
          },
        };
      }
      $this.Preview = params["Preview"] == "true";
    });

    if (!this.Loaded) {
      this.ScormInitialize();
      this.CourseId = this.Route.params.value.CourseId;
    }

    this.NavSubscription = this.Route.params.subscribe((params) => {
      $this.PageId = +params["PageId"];
      $this.LessonId = +params["LessonId"];
      $this.CourseId = +params["CourseId"];
      $this.ActivatePage();

      if (
        $this.DownloadedLessonId &&
        $this.LessonId != $this.DownloadedLessonId
      )
        $this.GetLesson($this.LessonId);
    });

    $this.CourseSubscription = $this.CourseChange.subscribe((course) => {});
    $this.GetLesson($this.LessonId);

    this.Loaded = true;
    if (!this.IsDemo) window.onbeforeunloadPrompt = true;
  }

  public Destroy() {
    if (this.NavSubscription) this.NavSubscription.unsubscribe();
    if (this.QuerySubscription) this.QuerySubscription.unsubscribe();
    if (this.CourseSubscription) this.CourseSubscription.unsubscribe();
    console.log("PlayerService:Destroy()");
  }
  public Unload() {
    this.Destroy();
    this.Loaded = false;
    this.DownloadedLessonId = null;
  }
  public GotoLesson(LessonId) {
    var lesson = _.find(this.Lessons, { ObjectID: LessonId });
    if (lesson && lesson.Folders.length == 0) {
      this.TranslateService.get("DEMO.LESSON_NOT_IN_DEMO").subscribe(
        (res: string) => {
          alert(res);
        }
      );
      return;
    }
    this.GotoUrl(
      "/course/" +
        (this.IsDemo ? "demo/" : "") +
        this.CourseId +
        "/lesson/" +
        LessonId
    );
  }
  public GotoPage(PageId) {
    var page = this.FindPage(PageId, null);
    this.GotoUrl(
      "/course/" +
        (this.IsDemo ? "demo/" : "") +
        this.CourseId +
        "/lesson/" +
        page.Parent.ObjectID +
        "/page/" +
        PageId
    );
  }
  public GotoUrl(Url, DoScormCommit = null) {
    var $this = this;
    $this.Shared.AddSpinner("GotoUrl");
    setTimeout(function () {
      //Only commit if active API session or DoScormCommit == true
      if (
        DoScormCommit == true ||
        (DoScormCommit == null && $this.GetRcoIdForApi() != -1)
      )
        $this.ScormCommit();

      //this.Router.navigate(['../', { page:PageId}],{relativeTo: this.Route });
      $this.Router.navigateByUrl(Url);
      console.log("GotoUrl: " + Url);
      $this.SplashPage = null;
      $this.TableCounter = 0;
      $this.VideoCounter = 0;
      $this.ImageCounter = 0;
      $this.NonLoadedImages = 0;
      $this.PedagogicalBlocks = [];
      if ($this.PedagogicalTools) $this.PedagogicalTools.Reset();
      $this.Shared.ClearSpinner("GotoUrl");
    }, 50);
  }
  public FindPage(PageId, Parent) {
    var $this = this;
    if (Parent == null) Parent = $this.CourseData;
    if (Parent == null)
      console.warn("FindPage executing and $this.CourseData is NULL");
    if (Parent.ObjectID == PageId) return Parent;
    var foundpage = null;
    if (Parent.Folders)
      $.each(Parent.Folders, function (i, o) {
        if (foundpage == null && o.ObjectID == PageId) foundpage = o;
        if (foundpage == null) foundpage = $this.FindPage(PageId, o);
      });
    //console.log("findpage: " + Parent.ObjectID + " " + (foundpage?"FOUND":""))
    return foundpage;
  }
  public GetNextLesson() {
    var $this = this;
    var lesson = $.grep(this.CourseData.Folders, function (o) {
      return o.ObjectID == $this.LessonId;
    })[0];
    if (!lesson && $this.LessonId > 0) return null;
    if (!lesson) return this.CourseData.Folders[0];
    while (lesson != null) {
      lesson =
        this.CourseData.Folders[this.CourseData.Folders.indexOf(lesson) + 1];
      if (
        lesson &&
        lesson.AuxField1 != "global" &&
        lesson.AuxField1 != "dictionary"
      )
        return lesson;
    }
  }

  public PrepareImg(elements) {
    var $this = this;
    var fnZoomEnabled = function (Img) {
      return !$(Img).is(".no-zoom"); // && (_Player.Player.Settings.Player.AutoZoomImages || $(Img).is(".do-zoom"));
    };

    $this.TranslateService.get(["THEORY.IMAGE", "THEORY.VIDEO"]).subscribe(
      (translations: string) => {
        elements.each(function (i, o) {
          var Alt = $(o).attr("alt");
          var Max = { w: $(o).attr("zoom-width"), h: $(o).attr("zoom-height") };
          var Min = {
            w: $(o).attr("normal-width"),
            h: $(o).attr("normal-height"),
          };
          if ($(o).is("iframe")) {
            var Alt = $(o).attr("title");
            if (Alt) {
              var isVideo = ("" + $(o).attr("src")).indexOf("vimeo") > -1;
              if (isVideo) {
                $this.VideoCounter++;
                var label = translations["THEORY.VIDEO"].replace(
                  "{0}",
                  "" + $this.VideoCounter
                );
              } else {
                $this.ImageCounter++;
                var label = translations["THEORY.IMAGE"].replace(
                  "{0}",
                  "" + $this.ImageCounter
                );
              }
              if ($(o).next().is("br")) $(o).next().remove();
              $(o)
                .wrap("<div class='image-caption' />")
                .wrap("<div class='img' />");
              $("<div class='title caption' style='font-style: italic;'/>")
                .html(label + " - " + Alt)
                .appendTo($(o).parent());
            }
          } else if ($(o).is("img")) {
            if (fnZoomEnabled(o))
              $(o).css("width", "auto").css("height", "auto");
            if (Alt) $this.ImageCounter++;

            var fnFixImage = function (Alt) {
              if ($(o).next().is("br")) $(o).next().remove();
              $(o)
                .wrap(
                  "<div class='image-caption " +
                    replaceAll(" " + $(o).attr("class"), " border", " ") +
                    "' />"
                )
                .wrap("<div class='img' />");
              if ($(o).is(".right, .left")) {
                var w = $(o).attr("width") || $(o).css("width");
                if (w) $(o).parents(".image-caption:eq(0)").css("width", w);
              }

              if (fnZoomEnabled(o)) {
                var _w = $(o).css("width").split("px").join("");
                var _h = $(o).css("height").split("px").join("");
                if (_w > 500 || _h > 300 || Max.w || Max.h || Min.w || Min.h) {
                  if (cInt(Max.w, Max.h) == 0 && (_w > 900 || _h > 900)) {
                    if (_w / 900 > _h / 900) Max.w = 900;
                    else Max.h = 900;
                  }
                  if (cInt(Max.w, Max.h) > 0) {
                    if (cInt(Max.h) > 0 && cInt(Max.w) > 0) {
                      _w = cInt(Max.w);
                      _h = cInt(Max.h);
                    } else if (cInt(Max.h) > 0) {
                      _w = _w * (cInt(Max.h) / _h);
                      _h = Max.h;
                    } else if (cInt(Max.w) > 0) {
                      _h = _h * (cInt(Max.w) / _w);
                      _w = Max.w;
                    }
                  }
                  var shrink = {
                    w: cStr(Min.w, "auto"),
                    h: cStr(Min.h, "auto"),
                  };
                  var fnCalculateShrink = function (print) {
                    var defaultW = print ? 800 : 500;
                    var defaultH = print ? 500 : 300;
                    if (_w <= defaultW && _h > defaultH)
                      shrink.h = Math.min(_h, defaultH);
                    else shrink.w = Math.min(_w, defaultW);
                  };

                  if (cInt(Min.w, Min.h) == 0) fnCalculateShrink(false);

                  var _resize = function (Shrink) {
                    //Recalc for print. We first need to calcululate for non-print, so that we can determine if zoom_in would show in non-print (for correct yellow marker printing)
                    if ($this.PrintRequest) fnCalculateShrink(true);

                    if (Shrink)
                      $(o)
                        .css("width", shrink.w)
                        .css("height", shrink.h)
                        .parent()
                        .css("left", "0");
                    else
                      $(o)
                        .css("width", cStr(Max.w, "auto"))
                        .css("height", cStr(Max.h, "auto"))
                        .parent();
                    if ($(o).width() > 850)
                      $(o)
                        .parent()
                        .css("left", (-$(o).width() + 850) / 2);
                    $(o).data("expanded", Shrink ? 0 : 1);
                    $(o)
                      .parent()
                      .parent()
                      .find(".material-icons")
                      .html(Shrink ? "zoom_in" : "zoom_out");
                  };

                  if (
                    !(
                      (cInt(Min.w, Min.h) > 0 &&
                        Max.w == Min.w &&
                        Max.h == Min.h) ||
                      Math.abs(_w - shrink.w) < 40 ||
                      Math.abs(_h - shrink.h) < 40
                    )
                  ) {
                    $(o)
                      .parent()
                      .parent()
                      .addClass("auto-expand")
                      .click(function () {
                        var i = $(o).data("expanded");
                        if (i == 1) {
                          _resize(true);
                        } else {
                          _resize(false);
                        }
                      });
                  }
                  _resize(true);
                }
              }
              if (Alt || $(o).parents(".auto-expand").length > 0) {
                var title = $("<div class='title'/>").appendTo(
                  $(o).parent().parent()
                );
                if ($(o).parents(".auto-expand").length > 0)
                  title.append("<i class='material-icons'>zoom_in</i>");
                if (Alt)
                  title
                    .addClass("caption")
                    .append(
                      "<div style='font-style: italic;'>" +
                        translations["THEORY.IMAGE"].replace(
                          "{0}",
                          $(o).data("image-index")
                        ) +
                        " - " +
                        Alt +
                        "</div>"
                    );
              }
            };
            $(o).data("image-index", $this.ImageCounter);

            if (
              !$(o).prop("complete") &&
              cInt(replaceAll($(o).css("width"), "px", "")) == 0
            ) {
              //$(o).css("visibility","hidden");
              console.debug("Image queued for processing: " + $(o).prop("src"));
              $this.NonLoadedImages++;
              $(o).on("load", function () {
                fnFixImage(Alt);
                $this.NonLoadedImages--;
                $(o).css("visibility", "");
                //$(o).addClass("wow fadeIn");
              });
            } else {
              fnFixImage(Alt);
            }
          }
        });
      }
    );
  }
  public PrepareTable(elements) {
    var $this = this;
    $this.TranslateService.get("THEORY.TABLE").subscribe((res: string) => {
      elements.each(function (i, o) {
        if (
          $(this).find("tr").length > 1 &&
          $(this).find("thead tr, th").length == 0 &&
          !/rowspan/i.test($(this).find("tr:eq(0)").html())
        ) {
          var h = $("<thead />").prependTo(this);
          $(this)
            .find("tr:eq(0) td")
            .each(function (i, o) {
              h.append(o);
            });
        }
        if ($(o).is("[summary][summary!='']"))
          $("<div class='image-caption' />").append($(
            "<div class='title caption' style='font-style: italic;'/>"
          )
          .html(
            (res + "").replace("{0}", "" + ++$this.TableCounter) +
            " - " +
            $(o).attr("summary")
          )).insertAfter($(o));
        else
          $("<div class='image-caption' />").append($(
            "<div class='title caption' style='font-style: italic;'/>"
          )
          .html(
            (res + "").replace("{0}", "" + ++$this.TableCounter)
          )).insertAfter($(o));
          
      });
    });
  }
  public SetBodyClass(className) {
    $("body").removeClass("highlight note");
    if (className) $("body").addClass(className);
    if (className != "note") $("#add-new-note").detach();
  }
  AddBlockToPedagogicalTools(block, contentId) {
    var $this = this;
    $this.PedagogicalBlocks.push(block);
    setTimeout(() => {
      //Initialize Rangy
      if (!window["rangy"]) {
        window["rangy"] = rangy;
        rangy.init();
      }

      if (!$(block).data("rangy")) {
        var r = rangy.createHighlighter(block, "TextRange");
        $(block).data("rangy", r);

        var colors = ["blue", "purple", "yellow", "green"];
        $.each(colors, function (i, o) {
          r.addClassApplier(
            rangy.createClassApplier("highlight-" + o, {
              ignoreWhiteSpace: true,
              tagNames: ["span", "a"],
            })
          );
        });

        //Restore existing Highlights
        var data = $this.GetScormContentData(contentId);
        if (
          data.Highlight &&
          (!$this.PrintRequest ||
            !$this.PrintRequest.Options ||
            $this.PrintRequest.Options.indexOf("Highlights") > -1)
        ) {
          var hl = _.map(data.Highlight.split("|"), function (o) {
            var parts = o.split("$");
            if ((parts[3] || "").indexOf("highlight") == 0 && !parts[4]) {
              //Adjust highlight by 3 when printing -- not sure why
              if (
                $this.PrintRequest &&
                typeof evoPdfConverter !== "undefined"
              ) {
                parts[0] = Number(parts[0]) - 3;
                parts[1] = Number(parts[1]) - 3;
              }

              var page = _.find($this.Pages, function (p) {
                return (
                  _.find(p.Content, function (c) {
                    return c.ObjectID == contentId;
                  }) != null
                );
              });
              /*
                            if(page)
                            {
                                var pageText = $("#page-" + page.ObjectID).text();
                                var contentText = $(block).text();
                                var diff = pageText.split(contentText)[0];
                                var textLength = diff.split("\t").join("").length;
                                parts[0] = Number(parts[0]) + textLength;
                                parts[1] = Number(parts[1]) + textLength;   
                            }
                            return parts.join("$") + "content-" + contentId;
                            */

              if (page && $this.PrintRequest && page.AuxField1 == "intro") {
                var textLength = (
                  $("#page-" + page.ObjectID + " #print-header").text() || ""
                )
                  .split("\t")
                  .join("").length;
                parts[0] = Number(parts[0]) + textLength;
                parts[1] = Number(parts[1]) + textLength;
              }
              return (
                parts.join("$") + (page == null ? "" : "page-" + page.ObjectID)
              );
            }
            return o;
          });

          r.deserialize(hl.join("|"));
        }
        console.debug("highlight load: " + data.Highlight);
        $(block).on("mouseup touchend touchmove", function (e) {
          $(block).trigger("metierselectionchange", e);
        });
        $(block).on("metierselectionchange", function (e) {
          if (!$this.PedagogicalTools) return;

          if ($this.PedagogicalTools.highlight.active) {
            var selection = window.getSelection
              ? window.getSelection()
              : document.selection;
            var fnDoHighlight = function () {
              if ($this.PedagogicalTools.highlight.color == "white")
                //clear
                r.unhighlightSelection();
              else
                r.highlightSelection(
                  "highlight-" + $this.PedagogicalTools.highlight.color
                );

              //De-select
              if (selection) {
                if (selection.removeAllRanges) {
                  selection.removeAllRanges();
                } else if (selection.empty) {
                  selection.empty();
                }
              }

              //Save selections
              var data = $this.GetScormContentData(contentId);
              data.Highlight = r.serialize();
              console.debug("highlight save: " + data.Highlight);
            };

            console.debug(
              "selection length: " + rangy.getSelection().toString().length
            );
            if (rangy.getSelection().toString() != "") {
              if ($this.IsTouchDevice()) {
                setTimeout(function () {
                  console.debug("displaying button");
                  $("#confirm-hl").detach();
                  // " + rangy.getSelection().toString().length + " / " + (new Date()).getSeconds()  +
                  $(
                    "<div id='confirm-hl' style='position:absolute;top:" +
                      (rangy.getSelection().getEndDocumentPos().y + 30) +
                      "px;left:" +
                      rangy.getSelection().getStartDocumentPos().x +
                      "px'><button type='button' style='height:50px;width:100px;background-color:silver;z-index:1000' id='button-confirm-hl'>Marker</button></div>"
                  )
                    .appendTo(document.body)
                    .find("button")
                    .on("mousedown touchstart", function () {
                      fnDoHighlight();
                      $("#confirm-hl").detach();
                    });
                }, 1);
              } else {
                fnDoHighlight();
              }
            } else if ($this.IsTouchDevice()) {
              $(window).trigger("touchend");
            }
          }
        });
      }
      $this.SetElementLoaded("Highlight");
    }, 500);
  }

  AddDictionaryToSection(_Words) {
    if (this.PrintRequest) return;
    var $this = this;
    if (!$this.CourseDictionary) {
      var dictionaryLesson = $.grep(this.CourseData.Folders, function (o) {
        return o.AuxField1 == "dictionary";
      })[0];
      if (
        dictionaryLesson &&
        dictionaryLesson.Content &&
        dictionaryLesson.Content.length > 0
      ) {
        $this.CourseDictionary = JSON.parse(dictionaryLesson.Content[0].Xml);
      }

      if (!$this.CourseDictionary) return;
    }

    $.expr[":"].noparents = function (a, i, m) {
      return $(a).parents(m[3]).length < 1;
    };
    _Words = _Words.filter(":noparents(.no-dict)");
    var expressions = {};
    function fnReplaceStr(s, pairs) {
      //~450ms (40% of processing)
      for (var x = 0; x < pairs.length; x = x + 2) {
        var e = expressions[pairs.length[x]];
        if (!e) {
          e = new RegExp(pairs[x], "gi");
          expressions[pairs.length[x]] = e;
        }
        s = s.replace(e, pairs[x + 1]);
      }
      return s;
    }

    function fnRemoveNonChars(s) {
      return (s + "").replace(/[^ÆØÅæøåA-Za-z0-9]/g, ""); //less than 50ms
    }

    //Returns {"V":lower case cleaned, "O": Original word, "C": is case sensitive dictionary word, "E": is exact dictionary word}
    function fnSingularize(s, c, dictionaryEntry) {
      //s=string, c=caseSensitive
      if (s.O != null) return s;
      var o = {
        O: cStr(s),
        C: 0,
        V: "",
        W: function () {
          if (!this._W) this._W = fnRemoveNonChars(this.O);
          return this._W;
        },
      };
      if (!s) return o;
      if (!dictionaryEntry) s = fnReplaceStr(s, ["!", " "]);
      s = fnReplaceStr(s, [
        '"',
        " ",
        "\\.",
        " ",
        ",",
        " ",
        "-",
        " ",
        "\\?",
        " ",
        " a ",
        "",
        " the ",
        "",
        " en ",
        "",
        " et ",
        "",
        " ei ",
        "",
        " har ",
        "",
        " å ",
        "",
      ]);
      s = $.trim(s);
      if (!s) return "";
      if (s.indexOf(" ") > -1) {
        var t = s.split(" ");
        $.each(t, function (i, o) {
          s = (i == 0 ? "" : s) + fnSingularize(this, c, dictionaryEntry).V;
        });
      } else {
        s = " " + s + " ";
        s = fnReplaceStr(s, [
          "'s ",
          " ",
          "’s ",
          " ",
          "ene ",
          "",
          "et ",
          "",
          "er ",
          "",
          "t ",
          "",
          "e ",
          "",
          "en ",
          "",
          "s ",
          "",
          "ies ",
          "y ",
        ]);

        s = fnRemoveNonChars(s);
      }
      o.V = c ? s : s.toLowerCase();
      if (dictionaryEntry) {
        var markers = ["^", "!"]; //^=Case Sensitive, !=Exact match
        var markerAttributes = ["C", "E"];
        var index = 0;
        while (index > -1 && o.O.length > 1) {
          index = markers.indexOf(o.O.substring(0, 1));
          if (index > -1) {
            o[markerAttributes[index]] = 1;
            o.O = o.O.substring(1);
          }
        }
      }
      return o;
    }
    var start = new Date();

    var _Singularized = [];
    for (var x = 0; x < $this.CourseDictionary.length; x++)
      $this.CourseDictionary[x][0] = fnSingularize(
        $this.CourseDictionary[x][0],
        0,
        true
      );
    for (var x = 0; x < _Words.length; x++) {
      var s = $.trim(cStr(_Words[x].innerText, _Words[x].textContent));
      if (s) {
        s = s.split(String.fromCharCode(160)).join(" ").split(" "); //Replace &nbsp;
        if (s.length == 1) {
          var o = fnSingularize(s[0], 0, false);
          if (!o) continue;
          o.Word = _Words[x];
          _Singularized.push(o);
        } else
          $.each(s, function () {
            var o = fnSingularize(this, 0, false);
            if (!o) return;
            o.Word = _Words[x];
            _Singularized.push(o);
          });
      }
    }
    for (var x = 0; x < _Singularized.length; x++) {
      for (var y = 0; y < $this.CourseDictionary.length; y++) {
        var Match = _Singularized[x].V;
        var Original = _Singularized[x].O;
        if (Match) {
          var Counter = 0,
            WordsMatched = 0;
          while ($this.CourseDictionary[y][0].V.indexOf(Match) == 0) {
            if ($this.CourseDictionary[y][0].V == Match) {
              WordsMatched = Counter;
              while (Counter >= 0) {
                if (
                  $(_Words[x + Counter]).parents(".title").length == 0 &&
                  (!$this.CourseDictionary[y][0].C ||
                    fnSingularize($this.CourseDictionary[y][0].O, 1, null).V ==
                      fnSingularize(_Singularized[x].O, 1, null).V) &&
                  (!$this.CourseDictionary[y][0].E ||
                    $this.CourseDictionary[y][0].W().toLowerCase() ==
                      _Singularized[x].W().toLowerCase())
                ) {
                  if (
                    !(
                      $this.CourseDictionary[y][0].E &&
                      $this.CourseDictionary[y][0].C &&
                      $this.CourseDictionary[y][0].W() != _Singularized[x].W()
                    )
                  ) {
                    $(_Singularized[x + Counter].Word)
                      .data("dict-index", y)
                      .wrap("<span class='dict-entry'></span>");
                  }
                }
                Counter--;
              }
              x += WordsMatched;
              break;
            } else {
              var v = _Singularized[x + ++Counter];
              if (!v) break;
              Match += v.V;
            }
          }
        }
      }
    }

    var _DisplayTimer;
    $(".dict-entry")
      .bind("mouseover", function (e) {
        if (_DisplayTimer) clearTimeout(_DisplayTimer);
        var fnShowDict = function (Element) {
          var D = $(".dict-popup");
          D.detach();
          var E = $(
            "<div class='dict-popup'>" +
              ($this.IsTouchDevice() ? "<div class='close' />" : "") +
              "<div class='modal-content'><div class='modal-body'>" +
              $this.CourseDictionary[Element.data("dict-index")][1] +
              "</div></div></div>"
          ).appendTo($(document.body));
          //var E = $("<div class='dict-popup'><div class='frame'>" + ($this.IsTouchDevice() ? "<div class='close' />" : "") + "<div class='lt'></div><div class='lb'></div><div class='rt'></div><div class='rb'></div><div class='top'></div><div class='bottom'></div><div class='left'></div><div class='right'></div><div class='trunk'></div>" + $this.CourseDictionary[Element.data("dict-index")][1] + "</div></div>").appendTo($(document.body));
          E.css("left", Element.offset().left - 140 + Element.width() / 2)
            .css("top", Element.offset().top - 20 - E.height())
            .data("curr-dict-index", Element.data("dict-index"))
            .find(".close")
            .click(function () {
              $(".dict-popup").detach();
            });
        };
        var Word = $(e.target).is(".dict-entry")
          ? $(e.target).find(".word")
          : $(e.target).is(".word")
          ? $(e.target)
          : null;
        if (Word) fnShowDict(Word);
      })
      .bind("mouseout", function () {
        $(".dict-popup").detach();
      });

    console.log(
      "Dictionary load took " +
        (new Date().getTime() - start.getTime()) +
        " milliseconds"
    );
  }
  // SCORM related

  //TODO: this should get SCORM data from navigator?
  public PopulateScormLesson() {
    var scorm = this.GetScormApi();
    if (scorm.Win) {
      //scorm.Lesson=
      //SetCurrentPage
    }
  }
  public ScormReset() {
    this.ScormData = { Content: {} };
    this.ScormLookupPerformed = false;
    this.ScormAPI = null;
  }
  public ScormInitialize() {
    var scorm = this.GetScormApi();
    var api = scorm.API;
    this.PopulateScormLesson();
    if (!api || (!api.Initialized && api.Initialize("") != "true")) {
      scorm.CreateNewScormSessionFailure = true;
      if (!this.Preview && !this.PrintRequest) {
        //alert("Your data could not be saved. This is probably because you've been logged out.\n\nPlease try clicking 'Save & Close' in the upper right corner, and re-opening the course player again.");
        alert(
          "Your data could not be saved. Please try starting the lesson playback again. You are now being redirected."
        );
        this.SaveAndClose();
      }
      if (!this.PrintRequest) this.ScormData = { Content: {} };
    } else {
      api.Initialized = true;
      this.ScormData = this.ParseSuspendData(this.GetScormSuspendData());
      if (this.GetScormStatus() == "not attempted")
        this.SetScormStatus("incomplete");

      api.SetAutoCommit(true);
    }
  }
  public GetScormValue(Field) {
    var scorm = this.GetScormApi();
    if (scorm.API) return scorm.API.GetValue(Field);
    return scorm.OfflineData[Field];
  }
  public GetScormScore() {
    var scorm = this.GetScormApi();
    return Math.max(
      cInt(this.GetScormValue(scorm.APIFields.SCORE_RAW)),
      cInt(
        this.Lesson && this.Lesson.ScormLesson
          ? this.Lesson.ScormLesson.Score
          : 0
      )
    );
  }
  public GetScormStatus() {
    var scorm = this.GetScormApi();
    return this.GetScormValue(scorm.APIFields.COMPLETION_STATUS);
  }
  public GetScormLearnerName() {
    var scorm = this.GetScormApi();
    return this.GetScormValue(scorm.APIFields.LEARNER_NAME);
  }
  public GetScormLearnerId() {
    var scorm = this.GetScormApi();
    return this.GetScormValue(scorm.APIFields.LEARNER_ID);
  }
  public GetScormSuspendData() {
    var scorm = this.GetScormApi();
    var data = this.GetScormValue(scorm.APIFields.SUSPEND_DATA);
    //Don't return invalid JSON
    if (data != null && data.length > 0 && data.indexOf("{") != 0) return null;
    return data;
  }
  private ParseSuspendData(Data) {
    console.log("ParseSuspendData", Data);
    try {
      return JSON.parse(Data || '{"Content":{}}');
    } catch (e) {}
    try {
      return JSON.parse(
        replaceAll(Data, "\\", "\\\\", "\n", "\\n", "\t", "\\t", "\r", "\\r") ||
          '{"Content":{}}'
      );
    } catch (e) {}
    return { Content: {} };
  }
  public GetUserEmail() {
    var scorm = this.GetScormApi();
    if (scorm && scorm.Win) return scorm.Win.LessonData.UserEmail;
  }
  public SetScormValue(Field, Value) {
    var scorm = this.GetScormApi();

    if (scorm.API && scorm.API.SetValue(Field, Value) == "true") {
      /*scorm.CurrentValues[Field] = Value;
            scorm.PreviousValues = $.extend({},scorm.CurrentValues);*/
    } else if (!scorm.API) {
      scorm.OfflineData[Field] = Value;
    }
  }

  public SetScormScore(Value) {
    Value = Math.round(cInt(Value));
    var scorm = this.GetScormApi();
    if (this.Lesson && this.Lesson["ScormLesson"]) {
      this.Lesson["ScormLesson"].Score = Value;
      this.CalculateCourseStatus();
    }
    return this.SetScormValue(scorm.APIFields.SCORE_RAW, Value);
  }
  public SetScormStatus(Value) {
    var scorm = this.GetScormApi();

    //If setting lesson to completed, and SetFullScoreOnCompleteIfNoQuiz==true, set score to 100
    if (
      Value == "completed" &&
      this.PlayerSettings.Quiz.SetFullScoreOnCompleteIfNoQuiz &&
      _.find(this.Pages, { AuxField1: "quiz" }) == null
    )
      this.SetScormScore(100);

    if (this.Lesson && this.Lesson["ScormLesson"]) {
      this.Lesson["ScormLesson"].Status = Value.toUpperCase().substring(0, 1);
      this.CalculateCourseStatus();
    }
    return this.SetScormValue(scorm.APIFields.COMPLETION_STATUS, Value);
  }
  public SetScormSuspendData(Value) {
    var scorm = this.GetScormApi();
    return this.SetScormValue(scorm.APIFields.SUSPEND_DATA, Value);
  }
  public GetNumberOfFinalTestAttempts() {
    if (this.PlayerSettings.Quiz.MaxFinalTestAttempts > 0)
      return this.PlayerSettings.Quiz.MaxFinalTestAttempts;
    return 3;
  }

  public GetScormApi() {
    var $this = this;
    if (this.ScormLookupPerformed) return this.ScormAPI;
    this.ScormAPI = {
      OfflineData: {},
      APIFields: {
        SESSION_TIME: "cmi.session_time",
        LOCATION: "cmi.location",
        SCORE_RAW: "cmi.score.raw",
        SUSPEND_DATA: "cmi.suspend_data",
        COMPLETION_STATUS: "cmi.completion_status",
        ENTRY: "cmi.entry",
        LEARNER_NAME: "cmi.learner_name",
        LEARNER_ID: "cmi.learner_id",
        CREDIT: "cmi.credit",
        TOTAL_TIME: "cmi.total_time",
      },
    };
    this.ScormLookupPerformed = true;

    var fnSearch = function (Scorm, win) {
      if ($this.Preview) return false;
      while (!win.API_1484_11 && win.parent && win.parent != win)
        win = win.parent;
      if (win.API_1484_11) {
        Scorm.Win = win;
        Scorm.API = win.API_1484_11;

        return true;
      }
      return false;
    };

    fnSearch(this.ScormAPI, window);
    if (this.ScormAPI.API == null && window.parent && window.parent != window)
      fnSearch(this.ScormAPI, window.parent);
    if (this.ScormAPI.API == null && window.top.opener)
      fnSearch(this.ScormAPI, window.top.opener);
    if (this.ScormAPI.API == null && window.LessonData)
      this.ScormAPI.Win = window;

    return this.ScormAPI;
  }

  public GetScormContentData(ContentId, ReturnNullIfNotSet = false) {
    if (!this.ScormData) this.ScormData = {};
    if (!this.ScormData.Content) this.ScormData.Content = {};
    var data = this.ScormData.Content[ContentId];
    if (!data) {
      if (ReturnNullIfNotSet) return null;

      console.log("GetScormContentData - creating data for " + ContentId);
      data = {};
      this.ScormData.Content[ContentId] = data;
    }
    return data;
  }

  public GetScormContentDataByType(Type) {
    var matches = [];
    for (var prop in this.ScormData.Content) {
      if (this.ScormData.Content[prop][Type])
        matches.push(this.ScormData.Content[prop][Type]);
    }
    return matches;
  }
  public GetScormContentDataByTypeForPage(Pages, Type) {
    var matches = [];
    var $this = this;
    var pages = Array.isArray(Pages) ? Pages : [Pages];
    _.each(pages, function (p) {
      $.each(p.Content, function (i, o) {
        if (
          $this.ScormData.Content[o.ObjectID] &&
          $this.ScormData.Content[o.ObjectID][Type]
        )
          matches.push($this.ScormData.Content[o.ObjectID][Type]);
      });
    });
    return matches;
  }

  public ScormCommit() {
    var $this = this;

    console.log("ScormCommit");
    //return new Promise(function(resolve, reject) { resolve()})
    //	.then(function (){
    var scorm = $this.GetScormApi();
    var api = scorm.API;
    if (api) {
      $this.SetScormSuspendData(JSON.stringify($this.ScormData));
      if (api.Commit() != "true") {
        alert(
          "Could not save your progress.\n\nPlease try clicking 'Save & Close' in the upper right corner, and re-opening the course player again."
        );
      }
      clearTimeout($this.ScormKeepAliveTimer);
      $this.ScormKeepAliveTimer = setTimeout(function () {
        api.MetierKeepAlive();
      }, 20 * 60 * 1000);
    }
    //$this.Shared.ClearSpinner("Scorm");
    //}).catch(function() {
    //$this.Shared.ClearSpinner("Scorm");
    //});
  }

  public ScormTerminate() {
    var $this = this;

    console.log("ScormTerminate");
    var scorm = $this.GetScormApi();
    var api = scorm.API;
    if (api) {
      api.Terminate();
      scorm.Win.iCurrentRCO = -1;
      clearTimeout($this.ScormKeepAliveTimer);
    }
  }

  //Course Score and progress
  public GetRcoIdForApi() {
    return ((this.GetScormApi() || {}).Win || {}).iCurrentRCO || 0;
  }
  InitiateNewScormSession() {
    console.log("InitiateNewScormSession - entry");
    var $this = this;
    var scorm = $this.GetScormApi();
    var api = scorm.API;
    if (
      api &&
      $this.Lesson &&
      $this.Lesson.ScormLesson &&
      $this.Lesson.ScormLesson.RCOID != $this.GetRcoIdForApi()
    ) {
      if ($this.GetRcoIdForApi() != -1) {
        $this.ScormCommit();
        api.Terminate();
      }
      var rcoId = $this.Lesson.ScormLesson.RCOID;
      console.log("InitiateNewScormSession - starting. RcoId: " + rcoId);
      return $this.ScormCreate(rcoId).subscribe((scorm) => {
        console.log(scorm);
        console.log("ScormInitialize --- ");
        $this.ScormInitialize();
        console.log("CalculateCourseStatus --- ");
        $this.CalculateCourseStatus();
        debugger;
        console.log("InitiateNewScormSession - done");
      });
    }
    //Successful
  }
  ScormCreate(RcoId) {
    var $this = this;
    clearTimeout($this.ScormKeepAliveTimer);
    var oldScorm = $this.GetScormApi();
    if (!oldScorm.Win) oldScorm.Win = window;
    //Make sure we don't call this again while waiting for Ajax call to finish
    //window["iCurrentRCO"] = RcoId;
    oldScorm.Win.iCurrentRCO = RcoId;
    if (this.IsDemo) {
      $this.ScormReset();
      var scorm = new clsScorm2004Local("", RcoId);
      if (!scorm.GetValue("cmi.completion_status"))
        scorm.SetValue("cmi.completion_status", "incomplete");
      oldScorm.Win["API_1484_11"] = scorm;
      return new Observable((observer) => {
        observer.next();
        observer.complete();
      });
    }
    const headers = new HttpHeaders().set(
      "Content-Type",
      "text/plain; charset=utf-8"
    );
    return $this.HttpClient.post(
      $this.PhoenixApiUrl + "/scorm/create?rcoId=" + RcoId,
      { headers, responseType: "text" }
    ).pipe(
      map((response) => {
        console.log("Completed creation");
        console.log(response);
        var scormSessionId = response.toString().split('"').join("");
        $this.ScormReset();
        var scorm = new clsScorm2004API($this.PhoenixApiUrl + "/scorm", RcoId);
        scorm.ScormSessionId = scormSessionId;
        oldScorm.Win["API_1484_11"] = scorm;
        $this.ScormKeepAliveTimer = setTimeout(function () {
          scorm.MetierKeepAlive();
        }, 20 * 60 * 1000);
        console.log(scorm);
        return scorm;
      }),
      catchError((response) => of($this.HandleHttpError(response)))
    );
  }

  ScormCreateSynchronous(RcoId) {
    var $this = this;
    var oldScorm = $this.GetScormApi();
    //Make sure we don't call this again while waiting for Ajax call to finish
    //window["iCurrentRCO"] = RcoId;
    oldScorm.Win.iCurrentRCO = RcoId;
    var _error = false;
    var request = $.ajax({
      url:
        "./../player/scorm.aspx?url=" +
        encodeURIComponent("create?rcoId=" + RcoId),
      cache: false,
      type: "post",
      async: false,
      error: function (oRequest, sStatus, oError) {
        _error = true;
        alert(
          "An error occurred initiating attempt on server. Output: " +
            sStatus +
            " (" +
            oError +
            ")"
        );
      },
    });
    if (_error) return;
    if (request.responseText.split("-").length < 5) {
      alert(
        "The returned value does not look like a valid GUID. Response from SCORM webservice was: " +
          request.responseText
      );
      return;
    }
    var scormSessionId = request.responseText.split('"').join("");
    $this.ScormReset();
    var scorm = new clsScorm2004API($this.PhoenixApiUrl + "/scorm", RcoId);
    scorm.ScormSessionId = scormSessionId;
    oldScorm.Win["API_1484_11"] = scorm;
    return scorm;
  }

  // todo: - refactor
  private HandleHttpError(Error: Response | any) {
    // let errMsg: string;
    // if (Error instanceof Response) {
    //   const body = Error || '';

    //   if(body.ValidationErrors && body.ValidationErrors.length>0)
    //       errMsg = body.ValidationErrors.join("\n");
    //     else{
    //       const err = body.error || JSON.stringify(body);
    //       errMsg = `${Error.status} - ${Error.statusText || ''} ${err}`;
    //     }
    // } else {
    //   errMsg = Error.message ? Error.message : Error.toString();
    // }
    // console.error(errMsg);
    // this.Shared.ClearSpinners();
    // return Observable.throw(errMsg);
    const errMsg = Error.message ? Error.message : Error.toString();

    console.error(errMsg);
    this.Shared.ClearSpinners();
    throwError(errMsg);
  }
  CalculateCourseStatus() {
    debugger;
    console.log("CalculateCourseStatus");
    var $this = this;
    Object.assign(this, {
      CompletedLessons: 0,
      TotalLessons: 0,
      CourseScore: 0,
      CourseTotalScore: 0,
      Lessons: [],
    });
    if (this.CourseData) {
      $.each(this.CourseData.Folders, function (i, o) {
        if (o.AuxField1 != "finaltest" && o.Folders) $this.Lessons.push(o);
        if (o.ScormLesson && o.AuxField1 != "finaltest") {
          if (o.ScormLesson.Status == "C" && o.ScormLesson.Type == "lesson")
            $this.CompletedLessons++;
          $this.TotalLessons++;
          $this.CourseScore += cInt(o.ScormLesson.Weight * o.ScormLesson.Score);
          $this.CourseTotalScore += o.ScormLesson.Weight * 100;
        }
      });
    }
  }

  //Handle Element loading cycle
  public SetCurrentPage(PageId) {
    //this.ScormData.CurrentPage=PageId;
    //Load notes
    this.PageLoadStatus = { Notes: false, Content: false, Highlight: false };
  }
  private LoadUnloadedElements() {
    var $this = this;
    if (!this.IsElementLoaded("Notes") && this.IsElementLoaded("Content")) {
      var contents = this.GetScormContentDataByTypeForPage(
        this.GetPagesToRender(),
        "Notes"
      );
      this.SetElementLoaded("Notes");

      //Delay loading in case there's multiple page elements on page
      setTimeout(function () {
        if (
          !$this.PrintRequest ||
          !$this.PrintRequest.Options ||
          $this.PrintRequest.Options.indexOf("Notes") > -1
        ) {
          for (var x = 0; x < contents.length; x++) {
            for (var id in contents[x]) {
              var note = contents[x][id];
              $this.AddNote(note.ContentId, note.WordIndex, id);
            }
          }
        }
        $(window).resize();
      }, 500);
    }
  }
  public SetElementLoaded(Type) {
    if (this.PageLoadStatus[Type] == true) return;
    this.PageLoadStatus[Type] = true;
    this.LoadUnloadedElements();
  }
  public IsElementLoaded(Type) {
    return this.PageLoadStatus[Type] == true;
  }

  public SaveAndClose() {
    var $this = this;
    $this.Shared.AddSpinner("Unload");
    var scorm = $this.GetScormApi();
    var api = scorm.API;

    //If RcoId == -1, we already called terminate
    if ($this.GetRcoIdForApi() == -1) api = null;
    if (api) $this.ScormCommit();
    window.onbeforeunloadPrompt = false;

    setTimeout(function () {
      if (api) {
        api.Terminate();
        scorm.Win.iCurrentRCO = -1;
      }
      $this.Unload();
      $this.ExpireParticipationData();
      clearTimeout($this.ScormKeepAliveTimer);
      if ($this.CurrentActivityId == 0) $this.GotoUrl("/portal/home", false);
      else
        $this.GotoUrl(
          "/portal/elearning/" +
            ($this.IsDemo ? "demo/" : "") +
            $this.CurrentActivityId,
          false
        );
      $this.Shared.ClearSpinner("Unload");
    }, 50);
  }

  ExpireParticipationData() {
    this.StorageService.remove("ParticipationData-Date");
  }

  private AddPagesToPage(pageToAddPagesTo, lessonToProcess) {
    var $this = this;
    var pageTypes = $.extend(true, [], $this.PrintRequest.PageTypes);

    //Remove ExcludedPageTypes
    if ($this.PrintRequest.ExcludedPageTypes)
      _.remove(lessonToProcess.Folders, function (o) {
        return $this.PrintRequest.ExcludedPageTypes.indexOf(o.AuxField1) > -1;
      });

    //Create quizAnswers pages
    if (lessonToProcess.AuxField1 != "finaltest") {
      var quizPages = _.filter(lessonToProcess.Folders, { AuxField1: "quiz" });
      if (pageTypes.indexOf("quizAnswers") > -1 && quizPages.length > 0) {
        _.each(quizPages, function (p) {
          if (pageTypes.indexOf("quiz") > -1) {
            p = $.extend(true, {}, p);
            lessonToProcess.Folders.push(p);
          }
          _.each(p.Content, function (c) {
            c.ShowAnswers = true;
          });
        });
      }
    } else if (lessonToProcess.AuxField1 == "finaltest") {
      //If not printing final tests, exit
      if (
        !_.some(pageTypes, function (o) {
          return o.indexOf("FinalTest") > -1;
        })
      )
        return;
      if (pageTypes.indexOf("quiz") == -1) pageTypes.push("quiz");

      //If not printing all, remove index 1+
      if (
        !_.some(pageTypes, function (o) {
          return o.indexOf("allFinalTest") > -1;
        })
      )
        lessonToProcess.Folders.splice(1, 999);

      var quizPages = _.filter(lessonToProcess.Folders, { AuxField1: "quiz" });
      if (
        (pageTypes.indexOf("firstFinalTestAnswers") > -1 ||
          pageTypes.indexOf("allFinalTestAnswers") > -1) &&
        quizPages.length > 0
      ) {
        _.each(quizPages, function (p) {
          if (
            pageTypes.indexOf("firstFinalTest") > -1 ||
            pageTypes.indexOf("allFinalTests") > -1
          ) {
            p = $.extend(true, {}, p);
            lessonToProcess.Folders.push(p);
          }
          _.each(p.Content, function (c) {
            c.ShowAnswers = true;
          });
        });
      }
    }

    //firstFinalTest,firstFinalTestAnswers,allFinalTests,allFinalTestAnswers

    //Remove page types not selected for printing
    _.remove(lessonToProcess.Folders, function (o) {
      return (
        o &&
        (o.AuxField1 == "splash" ||
          o.AuxField1 == "complete" ||
          (pageTypes.length > 0 &&
            !_.some(pageTypes, function (p) {
              return p.indexOf(o.AuxField1) > -1;
            })))
      );
    });

    while (lessonToProcess.Folders.length > 0) {
      var currentPage = lessonToProcess.Folders[0];
      if (currentPage.Content) {
        _.remove(currentPage.Content, { Mode: "hide" });
        for (
          var contentIndex = 0;
          contentIndex < currentPage.Content.length;
          contentIndex++
        ) {
          //Force header display
          if (contentIndex == 0) {
            currentPage.Content[contentIndex].Mode = "";
            currentPage.Content[contentIndex].Name = currentPage.Name;
            if (pageToAddPagesTo.Content.length > 0)
              currentPage.Content[contentIndex].PageBreakBefore = true;
          }
          if (
            $this.PrintRequest.BaseUrl &&
            currentPage.Content[contentIndex].Xml
          )
            currentPage.Content[contentIndex].Xml = currentPage.Content[
              contentIndex
            ].Xml.split('src="https://mymetier.net/learningportal').join(
              'src="' + $this.PrintRequest.BaseUrl
            );

          pageToAddPagesTo.Content.push(currentPage.Content[contentIndex]);
          if (currentPage.AuxField1 == "intro")
            currentPage.Content[contentIndex].LessonName = lessonToProcess.Name;
        }
      }
      _.pull(lessonToProcess.Folders, currentPage);
      //_.last(pageToAddPagesTo.Content).PageBreakAfter = true;
    }
  }
  public GetPagesToRender() {
    if (this.PrintRequest != null && this.PrintRequest.Source != "CMS") {
      //Do we need to generate PagesToPrint?
      if (!this.PagesToPrint && this.CourseData) {
        var pageTypes = _.clone(this.PrintRequest.PageTypes);
        if (this.PrintRequest.Command == "Checklist") pageTypes = ["checklist"];
        if (!pageTypes) pageTypes = [];

        this.PagesToPrint = _.clone(this.Pages);

        //Remove page types not selected for printing
        _.remove(this.PagesToPrint, function (o) {
          return (
            o.AuxField1 == "splash" ||
            o.AuxField1 == "complete" ||
            (pageTypes.length > 0 && pageTypes.indexOf(o.AuxField1) == -1)
          );
        });
      }
      return this.PagesToPrint || [];
    }

    return this.Page ? [this.Page] : [];
  }
  // Lesson and page loading
  public GetLesson(LessonId) {
    var $this = this;
    if (
      LessonId &&
      (this.DownloadedLessonId != LessonId || !$this.CourseData)
    ) {
      this.DownloadedLessonId = LessonId;
      this.Shared.AddSpinner("Content");

      //this.PrintRequest.Source = "CMS";
      //            this.PrintRequest.FromLesson = 9;
      //            this.PrintRequest.ToLesson = 99;
      return this.HttpClient.get(
        $this.LearningPortalApiUrl + "/player/course/" +
          this.CourseId +
          "/lesson/" +
          LessonId +
          "?Printing=" +
          (this.PrintRequest != null) +
          (this.PrintRequest && this.PrintRequest.Source == "CMS"
            ? "&IncludeAllLessons=true"
            : "")
      ).subscribe((sub: any) => {
        console.log("Downloaded lesson: " + LessonId);
        var course = sub;

        //If printing from CMS, Add all content to a single page. Consider moving this to GetPagesToRender()
        if ($this.PrintRequest != null && this.PrintRequest.Source == "CMS") {
          var currentLesson = course.Folders[$this.PrintRequest.FromLesson];
          if (currentLesson != null) {
            var startIndex = course.Folders.indexOf(currentLesson);

            if (this.PrintRequest.Source == "CMS")
              var endIndex =
                $this.PrintRequest.ToLesson -
                $this.PrintRequest.FromLesson +
                startIndex;
            else var endIndex = startIndex;

            var page = {
              Content: [],
              ObjectID: $this.PageId,
              AuxField1: "theory",
            };
            for (var i = startIndex; i <= endIndex; i++) {
              if (course.Folders[i] && course.Folders[i].Folders)
                $this.AddPagesToPage(page, course.Folders[i]);
            }

            currentLesson.Folders = [page];
            currentLesson.ObjectID = LessonId;
            $this.PageId = currentLesson.Folders[0].ObjectID;
            course.Folders = [currentLesson];
          }
        }

        if ($this.PrintRequest && $this.PrintRequest.LanguageCode)
          course.LanguageCode = $this.PrintRequest.LanguageCode;

        //_.remove(course.Folders,{AuxField1:"finaltest"});

        $this.PrepareCourse(course);
        $this.CourseData = course;
        $this.LanguageCode = course.LanguageCode || "en";
        try {
          $this.PlayerSettings = JSON.parse(course.Settings);
        } catch (e) {
          $this.PlayerSettings = {};
        }
        if (!$this.PlayerSettings.Player) $this.PlayerSettings.Player = {};
        if (!$this.PlayerSettings.Quiz) $this.PlayerSettings.Quiz = {};

        if ($this.LanguageCode == "no") $this.LanguageCode = "nb";

        //$this.TranslateService.setDefaultLang($this.LanguageCode);
        $this.TranslateService.use($this.LanguageCode);
        console.log("Setting language " + $this.LanguageCode);

        $this.LessonChanged();
        $this.CalculateCourseStatus();
        if (
          $this.Lesson &&
          $this.Lesson.Folders &&
          !$this.PrintRequest &&
          !$this.Preview
        )
          $this.SplashPage = $.grep($this.Lesson.Folders, function (o) {
            return o && o.AuxField1 == "splash";
          })[0];
        $this.Shared.ClearSpinner("Content");

        setTimeout(function () {
          if (typeof evoPdfConverter !== "undefined")
            evoPdfConverter.startConversion();
        }, 1000);
      });
    } else if ($this.CourseData) {
      $this.LessonChanged();
    } else {
      alert(
        "LessonId was not supplied, so I don't know what course you want to play. The full url is: " +
          window.location
      );
    }
  }
  private LessonChanged() {
    this._CourseChange.next(this.CourseData);
    this.ActivatePage();

    this.InitiateNewScormSession();
    console.log("LessonChanged");
  }
  public PrepareCourse(parent) {
    var $this = this;
    if (parent == null) {
      parent = $this.CourseData;
      $this.Dialogs = [];
    }
    if (parent.Folders)
      $.each(parent.Folders, function (i, o) {
        o.Parent = parent;
        o.ScormLesson = $this.GetSCORMLesson(o.ObjectID, null);
        if (o.ScormLesson) console.log("GetSCORMLesson - " + o.ScormLesson);
        $this.PrepareCourse(o);
      });
    if (parent.Content) {
      $.each(parent.Content, function (i, o) {
        o.Parent = parent;
        if (
          $this.Preview &&
          window.localStorage &&
          o.ObjectID == window.localStorage["CMS:ContentId"]
        ) {
          o.Xml = window.localStorage["CMS:Content"];
          window.localStorage["CMS:ContentId"] = "";
        }
      });
      if (parent.AuxField1 == "global") {
        $this.Dialogs = $.grep($this.Dialogs, function (o) {
          return o.Name != parent.Name;
        });
        $this.Dialogs.push(parent);
      }
    }
  }

  private GetSCORMLesson = function (LessonId, PageId) {
    var scorm = this.GetScormApi();
    var lessons = scorm.Win ? scorm.Win.LessonData : null;
    if (lessons && lessons.Lessons) {
      //Handling of lessons where URL does not point to a CMS object (ie. mohive, where we can't use RCOID/ContentId to locate the SCORM lesson)
      if (!!LessonId) {
        var l = _.find(lessons.Lessons, { Id: LessonId });
        return l;
      }
      for (var x = 0; x < lessons.Lessons.length; x++) {
        var o = lessons.Lessons[x];
        var url = o.Url;
        if (url) {
          var sTest =
            url.indexOf("/page/") > -1
              ? "/page/" + PageId
              : url.indexOf("/lesson/") > -1
              ? "/lesson/" + LessonId
              : url;
          if (url.indexOf(sTest) > -1) {
            return o;
          }
        }
      }
    }
  };
  public ActivatePage() {
    var $this = this;
    if (!$this.CourseData) {
      console.log("ActivatePage failed, since CourseData is null!");
      return;
    }
    if ($this.PageId) {
      var page = $this.FindPage($this.PageId, null);
      console.log("activatepage - PageId: " + $this.PageId, page);

      $this.Page = page;
      $this.Pages = page.Parent.Folders;
      $this.Lesson = page.Parent;
    } else if ($this.LessonId) {
      var lesson = $this.FindPage($this.LessonId, null);
      if (lesson) {
        $this.Lesson = lesson;
        $this.Page = lesson.Folders[0];
        $this.Pages = lesson.Folders;
        $this.PageId = $this.Page.ObjectID;
        console.log("activatelesson - PageId: " + $this.PageId, lesson);
      }
    }
    //Strip out unsupported page types
    if ($this.Pages)
      $this.Pages = $.grep($this.Pages, function (o) {
        return (
          o.AuxField1 != "splash" &&
          o.AuxField1 != "global" &&
          o.AuxField1 != "dictionary"
        );
      });
    if ($this.Page && $this.Pages && $this.Page.AuxField1 == "splash")
      $this.Page = $this.Pages[0];

    //Strip out hidden content
    if ($this.Page)
      $this.Page.Content = $.grep($this.Page.Content, function (o) {
        return o.Mode != "hide";
      });

    //Todo: if scorm is not initialized for $this.PlayerService.lessonid, initialize

    $this.SetCurrentPage($this.PageId);
  }
  public GetLessonIndex() {
    var $this = this;
    if (this.LessonId && this.CourseData && this.CourseData.Folders) {
      var lesson = $.grep(this.CourseData.Folders, function (o) {
        return o.ObjectID == $this.LessonId;
      })[0];
      if (lesson) return this.CourseData.Folders.indexOf(lesson);
    }
  }
  public GetCourseCode() {
    if (this.CourseData) return this.CourseData.CourseCode;
  }

  /* Notes support */
  public AddNote(ContentId, WordIndex, Id) {
    if (!Id) Id = this.GenerateGuid();
    var scormData = this.GetScormContentData(ContentId);
    if (!scormData.Notes) scormData.Notes = {};
    var scormNote = scormData.Notes[Id];
    var isNew = !scormNote;
    if (!scormNote) {
      scormNote = { Text: "" };
      scormData.Notes[Id] = scormNote;
    }
    var $this = this;
    var element = $("[data-id=" + ContentId + "]").find(
      ".word:eq(" + WordIndex + ")"
    )[0];
    if (element == null) $("[data-id=" + ContentId + "] *:eq(0)")[0];
    var note = $(
      '<div class="user-note" id="' +
        Id +
        '"><div class="mover"><i class="material-icons note">note</i><i class="material-icons close">close</i></div><textarea autosize placeholder="Type here..."></textarea></div>'
    )
      .insertBefore(element)
      .find("textarea")
      .val(scormNote.Text)
      .change(function () {
        scormNote.Text = $(this).val();
        console.log(JSON.stringify($this.ScormData));
      })
      .end()
      .find(".close")
      .click(function () {
        $this.DeleteNote($(this).parents(".user-note"));
      })
      .end();
    scormNote.WordIndex = WordIndex;
    scormNote.ContentId = ContentId;
    note.data("scormNote", scormNote).on("mousedown touchstart", function (e) {
      if (
        (e.which == 1 && $(e.target).is(".mover")) ||
        (e.type == "touchstart" && !$(e.target).is(".close"))
      ) {
        $(document).data("dragging-note", note);
      }
    });
    if (isNew)
      setTimeout(function () {
        note.find("textarea").focus();
      }, 500);
    return note;
  }
  public DeleteNote(Note) {
    var scormNote = $(Note).data("scormNote");
    var scormData = this.GetScormContentData(scormNote.ContentId);
    if (scormData.Notes) delete scormData.Notes[$(Note).attr("id")];
    $(Note).detach();
  }

  GetNearestWord(y) {
    var StartX = $(".container").offset().left;
    var Width = $(".container").width();
    var checker = function (y) {
      for (var x = StartX; x < StartX + Width; x = x + 10) {
        var element = document.elementFromPoint(x, y);
        if (element && element.className.indexOf("word") > -1) return element;
      }
    };
    var element = null;
    var InitialY = y;
    while (
      y < InitialY + 400 &&
      (!element || element.className.indexOf("word") == -1)
    ) {
      element = checker(y);
      y = y + 15;
    }
    return element;
  }

  // General public functions
  public SendMessage(Message) {
    this.Shared.AddSpinner("Contact");
    var scorm = this.GetScormApi();
    var win = scorm.Win || window;
    Message.To =
      Message.To ||
      cStr(
        win
          ? cStr(win.LessonData.EcoachEmail, win.LessonData.DistributorEmail)
          : "",
        "kurssporsmal@metier.no"
      );
    Message.From = Message.From || (scorm.Win ? this.GetUserEmail() : null);
    Message.Subject =
      Message.Subject ||
      "Question from course participant!" +
        (this.Lesson
          ? " Lesson: " +
            this.Lesson["Name"] +
            " / Course: " +
            this.CourseData.Name
          : "");
    fnSendEmail(
      Message.To,
      Message.From || "kurssporsmal@metier.no",
      Message.Subject,
      false,
      "Participant: " + Message.From + "\nQuestion: " + Message.Body
    );
    this.Shared.ClearSpinner("Contact");
  }
  public CreateDownload(Request) {
    var $this = this;
    //Append language
    if (Request) Request.LanguageCode = $this.TranslateService.currentLang;
    $this.Shared.AddSpinner("Download");
    return this.HttpClient.post("../api/player/print", Request)
      .pipe(timeout(60000 * 3))
      .toPromise()
      .then(function (sub) {
        $this.Shared.ClearSpinner("Download");
        var response = sub;
        /*
                if(response.Url) 
                {
                    var unloadPrompt = window.onbeforeunloadPrompt;
                    window.onbeforeunloadPrompt = false;
                    window.location.href=response.Url;					
                    window.onbeforeunloadPrompt = unloadPrompt;
                }
                */
        return response;
      });
  }

  public IsTouchDevice() {
    return /iPhone|iPod|iPad|Android|Windows Phone/.test(navigator.userAgent);
  }

  public GenerateGuid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return (
      s4() +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      s4() +
      s4()
    );
  }
}

function of(arg0: void): any {
  throw new Error("Function not implemented.");
}
