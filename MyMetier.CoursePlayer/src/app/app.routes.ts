import { Routes } from '@angular/router';
import { PlayerComponent } from './player/player/player.component';
import { PortalBaseExam2Component, PortalCatalogComponent, PortalClassroomComponent, PortalElearningComponent, PortalElearningDemoComponent, PortalExamComponent, PortalHomeComponent, PortalLoginComponent, PortalMetierplusComponent, PortalMockexamAttemptsComponent, PortalMockexamComponent, PortalMockexamConfirmComponent, PortalMockexamExamResultComponent, PortalMockexamQuestionsComponent, PortalMockexamScenarioComponent, PortalMockexamSummaryComponent, PortalPageComponent, PortalProfileComponent, PortalReportsComponent, PortalSeminarComponent, PortalWebinarComponent } from './portal';
import { CompletedCoursesComponent } from './portal/completed-courses/completed-courses.component';
import { UserDiplomasComponent } from './portal/user-diplomas/user-diplomas.component';

export const ROUTES: Routes = [
	{ path: 'portal', component: PortalPageComponent , children:[

		{ path: 'login', component: PortalLoginComponent },
		{ path: 'home', component: PortalHomeComponent },
		{ path: 'profile', component: PortalProfileComponent },
		{ path: 'completed', component: CompletedCoursesComponent },
		{ path: 'userdiplomas', component: UserDiplomasComponent },
		{ path: 'catalog', component: PortalCatalogComponent },
		{ path: 'reports', component: PortalReportsComponent },
		{ path: 'plus', component: PortalMetierplusComponent },
		{ path: 'elearning/:CourseId', component: PortalElearningComponent },
		{ path: 'elearning/demo/:CourseId', component: PortalElearningDemoComponent },
		{ path: 'classroom/:CourseId', component: PortalClassroomComponent },
		{ path: 'webinar/:CourseId', component: PortalWebinarComponent },
		{ path: 'seminar/:CourseId', component: PortalSeminarComponent },
		{ path: 'mockexam/:CourseId', component: PortalMockexamComponent }, 
		{ path: 'mockexam/:CourseId/play', component: PortalBaseExam2Component, children:
		[
			{ path: 'questions/:ExamId', component: PortalMockexamQuestionsComponent }, 		
			{ path: 'review/:ExamId', component: PortalMockexamQuestionsComponent }, 		
			{ path: 'summary/:ExamId', component: PortalMockexamSummaryComponent }, 		
			{ path: 'confirm/:ExamId', component: PortalMockexamConfirmComponent }, 		
			{ path: 'attempts/:ExamId', component: PortalMockexamAttemptsComponent }, 		
			{ path: 'result/:ExamId', component: PortalMockexamExamResultComponent }, 		
			{ path: 'scenario/:ExamId', component: PortalMockexamScenarioComponent }, 		
		]		
		}, 
		{ path: 'exam/:CourseId', component: PortalExamComponent }

	] },	
	{ path: 'course/:CourseId/page/:PageId', component: PlayerComponent },
	{ path: 'course/:CourseId/lesson/:LessonId', component: PlayerComponent },
	{ path: 'course/:CourseId/lesson/:LessonId/page/:PageId', component: PlayerComponent },

	{ path: 'course/demo/:CourseId/page/:PageId', component: PlayerComponent },
	{ path: 'course/demo/:CourseId/lesson/:LessonId', component: PlayerComponent },
	{ path: 'course/demo/:CourseId/lesson/:LessonId/page/:PageId', component: PlayerComponent },

	{ path: '**', redirectTo: '/portal/login'}
];