# MyMetier CoursePlayer

**Frontend for MyMetier Course-player.**  

The application is written with Angular and Typescript. 

Current Angular-version is 9. 

This project uses npm as package manager. 

Requires [NodeJS](https://nodejs.org/en/) v. 8.9.0 and NPM to be served locally.  
Node Version Manager for [Windows](https://github.com/coreybutler/nvm-windows) or [Linux](https://github.com/nvm-sh/nvm) is recomended for easy switching between different node-versions.

## Getting started
### Running the project


Clone the project: 

```bash
    git clone git@bitbucket.org:metier/behind.git
```


To run the application locally on your computer, start by installing dependenices in the *MyMetier.Courseplayer*-folder:

```bash
    npm install
```


It is defined two scripts in package.json to run the applicaton. Start the server by either running:

```bash
    npm run start:dev
```

or 

```bash
    npm run start:staging
```

`npm run start:dev` - here you need backend served localy on your computer.
`npm run start:staging` - wich uses the staging-environment as the backend. 

The simpelest way to run the application is by running the `npm run start:staging`-script as you do not need to have the backend setup locally. 
See *package.json* for other available scipts.

### Build & deploy

Update the version number in the package.json file and commit the changes. 
Run the `build`-script: 

```bash
    npm run start:staging
```

The build artifacts will be stored in the `dist/` directory. 
The dist directory then needs to be replaced on the respective server in the stage or prod-environment. 
The dist directory is located under 'learningportal/portal2/'.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.
## Further help

To get more help on the `angular-cli` use `ng help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
